﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class VisorGrillaVentas
    Inherits System.Web.UI.Page
    Dim sc As New ScriptManagerClass
    Dim objutil As New Util
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim iReporte As String = Request.QueryString("iReporte")

            Me.lblTitulo.Width = 50
            lblTitulo.ForeColor = Drawing.Color.Black
            lblTitulo.Font.Size = 20
            lblTitulo.Height = 40
            lblTitulo.BackColor = Drawing.Color.SkyBlue


            If iReporte Is Nothing Then
                iReporte = ""
            End If

            ViewState.Add("iReporte", iReporte)
            Select Case iReporte


                Case "1" 'Comparativo anual - compra
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("idMoneda", Request.QueryString("idMoneda"))
                    ViewState.Add("persona", Request.QueryString("persona"))
                    ViewState.Add("ano", Request.QueryString("ano"))
                    ViewState.Add("idtipo", Request.QueryString("idtipo"))

                Case "2"
                    ViewState.Add("fechaInicio", Request.QueryString("fechaInicio"))
                    ViewState.Add("fechaFin", Request.QueryString("fechaFin"))
                    ViewState.Add("empresa", Request.QueryString("empresa"))
                    ViewState.Add("tienda", Request.QueryString("tienda"))
                    ViewState.Add("perfil", Request.QueryString("perfil"))
                    ViewState.Add("rol", Request.QueryString("rol"))
                    ViewState.Add("usuario", Request.QueryString("usuario"))

            End Select
        End If

        mostrarReporte()
    End Sub
    Private Sub mostrarReporte()
        Select Case ViewState.Item("iReporte").ToString

            Case "1"
                ReporteComparativoAnualVentas()
            Case "2"
                ReporteComisiones()

        End Select
    End Sub
    Private Sub ReporteComisiones()
        Try

            Dim ds As DataSet = (New Negocio.ComparativoVentasAnual).reporteComisiones(CStr(ViewState("fechaInicio")), CStr(ViewState("fechaFin")), CInt(ViewState("empresa")), CInt(ViewState("tienda")), CInt(ViewState("perfil")), CInt(ViewState("rol")), CInt(ViewState("usuario")))
            If ds IsNot Nothing Then

                Me.lblTitulo.Text = "REPORTE DE COMISIONES"

                Me.dgwComparativoAnualCompras.DataSource = ds.Tables(0)
                Me.dgwComparativoAnualCompras.DataBind()

                'For Each row As GridViewRow In dgwComparativoAnualCompras.Rows
                '    'total Sublinea
                '    If row.Cells(0).Text = "999999" Then
                '        row.BackColor = Drawing.Color.SkyBlue
                '        row.Cells(0).Text = ""
                '    End If
                'Next


                Dim Nombre As String
                Nombre = "COMISIONES.xls"
                Me.ExportaExcel(Me.dgwComparativoAnualCompras, Nombre)

            End If
        Catch ex As Exception
            Response.Write("<script>alert('" + ex.Message + "');</script>")
        End Try

    End Sub

    Private Sub ReporteComparativoAnualVentas()
        'Me.txtvariable.Text = "dc_Importe"

        Try
            Dim var As String
            If CStr(ViewState.Item("idtipo")) = "" Or CStr(ViewState.Item("idtipo")) = "dc_Cantidad" Then
                var = "dc_Cantidad"
            Else
                var = "dc_Importe"
            End If
            Dim ds As DataSet = (New Negocio.ComparativoVentasAnual).ComparativoVentasAnual(CInt(ViewState("ano")), var, CInt(ViewState("persona")), CInt(ViewState("idMoneda")), CInt(ViewState("IdEmpresa")))

            If ds IsNot Nothing Then

                Me.lblTitulo.Text = "COMPARATIVO ANUAL DE VENTAS"

                Me.dgwComparativoAnualCompras.DataSource = ds
                Me.dgwComparativoAnualCompras.DataBind()

                For Each row As GridViewRow In dgwComparativoAnualCompras.Rows
                    'total Sublinea
                    If row.Cells(0).Text = "999999" Then
                        row.BackColor = Drawing.Color.SkyBlue
                        row.Cells(0).Text = ""
                    End If

                Next
                Dim Nombre As String
                Nombre = "ComparativoAnualdeventas.xls"
                Me.ExportaExcel(Me.dgwComparativoAnualCompras, Nombre)

                'Response.Clear()
                'Response.Buffer = True
                'Response.AddHeader("content-disposition", "attachment;filename=ComparativoAnualdeCompras.xls")
                'Response.Charset = ""
                'Response.ContentType = "application/vnd.ms-excel"

            End If
        Catch ex As Exception
            Response.Write("<script>alert('" + ex.Message + "');</script>")
        End Try


    End Sub

    Public Sub ExportaExcel(ByVal grilla As GridView, ByVal FilleNameExt As String)

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=" + FilleNameExt)
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"

        Response.ContentEncoding = System.Text.Encoding.UTF8

        Dim strWriter As System.IO.StringWriter
        strWriter = New System.IO.StringWriter
        Dim htmlTextWriter As System.Web.UI.HtmlTextWriter
        htmlTextWriter = New System.Web.UI.HtmlTextWriter(strWriter)

        grilla.RenderControl(htmlTextWriter)

        EnableViewState = False

        Response.Write(strWriter.ToString)
        grilla.Dispose()

        Response.End()

    End Sub
End Class