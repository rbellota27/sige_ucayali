﻿<%@ Page Title="" Language="vb" EnableViewState="false" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="frmReporteStockSucursales.aspx.vb" Inherits="APPWEB.frmReporteStockSucursales" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<div>
    <table>
    <tr class="TituloCelda">
        <td colspan="6">REPORTE TOP 8 SEMANAS RETAIL.</td>
    </tr>
    <tr>
        <td class="Texto">-Empresa:</td>
        <td colspan=4>
        <%--<asp:DropDownList ID="ddlEmpresa" runat="server"></asp:DropDownList>--%>
        <div id="divEmpresa">            
            </div>
        </td>        
    </tr>
    <tr>
        <td class="Texto">Tienda:</td>
        <td>
        <%--<asp:DropDownList ID="ddlTienda" runat="server"></asp:DropDownList>--%>
            <div id="divTienda">              
            </div>
        </td>
        <td class="Texto" bgcolor="White">&nbsp;</td>
        <td><asp:TextBox ID="txtFecInicio" runat="server" Enabled="false" 
                BorderColor="White" ForeColor="#E4E4E4" Height="16px" Width="16px"></asp:TextBox>
            <cc1:MaskedEditExtender ID="txtFecInicio_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFecInicio">
                                </cc1:MaskedEditExtender>
        </td>
        <td class="Texto">Fec. Final</td>
        <td><asp:TextBox ID="txtFecFin" runat="server" Enabled="false"></asp:TextBox>
            <cc1:MaskedEditExtender ID="txtFecFin_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFecFin">
                                </cc1:MaskedEditExtender>
        </td>
    </tr>
    <tr>
        <td class="Texto">
            Agregar almacen (comparativo):
        </td>
        <td colspan="2">
            <%--<asp:DropDownList ID="ddlAlmacenComparativo" runat="server"></asp:DropDownList>--%>
            <div id="divAlmacen">
                
            </div>
        </td>
        <td colspan="3">
            <asp:Button ID="btnAgregarAlmacen" runat="server" Text="Agregar Almacen" 
                Enabled="false" Visible="False" Width="121px" />
            <input id="btnReponer" type="submit" value="Visualizar" 
                onclick="return enviarDatos(this);"  onclick="return btnReponer_onclick()" 
                onclick="return btnReponer_onclick()" onclick="return btnReponer_onclick()" 
                onclick="return btnReponer_onclick()" style="width: 124px" />&nbsp;
            <%--<input id="btnExportarToExcel" type="button" value="Exportar .xls" onclick="return(ExportToExcel());" name="btnExportarToExcel" />--%>
            <asp:Button ID="btnPruebas" runat="server" Text="Exportar Datos" />
        </td>
    </tr>
    <tr>
        <td colspan="6">
            <div>
                <span style="font-weight:bold;color:Blue">NRO DE PRODUCTOS PROCESADOS:</span><span style="font-weight:bold;color:Red" id="lblContador"></span><br />
                <span style="font-weight:bold;color:Blue">NRO DE PRODUCTOS MARCADOS PARA REPONER:</span><span style="font-weight:bold;color:Red" id="lblReponer"></span>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div id="divEspera" style="display:none;text-align:center;position:fixed;left:0px;top:0px;width:100%;height:100%;background-color:White;">
                <h1>Espera un momento...</h1>
                <img src="../../Imagenes/loading.gif" width="400" height="200" alt="" />
            </div>            
            <asp:GridView ID="gv_AgregarAlmacen" runat="server" AutoGenerateColumns="false" 
                Width="254px">
                <RowStyle CssClass="GrillaRow" />
                <HeaderStyle CssClass="GrillaHeader" />
                <Columns>
                    <asp:BoundField DataField="campo1" HeaderText="Id" />
                    <asp:BoundField DataField="campo2" HeaderText="Almacen" />
                </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>
</div>
<div id="datos" style="overflow:scroll;width:100%;height:600px;position:absolute" >
</div>
<iframe id="txtArea1" style="display:none"></iframe>
<script language="javascript" type="text/javascript">
    //variables globales
    var nroFilasGlobal;
    var filas;
    var matriz = [];
    window.onload = cargarFormulario();
    function cargarFormulario() {
        var xhr = new XMLHttpRequest();
        var idUsuario = '<%=session("IdUsuario") %>';
        xhr.open("get", "frmReporteStockSucursales.aspx?idUsuario=" + idUsuario, true);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                crearcomboTienda(xhr.responseText);
            }
        }
        xhr.send();
        return false;
    }

    function crearcomboTienda(cadenaData) {
        //.split("*")[0]
        var divTienda = document.getElementById("divTienda");
        var divEmpresa = document.getElementById("divEmpresa");
        var divAlmacen = document.getElementById("divAlmacen");
        cadenaData = cadenaData.split("*")
        for (var h = 0; h < cadenaData.length; h++) {
            var filas = cadenaData[h].split(";");
            var nroFilas = filas.length;
            var contenido = "";
            var columnas = filas[0].split("|");
            var nroColumnas = columnas.length;
            var item;
            if (h == 0) { contenido += '<select id="cboTienda">'; }
            if (h == 1) { contenido += '<select id="cboEmpresa">'; }
            if (h == 2) { contenido += '<select id="cboAlmacen">'; }

            for (var i = 0; i < nroFilas; i++) {
                item = filas[i].split("|");
                for (var j = 0; j < nroColumnas; j++) {
                    if (i == 0) {
                        contenido += '<option selected="selected" value=';
                    } else {
                        contenido += '<option value=';
                    }

                    contenido += '"' + item[j] + '"';
                    j++;
                    contenido += '>'
                    contenido += item[j];
                    contenido += "</option>";
                }
            }
            contenido += "</select>";
            if (h == 0) { divTienda.innerHTML = contenido; }
            if (h == 1) { divEmpresa.innerHTML = contenido; }
            if (h == 2) { divAlmacen.innerHTML = contenido; }
        }

    }
    //    var xhr = new XMLHttpRequest();
    //    var idUsuario = <%=session("idUsuario") %>;
    //    xhr.open("get", "frmReponerStockGeneral.aspx?idUsuario=" + idUsuario, true);
    //    xhr.onreadystatechange = function () {
    //            if (xhr.readyState == 4 && xhr.status == 200) {
    //                fun_lista(xhr.responseText);
    //            }
    //        }
    //        xhr.send();
    //        return false;

    function esperarMientrasBusca() {
        var espera = document.getElementById("divEspera");
        espera.style.display = "block";
    }

    function enviarDatosCoti(nombreGrupo) {
        var grilla = document.getElementById("tabReposicion");

        var cb = [];
        var n = 0, cuales = "";
        var cadena1 = "";
        var cadena2 = "";
        cb = document.getElementsByName(nombreGrupo);
        //alert('El total de checkbox de name = ' + nombreGrupo + ' es: ' + cb.length);
        for (var i = 0; i < cb.length; i++) {
            var e = parseInt(i);

            if (cb[i].checked == true) {
                //                if (n < 25) 
                //                    cadena1 += cb[i].value + '|' + grilla.rows[e + 1].cells[1].children[0].value + '▼';
                //                }
                //valido que la cantidad ingresada sea númerica y número entero
                var valor = grilla.rows[e + 1].cells[1].children[0].value;
                var codigoSige = grilla.rows[e + 1].cells[2].innerHTML;
                if (isNaN(valor)) {
                    alert("Está intentando pasar un producto de código " + codigoSige + " con valor que no es numérico.");
                    return false;
                }
                //validacion para aceptar numeros enteros
                var patron = /^\d*$/;//Expresión regular para aceptar solo números enteros
                if (!patron.test(valor)) {
                    alert("El valor ingresado para el código " + codigoSige + " no es un número entero.");
                    return false;
                }
                
                //se le suma 2 al e porque representa que me la grilla tiene dos filas de cabecera para este caso solo sumare 1
                cadena2 += cb[i].value + '|' + grilla.rows[e + 1].cells[1].children[0].value + '▼';

                //alert('Valor del checkbox ' + (e + 1) + ': ' + cb[i].value + ' y su cantidad es: ' + grilla.rows[e + 1].cells[1].children[0].value);
                cuales += cb[i].value + ' ';
                n++;
            }
        } // fin loop
        if (n == 0) {
            var mensaje = ' no hubo selección';
            alert(mensaje);
            return false;
        }
        //        alert(cadena1.length);
        //        alert(cadena2.length);
        var data = new FormData();
        data.append('flag', 'C');
        data.append('datos2', cadena2.substr(0, cadena2.length - 1));

        var xhr = new XMLHttpRequest();
        //var nroCaracteres = cadena.length;
        //cadena = cadena.substr(0, parseInt(nroCaracteres) - 1).toString();
        //xhr.open('GET', "frmReponerStockGeneral.aspx?flag=C&datos2=" + cadena2.substr(0, cadena2.length - 1), true);
        xhr.open('POST', "frmReporteStockSucursales.aspx", true);
        //var parametros = "flag=C&datos2 =" + cadena2.substr(0, cadena2.length - 1);
        //xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        //xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                alert(xhr.responseText);
            }
        }
        xhr.send(data);
        return false;
        //alert(cadena);
        //        if (n == 0) {
        //            var mensaje = ' no hubo selección';
        //        } else {
        //            var xhr = new XMLHttpRequest();
        //            xhr.open("get", "frmReponerStockGeneral.aspx?flag=C&datos=" + cadena, true);
        //            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        //            xhr.onreadystatechange = function () {
        //                if (xhr.readyState == 4 && xhr.status == 200) {
        //                    alert(xhr.responseText);
        //                }
        //            }
        //            xhr.send()
        //            return false;
        //        }
        //        } else {
        //            var mensaje = 'Se marcaron el/los checkbox de valor: ' + cuales
        //        }
        //alert('El total de checkbox marcados es: ' + n);
        //alert(mensaje);
        //document.getElementById('boxes').reset();
        //return false;
    }
    function enviarDatos(objeto) {
        var espera = document.getElementById("divEspera");
        var txtFechaInicio = document.getElementById("<%=txtFecInicio.ClientID %>").value;
        var txtFechaFin = document.getElementById("<%=txtFecFin.ClientID %>").value;
        var tienda = document.getElementById("cboTienda");
        var idTienda = tienda.options[tienda.selectedIndex].value;

        var empresa = document.getElementById("cboEmpresa");
        var idEmpresa = empresa.options[empresa.selectedIndex].value;

        var div = document.getElementById("datos");

        var xhr = new XMLHttpRequest();
        xhr.open("get", "frmReporteStockSucursales.aspx?flag=G&idTienda=" + idTienda + "&idEmpresa=" + idEmpresa + "&txtInicio=" + txtFechaInicio + "&txtFin=" + txtFechaFin, true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.onloadstart = function () { espera.style.display = "block"; }
        xhr.onloadend = function () { espera.style.display = "none"; }
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                fun_lista(xhr.responseText);
            }
        }
        xhr.send();
        return false;
    }

    function fun_lista(lista) {
        filas = lista.split(",");
        crearTabla();
        crearMatriz();
        mostrarMatriz();
        configurarFiltros();
    }

    function crearTabla() {
        var nRegistros = filas.length;
        var cabeceras = ["", "", "", "CÓDIGO", "PRODUCTO", "s1", "s2", "s3", "s4", "s5", "s6", "s7", "s8","VentaTotalSemana","PromVenta", "#Alm. Tienda", "#Alm. Central", "Cobertura", "max", "min","Linea","SubLinea","Pais"];

        
        var nCabeceras = cabeceras.length;
        var contenido = '<table id="tabReposicion" style="border:1px solid blue"><thead>';
        //Crear la Fila con la Cabecera
        contenido += "<tr class='GrillaHeader'>";
        for (var j = 0; j < nCabeceras; j++) {
            if (j == 0 || j == 1) { contenido += "<th></th>"; }
            if (j != 2 && j != 0 && j != 1) {
                contenido += "<th>";
                //contenido += cabeceras[j];
                contenido += "<input type='text' class='texto' style='width:95%' placeholder='" + cabeceras[j] + "'/>";
                contenido += "</th>";
            }
        }
        contenido += "</tr></thead><tbody id='tbDocumentos' class='GrillaRow'></tbody>";
        contenido += "</table>";
        var div = document.getElementById("datos");
        div.innerHTML = contenido;
    }

    function crearMatriz() {
        matriz = [];
        var nRegistros = filas.length;
        var nCampos;
        var campos;
        var c = 0;
        var exito;
        var textos = document.getElementsByClassName("texto");
        var nTextos = textos.length;
  
        var texto;
        var x;
        for (var i = 0; i < nRegistros; i++) {
            exito = true;
            campos = filas[i].split("|");
            nCampos = campos.length; //los 3 que resto son los 3 ultimos cammpos pues no se verán
            if (i != 0) { // si la fila es igual a 0 entonces oculto la fila de cabecera
                for (var j = 0; j < nTextos; j++) {
                    texto = textos[j];
                    x = j + 2;
                    if (x < 10) {
                        if (isNaN(campos[x])) {
                            exito = campos[0].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                        } else {
                            exito = campos[x].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                        }
                    }
                    if (!exito) break;
                } //aquí termina el for de columnas
                if (exito) {
                    matriz[c] = [];
                    for (var j = 0; j < nCampos; j++) {
                            if (isNaN(campos[j])) matriz[c][j] = campos[j];
                            else matriz[c][j] = campos[j] * 1;
                    }
                    c++;
                } //aquí termina el flag de exito
            }//aquí termina el flag de i
        }//aquí termina el for de filas
    }

    function mostrarMatriz() {
        var nRegistros = matriz.length;
        var contenido = "";
        if (nRegistros > 0) {
            var nCampos = matriz[0].length;
            for (var i = 0; i < nRegistros; i++) {
                contenido += "<tr class='GrillaRow'>";
                for (var j = 0; j < nCampos; j++) {
                    if (j == 0) {
                        //Inserta Checkbox y textbox
                        //valido que la cantidad a reponer sea de tipo numerico para que se marque por defecto el checkbox
                        if (isNaN(parseFloat(matriz[i][25]))) {
                            contenido += '<td><input id="CheckSelect" onclick="contador(' + nRegistros + ')" type="checkbox" name="check_reponer" value=' + '' + matriz[i][0] + '|' + matriz[i][1] + '' + ' /></td>';
                        } else {
                            contenido += '<td><input id="CheckSelect" onclick="contador(' + nRegistros + ')" type="checkbox" name="check_reponer" value=' + '' + matriz[i][0] + '|' + matriz[i][1] + '' + ' checked="checked" /></td>';
                        }

                        //Crea Textbox con el valor de la reposición
                        contenido += '<td><input id="txtCantidadReponer" type="text" value=' + '' + matriz[i][25] + '' + ' style="width:40px" /></td>';
                        j = j + 1;
                    } else {
                        contenido += "<td>";
                        contenido += matriz[i][j];
                        contenido += "</td>";
                    }

                }
                contenido += "</tr>";
            }
            
        }
        var spnMensaje = document.getElementById("lblContador");
        spnMensaje.innerHTML = "<b>Total de Registros: " + (nRegistros) + "</b>";
        var tabla = document.getElementById("tbDocumentos");
        tabla.innerHTML = contenido;
        contador(nRegistros);
    }

    function configurarFiltros() {
        var textos = document.getElementsByClassName("texto");
        var nTextos = textos.length;
        var texto;
        for (var j = 0; j < nTextos; j++) {
            texto = textos[j];
            texto.onkeyup = function () {
                crearMatriz();
                mostrarMatriz();
            }
        }
    }
//    function crearTabla(filas) {

//        var nroFilas = filas.length;
//        var columnas = ["", "", "CÓDIGO", "PRODUCTO", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""];
//        var nroColumnas = columnas.length;
//        var item;

//        var nroColumnas;
//        var contenido = '<table id="tabReposicion" style="border:1px solid blue"><thead  class="GrillaHeader">';
//        contenido += "<tr>";
//        for (var h = 0; h < nroColumnas; h++) {
//            if (h == 4) {
//                contenido += '<th colspan="15" style="text-align:center">Semana de Ventas</th>';
//            } else {
//                if (h < 12) {
//                    contenido += '<th></th>';
//                }
//            }
//        }
//        contenido += "</tr>";
//        for (var i = 0; i < nroFilas; i++) {
//            contenido += "<tr>";
//            item = filas[i].split("|");
//            for (var j = 0; j < nroColumnas; j++) {
//                if (i == 0) { //recorro la fila 0 que es la cabecera
//                    if (j == 0) { //verifico si es la primera columna
//                        //contenido += '<th><input id="CheckSelect" type="checkbox" /></th>';
//                        contenido += '<th></th>';
//                        contenido += '<th>Cant. Reponer</th>';
//                        //Llena la columna con el código del producto
//                        if (j != 0 && j != 1) {
//                            contenido += "<th>";
//                            contenido += item[j];
//                            contenido += "</th>";
//                        }
//                    } else {
//                        if (j != 0 && j != 1) {
//                            contenido += "<th>";
//                            contenido += "<input type='text' class='texto' style='width:95%' placeholder='" + columnas[j] + "'/>";
//                            contenido += "</th>";
//                        }
//                    }
//                } else {
//                    if (j == 0) {
//                        //Inserta Checkbox y textbox
//                        //valido que la cantidad a reponer sea de tipo numerico para que se marque por defecto el checkbox
//                        if (isNaN(parseFloat(item[24]))) {
//                            contenido += '<td><input id="CheckSelect" onclick="contador(nroFilasGlobal)" type="checkbox" name="check_reponer" value=' + '' + item[0] + '|' + item[1] + '' + ' /></td>';
//                        } else {
//                            contenido += '<td><input id="CheckSelect" onclick="contador(nroFilasGlobal)" type="checkbox" name="check_reponer" value=' + '' + item[0] + '|' + item[1] + '' + ' checked="checked" /></td>';
//                        }
//                        contenido += '<td><input id="txtCantidadReponer" type="text" value=' + '' + item[24] + '' + ' style="width:40px" /></td>';
//                        //Llena la columna con el código del producto
//                        if (j != 0 && j != 1) {
//                            if (j >= 4 && j <= 18) {
//                                contenido += '<td style="width:auto>';
//                            } else { contenido += '<td style="width:auto">'; }
//                            contenido += item[j];
//                            contenido += "</td>";
//                        }
//                    } else {
//                        if (j != 0 && j != 1) {
//                            contenido += '<td style="width:auto">';
//                            contenido += item[j];
//                            contenido += "</td>";
//                        }
//                    }
//                }
//            }
//            if (i == 0) {
//                contenido += "</tr>";
//                contenido += "</thead>";
//                contenido += "<tbody class='GrillaRow'>";
//            } else {
//                contenido += "</tr>";
//            }
//        }
//        contenido += "</table>";
//        var divDatos = document.getElementById("datos");
//        divDatos.innerHTML = contenido;
//        nroFilasGlobal = nroFilas;
//        contador(nroFilasGlobal);
//    }

    function contador(nroFilas) {
        var contador = document.getElementById("lblContador");
        var contadorReponer = document.getElementById("lblReponer");

        var cb = [];
        cb = document.getElementsByName("check_reponer");
        var n = 0;
        for (var i = 0; i < cb.length; i++) {
            var e = parseInt(i);
            if (cb[i].checked == true) {
                n++;
            }
        }

        contador.innerHTML = nroFilas - 1;
        contadorReponer.innerHTML = n;
        return false;
    }

    function ExportToExcel() {
//        var tabla = document.getElementById('tabReposicion');
//        var nroFila = tabla.rows.length;
//        var columna;
//        //        for (var i = 0; i < nroFila; i++) {
//        //            columna = tabla.rows[i].cells[0].value;
//        //        }
//        var html = tabla.outerHTML.replace(/<input[^>]*>|<\/input>/gi, "");
//        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
//        var tienda = document.getElementById("cboTienda");
//        var nomTienda = tienda.options[tienda.selectedIndex].text;
        //var tabla = document.getElementById('tabReposicion');
        //Creamos un Elemento Temporal en forma de enlace
        var tmpElemento = document.createElement('a');
        // obtenemos la información desde el div que lo contiene en el html
        // Obtenemos la información de la tabla
        var data_type = 'data:application/vnd.ms-excel';
        var tabla_div = document.getElementById('tabReposicion');
        var tabla_html = tabla_div.outerHTML.replace(/<input[^>]*>|<\/input>/gi, "");
        
        tmpElemento.href = data_type + ', ' + encodeURIComponent(tabla_html);        
        //Asignamos el nombre a nuestro EXCEL
        tmpElemento.download = "Reposicion.xls";
        // Simulamos el click al elemento creado para descargarlo
        tmpElemento.click();


//        var ua = window.navigator.userAgent;
//        var msie = ua.indexOf("MSIE ");

//        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
//        {
//            txtArea1.document.open("txt/html", "replace");
//            txtArea1.document.write(tab_text);
//            txtArea1.document.close();
//            txtArea1.focus();
//            sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
//        }
//        else                 //other browser not tested on IE 11
//            sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));

//        return (sa);
    }




</script>
</asp:Content>