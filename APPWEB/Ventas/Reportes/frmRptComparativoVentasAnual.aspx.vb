﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Imports ADODB
Imports Microsoft.Office.Interop
Imports System.Runtime.InteropServices.Marshal
Imports System.Data.DataTable

Partial Public Class frmRptComparativoVentasAnual
    Inherits System.Web.UI.Page
    Dim objcombo As New Combo
    Private objscript As New ScriptManagerClass
    Private objNegUsu As New Negocio.UsuarioView

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            inicializarForm()
        End If
    End Sub
    Private Sub inicializarForm()
        objcombo.LlenarCboPropietario(Me.cmbEmpresa, True)
        objcombo.LlenarCboMonedaBase(Me.CmbMoneda, True)

        Dim objFechaActual As New Negocio.FechaActual
        txtano.Text = Format(objFechaActual.SelectFechaActual, "yyyy")
        Me.hddFechaActual.Value = CStr(Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy"))

    End Sub

    Private Sub DGV_BuscarPersona_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_BuscarPersona.SelectedIndexChanged
        addPersona(CInt(DGV_BuscarPersona.SelectedRow.Cells(1).Text))
        txtRazonSocial.Text = DGV_BuscarPersona.SelectedRow.Cells(2).Text
        Me.txtRuc.Text = DGV_BuscarPersona.SelectedRow.Cells(4).Text
        Me.txtdni.Text = DGV_BuscarPersona.SelectedRow.Cells(3).Text
        txtCodigoCliente.Text = DGV_BuscarPersona.SelectedRow.Cells(1).Text
        'Session("idPropietario") = DGV_BuscarPersona.SelectedRow.Cells(1).Text
        'llamar al reporte

    End Sub
    Private Sub LLenarCliente(ByVal cliente As Entidades.PersonaView)

        'Me.txtCodigoCliente.Text = CStr(cliente.IdPersona)
        If cliente.RazonSocial <> "" Then
            txtRazonSocial.Text = cliente.RazonSocial
            Me.cboTipoPersona.SelectedIndex = 2 'Jurirdica
        Else
            txtRazonSocial.Text = cliente.Nombre
            Me.cboTipoPersona.SelectedIndex = 1 'Natural
        End If

        Me.txtdni.Text = cliente.Dni
        Me.txtRuc.Text = cliente.Ruc

        Dim NegPersonaTipoAgente As New Negocio.PersonaTipoAgente
        Dim TipoAgente As New Entidades.TipoAgente
        TipoAgente = NegPersonaTipoAgente.SelectIdAgenteTasaxIdPersona(cliente.IdPersona)
        'Cargar la Linea de Crédito Disponible
    End Sub

    Private Sub addPersona(ByVal IdPersona As Integer)
        Try

            Dim objCliente As Entidades.PersonaView = (New Negocio.Cliente).SelectxId(IdPersona)

            Me.LLenarCliente(objCliente)

            '************ Por el momento NO actualizamos la lista de Precios
            'If Me.cboTipoPrecioV.SelectedValue <> "0" Then
            '    Actualizarlistapreciosdetalle()
            'End If 

            '********* Para volver a calcular los totales para efectos si es agente de percepcion o Retención.
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "addPersona_Venta();", True)

        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, "El Cliente no existe.")
        End Try
    End Sub

    Private Sub LimpiarBuscarCliente()
        txtRazonSocial.Text = ""
        Me.txtDni.Text = ""
        Me.txtRuc.Text = ""

    End Sub
    Private Sub BuscarClientexDni()
        Me.LimpiarBuscarCliente()
        Dim obj As New Negocio.Cliente
        Dim Cliente As New Entidades.PersonaView
        Try
            'Cliente = obj.SelectxDni(Me.txtBuscarCliente.Text.Trim)
            Me.LLenarCliente(Cliente)
        Catch ex As Exception
            Dim sc As New ScriptManagerClass
            sc.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub BuscarClientexRuc()
        Me.LimpiarBuscarCliente()
        Dim obj As New Negocio.Cliente
        Dim Cliente As New Entidades.PersonaView
        Try
            'Cliente = obj.SelectxRuc(Me.txtBuscarCliente.Text.Trim)
            Me.LLenarCliente(Cliente)
        Catch ex As Exception
            Dim sc As New ScriptManagerClass
            sc.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub BuscarClientexId(ByVal IdCliente As Integer)
        Me.LimpiarBuscarCliente()
        Dim obj As New Negocio.Cliente
        Dim Cliente As New Entidades.PersonaView
        Try
            Cliente = obj.SelectxId(IdCliente)
            Me.LLenarCliente(Cliente)
        Catch ex As Exception
            'MsgBox1.ShowMessage(ex.Message)
            Dim sc As New ScriptManagerClass
            sc.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosPersonaGrilla(ByVal grilla As GridView, ByVal texto As String, ByVal ruc As String, ByVal dni As String, ByVal opcion As Integer, ByVal tipoMov As Integer)
        Try
            '********** opcion 0: descripcion
            '********** opcion 1: ruc
            '********** opcion 2: dni
            Dim index As Integer = 0
            Select Case tipoMov
                Case 0 '**************** INICIO
                    index = 0
                Case 1 '**************** Anterior
                    index = (CInt(txtPageIndex_Persona.Text) - 1) - 1
                Case 2 '**************** Posterior
                    index = (CInt(txtPageIndex_Persona.Text) - 1) + 1
                Case 3 '***************** IR
                    index = (CInt(txtPageIndexGO_Persona.Text) - 1)
            End Select

            Dim listaPV As List(Of Entidades.PersonaView) = (New Negocio.PersonaView).SelectActivoxPersonaRucDni(texto, ruc, dni, opcion, grilla.PageSize, index)

            If listaPV.Count <= 0 Then
                objscript.mostrarMsjAlerta(Me, "No se hallaron registros.")
            Else
                grilla.DataSource = listaPV
                grilla.DataBind()
                txtPageIndex_Persona.Text = CStr(index + 1)
            End If

        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, "Problemas en la carga de datos Persona.")
        End Try
    End Sub

    Private Sub btnBuscarPersona_Grilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarPersona_Grilla.Click
        ViewState.Add("TextoBuscarPersona", txtBuscarPersona_Grilla.Text)
        ViewState.Add("Ruc", Me.txtBPruc.Text)
        ViewState.Add("Dni", Me.txtBPDni.Text)
        ViewState.Add("OpcionBuscarPersona", Me.rblpersona.SelectedValue)
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, txtBuscarPersona_Grilla.Text, txtBPruc.Text, txtBPDni.Text, CInt(rblpersona.SelectedValue), 0)

        'txtPageIndex_Persona.Text = "1"
        'txtPageIndexGo_Persona.Text = "1"
    End Sub

    Private Sub DGV_BuscarPersona_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGV_BuscarPersona.PageIndexChanging
        'DGV_BuscarPersona.PageIndex = e.NewPageIndex
        'cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, txtBuscarPersona_Grilla.Text, CInt(Me.cmbFiltro_BuscarPersona.SelectedValue))
    End Sub
    Private Sub btnAnterior_Persona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Persona.Click
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, CStr(ViewState("TextoBuscarPersona")), CStr(ViewState("Ruc")), CStr(ViewState("Dni")), CInt((ViewState("OpcionBuscarPersona"))), 1)
    End Sub
    Private Sub btnPosterior_Persona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Persona.Click
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, CStr(ViewState("TextoBuscarPersona")), CStr(ViewState("Ruc")), CStr(ViewState("Dni")), CInt((ViewState("OpcionBuscarPersona"))), 2)
    End Sub
    Private Sub btnIr_Persona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Persona.Click
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, CStr(ViewState("TextoBuscarPersona")), CStr(ViewState("Ruc")), CStr(ViewState("Dni")), CInt((ViewState("OpcionBuscarPersona"))), 3)
    End Sub


End Class