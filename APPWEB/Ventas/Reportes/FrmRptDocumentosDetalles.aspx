<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmRptDocumentosDetalles.aspx.vb" Inherits="APPWEB.FrmRptDocumentosDetalles" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%">
        <tr>
            <td align="center " class="TituloCelda">
                Detalle de Documentos de Ventas
            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 100%">
                    <tr>
                        <td align="center">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="center">
                                                <table>
                                                    <tr>
                                                        <td class="Label">
                                                            Empresa:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cmbEmpresa" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Label">
                                                            Tienda:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cmbtienda" runat="server" Enabled="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <table>
                                                    <tr>
                                                        <td class="Label">
                                                            Documento:
                                                        </td>
                                                        <td>
                                                        <asp:DropDownList ID="cmdTipoDocumento" runat="server">														
																<asp:ListItem Text="Factura Electr�nica" Value="1101353001"></asp:ListItem>
																<asp:ListItem Text="Boleta Electr�nica" Value="1101353002"></asp:ListItem>
                                                                <asp:ListItem Text="Factura/Boleta" Value="0" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="Factura" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="Boleta" Value="3"></asp:ListItem>
                                                                <asp:ListItem Text="Cotizacion" Value="14"></asp:ListItem>
                                                                <asp:ListItem Text="Recibo Ingreso" Value="17"></asp:ListItem>
                                                                <asp:ListItem Text="Recibo Egreso" Value="18"></asp:ListItem>
                                                                <asp:ListItem Text="Nota Credito" Value="4"></asp:ListItem>
                                                                <asp:ListItem Text="Nota Debito" Value="5"></asp:ListItem>
                                                                <asp:ListItem Text="Orden Compra" Value="16"></asp:ListItem>
                                                                <asp:ListItem Text="Orden de Pedido" Value="15"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Label">
                                                            Serie:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtSerie" onKeypress="return(onKeyPressEsNumero('event'));" runat="server"
                                                                MaxLength="4" Width="60px"></asp:TextBox>
                                                        </td>
                                                        <td class="Label">
                                                            Codigo:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCodigo" onKeypress="return(onKeyPressEsNumero('event'));" runat="server"
                                                                MaxLength="12" Width="80px"></asp:TextBox>
                                                        </td>
                                                        <td class="Label">
                                                            Moneda Emision:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="CmdMoneda" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Label">
                                                            Estado:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cmdEstado" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <fieldset>
                                <legend>
                                    <asp:RadioButtonList ID="rdbFiltrarFechaSemana" runat="server" RepeatDirection="Horizontal"
                                        CssClass="Texto">
                                        <asp:ListItem Text="Fecha" Value="1" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Semana" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Ninguno" Value="0"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </legend>
                                <table>
                                    <tr>
                                        <td class="Label">
                                            Fecha Inicio:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFechaInicio" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                                TargetControlID="txtFechaInicio">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td class="Label">
                                            Fecha Fin:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFechaFin" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                                TargetControlID="txtFechaFin">
                                            </cc1:CalendarExtender>
                                        </td>
                                    </tr>
                                </table>
                                <hr />
                                <table>
                                    <tr>
                                        <td class="Label_fsp">
                                            <asp:Label ID="lblDe" runat="server" Text="DE:"></asp:Label>
                                        </td>
                                        <td class="Label_fsp">
                                            <asp:Label ID="lblanoi" runat="server" Text="A�o:"></asp:Label>
                                            <asp:DropDownList ID="idYearI" runat="Server" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <asp:Label ID="lblSemanai" runat="server" Text="Semana:"></asp:Label>
                                            <asp:DropDownList ID="idSemanaI" runat="Server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFechaInicioSemanaI" runat="server" Width="70px" onFocus=" return (onFocus_ReadOnly(this)); "
                                                onBlur=" return ( valFecha_Blank(this) ); " CssClass="TextBox_Fecha"></asp:TextBox>
                                        </td>
                                        <td class="Texto">
                                            -
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFechaFinSemanaI" runat="server" Width="70px" onBlur=" return ( valFecha_Blank(this) ); "
                                                onFocus=" return (onFocus_ReadOnly(this)); " CssClass="TextBox_Fecha"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Label_fsp">
                                            <asp:Label ID="lblHasta" runat="server" Text="HASTA:"></asp:Label>
                                        </td>
                                        <td class="Label_fsp">
                                            <asp:Label ID="lblanof" runat="server" Text="A�o:"></asp:Label>
                                            <asp:DropDownList ID="idYearF" runat="Server" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <asp:Label ID="lblSemanaf" runat="server" Text="Semana:"></asp:Label>
                                            <asp:DropDownList ID="idSemanaF" runat="Server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFechaInicioSemanaFin" runat="server" Width="70px" onBlur=" return ( valFecha_Blank(this) ); "
                                                onFocus=" return (onFocus_ReadOnly(this)); " CssClass="TextBox_Fecha"></asp:TextBox>
                                        </td>
                                        <td class="Texto">
                                            -
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFechaFinSemanafin" runat="server" Width="70px" onBlur=" return ( valFecha_Blank(this) ); "
                                                onFocus=" return (onFocus_ReadOnly(this)); " CssClass="TextBox_Fecha"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td valign="top">
                                        <fieldset title="Agrupado Por:" class="FieldSetPanelReport">
                                            <legend>Agrupado Por: </legend>
                                            <asp:RadioButtonList ID="rbtlTpReporte" runat="server" RepeatDirection="Horizontal"
                                                Width="206px" Style="margin-bottom: 0px">
                                                <asp:ListItem Text="Cliente" Value="1" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="Fecha Emision" Value="2"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </fieldset>
                                    </td>
                                    <td valign="top">
                                        <fieldset title="Tipo Reporte:" class="FieldSetPanelReport">
                                            <legend>Tipo Reporte: </legend>
                                            <asp:RadioButtonList ID="rbtResDet" runat="server" RepeatDirection="Horizontal" Width="168px">
                                                <asp:ListItem Text="Detallado" Value="0" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="Resumido" Value="2"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </fieldset>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel_Cliente" runat="server">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td class="TituloCeldaLeft">
                                                <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="server" TargetControlID="PnlCliente"
                                                    CollapsedSize="0" ExpandedSize="80" Collapsed="true" ExpandControlID="ImgCliente"
                                                    CollapseControlID="ImgCliente" TextLabelID="lblCliente" ImageControlID="ImgCliente"
                                                    CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                                    CollapsedText="Buscar Cliente" ExpandedText="Buscar Cliente" ExpandDirection="Vertical"
                                                    SuppressPostBack="True" />
                                                <asp:Image ID="ImgCliente" runat="server" Width="16px" />
                                                <asp:Label ID="lblCliente" runat="server" Text="Buscar Cliente" CssClass="LabelBlanco"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Panel ID="PnlCliente" runat="server">
                                                    <table cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Imagenes/Limpiar_b.JPG"
                                                                    onmouseover="this.src='../../Imagenes/Limpiar_A.JPG';" onmouseout="this.src='../../Imagenes/Limpiar_b.JPG';"
                                                                    OnClientClick="return(LimpiarControlesCliente());" />
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="btnBuscarCliente" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                                    onmouseover="this.src='../../Imagenes/Buscar_A.JPG';" onmouseout="this.src='../../Imagenes/Buscar_b.JPG';"
                                                                    OnClientClick="return(mostrarCapaPersona());" />
                                                                <asp:Label ID="Label12024" runat="server" CssClass="Label" Text="Tipo de Persona"></asp:Label>
                                                                <asp:DropDownList ID="cboTipoPersona" runat="server" Width="116px" Font-Bold="True"
                                                                    Enabled="False">
                                                                    <asp:ListItem>---------</asp:ListItem>
                                                                    <asp:ListItem>Natural</asp:ListItem>
                                                                    <asp:ListItem>Juridica</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 139px; text-align: right;">
                                                                <asp:Label ID="Label1" runat="server" CssClass="Label" Text="Nombre o Raz�n Social"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRazonSocial" CssClass="TextBoxReadOnly" runat="server" Width="521px"
                                                                    MaxLength="50" ReadOnly="True"></asp:TextBox>
                                                                <asp:TextBox ID="txtCodigoCliente" CssClass="TextBoxReadOnly" runat="server" Width="99px"
                                                                    ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right; width: 139px">
                                                                <asp:Label ID="Label3" runat="server" Text="Dni" CssClass="Label"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtDni" runat="server" Width="70px" CssClass="TextBoxReadOnly" Style="margin-left: 0px;
                                                                    margin-bottom: 0px" MaxLength="8" ReadOnly="True"></asp:TextBox>
                                                                <asp:Label ID="Label4" runat="server" Text="Ruc" CssClass="Label"></asp:Label>
                                                                <asp:TextBox ID="txtRuc" runat="server" Width="90px" MaxLength="11" CssClass="TextBoxReadOnly"
                                                                    ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="DGV_BuscarPersona" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:ImageButton ID="btnMostrarReporte" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                                        onmouseout="this.src='../../Imagenes/Aceptar_B.JPG';" onmouseover="this.src='../../Imagenes/Aceptar_A.JPG';"
                                        OnClientClick="return(MostrarReporte());" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1130px">
                </iframe>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddFechaActual" runat="server" />
                <asp:HiddenField ID="hddRbtTipoReporte" runat="server" />
                <br />
            </td>
        </tr>
    </table>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 239px; left: 27px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="UpdatePanel_Persona" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                                onmouseout="this.src='../../Imagenes/Cerrar_B.JPG';" onmouseover="this.src='../../Imagenes/Cerrar_A.JPG';"
                                OnClientClick="return(offCapa('capaPersona'));" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table>
                                <tr>
                                    <td class="Label">
                                        Persona
                                    </td>
                                    <td class="Label">
                                        <asp:RadioButtonList ID="rblpersona" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="0" Text="Juridica" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="Natural"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LabelLeft">
                                        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarPersona_Grilla">                                        
                                        RUC:<asp:TextBox ID="txtBPruc" runat="server" onKeypress="return(validarCajaBusqueda());"></asp:TextBox>
                                        </asp:Panel>
                                    </td>
                                    <td class="LabelLeft">
                                    <asp:Panel ID="Panel2" runat="server" DefaultButton="btnBuscarPersona_Grilla">
                                        DNI:<asp:TextBox ID="txtBPDni" runat="server" onKeypress="return(validarCajaBusqueda());"></asp:TextBox>
                                        </asp:Panel>
                                    </td>
                                    <td class="LabelLeft">
                                        Razon Social/Nombre:
                                        <asp:Panel ID="Panel3" runat="server" DefaultButton="btnBuscarPersona_Grilla">
                                        <asp:TextBox ID="txtBuscarPersona_Grilla" runat="server" onKeypress="return(validarCajaBusqueda());"></asp:TextBox>
                                        </asp:Panel>
                                    </td>
                                    <td class="LabelLeft">
                                        <asp:ImageButton ID="btnBuscarPersona_Grilla" runat="server" CausesValidation="false"
                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../../Imagenes/Buscar_b.JPG';"
                                            onmouseover="this.src='../../Imagenes/Buscar_A.JPG';" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:GridView ID="DGV_BuscarPersona" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                                PageSize="20" Width="100%">
                                <Columns>
                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                    <asp:BoundField DataField="IdPersona" HeaderText="Id" NullDisplayText="0" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Descripci�n" NullDisplayText="---" />
                                    <asp:BoundField DataField="Dni" HeaderText="D.N.I." NullDisplayText="---" />
                                    <asp:BoundField DataField="Ruc" HeaderText="R.U.C." NullDisplayText="---" />
                                    <asp:BoundField DataField="Direccion" HeaderText="Direcci�n" NullDisplayText="---" />
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnAnterior_Persona" runat="server" Font-Bold="true" Width="50px"
                                Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionPersona('0'));" />
                            <asp:Button ID="btnPosterior_Persona" runat="server" Font-Bold="true" Width="50px"
                                Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionPersona('1'));" />
                            <asp:TextBox ID="txtPageIndex_Persona" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                runat="server"></asp:TextBox>
                            <asp:Button ID="btnIr_Persona" runat="server" Font-Bold="true" Width="50px" Text="Ir"
                                ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionPersona('2'));" />
                            <asp:TextBox ID="txtPageIndexGO_Persona" Width="50px" CssClass="TextBox_ReadOnly"
                                runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script language="javascript" type="text/javascript">

        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };

        function onCapaCliente(s) {
            LimpiarControlesCliente();
            onCapa(s);
            document.getElementById('<%= txtRazonSocial.ClientID%>').focus();
            return false;
        }
        void function LimpiarControlesCliente() {
            document.getElementById('<%= txtRazonSocial.ClientID%>').value = "";
            document.getElementById('<%= txtDNI.ClientID%>').value = "";
            document.getElementById('<%= txtRUC.ClientID%>').value = "";
            document.getElementById('<%= txtCodigoCliente.ClientID %>').value = "";
            return false;
        }



        function validarNumeroPunto(event) {
            var key = event.keyCode;
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }



        //Buscar SubLinea
        function ValidarEnteroSinEnter() {
            if (onKeyPressEsNumero('event') == false) {
                alert('Caracter no v�lido. Solo se permiten n�meros Enteros');
                return false;
            }
            return true;
        }


        //End Buscar SubLinea
        //Buscar Cliente

        function BuscarCliente() {

            return true;
        }


        function ValidarEnter() {
            return (!esEnter(event));
        }
        function ValidaEnter() {
            if (esEnter(event) == true) {
                alert('Tecla No permitida');
                caja.focus();
                return false;
            }
            return true;
        }



        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').select();
            document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').focus();

            return false;
        }
        function addPersona_Venta() {
            offCapa('capaPersona');

            return false;
        }


        function aceptarFoco(caja) {
            caja.select();
            caja.focus();
            return true;
        }




        function valNavegacionPersona(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Persona.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').select();
                document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }


        function validarCajaBusqueda() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btnBuscarPersona_Grilla.ClientID %>');
                boton.focus();
                return true;
            }
        }

        function onFocus_ReadOnly(ocaja) {
            ocaja.readOnly = true;
            return false;
        }


        function MostrarReporte() {
            var resudeta = 0;
            var fechaInicio;
            var fechaFin;
            var idempresa = document.getElementById('<%= cmbEmpresa.ClientID%>').value;
            var idtienda = document.getElementById('<%=cmbtienda.ClientID%>').value;
            var idtipoDocumento = document.getElementById('<%=cmdTipoDocumento.ClientID%>').value;
            var idMoneda = document.getElementById('<%=CmdMoneda.ClientID%>').value;
            var idestado = document.getElementById('<%=cmdEstado.ClientID%>').value;
            var serie = document.getElementById('<%=txtSerie.ClientID%>').value;
            var codigo = document.getElementById('<%=txtCodigo.ClientID%>').value;

            var rdbFiltrarFechaSemana = document.getElementById('<%=rdbFiltrarFechaSemana.ClientID %>');
            var FiltrarFechaSemana = rdbFiltrarFechaSemana.getElementsByTagName('input');
            var FiltrarSemana = 0;
            var FiltrarFecha = 0;
            var rblist = document.getElementById('<%=rbtlTpReporte.ClientID%>');

            var radioAgrupado = rblist.getElementsByTagName("INPUT");
            var reporte = 0;            
            if (radioAgrupado[0].checked == true) {
                reporte = 1;
            } else if (radioAgrupado[1].checked == true) {
                reporte = 2;
            } else {
                reporte = 1;
            }

            var rblresdet = document.getElementById('<%=rbtResDet.ClientID %>');
            var radioTipo = rblresdet.getElementsByTagName("INPUT");

            if (radioTipo[0].checked == true) {
                resudeta = 0;
            } else if (radioTipo[1].checked == true) {
                resudeta = 2;
            } else {
                resudeta = 0;
            }


            // Semana 2
            // Ninguno 3
            for (var i = 0; i < FiltrarFechaSemana.length; i++) {
                if (FiltrarFechaSemana[i].checked == true) {
                    switch (FiltrarFechaSemana[i].value) {

                        case '1': // fecha 
                            FiltrarFecha = 1;
                            break;

                        case '2': // Semana
                            FiltrarSemana = 1;
                            break;
                    }
                }
            }



            var idpersona = document.getElementById('<%=txtCodigoCliente.ClientID%>').value;
            var persona;
            if (idpersona == '') {
                persona = 0;
            } else {
                persona = idpersona;
            }

            if (document.getElementById('<%= txtFechaInicio.ClientID%>') == null) {
                fechaInicio = document.getElementById('<%= hddFechaActual.ClientID%>').value
                fechaFin = document.getElementById('<%= hddFechaActual.ClientID%>').value
            } else {
                fechaInicio = document.getElementById('<%= txtFechaInicio.ClientID%>').value;
                fechaFin = document.getElementById('<%= txtFechaFin.ClientID%>').value;
            }

            var YearIni = document.getElementById('<%= idYearI.ClientID%>').value;
            if (document.getElementById('<%= idYearI.ClientID%>') == null) {
                YearIni = 0
            }
            var SemanaIni = document.getElementById('<%= idSemanaI.ClientID%>').value;
            if (document.getElementById('<%= idSemanaI.ClientID%>') == null) {
                SemanaIni = 0
            }

            var YearFin = document.getElementById('<%= idYearF.ClientID%>').value;
            if (document.getElementById('<%= idYearF.ClientID%>') == null) {
                YearFin = 0
            }
            var SemanaFin = document.getElementById('<%=idSemanaF.ClientID%>').value;
            if (document.getElementById('<%= idSemanaF.ClientID%>') == null) {
                SemanaFin = 0
            }

            frame1.location.href = 'visorVentas.aspx?iReporte=1&FechaInicio=' + fechaInicio + '&FechaFin=' + fechaFin +
                                    '&IdEmpresa=' + idempresa + '&IdTienda=' + idtienda + '&idtipoDocumento=' + idtipoDocumento + '&idMoneda=' + idMoneda + '&idestado=' + idestado + '&persona=' + persona + '&serie=' + serie + '&codigo=' + codigo +
                                    '&FiltrarFecha=' + FiltrarFecha + '&FiltrarSemana=' + FiltrarSemana +
                                    '&YearIni=' + YearIni + '&SemanaIni=' + SemanaIni + '&YearFin=' + YearFin + '&SemanaFin=' + SemanaFin +
                                    '&reporte=' + reporte + '&resudeta=' + resudeta;
            return false;

        }
   
    
    </script>

</asp:Content>
