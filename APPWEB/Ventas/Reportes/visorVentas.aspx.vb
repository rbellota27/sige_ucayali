﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Partial Public Class visorVentas
    Inherits System.Web.UI.Page
    Private objNegDocView As New Negocio.DocumentoView
    Dim reporte As New ReportDocument
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then
            Dim iReporte As String = Request.QueryString("iReporte")

            If iReporte Is Nothing Then
                iReporte = ""
            End If
            ViewState.Add("iReporte", iReporte)
            Select Case iReporte
                Case "1" 'dEtalles de documentos 

                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdTienda", Request.QueryString("IdTienda"))
                    ViewState.Add("idtipoDocumento", Request.QueryString("idtipoDocumento"))
                    ViewState.Add("idMoneda", Request.QueryString("idMoneda"))
                    ViewState.Add("idestado", Request.QueryString("idestado"))
                    ViewState.Add("persona", Request.QueryString("persona"))
                    ViewState.Add("serie", Request.QueryString("serie"))
                    ViewState.Add("codigo", Request.QueryString("codigo"))
                    ViewState.Add("reporte", Request.QueryString("reporte"))
                    ViewState.Add("resudeta", Request.QueryString("resudeta"))

                    ViewState.Add("FiltrarFecha", Request.QueryString("FiltrarFecha"))
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))

                    ViewState.Add("FiltrarSemana", Request.QueryString("FiltrarSemana"))
                    ViewState.Add("YearIni", Me.Request.QueryString("YearIni"))
                    ViewState.Add("SemanaIni", Me.Request.QueryString("SemanaIni"))
                    ViewState.Add("YearFin", Me.Request.QueryString("YearFin"))
                    ViewState.Add("SemanaFin", Me.Request.QueryString("SemanaFin"))

                Case "9" 'dEtalles de documentos 

                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdTienda", Request.QueryString("IdTienda"))
                    ViewState.Add("idtipoDocumento", Request.QueryString("idtipoDocumento"))
                    ViewState.Add("idMoneda", Request.QueryString("idMoneda"))
                    ViewState.Add("idestado", Request.QueryString("idestado"))
                    ViewState.Add("persona", Request.QueryString("persona"))
                    ViewState.Add("serie", Request.QueryString("serie"))
                    ViewState.Add("codigo", Request.QueryString("codigo"))
                    ViewState.Add("reporte", Request.QueryString("reporte"))
                    ViewState.Add("resudeta", Request.QueryString("resudeta"))

                    ViewState.Add("FiltrarFecha", Request.QueryString("FiltrarFecha"))
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))

                    ViewState.Add("FiltrarSemana", Request.QueryString("FiltrarSemana"))
                    ViewState.Add("YearIni", Me.Request.QueryString("YearIni"))
                    ViewState.Add("SemanaIni", Me.Request.QueryString("SemanaIni"))
                    ViewState.Add("YearFin", Me.Request.QueryString("YearFin"))
                    ViewState.Add("SemanaFin", Me.Request.QueryString("SemanaFin"))

                Case "2"
                    ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
                Case "3"
                    ViewState.Add("IdTienda", Request.QueryString("IdTienda"))
                    ViewState.Add("Idlinea", Request.QueryString("Idlinea"))
                    ViewState.Add("IdSublinea", Request.QueryString("IdSublinea"))
                    ViewState.Add("IdProducto", Request.QueryString("IdProducto"))
                    ViewState.Add("IdTipoMedida", Request.QueryString("IdTipoMedida"))
                    ViewState.Add("idmoneda", Request.QueryString("idmoneda"))
                Case "4"
                    ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
                Case "5"
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdTienda", Request.QueryString("IdTienda"))
                    ViewState.Add("idtipodocumento", Request.QueryString("idtipodocumento"))
                    ViewState.Add("tipo", Request.QueryString("tipo"))
                    ViewState.Add("ascdes", Request.QueryString("ascdes"))

                Case "6"
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdTienda", Request.QueryString("IdTienda"))
                Case "7"
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdTienda", Request.QueryString("IdTienda"))
                Case "8"
                    ViewState.Add("Idempresa", Request.QueryString("Idempresa"))
                    ViewState.Add("IdTienda", Request.QueryString("IdTienda"))
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                    ViewState.Add("IdTipoDocumento", Request.QueryString("IdTipoDocumento"))
                    ViewState.Add("yeari", Request.QueryString("yeari"))
                    ViewState.Add("semanai", Request.QueryString("semanai"))
                    ViewState.Add("yearf", Request.QueryString("yearf"))
                    ViewState.Add("semanaf", Request.QueryString("semanaf"))
                    ViewState.Add("filtrarsemana", Request.QueryString("filtrarsemana"))
                    ViewState.Add("idpersona", Request.QueryString("idpersona"))
                Case "30"
                    ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))

                Case "31"
                    ViewState.Add("Idempresa", Request.QueryString("Idempresa"))
                    ViewState.Add("IdTienda", Request.QueryString("IdTienda"))
                    ViewState.Add("IdComisionCab", Request.QueryString("IdComisionCab"))
                    ViewState.Add("IdVendedor", Request.QueryString("IdVendedor"))
                    ViewState.Add("FechaIni", Request.QueryString("FechaIni"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                    ViewState.Add("Empresa", (New Negocio.Persona).getDescripcionPersona(CInt(ViewState("IdEmpresa"))))

                Case "32"
                    ViewState.Add("Idempresa", Request.QueryString("Idempresa"))
                    ViewState.Add("IdTienda", Request.QueryString("IdTienda"))
                    ViewState.Add("IdComisionCab", Request.QueryString("IdComisionCab"))
                    ViewState.Add("IdVendedor", Request.QueryString("IdVendedor"))
                    ViewState.Add("FechaIni", Request.QueryString("FechaIni"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))

                    ViewState.Add("IdTipoExistencia", Request.QueryString("IdTipoExistencia"))
                    ViewState.Add("IdLinea", Request.QueryString("IdLinea"))
                    ViewState.Add("IdSubLinea", Request.QueryString("IdSubLinea"))
                    ViewState.Add("IdProducto", Request.QueryString("IdProducto"))
                    ViewState.Add("AgruparComision", Request.QueryString("AgruparComision"))

                    ViewState.Add("Empresa", (New Negocio.Persona).getDescripcionPersona(CInt(ViewState("IdEmpresa"))))

            End Select
        End If

        mostrarReporte()

    End Sub
    Private Sub mostrarReporte()
        Select Case ViewState.Item("iReporte").ToString
            Case "1"
                ReporteDocumentosDetalles()
            Case "2"
                'ReporteCotizacion()
                RptCotizacion()
            Case "3"
                listarPrecios()
            Case "4"
                RptCotizacion()
            Case "5"
                rptDocUsuario()
            Case "6"
                rptDocumMontDiferen()
            Case "7"
                rptDocumtosMediosPago()
            Case "8"
                rptVtasProductoprom()
            Case "30"
                RptComprobantePercepcion()
            Case "31"
                rptDetalleComision()
            Case "32"
                rptComisionxVendedor()
            Case "9"
                ReporteDocumentosDetallesxMes()
        End Select
    End Sub

    Private Sub rptComisionxVendedor()

        Dim ds As New DataSet

        ds = (New Negocio.Comision_Det).CR_ComisionxVendedor(CInt(ViewState("IdComisionCab")), CInt(ViewState("Idempresa")), CInt(ViewState("IdTienda")), CInt(ViewState("IdVendedor")), CStr(ViewState("FechaIni")), CStr(ViewState("FechaFin")), CInt(ViewState("IdTipoExistencia")), CInt(ViewState("IdLinea")), CInt(ViewState("IdSubLinea")), CInt(ViewState("IdProducto")), obtenerDataTable_TipoTablaValor("ListaFiltroTablaValor2"))

        If CInt(ViewState("AgruparComision")) = 1 Then
            reporte = New CR_ComisionxVendedor
        Else
            reporte = New CR_ComisionxVendedor2
        End If



        reporte.SetDataSource(ds)
        reporte.SetParameterValue("@Empresa", CStr(ViewState("Empresa")))
        reporte.SetParameterValue("@FechaIni", CStr(ViewState("FechaIni")))
        reporte.SetParameterValue("@FechaFin", CStr(ViewState("FechaFin")))

        CRViewer.ReportSource = reporte
        CRViewer.DataBind()

    End Sub

    Private Sub rptDetalleComision()

        Dim ds As New DataSet

        ds = (New Negocio.Comision_Det).CR_DetalleComision(CInt(ViewState("IdComisionCab")), CInt(ViewState("Idempresa")), CInt(ViewState("IdTienda")), CInt(ViewState("IdVendedor")), CStr(ViewState("FechaIni")), CStr(ViewState("FechaFin")))
        reporte = New CR_DetalleComision
        reporte.SetDataSource(ds)
        reporte.SetParameterValue("@Empresa", CStr(ViewState("Empresa")))
        reporte.SetParameterValue("@FechaIni", CStr(ViewState("FechaIni")))
        reporte.SetParameterValue("@FechaFin", CStr(ViewState("FechaFin")))
        CRViewer.ReportSource = reporte
        CRViewer.DataBind()

    End Sub

    Private Sub rptVtasProductoprom()
        Try
            Dim idtienda, idempresa, iddocumento As Integer
            idtienda = CInt(ViewState("IdTienda"))
            idempresa = CInt(ViewState("Idempresa"))
            iddocumento = CInt(ViewState("IdTipoDocumento"))

            Dim listaProducto As New List(Of Entidades.ProductoView)
            Dim ListaTipoAlmacen As New List(Of Entidades.TipoAlmacen)

            listaProducto = CType(Session("listaProductoView"), Global.System.Collections.Generic.List(Of Global.Entidades.ProductoView))

            Dim ds As New DataSet
            Dim objReporte As New Negocio.VentasPorArticuloView
            ds = objReporte.CRVtasProductoProm(idempresa, idtienda, iddocumento, listaProducto, _
                 CStr(ViewState.Item("FechaInicio")), CStr(ViewState.Item("FechaFin")), CInt(ViewState.Item("yeari")), _
CInt(ViewState.Item("semanai")), CInt(ViewState.Item("yearf")), CInt(ViewState.Item("semanaf")), CInt(ViewState.Item("filtrarsemana")), CInt(ViewState.Item("idpersona")))
            reporte = New Cr_VtasProductoprom
            reporte.SetDataSource(ds)
            CRViewer.ReportSource = reporte
            CRViewer.DataBind()

        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub

    Private Sub rptDocumtosMediosPago()
        Try
            reporte = New CR_RptDocumentosMediosPago

            reporte.SetDataSource(objNegDocView.SelectDocumentMediosPagos(CInt(ViewState.Item("IdEmpresa")), CInt(ViewState.Item("IdTienda")), CStr(ViewState.Item("FechaInicio")), CStr(ViewState.Item("FechaFin"))))
            reporte.SetParameterValue("@fechaInicio", CStr(ViewState.Item("FechaInicio")))
            reporte.SetParameterValue("@fechaFin", CStr(ViewState.Item("FechaFin")))
            CRViewer.ReportSource = reporte
            CRViewer.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub rptDocumMontDiferen()
        Try
            reporte = New CR_DocumentoMontoDiferente
            reporte.SetDataSource(objNegDocView.SelectDocumMontDiferentes(CInt(ViewState.Item("IdEmpresa")), CInt(ViewState.Item("IdTienda")), CStr(ViewState.Item("FechaInicio")), CStr(ViewState.Item("FechaFin"))))
            reporte.SetParameterValue("@FechaInicio", CStr(ViewState.Item("FechaInicio")))
            reporte.SetParameterValue("@FechaFin", CStr(ViewState.Item("FechaFin")))
            CRViewer.ReportSource = reporte
            CRViewer.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub rptDocUsuario()
        Try
            reporte = New CR_RptDocumentosUsuario
            reporte.SetDataSource(objNegDocView.SelectDocumUsuario(CInt(ViewState.Item("IdEmpresa")), CInt(ViewState.Item("IdTienda")), CStr(ViewState.Item("FechaInicio")), CStr(ViewState.Item("FechaFin")), CInt(ViewState.Item("idtipodocumento")), CInt(ViewState.Item("tipo")), CShort(ViewState.Item("ascdes"))))
            reporte.SetParameterValue("@Fechainicio", CStr(ViewState.Item("FechaInicio")))
            reporte.SetParameterValue("@fechaFin", CStr(ViewState.Item("FechaFin")))
            'reporte.Subreports("")
            CRViewer.ReportSource = reporte
            CRViewer.DataBind()

        Catch ex As Exception
            Throw ex
            'Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub ReporteCotizacion()
        Try
            Dim IdDocumento As Integer = CInt(ViewState("IdDocumento"))
            reporte = New CR_Cotizacion
            reporte.SetDataSource((New Negocio.Reportes).getDataSetDocCotizacion(IdDocumento))
            CRViewer.ReportSource = reporte
            CRViewer.DataBind()

        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub RptCotizacion()
        Try
            Dim IdDocumento As Integer = CInt(ViewState("IdDocumento"))

            reporte = New CR_rptCotizacion

            reporte.SetDataSource((New Negocio.Reportes).getReporteDocCotizacion(IdDocumento, 3))

            CRViewer.ReportSource = reporte
            CRViewer.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub

    Private Sub RptComprobantePercepcion()
        Try
            Dim IdDocumento As Integer = CInt(ViewState("IdDocumento"))

            reporte = New CR_ComprobantePercepcion

            reporte.SetDataSource((New Negocio.Reportes).getReporteComprobantePercepcion(IdDocumento))

            CRViewer.ReportSource = reporte
            CRViewer.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub ReporteDocumentosDetallesxMes()
        Try
            Dim vidempresa, vidtienda, vidmoneda, vidtipoDoc, vEstadoDoc, vserie, vcodigo, vrango, rpte, resumen As Integer

            If CStr(ViewState.Item("reporte")) = "" Then
                rpte = 1
            Else
                rpte = CInt(ViewState.Item("reporte"))
            End If


            If CStr(ViewState.Item("serie")) = "" Then
                vserie = 0
            Else
                vserie = CInt(ViewState.Item("serie"))
            End If

            If CStr(ViewState.Item("codigo")) = "" Then
                vcodigo = 0
            Else
                vcodigo = CInt(ViewState.Item("codigo"))
            End If

            If CStr(ViewState.Item("FiltrarFecha")) = "" Then
                vrango = 0
            Else
                vrango = CInt(ViewState.Item("FiltrarFecha"))
            End If

            If CStr(ViewState.Item("IdEmpresa")) = "" Then
                vidempresa = 0
            Else
                vidempresa = CInt(ViewState.Item("IdEmpresa"))
            End If


            If CStr(ViewState.Item("IdTienda")) = "" Then
                vidtienda = 0
            Else
                vidtienda = CInt(ViewState.Item("IdTienda"))
            End If

            If CStr(ViewState.Item("idMoneda")) = "" Then
                vidmoneda = 0
            Else
                vidmoneda = CInt(ViewState.Item("idMoneda"))
            End If

            If CStr(ViewState.Item("idtipoDocumento")) = "" Then
                vidtipoDoc = 0
            Else
                vidtipoDoc = CInt(ViewState.Item("idtipoDocumento"))
            End If
            If CStr(ViewState.Item("idestado")) = "" Then
                vEstadoDoc = 0
            Else
                vEstadoDoc = CInt(ViewState.Item("idestado"))

            End If

            If CStr(ViewState.Item("resudeta")) = "" Then
                resumen = 0
            Else
                resumen = CInt(ViewState.Item("resudeta"))
            End If

            Dim FiltrarSemana As Integer = CInt(ViewState("FiltrarSemana"))

            Dim ds As New DataSet
            Dim objReporte As New Negocio.DocumentoView

            ds = objReporte.DocumentoDetallexMes(CInt(vidempresa), CInt(vidtienda), CInt(vidmoneda), CInt(vidtipoDoc), CInt(vEstadoDoc), CStr((ViewState.Item("FechaInicio"))), CStr((ViewState.Item("FechaFin"))), CInt(ViewState.Item("persona")), vserie, vcodigo, vrango, resumen, FiltrarSemana, CInt(ViewState("YearIni")), CInt(ViewState("SemanaIni")), CInt(ViewState("YearFin")), CInt(ViewState("SemanaFin")))

            reporte = New CR_DetallesDocumentosxMes

            reporte.SetDataSource(ds)

            If FiltrarSemana = 1 Then

                Dim Ini As String = CStr(ViewState("YearIni")) + " Semana " + CStr(ViewState("SemanaIni"))
                Dim Fin As String = CStr(ViewState("YearFin")) + " Semana " + CStr(ViewState("SemanaFin"))

                reporte.SetParameterValue("@FechaInicio", Ini)
                reporte.SetParameterValue("@FechaFin", Fin)

            Else
                reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
                reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
            End If

            CRViewer.ReportSource = reporte
            CRViewer.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub


    Private Sub ReporteDocumentosDetalles()
        Try
            Dim vidempresa, vidtienda, vidmoneda, vidtipoDoc, vEstadoDoc, vserie, vcodigo, vrango, rpte, resumen As Integer

            If CStr(ViewState.Item("reporte")) = "" Then
                rpte = 1
            Else
                rpte = CInt(ViewState.Item("reporte"))
            End If


            If CStr(ViewState.Item("serie")) = "" Then
                vserie = 0
            Else
                vserie = CInt(ViewState.Item("serie"))
            End If

            If CStr(ViewState.Item("codigo")) = "" Then
                vcodigo = 0
            Else
                vcodigo = CInt(ViewState.Item("codigo"))
            End If

            If CStr(ViewState.Item("FiltrarFecha")) = "" Then
                vrango = 0
            Else
                vrango = CInt(ViewState.Item("FiltrarFecha"))
            End If

            If CStr(ViewState.Item("IdEmpresa")) = "" Then
                vidempresa = 0
            Else
                vidempresa = CInt(ViewState.Item("IdEmpresa"))
            End If


            If CStr(ViewState.Item("IdTienda")) = "" Then
                vidtienda = 0
            Else
                vidtienda = CInt(ViewState.Item("IdTienda"))
            End If

            If CStr(ViewState.Item("idMoneda")) = "" Then
                vidmoneda = 0
            Else
                vidmoneda = CInt(ViewState.Item("idMoneda"))
            End If

            If CStr(ViewState.Item("idtipoDocumento")) = "" Then
                vidtipoDoc = 0
            Else
                vidtipoDoc = CInt(ViewState.Item("idtipoDocumento"))
            End If
            If CStr(ViewState.Item("idestado")) = "" Then
                vEstadoDoc = 0
            Else
                vEstadoDoc = CInt(ViewState.Item("idestado"))

            End If

            If CStr(ViewState.Item("resudeta")) = "" Then
                resumen = 0
            Else
                resumen = CInt(ViewState.Item("resudeta"))
            End If

            Dim FiltrarSemana As Integer = CInt(ViewState("FiltrarSemana"))

            Dim ds As New DataSet
            Dim objReporte As New Negocio.DocumentoView

            ds = objReporte.DocumentoDetalle(CInt(vidempresa), CInt(vidtienda), CInt(vidmoneda), CInt(vidtipoDoc), CInt(vEstadoDoc), CStr((ViewState.Item("FechaInicio"))), CStr((ViewState.Item("FechaFin"))), CInt(ViewState.Item("persona")), vserie, vcodigo, vrango, resumen, FiltrarSemana, CInt(ViewState("YearIni")), CInt(ViewState("SemanaIni")), CInt(ViewState("YearFin")), CInt(ViewState("SemanaFin")))

            If (resumen = 0) Then ''detallados

                If vidtipoDoc = 1 Or vidtipoDoc = 3 Or vidtipoDoc = 0 Then
                    If rpte = 1 Then
                        reporte = New CR_DetallesDocumentos  ''agrupado por cliente CR_DetallesDocumentos
                    Else
                        reporte = New CR_DetallesDocumentosFEmision ' agrupado por fecha emision
                    End If
                ElseIf vidtipoDoc = 17 Then
                    If rpte = 1 Then
                        reporte = New CR_DetallesDocumentosRecIngre
                    Else
                        reporte = New CR_DetallesDocumentosRecIngreFEmision
                    End If

                ElseIf vidtipoDoc = 18 Then

                    If rpte = 1 Then
                        reporte = New CR_DetallesDocumentosRecEgre
                    Else
                        reporte = New CR_DetallesDocumentosRecEgreFEmision
                    End If

                Else
                    If rpte = 1 Then
                        reporte = New CR_DetallesDocumentosRecIngre
                    Else
                        reporte = New CR_DetallesDocumentosRecIngreFEmision
                    End If
                End If
            Else 'resumidos
                If rpte = 1 Then
                    reporte = New CR_DocumentoResumen
                Else
                    reporte = New CR_DocumentoResumenFEmision
                End If

            End If

            reporte.SetDataSource(ds)
            If FiltrarSemana = 1 Then

                Dim Ini As String = CStr(ViewState("YearIni")) + " Semana " + CStr(ViewState("SemanaIni"))
                Dim Fin As String = CStr(ViewState("YearFin")) + " Semana " + CStr(ViewState("SemanaFin"))

                reporte.SetParameterValue("@FechaInicio", Ini)
                reporte.SetParameterValue("@FechaFin", Fin)

            Else
                reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
                reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
            End If

            CRViewer.ReportSource = reporte
            CRViewer.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub

    Private Sub listarPrecios()
        Try
            Dim idtienda, idlinea, idsublinea, idproducto, idtipomedida, idmoneda As Integer
            idtienda = CInt(ViewState("IdTienda"))
            idlinea = CInt(ViewState("Idlinea"))
            idsublinea = CInt(ViewState("IdSublinea"))
            idproducto = CInt(ViewState("IdProducto"))
            idtipomedida = CInt(ViewState("IdTipoMedida"))
            idmoneda = CInt(ViewState("idmoneda"))
            Dim ds As New DataSet
            Dim objReporte As New Negocio.VentasPorArticuloView
            ds = objReporte.ReporteListaPrecios(idtienda, idlinea, idsublinea, 0, idproducto, "", idtipomedida, idmoneda, obtenerDataTable_TipoTablaValor("TablaValorPV"))
            reporte = New CR_ListaPrecios
            reporte.SetDataSource(ds)
            CRViewer.ReportSource = reporte
            CRViewer.DataBind()

        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub




    Private Function obtenerDataTable_TipoTablaValor(ByVal nombre As String) As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim ListaTipoTablaValor As List(Of Entidades.TipoTablaValor)

        Try
            ListaTipoTablaValor = CType(Session.Item(nombre), List(Of Entidades.TipoTablaValor))

            For i As Integer = 0 To ListaTipoTablaValor.Count - 1
                dt.Rows.Add(ListaTipoTablaValor(i).IdTipoTabla, ListaTipoTablaValor(i).IdTipoTablaValor)
            Next

        Catch ex As Exception

        End Try


        Return dt
    End Function


    Private Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        reporte.Close()
        reporte.Dispose()
    End Sub
End Class