﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmComisionxVendedor.aspx.vb" Inherits="APPWEB.FrmComisionxVendedor" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td class="TituloCelda">
                Reporte Detalle Por Producto
            </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                    <legend><span class="Texto"></span></legend>
                    <table>
                        <tr>
                            <td class="Texto">
                                Empresa:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Tienda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Comisión Extraordinaria:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboComisionExt" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:CheckBox ID="ckAgruparComision" runat="server" CssClass="Texto" Text="Agrupar Por Comision" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                    <legend><span class="Texto"></span></legend>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tipo Existencia:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTipoExistencia" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Linea:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboLinea" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Sub-Linea:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboSubLinea" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td align="center">
                <fieldset>
                    <legend><span class="Texto">Busqueda de Producto</span></legend>
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnBuscarProducto" runat="server" ImageUrl="~/Imagenes/BuscarProducto_A.JPG"
                                    OnClientClick="return(valOnClick_btnBuscarProducto());" />
                            </td>
                            <td>
                                <asp:ImageButton ID="btnLimpiarDetalleDocumento" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:GridView ID="GV_Producto" runat="server" AutoGenerateColumns="False" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Código" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblCodigo" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Codigo")%>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Descripcion" ItemStyle-HorizontalAlign="Center" HeaderText="Descripción"
                                            HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                        <asp:BoundField DataField="UnidadMedida" ItemStyle-HorizontalAlign="Center" HeaderText="U.M."
                                            HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                        <asp:BoundField DataField="Linea" ItemStyle-HorizontalAlign="Center" HeaderText="Línea"
                                            HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                        <asp:BoundField DataField="SubLinea" ItemStyle-HorizontalAlign="Center" HeaderText="Sub Línea"
                                            HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                Vendedor
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Persona" runat="server">
                    <table>
                        <tr>
                            <td class="Texto" align="right">
                                Descripción:
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtDescripcionPersona" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft"
                                    Width="400px" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarPersona" OnClientClick="return( mostrarCapaPersona()  );"
                                    runat="server" Text="Buscar" ToolTip="Buscar Persona" />
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" align="right">
                                D.N.I.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDni" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>
                            </td>
                            <td align="right" class="Texto">
                                R.U.C.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRuc" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                    <legend><span class="Texto">Atributo de Producto</span></legend>
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnOpen_Atributo" runat="server" Text="Buscar" ToolTip="Buscar Atributos" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GV_FiltroTipoTablaValor" runat="server" AutoGenerateColumns="False" Width="650px">
                                    <Columns>
                                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                        <asp:TemplateField HeaderText="Atributo">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomTabla")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdTipoTablaValor" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTablaValor")%>' />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Valor">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblTipoTablaValor" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="GrillaRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td align="center">
                <fieldset>
                    <legend><span class="Texto">Rando de Fechas</span></legend>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="Texto">
                                Incio:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaIni" runat="server" CssClass="TextBox_Fecha" Width="80px"
                                    onBlur=" return ( valFecha_Blank(this) ); "></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender_FechaIni" runat="server" TargetControlID="txtFechaIni"
                                    Format="dd/MM/yyyy">
                                </cc1:CalendarExtender>
                            </td>
                            <td class="Texto">
                                Fin:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaFin" runat="server" CssClass="TextBox_Fecha" Width="80px"
                                    onBlur=" return ( valFecha_Blank(this) ); "></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender_FechaFin" runat="server" TargetControlID="txtFechaFin"
                                    Format="dd/MM/yyyy">
                                </cc1:CalendarExtender>
                            </td>
                            <td>
                                <asp:HiddenField ID="hddIdVendedor" runat="server" Value="0" />
                            </td>
                            <td>
                                <asp:HiddenField ID="hddIdProducto" runat="server" Value="0" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="imgAceptar" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                    OnClientClick=" return ( Aceptar() ); " />
            </td>
        </tr>
        <tr>
            <td>
                <iframe id="iReportFrame" name="iReportFrame" width="100%" height="2000px" scrolling="no">
                </iframe>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnOpen_TablaValor" runat="server" Text="" Width="1px" Height="1px" />
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <cc1:ModalPopupExtender ID="ModalPopup_Atributo" runat="server" TargetControlID="btnOpen_Atributo"
                                PopupControlID="Panel_Atributo" BackgroundCssClass="modalBackground" Enabled="true"
                                RepositionMode="None" CancelControlID="btnClose_Atributo" Y="50">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="Panel_Atributo" runat="server" CssClass="modalPopup" Style="display: none;"
                                Width="750px">
                                <table width="100%">
                                    <tr>
                                        <td align="right">
                                            <asp:Image ID="btnClose_Atributo" runat="server" ImageUrl="~/Imagenes/Cerrar.gif" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <fieldset>
                                                <legend><span class="Texto">Clases de Atributos</span></legend>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Descripción:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtBuscarTabla" runat="server" MaxLength="50" Width="250px" onKeyPress=" return ( BuscarTabla() ); "></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="ImgBuscarTabla" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                                            ToolTip="Buscar Tabla" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:GridView ID="GV_Tabla" runat="server" AutoGenerateColumns="False" Width="100%"
                                                                PageSize="20">
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <Columns>
                                                                    <asp:CommandField ShowSelectButton="True" />
                                                                    <asp:TemplateField HeaderText="Descripción">
                                                                        <ItemTemplate>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTabla") %>' />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="lblTabla" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="Abv" HeaderText="Abv." />
                                                                </Columns>
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Button ID="btnAnteriorTabla" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                            ToolTip="Página Anterior" OnClientClick="return(valNavegacionTabla('0'));" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button ID="btnSiguienteTabla" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                            ToolTip="Página Posterior" OnClientClick="return(valNavegacionTabla('1'));" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtPageIndexTabla" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                            runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button ID="btnIrTabla" runat="server" Font-Bold="true" Width="50px" Text="Ir"
                                                                            ToolTip="Ir a la Página" OnClientClick="return(valNavegacionTabla('2'));" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtPageIndexGoTabla" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                            onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <cc1:ModalPopupExtender ID="ModalPopup_TablaValor" runat="server" TargetControlID="btnOpen_TablaValor"
                                                PopupControlID="Panel_TablaValor" BackgroundCssClass="modalBackground" Enabled="true"
                                                RepositionMode="None" CancelControlID="ImgClose_TablaValor" Y="75">
                                            </cc1:ModalPopupExtender>
                                            <asp:Panel ID="Panel_TablaValor" runat="server" CssClass="modalPopup" Style="display: none;"
                                                Width="700px">
                                                <table width="100%">
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Image ID="ImgClose_TablaValor" runat="server" ImageUrl="~/Imagenes/Cerrar.gif" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <fieldset>
                                                                <legend><span class="Texto">Registros de </span>
                                                                    <asp:Label ID="lblNomTabla" runat="server" Font-Bold="true" CssClass="Texto" Text="Tabla"></asp:Label></legend>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="Texto">
                                                                                        Descripción:
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtBuscarTablaValor" runat="server" MaxLength="50" Width="250px"
                                                                                            onKeyPress=" return ( BuscarTablaValor() ); "></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:ImageButton ID="ImgBuscarTablaValor" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                                                            ToolTip="Buscar Tabla Valor" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:ImageButton ID="ImgAgregarTablaValor" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                                                            OnClientClick=" return ( AgregarTablaValor() ); " ToolTip="Agregar Valor" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:GridView ID="GV_TablaValor" runat="server" AutoGenerateColumns="False" Width="100%"
                                                                                PageSize="20">
                                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                                <Columns>
                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:CheckBox ID="ckTablaValor" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Código">
                                                                                        <ItemTemplate>
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTabla") %>' />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblTipoTabla" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Codigo") %>'></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Descripción">
                                                                                        <ItemTemplate>
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:HiddenField ID="hddIdTipoTablaValor" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTablaValor") %>' />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblTipoTablaValor" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField DataField="Abv" HeaderText="Abv." />
                                                                                </Columns>
                                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                                <PagerStyle CssClass="GrillaPager" />
                                                                                <RowStyle CssClass="GrillaRow" />
                                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                                            </asp:GridView>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Button ID="btnAnteriorTablaValor" runat="server" Font-Bold="true" Width="50px"
                                                                                            Text="<" ToolTip="Página Anterior" OnClientClick="return(valNavegacionTablaValor('0'));" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Button ID="btnSiguienteTablaValor" runat="server" Font-Bold="true" Width="50px"
                                                                                            Text=">" ToolTip="Página Posterior" OnClientClick="return(valNavegacionTablaValor('1'));" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtPageIndexTablaValor" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Button ID="btnIrTablaValor" runat="server" Font-Bold="true" Width="50px" Text="Ir"
                                                                                            ToolTip="Ir a la Página" OnClientClick="return(valNavegacionTablaValor('2'));" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtPageIndexGoTablaValor" Width="50px" CssClass="TextBox_ReadOnly"
                                                                                            runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div id="capaBuscarProducto_AddProd" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; top: 70px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaBuscarProducto_AddProd')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                TipoExistencia:
                            </td>
                            <td>
                                <asp:DropDownList ID="CboTipoExistencia_AddProd" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label17" runat="server" CssClass="Label" Text="Línea:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True"
                                    DataTextField="Descripcion" DataValueField="Id">
                                </asp:DropDownList>
                                <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Sub Línea:"></asp:Label>
                                <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" runat="server" DataTextField="Nombre"
                                    AutoPostBack="true" DataValueField="Id">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripción:"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarGrilla_AddProd"> 
                                            <asp:TextBox TabIndex="204" ID="txtDescripcionProd_AddProd" runat="server" Width="300px"
                                                onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd() );"></asp:TextBox>
                                                </asp:Panel>
                                        </td>
                                        <td class="Texto" style="text-align: right">
                                            Cód.:
                                        </td>
                                        <td>
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnBuscarGrilla_AddProd"> 
                                            <asp:TextBox ID="txtCodigoProducto" Font-Bold="true" Width="100px" runat="server"
                                                onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd() );"
                                                TabIndex="205"></asp:TextBox>
                                                </asp:Panel>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                TabIndex="207" onmouseout="this.src='../../Imagenes/Buscar_b.JPG';" onmouseover="this.src='../../Imagenes/Buscar_A.JPG';" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnLimpiar_AddProd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                onmouseout="this.src='../../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../../Imagenes/Limpiar_A.JPG';"
                                                OnClientClick="return(limpiarBuscarProducto());" TabIndex="209" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                        CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                        CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                        CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                        CollapsedText="Búsqueda Avanzada" ExpandedText="Búsqueda Avanzada" ExpandDirection="Vertical"
                        SuppressPostBack="true">
                    </cc1:CollapsiblePanelExtender>
                    <asp:Image ID="Image21_11" runat="server" Height="16px" />
                    <asp:Label ID="Label21_11" runat="server" Text="Búsqueda Avanzada" CssClass="Label"></asp:Label>
                    <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                        <table width="100">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Atributo:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                        Width="650px">
                                        <Columns>
                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                    <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                        DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="GrillaRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="DGV_AddProd" runat="server" AutoGenerateColumns="False" Width="100%">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" HeaderStyle-Width="85px"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                            <asp:TemplateField HeaderText="Código" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCodigo" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Codigo")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Descripcion" HeaderText="Descripción" HeaderStyle-Height="25px"
                                HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                            <asp:BoundField DataField="UnidadMedida" ItemStyle-HorizontalAlign="Center" HeaderText="U.M."
                                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text="<" ToolTip="Página Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                    <asp:Button TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text=">" ToolTip="Página Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                    <asp:TextBox TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true"
                        CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                    <asp:Button TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px"
                        Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacionProductos('2'));" />
                    <asp:TextBox TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly"
                        runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaPersona" style="border: 3px solid blue; padding: 4px; width: 900px;
        height: auto; position: absolute; top: 170px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:RadioButtonList ID="rdbTipoBusqueda_Persona" runat="server" CssClass="Texto"
                        Font-Bold="true" RepeatDirection="Horizontal" AutoPostBack="true">
                        <asp:ListItem Value="1" Selected="True">Empresa/Tienda/Perfil</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td class="Texto" style="font-weight: bold; text-align: right">
                                Empresa:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa_BuscarPersona" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto" style="font-weight: bold; text-align: right">
                                Tienda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTienda_BuscarPersona" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td colspan="5">
                                                <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                    AutoPostBack="false">
                                                    <asp:ListItem Selected="True" Value="N">Natural</asp:ListItem>
                                                    <asp:ListItem Value="J">Juridica</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Texto">
                                                Razón Social / Nombres:
                                            </td>
                                            <td colspan="4">
                                                <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this));" runat="server"
                                                    onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                    Width="450px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Texto">
                                                D.N.I.:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero());" onFocus="javascript:validarbusqueda();"
                                                    MaxLength="8" runat="server"></asp:TextBox>
                                            </td>
                                            <td class="Texto">
                                                R.U.C.:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero());"
                                                    MaxLength="11"></asp:TextBox>
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnBuscar_Persona_General" runat="server" Text="Buscar" Width="80px"
                                                                ToolTip="Buscar" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnAgregar_Persona_General" runat="server" Text="Agregar" Width="80px"
                                                                ToolTip="Agregar" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Texto">
                                                Rol:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboRol" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td colspan="3">
                                                <asp:Label ID="lbl_aviso_persona" runat="server" Text="La < Empresa > y  < Tienda > selecccionada son utilizadas para la asignacion de la comisión."
                                                    CssClass="LabelRojo"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        PageSize="20" Width="100%">
                                        <Columns>
                                            <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                            <asp:BoundField DataField="IdPersona" HeaderText="Cód." NullDisplayText="0" HeaderStyle-Height="25px"
                                                ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="Nombre" HeaderText="Nombre / Razón Social" NullDisplayText="---"
                                                HeaderStyle-Height="25px" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." NullDisplayText="---" HeaderStyle-Height="25px"
                                                ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." NullDisplayText="---" HeaderStyle-Height="25px"
                                                ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            </asp:BoundField>
                                        </Columns>
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                        ToolTip="Página Anterior" OnClientClick="return(valNavegacion('0'));" />
                                    <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                        ToolTip="Página Posterior" OnClientClick="return(valNavegacion('1'));" />
                                    <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                        runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                            Width="50px" Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacion('2'));" />
                                    <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                        onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_BusquedaPersona_Usuario" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold; text-align: right">
                                                Perfil:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboPerfil_BuscarPersona" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnBuscarPersona_Usuario" runat="server" Text="Buscar" Width="80px"
                                                    ToolTip="Buscar" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAdd_Persona_Usuario" Visible="false" runat="server" Text="Agregar"
                                                    Width="80px" ToolTip="Agregar los comisionistas seleccionados." />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_BuscarPersona_Usuario" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        PageSize="30" Width="100%">
                                        <Columns>
                                            <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                            <asp:BoundField DataField="IdPersona" HeaderText="Código" HeaderStyle-Height="25px"
                                                ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="Descripcion" HeaderText="Descripción" HeaderStyle-Height="25px"
                                                ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Dni" HeaderText="D.N.I." HeaderStyle-Height="25px" ItemStyle-Height="25px"
                                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                            <asp:BoundField DataField="Empresa" HeaderText="Empresa" HeaderStyle-Height="25px"
                                                ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Tienda" HeaderText="Tienda" HeaderStyle-Height="25px"
                                                ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Perfil" HeaderText="Perfil" HeaderStyle-Height="25px"
                                                ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            </asp:BoundField>
                                        </Columns>
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
        function Aceptar() {

            var FechaIni = document.getElementById('<%=txtFechaIni.ClientID %>').value;
            if (!valFecha_Blank(document.getElementById('<%=txtFechaIni.ClientID %>'))) {
                alert('INGRESE UNA FECHA VALIDA.\n NO SE PERMITE LA OPERACIÓN.');
                return false;
            }

            var FechaFin = document.getElementById('<%=txtFechaFin.ClientID %>').value;
            if (!valFecha_Blank(document.getElementById('<%=txtFechaFin.ClientID %>'))) {
                alert('INGRESE UNA FECHA VALIDA.\n NO SE PERMITE LA OPERACIÓN.');
                return false;
            }

            var IdEmpresa = parseInt(document.getElementById('<%=cboEmpresa.ClientID %>').value);
            var IdTienda = parseInt(document.getElementById('<%=cboTienda.ClientID %>').value);
            var IdComisionCab = parseInt(document.getElementById('<%=cboComisionExt.ClientID %>').value);

            var IdVendedor = parseInt(document.getElementById('<%=hddIdVendedor.ClientID %>').value);
            if (isNaN(IdVendedor)) { IdVendedor = 0; }


            var IdTipoExistencia = parseInt(document.getElementById('<%=cboTipoExistencia.ClientID %>').value);
            if (isNaN(IdTipoExistencia)) { IdTipoExistencia = 0; }

            var IdLinea = parseInt(document.getElementById('<%=cboLinea.ClientID %>').value);
            if (isNaN(IdLinea)) { IdLinea = 0; }

            var IdSubLinea = parseInt(document.getElementById('<%=cboSubLinea.ClientID %>').value);
            if (isNaN(IdSubLinea)) { IdSubLinea = 0; }


            var IdProducto = parseInt(document.getElementById('<%=hddIdProducto.ClientID %>').value);
            if (isNaN(IdProducto)) { IdProducto = 0; }


            var AgruparComision = 0;
            if (document.getElementById('<%=ckAgruparComision.ClientID %>').checked == true) {
                AgruparComision = 1;
            }

            iReportFrame.location.href = 'visorVentas.aspx?iReporte=32&IdEmpresa=' + IdEmpresa + '&IdTienda=' + IdTienda + '&IdComisionCab=' + IdComisionCab + '&IdVendedor=' + IdVendedor + '&FechaIni=' + FechaIni + '&FechaFin=' + FechaFin + '&IdTipoExistencia=' + IdTipoExistencia + '&IdLinea=' + IdLinea + '&IdSubLinea=' + IdSubLinea + '&IdProducto=' + IdProducto + '&AgruparComision=' + AgruparComision;

            return false;
        }


        /// Busqueda de producto

        function valKeyPressCodigoSL() {
            if (event.keyCode == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valKeyPressDescripcionProd() {
            if (event.keyCode == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }


        function limpiarBuscarProducto() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
            var txtCodigoProducto = document.getElementById('<%=txtCodigoProducto.ClientID%>');
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';
            txtCodigoProducto.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            //********* FILTRO AVANZADO
            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }


        function valOnClick_btnBuscarProducto() {
            onCapa('capaBuscarProducto_AddProd');
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
            return false;
        }

        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }

        /// fin de busqueda de producto


        /// busqueda de personas

        //***************************************************** BUSQUEDA PERSONA
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function mostrarCapaPersona() {
            onCapa('capaPersona');
            return false;
        }


        function valNavegacionTabla(tipoMov) {

            var index = parseInt(document.getElementById('<%=txtPageIndexTabla.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGoTabla.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=txtPageIndexGoTabla.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGoTabla.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=txtPageIndexGoTabla.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGoTabla.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function valNavegacionTablaValor(tipoMov) {

            var index = parseInt(document.getElementById('<%=txtPageIndexTablaValor.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGoTablaValor.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=txtPageIndexGoTablaValor.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGoTablaValor.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=txtPageIndexGoTablaValor.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGoTablaValor.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }


        function BuscarTablaValor() {
            if (event.keyCode == 13) {
                document.getElementById('<%=ImgBuscarTablaValor.ClientID %>').click();
                return false;
            }
            return true;
        }

        function BuscarTabla() {
            if (event.keyCode == 13) {
                document.getElementById('<%=ImgBuscarTabla.ClientID %>').click();
                return false;
            }
            return true;
        }

        function AgregarTablaValor() {

            var GV_TablaValor = document.getElementById('<%=GV_TablaValor.ClientID %>');
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');

            if (GV_TablaValor != null) {

                for (var j = 1; j < GV_TablaValor.rows.length; j++) {
                    var rowElem2 = GV_TablaValor.rows[j];

                    if (rowElem2.cells[0].children[0].cells[0].children[0].checked == true) {

                        if (grilla != null) {
                            for (var i = 1; i < grilla.rows.length; i++) {
                                var rowElem = grilla.rows[i];

                                if (rowElem2.cells[1].children[0].cells[0].children[0].value == rowElem.cells[1].children[0].cells[1].children[0].value && rowElem2.cells[2].children[0].cells[0].children[0].value == rowElem.cells[1].children[0].cells[2].children[0].value) {
                                    alert('LA LISTA DE ATRIBUTOS YA CONTIENE LA TABLA < ' + rowElem.cells[1].children[0].cells[0].children[0].innerText + ' > CON VALOR < ' + rowElem.cells[2].children[0].cells[0].children[0].innerText + ' >. \n NO SE PERMITE LA OPERACIÓN.');
                                    return false;
                                }

                            }
                        }

                    } //end id
                }
            }

            return true;
        }
        
    </script>

</asp:Content>
