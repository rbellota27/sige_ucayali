﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmComisionCab
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Private objCbo As New Combo

    Private listaComisionista As List(Of Entidades.Comisionista)
    Private listaRestriccionCliente As List(Of Entidades.Restriccion_Cliente)
    Private listaTipoPrecioV As List(Of Entidades.ComisionCab_TipoPrecioV)

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
    End Enum

    Private Function getListaComisionista() As List(Of Entidades.Comisionista)
        Return CType(Session.Item("listaComisionista"), List(Of Entidades.Comisionista))
    End Function
    Private Sub setListaComisionista(ByVal lista As List(Of Entidades.Comisionista))
        Session.Remove("listaComisionista")
        Session.Add("listaComisionista", lista)
    End Sub
    Private Function getListaRestriccionCliente() As List(Of Entidades.Restriccion_Cliente)
        Return CType(Session.Item("listaRestriccionCliente"), List(Of Entidades.Restriccion_Cliente))
    End Function
    Private Sub setListaRestriccionCliente(ByVal lista As List(Of Entidades.Restriccion_Cliente))
        Session.Remove("listaRestriccionCliente")
        Session.Add("listaRestriccionCliente", lista)
    End Sub
    Private Function getListaTipoPrecioV() As List(Of Entidades.ComisionCab_TipoPrecioV)
        Return CType(Session.Item("listaTipoPrecioV"), List(Of Entidades.ComisionCab_TipoPrecioV))
    End Function
    Private Sub setListaTipoPrecioV(ByVal lista As List(Of Entidades.ComisionCab_TipoPrecioV))
        Session.Remove("listaTipoPrecioV")
        Session.Add("listaTipoPrecioV", lista)
    End Sub

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        valOnLoad()
    End Sub

    Private Sub valOnLoad()
        Try

            If (Not Me.IsPostBack) Then
                ConfigurarDatos()
                inicializarFrm()
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub inicializarFrm()

        With objCbo
            .LlenarCboTipoPV(Me.cboTipoPv, False)
            .LlenarCboRol(Me.cboRol, True)

            .LlenarCboPropietario(Me.cboEmpresa_BuscarPersona, False)
            .llenarCboTiendaxIdEmpresa(Me.cboTienda_BuscarPersona, CInt(Me.cboEmpresa_BuscarPersona.SelectedValue), True)
            .LlenarCboPerfil(Me.cboPerfil_BuscarPersona, True)
            .llenarCboTipoComision(ddl_tipoComision)

        End With

        actualizarControles_BuscarPersona(CInt(Me.rdbTipoBusqueda_Persona.SelectedValue))

        verFrm(FrmModo.Inicio, False, True, True)

    End Sub
    Private Sub actualizarControles_BuscarPersona(ByVal opcionBuscarPersona As Integer)
        Me.pnlBusquedaPersona.Visible = False
        Me.Panel_BusquedaPersona_Usuario.Visible = False

        Select Case opcionBuscarPersona
            Case 0 '****************** GENERAL
                Me.pnlBusquedaPersona.Visible = True
            Case 1 '****************** EMPRESA / TIENDA
                Me.Panel_BusquedaPersona_Usuario.Visible = True
        End Select

    End Sub

    Protected Sub btnAgregarTipoPv_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAgregarTipoPv.Click
        valOnClick_btnAgregarTipoPv()
    End Sub
    Private Sub validar_Add_TipoPrecioV(ByVal IdTipoPrecioV As Integer)

        Me.listaTipoPrecioV = getListaTipoPrecioV()
        For i As Integer = 0 To Me.listaTipoPrecioV.Count - 1

            If (Me.listaTipoPrecioV(i).IdTipoPV = IdTipoPrecioV) Then
                Throw New Exception("EL TIPO DE PRECIO DE VENTA SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next

    End Sub
    Private Sub valOnClick_btnAgregarTipoPv()
        Try

            actualizarListaTipoPrecioV()
            Me.listaTipoPrecioV = getListaTipoPrecioV()
            Dim IdTipoPrecioV As Integer = CInt(Me.cboTipoPv.SelectedValue)

            validar_Add_TipoPrecioV(IdTipoPrecioV)

            Dim objTipoPrecioV As Entidades.TipoPrecioV = (New Negocio.TipoPrecioV).SelectxId(IdTipoPrecioV)(0)

            Dim objComision_TipoPV As New Entidades.ComisionCab_TipoPrecioV
            With objComision_TipoPV
                .IdTipoPV = objTipoPrecioV.IdTipoPv
                .TipoPrecioV = objTipoPrecioV.Nombre
                .Estado = True
            End With

            Me.listaTipoPrecioV.Add(objComision_TipoPV)
            setListaTipoPrecioV(Me.listaTipoPrecioV)

            Me.GV_TipoPrecioVenta.DataSource = Me.listaTipoPrecioV
            Me.GV_TipoPrecioVenta.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub actualizarListaTipoPrecioV()

        Me.listaTipoPrecioV = getListaTipoPrecioV()

        For i As Integer = 0 To Me.GV_TipoPrecioVenta.Rows.Count - 1

            Me.listaTipoPrecioV(i).Estado = CType(Me.GV_TipoPrecioVenta.Rows(i).FindControl("chbEstado"), CheckBox).Checked

        Next

        setListaTipoPrecioV(Me.listaTipoPrecioV)

    End Sub


    Protected Sub valOnClick_btnQuitar_TipoPrecioV(ByVal sender As Object, ByVal e As EventArgs)

        quitarTipoPrecioV(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)

    End Sub

    Private Sub quitarTipoPrecioV(ByVal index As Integer)
        Try

            actualizarListaTipoPrecioV()
            Me.listaTipoPrecioV = getListaTipoPrecioV()
            Me.listaTipoPrecioV.RemoveAt(index)
            setListaTipoPrecioV(Me.listaTipoPrecioV)

            Me.GV_TipoPrecioVenta.DataSource = Me.listaTipoPrecioV
            Me.GV_TipoPrecioVenta.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub GV_TipoPrecioVenta_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_TipoPrecioVenta.RowDataBound

        If (e.Row.RowType = DataControlRowType.DataRow) Then

            Dim btnQuitar As LinkButton = CType(e.Row.FindControl("btnQuitar_TipoPrecioV"), LinkButton)

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    btnQuitar.Visible = True
                Case FrmModo.Editar
                    btnQuitar.Visible = False
            End Select

        End If

    End Sub
    Private Sub verFrm(ByVal modo As Integer, ByVal limpiar As Boolean, ByVal initSession As Boolean, ByVal cargarGrillaDatos_Default As Boolean)

        If (limpiar) Then
            limpiarFrm()
        End If

        If (initSession) Then
            Me.listaComisionista = New List(Of Entidades.Comisionista)
            setListaComisionista(Me.listaComisionista)

            Me.listaTipoPrecioV = New List(Of Entidades.ComisionCab_TipoPrecioV)
            setListaTipoPrecioV(Me.listaTipoPrecioV)

            Me.listaRestriccionCliente = New List(Of Entidades.Restriccion_Cliente)
            setListaRestriccionCliente(Me.listaRestriccionCliente)
        End If

        If (cargarGrillaDatos_Default) Then

            Me.GV_BuscarComision.DataSource = (New Negocio.Comision_Cab).Comision_Cab_SelectxParams_DT(Nothing)
            Me.GV_BuscarComision.DataBind()

        End If

        Me.hddFrmModo.Value = CStr(modo)
        actualizarControles()

    End Sub
    Private Sub actualizarControles()

        Me.Panel_Registro.Visible = False
        Me.Panel_Busqueda.Visible = False
        Me.Panel_Registro.Enabled = False
        Me.Panel_Busqueda.Enabled = False

        Me.btnNuevo.Visible = False
        Me.btnGuardar.Visible = False
        Me.btnCancelar.Visible = False

        Select Case CInt(Me.hddFrmModo.Value)
            Case FrmModo.Inicio

                Me.Panel_Busqueda.Visible = True
                Me.Panel_Busqueda.Enabled = True

                Me.btnNuevo.Visible = True

            Case FrmModo.Nuevo

                Me.Panel_Registro.Visible = True
                Me.Panel_Busqueda.Visible = True
                Me.Panel_Registro.Enabled = True

                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True

            Case FrmModo.Editar

                Me.Panel_Registro.Visible = True
                Me.Panel_Busqueda.Visible = True
                Me.Panel_Registro.Enabled = True

                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True

        End Select


    End Sub
    Private Sub limpiarFrm()

        Me.txtAbv.Text = ""
        Me.txtDescripcion.Text = ""

        Me.GV_Comisionista.DataSource = Nothing
        Me.GV_Comisionista.DataBind()

        Me.GV_Restriccion_Cliente.DataSource = Nothing
        Me.GV_Restriccion_Cliente.DataBind()

        Me.GV_TipoPrecioVenta.DataSource = Nothing
        Me.GV_TipoPrecioVenta.DataBind()

        Me.chb_Factura.Checked = True
        Me.chb_Boleta.Checked = True

        Me.rdbEstado.SelectedValue = "1"

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        valOnClick_btnNuevo()
    End Sub
    Private Sub valOnClick_btnNuevo()
        Try

            verFrm(FrmModo.Nuevo, True, True, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As New List(Of Entidades.PersonaView)
        listaPersona = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)

        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text), True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer, ByVal cargarPuntoPartida As Boolean)

        'Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)

        'If (objPersona Is Nothing) Then Throw New Exception("NO SE HALLARON REGISTROS.")

        'Dim objCbo As New Combo
        'Me.txtNombre_Remitente.Text = objPersona.Descripcion
        'Me.txtIdRemitente.Text = CStr(objPersona.IdPersona)
        'Me.txtDNI_Remitente.Text = objPersona.Dni
        'Me.txtRUC_Remitente.Text = objPersona.Ruc
        'Me.hddIdRemitente.Value = CStr(objPersona.IdPersona)

        'If (cargarPuntoPartida) Then
        '    '**************** PARTIDA
        '    Dim ubigeo As String = objPersona.Ubigeo
        '    If (ubigeo.Trim.Length <= 0) Then
        '        ubigeo = "000000"
        '    End If

        '    Dim codDepto As String = ubigeo.Substring(0, 2)
        '    Dim codProv As String = ubigeo.Substring(2, 2)
        '    Dim codDist As String = ubigeo.Substring(4, 2)
        '    Me.cboDepto_Partida.SelectedValue = codDepto
        '    objCbo.LLenarCboProvincia(Me.cboProvincia_Partida, codDepto)
        '    Me.cboProvincia_Partida.SelectedValue = codProv
        '    objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, codDepto, codProv)
        '    Me.cboDistrito_Partida.SelectedValue = codDist
        '    Me.txtDireccion_Partida.Text = objPersona.Direccion
        'End If

        'Me.GV_BusquedaAvanzado.DataSource = Nothing
        'Me.GV_BusquedaAvanzado.DataBind()
        'Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        'Me.GV_DocumentosReferencia_Find.DataBind()

        'objScript.offCapa(Me, "capaPersona")

    End Sub

#End Region

    Private Sub cboEmpresa_BuscarPersona_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa_BuscarPersona.SelectedIndexChanged
        valOnChange_cboEmpresa_BuscarPersona()
    End Sub
    Private Sub valOnChange_cboEmpresa_BuscarPersona()
        Try

            objCbo.llenarCboTiendaxIdEmpresa(Me.cboTienda_BuscarPersona, CInt(Me.cboEmpresa_BuscarPersona.SelectedValue), True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub btnBuscarPersona_Usuario_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarPersona_Usuario.Click
        buscarPersona_EmpresaTienda()
    End Sub
    Private Sub buscarPersona_EmpresaTienda()
        Try

            Me.GV_BuscarPersona_Usuario.DataSource = (New Negocio.Usuario).Usuario_SelectxIdEmpresaxIdTiendaxIdPerfil_DT(CInt(Me.cboEmpresa_BuscarPersona.SelectedValue), CInt(Me.cboTienda_BuscarPersona.SelectedValue), CInt(Me.cboPerfil_BuscarPersona.SelectedValue))
            Me.GV_BuscarPersona_Usuario.DataBind()

            If (Me.GV_BuscarPersona_Usuario.Rows.Count <= 0) Then
                objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub rdbTipoBusqueda_Persona_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbTipoBusqueda_Persona.SelectedIndexChanged
        valOnChange_rdbTipoBusqueda_Persona()
    End Sub
    Private Sub valOnChange_rdbTipoBusqueda_Persona()
        Try

            actualizarControles_BuscarPersona(CInt(Me.rdbTipoBusqueda_Persona.SelectedValue))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnAdd_Persona_Usuario_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd_Persona_Usuario.Click
        valOnClick_btnAdd_Persona_Usuario()
    End Sub
    Private Sub valOnClick_btnAdd_Persona_Usuario()
        Try


            Select Case CInt(Me.hddOpcionPersona.Value)
                Case 0  '************************************ COMISIONISTA

                    actualizarListaComisionista()

                    Me.listaComisionista = getListaComisionista()

                    For i As Integer = 0 To Me.GV_BuscarPersona_Usuario.Rows.Count - 1

                        If (CType(Me.GV_BuscarPersona_Usuario.Rows(i).FindControl("chb_Select"), CheckBox).Checked) Then

                            Dim IdPersona As Integer = CInt(CType(Me.GV_BuscarPersona_Usuario.Rows(i).FindControl("hddIdPersona"), HiddenField).Value)

                            For x As Integer = 0 To Me.listaComisionista.Count - 1
                                If (Me.listaComisionista(x).IdPersona = IdPersona) Then
                                    Throw New Exception("LA PERSONA < " + Me.listaComisionista(x).Descripcion + " > YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
                                End If
                            Next

                            Dim objComisionista As Entidades.Comisionista = (New Negocio.Usuario).Usuario_SelectxIdPersona(IdPersona)
                            objComisionista.baseComision = "PV"
                            objComisionista.valPorcentaje = Decimal.Zero

                            objComisionista.Estado = True

                            If (objComisionista IsNot Nothing) Then
                                Me.listaComisionista.Add(objComisionista)
                            End If

                        End If

                    Next

                    setListaComisionista(Me.listaComisionista)
                    Me.GV_Comisionista.DataSource = Me.listaComisionista
                    Me.GV_Comisionista.DataBind()


                Case 1  '************************************ RESTRICCION PERSONA

                    actualizarListaRestriccion_Cliente()

                    Me.listaRestriccionCliente = getListaRestriccionCliente()

                    For i As Integer = 0 To Me.GV_BuscarPersona_Usuario.Rows.Count - 1

                        If (CType(Me.GV_BuscarPersona_Usuario.Rows(i).FindControl("chb_Select"), CheckBox).Checked) Then

                            Dim IdPersona As Integer = CInt(CType(Me.GV_BuscarPersona_Usuario.Rows(i).FindControl("hddIdPersona"), HiddenField).Value)

                            For x As Integer = 0 To Me.listaRestriccionCliente.Count - 1
                                If (Me.listaRestriccionCliente(x).IdPersona = IdPersona) Then
                                    Throw New Exception("LA PERSONA < " + Me.listaRestriccionCliente(x).Descripcion + " > YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
                                End If
                            Next

                            Dim objRestriccion_Cliente As Entidades.Restriccion_Cliente = obtenerRestriccion_Cliente_ET(IdPersona)
                            If (objRestriccion_Cliente IsNot Nothing) Then
                                Me.listaRestriccionCliente.Add(objRestriccion_Cliente)
                            End If

                        End If

                    Next

                    setListaRestriccionCliente(Me.listaRestriccionCliente)
                    Me.GV_Restriccion_Cliente.DataSource = Me.listaRestriccionCliente
                    Me.GV_Restriccion_Cliente.DataBind()


            End Select


            objScript.onCapa(Me, "capaPersona")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function obtenerRestriccion_Cliente_ET(ByVal IdPersona As Integer) As Entidades.Restriccion_Cliente

        Dim objRestriccion_Cliente As Entidades.Restriccion_Cliente = Nothing
        Dim objComisionista As Entidades.Comisionista = (New Negocio.Usuario).Usuario_SelectxIdPersona(IdPersona)
        If (objComisionista IsNot Nothing) Then

            objRestriccion_Cliente = New Entidades.Restriccion_Cliente
            With objRestriccion_Cliente

                .IdPersona = objComisionista.IdPersona
                .Descripcion = objComisionista.Descripcion
                .Dni = objComisionista.Dni
                .Empresa = objComisionista.Empresa
                .Tienda = objComisionista.Tienda
                .Perfil = objComisionista.Perfil
                .Estado = True

            End With


        Else
            Throw New Exception("NO SE HALLÓ LA PERSONA SELECCIONADA")
        End If

        Return objRestriccion_Cliente

    End Function

    Private Sub GV_Comisionista_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Comisionista.SelectedIndexChanged
        quitarComisionista(Me.GV_Comisionista.SelectedIndex)
    End Sub

    Private Sub quitarComisionista(ByVal index As Integer)
        Try

            actualizarListaComisionista()

            Me.listaComisionista = getListaComisionista()
            Me.listaComisionista.RemoveAt(index)
            setListaComisionista(Me.listaComisionista)

            Me.GV_Comisionista.DataSource = Me.listaComisionista
            Me.GV_Comisionista.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub actualizarListaComisionista()

        Me.listaComisionista = getListaComisionista()

        For i As Integer = 0 To Me.GV_Comisionista.Rows.Count - 1

            Me.listaComisionista(i).Estado = CType(Me.GV_Comisionista.Rows(i).FindControl("chbEstado"), CheckBox).Checked
            Me.listaComisionista(i).IdComisionista = Me.listaComisionista(i).IdPersona
            Me.listaComisionista(i).baseComision = CType(Me.GV_Comisionista.Rows(i).FindControl("gv_ddl_basecomision"), DropDownList).SelectedValue

            Dim strValPorcentaje As String = CType(Me.GV_Comisionista.Rows(i).FindControl("gv_txt_valcomision"), TextBox).Text
            If strValPorcentaje = "" Then
                Me.listaComisionista(i).valPorcentaje = Decimal.Zero
            Else
                Me.listaComisionista(i).valPorcentaje = CDec(strValPorcentaje)
            End If

        Next

        setListaComisionista(Me.listaComisionista)

    End Sub

    Private Sub actualizarListaRestriccion_Cliente()

        Me.listaRestriccionCliente = getListaRestriccionCliente()

        For i As Integer = 0 To Me.GV_Restriccion_Cliente.Rows.Count - 1

            Me.listaRestriccionCliente(i).Estado = CType(Me.GV_Restriccion_Cliente.Rows(i).FindControl("chbEstado"), CheckBox).Checked
            Me.listaRestriccionCliente(i).IdCliente = Me.listaRestriccionCliente(i).IdPersona

        Next

        setListaRestriccionCliente(Me.listaRestriccionCliente)

    End Sub

    Private Sub btnBuscar_Persona_General_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar_Persona_General.Click
        valOnClick_btnBuscar_Persona_General()
    End Sub
    Private Sub valOnClick_btnBuscar_Persona_General()
        Try

            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", Me.cboRol.SelectedValue)

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnAgregar_Persona_General_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAgregar_Persona_General.Click
        valOnClick_btnAgregar_Persona_General()
    End Sub
    Private Sub valOnClick_btnAgregar_Persona_General()
        Try

            Select Case CInt(Me.hddOpcionPersona.Value)
                Case 0 '****************** GENERAL

                    actualizarListaComisionista()

                    Me.listaComisionista = getListaComisionista()

                    For i As Integer = 0 To Me.gvBuscar.Rows.Count - 1

                        If (CType(Me.gvBuscar.Rows(i).FindControl("chb_Select"), CheckBox).Checked) Then

                            Dim IdPersona As Integer = CInt(CType(Me.gvBuscar.Rows(i).FindControl("hddIdPersona"), HiddenField).Value)

                            For x As Integer = 0 To Me.listaComisionista.Count - 1
                                If (Me.listaComisionista(x).IdPersona = IdPersona) Then
                                    Throw New Exception("LA PERSONA < " + Me.listaComisionista(x).Descripcion + " > YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
                                End If
                            Next

                            Dim objComisionista As Entidades.Comisionista = obtenerComisionista_FindGeneral(IdPersona)
                            objComisionista.Estado = True
                            If (objComisionista IsNot Nothing) Then
                                Me.listaComisionista.Add(objComisionista)
                            End If

                        End If

                    Next

                    setListaComisionista(Me.listaComisionista)
                    Me.GV_Comisionista.DataSource = Me.listaComisionista
                    Me.GV_Comisionista.DataBind()


                Case 1 '******************* EMPRESA / TIENDA

                    actualizarListaRestriccion_Cliente()

                    Me.listaRestriccionCliente = getListaRestriccionCliente()

                    For i As Integer = 0 To Me.gvBuscar.Rows.Count - 1

                        If (CType(Me.gvBuscar.Rows(i).FindControl("chb_Select"), CheckBox).Checked) Then

                            Dim IdPersona As Integer = CInt(CType(Me.gvBuscar.Rows(i).FindControl("hddIdPersona"), HiddenField).Value)

                            For x As Integer = 0 To Me.listaRestriccionCliente.Count - 1
                                If (Me.listaRestriccionCliente(x).IdPersona = IdPersona) Then
                                    Throw New Exception("LA PERSONA < " + Me.listaRestriccionCliente(x).Descripcion + " > YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
                                End If
                            Next

                            Dim objRestriccion_Cliente As Entidades.Restriccion_Cliente = obtenerRestriccion_Cliente_FindGeneral(IdPersona)
                            objRestriccion_Cliente.Estado = True
                            If (objRestriccion_Cliente IsNot Nothing) Then
                                Me.listaRestriccionCliente.Add(objRestriccion_Cliente)
                            End If

                        End If

                    Next

                    setListaRestriccionCliente(Me.listaRestriccionCliente)
                    Me.GV_Restriccion_Cliente.DataSource = Me.listaRestriccionCliente
                    Me.GV_Restriccion_Cliente.DataBind()


            End Select

            
            objScript.onCapa(Me, "capaPersona")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub



    Private Function obtenerRestriccion_Cliente_FindGeneral(ByVal IdPersona As Integer) As Entidades.Restriccion_Cliente

        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)
        Dim objRestriccion_Cliente As Entidades.Restriccion_Cliente = Nothing
        If (objPersona IsNot Nothing) Then

            objRestriccion_Cliente = New Entidades.Restriccion_Cliente
            With objRestriccion_Cliente

                .IdPersona = objPersona.IdPersona
                .Descripcion = objPersona.Descripcion
                .Dni = objPersona.Dni

            End With
        Else
            Throw New Exception("NO SE HALLÓ A LA PERSONA SELECCIONADA.")
        End If

        Return objRestriccion_Cliente

    End Function

    Private Function obtenerComisionista_FindGeneral(ByVal IdPersona As Integer) As Entidades.Comisionista

        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)
        Dim objComisionista As Entidades.Comisionista = Nothing
        If (objPersona IsNot Nothing) Then

            objComisionista = New Entidades.Comisionista
            With objComisionista

                .IdPersona = objPersona.IdPersona
                .Descripcion = objPersona.Descripcion
                .Dni = objPersona.Dni
                .baseComision = "PV"
                .valPorcentaje = Decimal.Zero
            End With
        Else
            Throw New Exception("NO SE HALLÓ A LA PERSONA SELECCIONADA.")
        End If

        Return objComisionista

    End Function

    Private Sub GV_BuscarPersona_Usuario_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_BuscarPersona_Usuario.PageIndexChanging
        Try

            Me.GV_BuscarPersona_Usuario.PageIndex = e.NewPageIndex
            buscarPersona_EmpresaTienda()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        valOnClick_btnCancelar()
    End Sub
    Private Sub valOnClick_btnCancelar()
        Try

            verFrm(FrmModo.Inicio, True, True, True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_Restriccion_Cliente_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Restriccion_Cliente.SelectedIndexChanged
        quitarRestriccion_Cliente(Me.GV_Restriccion_Cliente.SelectedIndex)
    End Sub
    Private Sub quitarRestriccion_Cliente(ByVal index As Integer)
        Try

            actualizarListaRestriccion_Cliente()
            Me.listaRestriccionCliente = getListaRestriccionCliente()

            Me.listaRestriccionCliente.RemoveAt(index)

            setListaRestriccionCliente(Me.listaRestriccionCliente)
            Me.GV_Restriccion_Cliente.DataSource = Me.listaRestriccionCliente
            Me.GV_Restriccion_Cliente.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        registrarComision()
    End Sub
    Private Sub registrarComision()

        Try

            actualizarListaComisionista()
            actualizarListaRestriccion_Cliente()
            actualizarListaTipoPrecioV()

            Me.listaComisionista = getListaComisionista()
            Me.listaRestriccionCliente = getListaRestriccionCliente()
            Me.listaTipoPrecioV = getListaTipoPrecioV()

            Dim objComision As Entidades.Comision_Cab = obtenerComisionCab()

            If (New Negocio.Comision_Cab).Registrar(objComision, Me.listaComisionista, Me.listaTipoPrecioV, Me.listaRestriccionCliente) Then

                verFrm(FrmModo.Inicio, True, True, True)
                objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito.")

            Else

                objScript.mostrarMsjAlerta(me,"PROBLEMAS EN LA OPERACIÓN")

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function obtenerComisionCab() As Entidades.Comision_Cab

        Dim objComision As New Entidades.Comision_Cab

        With objComision

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .IdComisionCab = Nothing
                Case FrmModo.Editar
                    .IdComisionCab = CInt(Me.hddIdComision.Value)
            End Select
            .Descripcion = Me.txtDescripcion.Text.Trim
            .Abv = Me.txtAbv.Text
            .Boleta = Me.chb_Boleta.Checked
            .Factura = Me.chb_Factura.Checked
            .IdTipoComision = CInt(Me.ddl_tipoComision.SelectedValue)

            Select Case CInt(Me.rdbEstado.SelectedValue)
                Case 0 '************ INACTIVO
                    .Estado = False
                Case 1 '************ ACTIVO
                    .Estado = True
            End Select

        End With

        Return objComision

    End Function

    Private Sub GV_BuscarComision_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_BuscarComision.SelectedIndexChanged
        cargarComisionCab_Editar(Me.GV_BuscarComision.SelectedIndex)
    End Sub
    Private Sub cargarComisionCab_Editar(ByVal index As Integer)

        Try

            Dim IdComisionCab As Integer = CInt(CType(Me.GV_BuscarComision.Rows(index).FindControl("hddIdComisionCab"), HiddenField).Value)

            Dim objComisionCab As Entidades.Comision_Cab = (New Negocio.Comision_Cab).Comision_Cab_SelectxIdComisionCab(IdComisionCab)
            Me.listaComisionista = (New Negocio.Comisionista).Comisionista_SelectxParams(IdComisionCab)
            Me.listaTipoPrecioV = (New Negocio.ComisionCab_TipoPrecioV).ComisionCab_TipoPrecioV_SelectxParams(IdComisionCab)
            Me.listaRestriccionCliente = (New Negocio.RestriccionComision_Cliente).RestriccionComision_Cliente_SelectxParams(IdComisionCab)

            cargarFrm_Editar(objComisionCab, Me.listaComisionista, Me.listaTipoPrecioV, Me.listaRestriccionCliente)

            setListaComisionista(Me.listaComisionista)
            setListaTipoPrecioV(Me.listaTipoPrecioV)
            setListaRestriccionCliente(Me.listaRestriccionCliente)

            verFrm(FrmModo.Editar, False, False, False)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub
    Private Sub cargarFrm_Editar(ByVal objComisionCab As Entidades.Comision_Cab, ByVal listaComisionista As List(Of Entidades.Comisionista), ByVal listaTipoPrecioV As List(Of Entidades.ComisionCab_TipoPrecioV), ByVal listaRestriccionCliente As List(Of Entidades.Restriccion_Cliente))

        '************************* CABECERA
        If (objComisionCab IsNot Nothing) Then

            With objComisionCab

                Me.hddIdComision.Value = CStr(.IdComisionCab)

                Me.txtDescripcion.Text = .Descripcion
                Me.txtAbv.Text = .Abv
                Me.chb_Boleta.Checked = .Boleta
                Me.chb_Factura.Checked = .Factura
                Me.ddl_tipoComision.SelectedValue = CStr(.IdTipoComision)

                If (.Estado) Then
                    If (Me.rdbEstado.Items.FindByValue(CStr(1)) IsNot Nothing) Then
                        Me.rdbEstado.SelectedValue = CStr(1)
                    End If
                Else
                    If (Me.rdbEstado.Items.FindByValue(CStr(0)) IsNot Nothing) Then
                        Me.rdbEstado.SelectedValue = CStr(0)
                    End If
                End If

            End With

        End If

        '************************** LISTA COMISIONISTA
        Me.GV_Comisionista.DataSource = listaComisionista
        Me.GV_Comisionista.DataBind()

        '************************** LISTA TIPO PV
        Me.GV_TipoPrecioVenta.DataSource = listaTipoPrecioV
        Me.GV_TipoPrecioVenta.DataBind()

        '************************** LISTA RESTRICCION CLIENTE
        Me.GV_Restriccion_Cliente.DataSource = listaRestriccionCliente
        Me.GV_Restriccion_Cliente.DataBind()



    End Sub
End Class
