﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class frmComisionDet
    Inherits System.Web.UI.Page

    Private cbo As Combo
    Dim objScript As New ScriptManagerClass
    Private listaProductoView As List(Of Entidades.ProductoView)
    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor
    Private listaComision_Det As List(Of Entidades.Comision_Det)
    Private listaMoneda As List(Of Entidades.Moneda)
    Private Const _IdTipoComision As Integer = 2

    Private Function getListaMoneda() As List(Of Entidades.Moneda)
        Return CType(Session.Item("listaMoneda"), List(Of Entidades.Moneda))
    End Function
    Private Sub setListaMoneda(ByVal lista As List(Of Entidades.Moneda))
        Session.Remove("listaMoneda")
        Session.Add("listaMoneda", lista)
    End Sub
    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Private Sub LLenar_Combo_onLoad()
        cbo = New Combo
        With cbo

            .llenarCboComsionCab(ddl_tipocomision, _IdTipoComision)
            .LlenarCboEmpresaxIdUsuario(ddl_empresa, CInt(hddIdUsuario.Value), False)
            .LlenarCboTiendaxIdEmpresaxIdUsuario(ddl_tienda, CInt(ddl_empresa.SelectedValue), CInt(hddIdUsuario.Value), False)

            .llenarCboTipoExistencia(Me.cboTipoExistencia, False)
            .llenarCboLineaxTipoExistencia(Me.cmbLinea_AddProd, CInt(Me.cboTipoExistencia.SelectedValue), True)
            .LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            .LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

        End With
    End Sub
    Private Sub on_load()
        If Not IsPostBack Then
            hddIdUsuario.Value = CStr(Session("IdUsuario"))

            LLenar_Combo_onLoad()
            ConfigurarDatos()


            Me.txt_fechaini.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.txt_fechafin.Text = Me.txt_fechaini.Text

            Me.listaMoneda = (New Negocio.Moneda).SelectCbo
            setListaMoneda(Me.listaMoneda)

            onClick_btn_limpiarproductos()
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        on_load()
    End Sub
    '++++++++++++++++++++++++++++++++++++++++++++++++++ Page_Load   
    Private Sub onChange_ddl_empresa(ByVal idEmpresa As String)
        cbo = New Combo
        cbo.LlenarCboTiendaxIdEmpresaxIdUsuario(ddl_tienda, CInt(idEmpresa), CInt(hddIdUsuario.Value), True)
    End Sub
    Private Sub ddl_empresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_empresa.SelectedIndexChanged
        onChange_ddl_empresa(ddl_empresa.SelectedValue)
    End Sub
    '++++++++++++++++++++++++++++++++++++++++++++++++++ ddl_empresa.SelectedIndexChanged
    Private Sub actualizarlistaComision_Det()

        Me.listaComision_Det = getlistaComision_Det()

        For i As Integer = 0 To Me.gv_productos.Rows.Count - 1
            Me.listaComision_Det(i).IdUnidadMedida = CInt(CType(Me.gv_productos.Rows(i).FindControl("gv_ddl_um"), DropDownList).SelectedValue)
            Me.listaComision_Det(i).IdMoneda = CInt(CType(Me.gv_productos.Rows(i).FindControl("gv_ddl_moneda"), DropDownList).SelectedValue)
            Me.listaComision_Det(i).ValorComision = CDec(CType(Me.gv_productos.Rows(i).FindControl("gv_txt_valorcomision"), TextBox).Text)
            Me.listaComision_Det(i).TipoCalculoCom = CStr(CType(Me.gv_productos.Rows(i).FindControl("gv_ddl_tipocalculo"), DropDownList).SelectedValue)
            Me.listaComision_Det(i).Dcto_Inicio = CDec(CType(Me.gv_productos.Rows(i).FindControl("gv_txt_dsctodesde"), TextBox).Text)
            Me.listaComision_Det(i).Dcto_Fin = CDec(CType(Me.gv_productos.Rows(i).FindControl("gv_txt_dsctohasta"), TextBox).Text)
            Me.listaComision_Det(i).FechaInicio = CDate(CType(Me.gv_productos.Rows(i).FindControl("gv_txt_fechaini"), TextBox).Text)
            Me.listaComision_Det(i).FechaFin = CDate(CType(Me.gv_productos.Rows(i).FindControl("gv_txt_fechafin"), TextBox).Text)
            Me.listaComision_Det(i).PrecioBaseComision = CStr(CType(Me.gv_productos.Rows(i).FindControl("gv_ddl_precioComision"), DropDownList).SelectedValue)
            Me.listaComision_Det(i).IncluyeIgv = CBool(CType(Me.gv_productos.Rows(i).FindControl("gv_cbk_igv"), CheckBox).Checked)
            Me.listaComision_Det(i).Estado = CBool(CType(Me.gv_productos.Rows(i).FindControl("gv_cbk_estado"), CheckBox).Checked)

        Next

        setlistaComision_Det(Me.listaComision_Det)

    End Sub
    Private Function getlistaComision_Det() As List(Of Entidades.Comision_Det)
        Return CType(Session.Item("listaComision_Det"), List(Of Entidades.Comision_Det))
    End Function
    Private Function obtenerDataTable_Producto() As DataTable

        Dim objDataTable As New DataTable

        objDataTable.Columns.Add("Columna1")

        For i As Integer = 0 To Me.DGV_AddProd.Rows.Count - 1

            If (CType(Me.DGV_AddProd.Rows(i).FindControl("chb_AddDetalle"), CheckBox).Checked) Then

                Dim IdProducto As Integer = CInt(CType(Me.DGV_AddProd.Rows(i).FindControl("hddIdProducto"), HiddenField).Value)
                objDataTable.Rows.Add(IdProducto)

            End If

        Next


        Return objDataTable

    End Function
    Private Function obtenerListaAdd_Campania_Detalle(ByVal TipoMov As Integer) As List(Of Entidades.Comision_Det)

        Dim listaAdd As New List(Of Entidades.Comision_Det)

        Dim index As Integer = 0
        Select Case TipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_ComisionDet.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_ComisionDet.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_ComisionDet.Text) - 1)
        End Select

        If Me.chb_SoloComisionDet.Checked Then

            listaAdd = (New Negocio.Comision_Det).Comision_SelectProducto_AddDetallexParams(CInt(ViewState("IdLineaDet")), CInt(ViewState("IdSubLineaDet")), Nothing, CInt(ViewState("IdTiendaDet")), CInt(ViewState("IdComisionCab")), Me.chb_SoloComisionDet.Checked, CStr(ViewState("DescripcionDet")), CStr(ViewState("CodigoProductoDet")), index, CInt(Me.cboPageSize.SelectedValue))
            txtPageIndex_ComisionDet.Text = CStr(index + 1)
        Else

            If (Me.chb_CargaMasiva.Checked) Then

                listaAdd = (New Negocio.Comision_Det).Comision_SelectProducto_AddDetallexParams(CInt(ViewState("IdLineaDet")), CInt(ViewState("IdSubLineaDet")), Nothing, CInt(ViewState("IdTiendaDet")), CInt(ViewState("IdComisionCab")), False, CStr(ViewState("DescripcionDet")), CStr(ViewState("CodigoProductoDet")), index, CInt(Me.cboPageSize.SelectedValue))
                txtPageIndex_ComisionDet.Text = CStr(index + 1)
            Else

                Dim objDataTable As DataTable = obtenerDataTable_Producto()
                listaAdd = (New Negocio.Comision_Det).Comision_SelectProducto_AddDetallexParams(Nothing, Nothing, objDataTable, CInt(Me.ddl_tienda.SelectedValue), CInt(Me.ddl_tipocomision.SelectedValue), False, "", "", 0, 0)
                txtPageIndex_ComisionDet.Text = ""
            End If

        End If



        Me.listaMoneda = getListaMoneda()
        For i As Integer = 0 To listaAdd.Count - 1
            listaAdd(i).ListaMoneda = Me.listaMoneda
            listaAdd(i).IdEmpresa = CInt(Me.ddl_empresa.SelectedValue)
            listaAdd(i).IdTienda = CInt(Me.ddl_tienda.SelectedValue)
            listaAdd(i).IdComisionCab = CInt(ddl_tipocomision.SelectedValue)

        Next
        Return listaAdd

    End Function
    Private Sub setlistaComision_Det(ByVal lista As List(Of Entidades.Comision_Det))
        Session.Remove("listaComision_Det")
        Session.Add("listaComision_Det", lista)
    End Sub

    Private Function validarDuplicadosComision_Det(ByVal lista_Comision_Det As List(Of Entidades.Comision_Det)) As List(Of Entidades.Comision_Det)
        Dim index% = 0
        Dim cont% = 0
        Dim totalreg As Integer

        totalreg = lista_Comision_Det.Count - 1
        Do While index < totalreg
            cont = 0
            For x As Integer = 0 To totalreg - 1
                If lista_Comision_Det(index).IdProducto = lista_Comision_Det(x).IdProducto Then
                    cont += 1
                    If cont > 1 Then
                        lista_Comision_Det.RemoveAt(x)
                        index -= 1
                        Exit For
                    End If
                End If
            Next
            totalreg = lista_Comision_Det.Count
            index += 1
        Loop

        Return lista_Comision_Det
    End Function
    Private Sub onClick_btnAddProductos_AddProd()
        actualizarlistaComision_Det()
        Me.listaComision_Det = getlistaComision_Det()

        ViewState.Add("IdLineaDet", CInt(Me.cmbLinea_AddProd.SelectedValue))
        ViewState.Add("IdSubLineaDet", CInt(Me.cmbSubLinea_AddProd.SelectedValue))
        ViewState.Add("DescripcionDet", Me.txtDescripcionProd_AddProd.Text.Trim)
        ViewState.Add("CodigoProductoDet", Me.txtCodigoProducto.Text.Trim)

        ViewState.Add("IdTiendaDet", CInt(Me.ddl_tienda.SelectedValue))
        ViewState.Add("IdComisionCab", CInt(Me.ddl_tipocomision.SelectedValue))

        Dim objDataTable As DataTable = obtenerDataTable_Producto()
        ViewState.Add("objDataTable", objDataTable)



        Dim listaAdd As List(Of Entidades.Comision_Det) = obtenerListaAdd_Campania_Detalle(0)
        Me.listaComision_Det.AddRange(listaAdd)

        setlistaComision_Det(validarDuplicadosComision_Det(Me.listaComision_Det))

        Me.gv_productos.DataSource = Me.listaComision_Det
        Me.gv_productos.DataBind()

        If Me.chb_SoloComisionDet.Checked = False Then
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        End If

    End Sub
    Private Sub val_onclick_btnAddProductos_AddProd()
        If gv_productos.Rows.Count > 0 Then
            ddl_empresa.Enabled = False
            ddl_tienda.Enabled = False
            ddl_tipocomision.Enabled = False
        Else
            ddl_empresa.Enabled = True
            ddl_tienda.Enabled = True
            ddl_tipocomision.Enabled = True
        End If
    End Sub
    Private Sub btnAddProductos_AddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddProductos_AddProd.Click

        onClick_btnAddProductos_AddProd()
        val_onclick_btnAddProductos_AddProd()
    End Sub
    '++++++++++++++++++++++++++++++++++++++++++++++++++ btnAddProductos_AddProd.Click

#Region "************************** BUSQUEDA PRODUCTO"
    Private Sub btnBuscarGrilla_AddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarGrilla_AddProd.Click
        BuscarProductoCatalogo(0)
    End Sub

    Protected Sub BuscarProductoCatalogo(ByVal opcion As Integer)
        Try

            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar

                    ViewState.Add("nombre_BuscarProducto", txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)
                    ViewState.Add("IdAlmacen_BuscarProducto", -1)
                    ViewState.Add("IdEmpresa_BuscarProducto", -1)
                    ViewState.Add("CodigoSubLinea_BuscarProducto", "")
                    ViewState.Add("IdTipoExistencia", Me.cboTipoExistencia.SelectedValue)

            End Select

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal codigoSubLinea As String, ByVal nomProducto As String, _
                                          ByVal IdAlmacen As Integer, ByVal tipoMov As Integer, _
                                          ByVal IdEmpresa As Integer, ByVal IdTipoExistencia As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select

        Dim codigoProducto As String = Me.txtCodigoProducto.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()

        Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams_V2(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, -1, -1, grilla.PageSize, index, codigoProducto, IdTipoExistencia, tableTipoTabla)

        If Me.listaProductoView.Count = 0 Then

            grilla.DataSource = Nothing
            grilla.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');    alert('No se hallaron registros.');        ", True)
            Return

        Else

            grilla.DataSource = Me.listaProductoView
            grilla.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');  ", True)

        End If

    End Sub
    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            -1, 1, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            -1, 2, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            -1, 3, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)


            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region
#Region "************************** BUSQUEDA AVANZADA PRODUCTOS"

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Protected Sub M_B_cmbSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1

            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)

        Next

        Return dt

    End Function
#End Region
    Private Sub gv_productos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv_productos.SelectedIndexChanged
        actualizarlistaComision_Det()
        listaComision_Det = getlistaComision_Det()
        listaComision_Det.RemoveAt(gv_productos.SelectedIndex)
        gv_productos.DataSource = listaComision_Det
        gv_productos.DataBind()

    End Sub

    Private Sub btn_guardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_guardar.Click
        Try
            actualizarlistaComision_Det()
            listaComision_Det = getlistaComision_Det()

            If ckb_Tienda.Checked = True Then
                For x As Integer = 0 To listaComision_Det.Count - 1
                    listaComision_Det(x).IdTienda = Nothing
                Next
            End If

            Dim objNEGComision_Det As New Negocio.Comision_Det
            objNEGComision_Det.Registrar(listaComision_Det)

            objScript.mostrarMsjAlerta(Me, "La operación finalizó con éxito")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub onClick_btn_limpiarproductos()
        txt_comision.Text = CStr(CDec(0))
        txt_montoini.Text = CStr(CDec(0))
        txt_montofin.Text = CStr(CDec(0))
        Me.listaComision_Det = New List(Of Entidades.Comision_Det)
        setlistaComision_Det(Me.listaComision_Det)
        gv_productos.DataBind()
    End Sub
    Private Sub btn_limpiarproductos_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_limpiarproductos.Click
        onClick_btn_limpiarproductos()
        val_onclick_btnAddProductos_AddProd()
    End Sub




    Private Sub cboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoExistencia.SelectedIndexChanged
        cbo = New Combo
        With cbo

            .llenarCboLineaxTipoExistencia(Me.cmbLinea_AddProd, CInt(Me.cboTipoExistencia.SelectedValue), True)
            .LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            .LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)
        End With

    End Sub

    Private Sub btnAnterior_ComisionDet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_ComisionDet.Click

        Me.listaComision_Det = New List(Of Entidades.Comision_Det)

        Dim listaAdd As List(Of Entidades.Comision_Det) = obtenerListaAdd_Campania_Detalle(1)
        Me.listaComision_Det.AddRange(listaAdd)

        setlistaComision_Det(validarDuplicadosComision_Det(Me.listaComision_Det))

        Me.gv_productos.DataSource = Me.listaComision_Det
        Me.gv_productos.DataBind()

    End Sub

    Private Sub btnSiguiente_ComisionDet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSiguiente_ComisionDet.Click
        Me.listaComision_Det = New List(Of Entidades.Comision_Det)

        Dim listaAdd As List(Of Entidades.Comision_Det) = obtenerListaAdd_Campania_Detalle(2)
        Me.listaComision_Det.AddRange(listaAdd)

        setlistaComision_Det(validarDuplicadosComision_Det(Me.listaComision_Det))

        Me.gv_productos.DataSource = Me.listaComision_Det
        Me.gv_productos.DataBind()
    End Sub

    Private Sub btnIr_ComisionDet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_ComisionDet.Click
        Me.listaComision_Det = New List(Of Entidades.Comision_Det)

        Dim listaAdd As List(Of Entidades.Comision_Det) = obtenerListaAdd_Campania_Detalle(3)
        Me.listaComision_Det.AddRange(listaAdd)

        setlistaComision_Det(validarDuplicadosComision_Det(Me.listaComision_Det))

        Me.gv_productos.DataSource = Me.listaComision_Det
        Me.gv_productos.DataBind()
    End Sub

End Class