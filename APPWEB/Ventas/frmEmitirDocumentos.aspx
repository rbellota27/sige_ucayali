<%@ Page Language="vb"  EnableEventValidation="false"  AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="frmEmitirDocumentos.aspx.vb" Inherits="APPWEB.frmEmitirDocumentos" title="Emitir Documento - Ventas" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="MsgBox" namespace="MsgBox" tagprefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">                
    <asp:UpdateProgress ID="UpdateProgress_Wait" runat="server">
                    <ProgressTemplate>
                    <div id="capaBase_Wait" style="width:100%; height:5000px; position:absolute; z-index:500;display:none; " class="modalBackground ">
                    </div>
                    <div id="capaBase_Wait_Texto" 
                            style="position:absolute;z-index:510; width:100%; height:100%; text-align:center; display:none; " 
                            class="Label">
                    <br >
                    <br >
                    <br >
                    <br >
                    <br >
                    <br >
                    <br >
                    <br >
                    <br >
                    <br >
                    <br >
                    <br >
                    
                    <h1>En proceso ...</h1> 
                    </div>
                    </ProgressTemplate>                    
                </asp:UpdateProgress>
    <table width="100%">
    <tr>
    <td style="background-color:Yellow">
    <table width="100%">
    <tr >
    <td style="width:350px"><asp:LinkButton CssClass="Label" Font-Bold="true"  ID="lkb_CapaConfigFrm" OnClientClick="return(onCapa('capaConfiguracionFrm'));"  runat="server">Config. Formulario</asp:LinkButton></td>
    <td style="text-align:right"><asp:LinkButton ForeColor="Green" CssClass="Texto" Font-Bold="true"  ID="btnHelp" OnClientClick=" return( onCapa('capaHelp') );  "  runat="server">Ayuda</asp:LinkButton></td>
    </tr>
    </table>
    </td>
    </tr>    
        <tr>
            <td class="TituloCelda">
            Emitir Documento
            </td>
        </tr>
        
        
        <tr>
        <td>
        <table>
        <tr>
        <td>
            <asp:Button ID="btnNuevo"  Width="70px" runat="server" Text="Nuevo" OnClientClick="return(  valNuevo()   );"  /></td>
        <td>
            <asp:Button ID="btnEditar"  Width="70px" runat="server" Text="Editar" OnClientClick="return(valEditar());" />
        </td>
        <td>
            <asp:Button ID="btnGuardar"  Width="70px" runat="server" Text="Guardar" OnClientClick="return(valSaveDocumento());" />        
        </td>
        <td>
        <asp:Button ID="btnAnular"  Width="70px" runat="server" Text="Anular" OnClientClick="return(valAnular());" />
        </td>
        <td>
        <asp:Button ID="btnBuscar"  Width="70px" runat="server" Text="Buscar"  OnClientClick="return(  valBuscar() );" />
        </td>
        <td>
        <asp:Button ID="btnBuscarDocumento"  Width="95px" runat="server" Text="Buscar Doc."  OnClientClick="return(validarBuscarDocumento());" />
        </td>
        <td>
        <asp:Button ID="btnImprimir"  Width="70px" runat="server" Text="Imprimir"  OnClientClick="return(validarMostrarCapaImpresion('0'));" />
        </td>
        <td>
        <asp:Button ID="btnDespachar"  Width="80px" runat="server" Text="Despachar"  OnClientClick="return(validarMostrarCapaImpresion('1'));" />
        </td>
        <td>
        <asp:Button ID="LinkButton1"  Width="110px" runat="server" Text="Emitir Gu�a Rem."  OnClientClick="return(validarMostrarCapaImpresion('2'));" />
        </td>
        <td>
        <asp:Button ID="btnCanjearCotizacion"  Width="125px" runat="server" Text="Canjear Cotizaci�n"  OnClientClick="return(  valCanjearCotizacion() );" />
        </td>
        <td>
        <asp:Button ID="btnGenerarCompPercepcion"  Width="125px" runat="server" Text="Comp. Percepci�n" ToolTip="Generar Comprobante de Percepci�n"  OnClientClick="return(  validarMostrarCapaImpresion('3') );" />
        </td>
        <td>
        <asp:Button ID="Button2"  Visible="false" Enabled="true" runat="server" Text="Imprimir" Width="10px" OnClientClick="return(  imprimirDocumento_JS()  );"  />
        </td>
        </tr>
        </table>
        </td>
        </tr>
    <tr class="TituloCeldaLeft" style=" height:20px">
    <td>
       
    </td>
    </tr>
    <tr>
    <td>
    
         <asp:Panel ID="Panel_FrmPrincipal" runat="server">
        <table border="0" cellspacing="0">
        <tr>
        <td>
        
            <asp:UpdatePanel ID="UpdatePanel_Cabecera" runat="server">
            <ContentTemplate>
        <table border="0" cellspacing="0" width="100%">
                    
                  <tr>
                                     <td style="width: 688px"/>
                                       
                                     <td>
                             <asp:Label ID="Label33" runat="server" CssClass="Label" Text="Tipo de Documento"></asp:Label>
                            <asp:DropDownList ID="cboTipoDocumento" runat="server" Width="140px"  
                                 AutoPostBack="True" CssClass="Combo">
                                 <asp:ListItem Selected="True" Value="1">Factura</asp:ListItem>
                                 <asp:ListItem Value="3">Boleta</asp:ListItem>
                                 <asp:ListItem Value="14">Cotizaci�n</asp:ListItem>
                            </asp:DropDownList>                          
                                     </td>
                                 </tr>
                                 <tr>
                                     <td style="width: 688px">
                            <asp:Label ID="Label52" runat="server" CssClass="Label" 
                                Text="Empresa"></asp:Label>
                            <asp:DropDownList ID="cboPropietario" runat="server" Width="248px" 
                                 AutoPostBack="True" CssClass="Combo" Font-Bold="True">
                            </asp:DropDownList>
                            <asp:Label ID="Label76" runat="server" CssClass="Label" 
                                Text="Tienda"></asp:Label>
                                         <asp:DropDownList ID="CboTienda" runat="server" Width="211px" 
                                 Font-Bold="True" AutoPostBack="True" Enabled="False">
                            </asp:DropDownList>
                                     </td>
                                     <td>
                                         <asp:Label ID="Label77" runat="server" CssClass="Label" Text="N� Serie"></asp:Label>
                             <asp:DropDownList ID="cboSerie" runat="server" AutoPostBack="True" 
                                 Width="56px" Font-Bold="True"  >
                             </asp:DropDownList>
                            <asp:Label ID="Label35" runat="server" CssClass="Label" Text="N�"></asp:Label>
                             <asp:TextBox ID="txtCodigoDocumento" runat="server" Width="104px" 
                                 ReadOnly="true" onKeyPress="return(  valKeyPressBuscarDocumento()     );" CssClass="TextBox_ReadOnly" Font-Bold="True"   
                                  ></asp:TextBox>
                                        
                                     </td>
                                 </tr>


                    </table>    
            </ContentTemplate>
            </asp:UpdatePanel>
        
        
        
        </td>
        </tr>    
         <tr>
             <td style="text-align: left;">
              
                 <table border="0" cellspacing="0" width="100%">
                  <tr>
                         <td style="text-align: left; ">
                           <asp:Label ID="Label63" runat="server" CssClass="Label"  Text="Moneda"></asp:Label>
                            <asp:DropDownList ID="cboMoneda" runat="server" 
                                 CssClass="Combo" AutoPostBack="True" >
                            </asp:DropDownList>
                             <asp:Label ID="Label54" runat="server" CssClass="Label" 
                                 Text="Estado del Documento"></asp:Label> 
                             <asp:DropDownList ID="cboEstadoDocumento" runat="server" 
                                 CssClass="ComboRojo" Enabled="False" Width="129px"></asp:DropDownList>
                             <asp:Label ID="Label46" runat="server" CssClass="Label" Text="Fecha de Emisi�n"></asp:Label> 
                                
                                
                                
                                
                                
                                
                                
                                
                             <asp:TextBox   ID="txtFechaEmision" runat="server" Width="90px" onblur="return(valFecha(this));"
                                   Font-Bold="True" CssClass="TextBox_Fecha" ></asp:TextBox>                             
                             <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender" runat="server" 
                                 CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" 
                                 CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                 CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True"                                  
                                 TargetControlID="txtFechaEmision"
                               ClearMaskOnLostFocus="false"    
                                  Mask="99/99/9999"
                                 
                                 >
                             </cc1:MaskedEditExtender>
                               <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender" runat="server" 
                                            TargetControlID="txtFechaEmision" Enabled="True" Format="dd/MM/yyyy" >
                                        </cc1:CalendarExtender>                                            
                                         
                             
                             
                             
                             
                             
                             
                             
                             
                         </td>
                     </tr>
                     
                     <tr>
                     <td class="TituloCelda" style="text-align: left; ">
                     
                         <asp:ImageButton ID="btnAgregar" runat="server" 
                             ImageUrl="~/Imagenes/BuscarProducto_b.JPG" 
                             OnClientClick="return(mostrarCapaCatalogo());" 
                             onmouseout="this.src='/Imagenes/BuscarProducto_b.JPG';" 
                             onmouseover="this.src='/Imagenes/BuscarProducto_A.JPG';" />
                         <asp:Button ID="btnAddProductoxId" runat="server" 
                             OnClientClick="return(showCapaBuscarProdxId());" Text="Buscar x ID" 
                             ToolTip="A�adir Producto al Detalle por su ID." />
                         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lista de Productos</td>
                 </tr>                                      
                   
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
              
                     <tr>
                     <td>
                         <!-- **************************** OPTIMIZACION  -->
                         <asp:UpdatePanel ID="UpdatePanel_DETALLE_DOC" runat="server" 
                             UpdateMode="Conditional">
                             <ContentTemplate>
                                 <table>
                                     <tr>
                                         <td style="vertical-align: top;">
                                         
                                             <asp:GridView ID="gvDetalle" runat="server" AutoGenerateColumns="False" 
                                                 CellPadding="4" GridLines="None" style="margin-top: 0px">
                                                 <Columns>
                                                     <asp:CommandField SelectText="Quitar" ShowSelectButton="True">
                                                         <HeaderStyle Width="10px" />
                                                     </asp:CommandField>
                                                     <asp:BoundField DataField="IdProducto" HeaderText="C�digo" ReadOnly="True">
                                                         <HeaderStyle Width="35px" />
                                                     </asp:BoundField>
                                                     <asp:TemplateField HeaderText="Cantidad">
                                                         <ItemTemplate>
                                                             <table>
                                                                 <tr>
                                                                     <td>
                                                                         <asp:TextBox ID="txtCantidad" runat="server" MaxLength="7" 
                                                                             onblur="return(valBlur_Cantidad());" onfocus="return(aceptarFoco(this));" 
                                                                             onKeypress="return(validarNumeroPunto());" 
                                                                             onKeyUp="return(calcularImporte('0'));" TabIndex="10" 
                                                                             Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F2}")%>' 
                                                                             Width="56px"></asp:TextBox>
                                                                     </td>
                                                                     <td>
                                                                         <asp:ImageButton ID="btnArea" runat="server" ImageUrl="~/Imagenes/Area_b.JPG" 
                                                                             OnClientClick="return(onCapaAddMagnitud('capaAddMagnitud'));" 
                                                                             onmouseout="this.src='/Imagenes/Area_b.JPG';" 
                                                                             onmouseover="this.src='/Imagenes/Area_A.JPG';" />
                                                                         <asp:ImageButton ID="btnLongitud" runat="server" 
                                                                             ImageUrl="~/Imagenes/Longitud_B.JPG" 
                                                                             OnClientClick="return(onCapaLongitud('CapaLongitud'));" 
                                                                             onmouseout="this.src='/Imagenes/Longitud_B.JPG';" 
                                                                             onmouseover="this.src='/Imagenes/Longitud_A.JPG';" />
                                                                         <asp:ImageButton ID="btnGlosa" runat="server" ImageUrl="~/Imagenes/Glosa_B.jpg" 
                                                                             OnClientClick="return(    onCapaGlosa()       );" 
                                                                             onmouseout="this.src='/Imagenes/Glosa_B.jpg';" 
                                                                             onmouseover="this.src='/Imagenes/Glosa_A.jpg';" />
                                                                     </td>
                                                                 </tr>
                                                             </table>
                                                         </ItemTemplate>
                                                         <HeaderStyle Width="60px" />
                                                     </asp:TemplateField>
                                                     <asp:BoundField DataField="StockDisponible" DataFormatString="{0:F3}" 
                                                         HeaderText="Stock">
                                                         <HeaderStyle Width="30px" />
                                                     </asp:BoundField>
                                                     <asp:TemplateField HeaderText="U.Medida">
                                                         <ItemTemplate>
                                                             <asp:DropDownList ID="cboUnidadMedida" runat="server" AutoPostBack="True" 
                                                                 DataSource='<%#DataBinder.Eval(Container.DataItem,"ListaUMedida")%>' 
                                                                 DataTextField="DescripcionCorto" DataValueField="Id" 
                                                                 onselectedindexchanged="cboUnidadMedida_SelectedIndexChanged" Width="64px">
                                                             </asp:DropDownList>
                                                         </ItemTemplate>
                                                         <HeaderStyle Width="20px" />
                                                     </asp:TemplateField>
                                                     <asp:BoundField DataField="getNomProducto" HeaderText="Descripci�n" />
                                                     <asp:BoundField DataField="PrecioSD" DataFormatString="{0:F2}" 
                                                         HeaderText="Precio">
                                                         <HeaderStyle Width="30px" />
                                                     </asp:BoundField>
                                                     <asp:TemplateField HeaderText="Dcto">
                                                         <ItemTemplate>
                                                             <asp:TextBox ID="txtDescuento" runat="server" MaxLength="4" 
                                                                 onblur="return(valBlur());" onfocus="return(aceptarFoco(this));" 
                                                                 onKeypress="return(validarNumeroPunto());" 
                                                                 onKeyUp="return(calcularImporte('1'));" 
                                                                 Text='<%#DataBinder.Eval(Container.DataItem,"Descuento","{0:F2}")%>' 
                                                                 Width="43px"></asp:TextBox>
                                                         </ItemTemplate>
                                                         <HeaderStyle Width="10px" />
                                                     </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="%Dcto ">
                                                         <ItemTemplate>
                                                             <asp:TextBox ID="txtPDescuento" runat="server" MaxLength="3" 
                                                                 onblur="return(valBlur());" onfocus="return(aceptarFoco(this));" 
                                                                 onKeypress="return(validarNumeroPunto());" 
                                                                 onKeyUp="return(calcularImporte('2'));" 
                                                                 Text='<%#DataBinder.Eval(Container.DataItem,"PDescuento","{0:F2}")%>' 
                                                                 Width="36px"></asp:TextBox>
                                                         </ItemTemplate>
                                                         <HeaderStyle Width="10px" />
                                                     </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="P.Final">
                                                         <ItemTemplate>
                                                             <asp:TextBox ID="txtPrecioCD" runat="server" MaxLength="7" 
                                                                 onblur="return(valBlur());" onfocus="return(aceptarFoco(this));" 
                                                                 onKeypress="return(validarNumeroPunto());" 
                                                                 onKeyUp="return(calcularImporte('3'));" TabIndex="11" 
                                                                 Text='<%#DataBinder.Eval(Container.DataItem,"PrecioCD","{0:F2}")%>' 
                                                                 Width="56px"></asp:TextBox>
                                                         </ItemTemplate>
                                                         <HeaderStyle Width="40px" />
                                                     </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Importe">
                                                         <ItemTemplate>
                                                             <asp:TextBox ID="txtImporte" runat="server" CssClass="TextBoxReadOnly" 
                                                                 Font-Bold="true" MaxLength="7" onFocus="return( lostFocus() );" 
                                                                 onKeypress="return( false );" 
                                                                 Text='<%#DataBinder.Eval(Container.DataItem,"Importe","{0:F2}")%>' Width="56px"></asp:TextBox>
                                                         </ItemTemplate>
                                                         <HeaderStyle Width="40px" />
                                                     </asp:TemplateField>
                                                     <asp:BoundField DataField="Percepcion" DataFormatString="{0:F2}" 
                                                         HeaderText="Perc. (%)">
                                                         <HeaderStyle Width="30px" />
                                                     </asp:BoundField>
                                                     <asp:BoundField DataField="Detraccion" DataFormatString="{0:F2}" 
                                                         HeaderText="Detr. (%)">
                                                         <HeaderStyle Width="30px" />
                                                     </asp:BoundField>
                                                 </Columns>
                                                 <RowStyle CssClass="GrillaRow" />
                                                 <HeaderStyle CssClass="GrillaHeader" />
                                                 <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                 <SelectedRowStyle CssClass="GrillaSelectedRow" Wrap="True" />
                                             </asp:GridView>
                                             
                                         </td>
                                     </tr>
                                     <tr>
                                         <td style="height: 56px">
                                             <table border="0" cellspacing="0">
                                                 <tbody>
                                                 
                                                     <tr>
                                                         <td style="width: 68px; text-align: right;">
                                                             <asp:Button ID="Button1" runat="server" 
                                                                 OnClientClick="return(valMostrarVistaPrevia());" TabIndex="12" 
                                                                 Text="Recalcular" />
                                                         </td>
                                                         <td>
                                                             &nbsp;</td>
                                                         <td style="text-align: right; width: 109px;">
                                                             <asp:Label ID="Label20" runat="server" CssClass="Label" Text="Descuento"></asp:Label>
                                                         </td>
                                                         <td>
                                                             <asp:TextBox ID="txtDescuento" runat="server" CssClass="TextBox_ReadOnly" 
                                                                 onFocus="return( lostFocus() );" onKeypress="return( false );" Width="90px">0</asp:TextBox>
                                                         </td>
                                                         <td style="text-align: right">
                                                             <asp:Label ID="Label21" runat="server" CssClass="Label" Text="Sub Total"></asp:Label>
                                                         </td>
                                                         <td>
                                                             <asp:TextBox ID="txtSubTotal" runat="server" CssClass="TextBox_ReadOnly" 
                                                                 onFocus="return( lostFocus() );" onKeypress="return( false );" Width="90px">0</asp:TextBox>
                                                         </td>
                                                         <td style="width: 74px; text-align: right;">
                                                             <asp:Label ID="Label24" runat="server" CssClass="Label" Text="I.G.V."></asp:Label>
                                                             &nbsp;<asp:Label ID="lblIgv" runat="server" CssClass="Label" Text="19"></asp:Label>
                                                             <asp:Label ID="Label86" runat="server" CssClass="Label" Text="%"></asp:Label>
                                                         </td>
                                                         <td>
                                                             <asp:TextBox ID="txtIGV" runat="server" CssClass="TextBox_ReadOnly" 
                                                                 onFocus="return( lostFocus() );" onKeypress="return( false );" Width="77px">0</asp:TextBox>
                                                         </td>
                                                         <td style="text-align: right; width: 85px;">
                                                             <asp:Label ID="Label22" runat="server" CssClass="Label" Text="Total :"></asp:Label>
                                                         </td>
                                                         <td>
                                                             <asp:Label ID="lblSimboloMoneda1" runat="server" CssClass="Label" Text="S/."></asp:Label>
                                                             <asp:TextBox ID="txtTotal" runat="server" CssClass="TextBox_ReadOnly" 
                                                                 onFocus="return( lostFocus() );" onKeypress="return( false );" Width="90px">0</asp:TextBox>
                                                         </td>
                                                     </tr>
                                                     <tr>
                                                         <td style="text-align: right; width: 68px;">
                                                             <asp:Label ID="Label25" runat="server" CssClass="Label" Text="Percepci�n"></asp:Label>
                                                         </td>
                                                         <td>
                                                             <asp:TextBox ID="txtPercepcion" runat="server" CssClass="TextBox_ReadOnly" 
                                                                 onFocus="return( lostFocus() );" onKeypress="return( false );" Width="90px">0</asp:TextBox>
                                                         </td>
                                                         <td style="width: 109px; text-align: right;">
                                                             <asp:Label ID="Label28" runat="server" CssClass="Label" 
                                                                 Text="Valor Referencial" Width="105px"></asp:Label>
                                                         </td>
                                                         <td>
                                                             <asp:TextBox ID="txtValorReferencial" runat="server" 
                                                                 CssClass="TextBox_ReadOnly" onFocus="return( lostFocus() );" 
                                                                 onKeypress="return( false );" Width="90px">0</asp:TextBox>
                                                         </td>
                                                         <td>
                                                             <asp:Label ID="Label26" runat="server" CssClass="Label" Text="Detracci�n"></asp:Label>
                                                         </td>
                                                         <td>
                                                             <asp:TextBox ID="txtDetraccion" runat="server" CssClass="TextBox_ReadOnly" 
                                                                 onFocus="return( lostFocus() );" onKeypress="return( false );" Width="90px">0</asp:TextBox>
                                                         </td>
                                                         <td style="text-align: right; width: 74px;">
                                                             <asp:Label ID="Label27" runat="server" CssClass="Label" Text="Retenci�n"></asp:Label>
                                                         </td>
                                                         <td>
                                                             <asp:TextBox ID="txtRetencion" runat="server" CssClass="TextBox_ReadOnly" 
                                                                 onFocus="return( lostFocus() );" onKeypress="return( false );" Width="77px">0</asp:TextBox>
                                                         </td>
                                                         <td style="width: 85px; text-align: right;">
                                                             <asp:Label ID="Label29" runat="server" CssClass="Label" Text="Total a Pagar :"></asp:Label>
                                                         </td>
                                                         <td>
                                                             <asp:Label ID="lblSimboloMoneda2" runat="server" CssClass="Label" Text="S/."></asp:Label>
                                                             <asp:TextBox ID="txtTotalAPagar" runat="server" CssClass="TextBox_ReadOnly" 
                                                                 onFocus="return( lostFocus() );" onKeypress="return( false );" Width="90px">0</asp:TextBox>
                                                         </td>
                                                     </tr>
                                                     
                                                 </tbody>
                                             </table>
                                         </td>
                                     </tr>
                                 </table>
                             </ContentTemplate>
                             <Triggers>
                                 <asp:AsyncPostBackTrigger ControlID="btnAddProductos_AddProd" 
                                     EventName="Click" />
                                 <asp:AsyncPostBackTrigger ControlID="btnBuscarClientexIdCliente" 
                                     EventName="Click" />
                                 <asp:AsyncPostBackTrigger ControlID="btnGuardarCliente" EventName="Click" />
                                 <asp:AsyncPostBackTrigger ControlID="DGV_BuscarPersona" 
                                     EventName="SelectedIndexChanged" />
                                 <asp:AsyncPostBackTrigger ControlID="cboTipoPrecioV" 
                                     EventName="SelectedIndexChanged" />
                             </Triggers>
                         </asp:UpdatePanel>
                     
                                          
                     
                     </td>
                     </tr>
                     <tr>
                     <td >
                     
                         <asp:UpdatePanel ID="UpdatePanel_Cliente" runat="server">
                             <ContentTemplate>
                                 <table width="100%">
                                     <tr>
                                         <td class="TituloCeldaLeft">
                                             <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="server" 
                                                 CollapseControlID="ImgCliente" Collapsed="false" 
                                                 CollapsedImage="~/Imagenes/Mas_B.JPG" CollapsedSize="0" 
                                                 CollapsedText="Datos del Cliente" ExpandControlID="ImgCliente" 
                                                 ExpandDirection="Vertical" ExpandedImage="~/Imagenes/Menos_B.JPG" 
                                                 ExpandedSize="252" ExpandedText="Datos del Cliente" ImageControlID="ImgCliente" 
                                                 SuppressPostBack="True" TargetControlID="PnlCliente" TextLabelID="lblCliente" />
                                             <asp:Image ID="ImgCliente" runat="server" Width="16px" />
                                             <asp:Label ID="lblCliente" runat="server" CssClass="LabelBlanco" 
                                                 Text="Datos del Cliente"></asp:Label>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td>
                                             <asp:Panel ID="PnlCliente" runat="server">
                                                 <table cellspacing="0" style="width: 100%">
                                                     <tr>
                                                         <td style="text-align: right;">
                                                             &nbsp;</td>
                                                         <td>
                                                             <asp:ImageButton ID="btnBuscarCliente" runat="server" 
                                                                 ImageUrl="~/Imagenes/Buscar_b.JPG" 
                                                                 OnClientClick="return(mostrarCapaPersona());" 
                                                                 onmouseout="this.src='/Imagenes/Buscar_b.JPG';" 
                                                                 onmouseover="this.src='/Imagenes/Buscar_A.JPG';" TabIndex="13" />
                                                             <asp:ImageButton ID="btnNuevoCliente" runat="server" 
                                                                 ImageUrl="~/Imagenes/Nuevo_b.JPG" OnClientClick="return(onCapaCliente());" 
                                                                 onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" 
                                                                 onmouseover="this.src='/Imagenes/Nuevo_A.JPG';" TabIndex="14" />
                                                             <asp:ImageButton ID="btnEditar_Cliente" runat="server" 
                                                                 ImageUrl="~/Imagenes/Editar_B.JPG" 
                                                                 OnClientClick=" return(    valEditarCliente()   ); " 
                                                                 onmouseout="this.src='/Imagenes/Editar_B.JPG';" 
                                                                 onmouseover="this.src='/Imagenes/Editar_A.JPG';" Visible="true" />
                                                             <asp:Label ID="Label12024" runat="server" CssClass="Label" 
                                                                 Text="Tipo de Persona"></asp:Label>
                                                             <asp:DropDownList ID="cboTipoPersona" runat="server" Enabled="False" 
                                                                 Font-Bold="True" Width="116px">
                                                                 <asp:ListItem Value="0">---------</asp:ListItem>
                                                                 <asp:ListItem Value="1">Natural</asp:ListItem>
                                                                 <asp:ListItem Value="2">Juridica</asp:ListItem>
                                                             </asp:DropDownList>
                                                         </td>
                                                     </tr>
                                                     <tr>
                                                         <td style="text-align: right;">
                                                             <asp:Label ID="Label1" runat="server" CssClass="Label" 
                                                                 Text="Ap. Paterno o R. Social:"></asp:Label>
                                                         </td>
                                                         <td>
                                                             <asp:TextBox ID="txtApPaterno_RazonSocial" runat="server" 
                                                                 CssClass="TextBoxReadOnly" ReadOnly="True" Width="270px"></asp:TextBox>
                                                             <asp:Label ID="Label39_12" runat="server" CssClass="Label" Text="Ap. Materno:"></asp:Label>
                                                             <asp:TextBox ID="txtApMaterno" runat="server" CssClass="TextBoxReadOnly" 
                                                                 ReadOnly="True" Width="100px"></asp:TextBox>
                                                             <asp:Label ID="Label40" runat="server" CssClass="Label" Text="Nombre:"></asp:Label>
                                                             <asp:TextBox ID="txtNombres" runat="server" CssClass="TextBoxReadOnly" 
                                                                 ReadOnly="True" Width="130px"></asp:TextBox>
                                                             <asp:TextBox ID="txtCodigoCliente" runat="server" CssClass="TextBoxReadOnly" 
                                                                 ReadOnly="True" Width="50px"></asp:TextBox>
                                                             <asp:TextBox ID="txtIdCliente_BuscarxId" runat="server" 
                                                                 onFocus="return( aceptarFoco(this)  );" 
                                                                 onKeyPress="return(  valKeyPressBuscarPersonaxId()  );" TabIndex="16" 
                                                                 Width="50px"></asp:TextBox>
                                                             <asp:ImageButton ID="btnBuscarClientexIdCliente" runat="server" 
                                                                 ImageUrl="~/Imagenes/Busqueda_b.JPG" 
                                                                 OnClientClick="return(valBuscarClientexId());" 
                                                                 onmouseout="this.src='/Imagenes/Busqueda_b.JPG';" 
                                                                 onmouseover="this.src='/Imagenes/Busqueda_A.JPG';" style="width: 27px" 
                                                                 TabIndex="17" />
                                                         </td>
                                                     </tr>
                                                     <tr>
                                                         <td style="text-align: right">
                                                             <asp:Label ID="Label3" runat="server" CssClass="Label" Text="Dni"></asp:Label>
                                                         </td>
                                                         <td>
                                                             <asp:TextBox ID="txtDni" runat="server" CssClass="TextBoxReadOnly" 
                                                                 MaxLength="8" ReadOnly="True" style="margin-left: 0px; margin-bottom: 0px" 
                                                                 Width="70px"></asp:TextBox>
                                                             <asp:Label ID="Label4" runat="server" CssClass="Label" Text="Ruc"></asp:Label>
                                                             <asp:TextBox ID="txtRuc" runat="server" CssClass="TextBoxReadOnly" 
                                                                 MaxLength="11" ReadOnly="True" Width="90px"></asp:TextBox>
                                                             <asp:Label ID="Label5" runat="server" CssClass="Label" Text="Tel�fono"></asp:Label>
                                                             <asp:TextBox ID="txtTelefono" runat="server" CssClass="TextBoxReadOnly" 
                                                                 MaxLength="20" ReadOnly="True" Width="143px"></asp:TextBox>
                                                             <asp:Label ID="Label12021" runat="server" CssClass="Label" 
                                                                 Text="Precio de Venta"></asp:Label>
                                                             <asp:DropDownList ID="cboTipoPrecioV" runat="server" AutoPostBack="true" 
                                                                 Enabled="true" Font-Bold="True" Width="123px">
                                                             </asp:DropDownList>
                                                         </td>
                                                     </tr>
                                                     <tr>
                                                         <td style="text-align: right;height: 25px;">
                                                             <asp:Label ID="Label2" runat="server" CssClass="Label" Text="Direcci�n"></asp:Label>
                                                         </td>
                                                         <td style="height: 25px">
                                                             <asp:TextBox ID="txtDireccionCliente" runat="server" CssClass="TextBoxReadOnly" 
                                                                 MaxLength="50" ReadOnly="True" Width="661px"></asp:TextBox>
                                                         </td>
                                                     </tr>
                                                     <tr>
                                                         <td style="text-align: right">
                                                             <asp:Label ID="Label12022" runat="server" CssClass="Label" Text="Tipo Agente"></asp:Label>
                                                         </td>
                                                         <td>
                                                             <asp:DropDownList ID="cboTipoAgente" runat="server" Enabled="False" 
                                                                 Font-Bold="True" Width="168px">
                                                             </asp:DropDownList>
                                                             <asp:Label ID="Label12025" runat="server" CssClass="Label" 
                                                                 Text="Tasa del Agente %"></asp:Label>
                                                             <asp:TextBox ID="txtTasaAgente" runat="server" CssClass="TextBox_ReadOnly" 
                                                                 MaxLength="11" ReadOnly="True" Width="65px"></asp:TextBox>
                                                         </td>
                                                     </tr>
                                                     <tr>
                                                         <td class="TituloCeldaLeft" colspan="2" style="text-align: left; ">
                                                             <asp:Image ID="ImgLineaCredito" runat="server" Height="16px" Width="16px" />
                                                             <asp:Label ID="lblLineaCredito" runat="server" CssClass="LabelBlanco" 
                                                                 Text="L�nea de Cr�dito"></asp:Label>
                                                         </td>
                                                     </tr>
                                                     <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender4" runat="server" 
                                                         CollapseControlID="ImgLineaCredito" Collapsed="false" 
                                                         CollapsedImage="~/Imagenes/Mas_B.JPG" CollapsedSize="0" 
                                                         CollapsedText="Linea de Cr�dito" ExpandControlID="ImgLineaCredito" 
                                                         ExpandDirection="Vertical" ExpandedImage="~/Imagenes/Menos_B.JPG" 
                                                         ExpandedSize="70" ExpandedText="Linea de Cr�dito" 
                                                         ImageControlID="ImgLineaCredito" SuppressPostBack="True" 
                                                         TargetControlID="PnlLineaCredito" TextLabelID="lblLineaCredito" />
                                                     <tr>
                                                         <td colspan="2" style="text-align: left; ">
                                                             <asp:Panel ID="PnlLineaCredito" runat="server">
                                                                 <table cellspacing="0" style="width: 100%">
                                                                     <tr>
                                                                         <td>
                                                                             <asp:GridView ID="gvLineaCredito" runat="server" AutoGenerateColumns="False" 
                                                                                 CellPadding="4" ForeColor="#333333" GridLines="None" Height="16px">
                                                                                 <RowStyle CssClass="GrillaRow" />
                                                                                 <HeaderStyle CssClass="GrillaHeader" />
                                                                                 <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                                 <Columns>
                                                                                     <asp:BoundField DataField="IdCuentaPersona" HeaderStyle-Width="150px" 
                                                                                         HeaderText="N� de Cuenta" />
                                                                                     <asp:BoundField DataField="MonedaSimbolo" HeaderStyle-Width="60px" 
                                                                                         HeaderText="Moneda" />
                                                                                     <asp:BoundField DataField="CargoMaximo" DataFormatString="{0:F2}" 
                                                                                         HeaderStyle-Width="100px" HeaderText="Otorgado" />
                                                                                     <asp:BoundField DataField="Disponible" DataFormatString="{0:F2}" 
                                                                                         HeaderStyle-Width="100px" HeaderText="Disponible" />
                                                                                     <asp:BoundField DataField="IdMoneda" HeaderText="" />
                                                                                 </Columns>
                                                                                 <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                             </asp:GridView>
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                             </asp:Panel>
                                                         </td>
                                                     </tr>
                                                 </table>
                                             </asp:Panel>
                                         </td>
                                     </tr>
                                 </table>
                             </ContentTemplate>
                             <Triggers>
                                 <asp:AsyncPostBackTrigger ControlID="DGV_BuscarPersona" 
                                     EventName="SelectedIndexChanged" />
                             </Triggers>
                         </asp:UpdatePanel>
                     </td>
                     
                     </tr>
              
                     <tr class="TituloCeldaLeft">
                         <td   >
                              <asp:Image ID="imgCancelacion" runat="server" Width="16px" />
                              <asp:Label ID="lblCancelacion" runat="server" Text="Datos de Cancelaci�n"></asp:Label>
                                                           
                         </td>
                     </tr>
                     <tr>
                         <td>
                             <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server" 
                                 CollapseControlID="ImgCancelacion" Collapsed="false" 
                                 CollapsedImage="~/Imagenes/Mas_B.JPG" CollapsedSize="0" 
                                 CollapsedText="Datos de Cancelaci�n" ExpandControlID="ImgCancelacion" 
                                 ExpandDirection="Vertical" ExpandedImage="~/Imagenes/Menos_B.JPG" 
                                 ExpandedSize="600" ExpandedText="Datos de Cancelaci�n" 
                                 ImageControlID="ImgCancelacion" SuppressPostBack="True" 
                                 TargetControlID="pnlCancelacion" TextLabelID="lblCancelacion" />
                             <asp:Panel ID="pnlCancelacion" runat="server">
                                 <asp:UpdatePanel ID="UpdatePanel_Cancelacion" runat="server" 
                                     UpdateMode="Conditional">
                                     <ContentTemplate>
                                         <table cellspacing="0">
                                             <tr>
                                                 <td style="text-align: right;">
                                                     <asp:Label ID="lblMonedaCancelacion0" runat="server" CssClass="Label" 
                                                         Text="Estado"></asp:Label>
                                                 </td>
                                                 <td>
                                                     <asp:DropDownList ID="cboEstadoCancelacion" runat="server" CssClass="ComboRojo" 
                                                         Enabled="False" Width="129px">
                                                     </asp:DropDownList>
                                                     <asp:Label ID="Label44" runat="server" CssClass="Label" 
                                                         Text="Condici�n de Pago"></asp:Label>
                                                     <asp:DropDownList ID="cboCondicionPago" runat="server" AutoPostBack="true" 
                                                         Enabled="true" onClick="return(valCreditoDisponible());">
                                                     </asp:DropDownList>
                                                     <asp:Label ID="lblMedioPago" runat="server" CssClass="Label" 
                                                         Text="Medio de Pago"></asp:Label>
                                                     <asp:DropDownList ID="cboMedioPago" runat="server" AutoPostBack="True">
                                                     <asp:ListItem Value="1" Selected="True">Efectivo</asp:ListItem>                                                     
                                                     <asp:ListItem Value="2" >Nota de Cr�dito</asp:ListItem>                                                     
                                                     <asp:ListItem Value="3" >Transferencia</asp:ListItem>
                                                     <asp:ListItem Value="7" >Cheque</asp:ListItem>
                                                     <asp:ListItem Value="8" >Dep�sito</asp:ListItem>
                                                     <asp:ListItem Value="9" >Comp. de Retenci�n</asp:ListItem>
                                                     </asp:DropDownList>
                                                     <asp:Label ID="lblFechaVencimiento" runat="server" CssClass="Label" 
                                                         Text="Fecha de Vencimiento"></asp:Label>
                                                     <asp:TextBox ID="txtFechaVencimiento" runat="server" CssClass="TextBox_Fecha" 
                                                         Font-Bold="True" onblur="return(      valFecha_Blank(this)          );" Width="88px"></asp:TextBox>
                                                     <cc1:CalendarExtender ID="txtFechaVencimiento_CalendarExtender" runat="server" 
                                                         Format="dd/MM/yyyy" TargetControlID="txtFechaVencimiento">
                                                     </cc1:CalendarExtender>
                                                     <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" 
                                                         ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" 
                                                         CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                                         CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                                         CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                                         Mask="99/99/9999" TargetControlID="txtFechaVencimiento">
                                                     </cc1:MaskedEditExtender>                                                 
                                                 </td>
                                             </tr>
                                             <tr>
                                                 <td style="text-align: right;">
                                                     <asp:Label ID="lblMonedaCancelacion" runat="server" CssClass="Label" 
                                                         Text="Total a Pagar:"></asp:Label>
                                                 </td>
                                                 <td>
                                                     <asp:Label ID="lblMonedaCancelacion_TotalAPagar" runat="server" 
                                                         CssClass="LabelRojo" Text="S/."></asp:Label>
                                                     <asp:TextBox ID="txtMonto" runat="server" CssClass="TextBox_ReadOnly" 
                                                         onFocus="return( lostFocus() );" onKeypress="return( false );" Width="96px">0</asp:TextBox>
                                                 </td>
                                             </tr>
                                             <tr>
                                                 <td align="center" class="SubTituloCelda" colspan="2">
                                                     Cancelaci�n
                                                 </td>
                                             </tr>
                                             <tr>
                                                 <td colspan="2">
                                                     <table cellspacing="0" style="width: 100%">
                                                         <tr>
                                                             <td style="text-align: right; width: 90px">
                                                                 <asp:Label ID="Label32" runat="server" CssClass="Label" Text="Moneda:"></asp:Label>
                                                             </td>
                                                             <td style="text-align: left">
                                                                 <asp:DropDownList ID="cmbMonedaCancelacion_Efectivo" runat="server">
                                                                 </asp:DropDownList>
                                                                 <asp:Label ID="lblMonedaCancelacion8" runat="server" CssClass="Label" 
                                                                     Text="Recibido:"></asp:Label>
                                                                 <asp:TextBox ID="txtEfectivo" runat="server" MaxLength="8" 
                                                                     onfocus="return(aceptarFoco(this));" 
                                                                     onKeypress="return(onKeyPressValidarDecimal('1'));" 
                                                                     style="margin-left: 0px; margin-bottom: 0px" Width="111px">0</asp:TextBox>
                                                                 <asp:Button ID="btnViewCapaNotaCredito" Width="120px"  Visible="false"  OnClientClick="return(   valOnClick_btnAddNotaCredito()   );"    runat="server" Text="Nota de Cr�dito" />
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="text-align: right;">
                                                                 <asp:Label ID="lblBanco" runat="server" CssClass="Label" Text="Banco" 
                                                                     Visible="False"></asp:Label>
                                                             </td>
                                                             <td>
                                                                 <asp:DropDownList ID="cboBanco" runat="server" AutoPostBack="True" 
                                                                     style="margin-left: 0px" Visible="False">
                                                                 </asp:DropDownList>
                                                                 <asp:Label ID="lblCheque" runat="server" CssClass="Label" Text="N� de Cheque" 
                                                                     Visible="False"></asp:Label>
                                                                 <asp:TextBox ID="txtNCheque" runat="server" 
                                                                     onFocus="return( aceptarFoco(this) );" 
                                                                     onKeypress="return( valKeyPressAddMedioPago() );" Visible="False" Width="200px"></asp:TextBox>
                                                                 <asp:Label ID="lblFechaCobro" runat="server" CssClass="Label" 
                                                                     Text="Fecha A Cobrar" Visible="False"></asp:Label>
                                                                 <asp:TextBox ID="txtFechaACobrar" runat="server" CssClass="TextBox_Fecha" 
                                                                     Font-Bold="True" onblur="return(    valFecha_Blank(this) );" Visible="False" 
                                                                     Width="100px"></asp:TextBox>                                                                 
                                                                 <cc1:CalendarExtender ID="txtFechaACobrar_CalendarExtender" runat="server" 
                                                                     Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFechaACobrar">
                                                                 </cc1:CalendarExtender>
                                                                 <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" 
                                                                     ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" 
                                                                     CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                                                     CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                                                     CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                                                     Mask="99/99/9999" TargetControlID="txtFechaACobrar">
                                                                 </cc1:MaskedEditExtender>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="text-align: right;">
                                                                 <asp:Label ID="lblNCuenta" runat="server" CssClass="Label" Text="N� de Cuenta" 
                                                                     Visible="False"></asp:Label>
                                                             </td>
                                                             <td>
                                                                 <asp:DropDownList ID="cboNCuenta" runat="server" Visible="False">
                                                                 </asp:DropDownList>
                                                                 <asp:Label ID="lblNOperacion" runat="server" CssClass="Label" 
                                                                     Text="N� de Operaci�n" Visible="False"></asp:Label>
                                                                 <asp:TextBox ID="txtNOperacion" runat="server" 
                                                                     onFocus="return( aceptarFoco(this) );" 
                                                                     onKeypress="return( valKeyPressAddMedioPago() );" Visible="False" Width="153px"></asp:TextBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="text-align: right;">
                                                                 <asp:Label ID="lblCaja" runat="server" CssClass="Label" Text="Caja"></asp:Label>
                                                             </td>
                                                             <td>
                                                                 <asp:DropDownList ID="cboCaja" runat="server" Enabled="False" Height="22px" 
                                                                     Width="157px">
                                                                 </asp:DropDownList>
                                                                 <asp:ImageButton ID="btnAdd_MedioCancelacion" runat="server" 
                                                                     ImageUrl="~/Imagenes/Agregar_B.JPG" 
                                                                     OnClientClick="return(validarAddMedioPago());" 
                                                                     onmouseout="this.src='/Imagenes/Agregar_B.JPG';" 
                                                                     onmouseover="this.src='/Imagenes/Agregar_A.JPG';" />
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="text-align: right;">
                                                                 &nbsp;</td>
                                                             <td>
                                                                 <asp:GridView ID="DGV_Cancelacion" runat="server" AutoGenerateColumns="false">
                                                                     <Columns>
                                                                         <asp:CommandField ButtonType="Link" SelectText="Quitar" 
                                                                             ShowSelectButton="true" />
                                                                         <asp:BoundField DataField="NomMonedaDestino" HeaderText="Moneda" />
                                                                         <asp:BoundField DataField="MontoEquivalenteDestino" DataFormatString="{0:F2}" 
                                                                             HeaderStyle-Width="90px" HeaderText="Monto" />
                                                                         <asp:BoundField DataField="getDescripcionxMedioPago" HeaderStyle-Width="450px" 
                                                                             HeaderText="Descripci�n" />
                                                                         <asp:BoundField DataField="IdMedioPago" HeaderText="" />
                                                                         <asp:BoundField DataField="IdMoneda" HeaderText="" />
                                                                     </Columns>
                                                                     <HeaderStyle CssClass="GrillaHeader" />
                                                                     <FooterStyle CssClass="GrillaFooter" />
                                                                     <PagerStyle CssClass="GrillaPager" />
                                                                     <RowStyle CssClass="GrillaRow" />
                                                                     <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                     <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                     <EditRowStyle CssClass="GrillaEditRow" />
                                                                 </asp:GridView>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td align="right">
                                                                 <asp:Label ID="Label36" runat="server" CssClass="Label" Text="Total Recibido:"></asp:Label>
                                                             </td>
                                                             <td>
                                                                 <asp:Label ID="lblMonto_Acumulado" runat="server" CssClass="LabelRojo" 
                                                                     Text="Label"></asp:Label>
                                                                 <asp:TextBox ID="txtMonto_Acumulado" runat="server" CssClass="TextBox_ReadOnly" 
                                                                     onFocus="return( lostFocus() );" onKeypress="return( false );" Width="120px"></asp:TextBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td align="right">
                                                                 <asp:Label ID="Label38" runat="server" CssClass="Label" Text="Faltante:"></asp:Label>
                                                             </td>
                                                             <td>
                                                                 <asp:Label ID="lblMonto_Saldo" runat="server" CssClass="LabelRojo" Text="Label"></asp:Label>
                                                                 <asp:TextBox ID="txtMonto_Saldo" runat="server" CssClass="TextBox_ReadOnly" 
                                                                     onFocus="return( lostFocus() );" onKeypress="return( false );" Width="120px"></asp:TextBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="text-align: right;">
                                                                 <asp:Label ID="Label12029" runat="server" CssClass="Label" Text="Vuelto:"></asp:Label>
                                                             </td>
                                                             <td>
                                                                 <asp:Label ID="lblMonto_VueltoTotal" runat="server" CssClass="LabelRojo" 
                                                                     Text="Label"></asp:Label>
                                                                 <asp:TextBox ID="txtMonto_VueltoTotal" runat="server" 
                                                                     CssClass="TextBox_ReadOnly" onFocus="return( lostFocus() );" 
                                                                     onKeypress="return( false );" Width="120px"></asp:TextBox>
                                                             </td>
                                                         </tr>
                                                     </table>
                                                 </td>
                                             </tr>
                                             <!--       <tr>
                                          <td align="center" class="SubTituloCelda" colspan="2">
                                              Vuelto
                                          </td>
                                      </tr>
                                      <tr>
                                          <td align="right" class="Label">
                                              Moneda:
                                          </td>
                                          <td>
                                              <asp:DropDownList ID="cmbMoneda_Vuelto" runat="server">
                                              </asp:DropDownList>
                                              <asp:Label ID="Label39" runat="server" CssClass="Label" Text="Efectivo:"></asp:Label>
                                              <asp:TextBox ID="txtMonto_VueltoEfectivo" runat="server" 
                                                  onfocus="return(aceptarFoco(this));"></asp:TextBox>
                                              <asp:ImageButton ID="btnAddVuelto" runat="server" 
                                                  ImageUrl="~/Imagenes/Agregar_B.JPG" OnClientClick="return(validarAddVuelto());" 
                                                  onmouseout="this.src='/Imagenes/Agregar_B.JPG';" 
                                                  onmouseover="this.src='/Imagenes/Agregar_A.JPG';" />
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                          </td>
                                          <td>
                                              <asp:GridView ID="DGV_Vuelto" runat="server" AutoGenerateColumns="false">
                                                  <Columns>
                                                      <asp:CommandField ButtonType="Link" SelectText="Quitar" 
                                                          ShowSelectButton="true" />
                                                      <asp:BoundField DataField="NomMonedaDestino" HeaderText="Moneda" />
                                                      <asp:BoundField DataField="MontoEquivalenteDestino" DataFormatString="{0:F2}" 
                                                          HeaderStyle-Width="90px" HeaderText="Monto" />
                                                      <asp:BoundField DataField="getDescripcionxMedioPago" HeaderStyle-Width="450px" 
                                                          HeaderText="Descripci�n" />
                                                      <asp:BoundField DataField="IdMedioPago" HeaderText="" />
                                                      <asp:BoundField DataField="IdMoneda" HeaderText="" />
                                                  </Columns>
                                                  <HeaderStyle CssClass="GrillaHeader" />
                                                  <FooterStyle CssClass="GrillaFooter" />
                                                  <PagerStyle CssClass="GrillaPager" />
                                                  <RowStyle CssClass="GrillaRow" />
                                                  <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                  <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                  <EditRowStyle CssClass="GrillaEditRow" />
                                              </asp:GridView>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td align="right" class="Label">
                                              Vuelto x Entregar:
                                          </td>
                                          <td>
                                              <asp:Label ID="lblMonedaVuelto" runat="server" CssClass="LabelRojo" 
                                                  Text="Label"></asp:Label>
                                              <asp:TextBox ID="txtVueltoTotal" runat="server" CssClass="TextBox_ReadOnly" 
                                                  Enabled="false" Width="120px"></asp:TextBox>
                                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                          </td>
                                      </tr>
                                      <tr>
                                          <td align="right" class="Label">
                                            Faltante Vuelto:
                                          </td>
                                          <td>
                                              <asp:Label ID="lblMontoFaltante_Vuelto" runat="server" CssClass="LabelRojo" 
                                                  Text="Label" ></asp:Label>
                                              <asp:TextBox ID="txtMontoFaltante_Vuelto"  runat="server" 
                                                  CssClass="TextBox_ReadOnly" Enabled="false" Width="120px"></asp:TextBox>
                                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                          </td>
                                      </tr>-->
                                         </table>
                                     </ContentTemplate>
                                     <Triggers>
                                         <asp:AsyncPostBackTrigger ControlID="CboTienda" 
                                             EventName="SelectedIndexChanged" />
                                         <asp:AsyncPostBackTrigger ControlID="cboPropietario" 
                                             EventName="SelectedIndexChanged" />
                                         <asp:AsyncPostBackTrigger   ControlID="GV_DocumentosReferencia_Find"   EventName="SelectedIndexChanged"  />
                                     </Triggers>
                                 </asp:UpdatePanel>
                             </asp:Panel>
                         </td>
                     </tr>
                     </table>
             </td>
         </tr>        
      
            <tr>
                <td class="TituloCeldaLeft">
                    <asp:Image ID="Image5" runat="server" Width="16px" />
                    <asp:Label ID="lblEntrega" runat="server" CssClass="LabelBlanco" 
                        Text="Datos de Entrega"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" 
                        CollapseControlID="Image5" Collapsed="true" 
                        CollapsedImage="~/Imagenes/Mas_B.JPG" CollapsedSize="0" 
                        CollapsedText="Datos de Entrega" ExpandControlID="Image5" 
                        ExpandDirection="Vertical" ExpandedImage="~/Imagenes/Menos_B.JPG" 
                        ExpandedText="Datos de Entrega" ImageControlID="Image5" SuppressPostBack="True" 
                        TargetControlID="PanelEntrega" TextLabelID="lblEntrega" />
                 
                    <asp:Panel ID="PanelEntrega" runat="server" Width="100%">
                        <asp:UpdatePanel ID="UpdatePanel_LugarEntrega" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                        
                        <table border="0" cellspacing="0">
                            <tr>
                                <td style="text-align: right;">
                                    <asp:Label ID="Label12019" runat="server" CssClass="Label" Text="Estado"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:DropDownList ID="cboEstadoEntrega" runat="server" CssClass="ComboRojo" 
                                        Enabled="False" Width="129px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right;">
                                    <asp:Label ID="Label65" runat="server" CssClass="Label" Text="Depto.:"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:DropDownList ID="cboDepartamento" runat="server" AutoPostBack="True" 
                                        Width="150px">
                                    </asp:DropDownList>
                                    <asp:Label ID="Label66" runat="server" CssClass="Label" Text="Provincia"></asp:Label>
                                    <asp:DropDownList ID="cboProvincia" runat="server" AutoPostBack="True" 
                                        Width="250px">
                                    </asp:DropDownList>
                                    <asp:Label ID="Label67" runat="server" CssClass="Label" Text="Distrito"></asp:Label>
                                    <asp:DropDownList ID="cboDistrito" runat="server" Width="250px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right;">
                                    <asp:Label ID="Label68" runat="server" CssClass="Label" Text="Direcci�n"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtDireccionEntrega" runat="server" Width="608px"></asp:TextBox>
                                    <asp:Label ID="Label69" runat="server" CssClass="Label" Text="Fecha a Entregar"></asp:Label>
                                    <asp:TextBox ID="txtFechaAEntregar"   onblur="return(     valFecha_Blank(this)  );"  runat="server" CssClass="TextBox_Fecha" Width="100px"></asp:TextBox>
                                    <cc1:CalendarExtender ID="txtFechaAEntregar_CalendarExtender" runat="server" 
                                        Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFechaAEntregar">
                                    </cc1:CalendarExtender>
                                    
                                      <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" 
                                 CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" 
                                 CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                 CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True"                                  
                                 TargetControlID="txtFechaAEntregar"
                               ClearMaskOnLostFocus="false"    
                                  Mask="99/99/9999"
                                 
                                 >
                             </cc1:MaskedEditExtender>
                                    
                                    
                                    
                                </td>
                            </tr>
                        </table>
                        
                        </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                    
                </td>
            </tr>
      
         </table>  
         
         </asp:Panel> 
             </td>
             </tr>
     
     <tr>
     
     <td>
     
       <asp:UpdatePanel ID="UpdatePanel_Hidden" runat="server" UpdateMode="Always">
            <ContentTemplate>
        
        <table>
           <tr>
             <td>
            
                 
                 <asp:HiddenField ID="hddIGV" runat="server"  Value="0" />
                 <asp:HiddenField ID="hddIdCuentaPersona" runat="server" />
                 <asp:HiddenField ID="hddFechaEmision" runat="server" />
                 <asp:HiddenField ID="hddIdDocumento" runat="server" />
                 <asp:HiddenField ID="hddIdDocumentoPrint" runat="server" />
                 <asp:HiddenField ID="hddIndexDetalle_Area" runat="server" />
                 <asp:HiddenField ID="hddIdAlmacen" runat="server" />
                 <asp:HiddenField ID="hddNomAlmacen" runat="server" />                 
                 <asp:HiddenField ID="hddMontoMaximoAfecto" runat="server" Value="0"  />
                 <asp:HiddenField ID="hddMontoMaximoNOAfecto" runat="server" Value="0" />
                 <asp:HiddenField ID="hddModoFrm" runat="server" Value="0" />
                 
                 <asp:HiddenField ID="hddPoseeOrdenDespacho" runat="server" Value="0" />
                 <asp:HiddenField ID="hddPoseeAmortizaciones" runat="server" Value="0" />                 
                 <asp:HiddenField ID="hddPoseeCompPercepcion" runat="server" Value="0" />
                 <asp:HiddenField ID="hddValAddDatoCancelacion" runat="server" Value="0" />
              
                 
             </td>
         </tr>
        
        </table>
            
            </ContentTemplate>
            </asp:UpdatePanel>
     </td>
     </tr>
     
     
   </table>        
 

<div  id="capaDocumentosReferencia" 
            style="border: 3px solid blue; padding: 8px; width:900px; height:auto; position:absolute; background-color:white; z-index:2; display :none; top: 738px; left: 25px;">
    <asp:UpdatePanel ID="UpdatePanel_NotaCredito" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
            <table style="width: 100%;">
                                <tr><td align="right">
                                       <asp:ImageButton ID="btnCerrar_capaDocumentosReferencia" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaDocumentosReferencia')   );" />   
                                </td>
                                </tr>                                                                                               
                                <tr>
                                <td>
                                    <asp:Panel ID="Panel_DocRef_Find" runat="server" ScrollBars="Horizontal" Width="100%">                                    
                                <asp:GridView ID="GV_DocumentosReferencia_Find" runat="server" AutoGenerateColumns="false" Width="100%"  >          
                                <Columns>
                                <asp:CommandField  ShowSelectButton="true" SelectText="Seleccionar" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px"  />                
                                <asp:TemplateField   HeaderText="Monto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"    >
                                <ItemTemplate>
                                <table>
                                <tr>
                                <td><asp:Label ID="lblMonedaMontoRecibir_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'  ></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="txtMontoRecibir_Find" Width="70px" Font-Bold="true" onKeypress="return(  validarNumeroPunto()  );"  onblur="return(  valBlur()   );"  onFocus="return(   aceptarFoco(this)   );"   runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ImporteTotal","{0:F2}")%>' ></asp:TextBox></td>
                                </tr>
                                </table>
                                
                                </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField  DataField="NomTipoDocumento"  ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"  HeaderText="Tipo"   />                                
                <asp:TemplateField   HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"   >
                <ItemTemplate>
                    <table>
                    <tr>
                    <td>
                        <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Numero")%>'  ></asp:Label></td>
                    <td>
                        <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                    </td>
                    <td>
                        <asp:HiddenField ID="hddIdMonedaDocRef_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                    </td>
                    <td>                    
                    <asp:HiddenField ID="hddIdEstadoCancelacion_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEstadoCancelacion")%>' />
                    </td>
                    </tr>
                    </table>
                </ItemTemplate>                
                </asp:TemplateField>                
                <asp:BoundField  DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisi�n"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField  DataField="FechaVenc" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Vcto."  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />                
                <asp:BoundField  DataField="Contador" DataFormatString="{0:F0}" HeaderText="Vigencia (D�as)"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"   />
                <asp:TemplateField HeaderText="Monto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
                <ItemTemplate>
                    <table>
                    <tr>
                    <td><asp:Label ID="lblMonedaMonto_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'  ></asp:Label></td>
                    <td><asp:Label ID="lblMonto_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F2}")%>' ></asp:Label></td>
                    </tr>
                    </table>
                </ItemTemplate>
                </asp:TemplateField>                
                <asp:TemplateField HeaderText="Saldo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
                <ItemTemplate>
                    <table>
                    <tr>
                    <td><asp:Label ID="lblMonedaSaldo_Find" ForeColor="Red" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'  ></asp:Label></td>
                    <td><asp:Label ID="lblSaldo_Find" runat="server" ForeColor="Red" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"ImporteTotal","{0:F2}")%>' ></asp:Label></td>
                    </tr>
                    </table>
                </ItemTemplate>
                </asp:TemplateField>                
                <asp:BoundField  DataField="NomPropietario"  HeaderText="Empresa"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField  DataField="NomTienda"  HeaderText="Tienda"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />                                
                <asp:BoundField  DataField="NomEstadoCancelacion"  ControlStyle-ForeColor="Red" ControlStyle-Font-Bold="true" HeaderText="Uso"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField  DataField="NomEstadoDocumento"  HeaderText="Estado"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                
                </Columns>
                     <HeaderStyle CssClass="GrillaHeader" />                            
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                </asp:GridView>
                                
                                    </asp:Panel>
                                </td>
                                </tr>                                             
                 </table>        
    </ContentTemplate>
    <Triggers>
    <asp:AsyncPostBackTrigger ControlID="btnViewCapaNotaCredito" EventName="Click" />
    </Triggers>
    </asp:UpdatePanel>


                                                  
        </div> 
                       
<div  id="capaRegistrarCliente"         
        style="border: 3px solid blue; padding: 10px; width:auto; height:auto; position:absolute; top:302px; left:23px; background-color:white; z-index:3; display :none; ">                        
                  <table cellspacing="0" style="width: 750px; height: 215px;">
                    <tr>
                        <td style="width: 86px; text-align: right;" >
                                <asp:Label ID="Label7" runat="server" Text="Tipo Persona:" CssClass="Label"></asp:Label>                                
                                </td>
                                <td colspan="3">
                                <asp:DropDownList onchange="return(activarPanel());" ID="cmbTipoPersona" runat="server">
                                <asp:ListItem Value="N" Selected="True">Natural</asp:ListItem>
                                <asp:ListItem Value="J">Jur�dica</asp:ListItem>
                                </asp:DropDownList>
                            <asp:Label ID="Label6" CssClass="Label" runat="server" Text="Tipo Agente:"></asp:Label>
                            <asp:DropDownList ID="cmbTipoAgente_RegistrarCliente" runat="server"  onClick=" return(   valOnClickTipoAgente_RegistrarCliente()   ); "     >
                            </asp:DropDownList>   
                                </td>                                
                    </tr>
                    <tr>
                        <td style="width: 86px; text-align: right;">
                                    <asp:Label ID="Label9" runat="server" Text="Raz�n Social:" CssClass="Label"></asp:Label>
                                </td>
                        <td colspan="3">
                                    <asp:TextBox ID="txtRazonSocial_Cliente" Width="653px" runat="server" MaxLength="100" 
                                   Enabled="false" CssClass="TextboxFlat" onKeypress="return( valKeyPressSaveCliente()  );" onFocus="return( aceptarFoco(this)  );"  ></asp:TextBox>
                                </td>
                    </tr>
                    <tr>
                        <td style="width: 86px; text-align: right;">
                                    <asp:Label ID="Label30" runat="server" Text="Ap. Paterno:" CssClass="Label"></asp:Label>
                                    </td>
                        <td style="width: 286px">
                                    <asp:TextBox ID="txtApPaterno_Cliente" runat="server" 
                                        Width="282px" MaxLength="25" CssClass="TextboxFlat"  onKeypress="return( valKeyPressSaveCliente()  );"  onFocus="return( aceptarFoco(this)  );"  ></asp:TextBox>
                                    </td>
                        <td style="width: 82px; text-align: right;">
                                    <asp:Label ID="Label31" runat="server" Text="Ap. Materno:" CssClass="Label"></asp:Label>
                                    </td>
                        <td>
                                    <asp:TextBox ID="txtApMaterno_Cliente" runat="server"  
                                        Width="282px" MaxLength="25" CssClass="TextboxFlat"  onKeypress="return( valKeyPressSaveCliente()  );"  onFocus="return( aceptarFoco(this)  );"   ></asp:TextBox>
                                    </td>
                    </tr>
                    <tr>
                        <td style="width: 86px; text-align: right;">
                                    <asp:Label ID="label12015" runat="server" Text="Nombres:" CssClass="Label"></asp:Label>
                                    </td>
                        <td style="width: 286px">
                            <asp:TextBox ID="txtNombres_Cliente" runat="server"  Width="282px" 
                                MaxLength="25" CssClass="TextboxFlat" onKeypress="return( valKeyPressSaveCliente()  );" onFocus="return( aceptarFoco(this)  );"  ></asp:TextBox>
                                    </td>
                        <td style="width: 82px; text-align: right;">
                                    <asp:Label ID="Label8" runat="server" Text="D.N.I.:" CssClass="Label"></asp:Label>
                                    </td>
                        <td>
                                    <asp:TextBox ID="txtDNI_Cliente" runat="server" MaxLength="8" 
                                        CssClass="TextboxFlat"  onKeypress="return( onKeyPressEsNumero()  );" onFocus="return( aceptarFoco(this)  );" ></asp:TextBox>
                                    </td>
                    </tr>
                    <tr>
                        <td style="width: 86px; text-align: right;">
                                    <asp:Label ID="Label34" runat="server" Text="R.U.C.:" CssClass="Label"></asp:Label>
                                    </td>
                        <td style="width: 286px">
                                    <asp:TextBox ID="txtRUC_Cliente"  onKeypress="return( onKeyPressEsNumero()  );"  onFocus="return( aceptarFoco(this)  );"   runat="server" MaxLength="11" 
                                        CssClass="TextboxFlat"></asp:TextBox>
                                    </td>
                        <td style="width: 82px">
                                    &nbsp;</td>
                        <td>
                                    &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 86px; text-align: right;">
                                    <asp:Label ID="Label12018" runat="server" CssClass="Label" Text="Direcci�n"></asp:Label>
                                    </td>
                        <td colspan="3">
                                    <asp:TextBox ID="txtDireccion_Cliente" runat="server"  MaxLength="120" 
                                        Width="653px" CssClass="TextboxFlat"  onKeypress="return( valKeyPressSaveCliente()  );" onFocus="return( aceptarFoco(this)  );" ></asp:TextBox>
                                    </td>
                    </tr>
                    <tr>
                        <td style="width: 86px; text-align: right;">
                                    <asp:Label ID="Label37" runat="server" Text="Tel�fono:" CssClass="Label"></asp:Label>
                                    </td>
                        <td colspan="3">
                                    <asp:TextBox ID="txtTelefono_Cliente" runat="server" 
                                        MaxLength="20" Width="156px" CssClass="TextboxFlat" onKeypress="return( onKeyPressEsNumero()  );"  onFocus="return( aceptarFoco(this)  );"  ></asp:TextBox>
                                    </td>
                    </tr>
                    <tr>
                        <td style="width: 86px; text-align: right;">
                                    &nbsp;</td>
                        <td colspan="3">
                            <asp:ImageButton ID="btnGuardarCliente" runat="server" 
                        ImageUrl="~/Imagenes/Guardar_B.JPG" OnClientClick="return(validarSaveCliente());"
                        onmouseover="this.src='/Imagenes/Guardar_A.JPG';"  
                        onmouseout="this.src='/Imagenes/Guardar_B.JPG';"  CausesValidation="true" /> 
                            
                            
                            <asp:ImageButton ID="ImageButton1" runat="server" 
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  OnClientClick="return(offCapa('capaRegistrarCliente'));"/> 
                                                
                                                
                                                
                                    </td>
                    </tr>
                   </table>
                      
                        </div>  

<div  id="capaAddMagnitud" style="border: 3px solid blue; padding: 8px; width:auto; height:auto; position:absolute; top:300px; left:250px; background-color:white; z-index:2; display :none; ">                          
     <table>                                    
                                <tr><td align="center">
                            <asp:ImageButton ID="ImageButton2" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  
                                        OnClientClick="return(offCapa('capaAddMagnitud'));" 
                                        />                                
                                </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label23" runat="server" Text="Ancho:" CssClass="Label"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox onKeypress="return(validarNumeroPunto());"  ID="txtAncho" Width="90px" runat="server"></asp:TextBox>
                                                    <asp:Label ID="Label12" runat="server" Text="metros" CssClass="Label"></asp:Label>
                                                </td>                                                    
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label10" runat="server" Text="Largo:" CssClass="Label"></asp:Label>
                                                </td>
                                                <td>
                                                <asp:TextBox ID="txtLargo" Width="90px" runat="server" onKeypress="return(validarNumeroPunto());"  ></asp:TextBox>
                                                <asp:Label ID="Label13" runat="server" Text="metros" CssClass="Label"></asp:Label>
                                                </td>
                                            </tr>                                            
                                            <tr>
                                            <td>
                                                <asp:Label ID="Label11" runat="server" Text="Cantidad:" CssClass="Label"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox onKeypress="return(validarNumeroPunto());"  ID="txtCantidadRetazo" Width="90px" runat="server"></asp:TextBox>
                                            </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                <td align="center">
                                <asp:ImageButton ID="btnAceptar_CapaArea" runat="server" 
                                            ImageUrl="~/Imagenes/Aceptar_B.JPG" 
                                            onmouseout="this.src='/Imagenes/Aceptar_B.JPG';" OnClientClick="return(saveIndexFila());"                                            
                                                onmouseover="this.src='/Imagenes/Aceptar_A.JPG';" /></td>
                                </tr>
                                </table>        
                                                                                                            
                        </div>            
<div  id="CapaLongitud" style="border: 3px solid blue; padding: 8px; width:auto; height:auto; position:absolute; top:300px; left:250px; background-color:white; z-index:2; display :none; ">                                          
                  <table>                                    
                                <tr><td align="center">
                            <asp:ImageButton ID="ImageButton3" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  
                                        OnClientClick="return(offCapa('CapaLongitud'));" 
                                        />                                
                                </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label14" runat="server" Text="Largo:" CssClass="Label"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox onKeypress="return(validarNumeroPunto());"  ID="txtLargo_Metros" Width="90px" runat="server"></asp:TextBox>
                                                    <asp:Label ID="Label15" runat="server" Text="metros" CssClass="Label"></asp:Label>
                                                </td>                                                    
                                            </tr>                                           
                                            <tr>
                                            <td>
                                                <asp:Label ID="Label18" runat="server" Text="Cantidad:" CssClass="Label"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox onKeypress="return(validarNumeroPunto());"  ID="txtCantidad_Metros" Width="90px" runat="server"></asp:TextBox>
                                            </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                <td align="center">
                                <asp:ImageButton ID="btnAceptar_RetazoMetros" runat="server" 
                                            ImageUrl="~/Imagenes/Aceptar_B.JPG" 
                                            onmouseout="this.src='/Imagenes/Aceptar_B.JPG';" OnClientClick="return(saveIndexFilaMetro());"
                                                onmouseover="this.src='/Imagenes/Aceptar_A.JPG';" /></td>
                                </tr>
                                </table>                                                                                                     
</div>    
<div  id="capaBuscarProducto_AddProd"         
        style="border: 3px solid blue; padding: 8px; width:900px; height:auto; position:absolute; top:237px; left:38px; background-color:white; z-index:2; display :none;">
                        <asp:UpdatePanel ID="UpdatePanelBuscarP" runat="server">
                        
                        
                            <ContentTemplate>  
                                <table style="width: 100%;">                                    
                                <tr><td align="right">
                            <asp:ImageButton ID="ImageButton4" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG"  TabIndex="200"
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  
                                        OnClientClick="return(offCapa('capaBuscarProducto_AddProd'));" 
                                        />                                
                                </td>
                                </tr>
                                    <tr>
                                        <td>
                                            <table >
                                                <tr>
                                                    <td align="right">
                                                    <asp:Label ID="Label17" runat="server" CssClass="Label" Text="L�nea:"></asp:Label>
                                                    </td>
                                                    <td>
                                                    <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True" DataTextField="Descripcion" DataValueField="Id">
                                                    </asp:DropDownList>
                                                    <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Sub L�nea:"></asp:Label>
                                                    <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" runat="server" DataTextField="Nombre" DataValueField="Id" >
                                                    </asp:DropDownList>                                    
                                                    <asp:Label ID="Label12026" runat="server" Text="C�d.:" CssClass="Label" ></asp:Label>
                                                    <asp:TextBox  TabIndex="203" ID="txtCodigoSubLinea_AddProd" onFocus="return( aceptarFoco(this)  );" runat="server" Width="50px" onKeypress="return(  valKeyPressCodigoSL()        );" ></asp:TextBox>                
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripci�n:"></asp:Label>
                                                    </td>
                                                    <td>
                                                    <asp:TextBox TabIndex="204" ID="txtDescripcionProd_AddProd" runat="server" Width="300px"  onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd() );" ></asp:TextBox>                                                    
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <asp:Label CssClass="Label" ID="Label12027" runat="server" Text="Almac�n:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="cmbAlmacen_AddProd"  TabIndex="205" runat="server" AutoPostBack="true"   >
                                                        </asp:DropDownList>
                                                        <asp:Label CssClass="Label" ID="Label12028" runat="server" Text="Precios de la Tienda:"></asp:Label>
                                                        <asp:DropDownList ID="cmbTienda_AddProd"  TabIndex="206"  runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                        <asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" 
                                                        ImageUrl="~/Imagenes/Buscar_b.JPG" TabIndex="207"
                                                        onmouseout="this.src='/Imagenes/Buscar_b.JPG';" 
                                                        onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                                    <asp:ImageButton ID="btnAddProductos_AddProd" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG" 
                                                        onmouseout="this.src='/Imagenes/Agregar_B.JPG';" onmouseover="this.src='/Imagenes/Agregar_A.JPG';"
                                                        OnClientClick="return(valAddProductos());" TabIndex="208"  />
                                                        <asp:ImageButton ID="btnLimpiar_AddProd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG" 
                                                        onmouseout="this.src='/Imagenes/Limpiar_B.JPG';" onmouseover="this.src='/Imagenes/Limpiar_A.JPG';"
                                                        OnClientClick="return(limpiarBuscarProducto());" TabIndex="209" />
                                                        
                                                        
                                                        
                                                    </td>                                                    
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                <tr>
                                <td>
                                    <asp:GridView ID="DGV_AddProd" runat="server" AllowPaging="True" 
                                        AutoGenerateColumns="False" Width="100%" PageSize="40">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Cantidad">
                                            <ItemTemplate>
                                                
                                                
                                                
                                                <asp:TextBox ID="txtCantidad_AddProd" TabIndex="210" Width="65px"  onKeyup="return(  valStockDisponible_AddProd() );" onblur="return( valBlurClear('0.00') );" onfocus="return(  aceptarFoco(this) );" onKeypress="return( valKeyPressCantidadAddProd()  );" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F2}")%>' runat="server"></asp:TextBox>
                                                
                                                
                                                
                                            </ItemTemplate>                                            
                                            </asp:TemplateField>                                                                                        
                                            <asp:BoundField DataField="IdProducto" HeaderText="C�digo" />
                                            <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" 
                                                NullDisplayText="---" />                                                
                                            <asp:TemplateField HeaderText="U.M.">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="cmbUnidadMedida_AddProd" runat="server"                                               
                                                OnSelectedIndexChanged="cmbUnidadMedidaVenta_Catalogo_SelectedIndexChanged"
                                                AutoPostBack="true" TabIndex="211"
                                                DataTextField="DescripcionCorto" DataValueField="Id" >
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            
                                            <asp:BoundField DataField="NomTipoPV" HeaderText="Tipo P.V." NullDisplayText="---" />
                                            <asp:BoundField DataField="SimbMoneda" ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true" NullDisplayText="---" />
                                            
                                            <asp:BoundField DataField="PrecioSD"  ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true" HeaderText="P. Venta" 
                                                NullDisplayText="0" DataFormatString="{0:F2}" />
                                                
                                            <asp:BoundField DataField="StockDisponible" HeaderText="Stock Disponible" 
                                                NullDisplayText="0"  DataFormatString="{0:F4}" />
                                                
                                            <asp:BoundField DataField="Percepcion" HeaderText="Percepci�n (%)" 
                                            NullDisplayText="0" DataFormatString="{0:F2}" />                                                                                            
                                                
                                        </Columns>
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    </asp:GridView></td>
                                </tr>
                                <tr>
                                <td>
                                <asp:Button  TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true" Width="50px" Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                                        <asp:Button  TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true" Width="50px" Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                                        <asp:TextBox  TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                                        <asp:Button  TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionProductos('2'));" />
                                        <asp:TextBox  TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly" runat="server" onKeyPress="return(onKeyPressEsNumero());"></asp:TextBox>
                                </td>
                                </tr>
                                                                    </table>
                            </ContentTemplate>                            
                        </asp:UpdatePanel>
                        </div>                                               
<div id="capaImpresionDocumento" style="border: 3px solid blue; padding: 8px; width:840px; height:auto; position:absolute; top:225px; left:38px; background-color:white; z-index:2; display :none;">
                        <asp:UpdatePanel ID="UpdatePanel_ImpresionDocumento" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>                            
                                 <table style="width: 100%;">                                    
                                <tr>
                                <td align="right">
                            <asp:ImageButton ID="ImageButton5" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  
                                        OnClientClick="return(offCapa('capaImpresionDocumento'));" 
                                        />                                
                                </td>
                                </tr>  
                                <tr align="left">
                                <td>                                
                                
                                <table>
                                <tr>
                                <td> 
                                    <asp:Button ID="btnImprimir_capa" Width="70px" runat="server" Text="Imprimir"  OnClientClick="return(imprimirDocumento_capa());"  /> </td>
                                <td> 
                                <asp:Button ID="btnDespachar_capa" Width="80px" runat="server" Text="Despachar"   OnClientClick="return(emitirOrdenDespacho_capa());" /> 
                                </td>
                                <td>  
                                
                                <asp:Button ID="lbtnEmitirGuiaRem_capa" Width="115px" runat="server" Text="Emitir Gu�a Rem."   OnClientClick="return(emitirGuiaRemisionRemitente_capa());" /> 
                                
                                </td>
                                
                                <td>  
                                
                                <asp:Button ID="btnGenerarCompPercepcion_Capa" Width="125px" runat="server" Text="Comp. Percepci�n" ToolTip="Generar Comprobante de Percepci�n"   OnClientClick="return( GenerarDocCompPercepcion_capa()   );" /> 
                                
                                </td>
                                
                                <td>
                                <asp:Button ID="btnImprimir_JS" runat="server" Text="Imprimir" Visible="false" Width="80px" OnClientClick="return( imprimirDocumento_JS() );" />
                                </td>
                                </tr>
                                </table>
                                    
                                </td>
                                </tr>                                  
                                <tr>
                                <td> 
                                
                                    <asp:GridView ID="DGV_ImpresionDoc_Cab" runat="server" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="true" HeaderStyle-Width="80px" SelectText="Ver Detalle" ButtonType="Link" />
                                        <asp:BoundField HeaderText="Id" HeaderStyle-Width="75px"  DataField="Id" />
                                       <asp:BoundField HeaderText="Serie" HeaderStyle-Width="80px" DataField="Serie" />
                                       <asp:BoundField HeaderText="C�digo" HeaderStyle-Width="80px" DataField="Codigo" />   
                                        <asp:BoundField HeaderText="Moneda" HeaderStyle-Width="70px" DataField="NomMoneda" />                                          
                                       <asp:BoundField HeaderText="Percepci�n" DataFormatString="{0:F2}" HeaderStyle-Width="110px" DataField="Percepcion" />
                                       <asp:BoundField HeaderText="Total A Pagar" DataFormatString="{0:F2}" HeaderStyle-Width="110px" DataField="TotalAPagar" />                                       
                                        <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                        
                                            <asp:CheckBox ID="chb_ImprimirDoc" runat="server" onClick="return(validarCheckImprimirDoc(this));" />
                                        
                                        </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                      <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />                                    
                                    </asp:GridView>
                                
                                                                                                                             
                                </td>
                                </tr>
                                
                                <tr>
                                <td>
                                
                                 <table>
                                <tr>
                                <td class="Label">Percepci�n Total:</td>
                                <td>
                                    <asp:Label CssClass="LabelRojo" ID="lblPercepcion_DocSAVED" runat="server" Text="S/."></asp:Label>
                                    <asp:TextBox ID="txtPercepcion_DocSAVED" CssClass="TextBox_ReadOnly" Width="100px" runat="server" Enabled="false"></asp:TextBox></td>
                                <td></td>
                                 <td class="Label">Total a Pagar:</td>
                                <td>
                                    <asp:Label CssClass="LabelRojo" ID="lblTotalAPagar_DocSAVED" runat="server" Text="S/."></asp:Label>
                                    <asp:TextBox ID="txtTotalAPagar_DocSAVED" CssClass="TextBox_ReadOnly" Width="100px" runat="server" Enabled="false"></asp:TextBox></td>
                                <td></td>
                                </tr>
                                </table>
                                
                                
                                </td>
                                </tr>
                                
                                <tr>
                                <td> 
                                
                                <asp:GridView ID="DGV_ImpresionDoc_Det" runat="server" AutoGenerateColumns="false">
                                    <Columns>                                        
                                       <asp:BoundField HeaderText="Id" ItemStyle-Height="20px" DataField="IdProducto" HeaderStyle-Width="75px"  />
                                       <asp:BoundField HeaderText="Descripci�n" ItemStyle-Height="20px"  HeaderStyle-Width="300px" DataField="Descripcion" />                                       
                                       <asp:BoundField HeaderText="U.M." ItemStyle-Height="20px" HeaderStyle-Width="65px" DataField="UM" />
                                       <asp:BoundField HeaderText="Cantidad" ItemStyle-Height="20px" HeaderStyle-Width="85px" DataFormatString="{0:F2}" DataField="Cantidad" />                                                                               
                                       <asp:BoundField HeaderText="P. Final" ItemStyle-Height="20px" HeaderStyle-Width="85px" DataFormatString="{0:F2}" DataField="PrecioCD" />
                                       <asp:BoundField HeaderText="Importe" ItemStyle-Height="20px" HeaderStyle-Width="85px" DataFormatString="{0:F2}" DataField="Importe" />                                                                           
                                    </Columns>
                                      <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />                                    
                                    </asp:GridView>
                                
                                                                                                                             
                                </td>
                                </tr>
                            </table>
                            </ContentTemplate>
             </asp:UpdatePanel>
</div>       
<asp:Panel ID="Panel_Recalcular" runat="server">
    
    <div id="capaVistaPreviaDocumento" 
            style="border: 3px solid blue; padding: 8px; width:auto; height:auto; position:absolute; background-color:white; z-index:2; display :none; top: 200px; left: 104px;">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                
<table >                                    
                                <tr>
                                <td align="right">                      
                            <asp:ImageButton ID="btnCerrar_capaVistaPrevia" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" OnClientClick="return(offCapa('capaVistaPreviaDocumento'));"
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  />
                                </td>
                                </tr>                                  
                                <tr>
                                <td> 
                                
                                    <asp:GridView ID="DGV_VistaPreviaDoc" runat="server" AutoGenerateColumns="false">
                                    <Columns>                                        
                                        <asp:BoundField ItemStyle-Height="30px" NullDisplayText="---" HeaderText="Id" HeaderStyle-Width="75px"   />
                                       <asp:BoundField NullDisplayText="---" HeaderText="Serie" HeaderStyle-Width="80px"  />
                                       <asp:BoundField NullDisplayText="---" HeaderText="C�digo" HeaderStyle-Width="80px"  />   
                                        <asp:BoundField NullDisplayText="---" HeaderText="Moneda" HeaderStyle-Width="70px" DataField="NomMoneda" />                                          
                                       <asp:BoundField  HeaderText="Percepci�n" DataFormatString="{0:F2}" HeaderStyle-Width="110px" DataField="Percepcion" />
                                       <asp:BoundField HeaderText="Total A Pagar" DataFormatString="{0:F2}" HeaderStyle-Width="110px" DataField="TotalAPagar" />
                                    </Columns>
                                      <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />                                    
                                    </asp:GridView>
                                
                                                                                                                             
                                </td>
                                </tr>
                                
                                <tr>
                                <td>                                                                                         
                                
                                <table>
                                <tr>
                                <td class="Label">Percepci�n Total:</td>
                                <td>
                                    <asp:Label CssClass="LabelRojo" ID="lbl_Percepcion_VistaPrevia" runat="server" Text="S/."></asp:Label>
                                    <asp:TextBox ID="txt_Percepcion_VistaPrevia" CssClass="TextBox_ReadOnly" Width="100px" runat="server" ReadOnly="true"></asp:TextBox></td>
                                <td></td>
                                 <td class="Label">Total a Pagar:</td>
                                <td>
                                    <asp:Label CssClass="LabelRojo" ID="lbl_TotalAPagar_VistaPrevia" runat="server" Text="S/."></asp:Label>
                                    <asp:TextBox ID="txt_TotalAPagar_VistaPrevia" CssClass="TextBox_ReadOnly" Width="100px" runat="server" ReadOnly="true"></asp:TextBox></td>
                                <td></td>
                                </tr>
                                </table>
                                
                                </td>
                                </tr>
                            </table>                
                </ContentTemplate>
                <Triggers > <asp:AsyncPostBackTrigger ControlID="Button1" EventName="Click" /></Triggers>
                </asp:UpdatePanel>           
</div>      
</asp:Panel>                        
<div id="capaConfiguracionFrm" style="border: 3px solid blue; padding: 8px; width:auto; height:auto; position:absolute; top:220px; left:150px; background-color:white; z-index:2; display :none;">
                                 <table >                                    
                                <tr>
                                <td align="right">
                            <asp:ImageButton ID="ImageButton6" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  
                                        OnClientClick="return(offCapa('capaConfiguracionFrm'));" 
                                        />                                
                                </td>
                                </tr>  
                                <tr align="left">
                                <td>                                
                                
                                <table>
                                    <tr>
                                    <td class="Label" align="right">
                                    Mover Almac�n:
                                    </td>
                                    <td style="width:75px">
                                        <asp:CheckBox ID="chb_MoverAlmacen" Enabled="false" Checked="true" runat="server" />
                                    </td>
                                    <td class="Label" align="right">
                                    Comprometer Stock:
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chb_ComprometerStock" Enabled="false" Checked="true" runat="server" />
                                    </td>
                                    </tr>
                                    <tr>
                                    <td class="Label" align="right">
                                    Separar Afectos / No Afectos:
                                    </td>
                                    <td style="width:75px">
                                        <asp:CheckBox ID="chb_SepararAfecto_NoAfecto" Checked="false" runat="server" />
                                    </td>
                                    <td class="Label" align="right">
                                    Saltar Correlativo:
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chb_SaltarCorrelativo" Checked="false" runat="server" />
                                    </td>
                                    
                                    </tr>
                                    <tr>
                                    <td class="Label" align="right">
                                    Separar x Montos:
                                    </td>
                                    <td style="width:75px">
                                        <asp:CheckBox ID="chb_SepararxMontos" Checked="false" runat="server" />
                                    </td>
                                    <td class="Label" align="right">
                                        Comprometer Percepci�n:
                                    </td>                                    
                                    <td>
                                        <asp:CheckBox ID="chb_CompPercepcion" Enabled="true" onClick="return(   valOnClickChb_CompPercepcion(this)    );" runat="server" Checked="false" />
                                    </td>                                                                                                            
                                    </tr>                                    
                                    <tr>
                                    <td class="Label" align="right">
                                        Despacho Autom�tico:</td>
                                    <td style="width:75px">
                                        <asp:CheckBox ID="chb_GenerarDespacho" Checked="false" runat="server" />
                                    </td>
                                    <td class="Label" align="right">
                                        &nbsp;</td>                                    
                                    <td>
                                        &nbsp;</td>                                                                                                            
                                    </tr>                                    
                                </table>
                                
                                </td>
                                </tr>                                  
                            </table>                            
</div>    
<div  id="capaPersona" 
        style="border: 3px solid blue; padding: 8px; width:900px; height:auto; position:absolute; top:239px; left:27px; background-color:white; z-index:2; display :none; ">
                        <asp:UpdatePanel ID="UpdatePanel_Persona" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table style="width: 100%;">                                    
                                <tr><td align="right">
                                       <asp:ImageButton ID="btnCerrar_Capa" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  
                                        OnClientClick="return(offCapa('capaPersona'));" />   
                                </td>
                                </tr>
                                    <tr class="Label">
                                        <td align="left">
                                            <table>
                                                <tr>
                                                    <td>
                                                        Filtro:</td>
                                                    <td>
                                                        <asp:DropDownList ID="cmbFiltro_BuscarPersona" runat="server">
                                                        <asp:ListItem Selected="True" Value="0">Descripci�n</asp:ListItem>
                                                        <asp:ListItem Value="1">R.U.C.</asp:ListItem>
                                                        <asp:ListItem Value="2">D.N.I.</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        Texto:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtBuscarPersona_Grilla" runat="server" onKeypress="return(  valKeypressBuscarPersona()  );" Width="250px"></asp:TextBox>
                                                        <asp:ImageButton ID="btnBuscarPersona_Grilla" runat="server" 
                                                            CausesValidation="false" ImageUrl="~/Imagenes/Buscar_b.JPG"                                                             
                                                            onmouseout="this.src='/Imagenes/Buscar_b.JPG';" 
                                                            onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                         <asp:GridView ID="DGV_BuscarPersona" runat="server" AllowPaging="false" 
                                                                  AutoGenerateColumns="false" PageSize="20" Width="100%">
                                                                  <Columns>
                                                                      <asp:CommandField ButtonType="Link" SelectText="Seleccionar" 
                                                                          ShowSelectButton="true" />
                                                                      <asp:BoundField DataField="IdPersona" HeaderText="Id" NullDisplayText="0" />
                                                                      <asp:BoundField DataField="getNombreParaMostrar" HeaderText="Descripci�n" 
                                                                          NullDisplayText="---" />
                                                                      <asp:BoundField DataField="Dni" HeaderText="D.N.I." NullDisplayText="---" />
                                                                      <asp:BoundField DataField="Ruc" HeaderText="R.U.C." NullDisplayText="---" />
                                                                      <asp:BoundField DataField="Direccion" HeaderText="Direcci�n" 
                                                                          NullDisplayText="---" />
                                                                      <asp:BoundField DataField="PorcentPercepcion" HeaderText="Percep. (%)" 
                                                                          NullDisplayText="0" DataFormatString="{0:F2}" />
                                                                          <asp:BoundField DataField="PorcentRetencion" HeaderText="Retenc. (%)" 
                                                                          NullDisplayText="0" DataFormatString="{0:F2}" />
                                                                  </Columns>
                                                                  <HeaderStyle CssClass="GrillaHeader" />
                                                                  <FooterStyle CssClass="GrillaFooter" />
                                                                  <PagerStyle CssClass="GrillaPager" />
                                                                  <RowStyle CssClass="GrillaRow" />
                                                                  <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                  <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                  <EditRowStyle CssClass="GrillaEditRow" />
                                                              </asp:GridView></td>
                                    </tr> 
                                    <tr>
                                    
                                    <td>
                                    
                                        <asp:Button ID="btnAnterior_Persona" runat="server" Font-Bold="true" Width="50px" Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionPersona('0'));" />
                                        <asp:Button ID="btnPosterior_Persona" runat="server" Font-Bold="true" Width="50px" Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionPersona('1'));" />
                                        <asp:TextBox ID="txtPageIndex_Persona" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                                        <asp:Button ID="btnIr_Persona" runat="server" Font-Bold="true" Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionPersona('2'));" />
                                        <asp:TextBox ID="txtPageIndexGO_Persona" Width="50px" CssClass="TextBox_ReadOnly" runat="server" onKeyPress="return(onKeyPressEsNumero());"></asp:TextBox>
                                    
                                    </td>
                                    </tr>                                     
                 
                 </table>      
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>                                                   
<div id="capaBuscarProdxID"
        
            style="border: 3px solid blue; padding: 8px; width:auto; height:auto; position:absolute;background-color:white; z-index:2; display :none; top: 196px; left: 378px;">
        <table>
        <tr>
        <td colspan="2"  align="center">      
        <asp:ImageButton ID="btnCerrarCapaProdxID" runat="server"  ToolTip="Cerrar" OnClientClick="return(  offCapa('capaBuscarProdxID')    );"
        ImageUrl="~/Imagenes/Cerrar_B.JPG" onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
        onmouseover="this.src='/Imagenes/Cerrar_A.JPG';" />   </td>        
        </tr>
        <tr>
        <td class="Label" >Ingrese ID:</td>        
        <td > 
            <asp:TextBox ID="txtBuscarProdxId" onkeypress="return(validarIdProducto());" Width="75px" runat="server"></asp:TextBox> 
        </td>        
        </tr>
        <tr>
        <td align="center"   colspan="2" > 
        <asp:ImageButton ID="btnAceptarBuscarProdxID" runat="server" 
        ImageUrl="~/Imagenes/Aceptar_B.JPG" 
        onmouseout="this.src='/Imagenes/Aceptar_B.JPG';" OnClientClick="return(valBuscarProdxID());"
        onmouseover="this.src='/Imagenes/Aceptar_A.JPG';" />  </td>                
        </tr>
        
        
        </table>        
        
    </div>    
<div id="capaImpresion" style="display:none;height:1000px;width:1000px;" >
                        <table class="Label_Print">
                        <tr>
                        <td>
                        <%=objStringB.ToString()%>
                        </td>
                        </tr>
                        </table>
                        
                        
                        </div>
<div id="capaHelp" 
            style="border: 3px solid blue; padding: 8px; width:auto; height:auto; position:absolute; top:233px; left:313px; background-color:white; z-index:2; display :none;">
<table >
<tr>
<td class="LabelRojo" style="font-weight:bold" >Opci�n Nuevo</td>
<td class="Texto" style="font-weight:bold;text-align:center;width:20px">-</td>
<td class="Texto" style="font-weight:bold">Ctrl</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold">Shift</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold;text-align:left">N</td>
</tr>
<tr>
<td class="LabelRojo"    style="font-weight:bold">Opci�n Editar</td>
<td class="Texto" style="font-weight:bold;text-align:center;width:20px">-</td>
<td class="Texto" style="font-weight:bold">Ctrl</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold">Shift</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold;text-align:left">E</td>
</tr>
<tr>
<td class="LabelRojo"    style="font-weight:bold">Opci�n Anular</td>
<td class="Texto" style="font-weight:bold;text-align:center;width:20px">-</td>
<td class="Texto" style="font-weight:bold">Ctrl</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold">Shift</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold;text-align:left">A</td>
</tr>
<tr>
<td class="LabelRojo"    style="font-weight:bold">Opci�n Buscar</td>
<td class="Texto" style="font-weight:bold;text-align:center;width:20px">-</td>
<td class="Texto" style="font-weight:bold">Ctrl</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold">Shift</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold;text-align:left">B</td>
</tr>
<tr>
<td class="LabelRojo"    style="font-weight:bold">Opci�n Guardar</td>
<td class="Texto" style="font-weight:bold;text-align:center;width:20px">-</td>
<td class="Texto" style="font-weight:bold">Ctrl</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold">Shift</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold;text-align:left">G</td>
</tr>
<tr>
<td class="LabelRojo"    style="font-weight:bold">Opci�n Imprimir</td>
<td class="Texto" style="font-weight:bold;text-align:center;width:20px">-</td>
<td class="Texto" style="font-weight:bold">Ctrl</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold">Shift</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold;text-align:left">T</td>
</tr>
<tr>
<td class="LabelRojo"    style="font-weight:bold">Opci�n Despachar</td>
<td class="Texto" style="font-weight:bold;text-align:center;width:20px">-</td>
<td class="Texto" style="font-weight:bold">Ctrl</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold">Shift</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold;text-align:left">S</td>
</tr>
<tr>
<td class="LabelRojo"    style="font-weight:bold">Opci�n Gu�a Remisi�n</td>
<td class="Texto" style="font-weight:bold;text-align:center;width:20px">-</td>
<td class="Texto" style="font-weight:bold">Ctrl</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold">Shift</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold;text-align:left">R</td>
</tr>
<tr>
<td class="LabelRojo"    style="font-weight:bold">Opci�n Canjear Cotizaci�n</td>
<td class="Texto" style="font-weight:bold;text-align:center;width:20px">-</td>
<td class="Texto" style="font-weight:bold">Ctrl</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold">Shift</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold;text-align:left">C</td>
</tr>
<tr>
<td class="LabelRojo"    style="font-weight:bold">Opci�n Ayuda</td>
<td class="Texto" style="font-weight:bold;text-align:center;width:20px">-</td>
<td class="Texto" style="font-weight:bold">Ctrl</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold">Shift</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold;text-align:left">L</td>
</tr>
<tr>
<td class="LabelRojo"    style="font-weight:bold">Opci�n Buscar Productos</td>
<td class="Texto" style="font-weight:bold;text-align:center;width:20px">-</td>
<td class="Texto" style="font-weight:bold">Ctrl</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold">Shift</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold;text-align:left">1 (Alfa num�rico)</td>
</tr>
<tr>
<td class="LabelRojo"    style="font-weight:bold">Opci�n Buscar Clientes</td>
<td class="Texto" style="font-weight:bold;text-align:center;width:20px">-</td>
<td class="Texto" style="font-weight:bold">Ctrl</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold">Shift</td>
<td class="Texto" style="font-weight:bold">+</td>
<td class="Texto" style="font-weight:bold;text-align:left">2 (Alfa num�rico)</td>
</tr>
<tr>
<td class="LabelRojo"  colspan="7"  style="color:Green;font-weight:bold;text-align:left">*** Presione ESC para cerrar cualquier ventana emergente.</td>
</tr>
</table>                            
</div>      
<div  id="capaEditarPersona"         
style="border: 3px solid blue; padding: 10px; width:auto; height:auto; position:absolute; top:302px; left:23px; background-color:white; z-index:3; display :none;">    
                  <table cellspacing="0" style="width: 750px; height: 215px;">
                    <tr>
                        <td  style="text-align:right" class="Texto">
                            Tipo Agente:
                        </td>                                
                        <td colspan="3">                                                        
                        <asp:DropDownList ID="cboTipoAgente_EditarCliente" runat="server" onClick="return(  valOnClickTipoAgente_EditarCliente()  );">
                        </asp:DropDownList>   
                        </td>                                
                    </tr>
                    <tr>
                        <td style="text-align:right" class="Texto">
                                    Raz�n Social:
                                </td>
                        <td colspan="3">
                                    <asp:TextBox ID="txtRazonSocial_EditarCliente" Width="653px" runat="server" MaxLength="100" 
                                   onFocus="return( aceptarFoco(this)  );"  onKeypress="return(  valOnKeyPress_EditarCliente('2')  );"  ></asp:TextBox>
                                </td>
                    </tr>
                    <tr>
                        <td style="text-align:right" class="Texto">
                                    Ap. Paterno:
                                    </td>
                        <td>
                                    <asp:TextBox ID="txtApPaterno_EditarCliente" runat="server" onKeypress="return(  valOnKeyPress_EditarCliente('1')  );"
                                        Width="282px" MaxLength="25"  onFocus="return( aceptarFoco(this)  );"  ></asp:TextBox>
                                    </td>
                        <td  style="text-align:right" class="Texto"  >
                                    Ap. Materno:
                                    </td>
                        <td>
                                    <asp:TextBox ID="txtApMaterno_EditarCliente" runat="server"   onKeypress="return(  valOnKeyPress_EditarCliente('1')  );"
                                        Width="282px" MaxLength="25" onFocus="return( aceptarFoco(this)  );"   ></asp:TextBox>
                                    </td>
                    </tr>
                    <tr>
                        <td    style="text-align:right" class="Texto"   >
                                    Nombres:
                                    </td>
                        <td style="width: 286px">
                            <asp:TextBox ID="txtNombres_EditarCliente" runat="server"  Width="282px"  onKeypress="return(  valOnKeyPress_EditarCliente('1')  );"
                                MaxLength="25"  onFocus="return( aceptarFoco(this)  );"  ></asp:TextBox>
                                    </td>
                        <td   style="text-align:right" class="Texto" >
                                    D.N.I.:
                                    </td>
                        <td>
                                    <asp:TextBox ID="txtDNI_EditarCliente" runat="server" MaxLength="8" onFocus="return( aceptarFoco(this)  );" onKeypress="return(  valOnKeyPressDNI_EditarCliente()  );" ></asp:TextBox>
                                    </td>
                    </tr>
                    <tr>
                        <td   style="text-align:right" class="Texto" >
                                    R.U.C.:
                                    </td>
                        <td style="width: 286px">
                                    <asp:TextBox ID="txtRUC_EditarCliente" onFocus="return( aceptarFoco(this)  );" runat="server" MaxLength="11" onKeypress="return( valOnKeyPressRUC_EditarCliente() );" ></asp:TextBox>
                                    </td>
                        <td style="width: 82px">
                                    &nbsp;</td>
                        <td>
                                    &nbsp;</td>
                    </tr>                    
                    <tr>
                        <td style="width: 86px; text-align: right;">
                                    &nbsp;</td>
                        <td colspan="3">
                            <asp:ImageButton ID="btnGuardar_EditarCliente" runat="server" 
                        ImageUrl="~/Imagenes/Guardar_B.JPG" OnClientClick="return(  valOnClickGuardar_EditarCliente()    );"
                        onmouseover="this.src='/Imagenes/Guardar_A.JPG';"  
                        onmouseout="this.src='/Imagenes/Guardar_B.JPG';"  CausesValidation="true" /> 
                            
                            
                            <asp:ImageButton ID="btnCerrar_CapaEditarCliente" runat="server" 
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  OnClientClick="return(offCapa('capaEditarPersona'));"/> 
                                                
                                                
                                                
                                    </td>
                    </tr>
                   </table>                      
                        </div>  
    
<div  id="capaGlosa" style="border: 3px solid blue; padding: 8px; width:auto; height:auto; position:absolute; top:300px; left:250px; background-color:white; z-index:2; display :none; ">
                  <table>                                    
                                <tr><td align="center">
                            <asp:ImageButton ID="ImageButton7" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  
                                        OnClientClick="return(offCapa('capaGlosa'));" />                                
                                </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <table>
                                            <tr>
                                                <td class="Texto">
                                                    Glosa:
                                                </td>
                                                <td>                                                    
                                                    <asp:TextBox ID="txtGlosa_CapaGlosa" runat="server" Width="300px" ></asp:TextBox>
                                                </td>                                                    
                                            </tr>                                                                                       
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                <td align="center">
                                <asp:ImageButton ID="btnAddGlosa_CapaGlosa" runat="server" 
                                            ImageUrl="~/Imagenes/Aceptar_B.JPG" 
                                            onmouseout="this.src='/Imagenes/Aceptar_B.JPG';" OnClientClick="return(     valOnClickAddGlosa()       );"
                                                onmouseover="this.src='/Imagenes/Aceptar_B.JPG';" /></td>
                                </tr>
                                </table>                                                                                                     
</div>    

                        <script type="text/javascript" language="javascript">
                            var valorRecalculado = false;

                            function imprimirCapa() {

                               printPreviewDiv('capaImpresion');
                                return false;                            
                            
                            /*
                                var ventimp = window.open(' ', 'Factura', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                                var capa =  capaImpresion;
                                ventimp.document.write('<head> <link href="../../Estilos/Controles.css" rel="stylesheet" type="text/css" /> </head>');
                                ventimp.document.write(capa.innerHTML);
                                ventimp.document.close();
                                ventimp.print();
                                //ventimp.close();
                                return false;
                                */
                            }                                                        
                        
                            function valAddProductos() {
                                var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
                                var cont=0;
                                if (grilla == null) {
                                    alert('No se seleccionaron productos.');
                                    return false;
                                }
                                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                                    var rowElem = grilla.rows[i];
                                    var txtCantidad = rowElem.cells[0].children[0];
                                    if (parseFloat(txtCantidad.value) > 0) {
                                        if (parseFloat(txtCantidad.value) > parseFloat(rowElem.cells[7].innerHTML)) {
                                            alert('La cantidad ingresada excede a la cantidad disponible.');
                                            txtCantidad.select();
                                            txtCantidad.focus();
                                            return false;
                                        } else if (parseFloat(txtCantidad.value) > 0) {
                                            cont = cont + 1;
                                        }
                                    }                                   
                                }

                                if (cont == 0) {
                                    alert('No se seleccionaron productos.');
                                    return false;
                                }

                                //************* Validamos que no agregue de otro almacen

                                var grillaDetalle = document.getElementById('<%=gvDetalle.ClientID%>');
                                if (grillaDetalle != null) {

                                    if (grillaDetalle.rows.length > 1) {

                                        //************ valido el almacen
                                        var cboAlmacen = document.getElementById('<%=cmbAlmacen_AddProd.ClientID%>');
                                        var hddIdAlmacen = document.getElementById('<%=hddIdAlmacen.ClientID%>');
                                        var hddNomAlmacen = document.getElementById('<%=hddNomAlmacen.ClientID%>');

                                        if (parseFloat(cboAlmacen.value) != parseFloat(hddIdAlmacen.value)) {
                                            alert('Debe agregar producto del almac�n ' + hddNomAlmacen.value);
                                            return false;
                                        }
                                        
                                    
                                    }
                                
                                
                                }                               
                                
                                
                                return true;
                            }
                         
                        
                            var indexGrilla = 0;
                            function saveIndexFilaMetro() {

                                //******************* validamos los valores

                                var txtLargo = document.getElementById('<%=txtLargo_Metros.ClientID%>');                                
                                var txtCantidad = document.getElementById('<%=txtCantidad_Metros.ClientID%>');

                                if (CajaEnBlanco(txtLargo) || parseFloat(txtLargo.value) == 0) {
                                    alert('Ingrese un valor.');
                                    txtLargo.select();
                                    txtLargo.focus();
                                    return false;
                                }

                                if (CajaEnBlanco(txtCantidad) || parseFloat(txtCantidad.value) == 0) {
                                    alert('Ingrese un valor.');
                                    txtCantidad.select();
                                    txtCantidad.focus();
                                    return false;
                                }
                                
                                var hddIndex = document.getElementById('<%=hddIndexDetalle_Area.ClientID%>');
                                hddIndex.value = indexGrilla - 1;
                                offCapa('CapaLongitud');
                                return true;
                            }
                            
                            function onCapaLongitud(s) {
                                //******************** validamos que exista el IdUnidadMedida=18
                                var grilla = document.getElementById('<%=gvDetalle.ClientID %>');
                                if (grilla != null) {
                                    for (var i = 1; i < grilla.rows.length; i++) {
                                        var rowElem = grilla.rows[i];
                                        if (rowElem.cells[2].children[0].cells[1].children[1].id == event.srcElement.id) {

                                            //***************
                                            var cboUM = rowElem.cells[4].children[0];
                                            var cont = 0;
                                            for (var x = 0; x < cboUM.length; x++) {
                                                if (cboUM[x].value == '15') {
                                                    cont = 1;
                                                    indexGrilla = i;
                                                    break;
                                                }
                                            }
                                            if (cont == 0) {
                                                alert('Este producto no tiene registrado la Unidad de Medida [Metros].');
                                                return false;
                                            } else {
                                                break;
                                            }
                                        }
                                    }
                                }
                                //************************ Limpiamos los controles
                                onCapa(s);
                                
                                document.getElementById('<%= txtCantidad_Metros.ClientID%>').value='';
                                document.getElementById('<%= txtLargo_Metros.ClientID%>').value = '';
                                document.getElementById('<%= txtLargo_Metros.ClientID%>').select();
                                document.getElementById('<%= txtLargo_Metros.ClientID%>').focus();
                                
                                return false;
                            }
                        
                        
                            function saveIndexFila() {

                                //******************* validamos los valores

                                var txtLargo = document.getElementById('<%=txtLargo.ClientID%>');
                                var txtAncho = document.getElementById('<%=txtAncho.ClientID%>');
                                var txtCantidad = document.getElementById('<%=txtCantidadRetazo.ClientID%>');

                                if (CajaEnBlanco(txtAncho) || parseFloat(txtAncho.value) == 0) {
                                    alert('Ingrese un valor.');
                                    txtAncho.select();
                                    txtAncho.focus();
                                    return false;
                                }

                                if (CajaEnBlanco(txtLargo) || parseFloat(txtLargo.value) == 0) {
                                    alert('Ingrese un valor.');
                                    txtLargo.select();
                                    txtLargo.focus();
                                    return false;
                                }                                
                                if (CajaEnBlanco(txtCantidad) || parseFloat(txtCantidad.value) == 0) {
                                    alert('Ingrese un valor.');
                                    txtCantidad.select();
                                    txtCantidad.focus();
                                    return false;
                                }
                                
                            
                                var hddIndex = document.getElementById('<%=hddIndexDetalle_Area.ClientID%>');                                                                
                                offCapa('capaAddMagnitud');
                                hddIndex.value = indexGrilla - 1;
                                return true;
                            }



                            function valCreditoDisponible() {
                              
                              /*  var grilla = document.getElementById('<%= gvDetalle.ClientID%>');
                                if (grilla == null) {
                                    alert('Debe ingresar productos.');
                                    return false;
                                }*/
                                
                                var txtIdPersona = document.getElementById('<%= txtCodigoCliente.ClientID%>');

                                if (CajaEnBlanco(txtIdPersona)) {
                                    alert('Ingrese un cliente.');
                                    return false;
                                }

                                var grilla = document.getElementById('<%= gvLineaCredito.ClientID%>');
                                if (grilla == null) {
                                    alert('El Cliente no posee L�neas de Cr�dito.');
                                    return false;
                                }

                                var grilla = document.getElementById('<%= DGV_Cancelacion.ClientID%>');
                                if (grilla != null) {
                                    alert('No puede darse cr�dito porque se han registrado Datos de Cancelaci�n. Si desea cambiar la condici�n de pago a [Cr�dito], quite los datos de cancelaci�n.');
                                    return false;
                                }

                                var grilla = document.getElementById('<%= DGV_Vuelto.ClientID%>');
                                if (grilla != null) {
                                    alert('No puede darse cr�dito porque se han registrado Datos de Cancelaci�n. Si desea cambiar la condici�n de pago a [Cr�dito], quite los datos de cancelaci�n.');
                                    return false;
                                }                                
                                return true;
                            }
                            
                            function emitirGuiaRem() {
                                var hdd = document.getElementById('<%= hddIdDocumento.ClientID%>');
                                if (hdd.value.length == 0 || hdd.value == '' || parseFloat(hdd.value) == 0) {
                                    alert('No se ha registrado ning�n documento de venta..');
                                    return false;
                                }
                                //if (confirm('Desea mandar la impresi�n de la Gu�a de Remisi�n?')) {
                                window.open('../../Almacen1/FrmGuiaRemision_1.aspx?IdDocumento=' + hdd.value, 'Reporte', 'resizable=yes,width=1024,height=700', null);
                                //}
                                return false;
                            }
                        
                            function imprimirDocumento() {
                                var hdd = document.getElementById('<%= hddIdDocumento.ClientID%>');
                                var cbo = document.getElementById('<%=cboTipoDocumento.ClientID%>');
                                if (!(esDecimal(hdd.value)) || isNaN(hdd.value) || hdd.value.length == 0) {
                                    alert('No se ha guardado ning�n documento.');
                                    return false;
                                }
                                if (!confirm('Desea mandar a impresi�n el documento?')) {
                                    return false;
                                }
                                switch (parseFloat(cbo.value)) {
                                    case 1: //factura
                                        window.open('../../Reportes/visor.aspx?iReporte=2&IdDoc=' + hdd.value, 'Factura', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                                        break;
                                    case 21: //orden de despacho
                                        window.open('../../Reportes/visor.aspx?iReporte=3&IdDoc=' + hdd.value, 'Orden de Despacho', 'resizable=yes,width=1024,height=700,scrollbars=1', null);
                                        break;
                                    case 3: //Boleta
                                        window.open('../../Reportes/visor.aspx?iReporte=6&IdDoc=' + hdd.value, 'Boleta', 'status:no,help:no,Width=800,Height=750,scrollbars=1', null);
                                        //showModelessDialog('../../Reportes/visor.aspx?iReporte=6&IdDoc=' + hdd.value, 'Boleta', 'status:no;help:no;dialogWidth:600px;dialogHeight:700px');
                                        //showModelessDialog('../../Reportes/visor.aspx?iReporte=6&IdDoc=' + hdd.value, 'Boleta', 'status:no;help:no;dialogWidth:300px;dialogHeight:550px');         

                                        break;
                                    case 14: //cotizacion
                                        window.open('../../Reportes/visor.aspx?iReporte=5&IdDoc=' + hdd.value, 'Cotizacion', null, null);
                                        break;
                                    case 15: //Pedido del Cliente
                                        window.open('../../Reportes/visor.aspx?iReporte=4&IdDoc=' + hdd.value, 'Pedido del Cliente', null, null);
                                        break;
                                }
                                return false;
                            }


                            function imprimirDocumento_capa() {
                                var grilla = document.getElementById('<%= DGV_ImpresionDoc_Cab.ClientID%>');
                                var cbo = document.getElementById('<%=cboTipoDocumento.ClientID%>');

                                var IdDocumento = 0;

                                if (grilla == null) {
                                    alert('No se ha registrado ning�n documento.');
                                    return false;
                                } else if (grilla.rows.length==2) {  //**** SOLO EXISTE UN DOCUMENTO
                                    validarMostrarCapaImpresion('0')  //*********** Imprimimos de frente el documento
                                    return false;
                                }

                                for (var i = 1; i < grilla.rows.length; i++) {
                                    var rowElem = grilla.rows[i];
                                    if (rowElem.cells[7].children[0].status==true) {
                                        IdDocumento = rowElem.cells[1].innerHTML;
                                        break;
                                    }
                                }

                                if (IdDocumento == 0) {
                                    alert('No se ha seleccionado ning�n documento para impresi�n.')
                                    return false;                                
                                }
                                
                                
                                
                                switch (parseFloat(cbo.value)) {
                                    case 1: //factura
                                        window.open('../../Reportes/visor.aspx?iReporte=2&IdDoc=' + IdDocumento, 'Factura', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                                        break;
                                    case 21: //orden de despacho
                                        window.open('../../Reportes/visor.aspx?iReporte=3&IdDoc=' + IdDocumento, 'Orden de Despacho', 'resizable=yes,width=1024,height=700', null);
                                        break;
                                    case 3: //Boleta
                                        window.open('../../Reportes/visor.aspx?iReporte=6&IdDoc=' + IdDocumento, 'Boleta', 'status:no,help:no,Width=800,Height=750', null);
                                        //showModelessDialog('../../Reportes/visor.aspx?iReporte=6&IdDoc=' + hdd.value, 'Boleta', 'status:no;help:no;dialogWidth:600px;dialogHeight:700px');
                                        //showModelessDialog('../../Reportes/visor.aspx?iReporte=6&IdDoc=' + hdd.value, 'Boleta', 'status:no;help:no;dialogWidth:300px;dialogHeight:550px');         

                                        break;
                                    case 14: //cotizacion
                                        window.open('../../Ventas/Reportes/visorVentas.aspx?iReporte=2&IdDocumento=' + IdDocumento, 'Cotizacion', 'resizable=yes,width=1000,height=700', null);
                                        break;
                                    case 15: //Pedido del Cliente
                                        window.open('../../Reportes/visor.aspx?iReporte=4&IdDoc=' + IdDocumento, 'Pedido del Cliente', null, null);
                                        break;
                                }
                                return false;
                            }




                            function emitirOrdenDespacho_capa() {


                                //********* Validamos la COTIZACION
                                var IdTipoDocumento = parseInt(document.getElementById('<%= cboTipoDocumento.ClientID%>').value);
                                if (IdTipoDocumento == 14) {
                                    alert('No puede generar una Orden de Despacho para una Cotizaci�n.');
                                    return false;
                                }                            
                                var grilla = document.getElementById('<%= DGV_ImpresionDoc_Cab.ClientID%>');                                
                                var IdDocumento = 0;
                                if (grilla == null) {
                                    alert('No se ha registrado ning�n documento.');
                                    return false;
                                } else if (grilla.rows.length == 2) {  //**** SOLO EXISTE UN DOCUMENTO
                                    validarMostrarCapaImpresion('1')  //*********** Imprimimos de frente el documento
                                    return false;
                                }
                                for (var i = 1; i < grilla.rows.length; i++) {
                                    var rowElem = grilla.rows[i];
                                    if (rowElem.cells[7].children[0].status == true) {
                                        IdDocumento =parseInt( rowElem.cells[1].innerHTML);
                                        break;
                                    }
                                }

                                if (IdDocumento == 0) {
                                    alert('No se ha seleccionado ning�n documento.')
                                    return false;
                                }


                                var IdEstadoDocumento = parseInt(document.getElementById('<%=cboEstadoDocumento.ClientID%>').value);
                                if (IdEstadoDocumento == 2) {  //********** ANULADO
                                    alert('El Documento est� ANULADO.');
                                    return false;
                                }                                                                
                                //if (confirm('Desea mandar la impresi�n de la Gu�a de Remisi�n?')) {
                                window.open('FrmOrdenDespacho.aspx?IdDocumento=' + IdDocumento, 'Reporte', 'resizable=yes,width=1024,height=700,scrollbars=1', null);
                                //}
                                return false;
                            }

                            function emitirGuiaRemisionRemitente_capa() {



                                //********* Validamos la COTIZACION
                                var IdTipoDocumento = parseInt(document.getElementById('<%= cboTipoDocumento.ClientID%>').value);
                                if (IdTipoDocumento == 14) {
                                    alert('No puede generar una Gu�a de Remisi�n para una Cotizaci�n.');
                                    return false;
                                }



                                var grilla = document.getElementById('<%= DGV_ImpresionDoc_Cab.ClientID%>');


                                var IdDocumento = 0;

                                if (grilla == null) {
                                    alert('No se ha registrado ning�n documento.');
                                    return false;
                                } else if (grilla.rows.length == 2) {  //**** SOLO EXISTE UN DOCUMENTO
                                    validarMostrarCapaImpresion('2')  //*********** Imprimimos de frente el documento
                                    return false;
                                }

                                for (var i = 1; i < grilla.rows.length; i++) {
                                    var rowElem = grilla.rows[i];
                                    if (rowElem.cells[7].children[0].status == true) {
                                        IdDocumento = parseInt(rowElem.cells[1].innerHTML);
                                        break;
                                    }
                                }

                                if (IdDocumento == 0) {
                                    alert('No se ha seleccionado ning�n documento.')
                                    return false;
                                }
                                
                                var IdEstadoDocumento = parseInt(document.getElementById('<%=cboEstadoDocumento.ClientID%>').value);
                                if (IdEstadoDocumento == 2) {  //********** ANULADO
                                    alert('El Documento est� ANULADO.');
                                    return false;
                                }
                                //if (confirm('Desea mandar la impresi�n de la Gu�a de Remisi�n?')) {
                                window.open('../../Almacen1/FrmGuiaRemision_1.aspx?IdDocumento=' + IdDocumento, 'Reporte', 'resizable=yes,width=1024,height=700,scrollbars=1', null);
                                //}
                                return false;
                            }

                            function emitirOrdenDespacho() {
                                var hdd = document.getElementById('<%= hddIdDocumento.ClientID%>');
                                if (hdd.value.length == 0 || hdd.value == '' || parseFloat(hdd.value) == 0) {
                                    alert('No se ha registrado ning�n documento de venta..');
                                    return false;
                                }
                                
                                
                                
                                
                                //if (confirm('Desea mandar la impresi�n de la Gu�a de Remisi�n?')) {
                                window.open('FrmOrdenDespacho.aspx?IdDocumento=' + hdd.value, 'Reporte', 'resizable=yes,width=1024,height=700', null);
                                //}
                                return false;
                            }

                            
                            //*************************** script generado para la busqueda de un documento
                            //    function buscarDocumento() {
                            //        var caja = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
                            //        caja.disabled = false;
                            //        caja.select();
                            //        caja.focus();
                            //        return true;
                            //    }
                            function validarBuscarDocumento() {
                                var caja = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
                                if (CajaEnBlanco(caja)) {
                                    alert('Ingrese un c�digo de b�squeda.');
                                    caja.focus();
                                    return false;
                                }
                                return true;
                            }
                            //*************************** fin - buscar documento
                            function validarEfectivo() {
                                var caja_monto = document.getElementById('<%=txtMonto.ClientID %>');
                                var caja_efectivo = document.getElementById('<%=txtEfectivo.ClientID %>');
                                var monto = 0;
                                var efectivo = 0;
                                if (!esDecimal(caja_efectivo.value)) {
                                    alert('Cantidad no v�lido.');
                                    
                                    caja_efectivo.select();
                                    caja_efectivo.focus();
                                }
                                if (esDecimal(caja_monto.value) && caja_monto.value.length > 0) {
                                    monto = redondear(parseFloat(caja_monto.value), 2);
                                }
                                if (esDecimal(caja_efectivo.value) && caja_efectivo.value.length > 0) {
                                    efectivo = redondear(parseFloat(caja_efectivo.value), 2);
                                }
                                if (efectivo < monto) {
                                    if (parseFloat(caja_efectivo.value) > 0) {
                                        alert('La monto de efectivo no puede ser menos al monto total a pagar.');
                                        
                                        caja_efectivo.select();
                                        caja_efectivo.focus();
                                    }
                                    if (parseFloat(caja_efectivo.value) == 0 || caja_efectivo.value.length == 0) {
                                        
                                        
                                    }
                                }
                                return false;
                            }


                            function calcularVuelto() {
                                var caja_monto = document.getElementById('<%=txtMonto.ClientID %>');
                                var caja_efectivo = document.getElementById('<%=txtEfectivo.ClientID %>');
                                var monto = 0;
                                var efectivo = 0;
                                var vuelto = 0;
                                if (esDecimal(caja_monto.value) && caja_monto.value.length > 0) {
                                    monto = redondear(parseFloat(caja_monto.value), 2);
                                }
                                if (esDecimal(caja_efectivo.value) && caja_efectivo.value.length > 0) {
                                    efectivo = redondear(parseFloat(caja_efectivo.value), 2);
                                }
                                vuelto = efectivo - monto;
                                
                                return true;
                            }



                            //REGISTRAR CLIENTE - ING. PERCY
                            function activarPanel() {
                                LimpiarControlesCliente();
                                var combo = document.getElementById('<%=cmbTipoPersona.ClientID %>');
                                var flag = false;
                                switch (combo.value) {
                                    case 'N':
                                        flag = false;
                                        break;
                                    case 'J':
                                        flag = true;
                                        break;
                                }
                                //controles de Per. Natural

                                document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').disabled = flag;
                                document.getElementById('<%= txtApMaterno_Cliente.ClientID%>').disabled = flag;
                                document.getElementById('<%= txtNombres_Cliente.ClientID%>').disabled = flag;
                                document.getElementById('<%= txtDNI_Cliente.ClientID%>').disabled = flag;

                                //controles de Per. Juridica
                                document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').disabled = !(flag);
                                switch (combo.value) {
                                    case 'N':
                                        document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').select();
                                        document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').focus();
                                        break;
                                    case 'J':
                                        document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').select();
                                        document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').focus();
                                        break;
                                }
                                return true;
                            }
                            function validarSaveCliente() {
                                var combo = document.getElementById('<%=cmbTipoPersona.ClientID %>');
                                switch (combo.value) {
                                    case 'N':
                                        if (document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').value.length == 0) {
                                            alert('El campo Ap. Paterno es un campo requerido.');
                                            document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').focus();
                                            return false;
                                        }
                                        if (document.getElementById('<%= txtNombres_Cliente.ClientID%>').value.length == 0) {
                                            alert('El campo Nombres es un campo requerido.');
                                            document.getElementById('<%= txtNombres_Cliente.ClientID%>').focus();
                                            return false;
                                        }
                                        break;
                                    case 'J':
                                        if (document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').value.length == 0) {
                                            alert('El campo Raz�n Social es un campo requerido.');
                                            document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').focus();
                                            return false;
                                        }

                                        if (document.getElementById('<%= txtRUC_Cliente.ClientID%>').value.length == 0) {
                                            alert('El campo [ R.U.C. ] es un campo requerido.');
                                            document.getElementById('<%= txtRUC_Cliente.ClientID%>').focus();
                                            return false;
                                        }
                                        
                                        break;
                                }
                                return (confirm('Desea continuar con la operaci�n?'));
                            }
                          
                            void function LimpiarControlesCliente() {
                                document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').value = "";
                                document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').value = "";
                                document.getElementById('<%= txtApMaterno_Cliente.ClientID%>').value = "";
                                document.getElementById('<%= txtNombres_Cliente.ClientID%>').value = "";
                                document.getElementById('<%= txtDNI_Cliente.ClientID%>').value = "";
                                document.getElementById('<%= txtRUC_Cliente.ClientID%>').value = "";
                                document.getElementById('<%= txtDireccion_Cliente.ClientID%>').value = "";
                                document.getElementById('<%= txtTelefono_Cliente.ClientID%>').value = "";
                                document.getElementById('<%= cmbTipoAgente_RegistrarCliente.ClientID%>').value = '0';

                            }



                            function onCapaAddMagnitud(s) {

                                //******************** validamos que exista el IdUnidadMedida=18
                                var grilla = document.getElementById('<%=gvDetalle.ClientID %>');
                                if (grilla != null) {
                                    for (var i = 1; i < grilla.rows.length; i++) {
                                        var rowElem = grilla.rows[i];
                                        if (rowElem.cells[2].children[0].cells[1].children[0].id == event.srcElement.id) {

                                            //***************
                                            var cboUM = rowElem.cells[4].children[0];
                                            var cont = 0;
                                            for (var x = 0; x < cboUM.length; x++) {
                                                if (cboUM[x].value == '18') {
                                                    cont = 1;
                                                    break;                                                
                                                }
                                            }
                                            if (cont == 0) {
                                                alert('Este producto no tiene registrado la Unidad de Medida [Metros Cuadrados].');
                                                return false;
                                            } else {
                                                break;
                                            }
                                        }
                                    }
                                }
                            
                            
                                LimpiarControlesAddMagnitud();                                
                                if (grilla != null) {
                                    for (var i = 1; i < grilla.rows.length; i++) {
                                        var rowElem = grilla.rows[i];
                                        if (rowElem.cells[2].children[0].cells[1].children[0].id == event.srcElement.id) {
                                            indexGrilla = i;
                                            break;
                                        }
                                    }
                                }
                                onCapa(s);
                                document.getElementById('<%= txtAncho.ClientID%>').focus();
                                return false;
                            }

                            function aceptarArea(s) {
                                // debo validar

                                var caja1 = parseFloat(document.getElementById('<%=txtAncho.ClientID%>').value);
                                var caja2 = parseFloat(document.getElementById('<%=txtLargo.ClientID%>').value);
                                var area = caja1 * caja2;

                                var grilla = document.getElementById('<%=gvDetalle.ClientID %>');
                                if (grilla != null) {
                                    for (var i = 1; i < grilla.rows.length; i++) {
                                        var rowElem = grilla.rows[i];
                                        if (i == indexGrilla) {
                                            rowElem.cells[2].children[0].cells[0].children[0].value = area;
                                            break;
                                        }
                                    }
                                }
                                offCapa(s);
                                calcularImporteONLOAD('-1');
                                return false;
                            }


                            void function LimpiarControlesAddMagnitud() {
                                document.getElementById('<%= txtAncho.ClientID%>').value = "";
                                document.getElementById('<%= txtLargo.ClientID%>').value = "";
                                document.getElementById('<%=txtCantidadRetazo.ClientID%>').value = "";
                            }
                            //FIN REGISTRAR CLIENTE - ING. PERCY
                            function valBlur() {
                                var id = event.srcElement.id;
                                var caja = document.getElementById(id);
                                if (CajaEnBlanco(caja)) {
                                    caja.focus();
                                    alert('Debe ingresar un valor.');
                                }
                            }
                            function valBlur_Cantidad() {
                                var id = event.srcElement.id;
                                var caja = document.getElementById(id);
                                if (CajaEnBlanco(caja)) {
                                    caja.focus();
                                    alert('Debe ingresar un valor.');
                                    return false;
                                }
                                var grilla = document.getElementById('<%=gvDetalle.ClientID %>');
                                for (var i = 1; i < grilla.rows.length; i++) {
                                    var rowElem = grilla.rows[i];
                                    if (caja.id == rowElem.cells[2].children[0].cells[0].children[0].id) {
                                        if (parseFloat(caja.value) > parseFloat(rowElem.cells[3].innerHTML)) {
                                            caja.select();
                                            caja.focus();
                                            alert('La Cantidad ingresada excede al stock Actual.');
                                            return false;
                                        }
                                    }

                                }

                            }
                            function validarNumeroPunto() {
                                var key = event.keyCode;
                                if (key == 46) {
                                    return true;
                                } else {
                                    if (!onKeyPressEsNumero()) {
                                        return false;
                                    }
                                }
                            }

                          /*  function validarNumeroPunto_Ejecutar(IdBoton) {
                                var key = event.keyCode;
                                if (key == 46) {
                                    return true;
                                } else if (key == 13) {
                                    document.getElementById(IdBoton).focus();
                                    alert('Hola');
                                    return false;
                                }else {
                                    if (!onKeyPressEsNumero()) {
                                        return false;
                                    }
                                }
                            }*/
                            
                            
                            function calcularImporteONLOAD(opc) {
                                var importeTotal = 0;
                                var descuentoTotal = 0;
                                var grilla = document.getElementById('<%=gvDetalle.ClientID %>');
                                var igv = parseFloat(document.getElementById('<%=hddIGV.ClientID %>').value);
                                var percepcion = 0;
                                var porcentPercep = 0;
                                
                                var retencion = 0;
                          

                                var cboTipoDocumento = document.getElementById('<%=cboTipoDocumento.ClientID %>');
                                var cboTipoAgente = document.getElementById('<%=cboTipoAgente.ClientID %>');
                                var txtTasaAgente = document.getElementById('<%=txtTasaAgente.ClientID %>');
                                var tasaAgente = 0;                                
                                if (esDecimal(txtTasaAgente.value) && !CajaEnBlanco(txtTasaAgente)) {
                                    tasaAgente = parseFloat(txtTasaAgente.value);
                                }

                                var importeProdConPercepcion = 0;

                                if (grilla == null) {
                                    document.getElementById('<%=txtTotalAPagar.ClientID %>').value = 0;
                                    document.getElementById('<%=txtRetencion.ClientID %>').value = 0;
                                    document.getElementById('<%=txtDetraccion.ClientID %>').value = 0;
                                    document.getElementById('<%=txtValorReferencial.ClientID %>').value = 0;
                                    document.getElementById('<%=txtPercepcion.ClientID %>').value = 0;
                                    document.getElementById('<%=txtTotal.ClientID %>').value = 0;
                                    document.getElementById('<%=txtDescuento.ClientID %>').value = 0;
                                    document.getElementById('<%=txtSubTotal.ClientID %>').value = 0;
                                    document.getElementById('<%=txtIGV.ClientID %>').value = 0;
                                    document.getElementById('<%=txtMonto.ClientID %>').value = 0;

                                    calcularMontosCancelacion();
                                    
                                    return true;
                                }

                                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                                    var rowElem = grilla.rows[i];
                                    var cantidad = redondear(parseFloat(rowElem.cells[2].children[0].cells[0].children[0].value), 2);
                                    if (isNaN(cantidad)) {
                                        cantidad = 0;
                                    }
                                    var precio = redondear(parseFloat(rowElem.cells[6].innerHTML), 2);
                                    if (isNaN(precio)) {
                                        precio = 0;
                                    }
                                    var dcto = redondear(parseFloat(rowElem.cells[7].children[0].value), 2);
                                    if (isNaN(dcto)) {
                                        dcto = 0;
                                    }
                                    var porcentDcto = redondear(parseFloat(rowElem.cells[8].children[0].value), 2);
                                    if (isNaN(porcentDcto)) {
                                        porcentDcto = 0;
                                    }
                                    var precioFinal = redondear(parseFloat(rowElem.cells[9].children[0].value), 2);
                                    if (isNaN(precioFinal)) {
                                        precioFinal = 0;
                                    }
                                    var importe = redondear(precioFinal * cantidad, 2);
                                    if (isNaN(importe)) {
                                        importe = 0;
                                    }
                                    //if (rowElem.cells[2].children[0].id == event.srcElement.id || rowElem.cells[6].children[0].id == event.srcElement.id || rowElem.cells[7].children[0].id == event.srcElement.id || rowElem.cells[8].children[0].id == event.srcElement.id) {//cantidad
                                    //leemos todas las variables a utilizar
                                    switch (opc) {
                                        case '-1': //cantidad                         
                                            break;
                                        case '0': //cantidad                         
                                            break;
                                        case '1': //dcto                        
                                            if (dcto >= precio) {
                                                dcto = precio;
                                                porcentDcto = 100;
                                                rowElem.cells[7].children[0].value = redondear(dcto, 2);
                                            } else if (precio == 0) {
                                                dcto = 0;
                                                porcentDcto = 0;
                                                rowElem.cells[7].children[0].value = redondear(dcto, 2);
                                            } else {
                                                porcentDcto = (dcto / precio) * 100;
                                            }
                                            precioFinal = precio - dcto;
                                            //imprimo                         
                                            rowElem.cells[8].children[0].value = redondear(porcentDcto, 2)
                                            rowElem.cells[9].children[0].value = redondear(precioFinal, 2)
                                            break;
                                        case '2': //% dcto
                                            if (porcentDcto >= 100) {
                                                dcto = precio;
                                                porcentDcto = 100;
                                                rowElem.cells[8].children[0].value = redondear(porcentDcto, 2)
                                            } else {
                                                dcto = (precio * (porcentDcto / 100));
                                            }
                                            precioFinal = precio - dcto;
                                            rowElem.cells[7].children[0].value = redondear(dcto, 2);
                                            rowElem.cells[9].children[0].value = redondear(precioFinal, 2)
                                            break;
                                        case '3':
                                            if (precioFinal > precio) {
                                                dcto = 0;
                                                porcentDcto = 0;
                                            } else {
                                                dcto = precio - precioFinal;
                                                porcentDcto = (dcto / precio) * 100;
                                            }
                                            rowElem.cells[7].children[0].value = redondear(dcto, 2);
                                            rowElem.cells[8].children[0].value = redondear(porcentDcto, 2)
                                            break;
                                    }
                                    importe = precioFinal * cantidad;
                                    rowElem.cells[10].children[0].value = redondear(importe, 2);
                                    //}
                                    importeTotal = importeTotal + importe;
                                    descuentoTotal = descuentoTotal + dcto * cantidad;
                                    //calculamos la percepcion
                                    porcentPercep = parseFloat(rowElem.cells[11].innerHTML)/100;
                                    percepcion = percepcion + importe * porcentPercep;

                                    if (porcentPercep > 0) {
                                        importeProdConPercepcion = importeProdConPercepcion + importe;
                                    }
                                    
                                }
                                var subTotal = (importeTotal) / (1 + igv);
                                var IdTipoDocumento = parseInt(cboTipoDocumento.value);
                                
                                if(IdTipoDocumento==14){  //*** Si es Cotizaci�n
                                    IdTipoDocumento = 1; //*** Lo trabajamos como Factura
                                }

                                //**************** Si es AGENTE PERCEPTOR y es FACTURA
                                if (cboTipoAgente.value == '1' && IdTipoDocumento == 1) {
                                    percepcion = (tasaAgente / 100) * importeProdConPercepcion;
                                }


                                //*********** Si es Retenedor y es Factura
                                if (cboTipoAgente.value == '2' && IdTipoDocumento == 1) {
                                    percepcion = 0;
                                    retencion = importeTotal * (tasaAgente / 100);
                                }

                                //**************** si es factura y es los pinos
                                var IdEmpresa=parseInt(document.getElementById('<%=cboPropietario.ClientID%>').value);
                                if (IdTipoDocumento == 1 && IdEmpresa == 2) {
                                    percepcion=0;
                                }

                                if ((IdTipoDocumento == 3) && (importeTotal < 700)) {
                                    var totalAPagar = importeTotal;
                                    percepcion = 0;
                                } else {
                                    var totalAPagar = importeTotal + percepcion;
                                }

                                //mostrar resultados
                                document.getElementById('<%=txtTotal.ClientID %>').value = redondear(importeTotal, 2);
                                document.getElementById('<%=txtSubTotal.ClientID %>').value = redondear(subTotal, 2);
                                document.getElementById('<%=txtIGV.ClientID %>').value = redondear(igv * subTotal, 2);
                                document.getElementById('<%=txtDescuento.ClientID %>').value = redondear(descuentoTotal, 2);
                                document.getElementById('<%=txtTotalAPagar.ClientID %>').value = redondear(totalAPagar, 2);
                                document.getElementById('<%=txtPercepcion.ClientID %>').value = redondear(percepcion, 2);
                                document.getElementById('<%=txtRetencion.ClientID %>').value = redondear(retencion, 2);

                                //******************* imprimimos en la secci�n Detalle de cancelaci�n
                                document.getElementById('<%=txtMonto.ClientID %>').value = redondear(totalAPagar, 2);

                                calcularMontosCancelacion();
                                
                                
                                return true;
                            }
                            
                            
                            
                            function calcularImporte(opc) {
                                var importeTotal = 0;
                                var descuentoTotal = 0;
                                var grilla = document.getElementById('<%=gvDetalle.ClientID %>');
                                var hddigv = document.getElementById('<%=hddIGV.ClientID %>');
                                var igv = parseFloat(hddigv.value);
                                var percepcion = 0;
                                var porcentPercep = 0;

                                var retencion = 0;

                                var cboTipoDocumento = document.getElementById('<%=cboTipoDocumento.ClientID %>');
                                var cboTipoAgente = document.getElementById('<%=cboTipoAgente.ClientID %>');
                                var txtTasaAgente = document.getElementById('<%=txtTasaAgente.ClientID %>');
                                var tasaAgente = 0;
                                if (esDecimal(txtTasaAgente.value) && !CajaEnBlanco(txtTasaAgente)) {
                                    tasaAgente = parseFloat(txtTasaAgente.value);
                                }

                                var importeProdConPercepcion = 0;


                                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                                    var rowElem = grilla.rows[i];
                                    var cantidad = redondear(parseFloat(rowElem.cells[2].children[0].cells[0].children[0].value), 2);
                                    if (isNaN(cantidad)) {
                                        cantidad = 0;
                                    }
                                    var precio = redondear(parseFloat(rowElem.cells[6].innerHTML), 2);
                                    if (isNaN(precio)) {
                                        precio = 0;
                                    }
                                    var dcto = redondear(parseFloat(rowElem.cells[7].children[0].value), 2);
                                    if (isNaN(dcto)) {
                                        dcto = 0;
                                    }
                                    var porcentDcto = redondear(parseFloat(rowElem.cells[8].children[0].value), 2);
                                    if (isNaN(porcentDcto)) {
                                        porcentDcto = 0;
                                    }
                                    var precioFinal = redondear(parseFloat(rowElem.cells[9].children[0].value), 2);
                                    if (isNaN(precioFinal)) {
                                        precioFinal = 0;
                                    }
                                    var importe = redondear(precioFinal * cantidad, 2);
                                    if (isNaN(importe)) {
                                        importe = 0;
                                    }
                                    if (rowElem.cells[2].children[0].cells[0].children[0].id == event.srcElement.id || rowElem.cells[7].children[0].id == event.srcElement.id || rowElem.cells[8].children[0].id == event.srcElement.id || rowElem.cells[9].children[0].id == event.srcElement.id) {//cantidad
                                        //leemos todas las variables a utilizar
                                        switch (opc) {
                                            case '-1': //cantidad                         
                                                break;
                                            case '0': //cantidad
                                                var stock = parseFloat(rowElem.cells[3].innerHTML);
                                                if (cantidad > stock) {
                                                    //alert('La cantidad ingresada no puede ser mayor que el stock actual.');

                                                    rowElem.cells[2].children[0].cells[0].children[0].select();
                                                    rowElem.cells[2].children[0].cells[0].children[0].focus();
                                                    return false;
                                                }
                                                break;
                                            case '1': //dcto                        
                                                if (dcto >= precio) {
                                                    dcto = precio;
                                                    porcentDcto = 100;
                                                    rowElem.cells[7].children[0].value = redondear(dcto, 2);
                                                } else if (precio == 0) {
                                                    dcto = 0;
                                                    porcentDcto = 0;
                                                    rowElem.cells[7].children[0].value = redondear(dcto, 2);
                                                } else {
                                                    porcentDcto = (dcto / precio) * 100;
                                                }
                                                precioFinal = precio - dcto;
                                                //imprimo                         
                                                rowElem.cells[8].children[0].value = redondear(porcentDcto, 2)
                                                rowElem.cells[9].children[0].value = redondear(precioFinal, 2)
                                                break;
                                            case '2': //% dcto
                                                if (porcentDcto >= 100) {
                                                    dcto = precio;
                                                    porcentDcto = 100;
                                                    rowElem.cells[8].children[0].value = redondear(porcentDcto, 2)
                                                } else {
                                                    dcto = (precio * (porcentDcto / 100));
                                                }
                                                precioFinal = precio - dcto;
                                                rowElem.cells[7].children[0].value = redondear(dcto, 2);
                                                rowElem.cells[9].children[0].value = redondear(precioFinal, 2)
                                                break;
                                            case '3':
                                                if (precioFinal > precio) {
                                                    dcto = 0;
                                                    porcentDcto = 0;
                                                } else {
                                                    dcto = precio - precioFinal;
                                                    porcentDcto = (dcto / precio) * 100;
                                                }
                                                rowElem.cells[7].children[0].value = redondear(dcto, 2);
                                                rowElem.cells[8].children[0].value = redondear(porcentDcto, 2)
                                                break;
                                        }
                                        importe = precioFinal * cantidad;
                                        rowElem.cells[10].children[0].value = redondear(importe, 2);
                                    }
                                    importeTotal = importeTotal + importe;
                                    descuentoTotal = descuentoTotal + dcto * cantidad;
                                    //calculamos la percepcion
                                    porcentPercep = parseFloat(rowElem.cells[11].innerHTML)/100;
                                    percepcion = percepcion + importe * porcentPercep;

                                    if (porcentPercep > 0) {
                                        importeProdConPercepcion = importeProdConPercepcion + importe;
                                    }
                                    
                                } //FIM DEL FOR         
                                var subTotal = (importeTotal) / (1 + igv);
                                //var percepcion = parseFloat(document.getElementById('<%=txtPercepcion.ClientID %>').value);
                                //var detraccion = parseFloat(document.getElementById('<%=txtDetraccion.ClientID %>').value);
                                //var retencion = parseFloat(document.getElementById('<%=txtRetencion.ClientID %>').value);


                                var IdTipoDocumento = parseInt(cboTipoDocumento.value);
                                if (IdTipoDocumento == 14) {  //**** Si es Cotizaci�n lo trabajamos como Factura
                                    IdTipoDocumento = 1;  //** Lo trabajamos como Factura                                
                                }

                                //**************** Si es AGENTE PERCEPTOR y es FACTURA
                                if (cboTipoAgente.value == '1' && IdTipoDocumento == 1) {
                                    percepcion = (tasaAgente / 100) * importeProdConPercepcion;
                                }

                                //*********** Si es Retenedor y es Factura
                                if (cboTipoAgente.value == '2' && IdTipoDocumento == 1) {
                                    percepcion = 0;
                                    retencion = importeTotal * (tasaAgente / 100);
                                }

                                //**************** si es factura y es los pinos
                                var IdEmpresa = parseInt(document.getElementById('<%=cboPropietario.ClientID%>').value);
                                if (IdTipoDocumento == 1 && IdEmpresa == 2) {
                                    percepcion = 0;
                                }

                                if (IdTipoDocumento == 3 && importeTotal < 700) {
                                    var totalAPagar = importeTotal;
                                    percepcion = 0;
                                } else {
                                    var totalAPagar = importeTotal + percepcion;
                                }


                                //mostrar resultados
                                document.getElementById('<%=txtTotal.ClientID %>').value = redondear(importeTotal, 2);
                                document.getElementById('<%=txtSubTotal.ClientID %>').value = redondear(subTotal, 2);
                                document.getElementById('<%=txtIGV.ClientID %>').value = redondear(igv * subTotal, 2);
                                document.getElementById('<%=txtDescuento.ClientID %>').value = redondear(descuentoTotal, 2);
                                document.getElementById('<%=txtTotalAPagar.ClientID %>').value = redondear(totalAPagar, 2);
                                document.getElementById('<%=txtPercepcion.ClientID %>').value = redondear(percepcion, 2);
                                document.getElementById('<%=txtRetencion.ClientID %>').value = redondear(retencion, 2);

                                //************************** imprimo en la secci�n Detalle de Cancelaci�n
                                document.getElementById('<%=txtMonto.ClientID %>').value = redondear(totalAPagar, 2);


                                calcularMontosCancelacion();
                                
                                
                                return true;
                            }

                            //Buscar SubLinea
                            function ValidarEnteroSinEnter() {
                                if (onKeyPressEsNumero() == false) {
                                    alert('Caracter no v�lido. Solo se permiten n�meros Enteros');
                                    return false;
                                }
                                return true;
                            }
                            function CajaEnBlancoCodigoSubLinea() {                                
                                return true;
                            }

                            //End Buscar SubLinea
                            //Buscar Cliente

                            function BuscarCliente() {

                                return true;
                            }
                            

                            function ValidarEnter() {
                                return (!esEnter());
                            }
                            function ValidaEnter() {
                                if (esEnter() == true) {
                                    alert('Tecla No permitida');
                                    caja.focus();
                                    return false;
                                }
                                return true;
                            }
                            //End Buscar Cliente


                            function mostrarCapaPersona() {
                                //******************* validamos que exista detalle

                                //var grilla = document.getElementById('<%=gvDetalle.ClientID%>');
                                //if (grilla != null) {
                                    onCapa('capaPersona');
                                    document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').select();
                                    document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').focus();
                                //} else {
                                //    alert('Debe ingresar primero el detalle del documento.');
                                //}
                                return false;
                            }
                            function addPersona_Venta() {
                                offCapa('capaPersona');
                                offCapa('capaEditarPersona');
                                offCapa('capaRegistrarCliente');                                
                                calcularImporteONLOAD('-1');
                                return false;
                            }
                            function mostrarCapaCatalogo() {
                                onCapa('capaBuscarProducto_AddProd');                                
                                document.getElementById('<%=txtCodigoSubLinea_AddProd.ClientID%>').select();
                                document.getElementById('<%=txtCodigoSubLinea_AddProd.ClientID%>').focus();                                
                                return false;
                            }

                            function aceptarFoco(caja) {
                                caja.select();
                                caja.focus();
                                return true;
                            }


                            function calcularMontosCancelacion() {
                                var grilla = document.getElementById('<%=DGV_Cancelacion.ClientID%>');
                                var txtTotalRecibido = document.getElementById('<%=txtMonto_Acumulado.ClientID%>');
                                var txtVuelto = document.getElementById('<%=txtMonto_VueltoTotal.ClientID%>');
                                var txtFaltante = document.getElementById('<%=txtMonto_Saldo.ClientID%>');
                                var txtMonto = document.getElementById('<%=txtMonto.ClientID%>');
                                var txtEfectivo = document.getElementById('<%=txtEfectivo.ClientID%>');

                                var acumulado = 0;
                                var monto = 0;
                                var faltante = 0;
                                var vuelto = 0;
                                if (grilla != null) {

                                
                                     monto = parseFloat(txtMonto.value);

                                    for (var i = 1; i < grilla.rows.length; i++) {
                                        var rowElem = grilla.rows[i];
                                        acumulado += parseFloat(rowElem.cells[2].innerHTML);
                                    }

                                   faltante = monto - acumulado;
                                    if (faltante < 0) {
                                        faltante = 0;
                                    }

                                    vuelto = acumulado - monto;
                                    if (vuelto < 0) {
                                        vuelto = 0;
                                    }
                                }

                                //***************** Imprimimos los valores
                                txtTotalRecibido.value = redondear(acumulado, 2);
                                txtFaltante.value = redondear(faltante, 2);
                                txtVuelto.value = redondear(vuelto, 2);
                                txtEfectivo.value = 0;


                                //****************** Valores para Vuelto
                           /*     grilla = document.getElementById('<%=DGV_Vuelto.ClientID%>');

                                acumulado = 0;
                                
                                if (grilla != null) {
                                
                                    for (var i = 1; i < grilla.rows.length; i++) {
                                        var rowElem = grilla.rows[i];
                                        acumulado += parseFloat(rowElem.cells[2].innerHTML);
                                    }
                                    
                                }
                                
                                
                                
                                
                                document.getElementById('<%=txtMonto_VueltoEfectivo.ClientID%>').value = 0;
                                document.getElementById('<%=txtVueltoTotal.ClientID%>').value = redondear(acumulado, 2);
                                
                                document.getElementById('<%=txtMontoFaltante_Vuelto.ClientID%>').value = redondear(vuelto-acumulado, 2);
                                */
                                return false;
                                
                            }



                            function validarAddMedioPago() {
                                
                                var IdTipoDocumento = parseInt(document.getElementById('<%=cboTipoDocumento.ClientID%>').value);
                                if (IdTipoDocumento == 14) {  //*********** Cotizaci�n
                                    alert('El Tipo de Documento Cotizaci�n no acepta Datos de Cancelaci�n.');
                                    return false;
                                }

                                //************ validamos la condicion de pago
                                var cboCondicionPago = document.getElementById('<%=cboCondicionPago.ClientID%>');
                                if (parseInt(cboCondicionPago.value) == 2) {
                                    alert('No puede registrarse datos de cancelaci�n porque la condici�n de Pago es al Cr�dito.');
                                    return false;
                                }
                            
                                var grilla = document.getElementById('<%=DGV_Cancelacion.ClientID%>');

                                var IdMoneda =parseFloat( document.getElementById('<%=cmbMonedaCancelacion_Efectivo.ClientID%>').value);
                                var IdMedioPago =parseFloat( document.getElementById('<%=cboMedioPago.ClientID%>').value);

                                var caja = document.getElementById('<%=txtEfectivo.ClientID%>');
                                if (parseFloat(caja.value)<=0 || caja.value.length <= 0 || isNaN( caja.value) ) {
                                    alert('El monto debe ser mayor a cero y/o Ingrese un valor v�lido.');
                                    caja.select();
                                    caja.focus();
                                    return false;
                                }
                                
                                
                                
                             /*   if (grilla != null) {
                                    for (var i = 1; i < grilla.rows.length; i++) {
                                        var rowElem = grilla.rows[i];

                                        if ((IdMedioPago == parseFloat(rowElem.cells[4].innerHTML)) && (IdMoneda == parseFloat(rowElem.cells[5].innerHTML))) {
                                            alert('El Medio de Pago / Moneda ya ha sido ingresado. S�lo se permite un Medio de Pago junto a un s�lo tipo de Moneda.');
                                            document.getElementById('<%=txtEfectivo.ClientID%>').select();
                                            document.getElementById('<%=txtEfectivo.ClientID%>').focus();
                                            return false;
                                        }                                        
                                    }
                                }
                                */
                                
                                return true;
                            }




                            function validarAddVuelto() {
                                var IdTipoDocumento = parseInt(document.getElementById('<%=cboTipoDocumento.ClientID%>').value);
                                if (IdTipoDocumento == 14) {  //*********** Cotizaci�n
                                    alert('El Tipo de Documento Cotizaci�n no acepta Datos de Cancelaci�n.');
                                    return false;
                                }
                                var grilla = document.getElementById('<%=DGV_Cancelacion.ClientID%>');
                                if (grilla == null) {
                                    alert('No puede registrar la entrega de vuelto ya que no se ha ingresado ning�n dato de cancelaci�n.');
                                    return false;
                                }
                            
                            
                                 grilla = document.getElementById('<%=DGV_Vuelto.ClientID%>');

                                var IdMoneda = parseFloat(document.getElementById('<%=cmbMoneda_Vuelto.ClientID%>').value);
                                var IdMedioPago = 1;

                                var caja = document.getElementById('<%=txtMonto_VueltoEfectivo.ClientID%>');
                                if (parseFloat(caja.value)<=0 || caja.value.length <= 0) {
                                    alert('El monto debe ser mayor a cero.');
                                    caja.select();
                                    caja.focus();
                                    return false;
                                }


                                if (grilla != null) {
                                    for (var i = 1; i < grilla.rows.length; i++) {
                                        var rowElem = grilla.rows[i];

                                        if ((IdMedioPago == parseFloat(rowElem.cells[4].innerHTML)) && (IdMoneda == parseFloat(rowElem.cells[5].innerHTML))) {
                                            alert('El Medio de Pago / Moneda ya ha sido ingresado. S�lo se permite un Medio de Pago junto a un s�lo tipo de Moneda.');
                                            document.getElementById('<%=txtMonto_VueltoEfectivo.ClientID%>').select();
                                            document.getElementById('<%=txtMonto_VueltoEfectivo.ClientID%>').focus();
                                            return false;
                                        }
                                    }
                                }
                                return true;
                            }


                            function validarMostrarCapaImpresion(Opcion) {
                                //******* Opcion
                                //*** 0: Imprimir
                                //*** 1: Despachar
                                //*** 2: Guia Remisi�n
                                //*** 3: Comprobante de Percepci�n
                                
                                var grilla = document.getElementById('<%=DGV_ImpresionDoc_Cab.ClientID%>');
                                if (grilla == null) {
                                    alert('No se encuentra ning�n documento disponible de venta.');
                                } else if (grilla.rows.length==2) {  //******** SOLO existe un documento.

                                var IdDocumento = grilla.rows[1].cells[1].innerHTML;
                                var IdTipoDocumento = parseInt(document.getElementById('<%=cboTipoDocumento.ClientID %>').value);
                                var IdEstadoDocumento = parseInt(document.getElementById('<%=cboEstadoDocumento.ClientID%>').value);
                                
                                    switch (Opcion) {
                                        case '0':  //************* IMPRIMIR
                                            
                                            switch (parseFloat(IdTipoDocumento)) {
                                                case 1: //factura
                                                    window.open('../../Reportes/visor.aspx?iReporte=2&IdDoc=' + IdDocumento, 'Factura', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                                                    break;
                                                case 3: //Boleta
                                                    window.open('../../Reportes/visor.aspx?iReporte=6&IdDoc=' + IdDocumento, 'Boleta', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                                                    break;
                                                case 14: //cotizacion
                                                    window.open('../../Ventas/Reportes/visorVentas.aspx?iReporte=2&IdDocumento=' + IdDocumento, 'Cotizacion', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                                                    break;
                                            }
                                            break;
                                        case '1':  //*********** Despachar                                            
                                            
                                            if (IdTipoDocumento == 14) {  //********** Cotizaci�n
                                                alert('No puede generar una Orden de Despacho para una Cotizaci�n.');
                                                return false;
                                            }                                            
                                            if (IdEstadoDocumento == 2) {  //********** ANULADO
                                                alert('El Documento est� ANULADO.');
                                                return false;
                                            }
                                            window.open('FrmOrdenDespacho.aspx?IdDocumento=' + IdDocumento, 'Despacho', 'resizable=yes,width=1000,height=800,scrollbars=1', null);                                            
                                            break;
                                        case '2':  //*********** GUIA DE REMISION
                                            if (IdTipoDocumento == 14) {  //********** Cotizaci�n
                                                alert('No puede generar una Gu�a de Remisi�n para una Cotizaci�n.');
                                                return false;
                                            }
                                            if (IdEstadoDocumento == 2) {  //********** ANULADO
                                                alert('El Documento est� ANULADO.');
                                                return false;
                                            }
                                            window.open('FrmGuiaRemision_Load.aspx?IdDocumento=' + IdDocumento, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                                            break;
                                        case '3':
                                            if (IdTipoDocumento == 14) {  //***** Cotizaci�n
                                                alert('No puede generar un Comprobante de Percepci�n para una Cotizaci�n.');
                                                return false;
                                            }
                                            if (IdEstadoDocumento == 2) {  //********** ANULADO
                                                alert('El Documento est� ANULADO.');
                                                return false;
                                            }
                                            window.open('FrmComprobantePercepcion.aspx?IdDocumento=' + IdDocumento, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                                            break;
                                    }                                    
                                } else {
                                    onCapa('capaImpresionDocumento');
                                }
                                return false;
                            }
                            function validarCheckImprimirDoc(objCheck) {
                                var grilla = document.getElementById('<%=DGV_ImpresionDoc_Cab.ClientID %>');
                                if (grilla != null) {
                                    for (var i = 1; i < grilla.rows.length; i++) {
                                        var rowElem = grilla.rows[i];

                                        if (rowElem.cells[7].children[0].id != objCheck.id) {
                                            rowElem.cells[7].children[0].status = false;
                                        }
                                    }
                                }
                                return true;
                            }

                        

                            function valMostrarVistaPrevia() {
                                //*************** Validamos los productos
                                var grilla = document.getElementById('<%=gvDetalle.ClientID %>');
                                if (grilla == null) {
                                    alert('No se ingresaron productos.');
                                    return false;
                                } else {
                                    //************* Validamos las cantidades
                                    for (var i = 1; i < grilla.rows.length; i++) {
                                        var rowElem = grilla.rows[i];

                                        var cantidad = redondear(parseFloat(rowElem.cells[2].children[0].cells[0].children[0].value), 2);
                                        if (isNaN(cantidad)) {
                                            cantidad = 0;
                                        }

                                        if (cantidad == 0) {
                                            alert(' Ingrese una cantidad mayor a cero para el producto ' + rowElem.cells[5].innerHTML + '.');
                                            rowElem.cells[2].children[0].cells[0].children[0].select();
                                            rowElem.cells[2].children[0].cells[0].children[0].focus();
                                            return false;
                                        }

                                    }
                                }

                                //****************** Validamos el cliente
                                var txtCodigoCliente = document.getElementById('<%=txtCodigoCliente.ClientID %>');
                                if (CajaEnBlanco(txtCodigoCliente)) {
                                    alert('Ingrese un cliente.');
                                    return false;
                                }

                                //***************** Si la venta es al cr�dito

                                grilla = document.getElementById('<%=gvLineaCredito.ClientID %>');
                                var IdMoneda = parseInt(document.getElementById('<%=cboMoneda.ClientID %>').value);
                                var IdCondicionPago = parseInt(document.getElementById('<%=cboCondicionPago.ClientID %>').value);
                                var totalAPagar = parseFloat(document.getElementById('<%=txtTotalAPagar.ClientID %>').value);
                                if (grilla == null && IdCondicionPago == 2) {
                                    alert('El cliente no posee L�neas de Cr�dito.');
                                    return false;
                                }

                                if (grilla != null && IdCondicionPago == 2) {

                                    var hddIdCuentaPersona = document.getElementById('<%=hddIdCuentaPersona.ClientID %>');

                                    for (var i = 1; i < grilla.rows.length; i++) {
                                        var rowElem = grilla.rows[i];
                                        if (parseInt(rowElem.cells[4].innerHTML) == IdMoneda) {

                                            //**************** Validamos que tenga saldo disponible

                                            if (parseFloat(rowElem.cells[3].innerHTML) < totalAPagar) {
                                                alert('No tiene saldo disponible en su l�nea de Cr�dito. [ Saldo Disponible : ' + rowElem.cells[1].innerHTML + ' ' + rowElem.cells[3].innerHTML + ' ]');
                                                return false;
                                            }

                                            hddIdCuentaPersona.value = rowElem.cells[0].innerHTML;
                                            break;
                                        }
                                    }
                                }





                                //********************** Validamos los MONTOS MAXIMOS   

                                var montoMax_Afecto = parseFloat(document.getElementById('<%=hddMontoMaximoAfecto.ClientID %>').value);
                                var montoMax_NoAfecto = parseFloat(document.getElementById('<%=hddMontoMaximoNOAfecto.ClientID %>').value);
                                var nomMoneda = getCampoxValorCombo(document.getElementById('<%=cboMoneda.ClientID %>'), document.getElementById('<%=cboMoneda.ClientID %>').value);
                                var separarxMontos = document.getElementById('<%=chb_SepararxMontos.ClientID %>').status;

                                //************************ Si se desea separar los montos y es al contado
                                if ((separarxMontos == true) && (IdCondicionPago == 1)) {
                                    grilla = document.getElementById('<%=gvDetalle.ClientID %>');
                                    if (grilla != null) {
                                        for (var i = 1; i < grilla.rows.length; i++) {
                                            var rowElem = grilla.rows[i];
                                            //********************************** Si es AFECTO
                                            if ((parseFloat(rowElem.cells[10].children[0].value) >= montoMax_Afecto) && (parseFloat(rowElem.cells[11].innerHTML) > 0)) {
                                                alert('El Importe para el Producto ' + rowElem.cells[5].innerHTML + ' ha superado el monto m�ximo asignado para la Separaci�n de Documentos AFECTOS [ Monto M�ximo : ' + nomMoneda + ' ' + montoMax_Afecto + ' ]. Si desea continuar con la operaci�n deshabilite la opci�n [ Separar por Montos ] en la ventana de Configuraci�n del Formulario, caso contrario registre varios detalles del producto seleccionado los cuales no sobrepasen el Monto M�ximo Asignado.');
                                                return false;
                                            }
                                            //********************************** Si es NO AFECTO
                                            if ((parseFloat(rowElem.cells[10].children[0].value) >= montoMax_NoAfecto) && (parseFloat(rowElem.cells[11].innerHTML) == 0)) {
                                                alert('El Importe para el Producto ' + rowElem.cells[5].innerHTML + ' ha superado el monto m�ximo asignado para la Separaci�n de Documentos NO AFECTOS [ Monto M�ximo : ' + nomMoneda + ' ' + montoMax_NoAfecto + ' ]. Si desea continuar con la operaci�n deshabilite la opci�n [ Separar por Montos ] en la ventana de Configuraci�n del Formulario, caso contrario registre varios detalles del producto seleccionado los cuales no sobrepasen el Monto M�ximo Asignado.');
                                                return false;
                                            }
                                        }
                                    }
                                }
                                return true;
                            }
                            
                            function mostrarCapaVistaPrevia() {
                                onCapa('capaVistaPreviaDocumento');
                                
                                var grilla = document.getElementById('<%=DGV_VistaPreviaDoc.ClientID%>');
                                var total = 0;
                                var percepcion = 0;
                                if (grilla != null) {
                                    for (var i = 1; i < grilla.rows.length; i++) {
                                        var rowElem = grilla.rows[i];

                                        total = total + parseFloat(rowElem.cells[5].innerHTML);
                                        percepcion = percepcion + parseFloat(rowElem.cells[4].innerHTML);
                                        
                                    }
                                }
                                document.getElementById('<%=txt_TotalAPagar_VistaPrevia.ClientID%>').value = redondear(total, 2);
                                document.getElementById('<%=txt_Percepcion_VistaPrevia.ClientID%>').value = redondear(percepcion, 2);

                                //***************** Pintamos los valores en el formulario                             
                                document.getElementById('<%=txtTotalAPagar.ClientID%>').value = redondear(total, 2);
                                document.getElementById('<%=txtMonto.ClientID%>').value = redondear(total, 2);
                                document.getElementById('<%=txtPercepcion.ClientID%>').value = redondear(percepcion, 2);


                                //*********************** Hallamos el vuelto
                                calcularMontosCancelacion();

                                valorRecalculado = true;
                            }

                            function valBuscarClientexId() {
                               /* var grilla = document.getElementById('<%=gvDetalle.ClientID%>');
                                if (grilla == null) {
                                    alert('Debe a�adir primero productos.');
                                    return false;
                            }*/
                                var caja = document.getElementById('<%=txtIdCliente_BuscarxId.ClientID%>');
                                if (CajaEnBlanco(caja)) {
                                    alert('Ingrese un C�digo de Cliente.');
                                    caja.select();
                                    caja.focus();
                                    return false;
                                }
                                return true;
                            }




                            function valSaveDocumento() {

                                //** VALIDAMOS CAMPOS REQUERIDOS
                                var cboEmpresa = document.getElementById('<%=cboPropietario.ClientID %>');
                                if (isNaN(parseInt(cboEmpresa.value)) || cboEmpresa.value.length <= 0 || parseInt(cboEmpresa.value) <= 0) {
                                    alert('Debe seleccionar una Empresa.');
                                    return false;
                                }
                                var cboTienda = document.getElementById('<%=cboTienda.ClientID %>');
                                if (isNaN(parseInt(cboTienda.value)) || cboTienda.value.length <= 0 || parseInt(cboTienda.value) <= 0) {
                                    alert('Debe seleccionar una Tienda.');
                                    return false;
                                }
                                var cboCaja = document.getElementById('<%=cboCaja.ClientID %>');
                                if (isNaN(parseInt(cboCaja.value)) || cboCaja.value.length <= 0 || parseInt(cboCaja.value) <= 0) {
                                    alert('Debe seleccionar una Caja.');
                                    return false;
                                }
                                var cboSerie = document.getElementById('<%=cboSerie.ClientID %>');
                                if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                                    alert('Debe seleccionar una Serie.');
                                    return false;
                                }
                            

                                //***** Obtenemos el Nombre del Documento.
                                var nomTipoDocumento = getCampoxValorCombo(document.getElementById('<%=cboTipoDocumento.ClientID %>'), document.getElementById('<%=cboTipoDocumento.ClientID %>').value    );                                
                                
                                
                                //*************** Validamos los productos                                
                                
                                var grilla = document.getElementById('<%=gvDetalle.ClientID %>');
                                if (grilla == null) {
                                    alert('No se ingresaron productos.');
                                    var boton = document.getElementById('<%=btnAgregar.ClientID %>');
                                    boton.click();
                                    return false;
                                } else {
                                    //************* Validamos las cantidades
                                    for (var i = 1; i < grilla.rows.length; i++) {
                                        var rowElem = grilla.rows[i];

                                        var cantidad = redondear(parseFloat(rowElem.cells[2].children[0].cells[0].children[0].value), 2);
                                        if (isNaN(cantidad)) {
                                            cantidad = 0;
                                        }

                                        if (cantidad == 0) {
                                            alert(' Ingrese una cantidad mayor a cero para el producto ' + rowElem.cells[5].innerHTML + '.');
                                            rowElem.cells[2].children[0].cells[0].children[0].select();
                                            rowElem.cells[2].children[0].cells[0].children[0].focus();
                                            return false;
                                        }

                                    }
                                }

                                //****************** Validamos el cliente
                                var txtCodigoCliente = document.getElementById('<%=txtCodigoCliente.ClientID %>');
                                if (CajaEnBlanco(txtCodigoCliente)) {
                                    alert('Ingrese un cliente.');
                                    var boton = document.getElementById('<%=btnBuscarCliente.ClientID %>');
                                    boton.click();
                                    return false;
                                }

                                //********************************* SI ES COTIZACION NO CONTINUAMOS CON LAS DEMAS VALIDACIONES
                                var IdTipoDocumento = parseInt(document.getElementById('<%=cboTipoDocumento.ClientID %>').value);
                                if (IdTipoDocumento == 14) {
                                    return (confirm('Desea registrar la Cotizaci�n?'));
                                }
                                

                                //***************** Si la venta es al cr�dito
                                grilla = document.getElementById('<%=gvLineaCredito.ClientID %>');
                                var IdMoneda = parseInt(document.getElementById('<%=cboMoneda.ClientID %>').value);
                                var IdCondicionPago = parseInt(document.getElementById('<%=cboCondicionPago.ClientID %>').value);
                                var totalAPagar = parseFloat(document.getElementById('<%=txtTotalAPagar.ClientID %>').value);
                                if (grilla == null && IdCondicionPago == 2) {
                                    alert('El cliente no posee L�neas de Cr�dito.');
                                    return false;
                                }

                                if (grilla != null && IdCondicionPago == 2) {

                                    var hddIdCuentaPersona = document.getElementById('<%=hddIdCuentaPersona.ClientID %>');

                                    for (var i = 1; i < grilla.rows.length; i++) {
                                        var rowElem = grilla.rows[i];
                                        if (parseInt(rowElem.cells[4].innerHTML) == IdMoneda) {

                                            //**************** Validamos que tenga saldo disponible

                                            if (parseFloat(rowElem.cells[3].innerHTML) < totalAPagar) {
                                                alert('No tiene saldo disponible en su l�nea de Cr�dito. [ Saldo Disponible : ' + rowElem.cells[1].innerHTML + ' ' + rowElem.cells[3].innerHTML + ' ]');
                                                return false;
                                            }

                                            hddIdCuentaPersona.value = rowElem.cells[0].innerHTML;
                                            break;
                                        }
                                    }
                                }





                                //********************** Validamos los MONTOS MAXIMOS   

                                var montoMax_Afecto = parseFloat(document.getElementById('<%=hddMontoMaximoAfecto.ClientID %>').value);
                                var montoMax_NoAfecto = parseFloat(document.getElementById('<%=hddMontoMaximoNOAfecto.ClientID %>').value);
                                var nomMoneda = getCampoxValorCombo(document.getElementById('<%=cboMoneda.ClientID %>'), document.getElementById('<%=cboMoneda.ClientID %>').value);
                                var separarxMontos = document.getElementById('<%=chb_SepararxMontos.ClientID %>').status;

                                //************************ Si se desea separar los montos y es al contado
                                if ((separarxMontos == true) && (IdCondicionPago == 1)) {
                                    grilla = document.getElementById('<%=gvDetalle.ClientID %>');
                                    if (grilla != null) {
                                        for (var i = 1; i < grilla.rows.length; i++) {
                                            var rowElem = grilla.rows[i];
                                            //********************************** Si es AFECTO
                                            if ((parseFloat(rowElem.cells[10].children[0].value) >= montoMax_Afecto) && (parseFloat(rowElem.cells[11].innerHTML) > 0)) {
                                                alert('El Importe para el Producto ' + rowElem.cells[5].innerHTML + ' ha superado el monto m�ximo asignado para la Separaci�n de Documentos AFECTOS [ Monto M�ximo : ' + nomMoneda + ' ' + montoMax_Afecto + ' ]. Si desea continuar con la operaci�n deshabilite la opci�n [ Separar por Montos ] en la ventana de Configuraci�n del Formulario, caso contrario registre varios detalles del producto seleccionado los cuales no sobrepasen el Monto M�ximo Asignado.');
                                                return false;
                                            }
                                            //********************************** Si es NO AFECTO
                                            if ((parseFloat(rowElem.cells[10].children[0].value) >= montoMax_NoAfecto) && (parseFloat(rowElem.cells[11].innerHTML) == 0)) {
                                                alert('El Importe para el Producto ' + rowElem.cells[5].innerHTML + ' ha superado el monto m�ximo asignado para la Separaci�n de Documentos NO AFECTOS [ Monto M�ximo : ' + nomMoneda + ' ' + montoMax_NoAfecto + ' ]. Si desea continuar con la operaci�n deshabilite la opci�n [ Separar por Montos ] en la ventana de Configuraci�n del Formulario, caso contrario registre varios detalles del producto seleccionado los cuales no sobrepasen el Monto M�ximo Asignado.');
                                                return false;
                                            }
                                        }
                                    }
                                }


                                
                                //******************* Validamos la CANCELACION
                                grilla = document.getElementById('<%=DGV_Cancelacion.ClientID %>');
                                var hddValAddDatoCancelacion = document.getElementById('<%=hddValAddDatoCancelacion.ClientID %>');
                                var cboTipoAgente = document.getElementById('<%=cboTipoAgente.ClientID %>');
                                if (grilla == null && IdCondicionPago == 1 && parseInt(hddValAddDatoCancelacion.value) > 0 && parseInt(cboTipoAgente.value)==2 ) {
                                    alert('El Cliente es Agente de Retenci�n. Ingrese al menos un dato de cancelaci�n.');
                                    return false;
                                    /*if (!(confirm('No se han ingresado Datos de Cancelaci�n. El Sistema considerar� que el pago es en efectivo y por el monto similar al Total a Pagar, de ser el cliente Agente RETENEDOR el sistema considerar� el descuento respectivo. Desea Continuar?'))) {
                                        return false;
                                    }*/
                                }
                                
                                grilla = document.getElementById('<%=DGV_Cancelacion.ClientID %>');
                                if (grilla != null && IdCondicionPago == 1) {
                                    var vuelto = parseFloat(document.getElementById('<%=txtMonto_VueltoTotal.ClientID%>').value);
                                    for (var i = 1; i < grilla.rows.length; i++) {
                                        var rowElem = grilla.rows[i];
                                        //********************************** SI EL MEDIO DE PAGO NO ES EN EFECTIVO, NO DEBE EXISTIR VUELTO

                                        if (parseInt(rowElem.cells[4].innerHTML) != 1 && vuelto > 0) {
                                            alert('Se han ingresado Medios de Pago que NO SON EFECTIVO, NO DEBE EXISTIR VUELTO.');
                                            return false;
                                        }
                                        if (parseFloat(rowElem.cells[2].innerHTML) <= 0) {
                                            alert('No puede a�adirse una cancelaci�n con monto CERO. Quite el dato de cancelaci�n.');
                                            return false;
                                        }
                                    }
                                }

                                //************************* Debemos Validar el total a PAGAR
                                var totalAPagar = parseFloat(document.getElementById('<%=txtTotalAPagar.ClientID %>').value);
                                var totalRecibido = parseFloat(document.getElementById('<%=txtMonto_Acumulado.ClientID %>').value);


                                var chb_SepararAfecto_NoAfecto = document.getElementById('<%=chb_SepararAfecto_NoAfecto.ClientID%>');
                                var chb_SaltarCorrelativo = document.getElementById('<%=chb_SaltarCorrelativo.ClientID%>');
                                var chb_SepararxMontos = document.getElementById('<%=chb_SepararxMontos.ClientID%>');
                                var chb_ComPercepcion = document.getElementById('<%=chb_CompPercepcion.ClientID%>');

                                if (chb_SepararAfecto_NoAfecto.status || chb_SaltarCorrelativo.status || chb_SepararxMontos.status || chb_ComPercepcion.status) {
                                    if (!valorRecalculado) {
                                        alert('La Configuraci�n del Formulario requiere que se Recalculen los montos. De click en el bot�n [ RECALCULAR ].');
                                        return false;
                                    }    
                                }
                                

/*
                                //******** Si es al contado
                                if ((IdCondicionPago == 1) && (totalRecibido < totalAPagar)) {
                                    if (!(confirm('El Total a Pagar excede el Total Recibido. Desea continuar?'))) {
                                        return false;    
                                    }
                                }
                                */
                                //********** Si es a CREDITO
//                                if (IdCondicionPago == 2) {
//                                    var cajaFecVcto = document.getElementById('<%=txtFechaVencimiento.ClientID %>');
//                                    if(!valFecha(caja)){                                        
//                                        return false;
//                                    }
//                                }

                                //************* Validamos la existencia de vuelto faltante x entregar
                   /*             var cajaVuelto = document.getElementById('<%=txtMontoFaltante_Vuelto.ClientID %>');
                                var grillaVuelto = document.getElementById('<%=DGV_Vuelto.ClientID %>');
                                if (grillaVuelto != null) {
                                    //********* Existe datos de vuelto
                                    if (parseFloat(cajaVuelto.value) > 0) {
                                        if (!confirm('Se han agregado datos de vuelto y existe un vuelto por entregar pendiente. El Sistema no considerar� el vuelto por entregar pendiente. Desea continuar de todos modos con la operaci�n?')) { return false; }
                                    }
                                    if (parseFloat(cajaVuelto.value) < 0) {
                                        if (!confirm('Se han agregado datos de vuelto. El Sistema ha detectado que se est� registrando un vuelto mayor al calculado. Desea continuar de todos modos con la operaci�n?')) { return false; }
                                    }
                                }*/

                                if (IdTipoDocumento == 14) {  //********** Cotizaci�n
                                    grillaVuelto = document.getElementById('<%=DGV_Vuelto.ClientID %>');
                                    var grillaCancelacion = document.getElementById('<%=DGV_Cancelacion.ClientID %>');
                                    if (grillaCancelacion != null || grillaVuelto != null) {
                                        alert('El Tipo de Documento COTIZACI�N no acepta Datos de Cancelaci�n. Quite los datos de cancelaci�n para continuar con la operaci�n.');
                                        return false;
                                    }
                                    return false;
                                }


                                //****** Validamos la existencia de RUC en caso de FActura
                                var txtRUC = document.getElementById('<%=txtRuc.ClientID %>');
                                if (IdTipoDocumento == 1  && (   (isNaN(txtRUC.value)) || (   txtRUC.value.length<=0 )                )) {
                                    alert('La Factura requiere que el cliente seleccionado posea R.U.C.');
                                    return false;
                                }

                                 grilla = document.getElementById('<%=DGV_Cancelacion.ClientID %>');
                                 if (IdCondicionPago == 1 && grilla == null) {  //****** CONTADO && No existe Datos de Cancelaci�n
                                     return (confirm('La Cancelaci�n es por un monto de ' + document.getElementById('<%=lblMonedaCancelacion_TotalAPagar.ClientID %>').innerHTML + ' ' + document.getElementById('<%=txtMonto.ClientID %>').value + ' en EFECTIVO. Tipo de Documento: ' + nomTipoDocumento.toUpperCase()  + '. Desea continuar con la operaci�n?.'));
                                 } else if (IdCondicionPago == 1 && (totalRecibido < totalAPagar) &&  grilla!=null ) {  //*** Contado && existe datos de cancelaci�n
                                    //return (confirm('El Total a Pagar excede el Total Recibido. Tipo de Documento: ' + nomTipoDocumento.toUpperCase() + ' . Desea continuar con la Operaci�n ?'));
                                    alert('LA CANTIDAD TOTAL RECIBIDA ES MENOR AL TOTAL DEL DOCUMENTO DE VENTA. NO SE PERMITE LA OPERACI�N.');
                                    return false;                                 
                                 } else {
                                 return (confirm('Tipo de Documento: ' + nomTipoDocumento.toUpperCase() + '. Desea continuar con la operaci�n?.'));
                                }                         
                            }


                            function mostrarCapaImpresionDocumento_SAVED() {
                                onCapa('capaImpresionDocumento');
                                var grilla = document.getElementById('<%=DGV_ImpresionDoc_Cab.ClientID%>');
                                var total = 0;
                                var percepcion = 0;
                                if (grilla != null) {
                                    for (var i = 1; i < grilla.rows.length; i++) {
                                        var rowElem = grilla.rows[i];

                                        total = total + parseFloat(rowElem.cells[6].innerHTML);
                                        percepcion = percepcion + parseFloat(rowElem.cells[5].innerHTML);

                                    }
                                }
                                document.getElementById('<%=txtTotalAPagar_DocSAVED.ClientID%>').value = redondear(total, 2);
                                document.getElementById('<%=txtPercepcion_DocSAVED.ClientID%>').value = redondear(percepcion, 2);                            
                            }
                            function valNavegacionPersona(tipoMov) {
                                var index = parseInt(document.getElementById('<%=txtPageIndex_Persona.ClientID%>').value);
                                if (isNaN(index) || index == null || index.length==0 || index<=0) {
                                    alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                                    document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').select();
                                    document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').focus();
                                    return false;
                                }
                                switch (tipoMov) {
                                    case '0':   //************ anterior
                                        if (index <= 1) {
                                            alert('No existen p�ginas con �ndice menor a uno.');
                                            return false;
                                        }
                                        break;
                                    case '1':
                                        break;
                                    case '2': //************ ir
                                        index = parseInt(document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').value);
                                        if (isNaN(index) || index == null || index.length == 0) {
                                            alert('Ingrese una P�gina de navegaci�n.');
                                            document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').select();
                                            document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').focus();
                                            return false;
                                        }
                                        if (index <1) {
                                            alert('No existen p�ginas con �ndice menor a uno.');
                                            document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').select();
                                            document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').focus();
                                            return false;
                                        }
                                        break;
                                }
                                return true;
                            }


                            function valNavegacionProductos(tipoMov) {
                                var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
                                var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
                                if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla==null) {
                                    alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                                    document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                                    document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                                    return false;
                                }
                                switch (tipoMov) {
                                    case '0':   //************ anterior
                                        if (index <= 1) {
                                            alert('No existen p�ginas con �ndice menor a uno.');
                                            return false;
                                        }
                                        break;
                                    case '1':
                                        break;
                                    case '2': //************ ir
                                        index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                                        if (isNaN(index) || index == null || index.length == 0 ) {
                                            alert('Ingrese una P�gina de navegaci�n.');
                                            document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                                            document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                                            return false;
                                        }
                                        if (index < 1) {
                                            alert('No existen p�ginas con �ndice menor a uno.');
                                            document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                                            document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                                            return false;
                                        }
                                        break;
                                }
                                return true;
                            }
                            function limpiarBuscarProducto() {
                                //************* limpiamos los controles
                                //    var cboLinea = document.getElementById('');

                                var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
                                var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
                                var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
                                var txtCodigoSL = document.getElementById('<%=txtCodigoSubLinea_AddProd.ClientID%>');
                                var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');                              
                                

                                //************** Limpiamos la caja de la descripcion
                                txtDescripcion.value = '';
                                txtCodigoSL.value = '';

                                //*********** Limpiamos la linea
                                cboLinea.selectedIndex = 0;

                                //********** Eliminamos todos los items menos el primero
                                for (var i = (cboSubLinea.length-1); i > 0; i--) {
                                    cboSubLinea[i] = null;
                                }

                                txtCodigoSL.select();
                                txtCodigoSL.focus();
                                
                                return false;
                            }
                            function valBuscarProdxID() {

                                var caja = document.getElementById('<%=txtBuscarProdxId.ClientID%>');

                                if (caja.value.length == 0) {
                                    alert('Ingrese un valor');
                                    caja.select();
                                    caja.focus();
                                    return false;
                                }
                                if (isNaN(caja.value)) {
                                    alert('Valor no v�lido.');
                                    caja.select();
                                    caja.focus();
                                    return false;
                                }
                                
                                return true;
                            }
                            function validarIdProducto() {
                                var key = event.keyCode;
                                if (key == 13) {
                                    document.getElementById('<%=btnAceptarBuscarProdxID.ClientID %>').focus();
                                }else{
                                    if (!onKeyPressEsNumero()) {
                                        return false;
                                    }
                                }
                            }
                            function showCapaBuscarProdxId() {
                                onCapa('capaBuscarProdxID');
                                document.getElementById('<%=txtBuscarProdxId.ClientID %>').value = '';
                                document.getElementById('<%=txtBuscarProdxId.ClientID %>').select();
                                document.getElementById('<%=txtBuscarProdxId.ClientID %>').focus();
                                return false;
                            }
                            function onKeyPressValidarDecimal(opcion) {
                                var key = event.keyCode;
                                   var button = null; 
                                    switch( opcion ){
                                        case '1':  //**************** Medio de Cancelaci�n
                                            button = document.getElementById('<%=btnAdd_MedioCancelacion.ClientID%>');                                            
                                            break;
                                    }
                                   
                                   
                                   if (key == 13 && button!=null) {
                                       button.focus();
                                   } else {

                                            if (key == 46) {
                                                return true;
                                            } else {
                                            if (!onKeyPressEsNumero()) {
                                                return false;
                                                } 
                                            }
                                        
                                    }
                                return true;
                            }
                            function valEditar() {

                                var IdEstadoDocumento = parseInt(document.getElementById('<%=cboEstadoDocumento.ClientID%>').value);
                                if (IdEstadoDocumento == 2) {
                                    alert('Este Documento ha sido anulado.');
                                    return false;
                                }

                                //******************* Validamos la Ordenes de Despacho
                                var poseeOrdenDespacho =parseInt( document.getElementById('<%=hddPoseeOrdenDespacho.ClientID%>').value);
                                var poseeAmortizaciones = parseInt(document.getElementById('<%=hddPoseeAmortizaciones.ClientID%>').value);
                                var poseeCompPercepcion = parseInt(document.getElementById('<%=hddPoseeCompPercepcion.ClientID%>').value);

                                if (poseeOrdenDespacho == 1) {
                                    alert('El Documento posee �rdenes de Despacho asociados. No puede habilitarse su edici�n.');
                                    return false;
                                }
                                
                                if(poseeAmortizaciones==1) {
                                    alert('El Documento es a cr�dito y posee Amortizaciones registradas. No puede habilitarse su edici�n.');
                                    return false;
                                }

                                if (poseeCompPercepcion == 1) {
                                    alert('El Documento posee un Documento Relacionado [ Doc. Comprobante de Percepci�n ]. No puede habilitarse su edici�n.');
                                    return false;
                                }
                                
                                return true;
                            }
                            function valAnular() {
                                //******************* Validamos la Ordenes de Despacho

                                var IdEstadoDocumento = parseInt(document.getElementById('<%=cboEstadoDocumento.ClientID%>').value);
                                if (IdEstadoDocumento == 2) {
                                    alert('Este Documento ya ha sido anulado.');
                                    return false;
                                }
                                
                                
                                var poseeOrdenDespacho = parseInt(document.getElementById('<%=hddPoseeOrdenDespacho.ClientID%>').value);
                                var poseeAmortizaciones = parseInt(document.getElementById('<%=hddPoseeAmortizaciones.ClientID%>').value);
                                var poseeCompPercepcion = parseInt(document.getElementById('<%=hddPoseeCompPercepcion.ClientID%>').value);

                                if (poseeOrdenDespacho == 1) {
                                    if( ! confirm('El Documento posee �rdenes de Despacho asociados. Desea anular las �rdenes de Despacho asociadas reponiendo el Stock correspondiente ?') ){
                                        return false;
                                    }
                                }
                                
                                if (poseeAmortizaciones == 1) {
                                    alert('El Documento es a cr�dito y posee Amortizaciones registradas. No puede habilitarse su anulaci�n. Debe anular primero los documentos de Amortizaci�n de la Deuda.');
                                    return false;
                                }
                                if (poseeCompPercepcion == 1) {
                                    alert('El Documento posee un Documento Relacionado [ Doc. Comprobante de Percepci�n ]. No puede habilitarse su edici�n.');
                                    return false;
                                }
                                return (confirm('Desea continuar con la anulaci�n del Documento de venta ?'));
                            }


                            function valCanjearCotizacion() {
                                var IdEstadoDocumento = parseInt(document.getElementById('<%=cboEstadoDocumento.ClientID%>').value);
                                if (IdEstadoDocumento == 2) {  //********** ANULADO
                                    alert('El Documento est� ANULADO.');
                                    return false;
                                }
                                var IdTipoDocumento = parseInt(document.getElementById('<%=cboTipoDocumento.ClientID%>').value);
                                if (IdTipoDocumento != 14) {  //********** COTIZACION
                                    alert('S�lo se permite el canje del Tipo de Documento COTIZACI�N.');
                                    return false;
                                }                                                                                             
                                return true;
                            }

                            function LimpiarCajaFechaAEntregar() {
                                document.getElementById('<%=txtFechaAEntregar.ClientID %>').value = '';
                                return false;
                            }
                            function LimpiarFechaACobrar() {
                                document.getElementById('<%=txtFechaACobrar.ClientID %>').value = '';
                                return false;
                            }

                            function LimpiarFechaVcto() {
                                document.getElementById('<%=txtFechaVencimiento.ClientID %>').value = '';
                                return false;
                            }
                            function valStockDisponible_AddProd() {
                                var grilla = document.getElementById('<%=DGV_AddProd.ClientID %>');
                                if (grilla != null) {
                                    for (var i = 1; i < grilla.rows.length; i++) {
                                        var rowElem = grilla.rows[i];
                                        if (rowElem.cells[0].children[0].id == event.srcElement.id) {
                                            if (parseFloat(rowElem.cells[0].children[0].value) > parseFloat(rowElem.cells[7].innerHTML)) {
                                                alert('La cantidad ingresada excede a la cantidad disponible.');
                                                rowElem.cells[0].children[0].select();
                                                rowElem.cells[0].children[0].focus();
                                                return false;
                                            }
                                            return false;
                                        }
                                        
                                    }
                                }
                                return false;
                            }
                            function lostFocus() {
                                document.getElementById('<%=btnAgregar.ClientID%>').focus();

                                //document.forms.txtFocus.focus();
                                //document.getElementById('txtFocus').focus();
                                return false;
                            }




                            function imprimirDocumento_JS() {
                                var grilla = document.getElementById('<%= DGV_ImpresionDoc_Cab.ClientID%>');
                                var cbo = document.getElementById('<%=cboTipoDocumento.ClientID%>');
                                var hddIdDocumentoPrint = document.getElementById('<%=hddIdDocumentoPrint.ClientID%>');

                                var IdDocumento = 0;

                                if (grilla == null) {
                                    alert('No se tiene ning�n Documento para Impresi�n.');
                                    return false;
                                } else if (grilla.rows.length == 2) {  //**** SOLO EXISTE UN DOCUMENTO
                                    hddIdDocumentoPrint.value=parseInt(grilla.rows[1].cells[1].innerHTML);
                                    return true;
                                }

                                for (var i = 1; i < grilla.rows.length; i++) {
                                    var rowElem = grilla.rows[i];
                                    if (rowElem.cells[7].children[0].status == true) {
                                        IdDocumento = rowElem.cells[1].innerHTML;
                                        break;
                                    }
                                }

                                if (IdDocumento == 0) {
                                    alert('No se ha seleccionado ning�n documento para impresi�n.')
                                    return false;
                                }



                                switch (parseFloat(cbo.value)) {
                                    case 1: //factura
                                        hddIdDocumentoPrint.value = IdDocumento;
                                        break;
                                    case 3: //Boleta
                                        alert('La Impresi�n s�lo es para Tipo de Documento Factura.');
                                        return false;
                                        break;
                                    case 14: //cotizacion
                                        alert('La Impresi�n s�lo es para Tipo de Documento Factura.');
                                        return false;
                                        break;                                    
                                }
                                return true;
                            }
                            function valNuevo() {
                                //**** 5
                                var hddFrmModo = document.getElementById('<%=hddModoFrm.ClientID%>');
                                if (parseInt(hddFrmModo.value) == 5) {  //**** Documento Guardao correctamente
                                    return true;
                                }
                            
                                var grilla = document.getElementById('<%=gvDetalle.ClientID%>');
                                if (grilla != null) {  //******** Se han ingresado productos
                                    return (   confirm('Se han ingresado PRODUCTOS al detalle del Documento. Desea iniciar un Documento de Venta < NUEVO > ?') );
                                }
                                return true;
                            }
                            function valBuscar() {
                                var hddFrmModo = document.getElementById('<%=hddModoFrm.ClientID%>');
                                if (parseInt(hddFrmModo.value) == 5) {  //**** Documento Guardao correctamente
                                    return true;
                                }
                                
                                var grilla = document.getElementById('<%=gvDetalle.ClientID%>');
                                if (grilla != null) {  //******** Se han ingresado productos
                                    return (confirm('Se han ingresado PRODUCTOS al detalle del Documento. Desea iniciar la b�squeda de un Documento de Venta ( Se inicializar� nuevamente el Formulario ) ?'));
                                }
                                return true;
                            }
                            
                            
                            
                            //******** Funciones para validar Teclado [Enter]
                            function valKeyPressCodigoSL() {
                                if (event.keyCode == 13) { // Enter                                    
                                    document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
                                }else{
                                    return onKeyPressEsNumero();
                                }
                                return true;
                            }
                            function valKeyPressDescripcionProd() {
                                if (event.keyCode == 13) { // Enter                                    
                                    document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
                                }
                                return true;
                            }
                            function valKeyPressCantidadAddProd() {
                                if (event.keyCode == 13) {
                                    document.getElementById('<%=btnAddProductos_AddProd.ClientID%>').focus();
                                }else{
                                    return validarNumeroPuntoPositivo();
                                }
                                return true;
                            }
                            function valKeypressBuscarPersona() {
                                if (event.keyCode == 13) {
                                    document.getElementById('<%=btnBuscarPersona_Grilla.ClientID%>').focus();
                                }
                                return true;
                            }
                            function valKeyPressSaveCliente() {
                                if (event.keyCode == 13) {
                                    document.getElementById('<%=btnGuardarCliente.ClientID%>').focus();
                                }
                                return true;
                            }
                            
                            //para levantar las capas
                            function onCapaCliente() {
                                LimpiarControlesCliente();
                                onCapa('capaRegistrarCliente');
                                var opcion = document.getElementById('<%=cmbTipoPersona.ClientID %>').value;
                                if (opcion == 'N') {
                                    document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').disabled = false;
                                    document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').disabled = true;
                                    document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').focus();

                                } else if (opcion == 'J') {
                                    document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').disabled = true;
                                    document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').disabled = false;       
                                    document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').focus();   
                                }                                
                                return false;
                            }
                            function valKeyPressBuscarPersonaxId() {
                                if (event.keyCode == 13) {
                                    document.getElementById('<%=btnBuscarClientexIdCliente.ClientID%>').focus();
                                } else {
                                    return onKeyPressEsNumero();
                                }
                                return true;
                            }
                            function valKeyPressAddMedioPago() {
                                if (event.keyCode == 13) {
                                    document.getElementById('<%=btnAdd_MedioCancelacion.ClientID%>').focus();
                                }   
                                return true;
                            }
                            function valKeyPressBuscarDocumento() {
                                if (event.keyCode == 13) {
                                    document.getElementById('<%=btnBuscarDocumento.ClientID%>').focus();
                                } else {
                                    return onKeyPressEsNumero();
                                }
                                return true;
                            }


                            




                            //******* Detectando Tecla ESC
                            function detectar_tecla() {
                                with (event) {
                                    if (keyCode == 27) {  //**** SI presiono ESC                                        
                                        window.capaAddMagnitud.style.display = 'none';
                                        window.capaBase.style.display = 'none';

                                        window.capaBuscarProducto_AddProd.style.display = 'none';
                                        //*****  window.capaBuscarProdxID.style.display = 'none';
                                        window.capaConfiguracionFrm.style.display = 'none';
                                        window.capaImpresion.style.display = 'none';
                                        window.capaImpresionDocumento.style.display = 'none';
                                        window.CapaLongitud.style.display = 'none';

                                        window.capaPersona.style.display = 'none';
                                        window.capaRegistrarCliente.style.display = 'none';
                                        window.capaVistaPreviaDocumento.style.display = 'none';
                                        window.capaHelp.style.display = 'none';
                                        window.capaEditarPersona.style.display = 'none';
                                        window.capaBuscarProdxID.style.display = 'none';
                                        window.capaDocumentosReferencia.style.display = 'none';

                                        return false;
                                    }
                                    if (keyCode == 78 && ctrlKey && shiftKey) {  //**  Ctrl + Shift +  N
                                        var boton = document.getElementById('<%=btnNuevo.ClientID%>');
                                        if (boton != null) { boton.click(); }
                                    }
                                    if (keyCode == 71 && ctrlKey && shiftKey) {  //**  Ctrl + Shift + G
                                        var btnGuardar = document.getElementById('<%=btnGuardar.ClientID%>');
                                        if(btnGuardar != null){btnGuardar.click();}
                                    }
                                    if (keyCode == 66 && ctrlKey && shiftKey) {  //**  Ctrl + Shift + B
                                        var boton = document.getElementById('<%=btnBuscar.ClientID%>');
                                        if (boton != null) { boton.click(); }
                                    }
                                    if (keyCode == 84 && ctrlKey && shiftKey) {  //**  Ctrl + Shift + T
                                        var boton = document.getElementById('<%=btnImprimir.ClientID%>');
                                        if (boton != null) { boton.click(); }
                                    }
                                    if (keyCode == 83 && ctrlKey && shiftKey) {  //**  Ctrl + Shift + S
                                        var boton = document.getElementById('<%=btnDespachar.ClientID%>');
                                        if (boton != null) { boton.click(); }
                                    }
                                    if (keyCode == 82 && ctrlKey && shiftKey) {  //**  Ctrl + Shift + R
                                        var boton = document.getElementById('<%=LinkButton1.ClientID%>');
                                        if (boton != null) { boton.click(); }
                                    }
                                    if (keyCode == 67 && ctrlKey && shiftKey) {  //**  Ctrl + Shift + C
                                        var boton = document.getElementById('<%=btnCanjearCotizacion.ClientID%>');
                                        if (boton != null) { boton.click(); }
                                    }
                                    if (keyCode == 87 && ctrlKey && shiftKey) {  //**  Ctrl + Shift + W
                                        var boton = document.getElementById('<%=btnBuscarDocumento.ClientID%>');
                                        if (boton != null) { boton.click(); }
                                    }
                                    if (keyCode == 69 && ctrlKey && shiftKey) {  //**  Ctrl + Shift + E
                                        var boton = document.getElementById('<%=btnEditar.ClientID%>');
                                        if (boton != null) { boton.click(); }
                                    }
                                    if (keyCode == 65 && ctrlKey && shiftKey) {  //**  Ctrl + Shift + A
                                        var boton = document.getElementById('<%=btnAnular.ClientID%>');
                                        if (boton != null) { boton.click(); }
                                    }
                                    if (keyCode == 76 && ctrlKey && shiftKey) {  //**  Ctrl + Shift + L
                                        var boton = document.getElementById('<%=btnHelp.ClientID%>');
                                        if (boton != null) { boton.click(); }
                                    }
                                    if ((keyCode == 49 && ctrlKey && shiftKey)) {  //**  Ctrl + Shift + 1
                                        var boton = document.getElementById('<%=btnAgregar.ClientID%>');
                                        if (boton != null) { boton.click(); }
                                    }
                                    if ((keyCode == 50 && ctrlKey && shiftKey)) {  //**  Ctrl + Shift + 2
                                        var boton = document.getElementById('<%=btnBuscarCliente.ClientID%>');
                                        if (boton != null) { boton.click(); }
                                    } 
                                    //else {
                                    //alert('Presionaste: ' + keyCode);}
                                }
                            }
                            document.onkeydown = detectar_tecla;
                            
                            
                            
                            function valEditarCliente() {
                                var txtIdCliente = document.getElementById('<%=txtCodigoCliente.ClientID%>');
                                if (isNaN(txtIdCliente.value) || txtIdCliente.value.length <= 0) {
                                    alert('Debe seleccionar un Cliente.');
                                    return false;
                                } else {

                                var IdTipoPersona = parseInt(document.getElementById('<%=cboTipoPersona.ClientID%>').value);
                                var IdTipoAgente = parseInt(document.getElementById('<%=cboTipoAgente.ClientID%>').value);
                                var txtRazonSocial = document.getElementById('<%=txtApPaterno_RazonSocial.ClientID%>');
                                var txtApPaterno = document.getElementById('<%=txtApPaterno_RazonSocial.ClientID%>');
                                var txtApMaterno = document.getElementById('<%=txtApMaterno.ClientID%>');
                                var txtNombres = document.getElementById('<%=txtNombres.ClientID%>');
                                var txtDNI = document.getElementById('<%=txtDni.ClientID%>');
                                var txtRUC = document.getElementById('<%=txtRuc.ClientID%>');

                                //********* Limpiamos las cajas
                                document.getElementById('<%=txtApPaterno_EditarCliente.ClientID%>').value = '';
                                document.getElementById('<%=txtRazonSocial_EditarCliente.ClientID%>').value = '';
                                document.getElementById('<%=cboTipoAgente_EditarCliente.ClientID%>').value = '0';
                                document.getElementById('<%=txtApMaterno_EditarCliente.ClientID%>').value = '';
                                document.getElementById('<%=txtNombres_EditarCliente.ClientID%>').value = '';
                                document.getElementById('<%=txtDNI_EditarCliente.ClientID%>').value = '';
                                document.getElementById('<%=txtRUC_EditarCliente.ClientID%>').value = '';

                                //********** Mostramos los valores para su Edici�n
                                switch (IdTipoPersona) {
                                    case 0: //**** No se tiene un Tipo de Persona
                                        alert('No se ha seleccionado ning�n tipo de Persona.');
                                        return false;
                                        break;
                                    case 1: //**** NATURAL
                                        document.getElementById('<%=txtApPaterno_EditarCliente.ClientID%>').value = txtApPaterno.value;
                                        break;
                                    case 2: //**** JURIDICA
                                        document.getElementById('<%=txtRazonSocial_EditarCliente.ClientID%>').value = txtRazonSocial.value;
                                        break;
                                }
                                document.getElementById('<%=cboTipoAgente_EditarCliente.ClientID%>').value = IdTipoAgente;
                                document.getElementById('<%=txtApMaterno_EditarCliente.ClientID%>').value = txtApMaterno.value;
                                document.getElementById('<%=txtNombres_EditarCliente.ClientID%>').value = txtNombres.value;
                                document.getElementById('<%=txtDNI_EditarCliente.ClientID%>').value = txtDNI.value;
                                document.getElementById('<%=txtRUC_EditarCliente.ClientID%>').value = txtRUC.value;
                                
                                
                                onCapa('capaEditarPersona');
                                } //*** Fin Else
                                return false;
                            }
                            
                            function valOnKeyPress_EditarCliente(opcion) {

                                if (event.keyCode == 13) {
                                    var boton = document.getElementById('<%=btnGuardar_EditarCliente.ClientID %>');
                                    boton.click();
                                }
                            
                                var tipoPersona = document.getElementById('<%=cboTipoPersona.ClientID %>').value;
                                if(tipoPersona=='0'){
                                    alert('Debe seleccionar un Tipo de Persona [Natural - Jur�dica].');
                                    return false;
                                }
                                if (opcion == '0') {  //**** Accesible con ambos tipos de persona
                                    return true;
                                }
                                if (tipoPersona == opcion) {  //**** Campo correcto para el tipo de persona
                                    return true;
                                } else {
                                    alert('Este campo no es accesible para el tipo de Persona [' + getCampoxValorCombo(document.getElementById('<%=cboTipoPersona.ClientID %>'), tipoPersona) + '].');
                                    return false;
                                }
                                
                                return false;
                            }


                            function valOnClickTipoAgente_EditarCliente() {
                                var tipoPersona = document.getElementById('<%=cboTipoPersona.ClientID %>').value;
                                if (tipoPersona == '0') {
                                    alert('Debe seleccionar un Tipo de Persona [Natural - Jur�dica].');
                                    return false;
                                }
                                if (tipoPersona == '1') {  //*** No es Jur�dica
                                    alert('Opci�n no disponible para el Tipo de Persona [Natural].');
                                    return false;
                                }                                
                                return true;
                            }
                            function valOnKeyPressDNI_EditarCliente() {
                                var flag = valOnKeyPress_EditarCliente('1');
                                if (flag) { //*** Validamos el DNI
                                    return onKeyPressEsNumero();
                                }
                                return false;
                            }
                            function valOnKeyPressRUC_EditarCliente() {
                                var flag = valOnKeyPress_EditarCliente('0');
                                if (flag) { //*** Validamos el DNI
                                    return onKeyPressEsNumero();
                                }
                                return false;
                            }



                            function valOnClickGuardar_EditarCliente() {
                                var tipoPersona = document.getElementById('<%=cboTipoPersona.ClientID %>').value;
                                switch( parseInt(tipoPersona)  ){
                                    case 0:  //**** ----
                                        alert('No se ha seleccionado un Tipo de Persona.');
                                        return false;
                                        break;
                                    case 1:  //**** NATURAL
                                        var txtApPaterno = document.getElementById('<%=txtApPaterno_EditarCliente.ClientID%>');
                                        if (CajaEnBlanco(txtApPaterno)) {
                                            alert('El campo [Ap. Paterno] es un campo requerido.');
                                            txtApPaterno.select();
                                            txtApPaterno.focus();
                                            return false;
                                        }
                                        var txtNombres = document.getElementById('<%=txtNombres_EditarCliente.ClientID%>');
                                        if (CajaEnBlanco(txtNombres)) {
                                            alert('El Campo [Nombre] es un campo requerido.');
                                            txtNombres.select();
                                            txtNombres.focus();
                                            return false;
                                        }
                                        break;
                                    case 2:  //**** JURIDICA
                                        var txtRazonSocial = document.getElementById('<%=txtRazonSocial_EditarCliente.ClientID%>');
                                        if (CajaEnBlanco(txtRazonSocial)) {
                                            alert('El Campo [Raz�n Social] es un campo requerido.');
                                            txtRazonSocial.select();
                                            txtRazonSocial.focus();
                                            return false;
                                        }
                                        var txtRUC  = document.getElementById('<%=txtRUC_EditarCliente.ClientID%>');
                                        if (CajaEnBlanco(txtRUC)) {
                                            alert('El Campo [R.U.C.] es un campo requerido.');
                                            txtRUC.select();
                                            txtRUC.focus();
                                            return false;
                                        }
                                        break;
                                }
                                return confirm('Desea guardar los cambios?');
                            }
                            function valOnClickChb_CompPercepcion(chb_CompPercepcion) {
                                if (chb_CompPercepcion.status) {
                                    alert('Se ha activado la Opci�n [ Comprometer Percepci�n ], el Sistema no tomar� en cuenta la Percepci�n, para ello deber� < Generar un Comprobante de Percepci�n >.');
                                }
                                return true;
                            }

                            function valOnClickTipoAgente_RegistrarCliente() {
                                var cboTipoPersona = document.getElementById('<%=cmbTipoPersona.ClientID%>');
                                var cmbTipoAgente_RegistrarCliente = document.getElementById('<%=cmbTipoAgente_RegistrarCliente.ClientID%>');
                                if (cboTipoPersona.value == 'N') {
                                    alert('Opci�n no disponible para la Persona tipo [Natural].');
                                    cmbTipoAgente_RegistrarCliente.value = '0';
                                    return false;
                                }
                                return true;
                            }




                            function GenerarDocCompPercepcion_capa() {

                                //********* Validamos la COTIZACION
                                var IdTipoDocumento = parseInt(document.getElementById('<%= cboTipoDocumento.ClientID%>').value);
                                if (IdTipoDocumento == 14) {
                                    alert('No puede generar un Comprobante de Percepci�n para una Cotizaci�n.');
                                    return false;
                                }
                                
                                var grilla = document.getElementById('<%= DGV_ImpresionDoc_Cab.ClientID%>');


                                var IdDocumento = 0;

                                if (grilla == null) {
                                    alert('No se ha registrado ning�n documento.');
                                    return false;
                                } else if (grilla.rows.length == 2) {  //**** SOLO EXISTE UN DOCUMENTO
                                    validarMostrarCapaImpresion('3')  //*********** 
                                    return false;
                                }

                                for (var i = 1; i < grilla.rows.length; i++) {
                                    var rowElem = grilla.rows[i];
                                    if (rowElem.cells[7].children[0].status == true) {
                                        IdDocumento = parseInt(rowElem.cells[1].innerHTML);
                                        break;
                                    }
                                }

                                if (IdDocumento == 0) {
                                    alert('No se ha seleccionado ning�n documento.')
                                    return false;
                                }

                                var IdEstadoDocumento = parseInt(document.getElementById('<%=cboEstadoDocumento.ClientID%>').value);
                                if (IdEstadoDocumento == 2) {  //********** ANULADO
                                    alert('El Documento est� ANULADO.');
                                    return false;
                                }

                                window.open('FrmComprobantePercepcion.aspx?IdDocumento=' + IdDocumento, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                                
                                return false;
                            }
                            
                            function onCapaGlosa() {
                                var grilla = document.getElementById('<%=gvDetalle.ClientID %>');
                                var index = 0;
                                if (grilla != null) {
                                    for (var i = 1; i < grilla.rows.length; i++) {
                                        var rowElem = grilla.rows[i];
                                        if (rowElem.cells[2].children[0].cells[1].children[2].id == event.srcElement.id) {
                                            index = i;
                                            break;
                                        }
                                    }
                                }
                                if(index<=0){
                                    alert('No se hall� el �ndice del Producto seleccionado.');
                                    return false;
                                }
                                onCapa('capaGlosa');
                                document.getElementById('<%=hddIndexDetalle_Area.ClientID%>').value = (index-1);
                                document.getElementById('<%= txtGlosa_CapaGlosa.ClientID%>').value = '';
                                document.getElementById('<%= txtGlosa_CapaGlosa.ClientID%>').select();
                                document.getElementById('<%= txtGlosa_CapaGlosa.ClientID%>').focus();                                
                                return false;
                            }
                            function valOnClickAddGlosa() {
                                var txtGlosa=document.getElementById('<%= txtGlosa_CapaGlosa.ClientID%>');
                                if (txtGlosa.value.length<=0) {
                                    alert('Ingrese una descripci�n.');
                                    txtGlosa.select();
                                    txtGlosa.focus();
                                    return false;
                                }
                                return true;
                            }
                            function valOnClick_btnAddNotaCredito() {
                                var txtCodigoCliente = document.getElementById('<%=txtCodigoCliente.ClientID%>');
                                if (isNaN(parseInt(txtCodigoCliente.value)) || txtCodigoCliente.value.length <= 0) {
                                    alert('Debe seleccionar un Cliente.');
                                    return false;
                                }                                
                                return true;
                            }
                        </script>
       </asp:Content>

