﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmCampaniaComercial
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Private objCbo As New Combo
    Private listaProductoView As List(Of Entidades.ProductoView)
    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor
    Private listaCampania_Detalle As List(Of Entidades.Campania_Detalle)
    Private listaMoneda As List(Of Entidades.Moneda)
    Private listaTiendaReferencia As List(Of Entidades.Tienda)

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
    End Enum

    Private Property ListaTiendaRef() As List(Of Entidades.Tienda)
        Get
            Return CType(Session.Item("ListaTiendaRef"), List(Of Entidades.Tienda))
        End Get
        Set(ByVal value As List(Of Entidades.Tienda))
            Session.Remove("ListaTiendaRef")
            Session.Add("ListaTiendaRef", value)
        End Set
    End Property


    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        valOnLoad_Frm()
    End Sub

    Private Sub valOnLoad_Frm()
        Try

            If (Not Me.IsPostBack) Then

                ConfigurarDatos()
                inicializarFrm()

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub inicializarFrm()

        Dim objCbo As New Combo
        With objCbo

            .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
            .llenarCboTiendaxIdEmpresa(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), False)
            .llenarCboTiendaxIdEmpresa(Me.cboTiendaRef, CInt(Me.cboEmpresa.SelectedValue), True)


            '--lineas---
            .llenarCboTipoExistencia(CboTipoExistencia, False)
            .llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(CboTipoExistencia.SelectedValue), True)
            .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
            .LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            '.LlenarCboLinea(Me.cmbLinea_AddProd, True)
            '.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            '.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)
            .LlenarCboTipoPV(Me.cboTipoPV, False)

        End With

        Me.txtFechaInicio.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.txtFechaFin.Text = Me.txtFechaInicio.Text

        Me.listaMoneda = (New Negocio.Moneda).SelectCbo
        setListaMoneda(Me.listaMoneda)

        verFrm(FrmModo.Inicio, True, True, True)



    End Sub

    Private Function getListaCampania_Detalle() As List(Of Entidades.Campania_Detalle)
        Return CType(Session.Item("listaCampania_Detalle"), List(Of Entidades.Campania_Detalle))
    End Function

    Private Sub setListaCampania_Detalle(ByVal lista As List(Of Entidades.Campania_Detalle))
        Session.Remove("listaCampania_Detalle")
        Session.Add("listaCampania_Detalle", lista)
    End Sub

    Private Function getListaMoneda() As List(Of Entidades.Moneda)
        Return CType(Session.Item("listaMoneda"), List(Of Entidades.Moneda))
    End Function

    Private Sub setListaMoneda(ByVal lista As List(Of Entidades.Moneda))
        Session.Remove("listaMoneda")
        Session.Add("listaMoneda", lista)
    End Sub

    Private Sub limpiarFrm()

        '************** COMBOS
        Me.txtDescripcionCampania.Text = ""
        Me.txtCantidadMinima_Venta.Text = "0"
        Me.txtDescuento.Text = "0"

        Me.GV_Campania_Detalle.DataSource = Nothing
        Me.GV_Campania_Detalle.DataBind()
        Me.GV_CampaniaRef.DataBind()

    End Sub

    Private Sub actualizarControles()

        Me.btnNuevo.Visible = False
        Me.btnGuardar.Visible = False
        Me.btnCancelar.Visible = False
        Me.btnBuscar.Visible = False
        Me.Panel_Grilla.Enabled = False

        Me.cboEmpresa.Enabled = True
        Me.cboTienda.Enabled = True

        Me.Panel_Campania_Detalle.Visible = False

        Select Case CInt(Me.hddFrmModo.Value)

            Case FrmModo.Inicio

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.Panel_Grilla.Enabled = True

            Case FrmModo.Nuevo

                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.Panel_Campania_Detalle.Visible = True

            Case FrmModo.Editar

                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.Panel_Campania_Detalle.Visible = True

        End Select

    End Sub

    Private Sub verFrm(ByVal modo As Integer, ByVal limpiar As Boolean, ByVal cargarGrilla As Boolean, ByVal initSession As Boolean)

        If (limpiar) Then
            limpiarFrm()
        End If

        If (cargarGrilla) Then

            Dim fecha As Date = (New Negocio.FechaActual).SelectFechaActual
            Me.GV_Campania_Find.DataSource = (New Negocio.Campania).Campania_SelectxParams(Nothing, CInt(Me.cboEmpresa.SelectedValue), Nothing, Nothing, Nothing, fecha, True)
            Me.GV_Campania_Find.DataBind()

        End If

        If (initSession) Then
            Me.listaCampania_Detalle = New List(Of Entidades.Campania_Detalle)
            setListaCampania_Detalle(Me.listaCampania_Detalle)
            Me.ListaTiendaRef = New List(Of Entidades.Tienda)
        End If

        Me.hddFrmModo.Value = CStr(modo)
        actualizarControles()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        valOnClick_btnCancelar()
    End Sub


    Private Sub valOnClick_btnCancelar()
        Try

            verFrm(FrmModo.Inicio, True, True, True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click
        valOnClick_btnBuscar()
    End Sub
    Private Sub valOnClick_btnBuscar()
        Try

            Dim fechaInicio As Date = Nothing
            Dim fechaFin As Date = Nothing
            Dim estado As Boolean = False

            If (IsDate(Me.txtFechaInicio.Text) And Me.txtFechaInicio.Text.Trim.Length > 0) Then
                fechaInicio = CDate(Me.txtFechaInicio.Text)
            End If

            If (IsDate(Me.txtFechaFin.Text) And Me.txtFechaFin.Text.Trim.Length > 0) Then
                fechaFin = CDate(Me.txtFechaFin.Text)
            End If

            If (CInt(Me.cboEstado.SelectedValue) = 1) Then  '************* ACTIVO
                estado = True
            End If

            Me.GV_Campania_Find.DataSource = (New Negocio.Campania).Campania_SelectxParams(Nothing, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), Me.txtDescripcionCampania.Text, fechaInicio, fechaFin, estado)
            Me.GV_Campania_Find.DataBind()

            If (Me.GV_Campania_Find.Rows.Count <= 0) Then
                objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        valOnChange_cboEmpresa()
    End Sub
    Private Sub valOnChange_cboEmpresa()
        Try

            objCbo.llenarCboTiendaxIdEmpresa(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), False)
            objCbo.llenarCboTiendaxIdEmpresa(Me.cboTiendaRef, CInt(Me.cboEmpresa.SelectedValue), False)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#Region "************************** BUSQUEDA PRODUCTO"
    Private Sub btnBuscarGrilla_AddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarGrilla_AddProd.Click
        BuscarProductoCatalogo(0)
    End Sub

    Protected Sub BuscarProductoCatalogo(ByVal opcion As Integer)
        Try

            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar

                    ViewState.Add("nombre_BuscarProducto", txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)
                    ViewState.Add("IdAlmacen_BuscarProducto", -1)
                    ViewState.Add("IdEmpresa_BuscarProducto", -1)
                    ViewState.Add("CodigoSubLinea_BuscarProducto", "")
                    ViewState.Add("IdTipoExistencia", Me.CboTipoExistencia.SelectedValue)
            End Select

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, _
                                          ByVal codigoSubLinea As String, ByVal nomProducto As String, _
                                          ByVal IdAlmacen As Integer, ByVal tipoMov As Integer, _
                                          ByVal IdEmpresa As Integer, ByVal IdTipoExistencia As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select

        Dim codigoProducto As String = Me.txtCodigoProducto.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()

        Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams_V2(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, -1, -1, grilla.PageSize, index, codigoProducto, IdTipoExistencia, tableTipoTabla)

        If Me.listaProductoView.Count = 0 Then

            grilla.DataSource = Nothing
            grilla.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');    alert('No se hallaron registros.');        ", True)
            Return

        Else

            grilla.DataSource = Me.listaProductoView
            grilla.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');           ", True)

        End If

    End Sub
    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            -1, 1, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            -1, 2, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            -1, 3, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)


            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region
#Region "************************** BUSQUEDA AVANZADA PRODUCTOS"

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Protected Sub M_B_cmbSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1

            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)

        Next

        Return dt

    End Function
#End Region

    Private Sub btnAddProductos_AddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddProductos_AddProd.Click

        valOnClick_btnAddProductos_AddProd()

    End Sub
    Private Sub valOnClick_btnAddProductos_AddProd()
        Try

            actualizarListaCampania_Detalle()
            Me.listaCampania_Detalle = getListaCampania_Detalle()

            Dim listaAdd As List(Of Entidades.Campania_Detalle) = obtenerListaAdd_Campania_Detalle()
            Me.listaCampania_Detalle.AddRange(listaAdd)

            setListaCampania_Detalle(Me.listaCampania_Detalle)

            Me.GV_Campania_Detalle.DataSource = Me.listaCampania_Detalle
            Me.GV_Campania_Detalle.DataBind()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function obtenerListaAdd_Campania_Detalle() As List(Of Entidades.Campania_Detalle)

        Dim listaAdd As New List(Of Entidades.Campania_Detalle)

        If (Me.chb_CargaMasiva.Checked) Then

            listaAdd = (New Negocio.Campania_Detalle).Campania_SelectProducto_AddDetallexParams(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), Nothing, CInt(Me.cboTienda.SelectedValue), CInt(Me.cboTipoPV.SelectedValue))

        Else

            Dim objDataTable As DataTable = obtenerDataTable_Producto()
            listaAdd = (New Negocio.Campania_Detalle).Campania_SelectProducto_AddDetallexParams(Nothing, Nothing, objDataTable, CInt(Me.cboTienda.SelectedValue), CInt(Me.cboTipoPV.SelectedValue))

        End If

        Me.listaMoneda = getListaMoneda()
        For i As Integer = 0 To listaAdd.Count - 1

            listaAdd(i).ListaMoneda = Me.listaMoneda
            listaAdd(i).Precio = listaAdd(i).PrecioActual

        Next

        Return listaAdd

    End Function

    Private Function obtenerDataTable_Producto() As DataTable

        Dim objDataTable As New DataTable

        objDataTable.Columns.Add("Columna1")

        For i As Integer = 0 To Me.DGV_AddProd.Rows.Count - 1

            If (CType(Me.DGV_AddProd.Rows(i).FindControl("chb_AddDetalle"), CheckBox).Checked) Then

                Dim IdProducto As Integer = CInt(CType(Me.DGV_AddProd.Rows(i).FindControl("hddIdProducto"), HiddenField).Value)
                objDataTable.Rows.Add(IdProducto)

            End If

        Next


        Return objDataTable

    End Function

    Private Sub actualizarListaCampania_Detalle()

        Me.listaCampania_Detalle = getListaCampania_Detalle()

        For i As Integer = 0 To Me.GV_Campania_Detalle.Rows.Count - 1

            Me.listaCampania_Detalle(i).IdUnidadMedida = CInt(CType(Me.GV_Campania_Detalle.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedValue)
            Me.listaCampania_Detalle(i).IdMoneda = CInt(CType(Me.GV_Campania_Detalle.Rows(i).FindControl("cboMoneda"), DropDownList).SelectedValue)
            Me.listaCampania_Detalle(i).Precio = CDec(CType(Me.GV_Campania_Detalle.Rows(i).FindControl("txtPrecioCampania"), TextBox).Text)
            Me.listaCampania_Detalle(i).CantidadMin = CDec(CType(Me.GV_Campania_Detalle.Rows(i).FindControl("txtCantidadMin"), TextBox).Text)

        Next

        setListaCampania_Detalle(Me.listaCampania_Detalle)

    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevo.Click
        valOnClick_btnNuevo()
    End Sub
    Private Sub valOnClick_btnNuevo()
        Try

            verFrm(FrmModo.Nuevo, True, False, True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_Campania_Detalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Campania_Detalle.RowDataBound

        If (e.Row.RowType = DataControlRowType.DataRow) Then

            Dim cboUnidadMedida As DropDownList = CType(e.Row.FindControl("cboUnidadMedida"), DropDownList)

            If (cboUnidadMedida.Items.FindByValue(CStr(Me.listaCampania_Detalle(e.Row.RowIndex).IdUnidadMedida)) IsNot Nothing) Then

                cboUnidadMedida.SelectedValue = CStr(Me.listaCampania_Detalle(e.Row.RowIndex).IdUnidadMedida)

            End If

            Dim cboMoneda As DropDownList = CType(e.Row.FindControl("cboMoneda"), DropDownList)

            If (cboMoneda.Items.FindByValue(CStr(Me.listaCampania_Detalle(e.Row.RowIndex).IdMoneda)) IsNot Nothing) Then

                cboMoneda.SelectedValue = CStr(Me.listaCampania_Detalle(e.Row.RowIndex).IdMoneda)

            End If

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    cboUnidadMedida.Enabled = True

                Case FrmModo.Editar
                    cboUnidadMedida.Enabled = False

            End Select

        End If

    End Sub

    Private Sub GV_Campania_Detalle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Campania_Detalle.SelectedIndexChanged

        quitarCampania_Detalle(Me.GV_Campania_Detalle.SelectedIndex)

    End Sub
    Private Sub quitarCampania_Detalle(ByVal index As Integer)

        Try

            actualizarListaCampania_Detalle()
            Me.listaCampania_Detalle = getListaCampania_Detalle()

            Me.listaCampania_Detalle.RemoveAt(index)

            setListaCampania_Detalle(Me.listaCampania_Detalle)
            Me.GV_Campania_Detalle.DataSource = Me.listaCampania_Detalle
            Me.GV_Campania_Detalle.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click

        registrar()

    End Sub

    Private Sub registrar()

        Try

            actualizarListaCampania_Detalle()

            Dim objCampania As Entidades.Campania = obtenerCampania_Cab()
            Me.listaCampania_Detalle = getListaCampania_Detalle()

            Dim operacion As Boolean

            operacion = (New Negocio.Campania).registrarCampania(objCampania, CInt(Session("IdUsuario")), Me.listaCampania_Detalle)

            Me.listaTiendaReferencia = Me.ListaTiendaRef
            If Me.listaTiendaReferencia.Count > 0 Then

                For i As Integer = 0 To listaTiendaReferencia.Count - 1

                    With objCampania
                        .IdTienda = Me.listaTiendaReferencia(i).Id
                        .IdCampania = 0
                    End With

                    operacion = (New Negocio.Campania).registrarCampania(objCampania, CInt(Session("IdUsuario")), Me.listaCampania_Detalle)

                Next

            End If


            If (operacion) Then

                objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito.")
                verFrm(FrmModo.Inicio, True, True, True)

            Else

                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function obtenerCampania_Cab() As Entidades.Campania

        Dim objCampania As New Entidades.Campania

        With objCampania

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .IdCampania = Nothing
                Case FrmModo.Editar
                    .IdCampania = CInt(Me.hddIdCampania.Value)
            End Select

            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)

            If (Me.cboEstado.SelectedValue = "1") Then
                .Estado = True
            Else
                .Estado = False
            End If

            .Descripcion = Me.txtDescripcionCampania.Text

            If (IsDate(Me.txtFechaInicio.Text) And Me.txtFechaInicio.Text.Trim.Length > 0) Then
                .FechaInicio = CDate(Me.txtFechaInicio.Text)
            End If

            If (IsDate(Me.txtFechaFin.Text) And Me.txtFechaFin.Text.Trim.Length > 0) Then
                .FechaFin = CDate(Me.txtFechaFin.Text)
            End If

        End With

        Return objCampania

    End Function
    Protected Sub valOnClick_btnEditar(ByVal sender As Object, ByVal e As EventArgs)

        editarCampania(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)

    End Sub

    Private Sub editarCampania(ByVal index As Integer)

        Try

            Dim IdCampania As Integer = CInt(CType(Me.GV_Campania_Find.Rows(index).FindControl("hddIdCampania"), HiddenField).Value)

            Dim objCampania As Entidades.Campania = (New Negocio.Campania).Campania_SelectxIdCampania(IdCampania)

            If (objCampania Is Nothing) Then
                Throw New Exception("NO SE HALLARON REGISTROS")
            End If

            Me.listaCampania_Detalle = obtenerListaCampania_Detalle_Load(objCampania.IdCampania)

            verFrm(FrmModo.Editar, False, False, False)

            cargarGUI(objCampania, Me.listaCampania_Detalle)

            setListaCampania_Detalle(Me.listaCampania_Detalle)

            Me.ListaTiendaRef = New List(Of Entidades.Tienda)
            Me.GV_CampaniaRef.DataBind()


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function obtenerListaCampania_Detalle_Load(ByVal IdCampania As Integer) As List(Of Entidades.Campania_Detalle)

        Dim lista As List(Of Entidades.Campania_Detalle) = (New Negocio.Campania_Detalle).Campania_Detalle_SelectxIdCampania(IdCampania)
        Me.listaMoneda = getListaMoneda()

        For i As Integer = 0 To lista.Count - 1

            lista(i).ListaMoneda = Me.listaMoneda

        Next

        Return lista

    End Function

    Private Sub cargarGUI(ByVal objCampania As Entidades.Campania, ByVal listaCampania_Detalle As List(Of Entidades.Campania_Detalle))

        With objCampania

            Me.hddIdCampania.Value = CStr(.IdCampania)

            If (Me.cboEmpresa.Items.FindByValue(CStr(.IdEmpresa)) IsNot Nothing) Then
                Me.cboEmpresa.SelectedValue = CStr(.IdEmpresa)
            End If
            If (Me.cboTienda.Items.FindByValue(CStr(.IdTienda)) IsNot Nothing) Then
                Me.cboTienda.SelectedValue = CStr(.IdTienda)
            End If

            Me.txtDescripcionCampania.Text = objCampania.Descripcion

            If (.FechaInicio <> Nothing) Then
                Me.txtFechaInicio.Text = Format(.FechaInicio, "dd/MM/yyyy")
            End If
            If (.FechaFin <> Nothing) Then
                Me.txtFechaFin.Text = Format(.FechaFin, "dd/MM/yyyy")
            End If

            If (.Estado) Then
                Me.cboEstado.SelectedValue = "1"
            Else
                Me.cboEstado.SelectedValue = "0"
            End If

        End With

        Me.GV_Campania_Detalle.DataSource = Me.listaCampania_Detalle
        Me.GV_Campania_Detalle.DataBind()

    End Sub

    Private Sub CboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboTipoExistencia.SelectedIndexChanged
        Try
            Dim objcbo As New Combo
            objcbo.llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(CboTipoExistencia.SelectedValue), True)
            objcbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub btnAddTiendaRef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddTiendaRef.Click
        OnClick_AddTiendaRef()
    End Sub
    Private Sub OnClick_AddTiendaRef()
        Try
            Me.listaTiendaReferencia = ListaTiendaRef


            Me.listaTiendaReferencia.RemoveAll(Function(k As Entidades.Tienda) k.Id = CInt(cboTiendaRef.SelectedValue))

            If cboTiendaRef.SelectedValue = "0" Then

                Dim Lista As List(Of Entidades.Tienda) = (New Negocio.Tienda).SelectCboxEmpresa(CInt(Me.cboEmpresa.SelectedValue))


                For i As Integer = 0 To Lista.Count - 1

                    Dim index As Integer = i
                    Me.listaTiendaReferencia.RemoveAll(Function(k As Entidades.Tienda) k.Id = Lista(index).Id)

                Next

                Me.listaTiendaReferencia.AddRange(Lista)

            Else

                Me.listaTiendaReferencia.AddRange((New Negocio.Tienda).SelectxId(CInt(cboTiendaRef.SelectedValue)))

            End If

            Me.listaTiendaReferencia.RemoveAll(Function(k As Entidades.Tienda) k.Id = CInt(cboTienda.SelectedValue))

            Me.ListaTiendaRef = Me.listaTiendaReferencia

            Me.GV_CampaniaRef.DataSource = Me.listaTiendaReferencia
            Me.GV_CampaniaRef.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnQuitarTienda_Click(ByVal sender As Object, ByVal e As EventArgs)
        QuitarTienda(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
    End Sub

    Private Sub QuitarTienda(ByVal Index As Integer)
        Me.listaTiendaReferencia = ListaTiendaRef

        Me.listaTiendaReferencia.RemoveAt(Index)

        Me.ListaTiendaRef = Me.listaTiendaReferencia

        Me.GV_CampaniaRef.DataSource = Me.listaTiendaReferencia
        Me.GV_CampaniaRef.DataBind()

    End Sub


End Class