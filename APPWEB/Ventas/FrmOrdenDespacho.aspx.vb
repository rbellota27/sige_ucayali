﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



Partial Public Class FrmOrdenDespacho
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    Private listaDetalle As List(Of Entidades.DetalleDocumento)


    '*************** ModoFrm
    '******** 0 : INICIO / nuevo - buscar
    '******** 1 : NUEVO / guardar - cancelar
    '******** 2 : EDITAR DOCUMENTO / guardar - cancelar
    '******** 3 : BUSCAR DOCUMENTO / buscar doc - habilitar busqueda
    '******** 4 : DOCUMENTO HALLADO CORRECTAMENTE / editar - anular - imprimir - despachar - remitir - cancelar
    '******** 5 : DOC. GUARDADO CORRECTAMENTE / imprimir - despachar - remitir - cancelar
    '******** 6: OCULTAR TODOS LOS BOTONES
    '**********************************************

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Dim IdDocumento As String = Request.QueryString("IdDocumento")
            inicializarFrm()
            If IdDocumento IsNot Nothing Then
                ViewState.Add("IdDocumento", IdDocumento)
                cargarDatosOrdenDespacho(CInt(IdDocumento))
                hddFrmModo.Value = "1"
            Else
                hddFrmModo.Value = "0"
            End If
            actualizarVistaFrm()
        End If
    End Sub

    Private Sub actualizarVistaFrm()

        Select Case CInt(hddFrmModo.Value)
            Case 0 '************* INICIO

                btnAnular.Visible = False
                btnGuardar.Visible = False
                btnBuscar.Visible = True
                btnBuscarDocumento.Visible = False
                btnImprimir.Visible = False
                btnAtras.Visible = False
                btnEditar.Visible = False


                Panel_DatosCliente.Enabled = False
                Panel_DetalleOD.Enabled = False
                btnMostrarCapaBuscarDoc.Enabled = True
                txtCodigoDocumento.ReadOnly = True


                Me.cboSerie.Enabled = True
                Me.cmbEmpresa.Enabled = True
                Me.cmbTienda.Enabled = True
                Me.txtFechaEmision.Enabled = False




            Case 1 '******** nuevo
                btnAnular.Visible = False
                btnGuardar.Visible = True
                btnBuscar.Visible = False
                btnBuscarDocumento.Visible = False
                btnImprimir.Visible = False
                btnAtras.Visible = True
                btnEditar.Visible = False


                Panel_DatosCliente.Enabled = True
                Panel_DetalleOD.Enabled = True
                btnMostrarCapaBuscarDoc.Enabled = False
                txtCodigoDocumento.ReadOnly = True

                Me.cboSerie.Enabled = True
                Me.cmbEmpresa.Enabled = False
                Me.cmbTienda.Enabled = False
                Me.txtFechaEmision.Enabled = True

            Case 2 '********** EDITAR
                btnAnular.Visible = False
                btnGuardar.Visible = True
                btnBuscar.Visible = False
                btnBuscarDocumento.Visible = False
                btnImprimir.Visible = False
                btnAtras.Visible = True
                btnEditar.Visible = False


                Panel_DatosCliente.Enabled = False
                Panel_DetalleOD.Enabled = True
                btnMostrarCapaBuscarDoc.Enabled = False
                txtCodigoDocumento.ReadOnly = True

                Me.cboSerie.Enabled = False
                Me.cmbEmpresa.Enabled = False
                Me.cmbTienda.Enabled = False
                Me.txtFechaEmision.Enabled = True

            Case 3 '********** Habilitar Busqueda
                btnAnular.Visible = False
                btnGuardar.Visible = False
                btnBuscar.Visible = True
                btnBuscarDocumento.Visible = True
                btnImprimir.Visible = False
                btnAtras.Visible = True
                btnEditar.Visible = False


                Panel_DatosCliente.Enabled = False
                Panel_DetalleOD.Enabled = False
                btnMostrarCapaBuscarDoc.Enabled = False
                txtCodigoDocumento.ReadOnly = False
                txtCodigoDocumento.Focus()

                Me.cboSerie.Enabled = True
                Me.cmbEmpresa.Enabled = True
                Me.cmbTienda.Enabled = True
                Me.txtFechaEmision.Enabled = False

            Case 4 '********* hallado correctamente
                btnAnular.Visible = True
                btnGuardar.Visible = False
                btnBuscar.Visible = False
                btnBuscarDocumento.Visible = False
                btnImprimir.Visible = True
                btnAtras.Visible = True
                btnEditar.Visible = True

                Panel_Cabecera.Enabled = True
                Panel_DatosCliente.Enabled = False
                Panel_DetalleOD.Enabled = False
                btnMostrarCapaBuscarDoc.Enabled = False
                txtCodigoDocumento.ReadOnly = True

                Me.cboSerie.Enabled = False
                Me.cmbEmpresa.Enabled = False
                Me.cmbTienda.Enabled = False
                Me.txtFechaEmision.Enabled = False

            Case 5 '******** doc. guardado correctamente
                btnAnular.Visible = False
                btnGuardar.Visible = False
                btnBuscar.Visible = True
                btnBuscarDocumento.Visible = False
                btnImprimir.Visible = True
                btnAtras.Visible = True
                btnEditar.Visible = False


                Panel_DatosCliente.Enabled = False
                Panel_DetalleOD.Enabled = False
                btnMostrarCapaBuscarDoc.Enabled = False
                txtCodigoDocumento.ReadOnly = True

                Me.cboSerie.Enabled = False
                Me.cmbEmpresa.Enabled = False
                Me.cmbTienda.Enabled = False
                Me.txtFechaEmision.Enabled = False

            Case 6 '********* ocultar todos los botones
                btnAnular.Visible = False
                btnGuardar.Visible = False
                btnBuscar.Visible = False
                btnBuscarDocumento.Visible = False
                btnImprimir.Visible = False
                btnAtras.Visible = True
                btnEditar.Visible = False


                Panel_DatosCliente.Enabled = False
                Panel_DetalleOD.Enabled = False
                btnMostrarCapaBuscarDoc.Enabled = False
                txtCodigoDocumento.ReadOnly = True

                Me.cboSerie.Enabled = False
                Me.cmbEmpresa.Enabled = False
                Me.cmbTienda.Enabled = False
                Me.txtFechaEmision.Enabled = False
        End Select


    End Sub


    Public Sub LlenarCboTipoDocumento(ByVal cbo As DropDownList, ByVal IdPropietario As Integer, ByVal IdArea As Integer)
        Dim obj As New Negocio.TipoDocumento
        cbo.DataSource = obj.SelectCboxIdEmpresaxIdArea(IdPropietario, IdArea)
        cbo.DataTextField = "DescripcionCorto"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    Private Sub verFrmInicio()
        Session.Remove("listaDetalle")
        limpiarFrm()
        hddFrmModo.Value = "0"
        actualizarVistaFrm()
    End Sub

    Private Sub limpiarFrm()
        txtCodigoDocumento.Text = ""
        txtDireccion.Text = ""
        txtDNI.Text = ""
        txtIdCliente.Text = ""
        txtNombreCliente.Text = ""
        txtRUC.Text = ""
        hddIdDocumento.Value = ""
        DGVDetalle.DataSource = Nothing
        DGVDetalle.DataBind()
    End Sub
    Private Sub setListaDetalle(ByVal lista As List(Of Entidades.DetalleDocumento))
        Session.Remove("listaDetalle")
        Session.Add("listaDetalle", lista)
    End Sub
    Private Function getListaDetalle() As List(Of Entidades.DetalleDocumento)
        Return CType(Session.Item("listaDetalle"), List(Of Entidades.DetalleDocumento))
    End Function
    Private Sub cargarDatosOrdenDespacho(ByVal IdDocumento As Integer)
        Try
            Dim objNegDocOrdenDespacho As New Negocio.DocOrdenDespacho
            Dim objNegDetalleDoc As New Negocio.DetalleDocumento
            Dim objDocOrdenDespacho As New Entidades.DocOrdenDespacho
            'Dim objNegMovAlmacen As New Negocio.MovAlmacen
            objDocOrdenDespacho = objNegDocOrdenDespacho.SelectxIdDocumento(IdDocumento)
            txtDireccion.Text = objDocOrdenDespacho.Direccion
            txtDNI.Text = objDocOrdenDespacho.Dni
            txtIdCliente.Text = objDocOrdenDespacho.IdPersona.ToString
            txtNombreCliente.Text = objDocOrdenDespacho.getNombreMostrar
            txtRUC.Text = objDocOrdenDespacho.Ruc
            If objDocOrdenDespacho.IdEmpresa <> 0 Then cmbEmpresa.SelectedValue = objDocOrdenDespacho.IdEmpresa.ToString
            cargarDatosCboTienda(cmbTienda)
            If objDocOrdenDespacho.IdTienda <> 0 Then cmbTienda.SelectedValue = objDocOrdenDespacho.IdTienda.ToString
            cargarDatosSerie(cboSerie)
            GenerarCodigo()

            Me.listaDetalle = objNegDetalleDoc.SelectCantxAtenderNoCeroxIdDocumento(IdDocumento)
            DGVDetalle.DataSource = listaDetalle
            DGVDetalle.DataBind()
            setListaDetalle(Me.listaDetalle)
            If Me.listaDetalle.Count = 0 Then
                objScript.mostrarMsjAlerta(Me, "No existen productos con cantidades pendientes.")
            Else

                hddFrmModo.Value = "1"
                actualizarVistaFrm()
                ViewState.Add("IdDocumento", IdDocumento)

                objScript.offCapa(Me, "capaDocxDespachar")


            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub inicializarFrm()
        Try
            '***************** cargamos la fecha actual
            Dim objFechaActual As New Negocio.FechaActual
            txtFechaEmision.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")

            cargarDatosCboEmpresa(cmbEmpresa)
            cargarDatosCboEmpresa(cmbEmpresa_B)


            LlenarCboTipoDocumento(Me.cmbTipoDocumento_B, CInt(cmbEmpresa_B.SelectedValue), 2)
            
            cargarDatosCboTienda(cmbTienda)
            cargarDatosCboTienda(cmbTienda_B, "B")




            

            

          

            cargarDatosSerie(cboSerie)
            cargarDatosSerie(cmbSerie_B, "B")
            GenerarCodigo()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosEstadoDocumento(ByVal cbo As DropDownList)
        Dim obj As New Combo
        obj.LLenarCboEstadoDocumento(cbo)
        cbo.DataBind()
    End Sub
    Private Sub cargarDatosSerie(ByVal cbo As DropDownList, Optional ByVal opcion As String = "")
        Dim obj As New Combo
        If opcion = "B" Then
            obj.LLenarCboSeriexIdsEmpTienTipoDoc(cbo, CInt(cmbEmpresa_B.SelectedValue), CInt(cmbTienda_B.SelectedValue), CInt(cmbTipoDocumento_B.SelectedValue))
        Else
            obj.LLenarCboSeriexIdsEmpTienTipoDoc(cbo, CInt(cmbEmpresa.SelectedValue), CInt(cmbTienda.SelectedValue), CInt(cmbTipoDocumento.SelectedValue))
        End If
        cbo.DataBind()
    End Sub
    Private Sub cargarDatosCboEmpresa(ByVal cbo As DropDownList)
        Dim objCombo As New Combo
        objCombo.LlenarCboEmpresaxIdUsuario(cbo, CInt(Session("IdUsuario")), False)
    End Sub
    Private Sub cargarDatosCboTienda(ByVal cbo As DropDownList, Optional ByVal opcion As String = "")
        Dim obj As New Combo
        If (opcion = "B") Then
            obj.LlenarCboTiendaxIdEmpresaxIdUsuario(cbo, CInt(Me.cmbEmpresa_B.SelectedValue), CInt(Session("IdUsuario")), False)
        Else
            obj.LlenarCboTiendaxIdEmpresaxIdUsuario(cbo, CInt(Me.cmbEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
        End If

    End Sub
    Protected Sub cmbEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbEmpresa.SelectedIndexChanged
        Try
            cargarDatosCboTienda(cmbTienda)
            cargarDatosSerie(cboSerie)
            GenerarCodigo()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GenerarCodigo()
        Dim obj As New Negocio.Serie
        Try
            'Me.txtSerie.Text = obj.GenerarSerie(CInt(Me.cboSerie.SelectedValue))
            If Me.cboSerie.Items.Count = 0 Then
                'objScript.mostrarMsjAlerta(Me, "No Existe Serie asignada para este Documento")
            Else
                Me.txtCodigoDocumento.Text = obj.GenerarCodigo(CInt(Me.cboSerie.SelectedValue))
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub cmbTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbTienda.SelectedIndexChanged
        Try
            cargarDatosSerie(cboSerie)
            GenerarCodigo()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboSerie.SelectedIndexChanged
        GenerarCodigo()
    End Sub
    Protected Sub DGVDetalle_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGVDetalle.SelectedIndexChanged
        Try
            saveEnListaDetalle()
            Me.listaDetalle = getListaDetalle()
            Me.listaDetalle.RemoveAt(DGVDetalle.SelectedIndex)
            setListaDetalle(Me.listaDetalle)
            DGVDetalle.DataSource = Me.listaDetalle
            DGVDetalle.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub saveEnListaDetalle()
        Me.listaDetalle = getListaDetalle()
        For i As Integer = 0 To DGVDetalle.Rows.Count - 1
            Me.listaDetalle.Item(i).CantADespachar = CDec(CType(DGVDetalle.Rows(i).Cells(6).FindControl("txtCantidadADespachar"), TextBox).Text)
        Next
        setListaDetalle(Me.listaDetalle)
    End Sub

    Protected Sub btnAtras_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAtras.Click
        verFrmInicio()
    End Sub
    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        If IsNumeric(hddIdDocumento.Value) And hddIdDocumento.Value.Trim.Length > 0 Then
            actualizarOrdenDespacho(CInt(hddIdDocumento.Value.Trim))
        Else
            registrarOrdenDespacho()
        End If
    End Sub
    Private Sub registrarOrdenDespacho()
        Try
            '************** generamos nuevamente el codigo
            GenerarCodigo()

            saveEnListaDetalle()
            Dim objDocOrdenDespacho As New Negocio.DocOrdenDespacho
            Dim objdocumento As New Entidades.Documento
            With objdocumento
                .Codigo = txtCodigoDocumento.Text
                .IdEmpresa = CInt(cmbEmpresa.SelectedValue)
                .IdPersona = CInt(txtIdCliente.Text)
                .IdSerie = CInt(cboSerie.SelectedValue)
                .IdTienda = CInt(cmbTienda.SelectedValue)
                .IdTipoDocumento = CInt(cmbTipoDocumento.SelectedValue)


                .FechaEmision = CDate(Me.txtFechaEmision.Text)


                .Serie = cboSerie.SelectedItem.ToString
                .IdEstadoDoc = CInt(Me.cboEstadoDocumento.SelectedValue)
            End With
            Me.listaDetalle = getListaDetalle()
            Dim IdDocumentoOrdenDesp As Integer = objDocOrdenDespacho.InsertaOrdenDespacho(Me.listaDetalle, objdocumento, CInt(ViewState.Item("IdDocumento")), 1)
            If IdDocumentoOrdenDesp > 0 Then

                hddFrmModo.Value = "5"
                actualizarVistaFrm()

                ViewState.Add("IdDocumentoOrdenDesp", IdDocumentoOrdenDesp)
                Me.hddIdDocumento.Value = CStr(IdDocumentoOrdenDesp)
                objScript.mostrarMsjAlerta(Me, "La operación se realizó con éxito.")
            Else
                Throw New Exception
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub cmbEmpresa_B_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbEmpresa_B.SelectedIndexChanged
        cargarDatosCboTienda(cmbTienda_B, "B")
        cargarDatosSerie(cmbSerie_B, "B")
        LlenarCboTipoDocumento(cmbTipoDocumento_B, CInt(cmbEmpresa_B.SelectedValue), 2)
    End Sub
    Protected Sub cmbTienda_B_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbTienda_B.SelectedIndexChanged
        cargarDatosSerie(cmbSerie_B, "B")
    End Sub
    Protected Sub btnAceptar_B_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAceptar_B.Click
        cargarDatosOrdenDespachoxIdSeriexNroCodigo(CInt(cmbSerie_B.SelectedValue), CInt(txtCodigo_B.Text))
    End Sub
    Private Sub cargarDatosOrdenDespachoxIdSeriexNroCodigo(ByVal IdSerie As Integer, ByVal codigo As Integer)
        Try
            Dim objDocumento As New Negocio.Documento
            Dim IdDocumento As Integer = objDocumento.SelectIdDocumentoxIdSeriexCodigo(IdSerie, codigo)
            If IdDocumento <= 0 Then
                objScript.mostrarMsjAlerta(Me, "No se hallaron registros.")
            Else
                ViewState.Add("IdDocumento", IdDocumento)
                cargarDatosOrdenDespacho(IdDocumento)

            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub cmbTipoDocumento_B_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbTipoDocumento_B.SelectedIndexChanged
        cargarDatosSerie(cmbSerie_B, "B")
    End Sub
    Private Sub btnBuscarDocumento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumento.Click
        buscarDocumento(CInt(Me.cboSerie.SelectedValue), CInt(txtCodigoDocumento.Text))
    End Sub
    Private Sub buscarDocumento(ByVal IdSerie As Integer, ByVal codigo As Integer)
        Dim objOrdenDespacho As New Negocio.DocOrdenDespacho
        Try
            Dim obj As Entidades.DocOrdenDespacho = objOrdenDespacho.SelectOrdenDespachoxIdSeriexCodigo(IdSerie, codigo)
            If obj Is Nothing Then
                objScript.mostrarMsjAlerta(Me, "El documento no existe.")
                Return
            End If

            '***************** cargamos la cabecera
            txtDireccion.Text = obj.Direccion
            txtDNI.Text = obj.Dni
            txtRUC.Text = obj.Ruc
            txtIdCliente.Text = obj.IdPersona.ToString
            txtNombreCliente.Text = obj.getNombreMostrar

            '***************** cargamos el detalle
            Me.listaDetalle = objOrdenDespacho.SelectDetalleOrdenDespachoxIdDocumento(obj.IdDocumento)
            setListaDetalle(Me.listaDetalle)

            DGVDetalle.DataSource = Me.listaDetalle
            DGVDetalle.DataBind()

            '************ actualizo la presentacion de los botones y la cabecera           
            hddIdDocumento.Value = obj.IdDocumento.ToString

            hddFrmModo.Value = "4"
            actualizarVistaFrm()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cmbTipoDocumento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTipoDocumento.SelectedIndexChanged
        cargarDatosSerie(Me.cboSerie)
        GenerarCodigo()
    End Sub
    Private Sub actualizarOrdenDespacho(ByVal IdDocumento As Integer)
        Try
            saveEnListaDetalle()
            Me.listaDetalle = getListaDetalle()

            Dim objDocumento As New Entidades.Documento
            With objDocumento
                .IdEmpresa = CInt(cmbEmpresa.SelectedValue)
                .IdTienda = CInt(cmbTienda.SelectedValue)
                .Id = CInt(hddIdDocumento.Value)
            End With

            Dim objOrdenDespachoNegocio As New Negocio.DocOrdenDespacho
            If objOrdenDespachoNegocio.ActualizaOrdenDespacho(Me.listaDetalle, objDocumento) Then
                objScript.mostrarMsjAlerta(Me, "El Proceso finalizó con éxito.")


                hddFrmModo.Value = "5"
                actualizarVistaFrm()
            Else
                Throw New Exception
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAnular.Click
        anularOrdenDespacho()
    End Sub
    Private Sub anularOrdenDespacho()

        Try

            If (New Negocio.DocOrdenDespacho).AnularxIdDocumento(CInt(hddIdDocumento.Value)) Then
                hddFrmModo.Value = "6"
                actualizarVistaFrm()
                objScript.mostrarMsjAlerta(Me, "La Anulación se realizó con éxito.")
            Else
                Throw New Exception
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEditar.Click

        hddFrmModo.Value = "2"
        actualizarVistaFrm()

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        hddFrmModo.Value = "3"
        actualizarVistaFrm()
    End Sub

    Private Sub btnMostrarCapaBuscarDoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMostrarCapaBuscarDoc.Click

        cargarDocumentoxDespachar()


    End Sub
    Private Sub cargarDocumentoxDespachar()
        Try

            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).SelectDocxDespachar(CInt(Me.cmbTienda.SelectedValue))
            Me.DGV_DocxDespachar.DataSource = lista
            Me.DGV_DocxDespachar.DataBind()

            objScript.onCapa(Me, "capaDocxDespachar")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub DGV_DocxDespachar_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGV_DocxDespachar.PageIndexChanging
        DGV_DocxDespachar.PageIndex = e.NewPageIndex
        cargarDocumentoxDespachar()
    End Sub

    Private Sub DGV_DocxDespachar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_DocxDespachar.SelectedIndexChanged
        cargarDatosOrdenDespacho(CInt(CType(DGV_DocxDespachar.SelectedRow.FindControl("hddIdDocumentoxDespachar"), HiddenField).Value))
    End Sub

End Class