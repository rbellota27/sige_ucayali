﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmCampaniaTG
    Inherits System.Web.UI.Page

    Dim oCombo As Combo
    Dim objScript As New ScriptManagerClass
    Dim listaCampaniaDetalleTG As List(Of Entidades.Campania_DetalleTG)
    Dim listaCampaniaDetalleTGSave As List(Of Entidades.Campania_DetalleTG)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            onLoad_CampaniaTG()

        Else
            If IsNumeric(hddBusquedaProducto.Value) Then
                Select Case CInt(hddBusquedaProducto.Value)
                    Case 1 'Busqueda campaña
                        ViewState.Add("hddBusquedaProducto", hddBusquedaProducto.Value)
                    Case 2 'Busqueda transferencia
                        ViewState.Add("hddBusquedaProducto", hddBusquedaProducto.Value)
                End Select
            End If
        End If

    End Sub
    Private Sub onLoad_CampaniaTG()
        oCombo = New Combo
        With oCombo
            .LlenarCboEmpresaxIdUsuario(Me.ddl_empresa, CInt(Session("IdUsuario")), False)
            .llenarCboTiendaxIdEmpresa(Me.ddl_tienda, CInt(Me.ddl_empresa.SelectedValue), False)
            .llenarCboCampania(Me.ddl_campania, CInt(Me.ddl_empresa.SelectedValue), CInt(Me.ddl_tienda.SelectedValue))
            .llenarCboTipoExistencia(Me.CboTipoExistencia)
            If Not CboTipoExistencia.Items.FindByValue("1") Is Nothing Then Me.CboTipoExistencia.SelectedValue = "1"
            .llenarCboLineaxTipoExistencia(Me.cmbLinea_AddProd, CInt(Me.CboTipoExistencia.SelectedValue), True)
            .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.CboTipoExistencia.SelectedValue), True)
        End With

    End Sub

    Private Sub ddl_empresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_empresa.SelectedIndexChanged
        oCombo = New Combo
        oCombo.llenarCboTiendaxIdEmpresa(Me.ddl_tienda, CInt(Me.ddl_empresa.SelectedValue), False)
        oCombo.llenarCboCampania(Me.ddl_campania, CInt(Me.ddl_empresa.SelectedValue), CInt(Me.ddl_tienda.SelectedValue))
        DGV_AddProd.DataSource = Nothing
        DGV_AddProd.DataBind()
    End Sub
    Private Sub ddl_tienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_tienda.SelectedIndexChanged
        oCombo = New Combo
        oCombo.llenarCboCampania(Me.ddl_campania, CInt(Me.ddl_empresa.SelectedValue), CInt(Me.ddl_tienda.SelectedValue))
        DGV_AddProd.DataSource = Nothing
        DGV_AddProd.DataBind()
    End Sub

    Private Sub habilitarDDL(ByVal valor As Boolean)
        ddl_empresa.Enabled = valor
        ddl_tienda.Enabled = valor
        ddl_campania.Enabled = valor
    End Sub
    Private Sub limpiar_Frm()
        GV_Campania_Detalle.DataBind()
        GV_DetalleTG.DataBind()
        DGV_AddProd.DataBind()
        setListaCampaniaDetalle(Nothing)
        setListaCampaniaDetalleTG(Nothing)
        setListaCampaniaDetalleTGSave(Nothing)

    End Sub


#Region "************************** BUSQUEDA AVANZADA PRODUCTOS"
    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Protected Sub cmbSubLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        Try

            oCombo = New Combo
            oCombo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1

            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)

        Next

        Return dt

    End Function
#End Region
#Region "************************* Busqueda de producto"

    Dim listaCampaniaDetalle As List(Of Entidades.Campania_Detalle)

    Private Function getListaCampaniaDetalle() As List(Of Entidades.Campania_Detalle)
        Return CType(Session.Item("listaCampaniaDetalle"), List(Of Entidades.Campania_Detalle))
    End Function
    Private Sub setListaCampaniaDetalle(ByVal lista As List(Of Entidades.Campania_Detalle))
        Session.Remove("listaCampaniaDetalle")
        Session.Add("listaCampaniaDetalle", lista)
    End Sub

    Private Function getListaCampaniaDetalleTG() As List(Of Entidades.Campania_DetalleTG)
        Return CType(Session.Item("listaCampaniaDetalleTG"), List(Of Entidades.Campania_DetalleTG))
    End Function
    Private Sub setListaCampaniaDetalleTG(ByVal lista As List(Of Entidades.Campania_DetalleTG))
        Session.Remove("listaCampaniaDetalleTG")
        Session.Add("listaCampaniaDetalleTG", lista)
    End Sub

    Private Function getListaCampaniaDetalleTGSave() As List(Of Entidades.Campania_DetalleTG)
        Return CType(Session.Item("listaCampaniaDetalleTGSave"), List(Of Entidades.Campania_DetalleTG))
    End Function
    Private Sub setListaCampaniaDetalleTGSave(ByVal lista As List(Of Entidades.Campania_DetalleTG))
        Session.Remove("listaCampaniaDetalleTGSave")
        Session.Add("listaCampaniaDetalleTGSave", lista)
    End Sub

    Private Sub btnBuscarGrilla_AddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarGrilla_AddProd.Click
        onClick_BuscarGrilla_AddProd()
    End Sub
    Private Sub onClick_BuscarGrilla_AddProd()

        ViewState.Add("IdCampania", CInt(ddl_campania.SelectedValue))
        ViewState.Add("IdTipoExistencia", CInt(CboTipoExistencia.SelectedValue))
        ViewState.Add("IdLinea", CInt(cmbLinea_AddProd.SelectedValue))
        ViewState.Add("IdSubLinea", CInt(cmbSubLinea_AddProd.SelectedValue))
        ViewState.Add("prod_Codigo", CStr(txtCodigoProducto.Text.Trim))
        ViewState.Add("prod_Nombre", CStr(txtDescripcionProd_AddProd.Text.Trim))

        Select Case CInt(ViewState("hddBusquedaProducto"))
            Case 1 'Busqueda campaña
                onClick_BuscarProducto(0)
            Case 2 'Busqueda transferencia
                onClick_BuscarProducto2(0)
        End Select

    End Sub

    Private Sub paginar_BuscarGrilla_AddProd(ByVal IdCampania As Integer, ByVal IdtipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal prod_codigo As String, ByVal prod_nombre As String, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select


        Dim codigoProducto As String = Me.txtCodigoProducto.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()

        listaCampaniaDetalle = (New Negocio.Campania_Detalle).Campania_Detalle_Select(IdCampania, IdtipoExistencia, IdLinea, IdSubLinea, prod_codigo, prod_nombre, index, DGV_AddProd.PageSize, tableTipoTabla)

        If Me.listaCampaniaDetalle.Count = 0 Then

            DGV_AddProd.DataSource = Nothing
            DGV_AddProd.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');    alert('No se hallaron registros.');        ", True)
            Return

        Else

            setListaCampaniaDetalle(Me.listaCampaniaDetalle)
            DGV_AddProd.DataSource = Me.listaCampaniaDetalle
            DGV_AddProd.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');           ", True)

        End If

    End Sub
    Private Sub onClick_BuscarProducto(ByVal TipoMov As Integer)
        paginar_BuscarGrilla_AddProd(CInt(ViewState("IdCampania")), CInt(ViewState("IdTipoExistencia")), CInt(ViewState("IdLinea")), CInt(ViewState("IdSubLinea")), CStr(ViewState("prod_Codigo")), CStr(ViewState("prod_Nombre")), TipoMov)
    End Sub

    Private Sub paginar_BuscarGrilla_AddProd2(ByVal IdCampaniaDetalle As Integer, ByVal IdtipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal prod_codigo As String, ByVal prod_nombre As String, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select


        Dim codigoProducto As String = Me.txtCodigoProducto.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()
        Dim IdCampania As Integer = 0
        If cb_productoTG.Checked = True Then
            IdCampania = CInt(ddl_campania.SelectedValue)
        End If

        listaCampaniaDetalleTG = (New Negocio.Campania_DetalleTG).Campania_DetalleTGxIdCampania(0, IdCampania, IdtipoExistencia, IdLinea, IdSubLinea, prod_codigo, prod_nombre, index, DGV_AddProd.PageSize, tableTipoTabla)

        If Me.listaCampaniaDetalleTG.Count = 0 Then

            DGV_AddProd.DataSource = Nothing
            DGV_AddProd.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');    alert('No se hallaron registros.');        ", True)
            Return

        Else

            setListaCampaniaDetalleTG(Me.listaCampaniaDetalleTG)
            DGV_AddProd.DataSource = Me.listaCampaniaDetalleTG
            DGV_AddProd.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');           ", True)

        End If

    End Sub
    Private Sub onClick_BuscarProducto2(ByVal TipoMov As Integer)
        paginar_BuscarGrilla_AddProd2(0, CInt(ViewState("IdTipoExistencia")), CInt(ViewState("IdLinea")), CInt(ViewState("IdSubLinea")), CStr(ViewState("prod_Codigo")), CStr(ViewState("prod_Nombre")), TipoMov)
    End Sub

    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        Select Case CInt(ViewState("hddBusquedaProducto"))
            Case 1 'Busqueda campaña
                onClick_BuscarProducto(1)
            Case 2 'Busqueda transferencia
                onClick_BuscarProducto2(1)
        End Select
    End Sub
    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        Select Case CInt(ViewState("hddBusquedaProducto"))
            Case 1 'Busqueda campaña
                onClick_BuscarProducto(2)
            Case 2 'Busqueda transferencia
                onClick_BuscarProducto2(2)
        End Select
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        Select Case CInt(ViewState("hddBusquedaProducto"))
            Case 1 'Busqueda campaña
                onClick_BuscarProducto(3)
            Case 2 'Busqueda transferencia
                onClick_BuscarProducto2(3)
        End Select
    End Sub

#End Region


    Private Sub CboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboTipoExistencia.SelectedIndexChanged
        oCombo = New Combo
        With oCombo
            .llenarCboLineaxTipoExistencia(Me.cmbLinea_AddProd, CInt(Me.CboTipoExistencia.SelectedValue), True)
            .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.CboTipoExistencia.SelectedValue), True)
        End With

        objScript.onCapa(Me, "capaBuscarProducto_AddProd")
    End Sub
    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        oCombo = New Combo
        oCombo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.CboTipoExistencia.SelectedValue), True)

        objScript.onCapa(Me, "capaBuscarProducto_AddProd")
    End Sub

    Private Sub DGV_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_AddProd.SelectedIndexChanged

        Dim index As Integer = DGV_AddProd.SelectedIndex
        Try


            Select Case CInt(ViewState("hddBusquedaProducto"))
                Case 1 'Busqueda campaña

                    habilitarDDL(False)

                    cb_guardarAll.Checked = False
                    cb_productoTG.Checked = False

                    GV_Campania_Detalle.DataSource = ObtenerCampaniaDetalle(index)
                    GV_Campania_Detalle.DataBind()

                    GV_DetalleTG.DataBind()
                    setListaCampaniaDetalleTGSave(Nothing)

                    listaCampaniaDetalleTGSave = ObtenerCampaniaDetalleTGSave(listaCampaniaDetalle(index).IdCampaniaDetalle)
                    GV_DetalleTG.DataSource = listaCampaniaDetalleTGSave
                    GV_DetalleTG.DataBind()

                Case 2 'Busqueda transferencia

                    habilitarDDL(False)

                    listaCampaniaDetalleTGSave = ObtenerCampaniaDetalleTG(index)
                    GV_DetalleTG.DataSource = listaCampaniaDetalleTGSave
                    GV_DetalleTG.DataBind()

            End Select

            DGV_AddProd.DataSource = Nothing
            DGV_AddProd.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function ObtenerCampaniaDetalle(ByVal index As Integer) As List(Of Entidades.Campania_Detalle)
        Dim listaProducto As New List(Of Entidades.Campania_Detalle)

        Me.listaCampaniaDetalle = getListaCampaniaDetalle()

        listaCampaniaDetalle(index).ListaMoneda = (New Negocio.Moneda).SelectCbo
        listaCampaniaDetalle(index).PrecioActual = (New Negocio.ProductoTipoPV).SelectValorCalculadoxParams(listaCampaniaDetalle(index).IdProducto, CInt(ddl_tienda.SelectedValue), 1, listaCampaniaDetalle(index).IdUnidadMedida, listaCampaniaDetalle(index).IdMoneda)

        If Not ddl_campania.Items.FindByValue(CStr(listaCampaniaDetalle(index).IdCampania)) Is Nothing Then
            ddl_campania.SelectedValue = CStr(listaCampaniaDetalle(index).IdCampania)
        End If

        listaProducto.Add(listaCampaniaDetalle(index))

        Return listaProducto
    End Function
    Private Function ObtenerCampaniaDetalleTGSave(ByVal IdCampaniaDetalle As Integer) As List(Of Entidades.Campania_DetalleTG)

        actualizar_GV_DetalleTG()
        listaCampaniaDetalleTGSave = getListaCampaniaDetalleTGSave()

        If listaCampaniaDetalleTGSave Is Nothing Then listaCampaniaDetalleTGSave = New List(Of Entidades.Campania_DetalleTG)

        listaCampaniaDetalleTGSave.AddRange((New Negocio.Campania_DetalleTG).Campania_DetalleTGxIdCampaniaDetalle(IdCampaniaDetalle))

        For i As Integer = 0 To listaCampaniaDetalleTGSave.Count - 1
            listaCampaniaDetalleTGSave(i).ListaMoneda = (New Negocio.Moneda).SelectCbo
            listaCampaniaDetalleTGSave(i).PrecioActual = (New Negocio.ProductoTipoPV).SelectValorCalculadoxParams(listaCampaniaDetalleTGSave(i).IdProducto, CInt(ddl_tienda.SelectedValue), 1, listaCampaniaDetalleTGSave(i).IdUnidadMedida, listaCampaniaDetalleTGSave(i).IdMoneda)
        Next

        setListaCampaniaDetalleTGSave(listaCampaniaDetalleTGSave)

        Return listaCampaniaDetalleTGSave
    End Function
    Private Function ObtenerCampaniaDetalleTG(ByVal Index As Integer) As List(Of Entidades.Campania_DetalleTG)


        actualizar_GV_DetalleTG()
        listaCampaniaDetalleTGSave = getListaCampaniaDetalleTGSave()

        listaCampaniaDetalleTG = getListaCampaniaDetalleTG()

        Dim IdProducto As Integer = listaCampaniaDetalleTG(Index).IdProducto
        Dim ListaValidarCampaniaDetalleTG As List(Of Entidades.Campania_DetalleTG)

        If Not listaCampaniaDetalleTGSave Is Nothing Then
            If Not listaCampaniaDetalleTGSave.Find(Function(e As Entidades.Campania_DetalleTG) e.IdProducto = IdProducto) Is Nothing Then
                DGV_AddProd.DataBind()
                Throw New Exception("El producto esta en la lista de tranferencia gratutita") : Exit Function
            End If
        End If

        If listaCampaniaDetalleTG(Index).IdCampania = 0 Then
            ListaValidarCampaniaDetalleTG = (New Negocio.Campania_DetalleTG).Campania_DetalleTGxIdCampania(IdProducto, CInt(ddl_campania.SelectedValue), 0, 0, 0, "", "", 0, 15, Nothing)
            If Not ListaValidarCampaniaDetalleTG Is Nothing Then
                If ListaValidarCampaniaDetalleTG.Count > 0 Then
                    listaCampaniaDetalleTG.RemoveAt(Index)
                    listaCampaniaDetalleTG.Insert(Index, ListaValidarCampaniaDetalleTG(0))
                End If
            End If
        End If

        listaCampaniaDetalleTG(Index).ListaMoneda = (New Negocio.Moneda).SelectCbo

        Dim IdUnidaMedida As Integer
        Dim IdMoneda As Integer
        If listaCampaniaDetalleTG(Index).IdUnidadMedida = 0 Then
            IdUnidaMedida = listaCampaniaDetalleTG(Index).getListaUnidadMedida(0).Id
        Else
            IdUnidaMedida = listaCampaniaDetalleTG(Index).IdUnidadMedida
        End If
        If listaCampaniaDetalleTG(Index).IdMoneda = 0 Then
            IdMoneda = listaCampaniaDetalleTG(Index).ListaMoneda(0).Id
        Else
            IdMoneda = listaCampaniaDetalleTG(Index).IdMoneda
        End If

        listaCampaniaDetalleTG(Index).PrecioActual = (New Negocio.ProductoTipoPV).SelectValorCalculadoxParams(listaCampaniaDetalleTG(Index).IdProducto, CInt(ddl_tienda.SelectedValue), 1, IdUnidaMedida, IdMoneda)



        If listaCampaniaDetalleTGSave Is Nothing Then listaCampaniaDetalleTGSave = New List(Of Entidades.Campania_DetalleTG)

        listaCampaniaDetalleTGSave.Add(listaCampaniaDetalleTG(Index))

        setListaCampaniaDetalleTGSave(listaCampaniaDetalleTGSave)

        Return listaCampaniaDetalleTGSave
    End Function

    Private Sub GV_Campania_Detalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Campania_Detalle.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim cboUnidadMedida As DropDownList = CType(e.Row.FindControl("cboUnidadMedida"), DropDownList)

            If (cboUnidadMedida.Items.FindByValue(CStr(Me.listaCampaniaDetalle(e.Row.RowIndex).IdUnidadMedida)) IsNot Nothing) Then

                cboUnidadMedida.SelectedValue = CStr(Me.listaCampaniaDetalle(e.Row.RowIndex).IdUnidadMedida)

            End If

            Dim cboMoneda As DropDownList = CType(e.Row.FindControl("cboMoneda"), DropDownList)

            If (cboMoneda.Items.FindByValue(CStr(Me.listaCampaniaDetalle(e.Row.RowIndex).IdMoneda)) IsNot Nothing) Then

                cboMoneda.SelectedValue = CStr(Me.listaCampaniaDetalle(e.Row.RowIndex).IdMoneda)

            End If


            cboUnidadMedida.Enabled = False
            cboMoneda.Enabled = False



        End If
    End Sub



    Private Sub actualizar_GV_DetalleTG()
        listaCampaniaDetalleTGSave = getListaCampaniaDetalleTGSave()

        For i As Integer = 0 To Me.GV_DetalleTG.Rows.Count - 1

            Me.listaCampaniaDetalleTGSave(i).IdUnidadMedida = CInt(CType(Me.GV_DetalleTG.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedValue)
            Me.listaCampaniaDetalleTGSave(i).IdMoneda = CInt(CType(Me.GV_DetalleTG.Rows(i).FindControl("cboMoneda"), DropDownList).SelectedValue)
            Me.listaCampaniaDetalleTGSave(i).cdtg_Precio = CDec(CType(Me.GV_DetalleTG.Rows(i).FindControl("txtPrecioCampania"), TextBox).Text)
            Me.listaCampaniaDetalleTGSave(i).cdtg_Cantidad = CDec(CType(Me.GV_DetalleTG.Rows(i).FindControl("txtCantidadMin"), TextBox).Text)
            Me.listaCampaniaDetalleTGSave(i).cdtg_cantCampania = CBool(CType(Me.GV_DetalleTG.Rows(i).FindControl("cb_cantCampania"), CheckBox).Checked)

        Next

        setListaCampaniaDetalleTGSave(listaCampaniaDetalleTGSave)

    End Sub

    Private Sub GV_DetalleTG_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_DetalleTG.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cboUnidadMedida As DropDownList = CType(e.Row.FindControl("cboUnidadMedida"), DropDownList)

            If (cboUnidadMedida.Items.FindByValue(CStr(Me.listaCampaniaDetalleTGSave(e.Row.RowIndex).IdUnidadMedida)) IsNot Nothing) Then

                cboUnidadMedida.SelectedValue = CStr(Me.listaCampaniaDetalleTGSave(e.Row.RowIndex).IdUnidadMedida)

            End If

            Dim cboMoneda As DropDownList = CType(e.Row.FindControl("cboMoneda"), DropDownList)

            If (cboMoneda.Items.FindByValue(CStr(Me.listaCampaniaDetalleTGSave(e.Row.RowIndex).IdMoneda)) IsNot Nothing) Then

                cboMoneda.SelectedValue = CStr(Me.listaCampaniaDetalleTGSave(e.Row.RowIndex).IdMoneda)

            End If

        End If
    End Sub

    Private Sub GV_DetalleTG_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DetalleTG.SelectedIndexChanged
        OnSelect_GV_DetalleTG(GV_DetalleTG.SelectedIndex)
    End Sub
    Private Sub OnSelect_GV_DetalleTG(ByVal index As Integer)

        actualizar_GV_DetalleTG()

        listaCampaniaDetalleTGSave = getListaCampaniaDetalleTGSave()
        listaCampaniaDetalleTGSave.RemoveAt(index)
        setListaCampaniaDetalleTGSave(listaCampaniaDetalleTGSave)

        If listaCampaniaDetalleTGSave.Count <= 0 Then btnAnterior_Productos.Enabled = True
        GV_DetalleTG.DataSource = listaCampaniaDetalleTGSave
        GV_DetalleTG.DataBind()

    End Sub



    Private Sub btn_nuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_nuevo.Click
        limpiar_Frm()
        habilitarDDL(True)
    End Sub

    Private Sub btn_guardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_guardar.Click
        Try

            Dim IdCampaniaDetalle, IdCampania As Integer
            Dim nCampaniaDetalleTG As New Negocio.Campania_DetalleTG

            actualizar_GV_DetalleTG()
            listaCampaniaDetalleTGSave = getListaCampaniaDetalleTGSave()

            If cb_guardarAll.Checked = False Then

                If GV_Campania_Detalle.Rows.Count > 0 Then
                    IdCampaniaDetalle = CInt(CType(GV_Campania_Detalle.Rows(0).FindControl("hddIdCampaniaDetalle"), HiddenField).Value)
                    IdCampania = CInt(CType(GV_Campania_Detalle.Rows(0).FindControl("hddIdCampania"), HiddenField).Value)
                End If

                nCampaniaDetalleTG.Campania_DetalleTG_Transaction(IdCampania, 0, IdCampaniaDetalle, listaCampaniaDetalleTGSave)

            Else

                For i As Integer = 0 To listaCampaniaDetalleTGSave.Count - 1
                    listaCampaniaDetalleTGSave(i).IdCampania = CInt(ddl_campania.SelectedValue)
                Next

                nCampaniaDetalleTG.Campania_DetalleTG_TransactionALL(listaCampaniaDetalleTGSave)

            End If



            objScript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub btn_cancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_cancelar.Click
        limpiar_Frm()
        habilitarDDL(True)
    End Sub

    Private Sub btnCerrar_capaAddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCerrar_capaAddProd.Click
        DGV_AddProd.DataSource = Nothing
        DGV_AddProd.DataBind()
    End Sub
End Class