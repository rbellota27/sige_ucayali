﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="frmAprobarCotizacion.aspx.vb" Inherits="APPWEB.frmAprobarCotizacion" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div style="width:100%">
    <div>
        <table>
            <tr>
                <td class="TituloCelda" >
                APROBAR COTIZACIÓN
                </td>
            </tr>
            <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto" style="font-weight:bold">
                            Empresa:</td>
                        <td>
                            <asp:DropDownList ID="ddlEmpresa" runat="server"  AutoPostBack="true" DataTextField="campo2" DataValueField="campo1" >
                            </asp:DropDownList>
                        </td>
                        <td class="Texto" style="font-weight:bold" >
                            Tienda:</td>
                        <td>
                            <asp:DropDownList ID="ddlTienda" runat="server" DataTextField="campo2" DataValueField="campo1"  >
                            </asp:DropDownList>
                        </td>
                        <td></td>
                        <td></td>
                        <td class="Texto" style="font-weight:bold" >
                            Aprobación:</td>
                        <td>
                            <asp:DropDownList ID="ddlAprobacion" runat="server">
                            <asp:ListItem Value="0" Selected="True">Por Aprobar</asp:ListItem>
                            <asp:ListItem Value="1" >Aprobados</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" Width="70px" ToolTip="Buscar" CssClass="btnBuscar"
                            OnClientClick="this.disabled=true;this.value='Procesando...'"  UseSubmitBehavior="false"/>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="Texto" style="font-weight:bold">
                            Fecha Inicio:</td>
                        <td colspan="8">
                        <table>
                        <tr>
                        <td>
                            <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="TextBox_Fecha" 
                                onblur="return(  valFecha_Blank(this) );" Width="100px"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="txtFechaInicio_MaskedEditExtender0" runat="server" 
                                ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" 
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                Mask="99/99/9999" TargetControlID="txtFechaInicio">
                            </cc1:MaskedEditExtender>
                            <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFechaInicio" >
                            </cc1:CalendarExtender>
                        </td>
                        <td class="Texto" style="font-weight:bold" >Fecha Fin:</td>
                        <td>
                           <asp:TextBox ID="txtFechaFin" runat="server" CssClass="TextBox_Fecha" 
                                onblur="return(  valFecha_Blank(this) );" Width="100px"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="txtFechaFin_MaskedEditExtender" runat="server" 
                                ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" 
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                Mask="99/99/9999" TargetControlID="txtFechaFin">
                            </cc1:MaskedEditExtender>
                            <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFechaFin" >
                            </cc1:CalendarExtender>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        </tr>
                        </table>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="15">
                <table style="width:100%">
                    <tr>
                        <td>
                            <asp:GridView ID="gv_CotizacionAprobar" runat="server" AutoGenerateColumns="false" DataKeyNames="id" Width="100%">
                            <RowStyle CssClass="GrillaRow" />
                            <HeaderStyle CssClass="GrillaHeader" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <span><%#Container.DataItemIndex + 1 %></span>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="NomTipoDocumento" HeaderText="Tipo Doc." />
                                <asp:BoundField DataField="Tienda" HeaderText="Tienda" />
                                <asp:BoundField DataField="NroDocumento" HeaderText="Número Doc." ItemStyle-ForeColor="Red" />
                                <asp:BoundField DataField="FechaEmision" HeaderText="Fec.Emisión" />
                                <asp:BoundField DataField="FechaVenc" HeaderText="Fec.Venc" />
                                <asp:BoundField DataField="DescripcionPersona" HeaderText="Cliente" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Button ID="btnAprobar" runat="server" Text="Aprobar" Visible='<%# If(Eval("Aprobar"), false, true) %>'
                                         CommandName="btnAprobar" CommandArgument='<%#Container.DataItemIndex %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        </table>
    </div>
</div>
</asp:Content>
