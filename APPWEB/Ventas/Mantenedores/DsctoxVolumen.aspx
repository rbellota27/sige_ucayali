<%@ Page Title="Dscto x Volumen" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="DsctoxVolumen.aspx.vb" Inherits="APPWEB.DsctoxVolumen" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td class="TituloCelda">
                Descuento Por Volumen de Venta
            </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Empresa:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="true" Font-Bold="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            Tienda:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboTienda" runat="server" Font-Bold="true">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table>
                                    <tr>
                                        <td class="Texto" style="font-weight: bold;">
                                            Replicar x Tienda ( Aplicable x Producto ) �
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboTiendaReplica" runat="server" Font-Bold="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnAddTiendaRef" runat="server" Text="Agregar" ToolTip="Agregar las tiendas a replicar la campa�a."
                                                OnClientClick=" return ( onclick_AddTiendaRef() ); " />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnLimpiarTienda" runat="server" Text="Limpiar" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:GridView ID="GV_TiendaReplica" runat="server" AutoGenerateColumns="False" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Height="25px" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:LinkButton ID="btnQuitarTienda" runat="server" OnClick="btnQuitarTienda_Click">Quitar</asp:LinkButton>
                                                                    </td>
                                                                    <td>
                                                                        <asp:HiddenField ID="hddIdTienda" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>' />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Center" Height="25px"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Nombre" HeaderText="Tienda" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <fieldset>
                                    <legend><span class="Texto"></span></legend>
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Perfil:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboPerfil" runat="server" Font-Bold="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                  
                                        <tr>
                                            <td class="Texto">
                                                Cond. Pago:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboCondicionPago" runat="server" AutoPostBack="true" Font-Bold="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="Texto">
                                                Medio Pago:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboMedioPago" runat="server" Font-Bold="true" >
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Texto">
                                                Tipo Precio:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTipoPrecioV" runat="server" Font-Bold="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <fieldset>
                                    <legend><span class="Texto">Rangos de Venta</span></legend>
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Inicio �
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaInicio" runat="server" Width="70px" CssClass="TextBox_Fecha"
                                                    Font-Bold="true" onBlur=" return ( valFecha_Blank(this) ); "></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaInicio">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fin �
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaFin" runat="server" Width="70px" CssClass="TextBox_Fecha"
                                                    Font-Bold="true" onBlur=" return ( valFecha_Blank(this) ); "></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaFin">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Cantidad Min.:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCantidadMin" runat="server" Font-Bold="true" onKeyPress="return(  validarNumeroPuntoPositivo('event')  );"
                                                    onFocus="return(  aceptarFoco(this)  );" Width="60px">0</asp:TextBox>
                                            </td>
                                            <td class="Texto">
                                                Cantidad Max.:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCantidadMax" runat="server" Font-Bold="true" onKeyPress="return(  validarNumeroPuntoPositivo('event')  );"
                                                    onFocus="return(  aceptarFoco(this)  );" Width="60px">0</asp:TextBox>
                                            </td>
                                            <td class="Texto">
                                                % Dscto:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPorcentajeDscto" runat="server" Font-Bold="true" onKeyPress="return(  validarNumeroPuntoPositivo('event')  );"
                                                    onFocus="return(  aceptarFoco(this)  );" Width="60px">0</asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdateP_General" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <cc1:TabContainer ID="TabC_Dscto" runat="server" AutoPostBack="true">                                                
                            <cc1:TabPanel ID="TabP_DsctoGeneral" Visible="false" runat="server" HeaderText="L�nea - SubL�nea">
                                <ContentTemplate>
                                    <fieldset>
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btnRegistrarGeneral" runat="server" Text="Guardar" OnClientClick=" return (  Guardar_LS()  ); " />
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnLimpiar_Generalidades" runat="server" Text="Limpiar" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="TituloCelda">
                                                    L�nea - SubL�nea
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td style="color: #4277AD; text-align: right;">
                                                                L�nea:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="cboLinea" runat="server" AutoPostBack="True" 
                                                                    Font-Bold="True">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="color: #4277AD; text-align: right;">
                                                                Subl�nea:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="cboSubLinea" runat="server" Font-Bold="True">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnAgregar_Generalidades" runat="server" Text="Agregar" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:GridView ID="GV_General" runat="server" AutoGenerateColumns="False" Width="100%">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lblItem" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:LinkButton ID="lkbQuitar_General" runat="server" OnClick="lkbQuitar_General_Click">Quitar</asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Empresa">
                                                                <ItemTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lblEmpresa" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Empresa") %>'></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:HiddenField ID="hddIdEmpresa" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdEmpresa") %>' />
                                                                            </td>
                                                                            <td>
                                                                                <asp:HiddenField ID="hddIdTienda" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTienda") %>' />
                                                                            </td>
                                                                            <td>
                                                                                <asp:HiddenField ID="hddIdPerfil" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdPerfil") %>' />
                                                                            </td>
                                                                            <td>
                                                                                <asp:HiddenField ID="hddIdCondicionPago" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdCondicionPago") %>' />
                                                                            </td>
                                                                            <td>
                                                                                <asp:HiddenField ID="hddIdMedioPago" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdMedioPago") %>' />
                                                                            </td>
                                                                            <td>
                                                                                <asp:HiddenField ID="hddIdLinea" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdLinea") %>' />
                                                                            </td>
                                                                            <td>
                                                                                <asp:HiddenField ID="hddIdSubLinea" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdSubLinea") %>' />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Tienda" DataField="Tienda" />
                                                            <asp:BoundField HeaderText="Perfil" DataField="Perfil" />
                                                            <asp:BoundField HeaderText="Cond. Pago" DataField="CondicionPago" />
                                                            <asp:BoundField HeaderText="Medio Pago" DataField="MedioPago" />
                                                            <asp:BoundField HeaderText="L�nea" DataField="Linea" />
                                                            <asp:BoundField HeaderText="SubL�nea" DataField="SubLinea" />
                                                        </Columns>
                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                        <FooterStyle CssClass="GrillaFooter" />
                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                        <PagerStyle CssClass="GrillaPager" />
                                                        <RowStyle CssClass="GrillaRow" />
                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px">
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="SubTituloCelda">
                                                    Consultar
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <fieldset>
                                                        <table width="100%">
                                                            <tr>
                                                                <td align="center">
                                                                    <table>
                                                                        <tr>
                                                                            <td style="color: #4277AD; text-align: right;">
                                                                                Empresa:
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="cboEmpresaLS" runat="server" AutoPostBack="True">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td style="color: #4277AD; text-align: right;">
                                                                                Tienda:
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="cboTiendaLS" runat="server">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <fieldset>
                                                                        <legend><span class="Texto"></span></legend>
                                                                        <table>
                                                                            <tr>
                                                                                <td style="color: #4277AD; text-align: right;">
                                                                                    Perfil:
                                                                                </td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="cboPerfilLS" runat="server">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="color: #4277AD; text-align: right;">
                                                                                    Cond. Pago:
                                                                                </td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="cboCondicionPagoLS" runat="server" AutoPostBack="True">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                                <td style="color: #4277AD; text-align: right;">
                                                                                    Medio Pago:
                                                                                </td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="cboMedioPagoLS" runat="server">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="color: #4277AD; text-align: right;">
                                                                                    Tipo Precio:
                                                                                </td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="cboTipoPrecioVLS" runat="server">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </fieldset>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <fieldset>
                                                                        <legend><span style="color: #4277AD;">Rangos de Venta</span></legend>
                                                                        <table>
                                                                            <tr>
                                                                                <td style="color: #4277AD; text-align: right;">
                                                                                    Inicio �
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtFechaInicioLS" runat="server" Width="70px" ForeColor="Red" BackColor="HighlightText"
                                                                                        onBlur=" return ( valFecha_Blank(this) ); "></asp:TextBox>
                                                                                    <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" ClearMaskOnLostFocus="False"
                                                                                        CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                                                        CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                                                        CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" 
                                                                                        TargetControlID="txtFechaInicioLS">
                                                                                    </cc1:MaskedEditExtender>
                                                                                    <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                                                        TargetControlID="txtFechaInicioLS">
                                                                                    </cc1:CalendarExtender>
                                                                                </td>
                                                                                <td style="color: #4277AD; text-align: right;">
                                                                                    Fin �
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtFechaFinLS" runat="server" Width="70px" ForeColor="Red" BackColor="HighlightText"
                                                                                        onBlur=" return ( valFecha_Blank(this) ); "></asp:TextBox>
                                                                                    <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" ClearMaskOnLostFocus="False"
                                                                                        CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                                                        CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                                                        CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" 
                                                                                        TargetControlID="txtFechaFinLS">
                                                                                    </cc1:MaskedEditExtender>
                                                                                    <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                                                        TargetControlID="txtFechaFinLS">
                                                                                    </cc1:CalendarExtender>
                                                                                </td>
                                                                                <td style="color: #4277AD; text-align: right;">
                                                                                    Cantidad Min.:
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtCantidadMinLS" runat="server" Font-Bold="True" onKeyPress="return(  validarNumeroPuntoPositivo('event')  );"
                                                                                        onFocus="return(  aceptarFoco(this)  );" Width="60px">0</asp:TextBox>
                                                                                </td>
                                                                                <td style="color: #4277AD; text-align: right;">
                                                                                    Cantidad Max.:
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtCantidadMaxLS" runat="server" Font-Bold="True" onKeyPress="return(  validarNumeroPuntoPositivo('event')  );"
                                                                                        onFocus="return(  aceptarFoco(this)  );" Width="60px">0</asp:TextBox>
                                                                                </td>
                                                                                <td style="color: #4277AD; text-align: right;">
                                                                                    % Dscto:
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtPorcentajeDsctoLS" runat="server" Font-Bold="True" onKeyPress="return(  validarNumeroPuntoPositivo('event')  );"
                                                                                        onFocus="return(  aceptarFoco(this)  );" Width="60px">0</asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </fieldset>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <table>
                                                        <tr>
                                                            <td style="color: #4277AD; text-align: right;">
                                                                L�nea:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="cboLineaLS" runat="server" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="color: #4277AD; text-align: right;">
                                                                Subl�nea:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="cboSubLineaLS" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <asp:RadioButtonList ID="rbActivoG" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Text="Activo" Value="1" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnConsultarGeneral" runat="server" Text="Buscar" />
                                                            </td>
                                                          
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="SubTituloCelda">
                                                    Actualizar
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td style="color: #4277AD; text-align: right;">
                                                                Fecha Ini �
                                                            </td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtFechaIniLS_U" runat="server" onblur="return(  valFecha_Blank(this) );"
                                                                    Width="70px" Font-Bold="True" BackColor="HighlightText"></asp:TextBox>
                                                                <cc1:CalendarExtender ID="CalendarExtender9" runat="server" 
                                                                    TargetControlID="txtFechaIniLS_U" Enabled="True">
                                                                </cc1:CalendarExtender>
                                                            </td>
                                                            <td style="color: #4277AD; text-align: right;">
                                                                Fecha Fin �
                                                            </td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtFechaFinLS_U" runat="server" onblur="return(  valFecha_Blank(this) );"
                                                                    Width="70px" Font-Bold="True" BackColor="HighlightText"></asp:TextBox>
                                                                <cc1:CalendarExtender ID="CalendarExtender7" runat="server" 
                                                                    TargetControlID="txtFechaFinLS_U" Enabled="True">
                                                                </cc1:CalendarExtender>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="Button2" runat="server" Text="Aceptar" OnClientClick="return ( onclick_FechaLS() );"
                                                                    Style="cursor: Hand;" ToolTip="Cambiar la fechas a la cuadricula de productos" />
                                                            </td>
                                                            <td style="color: #4277AD; text-align: right;">
                                                                % Dscto �
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtPorcentajeDsctoLS_U" runat="server" Width="90px" onKeyPress="return ( validarNumeroPuntoPositivo('event') );"
                                                                    Font-Bold="True" BackColor="HighlightText" 
                                                                    onFocus="return ( aceptarFoco(this) );">0</asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="Button5" runat="server" Text="Aceptar" OnClientClick="return ( onclick_DsctoLS_U() );"
                                                                    Style="cursor: Hand;" ToolTip="Cambiar la fechas a la cuadricula de productos" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="1" cellpadding="2">
                                                        <tr>
                                                            <td style="color: #4277AD; text-align: right;">
                                                                Cant. Min. �
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtCantidadMinLS_U" runat="server" Width="90px" onKeyPress="return ( validarNumeroPuntoPositivo('event') );"
                                                                    Font-Bold="True" BackColor="HighlightText" 
                                                                    onFocus="return ( aceptarFoco(this) );">0</asp:TextBox>
                                                            </td>
                                                            <td style="color: #4277AD; text-align: right;">
                                                                Cant. Max. �
                                                            </td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtCantidadMaxLS_U" runat="server" Width="90px" onKeyPress="return ( validarNumeroPuntoPositivo('event') );"
                                                                    Font-Bold="True" BackColor="HighlightText" 
                                                                    onFocus="return ( aceptarFoco(this) );">0</asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="Button6" runat="server" Text="Aceptar" OnClientClick="return ( onclick_CantidadLS_U() );"
                                                                    Style="cursor: Hand;" ToolTip="Cambiar las cantidades de descuentos a la cuadricula de productos" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="BtnUpdateEstadoG" runat="server" Text="Actualizar" OnClientClick=" return ( confirm('Esta seguro de continuar con la operaci�n.') ); " />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="GV_GeneralView" runat="server" AutoGenerateColumns="False" Width="100%">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lblItem" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:HiddenField ID="hddIdDsctoVolVtas" runat="server" Value='<%# DataBinder.eval(Container.DataItem,"IdDsctoVolVtas") %>' />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Tienda" DataField="Tienda" NullDisplayText="------" />
                                                            <asp:BoundField HeaderText="Perfil" DataField="Perfil" NullDisplayText="------" />
                                                            <asp:BoundField HeaderText="Cond. Pago" DataField="CondicionPago" NullDisplayText="------" />
                                                            <asp:BoundField HeaderText="Medio Pago" DataField="MedioPago" NullDisplayText="------" />
                                                            <asp:BoundField HeaderText="L�nea" DataField="Linea" NullDisplayText="------" />
                                                            <asp:BoundField HeaderText="SubL�nea" DataField="SubLinea" NullDisplayText="------" />
                                                            <asp:TemplateField HeaderText="% Dscto">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtPorcenajeDsctoP_U" runat="server" Width="55px" Style="text-align: right"
                                                                        onFocus="return ( aceptarFoco(this) );" onKeyPress="return ( validarNumeroPuntoPositivo('event') );"
                                                                        Text='<%# DataBinder.Eval(Container.DataItem,"Dscto","{0:F2}") %>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Cant. Min">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtCantidadMin_PGV" runat="server" Width="60px" Style="text-align: right"
                                                                        onFocus="return ( aceptarFoco(this) );" onKeyPress="return ( validarNumeroPuntoPositivo('event') );"
                                                                        Text='<%# DataBinder.Eval(Container.DataItem,"CantMin","{0:F4}") %>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Cant. Max">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtCantidadMax_PGV" runat="server" Width="60px" Style="text-align: right"
                                                                        onFocus="return ( aceptarFoco(this) );" onKeyPress="return ( validarNumeroPuntoPositivo('event') );"
                                                                        Text='<%# DataBinder.Eval(Container.DataItem,"CantMax","{0:F4}") %>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="F. Ini">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtFechaIni_PGV" runat="server" Width="65px" onblur="return(  valFecha_Blank(this) );"
                                                                        Text='<%# DataBinder.Eval(Container.DataItem,"FechaIni","{0:d}") %>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="F. Fin">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtFechaFin_PGV" runat="server" Width="65px" onblur="return(  valFecha_Blank(this) );"
                                                                        Text='<%# DataBinder.Eval(Container.DataItem,"FechaFin","{0:d}") %>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="ckEstado" runat="server" Checked='<%# DataBinder.eval(Container.DataItem,"Estado") %>' />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                Est.
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="CheckBox1" runat="server" OnClick="selectAllG(this)" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </HeaderTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="No Afectar">
                                                                <ItemTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="ckNoAfectar" runat="server" Checked='<%# DataBinder.eval(Container.DataItem,"NoAfectar") %>' />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                        <FooterStyle CssClass="GrillaFooter" />
                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                        <PagerStyle CssClass="GrillaPager" />
                                                        <RowStyle CssClass="GrillaRow" />
                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Button TabIndex="212" ID="btnAnterior_GeneralView" runat="server" Font-Bold="True"
                                                                    Width="50px" Text="<" ToolTip="P�gina Anterior" 
                                                                    OnClientClick="return(valNavegacionGeneralView('0'));" />
                                                            </td>
                                                            <td>
                                                                <asp:Button TabIndex="213" ID="btnSiguiente_GeneralView" runat="server" Font-Bold="True"
                                                                    Width="50px" Text=">" ToolTip="P�gina Posterior" 
                                                                    OnClientClick="return(valNavegacionGeneralView('1'));" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox TabIndex="214" ID="txtPageIndex_GeneralView" Width="50px" ReadOnly="True"
                                                                    CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Button TabIndex="215" ID="btnIr_GeneralView" runat="server" Font-Bold="True"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la P�gina" 
                                                                    OnClientClick="return(valNavegacionGeneralView('2'));" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox TabIndex="216" ID="txtPageIndexGO_GeneralView" Width="50px" CssClass="TextBox_ReadOnly"
                                                                    runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="cboPageSizeGView" runat="server">
                                                                    <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                                    <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </ContentTemplate>
                            </cc1:TabPanel>
                            <cc1:TabPanel ID="TabP_DsctoDetallado" runat="server" HeaderText="Producto">
                                <ContentTemplate>
                                    <fieldset>
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btnRegistrar" runat="server" Text="Guardar" OnClientClick=" return (  Guardar_P()  ); " />
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnLimpiar_Detallado" runat="server" Text="Limpiar" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="TituloCelda">
                                                    Producto
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:ImageButton ID="btn_buscarproductos" runat="server" ImageUrl="~/Imagenes/BuscarProducto_B.jpg"
                                                        onmouseout="this.src='../../Imagenes/BuscarProducto_B.JPG';" onmouseover="this.src='../../Imagenes/BuscarProducto_A.JPG';"
                                                        OnClientClick="return ( Buscar_Producto() );" ToolTip="Adicionar registros a la cuadricula de productos" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <fieldset>
                                                        <legend><span style="color: #4277AD; font-weight: bold;">Atributo de Producto</span>
                                                        </legend>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="btnOpen_Atributo" runat="server" Text="Agregar Atributo" ToolTip="Buscar Atributos"
                                                                        OnClientClick=" return ( onCapa('Div_Tabla') ); " />
                                                                    <asp:Button ID="btnConsultar_Atributo" runat="server" Text="Consultar" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <asp:GridView ID="GV_FiltroTipoTablaValor" runat="server" AutoGenerateColumns="False"
                                                                        Width="650px">
                                                                        <Columns>
                                                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                                                            <asp:TemplateField HeaderText="Atributo">
                                                                                <ItemTemplate>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomTabla")%>'></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:HiddenField ID="hddIdTipoTablaValor" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTablaValor")%>' />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Valor">
                                                                                <ItemTemplate>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="lblTipoTablaValor" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <RowStyle CssClass="GrillaRow" />
                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:GridView ID="GV_Detalle" runat="server" AutoGenerateColumns="False" Width="100%">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                                <asp:Label ID="lblItem" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Cod.">
                                                                <ItemTemplate>
                                                                                <asp:Label ID="lblcodigoProducto" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CodigoProducto") %>'></asp:Label>

                                                                                <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdProducto") %>' />

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Producto" DataField="Producto" />
                                                            <asp:TemplateField HeaderText="Inicio">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtFechaInicio" runat="server" Width="75px" Text = '<%# DataBinder.Eval(Container.DataItem,"FechaIni") %>'></asp:TextBox>
                                                                    <cc1:CalendarExtender ID="ceFecInicio" runat="server" TargetControlID="txtFechaInicio" Format="dd/MM/yyyy" DefaultView="Days"></cc1:CalendarExtender>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Fin">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtFechaFinal" runat="server" Width="75px" Text='<%# DataBinder.Eval(Container.DataItem,"FechaFin") %>'></asp:TextBox>
                                                                    <cc1:CalendarExtender ID="ceFecFin" runat="server" TargetControlID="txtFechaFinal" Format="dd/MM/yyyy" DefaultView="Days"></cc1:CalendarExtender>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="U.M." DataField="ProductoUM" />
                                                            <asp:TemplateField HeaderText="Precio">
                                                                <ItemTemplate>
                                                                                <asp:Label ID="lblMoneda" Font-Bold="true" ForeColor="red" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Moneda") %>'></asp:Label>

                                                                                <asp:Label ID="lblPrecio" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"ProductoPV","{0:F2}") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Cant. M�nima">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtCantidadMinima" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container.DataItem,"CantMin") %>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Cant. M�xima">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtCantidadMaxima" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container.DataItem,"CantMax") %>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="% Dscto.">
                                                                <ItemTemplate>
                                                                                <asp:TextBox ID="lblDscto" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Dscto","{0:F2}") %>' Width="50px" onkeyup="calcularDescuento();"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="P. Dscto.">
                                                                <ItemTemplate>
                                                                                <asp:Label ID="lblPrecioDscto" Font-Bold="true" ForeColor="red" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"PrecioDscto","{0:F2}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="No Afectar">
                                                                <ItemTemplate>
                                                                                <asp:CheckBox ID="ckNoAfectar" runat="server" Checked='<%# DataBinder.eval(Container.DataItem,"NoAfectar") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Excepcion">
                                                                <ItemTemplate>
                                                                                <asp:CheckBox ID="ckExcepcion" runat="server" Checked='<%# DataBinder.eval(Container.DataItem,"Excepcion") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                        <FooterStyle CssClass="GrillaFooter" />
                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                        <PagerStyle CssClass="GrillaPager" />
                                                        <RowStyle CssClass="GrillaRow" />
                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Button TabIndex="212" ID="btnAnterior_Detalle" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionDetalle('0'));" />
                                                            </td>
                                                            <td>
                                                                <asp:Button TabIndex="213" ID="btnSiguiente_Detalle" runat="server" Font-Bold="true"
                                                                    Width="50px" Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionDetalle('1'));" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox TabIndex="214" ID="txtPageIndex_Detalle" Width="50px" ReadOnly="true"
                                                                    CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Button TabIndex="215" ID="btnIr_Detalle" runat="server" Font-Bold="true" Width="50px"
                                                                    Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionDetalle('2'));" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox TabIndex="216" ID="txtPageIndexGO_Detalle" Width="50px" CssClass="TextBox_ReadOnly"
                                                                    runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="cboPageSize" runat="server">
                                                                    <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                                    <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px">
                                                    <hr />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="SubTituloCelda">
                                                    Cosultar
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <table>
                                                        <tr>
                                                            <td style="color: #4277AD; text-align: right;">
                                                                Empresa:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="cboEmpresaP" runat="server" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="color: #4277AD; text-align: right;">
                                                                Tienda:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="cboTiendaP" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <fieldset>
                                                        <legend><span class="Texto"></span></legend>
                                                        <table>
                                                            <tr>
                                                                <td style="color: #4277AD; text-align: right;">
                                                                    Perfil:
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="cboPerfilP" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="color: #4277AD; text-align: right;">
                                                                    Cond. Pago:
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="cboCondicionPagoP" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td style="color: #4277AD; text-align: right;">
                                                                    Medio Pago:
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="cboMedioPagoP" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="color: #4277AD; text-align: right;">
                                                                    Tipo Precio:
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="cboTipoPrecioVP" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <fieldset>
                                                        <legend><span style="color: #4277AD;">Rangos de Venta</span></legend>
                                                        <table>
                                                            <tr>
                                                                <td style="color: #4277AD; text-align: right;">
                                                                    Inicio �
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtFechaInicioP" runat="server" Width="70px" ForeColor="Red" BackColor="HighlightText"
                                                                        onBlur=" return ( valFecha_Blank(this) ); "></asp:TextBox>
                                                                    <cc1:MaskedEditExtender ID="MaskedEditExtender5" runat="server" ClearMaskOnLostFocus="false"
                                                                        CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                                        CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                                        CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicioP">
                                                                    </cc1:MaskedEditExtender>
                                                                    <cc1:CalendarExtender ID="CalendarExtender5" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                                        TargetControlID="txtFechaInicioP">
                                                                    </cc1:CalendarExtender>
                                                                </td>
                                                                <td style="color: #4277AD; text-align: right;">
                                                                    Fin �
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtFechaFinP" runat="server" Width="70px" ForeColor="Red" BackColor="HighlightText"
                                                                        onBlur=" return ( valFecha_Blank(this) ); "></asp:TextBox>
                                                                    <cc1:MaskedEditExtender ID="MaskedEditExtender6" runat="server" ClearMaskOnLostFocus="false"
                                                                        CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                                        CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                                        CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFinP">
                                                                    </cc1:MaskedEditExtender>
                                                                    <cc1:CalendarExtender ID="CalendarExtender6" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                                        TargetControlID="txtFechaFinP">
                                                                    </cc1:CalendarExtender>
                                                                </td>
                                                                <td style="color: #4277AD; text-align: right;">
                                                                    Cantidad Min.:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtCantidadMinP" runat="server" Font-Bold="true" onKeyPress="return(  validarNumeroPuntoPositivo('event')  );"
                                                                        onFocus="return(  aceptarFoco(this)  );" Width="60px">0</asp:TextBox>
                                                                </td>
                                                                <td style="color: #4277AD; text-align: right;">
                                                                    Cantidad Max.:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtCantidadMaxP" runat="server" Font-Bold="true" onKeyPress="return(  validarNumeroPuntoPositivo('event')  );"
                                                                        onFocus="return(  aceptarFoco(this)  );" Width="60px">0</asp:TextBox>
                                                                </td>
                                                                <td style="color: #4277AD; text-align: right;">
                                                                    % Dscto:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtPorcentajeDsctoP" runat="server" Font-Bold="true" onKeyPress="return(  validarNumeroPuntoPositivo('event')  );"
                                                                        onFocus="return(  aceptarFoco(this)  );" Width="60px">0</asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td style="color: #4277AD; text-align: right;">
                                                                L�nea:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="cboLineaP" runat="server" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="color: #4277AD; text-align: right;">
                                                                SubL�nea:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="cboSubLineaP" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="color: #4277AD; text-align: right;">
                                                                Producto:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtProductoP" runat="server" Width="300px" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                                    onBlur="return ( onBlurTextTransform(this,configurarDatos) );" TabIndex="204"></asp:TextBox>
                                                            </td>
                                                            <td style="color: #4277AD; text-align: right;">
                                                                C�d.:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtCodigoProductoP" Font-Bold="true" Width="90px" runat="server"
                                                                    onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                                    TabIndex="205"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:RadioButtonList ID="rbActivoD" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Text="Activo" Value="1" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnConsultarDetalleView" runat="server" Text="Buscar" />
                                                            </td>
                                                            <%--  <td>
                                                            <asp:Button id="btnExportarDatos" runat="server" Text="Exportar"/>
                                                            </td>--%>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="SubTituloCelda">
                                                    Actualizar
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="1" cellpadding="2">
                                                        <tr>
                                                            <td style="color: #4277AD; text-align: right;">
                                                                Fecha Ini �
                                                            </td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtFechaIniP_U" runat="server" onblur="return(  valFecha_Blank(this) );"
                                                                    Width="70px" Font-Bold="true" BackColor="HighlightText"></asp:TextBox>
                                                                <cc1:CalendarExtender ID="CalendarExtender8" runat="server" TargetControlID="txtFechaIniP_U">
                                                                </cc1:CalendarExtender>
                                                            </td>
                                                            <td style="color: #4277AD; text-align: right;">
                                                                Fecha Fin �
                                                            </td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtFechaFinP_U" runat="server" onblur="return(  valFecha_Blank(this) );"
                                                                    Width="70px" Font-Bold="true" BackColor="HighlightText"></asp:TextBox>
                                                                <cc1:CalendarExtender ID="CalendarExtender_txt_fechafin" runat="server" TargetControlID="txtFechaFinP_U">
                                                                </cc1:CalendarExtender>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="Button1" runat="server" Text="Aceptar" OnClientClick="return ( onclick_Fecha() );"
                                                                    Style="cursor: Hand;" ToolTip="Cambiar la fechas a la cuadricula de productos" />
                                                            </td>
                                                            <td style="color: #4277AD; text-align: right;">
                                                                % Dscto �
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtPorcentajeDsctoP_U" runat="server" Width="90px" onKeyPress="return ( validarNumeroPuntoPositivo('event') );"
                                                                    Font-Bold="true" BackColor="HighlightText" onFocus="return ( aceptarFoco(this) );">0</asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="Button4" runat="server" Text="Aceptar" OnClientClick="return ( onclick_DsctoP_U() );"
                                                                    Style="cursor: Hand;" ToolTip="Cambiar la fechas a la cuadricula de productos" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="1" cellpadding="2">
                                                        <tr>
                                                            <td style="color: #4277AD; text-align: right;">
                                                                Cant. Min. �
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtCantidadMinP_U" runat="server" Width="90px" onKeyPress="return ( validarNumeroPuntoPositivo('event') );"
                                                                    Font-Bold="true" BackColor="HighlightText" onFocus="return ( aceptarFoco(this) );">0</asp:TextBox>
                                                            </td>
                                                            <td style="color: #4277AD; text-align: right;">
                                                                Cant. Max. �
                                                            </td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtCantidadMaxP_U" runat="server" Width="90px" onKeyPress="return ( validarNumeroPuntoPositivo('event') );"
                                                                    Font-Bold="true" BackColor="HighlightText" onFocus="return ( aceptarFoco(this) );">0</asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="Button3" runat="server" Text="Aceptar" OnClientClick="return ( onclick_CantidadP_U() );"
                                                                    Style="cursor: Hand;" ToolTip="Cambiar las cantidades de descuentos a la cuadricula de productos" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="BtnUpdateEstadoD" runat="server" Text="Actualizar" OnClientClick=" return ( confirm('Esta seguro de continuar con la operaci�n.') ); " />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:GridView ID="GV_DetalleView" runat="server" AutoGenerateColumns="False" Width="100%">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lblItem" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:HiddenField ID="hddIdDsctoVolVtas" runat="server" Value='<%# DataBinder.eval(Container.DataItem,"IdDsctoVolVtas") %>' />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Cod." DataField="CodigoProducto" />
                                                            <asp:BoundField HeaderText="Producto" DataField="Producto" />
                                                            <asp:BoundField HeaderText="Tienda" DataField="Tienda" NullDisplayText="------" />
                                                            <asp:BoundField HeaderText="Perfil" DataField="Perfil" NullDisplayText="------" />
                                                            <asp:BoundField HeaderText="Cond. Pago" DataField="CondicionPago" NullDisplayText="------" />
                                                            <asp:BoundField HeaderText="Medio Pago" DataField="MedioPago" NullDisplayText="------" />
                                                            <asp:BoundField HeaderText="Tipo PV" DataField="TipoPV" NullDisplayText="------" />
                                                            <asp:BoundField HeaderText="Precio" DataField="ProductoPV" DataFormatString="{0:F2}" />
                                                            <asp:TemplateField HeaderText="% Dscto">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtPorcenajeDsctoP_U" runat="server" Width="55px" Style="text-align: right"
                                                                        onFocus="return ( aceptarFoco(this) );" onKeyPress="return ( validarNumeroPuntoPositivo('event') );"
                                                                        Text='<%# DataBinder.Eval(Container.DataItem,"Dscto","{0:F2}") %>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="P. Dscto" DataField="PrecioDscto" DataFormatString="{0:F2}"
                                                                ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true" />
                                                            <asp:TemplateField HeaderText="Cant. Min">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtCantidadMin_PGV" runat="server" Width="60px" Style="text-align: right"
                                                                        onFocus="return ( aceptarFoco(this) );" onKeyPress="return ( validarNumeroPuntoPositivo('event') );"
                                                                        Text='<%# DataBinder.Eval(Container.DataItem,"CantMin","{0:F4}") %>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Cant. Max">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtCantidadMax_PGV" runat="server" Width="60px" Style="text-align: right"
                                                                        onFocus="return ( aceptarFoco(this) );" onKeyPress="return ( validarNumeroPuntoPositivo('event') );"
                                                                        Text='<%# DataBinder.Eval(Container.DataItem,"CantMax","{0:F4}") %>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="F. Ini">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtFechaIni_PGV" runat="server" Width="65px" onblur="return(  valFecha_Blank(this) );"
                                                                        Text='<%# DataBinder.Eval(Container.DataItem,"FechaIni","{0:d}") %>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="F. Fin">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtFechaFin_PGV" runat="server" Width="65px" onblur="return(  valFecha_Blank(this) );"
                                                                        Text='<%# DataBinder.Eval(Container.DataItem,"FechaFin","{0:d}") %>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Excep." >
                                                                <ItemTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="ckExcepcion" runat="server" Checked='<%# DataBinder.eval(Container.DataItem,"Excepcion") %>' />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="ckEstado" runat="server" Checked='<%# DataBinder.eval(Container.DataItem,"Estado") %>' />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                Est.
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="CheckBox1" runat="server" OnClick="selectAllD(this)" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </HeaderTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="No Afectar">
                                                                <ItemTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:CheckBox ID="ckNoAfectar" runat="server" Checked='<%# DataBinder.eval(Container.DataItem,"NoAfectar") %>' />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                        <FooterStyle CssClass="GrillaFooter" />
                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                        <PagerStyle CssClass="GrillaPager" />
                                                        <RowStyle CssClass="GrillaRow" />
                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Button TabIndex="212" ID="btnAnterior_DetalleView" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionDetalleView('0'));" />
                                                            </td>
                                                            <td>
                                                                <asp:Button TabIndex="213" ID="btnSiguiente_DetalleView" runat="server" Font-Bold="true"
                                                                    Width="50px" Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionDetalleView('1'));" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox TabIndex="214" ID="txtPageIndex_DetalleView" Width="50px" ReadOnly="true"
                                                                    CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Button TabIndex="215" ID="btnIr_DetalleView" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionDetalleView('2'));" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox TabIndex="216" ID="txtPageIndexGO_DetalleView" Width="50px" CssClass="TextBox_ReadOnly"
                                                                    runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="cboPageSizeDView" runat="server">
                                                                    <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                                    <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="color: Red">
                                                    *** Es necesario seleccionar una tienda para trabajar en esta ficha.
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </ContentTemplate>
                            </cc1:TabPanel>
                        </cc1:TabContainer>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div id="capaBuscarProducto_AddProd" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaBuscarProducto_AddProd')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="1" cellspacing="0">
                        <tr>
                            <td class="Texto">
                                TipoExistencia:
                            </td>
                            <td>
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="cboTipoExistencia" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                L�nea:
                            </td>
                            <td>
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True"
                                                DataTextField="Descripcion" DataValueField="Id">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            Sub L�nea:
                                        </td>
                                        <td>
                                            <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" runat="server" DataTextField="Nombre"
                                                DataValueField="Id" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Descripci�n:
                            </td>
                            <td>
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarGrilla_AddProducto">
                                            <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" TabIndex="204"
                                                ID="txtDescripcionProd_AddProd" runat="server" Width="300px" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                onKeypress="return( valKeyPressDescripcionProd(this) );"></asp:TextBox>
                                            </asp:Panel>
                                        </td>
                                        <td class="Texto">
                                            C�d.:
                                        </td>
                                        <td>
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnBuscarGrilla_AddProducto">
                                            <asp:TextBox ID="txtCodigoProducto" Font-Bold="true" Width="100px" runat="server"
                                                onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                onKeypress="return( valKeyPressDescripcionProd(this) );" TabIndex="205"></asp:TextBox>
                                        </asp:Panel>
                                        </td>
                                        <td>
                                            <%--<asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                TabIndex="207" onmouseout="this.src='../Imagenes/Buscar_b.JPG';" onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />--%>
                                                <asp:Button ID="btnBuscarGrilla_AddProducto" runat="server" Text="Buscar" TabIndex="207" CssClass="btnBuscar"
                                            OnClientClick="this.disabled=true;this.value='Procesando..'" UseSubmitBehavior="false"/>
                                        </td>
                                        <td>
                                            <%--<asp:ImageButton ID="btnAddProductos_AddProd" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                onmouseout="this.src='../Imagenes/Agregar_B.JPG';" onmouseover="this.src='../Imagenes/Agregar_A.JPG';"
                                                TabIndex="208" />--%>
                                                <asp:Button ID="btnAddProductos_AddProducto" runat="server" Text="A�adir" TabIndex="208"  CssClass="btnA�adir"/>
                                        </td>
                                        <td>
                                            <%--<asp:ImageButton ID="btnLimpiar_AddProd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                onmouseout="this.src='../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../Imagenes/Limpiar_A.JPG';"
                                                OnClientClick="return(limpiarBuscarProducto());" TabIndex="209" />--%>
                                                <asp:Button ID="btnLimpiar_AddProducto" runat="server" Text="Limpiar" TabIndex="209" CssClass="btnLimpiar"
                                                OnClientClick="return(limpiarBuscarProducto());"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                            </td>
                            <td>
                                <asp:CheckBox ID="chb_CargaMasiva" runat="server" CssClass="Texto" Font-Bold="true"
                                    Text="Agregar productos contenidos en la < L�nea / Sub L�nea > seleccionada." />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                        CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                        CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                        CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                        CollapsedText="B�squeda Avanzada" ExpandedText="B�squeda Avanzada" ExpandDirection="Vertical"
                        SuppressPostBack="true">
                    </cc1:CollapsiblePanelExtender>
                    <asp:Image ID="Image21_11" runat="server" Height="16px" />
                    <asp:Label ID="Label21_11" runat="server" Text="B�squeda Avanzada" CssClass="Label"></asp:Label>
                    <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                        <table width="100">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Atributo:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                        Width="650px">
                                        <Columns>
                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                    <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                        DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="GrillaRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="DGV_AddProd" runat="server" AutoGenerateColumns="False" Width="100%"
                        PageSize="20" AllowPaging="True">
                        <Columns>
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="chb_AddDetalle" runat="server" />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" OnClick="selectAll(this)" />
                                </HeaderTemplate>
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="Codigo" HeaderText="C�digo" HeaderStyle-Height="25px"
                                HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" HeaderStyle-Height="25px"
                                HeaderStyle-HorizontalAlign="Center" NullDisplayText="---">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            </asp:BoundField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                    <asp:Button TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                    <asp:TextBox TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true"
                        CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                    <asp:Button TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px"
                        Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionProductos('2'));" />
                    <asp:TextBox TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly"
                        runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div id="Div_Tabla" style="border: 3px solid blue; padding: 8px; width: 900px; height: auto;
        position: absolute; top: 237px; left: 32px; background-color: white; z-index: 2;
        display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('Div_Tabla')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset>
                        <legend><span class="Texto">Clases de Atributos</span></legend>
                        <table width="100%">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Descripci�n:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtBuscarTabla" runat="server" MaxLength="50" Width="250px" onKeyPress=" return ( BuscarTabla() ); "></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="ImgBuscarTabla" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                    ToolTip="Buscar Tabla" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GV_Tabla" runat="server" AutoGenerateColumns="False" Width="100%"
                                        PageSize="20">
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <Columns>
                                            <asp:CommandField ShowSelectButton="True" />
                                            <asp:TemplateField HeaderText="Descripci�n">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTabla") %>' />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblTabla" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Abv" HeaderText="Abv." />
                                        </Columns>
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnAnteriorTabla" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                    ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionTabla('0'));" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnSiguienteTabla" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                    ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionTabla('1'));" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPageIndexTabla" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                    runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnIrTabla" runat="server" Font-Bold="true" Width="50px" Text="Ir"
                                                    ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionTabla('2'));" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPageIndexGoTabla" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                    onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </div>
    <div id="Div_TablaValor" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 6; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton2" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa2('Div_TablaValor')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset>
                        <legend><span class="Texto">Registros de </span>
                            <asp:Label ID="lblNomTabla" runat="server" Font-Bold="true" CssClass="Texto" Text="Tabla"></asp:Label></legend>
                        <table width="100%">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Descripci�n:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtBuscarTablaValor" runat="server" MaxLength="50" Width="250px"
                                                    onKeyPress=" return ( BuscarTablaValor() ); "></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="ImgBuscarTablaValor" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                    ToolTip="Buscar Tabla Valor" />
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="ImgAgregarTablaValor" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                    OnClientClick=" return ( AgregarTablaValor() ); " ToolTip="Agregar Valor" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GV_TablaValor" runat="server" AutoGenerateColumns="False" Width="100%"
                                        PageSize="20">
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="ckTablaValor" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="C�digo">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTabla") %>' />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblTipoTabla" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Codigo") %>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Descripci�n">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdTipoTablaValor" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTablaValor") %>' />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblTipoTablaValor" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Abv" HeaderText="Abv." />
                                        </Columns>
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnAnteriorTablaValor" runat="server" Font-Bold="true" Width="50px"
                                                    Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionTablaValor('0'));" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnSiguienteTablaValor" runat="server" Font-Bold="true" Width="50px"
                                                    Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionTablaValor('1'));" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPageIndexTablaValor" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                    runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnIrTablaValor" runat="server" Font-Bold="true" Width="50px" Text="Ir"
                                                    ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionTablaValor('2'));" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPageIndexGoTablaValor" Width="50px" CssClass="TextBox_ReadOnly"
                                                    runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    
    </div>

    <script language="javascript" type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function calcularDescuento() {
            var grilla = document.getElementById('<%=GV_Detalle.clientID %>');
            var precio = 0;
            var descuentoPorcentaje = 0;
            var calculoDescuento = 0;
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {                    
                    precio = parseFloat(grilla.rows[i].cells[6].children[1].innerHTML);
                    descuentoPorcentaje = parseFloat(grilla.rows[i].cells[9].children[0].value);
                    calculoDescuento = parseFloat(precio * descuentoPorcentaje / 100);
                    grilla.rows[i].cells[10].children[0].innerHTML = redondear((precio-calculoDescuento),2);
                }
            } else {alert('elseeee');}
        }
        function EndRequestHandler(sender, args) {
            if (args.get_error() != undefined) {
                args.set_errorHandled(true);
            }
        }
        function valKeyPressDescripcionProd(obj) {
            if (event.keyCode == 13) { // Enter
                onBlurTextTransform(obj, configurarDatos);
                document.getElementById('<%=btnBuscarGrilla_AddProducto.ClientID%>').focus();
            }
            return true;
        }

        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function limpiarBuscarProducto() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
            var txtCodigoProducto = document.getElementById('<%=txtCodigoProducto.ClientID%>');
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';
            txtCodigoProducto.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            //********* FILTRO AVANZADO
            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }

        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }

        function valNavegacionDetalle(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Detalle.ClientID%>').value);
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Detalle.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
            }
            return true;
        }

        function valNavegacionDetalleView(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_DetalleView.ClientID%>').value);
            var grilla = document.getElementById('<%=GV_DetalleView.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_DetalleView.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
            }
            return true;
        }


        function valNavegacionGeneralView(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_GeneralView.ClientID%>').value);
            var grilla = document.getElementById('<%=GV_GeneralView.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_GeneralView.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
            }
            return true;
        }



        function selectAll(invoker) {
            var grillaTipoTablaConfigxValor = document.getElementById('<%=DGV_AddProd.ClientID %>');

            if (grillaTipoTablaConfigxValor != null) {
                for (var i = 1; i < grillaTipoTablaConfigxValor.rows.length; i++) {
                    var rowElem = grillaTipoTablaConfigxValor.rows[i];
                    rowElem.cells[0].children[0].cells[0].children[0].checked = invoker.checked;
                }
            }
            return false;

        }

        function selectAllG(invoker) {
            var GV_GeneralView = document.getElementById('<%=GV_GeneralView.ClientID %>');

            if (GV_GeneralView != null) {
                for (var i = 1; i < GV_GeneralView.rows.length; i++) {
                    var rowElem = GV_GeneralView.rows[i];
                    rowElem.cells[12].children[0].cells[0].children[0].checked = invoker.checked;
                }
            }
            return false;

        }

        function selectAllD(invoker) {
            var GV_DetalleView = document.getElementById('<%=GV_DetalleView.ClientID %>');

            if (GV_DetalleView != null) {
                for (var i = 1; i < GV_DetalleView.rows.length; i++) {
                    var rowElem = GV_DetalleView.rows[i];
                    rowElem.cells[16].children[0].cells[0].children[0].checked = invoker.checked;
                }
            }
            return false;

        }

        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;



        function valNavegacionTabla(tipoMov) {

            var index = parseInt(document.getElementById('<%=txtPageIndexTabla.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGoTabla.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGoTabla.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGoTabla.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGoTabla.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGoTabla.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function valNavegacionTablaValor(tipoMov) {

            var index = parseInt(document.getElementById('<%=txtPageIndexTablaValor.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGoTablaValor.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGoTablaValor.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGoTablaValor.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGoTablaValor.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGoTablaValor.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }


        function BuscarTablaValor() {
            if (event.keyCode == 13) {
                document.getElementById('<%=ImgBuscarTablaValor.ClientID %>').click();
                return false;
            }
            return true;
        }

        function BuscarTabla() {
            if (event.keyCode == 13) {
                document.getElementById('<%=ImgBuscarTabla.ClientID %>').click();
                return false;
            }
            return true;
        }

        function AgregarTablaValor() {

            var GV_TablaValor = document.getElementById('<%=GV_TablaValor.ClientID %>');
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');

            if (GV_TablaValor != null) {

                for (var j = 1; j < GV_TablaValor.rows.length; j++) {
                    var rowElem2 = GV_TablaValor.rows[j];

                    if (rowElem2.cells[0].children[0].cells[0].children[0].checked == true) {

                        if (grilla != null) {
                            for (var i = 1; i < grilla.rows.length; i++) {
                                var rowElem = grilla.rows[i];

                                if (rowElem2.cells[1].children[0].cells[0].children[0].value == rowElem.cells[1].children[0].cells[1].children[0].value && rowElem2.cells[2].children[0].cells[0].children[0].value == rowElem.cells[1].children[0].cells[2].children[0].value) {
                                    alert('LA LISTA DE ATRIBUTOS YA CONTIENE LA TABLA < ' + rowElem.cells[1].children[0].cells[0].children[0].innerText + ' > CON VALOR < ' + rowElem.cells[2].children[0].cells[0].children[0].innerText + ' >. \n NO SE PERMITE LA OPERACI�N.');
                                    return false;
                                }

                            }
                        }

                    } //end id
                }
            }

            return true;
        }

        function Buscar_Producto() {

            var IdTienda = parseInt(document.getElementById('<%=cboTienda.ClientID %>').value);
            if (isNaN(IdTienda)) { IdTienda = 0; }

            //var IdTipoPrecioV = parseInt(document.getElementById('<%= cboTipoPrecioV.ClientID %>').value);
            //if (isNaN(IdTipoPrecioV)) { IdTipoPrecioV = 0; }

            if (IdTienda == 0) {
                alert('SELECCIONE UNA TIENDA.');
                return false;
            }

            //if (IdTipoPrecioV == 0) {
              //  alert('SELECCIONE UNA TIPO PRECIO.');
                //return false;
            //Rangos }


            onCapa('capaBuscarProducto_AddProd');
            return false;
        }

        function onclick_Fecha() {
            var txtFechaFinP_U = document.getElementById('<%=txtFechaFinP_U.ClientID %>');
            if (txtFechaFinP_U.value == '') {
                alert('INGRESE UNA FECHA DE FIN.');
                return false;
            }

            var txtFechaIniP_U = document.getElementById('<%=txtFechaIniP_U.ClientID %>');
            if (txtFechaIniP_U.value == '') {
                alert('INGRESE UNA FECHA DE INICIO.');
                return false;
            }

            if (compararFechas(txtFechaIniP_U, txtFechaFinP_U)) {
                alert('LA FECHA DE FIN NO PUEDE SER MENOR A LA FECHA DE INICIO.\nNO SE PERMITE LA OPERACI�N.');
                return false;
            }

            var GV_DetalleView = document.getElementById('<%=GV_DetalleView.ClientID %>');

            if (GV_DetalleView != null) {

                for (var i = 1; i < GV_DetalleView.rows.length; i++) {
                    var rowElem = GV_DetalleView.rows[i];

                    rowElem.cells[14].children[0].value = txtFechaFinP_U.value;
                    rowElem.cells[13].children[0].value = txtFechaIniP_U.value;

                }

            }

        }

        function onclick_FechaLS() {
            var txtFechaFinLS_U = document.getElementById('<%=txtFechaFinLS_U.ClientID %>');
            if (txtFechaFinLS_U.value == '') {
                alert('INGRESE UNA FECHA DE FIN');
                return false;
            }

            var txtFechaIniLS_U = document.getElementById('<%=txtFechaIniLS_U.ClientID %>');
            if (txtFechaIniLS_U.value == '') {
                alert('INGRESE UNA FECHA DE FIN');
                return false;
            }

            if (compararFechas(txtFechaIniLS_U, txtFechaFinLS_U)) {
                alert('LA FECHA DE FIN NO PUEDE SER MENOR A LA FECHA DE INICIO.\nNO SE PERMITE LA OPERACI�N.');
                return false;
            }

            var GV_GeneralView = document.getElementById('<%=GV_GeneralView.ClientID %>');

            if (GV_GeneralView != null) {

                for (var i = 1; i < GV_GeneralView.rows.length; i++) {
                    var rowElem = GV_GeneralView.rows[i];

                    rowElem.cells[11].children[0].value = txtFechaFinLS_U.value;
                    rowElem.cells[10].children[0].value = txtFechaIniLS_U.value;

                }

            }

        }


        function onclick_AddTiendaRef() {
            var IdTienda = parseInt(document.getElementById('<%=cboTienda.ClientID %>').value);
            var IdTiendaRef = parseInt(document.getElementById('<%=cboTiendaReplica.ClientID %>').value);

            if (IdTienda == IdTiendaRef && IdTienda == 0) {
                return false;
            }
            return true;
        }
        //
        function compararFechas(fecha, fecha2) {
            var xMonth = fecha.value.substring(3, 5);
            var xDay = fecha.value.substring(0, 2);
            var xYear = fecha.value.substring(6, 10);
            var yMonth = fecha2.value.substring(3, 5);
            var yDay = fecha2.value.substring(0, 2);
            var yYear = fecha2.value.substring(6, 10);

            if (xYear > yYear) {
                return (true)
            }
            else {
                if (xYear == yYear) {
                    if (xMonth > yMonth) {
                        return (true)
                    }
                    else {
                        if (xMonth == yMonth) {
                            if (xDay > yDay)
                                return (true);
                            else
                                return (false);
                        }
                        else
                            return (false);
                    }
                }
                else
                    return (false);
            }
        }

        ///


        function Guardar_LS() {

            confirm('Esta seguro de continuar con la operaci�n.');
        }


        function Guardar_P() {

            var txtFechaInicio = document.getElementById('<%=txtFechaInicio.ClientID %>');
            var txtFechaFin = document.getElementById('<%=txtFechaFin.ClientID %>');
            var PorcentajeDscto = parseFloat(document.getElementById('<%=txtPorcentajeDscto.ClientID %>').value);
            if (isNaN(PorcentajeDscto)) { PorcentajeDscto = 0; }
            if (PorcentajeDscto == 0) {
                alert('Ingrese un porcentaje de descuento. No se permite la operaci�n.');
                return false;
            }

            if (txtFechaInicio.value == '') {
                alert('Ingrese una fecha de inicio');
                return false;
            }
            if (txtFechaFin.value == '') {
                alert('Ingrese una fecha de fin');
                return false;
            }
            if (compararFechas(txtFechaInicio, txtFechaFin)) {
                alert('LA FECHA DE FIN NO PUEDE SER MENOR A LA FECHA DE INICIO.\nNO SE PERMITE LA OPERACI�N.');
                return false;
            }

            confirm('Esta seguro de continuar con la operaci�n.');
        }

        function onclick_CantidadP_U() {
            var CantidadMinP_U = parseFloat(document.getElementById('<%=txtCantidadMinP_U.ClientID %>').value);
            if (isNaN(CantidadMinP_U)) { CantidadMinP_U = 0; }

            var CantidadMaxP_U = parseFloat(document.getElementById('<%=txtCantidadMaxP_U.ClientID %>').value);
            if (isNaN(CantidadMaxP_U)) { CantidadMaxP_U = 0; }

            var GV_DetalleView = document.getElementById('<%=GV_DetalleView.ClientID %>');

            if (GV_DetalleView != null) {
                for (var i = 1; i < GV_DetalleView.rows.length; i++) {
                    var rowElem = GV_DetalleView.rows[i];

                    rowElem.cells[11].children[0].value = redondear(CantidadMinP_U, 4);
                    rowElem.cells[12].children[0].value = redondear(CantidadMaxP_U, 4);

                }
            }

        }

        function onclick_CantidadLS_U() {
            var CantidadMinP_U = parseFloat(document.getElementById('<%=txtCantidadMinLS_U.ClientID %>').value);
            if (isNaN(CantidadMinP_U)) { CantidadMinP_U = 0; }

            var CantidadMaxP_U = parseFloat(document.getElementById('<%=txtCantidadMaxLS_U.ClientID %>').value);
            if (isNaN(CantidadMaxP_U)) { CantidadMaxP_U = 0; }

            var GV_DetalleView = document.getElementById('<%=GV_GeneralView.ClientID %>');

            if (GV_DetalleView != null) {
                for (var i = 1; i < GV_DetalleView.rows.length; i++) {
                    var rowElem = GV_DetalleView.rows[i];

                    rowElem.cells[8].children[0].value = redondear(CantidadMinP_U, 4);
                    rowElem.cells[9].children[0].value = redondear(CantidadMaxP_U, 4);

                }
            }

        }

        function onclick_DsctoP_U() {
            var PorcentajeDscto = parseFloat(document.getElementById('<%=txtPorcentajeDsctoP_U.ClientID %>').value);
            if (isNaN(PorcentajeDscto)) { PorcentajeDscto = 0; }
            if (PorcentajeDscto == 0) {
                alert('Ingrese un porcentaje de descuento. No se permite la operaci�n.');
                return false;
            }

            var GV_DetalleView = document.getElementById('<%=GV_DetalleView.ClientID %>');

            if (GV_DetalleView != null) {
                for (var i = 1; i < GV_DetalleView.rows.length; i++) {
                    var rowElem = GV_DetalleView.rows[i];

                    rowElem.cells[9].children[0].value = redondear(PorcentajeDscto, 2);

                }
            }

        }


        function onclick_DsctoLS_U() {
            var PorcentajeDscto = parseFloat(document.getElementById('<%=txtPorcentajeDsctoLS_U.ClientID %>').value);
            if (isNaN(PorcentajeDscto)) { PorcentajeDscto = 0; }
            if (PorcentajeDscto == 0) {
                alert('Ingrese un porcentaje de descuento. No se permite la operaci�n.');
                return false;
            }

            var GV_DetalleView = document.getElementById('<%=GV_GeneralView.ClientID %>');

            if (GV_DetalleView != null) {
                for (var i = 1; i < GV_DetalleView.rows.length; i++) {
                    var rowElem = GV_DetalleView.rows[i];

                    rowElem.cells[7].children[0].value = redondear(PorcentajeDscto, 2);

                }
            }

        }   
        
        
    </script>

</asp:Content>
