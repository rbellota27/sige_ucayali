﻿Imports System.IO
Imports System.Data
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Configuration
Partial Public Class DsctoxVolumen
    Inherits System.Web.UI.Page


    Dim cbo As Combo
    Const _IdMercaderia As Integer = 1
    Const _IdFactura As Integer = 1
    Dim objScript As New ScriptManagerClass


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            onLoad_DsctoxVolumen()
        End If

        Try

            If Not (ViewState("TabIndex") Is Nothing) AndAlso (Not (sender Is Nothing)) Then
                If sender.[GetType]().ToString().Equals("AjaxControlToolkit.TabContainer") Then

                    DirectCast(sender, AjaxControlToolkit.TabContainer).ActiveTabIndex = Convert.ToInt16(ViewState("TabIndex"))

                End If
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(me,ex.Message)
        End Try
    End Sub

    Private Sub CopyCboItems(ByVal CopyCbo As DropDownList, ByVal ToCbo As DropDownList)

        For i As Integer = 0 To CopyCbo.Items.Count - 1
            ToCbo.Items.Add(CopyCbo.Items(i))
        Next

        ToCbo.DataTextField = CopyCbo.DataTextField
        ToCbo.DataValueField = CopyCbo.DataValueField

    End Sub
    Private Sub LimpiarGrillas()
 

        Session.Remove("ListaTiendaRep")

        'Me.GV_Detalle.DataSource = Nothing
        'Me.GV_Detalle.DataBind()

        Me.ListaTiendaRef = New List(Of Entidades.Tienda)

        Me.GV_TiendaReplica.DataSource = Nothing
        Me.GV_TiendaReplica.DataBind()

    End Sub
    Private Sub LimpiarSession()
        Session.Remove("IdEmpresaView")
        Session.Remove("IdTiendaView")
        Session.Remove("IdTipoPrecioVView")
        Session.Remove("IdPerfilView")
        Session.Remove("IdCondicionPagoView")
        Session.Remove("IdMedioPagoView")
        Session.Remove("IdLineaView")
        Session.Remove("IdSubLineaView")
        Session.Remove("CodigoProductoView")
        Session.Remove("ProductoView")
        Session.Remove("DsctoView")
        Session.Remove("CantMaxView")
        Session.Remove("CantMinView")
        Session.Remove("FechaIniView")
        Session.Remove("FechaFinView")
        Session.Remove("EstadoView")
    End Sub
    Private Sub onLoad_DsctoxVolumen()

        cbo = New Combo
        With cbo            
            .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
            CopyCboItems(Me.cboEmpresa, Me.cboEmpresaLS)
            CopyCboItems(Me.cboEmpresa, Me.cboEmpresaP)

            .llenarCboTiendaxIdEmpresa(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), True)
            CopyCboItems(Me.cboTienda, Me.cboTiendaLS)
            CopyCboItems(Me.cboTienda, Me.cboTiendaP)
            CopyCboItems(Me.cboTienda, Me.cboTiendaReplica)

            Try
                Me.cboTiendaP.Items.RemoveAt(0)
            Catch ex As Exception : End Try

            .LlenarCboPerfil(Me.cboPerfil, True)
            CopyCboItems(Me.cboPerfil, Me.cboPerfilLS)
            CopyCboItems(Me.cboPerfil, Me.cboPerfilP)

            .LlenarCboCondicionPago(Me.cboCondicionPago, True)
            CopyCboItems(Me.cboCondicionPago, Me.cboCondicionPagoLS)
            CopyCboItems(Me.cboCondicionPago, Me.cboCondicionPagoP)

            .LlenarCboMedioPagoxIdTipoDocumentoxIdCondicionPago(Me.cboMedioPago, _IdFactura, CInt(Me.cboCondicionPago.SelectedValue), True)
            CopyCboItems(Me.cboMedioPago, Me.cboMedioPagoLS)
            CopyCboItems(Me.cboMedioPago, Me.cboMedioPagoP)

            .LlenarCboTipoPV(Me.cboTipoPrecioV, True)
            CopyCboItems(Me.cboTipoPrecioV, Me.cboTipoPrecioVLS)
            CopyCboItems(Me.cboTipoPrecioV, Me.cboTipoPrecioVP)
            Try
                Me.cboTipoPrecioVP.Items.RemoveAt(0)
            Catch ex As Exception : End Try

            .llenarCboLineaxTipoExistencia(Me.cboLinea, _IdMercaderia, True)
            CopyCboItems(Me.cboLinea, Me.cboLineaLS)
            CopyCboItems(Me.cboLinea, Me.cboLineaP)

            .LlenarCboSubLineaxIdLinea(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), True)
            CopyCboItems(Me.cboSubLinea, Me.cboSubLineaLS)
            CopyCboItems(Me.cboSubLinea, Me.cboSubLineaP)

            .llenarCboTipoExistencia(Me.cboTipoExistencia, False)
            .llenarCboLineaxTipoExistencia(Me.cmbLinea_AddProd, CInt(Me.cboTipoExistencia.SelectedValue), True)
            .LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            .LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)



        End With

        Dim Fecha As Date = (New Negocio.FechaActual).SelectFechaActual

        Me.txtFechaInicio.Text = Format(Fecha, "dd/MM/yyyy")
        Me.txtFechaFin.Text = Format(Fecha, "dd/MM/yyyy")

        Me.txtFechaInicioLS.Text = Format(Fecha, "dd/MM/yyyy")
        Me.txtFechaFinLS.Text = Format(Fecha, "dd/MM/yyyy")

        Me.txtFechaInicioP.Text = Format(Fecha, "dd/MM/yyyy")
        Me.txtFechaFinP.Text = Format(Fecha, "dd/MM/yyyy")


        Me.txtFechaIniLS_U.Text = Format(Fecha, "dd/MM/yyyy")
        Me.txtFechaFinLS_U.Text = Format(Fecha, "dd/MM/yyyy")

        Me.txtFechaIniP_U.Text = Format(Fecha, "dd/MM/yyyy")
        Me.txtFechaFinP_U.Text = Format(Fecha, "dd/MM/yyyy")


        S_FiltroTablaValor = New List(Of Entidades.TipoTablaValor)
        S_TipoTablaValor = New List(Of Entidades.TipoTablaValor)
        ListaTiendaRef = New List(Of Entidades.Tienda)




    End Sub



    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try

            If IsNothing(cbo) Then cbo = New Combo

            With cbo

                .llenarCboTiendaxIdEmpresa(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), True)

            End With


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboEmpresaLS_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresaLS.SelectedIndexChanged
        Try

            If IsNothing(cbo) Then cbo = New Combo

            With cbo

                .llenarCboTiendaxIdEmpresa(Me.cboTiendaLS, CInt(Me.cboEmpresaLS.SelectedValue), True)

            End With


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboEmpresaP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresaP.SelectedIndexChanged
        Try

            If IsNothing(cbo) Then cbo = New Combo

            With cbo

                .llenarCboTiendaxIdEmpresa(Me.cboTiendaP, CInt(Me.cboEmpresaP.SelectedValue), True)

            End With


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLinea.SelectedIndexChanged

        Try

            If IsNothing(cbo) Then cbo = New Combo
            With cbo

                .LlenarCboSubLineaxIdLinea(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), True)

            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub cboLineaLS_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLineaLS.SelectedIndexChanged

        Try

            If IsNothing(cbo) Then cbo = New Combo
            With cbo

                .LlenarCboSubLineaxIdLinea(Me.cboSubLineaLS, CInt(Me.cboLineaLS.SelectedValue), True)

            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub cboLineaP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLineaP.SelectedIndexChanged
        Try

            If IsNothing(cbo) Then cbo = New Combo
            With cbo

                .LlenarCboSubLineaxIdLinea(Me.cboSubLineaP, CInt(Me.cboLineaP.SelectedValue), True)

            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub cboCondicionPago_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCondicionPago.SelectedIndexChanged
        Try

            If IsNothing(cbo) Then cbo = New Combo
            With cbo

                .LlenarCboMedioPagoxIdTipoDocumentoxIdCondicionPago(Me.cboMedioPago, _IdFactura, CInt(Me.cboCondicionPago.SelectedValue), True)

            End With
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboCondicionPagoLS_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCondicionPagoLS.SelectedIndexChanged
        Try

            If IsNothing(cbo) Then cbo = New Combo
            With cbo

                .LlenarCboMedioPagoxIdTipoDocumentoxIdCondicionPago(Me.cboMedioPagoLS, _IdFactura, CInt(Me.cboCondicionPagoLS.SelectedValue), True)                
            End With
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboCondicionPagoP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCondicionPagoP.SelectedIndexChanged
        Try

            If IsNothing(cbo) Then cbo = New Combo
            With cbo

                .LlenarCboMedioPagoxIdTipoDocumentoxIdCondicionPago(Me.cboMedioPagoP, _IdFactura, CInt(Me.cboCondicionPagoP.SelectedValue), True)

            End With
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub



#Region "************************** BUSQUEDA PRODUCTO"

    Private listaProductoView As List(Of Entidades.ProductoView)

    Protected Sub BuscarProductoCatalogo(ByVal opcion As Integer)
        Try

            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar

                    ViewState.Add("nombre_BuscarProducto", txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)
                    ViewState.Add("IdAlmacen_BuscarProducto", -1)
                    ViewState.Add("IdEmpresa_BuscarProducto", -1)
                    ViewState.Add("CodigoSubLinea_BuscarProducto", "")
                    ViewState.Add("IdTipoExistencia", Me.cboTipoExistencia.SelectedValue)

            End Select

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal codigoSubLinea As String, ByVal nomProducto As String, _
                                          ByVal IdAlmacen As Integer, ByVal tipoMov As Integer, _
                                          ByVal IdEmpresa As Integer, ByVal IdTipoExistencia As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select

        Dim codigoProducto As String = Me.txtCodigoProducto.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()

        'Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams_V2(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, -1, -1, grilla.PageSize, index, codigoProducto, IdTipoExistencia, tableTipoTabla)
        Me.listaProductoView = (New Negocio.Producto).SelectProductoxCodigoxDescripcion(codigoProducto, nomProducto)
        If IsNothing(Me.listaProductoView) Then

            grilla.DataSource = Nothing
            grilla.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');    alert('No se hallaron registros.');        ", True)
            Return
        Else

            grilla.DataSource = Me.listaProductoView
            grilla.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');  ", True)

        End If

    End Sub
    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            -1, 1, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            -1, 2, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            -1, 3, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)


            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region
#Region "************************** BUSQUEDA AVANZADA PRODUCTOS"

    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor


    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Protected Sub M_B_cmbSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1

            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)

        Next

        Return dt

    End Function
#End Region





    Private Sub btnRegistrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegistrar.Click
        Try
            If (GV_Detalle.Rows.Count > 0) Then
                Registrar()
                LimpiarGrillas()
            Else
                objScript.mostrarMsjAlerta(Me, "Seleccione por lo menos un producto para continuar con el registro.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub Registrar()
        Try
            Actualizar_ListaDetalle(False, True)
            Me.ListaDscto_Detalle = Me.S_ListaDsctoDetalle
            Dim operacion As Boolean
            operacion = (New Negocio.DsctoVolVtas).DsctoVolVtas_Registrar(Me.ListaDscto_Detalle)
            Me.listaTiendaReferencia = Me.ListaTiendaRef
            If Me.listaTiendaReferencia.Count > 0 Then

                For i As Integer = 0 To listaTiendaReferencia.Count - 1
                    For k As Integer = 0 To Me.ListaDscto_Detalle.Count - 1
                        Me.ListaDscto_Detalle(k).IdTienda = listaTiendaReferencia(i).Id
                    Next

                    operacion = (New Negocio.DsctoVolVtas).DsctoVolVtas_Registrar(Me.ListaDscto_Detalle)

                Next

            End If

            If (operacion) Then
                objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito.")
            Else
                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Sub



    Dim ListaDscto_Detalle As List(Of Entidades.DsctoVolVtas)

    Property S_ListaDsctoDetalle() As List(Of Entidades.DsctoVolVtas)
        Get
            Return CType(Session("ListaDsctoDetalle"), List(Of Entidades.DsctoVolVtas))
        End Get
        Set(ByVal value As List(Of Entidades.DsctoVolVtas))
            Session.Remove("ListaDsctoDetalle")
            Session.Add("ListaDsctoDetalle", value)
        End Set
    End Property

    Private Function obtenerDataTable_Producto() As DataTable

        Dim objDataTable As New DataTable

        objDataTable.Columns.Add("Columna1")

        For i As Integer = 0 To Me.DGV_AddProd.Rows.Count - 1

            If (CType(Me.DGV_AddProd.Rows(i).FindControl("chb_AddDetalle"), CheckBox).Checked) Then

                Dim IdProducto As Integer = CInt(CType(Me.DGV_AddProd.Rows(i).FindControl("hddIdProducto"), HiddenField).Value)
                objDataTable.Rows.Add(IdProducto)

            End If

        Next

        Return objDataTable

    End Function


    Private Sub Actualizar_ListaDetalle(Optional ByVal Inicializar As Boolean = False, Optional ByVal Registar As Boolean = False)

        If Inicializar = False Then Me.ListaDscto_Detalle = Me.S_ListaDsctoDetalle
        If IsNothing(Me.ListaDscto_Detalle) Then Me.ListaDscto_Detalle = New List(Of Entidades.DsctoVolVtas)

        If Me.ListaDscto_Detalle.Count > 0 Then

            For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1

                With Me.ListaDscto_Detalle(i)

                    .NoAfectar = CType(Me.GV_Detalle.Rows(i).FindControl("ckNoAfectar"), CheckBox).Checked
                    .Excepcion = CType(Me.GV_Detalle.Rows(i).FindControl("ckExcepcion"), CheckBox).Checked

                    If Registar Then

                        .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
                        .IdTienda = CInt(Me.cboTienda.SelectedValue)
                        .IdPerfil = CInt(Me.cboPerfil.SelectedValue)
                        .IdTipoPV = CInt(Me.cboTipoPrecioV.SelectedValue)

                        .IdCondicionPago = CInt(cboCondicionPago.SelectedValue)
                        .IdMedioPago = CInt(cboMedioPago.SelectedValue)

                        .Dscto = CDec(DirectCast(Me.GV_Detalle.Rows(i).FindControl("lblDscto"), TextBox).Text)
                        .CantMax = CDec(DirectCast(Me.GV_Detalle.Rows(i).FindControl("txtCantidadMaxima"), TextBox).Text)
                        .CantMin = CDec(DirectCast(Me.GV_Detalle.Rows(i).FindControl("txtCantidadMinima"), TextBox).Text)
                        .FechaIni = CDate(DirectCast(Me.GV_Detalle.Rows(i).FindControl("txtFechaInicio"), TextBox).Text)
                        .FechaFin = CDate(DirectCast(Me.GV_Detalle.Rows(i).FindControl("txtFechaFinal"), TextBox).Text)
                        .Estado = True ' *** ACTIVO

                    End If
                End With

            Next

        End If

        Me.S_ListaDsctoDetalle = Me.ListaDscto_Detalle

    End Sub


    Private Sub onClick_btnAddProductos_AddProd()

        Actualizar_ListaDetalle(True)
        Me.ListaDscto_Detalle = Me.S_ListaDsctoDetalle

        ViewState.Add("IdTiendaDet", CInt(Me.cboTienda.SelectedValue))
        ViewState.Add("IdPerfilDet", CInt(Me.cboPerfil.SelectedValue))
        ViewState.Add("IdTipoPVDet", CInt(Me.cboTipoPrecioV.SelectedValue))

        ViewState.Add("IdLineaDet", CInt(Me.cmbLinea_AddProd.SelectedValue))
        ViewState.Add("IdSubLineaDet", CInt(Me.cmbSubLinea_AddProd.SelectedValue))
        ViewState.Add("CodigoProductoDet", Me.txtCodigoProducto.Text.Trim)
        ViewState.Add("ProductoDet", Me.txtDescripcionProd_AddProd.Text.Trim)

        ViewState.Add("DsctoDet", CDec(Me.txtPorcentajeDscto.Text.Trim))

        Dim objDataTable As DataTable '= obtenerDataTable_Producto()
        Session.Add("objDataTable", objDataTable)

        Me.ListaDscto_Detalle.AddRange(obtenerListaAdd_Detalle(0))

        Me.S_ListaDsctoDetalle = Me.ListaDscto_Detalle

        Me.GV_Detalle.DataSource = Me.ListaDscto_Detalle
        Me.GV_Detalle.DataBind()



    End Sub

    Private Sub onClick_btnAddProductos_AddProd2()

        Actualizar_ListaDetalle()
        Me.ListaDscto_Detalle = S_ListaDsctoDetalle
        Dim ObjDscto As Entidades.DsctoVolVtas

        Dim IdProducto As Integer = 0

        For i As Integer = 0 To Me.DGV_AddProd.Rows.Count - 1
            If CType(Me.DGV_AddProd.Rows(i).FindControl("chb_AddDetalle"), CheckBox).Checked Then

                IdProducto = CInt(CType(Me.DGV_AddProd.Rows(i).FindControl("hddIdProducto"), HiddenField).Value)

                ObjDscto = (New Negocio.DsctoVolVtas).DsctoVolVtas_SelectNew(Me.txtFechaInicio.Text, Me.txtFechaFin.Text, Val(Me.txtCantidadMin.Text), Val(Me.txtCantidadMax.Text), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboTipoPrecioV.SelectedValue), CDec(Me.txtPorcentajeDscto.Text), 0, 0, IdProducto, "", "", Nothing, 0, 1)(0)
                
                If ObjDscto IsNot Nothing Then
                    Me.ListaDscto_Detalle.Add(ObjDscto)
                End If

            End If
        Next

        Me.S_ListaDsctoDetalle = Me.ListaDscto_Detalle

        Me.GV_Detalle.DataSource = Me.ListaDscto_Detalle
        Me.GV_Detalle.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');    alert('Se agregó el producto correctamente.');        ", True)

    End Sub

    Private Sub btnLimpiar_Detallado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_Detallado.Click

        Me.S_ListaDsctoDetalle = New List(Of Entidades.DsctoVolVtas)
        Me.GV_Detalle.DataBind()

    End Sub

#Region "************************** Buscar Producto ***"

    Private Function obtenerListaAdd_Detalle(ByVal TipoMov As Integer) As List(Of Entidades.DsctoVolVtas)

        Dim listaAdd As New List(Of Entidades.DsctoVolVtas)

        Dim index As Integer = 0
        Select Case TipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Detalle.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Detalle.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Detalle.Text) - 1)
        End Select



        listaAdd = (New Negocio.DsctoVolVtas).DsctoVolVtas_SelectNew(Me.txtFechaInicio.Text, Me.txtFechaFin.Text, Val(Me.txtCantidadMin.Text), Val(Me.txtCantidadMax.Text), CInt(ViewState("IdTiendaDet")), CInt(ViewState("IdTipoPVDet")), CDec(ViewState("DsctoDet")), CInt(ViewState("IdLineaDet")), CInt(ViewState("IdSubLineaDet")), CInt(ViewState("IdProductoDet")), CStr(ViewState("CodigoProductoDet")), CStr(ViewState("ProductoDet")), CType(Session("objDataTable"), DataTable), index, CInt(Me.cboPageSize.SelectedValue))
        txtPageIndex_Detalle.Text = CStr(index + 1)


        Return listaAdd

    End Function

    Private Sub btnAnterior_Detalle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Detalle.Click

        Me.ListaDscto_Detalle = obtenerListaAdd_Detalle(1)
        Me.S_ListaDsctoDetalle = Me.ListaDscto_Detalle

        Me.GV_Detalle.DataSource = Me.ListaDscto_Detalle
        Me.GV_Detalle.DataBind()

    End Sub

    Private Sub btnSiguiente_Detalle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSiguiente_Detalle.Click

        Me.ListaDscto_Detalle = obtenerListaAdd_Detalle(2)
        Me.S_ListaDsctoDetalle = Me.ListaDscto_Detalle

        Me.GV_Detalle.DataSource = Me.ListaDscto_Detalle
        Me.GV_Detalle.DataBind()

    End Sub

    Private Sub btnIr_Detalle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Detalle.Click


        Me.ListaDscto_Detalle = obtenerListaAdd_Detalle(3)
        Me.S_ListaDsctoDetalle = Me.ListaDscto_Detalle

        Me.GV_Detalle.DataSource = Me.ListaDscto_Detalle
        Me.GV_Detalle.DataBind()

    End Sub



#End Region




    Dim ListaDscto_General As List(Of Entidades.DsctoVolVtas)

    Private Sub btnAgregar_Generalidades_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAgregar_Generalidades.Click

        Agregar_Generalidades()

    End Sub

    Private Sub Actualizar_Generalidades(Optional ByVal Registar As Boolean = False)

        Me.ListaDscto_General = Me.S_ListaDsctoGeneral
        If IsNothing(Me.ListaDscto_General) Then Me.ListaDscto_General = New List(Of Entidades.DsctoVolVtas)

        If Me.ListaDscto_General.Count > 0 Then

            For i As Integer = 0 To Me.GV_General.Rows.Count - 1

                With Me.ListaDscto_General(i)

                    .Dscto = CDec(Me.txtPorcentajeDscto.Text)
                    .CantMax = CDec(Me.txtCantidadMax.Text)
                    .CantMin = CDec(Me.txtCantidadMin.Text)
                    .FechaIni = CDate(Me.txtFechaInicio.Text)
                    .FechaFin = CDate(Me.txtFechaFin.Text)
                    .Estado = True ' *** ACTIVO

                End With

            Next

        End If

        Me.S_ListaDsctoGeneral = Me.ListaDscto_General

    End Sub

    Property S_ListaDsctoGeneral() As List(Of Entidades.DsctoVolVtas)
        Get
            Return CType(Session("ListaDsctoGeneral"), List(Of Entidades.DsctoVolVtas))
        End Get
        Set(ByVal value As List(Of Entidades.DsctoVolVtas))
            Session.Remove("ListaDsctoGeneral")
            Session.Add("ListaDsctoGeneral", value)
        End Set
    End Property

    Private Sub Agregar_Generalidades()

        Try

            Actualizar_Generalidades()

            Me.ListaDscto_General = Me.S_ListaDsctoGeneral

            Me.ListaDscto_General.Add(Add_ObjDsctoGeneral)

            Me.S_ListaDsctoGeneral = Me.ListaDscto_General

            Me.GV_General.DataSource = Me.ListaDscto_General
            Me.GV_General.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function Add_ObjDsctoGeneral() As Entidades.DsctoVolVtas

        Dim ObjDsctoGeneral As New Entidades.DsctoVolVtas

        With ObjDsctoGeneral

            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .Empresa = Me.cboEmpresa.SelectedItem.Text.Trim

            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .Tienda = Me.cboTienda.SelectedItem.Text.Trim

            .IdPerfil = CInt(Me.cboPerfil.SelectedValue)
            .Perfil = Me.cboPerfil.SelectedItem.Text.Trim

            .IdCondicionPago = CInt(Me.cboCondicionPago.SelectedValue)
            .CondicionPago = Me.cboCondicionPago.SelectedItem.Text.Trim

            .IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
            .MedioPago = Me.cboMedioPago.SelectedItem.Text.Trim

            .IdTipoPV = CInt(Me.cboTipoPrecioV.SelectedValue)
            .TipoPV = Me.cboTipoPrecioV.SelectedItem.Text.Trim

            .IdLinea = CInt(Me.cboLinea.SelectedValue)
            .Linea = Me.cboLinea.SelectedItem.Text.Trim

            .IdSubLinea = CInt(Me.cboSubLinea.SelectedValue)
            .SubLinea = Me.cboSubLinea.SelectedItem.Text.Trim

        End With

        Return ObjDsctoGeneral

    End Function

    Private Sub btnRegistrarGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegistrarGeneral.Click

        Actualizar_Generalidades(True)
        Me.ListaDscto_General = Me.S_ListaDsctoGeneral


        If (New Negocio.DsctoVolVtas).DsctoVolVtas_Registrar(Me.ListaDscto_General) Then

            objScript.mostrarMsjAlerta(Me, "La operación finalizó con éxito")

        End If

    End Sub

    Protected Sub lkbQuitar_General_Click(ByVal sender As Object, ByVal e As EventArgs)
        Quitar_General(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
    End Sub

    Private Sub Quitar_General(ByVal Index As Integer)

        Try

            Me.ListaDscto_General = Me.S_ListaDsctoGeneral
            Me.ListaDscto_General.RemoveAt(Index)

            Me.S_ListaDsctoGeneral = Me.ListaDscto_General

            Me.GV_General.DataSource = Me.ListaDscto_General
            Me.GV_General.DataBind()


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnLimpiar_Generalidades_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_Generalidades.Click

        Me.S_ListaDsctoGeneral = New List(Of Entidades.DsctoVolVtas)
        Me.GV_General.DataBind()

    End Sub



#Region "********************* CONSULTAR DETALLE ****** '"

    Dim ListaDscto_DetalleView As List(Of Entidades.DsctoVolVtas)

    Private Sub btnConsultarDetalleView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConsultarDetalleView.Click
        Try
            LimpiarSession()
            Dim objDataTable As DataTable = obtenerDataTable_TipoTablaValor("ListaFiltroTablaValor3")
            Session.Add("objDataTable", objDataTable)
            Session.Add("objDataTableView", objDataTable)
            ConsultarDetalleView()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub ConsultarDetalleView()
        Try

            ListaDscto_DetalleView = New List(Of Entidades.DsctoVolVtas)
            ViewState.Add("IdEmpresaView", CInt(Me.cboEmpresaP.SelectedValue))
            ViewState.Add("IdTiendaView", CInt(Me.cboTiendaP.SelectedValue))
            ViewState.Add("IdTipoPrecioVView", CInt(Me.cboTipoPrecioVP.SelectedValue))
            ViewState.Add("IdPerfilView", CInt(Me.cboPerfilP.SelectedValue))
            ViewState.Add("IdCondicionPagoView", CInt(Me.cboCondicionPagoP.SelectedValue))
            ViewState.Add("IdMedioPagoView", CInt(Me.cboMedioPagoP.SelectedValue))

            ViewState.Add("IdLineaView", CInt(Me.cboLineaP.SelectedValue))
            ViewState.Add("IdSubLineaView", CInt(Me.cboSubLineaP.SelectedValue))

            ViewState.Add("CodigoProductoView", Me.txtCodigoProductoP.Text.Trim)
            ViewState.Add("ProductoView", Me.txtProductoP.Text.Trim)

            ViewState.Add("DsctoView", CDec(Me.txtPorcentajeDsctoP.Text))
            ViewState.Add("CantMaxView", CDec(Me.txtCantidadMaxP.Text))
            ViewState.Add("CantMinView", CDec(Me.txtCantidadMinP.Text))
            ViewState.Add("FechaIniView", Me.txtFechaInicioP.Text.Trim)
            ViewState.Add("FechaFinView", Me.txtFechaFinP.Text.Trim)
            ViewState.Add("EstadoView", CBool(IIf(Me.rbActivoD.SelectedValue = "1", True, False)))

            Dim objDataTable As DataTable = obtenerDataTable_TipoTablaValor("ListaFiltroTablaValor3")
            'Session.Remove("objDataTable")
            Session.Add("objDataTable", objDataTable)
            'Session.Remove("objDataTableView")
            Session.Add("objDataTableView", objDataTable)

            Me.ListaDscto_DetalleView.AddRange(obtenerListaAdd_DetalleView(0))

            Me.GV_DetalleView.DataSource = Me.ListaDscto_DetalleView
            Me.GV_DetalleView.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function obtenerListaAdd_DetalleView(ByVal TipoMov As Integer) As List(Of Entidades.DsctoVolVtas)
        Try

            Dim ObjDscto_DetalleView As New Entidades.DsctoVolVtas

            With ObjDscto_DetalleView

                .IdEmpresa = CInt(ViewState("IdEmpresaView"))
                .IdTienda = CInt(ViewState("IdTiendaView"))
                .IdPerfil = CInt(ViewState("IdPerfilView"))
                .IdCondicionPago = CInt(ViewState("IdCondicionPagoView"))
                .IdMedioPago = CInt(ViewState("IdMedioPagoView"))
                .IdTipoPV = CInt(ViewState("IdTipoPrecioVView"))
                .IdLinea = CInt(ViewState("IdLineaView"))
                .IdSubLinea = CInt(ViewState("IdSubLineaView"))
                .CodigoProducto = CStr(ViewState("CodigoProductoView"))
                .Producto = CStr(ViewState("ProductoView"))
                .Dscto = CDec(ViewState("DsctoView"))
                .CantMax = CDec(ViewState("CantMaxView"))
                .CantMin = CDec(ViewState("CantMinView"))
                .FechaIni = CDate(ViewState("FechaIniView"))
                .FechaFin = CDate(ViewState("FechaFinView"))
                .Estado = CBool(ViewState("EstadoView"))

            End With


            Dim listaAdd As New List(Of Entidades.DsctoVolVtas)

            Dim index As Integer = 0
            Select Case TipoMov
                Case 0 '**************** INICIO
                    index = 0
                Case 1 '**************** Anterior
                    index = (CInt(txtPageIndex_DetalleView.Text) - 1) - 1
                Case 2 '**************** Posterior
                    index = (CInt(txtPageIndex_DetalleView.Text) - 1) + 1
                Case 3 '**************** IR
                    index = (CInt(txtPageIndexGO_DetalleView.Text) - 1)
            End Select

            listaAdd = (New Negocio.DsctoVolVtas).DsctoVolVtas_SelectD(ObjDscto_DetalleView, CType(ViewState("objDataTableView"), DataTable), index, CInt(Me.cboPageSizeGView.SelectedValue))

            txtPageIndex_DetalleView.Text = CStr(index + 1)

            Return listaAdd

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnAnterior_DetalleView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_DetalleView.Click

        Me.GV_DetalleView.DataSource = obtenerListaAdd_DetalleView(1)
        Me.GV_DetalleView.DataBind()

    End Sub

    Private Sub btnSiguiente_DetalleView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSiguiente_DetalleView.Click

        Me.GV_DetalleView.DataSource = obtenerListaAdd_DetalleView(2)
        Me.GV_DetalleView.DataBind()

    End Sub

    Private Sub btnIr_DetalleView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_DetalleView.Click

        Me.GV_DetalleView.DataSource = obtenerListaAdd_DetalleView(3)
        Me.GV_DetalleView.DataBind()

    End Sub

#End Region


#Region " ************************ CONSULTAR GENERAL ***********"



#End Region

    Dim ListaDscto_GeneralView As List(Of Entidades.DsctoVolVtas)

    Private Sub btnConsultarGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConsultarGeneral.Click
        ConsultarGeneral()
    End Sub

    Private Sub ConsultarGeneral()

        ViewState.Add("IdEmpresaViewG", CInt(Me.cboEmpresaLS.SelectedValue))
        ViewState.Add("IdTiendaViewG", CInt(Me.cboTiendaLS.SelectedValue))
        ViewState.Add("IdPerfilViewG", CInt(Me.cboPerfilLS.SelectedValue))
        ViewState.Add("IdCondicionPagoViewG", CInt(Me.cboCondicionPagoLS.SelectedValue))
        ViewState.Add("IdMedioPagoViewG", CInt(Me.cboMedioPagoLS.SelectedValue))
        ViewState.Add("IdTipoPVViewG", CInt(Me.cboTipoPrecioVLS.SelectedValue))

        ViewState.Add("DsctoViewG", CDec(Me.txtPorcentajeDsctoLS.Text))
        ViewState.Add("CantMaxViewG", CDec(Me.txtCantidadMaxLS.Text))
        ViewState.Add("CantMinViewG", CDec(Me.txtCantidadMinLS.Text))
        ViewState.Add("FechaIniViewG", Me.txtFechaInicioLS.Text.Trim)
        ViewState.Add("FechaFinViewG", Me.txtFechaFinLS.Text.Trim)
        ViewState.Add("EstadoViewG", CBool(IIf(Me.rbActivoG.SelectedValue = "1", True, False)))

        Me.GV_GeneralView.DataSource = obtenerListaAdd_GeneralView(0)
        Me.GV_GeneralView.DataBind()

    End Sub

    Private Function obtenerListaAdd_GeneralView(ByVal TipoMov As Integer) As List(Of Entidades.DsctoVolVtas)

        Dim ObjDscto_GeneralView As New Entidades.DsctoVolVtas

        With ObjDscto_GeneralView

            .IdEmpresa = CInt(ViewState("IdEmpresaViewG"))
            .IdTienda = CInt(ViewState("IdTiendaViewG"))
            .IdPerfil = CInt(ViewState("IdPerfilViewG"))
            .IdCondicionPago = CInt(ViewState("IdCondicionPagoViewG"))
            .IdMedioPago = CInt(ViewState("IdMedioPagoViewG"))
            .IdTipoPV = CInt(ViewState("IdTipoPVViewG"))

            .Dscto = CDec(ViewState("DsctoViewG"))
            .CantMax = CDec(ViewState("CantMaxViewG"))
            .CantMin = CDec(ViewState("CantMinViewG"))
            .FechaIni = CDate(ViewState("FechaIniViewG"))
            .FechaFin = CDate(ViewState("FechaFinViewG"))
            .Estado = CBool(ViewState("EstadoViewG"))
        End With


        Dim listaAdd As New List(Of Entidades.DsctoVolVtas)

        Dim index As Integer = 0
        Select Case TipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_GeneralView.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_GeneralView.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_GeneralView.Text) - 1)
        End Select

        listaAdd = (New Negocio.DsctoVolVtas).DsctoVolVtas_SelectG(ObjDscto_GeneralView, index, CInt(Me.cboPageSizeGView.SelectedValue))

        If listaAdd.Count > 0 Then
            txtPageIndex_GeneralView.Text = CStr(index + 1)
        Else
            txtPageIndex_GeneralView.Text = ""
        End If

        Return listaAdd

    End Function

    Private Sub btnAnterior_GeneralView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_GeneralView.Click

        Me.GV_GeneralView.DataSource = obtenerListaAdd_GeneralView(1)
        Me.GV_GeneralView.DataBind()

    End Sub

    Private Sub btnSiguiente_GeneralView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSiguiente_GeneralView.Click

        Me.GV_GeneralView.DataSource = obtenerListaAdd_GeneralView(2)
        Me.GV_GeneralView.DataBind()

    End Sub

    Private Sub btnIr_GeneralView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_GeneralView.Click

        Me.GV_GeneralView.DataSource = obtenerListaAdd_GeneralView(3)
        Me.GV_GeneralView.DataBind()

    End Sub

    Private Sub BtnUpdateEstadoG_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnUpdateEstadoG.Click
        UpdateEstadoG()
    End Sub

    Private Sub UpdateEstadoG()

        Dim Lista As New List(Of Entidades.DsctoVolVtas)

        Dim FechaIni As Date = Nothing
        Dim FechaFin As Date = Nothing
        Dim cantidadMin As Decimal = 0
        Dim cantidadMax As Decimal = 0
        Dim PorcentajeDscto As Decimal = 0

        Try

            For i As Integer = 0 To Me.GV_GeneralView.Rows.Count - 1

                If CType(Me.GV_GeneralView.Rows(i).FindControl("ckNoAfectar"), CheckBox).Checked = False Then

                    If IsNumeric(CType(Me.GV_GeneralView.Rows(i).FindControl("txtCantidadMin_PGV"), TextBox).Text) Then cantidadMin = CDec(CType(Me.GV_GeneralView.Rows(i).FindControl("txtCantidadMin_PGV"), TextBox).Text)
                    If IsNumeric(CType(Me.GV_GeneralView.Rows(i).FindControl("txtCantidadMax_PGV"), TextBox).Text) Then cantidadMax = CDec(CType(Me.GV_GeneralView.Rows(i).FindControl("txtCantidadMax_PGV"), TextBox).Text)

                    If IsNumeric(CType(Me.GV_GeneralView.Rows(i).FindControl("txtPorcenajeDsctoP_U"), TextBox).Text) Then PorcentajeDscto = CDec(CType(Me.GV_GeneralView.Rows(i).FindControl("txtPorcenajeDsctoP_U"), TextBox).Text)

                    If IsDate(CType(Me.GV_GeneralView.Rows(i).FindControl("txtFechaIni_PGV"), TextBox).Text) Then FechaIni = CDate(CType(Me.GV_GeneralView.Rows(i).FindControl("txtFechaIni_PGV"), TextBox).Text)
                    If IsDate(CType(Me.GV_GeneralView.Rows(i).FindControl("txtFechaFin_PGV"), TextBox).Text) Then FechaFin = CDate(CType(Me.GV_GeneralView.Rows(i).FindControl("txtFechaFin_PGV"), TextBox).Text)

                    If PorcentajeDscto = 0 Or FechaIni > FechaFin Or IsNothing(FechaIni) Or IsNothing(FechaFin) Then
                        Throw New Exception("Verifique los datos del registro Nro " + CStr(i + 1) + ". No se permite la operaciòn")
                        Return
                    End If

                    Lista.Add(New Entidades.DsctoVolVtas(CInt(CType(Me.GV_GeneralView.Rows(i).FindControl("hddIdDsctoVolVtas"), HiddenField).Value), CType(Me.GV_GeneralView.Rows(i).FindControl("ckEstado"), CheckBox).Checked, FechaIni, FechaFin, cantidadMin, cantidadMax, PorcentajeDscto))

                    cantidadMin = 0
                    cantidadMax = 0
                    PorcentajeDscto = 0
                    FechaIni = Nothing
                    FechaFin = Nothing

                End If

            Next

            Dim Mensaje As String = ""

            Mensaje = (New Negocio.DsctoVolVtas).DsctoVolVtas_UpdateEstG(Lista)

            objScript.mostrarMsjAlerta(Me, Mensaje)

            Me.GV_GeneralView.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub BtnUpdateEstadoD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnUpdateEstadoD.Click
        UpdateEstadoD()
    End Sub

    Private Sub UpdateEstadoD()

        Dim Lista As New List(Of Entidades.DsctoVolVtas)

        Dim FechaIni As Date = Nothing
        Dim FechaFin As Date = Nothing
        Dim cantidadMin As Decimal = 0
        Dim cantidadMax As Decimal = 0
        Dim PorcentajeDscto As Decimal = 0

        Try

            For i As Integer = 0 To Me.GV_DetalleView.Rows.Count - 1

                If CType(Me.GV_DetalleView.Rows(i).FindControl("ckNoAfectar"), CheckBox).Checked = False Then

                    If IsNumeric(CType(Me.GV_DetalleView.Rows(i).FindControl("txtCantidadMin_PGV"), TextBox).Text) Then cantidadMin = CDec(CType(Me.GV_DetalleView.Rows(i).FindControl("txtCantidadMin_PGV"), TextBox).Text)
                    If IsNumeric(CType(Me.GV_DetalleView.Rows(i).FindControl("txtCantidadMax_PGV"), TextBox).Text) Then cantidadMax = CDec(CType(Me.GV_DetalleView.Rows(i).FindControl("txtCantidadMax_PGV"), TextBox).Text)

                    If IsNumeric(CType(Me.GV_DetalleView.Rows(i).FindControl("txtPorcenajeDsctoP_U"), TextBox).Text) Then PorcentajeDscto = CDec(CType(Me.GV_DetalleView.Rows(i).FindControl("txtPorcenajeDsctoP_U"), TextBox).Text)

                    If IsDate(CType(Me.GV_DetalleView.Rows(i).FindControl("txtFechaIni_PGV"), TextBox).Text) Then FechaIni = CDate(CType(Me.GV_DetalleView.Rows(i).FindControl("txtFechaIni_PGV"), TextBox).Text)
                    If IsDate(CType(Me.GV_DetalleView.Rows(i).FindControl("txtFechaFin_PGV"), TextBox).Text) Then FechaFin = CDate(CType(Me.GV_DetalleView.Rows(i).FindControl("txtFechaFin_PGV"), TextBox).Text)

                    If PorcentajeDscto = 0 Or FechaIni > FechaFin Or IsNothing(FechaIni) Or IsNothing(FechaFin) Then
                        Throw New Exception("Verifique los datos del registro Nro " + CStr(i + 1) + ". No se permite la operaciòn")
                        Return
                    End If

                    Lista.Add(New Entidades.DsctoVolVtas(CInt(CType(Me.GV_DetalleView.Rows(i).FindControl("hddIdDsctoVolVtas"), HiddenField).Value), CType(Me.GV_DetalleView.Rows(i).FindControl("ckEstado"), CheckBox).Checked, FechaIni, FechaFin, cantidadMin, cantidadMax, PorcentajeDscto))

                    cantidadMin = 0
                    cantidadMax = 0
                    PorcentajeDscto = 0
                    FechaIni = Nothing
                    FechaFin = Nothing

                End If

            Next

            Dim Mensaje As String = ""

            Mensaje = (New Negocio.DsctoVolVtas).DsctoVolVtas_UpdateEst(Lista)

            objScript.mostrarMsjAlerta(Me, Mensaje)

            Me.GV_DetalleView.DataBind()


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub




#Region " **************** Buscar Tabla *** "
    Private ListaTipoTabla As List(Of Entidades.TipoTabla)

    Private Sub ImgBuscarTabla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBuscarTabla.Click
        Buscar_Tabla()
    End Sub
    Private Sub Buscar_Tabla()

        Try

            Me.ViewState.Add("BuscarTabla", Me.txtBuscarTabla.Text.Trim)
            Me.Find_Tabla(0)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub Find_Tabla(ByVal TipoMov As Integer)

        Dim Index As Integer

        Select Case TipoMov
            Case 0
                Index = 0
            Case 1
                Index = (CInt(Me.txtPageIndexTabla.Text) - 1) - 1 ' ******* ANTERIOR
            Case 2
                Index = (CInt(Me.txtPageIndexTabla.Text) - 1) + 1 ' ******* SIGUIENTE
            Case 3
                Index = (CInt(Me.txtPageIndexGoTabla.Text) - 1) - 1 ' ******* IR
        End Select

        Dim lista As List(Of Entidades.TipoTabla) = (New Negocio.TipoTabla).TipoTablaSelectAllxEstado_Paginado(CStr(Me.ViewState("BuscarTabla")), 1, Index, Me.GV_Tabla.PageSize)

        If Not IsNothing(lista) Then

            If lista.Count > 0 Then

                Me.txtPageIndexTabla.Text = CStr(Index + 1)

                Me.GV_Tabla.DataSource = lista
                Me.GV_Tabla.DataBind()
                Me.GV_Tabla.SelectedIndex = -1

            Else
                objScript.mostrarMsjAlerta(Me, "No se hallaron Registros.")
            End If

        End If


        ScriptManager.RegisterStartupScript(Me, Me.GetType, " onload ", " onCapa('Div_Tabla'); ", True)


    End Sub

    Private Sub btnAnteriorTabla_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnteriorTabla.Click
        Find_Tabla(1)
    End Sub

    Private Sub btnSiguienteTabla_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSiguienteTabla.Click
        Find_Tabla(2)
    End Sub

    Private Sub btnIrTabla_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIrTabla.Click
        Find_Tabla(3)
    End Sub


    Private Sub GV_Tabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Tabla.SelectedIndexChanged

        Me.lblNomTabla.Text = HttpUtility.HtmlDecode(CType(Me.GV_Tabla.Rows(Me.GV_Tabla.SelectedRow.RowIndex).FindControl("lblTabla"), Label).Text).Trim

        ScriptManager.RegisterStartupScript(Me, Me.GetType, " onload ", " onCapa2('Div_TablaValor'); onCapa('Div_Tabla'); ", True)

    End Sub

#End Region

#Region " **************** Buscar Tabla Valor *** "

    Private ListaTipoTablaValor As List(Of Entidades.TipoTablaValor)
    Private ListaFiltroTablaValor As List(Of Entidades.TipoTablaValor)

    Private Property S_TipoTablaValor() As List(Of Entidades.TipoTablaValor)
        Get
            Return CType(Session.Item("ListaTipoTablaValor3"), List(Of Entidades.TipoTablaValor))
        End Get
        Set(ByVal value As List(Of Entidades.TipoTablaValor))
            Session.Remove("ListaTipoTablaValor3")
            Session.Add("ListaTipoTablaValor3", value)
        End Set
    End Property
    Private Property S_FiltroTablaValor() As List(Of Entidades.TipoTablaValor)
        Get
            Return CType(Session.Item("ListaFiltroTablaValor3"), List(Of Entidades.TipoTablaValor))
        End Get
        Set(ByVal value As List(Of Entidades.TipoTablaValor))
            Session.Remove("ListaFiltroTablaValor3")
            Session.Add("ListaFiltroTablaValor3", value)
        End Set
    End Property


    Private Sub ImgBuscarTablaValor_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBuscarTablaValor.Click
        Buscar_TablaValor()
    End Sub
    Private Sub Buscar_TablaValor()

        Dim Index As Integer = Me.GV_Tabla.SelectedRow.RowIndex

        If Index < 0 Then
            objScript.mostrarMsjAlerta(Me, "Seleccione un tipo de tabla")
            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onload ", " oncapa('Div_Tabla'); ", True)
            Exit Sub

        End If


        Me.ViewState.Add("BuscarTablaValor", Me.txtBuscarTablaValor.Text.Trim)
        Me.ViewState.Add("IdTipoTabla", CType(Me.GV_Tabla.Rows(Index).FindControl("hddIdTipoTabla"), HiddenField).Value)
        Find_TablaValor(0)

    End Sub
    Private Sub Find_TablaValor(ByVal TipoMov As Integer)
        Try
            Dim Index As Integer

            Select Case TipoMov
                Case 0
                    Index = 0
                Case 1
                    Index = (CInt(Me.txtPageIndexTablaValor.Text) - 1) - 1 ' ******* ANTERIOR
                Case 2
                    Index = (CInt(Me.txtPageIndexTablaValor.Text) - 1) + 1 ' ******* SIGUIENTE
                Case 3
                    Index = (CInt(Me.txtPageIndexGoTablaValor.Text) - 1) - 1 ' ******* IR
            End Select

            ListaTipoTablaValor = (New Negocio.TipoTablaValor).TipoTablaValorSelectAllxIdTipoTablaxEstado_Paginado(CInt(Me.ViewState("IdTipoTabla")), CStr(Me.ViewState("BuscarTablaValor")), 1, Index, Me.GV_TablaValor.PageSize)

            If Not IsNothing(ListaTipoTablaValor) Then

                If ListaTipoTablaValor.Count > 0 Then

                    S_TipoTablaValor = ListaTipoTablaValor

                    Me.txtPageIndexTablaValor.Text = CStr(Index + 1)

                    Me.GV_TablaValor.DataSource = ListaTipoTablaValor
                    Me.GV_TablaValor.DataBind()


                Else
                    objScript.mostrarMsjAlerta(Me, "No se hallaron Registros.")
                End If

            End If

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onload ", "onCapa2('Div_TablaValor'); onCapa('Div_Tabla'); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnAnteriorTablaValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnteriorTablaValor.Click
        Find_TablaValor(1)
    End Sub

    Private Sub btnSiguienteTablaValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSiguienteTablaValor.Click
        Find_TablaValor(2)
    End Sub

    Private Sub btnIrTablaValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIrTablaValor.Click
        Find_TablaValor(3)
    End Sub

    Private Sub ImgAgregarTablaValor_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgAgregarTablaValor.Click
        Agregar_TablaValor()
    End Sub
    Private Sub Agregar_TablaValor()
        Try
            ListaFiltroTablaValor = S_FiltroTablaValor
            ListaTipoTablaValor = S_TipoTablaValor

            Dim Lista As New List(Of Entidades.TipoTablaValor)

            For i As Integer = 0 To Me.GV_TablaValor.Rows.Count - 1
                If CType(Me.GV_TablaValor.Rows(i).FindControl("ckTablaValor"), CheckBox).Checked Then

                    ListaFiltroTablaValor.Add(ListaTipoTablaValor(i))

                End If
            Next

            Me.S_FiltroTablaValor = ListaFiltroTablaValor
            Me.GV_FiltroTipoTablaValor.DataSource = ListaFiltroTablaValor
            Me.GV_FiltroTipoTablaValor.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onload ", " onCapa2('Div_TablaValor'); onCapa('Div_Tabla'); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_FiltroTipoTablaValor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_FiltroTipoTablaValor.SelectedIndexChanged

        Me.ListaFiltroTablaValor = Me.S_FiltroTablaValor
        Me.ListaFiltroTablaValor.RemoveAt(Me.GV_FiltroTipoTablaValor.SelectedRow.RowIndex)
        Me.S_FiltroTablaValor = ListaFiltroTablaValor

        Me.GV_FiltroTipoTablaValor.DataSource = ListaFiltroTablaValor
        Me.GV_FiltroTipoTablaValor.DataBind()

    End Sub

#End Region





    Private Sub btnConsultar_Atributo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConsultar_Atributo.Click
        Consultar_Atributo()
    End Sub

    Sub Consultar_Atributo()

        Try

            Actualizar_ListaDetalle(True)
            Me.ListaDscto_Detalle = Me.S_ListaDsctoDetalle

            ViewState.Add("IdTiendaDet", CInt(Me.cboTienda.SelectedValue))
            ViewState.Add("IdPerfilDet", CInt(Me.cboPerfil.SelectedValue))
            ViewState.Add("IdTipoPVDet", CInt(Me.cboTipoPrecioV.SelectedValue))

            ViewState.Add("IdLineaDet", 0)
            ViewState.Add("IdSubLineaDet", 0)
            ViewState.Add("CodigoProductoDet", "")
            ViewState.Add("ProductoDet", "")

            ViewState.Add("DsctoDet", CDec(Me.txtPorcentajeDscto.Text.Trim))

            Dim objDataTable As DataTable = obtenerDataTable_TipoTablaValor("ListaFiltroTablaValor3")
            Session.Add("objDataTable", objDataTable)

            Me.ListaDscto_Detalle.AddRange(obtenerListaAdd_Detalle(0))

            Me.S_ListaDsctoDetalle = Me.ListaDscto_Detalle

            Me.GV_Detalle.DataSource = Me.ListaDscto_Detalle
            Me.GV_Detalle.DataBind()


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Private Function obtenerDataTable_TipoTablaValor(ByVal nombre As String) As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim ListaTipoTablaValor As List(Of Entidades.TipoTablaValor)

        Try
            ListaTipoTablaValor = CType(Session.Item(nombre), List(Of Entidades.TipoTablaValor))

            For i As Integer = 0 To ListaTipoTablaValor.Count - 1
                dt.Rows.Add(ListaTipoTablaValor(i).IdTipoTabla, ListaTipoTablaValor(i).IdTipoTablaValor)
            Next

        Catch ex As Exception

        End Try


        Return dt
    End Function


    Private Sub TabC_Dscto_ActiveTabChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabC_Dscto.ActiveTabChanged
        ViewState.Add("TabIndex", Me.TabC_Dscto.ActiveTabIndex)
    End Sub

    Private Property ListaTiendaRef() As List(Of Entidades.Tienda)
        Get
            Return CType(Session.Item("ListaTiendaRep"), List(Of Entidades.Tienda))
        End Get
        Set(ByVal value As List(Of Entidades.Tienda))
            Session.Remove("ListaTiendaRep")
            Session.Add("ListaTiendaRep", value)
        End Set
    End Property

    Private listaTiendaReferencia As List(Of Entidades.Tienda)

    Private Sub btnAddTiendaRef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddTiendaRef.Click
        OnClick_AddTiendaRef()
    End Sub
    Private Sub OnClick_AddTiendaRef()
        Try
            Me.listaTiendaReferencia = ListaTiendaRef
            If Me.listaTiendaReferencia Is Nothing Then Me.listaTiendaReferencia = New List(Of Entidades.Tienda)
            Me.listaTiendaReferencia.RemoveAll(Function(k As Entidades.Tienda) k.Id = CInt(cboTiendaReplica.SelectedValue))

            If cboTiendaReplica.SelectedValue = "0" Then
                Dim Lista As List(Of Entidades.Tienda) = (New Negocio.Tienda).SelectCboxEmpresa(CInt(Me.cboEmpresa.SelectedValue))
                Me.listaTiendaReferencia = New List(Of Entidades.Tienda)
                Me.listaTiendaReferencia.AddRange(Lista)
            Else
                Me.listaTiendaReferencia.AddRange((New Negocio.Tienda).SelectxId(CInt(cboTiendaReplica.SelectedValue)))
            End If

            Me.ListaTiendaRef = Me.listaTiendaReferencia

            Me.GV_TiendaReplica.DataSource = Me.listaTiendaReferencia
            Me.GV_TiendaReplica.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub btnQuitarTienda_Click(ByVal sender As Object, ByVal e As EventArgs)
        QuitarTienda(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
    End Sub

    Private Sub QuitarTienda(ByVal Index As Integer)
        Me.listaTiendaReferencia = ListaTiendaRef

        Me.listaTiendaReferencia.RemoveAt(Index)

        Me.ListaTiendaRef = Me.listaTiendaReferencia

        Me.GV_TiendaReplica.DataSource = Me.listaTiendaReferencia
        Me.GV_TiendaReplica.DataBind()

    End Sub

    Private Sub btnLimpiarTienda_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiarTienda.Click
        Me.ListaTiendaRef = New List(Of Entidades.Tienda)
        Me.GV_TiendaReplica.DataBind()
    End Sub

    'Protected Sub btnExportarDatos_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExportarDatos.Click
    '    ConsultarparaExportar()
    'End Sub

    'Private Sub ConsultarparaExportar()
    '    Dim ListaDscto_DetalleView As List(Of Entidades.DsctoVolVtas) = New List(Of Entidades.DsctoVolVtas)

    '    Session.Add("IdEmpresaView", CInt(Me.cboEmpresaP.SelectedValue))
    '    Session.Add("IdTiendaView", CInt(Me.cboTiendaP.SelectedValue))
    '    Session.Add("IdTipoPrecioVView", CInt(Me.cboTipoPrecioVP.SelectedValue))
    '    Session.Add("IdPerfilView", CInt(Me.cboPerfilP.SelectedValue))
    '    Session.Add("IdCondicionPagoView", CInt(Me.cboCondicionPagoP.SelectedValue))
    '    Session.Add("IdMedioPagoView", CInt(Me.cboMedioPagoP.SelectedValue))

    '    Session.Add("IdLineaView", CInt(Me.cboLineaP.SelectedValue))
    '    Session.Add("IdSubLineaView", CInt(Me.cboSubLineaP.SelectedValue))

    '    Session.Add("CodigoProductoView", Me.txtCodigoProductoP.Text.Trim)
    '    Session.Add("ProductoView", Me.txtProductoP.Text.Trim)

    '    Session.Add("DsctoView", CDec(Me.txtPorcentajeDsctoP.Text))
    '    Session.Add("CantMaxView", CDec(Me.txtCantidadMaxP.Text))
    '    Session.Add("CantMinView", CDec(Me.txtCantidadMinP.Text))
    '    Session.Add("FechaIniView", Me.txtFechaInicioP.Text.Trim)
    '    Session.Add("FechaFinView", Me.txtFechaFinP.Text.Trim)
    '    Session.Add("EstadoView", CBool(IIf(Me.rbActivoD.SelectedValue = "1", True, False)))

    '    Dim objDataTable As DataTable = obtenerDataTable_TipoTablaValor("ListaFiltroTablaValor3")

    '    'Session.Remove("objDataTable")
    '    Session.Add("objDataTable", objDataTable)

    '    'Session.Remove("objDataTableView")
    '    Session.Add("objDataTableView", objDataTable)

    '    'ListaDscto_DetalleView.AddRange(obtenerListaAdd_DetalleView(0))

    '    Dim url As String = "FrmExportarDatos.aspx"
    '    Dim s As String = "window.open('" & url + "', 'popup_window', 'width=1100,height=640,left=100,resizable=no,scrollbars=yes');"
    '    ClientScript.RegisterStartupScript(Me.GetType(), "pop", s, True)



    '    Dim queryString As String = "FrmExportarDatos.aspx"
    '    Dim newWin As String = "window.open('" & queryString & "');"
    '    ClientScript.RegisterStartupScript(Me.GetType(), "pop", newWin, True)

    'End Sub

    Private Sub GV_Detalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Detalle.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If Me.chb_CargaMasiva.Checked Then
                Dim txtFechaInicio As TextBox = DirectCast(e.Row.FindControl("txtFechaInicio"), TextBox)
                Dim txtFechaFin As TextBox = DirectCast(e.Row.FindControl("txtFechaFinal"), TextBox)                
                txtFechaInicio.Text = Format(Convert.ToDateTime(txtFechaInicio.Text), "dd/MM/yyyy")
                txtFechaFin.Text = Format(Convert.ToDateTime(txtFechaFin.Text), "dd/MM/yyyy")
            End If
            '        '
            '        '
            '        'Dim fechainicio As New DateTime(Date.Now.Year, Date.Now.Month, 1)
            '        'Dim fechafinal As New DateTime(Date.Now.Year, Date.Now.Month, DateTime.DaysInMonth(Date.Now.Year, Date.Now.Month))
            '        'txtFechaInicio.Text = fechainicio
            '        'txtFechaFin.Text = fechafinal
            '        'cbo = New Combo
            '        'Dim cboCondicion As DropDownList = DirectCast(e.Row.FindControl("ddlCondicionpago"), DropDownList)
            '        'Dim cboMedio As DropDownList = DirectCast(e.Row.FindControl("ddlMediopago"), DropDownList)
            '        'Dim cboTipoV As DropDownList = DirectCast(e.Row.FindControl("ddlTipoPrecio"), DropDownList)
            '        'With cbo
            '        '    .LlenarCboCondicionPago(cboCondicion, True)
            '        '    .LlenarCboMedioPagoxIdTipoDocumentoxIdCondicionPago(cboMedio, _IdFactura, CInt(cboCondicion.SelectedValue), True)
            '        '    .LlenarCboTipoPV(cboTipoV, True)
            '        'End With
        End If
    End Sub

    'Protected Sub onclickSelected(ByVal sender As Object, ByVal e As EventArgs)        
    '    Dim objDrop As DropDownList, Item As Integer = sender.BindingContainer.RowIndex(), objDropValor As DropDownList        
    '    objDrop = CType(sender, DropDownList)
    '    objDropValor = Me.GV_Detalle.Rows(Item).FindControl("ddlMediopago")
    '    Dim valor = objDrop.SelectedValue
    '    objDropValor.Items.Clear()
    '    cbo = New Combo
    '    With cbo
    '        .LlenarCboMedioPagoxIdTipoDocumentoxIdCondicionPago(objDropValor, _IdFactura, CInt(valor), True)
    '    End With
    'End Sub

    Private Sub btnBuscarGrilla_AddProducto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarGrilla_AddProducto.Click
        BuscarProductoCatalogo(0)
    End Sub

    Private Sub btnAddProductos_AddProducto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddProductos_AddProducto.Click
        If Me.chb_CargaMasiva.Checked = True Then

            onClick_btnAddProductos_AddProd()

        Else

            onClick_btnAddProductos_AddProd2()

        End If
    End Sub

    Private Sub DGV_AddProd_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGV_AddProd.PageIndexChanging
        Me.DGV_AddProd.PageIndex = e.NewPageIndex
        BuscarProductoCatalogo(0)
    End Sub
End Class
