﻿Imports System.IO
Imports System.Data
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Configuration
Partial Public Class FrmExportarDatos
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ConsultarparaExportar()
        End If
    End Sub
    Private Sub ExportToExcel(ByVal nameReport As String, ByVal wControl As GridView)
        Dim responsePage As HttpResponse = Response
        Dim sw As New StringWriter()
        Dim htw As New HtmlTextWriter(sw)
        Dim pageToRender As New Page()
        Dim form As New HtmlForm()

        form.Controls.Add(wControl)
        pageToRender.Controls.Add(form)
        responsePage.Clear()
        responsePage.Buffer = True
        responsePage.ContentType = "application/vnd.ms-excel"
        responsePage.AddHeader("Content-Disposition", "attachment;filename=" & nameReport)
        responsePage.Charset = "UTF-8"
        responsePage.ContentEncoding = Encoding.Default
        pageToRender.RenderControl(htw)
        responsePage.Write(sw.ToString())
        responsePage.End()

        Me.GridViewExportar.DataSource = Nothing
        Me.GridViewExportar.DataBind()
        LimpiarSession()
        Page.ClientScript.RegisterStartupScript(Me.GetType, "CloseWindow", "setTimeout('window.close();',3000);", True)
    End Sub

    Private Sub ConsultarparaExportar()

        Dim ListaDscto_DetalleView As List(Of Entidades.DsctoVolVtas) = New List(Of Entidades.DsctoVolVtas)

        ListaDscto_DetalleView.AddRange(obtenerListaAdd_DetalleView(0))

        Me.GridViewExportar.DataSource = ListaDscto_DetalleView
        Me.GridViewExportar.DataBind()

        ExportToExcel("DescuentoxVolumen.xls", GridViewExportar)

    End Sub

    Private Function obtenerListaAdd_DetalleView(ByVal TipoMov As Integer) As List(Of Entidades.DsctoVolVtas)

        Dim ObjDscto_DetalleView As New Entidades.DsctoVolVtas

        With ObjDscto_DetalleView

            .IdEmpresa = CInt(Session("IdEmpresaView"))
            .IdTienda = CInt(Session("IdTiendaView"))
            .IdPerfil = CInt(Session("IdPerfilView"))
            .IdCondicionPago = CInt(Session("IdCondicionPagoView"))
            .IdMedioPago = CInt(Session("IdMedioPagoView"))
            .IdTipoPV = CInt(Session("IdTipoPrecioVView"))

            .IdLinea = CInt(Session("IdLineaView"))
            .IdSubLinea = CInt(Session("IdSubLineaView"))

            .CodigoProducto = CStr(Session("CodigoProductoView"))
            .Producto = CStr(Session("ProductoView"))

            .Dscto = CDec(Session("DsctoView"))
            .CantMax = CDec(Session("CantMaxView"))
            .CantMin = CDec(Session("CantMinView"))
            .FechaIni = CDate(Session("FechaIniView"))
            .FechaFin = CDate(Session("FechaFinView"))
            .Estado = CBool(Session("EstadoView"))

        End With


        Dim listaAdd As New List(Of Entidades.DsctoVolVtas)


        listaAdd = (New Negocio.DsctoVolVtas).DsctoVolVtas_SelectD(ObjDscto_DetalleView, CType(ViewState("objDataTableView"), DataTable), 0, 5000)


        Return listaAdd

    End Function
    Private Sub LimpiarSession()
        Session.Remove("IdEmpresaView")
        Session.Remove("IdTiendaView")
        Session.Remove("IdTipoPrecioVView")
        Session.Remove("IdPerfilView")
        Session.Remove("IdCondicionPagoView")
        Session.Remove("IdMedioPagoView")
        Session.Remove("IdLineaView")
        Session.Remove("IdSubLineaView")
        Session.Remove("CodigoProductoView")
        Session.Remove("ProductoView")
        Session.Remove("DsctoView")
        Session.Remove("CantMaxView")
        Session.Remove("CantMinView")
        Session.Remove("FechaIniView")
        Session.Remove("FechaFinView")
        Session.Remove("EstadoView")
    End Sub
End Class