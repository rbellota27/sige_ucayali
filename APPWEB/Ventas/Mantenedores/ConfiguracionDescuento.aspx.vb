﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class ConfiguracionDescuento
    Inherits System.Web.UI.Page

#Region "variables"
    Private objScript As New ScriptManagerClass
    Private Enum operativo
        Ninguno = 0
        GuardarNuevo = 1
        Actualizar = 2
    End Enum
    Private objConfDescuento As Entidades.ConfiguracionDescuento
    Private drop As Combo

#End Region

#Region "Procedimientos"

    Private Sub CargarAlIniciar()
        drop = New Combo
        drop.LlenarCboCondicionPago(dlcondicion)
        drop.LlenarCboMedioPago(dlmedio)
        drop.LlenarCboPerfil(dlperfil)
        drop.LlenarCboTipoPV(dlprecio)

        drop.LlenarCboCondicionPago(cboCondPago)
        drop.LlenarCboMedioPago(cboMedPago, True)
        drop.LlenarCboPerfil(cboPerfil, True)
        drop.LlenarCboTipoPV(cboTipoPrecio)

    End Sub

    Private Sub verBotones(ByVal modo As String)
        Select Case modo
            Case "N"
                btNuevo.Visible = False
                btGuardar.Visible = True
                btCancelar.Visible = True
                pnlPrincipal.Enabled = True
                pnlbusqueda.Enabled = False
            Case "L"
                btNuevo.Visible = True
                btGuardar.Visible = False
                btCancelar.Visible = False
                pnlPrincipal.Enabled = False
                pnlbusqueda.Enabled = True
            Case "G"
                btNuevo.Visible = True
                btGuardar.Visible = False
                btCancelar.Visible = False
                pnlPrincipal.Enabled = False
                pnlbusqueda.Enabled = True
        End Select
    End Sub

    Private Sub Mantenimiento(ByVal tipo As Integer)
        objConfDescuento = New Entidades.ConfiguracionDescuento
        With objConfDescuento
            .IdDescuento = CInt(IIf(tbCodigo.Text = "", 0, tbCodigo.Text))
            .IdPerfil = CInt(dlperfil.SelectedValue)
            .IdCondicionPago = CInt(dlcondicion.SelectedValue)
            .IdMedioPago = CInt(dlmedio.SelectedValue)
            .IdTipoPV = CInt(dlprecio.SelectedValue)
            .desc_Porcentaje = CDec(tbdescuento.Text)
            .desc_Estado = rbestado.SelectedValue

            .Desc_PrecioBaseDscto = rbtPBD.SelectedValue
        End With

        Select Case tipo
            Case operativo.GuardarNuevo
                tbCodigo.Text = CStr((New Negocio.ConfiguracionDescuento).InsertConfiguracionDescuento(objConfDescuento))
                objScript.mostrarMsjAlerta(Me, "El registro ha sido guardado")
            Case operativo.Actualizar
                Dim update As Boolean = (New Negocio.ConfiguracionDescuento).UpdateConfiguracionDescuento(objConfDescuento)
                If update = True Then objScript.mostrarMsjAlerta(Me, "El registro ha sido actualizado")
        End Select

        'llenarGrillaConfDescuento(CInt(rbver.SelectedValue), CInt(cboMedPago.SelectedValue), CInt(cboPerfil.SelectedValue), CInt(cboTipoPrecio.SelectedValue), CInt(cboCondPago.SelectedValue), rbtlPrecioBase.SelectedValue)
        llenarGrillaConfDescuento(CInt(rbver.SelectedValue), CInt(dlmedio.SelectedValue), CInt(dlperfil.SelectedValue), CInt(dlprecio.SelectedValue), CInt(dlcondicion.SelectedValue), rbtlPrecioBase.SelectedValue)
        verBotones("L")

    End Sub

    Private Function getDataSource() As Object
        Return Session.Item("datasource")
    End Function

    Private Sub setDataSource(ByVal obj As Object)
        Session.Remove("datasource")
        Session.Add("datasource", obj)
    End Sub


    Private Sub llenarGrillaConfDescuento(ByVal ver As Integer, ByVal idMedPago As Integer, ByVal idperfil As Integer, ByVal idtipopv As Integer, ByVal idCondicionPago As Integer, ByVal desc_PrecioBaseDscto As String)
        gvBusqueda.DataSource = (New Negocio.ConfiguracionDescuento).SelectConfiguracionDescuento(ver, idMedPago, idperfil, idtipopv, idCondicionPago, desc_PrecioBaseDscto)
        gvBusqueda.DataBind()
        Me.setDataSource(gvBusqueda.DataSource)
    End Sub

    Private Sub LimpiarForm()
        tbdescuento.Text = ""
        tbCodigo.Text = ""
    End Sub

#End Region

#Region "Eventos Principales"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                CargarAlIniciar()
                ViewState.Add("operativo", operativo.Ninguno)
                verBotones("L")
                llenarGrillaConfDescuento(CInt(rbver.SelectedValue), CInt(dlmedio.SelectedValue), CInt(dlperfil.SelectedValue), CInt(dlprecio.SelectedValue), CInt(dlcondicion.SelectedValue), rbtlPrecioBase.SelectedValue)

            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#End Region

#Region "Edicion "
    Protected Sub lkEditarClick(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lk As LinkButton = CType(sender, LinkButton)
            Dim fila As GridViewRow = CType(lk.NamingContainer, GridViewRow)

            Dim IdCondicionPago As Integer = CInt(CType(fila.Cells(1).FindControl("hdd_IdCondicionPago"), HiddenField).Value)
            Dim IdMedioPago As Integer = CInt(CType(fila.Cells(1).FindControl("hdd_IdMedioPago"), HiddenField).Value)
            Dim IdPerfil As Integer = CInt(CType(fila.Cells(1).FindControl("hdd_IdPerfil"), HiddenField).Value)
            Dim IdTipoPV As Integer = CInt(CType(fila.Cells(1).FindControl("hdd_IdTipoPV"), HiddenField).Value)

            If Not dlcondicion.Items.FindByValue(CStr(IdCondicionPago)) Is Nothing Then
                dlcondicion.SelectedValue = CStr(IdCondicionPago)
            End If
            If Not dlmedio.Items.FindByValue(CStr(IdMedioPago)) Is Nothing Then
                dlmedio.SelectedValue = CStr(IdMedioPago)
            End If
            If Not dlperfil.Items.FindByValue(CStr(IdPerfil)) Is Nothing Then
                dlperfil.SelectedValue = CStr(IdPerfil)
            End If
            If Not dlprecio.Items.FindByValue(CStr(IdTipoPV)) Is Nothing Then
                dlprecio.SelectedValue = CStr(IdTipoPV)
            End If

            tbCodigo.Text = fila.Cells(2).Text
            tbdescuento.Text = fila.Cells(7).Text
            rbestado.SelectedValue = CStr(IIf(fila.Cells(9).Text = "Activo", "1", "0"))
            rbtPBD.SelectedValue = CStr(IIf(fila.Cells(10).Text = "Precio Lista", "PL", "PC"))
            ViewState.Add("operativo", operativo.Actualizar)
            verBotones("N")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region


    Private Sub btNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btNuevo.Click
        Try
            LimpiarForm()
            verBotones("N")
            ViewState.Add("operativo", operativo.GuardarNuevo)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btGuardar.Click
        Try
            Mantenimiento(CInt(ViewState("operativo")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btCancelar.Click
        Try
            LimpiarForm()
            verBotones("L")
            ViewState.Add("operativo", operativo.Ninguno)
            gvBusqueda.DataSource = Me.getDataSource()
            gvBusqueda.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Try
            llenarGrillaConfDescuento(CInt(rbver.SelectedValue), CInt(cboMedPago.SelectedValue), CInt(cboPerfil.SelectedValue), CInt(cboTipoPrecio.SelectedValue), CInt(cboCondPago.SelectedValue), rbtlPrecioBase.SelectedValue)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBusqueda_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBusqueda.PageIndexChanging
        Me.gvBusqueda.PageIndex = e.NewPageIndex
        Me.gvBusqueda.DataSource = Me.getDataSource
        Me.gvBusqueda.DataBind()
    End Sub

End Class
