﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="ConfiguracionDescuento.aspx.vb" Inherits="APPWEB.ConfiguracionDescuento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%;">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btNuevo" Width="85px" runat="server" Text="Nuevo" />
                        </td>
                        <td>
                            <asp:Button ID="btGuardar" Width="85px" runat="server" Text="Guardar" OnClientClick="return(validarGuardar());" />
                        </td>
                        <td>
                            <asp:Button ID="btCancelar" OnClientClick="return(confirm('Desea ir al modo Inicio?'));"
                                Width="85px" runat="server" Text="Cancelar" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                Configuraci&oacute;n Descuento
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlPrincipal" runat="server">
                    <table>
                        <tr>
                            <td class="Texto">
                                &nbsp;Codigo:
                            </td>
                            <td>
                                <asp:TextBox ID="tbCodigo" ReadOnly="true" CssClass="TextBoxReadOnly_Right" runat="server"
                                    Width="80px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                &nbsp;Perfil:
                            </td>
                            <td>
                                <asp:DropDownList ID="dlperfil" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                &nbsp;Tipo Precio:
                            </td>
                            <td>
                                <asp:DropDownList ID="dlprecio" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                &nbsp; Condicion de Pago:
                            </td>
                            <td>
                                <asp:DropDownList ID="dlcondicion" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                &nbsp;Medio Pago:
                            </td>
                            <td>
                                <asp:DropDownList ID="dlmedio" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                &nbsp; % Descuento:
                            </td>
                            <td>
                                <asp:TextBox ID="tbdescuento" runat="server" Width="100px" onKeyPress="return(    validarNumeroPuntoPositivo('event')       );"
                                    onFocus="return(   aceptarFoco(this)   );" onblur="return(   valBlur(event)     );"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                &nbsp; Estado:
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rbestado" runat="server" CssClass="Label" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" Value="1">Activo</asp:ListItem>
                                    <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Precio Base Descuento
                            </td>
                            <td class="Texto">
                                <asp:RadioButtonList ID="rbtPBD" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Precio Lista" Value="PL" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Precio Comercial" Value="PC"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td class="TituloCeldaLeft">
                &nbsp;busqueda
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlbusqueda" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            &nbsp;Perfil:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboPerfil" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            &nbsp;Tipo Precio:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboTipoPrecio" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto">
                                            &nbsp; Condicion de Pago:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboCondPago" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            &nbsp;Medio Pago:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboMedPago" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:RadioButtonList ID="rbver" runat="server" CssClass="Label" RepeatDirection="Horizontal">
                                                <asp:ListItem Selected="True" Value="2">Todos</asp:ListItem>
                                                <asp:ListItem Value="1">Activo</asp:ListItem>
                                                <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td colspan="2">
                                            <asp:RadioButtonList ID="rbtlPrecioBase" runat="server" CssClass="Label" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="Precio Lista" Value="PL" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="Precio Comercial" Value="PC"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnBuscar" runat="server" Text="Buscar" Width="90px" Style="cursor: hand;" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="gvBusqueda" runat="server" AutoGenerateColumns="False" Width="100%"
                                    AllowPaging="True" HeaderStyle-Height="25px" RowStyle-Height="25px" PageSize="10"
                                    GridLines="Horizontal">
                                    <RowStyle CssClass="GrillaRow" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Editar">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="linkEditar" runat="server" OnClick="lkEditarClick" Text="Editar"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdd_IdPerfil" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdPerfil") %>' />
                                                <asp:HiddenField ID="hdd_IdTipoPV" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdtipoPV") %>' />
                                                <asp:HiddenField ID="hdd_IdCondicionPago" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdCondicionPago") %>' />
                                                <asp:HiddenField ID="hdd_IdMedioPago" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdMedioPago") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="IdDescuento" HeaderText="Codigo" />
                                        <asp:BoundField DataField="Perfil" HeaderText="Perfil" ItemStyle-Font-Bold="true" />
                                        <asp:BoundField DataField="TipoPrecioV" HeaderText="TipoPrecio" ItemStyle-Font-Bold="true" />
                                        <asp:BoundField DataField="CondicionPago" HeaderText="Condicion Pago" />
                                        <asp:BoundField DataField="MedioPago" HeaderText="Medio Pago" ItemStyle-Font-Bold="true" />
                                        <asp:BoundField DataField="desc_Porcentaje" DataFormatString="{0:F2}" HeaderText="Dscto Porcentaje"
                                            ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red" />
                                        <asp:BoundField DataFormatString="{0:d}" HeaderText="Fecha Registro" DataField="desc_FechaRegistro" />
                                        <asp:BoundField DataField="desc_Estado" HeaderText="Estado" />
                                        <asp:BoundField DataField="Desc_PrecioBaseDscto" HeaderText="Precio Base Descuento" />
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Left" CssClass="GrillaPager" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">
        function validarGuardar() {
            var descuento = parseFloat(document.getElementById('<%=tbdescuento.CLientID %>').value);
            if (isNaN(descuento)) { descuento = 0; }

            if (descuento == 0) {
                alert('Ingrese un valor mayor a cero');
                return false;
            }

            return confirm('Desea, continuar ?');
        }
        //++++++++++++++++++++++++++
        function validarEliminar() {
            return confirm('Desea, eliminar el registro ?');
        }
    </script>

</asp:Content>
