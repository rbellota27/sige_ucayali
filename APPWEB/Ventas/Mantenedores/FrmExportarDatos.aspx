﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmExportarDatos.aspx.vb" Inherits="APPWEB.FrmExportarDatos" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
       <asp:GridView ID="GridViewExportar" runat="server" Visible="true" AutoGenerateColumns="False" Width="100%">
         <Columns>
            <asp:BoundField HeaderText="Cod." DataField="CodigoProducto" />
            <asp:BoundField HeaderText="Producto" DataField="Producto" />
            <asp:BoundField HeaderText="Tienda" DataField="Tienda"  />
            <asp:BoundField HeaderText="Perfil" DataField="Perfil" />
            <asp:BoundField HeaderText="Cond. Pago" DataField="CondicionPago" />
            <asp:BoundField HeaderText="Medio Pago" DataField="MedioPago"  />
            <asp:BoundField HeaderText="Tipo PV" DataField="TipoPV"  />
            <asp:BoundField HeaderText="Precio" DataField="ProductoPV"  />
            <asp:BoundField HeaderText="Porc. Dscto" DataField="Dscto"  />
            <asp:BoundField HeaderText="P. Dscto" DataField="PrecioDscto" />
            <asp:BoundField HeaderText="Cant. Min" DataField="CantMin"/>    
            <asp:BoundField HeaderText="Cant. Max" DataField="CantMax" />    
            <asp:BoundField HeaderText="F. Ini" DataField="FechaIni" />   
            <asp:BoundField HeaderText="F. Fin" DataField="FechaFin"  />    
  
        </Columns>
    
       </asp:GridView>
    </div>
    </form>
</body>
</html>
