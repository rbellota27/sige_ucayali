﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmLiquidacion
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Private listaCatalogo As List(Of Entidades.Catalogo)
    Private listaDetalleDocumento As List(Of Entidades.DetalleDocumento)  '********** SALIDA
    Private listaDocumentoRef As List(Of Entidades.DocumentoView)
    Private listaDetalleDocumento_Ingreso As List(Of Entidades.DetalleDocumento) '************* INGRESO
    Private listaCancelacion As List(Of Entidades.PagoCaja)
    Private listaCancelacion1 As List(Of Entidades.PagoCaja)
    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
    End Enum


    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            ValidarPermisos()
            inicializarFrm()
            ConfigurarDatos()
        End If
        visualizarMoneda()
    End Sub
    Private Sub visualizarMoneda()

        If (Me.cboMoneda.SelectedItem IsNot Nothing) Then

            Me.lblMonedaTotal_Ingreso.Text = Me.cboMoneda.SelectedItem.ToString
            Me.lblMonedaTotal_Salida.Text = Me.cboMoneda.SelectedItem.ToString
            Me.lblMoneda_Monto_DC.Text = Me.cboMoneda.SelectedItem.ToString
            Me.lblMoneda_SaldoRestante.Text = Me.cboMoneda.SelectedItem.ToString
            Me.lblMonedaFaltante.Text = Me.cboMoneda.SelectedItem.ToString

            Me.lblMonedaTotalRecibido.Text = Me.cboMoneda.SelectedItem.ToString
            Me.lblMonedaVuelto.Text = Me.cboMoneda.SelectedItem.ToString


         
            Me.lblMonedaFaltante1.Text = Me.cboMoneda.SelectedItem.ToString
            Me.lblMonedaTotalRecibido1.Text = Me.cboMoneda.SelectedItem.ToString
            Me.lblMonedaVuelto1.Text = Me.cboMoneda.SelectedItem.ToString

        End If



    End Sub


#Region "************************ MANEJO DE SESSION"
    Private Function getListaCatalogo() As List(Of Entidades.Catalogo)
        Return CType(Session.Item("listaCatalogo"), List(Of Entidades.Catalogo))
    End Function
    Private Sub setListaCatalogo(ByVal lista As List(Of Entidades.Catalogo))
        Session.Remove("listaCatalogo")
        Session.Add("listaCatalogo", lista)
    End Sub
    Private Function getListaDetalleDocumento() As List(Of Entidades.DetalleDocumento)
        Return CType(Session.Item("listaDetalleDocumento"), List(Of Entidades.DetalleDocumento))
    End Function
    Private Sub setListaDetalleDocumento(ByVal lista As List(Of Entidades.DetalleDocumento))
        Session.Remove("listaDetalleDocumento")
        Session.Add("listaDetalleDocumento", lista)
    End Sub
    Private Function getListaDetalleDocumento_Ingreso() As List(Of Entidades.DetalleDocumento)
        Return CType(Session.Item("listaDetalleDocumento_Ingreso"), List(Of Entidades.DetalleDocumento))
    End Function
    Private Sub setListaDetalleDocumento_Ingreso(ByVal lista As List(Of Entidades.DetalleDocumento))
        Session.Remove("listaDetalleDocumento_Ingreso")
        Session.Add("listaDetalleDocumento_Ingreso", lista)
    End Sub
    Private Function getListaCancelacion() As List(Of Entidades.PagoCaja)
        Return CType(Session.Item("listaCancelacion"), List(Of Entidades.PagoCaja))
    End Function
    Private Sub setListaCancelacion(ByVal lista As List(Of Entidades.PagoCaja))
        Session.Remove("listaCancelacion")
        Session.Add("listaCancelacion", lista)
    End Sub
    Private Function getListaDocumentoRef() As List(Of Entidades.DocumentoView)
        Return CType(Session.Item("listaDocumentoRef"), List(Of Entidades.DocumentoView))
    End Function
    Private Sub setListaDocumentoRef(ByVal lista As List(Of Entidades.DocumentoView))
        Session.Remove("listaDocumentoRef")
        Session.Add("listaDocumentoRef", lista)
    End Sub
    Private Function getListaCancelacion1() As List(Of Entidades.PagoCaja)
        Return CType(Session.Item("listaCancelacion1"), List(Of Entidades.PagoCaja))
    End Function
    Private Sub setListaCancelacion1(ByVal lista As List(Of Entidades.PagoCaja))
        Session.Remove("listaCancelacion1")
        Session.Add("listaCancelacion1", lista)
    End Sub
#End Region


    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION / SELECT PROD ING / PROD ING MOD UM / PROD ING MOD CANT / PROD ING MOD PRECIO UNIT / PROD EGRESO MOD UM / PROD EGRESO MOD CANT / PROD EGRESO MOD PRECIO UNIT / SELECT PROD EGRESO / DOC REF
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR 
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumentoxCodigo.Enabled = True
        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumentoxCodigo.Enabled = False
        End If

        If listaPermisos(4) > 0 Then  '********* FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

        If listaPermisos(5) > 0 Then  '********* SELECT PROD ING
            Me.btnBuscarProducto_Ingreso.Enabled = True
            Me.btnLimpiarDetalle_Ingreso.Enabled = True
        Else
            Me.btnBuscarProducto_Ingreso.Enabled = False
            Me.btnLimpiarDetalle_Ingreso.Enabled = False
        End If

        Me.hddModProdIngreso_UM.Value = CStr(listaPermisos(6))  '********* PROD ING MOD UM

        Me.hddModProdIngreso_Cant.Value = CStr(listaPermisos(7))  '********* PROD ING MOD CANT 

        Me.hddModProdIngreso_PrecioUnit.Value = CStr(listaPermisos(8))  '********* PROD ING MOD PRECIO UNIT

        Me.hddModProdEngreso_UM.Value = CStr(listaPermisos(9))  '********* PROD EGRESO MOD UM

        Me.hddModProdEngreso_Cant.Value = CStr(listaPermisos(10))  '********* PROD EGRESO MOD CANT 

        Me.hddModProdEngreso_PrecioUnit.Value = CStr(listaPermisos(11))  '********* PROD EGRESO MOD PRECIO UNIT

        If listaPermisos(12) > 0 Then  '********* SELECT PROD EGRESO
            Me.btnBuscarProducto_Salida.Enabled = True
            Me.btnLimpiarDetalle_Salida.Enabled = True
        Else
            Me.btnBuscarProducto_Salida.Enabled = False
            Me.btnLimpiarDetalle_Salida.Enabled = False
        End If

        If listaPermisos(13) > 0 Then  '********* SELECT DOC REF
            Me.btnBuscarDocumentoRef.Enabled = True
        Else
            Me.btnBuscarDocumentoRef.Enabled = False
        End If

        If (listaPermisos(16) > 0) Then  '***** SALIDA DE CAJA
            Me.rdbOpcionSaldoRestante.Items(2).Enabled = True
            Me.rdbOpcionSaldoRestante.SelectedValue = "2"
        Else
            Me.rdbOpcionSaldoRestante.Items(2).Enabled = False
            Me.rdbOpcionSaldoRestante.Items(2).Selected = False
        End If

        If (listaPermisos(15) > 0) Then  '***** NO SALDO A FAVOR DEL CLIENTE
            Me.rdbOpcionSaldoRestante.Items(1).Enabled = True
            Me.rdbOpcionSaldoRestante.SelectedValue = "1"
        Else
            Me.rdbOpcionSaldoRestante.Items(1).Enabled = False
            Me.rdbOpcionSaldoRestante.Items(1).Selected = False
        End If

        If (listaPermisos(14) > 0) Then  '***** SALDO A FAVOR DEL CLIENTE
            Me.rdbOpcionSaldoRestante.Items(0).Enabled = True
            Me.rdbOpcionSaldoRestante.SelectedValue = "0"
        Else
            Me.rdbOpcionSaldoRestante.Items(0).Enabled = False
            Me.rdbOpcionSaldoRestante.Items(0).Selected = False
        End If

        If (listaPermisos(17) > 0) Then
            Me.chb_MoverStockFisico_Ingreso.Enabled = True
        Else
            Me.chb_MoverStockFisico_Ingreso.Enabled = False
        End If

        If (listaPermisos(18) > 0) Then
            Me.chb_MoverStockFisico_Salida.Enabled = True
        Else
            Me.chb_MoverStockFisico_Salida.Enabled = False
        End If
    End Sub

    Private Sub inicializarFrm()

        Try

            Dim objCbo As New Combo

            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
                .LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)
                .llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)
                .LlenarCboMoneda(Me.cboMoneda, False)
                .LLenarCboEstadoDocumento(Me.cboEstado)

                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.hddIdTipoDocumento.Value), False)
                .LLenarCboTipoDocumentoRefxIdTipoDocumento(cboTipoDocumentoRef, CInt(Me.hddIdTipoDocumento.Value), 2)

                .LlenarCboCondicionPago(Me.cboCondicionPago)
                .LlenarCboMedioPagoxIdTipoDocumentoxIdCondicionPago(Me.cboMedioPago1, CInt(Me.hddIdTipoDocumento.Value), 1, False) '***** CONTADO
                .LlenarCboMedioPagoxIdTipoDocumentoxIdCondicionPago(Me.cboMedioPago_Credito, CInt(Me.hddIdTipoDocumento.Value), 2, False) '***** CREDITO
                .LlenarCboMoneda(Me.cboMoneda_DatoCancelacion1, True)
                '********************  .LlenarCboMedioPago(Me.cboMedioPago, False)
                .LlenarCboTipoPV(Me.cboTipoPV)

                '**************** BUSQUEDA DE PRODUCTOS
                '.llenarCboTipoExistencia(CboTipoExistencia, False)
                '.llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(CboTipoExistencia.SelectedValue), True)
                '.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
                '.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)
                .llenarCboTipoExistencia(Me.CboTipoExistencia, False)
                .llenarCboLineaxTipoExistencia(Me.cmbLinea_AddProd, CInt(CboTipoExistencia.SelectedValue), True)
                .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
                .LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

                '**************** DATOS DE CANCELACION
                .LlenarCboBanco(Me.cboBanco, False)
                .LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)
                .LlenarCboMoneda(Me.cboMoneda_DatoCancelacion, False)


            End With

            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")


            Me.txtFechaI.Text = Me.txtFechaEmision.Text
            Me.txtFechaF.Text = Me.txtFechaEmision.Text
            Me.txtFechaInicio_DocRef.Text = Me.txtFechaEmision.Text
            Me.txtFechaFin_DocRef.Text = Me.txtFechaEmision.Text

            Me.hddIdTipoPVDefault.Value = CStr((New Negocio.TipoPrecioV).SelectIdTipoPrecioVDefault())
            '************** MEDIO DE PAGO PRINCIPAL
            Me.hddIdMedioPagoPrincipal.Value = CStr((New Negocio.MedioPago).SelectIdMedioPagoPrincipal)


            actualizarOpcionesBusquedaDocRef(1, False)

            '********************** CONTADO
            actualizarControlesMedioPago(1, CInt(Me.cboMedioPago.SelectedValue))

            actualizarControlesMedioPago1(1, 13, 0) ' CONTADO EFECTIVO

            verFrm(FrmModo.Nuevo, True, True, True, True, True, True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub GenerarCodigoDocumento()

        If (IsNumeric(Me.cboSerie.SelectedValue)) Then
            Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
        Else
            Me.txtCodigoDocumento.Text = ""
        End If

    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
            objCbo.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)

            objCbo.llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
            objCbo.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        Try
            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub verFrm(ByVal FrmModo As Integer, ByVal limpiar As Boolean, ByVal removeSession As Boolean, ByVal initSession As Boolean, ByVal initParametrosGral As Boolean, ByVal viewMoneda As Boolean, ByVal generarCodigo As Boolean)

        If (limpiar) Then
            LimpiarFrm()
        End If

        If (removeSession) Then
            Session.Remove("listaDetalleDocumento")
            Session.Remove("listaDetalleDocumento_Ingreso")
            Session.Remove("listaCatalogo")
            Session.Remove("listaCancelacion")
            Session.Remove("listaCancelacion1")
            Session.Remove("listaDocumentoRef")
        End If

        If (initSession) Then
            Me.listaCatalogo = New List(Of Entidades.Catalogo)
            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)
            Me.listaDetalleDocumento_Ingreso = New List(Of Entidades.DetalleDocumento)
            Me.listaCancelacion = New List(Of Entidades.PagoCaja)
            Me.listaCancelacion1 = New List(Of Entidades.PagoCaja)
            Me.listaDocumentoRef = New List(Of Entidades.DocumentoView)
            setListaCatalogo(Me.listaCatalogo)
            setListaDetalleDocumento(Me.listaDetalleDocumento)
            setListaDetalleDocumento_Ingreso(Me.listaDetalleDocumento_Ingreso)
            setListaCancelacion(Me.listaCancelacion)
            setListaCancelacion1(Me.listaCancelacion1)
            setListaDocumentoRef(Me.listaDocumentoRef)
        End If

        If (initParametrosGral) Then

            '******* FECHA DE VCTO
            'Dim listaParametros() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {1, 2})
            'Me.txtFechaVcto.Text = Format(DateAdd(DateInterval.Day, listaParametros(0), CDate(Me.txtFechaEmision.Text)), "dd/MM/yyyy")

        End If

        If (viewMoneda) Then
            visualizarMoneda()
        End If

        If (generarCodigo) Then
            GenerarCodigoDocumento()
        End If

        Me.hddFrmModo.Value = CStr(FrmModo)
        ActualizarBotonesControl()

    End Sub
    Private Sub LimpiarFrm()

        ''************* CABECERA
        Me.txtCodigoDocumento.Text = ""
        Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.cboEstado.SelectedValue = "1"  '******* Activo por defecto


        '*************  DOCUMENTO REF
        Me.GV_DocumentoRef.DataSource = Nothing
        Me.GV_DocumentoRef.DataBind()

        '******************* PERSONA
        Me.txtDescripcion_Cliente.Text = ""
        Me.txtDni_Cliente.Text = ""
        Me.txtRuc_Cliente.Text = ""
        Me.txtIdCliente.Text = ""
        Me.cboTipoPV.SelectedIndex = 0

        '*********** DETALLES INGRESO
        Me.GV_Detalle_Ingreso.DataSource = Nothing
        Me.GV_Detalle_Ingreso.DataBind()
        Me.txtTotal_Ingreso.Text = "0"

        '*********** DETALLES SALIDA
        Me.GV_Detalle_Salida.DataSource = Nothing
        Me.GV_Detalle_Salida.DataBind()
        Me.txtTotal_Salida.Text = "0"

        '*************** DATOS DE CANCELACION
        Me.GV_Cancelacion.DataSource = Nothing
        Me.GV_Cancelacion.DataBind()

        Me.GV_Cancelacion1.DataSource = Nothing
        Me.GV_Cancelacion1.DataBind()

        Me.txtTotalRecibido.Text = "0"
        Me.txtFaltante.Text = "0"
        Me.txtVuelto.Text = "0"

        Me.txtTotalRecibido1.Text = "0"
        Me.txtFaltante1.Text = "0"
        Me.txtVuelto1.Text = "0"

        '*********** OBSERVACION
        Me.txtObservaciones.Text = ""

        '************* HIDDEN
        Me.hddCodigoDocumento.Value = ""
        Me.hddIdDocumento.Value = ""
        Me.hddIdPersona.Value = ""
        Me.hddIdDocumentoRef.Value = ""



        ''************* OTROS
        Me.DGV_AddProd.DataSource = Nothing
        Me.DGV_AddProd.DataBind()
        'Me.GV_BusquedaAvanzado.DataSource = Nothing
        'Me.GV_BusquedaAvanzado.DataBind()
        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()

        Me.txtSaldoRestante.Text = "0"
        Me.txtMonto_DC.Text = "0"

        '************* Me.rdbOpcionSaldoRestante.SelectedValue = "0"  '**** OPCION POR DEFECTO
        actualizarControlesMedioPago(1, 1)

    End Sub

    Private Sub ActualizarBotonesControl()

        Me.cboMoneda.Enabled = True

        Select Case CInt(hddFrmModo.Value)

            Case FrmModo.Inicio '************* INICIO
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_Doc_Ref.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle_Ingreso.Enabled = False
                Me.Panel_Detalle_Salida.Enabled = False
                Me.Panel_Obs.Enabled = False
                Me.Panel_DatosCancelacion.Enabled = False
                Me.Panel_Saldo_Cliente.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                Me.cboMoneda.Enabled = True
                Me.cboEstado.SelectedValue = "1"

            Case FrmModo.Nuevo '************** Nuevo
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnBuscarDocumentoRef.Visible = True

                Me.Panel_Cab.Enabled = True
                Me.Panel_Doc_Ref.Enabled = True
                Me.Panel_Cliente.Enabled = True
                Me.Panel_Detalle_Ingreso.Enabled = True
                Me.Panel_Detalle_Salida.Enabled = True
                Me.Panel_Obs.Enabled = True
                Me.Panel_DatosCancelacion.Enabled = True
                Me.Panel_Saldo_Cliente.Enabled = True







                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboMoneda.Enabled = True
                Me.cboEstado.SelectedValue = "1"

            Case FrmModo.Editar '*********************** Editar
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnBuscarDocumentoRef.Visible = True

                Me.Panel_Cab.Enabled = True
                Me.Panel_Doc_Ref.Enabled = True
                Me.Panel_Cliente.Enabled = True
                Me.Panel_Detalle_Ingreso.Enabled = True
                Me.Panel_Detalle_Salida.Enabled = True
                Me.Panel_Obs.Enabled = True
                Me.Panel_DatosCancelacion.Enabled = True
                Me.Panel_Saldo_Cliente.Enabled = True

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False

                If (Me.GV_DocumentoRef.Rows.Count > 0) Then
                    Me.cboMoneda.Enabled = False
                End If


            Case FrmModo.Buscar_Doc '**************************** Buscar documento
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = False
                Me.txtCodigoDocumento.Focus()
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_Doc_Ref.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle_Ingreso.Enabled = False
                Me.Panel_Detalle_Salida.Enabled = False
                Me.Panel_Obs.Enabled = False
                Me.Panel_DatosCancelacion.Enabled = False
                Me.Panel_Saldo_Cliente.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboMoneda.Enabled = True



            Case FrmModo.Documento_Buscar_Exito '********* documento hallado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True
                Me.btnImprimir.Visible = True
                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = False
                Me.Panel_Doc_Ref.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle_Ingreso.Enabled = False
                Me.Panel_Detalle_Salida.Enabled = False
                Me.Panel_Obs.Enabled = False
                Me.Panel_DatosCancelacion.Enabled = False
                Me.Panel_Saldo_Cliente.Enabled = False


                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


                Me.cboMoneda.Enabled = False


            Case FrmModo.Documento_Save_Exito '********* documento guardado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnBuscarDocumentoRef.Visible = False
                Me.btnImprimir.Visible = True


                Me.Panel_Cab.Enabled = False
                Me.Panel_Doc_Ref.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle_Ingreso.Enabled = False
                Me.Panel_Detalle_Salida.Enabled = False
                Me.Panel_Obs.Enabled = False
                Me.Panel_DatosCancelacion.Enabled = False
                Me.Panel_Saldo_Cliente.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


                Me.cboMoneda.Enabled = False



            Case FrmModo.Documento_Anular_Exito  '*********** Anulacion exitosa

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = True
                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = False
                Me.Panel_Doc_Ref.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle_Ingreso.Enabled = False
                Me.Panel_Detalle_Salida.Enabled = False
                Me.Panel_Obs.Enabled = False
                Me.Panel_DatosCancelacion.Enabled = False
                Me.Panel_Saldo_Cliente.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboMoneda.Enabled = False

                Me.cboEstado.SelectedValue = "2"

        End Select
    End Sub


#Region "**************** BUSCAR PERSONA "

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", 1)  '******************** CLIENTE
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            '************ Limpiamos la grilla de productos
            Me.DGV_AddProd.DataSource = Nothing
            Me.DGV_AddProd.DataBind()

            Me.GV_Detalle_Ingreso.DataSource = Nothing
            Me.GV_Detalle_Ingreso.DataBind()

            Me.GV_Detalle_Salida.DataSource = Nothing
            Me.GV_Detalle_Salida.DataBind()

            Me.GV_DocumentoRef.DataSource = Nothing
            Me.GV_DocumentoRef.DataBind()

            Me.GV_Cancelacion.DataSource = Nothing
            Me.GV_Cancelacion.DataBind()

            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)
            Me.listaDetalleDocumento_Ingreso = New List(Of Entidades.DetalleDocumento)
            Me.listaDocumentoRef = New List(Of Entidades.DocumentoView)
            Me.listaCancelacion = New List(Of Entidades.PagoCaja)

            setListaDetalleDocumento(Me.listaDetalleDocumento)
            setListaDetalleDocumento_Ingreso(Me.listaDetalleDocumento_Ingreso)
            setListaDocumentoRef(Me.listaDocumentoRef)
            setListaCancelacion(Me.listaCancelacion)

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     calcularTotales_GV_Detalle('I','0');    calcularTotales_GV_Detalle('S','0');     ", True)


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)


        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)

        If (objPersona Is Nothing) Then
            Throw New Exception("EL CLIENTE SELECCIONADO NO SE ENCUENTRA EN LA BASE DE DATOS. NO SE PERMITE LA OPERACIÓN.")
        End If

        Me.txtDescripcion_Cliente.Text = objPersona.Descripcion
        Me.txtDni_Cliente.Text = objPersona.Dni
        Me.txtRuc_Cliente.Text = objPersona.Ruc
        Me.txtIdCliente.Text = CStr(objPersona.IdPersona)

        If (Me.cboTipoPV.Items.FindByValue(CStr(objPersona.IdTipoPV)) IsNot Nothing) Then
            Me.cboTipoPV.SelectedValue = CStr(objPersona.IdTipoPV)
        End If

        '*************** HIDDEN
        Me.hddIdPersona.Value = CStr(objPersona.IdPersona)

        'Me.GV_BusquedaAvanzado.DataSource = Nothing
        'Me.GV_BusquedaAvanzado.DataBind()
        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()

        Me.GV_Cancelacion.DataSource = Nothing
        Me.GV_Cancelacion.DataBind()

        Me.listaCancelacion = New List(Of Entidades.PagoCaja)
        setListaCancelacion(Me.listaCancelacion)

        objScript.offCapa(Me, "capaPersona")

    End Sub

#End Region



    Private Sub ActualizarListaDetalleDocumento_Salida()
        Me.listaDetalleDocumento = getListaDetalleDocumento()
        For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1

            Dim TCantidad As TextBox = CType(Me.GV_Detalle_Salida.Rows(i).FindControl("txtCantidad"), TextBox)
            Me.listaDetalleDocumento.Item(i).Cantidad = CDec(TCantidad.Text)

            Dim cboUM As DropDownList = CType(Me.GV_Detalle_Salida.Rows(i).FindControl("cboUnidadMedida"), DropDownList)
            Me.listaDetalleDocumento(i).UMedida = CStr(IIf(cboUM.SelectedItem.Text = "", Nothing, cboUM.SelectedItem.Text))
            Me.listaDetalleDocumento(i).IdUnidadMedida = CInt(IIf(cboUM.SelectedValue = "", Nothing, cboUM.SelectedValue))

            Me.listaDetalleDocumento.Item(i).PrecioSD = CDec(Me.GV_Detalle_Salida.Rows(i).Cells(5).Text)  '******** Precio SD

            Dim TPrecioCD As TextBox = CType(Me.GV_Detalle_Salida.Rows(i).FindControl("txtPrecioUnit"), TextBox)
            Me.listaDetalleDocumento(i).PrecioCD = CDec(TPrecioCD.Text)

            Dim TImporte As TextBox = CType(Me.GV_Detalle_Salida.Rows(i).FindControl("txtImporte"), TextBox)
            Me.listaDetalleDocumento.Item(i).Importe = CDec(TImporte.Text)

            Me.listaDetalleDocumento(i).Utilidad = -1  '********** SALIDA

        Next

        setListaDetalleDocumento(Me.listaDetalleDocumento)

    End Sub
    Private Sub ActualizarlistaDetalleDocumento_Ingreso()
        Me.listaDetalleDocumento_Ingreso = getListaDetalleDocumento_Ingreso()
        For i As Integer = 0 To Me.listaDetalleDocumento_Ingreso.Count - 1

            Dim TCantidad As TextBox = CType(Me.GV_Detalle_Ingreso.Rows(i).FindControl("txtCantidad"), TextBox)
            Me.listaDetalleDocumento_Ingreso.Item(i).Cantidad = CDec(TCantidad.Text)

            Dim cboUM As DropDownList = CType(Me.GV_Detalle_Ingreso.Rows(i).FindControl("cboUnidadMedida"), DropDownList)
            Me.listaDetalleDocumento_Ingreso(i).UMedida = CStr(IIf(cboUM.SelectedItem.Text = "", Nothing, cboUM.SelectedItem.Text))
            Me.listaDetalleDocumento_Ingreso(i).IdUnidadMedida = CInt(IIf(cboUM.SelectedValue = "", Nothing, cboUM.SelectedValue))

            Me.listaDetalleDocumento_Ingreso.Item(i).PrecioSD = CDec(Me.GV_Detalle_Ingreso.Rows(i).Cells(5).Text)  '******** Precio SD

            Dim TPrecioCD As TextBox = CType(Me.GV_Detalle_Ingreso.Rows(i).FindControl("txtPrecioUnit"), TextBox)
            Me.listaDetalleDocumento_Ingreso(i).PrecioCD = CDec(TPrecioCD.Text)

            Dim TImporte As TextBox = CType(Me.GV_Detalle_Ingreso.Rows(i).FindControl("txtImporte"), TextBox)
            Me.listaDetalleDocumento_Ingreso.Item(i).Importe = CDec(TImporte.Text)

            Me.listaDetalleDocumento_Ingreso(i).Utilidad = 1  '********** SALIDA

        Next

        setListaDetalleDocumento_Ingreso(Me.listaDetalleDocumento_Ingreso)

    End Sub
    Private Function obtenerListaPUM(ByVal listaUnidadMedida As List(Of Entidades.UnidadMedida)) As List(Of Entidades.ProductoUMView)

        Dim lista As New List(Of Entidades.ProductoUMView)

        For i As Integer = 0 To listaUnidadMedida.Count - 1

            Dim obj As New Entidades.ProductoUMView
            With obj

                .IdUnidadMedida = listaUnidadMedida(i).Id
                .NombreCortoUM = listaUnidadMedida(i).DescripcionCorto

            End With

            lista.Add(obj)

        Next

        Return lista

    End Function
    Protected Sub cboUnidadMedida_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)

        Dim listaOld As List(Of Entidades.DetalleDocumento) = getListaDetalleDocumento()
        Dim lista As New List(Of Entidades.DetalleDocumento)

        Dim cbo As DropDownList = CType(sender, DropDownList)

        For i As Integer = 0 To listaOld.Count - 1
            Dim cboUM As DropDownList = CType(Me.GV_Detalle_Salida.Rows(i).Cells(3).FindControl("cboUnidadMedida"), DropDownList)
            If cbo.ClientID = cboUM.ClientID Then

                Dim IdUM_Old As Integer = listaOld(i).IdUnidadMedida
                Dim cant_Old As Decimal = 0
                Dim IdUM_New As Integer = 0

                ActualizarListaDetalleDocumento_Salida()

                lista = getListaDetalleDocumento()
                IdUM_New = lista(i).IdUnidadMedida
                cant_Old = lista(i).Cantidad



                Dim IdTipoPV As Integer = 1 '****** PUBLICO

                If IsNumeric(Me.cboTipoPV.SelectedValue) And Me.cboTipoPV.SelectedValue.Trim.Length > 0 Then

                    If CInt(Me.cboTipoPV.SelectedValue) > 0 Then

                        IdTipoPV = CInt(Me.cboTipoPV.SelectedValue)

                    End If

                End If


                Dim addPorcentRetazo As Boolean = False

                Dim objCatalogo As Entidades.Catalogo = (New Negocio.ProductoTipoPV).SelectValor_Equivalencia_VentaxParams(CInt(Me.cboAlmacen.SelectedValue), CInt(Me.cboMoneda.SelectedValue), _
                    IdUM_New, lista(i).IdProducto, CInt(Me.cboTienda.SelectedValue), IdTipoPV, addPorcentRetazo)

                '************ lista(i).Cantidad = CDec(0)
                lista(i).UMedida = cboUM.SelectedItem.Text
                '*********** lista(i).IdUMedida = CInt(cboUM.SelectedValue)
                lista.Item(i).PrecioSD = objCatalogo.PrecioSD
                lista.Item(i).PrecioCD = lista.Item(i).PrecioSD

                ''******************** actualizo la nueva cantidad
                'If lista(i).Glosa IsNot Nothing Then

                '    If lista(i).Glosa.Trim.Length > 0 Then

                '        lista(i).Cantidad = (New Negocio.Producto).SelectEquivalenciaEntreUM(lista(i).IdProducto, IdUM_Old, IdUM_New, cant_Old)

                '    End If

                'End If

                Exit For
            End If
        Next
        setListaDetalleDocumento(lista)

        Me.GV_Detalle_Salida.DataSource = lista
        Me.GV_Detalle_Salida.DataBind()

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     calcularTotales_GV_Detalle('S','0');         ", True)

    End Sub

    Protected Sub btnLimpiarDetalle_Salida_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarDetalle_Salida.Click
        Try
            Me.GV_Detalle_Salida.DataSource = Nothing
            Me.GV_Detalle_Salida.DataBind()

            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)
            setListaDetalleDocumento(Me.listaDetalleDocumento)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     calcularTotales_GV_Detalle('S','0');         ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub GV_Detalle_Salida_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Detalle_Salida.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim cboUMedida1 As DropDownList
            Dim gvrow As GridViewRow = CType(e.Row.Cells(3).NamingContainer, GridViewRow)
            cboUMedida1 = CType(gvrow.FindControl("cboUnidadMedida"), DropDownList)

            cboUMedida1.DataSource = Me.listaDetalleDocumento(e.Row.RowIndex).ListaUM
            cboUMedida1.DataBind()

            If Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida <> 0 Then
                cboUMedida1.SelectedValue = CStr(Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida)
            End If

            '*************** VALIDAMOS PERMISOS
            If (CInt(Me.hddModProdEngreso_UM.Value) > 0) Then '**** UM
                cboUMedida1.Enabled = True
            Else
                cboUMedida1.Enabled = False
            End If

            If (CInt(Me.hddModProdEngreso_Cant.Value) > 0) Then '**** CANT
                CType(gvrow.FindControl("txtCantidad"), TextBox).Enabled = True
            Else
                CType(gvrow.FindControl("txtCantidad"), TextBox).Enabled = False
            End If

            If (CInt(Me.hddModProdEngreso_PrecioUnit.Value) > 0) Then '**** PRECIO UNIT
                CType(gvrow.FindControl("txtPrecioUnit"), TextBox).Enabled = True
            Else
                CType(gvrow.FindControl("txtPrecioUnit"), TextBox).Enabled = False
            End If
        End If
    End Sub

    Private Sub GV_Detalle_Salida_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Detalle_Salida.SelectedIndexChanged
        quitarDetalleDocumento_Salida(Me.GV_Detalle_Salida.SelectedIndex)
    End Sub
    Private Sub quitarDetalleDocumento_Salida(ByVal index As Integer)

        Try

            ActualizarListaDetalleDocumento_Salida()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Me.listaDetalleDocumento.RemoveAt(index)

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle_Salida.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle_Salida.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     calcularTotales_GV_Detalle('S','0');         ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Protected Sub cboUnidadMedida_Ingreso_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)

        Dim listaOld As List(Of Entidades.DetalleDocumento) = getListaDetalleDocumento_Ingreso()
        Dim lista As New List(Of Entidades.DetalleDocumento)

        Dim cbo As DropDownList = CType(sender, DropDownList)

        For i As Integer = 0 To listaOld.Count - 1
            Dim cboUM As DropDownList = CType(Me.GV_Detalle_Ingreso.Rows(i).Cells(3).FindControl("cboUnidadMedida"), DropDownList)
            If cbo.ClientID = cboUM.ClientID Then

                Dim IdUM_Old As Integer = listaOld(i).IdUnidadMedida
                Dim cant_Old As Decimal = 0
                Dim IdUM_New As Integer = 0

                ActualizarlistaDetalleDocumento_Ingreso()

                lista = getListaDetalleDocumento_Ingreso()
                IdUM_New = lista(i).IdUnidadMedida
                cant_Old = lista(i).Cantidad



                Dim IdTipoPV As Integer = 1 '****** PUBLICO

                If IsNumeric(Me.cboTipoPV.SelectedValue) And Me.cboTipoPV.SelectedValue.Trim.Length > 0 Then

                    If CInt(Me.cboTipoPV.SelectedValue) > 0 Then

                        IdTipoPV = CInt(Me.cboTipoPV.SelectedValue)

                    End If

                End If


                Dim addPorcentRetazo As Boolean = False

                Dim objCatalogo As Entidades.Catalogo = (New Negocio.ProductoTipoPV).SelectValor_Equivalencia_VentaxParams(CInt(Me.cboAlmacen.SelectedValue), CInt(Me.cboMoneda.SelectedValue), _
                    IdUM_New, lista(i).IdProducto, CInt(Me.cboTienda.SelectedValue), IdTipoPV, addPorcentRetazo)

                '************ lista(i).Cantidad = CDec(0)
                lista(i).UMedida = cboUM.SelectedItem.Text
                '*********** lista(i).IdUMedida = CInt(cboUM.SelectedValue)
                lista.Item(i).PrecioSD = objCatalogo.PrecioSD
                lista.Item(i).PrecioCD = lista.Item(i).PrecioSD

                ''******************** actualizo la nueva cantidad
                'If lista(i).Glosa IsNot Nothing Then

                '    If lista(i).Glosa.Trim.Length > 0 Then

                '        lista(i).Cantidad = (New Negocio.Producto).SelectEquivalenciaEntreUM(lista(i).IdProducto, IdUM_Old, IdUM_New, cant_Old)

                '    End If

                'End If

                Exit For
            End If
        Next
        setListaDetalleDocumento_Ingreso(lista)

        Me.GV_Detalle_Ingreso.DataSource = lista
        Me.GV_Detalle_Ingreso.DataBind()

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     calcularTotales_GV_Detalle('I','0');         ", True)

    End Sub


    Private Sub GV_Detalle_Ingreso_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Detalle_Ingreso.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim cboUMedida1 As DropDownList
            Dim gvrow As GridViewRow = CType(e.Row.Cells(3).NamingContainer, GridViewRow)
            cboUMedida1 = CType(gvrow.FindControl("cboUnidadMedida"), DropDownList)

            cboUMedida1.DataSource = Me.listaDetalleDocumento_Ingreso(e.Row.RowIndex).ListaUM
            cboUMedida1.DataBind()

            If Me.listaDetalleDocumento_Ingreso(e.Row.RowIndex).IdUnidadMedida <> 0 Then
                cboUMedida1.SelectedValue = CStr(Me.listaDetalleDocumento_Ingreso(e.Row.RowIndex).IdUnidadMedida)
            End If

            '*************** VALIDAMOS PERMISOS
            If (CInt(Me.hddModProdIngreso_UM.Value) > 0) Then '**** UM
                cboUMedida1.Enabled = True
            Else
                cboUMedida1.Enabled = False
            End If

            If (CInt(Me.hddModProdIngreso_Cant.Value) > 0) Then '**** CANT
                CType(gvrow.FindControl("txtCantidad"), TextBox).Enabled = True
            Else
                CType(gvrow.FindControl("txtCantidad"), TextBox).Enabled = False
            End If

            If (CInt(Me.hddModProdIngreso_PrecioUnit.Value) > 0) Then '**** PRECIO UNIT
                CType(gvrow.FindControl("txtPrecioUnit"), TextBox).Enabled = True
            Else
                CType(gvrow.FindControl("txtPrecioUnit"), TextBox).Enabled = False
            End If


        End If
    End Sub
    Private Sub GV_Detalle_Ingreso_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Detalle_Ingreso.SelectedIndexChanged
        quitarDetalleDocumento_Ingreso(Me.GV_Detalle_Ingreso.SelectedIndex)
    End Sub
    Private Sub quitarDetalleDocumento_Ingreso(ByVal index As Integer)

        Try

            ActualizarlistaDetalleDocumento_Ingreso()
            Me.listaDetalleDocumento_Ingreso = getListaDetalleDocumento_Ingreso()

            Me.listaDetalleDocumento_Ingreso.RemoveAt(index)

            setListaDetalleDocumento_Ingreso(Me.listaDetalleDocumento_Ingreso)

            Me.GV_Detalle_Ingreso.DataSource = Me.listaDetalleDocumento_Ingreso
            Me.GV_Detalle_Ingreso.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     calcularTotales_GV_Detalle('I','0');         ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub


    Private Sub btnLimpiarDetalle_Ingreso_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarDetalle_Ingreso.Click
        Try

            Me.GV_Detalle_Ingreso.DataSource = Nothing
            Me.GV_Detalle_Ingreso.DataBind()

            Me.listaDetalleDocumento_Ingreso = New List(Of Entidades.DetalleDocumento)
            setListaDetalleDocumento_Ingreso(Me.listaDetalleDocumento_Ingreso)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     calcularTotales_GV_Detalle('I','0');         ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click

        mostrarDocumentosRef_Find()

    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As Integer = 0
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdPersona As Integer = 0

            If (Me.hddIdPersona.Value.Trim.Length > 0 And IsNumeric(Me.hddIdPersona.Value)) Then
                If (CInt(Me.hddIdPersona.Value) > 0) Then
                    IdPersona = CInt(Me.hddIdPersona.Value)
                End If
            End If

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CInt(Me.txtCodigo_BuscarDocRef.Text)
            End Select

            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.DocumentoLiquidacion).DocumentoLiquidacion_BuscarDocRef(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), IdPersona, serie, codigo, CInt(cboTipoDocumentoRef.SelectedValue), fechaInicio, fechafin)

            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Throw New Exception("NO SE HALLARON REGISTROS.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub

    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia")
        End If

    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged

        cargarDocumentoReferencia(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))

    End Sub

    Private Sub cargarDocumentoReferencia(ByVal IdDocumentoRef As Integer)

        Try

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    '************* INICIALIZAMOS EL FRM
                    verFrm(FrmModo.Nuevo, True, True, True, True, True, True)
                Case FrmModo.Editar

                    Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)
                    setListaDetalleDocumento(Me.listaDetalleDocumento)

                    Me.GV_Detalle_Salida.DataSource = Nothing
                    Me.GV_Detalle_Salida.DataBind()

            End Select

            '***************** OBTENEMOS EL DOCUMENTO DE REFERENCIA
            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumentoRef)
            Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdPersona)

            Me.listaDocumentoRef = getListaDocumentoRef()
            Me.listaDetalleDocumento_Ingreso = getListaDetalleDocumento_Ingreso()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Me.listaDocumentoRef.Add(obtenerDocumentoView_LoadDocRef(objDocumento))


            Select Case CInt(Me.cboTipoOperacion.SelectedValue)

                Case 1 ' **************     VENTA
                    Me.listaDetalleDocumento.AddRange(obtenerListaDetalleDocumento_LoadDocRefVenta(objDocumento.Id))

                Case 2 ' **************     COMPRA
                    Me.listaDetalleDocumento_Ingreso.AddRange(obtenerListaDetalleDocumento_LoadDocRef(objDocumento.Id))

            End Select




            '************** CARGAMOS EN INTERFACE
            cargarDocumentoRef_GUI(objDocumento, objPersona, Me.listaDetalleDocumento_Ingreso, Me.listaDetalleDocumento, Nothing, Me.listaDocumentoRef, False)

            setListaDetalleDocumento_Ingreso(Me.listaDetalleDocumento_Ingreso)
            setListaDocumentoRef(Me.listaDocumentoRef)
            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.cboMoneda.Enabled = False
            visualizarMoneda()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     calcularTotales_GV_Detalle('I','0');     calcularTotales_GV_Detalle('S','0');      ", True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub
    Private Function obtenerDocumentoView_LoadDocRef(ByVal objDocumento As Entidades.Documento) As Entidades.DocumentoView

        Dim objDocumentoView As New Entidades.DocumentoView
        With objDocumentoView

            .Id = objDocumento.Id
            .NomTipoDocumento = objDocumento.NomTipoDocumento
            .Numero = objDocumento.NroDocumento
            .IdEmpresa = objDocumento.IdEmpresa
            .IdAlmacen = objDocumento.IdAlmacen
            .IdPersona = objDocumento.IdPersona
            .Empresa = objDocumento.Empresa
            .Tienda = objDocumento.Tienda
            .NomAlmacen = objDocumento.NomAlmacen
            .NomEstadoDocumento = objDocumento.NomEstadoDocumento
            .IdMoneda = objDocumento.IdMoneda
            .Total = objDocumento.Total

        End With
        Return objDocumentoView
    End Function
    Private Function obtenerListaDetalleDocumento_LoadDocRef(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)

        Dim lista As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumento(IdDocumento)
        Dim lista_Retorno As New List(Of Entidades.DetalleDocumento)

        For i As Integer = 0 To lista.Count - 1

            Dim obj As New Entidades.DetalleDocumento

            With obj

                .ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(lista(i).IdProducto)
                .IdDocumento = IdDocumento
                .CodigoProducto = lista(i).CodigoProducto
                .NomProducto = lista(i).NomProducto
                .IdUnidadMedida = lista(i).IdUnidadMedida
                .PrecioSD = lista(i).PrecioCD
                .PrecioCD = lista(i).PrecioCD
                .UMedida = lista(i).UMedida
                .IdProducto = lista(i).IdProducto
                .Utilidad = 1  '**** INGRESO
                .IdDetalleAfecto = lista(i).IdDetalleDocumento
                .Cantidad = lista(i).Cantidad
                .Importe = .Cantidad * .PrecioCD

                '**** ALMACENAMOS LOS VALORES PREVIOS DEL DOCUMENTO REF PARA SU VALIDACION
                .CantADespachar = lista(i).Cantidad
                .IdUnidadMedidaPrincipal = lista(i).IdUnidadMedida
                .CantADespacharOriginal = 1  '***** INDICA QUE SE DEBE VALIDAR

            End With

            lista_Retorno.Add(obj)

        Next

        Return lista_Retorno

    End Function

    Private Function obtenerListaDetalleDocumento_LoadDocRefVenta(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)

        Dim lista As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumento(IdDocumento)
        Dim lista_Retorno As New List(Of Entidades.DetalleDocumento)

        For i As Integer = 0 To lista.Count - 1

            Dim obj As New Entidades.DetalleDocumento

            With obj

                .ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(lista(i).IdProducto)
                .IdDocumento = IdDocumento
                .CodigoProducto = lista(i).CodigoProducto
                .NomProducto = lista(i).NomProducto
                .IdUnidadMedida = lista(i).IdUnidadMedida
                .PrecioSD = lista(i).PrecioCD
                .PrecioCD = lista(i).PrecioCD
                .UMedida = lista(i).UMedida
                .IdProducto = lista(i).IdProducto
                .Utilidad = -1  ' ****** SALIDA
                .IdDetalleAfecto = lista(i).IdDetalleDocumento
                .Cantidad = lista(i).Cantidad
                .Importe = .Cantidad * .PrecioCD

                '**** ALMACENAMOS LOS VALORES PREVIOS DEL DOCUMENTO REF PARA SU VALIDACION
                .CantADespachar = lista(i).Cantidad
                .IdUnidadMedidaPrincipal = lista(i).IdUnidadMedida
                .CantADespacharOriginal = 1  '***** INDICA QUE SE DEBE VALIDAR

            End With

            lista_Retorno.Add(obj)

        Next

        Return lista_Retorno

    End Function

    Private Sub cargarDocumentoRef_GUI(ByVal objDocumento As Entidades.Documento, ByVal objPersona As Entidades.PersonaView, ByVal listaDetalle_Ingreso As List(Of Entidades.DetalleDocumento), ByVal listaDetalle_Salida As List(Of Entidades.DetalleDocumento), ByVal objObservaciones As Entidades.Observacion, ByVal listaDocumentoRef As List(Of Entidades.DocumentoView), ByVal cargarLiquidacion As Boolean)

        Dim objCbo As New Combo

        With objDocumento

            If (Me.cboEmpresa.Items.FindByValue(CStr(objDocumento.IdEmpresa)) IsNot Nothing) Then
                Me.cboEmpresa.SelectedValue = CStr(objDocumento.IdEmpresa)
            End If

            If (Me.cboAlmacen.Items.FindByValue(CStr(objDocumento.IdAlmacen)) IsNot Nothing) Then
                Me.cboAlmacen.SelectedValue = CStr(objDocumento.IdAlmacen)
            End If

            If (Me.cboMoneda.Items.FindByValue(CStr(objDocumento.IdMoneda)) IsNot Nothing) Then
                Me.cboMoneda.SelectedValue = CStr(objDocumento.IdMoneda)
            End If

            If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumento.IdTipoOperacion)) IsNot Nothing) Then
                Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)
            End If

            If (cargarLiquidacion) Then

                Me.cboEstado.SelectedValue = CStr(.IdEstadoDoc)
                Me.txtCodigoDocumento.Text = .Codigo

                If (.FechaEmision <> Nothing) Then
                    Me.txtFechaEmision.Text = Format(.FechaEmision, "dd/MM/yyyy")
                End If

                Me.hddIdDocumento.Value = CStr(.Id)
                Me.hddCodigoDocumento.Value = .Codigo

            End If

            Dim IdMedioPago As Integer = 0
            '******************** OBTENEMOS ANEXO DOCUMENTO
            Dim objAnexo_Documento As Entidades.Anexo_Documento = (New Negocio.Anexo_Documento).Anexo_DocumentoSelectxIdDocumento(objDocumento.Id)
            If Not objAnexo_Documento Is Nothing Then
                IdMedioPago = objAnexo_Documento.IdMedioPago
            End If
           

            Me.actualizarControlesMedioPago1(objDocumento.IdCondicionPago, IdMedioPago, objDocumento.IdMedioPagoCredito)



        End With

        '**************** LISTA DOC REFERENCIA
        Me.GV_DocumentoRef.DataSource = listaDocumentoRef
        Me.GV_DocumentoRef.DataBind()

        '******************* PERSONA
        If (objPersona IsNot Nothing) Then

            Me.txtDescripcion_Cliente.Text = objPersona.Descripcion
            Me.txtDni_Cliente.Text = objPersona.Dni
            Me.txtRuc_Cliente.Text = objPersona.Ruc
            Me.txtIdCliente.Text = CStr(objPersona.IdPersona)
            Me.hddIdPersona.Value = CStr(objPersona.IdPersona)

            If (Me.cboTipoPV.Items.FindByValue(CStr(objPersona.IdTipoPV)) IsNot Nothing) Then
                Me.cboTipoPV.SelectedValue = CStr(objPersona.IdTipoPV)
            End If

        End If

        Me.GV_Detalle_Ingreso.DataSource = listaDetalle_Ingreso
        Me.GV_Detalle_Ingreso.DataBind()

        Me.GV_Detalle_Salida.DataSource = listaDetalle_Salida
        Me.GV_Detalle_Salida.DataBind()


        '************** OBSERVACIONES
        If (objObservaciones IsNot Nothing) Then
            Me.txtObservaciones.Text = objObservaciones.Observacion
        End If

    End Sub

    Private Sub GV_DocumentoRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoRef.SelectedIndexChanged
        quitarDocumentoRef(Me.GV_DocumentoRef.SelectedRow.RowIndex)
    End Sub

    Private Sub quitarDocumentoRef(ByVal index As Integer)

        Try

            Me.listaDocumentoRef = getListaDocumentoRef()
            Me.listaDetalleDocumento_Ingreso = getListaDetalleDocumento_Ingreso()

            '*************** QUITAMOS EL DETALLE DEL DOCUMENTO DE REFERENCIA
            Dim i As Integer = (Me.listaDetalleDocumento_Ingreso.Count - 1)
            While i >= 0

                If (CInt(Me.listaDetalleDocumento_Ingreso(i).IdDocumento) = Me.listaDocumentoRef(index).Id) Then
                    Me.listaDetalleDocumento_Ingreso.RemoveAt(i)
                End If

                i = i - 1

            End While

            Me.listaDocumentoRef.RemoveAt(index)

            '********* GUARDAMOS EN SESSION
            setListaDocumentoRef(Me.listaDocumentoRef)
            setListaDetalleDocumento_Ingreso(Me.listaDetalleDocumento_Ingreso)

            '************ GUI
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            Me.GV_Detalle_Ingreso.DataSource = Me.listaDetalleDocumento_Ingreso
            Me.GV_Detalle_Ingreso.DataBind()

            Me.cboMoneda.Enabled = True

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     calcularTotales_GV_Detalle('I','0');         ", True)


        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

#End Region

#Region "************************ DATOS DE CANCELACION"


    Protected Sub btnAddDatoCancelacion_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddDatoCancelacion.Click

        addDatoCancelacion()

    End Sub

    Private Sub addDatoCancelacion()
        Try

            Me.listaCancelacion = getListaCancelacion()

            Dim objPagoCaja As New Entidades.PagoCaja

            objPagoCaja.IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
            objPagoCaja.Factor = 1
            objPagoCaja.IdTipoMovimiento = 1 '*************** INGRESO
            objPagoCaja.NomMedioPago = Me.cboMedioPago.SelectedItem.ToString
            objPagoCaja.NomMonedaDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
            objPagoCaja.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)
            objPagoCaja.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
            objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
            objPagoCaja.Efectivo = CDec(Me.txtMonto_DatoCancelacion.Text)
            objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
            objPagoCaja.MontoEquivalenteDestino = (New Negocio.Util).SelectValorxIdMonedaOxIdMonedaD(objPagoCaja.IdMoneda, objPagoCaja.IdMonedaDestino, objPagoCaja.Efectivo, "I")
            objPagoCaja.NomMoneda = Me.cboMoneda_DatoCancelacion.SelectedItem.ToString

            objPagoCaja.IdMedioPagoInterfaz = (New Negocio.MedioPago).SelectxId(objPagoCaja.IdMedioPago)(0).IdMedioPagoInterfaz


            Select Case CInt(Me.cboMedioPago.SelectedValue)

                Case 1  '*********** EFECTIVO



                Case 3  '*********** TRANSFERENCIA

                    objPagoCaja.IdBanco = CInt(Me.cboBanco.SelectedValue)
                    objPagoCaja.IdCuentaBancaria = CInt(Me.cboCuentaBancaria.SelectedValue)
                    objPagoCaja.NumeroOp = Me.txtNro_DC.Text
                    objPagoCaja.NomBanco = Me.cboBanco.SelectedItem.ToString
                    objPagoCaja.NumeroCuenta = Me.cboCuentaBancaria.SelectedItem.ToString


                Case 8  '*********** DEPOSITO

                    objPagoCaja.IdBanco = CInt(Me.cboBanco.SelectedValue)
                    objPagoCaja.IdCuentaBancaria = CInt(Me.cboCuentaBancaria.SelectedValue)
                    objPagoCaja.NumeroOp = Me.txtNro_DC.Text
                    objPagoCaja.NomBanco = Me.cboBanco.SelectedItem.ToString
                    objPagoCaja.NumeroCuenta = Me.cboCuentaBancaria.SelectedItem.ToString

                Case 3  '************** BANCO CHEQUE

                    objPagoCaja.IdBanco = CInt(Me.cboBanco.SelectedValue)
                    objPagoCaja.NomBanco = Me.cboBanco.SelectedItem.ToString
                    objPagoCaja.NumeroCheque = Me.txtNro_DC.Text

                    Dim fecha As String = ""
                    Try
                        objPagoCaja.FechaACobrar = CDate(Me.txtFechaACobrar.Text)
                        fecha = Me.txtFechaACobrar.Text
                    Catch ex As Exception
                        objPagoCaja.FechaACobrar = Nothing
                        fecha = "---"
                    End Try

            End Select

            Me.listaCancelacion.Add(objPagoCaja)
            setListaCancelacion(Me.listaCancelacion)

            Me.GV_Cancelacion.DataSource = Me.listaCancelacion
            Me.GV_Cancelacion.DataBind()

            Me.txtMonto_DatoCancelacion.Text = "0"
            Me.txtNro_DC.Text = ""
            Me.txtFechaACobrar.Text = ""

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " onCapa('capaDatosCancelacion');  calcularDatosCancelacion(); ", True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub



    Protected Sub GV_Cancelacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_Cancelacion.SelectedIndexChanged
        quitarDatoCancelacion()
    End Sub
    Private Sub quitarDatoCancelacion()

        Try

            Me.listaCancelacion = getListaCancelacion()
            Me.listaCancelacion.RemoveAt(Me.GV_Cancelacion.SelectedRow.RowIndex)

            setListaCancelacion(Me.listaCancelacion)

            Me.GV_Cancelacion.DataSource = Me.listaCancelacion
            Me.GV_Cancelacion.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularDatosCancelacion();  onCapa('capaDatosCancelacion'); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub cboMedioPago_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboMedioPago.SelectedIndexChanged
        Try
            actualizarControlesMedioPago(1, CInt(Me.cboMedioPago.SelectedValue))

            objScript.onCapa(Me, "capaDatosCancelacion")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub actualizarControlesMedioPago(ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer)

        '************ Limpiar Datos de Cancelación
        Me.txtNro_DC.Text = ""
        Me.txtMonto_DatoCancelacion.Text = "0"
        Me.txtFechaACobrar.Text = ""

        Select Case IdCondicionPago
            Case 1  '**************** CONTADO
                Me.Panel_CP_Contado.Visible = True

                '*************** MEDIO DE PAGO
                Me.lblMonto_DC.Visible = False
                Me.txtMonto_DatoCancelacion.Visible = False
                Me.cboMoneda_DatoCancelacion.Visible = False
                Me.btnAddDatoCancelacion.Visible = False

                Me.lblBanco_DC.Visible = False
                Me.cboBanco.Visible = False
                Me.lblCuentaBancaria_DC.Visible = False
                Me.cboCuentaBancaria.Visible = False
                Me.lblNro_DC.Visible = False
                Me.txtNro_DC.Visible = False
                Me.lblFechaACobrar_DC.Visible = False
                Me.txtFechaACobrar.Visible = False
                Me.btnBuscarLiquidaciones_App.Visible = False

                Select Case IdMedioPago

                    Case 1  '*************** EFECTIVO
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True

                    Case 3  '*************** TRANSFERENCIA
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        Me.lblBanco_DC.Visible = True
                        Me.cboBanco.Visible = True
                        Me.lblCuentaBancaria_DC.Visible = True
                        Me.cboCuentaBancaria.Visible = True
                        Me.lblNro_DC.Visible = True
                        Me.txtNro_DC.Visible = True

                    Case 8  '*************** DEPOSITO
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        Me.lblBanco_DC.Visible = True
                        Me.cboBanco.Visible = True
                        Me.lblCuentaBancaria_DC.Visible = True
                        Me.cboCuentaBancaria.Visible = True
                        Me.lblNro_DC.Visible = True
                        Me.txtNro_DC.Visible = True

                    Case 7  '**************** CHEQUE
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        Me.lblBanco_DC.Visible = True
                        Me.cboBanco.Visible = True
                        Me.lblNro_DC.Visible = True
                        Me.txtNro_DC.Visible = True
                        Me.lblFechaACobrar_DC.Visible = True
                        Me.txtFechaACobrar.Visible = True

                    Case 10  '*********** LIQUIDACIONES
                        Me.lblMonto_DC.Visible = False
                        Me.txtMonto_DatoCancelacion.Visible = False
                        Me.cboMoneda_DatoCancelacion.Visible = False
                        Me.btnAddDatoCancelacion.Visible = False
                        Me.btnBuscarLiquidaciones_App.Visible = True


                End Select


            Case 2  '**************** CREDITO


        End Select

    End Sub

    Private Sub mostrarDocumentosLiquidaciones_App()
        Try

            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.DocumentoLiquidacion).DocumentoLiquidacionSelectCabFind_Aplicacion(CInt(Me.hddIdPersona.Value))

            If (lista.Count > 0) Then

                Me.GV_Liq_App.DataSource = lista
                Me.GV_Liq_App.DataBind()

                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "    onCapa('capaDatosCancelacion');     onCapa2('capaLiquidacion_App');           ", True)

            Else

                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "       onCapa('capaDatosCancelacion');  alert('No se hallaron registros.');       ", True)

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboBanco_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBanco.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)

            objScript.onCapa(Me, "capaDatosCancelacion")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_Liq_App_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Liq_App.SelectedIndexChanged
        agregarDatoCancelacion_Liq()
    End Sub
    Private Sub agregarDatoCancelacion_Liq()
        Try

            '************** VALIDAMOS SALDO
            Dim saldo As Decimal = CDec(CType(Me.GV_Liq_App.SelectedRow.FindControl("lblSaldo_Find"), Label).Text)
            Dim monto As Decimal = CDec(CType(Me.GV_Liq_App.SelectedRow.FindControl("txtMontoRecibir_Find"), TextBox).Text)
            Dim moneda As String = CStr(CType(Me.GV_Liq_App.SelectedRow.FindControl("lblMonedaMontoRecibir_Find"), Label).Text)
            Dim IdDocumento As Integer = CInt(CType(Me.GV_Liq_App.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value)
            Dim IdMonedaNC As Integer = CInt(CType(Me.GV_Liq_App.SelectedRow.FindControl("hddIdMonedaDocRef_Find"), HiddenField).Value)
            Dim nroDocumento As String = CStr(CType(Me.GV_Liq_App.SelectedRow.FindControl("lblNroDocumento_Find"), Label).Text)

            If (monto > saldo) Then
                Throw New Exception("EL MONTO INGRESADO EXCEDE AL SALDO DISPONIBLE [ " + moneda + " " + CStr(Math.Round(saldo, 2)) + " ].")
            Else
                '**************** Validamos el dato en Grilla de cancelación
                Me.listaCancelacion = getListaCancelacion()
                For i As Integer = 0 To Me.listaCancelacion.Count - 1
                    If (CInt(Me.listaCancelacion(i).NumeroCheque) = IdDocumento) Then
                        Throw New Exception("LA LIQUIDACIÓN SELECCIONADA YA SE ENCUENTRA EN [ DATOS DE CANCELACIÓN ].")
                    End If
                Next

                '**************** Agregamos el dato de cancelación
                Dim objPagoCaja As New Entidades.PagoCaja
                With objPagoCaja

                    .Efectivo = monto
                    .IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
                    .IdMoneda = CInt(IdMonedaNC)
                    .IdTipoMovimiento = 1   '******** Ingreso
                    .Factor = 1

                    .NomMedioPago = Me.cboMedioPago.SelectedItem.ToString
                    .NomMoneda = moneda

                    .NumeroCheque = CStr(IdDocumento)
                    .NumeroOp = "Nro. " + nroDocumento

                    .NomMonedaDestino = Me.cboMoneda.SelectedItem.ToString
                    .IdMonedaDestino = CInt(cboMoneda.SelectedValue)
                    .MontoEquivalenteDestino = (New Negocio.Util).SelectValorxIdMonedaOxIdMonedaD(.IdMoneda, .IdMonedaDestino, .Efectivo, "E")

                    .EfectivoInicial = .Efectivo


                End With
                Me.listaCancelacion.Add(objPagoCaja)
                setListaCancelacion(Me.listaCancelacion)

                '************* Mostramos la grilla
                Me.GV_Cancelacion.DataSource = Me.listaCancelacion
                Me.GV_Cancelacion.DataBind()

                ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  onCapa('capaDatosCancelacion');    calcularDatosCancelacion();      ", True)

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        registrarDocumentoLiquidacion(0)
    End Sub
    Private Sub validarFrm(ByVal opcion As Integer)
        '**** Opcion:
        '**** 0:  Botón guardar < ingreso = salida > '** no existe saldo a favor
        '**** 1:  Botón guardar < ingreso > salida > '** saldo a favor
        '**** 2:  Botón guardar < ingreso < salida > '** datos de cancelacion

        Me.listaDocumentoRef = getListaDocumentoRef()
        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            If (Me.listaDocumentoRef(i).Total < CDec(Me.txtTotal_Ingreso.Text)) Then

                Throw New Exception("EL TOTAL DE INGRESO ES MAYOR AL TOTAL DEL DOCUMENTO DE REFERENCIA. NO SE PERMITE LA OPERACIÓN.")

            End If

        Next

        Me.listaDetalleDocumento = getListaDetalleDocumento()
        Me.listaDetalleDocumento_Ingreso = getListaDetalleDocumento_Ingreso()

        If (Me.listaDetalleDocumento.Count <= 0 And listaDetalleDocumento_Ingreso.Count <= 0) Then

            Throw New Exception("NO SE HA INGRESADO EL DETALLE DEL DOCUMENTO. NO SE PERMITE LA OPERACIÓN.")

        End If

        Select Case opcion

            Case 0

                'If (CDec(Me.txtTotal_Ingreso.Text) <> CDec(Me.txtTotal_Salida.Text)) Then

                '    Throw New Exception("EL TOTAL DE INGRESO Y EGRESO DE PRODUCTOS NO COINCIDE. NO SE PERMITE LA OPERACIÓN.")

                'End If

            Case 1

                If (CDec(Me.txtTotal_Ingreso.Text) < CDec(Me.txtTotal_Salida.Text)) Then

                    Throw New Exception("EL TOTAL DE INGRESO ES MENOR AL TOTAL DE EGRESO. NO SE PERMITE LA OPERACIÓN.")

                End If

            Case 2

                If (CDec(Me.txtTotal_Ingreso.Text) > CDec(Me.txtTotal_Salida.Text)) Then

                    Throw New Exception("EL TOTAL DE INGRESO ES MAYOR AL TOTAL DE EGRESO. NO SE PERMITE LA OPERACIÓN.")

                End If

                If (Me.GV_Cancelacion.Rows.Count > 0) Then
                    If (CDec(Me.txtTotalRecibido.Text) < CDec(Me.txtMonto_DC.Text)) Then
                        Throw New Exception("EL TOTAL INGRESADO EN LOS DATOS DE CANCELACIÓN ES MENOR AL SALDO RESTANTE. NO SE PERMITE LA OPERACIÓN.")
                    End If

                    '***************  VERIFICAMOS EL VUELTO
                    Me.listaCancelacion = getListaCancelacion()
                    For i As Integer = 0 To Me.listaCancelacion.Count - 1

                        If (Me.listaCancelacion(i).IdMedioPago <> 1 And CDec(Me.txtVuelto.Text) > 0) Then
                            Throw New Exception("SE HAN INGRESADO MEDIOS DE PAGO QUE NO DEBEN GENERAR VUELTO. NO SE PERMITE LA OPERACIÓN.")
                        End If

                    Next

                End If

        End Select

        Me.listaDetalleDocumento_Ingreso = getListaDetalleDocumento_Ingreso()
        For i As Integer = 0 To Me.listaDetalleDocumento_Ingreso.Count - 1

            If (Me.listaDetalleDocumento_Ingreso(i).CantADespacharOriginal = 1) Then  '**** VALIDAR

                Dim valorEquivalente_CantidadOriginal As Decimal = (New Negocio.Producto).fx_getValorEquivalenteProducto(Me.listaDetalleDocumento_Ingreso(i).IdProducto, Me.listaDetalleDocumento_Ingreso(i).IdUnidadMedida, Me.listaDetalleDocumento_Ingreso(i).IdUnidadMedidaPrincipal, Me.listaDetalleDocumento_Ingreso(i).Cantidad)

                If ((valorEquivalente_CantidadOriginal - 0.1) > Me.listaDetalleDocumento_Ingreso(i).CantADespachar) Then '**** 0.1 MARGEN DE ERROR

                    Throw New Exception("LA CANTIDAD DEL PRODUCTO DE INGRESO < " + Me.listaDetalleDocumento_Ingreso(i).NomProducto + " > SUPERA LA CANTIDAD REGISTRADA EN EL DOCUMENTO DE REFERENCIA. ")

                End If

            End If

        Next

        Dim nroFilas As Integer = (New Negocio.DocumentoView).SelectCantidadFilasDocumento(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
        If (nroFilas < (Me.GV_Detalle_Ingreso.Rows.Count + Me.GV_Detalle_Salida.Rows.Count)) Then
            Throw New Exception("EL DETALLE DEL DOCUMENTO SUPERA EL NÚMERO MÁXIMO DE FILAS DEL DOCUMENTO ( NRO. FILAS " + CStr(CInt(nroFilas)) + " ).")
        End If

    End Sub
    Private Sub registrarDocumentoLiquidacion(ByVal opcion As Integer)

        '**** Opcion:
        '**** 0:  Botón guardar < ingreso = salida > '** no existe saldo a favor
        '**** 1:  Botón guardar < ingreso > salida > '** saldo a favor
        '**** 2:  Botón guardar < ingreso < salida > '** datos de cancelacion

        Try

            ActualizarlistaDetalleDocumento_Ingreso()
            ActualizarListaDetalleDocumento_Salida()

            '*************** VALIDAMOS
            validarFrm(opcion)

            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()
            Dim objAnexoDocumento As Entidades.Anexo_Documento = obtenerAnexoDocumentoCab()
            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Me.listaDetalleDocumento_Ingreso = getListaDetalleDocumento_Ingreso()
            Dim objMovCaja As Entidades.MovCaja = Nothing
            Dim listaPagoCaja As List(Of Entidades.PagoCaja) = Nothing
            Dim objObservaciones As Entidades.Observacion = obtenerObservaciones()
            Dim objRelacionDocumento As Entidades.RelacionDocumento = obtenerRelacionDocumento()

            Select Case opcion

                Case 0

                    objDocumento.Total = 0
                    objDocumento.ImporteTotal = 0
                    objDocumento.TotalAPagar = 0

                Case 1

                    Select Case CInt(Me.rdbOpcionSaldoRestante.SelectedValue)

                        Case 0
                            '<asp:ListItem Value="0" Selected="True">Mantener el Saldo Restante a favor del Cliente</asp:ListItem>
                            Dim saldoRestante As Decimal = CDec(Me.txtSaldoRestante.Text)
                            objDocumento.Total = 0
                            objDocumento.ImporteTotal = saldoRestante
                            objDocumento.TotalAPagar = 0

                        Case 1
                            '<asp:ListItem  Value="1" >NO mantener el Saldo Restante a favor del Cliente</asp:ListItem>
                            objDocumento.Total = 0
                            objDocumento.ImporteTotal = 0
                            objDocumento.TotalAPagar = 0

                        Case 2
                            '<asp:ListItem Value="2" >El Saldo Restante genera una SALIDA de efectivo de Caja</asp:ListItem>
                            Dim saldoRestante As Decimal = CDec(Me.txtSaldoRestante.Text)
                            objDocumento.Total = saldoRestante
                            objDocumento.ImporteTotal = 0
                            objDocumento.TotalAPagar = saldoRestante

                            '******* SALIDA DE DINERO
                            objMovCaja = obtenerMovCaja()
                            objMovCaja.IdTipoMovimiento = 2 '*** EGRESO

                            Dim objPagoCaja_Egreso As New Entidades.PagoCaja
                            With objPagoCaja_Egreso
                                .IdDocumento = objDocumento.Id
                                .Efectivo = CDec(Me.txtSaldoRestante.Text)
                                .Factor = -1
                                .IdMedioPago = 1  '******* EFECTIVO
                                .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
                                .IdTipoMovimiento = 2  '****** EGRESO

                            End With
                            listaPagoCaja = New List(Of Entidades.PagoCaja)
                            listaPagoCaja.Add(objPagoCaja_Egreso)

                    End Select


                Case 2

                    Dim saldoRestante As Decimal = CDec(Me.txtMonto_DC.Text)

                    objDocumento.Total = saldoRestante
                    objDocumento.ImporteTotal = 0
                    objDocumento.TotalAPagar = saldoRestante

                    objMovCaja = obtenerMovCaja()
                    listaPagoCaja = obtenerListaPagoCaja_Save()

            End Select

            If (objDocumento.TotalAPagar <> Nothing) Then
                objDocumento.TotalLetras = (New Negocio.ALetras).Letras(CStr(Math.Round(CDec(objDocumento.TotalAPagar)))) + "/100" + " " + (New Negocio.Moneda).SelectxId(objDocumento.IdMoneda)(0).Descripcion
            End If


            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo

                    objDocumento.Id = (New Negocio.DocumentoLiquidacion).registrarDocumentoLiquidacion(objDocumento, objAnexoDocumento, Me.listaDetalleDocumento_Ingreso, Me.listaDetalleDocumento, objMovCaja, listaPagoCaja, objRelacionDocumento, objObservaciones, Me.chb_MoverStockFisico_Ingreso.Checked, Me.chb_MoverStockFisico_Salida.Checked, Me.chb_Comprometer.Checked)
                    Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                    objScript.mostrarMsjAlerta(Me, "El Documento se registró con éxito.")

                Case FrmModo.Editar

                    objDocumento.Id = (New Negocio.DocumentoLiquidacion).actualizarDocumentoLiquidacion(objDocumento, objAnexoDocumento, Me.listaDetalleDocumento_Ingreso, Me.listaDetalleDocumento, objMovCaja, listaPagoCaja, objRelacionDocumento, objObservaciones, Me.chb_MoverStockFisico_Ingreso.Checked, Me.chb_MoverStockFisico_Salida.Checked, Me.chb_Comprometer.Checked)
                    Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                    objScript.mostrarMsjAlerta(Me, "El Documento se actualizó con éxito.")

            End Select

            verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function obtenerAnexoDocumentoCab() As Entidades.Anexo_Documento
        Dim objAnexoDocumento As Entidades.Anexo_Documento

        Select Case CInt(Me.cboCondicionPago.SelectedValue)
            Case 1 ' Contado

                objAnexoDocumento = New Entidades.Anexo_Documento
                With objAnexoDocumento
                    .IdMedioPago = CInt(cboMedioPago1.SelectedValue)
                End With

            Case 2

        End Select


        Return objAnexoDocumento

    End Function


    Private Function obtenerRelacionDocumento() As Entidades.RelacionDocumento

        Dim objRelacionDocumento As Entidades.RelacionDocumento = Nothing

        Me.listaDocumentoRef = getListaDocumentoRef()
        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            objRelacionDocumento = New Entidades.RelacionDocumento
            objRelacionDocumento.IdDocumento1 = Me.listaDocumentoRef(i).Id

        Next

        Return objRelacionDocumento

    End Function
    Private Function obtenerListaPagoCaja_Save() As List(Of Entidades.PagoCaja)

        Me.listaCancelacion = getListaCancelacion()

        If (Me.listaCancelacion.Count = 0) Then  '**** Creamos un Pago Caja EFECTIVO

            Dim objPagoCaja As New Entidades.PagoCaja
            With objPagoCaja
                .Efectivo = CDec(Me.txtMonto_DC.Text)
                .Factor = 1
                .IdMedioPago = CInt(Me.hddIdMedioPagoPrincipal.Value)
                .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
                .IdTipoMovimiento = 1  '****** INGRESO
                .IdMedioPagoInterfaz = (New Negocio.MedioPago).SelectxId(objPagoCaja.IdMedioPago)(0).IdMedioPagoInterfaz

            End With

            Me.listaCancelacion.Add(objPagoCaja)

        Else

            If (CDec(Me.txtVuelto.Text) > 0) Then  '********** Si existe vuelto

                For i As Integer = 0 To Me.listaCancelacion.Count - 1

                    If ((Me.listaCancelacion(i).IdMedioPago = 1) And (Me.listaCancelacion(i).IdMoneda = CInt(Me.cboMoneda.SelectedValue))) Then '******** Si es EFECTIVO y es la misma moneda de Emision
                        Me.listaCancelacion(i).Vuelto = CDec(Me.txtVuelto.Text)
                        Return Me.listaCancelacion
                    End If

                Next

                '*********** Si no existe debemos crear un Medio de Pago para el Vuelto
                Dim objPagoCaja As New Entidades.PagoCaja
                With objPagoCaja
                    .Efectivo = 0
                    .Factor = 1
                    .IdMedioPago = CInt(Me.hddIdMedioPagoPrincipal.Value)
                    .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
                    .IdTipoMovimiento = 1  '****** INGRESO
                    .Vuelto = CDec(Me.txtVuelto.Text)
                    .IdMedioPagoInterfaz = (New Negocio.MedioPago).SelectxId(objPagoCaja.IdMedioPago)(.IdMedioPago).IdMedioPagoInterfaz

                End With

                Return Me.listaCancelacion

            End If

        End If

        Return Me.listaCancelacion

    End Function
    Private Function obtenerMovCaja() As Entidades.MovCaja

        Dim objMovCaja As Entidades.MovCaja = Nothing

        Select Case CInt(1)

            Case 1 '*********** CONTADO
                objMovCaja = New Entidades.MovCaja
                With objMovCaja

                    Select Case CInt(Me.hddFrmModo.Value)

                        Case FrmModo.Nuevo

                            .IdDocumento = Nothing

                        Case FrmModo.Editar

                            .IdDocumento = CInt(Me.hddIdDocumento.Value)

                    End Select

                    .IdCaja = CInt(Me.cboCaja.SelectedValue)
                    .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
                    .IdTienda = CInt(Me.cboTienda.SelectedValue)
                    .IdPersona = CInt(Session("IdUsuario"))
                    .IdTipoMovimiento = 1  '************** INGRESO

                End With

            Case 2 '*********** CREDITO
                objMovCaja = Nothing
        End Select

        Return objMovCaja

    End Function
    Private Function obtenerObservaciones() As Entidades.Observacion

        Dim objObservacion As Entidades.Observacion = Nothing

        If (Me.txtObservaciones.Text.Trim.Length > 0) Then
            objObservacion = New Entidades.Observacion

            Select Case CInt((Me.hddFrmModo.Value))
                Case FrmModo.Nuevo
                    objObservacion.IdDocumento = Nothing
                Case FrmModo.Editar
                    objObservacion.IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select
            objObservacion.Observacion = Me.txtObservaciones.Text.Trim

        End If

        Return objObservacion

    End Function

    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo
                    .Id = Nothing
                Case FrmModo.Editar
                    .Id = CInt(Me.hddIdDocumento.Value)
            End Select

            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .Serie = CStr(Me.cboSerie.SelectedItem.ToString)
            .Codigo = Me.txtCodigoDocumento.Text.Trim
            .FechaEmision = CDate(Me.txtFechaEmision.Text)
            .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
            .IdEstadoDoc = 1 '************** ACTIVO
            .IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
            .IdCondicionPago = CInt(Me.cboCondicionPago.SelectedValue)
            .IdAlmacen = CInt(Me.cboAlmacen.SelectedValue)
            .IdUsuario = CInt(Session("IdUsuario"))
            .IdEstadoCancelacion = 2 '********* CANCELADO
            .IdEstadoEntrega = 2 '*********** ENTREGADO

            .IdPersona = CInt(Me.hddIdPersona.Value)
            .IdRemitente = CInt(Me.cboEmpresa.SelectedValue)
            .IdDestinatario = CInt(Me.hddIdPersona.Value)
            .IdTipoPV = CInt(Me.cboTipoPV.SelectedValue)
            .IdTipoDocumento = CInt(Me.hddIdTipoDocumento.Value)
            .IdCaja = CInt(Me.cboCaja.SelectedValue)

            Select Case CInt(Me.cboCondicionPago.SelectedValue)

                Case 1 '*********** CONTADO

                    .IdMedioPago = CInt(Me.cboMedioPago1.SelectedValue)
                    .IdMedioPagoCredito = Nothing

                Case 2 '********** CREDITO

                    .IdMedioPagoCredito = CInt(Me.cboMedioPago_Credito.SelectedValue)
                    Try
                        .FechaVenc = CDate(Me.txtFechaVcto.Text)
                    Catch ex As Exception
                        .FechaVenc = Nothing
                    End Try
            End Select

        End With

        Return objDocumento

    End Function

    Private Sub btnGuardar_DC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar_DC.Click
        registrarDocumentoLiquidacion(2)
    End Sub

    Private Sub btnGuardar_SR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar_SR.Click
        registrarDocumentoLiquidacion(1)
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        verFrm(FrmModo.Nuevo, True, True, True, True, True, True)
    End Sub

    Private Sub btnBuscarLiquidaciones_App_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarLiquidaciones_App.Click
        mostrarDocumentosLiquidaciones_App()
    End Sub

    Protected Sub btnBuscarDocumentoxCodigo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumentoxCodigo.Click
        Try
            buscarDocumentoLiquidacion(CInt(Me.cboSerie.SelectedValue), CInt(Me.hddCodigoDocumento.Value), Nothing)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub buscarDocumentoLiquidacion(ByVal IdSerie As Integer, ByVal codigo As Integer, ByVal IdDocumento As Integer)

        '**************** LIMPIAMOS EL FORMULARIO
        verFrm(FrmModo.Inicio, True, True, True, True, True, False)

        '*********** OBTENEMOS LA CABECERA
        Dim objDocumento As Entidades.Documento = Nothing
        If (IdDocumento = Nothing) Then
            objDocumento = (New Negocio.Documento).SelectxIdSeriexCodigo(IdSerie, codigo)
        Else
            objDocumento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)
        End If

        
        '******************** OBTENEMOS LAS LISTAS DE INGRESO Y EGRESO
        obtenerListaDetalleDocumento_LoadLiq(objDocumento.Id)

        '****************** OBTENEMOS LA LISTA DE DOCUMENTOS DE REF
        Me.listaDocumentoRef = obtenerListaDocumentoRef_Load(objDocumento.Id)

        '*********************  OBTENEMOS LAS OBS
        Dim objObservaciones As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumento.Id)

        '******************* PERSONA
        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdPersona)

        '*************** GUARDAMOS EN SESSION
        setListaDetalleDocumento(Me.listaDetalleDocumento)
        setListaDetalleDocumento_Ingreso(Me.listaDetalleDocumento_Ingreso)
        setListaDocumentoRef(Me.listaDocumentoRef)

        '************** 
        cargarDocumentoRef_GUI(objDocumento, objPersona, Me.listaDetalleDocumento_Ingreso, Me.listaDetalleDocumento, objObservaciones, Me.listaDocumentoRef, True)
        verFrm(FrmModo.Documento_Buscar_Exito, False, False, False, False, True, False)

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     calcularTotales_GV_Detalle('I','0');        calcularTotales_GV_Detalle('S','0');        ", True)

    End Sub

    Private Function obtenerListaDocumentoRef_Load(ByVal IdDocumento As Integer) As List(Of Entidades.DocumentoView)

        Dim listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = (New Negocio.DocumentoLiquidacion).DocumentoLiquidacion_SelectIdDocumentoRef(IdDocumento)
        Dim listaDocumentoRef As New List(Of Entidades.DocumentoView)

        For i As Integer = 0 To listaRelacionDocumento.Count - 1

            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(listaRelacionDocumento(i).IdDocumento1)
            listaDocumentoRef.Add(obtenerDocumentoView_LoadDocRef(objDocumento))

        Next

        Return listaDocumentoRef

    End Function


    Private Sub obtenerListaDetalleDocumento_LoadLiq(ByVal IdDocumento As Integer)

        Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumento(IdDocumento)

        For i As Integer = 0 To listaDetalle.Count - 1

            listaDetalle(i).ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(listaDetalle(i).IdProducto)

            If (listaDetalle(i).Utilidad = 1) Then  '************ INGRESO

                Me.listaDetalleDocumento_Ingreso.Add(listaDetalle(i))

            End If

            If (listaDetalle(i).Utilidad = -1) Then    '************ EGRESO

                Me.listaDetalleDocumento.Add(listaDetalle(i))

            End If

        Next

    End Sub
#Region "************************ BÚSQUEDA AVANZADO"

    Private Sub btnAceptarBusquedaAvanzado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBusquedaAvanzado.Click
        busquedaAvanzado()
    End Sub

    Private Sub busquedaAvanzado()

        Try

            Dim IdPersona As Integer = 0
            Try
                IdPersona = CInt(Me.hddIdPersona.Value)
            Catch ex As Exception
                IdPersona = 0
            End Try

            Dim IdSerie As Integer = CInt(Me.cboSerie.SelectedValue)
            Dim fechaInicio As Date = CDate(Me.txtFechaI.Text)
            Dim fechaFin As Date = CDate(Me.txtFechaF.Text)

            Me.GV_BusquedaAvanzado.DataSource = (New Negocio.Documento).DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(CInt(Me.hddIdTipoDocumento.Value), IdSerie, IdPersona, fechaInicio, fechaFin)
            Me.GV_BusquedaAvanzado.DataBind()

            objScript.onCapa(Me, "capaBusquedaAvanzado")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Protected Sub GV_BusquedaAvanzado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BusquedaAvanzado.SelectedIndexChanged
        Try
            buscarDocumentoLiquidacion(0, 0, CInt(CType(Me.GV_BusquedaAvanzado.SelectedRow.FindControl("hddIdDocumento_BusquedaAvanzado"), HiddenField).Value))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
#End Region

    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditar.Click

        Try

            verFrm(FrmModo.Editar, False, False, False, False, False, False)

            Me.listaDetalleDocumento_Ingreso = getListaDetalleDocumento_Ingreso()
            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Me.listaDocumentoRef = getListaDocumentoRef()

            Me.GV_Detalle_Ingreso.DataSource = Me.listaDetalleDocumento_Ingreso
            Me.GV_Detalle_Ingreso.DataBind()

            Me.GV_Detalle_Salida.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle_Salida.DataBind()

            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     calcularTotales_GV_Detalle('I','0');        calcularTotales_GV_Detalle('S','0');        ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        anularDocumento()
    End Sub
    Private Sub anularDocumento()

        Try

            If ((New Negocio.DocumentoLiquidacion).anularDocumentoLiquidacion(CInt(Me.hddIdDocumento.Value))) Then

                verFrm(FrmModo.Documento_Anular_Exito, False, False, False, False, False, False)
                objScript.mostrarMsjAlerta(Me, "El documento se anuló con éxito.")

            Else
                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub Actualizarlistapreciosdetalle()
        Try

            Me.GV_Detalle_Ingreso.DataSource = Nothing
            Me.GV_Detalle_Ingreso.DataBind()

            Me.GV_DocumentoRef.DataSource = Nothing
            Me.GV_DocumentoRef.DataBind()

            Me.DGV_AddProd.DataSource = Nothing
            Me.DGV_AddProd.DataBind()

            Me.GV_Cancelacion.DataSource = Nothing
            Me.GV_Cancelacion.DataBind()

            Me.listaCancelacion = New List(Of Entidades.PagoCaja)
            setListaCancelacion(Me.listaCancelacion)

            Me.listaDetalleDocumento_Ingreso = New List(Of Entidades.DetalleDocumento)
            setListaDetalleDocumento_Ingreso(Me.listaDetalleDocumento_Ingreso)

            Me.listaDocumentoRef = New List(Of Entidades.DocumentoView)
            setListaDocumentoRef(Me.listaDocumentoRef)

            ActualizarListaDetalleDocumento_Salida()

            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Dim IdTipoPV As Integer = 1

            If IsNumeric(Me.cboTipoPV.SelectedValue) And Me.cboTipoPV.SelectedValue.Trim.Length > 0 Then
                If CInt(Me.cboTipoPV.SelectedValue) > 0 Then
                    IdTipoPV = CInt(Me.cboTipoPV.SelectedValue)
                End If
            End If


            For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1

                Dim objCatalogo As Entidades.Catalogo = (New Negocio.ProductoTipoPV).SelectValor_Equivalencia_VentaxParams(CInt(Me.cboAlmacen.SelectedValue), CInt(Me.cboMoneda.SelectedValue), _
                        Me.listaDetalleDocumento(i).IdUnidadMedida, Me.listaDetalleDocumento(i).IdProducto, CInt(Me.cboTienda.SelectedValue), IdTipoPV, False)

                If objCatalogo IsNot Nothing Then

                    Me.listaDetalleDocumento(i).PrecioSD = objCatalogo.PrecioSD
                    Me.listaDetalleDocumento(i).PrecioCD = objCatalogo.PrecioSD

                End If

            Next

            Me.setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle_Salida.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle_Salida.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     calcularTotales_GV_Detalle('I','0');   calcularTotales_GV_Detalle('S','0');      ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cboMoneda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMoneda.SelectedIndexChanged



        Actualizarlistapreciosdetalle()





    End Sub

#Region "************************** BUSQUEDA PRODUCTO"
    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    'Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
    '    BuscarProductoCatalogo(2)
    'End Sub

    Private Sub btnBuscarGrilla_AddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarGrilla_AddProd.Click
        BuscarProductoCatalogo(0)
    End Sub

    Protected Sub BuscarProductoCatalogo(ByVal opcion As Integer)
        Try

            Dim IdTipoPV As Integer = CInt(Me.hddIdTipoPVDefault.Value)
            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar
                    If IsNumeric(cboTipoPV.SelectedValue) And cboTipoPV.SelectedValue.Trim.Length > 0 Then
                        If CInt(cboTipoPV.SelectedValue) > 0 Then
                            IdTipoPV = CInt(cboTipoPV.SelectedValue)
                        End If
                    End If

                    ViewState.Add("nombre_BuscarProducto", txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)
                    ViewState.Add("IdAlmacen_BuscarProducto", Me.cboAlmacen.SelectedValue)
                    ViewState.Add("IdEmpresa_BuscarProducto", Me.cboEmpresa.SelectedValue)
                    ViewState.Add("IdTienda_BuscarProducto", Me.cboTienda.SelectedValue)
                    ViewState.Add("IdMoneda_BuscarProducto", Me.cboMoneda.SelectedValue)
                    ViewState.Add("IdTipoPV_BuscarProducto", IdTipoPV)
                    ViewState.Add("CodigoSubLinea_BuscarProducto", "")
                    ViewState.Add("IdTipoExistencia_BuscarProducto", Me.CboTipoExistencia.SelectedValue)

                Case 1
                    '******************* Cuando se cambia el almacen de busqueda
                    ViewState.Add("IdAlmacen_BuscarProducto", Me.cboAlmacen.SelectedValue)

                Case 2
                    '******************** cuando se cambia la tienda de precios
                    ViewState.Add("IdTienda_BuscarProducto", Me.cboTienda.SelectedValue)

            End Select

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), CInt(ViewState("IdTienda_BuscarProducto")), CInt(ViewState("IdTipoPV_BuscarProducto")), CInt(ViewState("IdMoneda_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia_BuscarProducto")))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal codigoSubLinea As String, ByVal nomProducto As String, _
                                          ByVal IdAlmacen As Integer, ByVal IdTienda As Integer, _
                                          ByVal IdTipoPV As Integer, ByVal IdMoneda As Integer, _
                                          ByVal tipoMov As Integer, ByVal IdEmpresa As Integer, _
                                          ByVal IdTipoExistencia As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select

        Dim IdMedioPago As Integer = CInt(Me.cboMedioPago.SelectedValue)

        Dim IdCliente As Integer = 0
        If (IsNumeric(Me.hddIdPersona.Value) And (Me.hddIdPersona.Value.Trim.Length) > 0) Then
            IdCliente = CInt(Me.hddIdPersona.Value)
        End If

        Dim codigoProducto As String = Me.txtCodigoProducto.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()

        'Me.listaCatalogo = (New Negocio.Catalogo).CatalogoProducto_PV_Paginado_Cotizacion(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdTienda, IdMoneda, IdEmpresa, IdAlmacen, IdTipoPV, grilla.PageSize, index, CInt(Session("IdUsuario")), CInt(Me.cboCondicionPago.SelectedValue), IdMedioPago, IdCliente, CInt(Me.cboPais.SelectedValue), CInt(Me.cboFabricante.SelectedValue), CInt(Me.cboMarca.SelectedValue), CInt(Me.cboModelo.SelectedValue), CInt(Me.cboFormato.SelectedValue), CInt(Me.cboProveedor.SelectedValue), CInt(Me.cboEstilo.SelectedValue), CInt(Me.cboTransito.SelectedValue), CInt(Me.cboCalidad.SelectedValue))
        Me.listaCatalogo = (New Negocio.Catalogo).CatalogoProducto_PV_Paginado_Cotizacion_V2(IdTipoExistencia, IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdTienda, IdMoneda, IdEmpresa, IdAlmacen, IdTipoPV, grilla.PageSize, index, CInt(Session("IdUsuario")), 1, IdMedioPago, IdCliente, tableTipoTabla, codigoProducto)

        If Me.listaCatalogo.Count = 0 Then

            grilla.DataSource = Nothing
            grilla.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');    alert('No se hallaron registros.');        ", True)
            Return

        Else

            setListaCatalogo(Me.listaCatalogo)
            grilla.DataSource = Me.listaCatalogo
            grilla.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');           ", True)

        End If

    End Sub
    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1

            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)

        Next

        Return dt

    End Function
    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacen.SelectedValue), CInt(ViewState("IdTienda_BuscarProducto")), CInt(ViewState("IdTipoPV_BuscarProducto")), CInt(ViewState("IdMoneda_BuscarProducto")), 1, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia_BuscarProducto")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacen.SelectedValue), CInt(ViewState("IdTienda_BuscarProducto")), CInt(ViewState("IdTipoPV_BuscarProducto")), CInt(ViewState("IdMoneda_BuscarProducto")), 2, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia_BuscarProducto")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
   
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacen.SelectedValue), CInt(ViewState("IdTienda_BuscarProducto")), CInt(ViewState("IdTipoPV_BuscarProducto")), CInt(ViewState("IdMoneda_BuscarProducto")), 3, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia_BuscarProducto")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub DGV_AddProd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGV_AddProd.RowDataBound
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim gvrow As GridViewRow = CType(e.Row.Cells(3).NamingContainer, GridViewRow)
                Dim cboUM As DropDownList = CType(gvrow.FindControl("cmbUnidadMedida_AddProd"), DropDownList)
                Me.listaCatalogo(e.Row.RowIndex).ListaUM_Venta = (New Negocio.ProductoTipoPV).SelectUM_Venta(Me.listaCatalogo(e.Row.RowIndex).IdProducto, CInt(Me.cboTienda.SelectedValue), Me.listaCatalogo(e.Row.RowIndex).IdTipoPV)
                cboUM.DataSource = Me.listaCatalogo(e.Row.RowIndex).ListaUM_Venta
                cboUM.DataBind()
                cboUM.SelectedValue = Me.listaCatalogo(e.Row.RowIndex).IdUnidadMedida.ToString
                setListaCatalogo(Me.listaCatalogo)

                If (Me.listaCatalogo(e.Row.RowIndex).Kit) Then  '********* KIT

                    CType(e.Row.FindControl("btnMostrarComponenteKit_Find"), ImageButton).Visible = True

                Else

                    CType(e.Row.FindControl("btnMostrarComponenteKit_Find"), ImageButton).Visible = False

                End If

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub cmbUnidadMedidaVenta_Catalogo_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try

            ActualizarListaCatalogo()

            Me.listaCatalogo = Me.getListaCatalogo
            Dim IdTipoPV As Integer = CInt(Me.hddIdTipoPVDefault.Value)

            If IsNumeric(cboTipoPV.SelectedValue) And cboTipoPV.SelectedValue.Trim.Length > 0 Then
                If CInt(cboTipoPV.SelectedValue) > 0 Then
                    IdTipoPV = CInt(cboTipoPV.SelectedValue)
                End If
            End If

            Dim cboUM As DropDownList = CType(sender, DropDownList)
            Dim i As Integer = CType(cboUM.NamingContainer, GridViewRow).RowIndex

            Dim objCatalogo As Entidades.Catalogo = (New Negocio.ProductoTipoPV).SelectValor_Equivalencia_VentaxParams_Cotizacion(CInt(Me.cboAlmacen.SelectedValue), CInt(Me.cboMoneda.SelectedValue), _
            CInt(cboUM.SelectedValue), Me.listaCatalogo(i).IdProducto, Me.listaCatalogo(i).IdTienda, IdTipoPV, CInt(Me.cboEmpresa.SelectedValue), CDate(Me.txtFechaEmision.Text))

            If objCatalogo IsNot Nothing Then

                Me.listaCatalogo(i).PrecioSD = objCatalogo.PrecioSD
                Me.listaCatalogo(i).PrecioLista = objCatalogo.PrecioLista
                Me.listaCatalogo(i).StockDisponibleN = objCatalogo.StockDisponibleN


            End If

            setListaCatalogo(Me.listaCatalogo)

            DGV_AddProd.DataSource = Me.listaCatalogo
            DGV_AddProd.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Private Sub ActualizarListaCatalogo()

        Me.listaCatalogo = getListaCatalogo()

        For i As Integer = 0 To Me.DGV_AddProd.Rows.Count - 1

            If (CType(DGV_AddProd.Rows(i).FindControl("txtCantidad_AddProd"), TextBox).Text.Trim.Length > 0) Then
                Me.listaCatalogo(i).IdUnidadMedida = CInt(CType(DGV_AddProd.Rows(i).Cells(3).FindControl("cmbUnidadMedida_AddProd"), DropDownList).SelectedValue)
                Me.listaCatalogo(i).Cantidad = CDec(CType(DGV_AddProd.Rows(i).FindControl("txtCantidad_AddProd"), TextBox).Text)
            End If

        Next

        setListaCatalogo(Me.listaCatalogo)

    End Sub

    Protected Sub mostrarCapaStockPrecioxProducto(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim gvRow As GridViewRow = CType(CType(sender, ImageButton).NamingContainer, GridViewRow)
            Me.lblProductoConsultarStockPrecioxProd.Text = HttpUtility.HtmlDecode(gvRow.Cells(2).Text)
            Dim IdProducto As Integer = CInt(CType(gvRow.FindControl("hddIdProducto_Find"), HiddenField).Value)
            Dim IdTipoPv As Integer = CInt(Me.hddIdTipoPVDefault.Value)
            If (IsNumeric(Me.cboTipoPV.SelectedValue) And Me.cboTipoPV.SelectedValue.Trim.Length > 0) Then
                If (CInt(Me.cboTipoPV.SelectedValue) > 0) Then
                    IdTipoPv = CInt(Me.cboTipoPV.SelectedValue)
                End If
            End If

            Me.GV_ConsultarStockPrecioxProd.DataSource = (New Negocio.Catalogo).Producto_Consultar_Stock_Precio_Venta(IdProducto, 0, IdTipoPv, CInt(Me.cboMoneda.SelectedValue), (New Negocio.FechaActual).SelectFechaActual, CInt(Me.cboEmpresa.SelectedValue))
            Me.GV_ConsultarStockPrecioxProd.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa2('capaConsultarStockPrecioxProducto');   onCapa('capaBuscarProducto_AddProd');   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Protected Sub mostrarCapaConsultarTipoPrecio(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim gvRow As GridViewRow = CType(CType(sender, ImageButton).NamingContainer, GridViewRow)
            Me.lblProducto_ConsultarTipoPrecio.Text = HttpUtility.HtmlDecode(gvRow.Cells(2).Text)
            Me.lblTienda_ConsultarTipoPrecio.Text = CStr(Me.cboTienda.SelectedItem.ToString)
            Dim IdProducto As Integer = CInt(CType(gvRow.FindControl("hddIdProducto_Find"), HiddenField).Value)
            Dim IdTienda As Integer = CInt(Me.cboTienda.SelectedValue)

            Me.GV_ConsultarTipoPrecio.DataSource = (New Negocio.DocumentoCotizacion).DocumentoCotizacion_ConsultarTipoPrecioxParams(IdTienda, IdProducto, CInt(Me.cboMoneda.SelectedValue), CDate(Me.txtFechaEmision.Text))
            Me.GV_ConsultarTipoPrecio.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa2('capaConsultarTipoPrecio');   onCapa('capaBuscarProducto_AddProd');   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    'Protected Sub mostrarCapaConsultarTipoPrecio_CapaCambiarTipoPV(ByVal sender As Object, ByVal e As System.EventArgs)

    '    Try

    '        Dim indexGrilla As Integer = CInt(Me.hddIndexCambiarTipoPV_GV_Detalle.Value)
    '        Me.lblProducto_ConsultarTipoPrecio.Text = HttpUtility.HtmlDecode(CType(Me.GV_Detalle.Rows(indexGrilla).FindControl("lblDescripcion"), Label).Text)
    '        Me.lblTienda_ConsultarTipoPrecio.Text = CStr(Me.cboTienda.SelectedItem.ToString)
    '        Dim IdProducto As Integer = CInt(CType(Me.GV_Detalle.Rows(indexGrilla).FindControl("hddIdProducto"), HiddenField).Value)
    '        Dim IdTienda As Integer = CInt(Me.cboTienda.SelectedValue)

    '        Me.GV_ConsultarTipoPrecio.DataSource = (New Negocio.DocumentoCotizacion).DocumentoCotizacion_ConsultarTipoPrecioxParams(IdTienda, IdProducto, CInt(Me.cboMoneda.SelectedValue), CDate(Me.txtFechaEmision.Text))
    '        Me.GV_ConsultarTipoPrecio.DataBind()

    '        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa2('capaConsultarTipoPrecio');   onCapa('capaCambiarTipoPrecioPV');   ", True)

    '    Catch ex As Exception
    '        objScript.mostrarMsjAlerta(Me, ex.Message)
    '    End Try

    'End Sub
#End Region
#Region "************************** BUSQUEDA AVANZADA PRODUCTOS"

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Protected Sub M_B_cmbSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#End Region

    Private Sub btnAddProductos_AddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddProductos_AddProd.Click
        Try

            ActualizarListaCatalogo()
            ActualizarListaDetalleDocumento_Salida()
            ActualizarlistaDetalleDocumento_Ingreso()

            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Me.listaDetalleDocumento_Ingreso = getListaDetalleDocumento_Ingreso()

            Me.listaCatalogo = getListaCatalogo()
            For i As Integer = 0 To DGV_AddProd.Rows.Count - 1

                If (Me.listaCatalogo(i).Cantidad > 0) Then

                    Dim Obj As New Entidades.DetalleDocumento
                    Obj.IdProducto = Me.listaCatalogo(i).IdProducto
                    Obj.CodigoProducto = Me.listaCatalogo(i).CodigoProducto
                    Obj.Cantidad = Me.listaCatalogo(i).Cantidad
                    Obj.ListaUM = obtenerListaPUM(Me.listaCatalogo(i).ListaUM_Venta)

                    Obj.NomProducto = Me.listaCatalogo(i).Descripcion
                    Obj.PrecioSD = Me.listaCatalogo(i).PrecioSD
                    Obj.PrecioCD = Me.listaCatalogo(i).PrecioSD
                    Obj.Importe = Obj.Cantidad * Obj.PrecioCD
                    Obj.IdUnidadMedida = Me.listaCatalogo(i).IdUnidadMedida

                    Select Case CInt(Me.hddOpcionBuscarProducto.Value)

                        Case 0   '**** INGRESO
                            Obj.Utilidad = 1
                            Me.listaDetalleDocumento_Ingreso.Add(Obj)

                        Case 1 '******* SALIDA
                            Obj.Utilidad = -1
                            Me.listaDetalleDocumento.Add(Obj)

                    End Select

                End If

            Next

            Select Case CInt(Me.hddOpcionBuscarProducto.Value)

                Case 0 '******** INGRESO

                    setListaDetalleDocumento_Ingreso(Me.listaDetalleDocumento_Ingreso)
                    Me.GV_Detalle_Ingreso.DataSource = Me.listaDetalleDocumento_Ingreso
                    Me.GV_Detalle_Ingreso.DataBind()

                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     calcularTotales_GV_Detalle('I','0');    onCapa('capaBuscarProducto_AddProd');     ", True)

                Case 1  '******** SALIDA

                    setListaDetalleDocumento(Me.listaDetalleDocumento)
                    Me.GV_Detalle_Salida.DataSource = Me.listaDetalleDocumento
                    Me.GV_Detalle_Salida.DataBind()

                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     calcularTotales_GV_Detalle('S','0');    onCapa('capaBuscarProducto_AddProd');     ", True)

            End Select


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnMostrarComponenteKit_Find_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim index As Integer = CType(CType(sender, ImageButton).NamingContainer, GridViewRow).RowIndex

            Me.listaCatalogo = getListaCatalogo()

            '****************** MOSTRAMOS LOS COMPONENTES DEL KIT
            Me.GV_ComponenteKit_Find.DataSource = (New Negocio.Kit).SelectComponentexIdKit(Me.listaCatalogo(index).IdProducto)
            Me.GV_ComponenteKit_Find.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');           ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click

    End Sub

    Private Sub CboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboTipoExistencia.SelectedIndexChanged
        Try
            Dim objcbo As New Combo
            objcbo.llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(CboTipoExistencia.SelectedValue), True)
            objcbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub



    Protected Sub cboCondicionPago_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboCondicionPago.SelectedIndexChanged
        Try

            actualizarControlesMedioPago1(CInt(Me.cboCondicionPago.SelectedValue), 0, 0)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Protected Sub btnAddDatoCancelacion1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddDatoCancelacion1.Click

        addDatoCancelacion1()

    End Sub

    Private Sub addDatoCancelacion1()
        Try


            Me.listaCancelacion1 = getListaCancelacion1()


            Dim objPagoCaja As New Entidades.PagoCaja

            objPagoCaja.IdMedioPago = CInt(Me.cboMedioPago1.SelectedValue)
            objPagoCaja.Factor = 1
            objPagoCaja.IdTipoMovimiento = 1 '*************** INGRESO
            objPagoCaja.IdMedioPagoInterfaz = CInt(Me.cboMedioPago1.SelectedValue)

            Select Case CInt(Me.cboMedioPago1.SelectedValue)

                Case 13  '*********** EFECTIVO

                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion1.Text)
                    objPagoCaja.NomMonedaDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objPagoCaja.Efectivo = CDec(Me.txtMonto_DatoCancelacion1.Text)
                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion1.Text)
                    objPagoCaja.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion1.SelectedValue)
                    objPagoCaja.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    objPagoCaja.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objPagoCaja.Efectivo, objPagoCaja.IdMoneda, objPagoCaja.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))
                    objPagoCaja.descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago1.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion1.SelectedItem.ToString + " " + CStr(Math.Round(objPagoCaja.Efectivo, 2)) + " ]"


            End Select

            Me.listaCancelacion1.Add(objPagoCaja)
            setListaCancelacion1(Me.listaCancelacion1)

            Me.GV_Cancelacion1.DataSource = Me.listaCancelacion1
            Me.GV_Cancelacion1.DataBind()

            Me.txtMonto_DatoCancelacion1.Text = "0"

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  calcularDatosCancelacion1(); ", True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub


    Private Sub actualizarControlesMedioPago1(ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer, ByVal IdMedioPago_Credito As Integer)

        If (Me.cboCondicionPago.Items.FindByValue(CStr(IdCondicionPago)) IsNot Nothing) Then
            Me.cboCondicionPago.SelectedValue = CStr(IdCondicionPago)
        End If

        Select Case IdCondicionPago
            Case 1  '**************** CONTADO

                If (Me.cboMedioPago1.Items.FindByValue(CStr(IdMedioPago)) IsNot Nothing) Then
                    Me.cboMedioPago1.SelectedValue = CStr(IdMedioPago)
                End If

                Me.Panel_CP_Contado1.Visible = True
                Me.Panel_CP_Credito.Visible = False

            Case 2  '**************** CREDITO

                If (Me.cboMedioPago_Credito.Items.FindByValue(CStr(IdMedioPago_Credito)) IsNot Nothing) Then
                    Me.cboMedioPago_Credito.SelectedValue = CStr(IdMedioPago_Credito)
                End If

                Me.Panel_CP_Contado1.Visible = False
                Me.Panel_CP_Credito.Visible = True

        End Select

    End Sub


    Private Sub GV_Cancelacion1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Cancelacion1.SelectedIndexChanged

        quitarDatoCancelacion1()
    End Sub
    Private Sub quitarDatoCancelacion1()

        Try

            Me.listaCancelacion1 = getListaCancelacion1()
            Me.listaCancelacion1.RemoveAt(Me.GV_Cancelacion1.SelectedRow.RowIndex)

            setListaCancelacion1(Me.listaCancelacion1)

            Me.GV_Cancelacion1.DataSource = Me.listaCancelacion1
            Me.GV_Cancelacion1.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularDatosCancelacion1(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

End Class