﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FrmResumenDocumentosManuales

    '''<summary>
    '''Control rutaDestino.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents rutaDestino As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control rblDocManuales.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents rblDocManuales As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''Control txtInicio.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtInicio As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtInicio_MaskedEditExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtInicio_MaskedEditExtender As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control txtFechaVcto_Inicio_CalendarExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaVcto_Inicio_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control ddlDropDownListMotivo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ddlDropDownListMotivo As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control btnGenerar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnGenerar As Global.System.Web.UI.WebControls.Button
End Class
