﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmComprobanteRetencion
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Private listaCancelacion As List(Of Entidades.PagoCaja)
    Private listaDocumentoRef As List(Of Entidades.DocumentoCompPercepcion)
    Private listaDocumentoRef_Find As List(Of Entidades.DocumentoCompPercepcion)
    Private listaMontoRegimenPercepcion As List(Of Entidades.MontoRegimen)



    '*************** ModoFrm
    '******** 0 : INICIO / nuevo - buscar
    '******** 1 : NUEVO / guardar - cancelar
    '******** 2 : EDITAR DOCUMENTO / guardar - cancelar
    '******** 3 : BUSCAR DOCUMENTO / buscar doc - habilitar busqueda
    '******** 4 : DOCUMENTO HALLADO CORRECTAMENTE / editar - anular - imprimir - despachar - remitir - cancelar
    '******** 5 : DOC. GUARDADO CORRECTAMENTE / imprimir - despachar - remitir - cancelar
    '******** 6: OCULTAR TODOS LOS BOTONES
    '**********************************************

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
    End Enum

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub

    Private Sub cargarDatosAutomaticos()
        Dim idProveedor As Integer = Request.QueryString("idProveedor")

    End Sub
    Private Sub validarVariablesUrl(e As EventArgs)
        Dim idTipoOperacion = Request.QueryString("idTipoOperacion")
        Dim cadenaIdDocumentos = Request.QueryString("cadenaIdDocumentos")
        Dim tc As Decimal = Request.QueryString("tc")
        If idTipoOperacion IsNot Nothing Then
            Dim idProveedor As Integer = Request.QueryString("idProveedor")
            Me.cboTipoOperacion.SelectedValue = idTipoOperacion
            'SECCION PROVEEDOR
            cargarPersona(idProveedor)
            'SECCION DOCUMENTO REFERENCIA
            Dim objetoPago As New Negocio.bl_pagoProveedor
            'Me.listaDocumentoRef = getListaDocumentoRef()
            Dim objeto As New Entidades.DocumentoCompPercepcion
            Dim listaDocRef
            Me.GV_Detalle.DataSource = objetoPago.buscarRelacionConDocumentoRetencion(cadenaIdDocumentos, tc)
            Me.GV_Detalle.DataBind()
            Dim idMoneda As Integer = Request.QueryString("idMoneda")
            cboMoneda.SelectedValue = idMoneda
            Me.cboMoneda_DatoCancelacion.SelectedValue = idMoneda
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFrm()
            validarRequest()
            ConfigurarDatos()
            validarVariablesUrl(e)
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "validarRetencion();", True)
        End If
        actualizarMoneda()
    End Sub
    Private Sub actualizarMoneda()

        If (Me.cboMoneda.SelectedItem IsNot Nothing) Then
            Me.lblMonedaFaltante.Text = Me.cboMoneda.SelectedItem.ToString
            Me.lblMonedaImporteTotal.Text = Me.cboMoneda.SelectedItem.ToString
            Me.lblMonedaTotalRecibido.Text = Me.cboMoneda.SelectedItem.ToString
            Me.lblMonedaVuelto.Text = Me.cboMoneda.SelectedItem.ToString
        End If

    End Sub
    Private Sub inicializarFrm()
        Try

            Dim objCbo As New Combo
            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)
                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
                GenerarCodigoDocumento()

                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.hddIdTipoDocumento.Value), True)
                .LlenarCboRol(Me.cboRol, False)

                If (Me.cboRol.Items.FindByValue("1") IsNot Nothing) Then '***** CLIENTE
                    Me.cboRol.SelectedValue = "1"
                End If

                .LlenarCboMonedaBase(Me.cboMoneda, False)
                .LLenarCboEstadoDocumento(Me.cboEstado)

                '******************** DATOS DE CANCELACION
                .LlenarCboCondicionPago(Me.cboCondicionPago)
                .LlenarCboMedioPagoxIdTipoDocumentoxIdCondicionPago(Me.cboMedioPago, CInt(Me.hddIdTipoDocumento.Value), 1, False)
                .LlenarCboMedioPagoxIdTipoDocumentoxIdCondicionPago(Me.cboMedioPago_Credito, CInt(Me.hddIdTipoDocumento.Value), 2, False)

                .LlenarCboBanco(Me.cboBanco, False)
                .LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)
                .LlenarCboMoneda(Me.cboMoneda_DatoCancelacion, False)

                ' cboPost_DC se llena el tipo de tarjeta no el POS fue cambiado el 19/05/2011
                '.LlenarCboPostxIdCaja(Me.cboPost_DC, CInt(Me.cboCaja.SelectedValue), False)
                .LlenarCboTipoTarjetaxIdCaja(Me.cboPost_DC, CInt(Me.cboCaja.SelectedValue), False)

                Me.hddIdMedioPagoPrincipal.Value = CStr((New Negocio.MedioPago).SelectIdMedioPagoPrincipal())
                actualizarControlesMedioPago(1, CInt(Me.cboMedioPago.SelectedValue))

            End With

            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.txtFechaI.Text = Me.txtFechaEmision.Text
            Me.txtFechaF.Text = Me.txtFechaEmision.Text

            ValidarPermisos()
            verFrm(FrmModo.Nuevo, False, True, True, True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub validarRequest()

        Try

            If (Request.QueryString("IdDocumento") <> Nothing) Then

                '************ CARGAMOS EL DOCUMENTO
                Dim objDocumento As Entidades.DocumentoCompPercepcion = (New Negocio.DocumentoCompPercepcion).DocumentoCompPercepcion_GenerarxIdDocumentoRef(CInt(Request.QueryString("IdDocumento")), CInt(Me.cboMoneda.SelectedValue))

                '************ CARGAMOS EL CLIENTE
                Me.cargarPersona(objDocumento.IdPersona)

                listaDocumentoRef = New List(Of Entidades.DocumentoCompPercepcion)
                Dim newIndex As Integer = 0

                Me.listaDocumentoRef.Add(objDocumento)


                Dim ListaAnticipo As List(Of Entidades.DocumentoCompPercepcion) = (New Negocio.DocumentoCompPercepcion).DocumentoCompPercepcionAnticipo(objDocumento.Id, objDocumento.IdMoneda, objDocumento.PorcentPercepcion)

                If Not ListaAnticipo Is Nothing Then
                    If ListaAnticipo.Count > 0 Then
                        For i As Integer = 0 To ListaAnticipo.Count - 1
                            newIndex = i
                            If Not Me.listaDocumentoRef.Exists(Function(k As Entidades.DocumentoCompPercepcion) k.Id = ListaAnticipo(newIndex).Id) Then

                                Me.listaDocumentoRef.Add(ListaAnticipo(newIndex))

                            End If

                        Next
                    End If
                End If


                Dim ListaNC As List(Of Entidades.DocumentoCompPercepcion) = (New Negocio.DocumentoCompPercepcion).DocumentoCompPercepcion_NC(objDocumento.Id, objDocumento.IdMoneda, objDocumento.PorcentPercepcion)

                If Not ListaNC Is Nothing Then
                    If ListaNC.Count > 0 Then

                        For i As Integer = 0 To ListaNC.Count - 1
                            newIndex = i
                            If Not Me.listaDocumentoRef.Exists(Function(k As Entidades.DocumentoCompPercepcion) k.Id = ListaNC(newIndex).Id) Then

                                Me.listaDocumentoRef.Add(ListaNC(newIndex))

                            End If

                        Next

                    End If
                End If


                Me.GV_Detalle.DataSource = Me.listaDocumentoRef
                Me.GV_Detalle.DataBind()

                setListaDocumentoRef(Me.listaDocumentoRef)


                '********** CALCULAMOS LOS TOTALES
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularTotales();", True)

            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub
    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION / CREDITO
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {23, 24, 25, 26, 27, 28})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumentoxCodigo.Enabled = True
        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumentoxCodigo.Enabled = False
        End If

        If listaPermisos(4) > 0 Then  '********* FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

        If listaPermisos(5) > 0 Then  '********* CREDITO
            Me.cboCondicionPago.Enabled = True
        Else
            Me.cboCondicionPago.Enabled = False
        End If

    End Sub
#Region "************************ BÚSQUEDA AVANZADO"

    Private Sub btnAceptarBusquedaAvanzado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBusquedaAvanzado.Click
        busquedaAvanzado()
    End Sub

    Private Sub busquedaAvanzado()

        Try

            Dim IdPersona As Integer = 0
            Try
                IdPersona = CInt(Me.hddIdPersona.Value)
            Catch ex As Exception
                IdPersona = 0
            End Try

            Dim IdSerie As Integer = CInt(Me.cboSerie.SelectedValue)
            Dim fechaInicio As Date = CDate(Me.txtFechaI.Text)
            Dim fechaFin As Date = CDate(Me.txtFechaF.Text)

            Me.GV_BusquedaAvanzado.DataSource = (New Negocio.Documento).DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(CInt(Me.hddIdTipoDocumento.Value), IdSerie, IdPersona, fechaInicio, fechaFin)
            Me.GV_BusquedaAvanzado.DataBind()

            objScript.onCapa(Me, "capaBusquedaAvanzado")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Protected Sub GV_BusquedaAvanzado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BusquedaAvanzado.SelectedIndexChanged
        Try
            buscarDocumento(Nothing, Nothing, CInt(CType(Me.GV_BusquedaAvanzado.SelectedRow.FindControl("hddIdDocumento_BusquedaAvanzado"), HiddenField).Value))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


#End Region
#Region "****************************** MANEJO DE SESSION"

    Private Function getListaDocumentoRef() As List(Of Entidades.DocumentoCompPercepcion)
        Return CType(Session.Item("listaDocumentoRef"), List(Of Entidades.DocumentoCompPercepcion))
    End Function

    Private Sub setListaDocumentoRef(ByVal lista As List(Of Entidades.DocumentoCompPercepcion))
        Session.Remove("listaDocumentoRef")
        Session.Add("listaDocumentoRef", lista)
    End Sub

    Private Function getListaDocumentoRef_Find() As List(Of Entidades.DocumentoCompPercepcion)
        Return CType(Session.Item("listaDocumentoRef_Find"), List(Of Entidades.DocumentoCompPercepcion))
    End Function

    Private Sub setListaDocumentoRef_Find(ByVal lista As List(Of Entidades.DocumentoCompPercepcion))
        Session.Remove("listaDocumentoRef_Find")
        Session.Add("listaDocumentoRef_Find", lista)
    End Sub

    Private Sub removeListaDocumentoRef_Find()
        Session.Remove("listaDocumentoRef_Find")
    End Sub

    Private Sub removeListaDocumentoRef()
        Session.Remove("listaDocumentoRef")
    End Sub

    Private Function getListaCancelacion() As List(Of Entidades.PagoCaja)
        Return CType(Session.Item("ListaCancelacion"), List(Of Entidades.PagoCaja))
    End Function

    Private Sub setListaCancelacion(ByVal lista As List(Of Entidades.PagoCaja))
        Session.Remove("ListaCancelacion")
        Session.Add("ListaCancelacion", lista)
    End Sub

    Private Sub removeListaCancelacion()
        Session.Remove("ListaCancelacion")
    End Sub
#End Region






    Private Sub verFrm(ByVal FrmModo As Integer, ByVal limpiar As Boolean, ByVal clearSession As Boolean, ByVal initSession As Boolean, ByVal initParamGral As Boolean)

        If (limpiar) Then
            limpiarFrm()
        End If

        If (clearSession) Then
            removeListaCancelacion()
            removeListaDocumentoRef()
            removeListaDocumentoRef_Find()
        End If

        If (initSession) Then

            Me.listaCancelacion = New List(Of Entidades.PagoCaja)
            setListaCancelacion(Me.listaCancelacion)

            Me.listaDocumentoRef = New List(Of Entidades.DocumentoCompPercepcion)
            setListaDocumentoRef(Me.listaDocumentoRef)

            Me.listaDocumentoRef_Find = New List(Of Entidades.DocumentoCompPercepcion)
            setListaDocumentoRef_Find(Me.listaDocumentoRef_Find)

        End If

        If (initParamGral) Then

            '******* FECHA DE VCTO
            Dim listaParametros() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {7})
            Me.txtFechaVcto.Text = Format(DateAdd(DateInterval.Day, listaParametros(0), CDate(Me.txtFechaEmision.Text)), "dd/MM/yyyy")

        End If

        Me.hddFrmModo.Value = CStr(FrmModo)
        ActualizarBotonesControl()

    End Sub

    Private Sub ActualizarBotonesControl()

        Select Case CInt(hddFrmModo.Value)

            Case FrmModo.Inicio '************* INICIO
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_DatosCancelacion.Enabled = False



                Me.Panel_Cab.Enabled = True

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboMoneda.Enabled = True

                Me.Panel_Obs.Enabled = True
                Me.Panel_LineaCredito.Enabled = True


            Case FrmModo.Nuevo '************** Nuevo
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False


                Me.Panel_Cliente.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_DatosCancelacion.Enabled = True
                Me.Panel_Cab.Enabled = True

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboMoneda.Enabled = True

                Me.Panel_Obs.Enabled = True
                Me.Panel_LineaCredito.Enabled = True

            Case FrmModo.Editar '*********************** Editar
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False


                Me.Panel_Cliente.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_DatosCancelacion.Enabled = True
                Me.Panel_Cab.Enabled = True


                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboMoneda.Enabled = False

                Me.Panel_Obs.Enabled = True
                Me.Panel_LineaCredito.Enabled = True

            Case FrmModo.Buscar_Doc '**************************** Buscar documento
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = False
                Me.txtCodigoDocumento.Focus()
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False


                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_DatosCancelacion.Enabled = False
                Me.Panel_Cab.Enabled = True

                Me.Panel_Obs.Enabled = False
                Me.Panel_LineaCredito.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboMoneda.Enabled = True


            Case FrmModo.Documento_Buscar_Exito '********* documento hallado correctamente

                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True
                Me.btnImprimir.Visible = True


                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_DatosCancelacion.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_Obs.Enabled = False
                Me.Panel_LineaCredito.Enabled = False


                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


                Me.cboMoneda.Enabled = False



            Case FrmModo.Documento_Save_Exito '********* documento guardado correctamente

                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False

                Me.btnImprimir.Visible = True

                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_DatosCancelacion.Enabled = False
                Me.Panel_Cab.Enabled = False

                Me.Panel_Obs.Enabled = False
                Me.Panel_LineaCredito.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


                Me.cboMoneda.Enabled = False



            Case FrmModo.Documento_Anular_Exito  '*********** Anulacion exitosa

                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_DatosCancelacion.Enabled = False
                Me.Panel_Cab.Enabled = False

                Me.Panel_Obs.Enabled = False
                Me.Panel_LineaCredito.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboMoneda.Enabled = False



        End Select
    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged

        Try


            Dim objCbo As New Combo

            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))

            '**************** LINEA DE CREDITO
            If (Me.hddIdPersona.Value.Trim.Length > 0 And IsNumeric(Me.hddIdPersona.Value)) Then
                If (CInt(Me.hddIdPersona.Value) <> 0) Then

                    Me.GV_LineaCredito.DataSource = (New Negocio.CuentaPersona).SelectxParams(CInt(Me.hddIdPersona.Value), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
                    Me.GV_LineaCredito.DataBind()


                End If

            End If


            GenerarCodigoDocumento()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub GenerarCodigoDocumento()

        If (IsNumeric(Me.cboSerie.SelectedValue)) Then
            Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
        Else
            Me.txtCodigoDocumento.Text = ""
        End If

    End Sub
    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged

        Try

            Dim objCbo As New Combo

            objCbo.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))

            '**************** LINEA DE CREDITO
            If (Me.hddIdPersona.Value.Trim.Length > 0 And IsNumeric(Me.hddIdPersona.Value)) Then

                If (CInt(Me.hddIdPersona.Value) <> 0) Then
                    Me.GV_LineaCredito.DataSource = (New Negocio.CuentaPersona).SelectxParams(CInt(Me.hddIdPersona.Value), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
                    Me.GV_LineaCredito.DataBind()
                End If
            End If


            GenerarCodigoDocumento()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        Try

            GenerarCodigoDocumento()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub



    Private Sub agregarDocumentoRef_Find(ByVal Index As Integer)
        'Try

        '    Me.listaDocumentoRef = getListaDocumentoRef()
        '    Me.listaDocumentoRef_Find = getListaDocumentoRef_Find()

        '    'Dim cadenaIdDocumentos = Request.QueryString("cadenaIdDocumentos")
        '    'Dim tc As Decimal = Request.QueryString("tc")
        '    'Dim objetoPago As New Negocio.bl_pagoProveedor
        '    'Me.listaDocumentoRef = objetoPago.buscarRelacionConDocumentoRetencion(cadenaIdDocumentos, tc)

        '    For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

        '        If (Me.listaDocumentoRef(i).Id = Me.listaDocumentoRef_Find(Index).Id) Then
        '            Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO AÑADIDO AL DETALLE. NO SE PERMITE LA OPERACIÓN.")
        '        End If

        '    Next

        '    Me.listaDocumentoRef.Add(Me.listaDocumentoRef_Find(Index))

        '    Dim newIndex As Integer = 0

        '    Dim Lista As List(Of Entidades.DocumentoCompPercepcion) = (New Negocio.DocumentoCompPercepcion).DocumentoCompPercepcionAnticipo(Me.listaDocumentoRef_Find(Index).Id, Me.listaDocumentoRef_Find(Index).IdMoneda, Me.listaDocumentoRef_Find(Index).PorcentPercepcion)

        '    If Not Lista Is Nothing Then
        '        If Lista.Count > 0 Then

        '            For i As Integer = 0 To Lista.Count - 1
        '                newIndex = i

        '                If Not Me.listaDocumentoRef.Exists(Function(k As Entidades.DocumentoCompPercepcion) k.Id = Lista(newIndex).Id) Then

        '                    Me.listaDocumentoRef.Add(Lista(newIndex))

        '                End If

        '            Next

        '        End If
        '    End If

        '    Dim ListaNC As List(Of Entidades.DocumentoCompPercepcion) = (New Negocio.DocumentoCompPercepcion).DocumentoCompPercepcion_NC(Me.listaDocumentoRef_Find(Index).Id, Me.listaDocumentoRef_Find(Index).IdMoneda, Me.listaDocumentoRef_Find(Index).PorcentPercepcion)

        '    If Not ListaNC Is Nothing Then
        '        If ListaNC.Count > 0 Then
        '            For i As Integer = 0 To ListaNC.Count - 1
        '                newIndex = i
        '                If Not Me.listaDocumentoRef.Exists(Function(k As Entidades.DocumentoCompPercepcion) k.Id = ListaNC(newIndex).Id) Then

        '                    Me.listaDocumentoRef.Add(ListaNC(newIndex))

        '                End If

        '            Next
        '        End If
        '    End If


        '    Me.GV_Detalle.DataSource = Me.listaDocumentoRef
        '    Me.GV_Detalle.DataBind()

        '    setListaDocumentoRef(Me.listaDocumentoRef)

        '    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularTotales();", True)

        'Catch ex As Exception
        '    objScript.mostrarMsjAlerta(Me, ex.Message)
        'End Try
    End Sub

    Private Sub btnMostrarCapa_BuscarDocRef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMostrarCapa_BuscarDocRef.Click
        mostrarDocumentosReferencia_Find(CInt(Me.hddIdPersona.Value), CInt(Me.cboMoneda.SelectedValue))
    End Sub
    Private Sub mostrarDocumentosReferencia_Find(ByVal IdPersona As Integer, ByVal IdMonedaDestino As Integer)
        Try
            'Me.listaDocumentoRef_Find = (New Negocio.DocumentoCompPercepcion).DocumentoCompPercepcionSelectDocReferencia_Find(IdPersona, IdMonedaDestino)

            If (Me.listaDocumentoRef_Find.Count <= 0) Then
                objScript.mostrarMsjAlerta(Me, "No se hallaron Documentos con la Percepción comprometida.")
            Else
                Me.GV_DocumentosReferencia_Find.DataSource = Me.listaDocumentoRef_Find
                Me.GV_DocumentosReferencia_Find.DataBind()

                objScript.onCapa(Me, "capaDocumentosReferencia")
            End If

            setListaDocumentoRef_Find(Me.listaDocumentoRef_Find)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub btnQuitarDocReferencia_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        quitarDocumentoReferencia(CType(sender, ImageButton))
    End Sub

    Protected Sub quitarDocumentoReferencia(ByVal btnQuitarDocumentoRef As ImageButton)
        Try

            Dim gvRow As GridViewRow = CType(btnQuitarDocumentoRef.NamingContainer, GridViewRow)

            Me.listaDocumentoRef = getListaDocumentoRef()
            Me.listaDocumentoRef.RemoveAt(gvRow.RowIndex)

            Me.GV_Detalle.DataSource = Me.listaDocumentoRef
            Me.GV_Detalle.DataBind()

            setListaDocumentoRef(Me.listaDocumentoRef)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularTotales();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar.Click
        registrarComprobantePercepcion()
    End Sub

    Private Function obtenerMovCaja() As Entidades.MovCaja
        Dim objMovCaja As New Entidades.MovCaja


        Select Case CInt(Me.cboCondicionPago.SelectedValue)
            Case 1  '********** CONTADO

                With objMovCaja

                    '***** .IdCaja = CInt(Me.cboCaja.SelectedValue)
                    .IdTienda = CInt(Me.cboTienda.SelectedValue)
                    .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
                    .IdPersona = CInt(Session("IdUsuario"))
                    .IdTipoMovimiento = 1   '************* Ingreso
                    .IdCaja = CInt(Me.cboCaja.SelectedValue)

                End With

            Case 2  '********** CREDITO

                Return Nothing

        End Select

        Return objMovCaja

    End Function

    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            '***************** CABECERA
            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .Id = Nothing
                Case FrmModo.Editar
                    .Id = CInt(Me.hddIdDocumento.Value)
            End Select
            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .IdCaja = CInt(Me.cboCaja.SelectedValue)
            .Serie = Me.cboSerie.SelectedItem.ToString
            .Codigo = Me.txtCodigoDocumento.Text
            .FechaEmision = CDate(Me.txtFechaEmision.Text)
            .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
            .IdEstadoDoc = 1  '*********** ACTIVO 
            .IdPersona = CInt(Me.hddIdPersona.Value)
            .IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
            .IdCondicionPago = CInt(Me.cboCondicionPago.SelectedValue)

            Select Case CInt(Me.cboCondicionPago.SelectedValue)
                Case 1 '**** CONTADO
                    .IdEstadoCancelacion = 2  '***** CANCELADO
                    .FechaCancelacion = (.FechaEmision)
                    .IdMedioPagoCredito = Nothing

                Case 2 '**** CREDITO
                    .IdEstadoCancelacion = 1    '***** POR PAGAR
                    .FechaCancelacion = Nothing
                    .IdMedioPagoCredito = CInt(Me.cboMedioPago_Credito.SelectedValue)
                    Try
                        .FechaVenc = CDate(Me.txtFechaVcto.Text)
                    Catch ex As Exception
                        .FechaVenc = Nothing
                    End Try

            End Select

            .IdUsuario = CInt(Session("IdUsuario"))
            .IdTipoDocumento = CInt(Me.hddIdTipoDocumento.Value)

            .TotalAPagar = CDec(Me.txtImporteTotal.Text)
            .Total = CDec(Me.txtImporteTotal.Text)

            '************** OBTENEMOS IGV
            Dim tasaIgv As Decimal = (New Negocio.Impuesto).SelectTasaIGV
            .SubTotal = .Total / (1 + tasaIgv)
            .IGV = .Total - .SubTotal

            .TotalLetras = (New Negocio.ALetras).Letras(Me.txtImporteTotal.Text) + "/100 " + (New Negocio.Moneda).SelectxId(.IdMoneda)(0).Descripcion

            Try
                .FechaVenc = CDate(Me.txtFechaVcto.Text)
            Catch ex As Exception
                .FechaVenc = Nothing
            End Try

        End With

        Return objDocumento

    End Function

    Private Function obtenerMovCuenta() As Entidades.MovCuenta

        Dim objMovCuenta As Entidades.MovCuenta = Nothing

        Select Case CInt(Me.cboCondicionPago.SelectedValue)

            Case 1  '*********** CONTADO

                objMovCuenta = Nothing

            Case 2  '************* CREDITO

                objMovCuenta = New Entidades.MovCuenta

                With objMovCuenta

                    .IdCuentaPersona = obtenerIdCuentaPersona()
                    If (.IdCuentaPersona = 0) Then
                        Throw New Exception("EL CLIENTE NO POSEE LÍNEA DE CRÉDITO. NO SE PERMITE LA OPERACIÓN.")
                    End If
                    .IdPersona = CInt(Me.hddIdPersona.Value)
                    .Monto = CDec(Me.txtImporteTotal.Text)
                    .Factor = 1  '********* AUMENTA LA CUENTA CORRIENTE DE LA PERSONA

                    Select Case CInt(Me.hddFrmModo.Value)
                        Case FrmModo.Nuevo
                            .IdDocumento = Nothing
                        Case FrmModo.Editar
                            .IdDocumento = CInt(Me.hddIdDocumento.Value)
                    End Select

                    .IdMovCuentaTipo = 1
                    .Saldo = CDec(Me.txtImporteTotal.Text)

                End With

        End Select

        Return objMovCuenta

    End Function
    Private Function obtenerIdCuentaPersona() As Integer
        Dim IdCuentaPersona As Integer = 0
        Dim total As Decimal = CDec(Me.txtImporteTotal.Text)
        Dim saldoDisponible As Decimal = 0

        For i As Integer = 0 To Me.GV_LineaCredito.Rows.Count - 1

            If ((CInt(CType(Me.GV_LineaCredito.Rows(i).FindControl("hddIdMoneda"), HiddenField).Value) = CInt(Me.cboMoneda.SelectedValue)) And (CInt(CType(Me.GV_LineaCredito.Rows(i).FindControl("hddIdEmpresa"), HiddenField).Value) = CInt(Me.cboEmpresa.SelectedValue))) Then

                saldoDisponible = CDec(CType(Me.GV_LineaCredito.Rows(i).FindControl("lblDisponible"), Label).Text)

                If (total > saldoDisponible And CInt(Me.hddFrmModo.Value) = FrmModo.Nuevo) Then
                    Throw New Exception("EL CLIENTE NO POSEE SALDO DISPONIBLE.")
                Else
                    IdCuentaPersona = CInt(CType(Me.GV_LineaCredito.Rows(i).FindControl("hddIdCuenta"), HiddenField).Value)
                    Return IdCuentaPersona
                End If

            End If

        Next

        Return IdCuentaPersona

    End Function

    Private Function obtenerListaMovBanco(ByVal lista As List(Of Entidades.PagoCaja), ByVal objDocumento As Entidades.Documento) As List(Of Entidades.MovBancoView)

        Dim listaMovBanco As New List(Of Entidades.MovBancoView)
        For i As Integer = 0 To lista.Count - 1

            Dim obj As New Entidades.MovBancoView
            With obj
                .IdBanco = lista(i).IdBanco
                .IdCuentaBancaria = lista(i).IdCuentaBancaria
                .Monto = lista(i).Efectivo
                .IdMoneda = lista(i).IdMoneda
                .IdMedioPago = lista(i).IdMedioPago
                .IdUsuario = objDocumento.IdUsuario
                .IdPersonaRef = objDocumento.IdPersona
                .IdCaja = objDocumento.IdCaja
                .IdTienda = objDocumento.IdTienda
                .FechaMov = objDocumento.FechaEmision

                Select Case (lista(i).IdMedioPagoInterfaz)
                    Case 3  '*********** CHEQUE
                        .NroOperacion = lista(i).NumeroCheque
                    Case Else
                        .NroOperacion = lista(i).NumeroOp
                End Select

                .IdTarjeta = lista(i).IdTarjeta
                .IdPost = lista(i).IdPost

            End With

            listaMovBanco.Add(obj)

        Next

        Return listaMovBanco

    End Function

    Private Sub validarFrm()

        If ((CDec(Me.txtTotalRecibido.Text) < CDec(Me.txtImporteTotal.Text)) And (CInt(Me.cboCondicionPago.SelectedValue) = 1)) Then '** CONTADO

            Throw New Exception("EL TOTAL RECIBIDO ES MENOR AL TOTAL A PAGAR. NO SE PERMITE LA OPERACIÓN.")

        End If

        If (Me.GV_Cancelacion.Rows.Count <= 0 And CInt(Me.cboCondicionPago.SelectedValue) = 1) Then '*** CONTADO
            Throw New Exception("INGRESE DATOS DE CANCELACIÓN. NO SE PERMITE LA OPERACIÓN.")
        End If

        If (Me.GV_Detalle.Rows.Count <= 0) Then
            Throw New Exception("NO SE HA INGRESADO DOCUMENTOS DE REFERENCIA. NO SE PERMITE LA OPERACIÓN.")
        End If

        Me.listaCancelacion = getListaCancelacion()
        For i As Integer = 0 To Me.listaCancelacion.Count - 1

            If ((Me.listaCancelacion(i).IdMedioPago <> CInt(Me.hddIdMedioPagoPrincipal.Value)) And (CDec(Me.txtVuelto.Text) > 0)) Then

                Throw New Exception("SE HAN INGRESADO MEDIOS DE PAGO QUE NO DEBEN GENERAR VUELTO. NO SE PERMITE LA OPERACIÓN.")

            End If

        Next

    End Sub

    Private Sub registrarComprobantePercepcion()

        Try
            Dim idCadenaRequerimiento As String = ""
            idCadenaRequerimiento = Request.QueryString("idCadenaRequerimiento")

            validarFrm()


            Dim objDocumentoCab As Entidades.Documento = obtenerDocumentoCab()
            Me.listaDocumentoRef = getListaDocumentoRef()

            Me.listaMontoRegimenPercepcion = obtenerMontoRegimen_Percepcion(Me.listaDocumentoRef)

            Dim objMovCaja As Entidades.MovCaja = obtenerMovCaja()
            Dim listaPagoCaja_Save As List(Of Entidades.PagoCaja) = obtenerListaPagoCaja_Save()
            Dim objMovCuenta As Entidades.MovCuenta = obtenerMovCuenta()
            Dim objObservaciones As Entidades.Observacion = obtenerObservaciones()
            Dim listaMovBanco As List(Of Entidades.MovBancoView) = obtenerListaMovBanco(listaPagoCaja_Save, objDocumentoCab)

            '**********obtiene datos para el asiento contable
            Dim objasiento As Entidades.be_asientoContable = obtenerDatoAsientoContable()

            Select Case CInt(hddFrmModo.Value)

                Case FrmModo.Nuevo

                    objDocumentoCab.Id = (New Negocio.DocumentoCompPercepcion).DocumentoCompPercepcion_Insert(objDocumentoCab, Me.listaDocumentoRef, objMovCaja, listaPagoCaja_Save, listaMovBanco, objMovCuenta, objObservaciones, listaMontoRegimenPercepcion, obtenerDatoAsientoContable, idCadenaRequerimiento)
                    Me.hddIdDocumento.Value = CStr(objDocumentoCab.Id)
                    objScript.mostrarMsjAlerta(Me, "El Proceso finalizó con éxito.")

                Case FrmModo.Editar

                    objDocumentoCab.Id = (New Negocio.DocumentoCompPercepcion).DocumentoCompPercepcion_Update(objDocumentoCab, Me.listaDocumentoRef, objMovCaja, listaPagoCaja_Save, listaMovBanco, objMovCuenta, objObservaciones, listaMontoRegimenPercepcion)
                    Me.hddIdDocumento.Value = CStr(objDocumentoCab.Id)
                    objScript.mostrarMsjAlerta(Me, "El Proceso finalizó con éxito.")

            End Select


            verFrm(FrmModo.Documento_Save_Exito, False, False, False, False)


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function obtenerDatoAsientoContable() As Entidades.be_asientoContable
        Dim idDocumentoReferencia As Integer = Request.QueryString("IdRequerimiento")
        Dim ctaContable As String = Request.QueryString("ctaContable")
        Dim idProgramacion As Integer = Request.QueryString("idProgramacion")
        Dim objEntidadAsiento As New Entidades.be_asientoContable()
        With objEntidadAsiento
            .idEmpresa = Me.cboEmpresa.SelectedValue
            .fechaEmision = Me.txtFechaEmision.Text
            .idTipoDocumento = Me.hddIdTipoDocumento.Value
            .usuarioCreacion = Session("IdUsuario")
            .idBanco = Me.cboBanco.SelectedValue
            .idMoneda = Me.cboMoneda.SelectedValue
            .ctaCancelacion = ctaContable
            .glosa = Me.txtObservaciones.Text
            .idproveedor = Me.hddIdPersona.Value
            .importeAPagar = Me.txtTotalRecibido.Text
            .idDocumentoxCancelar = idDocumentoReferencia
            .idProgramacion = idProgramacion
            'qwe()
        End With
        Return objEntidadAsiento
    End Function

    Private Function obtenerObservaciones() As Entidades.Observacion

        Dim objObservacion As Entidades.Observacion = Nothing

        If (Me.txtObservaciones.Text.Trim.Length > 0) Then
            objObservacion = New Entidades.Observacion

            Select Case CInt((Me.hddFrmModo.Value))
                Case FrmModo.Nuevo
                    objObservacion.IdDocumento = Nothing
                Case FrmModo.Editar
                    objObservacion.IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select
            objObservacion.Observacion = Me.txtObservaciones.Text.Trim

        End If

        Return objObservacion

    End Function
    Private Function obtenerListaPagoCaja_Save() As List(Of Entidades.PagoCaja)

        Me.listaCancelacion = getListaCancelacion()

        If (Me.listaCancelacion.Count = 0) Then  '**** Creamos un Pago Caja EFECTIVO

            Dim objPagoCaja As New Entidades.PagoCaja
            With objPagoCaja
                .Efectivo = CDec(Me.txtImporteTotal.Text)
                .Factor = 1
                .IdMedioPago = CInt(Me.hddIdMedioPagoPrincipal.Value)
                .IdMedioPagoInterfaz = CInt(Me.hddIdMedioPagoInterfaz.Value)
                .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
                .IdTipoMovimiento = 1  '****** INGRESO

            End With

            Me.listaCancelacion.Add(objPagoCaja)

        Else

            If (CDec(Me.txtVuelto.Text) > 0) Then  '********** Si existe vuelto

                For i As Integer = 0 To Me.listaCancelacion.Count - 1

                    If ((Me.listaCancelacion(i).IdMedioPago = CInt(Me.hddIdMedioPagoPrincipal.Value)) And (Me.listaCancelacion(i).IdMoneda = CInt(Me.cboMoneda.SelectedValue))) Then '******** Si es EFECTIVO y es la misma moneda de Emision
                        Me.listaCancelacion(i).Vuelto = CDec(Me.txtVuelto.Text)
                        Return Me.listaCancelacion
                    End If

                Next

                '*********** Si no existe debemos crear un Medio de Pago para el Vuelto
                Dim objPagoCaja As New Entidades.PagoCaja
                With objPagoCaja

                    .Efectivo = 0
                    .Factor = 1
                    .IdMedioPago = CInt(Me.hddIdMedioPagoPrincipal.Value)
                    .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
                    .IdTipoMovimiento = 1  '****** INGRESO
                    .Vuelto = CDec(Me.txtVuelto.Text)
                    .IdMedioPagoInterfaz = CInt(Me.hddIdMedioPagoInterfaz.Value)

                End With

                Return Me.listaCancelacion

            End If

        End If

        Return Me.listaCancelacion

    End Function
    Private Sub btnBuscarDocumentoxCodigo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumentoxCodigo.Click
        buscarDocumento(CInt(Me.cboSerie.SelectedValue), CInt(Me.hddCodigoDocumento.Value.Trim), Nothing)
    End Sub
    Private Sub buscarDocumento(ByVal IdSerie As Integer, ByVal Codigo As Integer, ByVal IdDocumento As Integer)

        Try

            verFrm(FrmModo.Nuevo, True, True, True, True)
            actualizarControlesMedioPago(1, CInt(Me.hddIdMedioPagoPrincipal.Value))

            '************ CABECERA
            Dim objDocumentoCab As Entidades.DocumentoCompPercepcion = Nothing
            objDocumentoCab = (New Negocio.DocumentoCompPercepcion).DocumentoCompPercepcionSelectCab(IdSerie, Codigo, IdDocumento)

            '************* DETALLE
            Me.listaDocumentoRef = (New Negocio.DocumentoCompPercepcion).DocumentoCompPercepcionSelectDetalle(objDocumentoCab.Id)

            '********** DATOS DE CANCELACION
            Me.listaCancelacion = obtenerListaCancelacion_Load(objDocumentoCab.Id)

            '*************** OBSERVACIONES
            Dim objObservacion As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumentoCab.Id)

            '***************** PERSONA
            Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumentoCab.IdPersona)

            cargarDocumento_GUI(objDocumentoCab, Me.listaDocumentoRef, Me.listaCancelacion, objObservacion, objPersona)

            setListaDocumentoRef(Me.listaDocumentoRef)
            setListaCancelacion(Me.listaCancelacion)

            verFrm(FrmModo.Documento_Buscar_Exito, False, False, False, False)

            '************* CALCULAMOS LOS TOTALES
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularTotales();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarDocumento_GUI(ByVal objDocumentoCab As Entidades.DocumentoCompPercepcion, ByVal listaDetalle As List(Of Entidades.DocumentoCompPercepcion), ByVal listaPagoCaja As List(Of Entidades.PagoCaja), ByVal objObservacion As Entidades.Observacion, ByVal objPersona As Entidades.PersonaView)

        '*********** CABECERA        
        Me.txtCodigoDocumento.Text = objDocumentoCab.Codigo

        If (objDocumentoCab.FechaEmision <> Nothing) Then
            Me.txtFechaEmision.Text = Format(objDocumentoCab.FechaEmision, "dd/MM/yyyy")
        End If

        If (objDocumentoCab.FechaVenc <> Nothing) Then
            Me.txtFechaVcto.Text = Format(objDocumentoCab.FechaVenc, "dd/MM/yyyy")
        End If

        If (Me.cboMoneda.Items.FindByValue(CStr(objDocumentoCab.IdMoneda)) IsNot Nothing) Then
            Me.cboMoneda.SelectedValue = CStr(objDocumentoCab.IdMoneda)
        End If

        If (Me.cboEstado.Items.FindByValue(CStr(objDocumentoCab.IdEstadoDoc)) IsNot Nothing) Then
            Me.cboEstado.SelectedValue = CStr(objDocumentoCab.IdEstadoDoc)
        End If

        If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumentoCab.IdTipoOperacion)) IsNot Nothing) Then
            Me.cboTipoOperacion.SelectedValue = CStr(objDocumentoCab.IdTipoOperacion)
        End If

        If (Me.cboCondicionPago.Items.FindByValue(CStr(objDocumentoCab.IdCondicionPago)) IsNot Nothing) Then
            Me.cboCondicionPago.SelectedValue = CStr(objDocumentoCab.IdCondicionPago)
        End If

        Me.hddIdDocumento.Value = CStr(objDocumentoCab.Id)

        '************ AUXILIARES
        Me.hddContAbonos.Value = CStr(objDocumentoCab.ContAbonos)

        '**************** CLIENTE
        If (objPersona IsNot Nothing) Then
            Me.txtDescripcionPersona.Text = objPersona.Descripcion
            Me.txtDNI.Text = objPersona.Dni
            Me.txtRUC.Text = objPersona.Ruc
            Me.hddIdPersona.Value = CStr(objPersona.IdPersona)

            Me.GV_LineaCredito.DataSource = (New Negocio.CuentaPersona).SelectxParams(CInt(Me.hddIdPersona.Value), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
            Me.GV_LineaCredito.DataBind()

        End If

        '************* DETALLE
        Me.GV_Detalle.DataSource = listaDetalle
        Me.GV_Detalle.DataBind()

        '*************** DATOS DE CANCELACION
        Me.GV_Cancelacion.DataSource = listaPagoCaja
        Me.GV_Cancelacion.DataBind()

        If (objObservacion IsNot Nothing) Then
            Me.txtObservaciones.Text = objObservacion.Observacion
        End If

        actualizarControlesMedioPago(CInt(Me.cboCondicionPago.SelectedValue), CInt(Me.hddIdMedioPagoPrincipal.Value))
        actualizarMoneda()

    End Sub

    Private Sub limpiarFrm()

        '************* CABECERA
        Me.txtCodigoDocumento.Text = ""
        Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.cboEstado.SelectedValue = "1"  '******* Activo por defecto        

        '************* Auxiliares
        Me.hddContAbonos.Value = ""

        '************ DATOS DEL CLIENTE
        Me.txtDescripcionPersona.Text = ""
        Me.txtDNI.Text = ""
        Me.txtRUC.Text = ""
        Me.GV_LineaCredito.DataSource = Nothing
        Me.GV_LineaCredito.DataBind()

        '********** DETALLE
        Me.GV_Detalle.DataSource = Nothing
        Me.GV_Detalle.DataBind()
        Me.txtImporteTotal.Text = "0"

        '************* DATOS DE CANCELACION
        Me.GV_Cancelacion.DataSource = Nothing
        Me.GV_Cancelacion.DataBind()

        '************* OBSERVACIONES
        Me.txtObservaciones.Text = ""

        '**************** HIDDEN
        Me.hddCodigoDocumento.Value = ""
        Me.hddIdPersona.Value = ""
        Me.hddIdDocumento.Value = ""
        Me.hddPermiso_AddMedioPago.Value = "0"

        '**************** DOCUMENTOS REF FIN
        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Try
            verFrm(FrmModo.Nuevo, True, True, True, True)
            GenerarCodigoDocumento()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularTotales();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnEditar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEditar.Click
        Try

            Me.listaDocumentoRef = getListaDocumentoRef()
            Me.GV_Detalle.DataSource = Me.listaDocumentoRef
            Me.GV_Detalle.DataBind()

            Me.listaCancelacion = New List(Of Entidades.PagoCaja)
            setListaCancelacion(Me.listaCancelacion)
            Me.GV_Cancelacion.DataSource = Nothing
            Me.GV_Cancelacion.DataBind()

            If (IsNumeric(Me.hddIdPersona.Value) And Me.hddIdPersona.Value.Trim.Length > 0) Then
                If (CInt(Me.hddIdPersona.Value) <> 0) Then

                    Me.GV_LineaCredito.DataSource = (New Negocio.CuentaPersona).SelectxParams(CInt(Me.hddIdPersona.Value), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
                    Me.GV_LineaCredito.DataBind()

                End If
            End If



            verFrm(FrmModo.Editar, False, False, False, False)

            '************* CALCULAMOS LOS TOTALES
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularTotales();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        anularDocumento()
    End Sub
    Private Sub anularDocumento()
        Try

            If (New Negocio.DocumentoCompPercepcion).DocumentoCompPercepcion_Anular(CInt(Me.hddIdDocumento.Value)) Then

                verFrm(FrmModo.Documento_Anular_Exito, False, False, False, False)
                objScript.mostrarMsjAlerta(Me, "El Documento se anuló con éxito.")

            Else

                Throw New Exception("Problemas en la Operación.")

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


#Region "**************************** DATOS DE CANCELACION"

    Protected Sub cboCondicionPago_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboCondicionPago.SelectedIndexChanged
        Try

            If (Me.GV_Cancelacion.Rows.Count > 0) Then
                Throw New Exception("SE HAN INGRESADO DATOS DE CANCELACIÓN. NO SE PERMITE LA OPERACIÓN.")
            End If

            actualizarControlesMedioPago(CInt(Me.cboCondicionPago.SelectedValue), CInt(Me.cboMedioPago.SelectedValue))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub actualizarControlesMedioPago(ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer)

        '************ Limpiar Datos de Cancelación
        Me.txtNro_DC.Text = ""
        Me.txtMonto_DatoCancelacion.Text = "0"
        Me.txtFechaACobrar.Text = ""
        Me.hddPermiso_AddMedioPago.Value = "0"

        Select Case IdCondicionPago
            Case 1  '**************** CONTADO
                Me.Panel_CP_Contado.Visible = True
                Me.Panel_CP_Credito.Visible = False

                '*************** MEDIO DE PAGO
                Me.lblMonto_DC.Visible = False
                Me.txtMonto_DatoCancelacion.Visible = False
                Me.cboMoneda_DatoCancelacion.Visible = False
                Me.btnAddDatoCancelacion.Visible = False

                Me.lblPost_DC.Visible = False
                Me.cboPost_DC.Visible = False
                Me.lblTarjeta_DC.Visible = False
                Me.cboTarjeta_DC.Visible = False
                Me.lblBanco_DC.Visible = False
                Me.cboBanco.Visible = False
                Me.lblCuentaBancaria_DC.Visible = False
                Me.cboCuentaBancaria.Visible = False
                Me.lblNro_DC.Visible = False
                Me.txtNro_DC.Visible = False
                Me.lblFechaACobrar_DC.Visible = False
                Me.txtFechaACobrar.Visible = False

                Me.btnVer_NotaCredito.Visible = False

                Dim objMedioPago As Entidades.MedioPago = (New Negocio.MedioPago).SelectxId(IdMedioPago)(0)

                Me.cboBanco.Enabled = True
                Me.cboCuentaBancaria.Enabled = True

                Select Case objMedioPago.IdMedioPagoInterfaz

                    Case 1  '*************** EFECTIVO
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True

                    Case 2  '*************** BANCO
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        Me.lblBanco_DC.Visible = True
                        Me.cboBanco.Visible = True
                        Me.lblCuentaBancaria_DC.Visible = True
                        Me.cboCuentaBancaria.Visible = True
                        Me.lblNro_DC.Visible = True
                        Me.txtNro_DC.Visible = True

                        actualizarControles_BANCOS()

                    Case 3  '**************** BANCO CHEQUE
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        Me.lblBanco_DC.Visible = True
                        Me.cboBanco.Visible = True
                        Me.lblNro_DC.Visible = True
                        Me.txtNro_DC.Visible = True
                        Me.lblFechaACobrar_DC.Visible = True
                        Me.txtFechaACobrar.Visible = True

                        actualizarControles_BANCOS()

                    Case 4  '*********** TARJETA
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        Me.lblBanco_DC.Visible = True
                        Me.cboBanco.Visible = True
                        Me.lblCuentaBancaria_DC.Visible = True
                        Me.cboCuentaBancaria.Visible = True
                        Me.lblNro_DC.Visible = True
                        Me.txtNro_DC.Visible = True
                        Me.lblPost_DC.Visible = True
                        Me.cboPost_DC.Visible = True
                        Me.lblTarjeta_DC.Visible = True
                        Me.cboTarjeta_DC.Visible = True

                        '************ Configuramos los cambios por POST
                        actualizarControles_POST()
                        Me.cboBanco.Enabled = False
                        Me.cboCuentaBancaria.Enabled = False

                    Case 5  '************* NOTA CREDITO

                        Me.btnVer_NotaCredito.Visible = True

                    Case 6  '************** COMPROBANTE DE RETENCION
                        Dim idMoneda As Integer = Request.QueryString("idMoneda")
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        Me.lblNro_DC.Visible = True
                        Me.txtNro_DC.Visible = True

                End Select

                Me.hddIdMedioPagoInterfaz.Value = CStr(objMedioPago.IdMedioPagoInterfaz)

            Case 2  '**************** CREDITO
                Me.Panel_CP_Contado.Visible = False
                Me.Panel_CP_Credito.Visible = True


        End Select

    End Sub

    Private Function obtenerListaCancelacion_Load(ByVal IdDocumento As Integer) As List(Of Entidades.PagoCaja)
        Dim lista As List(Of Entidades.PagoCaja) = (New Negocio.PagoCaja).SelectListaxIdDocumento(IdDocumento)

        For i As Integer = 0 To lista.Count - 1

            Select Case lista(i).IdMedioPagoInterfaz


                Case 1  '*********** EFECTIVO

                    lista(i).descripcionPagoCaja = " Medio Pago: [ " + lista(i).NomMedioPago + " ] Monto: [ " + lista(i).NomMoneda + " " + CStr(Math.Round(lista(i).Efectivo, 2)) + " ]"

                Case 2  '*********** BANCO  (NRO OPERACION)

                    lista(i).descripcionPagoCaja = " Medio Pago: [ " + lista(i).NomMedioPago + " ] Monto: [ " + lista(i).NomMoneda + " " + CStr(Math.Round(lista(i).Efectivo, 2)) + " ] Banco: [ " + lista(i).NomBanco + " ] Cuenta: [ " + lista(i).NumeroCuenta + " ]"


                Case 3  '************** BANCO CHEQUE

                    Dim fecha As String = ""

                    If (lista(i).FechaACobrar <> Nothing) Then
                        fecha = Format(lista(i).FechaACobrar, "dd/MM/yyyy")
                    Else
                        fecha = "---"
                    End If

                    lista(i).descripcionPagoCaja = " Medio Pago: [ " + lista(i).NomMedioPago + " ] Monto: [ " + lista(i).NomMoneda + " " + CStr(Math.Round(lista(i).Efectivo, 2)) + " ] Banco: [ " + lista(i).NomBanco + " ] Fecha a Cobrar: [ " + fecha + " ]"


                Case 4 '************* TARJETA

                    lista(i).descripcionPagoCaja = " Medio Pago: [ " + lista(i).NomMedioPago + " ] Monto: [ " + lista(i).NomMoneda + " " + CStr(Math.Round(lista(i).Efectivo, 2)) + " ] Banco: [ " + lista(i).NomBanco + " ] Cuenta: [ " + lista(i).NumeroCuenta + " ]"

                Case 5   '*********** NOTA DE CREDITO

                    lista(i).descripcionPagoCaja = " Medio Pago: [ " + lista(i).NomMedioPago + " ] Monto: [ " + lista(i).NomMoneda + " " + CStr(Math.Round(lista(i).Efectivo, 2)) + " ] Nro. Documento: [ " + lista(i).NumeroOp + " ] "

                Case 6  '************* COMP RETENCION

                    lista(i).descripcionPagoCaja = " Medio Pago: [ " + lista(i).NomMedioPago + " ] Monto: [ " + lista(i).NomMoneda + " " + CStr(Math.Round(lista(i).Efectivo, 2)) + " ] Nro. Documento: [ " + lista(i).NumeroOp + " ] "

            End Select


        Next

        Return lista
    End Function

    Protected Sub cboMedioPago_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboMedioPago.SelectedIndexChanged
        Try
            actualizarControlesMedioPago(1, CInt(Me.cboMedioPago.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAddDatoCancelacion_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddDatoCancelacion.Click

        addDatoCancelacion()

    End Sub

    Private Sub addDatoCancelacion()
        Try


            If (CInt(Me.hddPermiso_AddMedioPago.Value) <= 0) Then
                If ((New Negocio.Perfil_MedioPago).Perfil_MedioPago_ValidarxIdMedioPagoxIdUsuario(CInt(Session("IdUsuario")), CInt(Me.cboMedioPago.SelectedValue)) <= 0) Then

                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  alert('NO POSEE LOS PERMISOS NECESARIOS PARA INGRESAR ESTE MEDIO DE PAGO. NO SE PERMITE LA OPERACIÒN.');     verCapaPermiso_AddMedioPago();  ", True)
                    Return

                End If
            End If


            Me.listaCancelacion = getListaCancelacion()

            Dim objPagoCaja As New Entidades.PagoCaja

            objPagoCaja.IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
            objPagoCaja.Factor = 1
            objPagoCaja.IdTipoMovimiento = 1 '*************** INGRESO
            objPagoCaja.IdMedioPagoInterfaz = CInt(Me.hddIdMedioPagoInterfaz.Value)

            Select Case CInt(Me.hddIdMedioPagoInterfaz.Value)

                Case 1  '*********** EFECTIVO

                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.NomMonedaDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objPagoCaja.Efectivo = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)
                    objPagoCaja.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    objPagoCaja.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objPagoCaja.Efectivo, objPagoCaja.IdMoneda, objPagoCaja.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))
                    objPagoCaja.descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objPagoCaja.Efectivo, 2)) + " ]"

                Case 2  '*********** BANCO  (NRO OPERACION)

                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.Efectivo = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)
                    objPagoCaja.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)

                    objPagoCaja.NomMonedaDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objPagoCaja.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objPagoCaja.Efectivo, objPagoCaja.IdMoneda, objPagoCaja.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))

                    objPagoCaja.IdBanco = CInt(Me.cboBanco.SelectedValue)
                    objPagoCaja.IdCuentaBancaria = CInt(Me.cboCuentaBancaria.SelectedValue)

                    objPagoCaja.descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objPagoCaja.Efectivo, 2)) + " ] Banco: [ " + Me.cboBanco.SelectedItem.ToString + " ] Cuenta: [ " + Me.cboCuentaBancaria.SelectedItem.ToString + " ]"
                    objPagoCaja.NumeroOp = Me.txtNro_DC.Text

                Case 3  '************** BANCO CHEQUE

                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.Efectivo = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)

                    objPagoCaja.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    objPagoCaja.NomMonedaDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objPagoCaja.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objPagoCaja.Efectivo, objPagoCaja.IdMoneda, objPagoCaja.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))

                    objPagoCaja.IdBanco = CInt(Me.cboBanco.SelectedValue)

                    Dim fecha As String = ""
                    Try
                        objPagoCaja.FechaACobrar = CDate(Me.txtFechaACobrar.Text)
                        fecha = Me.txtFechaACobrar.Text
                    Catch ex As Exception
                        objPagoCaja.FechaACobrar = Nothing
                        fecha = "---"
                    End Try

                    objPagoCaja.descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objPagoCaja.Efectivo, 2)) + " ] Banco: [ " + Me.cboBanco.SelectedItem.ToString + " ] Fecha a Cobrar: [ " + fecha + " ]"
                    objPagoCaja.NumeroCheque = Me.txtNro_DC.Text

                Case 4 '************* TARJETA

                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.Efectivo = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)

                    objPagoCaja.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    objPagoCaja.NomMonedaDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objPagoCaja.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objPagoCaja.Efectivo, objPagoCaja.IdMoneda, objPagoCaja.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))
                    objPagoCaja.NumeroOp = CStr(Me.txtNro_DC.Text.Trim)

                    objPagoCaja.IdBanco = CInt(Me.cboBanco.SelectedValue)
                    objPagoCaja.IdCuentaBancaria = CInt(Me.cboCuentaBancaria.SelectedValue)
                    objPagoCaja.IdPost = (New Negocio.PostView).SelectIdPosxTipoTarjetaCaja(CInt(Me.cboTarjeta_DC.SelectedValue), CInt(Me.cboPost_DC.SelectedValue), CInt(Me.cboCaja.SelectedValue))
                    objPagoCaja.IdTarjeta = CInt(Me.cboTarjeta_DC.SelectedValue)
                    objPagoCaja.descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objPagoCaja.Efectivo, 2)) + " ] Banco: [ " + Me.cboBanco.SelectedItem.ToString + " ] Cuenta: [ " + Me.cboCuentaBancaria.SelectedValue.ToString + " ]"

                Case 5   '*********** NOTA DE CREDITO

                    '*************** NO SE UTILIZA

                Case 6   '************** COMP RETENCION


                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.Efectivo = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)

                    objPagoCaja.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    objPagoCaja.NomMonedaDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objPagoCaja.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objPagoCaja.Efectivo, objPagoCaja.IdMoneda, objPagoCaja.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))
                    objPagoCaja.NumeroOp = CStr(Me.txtNro_DC.Text.Trim)

                    objPagoCaja.descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objPagoCaja.Efectivo, 2)) + " ] "

            End Select

            Me.listaCancelacion.Add(objPagoCaja)
            setListaCancelacion(Me.listaCancelacion)

            Me.GV_Cancelacion.DataSource = Me.listaCancelacion
            Me.GV_Cancelacion.DataBind()

            Me.txtMonto_DatoCancelacion.Text = "0"
            Me.txtNro_DC.Text = ""
            Me.txtFechaACobrar.Text = ""

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularDatosCancelacion(); ", True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Protected Sub GV_Cancelacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_Cancelacion.SelectedIndexChanged
        quitarDatoCancelacion()
    End Sub
    Private Sub quitarDatoCancelacion()

        Try

            Me.listaCancelacion = getListaCancelacion()
            Me.listaCancelacion.RemoveAt(Me.GV_Cancelacion.SelectedRow.RowIndex)

            setListaCancelacion(Me.listaCancelacion)

            Me.GV_Cancelacion.DataSource = Me.listaCancelacion
            Me.GV_Cancelacion.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularDatosCancelacion(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Private Sub GV_NotaCredito_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_NotaCredito.SelectedIndexChanged
        agregarDatoCancelacion_NC()
    End Sub

    Private Sub agregarDatoCancelacion_NC()
        Try

            '************** VALIDAMOS SALDO
            Dim saldo As Decimal = CDec(CType(Me.GV_NotaCredito.SelectedRow.FindControl("lblSaldo_Find"), Label).Text)
            Dim monto As Decimal = CDec(CType(Me.GV_NotaCredito.SelectedRow.FindControl("txtMontoRecibir_Find"), TextBox).Text)
            Dim moneda As String = CStr(CType(Me.GV_NotaCredito.SelectedRow.FindControl("lblMonedaMontoRecibir_Find"), Label).Text)
            Dim IdDocumento As Integer = CInt(CType(Me.GV_NotaCredito.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value)
            Dim IdMonedaNC As Integer = CInt(CType(Me.GV_NotaCredito.SelectedRow.FindControl("hddIdMonedaDocRef_Find"), HiddenField).Value)
            Dim nroDocumento As String = CStr(CType(Me.GV_NotaCredito.SelectedRow.FindControl("lblNroDocumento_Find"), Label).Text)

            If (monto > saldo) Then
                Throw New Exception("El Monto Ingresado excede al saldo disponible [ " + moneda + " " + CStr(Math.Round(saldo, 2)) + " ].")
            Else
                '**************** Validamos el dato en Grilla de cancelación
                Me.listaCancelacion = getListaCancelacion()
                For i As Integer = 0 To Me.listaCancelacion.Count - 1
                    If (CInt(Me.listaCancelacion(i).NumeroCheque) = IdDocumento) Then
                        Throw New Exception("La Nota de Crédito seleccionado ya se encuentra en [ Datos de Cancelación ].")
                    End If
                Next

                '**************** Agregamos el dato de cancelación
                Dim objPagoCaja As New Entidades.PagoCaja
                With objPagoCaja

                    .Efectivo = monto
                    .IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
                    .IdMoneda = CInt(IdMonedaNC)
                    .IdTipoMovimiento = 1   '******** Ingreso
                    .Factor = 1

                    .NumeroCheque = CStr(IdDocumento)

                    .NomMonedaDestino = Me.cboMoneda.SelectedItem.ToString
                    .IdMonedaDestino = CInt(cboMoneda.SelectedValue)
                    .MontoEquivalenteDestino = (New Negocio.Util).SelectValorxIdMonedaOxIdMonedaD(.IdMoneda, .IdMonedaDestino, .Efectivo, "E")
                    .descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objPagoCaja.Efectivo, 2)) + " ] Nro. Documento: [ " + nroDocumento + " ]"

                    .IdMedioPagoInterfaz = CInt(Me.hddIdMedioPagoInterfaz.Value)

                End With
                Me.listaCancelacion.Add(objPagoCaja)
                setListaCancelacion(Me.listaCancelacion)

                '************* Mostramos la grilla
                Me.GV_Cancelacion.DataSource = Me.listaCancelacion
                Me.GV_Cancelacion.DataBind()

                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " offCapa('capaDocumento_NotaCredito');  calcularDatosCancelacion();", True)

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub mostrarDocumentosNotaCredito()
        Try

            If (CInt(Me.hddPermiso_AddMedioPago.Value) <= 0) Then
                If ((New Negocio.Perfil_MedioPago).Perfil_MedioPago_ValidarxIdMedioPagoxIdUsuario(CInt(Session("IdUsuario")), CInt(Me.cboMedioPago.SelectedValue)) <= 0) Then

                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  alert('NO POSEE LOS PERMISOS NECESARIOS PARA INGRESAR ESTE MEDIO DE PAGO. NO SE PERMITE LA OPERACIÒN.');     verCapaPermiso_AddMedioPago();   ", True)
                    Return

                End If
            End If


            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.DocumentoNotaCredito).DocumentoNotaCreditoSelectCabFind_Aplicacion(CInt(Me.hddIdPersona.Value), 0, 0)

            If (lista.Count > 0) Then

                Me.GV_NotaCredito.DataSource = lista
                Me.GV_NotaCredito.DataBind()
                objScript.onCapa(Me, "capaDocumento_NotaCredito")

            Else

                Throw New Exception("No se hallaron registros.")

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnVer_NotaCredito_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVer_NotaCredito.Click
        mostrarDocumentosNotaCredito()
    End Sub


    Private Sub cboPost_DC_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPost_DC.SelectedIndexChanged
        Try
            actualizarControles_POST()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub actualizarControles_POST()

        Dim objcbo As New Combo

        With objcbo
            ' Se Cambio
            '.LlenarCboTarjetaxIdPost(Me.cboTarjeta_DC, CInt(Me.cboPost_DC.SelectedValue), False)
            .LlenarCboTarjetaxTipoTarjetaCaja(Me.cboTarjeta_DC, CInt(Me.cboPost_DC.SelectedValue), CInt(Me.cboCaja.SelectedValue))

            Dim objPost As Entidades.Post = (New Negocio.PostView).SelectxIdPost(CInt(Me.cboPost_DC.SelectedValue))

            '************* Banco Cuenta bancaria
            Me.cboBanco.SelectedValue = CStr(objPost.IdBanco)

            .LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)
            Me.cboCuentaBancaria.SelectedValue = CStr(objPost.IdCuentaBancaria)

        End With

    End Sub

    Private Sub actualizarControles_BANCOS()

        Dim objCbo As New Combo
        With objCbo
            .LlenarCboBanco(Me.cboBanco, False)
            .LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)
        End With

    End Sub

    Private Sub cboBanco_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBanco.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region
#Region "****************Buscar Personas Mantenimiento"
    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            '************** DATOS DE CANCELACION
            Me.GV_Cancelacion.DataSource = Nothing
            Me.GV_Cancelacion.DataBind()

            Me.listaCancelacion = New List(Of Entidades.PagoCaja)
            setListaCancelacion(Me.listaCancelacion)

            '***************** DOCUMENTOS DE REFERENCIA
            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

            Me.listaDocumentoRef_Find = New List(Of Entidades.DocumentoCompPercepcion)
            setListaDocumentoRef_Find(Me.listaDocumentoRef_Find)

            '***************** DOCUMENTOS DE REFERENCIA FIND
            Me.GV_Detalle.DataSource = Nothing
            Me.GV_Detalle.DataBind()

            Me.listaDocumentoRef = New List(Of Entidades.DocumentoCompPercepcion)
            setListaDocumentoRef(Me.listaDocumentoRef)

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "   offCapa('capaPersona');    calcularTotales();    ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)

        Dim objPersonaView As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)
        If (objPersonaView IsNot Nothing) Then

            Me.txtDescripcionPersona.Text = objPersonaView.Descripcion
            Me.txtDNI.Text = objPersonaView.Dni
            Me.txtRUC.Text = objPersonaView.Ruc
            Me.hddIdPersona.Value = CStr(objPersonaView.IdPersona)
        End If

        '************ LINEA DE CREDITO
        Me.GV_LineaCredito.DataSource = (New Negocio.CuentaPersona).SelectxParams(CInt(Me.hddIdPersona.Value), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
        Me.GV_LineaCredito.DataBind()


    End Sub
    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)
        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", CInt(Me.cboRol.SelectedValue))

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region


    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged

        agregarDocumentoRef_Find(Me.GV_DocumentosReferencia_Find.SelectedIndex)

    End Sub


    Private Sub btnAceptar_AddMedioPago_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar_AddMedioPago.Click
        validarLogeo_PermisoAddMedioPago()
    End Sub

    Private Sub validarLogeo_PermisoAddMedioPago()

        Try

            If (Me.txtClave_AddMedioPago.Text.Trim <> Me.txtClave2_AddMedioPago.Text.Trim) Then
                Throw New Exception("LAS CLAVES INGRESADAS NO COINCIDEN. NO SE PERMITE LA OPERACIÓN.")
            End If

            Dim IdUsuario As Integer = (New Negocio.Usuario).ValidarIdentidad(Me.txtUsuario_AddMedioPago.Text.Trim, Me.txtClave_AddMedioPago.Text.Trim)

            If (IdUsuario = Nothing) Then
                Throw New Exception("USUARIO Y/O CLAVE INCORRECTOS. NO SE PERMITE LA OPERACIÓN.")
            End If

            If ((New Negocio.Perfil_MedioPago).Perfil_MedioPago_ValidarxIdMedioPagoxIdUsuario(IdUsuario, CInt(Me.cboMedioPago.SelectedValue)) <= 0) Then

                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  alert('NO POSEE LOS PERMISOS NECESARIOS PARA INGRESAR ESTE MEDIO DE PAGO. NO SE PERMITE LA OPERACIÒN.');   verCapaPermiso_AddMedioPago();   ", True)
                Return

            End If

            Me.hddPermiso_AddMedioPago.Value = "1"
            objScript.mostrarMsjAlerta(Me, ("Se ha concedido la autorización para agregar el Medio de Pago < " + Me.cboMedioPago.SelectedItem.ToString + " >"))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try






    End Sub


    Private Function obtenerMontoRegimen_Percepcion(ByVal listaDocumentoCompPercepcionMontoRegimen As List(Of Entidades.DocumentoCompPercepcion)) As List(Of Entidades.MontoRegimen)

        Dim listaMontoRegimen As New List(Of Entidades.MontoRegimen)
        Dim objMontoRegimen As Entidades.MontoRegimen = Nothing

        For i As Integer = 0 To listaDocumentoCompPercepcionMontoRegimen.Count - 1

            If (listaDocumentoCompPercepcionMontoRegimen(i).Percepcion) > 0 Then

                objMontoRegimen = New Entidades.MontoRegimen
                With objMontoRegimen

                    .IdDocumento = listaDocumentoCompPercepcionMontoRegimen(i).Id
                    .Monto = listaDocumentoCompPercepcionMontoRegimen(i).Percepcion
                    .ro_Id = 1 '***************** PERCEPCION

                End With

                listaMontoRegimen.Add(objMontoRegimen)

            End If

        Next

        Return listaMontoRegimen

    End Function



End Class