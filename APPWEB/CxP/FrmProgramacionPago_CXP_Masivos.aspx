﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmProgramacionPago_CXP_Masivos.aspx.vb" Inherits="APPWEB.FrmProgramacionPago_CXP_Masivos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
       <tr>
        <td style="text-align: center; color: #0000FF;"><strong>CANCELACION DE REQUERIMIENTOS PROGRAMADOS DE 
            FORMA MASIVA.</strong></td>
       </tr>
        <tr>
            <td>
                <table class="style1" style="width:87%">
                    <tr>
                        <td style="width: 365px">
                        <div id="CapaFiltros">
                        <table border=0  style="width: 213%">
                        <tr>

                          <td style="width: 232px">
                              <table style="width: 250px;border:1px solid blue;">
                                <tr>
                                <td style="height: 247px">
                                <table border=0>
                                    <td style="width: 654px; text-align: right; height: 26px; color: #0000FF;">
                                        Fecha Inicial :</td>
                                    <td style="width: 336px; height: 26px;">
                                         <asp:TextBox ID="TxtFechaInicio" runat="server" style="margin-left: 0px" ></asp:TextBox>                                         
                                           <cc1:calendarextender ID="txtfecha_CalendarExtender" runat="server" 
                                           TargetControlID="TxtFechaInicio"  Format="dd/MM/yyyy"  />
                                        </td>
                                </tr>
                                <tr>
                                    <td style="width: 654px; text-align: right;">
                                        </td>
                                    <td style="width: 336px">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 654px; height: 23px; text-align: right; color: #0000FF;">
                                        Fecha Final:</td>
                                    <td style="height: 23px; width: 336px">
                                        <asp:TextBox ID="TxtFechaFin" runat="server" ></asp:TextBox>                                         
                                           <cc1:calendarextender ID="Calendarextender1" runat="server" 
                                           TargetControlID="TxtFechaFin"  Format="dd/MM/yyyy"  />
                                        </td>
                                </tr>
                                <tr>
                                    <td style="width: 654px; text-align: right;">
                                        &nbsp;</td>
                                    <td style="width: 336px">                                     
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" colspan="2">
                                        <input id="btnVerProgramaciones" style="width: 264px" type="button" 
                                            value="Ver Requerimientos Programados"  
                                            onclick="return btnVerProgramaciones_onclick()"/></td>
                                </tr>
                                <tr>
                                    <td style="width: 654px; height: 12px;">
                                        </td>
                                    <td style="width: 336px; height: 12px;">
                                        </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: center; color: #0000FF;">
                                        Monto
                                        Total Requerimientos Marcados</td>
                                </tr>
                                <tr>
                                    <td style="width: 654px; height: 31px;">
                                        </td>
                                    <td style="width: 336px; height: 31px;">
                                        <input id="txtmontoTotal" type="text" /></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; height: 30px;" colspan="2">
                                        <input id="btnSumarMarcados" style="width: 266px" type="button" 
                                            onclick="return btnSumarMarcados_onclick()" value="Sumar Marcados" /></td>
                                   <tr> <td style="width: 654px; text-align: right">
                              <input id="chkMarcar" type="checkbox" onchange="return MarcaroDesmarcar()" /></td><td>
                                           Habilitar Cambiar Fechas de Pago de Programacion</td>
                                    </tr>
                                 
                                </table>
                                </td>
                                </tr>
                            </table>
                           
                          </td>
                          <td>
                          <div id="capaAgregarPago" style="width:450px;border:1px solid blue;background-color:White;z-index:99999">
        
            <table width="100%">
                <tr>
                    <td colspan="2" class="TituloCelda">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="Texto">Nro Constancia:</td>
                    <td>
                        <input id="txtNroOperacion" type="text" /></td>
                </tr>
                <tr>
                    <td class="Texto">Monto Depósito:</td>
                    <td>
                        <input id="txtMontoDeposito" type="text" /></td>
                </tr>
                <tr>
                    <td class="Texto">Banco:</td>
                    <td>
                        <select id="cboBanco" name="D1" style="width: 343px">
                            <option></option>
                        </select></td>
                </tr>
                <tr>
                    <td class="Texto">Fecha de Pago:</td>
                    <td>
                    <asp:TextBox ID="txtFechaPago" runat="server" Text=""></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtFechaPago">
                        </cc1:CalendarExtender>
                    </td>
                </tr>
                <tr runat="server" id="tr_PersonaRendir">
                    <td class="Texto">Persona a Rendir:</td>
                    <td>
                          <asp:TextBox ID="txtCodigoEmpleado" runat="server" Text="" Enabled="false" Width="100px"></asp:TextBox>
                          <asp:TextBox ID="txtFiltroEmpleados" runat="server" Text=""></asp:TextBox>
                          <asp:Button ID="btnFiltroEmpleados" runat="server" Text="Buscar" CssClass="btnBuscar"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="gvFiltroEmpleados" runat="server" AutoGenerateColumns="false">
                            <RowStyle CssClass="GrillaRow" />
                            <HeaderStyle CssClass="GrillaHeader" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbSelecEmpleado" runat="server" Text="Seleccioonar"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="IdPersona" HeaderText="Id" NullDisplayText="---" />
                                <asp:BoundField DataField="Nombre" HeaderText="Ap. y Nombres" NullDisplayText="---" />
                                <asp:BoundField DataField="DNI" HeaderText="D.N.I." NullDisplayText="---" />
                            </Columns>

                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <%--<asp:Button ID="btnGuardarCancelacion" runat="server" Text="Guardar Cancelación"  CssClass="btnGuardar" />
                        <asp:Button ID="btnRetroceder" runat="server" Text="Salir..." />--%>
                        <input id="btnGuardarCancelacion" style="width: 202px" type="button" 
                            value="Guardar Cancelacion Masiva"  onclick="return btnGuardarCancelacion_onclick(this)"/>
                              <span id="spnMensajeFiltroNroDoc" style="color:Red;font-weight:bold"></span>
                            <asp:DropDownList 
                            ID="ddlBancoCancelacion" runat="server" Height="16px" Visible="False" 
                            Width="16px"></asp:DropDownList>
                        </td>
                </tr>
            </table>
                         
                           
                     
                        </tr>
                        
                        
                            </div>
                        </td>
                        
                 
            
       
    </div>
                      
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 15px">
             <div id="CapaCambioFechaPago" 
                                style="width:450px;border:1px solid blue;background-color:White;z-index:99999; height: 50px; display: none;">
                            <table class="style1" style="background-color: #99FF66">
                                <tr>
                                    <td style="width: 180px">
                                        <strong>Fecha Reprogramacion :</strong></td>
                                    <td style="width: 128px">
                                        <asp:TextBox ID="TxtFechaReprogramacion" runat="server" 
                                            style="margin-left: 0px"></asp:TextBox>
                                        <cc1:CalendarExtender ID="txtTxtFechaReprogramacion_CalendarExtender" 
                                            runat="server" Format="dd/MM/yyyy" TargetControlID="TxtFechaReprogramacion" />
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="color: #006600; width: 180px">
                                        &nbsp;</td>
                                    <td style="width: 128px">
                                      
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="width: 180px">
                                        &nbsp;</td>
                                    <td style="width: 128px">
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </div>
                </td>
        </tr>
        <tr>
            <td>
                        <input id="txtNroVoucher" type="text" 
                    style="color: #FF00FF; font-size: small; font-weight: 700" /></td>
        </tr>
        <tr>
            <td>
               <div id="GrillaRequerimientos">
                   
</div></td>
        </tr>
    </table>
 
    <asp:HiddenField ID="hddIdDocumentoCancelacion" runat="server" />
 
 <asp:HiddenField ID="hddMontoMarcados" runat="server" />
    <asp:HiddenField ID="hddIdFormulario" runat="server" />
 <script type="text/javascript" language="javascript">

     window.onload = varios();

     function varios() {
         ConsultaCombos();
     }

     function btnVerProgramaciones_onclick() {

          BusquedaRequerimientosProgramados()

      }
      function MarcaroDesmarcar() {

          reprogramando();

      }

      function reprogramando(objeto, flag) {
          var chkMarcarD = document.getElementById("chkMarcar");

          if (chkMarcarD.checked == false) {
              CapaCambioFechaPago.style.display = 'none';

          }
          if (chkMarcarD.checked == true) {
              CapaCambioFechaPago.style.display = 'block';
          }

      }

      function btnSumarMarcados_onclick() {
          
          ObtenerMontoProgramado()
      }
     
      function btnGuardarCancelacion_onclick(objeto) {

          var txtNroOperacion = document.getElementById("txtNroOperacion").value;
          var txtMontoDeposito = document.getElementById("txtMontoDeposito").value; 

          if (txtNroOperacion == "") {
              return false;
          }
          if (txtMontoDeposito == "") {
              return false;
          }
      
         

         GuardarCancelacionMasiva(objeto)
     }

     function mostrarListasCombos(rptac) {
         
         if (rptac != "") {
             var data = rptac.split("Æ");
             crearComboBanco(data, "cboTienda", "|");
             ConsultarRegistros();
         }
         return false;

     }


     function mostrarListasCombos(rptacombo) {
         if (rptacombo != "") {
             var listas2 = rptacombo;
             data1 = crearArray(listas2, "▼");
             listarcombos();
         }
         return false;
     }

     function listarcombos() {
         var TipoServicio = [];
         var nRegistros = data1.length;
         var campos2;
         var TipoServ;
         for (var i = 0; i < nRegistros; i++) {
             campos2 = data1[i].split("|");
             {
                 TipoServ = campos2[0];
                 TipoServ += "|";
                 TipoServ += campos2[1];
                 TipoServicio.push(TipoServicio);
             }
         }
         crearCombo(data1, "cboBanco", "|");
     }


     function crearCombo(lista, nombreCombo, separador, nombreItem, valorItem) {
         var contenido = "";
         if (nombreItem != null && valorItem != null) {
             contenido += "<option value='";
             contenido += valorItem;
             contenido += "'>";
             contenido += nombreItem;
             contenido += "</option>";
         }
         var nRegistros = lista.length;
         if (nRegistros > 0) {
             var campos;
             for (var i = 0; i < nRegistros; i++) {
                 campos = lista[i].split(separador);
                 contenido += "<option value='";
                 contenido += campos[0];
                 contenido += "'>";
                 contenido += campos[1];
                 contenido += "</option>";
             }
         }
         var cbo = document.getElementById(nombreCombo);
         if (cbo != null) cbo.innerHTML = contenido;

     }


     function ConsultaCombos() {
         var opcion = "llenarCombo";
         var IdEstadoc = 1;

         var datacombob = new FormData();
         datacombob.append('flag', 'llenarCombo');
         datacombob.append('IdEstadoc', IdEstadoc);


         var xhrrep = new XMLHttpRequest();
         xhrrep.open("POST", "FrmProgramacionPago_CXP_Masivos.aspx?flag=" + opcion + "&IdEstadoc=" + IdEstadoc, true);
         xhrrep.onreadystatechange = function () {
             if (xhrrep.readyState == 4 && xhrrep.status == 200) {
                 mostrarListasCombos(xhrrep.responseText);
             }
         }

         xhrrep.send(datacombob);
         return false;
        
     }


     function crearArray(lista, separador) {
         var data = lista.split(separador);
         if (data.length === 1 && data[0] === "") data = [];
         return data;
     }

     function GuardarCancelacionMasiva(objeto) {

         var opcion = "GuardarCancelacionMasiva";
         var IdEstado = null;

         var FechaInicio = document.getElementById('<%= TxtFechaInicio.ClientID%>').value;
         var FechaFin = document.getElementById('<%= TxtFechaFin.ClientID%>').value;
         var idBancoCanc = document.getElementById("cboBanco").value;
         var nrooperacion = document.getElementById("txtNroOperacion").value;
         var montocancelado = document.getElementById("txtMontoDeposito").value;
         var FechaCancelacion = document.getElementById('<%= txtFechaPago.ClientID%>').value;



         var datarep = new FormData();
         datarep.append('flag', 'GuardarCancelacionMasiva');
         datarep.append('FechaInicio', FechaInicio);
         datarep.append('FechaFin', FechaFin);
         datarep.append('idBancoCanc', idBancoCanc);
         datarep.append('nrooperacion', nrooperacion);
         datarep.append('montocancelado', montocancelado);
         datarep.append('FechaCancelacion', FechaCancelacion);
         
         var xhrrep = new XMLHttpRequest();
         xhrrep.open("POST", "FrmProgramacionPago_CXP_Masivos.aspx?flag=" + opcion + "&FechaInicio=" + FechaInicio + "&FechaFin=" + FechaFin + "&idBancoCanc= " + idBancoCanc + "&nrooperacion= " + nrooperacion + "&montocancelado= " + montocancelado + "&FechaCancelacion= " + FechaCancelacion, true);
        
         xhrrep.onloadstart = function () { objeto.disabled = true; objeto.value = "Procesando..."; }
         xhrrep.onloadend = function () { objeto.disabled = false; objeto.value = "Guardando"; }

         xhrrep.onreadystatechange = function () {
             if (xhrrep.readyState == 4 && xhrrep.status == 200) {

                 document.getElementById("txtMontoDeposito").value = "";
                 document.getElementById("txtNroOperacion").value = "";
                 document.getElementById("txtmontoTotal").value = "0";
                 document.getElementById("txtNroVoucher").value = xhrrep.responseText;
                 BusquedaRequerimientosProgramadosrefresh();

             }
         }
         xhrrep.send(datarep);
         return false;

     }


      function ObtenerMontoProgramado(objeto, flag) {

          var opcion = "ObtenerMontoProgramado";
          var IdEstado = null;
          var FechaInicio = document.getElementById('<%= TxtFechaInicio.ClientID%>').value;
          var FechaFin = document.getElementById('<%= TxtFechaFin.ClientID%>').value;
          var datarep = new FormData();
          datarep.append('flag', 'ObtenerMontoProgramado');
          datarep.append('FechaInicio', FechaInicio);
          datarep.append('FechaFin', FechaFin);
          var xhrrep = new XMLHttpRequest();
          xhrrep.open("POST", "FrmProgramacionPago_CXP_Masivos.aspx?flag=" + opcion + "&FechaInicio=" + FechaInicio + "&FechaFin=" + FechaFin, true);
          xhrrep.onreadystatechange = function () {
              if (xhrrep.readyState == 4 && xhrrep.status == 200) {

                  var montototal = (xhrrep.responseText) 
                      document.getElementById("txtmontoTotal").value = montototal;

              }
          }
          xhrrep.send(datarep);
          return false;
      }


     function BusquedaRequerimientosProgramados(objeto, flag) {

         var opcion = "BuscarRequerimientosProgramados";
         var IdEstado = null;
         var FechaInicio = document.getElementById('<%= TxtFechaInicio.ClientID%>').value;
         var FechaFin = document.getElementById('<%= TxtFechaFin.ClientID%>').value;
         var datarep = new FormData();
         datarep.append('flag', 'BuscarRequerimientosProgramados');
         datarep.append('FechaInicio', FechaInicio);
         datarep.append('FechaFin', FechaFin);
         var xhrrep = new XMLHttpRequest();
         xhrrep.open("POST", "FrmProgramacionPago_CXP_Masivos.aspx?flag=" + opcion + "&FechaInicio=" + FechaInicio + "&FechaFin=" + FechaFin, true);
         xhrrep.onreadystatechange = function () {
             if (xhrrep.readyState == 4 && xhrrep.status == 200) {
                 fun_lista(xhrrep.responseText);

                      document.getElementById("txtNroVoucher").value = "";
                      document.getElementById("txtmontoTotal").value = 0;
             }
         }
         xhrrep.send(datarep);
         return false;
     }

     function BusquedaRequerimientosProgramadosrefresh(objeto, flag) {

         var opcion = "BuscarRequerimientosProgramados";
         var IdEstado = null;
         var FechaInicio = document.getElementById('<%= TxtFechaInicio.ClientID%>').value;
         var FechaFin = document.getElementById('<%= TxtFechaFin.ClientID%>').value;
         var datarep = new FormData();
         datarep.append('flag', 'BuscarRequerimientosProgramados');
         datarep.append('FechaInicio', FechaInicio);
         datarep.append('FechaFin', FechaFin);
         var xhrrep = new XMLHttpRequest();
         xhrrep.open("POST", "FrmProgramacionPago_CXP_Masivos.aspx?flag=" + opcion + "&FechaInicio=" + FechaInicio + "&FechaFin=" + FechaFin, true);
         xhrrep.onreadystatechange = function () {
             if (xhrrep.readyState == 4 && xhrrep.status == 200) {
                 fun_lista(xhrrep.responseText);

             }
         }
         xhrrep.send(datarep);
         return false;
     }


     function verificar(iddocumento) {

         var chkMarcarD = document.getElementById("chkMarcar");

         if (chkMarcarD.checked == false) {
           
             actualizando(iddocumento);

         }
         if (chkMarcarD.checked == true) {
             actualizandofecha(iddocumento);
         }
     }

     function actualizandofecha(iddocumento) {

         var opcion = "UpdateProgramacionFecha";
         var IdEstado = null;
         var FechaInicio = document.getElementById('<%= TxtFechaInicio.ClientID%>').value;
         var FechaFin = document.getElementById('<%= TxtFechaFin.ClientID%>').value;
         var FechaCambio = document.getElementById('<%= TxtFechaReprogramacion.ClientID%>').value;
         var iddoc = iddocumento
         var datarepact = new FormData();
         datarepact.append('flag', 'UpdateProgramacionFecha');

         datarepact.append('FechaInicio', FechaInicio);
         datarepact.append('FechaFin', FechaFin);
         datarepact.append('FechaCambio', FechaCambio);
         datarepact.append('iddoc', iddoc);
         var xhrrepact = new XMLHttpRequest();
         xhrrepact.open("POST", "FrmProgramacionPago_CXP_Masivos.aspx?flag=" + opcion + "&FechaInicio=" + FechaInicio + "&FechaFin=" + FechaFin + "&FechaCambio=" + FechaCambio + "&iddoc=" + iddoc, true);
         xhrrepact.onreadystatechange = function () {
             if (xhrrepact.readyState == 4 && xhrrepact.status == 200) {
                 fun_lista(xhrrepact.responseText);
             }
         }
         xhrrepact.send(datarepact);
         return false;
     }



     function actualizando(iddocumento) {

         var opcion = "UpdateRequerimiento";
         var IdEstado = null;
         var FechaInicio = document.getElementById('<%= TxtFechaInicio.ClientID%>').value;
         var FechaFin = document.getElementById('<%= TxtFechaFin.ClientID%>').value;
         var iddoc = iddocumento
         var datarep = new FormData();
         datarep.append('flag', 'BuscarRequerimientosProgramados');

         datarep.append('FechaInicio', FechaInicio);
         datarep.append('FechaFin', FechaFin);
         datarep.append('iddoc', iddoc);
         var xhrrep = new XMLHttpRequest();
         xhrrep.open("POST", "FrmProgramacionPago_CXP_Masivos.aspx?flag=" + opcion + "&FechaInicio=" + FechaInicio + "&FechaFin=" + FechaFin + "&iddoc=" + iddoc, true);
         xhrrep.onreadystatechange = function () {
             if (xhrrep.readyState == 4 && xhrrep.status == 200) {
                 fun_lista(xhrrep.responseText);
                
             }
         }
         xhrrep.send(datarep);
         return false;
     }

     function fun_lista(lista) {
         filas = lista.split("▼");
         crearTabla();
         crearMatriz();
         mostrarMatriz();
         configurarFiltros();
     }

     function crearTabla() {
         var nRegistros = filas.length;
         var cabeceras = ["Marcar", "NroDocumento", "Proveedor", "FechaPago", "Simbolo", "mcp_Monto", "mcp_Saldo", "doc_TotalApagar", "doc_FechaVenc"];
         var nCabeceras = cabeceras.length;
         var contenido = "<table><thead>";
         contenido += "<tr class='GrillaHeader'>";
         for (var j = 0; j < nCabeceras; j++) {
             contenido += "<th>";
             contenido += "<input type='text' class='texto' style='width:95%' placeholder='" + cabeceras[j] + "'/>";
             contenido += "</th>";
         }
         contenido += "</tr></thead><tbody id='tbDocumentos' class='GrillaRow'></tbody>";
         contenido += "</table>";
         var div = document.getElementById("GrillaRequerimientos");
         div.innerHTML = contenido;
     }

     function crearMatriz(){
         matriz = [];
         var nRegistros = filas.length;
         var nCampos;
         var campos;
         var c = 0;
         var exito;
         var textos = document.getElementsByClassName("texto");
         var nTextos = textos.length;
         var texto;
         var x;
         for (var i = 0; i < nRegistros; i++) {
             exito = true;
             campos = filas[i].split("|");
             nCampos = campos.length;
             for (var j = 0; j < nTextos; j++) {
                 texto = textos[j];
                 x = j;

                 if (texto.value.length > 0) 
                    {
                       exito = campos[x].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                    }


                 if (!exito) break;
             }
             if (exito) {
                 matriz[c] = [];
                 for (var j = 0; j < nCampos; j++) {
                     if (isNaN(campos[j])) matriz[c][j] = campos[j];

                     else matriz[c][j] = campos[j];
                 }
                 c++;
             }
         }
     }

     function configurarFiltros() {
         var textos = document.getElementsByClassName("texto");
         var nTextos = textos.length;
         var texto;
         for (var j = 0; j < nTextos; j++) {
             texto = textos[j];
             texto.onkeyup = function () {
                 crearMatriz();
                 mostrarMatriz();
             }
         }
     }



     function mostrarMatriz() {
         var nRegistros = matriz.length;
         var contenido = "";
         if (nRegistros > 0) {
             var nCampos = matriz[0].length;
             for (var i = 0; i < nRegistros; i++) {
                 
                 if (matriz[i][0] == 0) {
                     contenido += "<tr>";
                 }
                 if (matriz[i][0] != 0) {
                     contenido += "<tr bgcolor='#00FF99'>";
                 }
                 // ncampos -5
                 for (var j = 0; j < nCampos ; j++) {
                     var cadenaid
                     cadenaid = '' + matriz[i][13] + '';
                     contenido += "<td>";
                     if (j == 0) {
                         if (matriz[i][0] == 0) {
                             contenido += "<input type='checkbox' value='checkbox'  onchange='return verificar(" + '"' + cadenaid + '"' + ");' />"
                         }
                         if (matriz[i][0] == 1) {
                             contenido += "<input type='checkbox' value='checkbox' checked = true onchange='return verificar(" + '"' + cadenaid + '"' + ");'/>"
                         }

                     }

                     if (j > 0)
                        {
                         contenido += matriz[i][j];
                        }
                     contenido += "</td>";
                        
                 }
                 contenido += "</tr>";
             }
         }
         var spnMensaje = document.getElementById("nroFilas");
         var tabla = document.getElementById("tbDocumentos");
         tabla.innerHTML = contenido;
     }




  
</script>

</asp:Content>
