<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmComprobanteRetencion.aspx.vb" Inherits="APPWEB.FrmComprobanteRetencion" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" runat="server" OnClientClick="return(   valOnClickNuevo()  );"
                                Text="Nuevo" ToolTip="Nuevo" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnEditar" runat="server" OnClientClick="return(  valOnClickEditar()  );"
                                Text="Editar" ToolTip="Editar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" OnClientClick="return(  valOnClickBuscar()  );"
                                Text="Buscar" ToolTip="Buscar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnAnular" runat="server" OnClientClick="return(  valOnClickAnular()   );"
                                Text="Anular" ToolTip="Anular" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" runat="server" OnClientClick="return( valSaveDocumento()  );"
                                Text="Guardar" ToolTip="Guardar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimir" runat="server" OnClientClick="return(  print()  );"
                                Text="Imprimir" ToolTip="Imprimir Documento" Width="80px" />
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td align="right" style="width: 100%">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                COMPROBANTE DE RETENCI�N
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cab" runat="server">
                    <table>
                        <tr>
                            <td align="right" class="Texto">
                                Empresa:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Tienda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Serie:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboSerie" runat="server" Width="100%" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarDocumentoxCodigo">                                
                                <asp:TextBox ID="txtCodigoDocumento" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return( valOnKeyPressCodigoDoc(this)  );"
                                    ReadOnly="true" Width="100px"></asp:TextBox>
                                    </asp:Panel>
                            </td>
                            <td>
                                <asp:ImageButton ID="btnBuscarDocumentoxCodigo" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                    ToolTip="Buscar Documento por [ Serie - N�mero ]." OnClientClick="return( valOnClickBuscarDocumentoxCodigo()   );" />
                            </td>
                            <td>
                                <asp:LinkButton ID="btnBusquedaAvanzado" runat="server" CssClass="Texto" Enabled="false"
                                    OnClientClick="return(  valOnClick_btnBusquedaAvanzado()  );" ToolTip="B�squeda Avanzada">Avanzado</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Fecha Emisi�n:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaEmision">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaEmision">
                                </cc1:CalendarExtender>
                            </td>
                            <td align="right" class="Texto">
                                Moneda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboMoneda" runat="server" CssClass="LabelRojo" Font-Bold="true"
                                    Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Estado:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEstado" runat="server" Enabled="false">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Caja:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboCaja" runat="server" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Operaci�n:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboTipoOperacion" Width="100%" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                DATOS DEL CLIENTE
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cliente" runat="server">
                    <table>
                        <tr>
                            <td align="right" class="Texto">
                                Descripci�n:
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtDescripcionPersona" runat="server" CssClass="TextBox_ReadOnlyLeft"
                                    Enabled="true" ReadOnly="true" Width="400px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarPersona" runat="server" OnClientClick="return( valOnClick_btnBuscarPersona()   );"
                                    Text="Buscar" ToolTip="Buscar Cliente" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                D.N.I.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDNI" runat="server" CssClass="TextBox_ReadOnlyLeft" Enabled="true"
                                    ReadOnly="true"></asp:TextBox>
                            </td>
                            <td align="right" class="Texto">
                                R.U.C.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRUC" runat="server" CssClass="TextBox_ReadOnlyLeft" Enabled="true"
                                    ReadOnly="true"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr class="SubTituloCelda">
            <td>
                L�NEA DE CR�DITO
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Panel ID="Panel_LineaCredito" runat="server">
                    <asp:GridView ID="GV_LineaCredito" runat="server" AutoGenerateColumns="false" Width="500px">
                        <Columns>
                            <asp:TemplateField HeaderText="Otorgado" ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:HiddenField ID="hddIdCuenta" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCuentaPersona")%>' />

                                                <asp:HiddenField ID="hddIdMoneda" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />

                                                <asp:Label ID="lblMonedaOtorgado" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>

                                                <asp:Label ID="lblOtorgado" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CargoMaximo","{0:F2}")%>'></asp:Label>

                                                <asp:HiddenField ID="hddIdEmpresa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />
                                </ItemTemplate>
                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Disponible" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblMonedaDisponible" ForeColor="Red" Font-Bold="true" runat="server"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>

                                                <asp:Label ID="lblDisponible" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Saldo","{0:F2}")%>'></asp:Label>

                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                DOCUMENTO DE REFERENCIA
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Detalle" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:Button ID="btnMostrarCapa_BuscarDocRef" OnClientClick="return(  valOnClickAddDocRef_Show()      );"
                                    Width="220px" runat="server" Text="Agregar Documento de Referencia" ToolTip="Agregar Documentos de Referencia." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_Detalle" runat="server" AutoGenerateColumns="false" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Tipo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTipoDocumento_Desc" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomTipoDocumento")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNroDocumento" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Codigo")%>'></asp:Label>
                                                <asp:HiddenField ID="hddIdDocumentoReferencia" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}"
                                            HeaderText="Fec. Emisi�n" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderText="Monto M.N" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblmonedaN" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"monedasimboloN")%>'></asp:Label>
                                                <asp:Label ID="lblmontoMN" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"monedaNacional","{0:F3}")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Monto M.E" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblmonedaE" Font-Bold="true" ForeColor="Red" runat="server"
                                                Text='<%#DataBinder.Eval(Container.DataItem,"monedasimboloE")%>'></asp:Label>
                                                <asp:Label ID="lblmontoME" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"monedaExtranjera","{0:F3}")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Retenci�n" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblmonedaNDetra" Font-Bold="true" ForeColor="Red" runat="server"
                                                Text='<%#DataBinder.Eval(Container.DataItem,"monedaOriginal")%>'></asp:Label>
                                                <asp:Label ID="lblmontoRetenido" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"montoRetenido","{0:F3}")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto" style="font-weight: bold; text-align: right">
                                            Importe Total del Monto Retenido:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMonedaImporteTotal" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                Text="S/."></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtImporteTotal" onFocus="return(  valOnFocus_ImporteTotal_DocRef()   );"
                                                onKeypress="return( false );" Text="0" CssClass="TextBox_ReadOnly" runat="server"
                                                Width="100px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                DATOS DE CANCELACI�N
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_DatosCancelacion" runat="server" Width="100%">
                    <table width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblCondicionPago_DC" CssClass="Texto" Font-Bold="true" runat="server"
                                                Text="Condici�n de Pago:"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboCondicionPago" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_CP_Contado" runat="server" Width="100%">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="lblMedioPago_DC" CssClass="Texto" Font-Bold="true" runat="server"
                                                                Text="Medio de Pago:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboMedioPago" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                                AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <asp:Label ID="lblMonto_DC" CssClass="Texto" Font-Bold="true" runat="server" Text="Monto:"></asp:Label>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:DropDownList ID="cboMoneda_DatoCancelacion" runat="server" Width="100px">
                                                            </asp:DropDownList>
                                                            <asp:TextBox ID="txtMonto_DatoCancelacion" onFocus=" return(  aceptarFoco(this)   ); "
                                                                runat="server" onKeypress="  return(  valOnKeyPress_txtMonto_DatoCancelacion()  ); "
                                                                Width="100px" Text="0"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnAddDatoCancelacion" runat="server" Text="Agregar" Width="70px"
                                                                OnClientClick=" return(   valOnClick_btnAddDatoCancelacion()  ); " />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnVer_NotaCredito" runat="server" Text="Buscar Nota Cr�dito" Width="145px"
                                                                OnClientClick=" return(   valOnClick_btnVer_NotaCredito()  ); " />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="lblPost_DC" CssClass="Texto" Font-Bold="true" runat="server" Text="Tipo Tarjeta:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboPost_DC" runat="server" Width="100%" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <asp:Label ID="lblTarjeta_DC" CssClass="Texto" Font-Bold="true" runat="server" Text="Tarjeta:"></asp:Label>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:DropDownList ID="cboTarjeta_DC" runat="server" Width="100%">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="lblBanco_DC" CssClass="Texto" Font-Bold="true" runat="server" Text="Banco:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboBanco" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="lblCuentaBancaria_DC" CssClass="Texto" Font-Bold="true" runat="server"
                                                                Text="Cuenta:"></asp:Label>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:DropDownList ID="cboCuentaBancaria" runat="server" Width="100%">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="lblNro_DC" runat="server" CssClass="Texto" Font-Bold="true" Text="N�mero:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtNro_DC" runat="server" onFocus=" return(  aceptarFoco(this)   ); "
                                                                onKeypress=" return(    valOnKeyPress_txtNro_DC()    ); " Width="90%"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="lblFechaACobrar_DC" CssClass="Texto" Font-Bold="true" runat="server"
                                                                Text="Fecha a Cobrar:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtFechaACobrar" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                                                                Width="100px"></asp:TextBox>
                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender6" runat="server" ClearMaskOnLostFocus="false"
                                                                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                                CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaACobrar">
                                                            </cc1:MaskedEditExtender>
                                                            <cc1:CalendarExtender ID="CalendarExtender5" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                                TargetControlID="txtFechaACobrar">
                                                            </cc1:CalendarExtender>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="GV_Cancelacion" runat="server" AutoGenerateColumns="false" Width="100%">
                                                    <Columns>
                                                        <asp:CommandField ButtonType="Link" SelectText="Quitar" ItemStyle-HorizontalAlign="Center"
                                                            ShowSelectButton="true" ItemStyle-Width="75px" />
                                                        <asp:TemplateField HeaderText="Monto" ItemStyle-Width="120px" HeaderStyle-Height="25px"
                                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblMonedaMonto" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMonedaDestino")%>'></asp:Label>

                                                                            <asp:Label ID="lblMonto" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MontoEquivalenteDestino","{0:F2}")%>'></asp:Label>

                                                                            <asp:HiddenField ID="hddIdMedioPago" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMedioPago")%>' />

                                                                            <asp:HiddenField ID="hddIdMonedaOrigen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="descripcionPagoCaja" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderText="Descripci�n" />
                                                    </Columns>
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td class="Texto" align="right">
                                                            Total Recibido:
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMonedaTotalRecibido" Text="-" runat="server" CssClass="LabelRojo"
                                                                            Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtTotalRecibido" onKeypress="return(  false  );" onFocus="return(  valFocusCancelacion() );"
                                                                            Width="90px" Text="0" CssClass="TextBox_ReadOnly" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Texto" align="right">
                                                            Faltante:
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMonedaFaltante" Text="-" runat="server" CssClass="LabelRojo" Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtFaltante" onKeypress="return(  false  );" onFocus="return(  valFocusCancelacion() );"
                                                                            Width="90px" Text="0" CssClass="TextBox_ReadOnly" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Texto" align="right">
                                                            Vuelto:
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMonedaVuelto" Text="-" runat="server" CssClass="LabelRojo" Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtVuelto" onKeypress="return(  false  );" onFocus="return(  valFocusCancelacion() );"
                                                                            Width="90px" Text="0" CssClass="TextBox_ReadOnly" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_CP_Credito" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Medio de Pago:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboMedioPago_Credito" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="Texto" style="font-weight: bold">
                                                N� Dias:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtnrodias" runat="server" MaxLength="3" onKeyPress=" return(   validarNumeroPuntoPositivo('event')   );   "
                                                    Width="40px" onFocus="return ( aceptarFoco(this)  );" onKeyUp=" return(   validarNrodias()   ); "></asp:TextBox>
                                            </td>
                                            <td class="Texto" style="font-weight: bold">
                                                Fecha de Vcto.:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaVcto" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="txtFechaEmision0_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaVcto">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="txtFechaVcto_CalendarExtender" runat="server" Enabled="True"
                                                    Format="dd/MM/yyyy" TargetControlID="txtFechaVcto">
                                                </cc1:CalendarExtender>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                OBSERVACIONES
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Obs" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtObservaciones" TextMode="MultiLine" Width="100%" runat="server"
                                    onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                    Height="100px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
                <asp:HiddenField ID="hddIdPersona" runat="server" />
                <asp:HiddenField ID="hddIdTipoDocumento" runat="server" Value="2102866602" />
                <asp:HiddenField ID="hddFrmModo" runat="server" Value="0" />
                <asp:HiddenField ID="hddIdDocumento" runat="server" />
                <asp:HiddenField ID="hddIdCuentaPersona" runat="server" />
                <asp:HiddenField ID="hddCodigoDocumento" runat="server" />
                <asp:HiddenField ID="hddContAbonos" runat="server" />
                <asp:HiddenField ID="hddIdMedioPagoPrincipal" runat="server" />
                <asp:HiddenField ID="hddIdMedioPagoInterfaz" runat="server" />
                <asp:HiddenField ID="hddPermiso_AddMedioPago" runat="server" Value="0" />
            </td>
        </tr>
    </table>
    <div id="capaDocumentosReferencia" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 261px; left: 42px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaDocumentosReferencia" runat="server" ToolTip="Cerrar"
                        ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaDocumentosReferencia')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_DocRef_Find" runat="server" ScrollBars="Horizontal" Width="100%">
                        <asp:GridView ID="GV_DocumentosReferencia_Find" runat="server" AutoGenerateColumns="false"
                            Width="100%">
                            <Columns>
                                <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" HeaderStyle-Height="25px" />
                                <asp:TemplateField HeaderText="Tipo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblTipoDocumento_CodSunat_Find" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"getTipoDocumento_CodSunat")%>'></asp:Label>

                                                    <asp:Label ID="lblTipoDocumento_Desc_Find" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"getTipoDocumento_DescripcionCorto")%>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                    <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="getFechaEmisionText" DataFormatString="{0:dd/MM/yyyy}"
                                    HeaderText="Fec. Emisi�n" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Precio Venta" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblMonedaMontoTotal_Find" Font-Bold="true" ForeColor="Red" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                    <asp:Label ID="lblMontoTotal_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F2}")%>'></asp:Label>

                                                    <asp:HiddenField ID="hddIdMonedaOriginal_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="TipoCambio" DataFormatString="{0:F4}" HeaderText="Tipo Cambio"
                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="PorcentPercepcion" DataFormatString="{0:F4}" HeaderText="Percepci�n (%)"
                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Importe Percepci�n" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblMonedaPercepcion_Find" Font-Bold="true" ForeColor="Red" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"MonedaDestino")%>'></asp:Label>

                                                    <asp:Label ID="lblPercepcion_Find" Font-Bold="true" ForeColor="Red" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"Percepcion","{0:F2}")%>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Importe Total" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblMonedaImporteTotal_Find" Font-Bold="true" ForeColor="Red" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"MonedaDestino")%>'></asp:Label>

                                                    <asp:Label ID="lblImporteTotal_Find" Font-Bold="true" ForeColor="Red" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"ImporteTotal","{0:F2}")%>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Selected="True" Value="J">Juridica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razon Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this));" runat="server"
                                                                            onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                                            Width="450px"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel3" runat="server" DefaultButton="btBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero());" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel4" runat="server" DefaultButton="btBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero());"
                                                                            MaxLength="11"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Rol:
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="cboRol" runat="server" Width="100%">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaDocumento_NotaCredito" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; background-color: white; z-index: 2;
        display: none; top: 480px; left: 25px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton8" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaDocumento_NotaCredito')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_NotaCredito" runat="server" ScrollBars="Horizontal" Width="100%">
                        <asp:GridView ID="GV_NotaCredito" runat="server" AutoGenerateColumns="false" Width="100%">
                            <Columns>
                                <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="50px" HeaderStyle-Height="25px" />
                                <asp:TemplateField HeaderText="Monto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblMonedaMontoRecibir_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                    <asp:TextBox ID="txtMontoRecibir_Find" Width="70px" Font-Bold="true" onKeypress="return(  validarNumeroPunto(event)  );"
                                                        onblur="return(  valBlur(event)   );" onFocus="return(   aceptarFoco(this)   );" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"ImporteTotal","{0:F2}")%>'></asp:TextBox>
 
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Tipo" />
                                <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"Numero")%>'></asp:Label>

                                                    <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />

                                                    <asp:HiddenField ID="hddIdMonedaDocRef_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />

                                                    <asp:HiddenField ID="hddIdEstadoCancelacion_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEstadoCancelacion")%>' />

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisi�n"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="FechaVenc" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Vcto."
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="Contador" DataFormatString="{0:F0}" HeaderText="Vigencia (D�as)"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Monto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblMonedaMonto_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                    <asp:Label ID="lblMonto_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F2}")%>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Saldo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>

                                                    <asp:Label ID="lblMonedaSaldo_Find" ForeColor="Red" runat="server" Font-Bold="true"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                    <asp:Label ID="lblSaldo_Find" runat="server" ForeColor="Red" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"ImporteTotal","{0:F2}")%>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="NomPropietario" HeaderText="Empresa" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomTienda" HeaderText="Tienda" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaBusquedaAvanzado" style="border: 3px solid blue; padding: 8px; width: 850px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 142px; left: 22px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaBusquedaAvanzado')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BusquedaAvanzado" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaI" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaI">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaI">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaF" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaF">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaF">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBusquedaAvanzado" Width="70px" ToolTip="Buscar Documentos"
                                                    runat="server" Text="Buscar" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_BusquedaAvanzado" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50px" HeaderStyle-Height="25px">
                                <HeaderStyle Height="25px" Width="50px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:CommandField>
                            <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Tipo"></asp:BoundField>
                            <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblNroDocumento_BusquedaAvanzado" ForeColor="Red" Font-Bold="true"
                                                    runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                <asp:HiddenField ID="hddIdDocumento_BusquedaAvanzado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisi�n"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="DescripcionPersona" HeaderText="Destinatario" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td class="LabelRojo" style="font-weight: bold">
                    *** El proceso de b�squeda realiza un filtrado de acuerdo al destinatario seleccionado.
                </td>
            </tr>
        </table>
    </div>
    <div id="capaPermiso_AddMedioPago" style="border: 3px solid blue; padding: 8px; width: 350px;
        height: auto; position: absolute; top: 350px; left: 250px; background-color: white;
        z-index: 3; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton12" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaPermiso_AddMedioPago')   );" />
                </td>
            </tr>
            <tr>
                <td class="TituloCelda">
                    Autorizaci�n - Medio de Pago
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Usuario:
                            </td>
                            <td>
                                <asp:TextBox ID="txtUsuario_AddMedioPago" Width="150px" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Contrase�a:
                            </td>
                            <td>
                                <asp:TextBox ID="txtClave_AddMedioPago" Width="150px" runat="server" TextMode="Password"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Contrase�a:
                            </td>
                            <td>
                                <asp:TextBox ID="txtClave2_AddMedioPago" Width="150px" runat="server" TextMode="Password"> </asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnAceptar_AddMedioPago" OnClientClick=" return(  valOnClick_btnAceptar_AddMedioPago() ); "
                                    runat="server" Text="Aceptar" Width="80px" />
                            </td>
                            <td>
                                <asp:Button ID="btnCerrar_AddMedioPago" runat="server" Text="Cerrar" Width="80px"
                                    OnClientClick="return(    offCapa('capaPermiso_AddMedioPago')       );" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">

        //***************************************************** BUSQUEDA PERSONA

        function validarRetencion() {
            var grilla = document.getElementById('<%=GV_Detalle.ClientID %>');
            var textboxMontoREtenido = document.getElementById('<%=txtImporteTotal.ClientID %>');
            var txtCanclacion = document.getElementById('<%=txtMonto_DatoCancelacion.ClientID %>')
            var montoRetenido = 0;
            for (var i = 1; i < grilla.rows.length; i++) {
                montoRetenido = montoRetenido + parseFloat(grilla.rows[i].cells[5].children[1].innerHTML);
            }
            textboxMontoREtenido.value = Format(montoRetenido,2);
            txtCanclacion.value = Format(montoRetenido,2);
        }

        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(obj) {
            var key = event.keyCode;
            if (key == 13) {
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }

        //******* FIN 


        function valOnFocus_ImporteTotal_DocRef() {
            document.getElementById('<%=btnMostrarCapa_BuscarDocRef.ClientID%>').focus();
            return false;
        }
        function valOnClickAddDocRef() {
            var grilla = document.getElementById('<%=GV_DocumentosReferencia_Find.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    var chb = rowElem.cells[0].children[0];
                    if (chb.status == true) {
                        return true;
                    }
                }
            }
            offCapa('capaDocumentosReferencia');
            return false;
        }

        function valOnClickSelectDocRef(chbSelectDocRef) {
            //********* VALIDAMOS LA SELECCION DEL DOC REFERENCIA NO ESTE REPETIDO
            var grilla = document.getElementById('<%=GV_DocumentosReferencia_Find.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    var chb = rowElem.cells[0].children[0];
                    if (chb.id == chbSelectDocRef.id) {

                        //**** Obtenemos el IdDocumento a agregar
                        var hddIdDocumentoRef_Find = rowElem.cells[2].children[1];

                        var grilla_DocRef = document.getElementById('<%=GV_Detalle.ClientID%>');
                        if (grilla_DocRef != null) {
                            for (var j = 1; j < grilla_DocRef.rows.length; j++) {//comienzo en 1 xq 0 es la cabecera
                                var rowElem_DocRef = grilla_DocRef.rows[j];

                                //**** Obtenemos el IdDocumento a�adido
                                var hddIdDocumentoRef = rowElem_DocRef.cells[2].children[0].cells[1].children[0];
                                if (parseInt(hddIdDocumentoRef_Find.value) == parseInt(hddIdDocumentoRef.value)) {
                                    alert('Este Documento ya ha sido agregado como Documento de Referencia.');
                                    return false;
                                }
                            }
                        }
                        return true;
                    }
                }
            }
            return true;
        }

        function valOnClickAddDocRef_Show() {
            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('DEBE SELECCIONAR UN CLIENTE. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            return true;
        }


        function calcularTotalDocRef() {
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            var totalDocRef = 0;
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    totalDocRef = totalDocRef + parseFloat(rowElem.cells[7].children[1].innerHTML);
                }
            }
            document.getElementById('<%=txtImporteTotal.ClientID%>').value = Format(totalDocRef, 2);
            return false;
        }
        function calcularTotales() {
            calcularTotalDocRef();
            calcularDatosCancelacion();
            return false;
        }

        function valFocusCancelacion() {
            document.getElementById('<%=cboCondicionPago.ClientID%>').focus();
            return false;
        }
        function valSelectBanco_AddMedioPago() {
            var cboMedioPago = document.getElementById('<%=cboMedioPago.ClientID%>');
            switch (parseInt(cboMedioPago.value)) {
                case 1:  //*** Efectivo
                    alert('El Medio de Pago seleccionado [ ' + getCampoxValorCombo(cboMedioPago, cboMedioPago.value) + ' ] no utiliza la opci�n [ Banco ]');
                    return false;
                    break;
                case 3:  //*** Transferencia
                    break;
                case 7:  //*** Cheque
                    break;
                case 8:  //*** Deposito
                    break;
            }
            return true;
        }
        function valSelectNroCuenta_AddMedioPago() {
            var cboMedioPago = document.getElementById('<%=cboMedioPago.ClientID%>');
            switch (parseInt(cboMedioPago.value)) {
                case 1:  //*** Efectivo
                    alert('El Medio de Pago seleccionado [ ' + getCampoxValorCombo(cboMedioPago, cboMedioPago.value) + ' ] no utiliza la opci�n [ Nro. Cuenta ]');
                    return false;
                    break;
                case 3:  //*** Transferencia
                    break;
                case 7:  //*** Cheque
                    alert('El Medio de Pago seleccionado [ ' + getCampoxValorCombo(cboMedioPago, cboMedioPago.value) + ' ] no utiliza la opci�n [ Nro. Cuenta ]');
                    return false;
                    break;
                case 8:  //*** Deposito
                    break;
            }
            return true;
        }

        function valSelectNroCheque_AddMedioPago() {
            var cboMedioPago = document.getElementById('<%=cboMedioPago.ClientID%>');
            switch (parseInt(cboMedioPago.value)) {
                case 1:  //*** Efectivo
                    alert('El Medio de Pago seleccionado [ ' + getCampoxValorCombo(cboMedioPago, cboMedioPago.value) + ' ] no utiliza la opci�n [ Nro. Cheque ]');
                    return false;
                    break;
                case 3:  //*** Transferencia
                    alert('El Medio de Pago seleccionado [ ' + getCampoxValorCombo(cboMedioPago, cboMedioPago.value) + ' ] no utiliza la opci�n [ Nro. Cheque ]');
                    return false;
                    break;
                case 7:  //*** Cheque                                
                    break;
                case 8:  //*** Deposito
                    alert('El Medio de Pago seleccionado [ ' + getCampoxValorCombo(cboMedioPago, cboMedioPago.value) + ' ] no utiliza la opci�n [ Nro. Cheque ]');
                    return false;
                    break;
            }
            return true;
        }

        function valSelectNroOperacion_AddMedioPago() {
            var cboMedioPago = document.getElementById('<%=cboMedioPago.ClientID%>');
            switch (parseInt(cboMedioPago.value)) {
                case 1:  //*** Efectivo
                    alert('El Medio de Pago seleccionado [ ' + getCampoxValorCombo(cboMedioPago, cboMedioPago.value) + ' ] no utiliza la opci�n [ Nro. Operaci�n ]');
                    return false;
                    break;
                case 3:  //*** Transferencia                                
                    break;
                case 7:  //*** Cheque
                    alert('El Medio de Pago seleccionado [ ' + getCampoxValorCombo(cboMedioPago, cboMedioPago.value) + ' ] no utiliza la opci�n [ Nro. Operaci�n ]');
                    return false;
                    break;
                case 8:  //*** Deposito                                
                    break;
            }
            return true;
        }



        function calcularDatosCancelacion() {
            var totalAPagar = parseFloat(UnFormat(document.getElementById('<%=txtImporteTotal.ClientID%>').value, ',', ''));
            var totalRecibido = 0;
            var grilla = document.getElementById('<%=GV_Cancelacion.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    totalRecibido = totalRecibido + parseFloat(rowElem.cells[1].children[1].innerHTML);
                }
            }
            var faltante = 0;
            var vuelto = 0;
            if ((totalAPagar - totalRecibido) > 0) {
                faltante = Math.abs((totalAPagar - totalRecibido));
            } else {
                vuelto = Math.abs((totalAPagar - totalRecibido));
            }

            //********** Imprimimos valores
            var txtTotalRecibido = document.getElementById('<%=txtTotalRecibido.ClientID%>');
            if (txtTotalRecibido != null) {
                document.getElementById('<%=txtTotalRecibido.ClientID%>').value = Format(totalRecibido, 2);
            }

            var txtFaltante = document.getElementById('<%=txtFaltante.ClientID%>');
            if (txtFaltante != null) {
                document.getElementById('<%=txtFaltante.ClientID%>').value = Format(faltante, 2);
            }

            var txtVuelto = document.getElementById('<%=txtVuelto.ClientID%>');
            if (txtVuelto != null) {
                document.getElementById('<%=txtVuelto.ClientID%>').value = Format(vuelto, 2);
            }

            return false;
        }

        function valSaveDocumento() {            

            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
            if (isNaN(parseInt(cboEmpresa.value)) || cboEmpresa.value.length <= 0) {
                alert('Debe seleccionar una Empresa');
                return false;
            }
            var cboTienda = document.getElementById('<%=cboTienda.ClientID%>');
            if (isNaN(parseInt(cboTienda.value)) || cboTienda.value.length <= 0) {
                alert('Debe seleccionar una Tienda');
                return false;
            }
            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0) {
                alert('Debe seleccionar una Serie de Documento.');
                return false;
            }
            if (cboSerie.options[cboSerie.SelectedIndex].text == "001") {
                alert('Solo para Retenci�n deber� elegir una serie de tipo "002".');
                return false;
            }

            var cboTipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID%>');
            if (isNaN(parseInt(cboTipoOperacion.value)) || cboTipoOperacion.value.length <= 0 || parseInt(cboTipoOperacion.value) <= 0) {
                alert('Debe seleccionar un Tipo de Operaci�n.');
                return false;
            }

            var cboMoneda = document.getElementById('<%=cboMoneda.ClientID%>');
            if (isNaN(parseInt(cboMoneda.value)) || cboMoneda.value.length <= 0) {
                alert('Debe seleccionar una Moneda.');
                return false;
            }
            var cboCaja = document.getElementById('<%=cboCaja.ClientID%>');
            if (isNaN(parseInt(cboCaja.value)) || cboCaja.value.length <= 0) {
                alert('Debe seleccionar una Caja.');
                return false;
            }

            var cboEstadoDocumento = document.getElementById('<%=cboEstado.ClientID%>');
            if (isNaN(parseInt(cboEstadoDocumento.value)) || cboEstadoDocumento.value.length <= 0) {
                alert('Debe seleccionar un Estado del Documento.');
                return false;
            }

            var grillaDetalle = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grillaDetalle == null) {
                alert('Debe ingresar Documentos de Referencia.');
                return false;
            }

            var txtTotalAPagar = document.getElementById('<%=txtImporteTotal.ClientID%>');
            if (isNaN(parseFloat(txtTotalAPagar.value)) || txtTotalAPagar.value.length <= 0 || parseFloat(txtTotalAPagar.value) < 0) {
                alert('El Total a Pagar debe ser un valor mayor a cero.');
                return false;
            }

            var cboCondicionPago = document.getElementById('<%=cboCondicionPago.ClientID%>');
            if (isNaN(parseInt(cboCondicionPago.value)) || cboCondicionPago.value.length <= 0) {
                alert('Debe seleccionar una Condici�n de Pago.');
                return false;
            }

            var cont = 0;
            var totalRecibido = 0;
            var grilla = document.getElementById('<%=GV_Cancelacion.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    totalRecibido = totalRecibido + parseFloat(rowElem.cells[1].children[1].innerHTML);
                    if (totalRecibido == 0) {
                        cont = cont + 1;
                    }
                    
                }
            }
            if (cont > 1) {
                alert('Solo se puede ingresar un dato de cancelacion con monto < 0 >.');
                return false;
            }
            
            if (parseInt(cboCondicionPago.value) == 1) {//********* CONTADO
                var grilla = document.getElementById('<%=GV_Cancelacion.ClientID%>');
                if (grilla == null) {
                    alert('DEBE INGRESAR DATOS DE CANCELACI�N. NO SE PERMITE LA OPERACI�N.');
                    return false;
                }
            }

            return confirm('Desea continuar con la Operaci�n ?');
        }

        function valOnClickNuevo() {

            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID %>');
            if (parseInt(hddFrmModo.value) == 0 || parseInt(hddFrmModo.value) == 4 || parseInt(hddFrmModo.value) == 5 || parseInt(hddFrmModo.value) == 6) {
                return true;
            }

            var grillaDetalle = document.getElementById('<%=GV_Detalle.ClientID %>');
            if (grillaDetalle == null) {
                return true;
            }

            return (confirm('Est� seguro que desea generar un NUEVO Documento ?'));
        }


        function valOnClickBuscarDocumentoxCodigo() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                alert('Debe habilitar la B�squeda del Documento haciendo clic en el bot�n [ Buscar ]');
                return false;
            }

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }

            if (isNaN(parseInt(txtCodigoDocumento.value)) || txtCodigoDocumento.value.length <= 0) {
                alert('Debe ingresar un valor v�lido.');
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            document.getElementById('<%=hddCodigoDocumento.ClientID %>').value = txtCodigoDocumento.value;
            return true;
        }

        function valOnClick_CboCondicionPago() {
            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID %>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }
            return true;
        }
        function valOnClickEditar() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para Edici�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento est� ANULADO. No se permite su edici�n.');
                return false;
            }

            var hddContAbonos = document.getElementById('<%=hddContAbonos.ClientID%>');
            if (parseInt(hddContAbonos.value) > 0) {
                alert('El Documento posee [ ABONOS ]. No se permite su edici�n.');
                return false;
            }
            return true;
        }


        function valOnClickAnular() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para su Anulaci�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento ya est� ANULADO. No se permite su anulaci�n.');
                return false;
            }

            var hddContAbonos = document.getElementById('<%=hddContAbonos.ClientID%>');
            if (parseInt(hddContAbonos.value) > 0) {
                alert('El Documento posee [ ABONOS ]. No se permite su Anulaci�n.');
                return false;
            }

            return confirm('Desea continuar con el Proceso de ANULACI�N del Documento ?');
        }
        function print() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para Impresi�n.');
                return false;
            }
            window.open('../../Ventas/Reportes/visorVentas.aspx?iReporte=30&IdDocumento=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            return false;
        }
        function valOnClick_btnBuscarPersona() {
            mostrarCapaPersona();
            return false;
        }
        function valOnKeyPress_txtMonto_DatoCancelacion() {
            if (event.keyCode == 13) { // Enter
                document.getElementById('<%=btnAddDatoCancelacion.ClientID%>').focus();
            } else {
                return validarNumeroPuntoPositivo('event');
            }
            return true;
        }
        function valOnClick_btnAddDatoCancelacion() {

            
        
            var cboMoneda = document.getElementById('<%=cboMoneda_DatoCancelacion.ClientID%>');
            if (cboMoneda != null) {
                if (isNaN(parseInt(cboMoneda.value)) || parseInt(cboMoneda.value) <= 0 || cboMoneda.value.length <= 0) {
                    alert('Debe seleccionar una [ Moneda ].');
                    return false;
                }
            }
            var txtMonto = document.getElementById('<%=txtMonto_DatoCancelacion.ClientID%>');
            if (txtMonto != null) {
                if (isNaN(parseFloat(txtMonto.value)) || parseFloat(txtMonto.value) < 0 || txtMonto.value.length <= 0) {
                    alert('Debe ingresar un valor v�lido.');
                    txtMonto.select();
                    txtMonto.focus();
                    return false;
                }
            }
            var cboPost = document.getElementById('<%=cboPost_DC.ClientID%>');
            if (cboPost != null) {
                if (isNaN(parseInt(cboPost.value)) || parseInt(cboPost.value) <= 0 || cboPost.value.length <= 0) {
                    alert('Debe seleccionar un [ Post ].');
                    return false;
                }
            }
            var cboTarjeta = document.getElementById('<%=cboTarjeta_DC.ClientID%>');
            if (cboTarjeta != null) {
                if (isNaN(parseInt(cboTarjeta.value)) || parseInt(cboTarjeta.value) <= 0 || cboTarjeta.value.length <= 0) {
                    alert('Debe seleccionar un [ Tipo de Tarjeta ].');
                    return false;
                }
            }
            var cboBanco = document.getElementById('<%=cboBanco.ClientID%>');
            if (cboBanco != null) {
                if (isNaN(parseInt(cboBanco.value)) || parseInt(cboBanco.value) <= 0 || cboBanco.value.length <= 0) {
                    alert('Debe seleccionar un [ Banco ].');
                    return false;
                }
            }
            var cboCuentaBancaria = document.getElementById('<%=cboCuentaBancaria.ClientID%>');
            if (cboCuentaBancaria != null) {
                if (isNaN(parseInt(cboCuentaBancaria.value)) || parseInt(cboCuentaBancaria.value) <= 0 || cboCuentaBancaria.value.length <= 0) {
                    alert('Debe seleccionar una [ Cuenta Bancaria ].');
                    return false;
                }
            }
            var txtNro = document.getElementById('<%=txtNro_DC.ClientID%>');
            if (txtNro != null) {
                if (txtNro.value.length <= 0) {
                    alert('Debe ingresar un valor v�lido.');
                    txtNro.select();
                    txtNro.focus();
                    return false;
                }
            }
            return true;
        }
        function valOnClick_btnVer_NotaCredito() {
            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }
            return true;
        }
        function valFocusCancelacion() {
            document.getElementById('<%=btnAddDatoCancelacion.ClientID%>').focus();
            return false;
        }

        function valOnClick_btnBusquedaAvanzado() {

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (btnBusquedaAvanzado.disabled == true) {  //**** Se habilita la B�squeda
                alert('Debe habilitar la B�squeda del Documento haciendo clic en el bot�n [ Buscar ]');
                return false;
            }

            onCapa('capaBusquedaAvanzado');
            return false;

        }
        function valOnClickBuscar() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            var panel_Cab = document.getElementById('<%=Panel_Cab.ClientID %>');
            var btnBuscarDocumentoxCodigo = document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>');
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                panel_Cab.disabled = false;
                btnBuscarDocumentoxCodigo.disabled = false;
                btnBusquedaAvanzado.disabled = false;
                txtCodigoDocumento.readOnly = false;
                txtCodigoDocumento.disabled = false;
                txtCodigoDocumento.style.backgroundColor = 'Yellow';
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            return true;
        }

        function valOnKeyPressCodigoDoc(txtCodigoDocumento) {
            var key = event.keyCode;
            if (key == 13) {
                document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>').focus();
            } else {
                return onKeyPressEsNumero('event');
            }

        }

        function valOnKeyPress_txtNro_DC() {
            if (event.keyCode == 13) { //**************** Enter
                document.getElementById('<%=btnAddDatoCancelacion.ClientID%>').focus();
            }
            return true;
        }
        //***************************  PERFIL VS MEDIO DE PAGO
        function valOnClick_btnAceptar_AddMedioPago() {
            var txtUsuario = document.getElementById('<%=txtUsuario_AddMedioPago.ClientID %>');
            if (txtUsuario.value.length <= 0) {
                alert('CAMPO REQUERIDO. NO SE PERMITE LA OPERACI�N.');
                txtUsuario.select();
                txtUsuario.focus();
                return false;
            }
            var txtClave1 = document.getElementById('<%=txtClave_AddMedioPago.ClientID %>');
            if (txtClave1.value.length <= 0) {
                alert('CAMPO REQUERIDO. NO SE PERMITE LA OPERACI�N.');
                txtClave1.select();
                txtClave1.focus();
                return false;
            }
            var txtClave2 = document.getElementById('<%=txtClave2_AddMedioPago.ClientID %>');
            if (txtClave2.value.length <= 0) {
                alert('CAMPO REQUERIDO. NO SE PERMITE LA OPERACI�N.');
                txtClave2.select();
                txtClave2.focus();
                return false;
            }
            return true;
        }

        function verCapaPermiso_AddMedioPago() {
            onCapa('capaPermiso_AddMedioPago');
            document.getElementById('<%=txtUsuario_AddMedioPago.ClientID%>').select();
            document.getElementById('<%=txtUsuario_AddMedioPago.ClientID%>').focus();
            return false;
        }
        //
        function validarNrodias() {
            var txtFechaEmision = document.getElementById('<%=txtFechaEmision.ClientID %>');
            var day = parseInt(document.getElementById('<%=txtnrodias.ClientID %>').value);
            if (isNaN(day)) { day = 0; }

            var fecha = new Date(converToDate(txtFechaEmision.value));
            fecha.setDate(fecha.getDate() + day);

            document.getElementById('<%=txtFechaVcto.ClientID %>').value = (fecha.getDate() < 10 ? "0" + fecha.getDate().toString() : fecha.getDate().toString()) + "/" + (fecha.getMonth() + 1 < 10 ? "0" + (fecha.getMonth() + 1).toString() : (fecha.getMonth() + 1).toString()) + "/" + fecha.getFullYear().toString();

            return true;
        }
        
    
    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>
