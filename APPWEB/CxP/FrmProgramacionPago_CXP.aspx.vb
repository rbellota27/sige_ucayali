﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.
Imports Entidades
Imports Negocio
Imports general.librerias.serializador
Imports System.Linq
Partial Public Class FrmProgramacionPago_CXP
    Inherits System.Web.UI.Page

    Private objNegDocumentocheque As New Negocio.DocumentoCheque
    Private objScript As New ScriptManagerClass
    Private objNegCta As New Negocio.CuentaBancaria
    Private listaDetalleProv As New List(Of Entidades.Documento)
    Private listaRequerimiento As New List(Of Entidades.be_Requerimiento_x_pagar)

    Private listaAgentes As New List(Of Entidades.TipoAgente)
    Private listaProvisionesRelacionados As List(Of Entidades.Documento) = Nothing
    Private objNegSeriecheque As New Negocio.SerieChequeView
    Private listaDocProvisionados As List(Of Entidades.Documento)
    Private IdTipoDocumento As Integer = 0

    Private listaFacturasxCobrar As New List(Of Entidades.be_Requerimiento_x_pagar)

    Private Util As New Util
    'Este medio de Pago esta asociado a los cheques generados por la empresa (egresos)
    Private Const _IDMEDIOPAGO As Integer = 11
    Private Const _IDTIPODOCUMENTO As Integer = 42
    Private Const _IDCONCEPTOMOVBANCO As Integer = 13

    Private _Montototalapagar As Decimal = 0 'variable para recorrer y acumular la suma de cada registro
    Public _Totalfacturasacanjear As Decimal = 0 'variable para recorrer y acumular la suma de las facturas de ventas por aplicar

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Eliminar = 3
        Seleccion = 4
    End Enum

    Public ReadOnly Property SIdUsuario() As Integer
        Get
            Return CType(Session.Item("IdUsuario"), Integer)
        End Get
    End Property

    Private Function getListaFacturaxCobrar() As List(Of Entidades.be_Requerimiento_x_pagar)
        Return CType(Session.Item(CStr(ViewState("listaFacxAplicar"))), List(Of Entidades.be_Requerimiento_x_pagar))
    End Function

    Private Sub setListaFacturaxCobrar(ByVal lista As List(Of Entidades.be_Requerimiento_x_pagar))
        Session.Remove(CStr(ViewState("listaFacxAplicar")))
        Session.Add(CStr(ViewState("listaFacxAplicar")), lista)
    End Sub

    Private Function getListaProvisionado() As List(Of Entidades.Documento)
        Return CType(Session.Item(CStr(ViewState("listaDetalleProv"))), List(Of Entidades.Documento))
    End Function

    Private Sub setListaProvisionado(ByVal lista As List(Of Entidades.Documento))
        Session.Remove(CStr(ViewState("listaDetalleProv")))
        Session.Add(CStr(ViewState("listaDetalleProv")), lista)
    End Sub

    Private Function getListaAgente() As List(Of Entidades.TipoAgente)
        Return CType(Session.Item(CStr(ViewState("listaAgente"))), List(Of Entidades.TipoAgente))
    End Function

    Private Sub setListaAgente(ByVal lista As List(Of Entidades.TipoAgente))
        Session.Remove(CStr(ViewState("listaAgente")))
        Session.Add(CStr(ViewState("listaAgente")), lista)
    End Sub

    Private Function getListaRequerimiento() As List(Of Entidades.be_Requerimiento_x_pagar)
        Return CType(Session.Item(CStr(ViewState("listaRequerimiento"))), List(Of Entidades.be_Requerimiento_x_pagar))
    End Function

    Private Sub setListaRequerimiento(ByVal lista As List(Of Entidades.be_Requerimiento_x_pagar))
        Session.Remove(CStr(ViewState("listaRequerimiento")))
        Session.Add(CStr(ViewState("listaRequerimiento")), lista)
    End Sub

    Private Sub ConfigurarDatos()
        Session("AcumuladoReq") = Nothing
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub

    Private Sub onLoad_Session()
        Session("newlista") = Nothing
        Session("reqProgramar") = Nothing   'requerimientos seleccionados para programar
        Session("listaFacxAplicar") = Nothing ' facturas por aplicar a cuentas por pagar        
        Session("docAplicados") = Nothing
        Dim Aleatorio As New Random()
        ViewState.Add("listaRequerimiento", "listaRequerimientoPag" + Aleatorio.Next(0, 100000).ToString)
        ViewState.Add("listaAgente", "listaAgentePag" + Aleatorio.Next(0, 100000).ToString)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim flag As String = Request.QueryString("flag")

        'If (Me.IsPostBack = True) Then
        Select Case flag
            Case "AnularCheque"
                Dim iddocumentor As String = Request.QueryString("iddoc")
                Dim lista As New List(Of be_programacionPagos)

                Dim rpta As String = ""
                Try
                    Dim objeto As New bl_RequerimientosProgramados

                    objeto.AnularCheques(Convert.ToInt32(iddocumentor))

                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try
                If lista.Count > 0 Then
                    Try
                        rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    Response.Write(rpta)
                    Response.End()
                Else
                    Response.Write(rpta)
                    Response.End()
                End If
        End Select

        'End If
        If (Not Me.IsPostBack) Then
            Me.listaDetalleProv = New List(Of Entidades.Documento)
            setListaProvisionado(Me.listaDetalleProv)
            Me.txtDesde.Text = Date.Now
            Me.txtHasta.Text = Date.Now

            chkEsAdelanto.Attributes.Add("onclick", "return(revisarCheckAnticipo());")
            chkEsCtaPorRendir.Attributes.Add("onclick", "return(revisarCheckCtaxRendir());")
            chkPagoProveedor.Attributes.Add("onclick", "return(revisarCheckPagoProveedor());")

            listarConceptosAdelanto("A")
            listarConceptosAdelanto("C")
            listarConceptosAdelanto("P")
            Select Case Me.RadioButtonList1.SelectedValue
                Case 0
                    Me.trFiltroNroDocumento.Visible = False
                    Me.trFiltroProveedor.Visible = False
                    Me.trFiltroNroVoucher.Visible = False
                    Me.trFiltroFecha.Visible = True
                Case 1
                    Me.trFiltroNroDocumento.Visible = True
                    Me.trFiltroProveedor.Visible = False
                    Me.trFiltroFecha.Visible = False
                    Me.trFiltroNroVoucher.Visible = False
                Case 2
                    Me.trFiltroNroDocumento.Visible = False
                    Me.trFiltroNroVoucher.Visible = False
                    Me.trFiltroProveedor.Visible = True
                    Me.trFiltroFecha.Visible = False
                Case 3
                    Me.trFiltroNroDocumento.Visible = False
                    Me.trFiltroProveedor.Visible = False
                    Me.trFiltroNroVoucher.Visible = True
                    Me.trFiltroFecha.Visible = False

            End Select
            onLoad_Session()
            ConfigurarDatos()
            RemoveeSession()
            inicializarFrm()

            Session("reqProgramar") = Nothing



            'Else
            '    IsPostBack.ReadOnly = True

            '    onLoad_Session()

        End If


    End Sub

    '<WebMethod()> _
    Public Shared Function GetTheTime() As String

        Return DateTime.Now.ToShortTimeString()

    End Function

    Private Sub RemoveeSession()
        Session.Remove(CStr(ViewState("IdMovCuentaCXP")))
        Session.Remove(CStr(ViewState("IdDocumento")))
        Session.Remove(CStr(ViewState("IdTipoDocumento")))
        Session.Remove(CStr(ViewState("reqProgramar")))
        Session.Remove(CStr(ViewState("listaDetalleProv")))
        Session.Remove(CStr(ViewState("listaFacxAplicar")))
        Session.Remove(CStr(ViewState("docAplicados")))

    End Sub

    Public Sub LlenarCboTipoAgente(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)
        Dim lista As New List(Of Entidades.TipoAgente)
        lista = (New Negocio.TipoAgente).SelectCbo
        Session("tipoAgente") = lista

        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdAgente"
        cbo.DataBind()

        If addElement Then

        End If

    End Sub

    Private Sub inicializarFrm()
        Try
            Dim objCbo As New Combo
            With objCbo
                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), True)
                .LlenarCboMoneda(Me.cboMoneda, True)
                .LlenarCboTipoDocumentoProgPago(Me.cboTipoDocumento, False)
                .LLenarCboEstadoDocumento(Me.ddlEstadoDocEmitirCheque)
                .LLenarCboEstadoEntrega(Me.ddlEstadoEntregaEmitirCheque)
                .LLenarCboEstadoCancelacion(Me.ddlEstadoCancelacionEmitirCheque, False)
                'carga datos de cancelación
                llenarComboBox()

            End With
            Me.txtFechaVcto_Inicio.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.txtFechaVcto_Fin.Text = Me.txtFechaVcto_Inicio.Text
            Me.dgvCuentaBancaria.DataSource = Nothing
            Me.dgvCuentaBancaria.DataBind()
            txtserie.Visible = False
            txtcodigo.Visible = False
            lblcodigobus.Visible = False
            lblseriebus.Visible = False
            txtserie.Text = ""
            txtcodigo.Text = ""
            lbldocumentobus.Visible = False

            txtFechaVcto_Inicio.Visible = True
            txtFechaVcto_Fin.Visible = True
            lblfechainiciobus.Visible = True
            lblfechafinbus.Visible = True
            lblfechavctobus.Visible = True
            Me.Panel_VisualizarDetalle.Visible = False

            Me.gv_REquerimientoProgramados.DataSource = Nothing
            Me.gv_REquerimientoProgramados.DataBind()
            Me.GV_ProgramacionPago.DataSource = Nothing
            Me.GV_ProgramacionPago.DataBind()
            Me.gvDocAgregados.DataSource = Nothing
            Me.gvDocAgregados.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        valOnChange_cboEmpresa()
    End Sub
    Private Sub valOnChange_cboEmpresa()
        Try
            Dim objCbo As New Combo
            With objCbo
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), True)
            End With
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub btnAceptar_ViewRpt_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptar_ViewRpt.Click
        'limpiarVariablesSession()
        mostrarDeudasxParams(0)
    End Sub
    Private Sub mostrarDeudasxParams(ByVal TipoMov As Integer)
        Try
            Dim PageIndex As Integer = 0
            Select Case TipoMov
                Case 0
                    PageIndex = 0

                Case 1 '' ANTERIOR
                    PageIndex = (CInt(Me.txtPageIndex_Det.Text) - 1) - 1
                Case 2 '' SIGUIENTE
                    PageIndex = (CInt(Me.txtPageIndex_Det.Text) - 1) + 1
                Case 3 '' IR
                    PageIndex = (CInt(Me.txtPageIndexGo_Det.Text) - 1)
            End Select

            Dim codigopass As Integer = 0
            Dim IdPersona As Integer = 0
            Dim fechaVctoInicio As Date = Nothing
            Dim fechaVctoFin As Date = Nothing
            Dim codigo As Integer = 0
            Dim serie As Integer = 0

            If (IsNumeric(Me.hddIdPersona.Value) And Me.hddIdPersona.Value.Trim.Length > 0) Then
                If (CInt(Me.hddIdPersona.Value) > 0) Then
                    IdPersona = CInt(Me.hddIdPersona.Value)
                End If
            End If

            If (CStr(txtcodigo.Text) <> "" And CStr(txtserie.Text) <> "") Then

                Try
                    codigo = CInt(txtcodigo.Text)
                    serie = CInt(txtserie.Text)
                    codigopass = 1
                Catch ex As Exception
                    codigopass = 0
                End Try


            Else
                Try
                    fechaVctoInicio = CDate(Me.txtFechaVcto_Inicio.Text)
                    codigopass = 1
                Catch ex As Exception
                    fechaVctoInicio = Nothing
                    codigopass = 2
                End Try

                Try
                    fechaVctoFin = CDate(Me.txtFechaVcto_Fin.Text)
                    codigopass = 1
                Catch ex As Exception
                    fechaVctoFin = Nothing
                    codigopass = 2
                End Try

            End If

            If (codigopass = 1) Then
                Dim objeto As Entidades.be_Requerimiento_x_pagar = New Entidades.be_Requerimiento_x_pagar
                objeto = (New Negocio.MovCuentaPorPagar).MovCuentaPorPagar_SelectxParams_DataTableReport(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboMoneda.SelectedValue), IdPersona, fechaVctoInicio, fechaVctoFin, serie, codigo, CInt(Me.cboFiltro_Opcion_Programacion.SelectedValue), CInt(Me.cboFiltro_Opcion_Deuda.SelectedValue), PageIndex, CInt(Me.GV_MovCuenta_CXP.PageSize), CInt(cboTipoDocumento.SelectedValue))
                Me.listaRequerimiento = objeto.listaRequerimientos
                Me.listaAgentes = objeto.listaAgentes
                setListaRequerimiento(Me.listaRequerimiento)
                setListaAgente(Me.listaAgentes)
                Me.GV_MovCuenta_CXP.DataSource = listaRequerimiento
                Me.GV_MovCuenta_CXP.DataBind()

                If (Me.GV_MovCuenta_CXP.Rows.Count <= 0) Then
                    objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS.")
                Else
                    Me.txtPageIndex_Det.Text = CStr(PageIndex + 1)
                End If
                Me.Panel_VisualizarDetalle.Visible = False
            ElseIf (codigopass = 0) Then
                objScript.mostrarMsjAlerta(Me, "Ingrese solo números para buscar el documento. No procede la operación")
            Else
                objScript.mostrarMsjAlerta(Me, "Ingrese una fecha de Inicio y fin para continuar con la consulta. No procede la operación.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", 0)

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click

        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try
            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)

        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)
        If (objPersona IsNot Nothing) Then

            Me.txtDescripcionPersona.Text = objPersona.Descripcion
            Me.hddIdPersona.Value = CStr(objPersona.IdPersona)
        Else

            Throw New Exception("NO SE HALLARON REGISTROS.")

        End If


        objScript.offCapa(Me, "capaPersona")



    End Sub

#End Region

    Protected Sub btnLimpiar_Persona_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLimpiar_Persona.Click
        limpiarPersona()
    End Sub
    Private Sub limpiarPersona()
        Try

            Me.txtDescripcionPersona.Text = ""
            Me.hddIdPersona.Value = ""

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar.Click
        Call registrarProgramacion()
        verFrm(FrmModo.Inicio)        

        'Dim lb As LinkButton = DirectCast(sender, LinkButton)

        Dim index As Integer = 0
        Dim idRequerimiento As Integer = DirectCast(gv_REquerimientoProgramados.Rows(index).FindControl("hddIdDocumento"), HiddenField).Value
        Call mostrarReq(idRequerimiento, index)


    End Sub

    Private Sub registrarProgramacion()
        Dim cboTipoAgente As DropDownList = CType(Me.GV_DocumentosProvisionados.HeaderRow.FindControl("ddlTipoAgenteCombo"), DropDownList)
        Dim listaTipoAgente As New List(Of Entidades.TipoAgente)
        listaTipoAgente = getListaAgente()
        listaTipoAgente = (From item In listaTipoAgente
                         Where CInt(item.IdAgente) = cboTipoAgente.SelectedValue
                         Select item).ToList()
        Call ActualizarListaDetalleDocumento(listaTipoAgente)
        Dim objNegocioProgPago As New Negocio.ProgramacionPago_CXP
        Dim objProgramacionPago As Entidades.ProgramacionPago_CXP = obtenerProgramacionPago(listaTipoAgente)

        Dim esDetraccion As Boolean = False
        Dim esPercepcion As Boolean = False
        Dim esRetencion As Boolean = False

        'Si el monto Programado es menor o igual que el saldo de la deuda
        'If (montoProgramado <= saldoDeuda) Then
        'If (saldoDeuda >= 0) Then

        Dim cadenaDocumentoReferencia As String = ""
        For Each row As GridViewRow In Me.GV_DocumentosProvisionados.Rows
            cadenaDocumentoReferencia += DirectCast(row.FindControl("hddidDocumento"), HiddenField).Value
            cadenaDocumentoReferencia += ","
        Next
        If cadenaDocumentoReferencia <> "" Then
            cadenaDocumentoReferencia = cadenaDocumentoReferencia.Remove(cadenaDocumentoReferencia.Length - 1)
        End If

        Dim idRequerimientoCadena As String = String.Empty
        Dim lista As New List(Of Entidades.be_Requerimiento_x_pagar)
        lista = Session("reqProgramar")
        For i As Integer = 0 To lista.Count - 1
            idRequerimientoCadena += lista(i).idDocumento.ToString() + ","
        Next
        idRequerimientoCadena = idRequerimientoCadena.Remove(idRequerimientoCadena.Length - 1)
        Dim idRegimen As Integer = 0
        If esDetraccion Then
            idRegimen = 2
        ElseIf esRetencion Then
            idRegimen = 3
        ElseIf esPercepcion Then
            idRegimen = 1
        End If
        Try


            If chkPagoParcial.Checked = True Then
                Select Case hddFrmModo.Value
                    Case FrmModo.Nuevo
                        objNegocioProgPago.InsertParcial(cadenaDocumentoReferencia.ToString(), idRequerimientoCadena, objProgramacionPago, obtenerListaRelacionDocumento(esDetraccion), obtenerListaRelacionDocumento_LQ(esDetraccion), _
                          esDetraccion, _
                          esRetencion, _
                          esPercepcion, _
                          FrmModo.Nuevo, _
                          idRegimen, _
                          Nothing, _
                          listaTipoAgente
                         )
                    Case FrmModo.Editar
                        objNegocioProgPago.InsertParcial(cadenaDocumentoReferencia.ToString(), idRequerimientoCadena, objProgramacionPago, obtenerListaRelacionDocumento(esDetraccion), obtenerListaRelacionDocumento_LQ(esDetraccion), _
                          esDetraccion, _
                          esRetencion, _
                          esPercepcion, _
                          FrmModo.Editar, _
                          idRegimen, _
                          Nothing, _
                          listaTipoAgente)
                End Select
            End If

            If chkPagoParcial.Checked = False Then

                Select Case hddFrmModo.Value
                    Case FrmModo.Nuevo
                        objNegocioProgPago.Insert(cadenaDocumentoReferencia.ToString(), idRequerimientoCadena, objProgramacionPago, obtenerListaRelacionDocumento(esDetraccion), _
                          esDetraccion, _
                          esRetencion, _
                          esPercepcion, _
                          FrmModo.Nuevo, _
                          idRegimen, _
                          Nothing, _
                          listaTipoAgente)
                    Case FrmModo.Editar
                        objNegocioProgPago.Insert(cadenaDocumentoReferencia.ToString(), idRequerimientoCadena, objProgramacionPago, obtenerListaRelacionDocumento(esDetraccion), _
                          esDetraccion, _
                          esRetencion, _
                          esPercepcion, _
                          FrmModo.Editar, _
                          idRegimen, _
                          Nothing, _
                          listaTipoAgente)
                End Select

            End If

            
        Catch ex As Exception
            Throw ex
        End Try
        'End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');", True)
    End Sub

    Private Function obtenerListaRelacionDocumento(ByVal esDetraccion As Boolean) As List(Of Entidades.RelacionDocumento)
        Dim listaReq As New List(Of Entidades.be_Requerimiento_x_pagar)
        listaReq = Session("reqProgramar")

        Me.listaDetalleProv = getListaProvisionado()
        Me.listaFacturasxCobrar = Session("docAplicados")

        Dim lista As New List(Of Entidades.RelacionDocumento)
        'Me.ListaDocProvisionados = getListaDocProvisionados()
        Dim objRelacionDocumento As Entidades.RelacionDocumento = Nothing

        If (listaDetalleProv IsNot Nothing) Then
            For j As Integer = 0 To listaReq.Count - 1

                'If esDetraccion Then
                '    For Each row As GridViewRow In Me.gv_docAsociadosDetraccion.Rows
                '        objRelacionDocumento = New Entidades.RelacionDocumento
                '        With objRelacionDocumento
                '            .IdDocumento1 = listaReq(j).idDocumento
                '            '.IdDocumento2 = listaDocProvisonados(i).IdDocumento
                '            .IdDocumento2 = DirectCast(row.FindControl("hddIDDocumento"), HiddenField).Value

                '            Dim checkbox As CheckBox = DirectCast(row.FindControl("checkDetraccion"), CheckBox)
                '            If checkbox.Checked Then
                '                .sujetoDetraccion = True
                '            End If
                '        End With
                '        lista.Add(objRelacionDocumento)
                '    Next
                'End If
                Dim ddl As DropDownList = DirectCast(Me.GV_DocumentosProvisionados.HeaderRow.FindControl("ddlTipoAgenteCombo"), DropDownList)
                If esDetraccion = False Then
                    For i As Integer = 0 To listaDetalleProv.Count - 1
                        objRelacionDocumento = New Entidades.RelacionDocumento
                        With objRelacionDocumento
                            .IdDocumento1 = listaReq(j).idDocumento
                            .IdDocumento2 = listaDetalleProv(i).IdDocumento
                            .montoAmortizado = listaDetalleProv(i).montoXPagar
                            .montoDeuda = listaDetalleProv(i).montoAgente
                            .montoAgente = listaDetalleProv(i).montoAgente
                            .tipoRegimen = listaDetalleProv(i).tipoRegimen
                            .MontoOtroTributo = listaDetalleProv(i).MontoOtroTributo
                            .sujetoDetraccion = False
                        End With
                        lista.Add(objRelacionDocumento)
                    Next
                    If Not IsNothing(Me.listaFacturasxCobrar) Then
                        For h As Integer = 0 To Me.listaFacturasxCobrar.Count - 1
                            objRelacionDocumento = New Entidades.RelacionDocumento
                            With objRelacionDocumento
                                .IdDocumento1 = listaReq(j).idDocumento
                                .IdDocumento2 = listaFacturasxCobrar(h).idDocumento
                                .montoAmortizado = listaFacturasxCobrar(h).mcp_abono
                                .montoDeuda = listaFacturasxCobrar(h).mcp_saldo
                                .montoAgente = 0
                                .tipoRegimen = 0
                                .sujetoDetraccion = False
                            End With
                            lista.Add(objRelacionDocumento)
                        Next
                    End If
                End If
            Next
        Else
            lista = Nothing
        End If
        Return lista
    End Function


    Private Function obtenerListaRelacionDocumento_LQ(ByVal esDetraccion As Boolean) As List(Of Entidades.RelacionDocumento)
        Dim listaReq As New List(Of Entidades.be_Requerimiento_x_pagar)
        listaReq = Session("reqProgramar")

        Me.listaDetalleProv = getListaProvisionado()
        Me.listaFacturasxCobrar = Session("docAplicados")

        Dim lista As New List(Of Entidades.RelacionDocumento)
        'Me.ListaDocProvisionados = getListaDocProvisionados()
        Dim objRelacionDocumento As Entidades.RelacionDocumento = Nothing

        If (listaDetalleProv IsNot Nothing) Then


            Dim ddl As DropDownList = DirectCast(Me.GV_DocumentosProvisionados.HeaderRow.FindControl("ddlTipoAgenteCombo"), DropDownList)

            If esDetraccion = False Then
                For i As Integer = 0 To listaDetalleProv.Count - 1
                    objRelacionDocumento = New Entidades.RelacionDocumento
                    With objRelacionDocumento
                        ' .IdDocumento1 = listaReq(j).idDocumento
                        .IdDocumento2 = listaDetalleProv(i).IdDocumento
                        .montoAmortizado = listaDetalleProv(i).montoXPagar
                        .montoDeuda = listaDetalleProv(i).montoAgente
                        .montoAgente = listaDetalleProv(i).montoAgente
                        .tipoRegimen = listaDetalleProv(i).tipoRegimen
                        .MontoOtroTributo = listaDetalleProv(i).MontoOtroTributo
                        .sujetoDetraccion = False
                    End With
                    lista.Add(objRelacionDocumento)
                Next
                If Not IsNothing(Me.listaFacturasxCobrar) Then
                    For h As Integer = 0 To Me.listaFacturasxCobrar.Count - 1
                        objRelacionDocumento = New Entidades.RelacionDocumento
                        With objRelacionDocumento
                            '.IdDocumento1 = listaReq(j).idDocumento
                            .IdDocumento2 = listaFacturasxCobrar(h).idDocumento
                            .montoAmortizado = listaFacturasxCobrar(h).mcp_abono
                            .montoDeuda = listaFacturasxCobrar(h).mcp_saldo
                            .montoAgente = 0
                            .tipoRegimen = 0
                            .sujetoDetraccion = False
                        End With
                        lista.Add(objRelacionDocumento)
                    Next
                End If
            End If

        Else
            lista = Nothing
        End If
        Return lista
    End Function

    Private Function obtenerRelacionAplicacion() As List(Of Entidades.be_documentoAplicacionDeudasPendientes)
        Dim lista As List(Of Entidades.be_documentoAplicacionDeudasPendientes) = Nothing
        Dim objeto As Entidades.be_documentoAplicacionDeudasPendientes = Nothing
        lista = New List(Of Entidades.be_documentoAplicacionDeudasPendientes)
        For Each row As GridViewRow In Me.gv_AplicacionFacturas.Rows
            objeto = New Entidades.be_documentoAplicacionDeudasPendientes
            objeto.idProvision = DirectCast(row.FindControl("hddidDocumentoExterno"), HiddenField).Value
            objeto.idFacturaAplicacion = DirectCast(row.FindControl("hddidDocumento"), HiddenField).Value
            objeto.montoAplicadoContraDeudaPendiente = DirectCast(row.FindControl("lblTotal"), Label).Text
            lista.Add(objeto)
        Next
        Return lista
    End Function

    Private Function obtenerProgramacionPago(Optional ByVal listaTipoAgente As List(Of Entidades.TipoAgente) = Nothing) As Entidades.ProgramacionPago_CXP

        Dim obj As New Entidades.ProgramacionPago_CXP
        With obj
            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .IdProgramacionPago = Nothing
                Case FrmModo.Editar
                    .IdProgramacionPago = CInt(Me.hddIdProgramacionPago_CXP.Value)
            End Select
            If listaTipoAgente.Count = 0 Then .porc_agente = 0 Else .porc_agente = listaTipoAgente(0).Tasa
            .IdMovCtaPP = Session("IdMovCuentaCXP")
            '.IdMovCtaPP = DirectCast(Me.gv_REquerimientoProgramados.Rows(0).FindControl("hddIdMovCuenta_CXP"), HiddenField).Value
            .FechaPagoProg = CDate(Me.txtFecha_ProgPago.Text)
            .IdBanco = CInt(Me.cboBanco_Prog.SelectedValue)
            .IdMedioPago = CInt(Me.cboMedioPago_Prog.SelectedValue)
            .IdCuentaBancaria = CInt(Me.cboCuentaBancaria_Prog.SelectedValue)
            .Observacion = CStr(Me.txtObservaciones_Prog.Text)
            .TipoCambio = CDec(Me.txtTipoCambio.Text)
            .IdTipoMoneda = CInt(Me.cboTipoMoneda.SelectedValue)
            .MontoPagoProg = CDec(Me.txtMontoProg.Text)
            If Not IsNothing(gvConceptoReferencia) And gvConceptoReferencia.Rows.Count > 0 Then
                .idConcepto = DirectCast(Me.gvConceptoReferencia.Rows(0).FindControl("hddIdConcepto"), HiddenField).Value
            End If
        End With
        Return obj
    End Function

    Protected Sub GV_MovCuenta_CXP_oncLick(ByVal sender As Object, ByVal e As EventArgs)
        Dim lb As LinkButton = DirectCast(sender, LinkButton)
        Dim index As Integer = DirectCast(lb.NamingContainer, GridViewRow).RowIndex
        RemoveeSession()
        MandandoAlFormularioProgramacionPago(index)
    End Sub

    Protected Sub GV_MovCuenta_CXP_VerProgramacion_onClick(ByVal sender As Object, ByVal e As EventArgs)

        verFrm(FrmModo.Inicio)
        Dim lb As LinkButton = DirectCast(sender, LinkButton)
        Dim index As Integer = DirectCast(lb.NamingContainer, GridViewRow).RowIndex
        Dim idRequerimiento As Integer = DirectCast(GV_MovCuenta_CXP.Rows(index).FindControl("hddIdDocumento"), HiddenField).Value
        Call mostrarReq(idRequerimiento, index)

    End Sub

    Private Sub mostrarlistaregimen(ByVal idRequerimientoCadena As String)
        Dim listaRegimen As New List(Of Entidades.be_montoRegimen)
        'obtengo el objeto con las lista de Req. y Programaciones
        Dim objeto As New Entidades.be_programacionPagos
        objeto = (New Negocio.ProgramacionPago_CXP).SelectxIdMovCuentaCXPTexto(idRequerimientoCadena)
        listaRegimen = objeto.listaMontoRegimen
        Me.GV_TipoRegimen.DataSource = listaRegimen
        Me.GV_TipoRegimen.DataBind()
    End Sub

    Private Sub mostrarReq(ByVal idRequerimiento As Integer, ByVal index As Integer)
        Dim listaReq As New List(Of Entidades.be_Requerimiento_x_pagar)
        Dim listaProgramacion As New List(Of Entidades.ProgramacionPago_CXP)
        Dim listaProvisiones As New List(Of Entidades.Documento)
        Dim listaAplicaciones As New List(Of Entidades.be_Requerimiento_x_pagar)
        Dim listaRegimen As New List(Of Entidades.be_montoRegimen)
        Dim listaFacturasxAplicarSelect As New List(Of Entidades.be_Requerimiento_x_pagar)

        'obtengo el objeto con las lista de Req. y Programaciones
        Dim objeto As New Entidades.be_programacionPagos
        objeto = (New Negocio.ProgramacionPago_CXP).SelectxIdMovCuentaCXP(idRequerimiento)

        'lista de listaFacturasxAplicarSelect por aplciar
        listaFacturasxAplicarSelect = objeto.listaFacturasxAplicar
        Me.gv_AplicacionFacturas.DataSource = listaFacturasxAplicarSelect
        Me.gv_AplicacionFacturas.DataBind()

        'lista de Programaciones
        listaProgramacion = objeto.listaProgramaciones
        'Session("listaProgramacion") = listaProgramacion
        Me.GV_ProgramacionPago.DataSource = listaProgramacion
        Me.GV_ProgramacionPago.DataBind()

        'Lista de Requerimientos
        listaReq = objeto.listaRequerimientos
        Session("reqProgramar") = listaReq
        setListaRequerimiento(listaReq)

        Me.gv_REquerimientoProgramados.DataSource = listaReq
        Me.gv_REquerimientoProgramados.DataBind()

        'Lista de Doc. Provisionados
        listaProvisiones = objeto.listaProvisiones
        setListaProvisionado(listaProvisiones)

        Me.GV_DocumentosProvisionados.DataSource = listaProvisiones
        Me.GV_DocumentosProvisionados.DataBind()

        ''Lista de Facturas que fueron aplicados para pagar deudas pendientes
        'listaAplicaciones = objeto.listaAplicaciones
        'Me.gv_AplicacionFacturas.DataSource = listaAplicaciones
        'Me.gv_AplicacionFacturas.DataBind()

        listaRegimen = objeto.listaMontoRegimen

        Me.GV_TipoRegimen.DataSource = listaRegimen
        Me.GV_TipoRegimen.DataBind()

        'Si encuentra 1 registro o varios del mimso prov. busca el número de cuenta
        Dim idProveedor As Integer = 0
        Try
            idProveedor = TryCast(Me.GV_MovCuenta_CXP.Rows(index).FindControl("hddIdProveedor"), HiddenField).Value
        Catch ex As Exception
        End Try
        Dim igualProveedor As Boolean = False
        For i As Integer = 0 To listaReq.Count - 1
            If listaReq(i).idProveedor = idProveedor Then
                Me.dgvCuentaBancaria.DataSource = objNegCta.SelectcuentaBancaria(idProveedor, 0, 1, 0)
                Me.dgvCuentaBancaria.DataBind()
            End If
        Next

        Dim idTipoDocumento As Integer = 0
        idTipoDocumento = CInt(CType(Me.GV_MovCuenta_CXP.Rows(index).FindControl("hddIdTipoDocumento"), HiddenField).Value)
        Dim objCombo As New Combo
        With objCombo
            .LlenarCboMedioPagoxIdTipoDocumento(Me.cboMedioPago_Prog, idTipoDocumento, True)
        End With


        Me.gvConceptoReferencia.DataSource = objeto.listaConcepto
        Me.gvConceptoReferencia.DataBind()

        'Setea la grilla requerimientos
        Me.GV_ProgramacionPago.DataSource = listaProgramacion
        Me.GV_ProgramacionPago.DataBind()

        'setea la grilla de programaciones

        'marco el checkBox si fuese necesario
        If objeto.listaConcepto.Count > 0 Then
            Me.chkEsAdelanto.Checked = objeto.listaConcepto(0).ConceptoAdelanto
            Me.chkEsCtaPorRendir.Checked = objeto.listaConcepto(0).conceptoCtaxRendir
        End If

        If Not IsNothing(listaReq) Then
            If listaReq.Count > 0 Then
                Dim monto As Decimal = 0
                For Each row As GridViewRow In Me.gv_REquerimientoProgramados.Rows
                    monto = monto + CDec(DirectCast(row.FindControl("lblMonto"), Label).Text)
                Next
            End If
        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", " onCapa('capaProgramacionPago'); ", True)
    End Sub

    Private Sub valOnClick_btnNuevo()
        Try

            Me.lblResultadoDolares.Visible = False
            Me.txtResultadoTipoCambio.Visible = False
            Me.btncalcularTC.Visible = False
            Me.lblTipoCambio.Visible = True
            Me.txtTipoCambio.Visible = True
            Me.txtTipoCambio.Text = (New Negocio.bl_pagoProveedor).buscarTipoCambioCompra(Me.txtFecha_ProgPago.Text)
            Me.txtResultadoTipoCambio.Text = "0"
            verFrm(FrmModo.Nuevo)

            Dim ddlAgentesss As DropDownList = DirectCast(Me.GV_DocumentosProvisionados.HeaderRow.FindControl("ddlTipoAgenteCombo"), DropDownList)
            Dim listAgentes As New List(Of Entidades.TipoAgente)
            listAgentes = getListaAgente()
            ddlAgentesss.DataSource = listAgentes
            ddlAgentesss.DataValueField = "IdAgente"
            ddlAgentesss.DataTextField = "Nombre"
            ddlAgentesss.DataBind()
            'ddlAgentesss.Items.Insert(0, New ListItem("---", 0))

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago'); ", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevo.Click
        valOnClick_btnNuevo()

    End Sub

    Protected Sub linkButtonProgramar(ByVal sender As Object, ByVal e As EventArgs)
        Dim url As String = "frmProgramacionProvision.aspx"
        Dim s As String = "window.open('" & url + "', 'popup_window', 'width=1100,height=640,left=100,resizable=no,scrollbars=yes');"
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "script", s, True)
    End Sub

    Private Sub MandandoAlFormularioProgramacionPago(ByVal index As Integer)
        listaProvisionesRelacionados = New List(Of Entidades.Documento)
        listaRequerimiento = Me.getListaRequerimiento
        Dim IdMovCuentaCXP As Integer = CInt(CType(Me.GV_MovCuenta_CXP.Rows(index).FindControl("hddIdMovCuenta_CXP"), HiddenField).Value)
        Dim IdDocumento As Integer = CInt(CType(Me.GV_MovCuenta_CXP.Rows(index).FindControl("hddIdDocumento"), HiddenField).Value)
        Dim IdTipoDocumento As Integer = CInt(CType(Me.GV_MovCuenta_CXP.Rows(index).FindControl("hddIdTipoDocumento"), HiddenField).Value)

        listaRequerimiento = (From item In listaRequerimiento
               Where item.idMovCtaPP.ToString.Contains(IdMovCuentaCXP)
               Select item).ToList()

        Session.Add("IdTipoDocumento", IdTipoDocumento)
        Session.Add("IdMovCuentaCXP", IdMovCuentaCXP)
        AcumularProgramaciones(listaRequerimiento, index)
    End Sub

    Private Sub AcumularProgramaciones(ByVal list As List(Of Entidades.be_Requerimiento_x_pagar), ByVal index As Integer)
        Dim lista As New List(Of Entidades.be_Requerimiento_x_pagar)
        Dim idDocumentoAgregado As Integer = 0
        If IsNothing(Session("reqProgramar")) OrElse DirectCast(Session("reqProgramar"), List(Of Entidades.be_Requerimiento_x_pagar)).Count = 0 Then
            lista.AddRange(list)
            Session("reqProgramar") = lista
            Me.gvDocAgregados.DataSource = lista
            Me.gvDocAgregados.DataBind()
            If gvDocAgregados.Rows.Count > 0 Then
                DirectCast(Me.gvDocAgregados.FooterRow.FindControl("lblMoneda_Monto"), Label).Text = list(0).nom_simbolo
            End If
        Else
            lista = DirectCast(Session("reqProgramar"), List(Of Entidades.be_Requerimiento_x_pagar))
            'Verifico que el requerimiento agregado no exista
            idDocumentoAgregado = DirectCast(Me.GV_MovCuenta_CXP.Rows(index).FindControl("hddIdDocumento"), HiddenField).Value
            Dim listaFiltro As New List(Of Entidades.be_Requerimiento_x_pagar)

            listaFiltro = (From item In lista
               Where item.idDocumento.ToString.Contains(idDocumentoAgregado)
               Select item).ToList()
            If listaFiltro.Count = 0 Then
                'verifico que la moneda sea la msima para todos
                For i As Integer = 0 To list.Count - 1
                    If Trim(list(i).nom_simbolo) = Trim(lista(0).nom_simbolo) Then
                        lista.AddRange(list)
                        Session.Add("reqProgramar", lista)
                        Me.gvDocAgregados.DataSource = lista
                        Me.gvDocAgregados.DataBind()
                    Else
                        objScript.mostrarMsjAlerta(Me, "Operación Inválida, moneda no coincide con los anteriores Req. Agregados.")
                    End If
                Next
            Else
                objScript.mostrarMsjAlerta(Me, "Operación inválida, el documento seleccionado ya fue agregado.")
            End If
        End If
        seteaVariableSumaAcumulado()
    End Sub

    Private Sub seteaVariableSumaAcumulado()
        Dim monto As Decimal = 0
        ' Dim saldo As Decimal = 0
        For Each row As GridViewRow In Me.gvDocAgregados.Rows
            ' saldo = saldo + CDec(DirectCast(row.FindControl("lblSaldo"), Label).Text)
            monto = monto + CDec(DirectCast(row.FindControl("lblMonto"), Label).Text)
        Next
        If gvDocAgregados.Rows.Count > 0 Then
            ' DirectCast(Me.gvDocAgregados.FooterRow.FindControl("lblMontoTotalAgregados"), Label).Text = saldo
            DirectCast(Me.gvDocAgregados.FooterRow.FindControl("lblMontoTotalAgregados"), Label).Text = monto
        End If
    End Sub

    Protected Sub btnEditar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEditar.Click
        valOnClick_btnEditar()
    End Sub
    Private Sub valOnClick_btnEditar()
        Try
            verFrm(FrmModo.Editar)
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago'); ", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        eliminarProgramacionPago_CXP()
    End Sub
    Private Sub eliminarProgramacionPago_CXP()
        Try
            Dim cadenaIdRequerimientos As String = String.Empty
            Dim objProgramacionPagoCXP As New Negocio.ProgramacionPago_CXP

            For Each row As GridViewRow In Me.gv_REquerimientoProgramados.Rows
                cadenaIdRequerimientos += DirectCast(row.FindControl("hddidDocumento"), HiddenField).Value
                cadenaIdRequerimientos += ","
            Next
            '   cadenaIdRequerimientos = Convert.ToString(HddIdAnexoDocumento.Value)
            If cadenaIdRequerimientos <> "" Then
                cadenaIdRequerimientos = cadenaIdRequerimientos.Remove(cadenaIdRequerimientos.Length - 1)
            End If
            Dim retornar As Boolean = False

            Try
                retornar = objProgramacionPagoCXP.DeletexIdProgramacionPagoCXP(cadenaIdRequerimientos)
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago'); alert('La Operación se realizó con éxito.'); ", True)
            Catch ex As Exception
                objScript.mostrarMsjAlerta(Me, ex.Message)
            End Try

            Dim idReq As Integer = DirectCast(Me.gv_REquerimientoProgramados.Rows(0).FindControl("hddIdDocumento"), HiddenField).Value
            Call mostrarReq(idReq, 0)
            verFrm(FrmModo.Inicio)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancelar.Click
        valOnClick_btnCancelar()
    End Sub
    Private Sub valOnClick_btnCancelar()
        Try
            verFrm(FrmModo.Inicio)
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');  ", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboCuentaBancaria_Prog_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCuentaBancaria_Prog.SelectedIndexChanged
        Try

            If (cboCuentaBancaria_Prog.SelectedItem.Text.StartsWith("S")) Then
                If (Me.cboTipoMoneda.Items.FindByValue(CStr("1")) IsNot Nothing) Then
                    Me.cboTipoMoneda.SelectedValue = CStr(1)
                End If
            ElseIf (cboCuentaBancaria_Prog.SelectedItem.Text.StartsWith("US")) Then
                If (Me.cboTipoMoneda.Items.FindByValue(CStr("2")) IsNot Nothing) Then
                    Me.cboTipoMoneda.SelectedValue = CStr(2)
                End If
            Else
                If (Me.cboTipoMoneda.Items.FindByValue(CStr("3")) IsNot Nothing) Then
                    Me.cboTipoMoneda.SelectedValue = CStr(3)
                End If
            End If
            Dim obj As New Negocio.bl_pagoProveedor
            Dim montoOriginal As Decimal = 0
            montoOriginal = CDec(hddMontoProgramado.Value)

            lblMoneda_MontoProg.Text = CStr(cboTipoMoneda.SelectedItem.Text)
            Dim lista As New List(Of Entidades.be_Requerimiento_x_pagar)
            lista = Session("reqProgramar")
            'qweqweqwe()
            If (CStr(cboTipoMoneda.SelectedItem.Text) = "S/. ") Then
                If lista(0).idMoneda = 2 Then
                    Dim tc As Decimal = Math.Round(obj.buscarTipoCambioCompra(Me.txtFecha_ProgPago.Text), 3)
                    Me.txtMontoProg.Text = Math.Round(montoOriginal * tc, 3)
                    Me.txtTipoCambio.Text = tc
                    If tc <> 0 Then
                        Me.txtResultadoTipoCambio.Text = Math.Round(montoOriginal / tc, 3)
                    End If
                End If
                lblResultadoDolares.Visible = True
                'lblResultadoTipoCambio.Visible = True
                txtResultadoTipoCambio.Visible = True
                'lblResultadoTipoCambio.Visible = True
                btncalcularTC.Visible = True
                lblTipoCambio.Visible = True
                txtTipoCambio.Visible = True
            Else
                lblResultadoDolares.Visible = False
                'lblResultadoTipoCambio.Visible = False
                txtResultadoTipoCambio.Visible = False
                'lblResultadoTipoCambio.Visible = False
                btncalcularTC.Visible = False
                lblTipoCambio.Visible = True
                txtTipoCambio.Visible = True
                Me.txtMontoProg.Text = montoOriginal
                txtTipoCambio.Text = "0"
                txtResultadoTipoCambio.Text = "0"
            End If



            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');  ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboBanco_Prog_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBanco_Prog.SelectedIndexChanged
        valOnChange_cboBanco()
    End Sub
    Private Sub valOnChange_cboBanco()
        Try
            Dim objCbo As New Combo
            With objCbo
                .LlenarCboCuentaBancaria(Me.cboCuentaBancaria_Prog, CInt(Me.cboBanco_Prog.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), True)
            End With
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');  ", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub visualizarDocumento_MCXP(ByVal Index As Integer)
        Try

            Dim IdDocumento As Integer = CInt(CType(Me.GV_MovCuenta_CXP.Rows(Index).FindControl("hddIdDocumento"), HiddenField).Value)

            Me.GV_DocumentoRef_Cab.DataSource = obtenerDocumentoRef_Cab(IdDocumento)
            Me.GV_DocumentoRef_Cab.DataBind()

            Me.GV_DocumentoRef_Detalle.DataSource = (New Negocio.DetalleDocumento).SelectxIdDocumento(IdDocumento)
            Me.GV_DocumentoRef_Detalle.DataBind()

            Me.GV_DocumentoRef_DetalleConcepto.DataSource = (New Negocio.DetalleConcepto).SelectxIdDocumento(IdDocumento)
            Me.GV_DocumentoRef_DetalleConcepto.DataBind()

            Me.GV_DocumentoRef_CondicionC.DataSource = (New Negocio.Documento_CondicionC).SelectxIdDocumento(IdDocumento)
            Me.GV_DocumentoRef_CondicionC.DataBind()

            Me.Panel_VisualizarDetalle.Visible = True

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function obtenerDocumentoRef_Cab(ByVal IdDocumentoRef As Integer) As List(Of Entidades.Documento)

        Dim lista As New List(Of Entidades.Documento)

        Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumentoProgramacionPago(IdDocumentoRef)

        Try : objDocumento.NomCondicionPago = (New Negocio.CondicionPago).SelectxId(objDocumento.IdCondicionPago)(0).Nombre : Catch ex As Exception : End Try

        lista.Add(objDocumento)

        Return lista

    End Function

    Protected Sub visualizarDocumento_Datos(ByVal sender As Object, ByVal e As System.EventArgs)
        visualizarDocumento_MCXP(CInt(CType(CType(sender, ImageButton).NamingContainer, GridViewRow).RowIndex))
    End Sub

    Private Sub btnAnterior_Det_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Det.Click
        mostrarDeudasxParams(1)
    End Sub

    Private Sub btnSiguiente_Det_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSiguiente_Det.Click
        mostrarDeudasxParams(2)
    End Sub

    Private Sub btnIr_det_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_det.Click
        mostrarDeudasxParams(3)
    End Sub

    Private Sub calcularTipoCambio()
        'qweqweqwe()
        Dim obj As New Negocio.bl_pagoProveedor
        Dim montoOriginal As Decimal = 0
        montoOriginal = CDec(hddMontoProgramado.Value)

        lblMoneda_MontoProg.Text = CStr(cboTipoMoneda.SelectedItem.Text)
        Dim lista As New List(Of Entidades.be_Requerimiento_x_pagar)
        lista = Session("reqProgramar")

        If lista(0).idMoneda = 2 Then
            Dim tc As Decimal = Math.Round(obj.buscarTipoCambioCompra(Me.txtFecha_ProgPago.Text), 3)
            Me.txtMontoProg.Text = Math.Round(montoOriginal * tc, 3)
            Me.txtTipoCambio.Text = tc
            If tc <> 0 Then
                Me.txtResultadoTipoCambio.Text = Math.Round(montoOriginal / tc, 3)
            End If
        End If

        Dim ResultadoxTC As Decimal = CDec(CDec(txtMontoProg.Text) / CDec(txtTipoCambio.Text))
        txtResultadoTipoCambio.Text = CStr(FormatNumber(ResultadoxTC, 2))
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');  ", True)
    End Sub

    Private Sub btncalcularTC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncalcularTC.Click
        Call calcularTipoCambio()
    End Sub

    Protected Sub rdTipoBusqueda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdTipoBusqueda.SelectedIndexChanged

        If (rdTipoBusqueda.SelectedValue = "0") Then
            txtserie.Visible = False
            txtcodigo.Visible = False
            lblcodigobus.Visible = False
            lblseriebus.Visible = False
            txtserie.Text = ""
            txtcodigo.Text = ""
            lbldocumentobus.Visible = False

            txtFechaVcto_Inicio.Visible = True
            txtFechaVcto_Fin.Visible = True
            lblfechainiciobus.Visible = True
            lblfechafinbus.Visible = True
            lblfechavctobus.Visible = True
            Me.txtFechaVcto_Inicio.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.txtFechaVcto_Fin.Text = Me.txtFechaVcto_Inicio.Text
        Else
            txtFechaVcto_Inicio.Visible = False
            txtFechaVcto_Fin.Visible = False
            lblfechainiciobus.Visible = False
            lblfechafinbus.Visible = False
            lblfechavctobus.Visible = False

            txtserie.Visible = True
            txtcodigo.Visible = True
            lblcodigobus.Visible = True
            lblseriebus.Visible = True
            lbldocumentobus.Visible = True
        End If
    End Sub

    Private Sub lbCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbCancelar.Click
        limpiarVariablesSession()
    End Sub

    Protected Sub quitarDocAgregadoxProgramar(ByVal sender As Object, ByVal e As EventArgs)
        Dim linkbutton As LinkButton = DirectCast(sender, LinkButton)
        Dim index As Integer = DirectCast(linkbutton.NamingContainer, GridViewRow).RowIndex
        Dim idDocumento As Integer = DirectCast(Me.gvDocAgregados.Rows(index).FindControl("hddIdDocumento"), HiddenField).Value
        Dim listaOrigen As List(Of Entidades.be_Requerimiento_x_pagar)
        listaOrigen = Session("reqProgramar")
        listaOrigen.RemoveAt(index)
        Me.gvDocAgregados.DataSource = listaOrigen
        Me.gvDocAgregados.DataBind()
        seteaVariableSumaAcumulado()
    End Sub

    Private Sub lbProgramar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbProgramar.Click

        Session("docAplicados") = Nothing
        Dim lista As List(Of Entidades.be_Requerimiento_x_pagar) = Session("reqProgramar")
        Me.GV_ProgramacionPago.DataSource = Nothing
        Me.GV_ProgramacionPago.DataBind()
        Dim idDocumento As Integer = 0

        'Me.gv_REquerimientoProgramados.DataSource = lista
        'Me.gv_REquerimientoProgramados.DataBind()
        Dim monto As Decimal = 0
        Dim saldo As Decimal = 0
        For Each row As GridViewRow In Me.gv_REquerimientoProgramados.Rows
            saldo += CDec(DirectCast(row.FindControl("lblSaldo"), Label).Text)
            monto += CDec(DirectCast(row.FindControl("lblMonto"), Label).Text)
        Next

        llenarComboBox() 'Carga medio de pago, banco, moneda, cuentas e inicializa formulario.
        verFrm(FrmModo.Inicio)

        If lista.Count > 0 Then
            idDocumento = CInt(CType(Me.GV_MovCuenta_CXP.Rows(0).FindControl("hddIdTipoDocumento"), HiddenField).Value)
            Dim cboCombo As New Combo
            With cboCombo
                .LlenarCboMedioPagoxIdTipoDocumento(Me.cboMedioPago_Prog, idDocumento, True)
            End With
        End If
        mostrarCuentaBancaria(lista)

        Dim montoProvisiones As Decimal = 0
        If Me.GV_DocumentosProvisionados.Rows.Count > 0 Then
            montoProvisiones = CDec(DirectCast(Me.GV_DocumentosProvisionados.FooterRow.FindControl("lblTotal"), Label).Text)
        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "mostrarCapaProgramacion()", True)
    End Sub

    Protected Sub evento_quitar(ByVal sender As Object, ByVal e As EventArgs)
        Dim glosa As String = "PAGO A PROVEEDORES "
        Dim cboTipoAgente As DropDownList = CType(Me.GV_DocumentosProvisionados.HeaderRow.FindControl("ddlTipoAgenteCombo"), DropDownList)
        Dim listaTipoAgente As New List(Of Entidades.TipoAgente)
        listaTipoAgente = getListaAgente()
        If cboTipoAgente.SelectedValue <> 0 Then
            listaTipoAgente = (From item In listaTipoAgente
                 Where CInt(item.IdAgente) = cboTipoAgente.SelectedValue
                 Select item).ToList()
        End If

        Call ActualizarListaDetalleDocumento(listaTipoAgente)

        Dim linkButton As LinkButton = CType(sender, LinkButton)
        Dim index As Integer = DirectCast(linkButton.NamingContainer, GridViewRow).RowIndex
        'Dim totalaPagar As Decimal = CDec(IIf(Me.txtRedondeo.Text = "", 0, Me.txtRedondeo.Text))
        Dim montoRetirado As Decimal = CDec(DirectCast(Me.GV_DocumentosProvisionados.Rows(index).FindControl("lblTotal"), Label).Text)

        'remuevo el documento provisionado
        Me.listaDetalleProv = getListaProvisionado()
        Me.listaDetalleProv.RemoveAt(index)
        'Si no hay datos seteo el dropbox
        If Me.listaDetalleProv.Count = 0 Then
            Dim ddlAgentesss As DropDownList = DirectCast(Me.GV_DocumentosProvisionados.HeaderRow.FindControl("ddlTipoAgenteCombo"), DropDownList)
            Dim listAgentes As New List(Of Entidades.TipoAgente)
            listAgentes = getListaAgente()
            ddlAgentesss.DataSource = listAgentes
            ddlAgentesss.DataValueField = "IdAgente"
            ddlAgentesss.DataTextField = "Nombre"
            ddlAgentesss.DataBind()
            'ddlAgentesss.Items.Insert(0, New ListItem("---", 0))
        End If

        Me.GV_DocumentosProvisionados.DataSource = Me.listaDetalleProv
        Me.GV_DocumentosProvisionados.DataBind()

        For Each row As GridViewRow In Me.GV_DocumentosProvisionados.Rows
            glosa += row.Cells(1).Text.ToString() + " / "
        Next

        setListaProvisionado(Me.listaDetalleProv)
        'qweqweqwe()
        Dim listaRequerimientosss As New List(Of Entidades.be_Requerimiento_x_pagar)
        listaRequerimientosss = Session("reqProgramar")
        Dim idmoneda As Integer = listaRequerimientosss(0).idMoneda

        If (CStr(cboTipoMoneda.SelectedItem.Text) = "S/. ") Then
            If idmoneda = 2 Then
                Me.txtMontoProg.Text = Math.Round(calcularDeudaPendiente() * CDec(Me.txtTipoCambio.Text), 3)
            End If
        Else
            Me.txtMontoProg.Text = calcularDeudaPendiente()
        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "mostrarCapaProgramacion()", True)
    End Sub

    Protected Sub btnBuscarProv_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarxNumeroVoucher.Click
        Dim serie As String = Me.txtnroSerieProvision.Text.Trim
        Dim codigo As String = Me.txtNumeroProvision.Text.Trim
        Dim fechaInicio As Date = CDate(txtDesde.Text.Trim)
        Dim fechaFin As Date = CDate(txtHasta.Text.Trim)
        Dim Voucher As String = (Me.txtNumeroVoucher.Text.Trim)
        Dim flag As Integer = Me.RadioButtonList1.SelectedItem.Value

        Dim ListaDocProvisionados As New List(Of Entidades.Documento)

        Dim nroRuc As String = Me.txtFiltroRuc.Text.Trim()
        Try
            ListaDocProvisionados = (New Negocio.blProvisiones).listarDocumentosProvisionadosV2(serie, codigo, fechaInicio, fechaFin, flag, nroRuc, Voucher)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

        Session("ListaDocProvisionados") = ListaDocProvisionados
        Me.gvProvisiones.DataSource = ListaDocProvisionados
        Me.gvProvisiones.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onCapa('capaProgramacionPago');onCapa('capaDocProvisionados');", True)
    End Sub

    Protected Sub SeleccionarGuias(ByVal sender As Object, ByVal e As EventArgs)
        Dim listaRequerimientosss As New List(Of Entidades.be_Requerimiento_x_pagar)
        listaRequerimientosss = Session("reqProgramar")

        Dim lista As New List(Of Entidades.Documento)
        'Dim newlista As New List(Of Entidades.Documento)
        Dim objetoDocumento As Entidades.Documento
        Dim indice As Integer = 0
        lista = Session("ListaDocProvisionados")
        Dim idProvision As Integer = 0
        Dim datakey As DataKey
        Dim totalAPagar As Decimal = 0

        Dim cboTipoAgente As DropDownList = CType(Me.GV_DocumentosProvisionados.HeaderRow.FindControl("ddlTipoAgenteCombo"), DropDownList)
        Dim listaTipoAgente As New List(Of Entidades.TipoAgente)
        listaTipoAgente = getListaAgente()
        If cboTipoAgente.SelectedValue <> "" Then
            If cboTipoAgente.SelectedValue <> 0 Then
                listaTipoAgente = (From item In listaTipoAgente
                     Where CInt(item.IdAgente) = cboTipoAgente.SelectedValue
                     Select item).ToList()
            End If
        End If


        Call ActualizarListaDetalleDocumento(listaTipoAgente)
        Me.listaDetalleProv = getListaProvisionado()

        'If Session("newlista") IsNot Nothing Then
        '    newlista = Session("newlista")
        'End If
        Dim glosa As String = "PAGO A PROVEEDOR "
        Dim checkbox As CheckBox
        For Each row As GridViewRow In Me.gvProvisiones.Rows
            checkbox = New CheckBox()
            checkbox = DirectCast(row.FindControl("chkSeleccionar"), CheckBox)
            datakey = gvProvisiones.DataKeys(row.RowIndex)

            If checkbox.Checked Then
                idProvision = datakey(0)
                For Each item As Entidades.Documento In lista
                    If item.IdDocumento() = idProvision Then
                        If item.NomMoneda.Trim() = listaRequerimientosss(0).nom_simbolo.Trim() Then
                            'If CDec(item.TotalAPagar) <= saldoDeudaRequerimiento And (CDec(Me.txtTotalSaldo.Text) + CDec(item.TotalAPagar)) <= saldoDeudaRequerimiento Then
                            'Descuenta el saldo de la tabla requerimiento
                            If listaRequerimientosss.Count > 0 Then
                                Me.GV_MovCuenta_CXP.DataSource = listaRequerimientosss
                                Me.GV_MovCuenta_CXP.DataBind()
                                Session("reqProgramar") = listaRequerimientosss
                            End If

                            For Each itemDestino As Entidades.Documento In listaDetalleProv
                                If itemDestino.IdDocumento = item.IdDocumento Then
                                    checkbox.Checked = False
                                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');alert('El item ya existe.')", True)
                                    Exit Sub
                                End If
                            Next

                            lista.Remove(item)
                            Me.gvProvisiones.DataSource = lista
                            Me.gvProvisiones.DataBind()

                            Me.listaDetalleProv.Add(item)
                            setListaProvisionado(listaDetalleProv)


                            For i As Integer = 0 To Me.listaDetalleProv.Count - 1
                                With listaDetalleProv(i)
                                    .montoXPagar = .montoPendiente
                                    glosa += .Codigo.Trim() + " / "
                                End With
                            Next
                            setListaProvisionado(listaDetalleProv)

                            Me.GV_DocumentosProvisionados.DataSource = listaDetalleProv
                            Me.GV_DocumentosProvisionados.DataBind()

                            Dim ddlAgentesss As DropDownList = DirectCast(Me.GV_DocumentosProvisionados.HeaderRow.FindControl("ddlTipoAgenteCombo"), DropDownList)
                            Dim listAgentes As New List(Of Entidades.TipoAgente)
                            listAgentes = getListaAgente()
                            ddlAgentesss.DataSource = listAgentes
                            ddlAgentesss.DataValueField = "IdAgente"
                            ddlAgentesss.DataTextField = "Nombre"
                            ddlAgentesss.DataBind()
                            'ddlAgentesss.Items.Insert(0, New ListItem("---", 0))
                            'SUMA 
                            'Me.txtRedondeo.Text = totalAPagar + item.TotalAPagar
                            'qweqweqwe()

                            Dim idmoneda As Integer = listaRequerimientosss(0).idMoneda

                            If (CStr(cboTipoMoneda.SelectedItem.Text) = "S/. ") Then
                                If idmoneda = 2 Then
                                    Me.txtMontoProg.Text = Math.Round(calcularDeudaPendiente() * CDec(Me.txtTipoCambio.Text), 3)
                                End If
                            Else
                                Me.txtMontoProg.Text = calcularDeudaPendiente()
                            End If
                            Exit For
                            'Else
                            '    checkbox.Checked = False
                            '    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');alert('El monto total de la provisión excede al monto total del requerimiento.')", True)
                            '    Exit Sub
                            'End If
                        Else
                            checkbox.Checked = False
                            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');alert('El tipo de moneda no coincide con el requerimiento.')", True)
                        End If
                    End If
                Next
                Exit For
            End If
        Next
        Dim sumaTotalSaldo As Decimal = 0
        For Each rowSuma As GridViewRow In Me.GV_DocumentosProvisionados.Rows
            sumaTotalSaldo += CDec(DirectCast(rowSuma.FindControl("lblTotal"), Label).Text)
        Next
        Me.txtObservaciones_Prog.Text = glosa
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');onCapa('capaDocProvisionados');", True)
    End Sub

    Protected Sub lbSeleccionar_clickAnticipo(ByVal sender As Object, ByVal e As EventArgs)
        Dim lb As LinkButton = DirectCast(sender, LinkButton)
        Dim i As Integer = CInt(lb.CommandArgument)
        Dim id As Integer = DirectCast(gvTipoAnticipos.Rows(i).FindControl("hddidConcepto"), HiddenField).Value
        Dim nombre As String = DirectCast(gvTipoAnticipos.Rows(i).FindControl("lblNombreConcepto"), Label).Text

        Dim objeto As Entidades.Concepto = New Entidades.Concepto
        Dim lista As List(Of Entidades.Concepto) = New List(Of Entidades.Concepto)
        objeto.Id = id
        objeto.Nombre = nombre
        lista.Add(objeto)
        Me.gvConceptoReferencia.DataSource = lista
        Me.gvConceptoReferencia.DataBind()
        Me.txtMontoProg.Enabled = True
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "mostrarCapaProgramacion()", True)
    End Sub

    Protected Sub lbSeleccionar_clickPorRendir(ByVal sender As Object, ByVal e As EventArgs)
        Dim lb As LinkButton = DirectCast(sender, LinkButton)
        Dim i As Integer = CInt(lb.CommandArgument)
        Dim id As Integer = DirectCast(gv_CtasxRendir.Rows(i).FindControl("hddidConcepto"), HiddenField).Value
        Dim nombre As String = DirectCast(gv_CtasxRendir.Rows(i).FindControl("lblNombreConcepto"), Label).Text

        Dim objeto As Entidades.Concepto = New Entidades.Concepto
        Dim lista As List(Of Entidades.Concepto) = New List(Of Entidades.Concepto)
        objeto.Id = id
        objeto.Nombre = nombre
        lista.Add(objeto)
        Me.gvConceptoReferencia.DataSource = lista
        Me.gvConceptoReferencia.DataBind()
        Me.txtMontoProg.Enabled = True
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "mostrarCapaProgramacion()", True)
    End Sub

    Protected Sub lbSeleccionar_clickPagoRemuneracion(ByVal sender As Object, ByVal e As EventArgs)
        Dim lb As LinkButton = DirectCast(sender, LinkButton)
        Dim i As Integer = CInt(lb.CommandArgument)
        Dim id As Integer = DirectCast(gv_pagoRemuneracion.Rows(i).FindControl("hddidConcepto"), HiddenField).Value
        Dim nombre As String = DirectCast(gv_pagoRemuneracion.Rows(i).FindControl("lblNombreConcepto"), Label).Text

        Dim objeto As Entidades.Concepto = New Entidades.Concepto
        Dim lista As List(Of Entidades.Concepto) = New List(Of Entidades.Concepto)
        objeto.Id = id
        objeto.Nombre = nombre
        lista.Add(objeto)
        Me.gvConceptoReferencia.DataSource = lista
        Me.gvConceptoReferencia.DataBind()
        Me.txtMontoProg.Enabled = True
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "mostrarCapaProgramacion()", True)
    End Sub

    Private Sub verFrm(ByVal modo As Integer)
        Dim IdDocumento As Integer = 0
        Dim index As Integer = 0
        Me.Panel_ProgramacionPago.Enabled = False
        ' Me.Panel_ProgramacionPago_Grilla.Enabled = False
        Me.btnNuevo.Visible = False
        Me.btnEditar.Visible = False
        Me.btnEliminar.Visible = False
        Me.btnGuardar.Visible = False
        Me.btnCancelar.Visible = False
        Dim MontoTotal As Decimal = 0
        Dim MontoAcumulado As Decimal = 0
        Dim MontoPagado As Decimal = 0
        Dim monedaResult As String = ""
        Dim MontoTotalResultado As Decimal = 0
        Dim hddIdProveedor As Integer = 0

        Select Case modo

            Case FrmModo.Inicio
                ''=============================
                Dim listaReq As New List(Of Entidades.be_Requerimiento_x_pagar)
                listaReq = Session("reqProgramar")
                Me.gv_REquerimientoProgramados.DataSource = Session("reqProgramar")
                Me.gv_REquerimientoProgramados.DataBind()

                Me.GV_DocumentosProvisionados.DataSource = Nothing
                Me.GV_DocumentosProvisionados.DataBind()

                Me.GV_TipoRegimen.DataSource = Nothing
                Me.GV_TipoRegimen.DataBind()

                'Me.GV_DocumentosProvisionados.Visible = True

                Me.GV_DocumentosProvisionados.Enabled = False
                txtMontoProg.Enabled = True
                Me.btnAgregarDocProvisionados.Visible = False
                Me.btnDocCuentasPorCobrar.Visible = False
                limpiarControles_Prog()
                Me.lblMoneda_MontoProg.Text = ""

                Me.btnNuevo.Visible = True
                Me.pnAdelantos.Enabled = False

                Me.ListaDocProvisionados = New List(Of Entidades.Documento)
                hddSaldoRQ.Value = "0"

            Case FrmModo.Nuevo
                limpiarControles_Prog()
                Me.Panel_ProgramacionPago.Enabled = True

                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.btnAgregarDocProvisionados.Visible = True
                Me.btnDocCuentasPorCobrar.Visible = True
                Me.pnAdelantos.Enabled = True
                Me.GV_DocumentosProvisionados.Enabled = True

                Dim objCbo As New Combo
                With objCbo
                    .LlenarCboMoneda(Me.cboTipoMoneda, True)
                End With
                Try
                    Me.txtMontoProg.Enabled = False
                Catch ex As Exception

                End Try
            Case FrmModo.Editar
                Dim NroDoc As String = ""
                txtMontoProg.Enabled = False
                Me.pnAdelantos.Enabled = True
                Me.btnAgregarDocProvisionados.Visible = True
                Me.btnDocCuentasPorCobrar.Visible = True
                Me.GV_DocumentosProvisionados.Enabled = True

                If (NroDoc.Length <= 0) Then
                    Me.Panel_ProgramacionPago.Enabled = True
                    Me.btnGuardar.Visible = True
                    Me.btnCancelar.Visible = True

                    Try
                        Dim IdDocumentox As Integer = CInt(hddIdDocumento.Value)

                        ''SelectMovCuentaporPagarxIddocumento
                        Dim objDocumento As Entidades.MovCuentaPorPagar = New Negocio.MovCuentaPorPagar().SelectMovCuentaporPagarxIddocumento(IdDocumentox)
                        Me.hddSaldoRQ.Value = CStr(objDocumento.Saldo)

                        Me.lblMoneda_MontoProg.Text = CStr(objDocumento.MonedaSimbolo)
                        'Me.txtRedondeo.Text = CStr(objDocumento.Saldo)

                        MontoTotal = CDec(objDocumento.Saldo)
                        MontoTotalResultado = CDec(objDocumento.Monto)
                        hddIdProveedor = CInt(objDocumento.IdProveedor)
                        hddIdPersona.Value = CStr(objDocumento.IdProveedor)

                        IdDocumento = CInt(hddIdDocumento.Value)

                        Dim listaRelacionDocExterno As List(Of Entidades.Documento) = New Negocio.Documento().SelectDocExtxIdRQinProgPago(IdDocumento)

                        Dim CondicionPago As String = ""
                        Dim CondicionComercial As String = ""

                        Dim listaCP As List(Of Entidades.Documento) = obtenerDocumentoRef_Cab(IdDocumento)
                        For k As Integer = 0 To listaCP.Count - 1
                            CondicionPago += listaCP(k).NomCondicionPago
                        Next

                        Dim listaCC As List(Of Entidades.CondicionComercial) = (New Negocio.Documento_CondicionC).SelectxIdDocumento(IdDocumento)
                        For k As Integer = 0 To listaCC.Count - 1
                            CondicionComercial += listaCC(k).Descripcion
                        Next

                        Me.dgvCuentaBancaria.DataSource = objNegCta.SelectcuentaBancaria(hddIdProveedor, 0, 1, 0)
                        Me.dgvCuentaBancaria.DataBind()

                    Catch ex As Exception

                    End Try
                Else
                    Me.btnNuevo.Visible = True
                    objScript.mostrarMsjAlerta(Me, "El Documento tiene un cheque referenciado. No procede la edición.")
                End If

            Case FrmModo.Seleccion

                Me.btnNuevo.Visible = True
                Me.btnEditar.Visible = True
                Me.btnEliminar.Visible = True
                Me.btnAgregarDocProvisionados.Visible = True
                Me.btnDocCuentasPorCobrar.Visible = True
                Me.pnAdelantos.Enabled = False
                Me.GV_DocumentosProvisionados.Enabled = False

                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');", True)
        End Select
        Me.hddFrmModo.Value = CStr(modo)
    End Sub

    Private Sub limpiarControles_Prog()
        'desmarca checks
        Me.chkEsAdelanto.Checked = False
        Me.chkEsCtaPorRendir.Checked = False

        Me.txtMontoProg.Text = "0"
        Me.txtObservaciones_Prog.Text = ""
        Me.txtFecha_ProgPago.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.cboBanco_Prog.SelectedIndex = -1
        Me.cboCuentaBancaria_Prog.SelectedIndex = -1
        Me.cboMedioPago_Prog.SelectedIndex = -1
        Me.cboTipoMoneda.SelectedIndex = -1

        Me.lblResultadoDolares.Visible = False
        Me.txtResultadoTipoCambio.Visible = False
        Me.btncalcularTC.Visible = False
        Me.lblTipoCambio.Visible = True
        Me.txtTipoCambio.Visible = True
        Me.txtResultadoTipoCambio.Text = "0"

    End Sub

    Private Sub llenarComboBox()
        Try
            'Me.hddIdMovCuenta_CXP.Value = CStr(IdMovCuentaCXP2)
            Dim objCbo As New Combo
            With objCbo
                .LlenarCboBanco(Me.cboBanco_Prog, 1, True)
                .LlenarCboBanco(Me.ddlBancoCancelacion, 1, True)
                .LlenarCboCuentaBancaria(Me.cboCuentaBancaria_Prog, CInt(Me.cboBanco_Prog.SelectedValue), 1, True)
                '.LlenarCboMedioPagoxIdTipoDocumento(Me.cboMedioPago_Prog, IdTipoDocumento2, True)
                .LlenarCboMoneda(Me.cboTipoMoneda, True)
            End With

            'verFrm(FrmModo.Inicio)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub mostrarCuentaBancaria(ByVal lista As List(Of Entidades.be_Requerimiento_x_pagar))
        If lista.Count = 1 Then
            Me.dgvCuentaBancaria.DataSource = objNegCta.SelectcuentaBancaria(lista(0).idProveedor, 0, 1, 0)
            Me.dgvCuentaBancaria.DataBind()
        End If
    End Sub

    Private Sub RadioButtonList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioButtonList1.SelectedIndexChanged
        Select Case Me.RadioButtonList1.SelectedValue
            Case 0
                Me.trFiltroNroDocumento.Visible = False
                Me.trFiltroFecha.Visible = True
                Me.trFiltroProveedor.Visible = False
                Me.trFiltroNroVoucher.Visible = False
            Case 1
                Me.trFiltroNroDocumento.Visible = True
                Me.trFiltroFecha.Visible = False
                Me.trFiltroProveedor.Visible = False
                Me.trFiltroNroVoucher.Visible = False
            Case 2
                Me.trFiltroNroDocumento.Visible = False
                Me.trFiltroFecha.Visible = False
                Me.trFiltroProveedor.Visible = True
                Me.trFiltroNroVoucher.Visible = False

            Case 3
                Me.trFiltroProveedor.Visible = False
                Me.trFiltroNroDocumento.Visible = False
                Me.trFiltroFecha.Visible = False
                Me.trFiltroNroVoucher.Visible = True
        End Select
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');onCapa('capaDocProvisionados');", True)
    End Sub

    Private Sub limpiarVariablesSession()
        Session("reqProgramar") = Nothing
        Session("newlista") = Nothing
        'Session("listaFacxAplicar") = Nothing
        Me.gvDocAgregados.DataSource = Nothing
        Me.gvDocAgregados.DataBind()
        Me.GV_DocumentosProvisionados.DataSource = Nothing
        Me.GV_DocumentosProvisionados.DataBind()
        Me.gv_AplicacionFacturas.DataSource = Nothing
        Me.gv_AplicacionFacturas.DataBind()
        Me.gvConceptoReferencia.DataSource = Nothing
        Me.gvConceptoReferencia.DataBind()
    End Sub

    Private Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        limpiarVariablesSession()
    End Sub

    Private Sub txtFecha_ProgPago_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFecha_ProgPago.TextChanged
        Try
            Me.txtTipoCambio.Text = (New Negocio.bl_pagoProveedor).buscarTipoCambioCompra(Me.txtFecha_ProgPago.Text)
            calcularTipoCambio()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Fecha inválida")
        Finally
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');", True)
        End Try
    End Sub

    Private Sub listarConceptosAdelanto(ByVal flag As String)
        Dim objeto As New Negocio.bl_pagoProveedor
        Select Case flag
            Case "C"
                Me.gv_CtasxRendir.DataSource = objeto.ListarcuentasPorRendir(flag)
                Me.gv_CtasxRendir.DataBind()
            Case "A"
                Me.gvTipoAnticipos.DataSource = objeto.listarConceptoAdelantos(flag)
                Me.gvTipoAnticipos.DataBind()
            Case "P"
                Me.gv_pagoRemuneracion.DataSource = objeto.listarPagoProveedor(flag)
                Me.gv_pagoRemuneracion.DataBind()
        End Select
    End Sub

    Protected Sub GV_ProgramacionPago_onlick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lb As LinkButton = DirectCast(sender, LinkButton)
        Dim index As Integer = DirectCast(lb.NamingContainer, GridViewRow).RowIndex
        cargarDatos_ProgramacionPago_CXP(index)
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "MostrarOcultar('capaMostrar');", True)
    End Sub

    Private Sub cargarDatos_ProgramacionPago_CXP(ByVal index As Integer)
        Try
            Dim objCbo As New Combo
            Dim IdProgramacionPago_CXP As Integer = CInt(CType(Me.GV_ProgramacionPago.Rows(index).FindControl("hddIdProgramacionPago_CXP"), HiddenField).Value)
            limpiarControles_Prog()
            Dim objProgramacionPago As Entidades.ProgramacionPago_CXP = (New Negocio.ProgramacionPago_CXP).SelectxIdProgramacionPago(IdProgramacionPago_CXP)

            If (objProgramacionPago IsNot Nothing) Then
                With objProgramacionPago
                    Me.chkEsAdelanto.Checked = .esAdelanto
                    Me.chkEsCtaPorRendir.Checked = .esCtaPorRendir

                    Me.hddIdProgramacionPago_CXP.Value = CStr(.IdProgramacionPago)

                    If (.FechaPagoProg <> Nothing) Then
                        Me.txtFecha_ProgPago.Text = Format(.FechaPagoProg, "dd/MM/yyyy")
                    End If
                    Me.lblMoneda_MontoProg.Text = .Moneda
                    Me.txtTipoCambio.Text = .TipoCambio
                    Me.txtMontoProg.Text = .MontoPagoProg
                    'Me.txtRedondeo.Text = CStr(Math.Round(.MontoPagoProg, 3))

                    If (Me.cboMedioPago_Prog.Items.FindByValue(CStr(.IdMedioPago)) IsNot Nothing) Then
                        Me.cboMedioPago_Prog.SelectedValue = CStr(.IdMedioPago)
                    End If

                    If (Me.cboBanco_Prog.Items.FindByValue(CStr(.IdBanco)) IsNot Nothing) Then
                        Me.cboBanco_Prog.SelectedValue = CStr(.IdBanco)
                        objCbo.LlenarCboCuentaBancaria(Me.cboCuentaBancaria_Prog, CInt(.IdBanco), CInt(1), True)
                    End If

                    If (Me.cboCuentaBancaria_Prog.Items.FindByValue(CStr(.IdCuentaBancaria)) IsNot Nothing) Then
                        Me.cboCuentaBancaria_Prog.SelectedValue = CStr(.IdCuentaBancaria)
                    End If

                    If (Me.cboTipoMoneda.Items.FindByValue(CStr(.IdMoneda)) IsNot Nothing) Then
                        Me.cboTipoMoneda.SelectedValue = CStr(.IdMoneda)
                    End If
                    Me.txtTipoCambio.Text = CStr(.TipoCambio)

                    If (.Observacion <> Nothing) Then
                        Me.txtObservaciones_Prog.Text = .Observacion
                    End If
                End With
                verFrm(FrmModo.Seleccion)
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private _indiceProgramacion As Integer

    Private Sub GV_ProgramacionPago_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GV_ProgramacionPago.RowCommand

        If gvEmisionCheques.Rows.Count = 0 Then
            Me.btnGuardarEmitirCheque.Text = "Guardar"
            Me.btnGuardarEmitirCheque.CommandName = "crear_cheque"
        End If

        If e.CommandName = "crear_cheque" Then
            Dim indice As Integer = CInt(e.CommandArgument)
            _indiceProgramacion = CInt(e.CommandArgument)
            hddIndiceProgramacion.Value = indice ' setea el hidden con el id de la fila seleccionada
            Dim datakey As DataKey = GV_ProgramacionPago.DataKeys(indice)
            Dim objCbo As New Combo

            With objCbo

                .LlenarCboBanco(Me.ddlBancoEmitirCheque, 1, True)
                Me.ddlBancoEmitirCheque.SelectedValue = datakey(0)
                .LlenarCboCuentaBancaria(Me.ddlCuentaBancariaEmitirCheque, CInt(Me.ddlBancoEmitirCheque.SelectedValue), 1, True)
                Me.ddlCuentaBancariaEmitirCheque.SelectedValue = datakey(1)
                .LlenarCboSerieChequexIdCuentaBancaria(ddlSerieNumeracionEmitirCheque, CInt(ddlCuentaBancariaEmitirCheque.SelectedValue), True)
                ddlSerieNumeracionEmitirCheque.SelectedIndex = 1

                'Carga Correlativo

                Dim x As Integer
                x = CInt(ddlSerieNumeracionEmitirCheque.SelectedValue)
                If x <> 0 Then
                    ddlNroChequeEmitirCheque.Text = (New Negocio.DocumentoCheque).GenerarDocCodigoChequexIdSerieCheque(x)
                Else
                    ddlNroChequeEmitirCheque.Text = ""
                End If
                txtMontoEmitirCheque.Text = DirectCast(GV_ProgramacionPago.Rows(indice).FindControl("lblMontoProg"), Label).Text
                txtFEchaCobroEmitirCheque.Text = GV_ProgramacionPago.Rows(indice).Cells(1).Text
                txtFechaEmisionEmitirCheque.Text = GV_ProgramacionPago.Rows(indice).Cells(1).Text

            End With

            Dim datakeyProgramacion As DataKey = GV_ProgramacionPago.DataKeys(indice)
            Dim idDocumentoProgram As Integer = datakeyProgramacion(2)
            Dim listaCheques As New List(Of Entidades.DocumentoCheque)
            listaCheques = (New Negocio.DocumentoCheque).LN_EmitirCheque(idDocumentoProgram)
            Me.gvEmisionCheques.DataSource = listaCheques
            Me.gvEmisionCheques.DataBind()
            If gvEmisionCheques.Rows.Count <> 0 Then
                Me.btnGuardarEmitirCheque.Text = "Modificar"
                Me.btnGuardarEmitirCheque.CommandName = "modificar_cheque"
            End If

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onCapa('capaProgramacionPago');onCapa('capaEmitirCheque');", True)
        ElseIf e.CommandName = "btnCancelacionPrincipal" Then
            Dim idCuentaxRendir As Boolean = False
            Dim idAdelanto As Boolean = False

            Me.tituloCelda.Text = "DATOS DE CANCELACIÓN"
            Dim indice As Integer = CInt(e.CommandArgument)
            Me.hddIdDocumentoCancelacion.Value = DirectCast(GV_ProgramacionPago.Rows(indice).FindControl("hddIdProgramacionPago"), HiddenField).Value
            Me.hddIdFormulario.Value = "CAN"
            If gvConceptoReferencia.Rows.Count > 0 Then
                idCuentaxRendir = DirectCast(gvConceptoReferencia.Rows(indice).FindControl("hddConceptoCtaxRendir"), HiddenField).Value
                idAdelanto = DirectCast(gvConceptoReferencia.Rows(indice).FindControl("hdd_hd_ConceptoAdelanto"), HiddenField).Value

                If idCuentaxRendir Then
                    txtCodigoEmpleado.Text = ""
                    txtFiltroEmpleados.Text = ""
                    tr_PersonaRendir.Visible = True
                Else
                    txtCodigoEmpleado.Text = ""
                    txtFiltroEmpleados.Text = ""
                    tr_PersonaRendir.Visible = False
                End If
                If idAdelanto Then
                    txtCodigoEmpleado.Text = ""
                    txtFiltroEmpleados.Text = ""
                    tr_PersonaRendir.Visible = False
                Else
                    txtCodigoEmpleado.Text = ""
                    txtFiltroEmpleados.Text = ""
                    tr_PersonaRendir.Visible = False
                End If
            End If

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');;onCapa('capaAgregarPago');", True)
        End If
    End Sub

    Private Sub GV_ProgramacionPago_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_ProgramacionPago.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ImageButton As ImageButton = DirectCast(e.Row.FindControl("imgbtnCrearCheque"), ImageButton)
            Dim DocumentoCheque As String = e.Row.Cells(3).Text
            If DocumentoCheque = "CHEQUE PAGADOR" Then
                ImageButton.Enabled = True
                ImageButton.Visible = True
            End If
        End If
    End Sub

    Private Sub GV_ProgramacionPago_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_ProgramacionPago.SelectedIndexChanged
        cargarDatos_ProgramacionPago_CXP(Me.GV_ProgramacionPago.SelectedIndex)
    End Sub

    Private Sub btnGuardarEmitirCheque_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardarEmitirCheque.Click

        If (chkDiferido.Checked = True Or txtFechaDiferida.Text <> "") Then
            objScript.mostrarMsjAlerta(Me, "Usted Esta Ingresando un Cheque Diferido")

            If (txtFechaDiferida.Text = "") Then
                objScript.mostrarMsjAlerta(Me, "NO EXISTE FECHA DIFERIDA , DEBE MARCAR EL CHECK VERIFIQUE")
                Exit Sub
            End If

        End If

        Me.btnGuardarEmitirCheque.CommandName = "guardar_cheque"

        If Me.btnGuardarEmitirCheque.CommandName.ToLower() = "guardar_cheque" Then
            Dim nroFilasGrillaDocReferencia As Integer = 0
            nroFilasGrillaDocReferencia = Me.GV_MovCuenta_CXP.Rows.Count
            If nroFilasGrillaDocReferencia = 0 Then
                objScript.mostrarMsjAlerta(Me, "Operación invalida, cerrar programación y volver a intentar.")
                Exit Sub
            End If
            '======================================================
            'Obtenemos la lista RelacionDocumento del Requerimiento'
            '=======================================================
            Dim datakeyRequerimiento As DataKey = Me.gv_REquerimientoProgramados.DataKeys(0)
            Dim idDocumentoReferenciaReq As Integer = datakeyRequerimiento(0) ' Id del requerimiento
            Dim listaRelacionDocumentoReq As New List(Of Entidades.RelacionDocumento)
            Dim objetoRelacionDocumentoReq As New Entidades.RelacionDocumento
            With objetoRelacionDocumentoReq
                .IdDocumento1 = idDocumentoReferenciaReq
                .IdDocumento2 = Nothing
            End With
            listaRelacionDocumentoReq.Add(objetoRelacionDocumentoReq)
            '================================================================
            'Obtenemos la lista RelacionDocumento de la programacion de pagos'
            '=================================================================
            Dim indiceProgramacion As Integer = hddIndiceProgramacion.Value
            Dim datakeyProgramacion As DataKey = GV_ProgramacionPago.DataKeys(indiceProgramacion)
            Dim idDocumentoProgram As Integer = datakeyProgramacion(2)
            Dim listaRelacionDocumentoProgram As New List(Of Entidades.RelacionDocumento)
            Dim objetoRelacionDocumentoProgram As New Entidades.RelacionDocumento
            With objetoRelacionDocumentoProgram
                .IdDocumento1 = idDocumentoProgram
                .IdDocumento2 = Nothing
            End With
            listaRelacionDocumentoProgram.Add(objetoRelacionDocumentoProgram)
            '==========================================
            'Obtengo la cantidad del importe del cheque
            '==========================================
            Dim montoProgramacion As Decimal = 0
            montoProgramacion = CDec(DirectCast(GV_ProgramacionPago.Rows(indiceProgramacion).FindControl("lblMontoProg"), Label).Text)
            '==============================================================================
            'El monto de emisión del cheque no puede ser menor ni mayor al monto programado
            '===============================================================================
            If CDec(Me.txtMontoEmitirCheque.Text) <> montoProgramacion Then
                objScript.mostrarMsjAlerta(Me, "El Monto ingresado no coincide con la programación. No procede la operación.")
                Exit Sub
            End If
            'p.Id
            If objNegDocumentocheque.ValidarNumeroCheque(0, Me.ddlSerieNumeracionEmitirCheque.SelectedValue, Me.ddlNroChequeEmitirCheque.Text) Then

                If objNegDocumentocheque.RegistrarDocumentoCheque(CType(obtenerDocumento(), Entidades.Documento), obtenerObservacion(), obtenerMovBanco(obtenerDocumento), obtenerDocumento.AnexoDoc, listaRelacionDocumentoReq, listaRelacionDocumentoProgram) <> -1 Then
                    'registra datos de cancelacion y asiento contable
                    '==================================================
                    Dim idProgramacion As Integer = DirectCast(Me.GV_ProgramacionPago.Rows(_indiceProgramacion).FindControl("hddIdProgramacionPago"), HiddenField).Value

                    Dim obj As New Negocio.bl_pagoProveedor
                    '=======================Guarda la Fecha Con la fecha que se realiza el cheque para el asiento contable ==========================
                    If (chkDiferido.Checked = True And txtFechaDiferida.Text <> "") Then
                        obj.CancelarProgramacion(idProgramacion, Me.ddlNroChequeEmitirCheque.Text, CDec(Me.txtMontoEmitirCheque.Text), _
                                            Me.ddlBancoEmitirCheque.SelectedValue, CDate(txtFechaDiferida.Text), Val(Me.txtCodigoPersonaCheque.Text))
                    End If

                    If (chkDiferido.Checked = False And txtFechaDiferida.Text = "") Then
                        obj.CancelarProgramacion(idProgramacion, Me.ddlNroChequeEmitirCheque.Text, CDec(Me.txtMontoEmitirCheque.Text), _
                                            Me.ddlBancoEmitirCheque.SelectedValue, CDate(txtFechaEmisionEmitirCheque.Text), Val(Me.txtCodigoPersonaCheque.Text))
                    End If
                    ' ===================================================
                    Dim objeto As New Negocio.ProgramacionPago_CXP
                    Dim nroVoucher As String = String.Empty

                    nroVoucher = objeto.crearAsientoContableProgramacion(0, idProgramacion, Session("IdUsuario"))

                    Dim lista As New List(Of Entidades.DocumentoCheque)
                    lista = (New Negocio.DocumentoCheque).LN_EmitirCheque(idDocumentoProgram)
                    Me.gvEmisionCheques.DataSource = lista
                    Me.gvEmisionCheques.DataBind()
                    'Ver Programacion
                    Dim idReq As Integer = DirectCast(Me.gv_REquerimientoProgramados.Rows(0).FindControl("hddIdDocumento"), HiddenField).Value
                    Call mostrarReq(idReq, 0)

                    'Me.GV_ProgramacionPago.DataBind()
                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onCapa('capaProgramacionPago');onCapa('capaEmitirCheque'); alert('Cheque Emitido. Nro Asiento: " + nroVoucher + "'); ", True)
                End If
            End If
        End If
    End Sub

    Private Function obtenerDocumento() As Entidades.DocumentoCheque
        Dim datakey_GV_MovCuenta_CXP As DataKey = Me.gv_REquerimientoProgramados.DataKeys(0)
        Dim objetoDocumento As New Entidades.DocumentoCheque
        With objetoDocumento
            .IdBeneficiario = datakey_GV_MovCuenta_CXP(1)
            .Id = 0
            .SerieCheque.IdBanco = Me.ddlBancoEmitirCheque.SelectedValue
            .SerieCheque.IdCuentaBancaria = CInt(IIf(CStr(Me.ddlCuentaBancariaEmitirCheque.SelectedValue) <> "", Me.ddlCuentaBancariaEmitirCheque.SelectedValue, 0))
            If Me.txtMontoEmitirCheque.Text <> "" Then
                .Monto = CDec(Util.decodificarTexto(txtMontoEmitirCheque.Text))
            Else
                .Monto = 0D
            End If
            .IdUsuario = CType(Session.Item("IdUsuario"), Integer)
            'p.IdSupervisor = CType(Session.Item("IdSupervisor"), Integer)

            .IdSerieCheque = CInt(Me.ddlSerieNumeracionEmitirCheque.SelectedValue)
            If Me.txtFechaEmisionEmitirCheque.Text <> "" Then
                .FechaEmision = CDate(txtFechaEmisionEmitirCheque.Text)
            Else
                .FechaEmision = Nothing
            End If
            If Me.txtFEchaCobroEmitirCheque.Text <> "" Then
                .FechaACobrar = CDate(txtFEchaCobroEmitirCheque.Text)
            Else
                .FechaACobrar = Nothing
            End If

            .FechaRegistro = Now.Date()

            .AnexoDoc.IdMedioPago = _IDMEDIOPAGO
            Dim i As Integer
            Dim listaCheques As List(Of Entidades.SerieChequeView)
            If .IdSerieCheque <> 0 Then
                listaCheques = objNegSeriecheque.SelectAll()
                listaCheques = (From item In listaCheques
                              Where (item.Id = .IdSerieCheque)
                              Select item).ToList()
                With listaCheques(0)
                    .IdMoneda = .IdMoneda
                    .Serie = .Serie
                    objetoDocumento.IdMoneda = .IdMoneda
                    objetoDocumento.Serie = .Serie
                End With
            End If

            .IdEstadoDoc = CInt(ddlEstadoDocEmitirCheque.SelectedValue)
            .IdEstadoEntrega = CInt(ddlEstadoEntregaEmitirCheque.SelectedValue)
            .IdEstadoCancelacion = CInt(ddlEstadoCancelacionEmitirCheque.SelectedValue)

            .IdTipoDocumento = _IDTIPODOCUMENTO

            If (.IdTipoDocumento = 42) Then
                .TotalLetras = (New Negocio.ALetras).Letras(Me.txtMontoEmitirCheque.Text) + "/100 "
            Else
                .TotalLetras = (New Negocio.ALetras).Letras(Me.txtMontoEmitirCheque.Text)
            End If
            ' 


            .ChequeNumero = Me.ddlNroChequeEmitirCheque.Text
            .AnexoDoc.NroOperacion = Me.txtNroVoucherEmitirCheque.Text
        End With
        Return objetoDocumento
    End Function

    Private Function obtenerObservacion() As Entidades.Observacion
        Dim objObservacion As Entidades.Observacion = Nothing

        If (Me.txtObservaciones_Prog.Text.Trim.Length > 0) Then
            objObservacion = New Entidades.Observacion
            With objObservacion
                .Id = Nothing
                'Select Case SModo
                '    Case modo_menu.Nuevo
                '        .IdDocumento = Nothing
                '    Case modo_menu.Editar
                '        .IdDocumento = SId
                'End Select
                .IdDocumento = Nothing
                .Observacion = Me.txtObservaciones_Prog.Text.Trim
            End With
        End If
        Return objObservacion
    End Function

    Private Function obtenerMovBanco(ByVal x As Entidades.DocumentoCheque, Optional ByVal actualizar As Boolean = False) As Entidades.MovBanco
        'Esta funcion genera o actualiza un movbanco.
        Dim obj As Entidades.MovBanco
        If Not actualizar Then
            obj = New Entidades.MovBanco
        Else
            Dim objDAO As New Negocio.MovBancoView
            Dim objDAOAnexo As New Negocio.Anexo_MovBanco
            Dim L As List(Of Entidades.Anexo_MovBanco)
            L = objDAOAnexo.SelectxIdDocumento(0)
            'L = objDAOAnexo.SelectxIdDocumento(SId)

            Dim i As Integer
            Dim obj1 As Entidades.MovBanco

            For i = 0 To L.Count - 1
                obj1 = objDAO.SelectxId(L(i).IdMovBanco)
                If obj1.IdConceptoMovBanco = _IDCONCEPTOMOVBANCO Then
                    Exit For
                End If
            Next
            If obj1 IsNot Nothing Then
                obj = obj1
            Else
                obj = New Entidades.MovBanco
            End If

            'obj=objDAO.SelectxId(objDAOAnexo.

        End If

        If x.IdEstadoCancelacion = 2 Then
            obj.EstadoAprobacion = True
            obj.IdSupervisor = SIdUsuario
        Else
            obj.EstadoAprobacion = False
        End If

        obj.IdBanco = x.SerieCheque.IdBanco
        obj.IdCuentaBancaria = x.SerieCheque.IdCuentaBancaria
        obj.EstadoMov = True
        obj.Factor = -1
        obj.IdPersonaRef = x.IdBeneficiario
        obj.Monto = x.Monto
        obj.IdConceptoMovBanco = _IDCONCEPTOMOVBANCO
        obj.FechaMov = Now()

        Return obj

    End Function

    Private Sub btnBuscarFac_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarFac.Click
        Dim objeto As New Entidades.be_Requerimiento_x_pagar
        Dim idProveedor As Integer = 0
        If Me.gv_REquerimientoProgramados.Rows.Count > 0 Then
            idProveedor = CInt(TryCast(Me.gv_REquerimientoProgramados.Rows(0).FindControl("hddIdProveedor"), HiddenField).Value)
        End If

        With objeto
            .idProveedor = idProveedor
            .doc_serie = Me.txtSerieAplicarFac.Text
            .doc_codigo = Me.txtCodigoAplicarFac.Text
        End With
        Dim listaAplicaciones As New List(Of Entidades.be_Requerimiento_x_pagar) 'qweqwe
        listaAplicaciones = (New Negocio.ProgramacionPago_CXP).buscarfacturasPorAplicar(objeto)
        'Me.listaFacturasxCobrar = (New Negocio.ProgramacionPago_CXP).buscarfacturasPorAplicar(objeto)
        'setListaFacturaxCobrar(Me.listaFacturasxCobrar)
        Session("listaFacxAplicar") = listaAplicaciones
        Me.gv_AplicacionFacturasDeudadPendientes.DataSource = listaAplicaciones
        Me.gv_AplicacionFacturasDeudadPendientes.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onCapa('capaProgramacionPago');mostrarCapaAplicacionFacturas(); ", True)
    End Sub

    Protected Sub evento_seleccionarFac(ByVal sender As Object, ByVal e As EventArgs)
        Dim linkButton As LinkButton = DirectCast(sender, LinkButton)
        Dim index As Integer = DirectCast(linkButton.NamingContainer, GridViewRow).RowIndex
        Dim idDocumento As Integer = DirectCast(Me.gv_AplicacionFacturasDeudadPendientes.Rows(index).FindControl("hddidDocumento"), HiddenField).Value
        Dim listaFinal As New List(Of Entidades.be_Requerimiento_x_pagar)

        If Not IsNothing(Session("docAplicados")) Then
            listaFinal = Session("docAplicados")
        End If

        Dim objetoAplicaciones As New Entidades.be_Requerimiento_x_pagar
        Dim listaFacc As New List(Of Entidades.be_Requerimiento_x_pagar)
        'listaFacc = getListaFacturaxCobrar()
        listaFacc = Session("listaFacxAplicar")
        'lista = DirectCast(Session("listaFacxAplicar"), List(Of Entidades.be_Requerimiento_x_pagar))
        listaFacc = (From item In listaFacc
                 Where item.idDocumento = idDocumento
                 Select item).ToList()

        For i As Integer = 0 To listaFacc.Count - 1
            'comparar si existe la factura en la aplicacion
            For Each row As GridViewRow In Me.gv_AplicacionFacturas.Rows
                If DirectCast(row.FindControl("hddidDocumento"), HiddenField).Value = listaFacc(i).idDocumento Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onCapa('capaProgramacionPago');alert('La factura seleccionada ya fue aplicada´.');", True)
                    Exit Sub
                End If
            Next
            'lista(i).idDocumentoReferencia = DirectCast(Me.GV_DocumentosProvisionados.Rows(hddIndiceDocExterno.Value).FindControl("hddidDocumento"), HiddenField).Value
            'lista(i).documentoReferenciado = Me.GV_DocumentosProvisionados.Rows(hddIndiceDocExterno.Value).Cells(1).Text
            'lista(i).totalAPagarDocReferenciado = CDec(DirectCast(Me.GV_DocumentosProvisionados.Rows(hddIndiceDocExterno.Value).FindControl("lblTotal"), Label).Text)
            'lista(i).monedaReferenciado = DirectCast(Me.GV_DocumentosProvisionados.Rows(hddIndiceDocExterno.Value).FindControl("lblMonedaPrincipal"), Label).Text

            'Si el elemento no existe seteo el objeto
            With objetoAplicaciones
                .idDocumento = idDocumento
                .doc_codigo = listaFacc(i).doc_codigo
                .tipoDocumento = listaFacc(i).tipoDocumento
                .doc_FechaEmision = listaFacc(i).doc_FechaEmision
                .nom_simbolo = listaFacc(i).nom_simbolo
                .mcp_Monto = listaFacc(i).mcp_Monto
                .mcp_abono = listaFacc(i).mcp_abono
                .mcp_saldo = listaFacc(i).mcp_saldo

                '.idDocumentoReferencia = lista(i).idDocumentoReferencia
                '.totalAPagarDocReferenciado = lista(i).totalAPagarDocReferenciado
                '.nom_simbolo = DirectCast(Me.gv_AplicacionFacturasDeudadPendientes.Rows(i).FindControl("lblMonedaSaldo"), Label).Text
                '.mcp_saldo = DirectCast(Me.gv_AplicacionFacturasDeudadPendientes.Rows(i).FindControl("lblTotalSaldo"), Label).Text
                '.doc_codigo = Me.gv_AplicacionFacturasDeudadPendientes.Rows(i).Cells(2).Text
                '.documentoReferenciado = lista(i).documentoReferenciado
                '.monedaReferenciado = lista(i).monedaReferenciado
                txtMontoProg.Text -= .mcp_saldo
            End With

        Next

        listaFinal.Add(objetoAplicaciones)
        Session("docAplicados") = listaFinal
        Me.gv_AplicacionFacturas.DataSource = listaFinal
        Me.gv_AplicacionFacturas.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onCapa('capaProgramacionPago');", True)

        'identificar el monto acumulado de las facturas pendientes por aplicar
        _Totalfacturasacanjear = 0
        For i As Integer = 0 To listaFinal.Count - 1
            With listaFinal(i)
                If listaFinal.Count > 0 Then
                    .doc_TotalAPagar = listaFinal(i).mcp_Monto
                    _Totalfacturasacanjear += .doc_TotalAPagar
                End If
            End With
        Next

        'identificar el monto acumulado del saldo de las facturas provisionadas
        Me.listaDetalleProv = getListaProvisionado()

        If IsNothing(Me.listaDetalleProv) Then
            Me.listaDetalleProv = New List(Of Entidades.Documento)
        End If

        Dim sumaTotalSaldo As Decimal = 0
        For i As Integer = 0 To Me.listaDetalleProv.Count - 1
            With listaDetalleProv(i)
                sumaTotalSaldo += listaDetalleProv.Item(i).montoXPagar
            End With
        Next


        txtMontoProg.Text = Math.Round(sumaTotalSaldo - _Totalfacturasacanjear, 2)

    End Sub

    Protected Sub quitarDocAplicado(ByVal sender As Object, ByVal e As EventArgs)
        Dim link As LinkButton = DirectCast(sender, LinkButton)
        Dim index As Integer = DirectCast(link.NamingContainer, GridViewRow).RowIndex
        Dim lista As New List(Of Entidades.be_Requerimiento_x_pagar)
        lista = Session("docAplicados")
        Dim saldo As Decimal = lista(index).mcp_saldo
        lista.RemoveAt(index)
        Session("docAplicados") = lista
        Me.txtMontoProg.Text += saldo
        Me.gv_AplicacionFacturas.DataSource = lista
        Me.gv_AplicacionFacturas.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onCapa('capaProgramacionPago');", True)
    End Sub

    Private Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        Me.chkEsAdelanto.Checked = False
        Me.chkEsCtaPorRendir.Checked = False
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');", True)
    End Sub

    Private Sub ImageButton6_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton6.Click
        Me.chkEsAdelanto.Checked = False
        Me.chkEsCtaPorRendir.Checked = False
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');", True)
    End Sub

    Private Sub imgBtnCerrar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnCerrar.Click
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');", True)
    End Sub

    Private _total As Decimal = 0
    Private _totalDeuda As Decimal = 0
    Private _totalXPagar As Decimal = 0
    Private _totalMontoRegimen As Decimal = 0
    Private _montoOtrosTributos As Decimal = 0
    Private _moneda As String = String.Empty

    Private Sub GV_DocumentosProvisionados_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_DocumentosProvisionados.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            _total += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Total"))
            _totalDeuda += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "montoPendiente"))
            _totalXPagar += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "montoXPagar"))
            _totalMontoRegimen += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "montoAgente"))
            _montoOtrosTributos += Convert.ToDecimal(Val(DirectCast(e.Row.FindControl("txtOtroImpuesto"), TextBox).Text)) 'Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "MontoOtroTributo"))
            _moneda = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "NomMoneda"))


            Dim ddlAgentesss As DropDownList = DirectCast(Me.GV_DocumentosProvisionados.HeaderRow.FindControl("ddlTipoAgenteCombo"), DropDownList)
            If ddlAgentesss.Items.Count = 0 Then
                Dim listAgentes As New List(Of Entidades.TipoAgente)
                listAgentes = getListaAgente()
                ddlAgentesss.DataSource = listAgentes
                ddlAgentesss.DataValueField = "IdAgente"
                ddlAgentesss.DataTextField = "Nombre"
                ddlAgentesss.DataBind()
                ddlAgentesss.SelectedValue = CStr(ViewState("valorTipoAgente"))
            End If

            Dim textbox As TextBox = DirectCast(e.Row.FindControl("txtPorcentajeAgente"), TextBox)
            Dim textbox2 As TextBox = DirectCast(e.Row.FindControl("txtMontoAgente"), TextBox)
            If ddlAgentesss.SelectedValue <> 0 Or ddlAgentesss.SelectedValue <> 5 Then
                textbox.Enabled = True
                textbox2.Enabled = True
            Else
                textbox.Enabled = False
                textbox2.Enabled = False
            End If

        ElseIf e.Row.RowType = DataControlRowType.Footer Then
            'otros tributos
            e.Row.Cells(9).Text = _moneda + " " + _montoOtrosTributos.Round(_montoOtrosTributos, 2).ToString()
            e.Row.HorizontalAlign = HorizontalAlign.Right
            e.Row.Font.Bold = True
            'Total monto regimen
            e.Row.Cells(11).Text = _moneda + " " + _totalMontoRegimen.Round(_totalMontoRegimen, 2).ToString()
            e.Row.HorizontalAlign = HorizontalAlign.Right
            e.Row.Font.Bold = True
            'Total a pagar
            e.Row.Cells(12).Text = _moneda + " " + _totalXPagar.Round(_totalXPagar, 2).ToString()
            e.Row.HorizontalAlign = HorizontalAlign.Right
            e.Row.ForeColor = Drawing.Color.Red
            e.Row.Font.Bold = True
            'total()
            e.Row.Cells(8).Text = _moneda + " " + _total.Round(_totalDeuda, 3).ToString()
            e.Row.HorizontalAlign = HorizontalAlign.Right
            e.Row.Font.Bold = True

            'total
            'e.Row.Cells(8).Text = _moneda + " " + _total.Round(_totalXPagar, 3).ToString()
            'e.Row.HorizontalAlign = HorizontalAlign.Right
            'e.Row.Font.Bold = True

            ''total
            'e.Row.Cells(13).Text = _moneda + " " + _total.Round(_totalDeuda, 3).ToString()
            'e.Row.HorizontalAlign = HorizontalAlign.Right
            'e.Row.Font.Bold = True
        End If


        'identificar el acumulado del monto a pagar
        Me.listaDetalleProv = getListaProvisionado()
        If IsNothing(Me.listaDetalleProv) Then
            Me.listaDetalleProv = New List(Of Entidades.Documento)
        End If

        Dim sumaTotalSaldo As Decimal = 0
        For i As Integer = 0 To Me.listaDetalleProv.Count - 1
            With listaDetalleProv(i)
                sumaTotalSaldo += listaDetalleProv.Item(i).montoXPagar
            End With
        Next

        _Totalfacturasacanjear = 0
        Me.listaFacturasxCobrar = Session("docAplicados")
        Dim objRelacionDocumento As Entidades.RelacionDocumento = Nothing
        If Not IsNothing(Me.listaFacturasxCobrar) Then
            For h As Integer = 0 To Me.listaFacturasxCobrar.Count - 1
                objRelacionDocumento = New Entidades.RelacionDocumento
                With objRelacionDocumento
                    .montoDeuda = listaFacturasxCobrar(h).mcp_saldo
                    _Totalfacturasacanjear += .montoDeuda
                End With
            Next
        End If


        txtMontoProg.Text = Math.Round(sumaTotalSaldo - _Totalfacturasacanjear, 2)


    End Sub

    Protected Sub cambiarTipoAgente(ByVal sender As Object, ByVal e As EventArgs)
        Dim ddl As DropDownList = DirectCast(sender, DropDownList)

        Dim listAgente As New List(Of Entidades.TipoAgente)
        ViewState("valorTipoAgente") = ddl.SelectedValue
        listAgente = getListaAgente()
        listAgente = (From item In listAgente
                             Where CInt(item.IdAgente) = ddl.SelectedValue
                             Select item).ToList()

        'For h As Integer = 0 To Me.GV_DocumentosProvisionados.Rows.Count - 1
        '    listaDetalleProv(h).MontoOtroTributo = Me.GV_DocumentosProvisionados(h)
        'Next

        Me.listaDetalleProv = getListaProvisionado()
        'qweqweqwe()
        If Not IsNothing(listaDetalleProv) Then
            If (listaDetalleProv.Count > 0) Then
                If (ddl.SelectedValue <> 0) Then
                    For i As Integer = 0 To listaDetalleProv.Count - 1
                        With listaDetalleProv(i)
                            .TasaAgente = listAgente(0).Tasa
                            .tipoRegimen = listAgente(0).tipoAgente

                            If (.tipoRegimen = 3) Then

                                .montoAgente = .montoPendiente * listAgente(0).Tasa
                            Else
                                '.montoAgente = Math.Round(.montoPendiente * listAgente(0).Tasa)
                                If (CStr(cboTipoMoneda.SelectedItem.Text) = "S/. ") Then
                                    .montoAgente = Math.Round(.montoPendiente * listAgente(0).Tasa)
                                Else
                                    .montoAgente = .montoPendiente * listAgente(0).Tasa
                                End If

                            End If


                            .MontoOtroTributo = CDec(Val(DirectCast(Me.GV_DocumentosProvisionados.Rows(i).FindControl("txtOtroImpuesto"), TextBox).Text))
                            .montoXPagar = .montoPendiente - .montoAgente + .MontoOtroTributo

                        End With
                    Next
                Else
                    For i As Integer = 0 To listaDetalleProv.Count - 1
                        With listaDetalleProv(i)
                            If listAgente.Count > 0 Then
                                .TasaAgente = Nothing
                                .montoAgente = .montoPendiente * 0
                                .tipoRegimen = listAgente(0).tipoAgente
                                .MontoOtroTributo = CDec(Val(DirectCast(Me.GV_DocumentosProvisionados.Rows(i).FindControl("txtOtroImpuesto"), TextBox).Text))
                                .montoXPagar = .montoPendiente - .montoAgente + .MontoOtroTributo
                            End If
                        End With
                    Next
                End If
            End If
        End If
        setListaProvisionado(Me.listaDetalleProv)
        Me.GV_DocumentosProvisionados.DataSource = listaDetalleProv
        Me.GV_DocumentosProvisionados.DataBind()

        Dim listaRequerimientosss As New List(Of Entidades.be_Requerimiento_x_pagar)
        listaRequerimientosss = Session("reqProgramar")
        Dim idmoneda As Integer = listaRequerimientosss(0).idMoneda

        If (CStr(cboTipoMoneda.SelectedItem.Text) = "S/. ") Then
            If idmoneda = 2 Then
                Me.txtMontoProg.Text = Math.Round(calcularDeudaPendiente() * CDec(Me.txtTipoCambio.Text), 3)
            End If
        Else
            Me.txtMontoProg.Text = calcularDeudaPendiente()
        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');", True)
    End Sub

    Private Function calcularDeudaPendiente() As Decimal

        Dim pendienteAPagar As Decimal = 0
        Dim txtPendiente As TextBox

        For Each item As GridViewRow In Me.GV_DocumentosProvisionados.Rows
            txtPendiente = DirectCast(item.FindControl("txtMontoaPagar"), TextBox)
            pendienteAPagar = pendienteAPagar + CDec(txtPendiente.Text)
        Next
        Me.hddMontoProgramado.Value = pendienteAPagar
        Return pendienteAPagar

    End Function

    Private _montoRequerimiento As Decimal = 0
    Private _montoSaldo As Decimal = 0
    Private _monedaRequerimiento As String = String.Empty

    Private Sub gv_REquerimientoProgramados_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gv_REquerimientoProgramados.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            _montoRequerimiento += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "mcp_Monto"))
            _montoSaldo += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "mcp_Saldo"))
            _monedaRequerimiento = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "nom_simbolo"))
        ElseIf e.Row.RowType = DataControlRowType.Footer Then
            'monto de requerimiento
            e.Row.Cells(6).Text = _monedaRequerimiento + " " + (Convert.ToDecimal(_montoRequerimiento.Round(_montoRequerimiento, 2))).ToString()
            e.Row.HorizontalAlign = HorizontalAlign.Right
            e.Row.Font.Bold = True
            'saldo del requeriemiento
            e.Row.Cells(7).Text = _monedaRequerimiento + " " + (Convert.ToDecimal(_montoSaldo.Round(_montoSaldo, 2))).ToString()
            e.Row.HorizontalAlign = HorizontalAlign.Right
            e.Row.Font.Bold = True
            e.Row.ForeColor = Drawing.Color.Red
        End If
    End Sub

    Private Sub ActualizarListaDetalleDocumento(Optional ByVal listaAgente As List(Of Entidades.TipoAgente) = Nothing)

        Me.listaDetalleProv = getListaProvisionado()

        If IsNothing(Me.listaDetalleProv) Then
            Me.listaDetalleProv = New List(Of Entidades.Documento)
        End If

        For i As Integer = 0 To Me.listaDetalleProv.Count - 1
            With listaDetalleProv(i)
                .TasaAgente = CDec(CType(Me.GV_DocumentosProvisionados.Rows(i).FindControl("txtPorcentajeAgente"), TextBox).Text)
                .montoAgente = CDec(CType(Me.GV_DocumentosProvisionados.Rows(i).FindControl("txtMontoAgente"), TextBox).Text)
                .MontoOtroTributo = CDec(Val(CType(Me.GV_DocumentosProvisionados.Rows(i).FindControl("txtOtroImpuesto"), TextBox).Text))
                If .montoAgente = 0 Then
                    .tipoRegimen = 0
                Else
                    If listaAgente.Count > 0 Then
                        .tipoRegimen = listaAgente(0).tipoAgente
                    Else
                        .tipoRegimen = 0
                    End If
                End If

                .montoXPagar = CDec(CType(Me.GV_DocumentosProvisionados.Rows(i).FindControl("txtMontoaPagar"), TextBox).Text)
                '.montoXPagar = CDec(CType(Me.GV_DocumentosProvisionados.Rows(i).FindControl("txtmontonuevo"), TextBox).Text)
            End With
        Next
        setListaProvisionado(listaDetalleProv)
    End Sub

    Private Sub GV_TipoRegimen_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GV_TipoRegimen.RowCommand
        If e.CommandName = "btnCancelacionDetraccion" Then
            Dim indice As Integer = CInt(e.CommandArgument)
            Me.tituloCelda.Text = "DATOS DE DETRACCIÓN"
            Me.hddIdDocumentoCancelacion.Value = DirectCast(GV_TipoRegimen.Rows(indice).FindControl("hddIdDocumento"), HiddenField).Value
            Me.hddIdFormulario.Value = "DET"
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');onCapa('capaAgregarPago');", True)
        End If
    End Sub

    Protected Sub onClickGuardarCancelacion(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardarCancelacion.Click
        Dim btn As Button = DirectCast(sender, Button)
        'Dim index As Integer = _indiceProgramacion
        'Dim idProgramacion As Integer = DirectCast(Me.GV_ProgramacionPago.Rows(index).FindControl("hddIdProgramacionPago"), HiddenField).Value
        Dim idFormulario As String = CStr(Me.hddIdFormulario.Value)

        Dim mensajeError As String = String.Empty
        Dim nroOperacion, fechaCancelado As String
        Dim idBancoCancelacion As Integer
        Dim montoCancelado As Decimal
        Dim idPersona As Integer = 0
        nroOperacion = Me.txtNroOperacion.Text.Trim()
        idBancoCancelacion = ddlBancoCancelacion.SelectedValue
        montoCancelado = Val(Me.txtMontoDeposito.Text)
        fechaCancelado = Me.txtFechaPago.Text
        idPersona = Val(Me.txtCodigoEmpleado.Text)

        Dim obj As New Negocio.bl_pagoProveedor
        Dim nroVoucher As String = String.Empty
        Try

            Dim stringMensaje As String = ""
            Dim objComprobarConexion As New Negocio.bl_FrmGenerales
            stringMensaje = objComprobarConexion.detectarConexionSOLUCONT
            If idFormulario = "CAN" Then
                If stringMensaje <> "" Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');alert('No se pudo conectar al servidor SOLUCONT. No procede la cancelación.');", True)
                    Exit Sub
                Else
                    obj.CancelarProgramacion(Me.hddIdDocumentoCancelacion.Value, nroOperacion, montoCancelado, idBancoCancelacion, fechaCancelado, idPersona)
                    Dim objeto As New Negocio.ProgramacionPago_CXP

                    nroVoucher = objeto.crearAsientoContableProgramacion(0, Me.hddIdDocumentoCancelacion.Value, Session("IdUsuario"))
                End If
            ElseIf idFormulario = "DET" Then

                Dim objeto22 As New Negocio.ProgramacionPago_CXP
                Try
                    obj.CancelarProgramacionDetraccion(Me.hddIdDocumentoCancelacion.Value, nroOperacion, montoCancelado, idBancoCancelacion, fechaCancelado)
                    nroVoucher = objeto22.crearAsientoContableProgramacion(Me.hddIdDocumentoCancelacion.Value, 0, Session("IdUsuario"))
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, mensajeError)
                End Try
            End If

            Dim idReq As Integer = DirectCast(Me.gv_REquerimientoProgramados.Rows(0).FindControl("hddIdDocumento"), HiddenField).Value
            Call mostrarReq(idReq, 0)
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');alert('Nro Asiento Contable : " + nroVoucher + "');", True)
        Catch ex As Exception
            mensajeError = ex.Message.ToString()
            Throw ex
        Finally
            objScript.mostrarMsjAlerta(Me, mensajeError)
        End Try
    End Sub

    Private Sub btnRetroceder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRetroceder.Click
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');", True)
    End Sub

    Private Sub gvProvisiones_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvProvisiones.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='Gray'")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#E6EFF0'")
        End If
    End Sub

    Private Sub btnFiltroEmpleados_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFiltroEmpleados.Click
        Dim obj As New ScriptManagerClass
        Try
            Dim objUV As New Negocio.UsuarioView
            gvFiltroEmpleados.DataSource = objUV.SelectxEstadoxNombre(Me.txtFiltroEmpleados.Text.Trim, 1)
            gvFiltroEmpleados.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');onCapa('capaAgregarPago');", True)
            If gvFiltroEmpleados.Rows.Count = 0 Then
                obj.mostrarMsjAlerta(Me, "No se hallaron registros.")
            End If
        Catch ex As Exception
            obj.mostrarMsjAlerta(Me, "Problemas en la búsqueda.")
        End Try
    End Sub

    Protected Sub SelectCliente_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim lb As LinkButton = DirectCast(sender, LinkButton)
        Dim indice As Integer = DirectCast(lb.NamingContainer, GridViewRow).RowIndex
        Me.txtCodigoEmpleado.Text = Me.gvFiltroEmpleados.Rows(indice).Cells(1).Text
        Me.txtFiltroEmpleados.Text = Me.gvFiltroEmpleados.Rows(indice).Cells(2).Text
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');onCapa('capaAgregarPago');", True)
    End Sub

    Private Sub btnBuscarChequePersona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarChequePersona.Click
        Dim obj As New ScriptManagerClass
        Try
            Dim objUV As New Negocio.UsuarioView
            gv_EmpleadosCheques.DataSource = objUV.SelectxPersonaView(Me.txtNOmbrePersonaFiltroCheque.Text.Trim)
            gv_EmpleadosCheques.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');onCapa('capaEmitirCheque');", True)
            If gv_EmpleadosCheques.Rows.Count = 0 Then
                obj.mostrarMsjAlerta(Me, "No se hallaron registros.")
            End If
        Catch ex As Exception
            obj.mostrarMsjAlerta(Me, "Problemas en la búsqueda.")
        End Try
    End Sub

    Protected Sub SelectClienteCheque_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim lb As LinkButton = DirectCast(sender, LinkButton)
        Dim indice As Integer = DirectCast(lb.NamingContainer, GridViewRow).RowIndex
        Me.txtCodigoPersonaCheque.Text = Me.gv_EmpleadosCheques.Rows(indice).Cells(1).Text
        Me.txtNOmbrePersonaFiltroCheque.Text = Me.gv_EmpleadosCheques.Rows(indice).Cells(2).Text
        Me.gv_EmpleadosCheques.DataSource = Nothing
        Me.gv_EmpleadosCheques.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');onCapa('capaEmitirCheque');", True)
    End Sub

    Public _montoAplicaciones As Decimal
    Public _sumaTotalAplicaciones As Decimal
    Private Sub gv_AplicacionFacturas_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gv_AplicacionFacturas.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            _montoAplicaciones += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "mcp_saldo"))
        ElseIf e.Row.RowType = DataControlRowType.Footer Then
            e.Row.Cells(7).Text = _montoAplicaciones
            e.Row.HorizontalAlign = HorizontalAlign.Right
            e.Row.ForeColor = Drawing.Color.Red
            e.Row.Font.Bold = True
        End If
    End Sub

    Private Sub ImageButton5_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton5.Click
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');", True)
    End Sub

    Private Sub ImageButton7_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageButton7.Click
        Me.chkEsAdelanto.Checked = False
        Me.chkEsCtaPorRendir.Checked = False
        Me.chkPagoProveedor.Checked = False
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');", True)
    End Sub

    Protected Sub chkDiferido_CheckedChanged(sender As Object, e As EventArgs) Handles chkDiferido.CheckedChanged
        If (chkDiferido.Checked = True) Then
            lblchequediferido.Text = "Fecha de Generacion de Asiento"

        End If
        If (chkDiferido.Checked = False) Then
            lblchequediferido.Text = ""
            txtFechaDiferida.Text = ""
        End If
    End Sub


    Protected Sub btnGenDocRetencion_Click(sender As Object, e As EventArgs) Handles btnGenDocRetencion.Click
        Dim obj As New ScriptManagerClass
        If (txtFecha_ProgPagoRetencion.Text <> "") Then

            Dim idRequerimientoCadena As String = String.Empty
            Dim lista As New List(Of Entidades.be_Requerimiento_x_pagar)
            lista = Session("reqProgramar")
            For i As Integer = 0 To lista.Count - 1
                idRequerimientoCadena += lista(i).idDocumento.ToString() + ","
            Next
            Dim IdUsuario As Integer
            IdUsuario = CType(Session.Item("IdUsuario"), Integer)
            Dim FechaPAgo As String

            FechaPAgo = txtFecha_ProgPagoRetencion.Text
            If (FechaPAgo = "") Then
                obj.mostrarMsjAlerta(Me, "Debe Ingresar la Fecha en que se realizo el pago.")
                Return
            End If

            idRequerimientoCadena = idRequerimientoCadena.Remove(idRequerimientoCadena.Length - 1)
            Dim objeto As New Entidades.be_programacionPagos
            objeto = (New Negocio.ProgramacionPago_CXP).SelectxIdRetenciones(idRequerimientoCadena, IdUsuario, FechaPAgo)


            Dim objetoDocumento As New Entidades.be_programacionPagos
            objetoDocumento = (New Negocio.ProgramacionPago_CXP).InsertDocumentosSolucont(IdUsuario, FechaPAgo, idRequerimientoCadena)

            '@idpersona @idDocumento @fechaPago @IdStringProgramacion 

            Call mostrarlistaregimen(idRequerimientoCadena)
        End If


        If (txtFecha_ProgPagoRetencion.Text = "") Then
            obj.mostrarMsjAlerta(Me, "Debe Ingresar la Fecha en que se realizo el pago.")
        End If


        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", " onCapa('capaProgramacionPago'); ", True)
    End Sub

    Protected Sub txtFecha_ProgPagoRetencion_TextChanged(sender As Object, e As EventArgs) Handles txtFecha_ProgPagoRetencion.TextChanged

        Try
          
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Fecha inválida")
        Finally
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');", True)
        End Try

    End Sub

    Protected Sub btnImprimirRetencion_Click(sender As Object, e As EventArgs) Handles btnImprimirRetencion.Click

        Dim bl As New Negocio.bl_estructuraFacturacion
        Try
            Dim IdDocumento As Integer
            IdDocumento = 2102986299
            IdDocumento = 0
            bl.fun_GENERAR_PRINT_RETENCION_ELECTRONICA(IdDocumento)
            'SP_RETENCIONES_V1()
        Catch ex As Exception
            Throw ex
        End Try


    End Sub



    Protected Sub chkPagoParcial_CheckedChanged(sender As Object, e As EventArgs) Handles chkPagoParcial.CheckedChanged
        Dim obj As New ScriptManagerClass

        If chkPagoParcial.Checked = True Then

        End If

        If chkPagoParcial.Checked = False Then

            'Dim ddl As DropDownList = DirectCast(sender, DropDownList)

            'Dim listAgente As New List(Of Entidades.TipoAgente)
            'ViewState("valorTipoAgente") = ddl.SelectedValue
            'listAgente = getListaAgente()
            'listAgente = (From item In listAgente
            '                     Where CInt(item.IdAgente) = ddl.SelectedValue
            '                     Select item).ToList()

            'Dim mto As Decimal

            'Me.listaDetalleProv = getListaProvisionado()

            'If Not IsNothing(listaDetalleProv) Then
            '    If (listaDetalleProv.Count > 0) Then
            '        If (ddl.SelectedValue <> 0) Then
            '            For i As Integer = 0 To listaDetalleProv.Count - 1
            '                With listaDetalleProv(i)
            '                    If (chkPagoParcial.Checked = False) Then

            '                        .TasaAgente = listAgente(0).Tasa
            '                        .tipoRegimen = listAgente(0).tipoAgente
            '                        .montoAgente = .montoPendiente * listAgente(0).Tasa
            '                        .MontoOtroTributo = CDec(Val(DirectCast(Me.GV_DocumentosProvisionados.Rows(i).FindControl("txtOtroImpuesto"), TextBox).Text))
            '                        .montoXPagar = .montoPendiente - .montoAgente + .MontoOtroTributo

            '                    End If
            '                    If (chkPagoParcial.Checked = True) Then

            '                        .TasaAgente = listAgente(0).Tasa
            '                        .tipoRegimen = listAgente(0).tipoAgente
            '                        .MontoOtroTributo = CDec(Val(DirectCast(Me.GV_DocumentosProvisionados.Rows(i).FindControl("txtOtroImpuesto"), TextBox).Text))
            '                        .montoAgente = .MontoOtroTributo * listAgente(0).Tasa
            '                        .montoXPagar = .MontoOtroTributo - .montoAgente

            '                    End If


            '                End With
            '            Next
            '        Else
            '            For i As Integer = 0 To listaDetalleProv.Count - 1
            '                With listaDetalleProv(i)
            '                    If listAgente.Count > 0 Then
            '                        If (chkPagoParcial.Checked = False) Then
            '                            .TasaAgente = Nothing
            '                            .montoAgente = .montoPendiente * 0
            '                            .tipoRegimen = listAgente(0).tipoAgente
            '                            .MontoOtroTributo = CDec(Val(DirectCast(Me.GV_DocumentosProvisionados.Rows(i).FindControl("txtOtroImpuesto"), TextBox).Text))
            '                            .montoXPagar = .montoPendiente - .montoAgente + .MontoOtroTributo
            '                        End If
            '                        If (chkPagoParcial.Checked = True) Then
            '                            .TasaAgente = Nothing
            '                            .montoAgente = .MontoOtroTributo * 0
            '                            .tipoRegimen = listAgente(0).tipoAgente
            '                            .MontoOtroTributo = CDec(Val(DirectCast(Me.GV_DocumentosProvisionados.Rows(i).FindControl("txtOtroImpuesto"), TextBox).Text))
            '                            .montoXPagar = .MontoOtroTributo - .montoAgente
            '                        End If


            '                    End If
            '                End With
            '            Next
            '        End If
            '    End If
            'End If




        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');", True)


    End Sub

    'Protected Sub btnAnularChequeE_Click(sender As Object, e As EventArgs) Handles btnAnularChequeE.Click

    '    ''Me.hddIdDocumentoCancelacion.Value = DirectCast(GV_ProgramacionPago.Rows(indice).FindControl("hddIdProgramacionPago"), HiddenField).Value
    '    Me.btnGuardarEmitirCheque.CommandName = "guardar_cheque"


    '    Dim nroFilasGrillaDocReferencia As Integer = 0
    '    nroFilasGrillaDocReferencia = Me.GV_MovCuenta_CXP.Rows.Count
    '    If nroFilasGrillaDocReferencia = 0 Then
    '        objScript.mostrarMsjAlerta(Me, "Operación invalida, cerrar programación y volver a intentar.")
    '        Exit Sub
    '    End If


    'End Sub


End Class