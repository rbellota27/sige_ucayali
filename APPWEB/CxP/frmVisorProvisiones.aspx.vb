﻿Public Class frmVisorProvisiones
    Inherits System.Web.UI.Page

    Protected Sub btnbuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnbuscar.Click
        Dim list As List(Of Entidades.Programacion)
        Dim fechaInicio As String = Me.txtFechaInicio.Text
        Dim fechaFinal As String = Me.TextBox1.Text
        Dim estadoProvision As Integer = rbEstadoDocumento.SelectedValue
        Dim proveedor As String = Me.txtProveedorFiltro.Text.Trim
        Dim nroDocumento As String = Me.txtNroDocumentoFiltro.Text.Trim

        list = (New Negocio.bl_pagoProveedor).listarDeudasPendientes(fechaInicio, fechaFinal, estadoProvision, proveedor, nroDocumento)

        gv_visorProvisiones.DataSource = list
        gv_visorProvisiones.DataBind()
    End Sub

    Private Sub gv_visorProvisiones_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gv_visorProvisiones.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim estado As String = DataBinder.Eval(e.Row.DataItem, "ESTADO").ToString()
            If estado.ToUpper = "CANCELADO" Then
                e.Row.BackColor = Drawing.Color.LightGreen
            ElseIf estado.ToUpper = "PENDIENTE" Then
                e.Row.BackColor = Drawing.Color.LightYellow
            End If
        End If
    End Sub
End Class