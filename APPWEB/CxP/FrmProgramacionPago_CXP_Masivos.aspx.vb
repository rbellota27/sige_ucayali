﻿Imports Entidades
Imports Negocio
Imports general.librerias.serializador
Imports System.Linq

Partial Public Class FrmProgramacionPago_CXP_Masivos
    Inherits System.Web.UI.Page
    Private objNegDocumentocheque As New Negocio.DocumentoCheque
    Private objScript As New ScriptManagerClass
    Private objNegCta As New Negocio.CuentaBancaria
    Private listaDetalleProv As New List(Of Entidades.Documento)
    Private listaRequerimiento As New List(Of Entidades.be_Requerimiento_x_pagar)

    Private listaAgentes As New List(Of Entidades.TipoAgente)
    Private listaProvisionesRelacionados As List(Of Entidades.Documento) = Nothing
    Private objNegSeriecheque As New Negocio.SerieChequeView
    Private listaDocProvisionados As List(Of Entidades.Documento)
    Private IdTipoDocumento As Integer = 0
    Private listaFacturasxCobrar As New List(Of Entidades.be_Requerimiento_x_pagar)
    Private Util As New Util
    'Este medio de Pago esta asociado a los cheques generados por la empresa (egresos)
    Private Const _IDMEDIOPAGO As Integer = 11
    Private Const _IDTIPODOCUMENTO As Integer = 42
    Private Const _IDCONCEPTOMOVBANCO As Integer = 13


    Public ReadOnly Property SIdUsuario() As Integer
        Get
            Return CType(Session.Item("IdUsuario"), Integer)
        End Get
    End Property
  

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim flag As String = Request.QueryString("flag")
        Dim Usuario As Integer = CInt(Session("IdUsuario"))
        Dim nroVoucher As String = String.Empty
        Me.TxtFechaInicio.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.TxtFechaFin.Text = Me.TxtFechaInicio.Text

        'If flag = Nothing Then
        '    flag = "llenarCombo"
        'End If
        Select Case flag

            Case "llenarCombo"
                Dim IdEstado As String = Request.QueryString("IdEstadoc")
                Dim rpta As String = ""

                Dim lista As New List(Of be_Bancos)
                Try
                    lista = (New bl_Bancos).listarBancos(Convert.ToInt32(IdEstado))

                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try

                If lista.Count > 0 Then
                    Try
                        rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try
                    Response.Write(rpta)
                    Response.End()
                Else
                    Response.Write(rpta)
                    Response.End()
                End If


            Case "BuscarRequerimientosProgramados"
                '     llenarComboBox()
                Dim txtFechaInicio As String = Request.QueryString("FechaInicio")
                Dim txtFechaFinal As String = Request.QueryString("FechaFin")
                Dim lista As New List(Of be_RequerimientoProgramados)
                Dim rpta As String = ""

                Try
                    lista = (New bl_RequerimientosProgramados).listarprogramacionesEstado(txtFechaInicio, txtFechaFinal, 0, Usuario)
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try
                If lista.Count > 0 Then
                    Try
                        rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try
                    Response.Write(rpta)
                    Response.End()
                Else
                    Response.Write(rpta)
                    Response.End()
                End If

            Case "UpdateProgramacionFecha"
                Dim txtFechaInicio As String = Request.QueryString("FechaInicio")
                Dim txtFechaFinal As String = Request.QueryString("FechaFin")
                Dim txtFechaCambio As String = Request.QueryString("FechaCambio")
                Dim iddoc As String = Request.QueryString("iddoc")
                Dim iddocumento As Integer
                iddocumento = Convert.ToInt32(iddoc)
                Dim lista As New List(Of be_RequerimientoProgramados)
                Dim lista2 As New List(Of be_MontoProgramado)
                Dim rpta As String = ""
                Dim rpta2 As String = ""
                Try
                    lista = (New bl_RequerimientosProgramados).listarprogramacionesEstadoReprogramado(txtFechaInicio, txtFechaFinal, txtFechaCambio, iddocumento)
                    ' lista2 = (New bl_RequerimientosProgramados).listarmonto(txtFechaInicio, txtFechaFinal, iddocumento)  -- Reprogramados
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try
                If lista.Count > 0 Then
                    Try
                        rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                        rpta2 = (New objeto).SerializarLista(lista2, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    Me.hddMontoMarcados.Value = Convert.ToString(rpta2)
                    'Me.txtmontot.Text = Convert.ToString(rpta2)

                    Response.Write(rpta)
                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('CapaFiltros');", False)
                    Response.End()


                Else
                    Response.Write(rpta)
                    Response.End()
                End If


            Case "UpdateRequerimiento"
                Dim txtFechaInicio As String = Request.QueryString("FechaInicio")
                Dim txtFechaFinal As String = Request.QueryString("FechaFin")
                Dim iddoc As String = Request.QueryString("iddoc")
                Dim iddocumento As Integer
                iddocumento = Convert.ToInt32(iddoc)
                Dim lista As New List(Of be_RequerimientoProgramados)
                Dim lista2 As New List(Of be_MontoProgramado)
                Dim rpta As String = ""
                Dim rpta2 As String = ""
                Try
                    Dim IdUsuario As Integer
                    IdUsuario = Convert.ToInt32(Session.Item("IdUsuario"))

                    lista = (New bl_RequerimientosProgramados).listarprogramacionesEstado(txtFechaInicio, txtFechaFinal, iddocumento, IdUsuario)
                    lista2 = (New bl_RequerimientosProgramados).listarmonto(txtFechaInicio, txtFechaFinal, iddocumento, IdUsuario)
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try
                If lista.Count > 0 Then
                    Try
                        rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                        rpta2 = (New objeto).SerializarLista(lista2, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    Me.hddMontoMarcados.Value = Convert.ToString(rpta2)
                    'Me.txtmontot.Text = Convert.ToString(rpta2)

                    Response.Write(rpta)
                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('CapaFiltros');", False)
                    Response.End()


                Else
                    Response.Write(rpta)
                    Response.End()
                End If

                '

            Case "ObtenerMontoProgramado"
                Dim txtFechaInicio As String = Request.QueryString("FechaInicio")
                Dim txtFechaFinal As String = Request.QueryString("FechaFin")
                Dim iddoc As String = Request.QueryString("iddoc")
                Dim iddocumento As Integer
                iddocumento = Convert.ToInt32(iddoc)
                Dim lista As New List(Of be_RequerimientoProgramados)
                Dim lista2 As New List(Of be_MontoProgramado)
                Dim rpta As String = ""
                Dim rpta2 As String = ""
                Try
                    '  lista = (New bl_RequerimientosProgramados).listarprogramacionesEstado(txtFechaInicio, txtFechaFinal, iddocumento)
                    lista2 = (New bl_RequerimientosProgramados).listarmonto(txtFechaInicio, txtFechaFinal, iddocumento, Usuario)
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try
                If lista2.Count > 0 Then
                    Try
                        rpta = (New objeto).SerializarLista(lista2, "|", "▼", False, "", False)

                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    'Me.hddMontoMarcados.Value = Convert.ToString(rpta2)
                    'Me.txtmontot.Text = Convert.ToString(rpta2)

                    Response.Write(rpta)

                    Response.End()


                Else
                    Response.Write(rpta)
                    Response.End()
                End If



            Case "GuardarCancelacionMasiva"
                Dim txtFechaInicio As String = Request.QueryString("FechaInicio")
                Dim txtFechaFinal As String = Request.QueryString("FechaFin")
                Dim iddoc As String = Request.QueryString("iddoc")
                Dim idBancoCanc As String = Request.QueryString("idBancoCanc")

                Dim nrooperacion As String = Request.QueryString("nrooperacion")
                Dim montocancelado As String = Request.QueryString("montocancelado")
                Dim FechaCancelacion As String = Request.QueryString("FechaCancelacion")

                Dim iddocumento As Integer
                iddocumento = Convert.ToInt32(iddoc)
                Dim lista As New List(Of be_RequerimientoProgramados)
                Dim lista2 As New List(Of be_MontoProgramado)
                Dim rpta As String = ""
                Dim rpta2 As String = ""

                Dim mensajeError As String = String.Empty
                Dim montoCanceladot As Decimal
                Dim idPersona As Integer = 0
                montoCanceladot = Convert.ToDecimal(montocancelado)

                idPersona = Val(Me.txtCodigoEmpleado.Text)
                Try
                    Dim obj As New Negocio.bl_pagoProveedor
                    Dim IdUsuario As Integer
                    IdUsuario = Convert.ToInt32(Session.Item("IdUsuario"))
                    Dim objeto22 As New Negocio.bl_pagoProveedor

                    If FechaCancelacion <> "" Then
                        nroVoucher = objeto22.CancelarProgramacionMasivos(txtFechaInicio, txtFechaFinal, 0, nrooperacion, montocancelado, Convert.ToInt32(idBancoCanc), FechaCancelacion, idPersona, IdUsuario)
                    End If

                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try
                If nroVoucher <> "" Then
                    Try
                        rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)

                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    Response.Write(nroVoucher)

                    Response.End()


                Else
                    Response.Write(rpta)
                    Response.End()
                End If

        End Select



    End Sub


    Private Sub llenarComboBox()
        Try

            Dim objCbo As New Combo
            With objCbo

                .LlenarCboBanco(Me.ddlBancoCancelacion, 1, True)
             
            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Public Sub LlenarCboBanco(ByVal cbo As DropDownList, ByVal IdEmpresa As Integer, Optional ByVal addElement As Boolean = False)
        With cbo
            Dim lista As New List(Of Entidades.Banco)
            lista = (New Negocio.CuentaBancaria).SelectCboBanco(IdEmpresa)
            .DataSource = lista
            .DataValueField = "Id"
            .DataTextField = "Nombre"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    'Protected Sub onClickGuardarCancelacion(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardarCancelacion.Click
    '    Dim btn As Button = DirectCast(sender, Button)
    '    'Dim index As Integer = _indiceProgramacion
    '    'Dim idProgramacion As Integer = DirectCast(Me.GV_ProgramacionPago.Rows(index).FindControl("hddIdProgramacionPago"), HiddenField).Value
    '    Me.hddIdFormulario.Value = "CAN"
    '    Dim idFormulario As String = CStr(Me.hddIdFormulario.Value)

    '    Dim mensajeError As String = String.Empty
    '    Dim nroOperacion, fechaCancelado As String
    '    Dim idBancoCancelacion As Integer
    '    Dim montoCancelado As Decimal
    '    Dim idPersona As Integer = 0
    '    nroOperacion = Me.txtNroOperacion.Text.Trim()
    '    idBancoCancelacion = ddlBancoCancelacion.SelectedValue
    '    montoCancelado = Val(Me.txtMontoDeposito.Text)
    '    fechaCancelado = Me.txtFechaPago.Text
    '    idPersona = Val(Me.txtCodigoEmpleado.Text)

    '    Dim obj As New Negocio.bl_pagoProveedor
    '    Dim nroVoucher As String = String.Empty
    '    Try

    '        Dim stringMensaje As String = ""
    '        Dim objComprobarConexion As New Negocio.bl_FrmGenerales
    '        stringMensaje = objComprobarConexion.detectarConexionSOLUCONT
    '        If idFormulario = "CAN" Then
    '            If stringMensaje <> "" Then
    '                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');alert('No se pudo conectar al servidor SOLUCONT. No procede la cancelación.');", True)
    '                Exit Sub
    '            Else
    '                Dim IdUsuario As Integer
    '                IdUsuario = Convert.ToInt32(Session.Item("IdUsuario"))
    '                obj.CancelarProgramacionMasivos(Me.TxtFechaInicio.Text, Me.TxtFechaFin.Text, 0, nroOperacion, montoCancelado, idBancoCancelacion, fechaCancelado, idPersona, IdUsuario)
    '                Dim objeto As New Negocio.ProgramacionPago_CXP
    '                '    nroVoucher = objeto.crearAsientoContableProgramacion(0, Me.hddIdDocumentoCancelacion.Value, Session("IdUsuario"))

    '            End If
    '        ElseIf idFormulario = "DET" Then

    '            Dim objeto22 As New Negocio.ProgramacionPago_CXP
    '            Try
    '                obj.CancelarProgramacionDetraccion(Me.hddIdDocumentoCancelacion.Value, nroOperacion, montoCancelado, idBancoCancelacion, fechaCancelado)

    '                nroVoucher = objeto22.crearAsientoContableProgramacion(Me.hddIdDocumentoCancelacion.Value, 0, Session("IdUsuario"))
    '            Catch ex As Exception
    '                objScript.mostrarMsjAlerta(Me, mensajeError)
    '            End Try
    '        End If
    '        'Se comento para probar 11/08/2017
    '        'Dim idReq As Integer = DirectCast(Me.gv_REquerimientoProgramados.Rows(0).FindControl("hddIdDocumento"), HiddenField).Value
    '        'Call mostrarReq(idReq, 0)
    '        '   ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaProgramacionPago');alert('Nro Asiento Contable : " + nroVoucher + "');", True)
    '    Catch ex As Exception
    '        mensajeError = ex.Message.ToString()
    '        Throw ex
    '    Finally
    '        objScript.mostrarMsjAlerta(Me, mensajeError)
    '    End Try
    'End Sub


End Class