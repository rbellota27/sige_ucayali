﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmProgramacionPago_CXP.aspx.vb" Inherits="APPWEB.FrmProgramacionPago_CXP" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td colspan="2" class="TituloCelda">
                PROGRAMACIÓN DE PAGOS
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto" style="text-align: right; font-weight: bold">
                            Empresa:
                        </td>
                        <td colspan="5">
                            <table>
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Texto" style="text-align: right; font-weight: bold">
                                        Tienda:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboTienda" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Texto" style="text-align: right; font-weight: bold">
                                        Moneda:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboMoneda" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                      <td style="text-align: right; font-weight: bold" class="Texto">
                           Tipo de Búsqueda:
                        </td>
                        <td>
                        <asp:RadioButtonList runat="server" ID="rdTipoBusqueda" RepeatDirection="Horizontal"
                        CssClass="Texto" AutoPostBack="true" >
                        <asp:ListItem Value="0" Text="Por Fecha"  Selected="True"></asp:ListItem>
                        <asp:ListItem Value="1" Text="Por Documento" ></asp:ListItem>
                        </asp:RadioButtonList> 
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; font-weight: bold" class="Texto">
                              <asp:Label id="lblfechavctobus" runat="server" Text="Fecha Vcto.:"></asp:Label> 
                                                <asp:Label id="lbldocumentobus" runat="server" Text="Documento:"></asp:Label> 
                        </td>
                      <td>
                      <table>
                      <tr>
                             <td colspan="5">
                            <table>
                                <tr>
                                    <td style="width: 20px">
                                        &nbsp;
                                    </td>
                                    <td class="Texto" style="font-weight: bold">
                                       <asp:Label id="lblfechainiciobus" runat="server" Text="Inicio:"></asp:Label> 
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFechaVcto_Inicio" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                                            Width="100px"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="txtFechaVcto_Inicio_MaskedEditExtender" runat="server"
                                            ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                            CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                            CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaVcto_Inicio">
                                        </cc1:MaskedEditExtender>
                                        <cc1:CalendarExtender ID="txtFechaVcto_Inicio_CalendarExtender" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" TargetControlID="txtFechaVcto_Inicio">
                                        </cc1:CalendarExtender>
                                    </td>
                                    <td class="Texto" style="font-weight: bold">
                                       
                                          <asp:Label id="lblfechafinbus" runat="server" Text=" Fin:"></asp:Label> 
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFechaVcto_Fin" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                                            Width="100px"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="txtFechaVcto_Fin_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                            CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                            CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaVcto_Fin">
                                        </cc1:MaskedEditExtender>
                                        <cc1:CalendarExtender ID="txtFechaVcto_Fin_CalendarExtender" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" TargetControlID="txtFechaVcto_Fin">
                                        </cc1:CalendarExtender>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="text-align: right; font-weight: bold" class="Texto">
                         <asp:Label ID="lblseriebus" runat="server" Text="  Serie:"></asp:Label> </td>
                        <td>
                            <asp:Panel ID="Panel4" runat="server" DefaultButton="btnAceptar_ViewRpt">
                       <asp:TextBox ID="txtserie" runat="server" Width="100px"></asp:TextBox>
                       </asp:Panel>
                        </td>
                        
                        <td style="text-align: right; font-weight: bold" class="Texto">
                           <asp:Label ID="lblcodigobus" runat="server" Text="Código:"></asp:Label> 
                        </td>
                        <td> 
                        <asp:Panel ID="Panel5" runat="server" DefaultButton="btnAceptar_ViewRpt">
                        <asp:TextBox ID="txtcodigo" runat="server" Width="100px"></asp:TextBox> 
                        </asp:Panel>
                        </td>
                      </tr>
                      
                      </table>
                      </td>
                    
                        
                        
                    </tr>
                     <tr>
                        <td style="text-align: right; font-weight: bold" class="Texto">
                            Deudas:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboFiltro_Opcion_Deuda" runat="server" Width="62%" Font-Bold="true">
                                <asp:ListItem Value="0" Selected="True">Deudas Pendientes</asp:ListItem>
                                <asp:ListItem Value="1">Deudas Canceladas</asp:ListItem>
                                <asp:ListItem Value="2">Todos</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; font-weight: bold" class="Texto">
                            Programación:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboFiltro_Opcion_Programacion" runat="server" Width="62%"
                                Font-Bold="true">
                                <asp:ListItem Value="0" Selected="True">Sin Programar</asp:ListItem>
                                <asp:ListItem Value="1">Programados</asp:ListItem>
                                <asp:ListItem Value="2">Todos</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                    <td style="text-align: right; font-weight: bold" class="Texto">
                    Tipo Documento:
                    </td>
                    <td><asp:DropDownList ID="cboTipoDocumento" runat="server" Width="62%" Font-Bold="true" ></asp:DropDownList> 
                    </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; font-weight: bold" class="Texto">
                            Descripción:
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtDescripcionPersona" runat="server" CssClass="TextBox_Fecha" ReadOnly="true"
                                            Width="350px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnBuscar_Persona" runat="server" Text="Buscar Prov." ToolTip="Buscar Proveedor / Beneficiario"
                                            Width="85px" OnClientClick=" return(  valOnClick_btnBuscar_Persona() ); " />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnLimpiar_Persona" runat="server" Text="Limpiar" Width="70px" />
                                    </td>
                                </tr>
                                
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <asp:Panel ID="pnlDocAgreagados" runat="server">
                <table style="width:100%">
                    <tr>
                        <td class="SubTituloCelda">
                            <span>PROGRAMAR</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:LinkButton ID="lbProgramar" runat="server" Text="Programar"></asp:LinkButton>
                            <span style="font-weight:bold">|</span>
                            <asp:LinkButton ID="lbCancelar" runat="server" Text="Nuevo" ></asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="gvDocAgregados" runat="server" AutoGenerateColumns="false" 
                                ShowFooter="True">
                                <RowStyle CssClass="GrillaRow" />
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbQuitar" runat="server" Text="Quitar" OnClick="quitarDocAgregadoxProgramar"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" HeaderText="Nro. Documento" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNroDocumento" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                        <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                        <asp:HiddenField ID="hddIdMovCuenta_CXP" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMovCtaPP")%>' />
                                        <asp:HiddenField ID="hddIdTipoDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoDocumento")%>' />
                                        <asp:HiddenField ID="hddIdUsuario" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUsuario")%>' />
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" HeaderText="Proveedor" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblProveedor" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"Proveedor")%>'></asp:Label>
                                        <asp:HiddenField ID="hddIdProveedor" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProveedor")%>' />
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" HeaderText="Monto" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                                <asp:Label ID="lblMoneda_Monto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"nom_simbolo")%>'></asp:Label>
                                                <asp:Label ID="lblMonto" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"mcp_Monto","{0:F3}")%>'
                                                    Font-Bold="true"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <span style="text-align:right;vertical-align:middle">
                                                <asp:Label ID="lblMoneda_Monto" runat="server" Font-Bold="true" ForeColor="White" Text='<%#DataBinder.Eval(Container.DataItem,"nom_simbolo")%>'></asp:Label>
                                                <asp:Label ID="lblMontoTotalAgregados" runat="server" Text="0" ForeColor="White" Font-Bold="true" ></asp:Label>
                                            </span>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                        <td align="center" >
                            <asp:Button ID="btnAceptar_ViewRpt" runat="server" Text="Buscar Documentos" Width="200px" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel ID="Panel_MovCuenta_CxP" runat="server" Height="300px" ScrollBars="Vertical">
                    <asp:GridView ID="GV_MovCuenta_CXP" runat="server" AutoGenerateColumns="False" Width="100%"
                        PageSize="50">
                        <HeaderStyle CssClass="GrillaHeader" />
                        <Columns>
<%--                            <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" SelectText="Agregar" ShowSelectButton="True" />--%>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbAgregarReq" runat="server" Text="Agregar" OnClick="GV_MovCuenta_CXP_oncLick" 
                                    Visible='<%# if(eval("mcp_Saldo") <> 0 and eval("mcp_Saldo") = eval("mcp_Monto"),true,false) %>'></asp:LinkButton>
                                    <asp:LinkButton ID="lbVerRequerimiento" runat="server" Text="Ver Programación" OnClick="GV_MovCuenta_CXP_VerProgramacion_onClick" 
                                    Visible='<%# if((eval("mcp_Saldo") <> 0 and eval("mcp_Saldo") <> eval("mcp_Monto")) or eval("mcp_Saldo") = 0,true,false) %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="TipoDocumento" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Documento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red" ItemStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Nro. Documento" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblNroDocumento" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                    <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                    <asp:HiddenField ID="hddIdMovCuenta_CXP" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMovCtaPP")%>' />
                                    <asp:HiddenField ID="hddIdTipoDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoDocumento")%>' />
                                    <asp:HiddenField ID="hddIdUsuario" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUsuario")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                               <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Proveedor" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                   <asp:Label ID="lblProveedor" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"Proveedor")%>'></asp:Label>
                                   <asp:HiddenField ID="hddIdProveedor" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProveedor")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                              <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Tienda" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblTienda" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"Tienda")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            
                              <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Fecha Emisión" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblfechaEmision" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"doc_FechaEmision","{0:d}")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            
                               <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Fecha Vcto." ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblfechaVencimiento" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"doc_FechaVenc","{0:d}")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblMoneda_Monto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"nom_simbolo")%>'></asp:Label>

                                                <asp:Label ID="lblMonto" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"mcp_Monto","{0:F3}")%>'
                                                    Font-Bold="true"></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Deuda" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblMoneda_Saldo" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"nom_simbolo")%>'></asp:Label>

                                                <asp:Label ID="lblSaldo" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"mcp_Saldo","{0:F3}")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                          <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Ver Tipo Documento" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                   <asp:Button id="btnMostrarOC" runat="server" ToolTip="Visualizar Documento" Text="Ver Documento"  OnClientClick=" return( valOnClick_btnMostrarDetalle(this)  ); " />
                                </ItemTemplate>
                           </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnAnterior_Det" runat="server" Font-Bold="true" Width="50px" Text="<"
                                ToolTip="Página Anterior" OnClientClick="return(valNavegacion_Det('0'));" />
                            <asp:Button ID="btnSiguiente_Det" runat="server" Font-Bold="true" Width="50px" Text=">"
                                ToolTip="Página Posterior" OnClientClick="return(valNavegacion_Det('1'));" />
                            <asp:TextBox ID="txtPageIndex_Det" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                runat="server"></asp:TextBox><asp:Button ID="btnIr_det" runat="server" Font-Bold="true"
                                    Width="50px" Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacion_Det('2'));" />
                            <asp:TextBox ID="txtPageIndexGo_Det" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_VisualizarDetalle" runat="server">
                    <table width="100%">
                        <tr class="SubTituloCelda">
                            <td>
                                VISUALIZAR DATOS DEL DOCUMENTOS
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_DocumentoRef_Cab" runat="server" AutoGenerateColumns="False"
                                    Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:BoundField DataField="DescripcionPersona" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Descripción" ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="FechaAEntregar" DataFormatString="{0:dd/MM/yyy}" HeaderStyle-Height="25px"
                                            HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha a Entregar" ItemStyle-Font-Bold="true"
                                            ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                        <asp:BoundField DataField="FechaVenc" DataFormatString="{0:dd/MM/yyy}" HeaderStyle-Height="25px"
                                            HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha Vcto." ItemStyle-Font-Bold="true"
                                            ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblMonedaCab" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                            <asp:Label ID="lblMontoCab" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F3}")%>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Cond. Pago" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblCondPago" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomCondicionPago")%>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr class="SubTituloCelda">
                            <td>
                                DETALLE
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_DocumentoRef_Detalle" runat="server" AutoGenerateColumns="False"
                                    Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:BoundField DataField="CodigoProducto" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Cód." ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="NomProducto" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Producto" ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="Cantidad" DataFormatString="{0:F3}" HeaderStyle-Height="25px"
                                            HeaderStyle-HorizontalAlign="Center" HeaderText="Cantidad" ItemStyle-Font-Bold="true"
                                            ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="UMedida" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="U.M." ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="PrecioCD" DataFormatString="{0:F3}" HeaderStyle-Height="25px"
                                            HeaderStyle-HorizontalAlign="Center" HeaderText="P. Venta" ItemStyle-Font-Bold="true"
                                            ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Importe" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblMoneda" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>

                                                            <asp:Label ID="lblImporte" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Importe","{0:F3}")%>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr class="SubTituloCelda">
                            <td>
                                CONCEPTOS
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_DocumentoRef_DetalleConcepto" runat="server" AutoGenerateColumns="False"
                                    Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:BoundField DataField="Descripcion" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Concepto" ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Total" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblMoneda0" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>

                                                            <asp:Label ID="lblMonto0" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Monto","{0:F3}")%>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr class="SubTituloCelda">
                            <td>
                                CONDICIONES COMERCIALES
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_DocumentoRef_CondicionC" runat="server" AutoGenerateColumns="False"
                                    Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:BoundField DataField="Descripcion" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Condición Comercial" ItemStyle-Font-Bold="true" ItemStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" />
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdPersona" runat="server" />
                <asp:HiddenField ID="hddIdMovCuenta_CXP" runat="server" />
                <asp:HiddenField ID="hddIdProgramacionPago_CXP" runat="server" />
                <asp:HiddenField ID="hddFrmModo" runat="server" />
                <asp:HiddenField ID="HddIdAnexoDocumento" runat="server" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
            </td>
        </tr>
    </table>

    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 170px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Selected="True" Value="J">Juridica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razon Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:Panel ID="Panel1" runat="server" DefaultButton="btBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this,event));" runat="server"
                                                                            onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                                            Width="450px"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel2" runat="server" DefaultButton="btBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero(event));" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel3" runat="server" DefaultButton="btBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero(event));"
                                                                            MaxLength="11"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="Página Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="Página Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <!--QWE-->
    <div id="capaProgramacionPago" style="border: 3px solid blue; padding: 8px; width: 93%;
        height: auto; position: absolute; top: 50px; left: 4%; background-color: white;
        z-index: 3;display: none;"> <%--display: none;--%>
        <div style="width:100%">
            <div style="width:100%" >
                <div style="width:98%;float:left" class="TituloCelda">REQUERIMIENTO</div>
                <div style="float:left;width:2%">
                    <asp:ImageButton ID="ImageButton2" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF" />        
                </div>
            </div>
            <div style="width:100%;float:left">
                <asp:GridView ID="gv_REquerimientoProgramados" runat="server" AutoGenerateColumns="False" Width="100%" DataKeyNames="IdDocumento,IdProveedor" ShowFooter="true"
                    PageSize="50">
                    <HeaderStyle CssClass="GrillaHeader"/>
                    <Columns>
                        <asp:BoundField DataField="TipoDocumento" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            HeaderText="Documento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red" ItemStyle-Height="25px"
                            ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            HeaderText="Nro. Documento" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblNroDocumento" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                <asp:HiddenField ID="hddIdMovCuenta_CXP" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMovCtaPP")%>' />
                                <asp:HiddenField ID="hddIdTipoDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoDocumento")%>' />
                                <asp:HiddenField ID="hddIdUsuario" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUsuario")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            HeaderText="Proveedor" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblProveedor" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"Proveedor")%>'></asp:Label>
                                <asp:HiddenField ID="hddIdProveedor" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProveedor")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            HeaderText="Tienda" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                    <asp:Label ID="lblTienda" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"Tienda")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>                            
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            HeaderText="Fecha Emisión" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                    <asp:Label ID="lblfechaEmision" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"doc_FechaEmision","{0:d}")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            HeaderText="Fecha Vcto." ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblfechaVencimiento" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"doc_FechaVenc","{0:d}")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>             
                        <asp:TemplateField HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Right" HeaderText="Monto" >
                            <ItemTemplate>
                                    <asp:Label ID="lblMoneda_Monto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"nom_simbolo")%>'></asp:Label>
                                    <asp:Label ID="lblMonto" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"mcp_Monto","{0:F3}")%>'
                                    Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Right" HeaderText="Deuda">
                            <ItemTemplate>
                                <asp:Label ID="lblMoneda_Saldo" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"nom_simbolo")%>'></asp:Label>
                                <asp:Label ID="lblSaldo" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"mcp_Saldo","{0:F3}")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle CssClass="GrillaRow" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                </asp:GridView>
            </div><!--Aca termina la grilla requerimiento-->

            <div style="width:100%;float:left;margin-top:5px" class="TituloCelda">
            <span>CUENTA BANCARIA - PROVEEDOR </span>
            </div>

            <div style="width:100%;float:left"> 
                <asp:GridView ID="dgvCuentaBancaria" runat="server" AutoGenerateColumns="False" Width="100%" EmptyDataRowStyle-Font-Bold="true"
                EmptyDataText="Sin datos" EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-HorizontalAlign="Center" >
                <Columns>                  
                                                
                <asp:BoundField DataField="Empresa" HeaderText="Empresa" >
                </asp:BoundField>
                <asp:BoundField DataField="Banco" HeaderText="Banco" NullDisplayText="0" />
                <asp:BoundField DataField="MonSimbolo" HeaderText="Moneda" NullDisplayText="0" />
                <asp:BoundField DataField="NroCuentaBancaria" HeaderText="Numero" NullDisplayText="0" />
                <asp:BoundField DataField="esCuentaDetraccion" HeaderText="Cta. Detracción" NullDisplayText="---" />
                <asp:BoundField DataField="Estado" HeaderText="Estado" NullDisplayText="0" />
                                                  
                </Columns>

                <AlternatingRowStyle CssClass="GrillaRowAlternating" />    
                <FooterStyle CssClass="GrillaFooter" />
                <HeaderStyle CssClass="GrillaHeader" />
                <PagerStyle CssClass="GrillaPager" />
                <RowStyle CssClass="GrillaRow" />                            
                </asp:GridView>
            </div><!--Aquin termina div cuenta bancaria - proveedor-->

            
            <div style="width:100%;float:left;margin-top:5px" class="TituloCelda">
            <img style="vertical-align:middle;text-align:left" id="imgContraerExpandir" alt="" src="../Imagenes/Mas_B.JPG"/>
                <span>PROGRAMACIÓN DE PAGOS</span>
            </div>
            <div id="capaMostrar" >
            <div style="width:100%;float:left;margin-top:5px;margin-bottom:5px">
                <table>
                    <tr>
                        <td>
                <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" Width="70px" ToolTip="Nuevo" />
            </td>
                        <td>
                <asp:Button ID="btnEditar" runat="server" Text="Editar" Width="70px" ToolTip="Editar Programación de Pago" />
            </td>
                        <td>
                <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" Width="70px" ToolTip="Eliminar Programación de Pago" OnClientClick="return(  valOnClick_btnEliminar() );" />
            </td>
                        <td>
                <asp:Button ID="btnGuardar" runat="server" Text="Guardar" Width="70px" ToolTip="Registrar Programación de Pago" OnClientClick="return(  valOnClick_btnGuardar() );" />
            </td>
                        <td>
                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" Width="70px" ToolTip="Cancelar" />
            </td>
                        <td>
                <asp:Button ID="btnAgregarDocProvisionados" runat="server" Text="Doc. Provisionados" OnClientClick="return(mostrarProvisiones());" Visible="false"/>
            </td>
                        <td>
                 <asp:Button ID="btnDocCuentasPorCobrar" runat="server" Text="Doc. Por Cobrar" OnClientClick="return(mostrarCapaAplicacionFacturas());" Visible="false"/>
            </td>
                        <td>
                <img id="btnCrearCuentaDetraccion" src="../Imagenes/ctrlShiftP.png" alt="" title="Crear Cuenta Bancaria Detracción." onclick="return(mostrarCrearCuentaDetraccion());" runat="server" visible="false" />
            </td>
            <td>
                <asp:Panel ID="pnAdelantos" runat="server">
                    <asp:CheckBox ID="chkEsAdelanto" runat="server" Text="Estoy programando un anticipo (<b>Proveedores</b>)." AutoPostBack="true"
                    ToolTip="Habilitar esta opción solo si no se relacionará con un documento valor"  /><br />

                    <asp:CheckBox ID="chkEsCtaPorRendir" runat="server" Text="Estoy programando una cuenta por rendir (<b>Trabajadores</b>)." AutoPostBack="true"
                    ToolTip="Habilitar esta opción solo si no se relacionará con un documento valor." /><br />

                    <asp:CheckBox ID="chkPagoProveedor" runat="server" Text="Estoy programando Otros Tipos de Pago. (<b>Extraordinarios</b>)." AutoPostBack="true"
                    ToolTip="Habilitar esta opción solo si no se relacionará con un documento valor." />
                </asp:Panel>
            </td>
             <td>
                <asp:GridView ID="gvConceptoReferencia" runat="server" AutoGenerateColumns="false">
                    <RowStyle CssClass="GrillaRow" />
                            <HeaderStyle CssClass="GrillaHeader" />
                            <Columns>
                                <asp:TemplateField HeaderText="Concepto">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hddidConcepto" runat="server" Value='<%# Eval("id") %>' />
                                        <asp:Label ID="lblNombreConcepto" runat="server" Text='<%# Eval("Nombre") %>'></asp:Label>
                                        <asp:HiddenField ID="hdd_hd_ConceptoAdelanto" runat="server" Value='<%# Eval("ConceptoAdelanto") %>' />
                                        <asp:HiddenField ID="hddConceptoCtaxRendir" runat="server" Value='<%# Eval("conceptoCtaxRendir") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>                                
                            </Columns>
                 </asp:GridView>
            </td>
                    </tr>
                </table>
            </div> <!--Aqui termina el div de botones de acciones sobre la programación-->

             <!--Formulario-->
             <div style="width:100%;float:left">
                <asp:Panel id="Panel_ProgramacionPago" runat="server" Enabled="false">
                    <table>
                        <tr>
                <td style="text-align: right; font-weight: bold" class="Texto" >
                    Fecha de Pago:
                </td>
                <td>
                    <asp:TextBox ID="txtFecha_ProgPago" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );" AutoPostBack="true"
                                                                Width="80px"></asp:TextBox>                    
                    <cc1:calendarextender ID="txtFecha_ProgPago_CalendarExtender" runat="server" Enabled="True"
                        Format="dd/MM/yyyy" TargetControlID="txtFecha_ProgPago">
                    </cc1:calendarextender>
                </td>
                <td style="text-align: right; font-weight: bold" class="Texto" >
                    Medio de Pago:
                </td>
                <td>
                    <asp:DropDownList ID="cboMedioPago_Prog" runat="server" Width="170px"></asp:DropDownList>
                </td>
                <td style="text-align: right; font-weight: bold" class="Texto" >
                    Banco:
                </td>
                <td>
                    <asp:DropDownList ID="cboBanco_Prog" runat="server" AutoPostBack="true" Width="180px"></asp:DropDownList>
                </td>
                <td style="text-align: right; font-weight: bold" class="Texto" >
                    Cuenta Bancaria:
                </td>
                <td>
                    <asp:DropDownList ID="cboCuentaBancaria_Prog" runat="server" AutoPostBack="true"></asp:DropDownList>
                </td>
            </tr>
                        <tr>
                <td style="text-align: right; font-weight: bold" class="Texto" >
                    Tipo Moneda:
                </td>
                <td>
                    <asp:DropDownList ID="cboTipoMoneda" runat="server" Enabled="false"></asp:DropDownList>
                </td>
                <td colspan="6">
                    <table>
                        <tr>
                            <td style="text-align: right; font-weight: bold" class="Texto" >
                            Monto Total:
                            </td>
                            <td>
                            <asp:Label ID="lblMoneda_MontoProg" runat="server" Text="S/." CssClass="LabelRojo"
                                                                            Font-Bold="true"></asp:Label>
                                <asp:TextBox ID="txtMontoProg" Text="0" Font-Bold="true" Width="70px" onKeyPress=" return( validarNumeroPuntoPositivo('event') ); "
                                                                            onFocus=" return( aceptarFoco(this)  ); " onblur=" return( valBlur(event)  ); " runat="server"></asp:TextBox>
                            </td>
                            <td style="text-align: right; font-weight: bold" class="Texto" >
                                <asp:Label ID="lblTipoCambio" Text="Tipo de Cambio:" runat="server" Visible="false"> </asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtTipoCambio" Visible="false"  runat="server" Text="0" Font-Bold="true" Width="70px" onKeyPress=" return( validarNumeroPuntoPositivo('event') ); " ></asp:TextBox>
                            </td>
                            <td style="text-align: right; font-weight: bold" class="Texto" >
                                <asp:Label ID="Label2" runat="server" Text="Monto a Pagar:" Visible="false"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblResultadoDolares" runat="server" Text="US$." Font-Bold="true" Visible="false" Style="color:Red"></asp:Label>

                                <asp:TextBox ID="txtResultadoTipoCambio" runat="server" Text="0" AutoPostBack="true" Visible="false" ReadOnly ="true" ></asp:TextBox>
                                <asp:Button  ID="btncalcularTC" runat="server"  Text="Calcular Monto" AutoPostBack="true" Visible="false"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
                        <tr>
            <td colspan="8" style="width:100%">
                <asp:TextBox ID="txtObservaciones_Prog" runat="server" Text="" TextMode="MultiLine" Width="100%" Rows="3"></asp:TextBox>
            </td>
            </tr>
                    </table>

   
                </asp:Panel>
             </div>   
             <!--Aca termina el formulario-->

             <asp:Panel ID="Panel11" runat="server">
             <div>
             <asp:CheckBox ID="chkPagoParcial" runat="server" Text="Marcar solo para Pagos Parciales con Documentos Relacionados." 
                     AutoPostBack="true"  onchange="return SeleccionarChkparcial();" 
                     ForeColor="Blue" />

             </div>
     
             </asp:Panel>
             </div>

            <div style="width:100%;float:left;margin-top:5px" class="TituloCelda">
                <span>DOCUMENTOS PROVISIONADOS AGREGADOS</span>
            </div>

            <div style="width:100%">
            <table style="width:100%">
                <tr>
                    <td>
                        <asp:GridView ID="GV_DocumentosProvisionados" runat="server" 
                            AutoGenerateColumns="False" Width="100%" ShowFooter="True" 
                            ShowHeaderWhenEmpty="True">
                         <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                               <%-- <%# Container.DataItemIndex + 1 %>--%>
                                <asp:LinkButton runat="server" ID="lkbtnQuitar" Text="Quitar" OnClick="evento_quitar" CommandArgument="<%# Container.DataItemIndex %>"></asp:LinkButton>
                                <asp:HiddenField ID="hddidDocumento" runat="server" Value='<%# Eval("idDocumento") %>' />
                            </ItemTemplate>
                            <HeaderStyle VerticalAlign="Middle" />
                            <ItemStyle Font-Italic="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>                         
                        <asp:BoundField DataField="Codigo" HeaderText="Número Doc." /> 
                        <asp:BoundField DataField="FechaEmision" HeaderText="Fec. Emision" />
                        <asp:TemplateField HeaderText="Moneda" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-Wrap="true">
                            <ItemTemplate>
                                <asp:Label ID="lblMoneda" runat="server" Text='<%# Eval("NomMoneda") %>'></asp:Label>
                            </ItemTemplate>

<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Wrap="True"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="SubTotal" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-Wrap="true">
                            <ItemTemplate>
                                <asp:Label ID="lblSubtotal" runat="server" Text='<%# Eval("SubTotal") %>'></asp:Label>
                            </ItemTemplate>

<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Wrap="True"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="I.G.V" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-Wrap="true">
                            <ItemTemplate>
                                <asp:Label ID="lblIgv" runat="server" Text='<%# Eval("IGV") %>'></asp:Label>
                            </ItemTemplate>

<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Wrap="True"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="AutoDetracción" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-Wrap="true">
                            <ItemTemplate>
                                <asp:Label ID="lblAutoDetraccion" runat="server" Text='<%# Eval("Detraccion") %>'></asp:Label>
                            </ItemTemplate>

<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Wrap="True"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-Wrap="true">
                            <ItemTemplate>
                                <asp:Label ID="lblMonedaPrincipal" runat="server" Text='<%# Eval("NomMoneda") %>' ForeColor="Red" Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblTotal" runat="server" Text='<%# Eval("Total") %>' ForeColor="Red" Font-Bold="true"></asp:Label>
                            </ItemTemplate>

<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Wrap="True"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Deuda" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-Wrap="true">
                            <ItemTemplate>
                                <asp:Label ID="lblMonedaPrincipalDeuda" runat="server" Text='<%# Eval("NomMoneda") %>' ForeColor="Red" Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblTotalDeuda" runat="server" Text='<%# Eval("montoPendiente") %>' ForeColor="Red" Font-Bold="true"></asp:Label>
                            </ItemTemplate>

<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Wrap="True"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderText="OT IMP." ItemStyle-Wrap="true">
                            <ItemTemplate>
                                <asp:TextBox ID="txtOtroImpuesto" runat="server" Width="70px" Text='<%# Eval("MontoOtroTributo","{0:F2}") %>' onkeyup="calcularTotalPagarXmonto();"></asp:TextBox>
                            </ItemTemplate>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" ItemStyle-Wrap="true">
                            <HeaderTemplate>                                
                                <asp:DropDownList ID="ddlTipoAgenteCombo" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cambiarTipoAgente">
                                </asp:DropDownList>
                            </HeaderTemplate>
                            <ItemTemplate>                                
                                <asp:TextBox ID="txtPorcentajeAgente" runat="server" Width="50px" Text='<%# Eval("TasaAgente","{0:F2}") %>' onkeyup="calcularTotalPagarXporc();" Enabled="false"></asp:TextBox>
                            </ItemTemplate>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderText="DET/RET" ItemStyle-Wrap="true">
                            <ItemTemplate>
                                <asp:TextBox ID="txtMontoAgente" runat="server" Width="70px" Text='<%# Eval("montoAgente","{0:F2}") %>' Enabled="false" onkeyup="calcularTotalPagarXmonto();"></asp:TextBox>
                            </ItemTemplate>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderText="Por Pagar" ItemStyle-Wrap="true" FooterStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:TextBox ID="txtMontoaPagar" runat="server" Width="70px" Text='<%# Eval("montoXPagar","{0:F2}") %>' Enabled="false"></asp:TextBox>
                            </ItemTemplate>

<FooterStyle HorizontalAlign="Right"></FooterStyle>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></ItemStyle>
                        </asp:TemplateField >
                             <asp:TemplateField HeaderText="Monto" >
                                 <ItemTemplate>
                                     <asp:TextBox ID="txtmontonuevo" Width="70px"  onkeyup="calcularTotalPagarNuevo();" runat="server"></asp:TextBox>
                                 </ItemTemplate>
                             </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaRow" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                    </td>
                </tr>
            <tr>
                <td style="float:right">
                    <asp:TextBox ID="txtTotalSaldo" runat="server" Text="0" Visible="false"></asp:TextBox>
                </td>
            </tr>
            </table>
            
            <div style="width:100%;float:left;margin-top:5px" class="SubTituloCelda">
                <span>APLICACIÓN DE FACTURAS CONTRA DEUDAS PENDIENTES</span>
            </div>
            <div style="width:100%">
                <asp:GridView ID="gv_AplicacionFacturas" runat="server" AutoGenerateColumns="false" ShowFooter="true">
                    <RowStyle CssClass="GrillaRow" />
                    <HeaderStyle CssClass="GrillaHeader" />
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="lnkQuitarAplicacion" Text="Quitar" OnClick="quitarDocAplicado" CommandArgument="<%# Container.DataItemIndex %>"></asp:LinkButton>
                                <asp:HiddenField ID="hddidDocumento" runat="server" Value='<%# Eval("idDocumento") %>' />
                            </ItemTemplate>
                            <HeaderStyle VerticalAlign="Middle" />
                            <ItemStyle Font-Italic="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="doc_codigo" HeaderText="Número" /> 
                        <asp:BoundField DataField="tipoDocumento" HeaderText="Documento" /> 
                        <asp:BoundField DataField="doc_FechaEmision" HeaderText="Fec. Emisión" /> 
                        <asp:BoundField DataField="nom_simbolo" HeaderText="Moneda" />                        
                        <asp:TemplateField HeaderText="Importe" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:Label id="lblImporteNeto" runat="server" Text='<%# Eval("mcp_Monto","{0:F3}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total Abono" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:Label id="lblAbono" runat="server" Text='<%# Eval("mcp_abono","{0:F3}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total a Aplicar" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>                                
                                <asp:Label ID="lblSaldoDocumento" runat="server" Text='<%# Eval("mcp_saldo","{0:F3}") %>' ForeColor="Red" Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalMontoAPagar" runat="server" ForeColor="White" Font-Bold="true"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <!--Programaciones realizadas-->
            <table>
         <tr>
            <td class="SubTituloCelda">
                PROGRAMACIONES REALIZADAS
            </td>
        </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel7" runat="server">
                        <table style="white-space:100%">
                            <tr>
                                
                                <td>
                                    <asp:GridView ID="GV_ProgramacionPago" runat="server" AutoGenerateColumns="False"
                                        Width="100%" DataKeyNames="idBanco,IdCuentaBancaria,IdProgramacionPago">
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbSeleccionarProgramacion" runat="server" Text="Seleccionar" Enabled='<%# if(Eval("can_NroOperacion") = "-",true,false) %>'
                                                    OnClick="GV_ProgramacionPago_onlick"></asp:LinkButton>
                                                    <asp:HiddenField ID="hddIdProgramacionPago" runat="server" Value='<%#Eval("IdProgramacionPago") %>' />
                                                    <asp:HiddenField ID="hddIdConcepto" runat="server" Value='<%#Eval("IdConcepto") %>' />
                                                 <%--   <asp:HiddenField ID="HddIdAnexoDocumento" runat="server" Value='<%#Eval("IdDocumento") %>' />--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="FechaPagoProg" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Height="25px"
                                                HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha Programacion Pago" ItemStyle-Font-Bold="true"
                                                ItemStyle-ForeColor="Red" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblMoneda_MontoProg" runat="server" Font-Bold="true" ForeColor="Red"
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblMontoProg" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"MontoPagoProg","{0:F2}")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdProgramacionPago_CXP" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProgramacionPago")%>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="MedioPago" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Medio de Pago" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="Banco" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Banco" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="NroCuentaBancaria" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Cuenta Bancaria" ItemStyle-HorizontalAlign="Center" />
                                             <asp:BoundField DataField="MonedaNew" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Tipo Moneda" ItemStyle-HorizontalAlign="Center" />
                                                 <asp:BoundField DataField="TipoCambio" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Tipo de Cambio" ItemStyle-HorizontalAlign="Center" />
                                                
                                                <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Documento Ref." ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbldocreferencia" runat="server" Font-Bold="true" ForeColor="Red"
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumentoRef")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdDocReferencia" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumentoRef")%>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Emitir Cheque">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgbtnCrearCheque" ImageUrl="~/Imagenes/Añadir.png" runat="server" Enabled="false" Visible="false"
                                                    ToolTip="Emitir Cheque." CommandName="crear_cheque" CommandArgument='<%# Container.DataItemIndex %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ibDetalleCancelacion" runat="server" ImageUrl="~/Imagenes/Mas_B.JPG" ToolTip="Agregar Datos del Pago..."
                                                    CommandName="btnCancelacionPrincipal" Visible='<%# if( eval("MedioPago")="CHEQUE PAGADOR" or Eval("can_NroOperacion") <> "-",false,true) %>'
                                                    CommandArgument='<%# Container.DataItemIndex %>'/>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Nro Operación" ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbNroOperacion" runat="server" Text='<%# Eval("can_NroOperacion") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Banco" ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBancoPago" runat="server" Text='<%# Eval("bancoCancelacion") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Monto Pagado" ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Right">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMontoPagado" runat="server" Text='<%# Eval("can_montoDeposito") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Fecha Pago" ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFechaPago" runat="server" Text='<%# Eval("can_fechaPago") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Nro Voucher" ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="nroVoucher" runat="server" Text='<%# Eval("nroVoucher") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   <asp:GridView ID="GV_TipoRegimen" runat="server" AutoGenerateColumns="false">
                                     <HeaderStyle CssClass="GrillaHeader" />
                                     <RowStyle CssClass="GrillaRow" />
                                         <Columns>
                                           <asp:BoundField HeaderText="Tipo Régimen" DataField="descripcion" />
                                           <asp:TemplateField HeaderText="Doc. Referencia">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%# Eval("idDocumento") %>' />
                                                <asp:HiddenField ID="hddTipoRegimen" runat="server" Value='<%# Eval("tipoRegimen") %>' />
                                               <asp:label ForeColor="Red" ID="LblCodigoRef" runat="server" Text='<%# Eval("codigo") %>'></asp:label>
                                            </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Monto">
                                            <ItemTemplate>
                                               <asp:label ForeColor="Red" ID="lblMontoRegimen" runat="server" Text='<%# Eval("montoRegimen") %>'></asp:label>
                                            </ItemTemplate>
                                            </asp:TemplateField>
                                           <asp:TemplateField>
                                            <ItemTemplate>
                                               <asp:ImageButton ID="ibDetalle" runat="server" ImageUrl="~/Imagenes/Mas_B.JPG" ToolTip="Agregar Datos del Pago..."
                                                            CommandName="btnCancelacionDetraccion" Visible='<%# if( eval("tipoRegimen")=3 or Eval("canNroOperacion") <> "-",false,true) %>'
                                                            CommandArgument='<%# Container.DataItemIndex %>' />
                                               </ItemTemplate>
                                                </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Nro Operacion" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                               <asp:label ForeColor="Red" ID="lblNroOperaDet" runat="server" Text='<%# Eval("canNroOperacion") %>'></asp:label>
                                            </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Monto Cancelado - Soles" ItemStyle-HorizontalAlign="Right">
                                            <ItemTemplate>
                                               <asp:label ForeColor="Red" ID="lblMontoDetPagado" runat="server" Text='<%# Eval("montoCancelado") %>'></asp:label>
                                            </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Banco" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                               <asp:label ForeColor="Red" ID="lblBancoDet" runat="server" Text='<%# Eval("bancoCancelacion") %>'></asp:label>
                                            </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Fec. Pago" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                               <asp:label ForeColor="Red" ID="lblFEcPagoDet" runat="server" Text='<%# Eval("fechaCancelacion") %>'></asp:label>
                                            </ItemTemplate>
                                            </asp:TemplateField>

                                               <asp:TemplateField HeaderText="Documento Retencion" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                               <asp:label ForeColor="Red" ID="lblDocumentoRetencion" runat="server" Text='<%# Eval("DocumentoRetencion") %>'></asp:label>
                                            </ItemTemplate>
                                            </asp:TemplateField>
                                          </Columns>
                                   </asp:GridView>
                                   </td>
                            </tr>
                            <tr>
                            <td>
                                <asp:Button ID="btnGenDocRetencion" runat="server" 
                                    Text="Emitir Documento Retencion" Width="186px" 
                                    OnClientClick="return(valOnClick_btnGenDocRetencion());" Visible="False" />
                                         
                                <asp:Button ID="btnImprimirRetencion" runat="server" 
                                    OnClientClick="return(valOnClick_btnGenDocRetencion());" 
                                    Text="Imprimir C Retencion." Width="186px" Visible="False" />
                                         
                            </td>
                            </tr>
                            <tr>
                            <td>
                            ....
                            </td>
                            </tr>
                                 <tr>
                     <td>
                      <asp:TextBox ID="txtFecha_ProgPagoRetencion" runat="server" AutoPostBack="true" 
                                    CssClass="TextBox_Fecha" Height="18px" 
                                    Width="168px" Visible="False"></asp:TextBox>
                                <cc1:CalendarExtender ID="txtFecha_ProgPagoRetencion_calendarextender" runat="server" 
                                    Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFecha_ProgPagoRetencion">
                                </cc1:CalendarExtender>
                     </td>
                     </tr>
                           
                        </table>
            

                       
                    </asp:Panel>
                </td>
            </tr>
        </table>
            <asp:HiddenField ID="HiddenField1" runat="server" />
        <asp:HiddenField ID="HiddenField2" runat="server" />
                                    <asp:HiddenField ID="hddIndiceProgramacion" runat="server" 
                    Value="-1" />
        <asp:HiddenField ID="HiddenField3" runat="server" />
        <asp:HiddenField ID="HiddenField4" runat="server" />
        <asp:HiddenField ID="hddIdDocumento" runat="server" />
        <asp:HiddenField ID="hddSaldoRQ" runat="server" />
            </div>
        </div>
    </div>



    <div id="capaDocProvisionados" style="width:1000px;height:500px;top:50%;left:50%;margin-top:-250px;margin-left:-500px;border:2px solid blue;display:none;position:absolute;background-color:White;z-index:5;overflow:scroll">
    <div style="width:100%">
        <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                    OnClientClick="return(offCapa('capaDocProvisionados'));" />        
    </div>
    <div style="width:100%">
        <table style="width:100%">
            <tr>
                <td>
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="Texto" RepeatDirection="Horizontal" AutoPostBack="true" >
                    <asp:ListItem Text="Por Nro. Documento" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Por Fecha de Emisión" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Por Proveedor" Value="2"></asp:ListItem>
                     <asp:ListItem Text="Por Nro Voucher Contable" Value="3"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
             <tr id="trFiltroNroVoucher" runat="server" style="display:block">
                <td>
                    <table>
                        <tr>
                          
                            <td class="Texto">
                                Número Voucher: 
                            </td>
                            <td>
                                <asp:Panel ID="Panel25" runat="server" DefaultButton="btnBuscarxNumeroVoucher">
                                <asp:TextBox ID="txtNumeroVoucher" runat="server" Text="" Width="150px" 
                                        MaxLength="10"></asp:TextBox>
                                </asp:Panel>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarxNumeroVoucher" runat="server" Text="Buscar" OnClick="btnBuscarProv_Click" 
                                OnClientClick="return(validarVoucherFiltro(this));" />
                                <span id="spnMensajeFiltroNroVoucher" style="color:Red;font-weight:bold"></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trFiltroNroDocumento" runat="server" style="display:block">
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Serie: 
                            </td>
                            <td>
                                <asp:Panel ID="Panel6" runat="server" DefaultButton="btnBuscarxNumeroProvision">
                                <asp:TextBox ID="txtnroSerieProvision" runat="server" Text="" Width="80px"></asp:TextBox>
                                </asp:Panel>
                            </td>
                            <td class="Texto">
                                Número: 
                            </td>
                            <td>
                                <asp:Panel ID="Panel8" runat="server" DefaultButton="btnBuscarxNumeroProvision">
                                <asp:TextBox ID="txtNumeroProvision" runat="server" Text="" Width="80px"></asp:TextBox>
                                </asp:Panel>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarxNumeroProvision" runat="server" Text="Buscar" OnClick="btnBuscarProv_Click" 
                                OnClientClick="return(validarRucFiltro(this));" />
                                <span id="spnMensajeFiltroNroDoc" style="color:Red;font-weight:bold"></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trFiltroFecha" runat="server">
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Desde: 
                            </td>
                            <td>
                                <asp:TextBox ID="txtDesde" runat="server" Text="" Width="80px"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendartxtDesde" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" TargetControlID="txtDesde">
                                        </cc1:CalendarExtender>
                            </td>
                            <td class="Texto">
                                Hasta: 
                            </td>
                            <td>
                                <asp:TextBox ID="txtHasta" runat="server" Text="" Width="80px"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendartxtHasta" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" TargetControlID="txtHasta">
                                        </cc1:CalendarExtender>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarxFecha" runat="server" Text="Buscar" OnClick="btnBuscarProv_Click" 
                                OnClientClick="return(validarRucFiltro(this));" />
                                <span id="Span1" style="color:Red;font-weight:bold"></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trFiltroProveedor" runat="server">
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtFiltroRuc" runat="server" placeholder="R.U.C"></asp:TextBox>
                                <asp:Button ID="btnBuscarPoProveedor" runat="server" Text="Buscar" CssClass="btnBuscar" OnClick="btnBuscarProv_Click" 
                                OnClientClick="return(validarRucFiltro(this));" />
                                <span id="spnFiltroRuc" style="color:Red;font-weight:bold"></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table style="width:100%">
            <tr>
                <td>
                    <asp:GridView ID="gvProvisiones" runat="server" Width="100%" AutoGenerateColumns="false" EmptyDataText="No se encontraron documentos provisionados"
                    DataKeyNames="IdDocumento" EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-Font-Bold="true">
                    <HeaderStyle CssClass="GrillaHeader" />
                    <RowStyle CssClass="GrillaRow" />
                    <EditRowStyle CssClass="Grilla_Seleccion" />
                    <Columns>
                    <asp:TemplateField ShowHeader="False">
                                 <ItemTemplate>
                                 <asp:CheckBox ID="chkSeleccionar" runat="server" AutoPostBack="true" OnCheckedChanged="SeleccionarGuias" />
                                 </ItemTemplate>
                                 <HeaderStyle VerticalAlign="Middle" />
                                 <ItemStyle Font-Italic="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                             </asp:TemplateField>                 
                    <asp:BoundField DataField="regNombre" HeaderText="Proveedor" />
                    <asp:BoundField DataField="Codigo" HeaderText="Número Doc." /> 
                    <asp:BoundField DataField="FechaEmision" HeaderText="Fec. Emision" />
                    <asp:BoundField DataField="NomTipoOperacion" HeaderText="Tipo. Ope." />
                    <asp:BoundField DataField="NomMoneda" HeaderText="Moneda" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="SubTotal" HeaderText="SubTotal" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="IGV" HeaderText="i.g.v" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="Detraccion" HeaderText="Detracción" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="Total" HeaderText="Total" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="TotalAPagar" HeaderText="Total a Pagar" ItemStyle-HorizontalAlign="Right" />
                    </Columns>
                    </asp:GridView>
                </td>
            </tr>

        </table>
    </div>
</div> 
    <div id="capaCrearCuentas" style="width:1000px;display:none;background-color:White;top:150px;left:30px;position:absolute;border:3px solid blue;z-index:2">
<div style="width:100%">
    <div style="float:left">
        <asp:Button id="btnGuardarCuentaBancaria" runat="server" CssClass="btnGuardar" Text="Guardar"/>
    </div>
    <div style="float:right; height: 15px;">
        <img src="../Imagenes/Cerrar.gif" alt="" title="Cerrar." style="float:right" onclick="return(ocultarCapaCuentaDetraccion());" />
    </div>
</div>
<div>
    <table>
        <tr>
            <td  class="Texto" style="font-weight:bold;text-align:right" >
                Banco:</td>
            <td>
            <asp:DropDownList ID="cboBanco" runat="server" >
            </asp:DropDownList>
            </td>
            
            <td class="Texto" style="font-weight:bold;text-align:right" >
                Moneda:</td>
            <td >
            <asp:DropDownList ID="DropDownList5" runat="server" >
            </asp:DropDownList>
            </td>
            <td class="Texto" style="font-weight:bold;text-align:right" >
                Nro. Cuenta Bancaria:</td>
            <td>
            <asp:TextBox ID="txtNumero" runat="server" Width="200px" ></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
         <td class="Texto" style="font-weight:bold;text-align:right" >
                Cuenta Contable:</td><td>
            <asp:TextBox ID="txtCtaContable" runat="server" Width="100%" ></asp:TextBox>
            </td >
            <td class="Texto" style="font-weight:bold;text-align:right">
                    Swift Bancario:
            </td>
            <td >
            <asp:TextBox ID="txtswiftbanca" runat="server" Text=""></asp:TextBox>
            </td>
            
            <td class="Texto" style="font-weight:bold;text-align:right">
             Nro. Cuenta Interbancaria:
            </td>
            
            <td> 
            <asp:TextBox ID="txtcuentaInter" runat="server"  Width="200px"> </asp:TextBox></td>
        </tr>
    </table>
</div>
</div>

    <!--Capa Emitir Cheque-->
    <div id="capaEmitirCheque" style="width:1200px;border:2px solid blue;background-color:White;z-index:3;position:absolute;display:none;top:180px;left:30px;border-radius:6px">
<div style="width:100%" class="TituloCelda">
    <span>EMITIR CHEQUE</span>
<div style="width:100%">
    <div style="float:right; height: 15px;">
        <asp:ImageButton ID="ImageButton4" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                    OnClientClick="return(offCapa('capaEmitirCheque'));" />
    </div>
</div>
</div>
<div>
    <table>
        <tr>
            <td class="Texto">
                Banco:
            </td>
            <td>
                <asp:DropDownList ID="ddlBancoEmitirCheque" runat="server" Width="150px" Enabled="false"></asp:DropDownList>
            </td>
            <td class="Texto">Cuenta Bancaria:</td>
            <td>
                <asp:DropDownList ID="ddlCuentaBancariaEmitirCheque" runat="server" Enabled="false"></asp:DropDownList>
            </td>
            <td class="Texto">
                Estado del Documento
            </td>
            <td>
                <asp:DropDownList ID="ddlEstadoDocEmitirCheque" runat="server" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="Texto">
                Serie / Numeración:
            </td>
            <td>
                <asp:DropDownList ID="ddlSerieNumeracionEmitirCheque" runat="server" Enabled="false"></asp:DropDownList>
            </td>
            <td class="Texto">Nro Cheque:</td>
            <td>
                <asp:TextBox ID="ddlNroChequeEmitirCheque" runat="server" ></asp:TextBox>
            </td>
            <td class="Texto">
                Estado de entrega:
            </td>
            <td>
                <asp:DropDownList ID="ddlEstadoEntregaEmitirCheque" runat="server" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="Texto">
                Monto: 
            </td>
            <td>
                <asp:TextBox ID="txtMontoEmitirCheque" runat="server" Text="0" Enabled="false"></asp:TextBox>
            </td>
            <td class="Texto">
                Nro voucher:
            </td>
            <td class="Texto">
                <asp:TextBox ID="txtNroVoucherEmitirCheque" runat="server" Text=""></asp:TextBox>
            </td>
            <td class="Texto">
                Estado de Cancelación:
            </td>
            <td>
                <asp:DropDownList ID="ddlEstadoCancelacionEmitirCheque" runat="server" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="Texto">
                Fecha de Cobro:
            </td>
            <td>
                <asp:TextBox ID="txtFEchaCobroEmitirCheque" runat="server"></asp:TextBox>
                <cc1:calendarextender ID="txtFEchaCobroEmitirCheque_CalendarExtender" runat="server" Enabled="True"
                        Format="dd/MM/yyyy" TargetControlID="txtFEchaCobroEmitirCheque">
                    </cc1:calendarextender>
            </td>
            <td class="Texto">
                Fecha de Emisión:
            </td>
            <td>
                <asp:TextBox ID="txtFechaEmisionEmitirCheque" runat="server"></asp:TextBox>
                <cc1:calendarextender ID="txtFechaEmisionEmitirCheque_CalendarExtender" runat="server" Enabled="True"
                        Format="dd/MM/yyyy" TargetControlID="txtFechaEmisionEmitirCheque">
                    </cc1:calendarextender>
            </td>
           
            <td colspan="2" align="center">
                <asp:Button ID="btnGuardarEmitirCheque" runat="server" Text="Guardar" CssClass="btnGuardar" CommandName="guardar_cheque" OnClientClick="this.disabled=true;this.value='Procesando'" UseSubmitBehavior="false" />
                                                                                                                                      
            </td>
        </tr>
        <tr>
        <td></td>
        <td></td>
        <td class="Texto"><asp:Label ID="lblchequediferido" runat="server" Text=""></asp:Label></td>
        <td>
            
        <asp:TextBox ID="txtFechaDiferida" runat="server"></asp:TextBox>
                <cc1:calendarextender ID="txtFechaDiferida_Calendarextender" runat="server" Enabled="true"
                        Format="dd/MM/yyyy" TargetControlID="txtFechaDiferida">
                    </cc1:calendarextender>
         </td>
 <td>
     <asp:CheckBox ID="chkDiferido" runat="server" onchange="return verificar();"  />
            </td>
        </tr>
        <tr runat="server" id="tr_chequePersonaRendir">
            <td class="Texto">Persona a Rendir:</td>
            <td colspan="3">
            <asp:Panel ID="pnlPersonaCheque" runat="server" DefaultButton="btnBuscarChequePersona">
                <asp:TextBox ID="txtCodigoPersonaCheque" runat="server" Text="" Enabled="false" Width="80px"></asp:TextBox>
                <asp:TextBox ID="txtNOmbrePersonaFiltroCheque" runat="server" Text=""></asp:TextBox>
            </asp:Panel>
                <asp:Button ID="btnBuscarChequePersona" runat="server" Text="Buscar" CssClass="btnBuscar"/>
            </td>
         </tr>
         <tr>
            <td colspan="4">
                <asp:GridView ID="gv_EmpleadosCheques" runat="server" AutoGenerateColumns="false">
                    <RowStyle CssClass="GrillaRow" />
                    <HeaderStyle CssClass="GrillaHeader" />
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbSelecEmpleado" runat="server" OnClick="SelectClienteCheque_Click" Text="Seleccioonar"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="IdPersona" HeaderText="Id" NullDisplayText="---" />
                        <asp:BoundField DataField="Nombre" HeaderText="Ap. y Nombres" NullDisplayText="---" />
                        <asp:BoundField DataField="DNI" HeaderText="D.N.I." NullDisplayText="---" />
                    </Columns>

                </asp:GridView>
            </td>
        </tr>
        </table>
        <table width="100%">
            <tr>
                <td>
                    <asp:GridView ID="gvEmisionCheques" runat="server" Width="100%" ShowHeader="true" AutoGenerateColumns="false">
                        <HeaderStyle CssClass="GrillaHeader" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Label ID="lblBancoDescripcion" runat="server" Text='<%# Eval("banco_nombre") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Cuenta Bancaria" DataField="ctaBancaria_nombre" />
                            <asp:BoundField HeaderText="Serie" ItemStyle-ForeColor="Red" DataField="Serie" />
                            <asp:BoundField HeaderText="Nro. Cheque" ItemStyle-ForeColor="Red" DataField="ChequeNumero" />
                            <asp:BoundField HeaderText="Mon." DataField="NomMoneda" />
                            <asp:BoundField HeaderText="Monto" DataField="Monto" />
                            <asp:BoundField HeaderText="Beneficiario" DataField="Beneficiario" />
                            <asp:BoundField HeaderText="Fecha a Cobrar" ItemStyle-ForeColor="Red" DataField="FechaACobrar" />
                            <asp:BoundField HeaderText="Fecha de Emisión" ItemStyle-ForeColor="Red" DataField="FechaEmision" />
                            <asp:BoundField HeaderText="Est. Documento" DataField="NomEstadoDocumento" />
                            <asp:BoundField HeaderText="Est. Entrega" DataField="NomEstadoEntregado" />
                            <asp:BoundField HeaderText="Est. Cancelación" DataField="NomEstadoCancelacion"  />
                            <asp:TemplateField HeaderStyle-Width="150px">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnImprimirImg" ImageUrl="~/Imagenes/chequePrint.png" runat="server" ToolTip="Imprimir Cheque"
                                    OnClientClick="return(  valOnClick_btnImprimir2(this)  );" />

                                    <asp:ImageButton ID="btnVoucherImg" ImageUrl="~/Imagenes/voucherCheque.png" runat="server" ToolTip="Imprimir Voucher"
                                    OnClientClick="return(  valOnClick_btnImprimirVoucher(this)  );" />
                                   
                                    <asp:ImageButton ID="btnVoucherImg2" ImageUrl="~/Imagenes/chequePrint.png" runat="server" ToolTip="Imprimir Cheque Formato 2"
                                    OnClientClick="return(  valOnClick_btnImprimir2V2(this)  );" />
                                    
                                    <asp:ImageButton ID="btnAnularCheque" ImageUrl="~/Imagenes/cancelCheque.png" runat="server" ToolTip="Anular Cheque"
                                    OnClientClick="return(  valOnClick_btnAnularCheque(this)  );" />

                                    <asp:HiddenField ID="hddIdDocumentoCheque" runat="server" Value='<%# eval("idDocumento") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
            <td>

             <%--  <asp:Button ID="btnAnularChequeE" runat="server" Text="Anular Cheque" CssClass="btnAnularCheque" CommandName="Anular_cheque" OnClientClick="return(valOnClick_btnAnularChequeE());" UseSubmitBehavior="false" />
           --%>
         
            </td>
            </tr>
        </table>
</div>
</div>

    <div id="capaImprimirVoucher" style="width:1200px;border:2px solid blue;background-color:White;z-index:4;position:absolute;display:none;top:190px;left:30px">
    <asp:Panel ID="pnlContents" runat="server">
        <table style="width:100%;border-top-width: 1px">
            <tr style="padding-top:1px"> 
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%" colspan="3"><span>&nbsp&nbsp 01/02/2016</span></td>
                <td style="width:5%" colspan="3"><span>05/02/2016</span></td>
                <td style="width:5%" colspan="4"><span>400.00</span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
            </tr>
            <tr>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%" colspan="13">ALAN SAN MARTIN FRANK NOMBRE NOMBRE<span>3</span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
            </tr>
            <tr>
                <td style="width:5%" colspan="15"><span>CINCUENTA Y CUATRO CON 78/100 SOLES</span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
                <td style="width:5%"><span></span></td>
            </tr>
        </table>
        </asp:Panel>
    </div>

    <!--Capa tipo de Anticipos-->
    <div id="capaAnticipoProveedores" style="width:200px;border:2px solid blue;background-color:White;z-index:4;position:absolute;display:none;top:120px;left:100px">
        <div style="width:100%">
            <table>
                <tr class="TituloCelda">
                    <td><asp:Label ID="lblSubtitulo" runat="server" Text="ELEGIR TIPO DE ANTICIPO"></asp:Label></td>

                    <td><asp:ImageButton ID="ImageButton3" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF" /></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="gvTipoAnticipos" runat="server" AutoGenerateColumns="false">
                            <RowStyle CssClass="GrillaRow" />
                            <HeaderStyle CssClass="GrillaHeader" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>                                    
                                        <asp:LinkButton ID="lbSeleccionar" runat="server" Text="Seleccionar" OnClick="lbSeleccionar_clickAnticipo" CommandArgument="<%# Container.DataItemIndex %>"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Concepto">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hddidConcepto" runat="server" Value='<%# Eval("id") %>' />
                                        <asp:Label ID="lblNombreConcepto" runat="server" Text='<%# Eval("Nombre") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </div>

        <!--Capa tipo Cuentas x REndir-->
    <div id="capaCtasXRendir" style="width:200px;border:2px solid blue;background-color:White;z-index:4;position:absolute;display:none;top:120px;left:100px">
        <div style="width:100%">
            <table>
                <tr class="TituloCelda">
                    <td><asp:Label ID="Label1" runat="server" Text="ELEGIR TIPO DE CUENTA POR RENDIR"></asp:Label></td>

                    <td><asp:ImageButton ID="ImageButton6" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF" /></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="gv_CtasxRendir" runat="server" AutoGenerateColumns="false">
                            <RowStyle CssClass="GrillaRow" />
                            <HeaderStyle CssClass="GrillaHeader" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>                                    
                                        <asp:LinkButton ID="lbSeleccionar" runat="server" Text="Seleccionar" OnClick="lbSeleccionar_clickPorRendir" CommandArgument="<%# Container.DataItemIndex %>"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Concepto">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hddidConcepto" runat="server" Value='<%# Eval("id") %>' />
                                        <asp:Label ID="lblNombreConcepto" runat="server" Text='<%# Eval("Nombre") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <!--Capa tipo de PAGO REMUNERACIONES-->
    <div id="capaPagoRemuneraciones" style="width:200px;border:2px solid blue;background-color:White;z-index:4;position:absolute;display:none;top:120px;left:100px">
        <div style="width:100%">
            <table>
                <tr class="TituloCelda">
                    <td><asp:Label ID="Label3" runat="server" Text="ELEGIR TIPO DE PAGO PROVEEDOR"></asp:Label></td>

                    <td><asp:ImageButton ID="ImageButton7" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF" /></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="gv_pagoRemuneracion" runat="server" AutoGenerateColumns="false">
                            <RowStyle CssClass="GrillaRow" />
                            <HeaderStyle CssClass="GrillaHeader" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>                                    
                                        <asp:LinkButton ID="lbSeleccionar" runat="server" Text="Seleccionar" OnClick="lbSeleccionar_clickPagoRemuneracion" CommandArgument="<%# Container.DataItemIndex %>"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Concepto">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hddidConcepto" runat="server" Value='<%# Eval("id") %>' />
                                        <asp:Label ID="lblNombreConcepto" runat="server" Text='<%# Eval("Nombre") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <!--Capa Asociar documentos con la detracción-->
    <div id="capaAsociarDetracciones" style="width:700px;border:2px solid blue;background-color:White;z-index:3;position:absolute;display:none;top:180px;left:30px">
        <div style="float:right; height: 15px;">
            <%--                            <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" SelectText="Agregar" ShowSelectButton="True" />--%>
            <asp:ImageButton ID="imgBtnCerrar" runat="server" ImageUrl="~/Imagenes/Cerrar.gif" />
        </div>
        <div style="width:100%">
            <table>
                <tr>
                    <td colspan="10">
                        <asp:Label ID="lblMensaje" ForeColor="Red" runat="server" Text="* Deberá marcar los documentos que estan afecto a detracción." Font-Bold="true"></asp:Label>
                        <span style="font-weight:bold">|</span>
                        <asp:LinkButton ID="lbContinuarGrabando" runat="server" Text="Continar Grabando" ForeColor="Blue" Font-Bold="true" Font-Italic="true" OnClientClick="return confirm('Desea continuar con la Operación ?');"></asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td colspan="10">
                        <asp:GridView ID="gv_docAsociadosDetraccion" runat="server" AutoGenerateColumns="false">
                        <RowStyle CssClass="GrillaRow" />
                        <HeaderStyle CssClass="GrillaHeader" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="checkDetraccion" runat="server" />
                                    <asp:HiddenField ID="hddIDDocumento" runat="server" Value = '<%# Eval("idDocumento") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="regNombre" HeaderText="Proveedor" />
                            <asp:BoundField DataField="FechaEmision" HeaderText="Fec. Emision" />
                            <asp:BoundField DataField="Codigo" HeaderText="Nro. Documento" />
                            <asp:TemplateField HeaderText="Total a Pagar">
                            <ItemTemplate>
                                <asp:Label ID="lblMonedaPrincipal" runat="server" Text='<%# Eval("NomMoneda") %>' ForeColor="Red" Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblTotalAPagar" runat="server" Text='<%# Eval("TotalAPagar") %>' ForeColor="Red" Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </div>       

    <!--Capa Aplicacion contra deudas pendientes-->
    <div id="capaAplicarFactura" style="top:50%;left:50%;;width:500px;margin-top:-400px;margin-left:-250px;border:1px solid blue;background-color:White;z-index:5;position:fixed;display:none">
        <div style="float:right; height: 15px;">
            <asp:ImageButton ID="ImageButton5" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF" />
        </div>
        <div style="width:100%">
            <table>
                <tr>
                    <td><asp:Label ID="lblSerieAplicarFac" runat="server" Text="Serie:" CssClass="Texto"></asp:Label></td>
                    <td>
                        <asp:Panel ID="Panel9" runat="server" DefaultButton="btnBuscarFac">
                        <asp:TextBox ID="txtSerieAplicarFac" runat="server" Text="" placeholder="Serie"></asp:TextBox>
                        </asp:Panel>
                    </td>
                    <td>
                        <asp:Label ID="lblCodigoAplicarFac" runat="server" Text="Código:" CssClass="Texto"></asp:Label>
                    </td>
                    <td>
                        <asp:Panel ID="Panel10" runat="server" DefaultButton="btnBuscarFac">
                        <asp:TextBox ID="txtCodigoAplicarFac" runat="server" Text="" placeholder="Código"></asp:TextBox>
                        </asp:Panel>
                    </td>
                    <td><asp:Button ID="btnBuscarFac" runat="server" Text="Buscar" CssClass="btnBuscar" /></td>
                    <td><asp:HiddenField ID="hddIndiceDocExterno" runat="server" Value="-1" /></td>
                </tr>
                <tr>
                    <asp:GridView ID="gv_AplicacionFacturasDeudadPendientes" AutoGenerateColumns="false" runat="server">
                    <RowStyle CssClass="GrillaRow" />
                    <HeaderStyle CssClass="GrillaHeader" />
                        <Columns>                        
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" ID="lkbSeleccionarFac" Text="Seleccionar" OnClick="evento_seleccionarFac" CommandArgument="<%# Container.DataItemIndex %>"></asp:LinkButton>
                                    <asp:HiddenField ID="hddidDocumento" runat="server" Value='<%# Eval("idDocumento") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>   
                            <asp:BoundField DataField="proveedor" HeaderText="Proveedor" />
                            <asp:BoundField DataField="doc_codigo" HeaderText="Número Fac." /> 
                             <asp:TemplateField HeaderText="Total Documento" FooterStyle-HorizontalAlign="Center">
                               <ItemTemplate>
                                   <asp:Label ID="lblMonedaPrincipal" runat="server" Text='<%# Eval("nom_simbolo") %>' ForeColor="Red" Font-Bold="true"></asp:Label>
                                   <asp:Label ID="lblTotalMonto" runat="server" Text='<%# Eval("mcp_Monto","{0:F3}") %>' ForeColor="Red" Font-Bold="true"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle VerticalAlign="Middle" />
                                <ItemStyle Font-Italic="True" HorizontalAlign="Center" VerticalAlign="Middle" /> 
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Total Abono" FooterStyle-HorizontalAlign="Center">
                               <ItemTemplate>
                                   <asp:Label ID="lblMonedaAbono" runat="server" Text='<%# Eval("nom_simbolo") %>' ForeColor="Red" Font-Bold="true"></asp:Label>
                                   <asp:Label ID="lblTotalAbono" runat="server" Text='<%# Eval("mcp_abono","{0:F3}") %>' ForeColor="Red" Font-Bold="true"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle VerticalAlign="Middle" />
                                <ItemStyle Font-Italic="True" HorizontalAlign="Center" VerticalAlign="Middle" /> 
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Total Saldo" FooterStyle-HorizontalAlign="Center">
                               <ItemTemplate>
                                   <asp:Label ID="lblMonedaSaldo" runat="server" Text='<%# Eval("nom_simbolo") %>' ForeColor="Red" Font-Bold="true"></asp:Label>
                                   <asp:Label ID="lblTotalSaldo" runat="server" Text='<%# Eval("mcp_saldo","{0:F3}") %>' ForeColor="Red" Font-Bold="true"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle VerticalAlign="Middle" />
                                <ItemStyle Font-Italic="True" HorizontalAlign="Center" VerticalAlign="Middle" /> 
                             </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </tr>
            </table>
        </div>
    </div>

    <div id="capaAgregarPago" style="width:500px;border:1px solid blue;background-color:White;z-index:99999;position:absolute;display:none;top:15%;left:45%;">
        <div>
            <table width="100%">
                <tr>
                    <td colspan="2" class="TituloCelda">
                        <asp:Label ID="tituloCelda" runat="server" Text=""></asp:Label>
                        <asp:HiddenField ID="hddIdDocumentoCancelacion" Value="0" runat="server" />
                        <asp:HiddenField ID="hddIdFormulario" Value="0" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="Texto">Nro Constancia:</td>
                    <td><asp:TextBox ID="txtNroOperacion" runat="server" Text=""></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="Texto">Monto Depósito:</td>
                    <td><asp:TextBox ID="txtMontoDeposito" runat="server" Text=""></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="Texto">Banco:</td>
                    <td><asp:DropDownList ID="ddlBancoCancelacion" runat="server"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td class="Texto">Fecha de Pago:</td>
                    <td>
                    <asp:TextBox ID="txtFechaPago" runat="server" Text=""></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFechaPago">
                        </cc1:CalendarExtender>
                    </td>
                </tr>
                <tr runat="server" id="tr_PersonaRendir">
                    <td class="Texto">Persona a Rendir:</td>
                    <td>
                          <asp:TextBox ID="txtCodigoEmpleado" runat="server" Text="" Enabled="false" Width="100px"></asp:TextBox>
                          <asp:TextBox ID="txtFiltroEmpleados" runat="server" Text=""></asp:TextBox>
                          <asp:Button ID="btnFiltroEmpleados" runat="server" Text="Buscar" CssClass="btnBuscar"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="gvFiltroEmpleados" runat="server" AutoGenerateColumns="false">
                            <RowStyle CssClass="GrillaRow" />
                            <HeaderStyle CssClass="GrillaHeader" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbSelecEmpleado" runat="server" OnClick="SelectCliente_Click" Text="Seleccioonar"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="IdPersona" HeaderText="Id" NullDisplayText="---" />
                                <asp:BoundField DataField="Nombre" HeaderText="Ap. y Nombres" NullDisplayText="---" />
                                <asp:BoundField DataField="DNI" HeaderText="D.N.I." NullDisplayText="---" />
                            </Columns>

                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnGuardarCancelacion" runat="server" Text="Guardar Cancelación" OnClick="onClickGuardarCancelacion" CssClass="btnGuardar" />
                        <asp:Button ID="btnRetroceder" runat="server" Text="Salir..." />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <asp:HiddenField ID="hddMontoProgramado" runat="server" Value="0" />
    <script language="javascript" type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };

        function MostrarOcultar(ref) {
            // Nodo del documento que se [muestra /oculta]
            // Tiene que tener un ID unico
            var nodoContraible = document.getElementById(ref);
            var img = document.getElementById("imgContraerExpandir");
            // Nodo que contiene el [enlace] que maneja el que se [muestra /oculta]
            // DEBE tener el mismo ID que el nodo [nodoContraible] pero precedido por una x
            //var xNodoDelEnlace = document.getElementById("x" + ref);

            if (nodoContraible.style.display == 'none') {
                //xNodoDelEnlace.innerHTML = ' - Ocultar';
                img.src = "../Imagenes/Menos_B.JPG"
                nodoContraible.style.display = "block";
            }
            else {
                //xNodoDelEnlace.innerHTML = ' + Mostrar ';
                img.src = "../Imagenes/Mas_B.JPG"
                nodoContraible.style.display = 'none';
            }
        }

        //--------------------------------------------------------------------------
        function SeleccionarChkparcial() {


            var chkpagoparcial = document.getElementById('<%= chkpagoparcial.ClientID %>');
            //     var grillaProvisiones = document.getElementById('<%= GV_DocumentosProvisionados.ClientID %>');
            //   var txtMonto = document.getElementById('<%= txtMontoProg.ClientID %>');

            //            var montoDeuda = 0;
            //            var porcAgente = 0;
            //            var montoAgente = 0;
            //            var montoTotalaPagar = 0;
            //            var montoPieMontoAgente = 0;
            //            var montoPieTotalaPagar = 0;
            //            var montoPieOtroImpuesto = 0;
            //            var indicePie = 0;
            //            var simboloMoneda = "";
            //            var otroImpuesto = 0;


            if (chkpagoparcial.checked == false) {
                var grillaProvisiones = document.getElementById('<%= GV_DocumentosProvisionados.ClientID %>');

                var txtMonto = document.getElementById('<%= txtMontoProg.ClientID %>');
                var montoDeuda = 0;
                var porcAgente = 0;
                var montoAgente = 0;
                var montoTotalaPagar = 0;
                var montoPieMontoAgente = 0;
                var montoPieTotalaPagar = 0;
                var montoPieOtroImpuesto = 0;
                var indicePie = 0;
                var simboloMoneda = "";
                var otroImpuesto = 0;

                for (var i = 1; i < grillaProvisiones.rows.length - 1; i++) {//Le quito 1 al total de filas porque el último es el pie de página
                    montoDeuda = parseFloat(grillaProvisiones.rows[i].cells[8].children[1].innerHTML);

                    //  otroImpuesto = parseFloat(grillaProvisiones.rows[i].cells[9].children[0].value);
                    otroImpuesto = 0;
                    //  montoAgente = parseFloat(grillaProvisiones.rows[i].cells[11].children[0].value);
                    montoAgente = 0;
                    if (isNaN(montoAgente)) {
                        montoAgente = 0;
                    }

                    if (isNaN(otroImpuesto)) {
                        otroImpuesto = 0;
                    }
                    // porcAgente = (montoAgente / montoDeuda);
                    porcAgente = 0;

                    montoTotalaPagar = montoDeuda - montoAgente + otroImpuesto;

                    (grillaProvisiones.rows[i].cells[8].children[1].innerHTML) = 0;
                    (grillaProvisiones.rows[i].cells[9].children[0].value) = 0;
                    (grillaProvisiones.rows[i].cells[11].children[0].value) = 0;

                    grillaProvisiones.rows[i].cells[10].children[0].value = redondear(porcAgente, 2);
                    grillaProvisiones.rows[i].cells[12].children[0].value = redondear(montoTotalaPagar, 2);
                    montoPieMontoAgente += montoAgente;
                    montoPieTotalaPagar += montoTotalaPagar;
                    montoPieOtroImpuesto += otroImpuesto;
                    //moneda
                    simboloMoneda = grillaProvisiones.rows[i].cells[3].children[0].innerHTML;
                    //pie de pagina
                    indicePie = grillaProvisiones.rows.length - 1;

                    grillaProvisiones.rows[indicePie].cells[9].innerHTML = simboloMoneda + " " + redondear(montoPieOtroImpuesto, 2);
                    grillaProvisiones.rows[indicePie].cells[11].innerHTML = simboloMoneda + " " + redondear(montoPieMontoAgente, 2);
                    grillaProvisiones.rows[indicePie].cells[12].innerHTML = simboloMoneda + " " + redondear(montoPieTotalaPagar, 2);




                    //                    (grillaProvisiones.rows[indicePie].cells[9].children[0].value) = redondear(porcAgente, 2);
                    //                    (grillaProvisiones.rows[indicePie].cells[10].children[0].value) = 0;
                    //                    (grillaProvisiones.rows[indicePie].cells[11].children[0].value) = 0;

                    //                    (grillaProvisiones.rows[indicePie].cells[9].children[0].value) = 0;  //simboloMoneda + " " + redondear(montoPieOtroImpuesto, 2);
                    //                    (grillaProvisiones.rows[indicePie].cells[11].children[0].value) = 1; //simboloMoneda + " " + redondear(montoPieMontoAgente, 2);
                    //                    grillaProvisiones.rows[indicePie].cells[12].children[0].value = 2; //simboloMoneda + " " + redondear(montoPieTotalaPagar, 2);
                    //                    //txtMonto.value = redondear(montoPieTotalaPagar, 2);
                }
                var montoTotalAplicaciones = 0;
                var montoAplicacion;
                if (document.getElementById('<%= gv_AplicacionFacturas.ClientID %>')) {
                    var grillaAplicacion = document.getElementById('<%= gv_AplicacionFacturas.ClientID %>');
                    for (var j = 1; j < grillaAplicacion.rows.length - 1; j++) {
                        montoAplicacion = parseFloat(grillaAplicacion.rows[j].cells[7].children[0].innerText);
                        montoTotalAplicaciones += montoAplicacion;
                    }
                }
                var monedaTexto = document.getElementById("<%= lblMoneda_MontoProg.ClientID %>").innerHTML;
//                var tc = document.getElementById("<%= txtTipoCambio.ClientID %>").value;
                if (monedaTexto.trim() == "S/.") {
                    var grillaReq = document.getElementById("<%= gv_REquerimientoProgramados.ClientID %>");
                    var monedaReq = grillaReq.rows[1].cells[6].children[0].innerHTML;
                    if (monedaReq.trim() == "US$") {
                        txtMonto.value = redondear(montoPieTotalaPagar * parseFloat(tc), 2);
                    } else {
                        txtMonto.value = redondear(montoPieTotalaPagar - montoTotalAplicaciones, 2);
                    }
                } else {
                    txtMonto.value = redondear(montoPieTotalaPagar - montoTotalAplicaciones, 2);
                }

            }


        }

        //--------------------------------------------------------------------------



    
        function desbloquearCampos() {
            var grilla = document.getElementById('<%= GV_DocumentosProvisionados.ClientID %>');
            var ddlValor = ddl = grilla.rows[0].cells[9].children[0].value;
            if (grilla != null) {

                for (var i = 1; i < grilla.rows.length; i++) {
                    if (i == grilla.rows.length - 1) {
                    } else {
                    if (ddlValor == 300020000) {
                        grilla.rows[i].cells[9].children[0].disabled = false;
                        grilla.rows[i].cells[10].children[0].disabled = false;
                    } else {
                        grilla.rows[i].cells[9].children[0].disabled = true;
                        grilla.rows[i].cells[10].children[0].disabled = true;                   
                    }
                    }
                }
        }
        return true;
        }

        function replicaValor() {
            var grilla = document.getElementById('<%= GV_DocumentosProvisionados.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    if (isNaN(grilla.rows[i].cells[10].children[0].value)) {
                        grilla.rows[i].cells[10].children[0].value = 0;
                    } else {
                        grilla.rows[i].cells[10].children[0].value = grilla.rows[0].cells[10].children[0].value;
                    }
                    
                }
            }
        }

        function mostrarCapaAplicacionFacturas() {
            xcapaBase = document.getElementById('capaBase');
            xcapaBase.style.display = 'block';
            xcapa = document.getElementById('capaAplicarFactura');
            xcapa.style.display = 'block';
            return false;
        }

        function cerrarCapaAplicacionFacturas() {
            xcapaBase = document.getElementById('capaBase');
            xcapaBase.style.display = 'none';
            xcapa = document.getElementById('capaAplicarFactura');
            xcapa.style.display = 'none';
            return false;
        }



        var hddIdDocumentoAnular;

        function valOnClick_btnAnularCheque(objeto) 
        {
            var grilla = document.getElementById('<%=gvEmisionCheques.ClientID%>');
            if (grilla != null) {

                for (var i = 1; i < grilla.rows.length; i++) 
                {
                    var rowElem = grilla.rows[i];
                     hddIdDocumentoAnular = rowElem.cells[12].children[4];
                    var r = confirm('\nDESEA CONTINUAR CON LA ANULACION DEL CHEQUE. ?');

                if (r == true) 
                 {
                     AnularCheque(objeto,hddIdDocumentoAnular);
                    return false; ;
                 } 
                else
                 {
                    alert("No se Realizo ningun cambio");
                    return false;
                 }
               }

            }
            alert("Pendiente...");
            return false; 
            //Esta opción deberá cambiar el estado del cheque a anulado y a su vez actualizar el campo can_nroOpeacion con el valor null de la tabla programacionPago_CxP  para que se active la programacion.
        }


        function AnularCheque(objeto, hddIdDocumentoAnular) 
        {
            var opcion = "AnularCheque";
            var iddoc;

            iddoc = hddIdDocumentoAnular.value;

            var dataDetraccion = new FormData();

            dataDetraccion.append('flag', 'AnularCheque');
            dataDetraccion.append('iddoc', iddoc);

            var xhtProg = new XMLHttpRequest();
            xhtProg.open("POST", "FrmProgramacionPago_CXP.aspx?flag=" + opcion + "&iddoc=" + iddoc, true);

            xhtProg.onreadystatechange = function ()
             {
                 if (xhtProg.readyState == 4 && xhtProg.status == 200)
                 {
                     fun_lista(xhtProg.responseText);
                 }
            }

             xhtProg.send(dataDetraccion);
            return false;

        }


       
        function valOnClick_btnImprimir2V2(control) {
            var grilla = document.getElementById('<%=gvEmisionCheques.ClientID%>');
            var IdDocumento = 0;
            var Banco = "";
            var Formato = "1";
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var boton = rowElem.cells[12].children[2];
                    var hddIdDocumento = rowElem.cells[12].children[4];
                    var Banco = rowElem.cells[0].children[0];
                    var moneda = rowElem.cells[4].textContent;
                  
                    if (control.id == boton.id) {
                        IdDocumento = parseInt(hddIdDocumento.value);
                        Banco = Banco.innerText;
                        Formato = "2";
                        break;
                    }
                }
            }

            if (IdDocumento == 0 || isNaN(IdDocumento)) {
                alert('El documento no tiene Documentos Referenciados. No procede la Operación.');
                return false;
            } else {
                window.open('../../Caja/Reportes/Visor1.aspx?iReporte=21&IdDocumento=' + IdDocumento + '&Banco=' + Banco + '&moneda=' + moneda + '&Formato=' + Formato, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
               // window.open('../../Caja/Reportes/Visor1.aspx?iReporte=19&IdDocumento=' + IdDocumento + '&Banco=' + Banco + '&moneda=' + moneda, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
          
                 return false;
            }
        }


        function valOnClick_btnImprimirVoucher(control) {
            var grilla = document.getElementById('<%=gvEmisionCheques.ClientID%>');
            var IdDocumento = 0;
            var Banco = "";
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var boton = rowElem.cells[12].children[1];
                    var hddIdDocumento = rowElem.cells[12].children[4];
                    var Banco = rowElem.cells[0].children[0];

                    if (control.id == boton.id) {
                        IdDocumento = parseInt(hddIdDocumento.value);
                        Banco = Banco.innerText;
                        break;
                    }
                }
            }

            if (IdDocumento == 0 || isNaN(IdDocumento)) {
                alert('El documento no tiene Documentos Referenciados. No procede la Operación.');
                return false;
            } else {
                
                window.open('../../Caja/Reportes/Visor1.aspx?iReporte=20&IdDocumento=' + IdDocumento + '&Banco=' + Banco, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
              
                return false;
            }
        }

        function verificar() {
            var lblchequediferido = document.getElementById('<%= lblchequediferido.ClientID %>');
            var txtFechaDiferida = document.getElementById('<%= txtFechaDiferida.ClientID %>');
            var checkdiferido = document.getElementById('<%= chkDiferido.ClientID %>');
            if (checkdiferido.checked == false) {
                lblchequediferido.visible = false;
                txtFechaDiferida.Text = "";

            }
            if (checkdiferido.checked == true) {
                lblchequediferido.visible = true;
            }

        }

        function valOnClick_btnImprimir2(control) {
            var grilla = document.getElementById('<%=gvEmisionCheques.ClientID%>');
            var IdDocumento = 0;
            var Banco = "";
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var boton = rowElem.cells[12].children[0];
                    var hddIdDocumento = rowElem.cells[12].children[4];
                    var Banco = rowElem.cells[0].children[0];
                    var moneda = rowElem.cells[4].textContent;

                    if (control.id == boton.id) {
                        IdDocumento = parseInt(hddIdDocumento.value);
                        Banco = Banco.innerText;
                        break;
                    }
                }
            }

            if (IdDocumento == 0 || isNaN(IdDocumento)) {
                alert('El documento no tiene Documentos Referenciados. No procede la Operación.');
                return false;
            } else {
                window.open('../../Caja/Reportes/Visor1.aspx?iReporte=19&IdDocumento=' + IdDocumento + '&Banco=' + Banco + '&moneda=' + moneda, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                return false;
            }
        }

  

        function cerrarAnticipos() {
            var checkboxAdelanto = document.getElementById('<%= chkEsAdelanto.ClientID %>');
            var checkboxCtaRendir = document.getElementById('<%= chkEsCtaPorRendir.ClientID %>');

            checkboxAdelanto.checked = false;
            checkboxCtaRendir.checked = false;
            offCapa('capaAnticipos');
            return false;
        }

        function revisarCheckAnticipo() {
            var grilla = document.getElementById('<%= GV_DocumentosProvisionados.ClientID%>');
            var checkboxAdelanto = document.getElementById('<%= chkEsAdelanto.ClientID %>');
            var checkboxCtaRendir = document.getElementById('<%= chkEsCtaPorRendir.ClientID %>');
            var checkPagoProveedor = document.getElementById('<%= chkPagoProveedor.ClientID %>');
            var capaAnticipos = document.getElementById('capaAnticipoProveedores');
            var capaProgramacion = document.getElementById('capaProgramacionPago');
            if (document.getElementById('<%= GV_DocumentosProvisionados.ClientID%>')) {
                if (document.getElementById('<%= GV_DocumentosProvisionados.ClientID%>').rows.length > 1) {
                    alert('Esta programación tiene uno o mas documentos relacionados.\nNo puede ser considerada como adelanto.');                    
                    return false;
                } else {
                    if (checkboxAdelanto.checked) {
                        capaAnticipos.style.display = "Block";
                        return true;
                    } else {
                        checkboxAdelanto.checked = false;
                        checkPagoProveedor.checked = false;
                        return true;
                    }
                }
            }

    }

    function revisarCheckCtaxRendir() {
        var grilla = document.getElementById('<%= GV_DocumentosProvisionados.ClientID%>');
        var checkboxCtaRendir = document.getElementById('<%= chkEsCtaPorRendir.ClientID %>');
        var checkboxAdelanto = document.getElementById('<%= chkEsAdelanto.ClientID %>');
        var capaCtaxRendir = document.getElementById('capaCtasXRendir');
        var checkPagoProveedor = document.getElementById('<%= chkPagoProveedor.ClientID %>');
        var capaProgramacion = document.getElementById('capaProgramacionPago');
        if (document.getElementById('<%= GV_DocumentosProvisionados.ClientID%>')) {
            if (document.getElementById('<%= GV_DocumentosProvisionados.ClientID%>').rows.length > 1) {
                alert('Esta programación tiene uno o mas documentos relacionados.\nNo puede ser considerada como adelanto.');
                return false;
            } else {
                if (checkboxCtaRendir.checked) {
                    capaCtaxRendir.style.display = "Block";
                    return true;
                } else {
                    checkboxAdelanto.checked = false;
                    checkPagoProveedor.checked = false;
                    return true;
                }
            }

        }

    }

    function revisarCheckPagoProveedor() {
        var grilla = document.getElementById('<%= GV_DocumentosProvisionados.ClientID%>');
        var checkboxCtaRendir = document.getElementById('<%= chkEsCtaPorRendir.ClientID %>');
        var checkboxAdelanto = document.getElementById('<%= chkEsAdelanto.ClientID %>');
        var checkPagoProveedor = document.getElementById('<%= chkPagoProveedor.ClientID %>');
        var capaPagoRemuneracion = document.getElementById('capaPagoRemuneraciones');
        var capaProgramacion = document.getElementById('capaProgramacionPago');
        if (document.getElementById('<%= GV_DocumentosProvisionados.ClientID%>')) {
            if (document.getElementById('<%= GV_DocumentosProvisionados.ClientID%>').rows.length > 1) {
                alert('Esta programación tiene uno o mas documentos relacionados.\nNo puede ser considerada como adelanto.');
                return false;
            } else {
                if (checkPagoProveedor.checked) {
                    capaPagoRemuneracion.style.display = "Block";
                    return true;
                } else {
                    checkboxAdelanto.checked = false;
                    checkboxCtaRendir.checked = false;
                    return true;
                }
            }

        }

    }

        function mostrarCapaAnticipos() {
            //var check = document.getElementById('<%= chkEsAdelanto.ClientID %>');
            //if (check.checked) {
            xcapaBase = document.getElementById('capaBase');
            xcapaBase.style.display = 'block';
            xcapa = document.getElementById('capaAnticipos');
            xcapa.style.display = 'block';
            //} else {
            return false;
            //}
            //return false;
        }

        function validarVoucherFiltro(boton) {
            var spnMensajeFiltroVoucher = document.getElementById('spnMensajeFiltroNroVoucher');
            var spnMensajeFiltroRuc = document.getElementById('spnFiltroRuc');

            var radioButton = document.getElementById('<%= RadioButtonList1.ClientID %>');
            var radio = radioButton.getElementsByTagName('INPUT');
            var radioValor;
            for (var i = 0; i < radio.length; i++) {
                if (radio[i].checked) {
                    radioValor = parseInt(radio[i].value);
                }
            }

            switch (radioValor) {
                case 1: //si es filtro por número de documento
                    var txtSerie = document.getElementById('<%= txtnroSerieProvision.ClientID %>');
                    var txtNumero = document.getElementById('<%= txtNumeroProvision.ClientID %>');

                    if (txtSerie.value.length == 0) {
                        txtSerie.focus();
                        txtSerie.select();
                        spnMensajeFiltro.innerHTML = "Nro de Serie Invalido";
                        return false;
                    }
                    if (txtNumero.value.length == 0) {
                        txtNumero.focus();
                        txtNumero.select();
                        spnMensajeFiltro.innerHTML = "Nro de Documento Invalido";
                        return false;
                    }
                    spnMensajeFiltro.innerHTML = "";
                    break;
                case 2: //si es filtro por proveedor
                    var nroRuc = document.getElementById('<%= txtFiltroRuc.ClientID %>');

                    if (nroRuc.value.length == 0) {
                        spnMensajeFiltroRuc.innerHTML = "Nro Ruc Incorrecto";
                        return false;
                    }
                    spnMensajeFiltroRuc.innerHTML = "";
                    break;
            

                case 3: //si es filtro por proveedor
                    var NroVoucher = document.getElementById('<%= txtNumeroVoucher.ClientID %>');

                    if (NroVoucher.value.length == 0) {
                        spnMensajeFiltroVoucher.innerHTML = "Nro Voucher No Existe";
                        return false;
                    }
                    spnMensajeFiltroVoucher.innerHTML = "";
                    break;
                case 1:
                    return true;
                    break;
                default:
                    return false;
            }
            return true;


        }

        function validarRucFiltro(boton) {
            var spnMensajeFiltro = document.getElementById('spnMensajeFiltroNroDoc');
            var spnMensajeFiltroRuc = document.getElementById('spnFiltroRuc');

            var radioButton = document.getElementById('<%= RadioButtonList1.ClientID %>');
            var radio = radioButton.getElementsByTagName('INPUT');
            var radioValor;
            for (var i = 0; i < radio.length; i++) {
                if (radio[i].checked) {
                    radioValor = parseInt(radio[i].value);
                }
            }

            switch (radioValor) {

                case 1: //si es filtro por número de documento
                    var txtSerie = document.getElementById('<%= txtnroSerieProvision.ClientID %>');
                    var txtNumero = document.getElementById('<%= txtNumeroProvision.ClientID %>'); 

                    if (txtSerie.value.length == 0) {
                        txtSerie.focus();
                        txtSerie.select();
                        spnMensajeFiltro.innerHTML = "Nro de Serie Invalido";
                        return false;
                    }
                    if (txtNumero.value.length == 0) {
                        txtNumero.focus();
                        txtNumero.select();
                        spnMensajeFiltro.innerHTML = "Nro de Documento Invalido";
                        return false;
                    }
                    spnMensajeFiltro.innerHTML = "";
                    break;
                case 2: //si es filtro por proveedor
                    var nroRuc = document.getElementById('<%= txtFiltroRuc.ClientID %>');

                    if (nroRuc.value.length == 0) {
                        spnMensajeFiltroRuc.innerHTML = "Nro Ruc Incorrecto";
                        return false;
                    }
                    spnMensajeFiltroRuc.innerHTML = "";
                    break;
                case 1:
                    return true;
                    break;
                default:
                    return false;
            }
            return true;

//            boton.disabled = true;
//            boton.value = 'Procesando...';
        }

        function mostrarProvisiones() {
            xcapaBase = document.getElementById('capaBase');
            xcapaBase.style.display = 'block';
            xcapa = document.getElementById('capaDocProvisionados');
            xcapa.style.display = 'block';
            return false;
        }

        


        //***************************************************** BUSQUEDA PERSONA
        function mostrarCapaProgramacion() {
            xcapaBase = document.getElementById('capaBase');
            xcapaBase.style.display = 'block';
            xcapa = document.getElementById('capaProgramacionPago');
            xcapa.style.display = 'block';
            return false;
        }

        function valOnClick_btnMostrarDetalle(control) {

            var grilla = document.getElementById('<%=GV_MovCuenta_CXP.ClientID%>');
            var IdDocumento = 0;
            
            var IdUsuario = '<%= Session("IdUsuario") %>';
            
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var boton = rowElem.cells[9].children[0];
                    var hddIdDocumento = rowElem.cells[2].children[1];
                    if (control.id == boton.id) {
                        IdDocumento = parseInt(hddIdDocumento.value);
                        break;
                      }
                                                             }
                                                         }
                 if (IdDocumento == 0 || isNaN(IdDocumento)) {
                     alert('El documento no tiene Documentos Referenciados. No procede la Operación.');
                     return false;
                 } else {
                     window.open('../Finanzas/FrmRequerimientoGasto_V2.aspx?IdDocumento=' + IdDocumento + '&IdUsuario=' + IdUsuario, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                     return false;
                 }
             }

             function valOnClick_btnCancelacion(control) {

                 var grilla = document.getElementById('<%=GV_MovCuenta_CXP.ClientID%>');
                 var IdDocumento = 0;

                 var IdUsuario = '<%= Session("IdUsuario") %>';

                 if (grilla != null) {
                     for (var i = 1; i < grilla.rows.length; i++) {
                         var rowElem = grilla.rows[i];
                         var boton = rowElem.cells[10].children[0];
                         var hddIdDocumento = rowElem.cells[2].children[1];
                         if (control.id == boton.id) {
                             IdDocumento = parseInt(hddIdDocumento.value);
                             break;
                         }
                     }
                 }
                 if (IdDocumento == 0 || isNaN(IdDocumento)) {
                     alert('El documento no tiene Documentos Referenciados. No procede la Operación.');
                     return false;
                 } else {
                     window.open('../Finanzas/FrmDocCancelacionBancos.aspx?IdDocumento=' + IdDocumento + '&IdUsuario=' + IdUsuario, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                     return false;
                 }
             }


//        function valOnClick_btnMostrarDetalle(control) {

//            var grilla = document.getElementById('<%=GV_MovCuenta_CXP.ClientID%>');
//            var IdDocumento = 0;
//            var IdUsuario = 0;
//            if (grilla != null) {
//                for (var i = 1; i < grilla.rows.length; i++) {
//                    var rowElem = grilla.rows[i];
//                    var boton = rowElem.cells[8].children[0].cells[0].children[0];
//                    var hddIdDocumento = rowElem.cells[2].children[0].cells[1].children[0];
//                    var hddIdUsuario = rowElem.cells[2].children[0].cells[4].children[0];
//                    if (control.id == boton.id) {
//                        IdDocumento = parseInt(hddIdDocumento.value);
//                        IdUsuario = parseInt(hddIdUsuario.value)
//                        break;
//                    }


//                }
//            }

//            window.open('../../Compras/frmOrdenCompra1.aspx?IdDocumento=' + IdDocumento + '&IdUsuario=' + IdUsuario, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
//            return false;
//        }
        
        
        
        
        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (caracter == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(obj, e) {
            //obtener caracter pulsado en todos los exploradores
            var evento = e || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);       
            if (caracter == 13) {
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }
        function valOnClick_btnBuscar_Persona() {
            mostrarCapaPersona();
            return false;
        }

//        function valOnClick_btnGuardarEmitirCheque() {
//            var chk = document.getElementById('<%= chkDiferido.ClientID %>');

//            if (chkDiferido.checked == true) 
//            {
//                return confirm('\nDESEA CONTINUAR CON LA CREACION. ?');
//                return false;
//            }

//        }

//        function valOnClick_btnAnular_ChequeE() {
//            var grilla = null;
//        }
        function valOnClick_btnGuardar() {
            var cboMEdioPago = document.getElementById('<%= cboMedioPago_Prog.ClientID %>');
            var cboBAnco = document.getElementById('<%= cboBanco_Prog.ClientID %>');
            var cboCuentaBancaria = document.getElementById('<%= cboCuentaBancaria_Prog.ClientID %>')
            var checkProveedor = document.getElementById('<%= chkEsAdelanto.ClientID %>');
            var checkTrabajador = document.getElementById('<%= chkEsCtaPorRendir.ClientID %>');
            var checkPagoProveedor = document.getElementById('<%= chkPagoProveedor.ClientID %>');
            var grilla = null;
            var grillaValidacion = null;
            if (cboMEdioPago.value == 0) {
                alert('Operación Inválida, Ingresar Medio de Pago.')
                cboMEdioPago.focus();
                return false;
            }

            if (cboBAnco.value == 0) {
                alert('Operación Inválida, Ingresar Banco.')
                cboBAnco.focus();
                return false;
            }

            if (cboCuentaBancaria.value == 0) {
                alert('Operación Inválida, Ingresar Cuenta Bancaria.');
                cboCuentaBancaria.focus();
                return false;
            }

            if (document.getElementById('<%= gvConceptoReferencia.ClientID %>')) {
                grillaValidacion = document.getElementById('<%= gvConceptoReferencia.ClientID %>');
            }
            if (document.getElementById('<%= GV_DocumentosProvisionados.ClientID%>')) {
                grilla = document.getElementById('<%= GV_DocumentosProvisionados.ClientID%>');
            }
            if (checkProveedor.checked == false && checkTrabajador.checked == false && checkPagoProveedor.checked == false && grilla.rows.length <= 1) {
                alert('Para continuar deberá marcar el tipo de anticipo / cta Rendir.');
                return false;
            }

            if (checkProveedor.checked || checkTrabajador.checked || checkPagoProveedor.checked) {
                if (grilla != null && grilla.rows.length <= 1) {
                    if (grilla.rows.length >     1) {
                        alert('Esta programación no puede ser considerada como Adelanto o Cuenta por rendir debido a que se ha agregado documentos sustentos.');
                        return false;
                    }
                }
            }

            var observacion = document.getElementById('<%= txtObservaciones_Prog.ClientID %>');
            if (observacion.value.length == 0) {    
                alert('INGRESE UNA OBSERVACIÓN. NO SE PERMITE LA OPERACIÓN');
                return false;
            }

            var txtMontoProg = document.getElementById('<%= txtMontoProg.ClientID %>');
            if (txtMontoProg.value=="0") {
                alert('INGRESE UN MONTO DIFERENTE A CERO NO SE PERMITE LA OPERACIÓN');
                return false;
            }



            if (checkProveedor.checked || checkTrabajador.checked || checkPagoProveedor.checked) {
                var mensaje;
                if (checkProveedor.checked) {
                    mensaje = checkProveedor.nextSibling.innerHTML.toUpperCase();
                }
                if (checkTrabajador.checked) {
                    mensaje = checkTrabajador.nextSibling.innerHTML.toUpperCase();
                }
                if (checkPagoProveedor.checked) {
                    mensaje = checkPagoProveedor.nextSibling.innerHTML.toUpperCase();
                }
                
                if (grillaValidacion == null) {
                    alert('Operación Inválida, deberá elegir un anticipo o cta por Rendir.');
                    return false;
                } else {
                return confirm('\nDESEA CONTINUAR CON LA OPERACIÓN. ?');
                }
            } //fin del if checks
            if (checkProveedor.checked == false && checkTrabajador.checked == false && checkPagoProveedor.checked == false && grilla != null) {
                var mensaje;
                var montoPagar = 0;
                var tipoAgente;
                var comboMoneda = document.getElementById('<%= cboTipoMoneda.ClientID %>');
                var moneda = comboMoneda.options[comboMoneda.selectedIndex].text;
                var montoAplicacion = 0;
                var grillaAplicacionfacturas;
                if (document.getElementById('<%=gv_AplicacionFacturas.ClientID %>')) {
                    grillaAplicacionfacturas = document.getElementById('<%=gv_AplicacionFacturas.ClientID %>');
//                    if (grillaAplicacionfacturas != null) {
//                        for (var i = 1; i < grillaAplicacionfacturas.rows.length; i++) {
//                            moneda = grillaAplicacionfacturas.rows[i].cells[5].children[0].innerHTML;
//                            montoAplicacion += parseFloat(grillaAplicacionfacturas.rows[i].cells[5].children[1].innerHTML);
//                        }
//                    }
                }

                return confirm( '\nDesea continuar con la Operación ?');
            } else {alert('Operación inválida. Deberá marcar un Anticipo o Cta Por Rendir.');return false;}
        }

        function valOnClick_btnEliminar() {
            return confirm('Desea continuar con la Operación ?');
        }

        function valNavegacion_Det(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Det.ClientID%>').value);
            var grilla = document.getElementById('<%=GV_MovCuenta_CXP.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Det.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
            }
            return true;
        }

        function calcularTotalPagarXporc() {
            var grillaProvisiones = document.getElementById('<%= GV_DocumentosProvisionados.ClientID %>');
            var txtMonto = document.getElementById('<%= txtMontoProg.ClientID %>');
            var montoDeuda = 0;
            var porcAgente = 0;
            var montoAgente = 0;
            var montoTotalaPagar = 0;
            var montoPieMontoAgente = 0;
            var montoPieTotalaPagar = 0;
            var montoPieOtroImpuesto = 0;
            var indicePie = 0;
            var simboloMoneda = "";
            var otroImpuesto = 0;

            for (var i = 1; i < grillaProvisiones.rows.length - 1; i++) {//Le quito 1 al total de filas porque el último es el pie de página
                montoDeuda = parseFloat(grillaProvisiones.rows[i].cells[8].children[1].innerHTML);
                porcAgente = parseFloat(grillaProvisiones.rows[i].cells[10].children[0].value);
                otroImpuesto = parseFloat(grillaProvisiones.rows[i].cells[9].children[0].value);
                if (isNaN(porcAgente)) {
                    porcAgente = 0;
                }

                if (isNaN(otroImpuesto)) {
                    otroImpuesto = 0;
                }

                montoAgente = montoDeuda * porcAgente;
                montoTotalaPagar = montoDeuda - montoAgente + otroImpuesto;

                grillaProvisiones.rows[i].cells[11].children[0].value = redondear(montoAgente, 2);
                grillaProvisiones.rows[i].cells[12].children[0].value = redondear(montoTotalaPagar, 2);
                montoPieMontoAgente += montoAgente;
                montoPieTotalaPagar += montoTotalaPagar;
                montoPieOtroImpuesto += otroImpuesto;
                //moneda
                simboloMoneda = grillaProvisiones.rows[i].cells[3].children[0].innerHTML;
                //pie de pagina
                indicePie = grillaProvisiones.rows.length - 1;
                grillaProvisiones.rows[indicePie].cells[9].innerHTML = simboloMoneda + " " + redondear(montoPieOtroImpuesto, 2);
                grillaProvisiones.rows[indicePie].cells[11].innerHTML = simboloMoneda + " " + redondear(montoPieMontoAgente, 2);
                grillaProvisiones.rows[indicePie].cells[12].innerHTML = simboloMoneda + " " + redondear(montoPieTotalaPagar, 2);
                //txtMonto.value = redondear(montoPieTotalaPagar, 2);
            }
            var montoTotalAplicaciones = 0;
            var montoAplicacion;
            if (document.getElementById('<%= gv_AplicacionFacturas.ClientID %>')) {
                var grillaAplicacion = document.getElementById('<%= gv_AplicacionFacturas.ClientID %>');
                for (var j = 1; j < grillaAplicacion.rows.length - 1; j++) {
                    montoAplicacion = parseFloat(grillaAplicacion.rows[j].cells[7].children[0].innerText);
                    montoTotalAplicaciones += montoAplicacion;
                }
            }
            txtMonto.value = redondear(montoPieTotalaPagar - montoTotalAplicaciones, 2);
        }

        function calcularTotalPagarXmonto() {
            var grillaProvisiones = document.getElementById('<%= GV_DocumentosProvisionados.ClientID %>');
            
            var txtMonto = document.getElementById('<%= txtMontoProg.ClientID %>');
            var montoDeuda = 0;
            var porcAgente = 0;
            var montoAgente = 0;
            var montoTotalaPagar = 0;
            var montoPieMontoAgente = 0;
            var montoPieTotalaPagar = 0;
            var montoPieOtroImpuesto = 0;
            var indicePie = 0;
            var simboloMoneda = "";
            var otroImpuesto = 0;

            for (var i = 1; i < grillaProvisiones.rows.length - 1; i++) {//Le quito 1 al total de filas porque el último es el pie de página
                montoDeuda = parseFloat(grillaProvisiones.rows[i].cells[8].children[1].innerHTML);
                otroImpuesto = parseFloat(grillaProvisiones.rows[i].cells[9].children[0].value);
                montoAgente = parseFloat(grillaProvisiones.rows[i].cells[11].children[0].value);

                monto_pagar_nuevo = parseFloat(grillaProvisiones.rows[i].cells[13].children[0].value);

                if (isNaN(montoAgente)) {
                    montoAgente = 0;
                }

                if (isNaN(otroImpuesto)) {
                    otroImpuesto = 0;
                }
                if (isNaN(monto_pagar_nuevo)) {
                    monto_pagar_nuevo = 0;
                }



                porcAgente = (montoAgente / montoDeuda);
              //  montoTotalaPagar = montoDeuda - montoAgente + otroImpuesto;
                montoTotalaPagar = monto_pagar_nuevo;
                if (montoAgente == 0) {
                    grillaProvisiones.rows[i].cells[10].children[0].value = 0;
                }
                else { grillaProvisiones.rows[i].cells[10].children[0].value = redondear(porcAgente, 2);}


               // grillaProvisiones.rows[i].cells[12].children[0].value = redondear(porcAgente, 2);

                montoPieMontoAgente += montoAgente;
                montoPieTotalaPagar += montoTotalaPagar;
                montoPieOtroImpuesto += otroImpuesto;
                //moneda
                simboloMoneda = grillaProvisiones.rows[i].cells[3].children[0].innerHTML;
                //pie de pagina
                indicePie = grillaProvisiones.rows.length - 1;
                grillaProvisiones.rows[indicePie].cells[9].innerHTML = simboloMoneda + " " + redondear(montoPieOtroImpuesto, 2);
                grillaProvisiones.rows[indicePie].cells[11].innerHTML = simboloMoneda + " " + redondear(montoPieMontoAgente, 2);
               // grillaProvisiones.rows[indicePie].cells[12].innerHTML = simboloMoneda + " " + redondear(montoPieTotalaPagar, 2);
                //txtMonto.value = redondear(montoPieTotalaPagar, 2);
            }
            var montoTotalAplicaciones = 0;
            var montoAplicacion;
            if (document.getElementById('<%= gv_AplicacionFacturas.ClientID %>')) {
                var grillaAplicacion = document.getElementById('<%= gv_AplicacionFacturas.ClientID %>');
                for (var j = 1; j < grillaAplicacion.rows.length - 1; j++) {
                    montoAplicacion = parseFloat(grillaAplicacion.rows[j].cells[7].children[0].innerText);
                    montoTotalAplicaciones += montoAplicacion;
                }
            }
            var monedaTexto = document.getElementById("<%= lblMoneda_MontoProg.ClientID %>").innerHTML;
            var tc = document.getElementById("<%= txtTipoCambio.ClientID %>").value;
            if (monedaTexto.trim() == "S/.") 
            {
                var grillaReq = document.getElementById("<%= gv_REquerimientoProgramados.ClientID %>");
                var monedaReq = grillaReq.rows[1].cells[6].children[0].innerHTML;

                if (monedaReq.trim() == "US$") 
                {
                    //txtMonto.value = redondear(montoPieTotalaPagar * parseFloat(tc), 2);
                    txtMonto.value =   redondear((montoPieTotalaPagar-montoTotalAplicaciones) * parseFloat(tc), 2);
                } 
                else {
                    txtMonto.value = redondear(montoPieTotalaPagar - montoTotalAplicaciones, 2);
                   // txtMonto.value = redondear(montoPieTotalaPagar, 2);
                }
            }
           else
             {
               txtMonto.value = redondear(montoPieTotalaPagar - montoTotalAplicaciones, 2);
               //  txtMonto.value = redondear(montoPieTotalaPagar, 2);    
            }
        }


        function calcularTotalPagarNuevo() {
            var grillaProvisiones = document.getElementById('<%= GV_DocumentosProvisionados.ClientID %>');

         
            var montoDeuda = 0;
            var porcAgente = 0;
            var montoAgente = 0;
            var montoTotalaPagar = 0;
            var montoPieMontoAgente = 0;
            var montoPieTotalaPagar = 0;
            var montoPieOtroImpuesto = 0;
            var indicePie = 0;
            var simboloMoneda = "";
            var otroImpuesto = 0;
            var montofinal = 0;

            for (var i = 1; i < grillaProvisiones.rows.length - 1; i++) {//Le quito 1 al total de filas porque el último es el pie de página
                montoDeuda = parseFloat(grillaProvisiones.rows[i].cells[8].children[1].innerHTML);
                monto_otro_importe = parseFloat(grillaProvisiones.rows[i].cells[9].children[0].value);
                montofinal = parseFloat(grillaProvisiones.rows[i].cells[13].children[0].value);
                montodetraccion = parseFloat(grillaProvisiones.rows[i].cells[11].children[0].value);
                montoporpagar = montoDeuda - montodetraccion
              
                if (isNaN(monto_otro_importe)) {
                    monto_otro_importe = 0;
                }
                if (isNaN(montofinal)) {
                    montofinal = 0;
                }
                montoPieporpagar = (montoporpagar - montofinal) + monto_otro_importe;
               
                if (isNaN(montofinal)) {
                    montofinal = 0;
                }
                grillaProvisiones.rows[i].cells[12].children[0].value = redondear(montofinal, 2);
                //                grillaProvisiones.rows[i].cells[9].children[0].value = montofinal - montoDeuda;

                indicePie = grillaProvisiones.rows.length - 1;
                //grillaProvisiones.rows[indicePie].cells[12].innerHTML = simboloMoneda + " " + redondear(montofinal, 2);
            }

            calcularTotalPagarXmonto();
        }



        function valOnClick_btnGenDocRetencion() {
          
         
          imprimir(1)
        }


        function imprimir(tipoimpresion) {

        }




    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>
