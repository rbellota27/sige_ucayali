﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ProgPagoDocExterno.aspx.vb" Inherits="APPWEB.ProgPagoDocExterno" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
 
 
 
 
  
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaProgramacionPago'));" />
                </td>
            </tr>
              <tr>
            <td class="TituloCelda">
                PROGRAMACIÓN DE PAGOS
            </td>
             
            
        </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" Width="70px" ToolTip="Nuevo" />
                            </td>
                            <td>
                                <asp:Button ID="btnEditar" runat="server" Text="Editar" Width="70px" ToolTip="Editar Programación de Pago" />
                            </td>
                            <td>
                                <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" Width="70px" ToolTip="Eliminar Programación de Pago"
                                    OnClientClick="return(  valOnClick_btnEliminar() );" />
                            </td>
                            <td>
                                <asp:Button ID="btnGuardar" runat="server" Text="Guardar" Width="70px" ToolTip="Registrar Programación de Pago"
                                    OnClientClick="return(  valOnClick_btnGuardar() );" />
                            </td>
                            <td>
                                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" Width="70px" ToolTip="Cancelar" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_ProgramacionPago" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table class="style1">
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td style="text-align: right; font-weight: bold" class="Texto">
                                                            Fecha de Pago:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtFecha_ProgPago" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                                Width="100px"></asp:TextBox>
                                                            <cc1:MaskedEditExtender ID="txtFecha_ProgPago_MaskedEditExtender" runat="server"
                                                                ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                                                CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                                CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFecha_ProgPago">
                                                            </cc1:MaskedEditExtender>
                                                            <cc1:CalendarExtender ID="txtFecha_ProgPago_CalendarExtender" runat="server" Enabled="True"
                                                                Format="dd/MM/yyyy" TargetControlID="txtFecha_ProgPago">
                                                            </cc1:CalendarExtender>
                                                        </td>
                                                       <td></td>
                                                        <td></td> 
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                        <asp:Label ID="lblTituloDocumentos" runat="server" Text="Documento: " Visible="false" style="text-align: right;color:Red" ></asp:Label>
                                                        </td>
                                                        <td>
                                                         <asp:Label ID="lblDocumento" runat="server" Text=" " Visible="false" style="text-align: right;color:blue" ></asp:Label>
                                                         </td>
                                                         <td>  </td>
                                                         <td>  </td>
                                                           <td>
                                                        <asp:Label ID="lblTituloTotalDeuda" runat="server" Text="Monto Total: " style="text-align: right;color:Red"  Visible="false"></asp:Label>
                                                        </td>
                                                        <td>
                                                         <asp:Label ID="lblDeudaResult" runat="server" Text=" " Visible="false" style="text-align: right;color:blue"></asp:Label>
                                                         </td>
                                                         <td>  </td>
                                                         <td>  </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right; font-weight: bold" class="Texto">
                                                            Medio de Pago:
                                                        </td>
                                                        <td colspan="3">
                                                            <asp:DropDownList ID="cboMedioPago_Prog" runat="server" Width="100%">
                                                            </asp:DropDownList>
                                                        </td>
                                                         <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                          <td>
                                                        <asp:Label ID="lblTituloProveedor" runat="server" Text="Proveedor: " Visible="false" style="text-align: right;color:Red"></asp:Label>
                                                        </td>
                                                        <td>
                                                         <asp:Label ID="lblProveedorResult" runat="server" Text=" " Visible="false" style="text-align: right;color:blue"  ></asp:Label>
                                                         </td>
                                                         <td>  </td>
                                                         <td>  </td>
                                                         <td>
                                                          <asp:Label ID="lblTituloDeudaRestante" runat="server" Text="Deuda Restante: " style="text-align: right;color:Red"  Visible="false"></asp:Label>
                                                         </td>
                                                         <td>
                                                          <asp:Label ID="lblmonedaResult" runat="server" Text=" " style="text-align: right;color:Blue"  Visible="false"></asp:Label>
                                                          <asp:Label ID="lblDeudaRestanteResult" runat="server" Text="0" style="text-align: right;color:Blue;background-color:Yellow   "  Visible="false"></asp:Label>
                                                            <asp:Label ID="lblValoreditarDeudaSave" runat="server" Text="0" style="text-align: right;color:Green"  Visible="false"></asp:Label>
                                                         </td>
                                                         
                                                         <td>  </td>
                                                         <td>  </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right; font-weight: bold" class="Texto">
                                                            Banco:
                                                        </td>
                                                        <td colspan="3">
                                                            <asp:DropDownList ID="cboBanco_Prog" runat="server" Width="100%" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                        <asp:Label ID="lblTituloFechaEmision" runat="server" Text="Fecha Emisión: " Visible="false" style="text-align: right;color:Red"></asp:Label>
                                                        </td>
                                                        <td>
                                                         <asp:Label ID="lblfechaemisionResult" runat="server" Text=" " Visible="false" style="text-align: right;color:Blue" ></asp:Label>
                                                         </td>
                                                        <td>  </td>
                                                        <td>  </td>
                                                         <td>
                                                        <asp:Label ID="lblTituloFechaVenc" runat="server" Text="Fecha Vencimiento: " Visible="false" style="text-align: right;color:Red"></asp:Label>
                                                        </td>
                                                         <td>
                                                         <asp:Label ID="lblfechaVenciResult" runat="server" Text=" " Visible="false" style="text-align: right;color:Blue" ></asp:Label>
                                                         </td>
                                                         <td>  </td>
                                                        <td>  </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right; font-weight: bold" class="Texto">
                                                            Cuenta Bancaria:
                                                        </td>
                                                        <td colspan="3">
                                                            <asp:DropDownList ID="cboCuentaBancaria_Prog" runat="server" Width="100%" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                         <td>
                                                         <asp:Label ID="lblTituloCondicionomercia" runat="server" Text="Condicion Comercial: "  Visible="false" style="text-align: right;color:Red"   ></asp:Label>
                                                          </td>
                                                            <td>
                                                            <asp:Label ID="lblCondicionComerciaResult" runat="server" Text=" " Visible="false" style="text-align: right;color:Blue" ></asp:Label>
                                                            </td>
                                                         <td>  </td>
                                                         <td>  </td>
                                                        
                                                              <td>
                                                         <asp:Label ID="lblTituloCondicionPago" runat="server" Text="Condicion de Pago: "  Visible="false" style="text-align: right;color:Red"   ></asp:Label>
                                                          </td>
                                                            <td>
                                                            <asp:Label ID="lblCondicionPagoResult" runat="server" Text=" " Visible="false" style="text-align: right;color:Blue" ></asp:Label>
                                                            </td>
                                                           
                                                            <td>  </td>
                                                            <td>  </td>
                                                    </tr>
                                                    <tr>
                                                    <td style="text-align: right; font-weight: bold" class="Texto">
                                                    <asp:Label ID="lblTipoMoneda" runat="server" Text ="Tipo Moneda: " ></asp:Label>
                                                    </td>
                                                    <td colspan="3">
                                                    <asp:DropDownList ID="cboTipoMoneda" runat="server" Width="100%"  AutoPostBack="true"  Enabled="false"></asp:DropDownList>
                                                    </td>
                                                    <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    <td>
                                                     <asp:Label ID="lblTituloSaldoProgramado" runat="server" Text="Saldo Programado: "  Visible="false" style="text-align: right;color:Red"   ></asp:Label>
                                                    </td>
                                                         <td>
                                                         <asp:Label ID="lblSaldoProgramado" runat="server" Text=" " Visible="false" style="text-align: right;color:Blue" ></asp:Label>
                                                         </td>
                                                           
                                                            <td>  </td>
                                                            <td>  </td>
                                                    </tr>
                                                    <tr>
                                                     <td class="Texto" style="font-weight: bold; text-align: right">
                                                            Monto:
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMoneda_MontoProg" runat="server" Text="S/." CssClass="LabelRojo"
                                                                            Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtMontoProg" Text="0" Font-Bold="true" Width="70px" onKeyPress=" return( validarNumeroPuntoPositivo('event') ); "
                                                                            onFocus=" return( aceptarFoco(this)  ); " onblur=" return( valBlur(event)  ); " runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                      <td style="text-align: right; font-weight: bold" class="Texto"> 
                                                      <asp:Label ID="lblTipoCambio" Text="Tipo de Cambio:" runat="server" Visible="false">                                                     
                                                      </asp:Label>
                                                      </td>
                                                      <td><asp:TextBox ID="txtTipoCambio" Visible="false"  runat="server" Text="0" Font-Bold="true" Width="70px" onKeyPress=" return( validarNumeroPuntoPositivo('event') ); " ></asp:TextBox></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                          <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                    <td class="Texto" style="text-align: right; font-weight: bold"> 
                                                    <asp:Label ID="lblResultadoTipoCambio" runat="server" Text="Monto a Pagar:" Visible="false"></asp:Label>
                                                                                                      </td>
                                                    <td colspan="3">
                                                    <table>
                                                    <tr>
                                                    <td>
                                                       <asp:Label ID="lblResultadoDolares" runat="server" Text="US$." Font-Bold="true" Visible="false" Style="color:Red"></asp:Label>
                                                    </td>
                                                    <td>
                                                       <asp:TextBox ID="txtResultadoTipoCambio" runat="server" Text="0" AutoPostBack="true" Visible="false" ReadOnly ="true" ></asp:TextBox>
                                                    </td>
                                                    <td></td>
                                                    
                                                       <td>
                                                    <asp:Button  ID="btncalcularTC" runat="server"  Text="Calcular Monto" AutoPostBack="true" Visible="false"/>
                                                    </td>
                                                    </tr>
                                                    </table>
                                                 
                                                    </td>
                                                                                       
                                                                                                    
                                                    
                                                    </tr>
                                                    <tr>
                                                        <td class="Texto" style="text-align: right; font-weight: bold">
                                                            Observaciones:
                                                        </td>
                                                        <td colspan="3">
                                                            <asp:TextBox ID="txtObservaciones_Prog" runat="server" Height="70px" TextMode="MultiLine"
                                                                onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                                Width="300px"></asp:TextBox>
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>                                      
                                                            <td>
                                                             <asp:Label ID="lblindex" runat="server" Text=" " Visible="false"></asp:Label>
                                                            </td>
                                                             <td></td>
                                                        <td></td> 
                                                   
                                                    </tr>
                                                    
                                                </table  >
                                                <table width="100%" >
                                                  <tr>
                                                    <td class="SubTituloCelda">
                                                      CUENTA BANCARIA - PROVEEDOR
                                                    </td>
                                                </tr>
                                                <tr>
                                                 <td align="left">
                                                        
                                                    <asp:GridView ID="dgvCuentaBancaria" runat="server" AutoGenerateColumns="False" Width="100%" >
                                                    <Columns>                  
                                                
                                                    <asp:BoundField DataField="Empresa" HeaderText="Empresa" >
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Banco" HeaderText="Banco" NullDisplayText="0" />
                                                    <asp:BoundField DataField="MonSimbolo" HeaderText="Moneda" NullDisplayText="0" />
                                                    <asp:BoundField DataField="NroCuentaBancaria" HeaderText="Numero" NullDisplayText="0" />
                                                    <asp:BoundField DataField="Estado" HeaderText="Estado" NullDisplayText="0" />
                                                  
                                                    </Columns>

                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                  
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <RowStyle CssClass="GrillaRow" />
                                              
                                                    </asp:GridView>
                                                        </td> 
                                                </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                        <td>
                                        <table width="100%">
                                        <tr>
                                                    <td class="SubTituloCelda">
                                                      DOCUMENTOS EXTERNOS RELACIONADOS
                                                    </td>
                                                </tr>
                                        <tr>
                                        <td> 
                                                <asp:GridView ID="GV_DocumentoRelacionado" runat="server" AutoGenerateColumns="False" Width="100%">
                                                   <Columns>                  
                                                
                                                    <asp:BoundField DataField="NroDocumento" HeaderText="Documento" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" 
                                                     ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" ItemStyle-ForeColor="Red" HeaderStyle-Font-Bold="true" ItemStyle-Font-Bold="true"  >
                                                        </asp:BoundField>
                                                    <asp:BoundField DataField="DescripcionPersona" HeaderText="Beneficiario" NullDisplayText="0"
                                                    HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="25px" ItemStyle-Font-Bold="true"
                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true"/>
                                                    
                                                    <asp:BoundField DataField="FechaEmision" HeaderText="Fecha emisión" DataFormatString="{0:dd/MM/yyy}" 
                                                    HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"  ItemStyle-Height="25px" 
                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true"/>
                                                     

                                                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"  HeaderText="Total" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMonedaRefExt" runat="server" Font-Bold="true" ForeColor="Red"   Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblMontoRefExt" runat="server"  Font-Bold="true" ForeColor="Red"  Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F3}")%>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                     
                                                    </Columns>
                                                      <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                  
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <RowStyle CssClass="GrillaRow" />
                                                </asp:GridView>
                                                </td>
                                        </tr>
                                        </table>
                                        </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
              <tr>
            <td class="TituloCelda">
                PROGRAMACIONES REALIZADAS
            </td>
             
            
        </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_ProgramacionPago_Grilla" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_ProgramacionPago" runat="server" AutoGenerateColumns="False"
                                        Width="100%">
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <Columns>
                                            <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" SelectText="Seleccionar" ShowSelectButton="True" />
                                            <asp:BoundField DataField="FechaPagoProg" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Height="25px"
                                                HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha de Pago" ItemStyle-Font-Bold="true"
                                                ItemStyle-ForeColor="Red" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblMoneda_MontoProg" runat="server" Font-Bold="true" ForeColor="Red"
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblMontoProg" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"MontoPagoProg","{0:F3}")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdProgramacionPago_CXP" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProgramacionPago")%>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="MedioPago" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Medio de Pago" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="Banco" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Banco" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="NroCuentaBancaria" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Cuenta Bancaria" ItemStyle-HorizontalAlign="Center" />
                                             <asp:BoundField DataField="MonedaNew" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Tipo Moneda" ItemStyle-HorizontalAlign="Center" />
                                                 <asp:BoundField DataField="TipoCambio" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Tipo de Cambio" ItemStyle-HorizontalAlign="Center" />
                                                
                                                <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Documento Ref." ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbldocreferencia" runat="server" Font-Bold="true" ForeColor="Red"
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumentoRef")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdDocReferencia" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumentoRef")%>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                
                                                
                                                
                                        </Columns>
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>

</body>
</html>
