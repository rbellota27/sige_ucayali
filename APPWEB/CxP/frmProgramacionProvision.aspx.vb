﻿Imports System.Linq
Public Class frmProgramacionProvision
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    Private objNegCta As New Negocio.CuentaBancaria
    Private objNegDocumentocheque As New Negocio.DocumentoCheque
    Private IdDocumento As Integer = 0
    Private IdMovCuentaCXP As Integer = 0
    Private IdTipoDocumento As Integer = 0
    Private Index As Integer = 0
    Private objNegSeriecheque As New Negocio.SerieChequeView
    Private ListaDocProvisionados As List(Of Entidades.Documento)
    Private Util As New Util
    'Este medio de Pago esta asociado a los cheques generados por la empresa (egresos)
    Private Const _IDMEDIOPAGO As Integer = 11
    Private Const _IDTIPODOCUMENTO As Integer = 42
    Private Const _IDCONCEPTOMOVBANCO As Integer = 13

    Protected Sub evento_quitar(ByVal sender As Object, ByVal e As EventArgs)
        Dim linkButton As LinkButton = CType(sender, LinkButton)
        Dim indice As Integer = CInt(linkButton.CommandArgument)
        'Actualiza el saldo del requerimiento - no en la bd sino visualmente
        Dim saldo As Decimal = CDec(DirectCast(Me.GV_MovCuenta_CXP.Rows(0).FindControl("lblSaldo"), Label).Text)
        Dim montoRetirado As Decimal = CDec(DirectCast(Me.GV_DocumentosProvisionados.Rows(indice).FindControl("lblTotalAPagar"), Label).Text)

        Dim lista As New List(Of Entidades.be_Requerimiento_x_pagar)
        lista = Session("reqProgramar")
        For i As Integer = 0 To lista.Count - 1
            lista(i).mcp_saldo = saldo + CDec(montoRetirado)
        Next
        Session("reqProgramar") = lista
        Me.GV_MovCuenta_CXP.DataSource = lista
        Me.GV_MovCuenta_CXP.DataBind()
        'remuevo el documento provisionado
        Dim listaprovisionesRelacionadas As New List(Of Entidades.Documento)
        listaprovisionesRelacionadas = Session("listaProvisionesRelacionadas")
        listaprovisionesRelacionadas.RemoveAt(indice)
        Me.GV_DocumentosProvisionados.DataSource = listaprovisionesRelacionadas
        Me.GV_DocumentosProvisionados.DataBind()
    End Sub

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Eliminar = 3
        Seleccion = 4
    End Enum

    Public ReadOnly Property SIdUsuario() As Integer
        Get
            Return CType(Session.Item("IdUsuario"), Integer)
        End Get
    End Property

    Private Function getListaRequerimiento() As List(Of Entidades.be_Requerimiento_x_pagar)
        Return CType(Session.Item(CStr(ViewState("listaRequerimiento"))), List(Of Entidades.be_Requerimiento_x_pagar))
    End Function

    Private Sub setListaRequerimiento(ByVal lista As List(Of Entidades.be_Requerimiento_x_pagar))
        Session.Remove(CStr(ViewState("listaRequerimiento")))
        Session.Add(CStr(ViewState("listaRequerimiento")), lista)
    End Sub

    Private Function getListaDocProvisionados() As List(Of Entidades.Documento)
        Return CType(Session.Item("ListaDocProvisionados"), List(Of Entidades.Documento))
    End Function

    Protected Sub lbSeleccionar_click(ByVal sender As Object, ByVal e As EventArgs)
        Dim lb As LinkButton = DirectCast(sender, LinkButton)
        Dim i As Integer = CInt(lb.CommandArgument)
        Dim id As Integer = DirectCast(gvTipoAnticipos.Rows(i).FindControl("hddidConcepto"), HiddenField).Value
        Dim nombre As String = DirectCast(gvTipoAnticipos.Rows(i).FindControl("lblNombreConcepto"), Label).Text

        Dim objeto As Entidades.Concepto = New Entidades.Concepto
        Dim lista As List(Of Entidades.Concepto) = New List(Of Entidades.Concepto)
        objeto.Id = id
        objeto.Nombre = nombre
        lista.Add(objeto)
        Me.gvConceptoReferencia.DataSource = lista
        Me.gvConceptoReferencia.DataBind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            Select Case Me.RadioButtonList1.SelectedValue
                Case 0
                    Me.trFiltroNroDocumento.Visible = False
                    Me.trFiltroProveedor.Visible = False
                    Me.trFiltroFecha.Visible = True
                Case 1
                    Me.trFiltroNroDocumento.Visible = True
                    Me.trFiltroProveedor.Visible = False
                    Me.trFiltroFecha.Visible = False
                Case 2
                    Me.trFiltroNroDocumento.Visible = False
                    Me.trFiltroProveedor.Visible = True
                    Me.trFiltroFecha.Visible = False
            End Select
            inicializarFrm()
            RecuperandoSession()
            LlenarCboTipoAgente(ddlTipoAgente, True)
            LlenarComboBancoDEtraccion(cboBanco)
            'listarConceptosAdelanto()
            Dim obj As New Combo
            obj.LlenarCboMoneda(Me.cboMoneda, True)
            Dim lista As New List(Of Entidades.be_Requerimiento_x_pagar)
            lista = Session("reqProgramar")
            Me.GV_MovCuenta_CXP.DataSource = lista
            Me.GV_MovCuenta_CXP.DataBind()
            seteaVariableSumaAcumulado()
        End If
    End Sub

    Private Sub inicializarFrm()
        Try
            'Ingresa fechaActual 
            Dim fechaActual As DateTime = Date.Today
            Dim fechaInicial As DateTime = New DateTime(fechaActual.Year, fechaActual.Month, 1)
            Dim fechaFinal As Date = New Date(fechaActual.Year, IIf(fechaActual.Month + 1 = 13, 12, fechaActual.Month + 1), 1).AddDays(-1)
            Me.txtDesde.Text = fechaInicial
            Me.txtHasta.Text = fechaFinal

            Dim objCbo As New Combo
            GV_DocumentosProvisionados.DataSource = Nothing
            GV_DocumentosProvisionados.DataBind()
            Session("newlista") = New List(Of Entidades.Documento)
            objCbo.LLenarCboEstadoDocumento(Me.ddlEstadoDocEmitirCheque)
            objCbo.LLenarCboEstadoEntrega(Me.ddlEstadoEntregaEmitirCheque)
            objCbo.LLenarCboEstadoCancelacion(Me.ddlEstadoCancelacionEmitirCheque, False)
            Me.txtFecha_ProgPago.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.dgvCuentaBancaria.DataSource = Nothing
            Me.dgvCuentaBancaria.DataBind()
            hddSaldoRQ.Value = "0"
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub RecuperandoSession()
        hddIdDocumento.Value = "0"
        hddIdMovCuenta_CXP.Value = "0"
        hddIdPersona.Value = "0"

        hddIdDocumento.Value = CStr(Session("IdDocumento"))
        hddIdMovCuenta_CXP.Value = CStr(Session("IdMovCuentaCXP"))
        IdTipoDocumento = CInt(Session("IdTipoDocumento"))

        'mostrarCapaProgramacionPago(CInt(hddIdDocumento.Value), CInt(hddIdMovCuenta_CXP.Value), IdTipoDocumento)
        mostrarCapaProgramacionPago(IdTipoDocumento)
    End Sub

    Private Sub mostrarCapaProgramacionPago(ByVal IdTipoDocumento2 As Integer)
        Try

            'Me.hddIdMovCuenta_CXP.Value = CStr(IdMovCuentaCXP2)

            Dim objCbo As New Combo
            With objCbo
                .LlenarCboBanco(Me.cboBanco_Prog, 1, True)
                .LlenarCboCuentaBancaria(Me.cboCuentaBancaria_Prog, CInt(Me.cboBanco_Prog.SelectedValue), 1, True)
                .LlenarCboMedioPagoxIdTipoDocumento(Me.cboMedioPago_Prog, IdTipoDocumento2, True)
                .LlenarCboMoneda(Me.cboTipoMoneda, True)
            End With

            verFrm(FrmModo.Inicio)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub seteaVariableSumaAcumulado()
        Dim monto As Integer = 0
        For Each row As GridViewRow In Me.GV_MovCuenta_CXP.Rows
            monto = monto + CDec(DirectCast(row.FindControl("lblSaldo"), Label).Text)
        Next
        DirectCast(Me.GV_MovCuenta_CXP.FooterRow.FindControl("lblMontoTotalAgregados"), Label).Text = monto
        Me.txtMontoProg.Text = monto
    End Sub

    Public Sub LlenarCboTipoAgente(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)
        Dim lista As New List(Of Entidades.TipoAgente)
        lista = (New Negocio.TipoAgente).SelectCbo
        Session("tipoAgente") = lista

        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdAgente"
        cbo.DataBind()

        If addElement Then
            cbo.Items.Insert(0, New ListItem("------", "0"))
        End If

    End Sub

    Private Sub LlenarComboBancoDEtraccion(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)
        Dim lista As New List(Of Entidades.Banco)
        lista = (New Negocio.Banco).ln_selectCuentaDetraccion
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()

        If addElement Then
            cbo.Items.Insert(0, New ListItem("------", "0"))
        End If
    End Sub

    Private Sub verFrm(ByVal modo As Integer)
        Dim IdDocumento As Integer = 0
        Dim index As Integer = 0
        Me.Panel_ProgramacionPago.Enabled = False
        Me.Panel_ProgramacionPago_Grilla.Enabled = False
        Me.btnNuevo.Visible = False
        Me.btnEditar.Visible = False
        Me.btnEliminar.Visible = False
        Me.btnGuardar.Visible = False
        Me.btnCancelar.Visible = False
        Dim MontoTotal As Decimal = 0
        Dim MontoAcumulado As Decimal = 0
        Dim MontoPagado As Decimal = 0
        Dim monedaResult As String = ""
        Dim MontoTotalResultado As Decimal = 0
        Dim hddIdProveedor As Integer = 0


        Select Case modo

            Case FrmModo.Inicio
                '=============================
                Dim listaReq As New List(Of Entidades.be_Requerimiento_x_pagar)
                listaReq = Session("reqProgramar")
                ''qwe
                'For i As Integer = 0 To listaReq.Count - 1
                '    listaReq(i).mcp_saldo += CDec(Me.txtMontoProg.Text)
                'Next
                'Me.GV_MovCuenta_CXP.DataSource = listaReq
                'Me.GV_MovCuenta_CXP.DataBind()

                txtMontoProg.Enabled = True
                Me.btnAgregarDocProvisionados.Visible = False
                limpiarControles_Prog()
                Me.lblMoneda_MontoProg.Text = ""
                Me.Panel_ProgramacionPago_Grilla.Enabled = True
                Me.btnNuevo.Visible = True


                'Me.GV_ProgramacionPago.DataSource = (New Negocio.ProgramacionPago_CXP).SelectxIdMovCuentaCXP(CInt(Me.hddIdMovCuenta_CXP.Value))
                'Me.GV_ProgramacionPago.DataBind()

                'Dim listarovisionesRelacionadas As New List(Of Entidades.Documento)
                'listarovisionesRelacionadas = Session("listaProvisionesRelacionadas")
                'Me.GV_DocumentosProvisionados.DataSource = listarovisionesRelacionadas
                'Me.GV_DocumentosProvisionados.DataBind()

                If listaReq.Count = 1 Then
                    Me.dgvCuentaBancaria.DataSource = objNegCta.SelectcuentaBancaria(listaReq(0).idProveedor, 0, 1, 0)
                    Me.dgvCuentaBancaria.DataBind()
                End If

                Me.ListaDocProvisionados = New List(Of Entidades.Documento)
                hddSaldoRQ.Value = "0"

            Case FrmModo.Nuevo
                Session("newlista") = New List(Of Entidades.Documento)
                limpiarControles_Prog()
                Me.Panel_ProgramacionPago.Enabled = True

                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.btnAgregarDocProvisionados.Visible = True

                Dim objCbo As New Combo
                With objCbo
                    .LlenarCboMoneda(Me.cboTipoMoneda, True)
                End With

                Try

                    Dim IdDocumentox As Integer = CInt(hddIdDocumento.Value)

                    ''SelectMovCuentaporPagarxIddocumento
                    Dim objDocumento As Entidades.MovCuentaPorPagar = New Negocio.MovCuentaPorPagar().SelectMovCuentaporPagarxIddocumento(IdDocumentox)

                    Me.hddSaldoRQ.Value = CStr(objDocumento.Saldo)

                    Me.lblMoneda_MontoProg.Text = CStr(objDocumento.MonedaSimbolo)
                    Me.txtMontoProg.Text = CStr(objDocumento.Saldo)
                    MontoTotal = CDec(objDocumento.Saldo)
                    MontoTotalResultado = CDec(objDocumento.Monto)
                    hddIdProveedor = CInt(objDocumento.IdProveedor)
                    hddIdPersona.Value = CStr(objDocumento.IdProveedor)

                    For Each row As GridViewRow In Me.GV_ProgramacionPago.Rows

                        MontoPagado = CDec(CType(row.FindControl("lblMontoProg"), Label).Text)
                        MontoAcumulado += MontoPagado
                        monedaResult = CStr(CType(row.FindControl("lblMoneda_MontoProg"), Label).Text)
                        MontoPagado = 0
                    Next

                    'index = CInt(lblindex.Text)
                    IdDocumento = CInt(hddIdDocumento.Value)
                    'lblindex.Visible = False


                    Dim listaRelacionDocExterno As List(Of Entidades.Documento) = New Negocio.Documento().SelectDocExtxIdRQinProgPago(IdDocumento)

                    Dim CondicionPago As String = ""
                    Dim CondicionComercial As String = ""

                    Dim listaCP As List(Of Entidades.Documento) = obtenerDocumentoRef_Cab(IdDocumento)
                    For k As Integer = 0 To listaCP.Count - 1
                        CondicionPago += listaCP(k).NomCondicionPago
                    Next


                    Dim listaCC As List(Of Entidades.CondicionComercial) = (New Negocio.Documento_CondicionC).SelectxIdDocumento(IdDocumento)
                    For k As Integer = 0 To listaCC.Count - 1
                        CondicionComercial += listaCC(k).Descripcion
                    Next

                    'Me.GV_DocumentosProvisionados.DataSource = Nothing
                    'Me.GV_DocumentosProvisionados.DataBind()
                    Me.txtTotalSaldo.Text = 0
                Catch ex As Exception

                End Try

            Case FrmModo.Editar
                Dim NroDoc As String = ""
                txtMontoProg.Enabled = False
                Me.btnAgregarDocProvisionados.Visible = True

                Dim listarovisionesRelacionadas As New List(Of Entidades.Documento)
                listarovisionesRelacionadas = Session("listaProvisionesRelacionadas")
                Me.GV_DocumentosProvisionados.DataSource = listarovisionesRelacionadas
                Me.GV_DocumentosProvisionados.DataBind()

                For Each row As GridViewRow In Me.GV_ProgramacionPago.Rows

                    NroDoc = CStr(CType(row.FindControl("lbldocreferencia"), Label).Text)
                    Exit For
                Next

                If (NroDoc.Length <= 0) Then


                    Me.Panel_ProgramacionPago.Enabled = True
                    Me.btnGuardar.Visible = True
                    Me.btnCancelar.Visible = True

                    Try
                        Dim IdDocumentox As Integer = CInt(hddIdDocumento.Value)

                        ''SelectMovCuentaporPagarxIddocumento
                        Dim objDocumento As Entidades.MovCuentaPorPagar = New Negocio.MovCuentaPorPagar().SelectMovCuentaporPagarxIddocumento(IdDocumentox)
                        Me.hddSaldoRQ.Value = CStr(objDocumento.Saldo)

                        Me.lblMoneda_MontoProg.Text = CStr(objDocumento.MonedaSimbolo)
                        Me.txtMontoProg.Text = CStr(objDocumento.Saldo)
                        MontoTotal = CDec(objDocumento.Saldo)
                        MontoTotalResultado = CDec(objDocumento.Monto)
                        hddIdProveedor = CInt(objDocumento.IdProveedor)
                        hddIdPersona.Value = CStr(objDocumento.IdProveedor)

                        For Each row As GridViewRow In Me.GV_ProgramacionPago.Rows

                            MontoPagado = CDec(CType(row.FindControl("lblMontoProg"), Label).Text)
                            MontoAcumulado += MontoPagado
                            monedaResult = CStr(CType(row.FindControl("lblMoneda_MontoProg"), Label).Text)
                            MontoPagado = 0
                        Next

                        IdDocumento = CInt(hddIdDocumento.Value)

                        Dim listaRelacionDocExterno As List(Of Entidades.Documento) = New Negocio.Documento().SelectDocExtxIdRQinProgPago(IdDocumento)

                        Dim CondicionPago As String = ""
                        Dim CondicionComercial As String = ""

                        Dim listaCP As List(Of Entidades.Documento) = obtenerDocumentoRef_Cab(IdDocumento)
                        For k As Integer = 0 To listaCP.Count - 1
                            CondicionPago += listaCP(k).NomCondicionPago
                        Next


                        Dim listaCC As List(Of Entidades.CondicionComercial) = (New Negocio.Documento_CondicionC).SelectxIdDocumento(IdDocumento)
                        For k As Integer = 0 To listaCC.Count - 1
                            CondicionComercial += listaCC(k).Descripcion
                        Next

                        Me.dgvCuentaBancaria.DataSource = objNegCta.SelectcuentaBancaria(hddIdProveedor, 0, 1, 0)
                        Me.dgvCuentaBancaria.DataBind()

                    Catch ex As Exception

                    End Try

                Else
                    Me.btnNuevo.Visible = True
                    objScript.mostrarMsjAlerta(Me, "El Documento tiene un cheque referenciado. No procede la edición.")

                End If


            Case FrmModo.Seleccion

                Me.btnNuevo.Visible = True
                Me.btnEditar.Visible = True
                Me.btnEliminar.Visible = True
                Me.btnAgregarDocProvisionados.Visible = True

                Me.Panel_ProgramacionPago_Grilla.Enabled = True

                Dim listarovisionesRelacionadas As New List(Of Entidades.Documento)
                listarovisionesRelacionadas = Session("listaProvisionesRelacionadas")
                Me.GV_DocumentosProvisionados.DataSource = listarovisionesRelacionadas
                Me.GV_DocumentosProvisionados.DataBind()

                Try
                    Dim IdDocumentox As Integer = CInt(hddIdDocumento.Value)

                    ''SelectMovCuentaporPagarxIddocumento
                    Dim objDocumento As Entidades.MovCuentaPorPagar = New Negocio.MovCuentaPorPagar().SelectMovCuentaporPagarxIddocumento(IdDocumentox)

                    Me.hddSaldoRQ.Value = CStr(objDocumento.Saldo)
                    'Me.lblMoneda_MontoProg.Text = CStr(objDocumento.MonedaSimbolo)
                    'Me.txtMontoProg.Text = CStr(objDocumento.Saldo)
                    MontoTotal = CDec(objDocumento.Saldo)
                    MontoTotalResultado = CDec(objDocumento.Monto)
                    hddIdProveedor = CInt(objDocumento.IdProveedor)
                    hddIdPersona.Value = CStr(objDocumento.IdProveedor)

                    'For Each row As GridViewRow In Me.GV_ProgramacionPago.Rows

                    '    MontoPagado = CDec(CType(row.FindControl("lblMontoProg"), Label).Text)
                    '    MontoAcumulado += MontoPagado
                    '    monedaResult = CStr(CType(row.FindControl("lblMoneda_MontoProg"), Label).Text)
                    '    MontoPagado = 0
                    'Next

                    IdDocumento = CInt(hddIdDocumento.Value)
                    Dim listaRelacionDocExterno As List(Of Entidades.Documento) = New Negocio.Documento().SelectDocExtxIdRQinProgPago(IdDocumento)


                    Dim CondicionPago As String = ""
                    Dim CondicionComercial As String = ""

                    Dim listaCP As List(Of Entidades.Documento) = obtenerDocumentoRef_Cab(IdDocumento)
                    For k As Integer = 0 To listaCP.Count - 1
                        CondicionPago += listaCP(k).NomCondicionPago
                    Next


                    Dim listaCC As List(Of Entidades.CondicionComercial) = (New Negocio.Documento_CondicionC).SelectxIdDocumento(IdDocumento)
                    For k As Integer = 0 To listaCC.Count - 1
                        CondicionComercial += listaCC(k).Descripcion
                    Next

                    Me.dgvCuentaBancaria.DataSource = objNegCta.SelectcuentaBancaria(hddIdProveedor, 0, 1, 0)
                    Me.dgvCuentaBancaria.DataBind()
                Catch ex As Exception
                End Try
        End Select
        Me.hddFrmModo.Value = CStr(modo)
    End Sub

    Private Sub limpiarControles_Prog()
        'desmarca checks
        Me.chkEsAdelanto.Checked = False
        Me.chkEsCtaPorRendir.Checked = False

        Me.txtMontoProg.Text = "0"
        Me.txtObservaciones_Prog.Text = ""
        Me.txtFecha_ProgPago.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.cboBanco_Prog.SelectedIndex = 0
        Me.cboCuentaBancaria_Prog.SelectedIndex = -1
        Me.cboMedioPago_Prog.SelectedIndex = 0
        Me.cboTipoMoneda.SelectedIndex = 0

        Me.lblResultadoDolares.Visible = False
        Me.txtResultadoTipoCambio.Visible = False
        Me.btncalcularTC.Visible = False
        Me.lblTipoCambio.Visible = True
        Me.txtTipoCambio.Visible = True
        Me.txtResultadoTipoCambio.Text = "0"

    End Sub

    Private Function obtenerDocumentoRef_Cab(ByVal IdDocumentoRef As Integer) As List(Of Entidades.Documento)

        Dim lista As New List(Of Entidades.Documento)

        Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumentoProgramacionPago(IdDocumentoRef)

        Try : objDocumento.NomCondicionPago = (New Negocio.CondicionPago).SelectxId(objDocumento.IdCondicionPago)(0).Nombre : Catch ex As Exception : End Try

        lista.Add(objDocumento)

        Return lista

    End Function

    'Private Sub rdbBuscarDocRef_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
    '    Select Case rdbBuscarDocRef.SelectedValue
    '        Case 0
    '            Me.trFiltroFecha.Visible = False
    '            Me.trFiltroNroDocumento.Visible = True
    '            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaDocProvisionados');", True)
    '        Case 1
    '            Me.trFiltroFecha.Visible = True
    '            Me.trFiltroNroDocumento.Visible = False
    '            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaDocProvisionados');", True)
    '    End Select
    'End Sub

    Protected Sub btnBuscarProvision_Click(sender As Object, e As System.EventArgs)
        Dim serie As String = Me.txtnroSerieProvision.Text
        Dim codigo As String = Me.txtNumeroProvision.Text
        Dim fechaInicio As Date = CDate(txtDesde.Text)
        Dim fechaFin As Date = CDate(txtHasta.Text)
        Dim flag As Integer = Me.RadioButtonList1.SelectedItem.Value

        'Me.RadioButtonList1.SelectedItem.Value
        Dim nroRuc As String = Me.txtFiltroRuc.Text.Trim()
        Try
            Dim ListaDocProvisionados As List(Of Entidades.Documento) = (New Negocio.blProvisiones).listarDocumentosProvisionados(serie, codigo, fechaInicio, fechaFin, flag, nroRuc)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

        Session("ListaDocProvisionados") = ListaDocProvisionados
        Me.gvProvisiones.DataSource = ListaDocProvisionados
        Me.gvProvisiones.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaDocProvisionados');", True)
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As System.EventArgs) Handles btnNuevo.Click
        valOnClick_btnNuevo()
    End Sub

    Private Sub valOnClick_btnNuevo()
        Try
            Me.lblResultadoDolares.Visible = False
            Me.txtResultadoTipoCambio.Visible = False
            Me.btncalcularTC.Visible = False
            Me.lblTipoCambio.Visible = True
            Me.txtTipoCambio.Visible = True
            Me.txtTipoCambio.Text = (New Negocio.bl_pagoProveedor).buscarTipoCambioCompra(Me.txtFecha_ProgPago.Text)
            Me.txtResultadoTipoCambio.Text = "0"

            verFrm(FrmModo.Nuevo)



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    'Private Sub ddlTipoAgente_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlTipoAgente.SelectedIndexChanged
    '    If ddlTipoAgente.SelectedValue <> 0 Then
    '        btnCrearCuentaDetraccion.Visible = False
    '        Dim flagCuentaDetraccion As Integer = 0
    '        Dim lista As New List(Of Entidades.be_Requerimiento_x_pagar)
    '        Dim listaTipoAgente As New List(Of Entidades.TipoAgente)
    '        listaTipoAgente = Session("tipoAgente")
    '        listaTipoAgente = (From item In listaTipoAgente
    '                         Where CInt(item.IdAgente) = ddlTipoAgente.SelectedValue
    '                         Select item).ToList()
    '        lista = Session("reqProgramar")
    '        Dim idProveedor As Integer = lista(0).idProveedor
    '        'esto sucede siempre y cuando el combo elegido sea de tipo detraccion
    '        If listaTipoAgente(0).SujetoADetraccion Then
    '            flagCuentaDetraccion = (New Negocio.CuentaBancaria).ln_existeCuentaDetraccion(idProveedor)
    '            'actualizo la grilla de cuentas bancarias
    '            Me.dgvCuentaBancaria.DataSource = objNegCta.SelectcuentaBancaria(idProveedor, 0, 1, 0)
    '            Me.dgvCuentaBancaria.DataBind()

    '            If flagCuentaDetraccion = 0 Then
    '                btnCrearCuentaDetraccion.Visible = True
    '            Else
    '                btnCrearCuentaDetraccion.Visible = False
    '            End If
    '        End If
    '    End If
    'End Sub

    Private Sub cboBanco_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboBanco.SelectedIndexChanged
        valOnChange_cboBanco()
    End Sub


    Private Sub GV_ProgramacionPago_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GV_ProgramacionPago.RowCommand
        If e.CommandName = "crear_cheque" Then
            Dim indice As Integer = CInt(e.CommandArgument)
            hddIndiceProgramacion.Value = indice ' setea el hidden con el id de la fila seleccionada
            Dim datakey As DataKey = GV_ProgramacionPago.DataKeys(indice)
            Dim objCbo As New Combo
            With objCbo
                .LlenarCboBanco(Me.ddlBancoEmitirCheque, 1, True)
                Me.ddlBancoEmitirCheque.SelectedValue = datakey(0)
                .LlenarCboCuentaBancaria(Me.ddlCuentaBancariaEmitirCheque, CInt(Me.ddlBancoEmitirCheque.SelectedValue), 1, True)
                Me.ddlCuentaBancariaEmitirCheque.SelectedValue = datakey(1)
                .LlenarCboSerieChequexIdCuentaBancaria(ddlSerieNumeracionEmitirCheque, CInt(ddlCuentaBancariaEmitirCheque.SelectedValue), True)
                ddlSerieNumeracionEmitirCheque.SelectedIndex = 1
                'Carga Correlativo
                Dim x As Integer
                x = CInt(ddlSerieNumeracionEmitirCheque.SelectedValue)
                If x <> 0 Then
                    ddlNroChequeEmitirCheque.Text = (New Negocio.DocumentoCheque).GenerarDocCodigoChequexIdSerieCheque(x)
                Else
                    ddlNroChequeEmitirCheque.Text = ""
                End If
                txtMontoEmitirCheque.Text = DirectCast(GV_ProgramacionPago.Rows(indice).FindControl("lblMontoProg"), Label).Text
                txtFEchaCobroEmitirCheque.Text = GV_ProgramacionPago.Rows(indice).Cells(1).Text
                txtFechaEmisionEmitirCheque.Text = GV_ProgramacionPago.Rows(indice).Cells(1).Text
            End With

            Dim datakeyProgramacion As DataKey = GV_ProgramacionPago.DataKeys(indice)
            Dim idDocumentoProgram As Integer = datakeyProgramacion(2)
            Dim listaCheques As New List(Of Entidades.DocumentoCheque)
            listaCheques = (New Negocio.DocumentoCheque).LN_EmitirCheque(idDocumentoProgram)
            Me.gvEmisionCheques.DataSource = listaCheques
            Me.gvEmisionCheques.DataBind()
            If gvEmisionCheques.Rows.Count <> 0 Then
                Me.btnGuardarEmitirCheque.Text = "Modificar"
                Me.btnGuardarEmitirCheque.CommandName = "modificar_cheque"
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onCapa('capaEmitirCheque');", True)
        End If
    End Sub

    Private Sub GV_ProgramacionPago_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_ProgramacionPago.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ImageButton As ImageButton = DirectCast(e.Row.FindControl("imgbtnCrearCheque"), ImageButton)
            Dim DocumentoCheque As String = e.Row.Cells(3).Text
            If DocumentoCheque = "CHEQUE PAGADOR" Then
                ImageButton.Enabled = True
                ImageButton.Visible = True
            End If
        End If
    End Sub

    Private Sub GV_ProgramacionPago_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles GV_ProgramacionPago.SelectedIndexChanged
        cargarDatos_ProgramacionPago_CXP(Me.GV_ProgramacionPago.SelectedIndex)
    End Sub

    Private Sub cargarDatos_ProgramacionPago_CXP(ByVal index As Integer)
        Try
            Dim objCbo As New Combo
            Dim IdProgramacionPago_CXP As Integer = CInt(CType(Me.GV_ProgramacionPago.Rows(index).FindControl("hddIdProgramacionPago_CXP"), HiddenField).Value)
            limpiarControles_Prog()
            Dim objProgramacionPago As Entidades.ProgramacionPago_CXP = (New Negocio.ProgramacionPago_CXP).SelectxIdProgramacionPago(IdProgramacionPago_CXP)

            If (objProgramacionPago IsNot Nothing) Then
                With objProgramacionPago
                    Me.hddIdProgramacionPago_CXP.Value = CStr(.IdProgramacionPago)

                    If (.FechaPagoProg <> Nothing) Then
                        Me.txtFecha_ProgPago.Text = Format(.FechaPagoProg, "dd/MM/yyyy")
                    End If
                    Me.lblMoneda_MontoProg.Text = .Moneda
                    Me.txtMontoProg.Text = CStr(Math.Round(.MontoPagoProg, 3))

                    If (Me.cboMedioPago_Prog.Items.FindByValue(CStr(.IdMedioPago)) IsNot Nothing) Then
                        Me.cboMedioPago_Prog.SelectedValue = CStr(.IdMedioPago)
                    End If

                    If (Me.cboBanco_Prog.Items.FindByValue(CStr(.IdBanco)) IsNot Nothing) Then
                        Me.cboBanco_Prog.SelectedValue = CStr(.IdBanco)
                        objCbo.LlenarCboCuentaBancaria(Me.cboCuentaBancaria_Prog, CInt(.IdBanco), CInt(1), True)
                    End If

                    If (Me.cboCuentaBancaria_Prog.Items.FindByValue(CStr(.IdCuentaBancaria)) IsNot Nothing) Then
                        Me.cboCuentaBancaria_Prog.SelectedValue = CStr(.IdCuentaBancaria)
                    End If

                    If (Me.cboTipoMoneda.Items.FindByValue(CStr(.IdMoneda)) IsNot Nothing) Then
                        Me.cboTipoMoneda.SelectedValue = CStr(.IdMoneda)
                    End If
                    Me.txtTipoCambio.Text = CStr(.TipoCambio)

                    If (.Observacion <> Nothing) Then
                        Me.txtObservaciones_Prog.Text = .Observacion
                    End If
                End With
                verFrm(FrmModo.Seleccion)
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnEditar_Click(sender As Object, e As System.EventArgs) Handles btnEditar.Click
        valOnClick_btnEditar()
    End Sub

    Private Sub valOnClick_btnEditar()
        Try

            verFrm(FrmModo.Editar)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As System.EventArgs) Handles btnEliminar.Click
        eliminarProgramacionPago_CXP()
    End Sub

    Private Sub eliminarProgramacionPago_CXP()
        'Try

        '    Dim IdProgramacionPago_CXP As Integer = CInt(Me.hddIdProgramacionPago_CXP.Value)
        '    Dim idDocumentoRequerimiento As Integer = hddIdDocumento.Value
        '    Dim objProgramacionPagoCXP As New Negocio.ProgramacionPago_CXP
        '    Dim saldo As Decimal = CDec(DirectCast(Me.GV_MovCuenta_CXP.Rows(0).FindControl("lblSaldo"), Label).Text)

        '    Try
        '        objProgramacionPagoCXP.DeletexIdProgramacionPagoCXP(IdProgramacionPago_CXP, idDocumentoRequerimiento, CDec(Me.txtMontoProg.Text))
        '    Catch ex As Exception
        '        Throw ex
        '    End Try


        '    Dim lista As New List(Of Entidades.be_Requerimiento_x_pagar)
        '    lista = Session("reqProgramar")
        '    For i As Integer = 0 To lista.Count - 1
        '        lista(i).mcp_saldo = saldo + CDec(Me.txtMontoProg.Text)
        '    Next
        '    Session("reqProgramar") = lista
        '    Me.GV_MovCuenta_CXP.DataSource = lista
        '    Me.GV_MovCuenta_CXP.DataBind()

        '    Dim MontoRecuperar As Decimal = 0
        '    'If (GV_DocumentoRelacionado.Rows.Count > 0) Then
        '    '    For Each row As GridViewRow In Me.GV_DocumentoRelacionado.Rows
        '    '        MontoRecuperar = CInt(CType(row.FindControl("lblMontoRefExt"), Label).Text)
        '    '        MontoRecuperar = MontoRecuperar + CDec(hddSaldoRQ.Value)
        '    '        Dim query As String = "update MovCuentaporPagar set mcp_Saldo = @mcp_Saldo where IdDocumento = @Iddocumento"
        '    '        Using cmd As New SqlCommand(query, con)
        '    '            cmd.CommandTimeout = 0
        '    '            cmd.Parameters.AddWithValue("@mcp_Saldo", MontoRecuperar)
        '    '            cmd.Parameters.AddWithValue("@Iddocumento", CInt(hddIdDocumento.Value))
        '    '            con.Open()
        '    '            cmd.ExecuteNonQuery()
        '    '            con.Close()
        '    '        End Using
        '    '        Exit For
        '    '    Next
        '    'End If

        '    objScript.mostrarMsjAlerta(Me, "El registro se eliminó correctamente.")
        '    verFrm(FrmModo.Inicio)


        'Catch ex As Exception
        '    objScript.mostrarMsjAlerta(Me, ex.Message)
        'End Try
    End Sub

    Private Sub cboBanco_Prog_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboBanco_Prog.SelectedIndexChanged
        valOnChange_cboBanco()
    End Sub

    Private Sub valOnChange_cboBanco()
        Try
            Dim objCbo As New Combo
            With objCbo

                .LlenarCboCuentaBancaria(Me.cboCuentaBancaria_Prog, CInt(Me.cboBanco_Prog.SelectedValue), 1, True)
            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboCuentaBancaria_Prog_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboCuentaBancaria_Prog.SelectedIndexChanged
        Try

            If (cboCuentaBancaria_Prog.SelectedItem.Text.StartsWith("S")) Then
                If (Me.cboTipoMoneda.Items.FindByValue(CStr("1")) IsNot Nothing) Then
                    Me.cboTipoMoneda.SelectedValue = CStr(1)
                End If
            ElseIf (cboCuentaBancaria_Prog.SelectedItem.Text.StartsWith("US")) Then
                If (Me.cboTipoMoneda.Items.FindByValue(CStr("2")) IsNot Nothing) Then
                    Me.cboTipoMoneda.SelectedValue = CStr(2)
                End If
            Else
                If (Me.cboTipoMoneda.Items.FindByValue(CStr("3")) IsNot Nothing) Then
                    Me.cboTipoMoneda.SelectedValue = CStr(3)
                End If
            End If

            lblMoneda_MontoProg.Text = CStr(cboTipoMoneda.SelectedItem.Text)
            If (CStr(cboTipoMoneda.SelectedItem.Text) = "S/. ") Then
                lblResultadoDolares.Visible = True
                txtResultadoTipoCambio.Visible = True
                btncalcularTC.Visible = True
                lblTipoCambio.Visible = True
                txtTipoCambio.Visible = True
            Else
                lblResultadoDolares.Visible = False
                txtResultadoTipoCambio.Visible = False

                btncalcularTC.Visible = False
                lblTipoCambio.Visible = True
                txtTipoCambio.Visible = True
                txtResultadoTipoCambio.Text = "0"
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As System.EventArgs) Handles btnCancelar.Click
        valOnClick_btnCancelar()
    End Sub

    Private Sub valOnClick_btnCancelar()
        Try
            Dim listarovisionesRelacionadas As New List(Of Entidades.Documento)
            listarovisionesRelacionadas = Session("listaProvisionesRelacionadas")
            Me.GV_DocumentosProvisionados.DataSource = listarovisionesRelacionadas
            Me.GV_DocumentosProvisionados.DataBind()

            verFrm(FrmModo.Inicio)

            Me.txtTotalSaldo.Text = 0
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub SeleccionarGuias(ByVal sender As Object, ByVal e As EventArgs)
        Dim listaRequerimientosss As New List(Of Entidades.be_Requerimiento_x_pagar)
        listaRequerimientosss = Session("reqProgramar")

        Dim lista As New List(Of Entidades.Documento)
        Dim newlista As New List(Of Entidades.Documento)
        Dim objetoDocumento As Entidades.Documento
        Dim indice As Integer = 0
        lista = Session("ListaDocProvisionados")
        Dim idProvision As Integer = 0
        Dim datakey As DataKey
        Dim saldoDeudaRequerimiento As Decimal = CDec(DirectCast(Me.GV_MovCuenta_CXP.Rows(0).FindControl("lblMonto"), Label).Text)

        If Session("newlista") IsNot Nothing Then
            newlista = Session("newlista")
        End If

        Dim checkbox As CheckBox
        For Each row As GridViewRow In Me.gvProvisiones.Rows
            checkbox = New CheckBox()
            checkbox = DirectCast(row.FindControl("chkSeleccionar"), CheckBox)
            datakey = gvProvisiones.DataKeys(row.RowIndex)

            If checkbox.Checked Then
                idProvision = datakey(0)
                For Each item As Entidades.Documento In lista
                    If item.IdDocumento() = idProvision Then
                        If item.NomMoneda.Trim() = listaRequerimientosss(0).nom_simbolo.Trim() Then
                            If CDec(item.TotalAPagar) <= saldoDeudaRequerimiento And (CDec(Me.txtTotalSaldo.Text) + CDec(item.TotalAPagar)) <= saldoDeudaRequerimiento Then
                                'Descuenta el saldo de la tabla requerimiento
                                If listaRequerimientosss.Count > 0 Then
                                    listaRequerimientosss(0).mcp_saldo = listaRequerimientosss(0).mcp_saldo - item.TotalAPagar
                                    Me.GV_MovCuenta_CXP.DataSource = listaRequerimientosss
                                    Me.GV_MovCuenta_CXP.DataBind()
                                    Session("reqProgramar") = listaRequerimientosss
                                End If

                                For Each itemDestino As Entidades.Documento In newlista
                                    If itemDestino.IdDocumento = item.IdDocumento Then
                                        objScript.mostrarMsjAlerta(Me, "El item ya existe.")
                                        Exit Sub
                                    End If
                                Next
                                lista.Remove(item)
                                Me.gvProvisiones.DataSource = lista
                                Me.gvProvisiones.DataBind()

                                newlista.Add(item)
                                Session("newlista") = newlista
                                Session("listaProvisionesRelacionadas") = newlista
                                Me.GV_DocumentosProvisionados.DataSource = newlista
                                Me.GV_DocumentosProvisionados.DataBind()
                                Exit For
                            Else
                                objScript.mostrarMsjAlerta(Me, "El monto total de la provisión excede al monto total del requerimiento.")
                                Exit Sub
                            End If
                        Else
                            objScript.mostrarMsjAlerta(Me, "EL tipo de moneda no coincide con el del requerimiento.")
                            objScript.onCapa(Me, "mostrarProvisiones")
                        End If
                    End If
                Next
                Exit For
            End If
        Next
        Dim sumaTotalSaldo As Decimal = 0
        For Each rowSuma As GridViewRow In Me.GV_DocumentosProvisionados.Rows
            sumaTotalSaldo += CDec(DirectCast(rowSuma.FindControl("lblTotalAPagar"), Label).Text)
        Next
        If GV_DocumentosProvisionados.Rows.Count > 0 Then
            Me.txtTotalSaldo.Visible = True
            Me.txtTotalSaldo.ReadOnly = True
            Me.txtTotalSaldo.Text = sumaTotalSaldo
        Else
            Me.txtTotalSaldo.Visible = False
            Me.txtTotalSaldo.Text = 0
        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaDocProvisionados');", True)
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As System.EventArgs) Handles btnGuardar.Click
        If Me.ddlTipoAgente.SelectedValue = 0 Or Me.ddlTipoAgente.SelectedValue = 2 Or Me.ddlTipoAgente.SelectedValue = 3 Then
            Try
                Call registrarProgramacion()
                verFrm(FrmModo.Inicio)
            Catch ex As Exception
                objScript.mostrarMsjAlerta(Me, ex.Message)
            End Try
        Else
            If Me.chkEsAdelanto.Checked Then
                Call registrarProgramacion()
                verFrm(FrmModo.Inicio)
            Else
                Me.gv_docAsociadosDetraccion.DataSource = Session("newlista")
                Me.gv_docAsociadosDetraccion.DataBind()
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaAsociarDetracciones'); ", True)
            End If
        End If
    End Sub

    Private Sub registrarProgramacion()
        Dim saldoDeuda As Decimal = CDec(DirectCast(Me.GV_MovCuenta_CXP.Rows(0).FindControl("lblSaldo"), Label).Text)
        Dim montoProgramado As Decimal = CDec(txtMontoProg.Text)
        Dim objNegocioProgPago As New Negocio.ProgramacionPago_CXP

        Dim listaTipoAgente As New List(Of Entidades.TipoAgente)
        listaTipoAgente = Session("tipoAgente")
        listaTipoAgente = (From item In listaTipoAgente
                         Where CInt(item.IdAgente) = ddlTipoAgente.SelectedValue
                         Select item).ToList()

        Dim objProgramacionPago As Entidades.ProgramacionPago_CXP = obtenerProgramacionPago(listaTipoAgente)

        Dim esDetraccion As Boolean = False
        Dim esPercepcion As Boolean = False
        Dim esRetencion As Boolean = False
        
        'Si el monto Programado es menor o igual que el saldo de la deuda
        'If (montoProgramado <= saldoDeuda) Then
        If (saldoDeuda >= 0) Then

            Dim cadenaDocumentoReferencia As String = ""
            For Each row As GridViewRow In Me.GV_DocumentosProvisionados.Rows
                cadenaDocumentoReferencia += DirectCast(row.FindControl("hddidDocumento"), HiddenField).Value
                cadenaDocumentoReferencia += ","
            Next
            If cadenaDocumentoReferencia <> "" Then
                cadenaDocumentoReferencia = cadenaDocumentoReferencia.Remove(cadenaDocumentoReferencia.Length - 1)
            End If

            Dim idRequerimiento As Integer = Me.hddIdMovCuenta_CXP.Value
            Try
                'objNegocioProgPago.Insert(cadenaDocumentoReferencia.ToString(), hddIdDocumento.Value, objProgramacionPago, obtenerListaRelacionDocumento, _
                '                              esDetraccion, _
                '                              esRetencion, _
                '                              esPercepcion, _
                '                              hddFrmModo.Value)

            Catch ex As Exception
                Throw ex
            End Try

            'ModificarPrecios()         

        End If
    End Sub

    Private Function obtenerProgramacionPago(Optional ByVal listaTipoAgente As List(Of Entidades.TipoAgente) = Nothing) As Entidades.ProgramacionPago_CXP
        'Dim listaTipoAgente As New List(Of Entidades.TipoAgente)
        'listaTipoAgente = Session("tipoAgente")
        'listaTipoAgente = (From item In listaTipoAgente
        '                 Where CInt(item.IdAgente) = ddlTipoAgente.SelectedValue
        '                 Select item).ToList()
        Dim montoSujetoADetraccion As Decimal = 0
        Dim checkBox As CheckBox
        Dim contador As Integer = 0
        For Each row As GridViewRow In Me.gv_docAsociadosDetraccion.Rows
            checkBox = New CheckBox
            checkBox = DirectCast(row.FindControl("checkDetraccion"), CheckBox)
            If checkBox.Checked Then
                contador += 1
                montoSujetoADetraccion += CDec(DirectCast(row.FindControl("lblTotalAPagar"), Label).Text)
            End If
        Next
        Dim obj As New Entidades.ProgramacionPago_CXP
        If contador = 0 And Me.ddlTipoAgente.SelectedValue <> 0 And Me.ddlTipoAgente.SelectedValue <> 2 And Me.ddlTipoAgente.SelectedValue <> 3 Then
            If Me.chkEsAdelanto.Checked = False Then
                Dim mensaje As String = "Debe marcar como mínimo un documento."
                'objScript.mostrarMsjAlerta(Me, "Debe marcar como mínimo un documento.")
                Dim exception As Exception
                objScript.mostrarMsjAlerta(Me, mensaje)
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaAsociarDetracciones');", True)
                Throw exception
            Else
                montoSujetoADetraccion = Me.txtMontoProg.Text.Trim()
            End If
        End If
        With obj
            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .IdProgramacionPago = Nothing
                Case FrmModo.Editar
                    .IdProgramacionPago = CInt(Me.hddIdProgramacionPago_CXP.Value)
            End Select
            If listaTipoAgente.Count = 0 Then .porc_agente = 0 Else .porc_agente = listaTipoAgente(0).Tasa

            .IdMovCtaPP = CInt(Me.hddIdMovCuenta_CXP.Value)
            .FechaPagoProg = CDate(Me.txtFecha_ProgPago.Text)
            .IdBanco = CInt(Me.cboBanco_Prog.SelectedValue)
            .IdMedioPago = CInt(Me.cboMedioPago_Prog.SelectedValue)
            .IdCuentaBancaria = CInt(Me.cboCuentaBancaria_Prog.SelectedValue)
            .Observacion = CStr(Me.txtObservaciones_Prog.Text)
            .TipoCambio = CDec(Me.txtTipoCambio.Text)
            .IdTipoMoneda = CInt(Me.cboTipoMoneda.SelectedValue)
            .MontoPagoProg = CDec(Me.txtMontoProg.Text)
            .montoSujetoADetraccion = montoSujetoADetraccion
            If Me.gvConceptoReferencia.Rows.Count > 0 Then
                .idConcepto = DirectCast(Me.gvConceptoReferencia.Rows(0).FindControl("hddidConcepto"), HiddenField).Value
            End If

            Dim saldo As Decimal = CDec(DirectCast(Me.GV_MovCuenta_CXP.Rows(0).FindControl("lblSaldo"), Label).Text)

            If Me.chkEsAdelanto.Checked Then
                .saldoProgramacion = saldo - CDec(Me.txtMontoProg.Text)
            Else
                .saldoProgramacion = saldo
            End If
            Dim lista As New List(Of Entidades.be_Requerimiento_x_pagar)
            lista = Session("reqProgramar")
            For i As Integer = 0 To lista.Count - 1
                lista(i).mcp_saldo = .saldoProgramacion
            Next
            Session("reqProgramar") = lista
            Me.GV_MovCuenta_CXP.DataSource = lista
            Me.GV_MovCuenta_CXP.DataBind()
            'Dim saldo As Decimal = Deuda - .MontoPagoProg            
            'lblValoreditarDeudaSave.Text = CStr(Me.txtMontoProg.Text)

            'And lblmonedaResult.Text = "US$"
            'If (CStr(cboTipoMoneda.SelectedItem.Text) = "S/. ") Then
            '    .TipoCambio = CDec(Me.txtTipoCambio.Text)
            '    .IdTipoMoneda = CInt(Me.cboTipoMoneda.SelectedValue)
            '    .MontoPagoProg = CDec(Me.txtResultadoTipoCambio.Text)
            'Else
            '    .TipoCambio = CDec(Me.txtTipoCambio.Text)
            '    .MontoPagoProg = CDec(Me.txtMontoProg.Text)
            '    .IdTipoMoneda = CInt(Me.cboTipoMoneda.SelectedValue)
            'End If

        End With

        Return obj

    End Function

    Private Function obtenerListaRelacionDocumento() As List(Of Entidades.RelacionDocumento)
        Dim listaDocProvisonados As New List(Of Entidades.Documento)
        listaDocProvisonados = Session("newlista")
        Dim lista As New List(Of Entidades.RelacionDocumento)
        'Me.ListaDocProvisionados = getListaDocProvisionados()
        If (listaDocProvisonados IsNot Nothing) Then
            For i As Integer = 0 To listaDocProvisonados.Count - 1
                Dim objRelacionDocumento As New Entidades.RelacionDocumento
                With objRelacionDocumento
                    .IdDocumento1 = CInt(Me.hddIdDocumento.Value)
                    .IdDocumento2 = listaDocProvisonados(i).IdDocumento
                End With
                lista.Add(objRelacionDocumento)
            Next
        Else
            lista = Nothing
        End If
        Return lista
    End Function

    Private Sub ModificarPrecios()
        Try

            Dim MontoValidacion As Decimal = 0
            For i As Integer = 0 To Me.ListaDocProvisionados.Count - 1
                Dim TipoDocumento As String = Me.ListaDocProvisionados(i).NomTipoDocumento
                If (TipoDocumento <> "O/C") Then
                    MontoValidacion = Me.ListaDocProvisionados(i).Total
                    Exit For
                End If
            Next

            If (MontoValidacion <= CDec(hddSaldoRQ.Value)) Then
                Dim MontoADescontarGlobal As Decimal = 0
                Dim MontoADescontar As Decimal = 0
                Dim Monto As Decimal = 0
                Dim Iddocumento As Integer = 0
                Dim TipoDocumento As String = ""
                Dim SaldoOC As Decimal = 0
                Dim ResultadoSaldoRQ As Decimal = 0
                Dim IdDocumentoRQ As Integer = 0
                IdDocumentoRQ = CInt(hddIdDocumento.Value)

                For i As Integer = 0 To Me.ListaDocProvisionados.Count - 1
                    TipoDocumento = Me.ListaDocProvisionados(i).NomTipoDocumento
                    If (TipoDocumento <> "O/C") Then
                        Try

                            MontoADescontar = Me.ListaDocProvisionados(i).Total
                            'Iddocumento = ListaDocumentoRef(i).IdDocumento
                            ResultadoSaldoRQ = CDec(hddSaldoRQ.Value) - MontoADescontar

                            Dim query As String = "update MovCuentaporPagar set mcp_Saldo = @ResultadoSaldoRQ where IdDocumento = @Iddocumento"
                            'Using cmd As New SqlCommand(query, con)
                            '    cmd.CommandTimeout = 0
                            '    cmd.Parameters.AddWithValue("@ResultadoSaldoRQ", ResultadoSaldoRQ)
                            '    cmd.Parameters.AddWithValue("@Iddocumento", IdDocumentoRQ)
                            '    con.Open()
                            '    cmd.ExecuteNonQuery()
                            '    con.Close()
                            '    MontoADescontar = 0
                            '    Iddocumento = 0
                            '    TipoDocumento = ""
                            '    MontoADescontarGlobal = ResultadoSaldoRQ
                            'End Using
                        Catch ex As Exception
                            Throw ex
                        End Try
                    Else
                        Try

                            Iddocumento = Me.ListaDocProvisionados(i).IdDocumento
                            MontoADescontar = Me.ListaDocProvisionados(i).Total
                            If (MontoADescontarGlobal = 0) Then
                                MontoADescontarGlobal = MontoADescontar
                            End If
                            Dim query As String = "update MovCuentaporPagar set mcp_Saldo = @mcp_Saldo where IdDocumento = @Iddocumento"
                            'Using cmd As New SqlCommand(query, con)
                            '    cmd.CommandTimeout = 0
                            '    cmd.Parameters.AddWithValue("@mcp_Saldo", MontoADescontarGlobal)
                            '    cmd.Parameters.AddWithValue("@Iddocumento", Iddocumento)
                            '    con.Open()
                            '    cmd.ExecuteNonQuery()
                            '    con.Close()
                            '    MontoADescontar = 0
                            '    Iddocumento = 0
                            '    TipoDocumento = ""
                            '    MontoADescontarGlobal = 0
                            'End Using
                        Catch ex As Exception
                            Throw ex
                        End Try
                    End If
                Next
            Else
                objScript.mostrarMsjAlerta(Me, "El monto del Documento provisionado supera el monto del R/Q o O/C favor de seleccionar un documento de provisión de menor monto. No procede la operación.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btncalcularTC_Click(sender As Object, e As System.EventArgs) Handles btncalcularTC.Click
        If (txtMontoProg.Text = "" Or txtTipoCambio.Text = "" Or txtTipoCambio.Text = "0") Then
            objScript.mostrarMsjAlerta(Me, "Ingrese un Tipo de Cambio Correcto.")
        Else
            Dim ResultadoxTC As Decimal = CDec(CDec(txtMontoProg.Text) / CDec(txtTipoCambio.Text))
            txtResultadoTipoCambio.Text = CStr(FormatNumber(ResultadoxTC, 2))
        End If
    End Sub

    Private Sub btnGuardarEmitirCheque_Click(sender As Object, e As System.EventArgs) Handles btnGuardarEmitirCheque.Click
        If Me.btnGuardarEmitirCheque.CommandName.ToLower() = "guardar_cheque" Then
            Dim nroFilasGrillaDocReferencia As Integer = 0
            nroFilasGrillaDocReferencia = Me.GV_MovCuenta_CXP.Rows.Count
            If nroFilasGrillaDocReferencia = 0 Then
                objScript.mostrarMsjAlerta(Me, "Operación invalida, cerrar programación y volver a intentar.")
                Exit Sub
            End If

            '======================================================
            'Obtenemos la lista RelacionDocumento del Requerimiento'
            '=======================================================
            Dim datakeyRequerimiento As DataKey = Me.GV_MovCuenta_CXP.DataKeys(0)
            Dim idDocumentoReferenciaReq As Integer = datakeyRequerimiento(0) ' Id del requerimiento
            Dim listaRelacionDocumentoReq As New List(Of Entidades.RelacionDocumento)
            Dim objetoRelacionDocumentoReq As New Entidades.RelacionDocumento
            With objetoRelacionDocumentoReq
                .IdDocumento1 = idDocumentoReferenciaReq
                .IdDocumento2 = Nothing
            End With
            listaRelacionDocumentoReq.Add(objetoRelacionDocumentoReq)
            '================================================================
            'Obtenemos la lista RelacionDocumento de la programacion de pagos'
            '=================================================================
            Dim indiceProgramacion As Integer = hddIndiceProgramacion.Value
            Dim datakeyProgramacion As DataKey = GV_ProgramacionPago.DataKeys(indiceProgramacion)
            Dim idDocumentoProgram As Integer = datakeyProgramacion(2)
            Dim listaRelacionDocumentoProgram As New List(Of Entidades.RelacionDocumento)
            Dim objetoRelacionDocumentoProgram As New Entidades.RelacionDocumento
            With objetoRelacionDocumentoProgram
                .IdDocumento1 = idDocumentoProgram
                .IdDocumento2 = Nothing
            End With
            listaRelacionDocumentoProgram.Add(objetoRelacionDocumentoProgram)
            '==========================================
            'Obtengo la cantidad del importe del cheque
            '==========================================
            Dim montoProgramacion As Decimal = 0
            montoProgramacion = CDec(DirectCast(GV_ProgramacionPago.Rows(indiceProgramacion).FindControl("lblMontoProg"), Label).Text)
            '==============================================================================
            'El monto de emisión del cheque no puede ser menor ni mayor al monto programado
            '===============================================================================
            If CDec(Me.txtMontoEmitirCheque.Text) <> montoProgramacion Then
                objScript.mostrarMsjAlerta(Me, "El Monto ingresado no coincide con la programación. No procede la operación.")
                Exit Sub
            End If
            'p.Id
            If objNegDocumentocheque.ValidarNumeroCheque(0, Me.ddlSerieNumeracionEmitirCheque.SelectedValue, Me.ddlNroChequeEmitirCheque.Text) Then
                If objNegDocumentocheque.RegistrarDocumentoCheque(CType(obtenerDocumento(), Entidades.Documento), obtenerObservacion(), obtenerMovBanco(obtenerDocumento), obtenerDocumento.AnexoDoc, listaRelacionDocumentoReq, listaRelacionDocumentoProgram) <> -1 Then
                    Dim lista As New List(Of Entidades.DocumentoCheque)
                    lista = (New Negocio.DocumentoCheque).LN_EmitirCheque(idDocumentoProgram)
                    Me.gvEmisionCheques.DataSource = lista
                    Me.gvEmisionCheques.DataBind()
                    Me.GV_ProgramacionPago.DataSource = (New Negocio.ProgramacionPago_CXP).SelectxIdMovCuentaCXP(CInt(Me.hddIdMovCuenta_CXP.Value))
                    Me.GV_ProgramacionPago.DataBind()
                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaEmitirCheque'); alert('Cheque Emitido.'); ", True)
                End If
            End If
        End If
    End Sub

    Private Function obtenerDocumento() As Entidades.DocumentoCheque
        Dim datakey_GV_MovCuenta_CXP As DataKey = Me.GV_MovCuenta_CXP.DataKeys(0)
        Dim objetoDocumento As New Entidades.DocumentoCheque
        With objetoDocumento
            .IdBeneficiario = datakey_GV_MovCuenta_CXP(1)
            .Id = 0
            .SerieCheque.IdBanco = Me.ddlBancoEmitirCheque.SelectedValue
            .SerieCheque.IdCuentaBancaria = CInt(IIf(CStr(Me.ddlCuentaBancariaEmitirCheque.SelectedValue) <> "", Me.ddlCuentaBancariaEmitirCheque.SelectedValue, 0))
            If Me.txtMontoEmitirCheque.Text <> "" Then
                .Monto = CDec(Util.decodificarTexto(txtMontoEmitirCheque.Text))
            Else
                .Monto = 0D
            End If
            .IdUsuario = CType(Session.Item("IdUsuario"), Integer)
            'p.IdSupervisor = CType(Session.Item("IdSupervisor"), Integer)

            .IdSerieCheque = CInt(Me.ddlSerieNumeracionEmitirCheque.SelectedValue)
            If Me.txtFechaEmisionEmitirCheque.Text <> "" Then
                .FechaEmision = CDate(txtFechaEmisionEmitirCheque.Text)
            Else
                .FechaEmision = Nothing
            End If
            If Me.txtFEchaCobroEmitirCheque.Text <> "" Then
                .FechaACobrar = CDate(txtFEchaCobroEmitirCheque.Text)
            Else
                .FechaACobrar = Nothing
            End If

            .FechaRegistro = Now.Date()

            .AnexoDoc.IdMedioPago = _IDMEDIOPAGO
            Dim i As Integer
            Dim listaCheques As List(Of Entidades.SerieChequeView)
            If .IdSerieCheque <> 0 Then
                listaCheques = objNegSeriecheque.SelectAll()
                listaCheques = (From item In listaCheques
                              Where (item.Id = .IdSerieCheque)
                              Select item).ToList()
                With listaCheques(0)
                    .IdMoneda = .IdMoneda
                    .Serie = .Serie
                    objetoDocumento.IdMoneda = .IdMoneda
                    objetoDocumento.Serie = .Serie
                End With
            End If

            .IdEstadoDoc = CInt(ddlEstadoDocEmitirCheque.SelectedValue)
            .IdEstadoEntrega = CInt(ddlEstadoEntregaEmitirCheque.SelectedValue)
            .IdEstadoCancelacion = CInt(ddlEstadoCancelacionEmitirCheque.SelectedValue)

            .IdTipoDocumento = _IDTIPODOCUMENTO
            .TotalLetras = (New Negocio.ALetras).Letras(Me.txtMontoEmitirCheque.Text)
            .ChequeNumero = Me.ddlNroChequeEmitirCheque.Text
            .AnexoDoc.NroOperacion = Me.txtNroVoucherEmitirCheque.Text
        End With
        Return objetoDocumento
    End Function


    Private Function obtenerObservacion() As Entidades.Observacion
        Dim objObservacion As Entidades.Observacion = Nothing

        If (Me.txtObservaciones_Prog.Text.Trim.Length > 0) Then
            objObservacion = New Entidades.Observacion
            With objObservacion
                .Id = Nothing
                'Select Case SModo
                '    Case modo_menu.Nuevo
                '        .IdDocumento = Nothing
                '    Case modo_menu.Editar
                '        .IdDocumento = SId
                'End Select
                .IdDocumento = Nothing
                .Observacion = Me.txtObservaciones_Prog.Text.Trim
            End With
        End If
        Return objObservacion
    End Function

    Private Function obtenerMovBanco(ByVal x As Entidades.DocumentoCheque, Optional ByVal actualizar As Boolean = False) As Entidades.MovBanco
        'Esta funcion genera o actualiza un movbanco.
        Dim obj As Entidades.MovBanco
        If Not actualizar Then
            obj = New Entidades.MovBanco
        Else
            Dim objDAO As New Negocio.MovBancoView
            Dim objDAOAnexo As New Negocio.Anexo_MovBanco
            Dim L As List(Of Entidades.Anexo_MovBanco)
            L = objDAOAnexo.SelectxIdDocumento(0)
            'L = objDAOAnexo.SelectxIdDocumento(SId)

            Dim i As Integer
            Dim obj1 As Entidades.MovBanco

            For i = 0 To L.Count - 1
                obj1 = objDAO.SelectxId(L(i).IdMovBanco)
                If obj1.IdConceptoMovBanco = _IDCONCEPTOMOVBANCO Then
                    Exit For
                End If
            Next
            If obj1 IsNot Nothing Then
                obj = obj1
            Else
                obj = New Entidades.MovBanco
            End If

            'obj=objDAO.SelectxId(objDAOAnexo.

        End If

        If x.IdEstadoCancelacion = 2 Then
            obj.EstadoAprobacion = True
            obj.IdSupervisor = SIdUsuario
        Else
            obj.EstadoAprobacion = False
        End If

        obj.IdBanco = x.SerieCheque.IdBanco
        obj.IdCuentaBancaria = x.SerieCheque.IdCuentaBancaria
        obj.EstadoMov = True
        obj.Factor = -1
        obj.IdPersonaRef = x.IdBeneficiario
        obj.Monto = x.Monto
        obj.IdConceptoMovBanco = _IDCONCEPTOMOVBANCO
        obj.FechaMov = Now()

        Return obj

    End Function

    Private Sub RadioButtonList1_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles RadioButtonList1.SelectedIndexChanged
        Select Case Me.RadioButtonList1.SelectedValue
            Case 0
                Me.trFiltroNroDocumento.Visible = False
                Me.trFiltroFecha.Visible = True
                Me.trFiltroProveedor.Visible = False
            Case 1
                Me.trFiltroNroDocumento.Visible = True
                Me.trFiltroFecha.Visible = False
                Me.trFiltroProveedor.Visible = False
            Case 2
                Me.trFiltroNroDocumento.Visible = False
                Me.trFiltroFecha.Visible = False
                Me.trFiltroProveedor.Visible = True
        End Select
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('capaDocProvisionados');", True)
    End Sub

    Private Sub lbContinuarGrabando_Click(sender As Object, e As System.EventArgs) Handles lbContinuarGrabando.Click
        Try
            Call registrarProgramacion()
            verFrm(FrmModo.Inicio)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnGuardarCuentaBancaria_Click(sender As Object, e As System.EventArgs) Handles btnGuardarCuentaBancaria.Click
        Dim dato As New Negocio.bl_pagoProveedor
        Dim idProveedor As Integer = hddIdPersona.Value
        Try
            dato.crearCuentaDetraccionxProveedor(Me.cboBanco.SelectedValue, idProveedor, Me.txtNumero.Text.Trim(), Me.txtCtaContable.Text, Me.txtcuentaInter.Text, _
                                        Me.txtswiftbanca.Text, Me.cboMoneda.SelectedValue)
            objScript.mostrarMsjAlerta(Me, "Cuenta bancaria Detracción creada satisfactoriamente.")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub txtFecha_ProgPago_TextChanged(sender As Object, e As System.EventArgs) Handles txtFecha_ProgPago.TextChanged
        Try
            Me.txtTipoCambio.Text = (New Negocio.bl_pagoProveedor).buscarTipoCambioCompra(Me.txtFecha_ProgPago.Text)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Fecha inválida")
        End Try
    End Sub


    Private Sub chkEsAdelanto_CheckedChanged(sender As Object, e As System.EventArgs) Handles chkEsAdelanto.CheckedChanged
        If chkEsAdelanto.Checked Then
            chkEsCtaPorRendir.Checked = False
            Me.lblSubtitulo.Text = "ELEGIR TIPO DE ANTICIPO"
            listarConceptosAdelanto("A")
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "onLoad", "mostrarCapaAnticipos();", True)
        Else
            chkEsCtaPorRendir.Checked = False
            chkEsAdelanto.Checked = False
            Me.gvConceptoReferencia.DataSource = Nothing
            Me.gvConceptoReferencia.DataBind()
        End If
    End Sub

    Private Sub chkEsCtaPorRendir_CheckedChanged(sender As Object, e As System.EventArgs) Handles chkEsCtaPorRendir.CheckedChanged
        If chkEsCtaPorRendir.Checked Then
            chkEsAdelanto.Checked = False
            Me.lblSubtitulo.Text = "ELEGIR TIPO DE CUENTA X RENDIR"
            Dim objeto As New Negocio.bl_pagoProveedor
            Me.gvTipoAnticipos.DataSource = objeto.ListarcuentasPorRendir("C")
            Me.gvTipoAnticipos.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "onLoad", "mostrarCapaAnticipos();", True)
        Else
            chkEsCtaPorRendir.Checked = False
            chkEsAdelanto.Checked = False
            Me.gvConceptoReferencia.DataSource = Nothing
            Me.gvConceptoReferencia.DataBind()
        End If
    End Sub

    Private Sub listarConceptosAdelanto(flag As String)
        Dim objeto As New Negocio.bl_pagoProveedor
        Me.gvTipoAnticipos.DataSource = objeto.listarConceptoAdelantos(flag)
        Me.gvTipoAnticipos.DataBind()
    End Sub

    Private Sub frmProgramacionProvision_Disposed(sender As Object, e As System.EventArgs) Handles Me.Disposed
        Session("ListaProgramacion") = Nothing
    End Sub
End Class