﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmResumenDocumentosManuales.aspx.vb" Inherits="APPWEB.FrmResumenDocumentosManuales" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
    <table>
        <tr>
            <td colspan="3">
                <span class="Texto">Configurar ruta Destino (server)</span>
                <asp:TextBox ID="rutaDestino" runat="server" Width="550px" placeholder="Ingrese ruta del servidor en donde se alojará el archivo"></asp:TextBox>
            </td>
            <td>
                
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButtonList ID="rblDocManuales" runat ="server">
                    <asp:ListItem Text="Generar reporte de documentos manuales." Value="" Selected="True"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
                <span style="font-weight:bold" class="Texto">Fecha:</span>
                <asp:TextBox ID="txtInicio" runat="server" CssClass="TextBox_Fecha"
                                            Width="100px"></asp:TextBox>
                 <cc1:MaskedEditExtender ID="txtInicio_MaskedEditExtender" runat="server"
                                            ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                            CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                            CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtInicio">
                 </cc1:MaskedEditExtender>
                 <cc1:CalendarExtender ID="txtFechaVcto_Inicio_CalendarExtender" runat="server" Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtInicio">
                </cc1:CalendarExtender>
            </td>
            <td>
                <asp:DropDownList ID="ddlDropDownListMotivo" runat = "server">
                    <asp:ListItem Text="Conexión a internet" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Fallas Fluido Eléctrico" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Desastres Naturales" Value="3"></asp:ListItem>
                    <asp:ListItem Text="Robo" Value="4"></asp:ListItem>
                    <asp:ListItem Text="Fallas en el sistema de facturación" Value="5"></asp:ListItem>
                    <asp:ListItem Text="Venta Itinerante" Value="6"></asp:ListItem>
                    <asp:ListItem Text="Otros" Value="7"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="btnGenerar" runat="server" Text="Generar" />
            </td>
        </tr>
    </table>
</div>
</asp:Content>