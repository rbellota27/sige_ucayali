<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmReporte_CXP.aspx.vb" Inherits="APPWEB.FrmReporte_CXP" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <table class="style1">
        <tr>
            <td class="TituloCelda" >
                REPORTE - CUENTAS POR PAGAR</td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto" style="font-weight:bold;text-align:right">
                            Empresa:</td>
                        <td>
                            <asp:DropDownList ID="cboEmpresa" runat="server" Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="Texto" style="font-weight:bold;text-align:right">
                            Deudas:</td>
                        <td>
                            <asp:DropDownList ID="cboOpcion_Deuda" runat="server" Width="100%" >
                            <asp:ListItem Value="0">Deudas pendientes</asp:ListItem>
                            <asp:ListItem Value="1" >Deudas canceladas</asp:ListItem>
                            <asp:ListItem Value="2" >Todos</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="Texto" style="font-weight:bold;text-align:right" >
                            Fecha:</td>
                        <td>
                            <table>
                                <tr>
                                    <td style="width:20px">
                                    </td>
                                    <td class="Texto" style="font-weight:bold;text-align:right" >
                                        Inicio:</td>
                                    <td>
                                    <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="TextBox_Fecha" 
                                onblur="return(  valFecha_Blank(this) );" Width="100px"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="_MaskedEditExtender" runat="server" 
                                ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" 
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                Mask="99/99/9999" TargetControlID="txtFechaInicio">
                            </cc1:MaskedEditExtender>
                            <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFechaInicio">
                            </cc1:CalendarExtender>
                                    </td>
                                    <td class="Texto" style="font-weight:bold;text-align:right" >
                                        Fin:</td>
                                    <td>
                                    
                                    <asp:TextBox ID="txtFechaFin" runat="server" CssClass="TextBox_Fecha" 
                                onblur="return(  valFecha_Blank(this) );" Width="100px"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaFin" runat="server" 
                                ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" 
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                Mask="99/99/9999" TargetControlID="txtFechaFin" >
                            </cc1:MaskedEditExtender>
                            <cc1:CalendarExtender ID="CalendarExtender_txtFechaFin" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFechaFin" >
                            </cc1:CalendarExtender>
                                    
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <asp:Button ID="btnAceptar_ViewRpt" runat="server" Text="Reporte" ToolTip="Visualizar Reporte" Width="70px"  />
                        </td>
                            <td>
                                <asp:Button id="btnExport" runat="server" Text="Exportar" Width="140px"/>
                            </td>
                    </tr>
                    </table>
            </td>
        </tr>
        <tr>
        <td>
              <table style="width: 100%;">                                    
                              
                                <tr>
                                    
                                <td>
                                        <asp:Panel ID="pnlBusquedaPersona" runat="server">
                            <table width="100%">
                              <tr>
                                  <td class="SubTituloCelda">
                                      B�SQUEDA DE PERSONA</td>
                                </tr>  
                                <tr>
                                
                                    <td>
                                        <table width="100%">                                            
                                            <tr>
                                                <td>
                                                    <table width="100%">                                              
                                                        <tr>
                                                            <td>
                                                                <table >
                                                                <tr>
                                                                <td colspan="5">
                                                                  <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal" AutoPostBack="false">
                                                                <asp:ListItem  Value="N">Natural</asp:ListItem>
                                                                <asp:ListItem Selected="True" Value="J">Juridica</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                                </td>
                                                                </tr>
                                                                    <tr>
                                                                        <td class="Texto">
                                                                            Razon Social / Nombres:                                                                            
                                                                        </td>
                                                                        <td colspan="4" >
                                                                            <asp:Panel ID="Panel1" runat="server" DefaultButton="btBuscarPersonaGrilla">                                                                            
                                                                        <asp:TextBox ID="tbRazonApe"  runat="server"
                                                                                Width="450px"></asp:TextBox>
                                                                                </asp:Panel>
                                                                        </td>                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="Texto">                                                                                                                                                    
                                                                        D.N.I.:
                                                                        </td>
                                                                        <td >               
                                                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btBuscarPersonaGrilla">                                                             
                                                                            <asp:TextBox ID="tbbuscarDni" 
                                                                                MaxLength="8" runat="server"></asp:TextBox>
                                                                                </asp:Panel>
                                                                        </td>
                                                                        <td class="Texto" >
                                                                          Ruc:                                                                            
                                                                        </td>
                                                                        <td>
                                                                        <asp:Panel ID="Panel3" runat="server" DefaultButton="btBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" 
                                                                                MaxLength="11"></asp:TextBox>                  
                                                                                </asp:Panel>                                                          
                                                                        </td>
                                                                        <td>
                                                                        <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                                                onmouseout="this.src='../Imagenes/Buscar_b.JPG';" onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                    PageSize="20" Width="100%">
                                                                    <Columns>
                                                                        <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                        <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                        <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                            <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>                                                                            
                                                                    </Columns>
                                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                                    <FooterStyle CssClass="GrillaFooter" />
                                                                    <PagerStyle CssClass="GrillaPager" />
                                                                    <RowStyle CssClass="GrillaRow" />
                                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                    ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                                <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                    ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                                <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                    runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                        Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                                <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                    onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>                                
                            </table>
                        </asp:Panel>
                                </td>
                                </tr>                                                                 
                 </table>      
                 
                    </td>
        </tr>
         <tr>
                                  <td class="SubTituloCelda">
                                      PERSONA SELECCIONADA</td>
                                </tr>  
        <tr>
            <td >
            
   <asp:Panel ID="Panel_Persona" runat="server"  >
    <table >
        <tr>
            <td class="Texto" style="font-weight:bold;text-align:right">
                Descripci�n:</td>
            <td colspan="3">
                <asp:TextBox ID="txtDescripcionPersona" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" Width="400px" runat="server"></asp:TextBox>
            </td>
            
          <td>
                <asp:Button ID="btnBuscarPersona" runat="server" Text="Buscar" Width="70px" CssClass="btnBuscar" />
            </td>
            <td>
                <asp:Button ID="btnLimpiarPersona" runat="server" Text="Limpiar" Width="70px" CssClass="btnLimpiar" />
            </td>
        </tr>
        <tr>
            <td class="Texto" style="font-weight:bold;text-align:right" >
                D.N.I.:</td>
            <td>
                <asp:TextBox ID="txtDNI" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>
            </td>
            <td class="Texto" style="font-weight:bold;text-align:right" >
                R.U.C.:</td>
            <td>
                <asp:TextBox ID="txtRUC"  ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>                                
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>

    </asp:Panel>
            
           </td>
        </tr>
        <tr>
            <td>
            
                                                          

                    
                    <asp:GridView ID="GV_ReporteCXP" runat="server" AutoGenerateColumns="False" 
                    Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" 
                    BorderWidth="1px" CellPadding="3" PageSize="10"  AllowPaging="true" 
                        AllowSorting="True">
                        <Columns>
                        
                            <asp:TemplateField HeaderText="Documento" >
                                <ItemTemplate>
                                <table>
                                <tr>
                                <td>
                                <asp:Label ID="lblDocumento"  runat="server" ForeColor="#0B3861"
                                Text='<%#DataBinder.Eval(Container.DataItem,"TipoDoc_NroDocumento")%>'></asp:Label></td> 
                                </tr>
                                </table>                                        
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Fecha Emisi�n" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                <asp:Label ID="lblfechaems"  runat="server" ForeColor="#0B3861"
                                Text='<%#DataBinder.Eval(Container.DataItem,"doc_FechaEmision","{0:d}")%>'></asp:Label>
                                </ItemTemplate>                       
                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                             
                            
                            <asp:TemplateField HeaderText="Fecha Venc." HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                            <asp:Label ID="lblFechavenc"  runat="server" ForeColor="#0B3861"
                            Text='<%#DataBinder.Eval(Container.DataItem,"doc_FechaVenc","{0:d}")%>'></asp:Label>
                            </ItemTemplate>                       
                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                                                  
                            
                            <asp:TemplateField HeaderText="Monto" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                            <table>
                            <tr>
                            <td> <asp:Label ID="lblMonto"  runat="server" Font-Bold="true" ForeColor="Red"
                            Text='<%#DataBinder.Eval(Container.DataItem,"Moneda_Monto")%>'></asp:Label></td>
                            </tr>
                            </table>
                            </ItemTemplate>                      
                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            


                            <asp:TemplateField HeaderText="Saldo" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                            <table>
                            <tr>
                            <td> <asp:Label ID="lblsaldo"  runat="server" Font-Bold="true" ForeColor="Red"
                            Text='<%#DataBinder.Eval(Container.DataItem,"Moneda_Saldo")%>'></asp:Label></td>
                            </tr>
                            </table>
                            </ItemTemplate>                      
                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>

                            
                            <asp:TemplateField HeaderText="Ruc / Dni" >
                            <ItemTemplate>
                            <table>
                            <tr>
                            <td>
                            <asp:Label ID="lblruc"  runat="server" ForeColor="#0B3861"
                            Text='<%#DataBinder.Eval(Container.DataItem,"Ruc")%>'></asp:Label></td> 
                            </tr>
                            </table>                                        
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            
                            <asp:TemplateField HeaderText="Empresa" >
                            <ItemTemplate>
                            <table>
                            <tr>
                            <td>
                            <asp:Label ID="lblEmpresa"  runat="server" ForeColor="#0B3861"
                            Text='<%#DataBinder.Eval(Container.DataItem,"Empresa")%>'></asp:Label></td> 
                            </tr>
                            </table>                                        
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            
                            <asp:TemplateField HeaderText="Persona" >
                            <ItemTemplate>
                            <table>
                            <tr>
                            <td>
                            <asp:Label ID="lblPersona"  runat="server" ForeColor="#0B3861"
                            Text='<%#DataBinder.Eval(Container.DataItem,"DescripcionPersona")%>'></asp:Label></td> 
                            </tr>
                            </table>                                        
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            
                            <asp:TemplateField HeaderText="Cheque Asociado" >
                            <ItemTemplate>
                            <table>
                            <tr>
                            <td>
                            <asp:Label ID="lblcheque"  runat="server" ForeColor="#0B3861"
                            Text='<%#DataBinder.Eval(Container.DataItem,"ChequesAsociados")%>'></asp:Label></td> 
                            </tr>
                            </table>                                        
                            </ItemTemplate>
                            </asp:TemplateField>
                            

                            <asp:TemplateField HeaderText="Pago Programado" >
                            <ItemTemplate>
                            <table>
                            <tr>
                            <td>
                            <asp:Label ID="lblpagoprog"  runat="server" ForeColor="#0B3861"
                            Text='<%#DataBinder.Eval(Container.DataItem,"PagoProgramado")%>'></asp:Label></td> 
                            </tr>
                            </table>                                        
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                             <asp:TemplateField HeaderText="Doc. Externo." >
                            <ItemTemplate>
                            <table>
                            <tr>
                            <td>
                            <asp:Label ID="lblNroDocumentoExterno"  runat="server" ForeColor="#0B3861"
                            Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumentoExterno")%>'></asp:Label></td> 
                            </tr>
                            </table>                                        
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            
                        
                 </Columns>
                 
            <RowStyle CssClass="GrillaRow" ForeColor="#000066" />
            <FooterStyle CssClass="GrillaFooter" BackColor="White" ForeColor="#000066" />
            <PagerStyle CssClass="GrillaPager"
            HorizontalAlign="Left" BackColor="White" ForeColor="#000066" />
            <SelectedRowStyle CssClass="GrillaSelectedRow" BackColor="#669999" Font-Bold="True" 
            ForeColor="White" />
            <HeaderStyle CssClass="GrillaHeader" BackColor="#006699" Font-Bold="True" 
            ForeColor="White" />
            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
    </asp:GridView>

            </td>      
        
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdPersona" Value="" runat="server" />
            </td>
        </tr>
    </table>
  
  
                      
                 
                     
                                                                                
    <script language="javascript" type="text/javascript" >
        //************************** BUSQUEDA PERSONA
        //***************************************************** BUSQUEDA PERSONA



    </script>
    
</asp:Content>
