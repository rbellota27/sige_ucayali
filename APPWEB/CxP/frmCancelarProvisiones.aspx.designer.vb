﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmCancelarProvisiones

    '''<summary>
    '''Control txtDesde.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDesde As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtDesde_MaskedEditExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDesde_MaskedEditExtender As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control CalendarExtender1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CalendarExtender1 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control txtHasta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtHasta As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtHasta_MaskedEditExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtHasta_MaskedEditExtender As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control CalendarExtender2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CalendarExtender2 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control txtSerie.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtSerie As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtCodigo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtCodigo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control ddlBanco.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ddlBanco As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control btnBuscarProgramacionesxPagar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscarProgramacionesxPagar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnBuscarProgramacionesCanceladas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscarProgramacionesCanceladas As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control gv_programacionCancelacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents gv_programacionCancelacion As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control ImageButton2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ImageButton2 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control txtNroConstancia.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtNroConstancia As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtmontoDeposito.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtmontoDeposito As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtFechaPago.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaPago As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control MaskedEditExtender1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MaskedEditExtender1 As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control CalendarExtender3.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CalendarExtender3 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control txtHorapago.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtHorapago As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control MaskedEditExtender2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MaskedEditExtender2 As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control gvDocRefDetracciones.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents gvDocRefDetracciones As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control lbSiguiente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lbSiguiente As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control lbCancelar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lbCancelar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control hddidproveedor.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddidproveedor As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddidMoneda.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddidMoneda As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddidtienda.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddidtienda As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddidEmpresa.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddidEmpresa As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddidtipoDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddidtipoDocumento As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddidRequerimiento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddidRequerimiento As Global.System.Web.UI.WebControls.HiddenField
End Class
