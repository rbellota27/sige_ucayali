﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Imports System.IO
Imports System.Data
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Configuration
Partial Public Class FrmReporte_CXP
    Inherits System.Web.UI.Page

    Private ListaReporte As List(Of Entidades.MovCuentaPorPagar)
    Private objScript As New ScriptManagerClass

    Private Function getListaReporte() As List(Of Entidades.MovCuentaPorPagar)
        Return CType(Session.Item("ListaReporte"), List(Of Entidades.MovCuentaPorPagar))
    End Function
    Private Sub setListaReporte(ByVal lista As List(Of Entidades.MovCuentaPorPagar))
        Session.Remove("ListaReporte")
        Session.Add("ListaReporte", lista)
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        valOnLoad_Frm()

    End Sub

    Private Sub valOnLoad_Frm()

        Try

            If (Not Me.IsPostBack) Then
                inicializarFrm()
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub inicializarFrm()

        Dim objCbo As New Combo
        With objCbo
            .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
        End With

        Dim objProgramacionPedido As Entidades.ProgramacionPedido = (New Negocio.ProgramacionPedido).ProgramacionPedidoSelectxFecha((New Negocio.FechaActual).SelectFechaActual)
        With objProgramacionPedido
            Me.txtFechaInicio.Text = Format(.cal_FechaIni, "dd/MM/yyyy")
            Me.txtFechaFin.Text = Format(.cal_FechaFin, "dd/MM/yyyy")
        End With
        Me.pnlBusquedaPersona.Visible = False

        Session.Add("ListaReporte", ListaReporte)
        Session.Remove("ListaReporte")

    End Sub
#Region "****************Buscar Personas Mantenimiento"
    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))

            'ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "   offCapa('capaPersona');     ", True)

            gvBuscar.DataSource = Nothing
            gvBuscar.DataBind()
            Me.pnlBusquedaPersona.Visible = False
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)

        Dim objPersonaView As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)
        If (objPersonaView IsNot Nothing) Then

            Me.txtDescripcionPersona.Text = objPersonaView.Descripcion
            Me.txtDNI.Text = objPersonaView.Dni
            Me.txtRUC.Text = objPersonaView.Ruc
            Me.hddIdPersona.Value = CStr(objPersonaView.IdPersona)

        End If
    End Sub
    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            'objScript.onCapa(Me, "capaPersona")
        Else
            objScript.mostrarMsjAlerta(Me, "No se hallaron registros.")
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", 0)

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

    Protected Sub btnLimpiarPersona_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLimpiarPersona.Click
        valOnClick_btnLimpiarPersona()
    End Sub
    Private Sub valOnClick_btnLimpiarPersona()
        Try
            Me.txtDescripcionPersona.Text = ""
            Me.txtDNI.Text = ""
            Me.txtRUC.Text = ""
            Me.hddIdPersona.Value = ""
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub ResultadoBusqueda()

        Try
            Dim objMovCuentaPorPagar As Entidades.Documento = New Entidades.Documento
            objMovCuentaPorPagar.IdEmpresa = CInt(cboEmpresa.SelectedValue)
            'hddIdPersona.Value.Length > 0
            If ((hddIdPersona.Value <> Nothing)) Then
                objMovCuentaPorPagar.IdPersona = CInt(hddIdPersona.Value)
            Else
                objMovCuentaPorPagar.IdPersona = 0
            End If
            objMovCuentaPorPagar.FechaInicio = CDate(txtFechaInicio.Text)
            objMovCuentaPorPagar.FechaFin = CDate(txtFechaFin.Text)
            objMovCuentaPorPagar.opciondeuda = CInt(cboOpcion_Deuda.SelectedValue)
            Dim lista As List(Of Entidades.MovCuentaPorPagar) = New List(Of Entidades.MovCuentaPorPagar)
            lista = (New Negocio.MovCuentaPorPagar).CR_MovCuenta_CXP_Lista(objMovCuentaPorPagar.IdEmpresa, objMovCuentaPorPagar.IdPersona, objMovCuentaPorPagar.FechaInicio, objMovCuentaPorPagar.FechaFin, objMovCuentaPorPagar.opciondeuda)
            GV_ReporteCXP.DataSource = lista
            GV_ReporteCXP.DataBind()

            ListaReporte = lista
            setListaReporte(Me.ListaReporte)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnAceptar_ViewRpt_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptar_ViewRpt.Click
        If (cboEmpresa.SelectedItem.Text.Length > 1 And txtFechaInicio.Text.Length > 0 And txtFechaFin.Text.Length > 0 And cboOpcion_Deuda.SelectedItem.Text.Length > 1) Then
            Try
                ResultadoBusqueda()
            Catch ex As Exception
                Throw ex
            End Try
        Else
            objScript.mostrarMsjAlerta(Me, "Elija todos los parámetros para seguir con la búsqueda.")
        End If
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExport.Click
        If (GV_ReporteCXP.Rows.Count = 0) Then
            objScript.mostrarMsjAlerta(Me, "Por Favor Ingrese los Criterios de Búsqueda")
        Else
            ExportToExcel("Export.xls", GV_ReporteCXP)
        End If
    End Sub
    Private Sub ExportToExcel(ByVal nameReport As String, ByVal wControl As GridView)
        Dim responsePage As HttpResponse = Response
        Dim sw As New StringWriter()
        Dim htw As New HtmlTextWriter(sw)
        Dim pageToRender As New Page()
        Dim form As New HtmlForm()
        form.Controls.Add(wControl)
        pageToRender.Controls.Add(form)
        responsePage.Clear()
        responsePage.Buffer = True
        responsePage.ContentType = "application/vnd.ms-excel"
        responsePage.AddHeader("Content-Disposition", "attachment;filename=" & nameReport)
        responsePage.Charset = "UTF-8"
        responsePage.ContentEncoding = Encoding.Default
        pageToRender.RenderControl(htw)
        responsePage.Write(sw.ToString())
        responsePage.End()
    End Sub

    Protected Sub btnBuscarPersona_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarPersona.Click
        Me.pnlBusquedaPersona.Visible = True
    End Sub

    Protected Sub GV_ReporteCXP_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_ReporteCXP.PageIndexChanging
        GV_ReporteCXP.PageIndex  = e.NewPageIndex
        Me.ListaReporte = getListaReporte()
        GV_ReporteCXP.DataSource = ListaReporte
        GV_ReporteCXP.DataBind()
    End Sub

End Class