<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Visor.aspx.vb" Inherits="APPWEB.Visor4" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="margin-right: 0px; margin-left: 0px; margin-top: 0px;">
    <form id="form1" runat="server">
    <div>
        <CR:CrystalReportViewer ID="CRViewer" runat="server" AutoDataBind="true" PrintMode="ActiveX" HasCrystalLogo="False" HasToggleGroupTreeButton="False" ToolPanelView="None"/>  
    </div>
    </form>
</body>
</html>
