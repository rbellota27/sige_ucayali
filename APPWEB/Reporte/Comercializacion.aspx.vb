﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class Comercializacion
    Inherits System.Web.UI.Page


    Dim objScript As New ScriptManagerClass
    Dim cbo As Combo


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        OnLoad_Comercializacion()
    End Sub


    Private Sub OnLoad_Comercializacion()
        If Not IsPostBack Then
            Try
                Dim FechaActual As Date = (New Negocio.FechaActual).SelectFechaActual

                cbo = New Combo
                With cbo


                    .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                    .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), True)
                    .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTiendaPrecio, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)

                    .LlenarCboTipoAlmacen(Me.cboTipoAlmacen, False)
                    .LLenarCboALmacenPrincipalxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

                    .llenarCboTipoExistencia(Me.cboTipoExistencia, True)
                    .llenarCboLineaxTipoExistencia(Me.cboLinea, CInt(Me.cboTipoExistencia.SelectedValue), True)
                    .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), CInt(Me.cboTipoExistencia.SelectedValue), True)


                    .LlenarCboYear(Me.idYearI, False) : Me.idYearI.SelectedValue = FechaActual.Year.ToString
                    .LlenarCboSemanaxIdYear(Me.idSemanaI, CInt(idYearI.SelectedValue), False)
                    .LlenarCboYear(Me.idYearF, False) : Me.idYearF.SelectedValue = FechaActual.Year.ToString
                    .LlenarCboSemanaxIdYear(Me.idSemanaF, CInt(idYearF.SelectedValue), False)

                    .llenarCboTipoDocumentoxIdTipoDocumentos(Me.cboTipoDocumento, "16,15,56,6", 2, False)

                End With

                S_FiltroTablaValor = New List(Of Entidades.TipoTablaValor)
                S_TipoTablaValor = New List(Of Entidades.TipoTablaValor)
                S_ListaTipoAlmacen = New List(Of Entidades.TipoAlmacen)

                Add_TipoAlmacen(0, True)

                actualizarProgramacionSemana(FechaActual)


                Me.txtFechaInicio_DocRef.Text = Format(FechaActual, "dd/MM/yyyy").Trim
                Me.txtFechaFin_DocRef.Text = Me.txtFechaInicio_DocRef.Text.Trim

                Me.actualizarOpcionesBusquedaDocRef(0, False)

                Me.listaDocumentoRef = New List(Of Entidades.Documento)
                setlistaDocumentoRef(Me.listaDocumentoRef)


            Catch ex As Exception
                objScript.mostrarMsjAlerta(Me, ex.Message)
            End Try
        End If
    End Sub

    Private Sub actualizarProgramacionSemana(ByVal Fecha As Date)

        Dim objCbo As New Combo
        Dim objProgramacionSemana As Entidades.ProgramacionPedido = (New Negocio.ProgramacionPedido).ProgramacionPedidoSelectxFecha(Fecha)

        If (objProgramacionSemana IsNot Nothing) Then
            If (Me.idYearI.Items.FindByValue(CStr(objProgramacionSemana.IdYear)) IsNot Nothing) Then
                Me.idYearI.SelectedValue = CStr(objProgramacionSemana.IdYear)
                objCbo.LlenarCboSemanaxIdYear(Me.idSemanaI, CInt(Me.idYearI.SelectedValue), False)
            End If

            If (Me.idSemanaI.Items.FindByValue(CStr(objProgramacionSemana.IdSemana)) IsNot Nothing) Then
                Me.idSemanaI.SelectedValue = CStr(objProgramacionSemana.IdSemana)
            End If

            If (Me.idYearF.Items.FindByValue(CStr(objProgramacionSemana.IdYear)) IsNot Nothing) Then
                Me.idYearF.SelectedValue = CStr(objProgramacionSemana.IdYear)
                objCbo.LlenarCboSemanaxIdYear(Me.idSemanaI, CInt(Me.idYearF.SelectedValue), False)
            End If

            If (Me.idSemanaF.Items.FindByValue(CStr(objProgramacionSemana.IdSemana)) IsNot Nothing) Then
                Me.idSemanaF.SelectedValue = CStr(objProgramacionSemana.IdSemana)
            End If

            Me.txtFechaInicioSemanaI.Text = Format(objProgramacionSemana.cal_FechaIni, "dd/MM/yyyy")
            Me.txtFechaFinSemanaI.Text = Format(objProgramacionSemana.cal_FechaFin, "dd/MM/yyyy")

            Me.txtFechaInicioSemanaFin.Text = Format(objProgramacionSemana.cal_FechaIni, "dd/MM/yyyy")
            Me.txtFechaFinSemanafin.Text = Format(objProgramacionSemana.cal_FechaFin, "dd/MM/yyyy")

        End If


    End Sub

    Private Sub idSemanaI_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles idSemanaI.SelectedIndexChanged
        llenarcboSemanaActual(Me.idYearI, idSemanaI, Me.txtFechaInicioSemanaI, Me.txtFechaFinSemanaI)
    End Sub
    Private Sub idYearI_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles idYearI.SelectedIndexChanged
        llenarcboSemanaActual(Me.idYearI, idSemanaI, Me.txtFechaInicioSemanaI, Me.txtFechaFinSemanaI)
    End Sub
    Private Sub idSemanaF_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles idSemanaF.SelectedIndexChanged
        llenarcboSemanaActual(Me.idYearF, idSemanaF, Me.txtFechaInicioSemanaFin, Me.txtFechaFinSemanafin)
    End Sub
    Private Sub idYearF_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles idYearF.SelectedIndexChanged
        llenarcboSemanaActual(Me.idYearF, idSemanaF, Me.txtFechaInicioSemanaFin, Me.txtFechaFinSemanafin)
    End Sub

    Public Sub llenarcboSemanaActual(ByVal cboano As DropDownList, ByVal cboSem As DropDownList, ByVal txtFechaInicio As TextBox, ByVal txtFechafin As TextBox)
        Try
            Dim objProgramacionPedido As Entidades.ProgramacionPedido = (New Negocio.ProgramacionPedido).ProgramacionPedidoSelectxIdYearxIdSemana(CInt(cboano.SelectedValue), CInt(cboSem.SelectedValue))
            If (objProgramacionPedido IsNot Nothing) Then

                txtFechaInicio.Text = Format(objProgramacionPedido.cal_FechaIni, "dd/MM/yyyy")
                txtFechafin.Text = Format(objProgramacionPedido.cal_FechaFin, "dd/MM/yyyy")


            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try
            cbo = New Combo
            With cbo

                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), True)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTiendaPrecio, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)

            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoExistencia.SelectedIndexChanged

        cbo = New Combo
        With cbo

            .llenarCboLineaxTipoExistencia(Me.cboLinea, CInt(Me.cboTipoExistencia.SelectedValue), True)
            .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), CInt(Me.cboTipoExistencia.SelectedValue), True)

        End With

    End Sub

    Private Sub cboLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLinea.SelectedIndexChanged

        cbo = New Combo
        With cbo

            .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), CInt(Me.cboTipoExistencia.SelectedValue), True)

        End With


    End Sub


#Region " **************** Buscar Tabla *** "
    Private ListaTipoTabla As List(Of Entidades.TipoTabla)

    Private Sub ImgBuscarTabla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBuscarTabla.Click
        Buscar_Tabla()
    End Sub
    Private Sub Buscar_Tabla()

        Try

            Me.ViewState.Add("BuscarTabla", Me.txtBuscarTabla.Text.Trim)
            Me.Find_Tabla(0)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub Find_Tabla(ByVal TipoMov As Integer)

        Dim Index As Integer

        Select Case TipoMov
            Case 0
                Index = 0
            Case 1
                Index = (CInt(Me.txtPageIndexTabla.Text) - 1) - 1 ' ******* ANTERIOR
            Case 2
                Index = (CInt(Me.txtPageIndexTabla.Text) - 1) + 1 ' ******* SIGUIENTE
            Case 3
                Index = (CInt(Me.txtPageIndexGoTabla.Text) - 1) - 1 ' ******* IR
        End Select

        Dim lista As List(Of Entidades.TipoTabla) = (New Negocio.TipoTabla).TipoTablaSelectAllxEstado_Paginado(CStr(Me.ViewState("BuscarTabla")), 1, Index, Me.GV_Tabla.PageSize)

        If Not IsNothing(lista) Then

            If lista.Count > 0 Then

                Me.txtPageIndexTabla.Text = CStr(Index + 1)

                Me.GV_Tabla.DataSource = lista
                Me.GV_Tabla.DataBind()
                Me.GV_Tabla.SelectedIndex = -1

            Else
                objScript.mostrarMsjAlerta(Me, "No se hallaron Registros.")
            End If

        End If

        Me.ModalPopup_Atributo.Show()

    End Sub

    Private Sub btnAnteriorTabla_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnteriorTabla.Click
        Find_Tabla(1)
    End Sub

    Private Sub btnSiguienteTabla_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSiguienteTabla.Click
        Find_Tabla(2)
    End Sub

    Private Sub btnIrTabla_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIrTabla.Click
        Find_Tabla(3)
    End Sub


    Private Sub GV_Tabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Tabla.SelectedIndexChanged

        Me.lblNomTabla.Text = HttpUtility.HtmlDecode(CType(Me.GV_Tabla.Rows(Me.GV_Tabla.SelectedRow.RowIndex).FindControl("lblTabla"), Label).Text).Trim

        Me.ModalPopup_Atributo.Show()
        Me.ModalPopup_TablaValor.Show()

    End Sub

#End Region

#Region " **************** Buscar Tabla Valor *** "

    Private ListaTipoTablaValor As List(Of Entidades.TipoTablaValor)
    Private ListaFiltroTablaValor As List(Of Entidades.TipoTablaValor)

    Private Property S_TipoTablaValor() As List(Of Entidades.TipoTablaValor)
        Get
            Return CType(Session.Item("ListaTipoTablaValor2"), List(Of Entidades.TipoTablaValor))
        End Get
        Set(ByVal value As List(Of Entidades.TipoTablaValor))
            Session.Remove("ListaTipoTablaValor2")
            Session.Add("ListaTipoTablaValor2", value)
        End Set
    End Property
    Private Property S_FiltroTablaValor() As List(Of Entidades.TipoTablaValor)
        Get
            Return CType(Session.Item("ListaFiltroTablaValor2"), List(Of Entidades.TipoTablaValor))
        End Get
        Set(ByVal value As List(Of Entidades.TipoTablaValor))
            Session.Remove("ListaFiltroTablaValor2")
            Session.Add("ListaFiltroTablaValor2", value)
        End Set
    End Property


    Private Sub ImgBuscarTablaValor_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBuscarTablaValor.Click
        Buscar_TablaValor()
    End Sub
    Private Sub Buscar_TablaValor()

        Dim Index As Integer = Me.GV_Tabla.SelectedRow.RowIndex

        If Index < 0 Then
            objScript.mostrarMsjAlerta(Me, "Seleccione un tipo de tabla")
            ModalPopup_Atributo.Show()
            Exit Sub

        End If


        Me.ViewState.Add("BuscarTablaValor", Me.txtBuscarTablaValor.Text.Trim)
        Me.ViewState.Add("IdTipoTabla", CType(Me.GV_Tabla.Rows(Index).FindControl("hddIdTipoTabla"), HiddenField).Value)
        Find_TablaValor(0)

    End Sub
    Private Sub Find_TablaValor(ByVal TipoMov As Integer)
        Try
            Dim Index As Integer

            Select Case TipoMov
                Case 0
                    Index = 0
                Case 1
                    Index = (CInt(Me.txtPageIndexTablaValor.Text) - 1) - 1 ' ******* ANTERIOR
                Case 2
                    Index = (CInt(Me.txtPageIndexTablaValor.Text) - 1) + 1 ' ******* SIGUIENTE
                Case 3
                    Index = (CInt(Me.txtPageIndexGoTablaValor.Text) - 1) - 1 ' ******* IR
            End Select

            ListaTipoTablaValor = (New Negocio.TipoTablaValor).TipoTablaValorSelectAllxIdTipoTablaxEstado_Paginado(CInt(Me.ViewState("IdTipoTabla")), CStr(Me.ViewState("BuscarTablaValor")), 1, Index, Me.GV_TablaValor.PageSize)

            If Not IsNothing(ListaTipoTablaValor) Then

                If ListaTipoTablaValor.Count > 0 Then

                    S_TipoTablaValor = ListaTipoTablaValor

                    Me.txtPageIndexTablaValor.Text = CStr(Index + 1)

                    Me.GV_TablaValor.DataSource = ListaTipoTablaValor
                    Me.GV_TablaValor.DataBind()


                Else
                    objScript.mostrarMsjAlerta(Me, "No se hallaron Registros.")
                End If

            End If

            Me.ModalPopup_Atributo.Show()
            Me.ModalPopup_TablaValor.Show()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnAnteriorTablaValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnteriorTablaValor.Click
        Find_TablaValor(1)
    End Sub

    Private Sub btnSiguienteTablaValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSiguienteTablaValor.Click
        Find_TablaValor(2)
    End Sub

    Private Sub btnIrTablaValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIrTablaValor.Click
        Find_TablaValor(3)
    End Sub

    Private Sub ImgAgregarTablaValor_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgAgregarTablaValor.Click
        Agregar_TablaValor()
    End Sub
    Private Sub Agregar_TablaValor()
        Try
            ListaFiltroTablaValor = S_FiltroTablaValor
            ListaTipoTablaValor = S_TipoTablaValor

            Dim Lista As New List(Of Entidades.TipoTablaValor)

            For i As Integer = 0 To Me.GV_TablaValor.Rows.Count - 1
                If CType(Me.GV_TablaValor.Rows(i).FindControl("ckTablaValor"), CheckBox).Checked Then

                    ListaFiltroTablaValor.Add(ListaTipoTablaValor(i))

                End If
            Next

            Me.S_FiltroTablaValor = ListaFiltroTablaValor
            Me.GV_FiltroTipoTabla.DataSource = ListaFiltroTablaValor
            Me.GV_FiltroTipoTabla.DataBind()

            Me.ModalPopup_Atributo.Show()
            Me.ModalPopup_TablaValor.Show()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged

        Me.ListaFiltroTablaValor = Me.S_FiltroTablaValor
        Me.ListaFiltroTablaValor.RemoveAt(Me.GV_FiltroTipoTabla.SelectedRow.RowIndex)
        Me.S_FiltroTablaValor = ListaFiltroTablaValor

        Me.GV_FiltroTipoTabla.DataSource = ListaFiltroTablaValor
        Me.GV_FiltroTipoTabla.DataBind()

    End Sub

#End Region


#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"

    Private listaDocumentoRef As List(Of Entidades.Documento)

    Private Function getlistaDocumentoRef() As List(Of Entidades.Documento)
        Return CType(Session.Item("listaTrazabilidadDocRef2"), List(Of Entidades.Documento))
    End Function
    Private Sub setlistaDocumentoRef(ByVal lista As List(Of Entidades.Documento))
        Session.Remove("listaTrazabilidadDocRef2")
        Session.Add("listaTrazabilidadDocRef2", lista)
    End Sub

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click

        mostrarDocumentosRef_Find()

    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As Integer = 0
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdPersona As Integer = 0


            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CInt(Me.txtCodigo_BuscarDocRef.Text)
            End Select

            'Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).Documento_BuscarDocumentoRef(CInt(Me.ddlEmpresa.SelectedValue), CInt(Me.ddlTienda.SelectedValue), IdPersona, CInt(Me.cboTipoDocumento.SelectedValue), serie, codigo, 0, fechaInicio, fechafin, False)

            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).Documento_Buscar(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), IdPersona, CInt(Me.cboTipoDocumento.SelectedValue), serie, codigo, fechaInicio, fechafin, False)


            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged

        cargarDocumentoReferencia(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))

    End Sub

    Private Sub cargarDocumentoReferencia(ByVal IdDocumentoRef As Integer)

        Try

            validar_AddDocumentoRef(IdDocumentoRef)

            '***************** OBTENEMOS EL DOCUMENTO DE REFERENCIA
            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumentoRef)

            '*************** GUARDAMOS EN SESSION
            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.listaDocumentoRef.Add(objDocumento)
            setlistaDocumentoRef(Me.listaDocumentoRef)

            '************** 
            'cargarDocumentoRef_GUI(IdDocumentoRef, objDocumento, Nothing, Nothing, Nothing, Nothing, Nothing)

            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            Me.hddIdDocumento.Value = CStr(IdDocumentoRef)

            Me.GV_DocumentosReferencia_Find.DataBind()



        Catch ex As Exception
            Me.hddIdDocumento.Value = "0"
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

    Private Function obtenerListaDetalleDocumento_Load_DocRef(ByVal IdDocumentoRef As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Fecha As Date, ByVal IdTipoPV As Integer, ByVal IdPersona As Integer, ByVal IdMoneda As Integer, ByVal IdVendedor As Integer) As List(Of Entidades.DetalleDocumento)

        Dim lista As List(Of Entidades.DetalleDocumento) = (New Negocio.DocumentoCotizacion).DocumentoVenta_SelectDetxIdDocumentoRef(IdDocumentoRef, IdEmpresa, IdTienda, Fecha, IdTipoPV, IdPersona, IdMoneda, IdVendedor)

        For i As Integer = 0 To lista.Count - 1

            lista(i).ListaUM_Venta = (New Negocio.ProductoTipoPV).SelectUM_Venta(lista(i).IdProducto, IdTienda, IdTipoPV)

        Next

        Return lista

    End Function

    Private Function validar_AddDocumentoRef(ByVal IdDocumentoRef As Integer) As Boolean

        Me.listaDocumentoRef = getlistaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            If (Me.listaDocumentoRef(i).Id = IdDocumentoRef) Then
                Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next

        Return True

    End Function

    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia")
        End If

    End Sub

    Private Sub GV_DocumentoRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoRef.SelectedIndexChanged
        quitarDocumentoRef(Me.GV_DocumentoRef.SelectedRow.RowIndex)
    End Sub
    Private Sub quitarDocumentoRef(ByVal index As Integer)

        Try

            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.listaDocumentoRef.RemoveAt(index)

            '********* GUARDAMOS EN SESSION
            setlistaDocumentoRef(Me.listaDocumentoRef)

            '************ GUI
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

#End Region


    Private Sub imgLimpiarDocumento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgLimpiarDocumento.Click
        Me.hddIdDocumento.Value = "0"
        Me.GV_DocumentoRef.DataBind()
        Me.GV_DocumentosReferencia_Find.DataBind()
    End Sub

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        cbo = New Combo
        With cbo
            .LLenarCboALmacenPrincipalxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)
        End With
    End Sub

    Private Sub cboTipoAlmacen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoAlmacen.SelectedIndexChanged
        cbo = New Combo
        With cbo
            .LLenarCboALmacenPrincipalxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)
        End With
    End Sub

#Region "************************ TIPO ALMACEN"

    Dim ListaTipoAlmacen As List(Of Entidades.TipoAlmacen)

    Private Property S_ListaTipoAlmacen() As List(Of Entidades.TipoAlmacen)
        Get
            Return CType((Session.Item("ListaTipoAlmacen1")), List(Of Entidades.TipoAlmacen))
        End Get
        Set(ByVal value As List(Of Entidades.TipoAlmacen))
            Session.Remove("ListaTipoAlmacen1")
            Session.Add("ListaTipoAlmacen1", value)
        End Set
    End Property

    Private Sub imgAddTipoAlmacen_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgAddTipoAlmacen.Click

        Add_TipoAlmacen(CInt(Me.cboTipoAlmacen.SelectedValue))

    End Sub

    Private Sub Add_TipoAlmacen(ByVal IdTipoAlmacen As Integer, Optional ByVal add_Principal As Boolean = False)

        Me.ListaTipoAlmacen = (New Negocio.TipoAlmacen).SelectAllActivo()

        If add_Principal Then

            Me.S_ListaTipoAlmacen.AddRange(ListaTipoAlmacen.FindAll(Function(T As Entidades.TipoAlmacen) T.Tipoalm_Principal = "True"))

        Else

            Me.S_ListaTipoAlmacen.AddRange(ListaTipoAlmacen.FindAll(Function(T As Entidades.TipoAlmacen) T.IdTipoAlmacen = IdTipoAlmacen))

        End If

        Me.GV_TipoAlmacen.DataSource = Me.S_ListaTipoAlmacen
        Me.GV_TipoAlmacen.DataBind()

    End Sub


    Protected Sub lkbQuitar_Click(ByVal sender As Object, ByVal e As EventArgs)

        remove_TipoAlmacen(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)

    End Sub

    Private Sub remove_TipoAlmacen(ByVal Index As Integer)

        Me.ListaTipoAlmacen = S_ListaTipoAlmacen

        Me.ListaTipoAlmacen.RemoveAt(Index)

        Me.S_ListaTipoAlmacen = Me.ListaTipoAlmacen

        Me.GV_TipoAlmacen.DataSource = Me.ListaTipoAlmacen
        Me.GV_TipoAlmacen.DataBind()

    End Sub




#End Region



End Class