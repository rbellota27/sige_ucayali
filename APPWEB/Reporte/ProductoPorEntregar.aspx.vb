﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class ProductoPorEntregar
    Inherits System.Web.UI.Page

    Dim objScript As New ScriptManagerClass
    Dim cbo As Combo


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        onLoad_ProductoPorEntregar()
    End Sub


    Private Sub onLoad_ProductoPorEntregar()

        Try
            cbo = New Combo
            With cbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), True)

                .LlenarCboTipoAlmacen(Me.cboTipoAlmacen, False)
                .LlenarCboAlmacenxIdTipoAlmacenxIdEmpresa(Me.cboAlmacen, CInt(Me.cboTipoAlmacen.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), True)

                .llenarCboTipoDocumentoxIdTipoDocumentos(Me.cboTipoDocumento, "1,3,15", 2, False)
                .LlenarCboRol(Me.cmbRol, True)

            End With

            Me.txtFechaInicio_DocRef.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.txtFechaFin_DocRef.Text = Me.txtFechaInicio_DocRef.Text

            Me.actualizarOpcionesBusquedaDocRef(0, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        cbo = New Combo
        With cbo
            .LlenarCboAlmacenxIdTipoAlmacenxIdEmpresa(Me.cboAlmacen, CInt(Me.cboTipoAlmacen.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), True)
            .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), True)
        End With
    End Sub

    Private Sub cboTipoAlmacen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoAlmacen.SelectedIndexChanged
        cbo = New Combo
        With cbo
            .LlenarCboAlmacenxIdTipoAlmacenxIdEmpresa(Me.cboAlmacen, CInt(Me.cboTipoAlmacen.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), True)
        End With
    End Sub


#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"

    Private listaDocumentoRef As List(Of Entidades.Documento)

    Private Function getlistaDocumentoRef() As List(Of Entidades.Documento)
        Return CType(Session.Item("listaTrazabilidadDocRef3"), List(Of Entidades.Documento))
    End Function
    Private Sub setlistaDocumentoRef(ByVal lista As List(Of Entidades.Documento))
        Session.Remove("listaTrazabilidadDocRef3")
        Session.Add("listaTrazabilidadDocRef3", lista)
    End Sub

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click

        mostrarDocumentosRef_Find()

    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As Integer = 0
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdPersona As Integer = 0


            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CInt(Me.txtCodigo_BuscarDocRef.Text)
            End Select

            'Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).Documento_BuscarDocumentoRef(CInt(Me.ddlEmpresa.SelectedValue), CInt(Me.ddlTienda.SelectedValue), IdPersona, CInt(Me.cboTipoDocumento.SelectedValue), serie, codigo, 0, fechaInicio, fechafin, False)

            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).Documento_Buscar(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), IdPersona, CInt(Me.cboTipoDocumento.SelectedValue), serie, codigo, fechaInicio, fechafin, False)


            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged

        cargarDocumentoReferencia(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))

    End Sub

    Private Sub cargarDocumentoReferencia(ByVal IdDocumentoRef As Integer)

        Try

            validar_AddDocumentoRef(IdDocumentoRef)

            '***************** OBTENEMOS EL DOCUMENTO DE REFERENCIA
            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumentoRef)

            '*************** GUARDAMOS EN SESSION
            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.listaDocumentoRef.Add(objDocumento)
            setlistaDocumentoRef(Me.listaDocumentoRef)

            '************** 
            'cargarDocumentoRef_GUI(IdDocumentoRef, objDocumento, Nothing, Nothing, Nothing, Nothing, Nothing)

            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            Me.hddIdDocumento.Value = CStr(IdDocumentoRef)

            Me.GV_DocumentosReferencia_Find.DataBind()



        Catch ex As Exception
            Me.hddIdDocumento.Value = "0"
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

    Private Function obtenerListaDetalleDocumento_Load_DocRef(ByVal IdDocumentoRef As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Fecha As Date, ByVal IdTipoPV As Integer, ByVal IdPersona As Integer, ByVal IdMoneda As Integer, ByVal IdVendedor As Integer) As List(Of Entidades.DetalleDocumento)

        Dim lista As List(Of Entidades.DetalleDocumento) = (New Negocio.DocumentoCotizacion).DocumentoVenta_SelectDetxIdDocumentoRef(IdDocumentoRef, IdEmpresa, IdTienda, Fecha, IdTipoPV, IdPersona, IdMoneda, IdVendedor)

        For i As Integer = 0 To lista.Count - 1

            lista(i).ListaUM_Venta = (New Negocio.ProductoTipoPV).SelectUM_Venta(lista(i).IdProducto, IdTienda, IdTipoPV)

        Next

        Return lista

    End Function

    Private Function validar_AddDocumentoRef(ByVal IdDocumentoRef As Integer) As Boolean

        Me.listaDocumentoRef = getlistaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            If (Me.listaDocumentoRef(i).Id = IdDocumentoRef) Then
                Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next

        Return True

    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="opcion">0--> por Fecha, 1--> por nro Documento</param>
    ''' <param name="onCapa"></param>
    ''' <remarks></remarks>
    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia")
        End If

    End Sub

    Private Sub GV_DocumentoRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoRef.SelectedIndexChanged
        quitarDocumentoRef(Me.GV_DocumentoRef.SelectedRow.RowIndex)
    End Sub
    Private Sub quitarDocumentoRef(ByVal index As Integer)
        Try

            Me.GV_DocumentoRef.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

#End Region

#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", CInt(cmbRol.SelectedValue))  '******************** TODOS

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try
            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub cargarPersona(ByVal IdPersona As Integer, Optional ByVal valGv_Detalle As Boolean = True)

        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)

        Me.txtDescripcionPersona.Text = objPersona.Descripcion
        Me.txtDni.Text = objPersona.Dni
        Me.txtRuc.Text = objPersona.Ruc
        Me.hddIdPersona.Value = CStr(objPersona.IdPersona)

    End Sub

#End Region


End Class