﻿<%@ Page Title="Comercializacion Mes" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="ComercializacionMes.aspx.vb" Inherits="APPWEB.ComercializacionMes" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td class="TituloCelda">
                Reporte Comparativo de Comercialización Por Mes
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td align="center">
                            <fieldset>
                                <legend><span class="Texto"></span></legend>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Empresa:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            Tienda:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            Almacén:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboAlmacen" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnBuscarDocumento" runat="server" Text="Buscar Documento" ToolTip="Buscar Documento"
                                                OnClientClick=" return ( onCapa('capaDocumentosReferencia') ); " />
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <fieldset>
                                <legend><span class="Texto">Rango de Fecha</span></legend>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Inicio:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFechaInicio1" runat="server" Width="70px" CssClass="TextBox_Fecha"
                                                onBlur=" return ( valFecha_Blank(this) ); "></asp:TextBox>
                                            <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" ClearMaskOnLostFocus="false"
                                                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio1">
                                            </cc1:MaskedEditExtender>
                                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                TargetControlID="txtFechaInicio1">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td class="Texto">
                                            Fin:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFechaFin1" runat="server" Width="70px" CssClass="TextBox_Fecha"
                                                onBlur=" return ( valFecha_Blank(this) ); "></asp:TextBox>
                                            <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" ClearMaskOnLostFocus="false"
                                                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin1">
                                            </cc1:MaskedEditExtender>
                                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                TargetControlID="txtFechaFin1">
                                            </cc1:CalendarExtender>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fieldset>
                                <legend><span class="Texto"></span></legend>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Tipo Existencia:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboTipoExistencia" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            Linea:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboLinea" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            Sub-Linea:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboSubLinea" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fieldset>
                                <legend><span class="Texto">Atributo de Producto</span></legend>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnOpen_Atributo" runat="server" Text="Buscar" ToolTip="Buscar Atributos" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                                Width="650px">
                                                <Columns>
                                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                                    <asp:TemplateField HeaderText="Atributo">
                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomTabla")%>'></asp:Label>

                                                                        <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />

                                                                        <asp:HiddenField ID="hddIdTipoTablaValor" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTablaValor")%>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Valor">
                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblTipoTablaValor" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="GrillaRow" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fieldset>
                                <legend><span class="Texto">Consultar Saldos y Precios</span></legend>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Tipo Almacén:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboTipoAlmacen" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            Precio:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboTiendaPrecio" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="imgAddTipoAlmacen" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                OnClientClick=" return ( add_TipoAlmacen() ); " />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td colspan="2" align="center">
                                            <asp:GridView ID="GV_TipoAlmacen" runat="server" AutoGenerateColumns="False" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblItem" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>

                                                                        <asp:LinkButton ID="lkbQuitar" runat="server" OnClick="lkbQuitar_Click" OnClientClick=" return ( remove_TipoAlmacen(this) ); ">Quitar</asp:LinkButton>

                                                                        <asp:HiddenField ID="hddIdTipoAlmacen" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoAlmacen") %>' />

                                                                        <asp:HiddenField ID="hddTipoAlmacenPrincipal" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Tipoalm_Principal") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Tipo de Almacén" DataField="Tipoalm_Nombre" HeaderStyle-Height="25px"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                                    </asp:BoundField>
                                                </Columns>
                                                <RowStyle CssClass="GrillaRow" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                    <legend><span class="Texto">Documento</span></legend>
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgLimpiarDocumento" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_DocumentoRef" runat="server" AutoGenerateColumns="False" Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:BoundField DataField="NomTipoDocumento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red"
                                            HeaderText="Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="25px" />
                                        <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblNroDocumento" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                            <asp:HiddenField ID="hddIdDocumento0" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                            <asp:HiddenField ID="hddIdEmpresa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />
                                                            <asp:HiddenField ID="hddIdAlmacen0" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />
                                                            <asp:HiddenField ID="hddIdPersona0" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Empresa" HeaderText="Empresa" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="Tienda" HeaderText="Tienda" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>        
        <tr>
            <td>
                <asp:ImageButton ID="imgAceptar" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                    OnClientClick=" return ( Aceptar() ); " />
            </td>
        </tr>
        <tr>
            <td>
                <iframe id="iReportFrame" name="iReportFrame" width="100%" height="2000px" scrolling="no">
                </iframe>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnOpen_TablaValor" runat="server" Text="" Width="1px" Height="1px" />
                        </td>
                        <td>
                            <asp:HiddenField ID="hddIdProducto" runat="server" Value="0" />
                        </td>
                        <td>
                            <asp:HiddenField ID="hddIdDocumento" runat="server" Value="0" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <cc1:ModalPopupExtender ID="ModalPopup_Atributo" runat="server" TargetControlID="btnOpen_Atributo"
                                PopupControlID="Panel_Atributo" BackgroundCssClass="modalBackground" Enabled="true"
                                RepositionMode="None" CancelControlID="btnClose_Atributo" Y="50">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="Panel_Atributo" runat="server" CssClass="modalPopup" Style="display: none;"
                                Width="750px">
                                <table width="100%">
                                    <tr>
                                        <td align="right">
                                            <asp:Image ID="btnClose_Atributo" runat="server" ImageUrl="~/Imagenes/Cerrar.gif" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <fieldset>
                                                <legend><span class="Texto">Clases de Atributos</span></legend>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Descripción:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtBuscarTabla" runat="server" MaxLength="50" Width="250px" onKeyPress=" return ( BuscarTabla(event) ); "></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="ImgBuscarTabla" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                                            ToolTip="Buscar Tabla" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:GridView ID="GV_Tabla" runat="server" AutoGenerateColumns="False" Width="100%"
                                                                PageSize="20">
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <Columns>
                                                                    <asp:CommandField ShowSelectButton="True" />
                                                                    <asp:TemplateField HeaderText="Descripción">
                                                                        <ItemTemplate>
                                                                                        <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTabla") %>' />
                                                                                        <asp:Label ID="lblTabla" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="Abv" HeaderText="Abv." />
                                                                </Columns>
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Button ID="btnAnteriorTabla" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                            ToolTip="Página Anterior" OnClientClick="return(valNavegacionTabla('0'));" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button ID="btnSiguienteTabla" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                            ToolTip="Página Posterior" OnClientClick="return(valNavegacionTabla('1'));" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtPageIndexTabla" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                            runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button ID="btnIrTabla" runat="server" Font-Bold="true" Width="50px" Text="Ir"
                                                                            ToolTip="Ir a la Página" OnClientClick="return(valNavegacionTabla('2'));" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtPageIndexGoTabla" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                            onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <cc1:ModalPopupExtender ID="ModalPopup_TablaValor" runat="server" TargetControlID="btnOpen_TablaValor"
                                                PopupControlID="Panel_TablaValor" BackgroundCssClass="modalBackground" Enabled="true"
                                                RepositionMode="None" CancelControlID="ImgClose_TablaValor" Y="75">
                                            </cc1:ModalPopupExtender>
                                            <asp:Panel ID="Panel_TablaValor" runat="server" CssClass="modalPopup" Style="display: none;"
                                                Width="700px">
                                                <table width="100%">
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Image ID="ImgClose_TablaValor" runat="server" ImageUrl="~/Imagenes/Cerrar.gif" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <fieldset>
                                                                <legend><span class="Texto">Registros de </span>
                                                                    <asp:Label ID="lblNomTabla" runat="server" Font-Bold="true" CssClass="Texto" Text="Tabla"></asp:Label></legend>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="Texto">
                                                                                        Descripción:
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtBuscarTablaValor" runat="server" MaxLength="50" Width="250px"
                                                                                            onKeyPress=" return ( BuscarTablaValor(event) ); "></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:ImageButton ID="ImgBuscarTablaValor" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                                                            ToolTip="Buscar Tabla Valor" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:ImageButton ID="ImgAgregarTablaValor" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                                                            OnClientClick=" return ( AgregarTablaValor() ); " ToolTip="Agregar Valor" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:GridView ID="GV_TablaValor" runat="server" AutoGenerateColumns="False" Width="100%"
                                                                                PageSize="20">
                                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                                <Columns>
                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                                        <asp:CheckBox ID="ckTablaValor" runat="server" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Código">
                                                                                        <ItemTemplate>
                                                                                                        <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTabla") %>' />

                                                                                                        <asp:Label ID="lblTipoTabla" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Codigo") %>'></asp:Label>

                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Descripción">
                                                                                        <ItemTemplate>
                                                                                                        <asp:HiddenField ID="hddIdTipoTablaValor" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTablaValor") %>' />
                                                                                                        <asp:Label ID="lblTipoTablaValor" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField DataField="Abv" HeaderText="Abv." />
                                                                                </Columns>
                                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                                <PagerStyle CssClass="GrillaPager" />
                                                                                <RowStyle CssClass="GrillaRow" />
                                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                                            </asp:GridView>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Button ID="btnAnteriorTablaValor" runat="server" Font-Bold="true" Width="50px"
                                                                                            Text="<" ToolTip="Página Anterior" OnClientClick="return(valNavegacionTablaValor('0'));" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Button ID="btnSiguienteTablaValor" runat="server" Font-Bold="true" Width="50px"
                                                                                            Text=">" ToolTip="Página Posterior" OnClientClick="return(valNavegacionTablaValor('1'));" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtPageIndexTablaValor" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Button ID="btnIrTablaValor" runat="server" Font-Bold="true" Width="50px" Text="Ir"
                                                                                            ToolTip="Ir a la Página" OnClientClick="return(valNavegacionTablaValor('2'));" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtPageIndexGoTablaValor" Width="50px" CssClass="TextBox_ReadOnly"
                                                                                            runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div id="capaDocumentosReferencia" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 100px; left: 25px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton8" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaDocumentosReferencia')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tipo Documento:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTipoDocumento" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td align="right" style="text-align: left">
                                <asp:RadioButtonList ID="rdbBuscarDocRef" runat="server" RepeatDirection="Horizontal"
                                    CssClass="Texto" AutoPostBack="true">
                                    <asp:ListItem Value="0" Selected="True">Por Fecha de Emisión</asp:ListItem>
                                    <asp:ListItem Value="1">Por Nro. Documento</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarPersonaRef" runat="server" Text="Buscar Persona" ToolTip="Buscar Persona"
                                    OnClientClick=" return ( onClick_BuscarPersonaRef()  ); " />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxFecha" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaInicio_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaFin_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender5" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaFin_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRef" Width="70px" ToolTip="Buscar Documentos"
                                                    runat="server" Text="Buscar" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxCodigo" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Nro. Serie:
                                            </td>
                                            <td>
                                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnAceptarBuscarDocRefxCodigo">                                               
                                                <asp:TextBox ID="txtSerie_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                                    </asp:Panel>
                                            </td>
                                            <td class="Texto">
                                                Nro. Código:
                                            </td>
                                            <td>
                                            <asp:Panel ID="Panel2" runat="server" DefaultButton="btnAceptarBuscarDocRefxCodigo">
                                                <asp:TextBox ID="txtCodigo_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                                    </asp:Panel>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRefxCodigo" OnClientClick="return(  valOnClick_btnAceptarBuscarDocRefxCodigo()  );"
                                                    runat="server" Text="Buscar" Width="70px" ToolTip="Buscar Documentos" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_DocRef_Find" runat="server" ScrollBars="Horizontal" Width="100%">
                        <asp:GridView ID="GV_DocumentosReferencia_Find" runat="server" AutoGenerateColumns="false"
                            Width="100%">
                            <Columns>
                                <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="50px" />
                                <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Tipo" />
                                <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                    <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisión"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DescripcionPersona" HeaderText="Cliente" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">
Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
        function valNavegacionTabla(tipoMov) {

            var index = parseInt(document.getElementById('<%=txtPageIndexTabla.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGoTabla.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=txtPageIndexGoTabla.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGoTabla.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=txtPageIndexGoTabla.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGoTabla.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function valNavegacionTablaValor(tipoMov) {

            var index = parseInt(document.getElementById('<%=txtPageIndexTablaValor.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGoTablaValor.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=txtPageIndexGoTablaValor.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGoTablaValor.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=txtPageIndexGoTablaValor.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGoTablaValor.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function BuscarTablaValor(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) {
                document.getElementById('<%=ImgBuscarTablaValor.ClientID %>').click();
                return false;
            }
            return true;
        }

        function BuscarTabla(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) {
                document.getElementById('<%=ImgBuscarTabla.ClientID %>').click();
                return false;
            }
            return true;
        }

        function AgregarTablaValor() {

            var GV_TablaValor = document.getElementById('<%=GV_TablaValor.ClientID %>');
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');

            if (GV_TablaValor != null) {

                for (var j = 1; j < GV_TablaValor.rows.length; j++) {
                    var rowElem2 = GV_TablaValor.rows[j];

                    if (rowElem2.cells[0].children[0].cells[0].children[0].checked == true) {

                        if (grilla != null) {
                            for (var i = 1; i < grilla.rows.length; i++) {
                                var rowElem = grilla.rows[i];

                                if (rowElem2.cells[1].children[0].value == rowElem.cells[1].children[1].value && rowElem2.cells[2].children[0].value == rowElem.cells[1].children[2].value) {
                                    alert('LA LISTA DE ATRIBUTOS YA CONTIENE LA TABLA < ' + rowElem.cells[1].children[0].innerText + ' > CON VALOR < ' + rowElem.cells[2].children[0].innerText + ' >. \n NO SE PERMITE LA OPERACIÓN.');
                                    return false;
                                }

                            }
                        }

                    } //end id
                }
            }

            return true;
        }

        function add_TipoAlmacen() {
            var cboTipoAlmacen = document.getElementById('<%=cboTipoAlmacen.ClientID %>');

            var grilla = document.getElementById('<%=GV_TipoAlmacen.ClientID %>');
            if (grilla != null) {

                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];

                    if (parseInt(rowElem.cells[0].children[2].value) == parseInt(cboTipoAlmacen.value)) {
                        alert('LA LISTA CONTIENE EL TIPO DE ALMACEN [ ' + cboTipoAlmacen.options[cboTipoAlmacen.selectedIndex].text + ' ] SELECCIONADO. \n NO SE PERMITE LA OPERACIÓN.');
                        return false;
                    }

                }

            }
            return true;
        }

        function remove_TipoAlmacen(obj) {

            var grilla = document.getElementById('<%=GV_TipoAlmacen.ClientID %>');
            if (grilla != null) {

                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];

                    if (rowElem.cells[0].children[3].value == 'True' && obj.id == rowElem.cells[0].children[1].id) {
                        alert('EL PRINCIPAL TIPO DE ALMACEN [ ' + rowElem.cells[1].innerText + ' ] NO PUEDE SER REMOVIDO DE LA LISTA. \n NO SE PERMITE LA OPERACIÓN.');
                        return false;
                    }

                }

            }
            return true;
        }

        function Aceptar() {

            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID %>');
            var IdEmpresa = parseInt(cboEmpresa.value);

            var cboTienda = document.getElementById('<%=cboTienda.ClientID %>');
            var IdTienda = parseInt(cboTienda.value);

            var IdTipoAlmacen = parseInt(document.getElementById('<%=cboTipoAlmacen.ClientID %>').value);
            var IdTiendaPrecio = parseInt(document.getElementById('<%=cboTiendaPrecio.ClientID %>').value);

            var IdAlmacen = parseInt(document.getElementById('<%=cboAlmacen.ClientID %>').value);
            if (isNaN(IdAlmacen)) { IdAlmacen = 0; }

            var IdTipoExistencia = parseInt(document.getElementById('<%=cboTipoExistencia.ClientID %>').value);
            var IdLinea = parseInt(document.getElementById('<%=cboLinea.ClientID %>').value);
            var IdSubLinea = parseInt(document.getElementById('<%=cboSubLinea.ClientID %>').value);
            var IdProducto = parseInt(document.getElementById('<%=hddIdProducto.ClientID %>').value);

            var IdDocumento = parseInt(document.getElementById('<%=hddIdDocumento.ClientID %>').value);


            var FechaIni = document.getElementById('<%=txtFechaInicio1.ClientID %>').value;
            if (!valFecha_Blank(document.getElementById('<%=txtFechaInicio1.ClientID %>'))) {
                alert('INGRESE UNA FECHA VALIDA.\n NO SE PERMITE LA OPERACIÓN.');
                return false;
            }

            var FechaFin = document.getElementById('<%=txtFechaFin1.ClientID %>').value;
            if (!valFecha_Blank(document.getElementById('<%=txtFechaFin1.ClientID %>'))) {
                alert('INGRESE UNA FECHA VALIDA.\n NO SE PERMITE LA OPERACIÓN.');
                return false;
            }

            iReportFrame.location.href = 'VisorGrilla.aspx?iReport=3&IdEmpresa=' + IdEmpresa + '&IdTienda=' + IdTienda + '&IdAlmacen=' + IdAlmacen +
                                         '&IdTipoExistencia=' + IdTipoExistencia + '&IdLinea=' + IdLinea + '&IdSubLinea=' + IdSubLinea + '&IdProducto=' + IdProducto +
                                         '&FechaIni=' + FechaIni + '&FechaFin=' + FechaFin + '&IdTipoAlmacen=' + IdTipoAlmacen +
                                         '&IdTiendaPrecio=' + IdTiendaPrecio + '&IdDocumento=' + IdDocumento;


            return false;
        }
    
    </script>

</asp:Content>
