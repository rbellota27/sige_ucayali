﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="ProductoEnTransito.aspx.vb" Inherits="APPWEB.ProductoEnTransito" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td class="TituloCelda">
                Reporte de Productos en Tránsito
            </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                    <legend><span class="Texto"></span></legend>
                    <table>
                        <tr>
                            <td class="Texto">
                                Empresa:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Tipo Almacén:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTipoAlmacen" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Almacén:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboAlmacen" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                    <legend><span class="Texto"></span></legend>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tipo Existencia:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTipoExistencia" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Linea:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboLinea" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Sub-Linea:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboSubLinea" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="imgAceptar" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                    OnClientClick=" return ( Aceptar() ); " />
            </td>
        </tr>
        <tr>
            <td>
                <iframe id="iReportFrame" name="iReportFrame" width="100%" height="2000px" scrolling="no">
                </iframe>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:HiddenField ID="hddIdProducto" runat="server" Value="0" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">


        function Aceptar() {

            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID %>');
            var IdEmpresa = parseInt(cboEmpresa.value);

            var cboAlmacen = document.getElementById('<%=cboAlmacen.ClientID %>');
            var IdAlmacen = parseInt(cboAlmacen.value);

            var IdTipoExistencia = parseInt(document.getElementById('<%=cboTipoExistencia.ClientID %>').value);
            var IdLinea = parseInt(document.getElementById('<%=cboLinea.ClientID %>').value);
            var IdSubLinea = parseInt(document.getElementById('<%=cboSubLinea.ClientID %>').value);
            var IdProducto = parseInt(document.getElementById('<%=hddIdProducto.ClientID %>').value);


            iReportFrame.location.href = 'Visor.aspx?iReport=3&IdAlmacen=' + IdAlmacen + '&IdTipoExistencia=' + IdTipoExistencia + '&IdLinea=' + IdLinea + '&IdSubLinea=' + IdSubLinea + '&IdProducto=' + IdProducto;

            return false;
        }
        
        
    </script>

</asp:Content>
