﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmResumenGlobal.aspx.vb" Inherits="APPWEB.FrmResumenGlobal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td class="TituloCelda">
                Reporte Gerencial del dia
                <asp:Label ID="lblFecha" runat="server" Text="--"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td class="Texto">
                            Empresa:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboEmpresa" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="imgAceptar" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                    OnClientClick=" return ( Aceptar() ); " />
            </td>
        </tr>
        <tr>
            <td>
                <iframe id="iReportFrame" name="iReportFrame" width="100%" height="2000px" scrolling="no">
                </iframe>
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">

        function Aceptar() {

            var IdEmpresa = parseInt(document.getElementById('<%=cboEmpresa.ClientID %>').value);

            var Fecha = document.getElementById('<%=lblFecha.ClientID %>').innerText;
            
            iReportFrame.location.href = 'Visor.aspx?iReport=6&IdEmpresa=' + IdEmpresa + '&Fecha=' + Fecha;
            return false;
        }
        
    </script>

</asp:Content>
