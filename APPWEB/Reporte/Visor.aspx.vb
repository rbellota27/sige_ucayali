﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports CrystalDecisions.CrystalReports.Engine


Partial Public Class Visor4
    Inherits System.Web.UI.Page


    Dim DS As DataSet
    Dim REPORTE As ReportDocument

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            Dim IReport As Integer = CInt(Me.Request.QueryString("iReport"))

            ViewState.Add("iReporte", IReport)

            Select Case IReport

                Case 1 ' ***************    REPORTE DE SALDOS

                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdAlmacen", Request.QueryString("IdAlmacen"))

                    ViewState.Add("IdTipoExistencia", Request.QueryString("IdTipoExistencia"))
                    ViewState.Add("IdLinea", Request.QueryString("IdLinea"))
                    ViewState.Add("IdSubLinea", Request.QueryString("IdSubLinea"))
                    ViewState.Add("IdProducto", Request.QueryString("IdProducto"))

                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                    ViewState.Add("FiltrarSaldo", Request.QueryString("FiltrarSaldo"))
                    ViewState.Add("Saldo", Request.QueryString("Saldo"))

                    ViewState.Add("Empresa", "")
                    ViewState.Add("Almacen", "")

                    If CInt(ViewState("IdEmpresa")) > 0 Then ViewState.Add("Empresa", (New Negocio.Persona).getDescripcionPersona(CInt(ViewState("IdEmpresa"))))
                    If CInt(ViewState("IdAlmacen")) > 0 Then ViewState.Add("Almacen", (New Negocio.Almacen).SelectxId(CInt(ViewState("IdAlmacen"))).Nombre)

                Case 2 ' ***************    REPORTE DE CAJA-LIQUIDACION

                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdTienda", Request.QueryString("IdTienda"))
                    ViewState.Add("IdCaja", Request.QueryString("IdCaja"))

                    ViewState.Add("FechaIni", Request.QueryString("FechaIni"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))

                Case 3 ' ***************    REPORTE DE PRODUCTO EN TRANSITO


                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdAlmacen", Request.QueryString("IdAlmacen"))

                    ViewState.Add("IdTipoExistencia", Request.QueryString("IdTipoExistencia"))
                    ViewState.Add("IdLinea", Request.QueryString("IdLinea"))
                    ViewState.Add("IdSubLinea", Request.QueryString("IdSubLinea"))
                    ViewState.Add("IdProducto", Request.QueryString("IdProducto"))

                    If CInt(ViewState("IdAlmacen")) > 0 Then ViewState.Add("Almacen", (New Negocio.Almacen).SelectxId(CInt(ViewState("IdAlmacen"))).Nombre)

                Case 4 ' ***************    REPORTE DE PRODUCTO COMPROMETIDO


                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdAlmacen", Request.QueryString("IdAlmacen"))

                    ViewState.Add("IdTipoExistencia", Request.QueryString("IdTipoExistencia"))
                    ViewState.Add("IdLinea", Request.QueryString("IdLinea"))
                    ViewState.Add("IdSubLinea", Request.QueryString("IdSubLinea"))
                    ViewState.Add("IdProducto", Request.QueryString("IdProducto"))

                Case 5 ' ***************    REPORTE DE PRODUCTO POR ENTREGAR

                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdAlmacen", Request.QueryString("IdAlmacen"))
                    ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
                    ViewState.Add("IdPersona", Request.QueryString("IdPersona"))

                Case 6 ' ***************    REPORTE DE GERENCIAL

                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("Fecha", Request.QueryString("Fecha"))


            End Select

        End If

        verReporte(CInt(ViewState("iReporte")))

    End Sub


    Private Sub verReporte(ByVal Tipo As Integer)

        Try

            Select Case Tipo

                Case 1 ' ***************    REPORTE DE SALDOS


                    DS = (New Negocio.Almacen).getDataSet_AlmacenSaldoxFecha(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdAlmacen")), CInt(ViewState("IdTipoExistencia")), CInt(ViewState("IdLinea")), CInt(ViewState("IdSubLinea")), CInt(ViewState("IdProducto")), CStr(ViewState("FechaFin")), CInt(ViewState("FiltrarSaldo")), CDec(ViewState("Saldo")), obtenerDataTable_TipoTablaValor)

                    REPORTE = New CR_AlmacenSaldoxFecha
                    REPORTE.SetDataSource(DS)
                    REPORTE.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
                    REPORTE.SetParameterValue("@Empresa", (ViewState("Empresa")))
                    REPORTE.SetParameterValue("@Almacen", (ViewState("Almacen")))

                    CRViewer.ReportSource = REPORTE
                    CRViewer.DataBind()

                Case 2 ' ***************    REPORTE DE CAJA-LIQUIDACION

                    DS = (New Negocio.Caja).getDataSet_LiquidacionCaja(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdTienda")), CInt(ViewState("IdCaja")), CStr(ViewState("FechaIni")), CStr(ViewState("FechaFin")))

                    REPORTE = New CR_LiquidacionCaja
                    REPORTE.SetDataSource(DS)
                    REPORTE.SetParameterValue("@FechaIni", (ViewState("FechaIni")))
                    REPORTE.SetParameterValue("@FechaFin", (ViewState("FechaFin")))

                    CRViewer.ReportSource = REPORTE
                    CRViewer.DataBind()


                Case 3 ' ***************   REPORTE DE PRODUCTO EN TRANSITO

                    DS = (New Negocio.Almacen).getDataSet_ProductoEnTransito(CInt(ViewState("IdAlmacen")), CInt(ViewState("IdTipoExistencia")), CInt(ViewState("IdLinea")), CInt(ViewState("IdSubLinea")), CInt(ViewState("IdProducto")))

                    REPORTE = New CR_ProductoEnTransito

                    REPORTE.SetDataSource(DS)
                    REPORTE.SetParameterValue("@Almacen", (ViewState("Almacen")))

                    CRViewer.ReportSource = REPORTE
                    CRViewer.DataBind()

                Case 4 ' ***************    REPORTE DE PRODUCTO COMPROMETIDO

                    DS = (New Negocio.Almacen).getDataSet_ProductoComprometido(CInt(ViewState("IdAlmacen")), CInt(ViewState("IdTipoExistencia")), CInt(ViewState("IdLinea")), CInt(ViewState("IdSubLinea")), CInt(ViewState("IdProducto")))

                    REPORTE = New CR_ProductoComprometido

                    REPORTE.SetDataSource(DS)

                    CRViewer.ReportSource = REPORTE
                    CRViewer.DataBind()

                Case 5 ' ***************    REPORTE DE PRODUCTO POR ENTREGAR

                    DS = (New Negocio.Almacen).getDataSet_ProductoPorEntregar(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdAlmacen")), CInt(ViewState("IdDocumento")), CInt(ViewState("IdPersona")))

                    REPORTE = New CR_ProductoPorEntregar

                    REPORTE.SetDataSource(DS)

                    CRViewer.ReportSource = REPORTE
                    CRViewer.DataBind()


                Case 6 ' ***************    REPORTE DE GERENCIAL
                    DS = (New Negocio.Reportes).getResumidoGlobal(CInt(ViewState("IdEmpresa")), 0, CStr(ViewState("Fecha")))

                    REPORTE = New CR_ReporteGerencial

                    REPORTE.SetDataSource(DS)
                    REPORTE.SetParameterValue("@Fecha", (ViewState("Fecha")))

                    CRViewer.ReportSource = REPORTE
                    CRViewer.DataBind()


            End Select

        Catch ex As Exception
            Response.Write("<script>alert('" + ex.Message + "');</script>")
        End Try

    End Sub

    Private Sub Visor4_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        REPORTE.Close()
        REPORTE.Dispose()
    End Sub



    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim ListaTipoTablaValor As List(Of Entidades.TipoTablaValor)

        Try
            ListaTipoTablaValor = CType(Session.Item("ListaFiltroTablaValor"), List(Of Entidades.TipoTablaValor))

            For i As Integer = 0 To ListaTipoTablaValor.Count - 1
                dt.Rows.Add(ListaTipoTablaValor(i).IdTipoTabla, ListaTipoTablaValor(i).IdTipoTablaValor)
            Next

        Catch ex As Exception

        End Try


        Return dt
    End Function



    

End Class