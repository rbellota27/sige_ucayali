﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="LiquidacionCaja.aspx.vb" Inherits="APPWEB.LiquidacionCaja" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td class="TituloCelda">
                Reporte de Caja-Liquidación
            </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                    <legend><span class="Texto"></span></legend>
                    <table>
                        <tr>
                            <td class="Texto">
                                Empresa:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Tienda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Caja:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboCaja" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                    <legend><span class="Texto"></span></legend>
                    <table>
                        <tr>
                            <td class="Texto">
                                Fecha Inicio:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaIni" onBlur=" return ( valFecha_Blank(this) ); " CssClass="TextBox_Fecha"
                                    runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaIni" runat="server" TargetControlID="txtFechaIni"
                                    Format="dd/MM/yyyy">
                                </cc1:CalendarExtender>
                            </td>
                            <td class="Texto">
                                Fecha Fin:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaFin" onBlur=" return ( valFecha_Blank(this) ); " CssClass="TextBox_Fecha"
                                    runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <cc1:CalendarExtender ID="CalendarExtender_FechaFin" runat="server" TargetControlID="txtFechaFin"
                                    Format="dd/MM/yyyy">
                                </cc1:CalendarExtender>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="imgAceptar" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG" OnClientClick="return(Aceptar(this));" />
            </td>
        </tr>
        <tr>
            <td>
                <iframe id="iReportFrame" name="iReportFrame" width="100%" height="2000px" scrolling="no">
                </iframe>
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">



        function Aceptar(btn) {

            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID %>');
            var IdEmpresa = parseInt(cboEmpresa.value);

            var cboTienda = document.getElementById('<%=cboTienda.ClientID %>');
            var IdTienda = parseInt(cboTienda.value);

            var cboCaja = document.getElementById('<%=cboCaja.ClientID %>');
            var IdCaja = parseInt(cboCaja.value);


            var FechaFin = document.getElementById('<%=txtFechaFin.ClientID %>').value;
            if (!valFecha_Blank(document.getElementById('<%=txtFechaFin.ClientID %>'))) {
                alert('INGRESE UNA FECHA VALIDA.\n NO SE PERMITE LA OPERACIÓN.');
                return false;
            }

            var FechaIni = document.getElementById('<%=txtFechaIni.ClientID %>').value;
            if (!valFecha_Blank(document.getElementById('<%=txtFechaIni.ClientID %>'))) {
                alert('INGRESE UNA FECHA VALIDA.\n NO SE PERMITE LA OPERACIÓN.');
                return false;
            }
            iReportFrame.location.href = 'Visor.aspx?iReport=2&IdEmpresa=' + IdEmpresa + '&IdTienda=' + IdTienda + '&IdCaja=' + IdCaja + '&FechaIni=' + FechaIni + '&FechaFin=' + FechaFin;
            return false;

        }
      
    </script>

</asp:Content>
