﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class Foshan
    Inherits System.Web.UI.Page

    Dim objScript As New ScriptManagerClass
    Dim cbo As Combo
    Dim ListaTipoAlmacen As List(Of Entidades.TipoAlmacen)
    Private Const _TipoAlmacenTienda As Integer = 1


    Private Property S_ListaTipoAlmacen() As List(Of Entidades.TipoAlmacen)
        Get
            Return CType(Session("ListaTipoAlmacen"), List(Of Entidades.TipoAlmacen))
        End Get
        Set(ByVal value As List(Of Entidades.TipoAlmacen))
            Session.Remove("ListaTipoAlmacen")
            Session.Add("ListaTipoAlmacen", value)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        OnLoad_Foshan()
    End Sub

    Private Sub OnLoad_Foshan()
        If Not IsPostBack Then
            Try


                Me.txtFechaInicio1.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
                Me.txtFechaFin1.Text = Me.txtFechaInicio1.Text

                cbo = New Combo
                With cbo

                    .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), True)
                    .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), True)
                    .LlenarCboTipoAlmacen(Me.cboTipoAlmacen, False)

                    .llenarCboTipoExistencia(Me.cboTipoExistencia, True)
                    .llenarCboLineaxTipoExistencia(Me.cboLinea, CInt(Me.cboTipoExistencia.SelectedValue), True)
                    .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), CInt(Me.cboTipoExistencia.SelectedValue), True)


                    .LlenarCboYear(Me.idYearI, False)
                    Me.idYearI.SelectedValue = CStr(Convert.ToDateTime(Me.txtFechaInicio1.Text).Year)
                    .LlenarCboSemanaxIdYear(Me.idSemanaI, CInt(idYearI.SelectedValue), False)
                    .LlenarCboYear(Me.idYearF, False)
                    Me.idYearF.SelectedValue = CStr(Convert.ToDateTime(Me.txtFechaInicio1.Text).Year)
                    .LlenarCboSemanaxIdYear(Me.idSemanaF, CInt(idYearF.SelectedValue), False)

                End With

                llenarcboSemanaActual(Me.idYearI, idSemanaI, Me.txtFechaInicioSemanaI, Me.txtFechaFinSemanaI)
                llenarcboSemanaActual(Me.idYearF, idSemanaF, Me.txtFechaInicioSemanaFin, Me.txtFechaFinSemanafin)



                ' ********* TIPO ALMACEN
                Me.S_ListaTipoAlmacen = New List(Of Entidades.TipoAlmacen)
                Add_TipoAlmacen(_TipoAlmacenTienda, Me.cboTipoAlmacen.SelectedItem.Text)

                S_FiltroTablaValor = New List(Of Entidades.TipoTablaValor)
                S_TipoTablaValor = New List(Of Entidades.TipoTablaValor)

            Catch ex As Exception
                objScript.mostrarMsjAlerta(Me, ex.Message)
            End Try
        End If
    End Sub

    Private Sub idSemanaI_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles idSemanaI.SelectedIndexChanged
        llenarcboSemanaActual(Me.idYearI, idSemanaI, Me.txtFechaInicioSemanaI, Me.txtFechaFinSemanaI)
    End Sub
    Private Sub idYearI_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles idYearI.SelectedIndexChanged
        llenarcboSemanaActual(Me.idYearI, idSemanaI, Me.txtFechaInicioSemanaI, Me.txtFechaFinSemanaI)
    End Sub
    Private Sub idSemanaF_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles idSemanaF.SelectedIndexChanged
        llenarcboSemanaActual(Me.idYearF, idSemanaF, Me.txtFechaInicioSemanaFin, Me.txtFechaFinSemanafin)
    End Sub
    Private Sub idYearF_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles idYearF.SelectedIndexChanged
        llenarcboSemanaActual(Me.idYearF, idSemanaF, Me.txtFechaInicioSemanaFin, Me.txtFechaFinSemanafin)
    End Sub

    Public Sub llenarcboSemanaActual(ByVal cboano As DropDownList, ByVal cboSem As DropDownList, ByVal txtFechaInicio As TextBox, ByVal txtFechafin As TextBox)
        Try
            Dim objProgramacionPedido As Entidades.ProgramacionPedido = (New Negocio.ProgramacionPedido).ProgramacionPedidoSelectxIdYearxIdSemana(CInt(cboano.SelectedValue), CInt(cboSem.SelectedValue))
            If (objProgramacionPedido IsNot Nothing) Then

                txtFechaInicio.Text = Format(objProgramacionPedido.cal_FechaIni, "dd/MM/yyyy")
                txtFechafin.Text = Format(objProgramacionPedido.cal_FechaFin, "dd/MM/yyyy")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnAddTipoAlmacen_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddTipoAlmacen.Click

        Add_TipoAlmacen(_TipoAlmacenTienda, Me.cboTipoAlmacen.SelectedItem.Text)

    End Sub

    Private Sub Add_TipoAlmacen(ByVal IdTipoAlmacen As Integer, ByVal NomTipoAlmacen As String)

        Me.ListaTipoAlmacen = Me.S_ListaTipoAlmacen
        Me.ListaTipoAlmacen.Add(New Entidades.TipoAlmacen(IdTipoAlmacen, NomTipoAlmacen))

        Me.GV_TipoAlmacen.DataSource = Me.S_ListaTipoAlmacen
        Me.GV_TipoAlmacen.DataBind()

        Me.S_ListaTipoAlmacen = Me.ListaTipoAlmacen

    End Sub


    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try
            cbo = New Combo
            With cbo

                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), True)

            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoExistencia.SelectedIndexChanged

        cbo = New Combo
        With cbo

            .llenarCboLineaxTipoExistencia(Me.cboLinea, CInt(Me.cboTipoExistencia.SelectedValue), True)
            .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), CInt(Me.cboTipoExistencia.SelectedValue), True)

        End With

    End Sub

    Private Sub cboLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLinea.SelectedIndexChanged

        cbo = New Combo
        With cbo

            .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), CInt(Me.cboTipoExistencia.SelectedValue), True)

        End With


    End Sub




#Region " **************** Buscar Tabla *** "
    Private ListaTipoTabla As List(Of Entidades.TipoTabla)

    Private Sub ImgBuscarTabla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBuscarTabla.Click
        Buscar_Tabla()
    End Sub
    Private Sub Buscar_Tabla()

        Try

            Me.ViewState.Add("BuscarTabla", Me.txtBuscarTabla.Text.Trim)
            Me.Find_Tabla(0)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub Find_Tabla(ByVal TipoMov As Integer)

        Dim Index As Integer

        Select Case TipoMov
            Case 0
                Index = 0
            Case 1
                Index = (CInt(Me.txtPageIndexTabla.Text) - 1) - 1 ' ******* ANTERIOR
            Case 2
                Index = (CInt(Me.txtPageIndexTabla.Text) - 1) + 1 ' ******* SIGUIENTE
            Case 3
                Index = (CInt(Me.txtPageIndexGoTabla.Text) - 1) - 1 ' ******* IR
        End Select

        Dim lista As List(Of Entidades.TipoTabla) = (New Negocio.TipoTabla).TipoTablaSelectAllxEstado_Paginado(CStr(Me.ViewState("BuscarTabla")), 1, Index, Me.GV_Tabla.PageSize)

        If Not IsNothing(lista) Then

            If lista.Count > 0 Then

                Me.txtPageIndexTabla.Text = CStr(Index + 1)

                Me.GV_Tabla.DataSource = lista
                Me.GV_Tabla.DataBind()
                Me.GV_Tabla.SelectedIndex = -1

            Else
                objScript.mostrarMsjAlerta(Me, "No se hallaron Registros.")
            End If

        End If

        Me.ModalPopup_Atributo.Show()

    End Sub

    Private Sub btnAnteriorTabla_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnteriorTabla.Click
        Find_Tabla(1)
    End Sub

    Private Sub btnSiguienteTabla_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSiguienteTabla.Click
        Find_Tabla(2)
    End Sub

    Private Sub btnIrTabla_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIrTabla.Click
        Find_Tabla(3)
    End Sub


    Private Sub GV_Tabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Tabla.SelectedIndexChanged

        Me.lblNomTabla.Text = HttpUtility.HtmlDecode(CType(Me.GV_Tabla.Rows(Me.GV_Tabla.SelectedRow.RowIndex).FindControl("lblTabla"), Label).Text).Trim

        Me.ModalPopup_Atributo.Show()
        Me.ModalPopup_TablaValor.Show()

    End Sub

#End Region

#Region " **************** Buscar Tabla Valor *** "

    Private ListaTipoTablaValor As List(Of Entidades.TipoTablaValor)
    Private ListaFiltroTablaValor As List(Of Entidades.TipoTablaValor)

    Private Property S_TipoTablaValor() As List(Of Entidades.TipoTablaValor)
        Get
            Return CType(Session.Item("ListaTipoTablaValor1"), List(Of Entidades.TipoTablaValor))
        End Get
        Set(ByVal value As List(Of Entidades.TipoTablaValor))
            Session.Remove("ListaTipoTablaValor1")
            Session.Add("ListaTipoTablaValor1", value)
        End Set
    End Property
    Private Property S_FiltroTablaValor() As List(Of Entidades.TipoTablaValor)
        Get
            Return CType(Session.Item("ListaFiltroTablaValor1"), List(Of Entidades.TipoTablaValor))
        End Get
        Set(ByVal value As List(Of Entidades.TipoTablaValor))
            Session.Remove("ListaFiltroTablaValor1")
            Session.Add("ListaFiltroTablaValor1", value)
        End Set
    End Property


    Private Sub ImgBuscarTablaValor_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBuscarTablaValor.Click
        Buscar_TablaValor()
    End Sub
    Private Sub Buscar_TablaValor()

        Dim Index As Integer = Me.GV_Tabla.SelectedRow.RowIndex

        If Index < 0 Then
            objScript.mostrarMsjAlerta(Me, "Seleccione un tipo de tabla")
            ModalPopup_Atributo.Show()
            Exit Sub

        End If


        Me.ViewState.Add("BuscarTablaValor", Me.txtBuscarTablaValor.Text.Trim)
        Me.ViewState.Add("IdTipoTabla", CType(Me.GV_Tabla.Rows(Index).FindControl("hddIdTipoTabla"), HiddenField).Value)
        Find_TablaValor(0)

    End Sub
    Private Sub Find_TablaValor(ByVal TipoMov As Integer)
        Try
            Dim Index As Integer

            Select Case TipoMov
                Case 0
                    Index = 0
                Case 1
                    Index = (CInt(Me.txtPageIndexTablaValor.Text) - 1) - 1 ' ******* ANTERIOR
                Case 2
                    Index = (CInt(Me.txtPageIndexTablaValor.Text) - 1) + 1 ' ******* SIGUIENTE
                Case 3
                    Index = (CInt(Me.txtPageIndexGoTablaValor.Text) - 1) - 1 ' ******* IR
            End Select

            ListaTipoTablaValor = (New Negocio.TipoTablaValor).TipoTablaValorSelectAllxIdTipoTablaxEstado_Paginado(CInt(Me.ViewState("IdTipoTabla")), CStr(Me.ViewState("BuscarTablaValor")), 1, Index, Me.GV_TablaValor.PageSize)

            If Not IsNothing(ListaTipoTablaValor) Then

                If ListaTipoTablaValor.Count > 0 Then

                    S_TipoTablaValor = ListaTipoTablaValor

                    Me.txtPageIndexTablaValor.Text = CStr(Index + 1)

                    Me.GV_TablaValor.DataSource = ListaTipoTablaValor
                    Me.GV_TablaValor.DataBind()


                Else
                    objScript.mostrarMsjAlerta(Me, "No se hallaron Registros.")
                End If

            End If

            Me.ModalPopup_Atributo.Show()
            Me.ModalPopup_TablaValor.Show()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnAnteriorTablaValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnteriorTablaValor.Click
        Find_TablaValor(1)
    End Sub

    Private Sub btnSiguienteTablaValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSiguienteTablaValor.Click
        Find_TablaValor(2)
    End Sub

    Private Sub btnIrTablaValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIrTablaValor.Click
        Find_TablaValor(3)
    End Sub

    Private Sub ImgAgregarTablaValor_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgAgregarTablaValor.Click
        Agregar_TablaValor()
    End Sub
    Private Sub Agregar_TablaValor()
        Try
            ListaFiltroTablaValor = S_FiltroTablaValor
            ListaTipoTablaValor = S_TipoTablaValor

            Dim Lista As New List(Of Entidades.TipoTablaValor)

            For i As Integer = 0 To Me.GV_TablaValor.Rows.Count - 1
                If CType(Me.GV_TablaValor.Rows(i).FindControl("ckTablaValor"), CheckBox).Checked Then

                    ListaFiltroTablaValor.Add(ListaTipoTablaValor(i))

                End If
            Next

            Me.S_FiltroTablaValor = ListaFiltroTablaValor
            Me.GV_FiltroTipoTabla.DataSource = ListaFiltroTablaValor
            Me.GV_FiltroTipoTabla.DataBind()

            Me.ModalPopup_Atributo.Show()
            Me.ModalPopup_TablaValor.Show()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged

        Me.ListaFiltroTablaValor = Me.S_FiltroTablaValor
        Me.ListaFiltroTablaValor.RemoveAt(Me.GV_FiltroTipoTabla.SelectedRow.RowIndex)
        Me.S_FiltroTablaValor = ListaFiltroTablaValor

        Me.GV_FiltroTipoTabla.DataSource = ListaFiltroTablaValor
        Me.GV_FiltroTipoTabla.DataBind()

    End Sub

#End Region


End Class