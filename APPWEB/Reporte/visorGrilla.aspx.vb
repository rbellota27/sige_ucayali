﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.IO

Partial Public Class visorGrilla
    Inherits System.Web.UI.Page

    Dim DS As DataSet
    Dim ObjScript As New ScriptManagerClass



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        OnLoad_visorGrilla()
    End Sub

    Private Sub OnLoad_visorGrilla()
        If Not IsPostBack Then

            ViewState.Add("iReport", Me.Request.QueryString("iReport"))

            Select Case CInt(ViewState("iReport"))

                Case 1 ' ********************* COMERCIALIZACION -FOSHAN02

                    ViewState.Add("IdEmpresa", Me.Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdTienda", Me.Request.QueryString("IdTienda"))

                    ViewState.Add("IdTipoExistencia", Me.Request.QueryString("IdTipoExistencia"))
                    ViewState.Add("IdLinea", Me.Request.QueryString("IdLinea"))
                    ViewState.Add("IdSubLinea", Me.Request.QueryString("IdSubLinea"))


                    ViewState.Add("FechaIni", Me.Request.QueryString("FechaIni"))
                    ViewState.Add("FechaFin", Me.Request.QueryString("FechaFin"))

                    ViewState.Add("YearIni", Me.Request.QueryString("YearIni"))
                    ViewState.Add("SemanaIni", Me.Request.QueryString("SemanaIni"))
                    ViewState.Add("YearFin", Me.Request.QueryString("YearFin"))
                    ViewState.Add("SemanaFin", Me.Request.QueryString("SemanaFin"))

                    ViewState.Add("FiltrarSemana", Me.Request.QueryString("FiltrarSemana"))

                Case 2 ' ********************* COMERCIALIZACION POR SEMANA

                    ViewState.Add("IdEmpresa", Me.Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdTienda", Me.Request.QueryString("IdTienda"))
                    ViewState.Add("IdAlmacen", Me.Request.QueryString("IdAlmacen"))

                    ViewState.Add("IdTipoExistencia", Me.Request.QueryString("IdTipoExistencia"))
                    ViewState.Add("IdLinea", Me.Request.QueryString("IdLinea"))
                    ViewState.Add("IdSubLinea", Me.Request.QueryString("IdSubLinea"))
                    ViewState.Add("IdProducto", Me.Request.QueryString("IdProducto"))

                    ViewState.Add("YearIni", Me.Request.QueryString("YearIni"))
                    ViewState.Add("SemanaIni", Me.Request.QueryString("SemanaIni"))
                    ViewState.Add("YearFin", Me.Request.QueryString("YearFin"))
                    ViewState.Add("SemanaFin", Me.Request.QueryString("SemanaFin"))

                    ViewState.Add("IdTiendaPrecio", Me.Request.QueryString("IdTiendaPrecio"))
                    ViewState.Add("IdDocumento", Me.Request.QueryString("IdDocumento"))

                Case 3 ' ********************* COMERCIALIZACION POR MES

                    ViewState.Add("IdEmpresa", Me.Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdTienda", Me.Request.QueryString("IdTienda"))
                    ViewState.Add("IdAlmacen", Me.Request.QueryString("IdAlmacen"))

                    ViewState.Add("IdTipoExistencia", Me.Request.QueryString("IdTipoExistencia"))
                    ViewState.Add("IdLinea", Me.Request.QueryString("IdLinea"))
                    ViewState.Add("IdSubLinea", Me.Request.QueryString("IdSubLinea"))
                    ViewState.Add("IdProducto", Me.Request.QueryString("IdProducto"))

                    ViewState.Add("FechaIni", Me.Request.QueryString("FechaIni"))
                    ViewState.Add("FechaFin", Me.Request.QueryString("FechaFin"))

                    ViewState.Add("IdTiendaPrecio", Me.Request.QueryString("IdTiendaPrecio"))
                    ViewState.Add("IdDocumento", Me.Request.QueryString("IdDocumento"))

            End Select



        End If
        verReporte()
    End Sub

    Private Sub verReporte()

        Select Case CInt(ViewState("iReport"))

            Case 1 ' ********************* COMERCIALIZACION -FOSHAN02

                DS = (New Negocio.Almacen).getDataSet_CR_Comercializacion_Foshan02(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdTienda")), CInt(ViewState("IdTipoExistencia")), CInt(ViewState("IdLinea")), CInt(ViewState("IdSubLinea")), CStr(ViewState("FechaIni")), CStr(ViewState("FechaFin")), CInt(ViewState("YearIni")), CInt(ViewState("SemanaIni")), CInt(ViewState("YearFin")), CInt(ViewState("SemanaFin")), CInt(ViewState("FiltrarSemana")), obtenerDataTable_TipoTablaValor("ListaFiltroTablaValor1"), obtenerDataTable_TipoAlmacen("ListaTipoAlmacen"))

                If DS IsNot Nothing Then

                    Me.lblTitulo.Text = "REPORTE DE COMERCIALIZACION FOSHAN02"

                    Me.GV_Exportar.DataSource = DS.Tables(0)
                    Me.GV_Exportar.DataBind()

                    Dim Nombre As String
                    Nombre = "FOSHAN02.xls"

                    Me.ExportaExcel(Me.GV_Exportar, Nombre)

                Else

                    Response.Write("<script>alert('No se hallaron registros a exportar.');</script>")

                End If

            Case 2 ' ********************* COMERCIALIZACION POR SEMANAS

                DS = (New Negocio.Almacen).getDataSet_ComercializacionPorSemana(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdTienda")), CInt(ViewState("IdAlmacen")), CInt(ViewState("IdTipoExistencia")), CInt(ViewState("IdLinea")), CInt(ViewState("IdSubLinea")), CInt(ViewState("IdProducto")), CInt(ViewState("YearIni")), CInt(ViewState("SemanaIni")), CInt(ViewState("YearFin")), CInt(ViewState("SemanaFin")), obtenerDataTable_TipoAlmacen("ListaTipoAlmacen1"), CInt(ViewState("IdDocumento")), CInt(ViewState("IdTiendaPrecio")), obtenerDataTable_TipoTablaValor("ListaFiltroTablaValor2"))

                If DS IsNot Nothing Then

                    Me.lblTitulo.Text = "COMERCIALIZACION POR SEMANAS"

                    Me.GV_Exportar.DataSource = DS.Tables(0)
                    Me.GV_Exportar.DataBind()

                    Dim Nombre As String
                    Nombre = "ComercializacionS.xls"

                    Me.ExportaExcel(Me.GV_Exportar, Nombre)

                Else
                    Response.Write("<script>alert('No se hallaron registros a exportar.');</script>")
                End If

            Case 3 ' ********************* COMERCIALIZACION POR MES

                DS = (New Negocio.Almacen).getDataSet_ComercializacionPorMes(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdTienda")), CInt(ViewState("IdAlmacen")), CInt(ViewState("IdTipoExistencia")), CInt(ViewState("IdLinea")), CInt(ViewState("IdSubLinea")), CInt(ViewState("IdProducto")), CStr(ViewState("FechaIni")), CStr(ViewState("FechaFin")), obtenerDataTable_TipoAlmacen("ListaTipoAlmacen2"), CInt(ViewState("IdDocumento")), CInt(ViewState("IdTiendaPrecio")), obtenerDataTable_TipoTablaValor("ListaFiltroTablaValor3"))

                If DS IsNot Nothing Then

                    Me.lblTitulo.Text = "COMERCIALIZACION POR MES"

                    Me.GV_Exportar.DataSource = DS.Tables(0)
                    Me.GV_Exportar.DataBind()

                    Dim Nombre As String
                    Nombre = "ComercializacionM.xls"

                    'Me.ExportaExcel(Me.GV_Exportar, Nombre)


                    'nuevo código
                    Dim dg As DataGrid = New DataGrid()
                    dg.DataSource = DS.Tables(0)
                    dg.DataBind()

                    Response.Clear()
                    Response.Buffer = True
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("Content-Disposition", "attachment; filename=Consolidated" + Nombre)
                    Response.Charset = ""
                    Me.EnableViewState = False

                    Dim oStringWriter As StringWriter = New StringWriter()
                    Dim oHtmlTextWriter As HtmlTextWriter = New HtmlTextWriter(oStringWriter)
                    dg.RenderControl(oHtmlTextWriter)
                    Response.Write(oStringWriter.ToString())
                    Response.End()





                Else

                    Response.Write("<script>alert('No se hallaron registros a exportar.');</script>")
                End If

        End Select

    End Sub

    Public Sub ExportaExcel(ByVal grilla As GridView, ByVal FilleNameExt As String)




        Dim sb As StringBuilder = New StringBuilder()
        Dim sw As StringWriter = New StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim pagina As Page = New Page
        Dim form As New HtmlForm

        grilla.EnableViewState = False
        'lblTitulo.EnableViewState = False
        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()
        pagina.Controls.Add(form)
        form.Controls.Add(grilla)
        form.Controls.Add(lblTitulo)
        pagina.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("content-disposition", "attachment;filename=" + FilleNameExt)
        Response.Charset = "UTF-8"

        Response.ContentEncoding = Encoding.Default
        Response.Write(sb.ToString())
        Response.End()

    End Sub


    Private Function obtenerDataTable_TipoTablaValor(ByVal Nombre As String) As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim ListaTipoTablaValor As List(Of Entidades.TipoTablaValor)

        Try
            ListaTipoTablaValor = CType(Session.Item(Nombre), List(Of Entidades.TipoTablaValor))

            For i As Integer = 0 To ListaTipoTablaValor.Count - 1
                dt.Rows.Add(ListaTipoTablaValor(i).IdTipoTabla, ListaTipoTablaValor(i).IdTipoTablaValor)
            Next

        Catch ex As Exception

        End Try


        Return dt
    End Function


    Private Function obtenerDataTable_TipoAlmacen(ByVal Nombre As String) As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")

        Dim ListaTipoAlmacen As List(Of Entidades.TipoAlmacen)

        Try
            ListaTipoAlmacen = CType(Session.Item(Nombre), List(Of Entidades.TipoAlmacen))

            For i As Integer = 0 To ListaTipoAlmacen.Count - 1
                dt.Rows.Add(ListaTipoAlmacen(i).IdTipoAlmacen)
            Next

        Catch ex As Exception

        End Try

        Return dt

    End Function


End Class