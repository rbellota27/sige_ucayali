﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class ProductoComprometido
    Inherits System.Web.UI.Page


    Dim objScript As New ScriptManagerClass
    Dim cbo As Combo


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            onLoad_ProductoComprometido()
        End If

    End Sub

    Private Sub onLoad_ProductoComprometido()

        Try
            cbo = New Combo
            With cbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTipoAlmacen(Me.cboTipoAlmacen, False)
                .LlenarCboAlmacenxIdTipoAlmacenxIdEmpresa(Me.cboAlmacen, CInt(Me.cboTipoAlmacen.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)

                .llenarCboTipoExistencia(Me.cboTipoExistencia, True)
                .llenarCboLineaxTipoExistencia(Me.cboLinea, CInt(Me.cboTipoExistencia.SelectedValue), True)
                .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), CInt(Me.cboTipoExistencia.SelectedValue), True)

            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        cbo = New Combo
        With cbo
            .LlenarCboAlmacenxIdTipoAlmacenxIdEmpresa(Me.cboAlmacen, CInt(Me.cboTipoAlmacen.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), True)
        End With
    End Sub

    Private Sub cboTipoAlmacen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoAlmacen.SelectedIndexChanged
        cbo = New Combo
        With cbo
            .LlenarCboAlmacenxIdTipoAlmacenxIdEmpresa(Me.cboAlmacen, CInt(Me.cboTipoAlmacen.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), True)
        End With
    End Sub

    Private Sub cboLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLinea.SelectedIndexChanged
        Try
            cbo = New Combo
            With cbo

                .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), CInt(Me.cboTipoExistencia.SelectedValue), True)

            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoExistencia.SelectedIndexChanged
        Try
            cbo = New Combo
            With cbo

                .llenarCboLineaxTipoExistencia(Me.cboLinea, CInt(Me.cboTipoExistencia.SelectedValue), True)
                .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), CInt(Me.cboTipoExistencia.SelectedValue), True)

            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

End Class