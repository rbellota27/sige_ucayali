﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.
Imports Telerik.Web.UI
Partial Public Class FrmPrincipal_Inicio
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            inicializarFrm()
        End If

    End Sub


    Private Sub inicializarFrm()

        Try

            Dim idusuario As Integer
            idusuario = CType(Session("IdUsuario"), Integer)
            If idusuario <> Nothing Then

                lblUsuario.Text = (New Negocio.Persona).getDescripcionPersona(idusuario)
                'lblFechaIngreso.Text = Format((New Negocio.FechaActual).SelectFechaActual)

                Lb_Tienda.Text = (New Negocio.EmpresaTiendaUsuario).fn_GetTiendaPrincipal(idusuario).ToString.Trim


                CargaItemMenu(Me.RadMenu1, idusuario)

                Session.Add("MenuDinamico", RadMenu1)

                cargarTipoCambio(0, 2) '******* DOLARES
                cargarTipoCambio(1, 2) '******* DOLARES

                If RadMenu1.Items.Count < 1 Then
                    Dim menuItems As New RadMenuItem
                    menuItems.Text = "Sige"
                    menuItems.Value = "1"
                    menuItems.NavigateUrl = ""
                    menuItems.Target = "frame_Content"
                    menuItems.Selected = True
                    RadMenu1.Items.Add(menuItems)
                End If
            Else
                Throw New Exception("No se posee una variable < IdUsuario > en sesión. Cierre el Sistema y vuelva a ingresar.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarTipoCambio(ByVal tipo As Integer, ByVal IdMoneda As Integer) '*** INTERNO

        '****** 0: Oficial
        '****** 1: Interno

        '*** OFICIAL / INTERNO
        Dim listaParametros() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {9, 10})

        Dim objTipoCambio As Entidades.TipoCambio = (New Negocio.TipoCambio).SelectVigentexIdMoneda(IdMoneda)

        Select Case tipo
            Case 0  '************ OFICIAL

                Select Case listaParametros(0)
                    Case 0 '** NO VISUALIZAR
                        Me.lblTC_Sunat.Text = "-----"
                    Case 1 '*** VENTA
                        Me.lblTC_Sunat.Text = CStr(Math.Round(objTipoCambio.VentaOf, 3))

                    Case 2 '*** COMPRA
                        Me.lblTC_Sunat.Text = CStr(Math.Round(objTipoCambio.CompraOf, 3))

                End Select



            Case 1  '*************** INTERNO

                Select Case listaParametros(0)
                    Case 0 '** NO VISUALIZAR
                        Me.lblTC_Indusferr.Text = "-----"
                    Case 1 '*** VENTA
                        Me.lblTC_Indusferr.Text = CStr(Math.Round(objTipoCambio.VentaC, 3))

                    Case 2 '*** COMPRA
                        Me.lblTC_Indusferr.Text = CStr(Math.Round(objTipoCambio.CompraC, 3))

                End Select

        End Select

    End Sub



    Public Sub CargaItemMenu(ByVal ctrlmenu As RadMenu, ByVal idUsuario As Integer)


        Dim ListaMenuItems As New List(Of Entidades.Opcion)
        ListaMenuItems = (New Negocio.opcion).SelectOpcionxUsuario(idUsuario)
        For Each objopcion As Entidades.Opcion In ListaMenuItems

            If objopcion.Id.Equals(objopcion.IdMenu) Then
                Dim mnuMenuItem As New RadMenuItem
                mnuMenuItem.Value = objopcion.Id.ToString
                mnuMenuItem.Text = objopcion.Nombre.ToString
                mnuMenuItem.NavigateUrl = objopcion.Formulario.ToString
                mnuMenuItem.Target = "frame_Content"

                If (mnuMenuItem.NavigateUrl.Trim.Length > 0) Then
                    mnuMenuItem.Selected = True
                Else
                    mnuMenuItem.Selected = False
                End If


                ctrlmenu.Items.Add(mnuMenuItem)
                AddMenuItem(mnuMenuItem, ListaMenuItems, ctrlmenu)
            End If

        Next

    End Sub

    Private Sub AddMenuItem(ByRef mnuMenuItem As RadMenuItem, ByVal listaOpciones As List(Of Entidades.Opcion), ByRef ctrlmenu As RadMenu)

        For Each objopcion As Entidades.Opcion In listaOpciones
            If objopcion.IdMenu.ToString.Equals(mnuMenuItem.Value) AndAlso _
            Not objopcion.Id.Equals(objopcion.IdMenu) Then
                Dim mnuNewMenuItem As New RadMenuItem
                mnuNewMenuItem.Value = objopcion.Id.ToString
                mnuNewMenuItem.Text = objopcion.Nombre.ToString
                mnuNewMenuItem.NavigateUrl = objopcion.Formulario.ToString
                If objopcion.IdMenu = 1 Then
                    Dim menuSeparador As New RadMenuItem
                    menuSeparador.IsSeparator = True
                    ctrlmenu.Items.Add(menuSeparador)
                    ctrlmenu.Items.Add(mnuNewMenuItem)
                Else
                    mnuNewMenuItem.ImageUrl = objopcion.dir_imagen ' "Imagenes/carro.png"
                    mnuMenuItem.Items.Add(mnuNewMenuItem)
                End If
                mnuNewMenuItem.Target = "frame_Content"

                If (mnuNewMenuItem.NavigateUrl.Trim.Length > 0) Then
                    mnuNewMenuItem.Selected = True
                Else
                    mnuNewMenuItem.Selected = False
                End If

                AddMenuItem(mnuNewMenuItem, listaOpciones, ctrlmenu)
            End If
        Next

    End Sub



    Private Sub mostrarOpcion()

        Dim cad As String = ""
        cad = cad + "<script language=""javascript"" type=""text/javascript"">"
        cad = cad + "top.window.document.frame_Content.location.href = '" + Me.RadMenu1.SelectedItem.NavigateUrl + "'"
        cad = cad + "</script>"
        Response.Write(cad)

    End Sub

    'Protected Sub menu_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles menu.MenuItemClick
    '    mostrarOpcion()
    'End Sub

    Protected Sub btnCerrarSesion_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCerrarSesion.Click

        cerrarSession()

    End Sub

    Private Sub cerrarSession()
        Try : Session.Clear() : Catch ex As Exception : End Try
        Try : Session.Abandon() : Catch ex As Exception : End Try
        Try : FormsAuthentication.SignOut() : Catch ex As Exception : End Try
        Try : FormsAuthentication.RedirectToLoginPage() : Catch ex As Exception : End Try
    End Sub


    Private Sub xbtnProducto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles xbtnProducto.Click

        Dim url As String = Page.ResolveClientUrl("~/Finanzas/FrmPage_Load.aspx?Tipo=3")

        Dim Script As String = "window.open('" + url + "', '_blank', 'height=700,width=900,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );"

        ScriptManager.RegisterStartupScript(Page, GetType(Page), "Redirect", Script, True)
    End Sub

    Private Sub xbtnProductoSIGE_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles xbtnProductoSIGE.Click
        Dim url As String = Page.ResolveClientUrl("~/Finanzas/FrmPage_Load.aspx?Tipo=4")

        Dim Script As String = "window.open('" + url + "', '_blank', 'height=700,width=900,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );"

        ScriptManager.RegisterStartupScript(Page, GetType(Page), "Redirect", Script, True)
    End Sub
End Class