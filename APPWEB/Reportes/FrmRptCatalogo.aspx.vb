﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmRptCatalogo
    Inherits System.Web.UI.Page
    Private objNegUsu As New Negocio.UsuarioView
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim FunCombo As New Combo
        If Not Page.IsPostBack Then
            'FunCombo.LLenarCboTienda(Me.cboTienda)

            If objNegUsu.FunVerTodasTiendas(CInt(Session("IdUsuario")), 9) > 0 Then
                FunCombo.LlenarCboTiendaxIdUsuario(Me.cboTienda, CInt(Session("IdUsuario")), True)
            Else
                FunCombo.LlenarCboTiendaxIdUsuario(Me.cboTienda, CInt(Session("IdUsuario")), False)
            End If
            FunCombo.LLenarCboSubLinea(Me.cboSubLinea)
            Session("IdTienda") = Me.cboTienda.SelectedValue
            Session("IdSubLinea") = Me.cboSubLinea.SelectedValue
        End If
    End Sub
    Protected Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboTienda.SelectedIndexChanged
        Session("IdTienda") = Me.cboTienda.SelectedValue
    End Sub
    Protected Sub cboSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboSubLinea.SelectedIndexChanged
        Session("IdSubLinea") = Me.cboSubLinea.SelectedValue
    End Sub
    'Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
    '    Me.Page.RegisterStartupScript("frame1", "<script languaje=javascript>parent.frame1.location='visor.aspx';</script>")
    'End Sub
End Class