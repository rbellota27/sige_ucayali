﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmCatalogoProductos
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFrm()
        End If
    End Sub
    Private Sub inicializarFrm()
        Try
            Dim objCombo As New Combo
            objCombo.LlenarCboLinea(Me.cmbLinea, True)
            objCombo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea, CInt(Me.cmbLinea.SelectedValue), True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub

    Private Sub cmbLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea.SelectedIndexChanged
        Try
            Dim objCombo As New Combo
            objCombo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea, CInt(Me.cmbLinea.SelectedValue), True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub
End Class