﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPruValorizado.aspx.vb" Inherits="APPWEB.frmPruValorizado" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<asp:scriptmanager runat="server"></asp:scriptmanager>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Estilos/Controles.css" rel="stylesheet" type="text/css" />
</head>
<body style="background-color:White;height:500px;overflow: hidden">
    <form id="form1" runat="server">
   <div style="width:100%;height:100%;margin-top:15px;overflow:hidden">
    <div style="width:20%;float:left;margin-right:10px;border:1px solid blue">
        <div align="center" class="TituloCelda">
            <span style="font-weight:bold">Marcar Reporte a Visualizar</span>
        </div>
        <div style="width:100%;float:left;margin-top:5px">
            <asp:Panel ID="pnlFiltros" runat="server">
            <asp:RadioButton ID="rbTopValorizadoCajas" runat="server" Text="Top Valorizado por Cajas" Checked="true" />
        </asp:Panel>
        </div>        
    </div>
    <div style="width:25%;float:left;border:1px solid blue;margin-right:5px">
        <div style="float:left;width:100%">
            <table style="width:100%">
                <tr>
                    <td style="width:40%" align="right"  class="Texto">
                        Tipo Existencia:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlTipoExistencia" runat="server" Width="100%" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="width:40%" align="right"  class="Texto">
                        Linea:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlLinea" runat="server" Width="100%" AutoPostBack="true">
                        </asp:DropDownList>                       
                    </td>
                </tr>
                <tr>
                    <td style="width:40%" align="right"  class="Texto">
                        Sub Linea:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlSubLinea" runat="server" Width="100%">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="width:40%" align="right"  class="Texto">
                        Pais:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlPais" runat="server" Width="100%" DataTextField="ttv_Nombre" 
                            DataValueField="IdTipoTablaValor">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="width:40%" align="right" class="Texto">
                        Tipo Cliente:
                    </td>
                    <td>
                        <asp:CheckBoxList ID="CheckBoxList1" runat="server" Width="100%" Font-Size="Smaller" BackColor="AliceBlue" 
                            DataSourceID="sqlddlTipoCliente" DataTextField="rol_Nombre"
                            DataValueField="IdRol" >
                        </asp:CheckBoxList>
                        <asp:SqlDataSource ID="sqlddlTipoCliente" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:Conexion %>" 
                            SelectCommand="select IdRol, rol_Nombre from Rol where flagventa = 1"></asp:SqlDataSource>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div style="width:25%;float:left;border:1px solid blue">
        <div style="width:100%">
            <table style="width:100%">
                <tr>
                    <td style="width:20%" align="right" class="Texto">
                        Tienda:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlTienda" runat="server" Width="100%">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table style="width:100%">
                            <tr>
                                <td style="width:20%" class="Texto">
                                    Fecha Inicio:
                                </td>
                                <td style="width:20%">
                                    <asp:TextBox ID="txtFecInicio" runat="server" Width="80px"></asp:TextBox>                                    
                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFecInicio" Format="dd/MM/yyyy" />
                                </td>
                                <td style="width:20%" class="Texto">
                                    Fecha Fin:
                                </td>
                                <td style="width:40%">
                                    <asp:TextBox ID="txtFecFinal" runat="server" Width="80px"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtFecFinal" Format="dd/MM/yyyy" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width:20%" align="right" class="Texto">
                        TOP :
                    </td>
                    <td>
                        <asp:TextBox ID="txtTop" runat="server" Width="40%" Text="10"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Button ID="btnMostrar" runat="server" Text="Mostrar" UseSubmitBehavior="false" OnClientClick="this.disabled=true;this.value='Procesando...'" />
                        <asp:Button ID="btnExportar" runat="server" Text="Exportar"/>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
    <div style="width:1400px;margin-top:20px;height:450px" align="center">
           <%-- <asp:GridView ID="gvDatos" runat="server" Width="100%" Visible="false">
                <HeaderStyle CssClass="GrillaHeader" />
                <RowStyle CssClass="GrillaRow" />
            </asp:GridView>--%>
            <asp:Label ID="lblmensaje" runat="server" Font-Bold="true" ForeColor="Blue" Font-Size="Large"></asp:Label>
    </div>
    </form>
</body>
</html>
