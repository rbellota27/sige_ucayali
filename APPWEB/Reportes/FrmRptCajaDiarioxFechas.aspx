<%@ Page Title="Movimiento de Caja" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmRptCajaDiarioxFechas.aspx.vb" Inherits="APPWEB.FrmRptCajaDiarioxFechas" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td class="TituloCelda">
                Reporte de Caja - Liquidacion
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <table width="100%" cellspacing="0">
                            <tr>
                                <td class="Texto">
                                    Empresa:
                                </td>
                                <td style="width: 40%">
                                    <asp:DropDownList ID="cmbEmpresa" runat="server" AutoPostBack="false" Width="95%">
                                    </asp:DropDownList>
                                </td>
                                <td class="Texto">
                                    Documento:
                                </td>
                                <td style="width: 40%">
                                    <asp:DropDownList ID="cboTipoDocumento" runat="server" AutoPostBack="false" Width="95%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="Texto">
                                    Tienda:
                                </td>
                                <td>
                                    <asp:DropDownList ID="cmbTienda" runat="server" AutoPostBack="true" Width="95%">
                                    </asp:DropDownList>
                                </td>
                                <td class="Texto">
                                    Caja:
                                </td>
                                <td>
                                    <asp:DropDownList ID="cmbCaja" runat="server" AutoPostBack="false" Width="95%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="Texto">
                                    Medio Pago:
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="cmbMedioPago" runat="server" AutoPostBack="false">
                                    </asp:DropDownList>
                                    <asp:CheckBox ID="chkConCaja" Text="Con Caja" runat="server" CssClass="Texto" Checked="true" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td rowspan="2">
                            <asp:Panel ID="Panel_Fechas" runat="server">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="Texto">
                                            Fecha Inicio:
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtFechaInicio" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" TargetControlID="txtFechaInicio">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td class="Texto">
                                            Fecha Fin:
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtFechaFin" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" TargetControlID="txtFechaFin">
                                            </cc1:CalendarExtender>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                            <asp:ImageButton ID="btnMostrarReporte" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                                onmouseout="this.src='../Imagenes/Aceptar_B.JPG';" onmouseover="this.src='../Imagenes/Aceptar_A.JPG';"
                                OnClientClick="return(mostrarReporte());" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1130px">
                </iframe>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddFechaActual" runat="server" />
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">
        function mostrarReporte() {
            var IdEmpresa = document.getElementById('<%= cmbEmpresa.ClientID%>').value;
            var IdTienda = document.getElementById('<%= cmbTienda.ClientID%>').value;
            var IdtipoDoc = document.getElementById('<%= cboTipoDocumento.ClientID%>').value;
            var IdCaja = document.getElementById('<%= cmbCaja.ClientID%>').value;
            var IdMedioPago = document.getElementById('<%= cmbMedioPago.ClientID%>').value;
            var check = document.getElementById('<%= chkConCaja.ClientID%>');
            var concaja;
            if (check.status == true) {
                concaja = true;
            } else {
                concaja = false;
            }

            var fechaInicio;
            var fechaFin;
            if (document.getElementById('<%= txtFechaInicio.ClientID%>') == null) {
                fechaInicio = document.getElementById('<%= hddFechaActual.ClientID%>').value
                fechaFin = document.getElementById('<%= hddFechaActual.ClientID%>').value
            } else {
                fechaInicio = document.getElementById('<%= txtFechaInicio.ClientID%>').value;
                fechaFin = document.getElementById('<%= txtFechaFin.ClientID%>').value;
            }
            frame1.location.href = 'visor.aspx?iReporte=11&FechaInicio=' + fechaInicio + '&FechaFin=' + fechaFin + '&IdEmpresa=' + IdEmpresa + '&IdTienda=' + IdTienda + '&tipodoc=' + IdtipoDoc + '&IdCaja=' + IdCaja + '&IdMedioPago=' + IdMedioPago + '&IdTipoMovimiento=1' + '&caja=' + concaja;

            return false;
        }
    </script>

</asp:Content>
