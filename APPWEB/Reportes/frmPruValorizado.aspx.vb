﻿Imports Entidades
Imports Negocio
Imports System.IO
Public Class frmPruValorizado
    Inherits System.Web.UI.Page
    Dim clsnegocio As New LNValorizadoCajas

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CargarTienda()
            Dim fecha As New DateTime(Date.Now.Year, Date.Now.Month, 1)
            Me.txtFecInicio.Text = fecha
            fecha = New DateTime(Date.Now.Year, Date.Now.Month, DateTime.DaysInMonth(Date.Now.Year, Date.Now.Month))
            Me.txtFecFinal.Text = fecha
            CargarTipoExistencia()
            Cargarpais()
            ddlLinea.Items.Insert(0, New ListItem("---", "-1"))
            ddlSubLinea.Items.Insert(0, New ListItem("---", "-1"))
        End If
    End Sub

    Private Sub CargarTienda()
        Dim cbo As New Combo
        With cbo            
            .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.ddlTienda, 1, CInt(Session("IdUsuario")), False)

        End With
    End Sub

    Private Sub Cargarpais()
        Dim dt As New DataTable
        dt = clsnegocio.LN_ReturnDataTable(Me.ddlPais.SelectedValue.ToString(), "PAIS")
        ddlPais.DataValueField = "IdTipoTablaValor"
        ddlPais.DataTextField = "ttv_Nombre"
        ddlPais.DataSource = dt
        ddlPais.DataBind()
        ddlPais.Items.Insert(0, New ListItem("---", "-1"))
    End Sub
    Private Sub CargarTipoExistencia()
        Dim dt As New DataTable
        dt = clsnegocio.LN_ReturnDataTable(Me.ddlLinea.SelectedValue.ToString(), "EXISTENCIA")
        ddlTipoExistencia.DataValueField = "IdTipoExistencia"
        ddlTipoExistencia.DataTextField = "tex_Nombre"
        ddlTipoExistencia.DataSource = dt
        ddlTipoExistencia.DataBind()
        ddlTipoExistencia.Items.Insert(0, New ListItem("---", "-1"))
    End Sub
    Protected Sub ddlTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTipoExistencia.SelectedIndexChanged
        Dim dt As New DataTable
        dt = clsnegocio.LN_ReturnDataTable(Me.ddlTipoExistencia.SelectedValue.ToString(), "LINEA")
        ddlLinea.DataValueField = "IdLinea"
        ddlLinea.DataTextField = "lin_Nombre"
        ddlLinea.DataSource = dt
        ddlLinea.DataBind()
        ddlLinea.Items.Insert(0, New ListItem("---", "-1"))
    End Sub

    Protected Sub ddlLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlLinea.SelectedIndexChanged
        Dim dt As New DataTable
        dt = clsnegocio.LN_ReturnDataTable(Me.ddlLinea.SelectedValue.ToString(), "SUBLINEA")
        ddlSubLinea.DataValueField = "IdSubLInea"
        ddlSubLinea.DataTextField = "sl_Nombre"
        ddlSubLinea.DataSource = dt
        ddlSubLinea.DataBind()
        ddlSubLinea.Items.Insert(0, New ListItem("---", "-1"))
    End Sub

    Protected Sub btnMostrar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMostrar.Click
        Dim optFiltro As Integer = 0
        Dim existencia As Integer = 0
        Dim opcion As Integer = 0
        Dim linea As Integer = 0
        Dim sublinea As Integer = 0
        Dim pais As Integer = 0
        Dim fabrica As String = String.Empty
        Dim tienda As Integer = 0
            optFiltro = 1
            existencia = Me.ddlTipoExistencia.SelectedValue
            linea = IIf(Me.ddlLinea.SelectedValue = "-1", 0, Me.ddlLinea.SelectedValue)
            sublinea = IIf(Me.ddlSubLinea.SelectedValue = "-1", 0, Me.ddlSubLinea.SelectedValue)
            pais = IIf(Me.ddlPais.SelectedValue = "-1", 0, Me.ddlPais.SelectedValue)
           
        Dim dt As New DataTable

        Dim cadena As String = ""
        For Each elemento As ListItem In CheckBoxList1.Items            
            If elemento.Selected Then
                cadena = cadena & elemento.Value & ","
            End If
        Next
        cadena = cadena.Remove(cadena.Length - 1)        
        dt = clsnegocio.LN_ObtenerDatosGrilla(Me.ddlTienda.SelectedValue, Me.txtFecInicio.Text, Me.txtFecFinal.Text, _
                                              Convert.ToInt32(IIf(Me.txtTop.Text = "", 0, Me.txtTop.Text)), 2, optFiltro, existencia, linea, sublinea, pais, "14", cadena)
        Session("ValorizadoCajas") = dt        
        lblmensaje.Text = "DATOS PROCESADOS, CLICK EN EXPORTAR PARA CONTINUAR.."
        'gvDatos.DataSource = dt
        'gvDatos.DataBind()
    End Sub

    Private Sub btnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportar.Click        
        Dim dt As New DataTable
        dt = Session("ValorizadoCajas")
        Dim grilla As New GridView()
        grilla.HeaderStyle.CssClass = "GrillaHeader"
        grilla.HeaderStyle.CssClass = "GrillaRow"
        grilla.DataSource = dt
        grilla.DataBind()

        Dim sb As StringBuilder = New StringBuilder()
        Dim sw As StringWriter = New StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim pagina As Page = New Page
        Dim form = New HtmlForm
        grilla.EnableViewState = False
        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()
        pagina.Controls.Add(form)
        form.Controls.Add(grilla)
        pagina.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=data.xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(sb.ToString())

        Response.End()

    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
    End Sub

    Private Sub frmPruValorizado_PreRenderComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRenderComplete
        CheckBoxList1.Items(0).Selected = True
    End Sub

End Class