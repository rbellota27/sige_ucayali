﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmTrazabilidad
    Inherits System.Web.UI.Page


    Dim objScript As New ScriptManagerClass
    Dim oCombo As Combo

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        OnLoad_Trazabilidad()
    End Sub
    Private Sub OnLoad_Trazabilidad()
        If Not Me.IsPostBack Then

            Me.hddIdUsuario.Value = CStr(Session("IdUsuario"))

            Me.txtFechaInicio_DocRef.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy").Trim
            Me.txtFechaFin_DocRef.Text = Me.txtFechaInicio_DocRef.Text.Trim

            oCombo = New Combo
            With oCombo

                .LlenarCboEmpresaxIdUsuario(Me.ddlEmpresa, CInt(Me.hddIdUsuario.Value), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.ddlTienda, CInt(Me.ddlEmpresa.SelectedValue), CInt(Me.hddIdUsuario.Value), True)
                '.llenarCboTipoDocumentoxIdTipoDocumentos(cboTipoDocumento, "1,3,4,6,9,14,15,16,17,18,20,21,25,26,28,29,30,33,36,47,56,57", 2)
                .LlenarCboTipoDocumentoIndep(Me.cboTipoDocumento, False)
                .LLenarCboEstadoEntrega(Me.cboEstadoEntrega, True)
                .LLenarCboEstadoCancelacion(Me.cboEstadoCancelacion, True)
                .LlenarCboRol(Me.ddl_Rol, True)

                If Not Me.cboTipoDocumento.Items.FindByValue("14") Is Nothing Then
                    Me.cboTipoDocumento.SelectedValue = "14"
                End If

            End With

            Me.actualizarOpcionesBusquedaDocRef(0, False)
            Me.ConfigurarDatos()

            Me.listaDocumentoRef = New List(Of Entidades.Documento)
            setlistaDocumentoRef(Me.listaDocumentoRef)

            validarEstado()

        End If
    End Sub
    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub







#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"

    Private listaDocumentoRef As List(Of Entidades.Documento)

    Private Function getlistaDocumentoRef() As List(Of Entidades.Documento)
        Return CType(Session.Item("listaTrazabilidadDocRef"), List(Of Entidades.Documento))
    End Function
    Private Sub setlistaDocumentoRef(ByVal lista As List(Of Entidades.Documento))
        Session.Remove("listaTrazabilidadDocRef")
        Session.Add("listaTrazabilidadDocRef", lista)
    End Sub

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click

        mostrarDocumentosRef_Find(0)

    End Sub

    Private Sub mostrarDocumentosRef_Find(ByVal TipoMov As Integer)
        Try

            Dim index As Integer = 0
            Select Case TipoMov
                Case 0 '**************** INICIO
                    index = 0
                Case 1 '**************** Anterior
                    index = (CInt(txtPageIndex_DocRef.Text) - 1) - 1
                Case 2 '**************** Posterior
                    index = (CInt(txtPageIndex_DocRef.Text) - 1) + 1
                Case 3 '**************** IR
                    index = (CInt(txtPageIndexGO_DocRef.Text) - 1)
            End Select


            Dim serie As Integer = 0
            Dim codigo As String = ""
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdPersona As Integer = 0

            If (Me.hddIdPersona.Value.Trim.Length > 0 And IsNumeric(Me.hddIdPersona.Value)) Then
                If (CInt(Me.hddIdPersona.Value) > 0) Then
                    IdPersona = CInt(Me.hddIdPersona.Value)
                End If
            End If

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CStr(Me.txtCodigo_BuscarDocRef.Text)
            End Select




            'Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).Documento_BuscarDocumentoRef(CInt(Me.ddlEmpresa.SelectedValue), CInt(Me.ddlTienda.SelectedValue), IdPersona, CInt(Me.cboTipoDocumento.SelectedValue), serie, codigo, 0, fechaInicio, fechafin, False)

            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).Documento_BuscarxTraza(CInt(Me.ddlEmpresa.SelectedValue), CInt(Me.ddlTienda.SelectedValue), IdPersona, CInt(Me.cboTipoDocumento.SelectedValue), serie, codigo, fechaInicio, fechafin, False, index, Me.GV_DocumentosReferencia_Find.PageSize, CInt(Me.cboEstadoEntrega.SelectedValue), CInt(Me.cboEstadoCancelacion.SelectedValue))

            If (lista.Count > 0) Then

                Me.txtPageIndex_DocRef.Text = CStr(index + 1)
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                objScript.mostrarMsjAlerta(Me, "No se hallaron Registros")
            End If



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find(0)
    End Sub

    Private Sub btnAnterior_DocRef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_DocRef.Click
        mostrarDocumentosRef_Find(1)
    End Sub

    Private Sub btnSiguiente_DocRef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSiguiente_DocRef.Click
        mostrarDocumentosRef_Find(2)
    End Sub

    Private Sub btnIr_DocRef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_DocRef.Click
        mostrarDocumentosRef_Find(3)
    End Sub


    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub

    Private Sub GV_DocumentosReferencia_Find_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_DocumentosReferencia_Find.PageIndexChanging
    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged

        cargarDocumentoReferencia(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))

    End Sub

    Private Sub cargarDocumentoReferencia(ByVal IdDocumentoRef As Integer)

        Try

            'validar_AddDocumentoRef(IdDocumentoRef)

            VisualizarDet_Click(IdDocumentoRef)

            cargaTreeviewDocumentoRef(IdDocumentoRef, TreeViewDocumentoRef)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

    Private Function obtenerListaDetalleDocumento_Load_DocRef(ByVal IdDocumentoRef As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Fecha As Date, ByVal IdTipoPV As Integer, ByVal IdPersona As Integer, ByVal IdMoneda As Integer, ByVal IdVendedor As Integer) As List(Of Entidades.DetalleDocumento)

        Dim lista As List(Of Entidades.DetalleDocumento) = (New Negocio.DocumentoCotizacion).DocumentoVenta_SelectDetxIdDocumentoRef(IdDocumentoRef, IdEmpresa, IdTienda, Fecha, IdTipoPV, IdPersona, IdMoneda, IdVendedor)

        For i As Integer = 0 To lista.Count - 1

            lista(i).ListaUM_Venta = (New Negocio.ProductoTipoPV).SelectUM_Venta(lista(i).IdProducto, IdTienda, IdTipoPV)

        Next

        Return lista

    End Function

    Private Function validar_AddDocumentoRef(ByVal IdDocumentoRef As Integer) As Boolean

        Me.listaDocumentoRef = getlistaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            If (Me.listaDocumentoRef(i).Id = IdDocumentoRef) Then
                Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next

        Return True

    End Function


    Private Sub cargarDocumentoRef_GUI(ByVal IdDocumentoRef As Integer, ByVal objDocumento As Entidades.Documento, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal objObservaciones As Entidades.Observacion)

        If Me.hddIdPersona.Value.Trim <> CStr(objDocumento.IdPersona) Then

            Me.cargarPersona(objDocumento.IdPersona, False)

        End If


    End Sub

    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then

        End If

    End Sub

    Private Sub GV_DocumentoRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoRef.SelectedIndexChanged
        quitarDocumentoRef(Me.GV_DocumentoRef.SelectedRow.RowIndex)
    End Sub
    Private Sub quitarDocumentoRef(ByVal index As Integer)

        Try

            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.listaDocumentoRef.RemoveAt(index)

            '********* GUARDAMOS EN SESSION
            setlistaDocumentoRef(Me.listaDocumentoRef)

            '************ GUI
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

#End Region




#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", CInt(Me.ddl_Rol.SelectedValue))



            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            Me.GV_DocumentoRef.DataSource = Nothing
            Me.GV_DocumentoRef.DataBind()

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub


    Private Sub cargarPersona(ByVal IdPersona As Integer, Optional ByVal mostrarDocumentosRef As Boolean = True)

        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)


        If (objPersona IsNot Nothing) Then

            Me.txtDescripcionPersona.Text = objPersona.Descripcion
            Me.txtDni.Text = objPersona.Dni
            Me.txtRuc.Text = objPersona.Ruc
            Me.hddIdPersona.Value = CStr(objPersona.IdPersona)

        Else

            Throw New Exception("NO SE HALLARON REGISTROS.")

        End If

    End Sub

#End Region



    Private Sub ddlEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmpresa.SelectedIndexChanged
        oCombo = New Combo

        With oCombo
            .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.ddlTienda, CInt(Me.ddlEmpresa.SelectedValue), CInt(Me.hddIdUsuario.Value), True)
        End With

    End Sub







#Region "Documento Referencia"

    Public Sub cargaTreeviewDocumentoRef(ByVal IdDocumento As Integer, ByVal ctrlTreeview As TreeView)
        Dim listaopciones As New List(Of Entidades.Opcion)
        listaopciones = (New Negocio.opcion).SelectOpcionxDocumentoRef(IdDocumento)

        ctrlTreeview.Nodes.Clear()

        CargaNodosTreeView(ctrlTreeview, listaopciones)

    End Sub

    Public Sub CargaNodosTreeView(ByVal ctrlTreeview As TreeView, ByVal listaOpcion As List(Of Entidades.Opcion))

        For Each opcion As Entidades.Opcion In listaOpcion
            Dim id, idmenu As Integer
            id = opcion.Id
            idmenu = opcion.IdMenu
            'si padre es igual al hijo entonces es menu padre.
            If (opcion.Id.Equals(opcion.IdMenu)) Then
                Dim TvwTreeNodo As New TreeNode
                'TvwTreeNodo.ShowCheckBox = True

                If opcion.IsPermitido = "1" Then
                    TvwTreeNodo.Checked = True
                Else
                    TvwTreeNodo.Checked = False
                End If
                TvwTreeNodo.Value = opcion.Id.ToString
                TvwTreeNodo.Text = opcion.Nombre.ToString
                'TvwTreeNodo.NavigateUrl = opcion.Formulario.ToString
                TvwTreeNodo.ToolTip = opcion.Formulario.ToString
                TvwTreeNodo.Expand()
                TvwTreeNodo.SelectAction = TreeNodeSelectAction.Select
                ctrlTreeview.Nodes.Add(TvwTreeNodo)
                'hacemos un llamado al metodo recursivo encargado de generar el árbol del menú.
                AddTreeViewItem(TvwTreeNodo, listaOpcion)
            End If
        Next
    End Sub

    Private Sub AddTreeViewItem(ByRef mnuMenuItem As TreeNode, ByVal listaOpciones As List(Of Entidades.Opcion))
        'recorremos cada elemento del datatable para poder determinar cuales son elementos hijos
        'del menuitem dado pasado como parametro ByRef.
        For Each opcion As Entidades.Opcion In listaOpciones
            Dim id, idmenu, mnu As Integer
            id = opcion.Id
            idmenu = opcion.IdMenu
            mnu = CInt(mnuMenuItem.Value)

            If opcion.IdMenu.Equals(CInt(mnuMenuItem.Value)) AndAlso Not opcion.Id.Equals(CInt(opcion.IdMenu)) Then
                Dim TvwNewTreeNode As New TreeNode
                'TvwNewTreeNode.ShowCheckBox = True
                If opcion.IsPermitido = "1" Then
                    TvwNewTreeNode.Checked = True
                Else
                    TvwNewTreeNode.Checked = False
                End If
                TvwNewTreeNode.Value = opcion.Id.ToString
                TvwNewTreeNode.Text = opcion.Nombre.ToString
                'TvwNewTreeNode.NavigateUrl = opcion.Formulario.ToString
                TvwNewTreeNode.ToolTip = opcion.Formulario.ToString
                TvwNewTreeNode.SelectAction = TreeNodeSelectAction.Select
                'Agregamos el Nuevo MenuItem al MenuItem que viene de un nivel superior.
                mnuMenuItem.ChildNodes.Add(TvwNewTreeNode)
                'llamada recursiva para ver si el nuevo menú ítem aun tiene elementos hijos.
                AddTreeViewItem(TvwNewTreeNode, listaOpciones)
            End If
        Next
    End Sub



#End Region


    Private Sub TreeViewDocumentoRef_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeViewDocumentoRef.SelectedNodeChanged

        VisualizarDet_Click(CInt(Me.TreeViewDocumentoRef.SelectedNode.Value))

    End Sub


    Private Sub VisualizarDet_Click(ByVal IdDocumento As Integer)

        Try


            '*************** CARGAMOS EL DOCUMETO DE REFERENCIA

            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)
            Me.listaDocumentoRef = New List(Of Entidades.Documento)
            Me.listaDocumentoRef.Add(objDocumento)
            Me.cargarPersona(objDocumento.IdPersona)
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            '************** 

            Me.lblTituloDetalleDocumento.Visible = True
            Me.lblTituloDetalleDocumento.Text = objDocumento.NomTipoDocumento + " " + objDocumento.NroDocumento + " " + objDocumento.NomTipoOperacion

            Dim ListaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumento(IdDocumento)

            If ListaDetalle IsNot Nothing Then

                If objDocumento.IdTipoDocumento = 25 Or objDocumento.IdTipoDocumento = 21 Or objDocumento.IdTipoDocumento = 6 Then

                    For i As Integer = 0 To ListaDetalle.Count - 1
                        ListaDetalle(i).Importe = 0
                        ListaDetalle(i).Moneda = ""
                        ListaDetalle(i).PrecioCD = 0
                    Next

                End If

            End If

            Me.GV_DocumentoRef_Detalle.DataSource = ListaDetalle
            Me.GV_DocumentoRef_Detalle.DataBind()

            If Me.GV_DocumentoRef_Detalle.Rows.Count > 0 Then
                tr_DetalleDocumento.Visible = True
            Else
                tr_DetalleDocumento.Visible = False
            End If

            Me.GV_DocumentoRef_DetalleConcepto.DataSource = (New Negocio.DetalleConcepto).SelectxIdDocumento(IdDocumento)
            Me.GV_DocumentoRef_DetalleConcepto.DataBind()

            If Me.GV_DocumentoRef_DetalleConcepto.Rows.Count > 0 Then
                tr_ConceptoDocumento.Visible = True
            Else
                tr_ConceptoDocumento.Visible = False
            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Private Sub btnLimpiarPersona_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarPersona.Click

        Me.txtDescripcionPersona.Text = ""
        Me.txtRuc.Text = ""
        Me.txtDni.Text = ""
        Me.hddIdPersona.Value = ""

    End Sub

    Private Sub cboTipoDocumento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoDocumento.SelectedIndexChanged

        validarEstado()

    End Sub

    Private Sub validarEstado()
        If Me.cboTipoDocumento.SelectedValue = "16" Then
            tr_Estado.Visible = True
        Else
            tr_Estado.Visible = False
            Me.cboEstadoEntrega.SelectedIndex = 0
            Me.cboEstadoCancelacion.SelectedIndex = 0
        End If
    End Sub

End Class