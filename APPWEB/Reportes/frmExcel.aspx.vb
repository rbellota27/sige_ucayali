﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.IO

Partial Public Class frmExcel
    Inherits System.Web.UI.Page


    Private listaTipoCambio As List(Of Entidades.TipoCambio)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            OnLoad_Excel()

        End If
    End Sub

    Private Sub OnLoad_Excel()
        Dim iReport As Integer = CInt(Request.QueryString("iReport"))
        Select Case iReport

            Case 1
                Exportar_Tipocambio()

        End Select


    End Sub

    Private Sub Exportar_Tipocambio()
        Me.listaTipoCambio = CType(Session.Item("DataSource"), List(Of Entidades.TipoCambio))

        For i As Integer = 0 To listaTipoCambio.Count - 1
            Dim trow As New TableRow()
            For p As Integer = 0 To 4
                Dim tcell As New TableCell()
                Select Case p
                    Case 0
                        tcell.Text = listaTipoCambio(i).Fecha
                    Case 1
                        tcell.Text = listaTipoCambio(i).CompraOf.ToString
                    Case 2
                        tcell.Text = listaTipoCambio(i).VentaOf.ToString
                    Case 3
                        tcell.Text = listaTipoCambio(i).CompraC.ToString
                    Case 4
                        tcell.Text = listaTipoCambio(i).VentaC.ToString
                End Select

                trow.Cells.Add(tcell)

            Next

            tablaPrincipal.Rows.Add(trow)
        Next

        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("content-disposition", "attachment;filename=TipoCambio.xls")
        Response.Charset = ""
        Me.EnableViewState = False

        Dim sw As New System.IO.StringWriter()
        Dim htw As New System.Web.UI.HtmlTextWriter(sw)

        Me.tablaPrincipal.RenderControl(htw)

        Response.Write(sw.ToString())
        Response.End()



    End Sub

End Class