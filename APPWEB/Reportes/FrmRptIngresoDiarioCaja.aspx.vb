﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmRptIngresoDiarioCaja
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.IsPostBack = False Then
            Dim cbo As New Combo
            cbo.LlenarCboPropietario(Me.cmbEmpresa)
            cbo.LLenarCboTienda(Me.cboTienda)
        End If
        mostrarRptIngresoCajaDiario()
    End Sub
    Private Sub mostrarRptIngresoCajaDiario()
        Dim objMovCaja As New Negocio.MovCaja
        Dim obj As New CR_Caja_Diario
        obj.SetDataSource(objMovCaja.IngresosDiariosxParams(CInt(cboTienda.SelectedValue), CInt(cmbEmpresa.SelectedValue)))

        With CRV1
            .ReportSource = obj
            .DataBind()
        End With

    End Sub

    Protected Sub btnAceptar_TipoMoneda_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAceptar_TipoMoneda.Click
        If Me.IsPostBack = True Then
            mostrarRptIngresoCajaDiario()
        End If
    End Sub
End Class