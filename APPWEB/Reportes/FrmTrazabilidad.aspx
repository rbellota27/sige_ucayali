﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmTrazabilidad.aspx.vb" Inherits="APPWEB.FrmTrazabilidad" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel_Ini" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td class="TituloCelda">
                        Trazabilidad de Documentos
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="Texto">
                                    Empresa:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlEmpresa" runat="server" Width="300px" AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                                <td class="Texto">
                                    Tienda:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlTienda" runat="server" Width="300px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="Texto">
                                    Tipo Documento:
                                </td>
                                <td>
                                    <asp:DropDownList ID="cboTipoDocumento" runat="server" Width="300px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="tr_Estado" runat="server">
                    <td align="center">
                        <table>
                            <tr>
                                <td class="Texto">
                                    Estado Entrega:
                                </td>
                                <td>
                                    <asp:DropDownList ID="cboEstadoEntrega" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td class="Texto">
                                    Estado Cancelación:
                                </td>
                                <td>
                                    <asp:DropDownList ID="cboEstadoCancelacion" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="TituloCeldaLeft">
                        Buscar Documento
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td align="right" style="text-align: left">
                                    <asp:RadioButtonList ID="rdbBuscarDocRef" runat="server" RepeatDirection="Horizontal"
                                        CssClass="Texto" AutoPostBack="true">
                                        <asp:ListItem Value="0" Selected="True">Por Fecha de Emisión</asp:ListItem>
                                        <asp:ListItem Value="1">Por Nro. Documento</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:Panel ID="Panel_BuscarDocRefxFecha" runat="server">
                                        <table>
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                                <td class="Texto">
                                                    Fecha Inicio:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFechaInicio_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                        Width="100px"></asp:TextBox>
                                                    <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" ClearMaskOnLostFocus="false"
                                                        CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                        CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                        CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_DocRef">
                                                    </cc1:MaskedEditExtender>
                                                    <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                        TargetControlID="txtFechaInicio_DocRef">
                                                    </cc1:CalendarExtender>
                                                </td>
                                                <td class="Texto">
                                                    Fecha Fin:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFechaFin_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                        Width="100px"></asp:TextBox>
                                                    <cc1:MaskedEditExtender ID="MaskedEditExtender5" runat="server" ClearMaskOnLostFocus="false"
                                                        CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                        CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                        CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_DocRef">
                                                    </cc1:MaskedEditExtender>
                                                    <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                        TargetControlID="txtFechaFin_DocRef">
                                                    </cc1:CalendarExtender>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnAceptarBuscarDocRef" Width="70px" ToolTip="Buscar Documentos" CssClass="btnBuscar"
                                                        runat="server" Text="Buscar" OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="Panel_BuscarDocRefxCodigo" runat="server">
                                        <table>
                                            <tr>
                                                <td class="Texto">
                                                    Nro. Serie:
                                                </td>
                                                <td>
                                                    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnAceptarBuscarDocRefxCodigo">                                                    
                                                    <asp:TextBox ID="txtSerie_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                        Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                                        </asp:Panel>
                                                </td>
                                                <td class="Texto">
                                                    Nro. Código:
                                                </td>
                                                <td>
                                                <asp:Panel ID="Panel2" runat="server" DefaultButton="btnAceptarBuscarDocRefxCodigo">
                                                    <asp:TextBox ID="txtCodigo_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                        Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                                        </asp:Panel>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnAceptarBuscarDocRefxCodigo" OnClientClick="return(  valOnClick_btnAceptarBuscarDocRefxCodigo()  );"
                                                        runat="server" Text="Buscar" Width="70px" ToolTip="Buscar Documentos" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_DocRef_Find" runat="server" ScrollBars="Horizontal" Width="100%">
                            <asp:GridView ID="GV_DocumentosReferencia_Find" runat="server" AutoGenerateColumns="false"
                                Width="100%" PageSize="5" AllowPaging="True">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-Width="50px" />
                                    <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                        HeaderText="Tipo" />
                                    <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                            Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisión"
                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="DescripcionPersona" HeaderText="Persona" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" />
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td class="LabelRojo">
                        ********* Ingrese a una persona para una busqueda rapida de documentos
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table>
                            <tr>
                                <td>
                                    <asp:Button TabIndex="212" ID="btnAnterior_DocRef" runat="server" Font-Bold="true"
                                        Width="50px" Text="<" ToolTip="Página Anterior" OnClientClick="return(valNavegacionDocRef('0'));" />
                                </td>
                                <td>
                                    <asp:Button TabIndex="213" ID="btnSiguiente_DocRef" runat="server" Font-Bold="true"
                                        Width="50px" Text=">" ToolTip="Página Posterior" OnClientClick="return(valNavegacionDocRef('1'));" />
                                </td>
                                <td>
                                    <asp:TextBox TabIndex="214" ID="txtPageIndex_DocRef" Width="50px" ReadOnly="true"
                                        CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Button TabIndex="215" ID="btnIr_DocRef" runat="server" Font-Bold="true" Width="50px"
                                        Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacionDocRef('2'));" />
                                </td>
                                <td>
                                    <asp:TextBox TabIndex="216" ID="txtPageIndexGO_DocRef" Width="50px" CssClass="TextBox_ReadOnly"
                                        runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="TituloCeldaLeft">
                        Persona
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table>
                            <tr>
                                <td class="Texto" align="right">
                                    Descripción:
                                </td>
                                <td colspan="3" align="left">
                                    <asp:TextBox ID="txtDescripcionPersona" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft"
                                        runat="server" Width="300px"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:ImageButton ID="btnLimpiarPersona" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                        OnClientClick=" return ( onClick_LimpiarPersona() ); " />
                                </td>
                                <td>
                                    <asp:ImageButton ID="btnBuscarPersona" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                        OnClientClick=" return (mostrarCapaPersona()); " />
                                </td>
                            </tr>
                            <tr>
                                <td class="Texto" align="right">
                                    D.N.I.:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDni" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>
                                </td>
                                <td align="right" class="Texto">
                                    R.U.C.:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtRuc" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_DocRef" runat="server">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:GridView ID="GV_DocumentoRef" runat="server" AutoGenerateColumns="False" Width="100%">
                                            <HeaderStyle CssClass="GrillaHeader" />
                                            <Columns>
                                                <asp:BoundField DataField="NomTipoDocumento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red"
                                                    HeaderText="Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="25px" />
                                                <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                    HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblNroDocumento" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:HiddenField ID="hddIdDocumento0" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Empresa" HeaderText="Empresa" HeaderStyle-Height="25px"
                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                <asp:BoundField DataField="Tienda" HeaderText="Tienda" HeaderStyle-Height="25px"
                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                <asp:BoundField DataField="NomAlmacen" HeaderText="Almacen" HeaderStyle-Height="25px"
                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                <asp:BoundField DataField="NomMoneda" HeaderText="" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                    HeaderStyle-HorizontalAlign="Center" />
                                                <asp:BoundField DataField="TotalAPagar" HeaderText="Importe" DataFormatString="{0:F3}"
                                                    HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                            </Columns>
                                            <FooterStyle CssClass="GrillaFooter" />
                                            <PagerStyle CssClass="GrillaPager" />
                                            <RowStyle CssClass="GrillaRow" />
                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                            <EditRowStyle CssClass="GrillaEditRow" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td class="SubTituloCelda">
                        Documento de Referencia
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TreeView ID="TreeViewDocumentoRef" runat="server" Font-Bold="true" SelectedNodeStyle-BackColor="#FFCC99"
                            CssClass="LabelLeft">
                        </asp:TreeView>
                    </td>
                </tr>
                <tr>
                    <td class="TituloCelda">
                        <asp:Label ID="lblTituloDetalleDocumento" runat="server" Text="TituloDetalleDocumento"
                            Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr id="tr_DetalleDocumento" runat="server" visible="false">
                                <td class="SubTituloCelda">
                                    Detalle Documento
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_DocumentoRef_Detalle" runat="server" AutoGenerateColumns="False"
                                        Width="100%">
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <Columns>
                                            <asp:BoundField DataField="CodigoProducto" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Cód." ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="NomProducto" ItemStyle-Width="400px" HeaderStyle-Height="25px"
                                                HeaderStyle-HorizontalAlign="Center" HeaderText="Producto" ItemStyle-Font-Bold="true"
                                                ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="Cantidad" DataFormatString="{0:F3}" HeaderStyle-Height="25px"
                                                HeaderStyle-HorizontalAlign="Center" HeaderText="Cantidad" ItemStyle-Font-Bold="true"
                                                ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="UMedida" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="U.M." ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="PrecioCD" DataFormatString="{0:F3}" HeaderStyle-Height="25px"
                                                HeaderStyle-HorizontalAlign="Center" HeaderText="P. Venta" ItemStyle-Font-Bold="true"
                                                ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Importe" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblMoneda" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblImporte" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Importe","{0:F3}")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:BoundField DataField="CantxAtender" DataFormatString="{0:F3}" HeaderStyle-Height="25px"
                                                HeaderStyle-HorizontalAlign="Center" HeaderText="Pendiente" ItemStyle-Font-Bold="true"
                                                ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />                                                
                                            <asp:BoundField DataField="CantidadTransito" DataFormatString="{0:F3}" HeaderStyle-Height="25px"
                                                HeaderStyle-HorizontalAlign="Center" HeaderText="Cant. Transito" ItemStyle-Font-Bold="true"
                                                ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                        </Columns>
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr id="tr_ConceptoDocumento" runat="server" visible="false">
                                <td class="SubTituloCelda">
                                    Detalle Concepto
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_DocumentoRef_DetalleConcepto" runat="server" AutoGenerateColumns="False"
                                        Width="100%">
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <Columns>
                                            <asp:BoundField DataField="Descripcion" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Concepto" ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Total" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblMoneda" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblMonto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Monto","{0:F3}")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:HiddenField ID="hddIdPersona" runat="server" />
                        <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
                        <asp:HiddenField ID="hddIdUsuario" runat="server" />
                        <asp:HiddenField ID="hddmostrarDocumentosRef" runat="server" Value="0" />
                    </td>
                </tr>
            </table>
            <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
                height: auto; position: absolute; left: 25px; top: 50px; background-color: white;
                z-index: 2; display: none;">
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                                OnClientClick="return(offCapa('capaPersona'));" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="pnlBusquedaPersona" runat="server">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td colspan="5">
                                                                                <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                                    AutoPostBack="false">
                                                                                    <asp:ListItem Value="N">Natural</asp:ListItem>
                                                                                    <asp:ListItem Selected="True" Value="J">Juridica</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="Texto">
                                                                                Razon Social / Nombres:
                                                                            </td>
                                                                            <td colspan="4">
                                                                                <asp:Panel ID="Panel3" runat="server" DefaultButton="btBuscarPersonaGrilla">                                                                                
                                                                                <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this));" runat="server"
                                                                                    onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                                                    Width="450px"></asp:TextBox>
                                                                                    </asp:Panel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="Texto">
                                                                                D.N.I.:
                                                                            </td>
                                                                            <td>
                                                                            <asp:Panel ID="Panel4" runat="server" DefaultButton="btBuscarPersonaGrilla">
                                                                                <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero());" onFocus="javascript:validarbusqueda();"
                                                                                    MaxLength="8" runat="server"></asp:TextBox>
                                                                                    </asp:Panel>
                                                                            </td>
                                                                            <td class="Texto">
                                                                                Ruc:
                                                                            </td>
                                                                            <td>
                                                                            <asp:Panel ID="Panel5" runat="server" DefaultButton="btBuscarPersonaGrilla">
                                                                                <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero());"
                                                                                    MaxLength="11"></asp:TextBox>
                                                                                    </asp:Panel>
                                                                            </td>
                                                                            <td>
                                                                                <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                                    ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../Imagenes/Buscar_b.JPG';"
                                                                                    onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="Texto">
                                                                                Ruc:
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddl_Rol" runat="server">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                        PageSize="20" Width="100%">
                                                                        <Columns>
                                                                            <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                            <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                            <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                                <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                        <RowStyle CssClass="GrillaRow" />
                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                        ToolTip="Página Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                                    <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                        ToolTip="Página Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                                    <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                        runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                            Width="50px" Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacion('2'));" />
                                                                    <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                        onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script language="javascript" type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;

        //***************************************************** BUSQUEDA PERSONA

        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(obj) {
            var key = event.keyCode;
            if (key == 13) {
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }
        function onClick_BuscarPersonaRef() {
            return false;
        }

        function valOnClick_btnBuscar_DocumentoRef() {

            return false;
        }

        function valOnClick_btnAceptarBuscarDocRefxCodigo() {
            var txtSerie = document.getElementById('<%=txtSerie_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtSerie.value)) || txtSerie.value.length <= 0) {
                alert('Ingrese un Nro. de Serie para la búsqueda.');
                txtSerie.select();
                txtSerie.focus();
                return false;
            }
            var txtCodigo = document.getElementById('<%=txtCodigo_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtCodigo.value)) || txtCodigo.value.length <= 0) {
                alert('Ingrese un Nro. de Documento para la búsqueda.');
                txtCodigo.select();
                txtCodigo.focus();
                return false;
            }
            return true;
        }
        function onClick_LimpiarPersona() {
            return true;
        }

        function valNavegacionDocRef(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_DocRef.ClientID%>').value);
            var grilla = document.getElementById('<%=GV_DocumentosReferencia_Find.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_DocRef.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
            }
            return true;
        }

        
    </script>

</asp:Content>
