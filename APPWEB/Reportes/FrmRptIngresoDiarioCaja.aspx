<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmRptIngresoDiarioCaja.aspx.vb" Inherits="APPWEB.FrmRptIngresoDiarioCaja" %>
<%@ Register assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--  <asp:UpdatePanel ID="UpdatePanel1" runat="server"> <ContentTemplate>-->
            <table style="width: 100%">
                <tr>
                    <td>
                        <asp:Label ID="Label4" runat="server" CssClass="Label" Text="Empresa:"></asp:Label>
                        <asp:DropDownList ID="cmbEmpresa" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                        <asp:Label ID="Label5" runat="server" CssClass="Label" Text="Tienda:"></asp:Label>
                        <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True" 
                            Enabled="False">
                        </asp:DropDownList>
                        <asp:ImageButton ID="btnAceptar_TipoMoneda" runat="server" 
                            ImageUrl="~/Imagenes/Aceptar_B.JPG" 
                            onmouseout="this.src='/Imagenes/Aceptar_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Aceptar_A.JPG';" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <CR:CrystalReportViewer ID="CRV1" runat="server" AutoDataBind="true" 
                            PrintMode="ActiveX" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        <!--</ContentTemplate>    </asp:UpdatePanel>-->
</asp:Content>
