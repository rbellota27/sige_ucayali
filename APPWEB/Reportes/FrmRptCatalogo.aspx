<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmRptCatalogo.aspx.vb" Inherits="APPWEB.FrmRptCatalogo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" >
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
     <ContentTemplate>
        <table>
            <tr>
                <td><asp:Label ID="Label1" runat="server" Text="Tienda:" CssClass="Label"></asp:Label>
                    <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td><asp:Label ID="Label2" runat="server" Text="Sub Lineas:" CssClass="Label"></asp:Label>
                    <asp:DropDownList ID="cboSubLinea" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
            </tr>  
            <tr>
                <td>
                    <input ID="btnReporte" type="button" value="Reporte" onclick='javascript:js_verprogramacion();' />
                    
                </td>   
            </tr>    
        </table>          
     </ContentTemplate>
 </asp:UpdatePanel>
 <iframe name="frame1" id="frame1" width="100%" scrolling="no" style="height: 1129px" ></iframe> 
<script language="javascript" type="text/javascript">
     function js_verprogramacion() {
         frame1.location.href = 'visor.aspx';         
     }
</script>
</asp:Content>
