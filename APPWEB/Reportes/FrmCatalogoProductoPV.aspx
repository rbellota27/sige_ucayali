﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmCatalogoProductoPV.aspx.vb" Inherits="APPWEB.FrmCatalogoProductoPV" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table>
            <tr>
            <td style="width: 945px">
                <table>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label4" runat="server" CssClass="Label" Text="Tienda"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="cmbTienda" runat="server" DataTextField="Nombre" 
                                DataValueField="Id">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label10" runat="server" CssClass="Label" 
                                Text="Tipo Precio Venta:"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="cmbTipoPrecioVenta_SubLinea" runat="server" 
                                DataTextField="Nombre" DataValueField="IdTipoPv">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label2" runat="server" CssClass="Label" Text="Línea:"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="cmbLinea" runat="server" AutoPostBack="true" 
                                DataTextField="Descripcion" DataValueField="Id">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label3" runat="server" CssClass="Label" Text="SubLínea:"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="cmbSubLinea" runat="server" DataTextField="Nombre" 
                                DataValueField="Id">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label20" runat="server" CssClass="Label" Text="Por U. Medida:"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="cmbUM" runat="server">
                                <asp:ListItem Value="1">Todos</asp:ListItem>
                                <asp:ListItem Value="2">Unidad Medida Principal</asp:ListItem>
                                <asp:ListItem Value="3">Unidad Medida Retazo</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:ImageButton ID="btnAceptar_SubLinea_Cab" runat="server" 
                                ImageUrl="~/Imagenes/Aceptar_B.JPG" OnClientClick="return(verReporte());" 
                                onmouseout="this.src='/Imagenes/Aceptar_B.JPG';" 
                                onmouseover="this.src='/Imagenes/Aceptar_A.JPG';" />
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            </table>
            
        </ContentTemplate>
    </asp:UpdatePanel>
    <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1129px" ></iframe> 
    
<script language="javascript" type="text/javascript">
    var idTienda = 0;
    var idlinea = 0;
    var idsublinea = 0;
    var idtipopv = 0;
    var opcion = 0;
    var ump = 0;
    var umr = 0;
    function verReporte() {
        idTienda = document.getElementById('<%=  cmbTienda.ClientID %>').value;
        idtipopv = document.getElementById('<%=  cmbTipoPrecioVenta_SubLinea.ClientID %>').value;
        idlinea = document.getElementById('<%=  cmbLinea.ClientID %>').value;
        idsublinea = document.getElementById('<%=  cmbSubLinea.ClientID %>').value;
        opcion=parseFloat(document.getElementById('<%=  cmbUM.ClientID %>').value)
        switch (opcion) {
            case 1://todos
                ump = 0;
                umr = 0;
                break;
            case 2://um p
                ump = 1;
                umr = 0;
                break;
            case 3://um r
                ump = 0;
                umr = 1;
                break;
        }
        js_verprogramacion();
        return false;
    }
    
     function js_verprogramacion() {
         frame1.location.href = 'visor.aspx?iReporte=7&IdTienda=' + idTienda + '&IdTipoPV=' + idtipopv + '&IdLinea=' + idlinea + '&IdSubLinea=' + idsublinea + '&UM_Principal=' + ump + '&UM_Retazo='+umr;
     }
</script>
</asp:Content>

