﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Partial Public Class FrmVisorReportes
    Inherits System.Web.UI.Page
    Private objReportes As Negocio.Reportes
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        mostrarMensaje(False)
        mostrarReporte()
    End Sub
    Private Sub mostrarMensaje(ByVal isVisible As Boolean, Optional ByVal texto As String = "")
        lblMensaje.Visible = isVisible
        lblMensaje.Text = texto
    End Sub
    Private Sub mostrarReporte()
        Dim iReport As Integer = CInt(Request.QueryString("iReport"))
        'iReport = 1
        Select Case iReport
            Case 1 'reporte Factura
                mostrarReportFactura()
            Case 2 'reporte catalogo de productos
                mostrarReportCatalogo()
        End Select
    End Sub
    Private Sub mostrarReportFactura()
        objReportes = New Negocio.Reportes
        Dim IdDoc As Integer = CInt(Request.QueryString("IdDoc"))
        'IdDoc = 36
        Dim ds As DataSet = objReportes.getDataSetFactura(IdDoc)
        If ds IsNot Nothing Then
            Dim obj As New CR_Factura
            obj.SetDataSource(ds)
            'CRViewer.ReportSource = obj
            'CRViewer.Visible = True
            'obj.PrintOptions.PrinterName = "\\digrafic01\Samsung ML-2010 Series"
            'obj.PrintToPrinter(1, True, 1, 1)
            With CRViewer
                .ReportSource = obj
                ' .DataBind()
            End With

            ' If Me.CRViewer.PrintMode
        Else
            'MsgBox1.ShowMessage("Problemas en la carga de datos del Reporte.")
            mostrarMensaje(True, "Problemas en la carga de datos del Reporte.")
        End If
        'Response.Redirect("~/Ventas/FrmEmitirDocumentos.aspx")
    End Sub
    Private Sub mostrarReportCatalogo()
        objReportes = New Negocio.Reportes
        'Dim ds As DataSet = objReportes.getDataSetCatalogo
        'If ds IsNot Nothing Then
        Dim obj As New CR_Catalogo
        obj.SetDataSource(objReportes.getDataSetCatalogo)
        CRViewer.ReportSource = obj
        CRViewer.DataBind()
        'CRViewer.Visible = True
        'Else
        ' MsgBox("Problemas en la carga de datos del Reporte.")
        ' End If
    End Sub
End Class