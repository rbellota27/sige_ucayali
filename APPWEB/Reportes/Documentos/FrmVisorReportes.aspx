<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master1.Master" CodeBehind="FrmVisorReportes.aspx.vb" Inherits="APPWEB.FrmVisorReportes" %>
<%@ Register assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>
<%@ Register assembly="MsgBox" namespace="MsgBox" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%">
        <tr>
            <td>
                <asp:Menu ID="Menu1" runat="server" BackColor="Control" DynamicHorizontalOffset="2"
                    Font-Bold="False" Font-Italic="False" Font-Names="Verdana" Font-Size="8pt" ForeColor="DarkRed"
                    Orientation="Horizontal" StaticSubMenuIndent="10px" Style="vertical-align: top">
                    <StaticSelectedStyle BackColor="#5D7B9D" />
                    <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                    <DynamicHoverStyle BackColor="Peru" ForeColor="White" />
                    <DynamicMenuStyle BackColor="#F7F6F3" />
                    <DynamicSelectedStyle BackColor="SteelBlue" />
                    <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                    <StaticHoverStyle BackColor="#7C6F57" ForeColor="White" />
                    <Items>
                        <asp:MenuItem Text="Areas" Value="Areas">
                            <asp:MenuItem NavigateUrl="~/Ventas.aspx" Text="Ventas" Value="Ventas"></asp:MenuItem>
                            <asp:MenuItem NavigateUrl="~/Almacen.aspx" Text="Almac&#233;n" Value="Almac&#233;n">
                            </asp:MenuItem>
                            <asp:MenuItem NavigateUrl="~/CuentasxCobrar.aspx" Text="Cuentas Por Cobrar" Value="Cuentas Por Cobrar">
                            </asp:MenuItem>
                            <asp:MenuItem NavigateUrl="~/CuentasxPagar.aspx" Text="Cuentas Por Pagar" Value="Cuentas Por Pagar">
                            </asp:MenuItem>
                            <asp:MenuItem NavigateUrl="~/Compras.aspx" Text="Compras" Value="Compras"></asp:MenuItem>
                            <asp:MenuItem NavigateUrl="~/frmAdministracionSistema.aspx" Text="Administraci&#243;n del Sistema"
                                Value="Administraci&#243;n del Sistema"></asp:MenuItem>
                            <asp:MenuItem NavigateUrl="~/frmInformacionGerencial.aspx" Text="Informaci&#243;n Gerencial"
                                Value="Informaci&#243;n Gerencial"></asp:MenuItem>
                            <asp:MenuItem NavigateUrl="~/Principal.aspx" Text="Principal" Value="Principal"></asp:MenuItem>
                        </asp:MenuItem>
                        <asp:MenuItem Text="Mantenimientos" Value="Mantenimientos">
                            <asp:MenuItem Text="Tablas Generales" Value="Clientes" 
                                NavigateUrl="~/Mantenedores/FrmMantTablasGenerales.aspx"></asp:MenuItem>
                            <asp:MenuItem Text="Almacenes" Value="Almacenes"></asp:MenuItem>
                            <asp:MenuItem Text="Tiendas" Value="Tiendas"></asp:MenuItem>
                        </asp:MenuItem>
                        <asp:MenuItem NavigateUrl="~/Ventas/frmEmitirDocumentos.aspx" Text="Tipo de Cambio"
                            Value="Multifacturaci&#243;n"></asp:MenuItem>
                        <asp:MenuItem Text="Backups" Value="Cat&#225;logo"></asp:MenuItem>
                        <asp:MenuItem Text="Asignar Almacenes" Value="Asignaci&#243;n de Almacenes">
                        </asp:MenuItem>
                        <asp:MenuItem Text="Asignar Precios" Value="Asignar Precios" 
                            NavigateUrl="~/Administracion/frmAsignaPrecios.aspx"></asp:MenuItem>
                        <asp:MenuItem NavigateUrl="~/Ventas/FrmVisorReportes.aspx?iReport=2" 
                            Text="Catálogo de Productos" Value="Catálogo de Productos"></asp:MenuItem>
                    </Items>
                </asp:Menu>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblMensaje" runat="server" CssClass="LabelRojo" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <CR:CrystalReportViewer ID="CRViewer" runat="server"  
                    AutoDataBind="true" DisplayGroupTree="False" PrintMode="ActiveX" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="height: 23px">
                </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="height: 23px">
                </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>
