<%@ Page Title="" Language="vb" AutoEventWireup="false"   MasterPageFile="~/Principal.Master"  CodeBehind="FrmRptInvInicial.aspx.vb" Inherits="APPWEB.FrmRptInvInicial" %>
<%@ Register assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script language="javascript" type="text/javascript">
    function mostrarReporte(){        
        var cboEmpresa=document.getElementById('<%=cmbEmpresa.ClientID%>');
        var cboAlmacen = document.getElementById('<%=cmbAlmacen.ClientID%>');
        var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
        var cboSublinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
        frame1.location.href = 'visor.aspx?iReporte=9&IdEmpresa=' + cboEmpresa.value + '&IdAlmacen=' + cboAlmacen.value + '&IdLinea=' + cboLinea.value + '&IdSubLinea=' + cboSublinea.value;
         return false;
    }
</script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        <table width="100%">
        <tr>
        <td><table>
                <tr>
                    <td>
                        <asp:Label ID="Label4" runat="server" CssClass="Label" Text="Empresa:"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="cmbEmpresa" runat="server" AutoPostBack="True" 
                            DataTextField="NombreComercial" DataValueField="Id">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label20" runat="server" CssClass="Label" Text="Almacén:"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="cmbAlmacen" runat="server" DataTextField="Nombre" 
                            DataValueField="IdAlmacen">
                        </asp:DropDownList>
                    </td>
                </tr>
                 <tr>
                            <td class="Texto">
                                Tipo Existencia:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTipoExistencia" runat="server" AutoPostBack ="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label17" runat="server" CssClass="Label" Text="Línea:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True"
                                    DataTextField="Descripcion" DataValueField="Id">
                                </asp:DropDownList>
                                
                                &nbsp;&nbsp;&nbsp;
                                
                                <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Sub Línea:"></asp:Label>
                                <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" AutoPostBack="true" runat="server"
                                    DataTextField="Nombre" DataValueField="Id">
                                </asp:DropDownList>
                            </td>
                        </tr>
            </table></td>
        </tr>
        <tr>
        <td>
            <asp:ImageButton ID="btnAceptar_TipoMoneda" runat="server" 
                ImageUrl="~/Imagenes/Aceptar_B.JPG" OnClientClick="return(mostrarReporte())"
                onmouseout="this.src='/Imagenes/Aceptar_B.JPG';" 
                onmouseover="this.src='/Imagenes/Aceptar_A.JPG';" />
            </td>
        </tr>        
        </table>            
        </ContentTemplate>
    </asp:UpdatePanel>   
    <iframe name="frame1" id="frame1" width="100%" scrolling="no" style="height: 1129px" ></iframe> 
</asp:Content>
