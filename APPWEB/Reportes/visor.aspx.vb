﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Partial Public Class visor
    Inherits System.Web.UI.Page
    Private objReportes As Negocio.Catalogo
    Dim reporte As New ReportDocument
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then
            Dim iReporte As String = Request.QueryString("iReporte")

            If iReporte Is Nothing Then
                iReporte = ""
            End If
            ViewState.Add("iReporte", iReporte)
            Select Case iReporte
                Case "1" 'INVENTARIO INICIAL
                    ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
                Case "2" 'FACTURA
                    ViewState.Add("IdDoc", Request.QueryString("IdDoc"))
                Case "3" 'ORDEN DE DESPACHO
                    ViewState.Add("IdDoc", Request.QueryString("IdDoc"))
                Case "4" 'PEDIDO CLIENTE
                    ViewState.Add("IdDoc", Request.QueryString("IdDoc"))
                Case "5" 'COTIZACION
                    ViewState.Add("IdDoc", Request.QueryString("IdDoc"))
                Case "6" 'BOLETA
                    ViewState.Add("IdDoc", Request.QueryString("IdDoc"))
                Case "7" 'catalogo cubo
                    ViewState.Add("IdTienda", Request.QueryString("IdTienda"))
                    ViewState.Add("IdTipoPV", Request.QueryString("IdTipoPV"))
                    ViewState.Add("IdLinea", Request.QueryString("IdLinea"))
                    ViewState.Add("IdSubLinea", Request.QueryString("IdSubLinea"))
                    ViewState.Add("UM_Principal", Request.QueryString("UM_Principal"))
                    ViewState.Add("UM_Retazo", Request.QueryString("UM_Retazo"))
                Case "8" 'GUIA DE RECEPCION
                    ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
                Case "9"
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdAlmacen", Request.QueryString("IdAlmacen"))
                    ViewState.Add("IdLinea", Request.QueryString("IdLinea"))
                    ViewState.Add("IdSubLinea", Request.QueryString("IdSubLinea"))
                Case "10" 'GUIA DE REMISION
                    ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
                Case "11" 'Mov Caja x Parametros
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdTienda", Request.QueryString("IdTienda"))
                    ViewState.Add("tipodoc", Request.QueryString("tipodoc"))
                    ViewState.Add("IdCaja", Request.QueryString("IdCaja"))
                    ViewState.Add("IdMedioPago", Request.QueryString("IdMedioPago"))
                    ViewState.Add("IdTipoMovimiento", Request.QueryString("IdTipoMovimiento"))
                    ViewState.Add("caja", Request.QueryString("caja"))

                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                    ViewState.Add("caja", Request.QueryString("caja"))
                Case "12" '*********** Catalogo Producto UM
                    ViewState.Add("IdLinea", Request.QueryString("IdLinea"))
                    ViewState.Add("IdSubLinea", Request.QueryString("IdSubLinea"))
                    ViewState.Add("UMedida", Request.QueryString("UMedida"))
                Case "13"
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdTienda", Request.QueryString("IdTienda"))
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                    ViewState.Add("NomEmpresa", Request.QueryString("NomEmpresa"))
                    ViewState.Add("NomTienda", Request.QueryString("NomTienda"))
                    ViewState.Add("Percepcion", Request.QueryString("Percepcion"))
                    ViewState.Add("Cliente", Request.QueryString("Cliente"))
                    ViewState.Add("monBase", Request.QueryString("monBase"))
                Case "14" 'detallado qt
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    'ViewState.Add("textEmpresa", Request.QueryString("textEmpresa"))
                    ViewState.Add("idtienda", Request.QueryString("idtienda"))
                    'ViewState.Add("textTienda", Request.QueryString("textTienda")) idproducto
                    ViewState.Add("idlinea", Request.QueryString("idlinea"))
                    ViewState.Add("idSublinea", Request.QueryString("idSublinea"))
                    ViewState.Add("idproducto", Request.QueryString("idproducto"))
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                    ViewState.Add("yeari", Request.QueryString("yeari"))
                    ViewState.Add("semanai", Request.QueryString("semanai"))
                    ViewState.Add("yearf", Request.QueryString("yearf"))
                    ViewState.Add("semanaf", Request.QueryString("semanaf"))
                    ViewState.Add("filtrarsemana", Request.QueryString("filtrarsemana"))

                Case "15" 'Resumido qt
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    'ViewState.Add("textEmpresa", Request.QueryString("textEmpresa"))
                    ViewState.Add("idtienda", Request.QueryString("idtienda"))
                    'ViewState.Add("textTienda", Request.QueryString("textTienda"))
                    ViewState.Add("idlinea", Request.QueryString("idlinea"))
                    ViewState.Add("idSublinea", Request.QueryString("idSublinea"))
                    ViewState.Add("idProducto", Request.QueryString("idProducto"))
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                    ViewState.Add("yeari", Request.QueryString("yeari"))
                    ViewState.Add("semanai", Request.QueryString("semanai"))
                    ViewState.Add("yearf", Request.QueryString("yearf"))
                    ViewState.Add("semanaf", Request.QueryString("semanaf"))
                    ViewState.Add("filtrarsemana", Request.QueryString("filtrarsemana"))
                Case "16" 'detallado Valorizado
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("idtienda", Request.QueryString("idtienda"))
                    ViewState.Add("IdTipoExistencia", Request.QueryString("IdTipoExistencia"))
                    ViewState.Add("idlinea", Request.QueryString("idlinea"))
                    ViewState.Add("idSublinea", Request.QueryString("idSublinea"))
                    ViewState.Add("idproducto", Request.QueryString("idproducto"))
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))

                    ViewState.Add("yeari", Request.QueryString("yeari"))
                    ViewState.Add("semanai", Request.QueryString("semanai"))
                    ViewState.Add("yearf", Request.QueryString("yearf"))
                    ViewState.Add("semanaf", Request.QueryString("semanaf"))
                    ViewState.Add("filtrarsemana", Request.QueryString("filtrarsemana"))

                Case "17" 'Resumido Valorizado
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("idtienda", Request.QueryString("idtienda"))
                    ViewState.Add("idlinea", Request.QueryString("idlinea"))
                    ViewState.Add("idSublinea", Request.QueryString("idSublinea"))
                    ViewState.Add("idProducto", Request.QueryString("idProducto"))
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))

                    ViewState.Add("yeari", Request.QueryString("yeari"))
                    ViewState.Add("semanai", Request.QueryString("semanai"))
                    ViewState.Add("yearf", Request.QueryString("yearf"))
                    ViewState.Add("semanaf", Request.QueryString("semanaf"))
                    ViewState.Add("filtrarsemana", Request.QueryString("filtrarsemana"))

                Case "18"
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("idtienda", Request.QueryString("idtienda"))
                    ViewState.Add("idtipopersona", Request.QueryString("idtipopersona"))
                    ViewState.Add("idtipooperacion", Request.QueryString("idtipooperacion"))
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                    ViewState.Add("idMoneda", Request.QueryString("idMoneda"))
                    ViewState.Add("nprimeros", Request.QueryString("nprimeros"))
                    ViewState.Add("idlinea", Request.QueryString("idlinea"))
                    ViewState.Add("idsublinea", Request.QueryString("idsublinea"))

            End Select
        End If

        mostrarReporte()

    End Sub
    Private Sub mostrarReporte()
        Select Case ViewState.Item("iReporte").ToString
            Case "1"
                ReporteDocInventarioInicial()
            Case "2"
                ReporteFactura()
            Case "3"
                ReporteDocOrdenDespacho()
            Case "4"
                ReporteDocPedidoCliente()
            Case "5"
                ReporteDocCotizacion()
            Case "6"
                ReporteDocBoleta()
            Case "7"
                ReporteCatalogoProdPVxParametros()
            Case "8"
                ReporteDocGuiaRecepcion()
            Case "9"
                ReporteInventarioInicialxEmpresaxAlmacen()
            Case "10"
                ReporteDocGuiaRemision()
            Case "11"
                ReporteMovCaja2()
            Case "12"
                ReporteCatalogoProducto_UM()
            Case "13"
                ReporteRegistroVentas()
            Case "14"
                ReporteVentasxArticulos()
            Case "15"
                ReporteVentasxArticulosResumido()
            Case "16"
                ReporteVentasxArticulosDetalladoValorizado()
            Case "17"
                ReporteVentasxArticulosResumidoValorizado()
            Case "18"
                ReporteRankingClientesD()
            Case Else
                ReporteCatalogo()
        End Select
    End Sub
    Private Sub ReporteRegistroVentas()
        Try
            Dim obj As New Negocio.RegistroVentas
            Dim vidempresa, vidtienda, vpercepcion, vcliente, vmonBase As Integer
            vidempresa = CInt(ViewState.Item("IdEmpresa"))
            vidtienda = CInt(ViewState.Item("IdTienda"))
            vpercepcion = CInt(ViewState.Item("Percepcion"))
            vcliente = CInt(ViewState.Item("Cliente"))
            vmonBase = CInt(ViewState.Item("monBase"))

            Dim objDataTable As DataTable = obj.RegistroVentasxParams(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdTienda")), CDate(ViewState("FechaInicio")), CDate(ViewState("FechaFin")), vpercepcion, vmonBase).Tables("_CR_RegistroVentas")

            reporte = ReporteRegistroVenta(vcliente, vmonBase)
            For i As Integer = 0 To objDataTable.Rows.Count - 1
                '*********** si es anulado
                If CInt(objDataTable.Rows(i).Item("IdEstadoDoc").ToString) = 2 Then
                    With objDataTable.Rows(i)
                        .Item("doc_SubTotal") = DBNull.Value
                        .Item("doc_Igv") = DBNull.Value
                        .Item("doc_Total") = DBNull.Value
                        .Item("Percepcion") = DBNull.Value
                        .Item("Detraccion") = DBNull.Value
                        .Item("Retencion") = DBNull.Value
                        .Item("RucDni") = DBNull.Value
                        '.Item("edoc_Nombre") = "ANULADO"
                        '.Item("CondPago") = DBNull.Value
                        If vmonBase = 0 Then
                            .Item("TaPTmon") = DBNull.Value
                        End If
                        .Item("doc_TotalAPagar") = DBNull.Value

                    End With
                Else
                    With objDataTable.Rows(i)
                        .Item("edoc_Nombre") = ""
                    End With

                End If

            Next

            reporte.SetDataSource(objDataTable)
            reporte.SetParameterValue("@NomTienda", ViewState("NomTienda"))
            reporte.SetParameterValue("@NomEmpresa", ViewState("NomEmpresa"))
            reporte.SetParameterValue("@FechaInicio", ViewState("FechaInicio"))
            reporte.SetParameterValue("@FechaFin", ViewState("FechaFin"))

            CRViewer.ReportSource = reporte
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
            '''''ex.Message()

        End Try
    End Sub

    Private Function ReporteRegistroVenta(ByVal cliente As Integer, ByVal monbase As Integer) As ReportDocument
        If monbase = 0 Then
            If cliente = 0 Then
                reporte = New CR_ReporteVentas
            ElseIf monbase = 1 Then
                reporte = New CR_ReporteVentasxCliente
            Else
                reporte = New CR_ReporteVentasxCliente
            End If
        Else
            If cliente = 0 Then
                reporte = New CR_ReporteVentasMb
            ElseIf cliente = 1 Then
                reporte = New CR_ReporteVentasxClienteMb
            Else
                reporte = New CR_ReporteVentasxClienteMb
            End If

        End If
        Return reporte
    End Function
    Private Sub ReporteCatalogoProducto_UM()
        Try
            Dim objReporte As New Negocio.Reportes
            Dim ds As DataSet = objReporte.getDataSetCatalogoProducto_UM(CInt(ViewState("IdLinea")), CInt(ViewState("IdSubLinea")), CInt(ViewState("UMedida")))
            reporte = New CR_CatalogoProducto_UM
            reporte.SetDataSource(ds.Tables("DT_CatalogoProducto_UM"))
            CRViewer.ReportSource = reporte
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub ReporteMovCaja()
        Try
            Dim objReporte As New Negocio.Reportes
            Dim ds As DataSet = objReporte.getDataSetMovCajaxParams(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdTienda")), CInt(ViewState("IdCaja")), CInt(ViewState("IdMedioPago")), CInt(ViewState("IdTipoMovimiento")), CDate(ViewState("FechaInicio")), CDate(ViewState("FechaFin")))
            reporte = New CR_MovCaja

            reporte.SetDataSource(ds.Tables("DT_MovCaja"))
            reporte.SetParameterValue("@FechaInicio", ViewState("FechaInicio"))
            reporte.SetParameterValue("@FechaFin", ViewState("FechaFin"))
            CRViewer.ReportSource = reporte

        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub

    Private Sub ReporteMovCaja2()
        Try
            Dim objReporte As New Negocio.Reportes
            'Dim objTable As New DataTable

            'Dim ds As DataSet = objReporte.getDataSetMovCajaxParams2(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdTienda")), CInt(ViewState("IdCaja")), CInt(ViewState("IdMedioPago")), CInt(ViewState("IdTipoMovimiento")), CDate(ViewState("FechaInicio")), CDate(ViewState("FechaFin")))
            'objTable = objReporte.getDataSetMovCajaxParams2(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdTienda")), CInt(ViewState("IdCaja")), CInt(ViewState("IdMedioPago")), CInt(ViewState("IdTipoMovimiento")), CDate(ViewState("FechaInicio")), CDate(ViewState("FechaFin"))).Tables("DT_MovCajaFechas")

            'For i As Integer = 0 To objTable.Rows.Count - 1

            '    '*********** si es anulado

            '    If CInt(objTable.Rows(i).Item("IdEstadoDoc").ToString) = 2 Then

            '        With objTable.Rows(i)

            '            '.Item("MontoS") = DBNull.Value
            '            '.Item("MontoD") = DBNull.Value
            '            '.Item("PercSoles") = DBNull.Value
            '            '.Item("PercDolares") = DBNull.Value
            '            '.Item("mp_Nombre") = DBNull.Value
            '            '.Item("Nombre") = "ANULADO"

            '        End With
            '        'Else
            '        '    With objTable.Rows(i)
            '        '        Dim doc As Integer
            '        '        doc = CInt(.Item("CanTDoc"))
            '        '    End With

            '    End If

            'Next
            Dim caja As Boolean
            caja = CBool(ViewState.Item("caja"))
            reporte = New ReportDocument
            reporte = tipoReporteCaja(caja)
            'reporte.SetDataSource(objTable)
            reporte.SetDataSource(objReporte.getDataSetMovCajaxParams2(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdTienda")), CInt(ViewState("tipodoc")), CInt(ViewState("IdCaja")), CInt(ViewState("IdMedioPago")), CInt(ViewState("IdTipoMovimiento")), CDate(ViewState("FechaInicio")), CDate(ViewState("FechaFin"))))
            reporte.SetParameterValue("@FechaInicio", ViewState("FechaInicio"))
            reporte.SetParameterValue("@FechaFin", ViewState("FechaFin"))
            CRViewer.ReportSource = reporte

        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Function tipoReporteCaja(ByVal Caja As Boolean) As ReportDocument
        If Caja = True Then
            reporte = New CR_MovCaja2
        Else
            reporte = New CR_MovCaja2SinCaja5
        End If
        Return reporte
    End Function

    Private Sub ReporteInventarioInicialxEmpresaxAlmacen()
        Try
            Dim objReporte As New Negocio.Reportes
            Dim ds As DataSet = objReporte.getDataSetInvInicial(CInt(ViewState.Item("IdEmpresa")), CInt(ViewState.Item("IdAlmacen")), CInt(ViewState.Item("IdLinea")), CInt(ViewState.Item("IdSubLinea")))
            reporte = New CR_InventarioInicial
            reporte.SetDataSource(ds.Tables("CR_InventarioInicial"))

            CRViewer.ReportSource = reporte
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub ReporteDocGuiaRecepcion()
        Try
            reporte = New CR_DocGuiaRecepcion
            Dim objReporte As New Negocio.Reportes
            reporte.SetDataSource(objReporte.getDataSetDocGuiaRecepcion(CInt(ViewState.Item("IdDocumento"))))
            CRViewer.ReportSource = reporte
        Catch ex As Exception
            'objScript.mostrarMsjAlerta(Me, "Problemas en el reporte.")
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub ReporteDocGuiaRemision()
        Try
            reporte = New CR_DocGuiaRemision
            Dim objReporte As New Negocio.Reportes
            reporte.SetDataSource(objReporte.getDataSetDocGuiaRemision(CInt(ViewState.Item("IdDocumento"))))
            CRViewer.ReportSource = reporte
        Catch ex As Exception
            'objScript.mostrarMsjAlerta(Me, "Problemas en el reporte.")
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub

    Private Sub ReporteCatalogo()
        Dim ds As New DataSet
        objReportes = New Negocio.Catalogo
        ds = objReportes.RptCatalogo(CInt(Session("IdTienda").ToString), CInt(Session("IdSubLinea").ToString))
        reporte = New crp_Catalogo
        reporte.SetDataSource(ds)
        Me.CRViewer.ReportSource = reporte
        CRViewer.DataBind()
    End Sub
    Private Sub ReporteCatalogoProdPVxParametros()
        Dim ds As New DataSet
        objReportes = New Negocio.Catalogo
        With ViewState
            ds = objReportes.RptCatalogoProductoPVxParametros(CInt(.Item("IdTienda")), CInt(.Item("IdTipoPV")), CInt(.Item("IdLinea")), CInt(.Item("IdSubLinea")), CInt(.Item("UM_Principal")), CInt(.Item("UM_Retazo")))
        End With
        reporte = New CR_CatalogoPV
        reporte.SetDataSource(ds)
        Me.CRViewer.ReportSource = reporte
        CRViewer.DataBind()
    End Sub
    Private Sub ReporteDocInventarioInicial()
        Try
            reporte = New CR_DocInventarioInicial
            Dim objReporte As New Negocio.Reportes
            reporte.SetDataSource(objReporte.getDataSetDocInventarioInicial(CInt(ViewState.Item("IdDocumento"))))
            CRViewer.ReportSource = reporte
        Catch ex As Exception
            'objScript.mostrarMsjAlerta(Me, "Problemas en el reporte.")
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub ReporteFactura()
        Dim objReportes As Negocio.Reportes = New Negocio.Reportes
        Dim IdDoc As Integer = CInt(ViewState.Item("IdDoc"))
        Dim ds As DataSet = objReportes.getDataSetFactura(IdDoc)
        If ds IsNot Nothing Then
            reporte = New CR_Factura
            reporte.SetDataSource(ds)
            'CRViewer.ReportSource = obj
            'CRViewer.Visible = True
            'obj.PrintOptions.PrinterName = "\\digrafic01\Samsung ML-2010 Series"
            'obj.PrintToPrinter(1, True, 1, 1)
            With CRViewer
                .ReportSource = reporte
                ' .DataBind()
            End With
            ' If Me.CRViewer.PrintMode
        Else
            'MsgBox1.ShowMessage("Problemas en la carga de datos del Reporte.")
        End If
        'Response.Redirect("~/Ventas/FrmEmitirDocumentos.aspx")
    End Sub
    Private Sub ReporteDocOrdenDespacho()
        Try
            reporte = New CR_DocOrdenDespacho
            Dim objReporte As New Negocio.Reportes
            reporte.SetDataSource(objReporte.getDataSetOrdenDespacho(CInt(ViewState.Item("IdDoc"))))
            CRViewer.ReportSource = reporte
        Catch ex As Exception
            'objScript.mostrarMsjAlerta(Me, "Problemas en el reporte.")
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub ReporteDocPedidoCliente()
        Try
            reporte = New CR_DocPedido
            Dim objReporte As New Negocio.Reportes
            reporte.SetDataSource(objReporte.getDataSetPedidoCliente(CInt(ViewState.Item("IdDoc"))))
            CRViewer.ReportSource = reporte
        Catch ex As Exception
            'objScript.mostrarMsjAlerta(Me, "Problemas en el reporte.")
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub ReporteDocCotizacion()
        Try
            reporte = New CR_DocCotizacion
            Dim objReporte As New Negocio.Reportes
            reporte.SetDataSource(objReporte.getDataSetCotizacion(CInt(ViewState.Item("IdDoc"))))
            CRViewer.ReportSource = reporte
        Catch ex As Exception
            'objScript.mostrarMsjAlerta(Me, "Problemas en el reporte.")
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub ReporteDocBoleta()
        Try
            reporte = New CR_DocBoleta
            Dim objReporte As New Negocio.Reportes
            reporte.SetDataSource(objReporte.getDataSetBoleta(CInt(ViewState.Item("IdDoc"))))
            CRViewer.ReportSource = reporte
        Catch ex As Exception
            'objScript.mostrarMsjAlerta(Me, "Problemas en el reporte.")
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Function TipoReporteResumidoQT(ByVal idempresa As Integer, ByVal idTienda As Integer) As ReportDocument

        If (idempresa <> 0) Then
            If (idTienda <> 0) Then '17
                reporte = New CR_VentasPorArticuloResumido
            Else '18
                reporte = New CR_VentasPorArticuloResumidoE
            End If
        Else
            If (idTienda <> 0) Then '17
                reporte = New CR_VentasPorArticuloResumidoT
            Else '18
                reporte = New CR_VentasPorArticuloResumidoN
            End If
        End If
        Return reporte
    End Function
    Private Function TipoReporteDetalladoQT(ByVal idempresa As Integer, ByVal idTienda As Integer) As ReportDocument

        If (idempresa <> 0) Then
            If (idTienda <> 0) Then '17
                reporte = New CR_VentasPorArticulo
            Else '18
                reporte = New CR_VentasPorArticuloE
            End If
        Else
            If (idTienda <> 0) Then '17
                reporte = New CR_VentasPorArticuloT
            Else '18
                reporte = New CR_VentasPorArticuloN
            End If
        End If
        Return reporte
    End Function
    Private Sub ReporteVentasxArticulos()
        Try

            '14

            Dim vidempresa, vidtienda, vlinea, vsublinea, vprod As Integer
            If CStr(ViewState.Item("IdEmpresa")) = "" Then
                vidempresa = 0
            Else
                vidempresa = CInt(ViewState.Item("IdEmpresa"))
            End If

            If CStr(ViewState.Item("idtienda")) = "" Then
                vidtienda = 0
            Else
                vidtienda = CInt(ViewState.Item("idtienda"))
            End If

            If CStr(ViewState.Item("idlinea")) = "" Then
                vlinea = 0
            Else
                vlinea = CInt(ViewState.Item("idlinea"))
            End If
            If CStr(ViewState.Item("idSublinea")) = "" Then
                vsublinea = 0
            Else
                vsublinea = CInt(ViewState.Item("idSublinea"))
            End If
            If CStr(ViewState.Item("idproducto")) = "" Then
                vprod = 0
            Else
                vprod = CInt(ViewState.Item("idproducto"))
            End If

            Dim yeari, yearf, semanai, semanaf, filtrarsemana As Integer

            If CStr(ViewState.Item("yeari")) = "" Then
                yeari = 0
            Else
                yeari = CInt(ViewState.Item("yeari"))
            End If

            If CStr(ViewState.Item("semanai")) = "" Then
                semanai = 0
            Else
                semanai = CInt(ViewState.Item("semanai"))
            End If

            If CStr(ViewState.Item("yearf")) = "" Then
                yearf = 0
            Else
                yearf = CInt(ViewState.Item("yearf"))
            End If

            If CStr(ViewState.Item("semanaf")) = "" Then
                semanaf = 0
            Else
                semanaf = CInt(ViewState.Item("semanaf"))
            End If

            If CStr(ViewState.Item("filtrarsemana")) = "" Then
                filtrarsemana = 0
            Else
                filtrarsemana = CInt(ViewState.Item("filtrarsemana"))
            End If

            Dim ds As New DataSet
            Dim objReporte As New Negocio.VentasPorArticuloView

            ds = objReporte.CRVentasPorArticulos(CInt(vidempresa), CInt(vidtienda), CInt(vlinea), CInt(vsublinea), CInt(vprod), CStr((ViewState.Item("FechaInicio"))), CStr((ViewState.Item("FechaFin"))), yeari, semanai, yearf, semanaf, filtrarsemana)

            reporte = TipoReporteDetalladoQT(vidempresa, vidtienda)
            reporte.SetDataSource(ds)
            reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
            reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
            'reporte.SetParameterValue("TextEmpresa", (ViewState("TextEmpresa")))
            'reporte.SetParameterValue("TextTienda", (ViewState("TextTienda")))

            CRViewer.ReportSource = reporte
            CRViewer.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub
    Private Sub ReporteVentasxArticulosResumido()
        Try
            '15
            Dim vidempresa, vidtienda, vlinea, vsublinea, vprod As Integer
            If CStr(ViewState.Item("IdEmpresa")) = "" Then
                vidempresa = 0
            Else
                vidempresa = CInt(ViewState.Item("IdEmpresa"))
            End If

            If CStr(ViewState.Item("idtienda")) = "" Then
                vidtienda = 0
            Else
                vidtienda = CInt(ViewState.Item("idtienda"))
            End If

            If CStr(ViewState.Item("idlinea")) = "" Then
                vlinea = 0
            Else
                vlinea = CInt(ViewState.Item("idlinea"))
            End If
            If CStr(ViewState.Item("idSublinea")) = "" Then
                vsublinea = 0
            Else
                vsublinea = CInt(ViewState.Item("idSublinea"))
            End If
            If CStr(ViewState.Item("idProducto")) = "" Then
                vprod = 0
            Else
                vprod = CInt(ViewState.Item("idProducto"))
            End If

            Dim yeari, yearf, semanai, semanaf, filtrarsemana As Integer

            If CStr(ViewState.Item("yeari")) = "" Then
                yeari = 1900
            Else
                yeari = CInt(ViewState.Item("yeari"))
            End If

            If CStr(ViewState.Item("semanai")) = "" Then
                semanai = 12
            Else
                semanai = CInt(ViewState.Item("semanai"))
            End If

            If CStr(ViewState.Item("yearf")) = "" Then
                yearf = 20100
            Else
                yearf = CInt(ViewState.Item("yearf"))
            End If

            If CStr(ViewState.Item("semanaf")) = "" Then
                semanaf = 12
            Else
                semanaf = CInt(ViewState.Item("semanaf"))
            End If

            If CStr(ViewState.Item("filtrarsemana")) = "" Then
                filtrarsemana = 0
            Else
                filtrarsemana = CInt(ViewState.Item("filtrarsemana"))
            End If


            Dim ds As New DataSet
            Dim objReporte As New Negocio.VentasPorArticuloView
            ds = objReporte.CRVentasPorArticulosResumido(CInt(vidempresa), CInt(vidtienda), CInt(vlinea), CInt(vsublinea), CInt(vprod), CStr((ViewState.Item("FechaInicio"))), CStr((ViewState.Item("FechaFin"))), yeari, semanai, yearf, semanaf, filtrarsemana)

            reporte = TipoReporteResumidoQT(vidempresa, vidtienda)
            reporte.SetDataSource(ds)
            reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
            reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
            'reporte.SetParameterValue("TextEmpresa", (ViewState("TextEmpresa")))
            'reporte.SetParameterValue("TextTienda", (ViewState("TextTienda")))
            CRViewer.ReportSource = reporte
            CRViewer.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub
    Private Sub ReporteVentasxArticulosDetalladoValorizado()
        Try
            Dim vidempresa, vidtienda, vlinea, vsublinea, vprod As Integer
            If CStr(ViewState.Item("IdEmpresa")) = "" Then
                vidempresa = 0
            Else
                vidempresa = CInt(ViewState.Item("IdEmpresa"))
            End If

            If CStr(ViewState.Item("idtienda")) = "" Then
                vidtienda = 0
            Else
                vidtienda = CInt(ViewState.Item("idtienda"))
            End If

            If CStr(ViewState.Item("idlinea")) = "" Then
                vlinea = 0
            Else
                vlinea = CInt(ViewState.Item("idlinea"))
            End If
            If CStr(ViewState.Item("idSublinea")) = "" Then
                vsublinea = 0
            Else
                vsublinea = CInt(ViewState.Item("idSublinea"))
            End If
            If CStr(ViewState.Item("idproducto")) = "" Then
                vprod = 0
            Else
                vprod = CInt(ViewState.Item("idproducto"))
            End If

            Dim yeari, yearf, semanai, semanaf, filtrarsemana As Integer

            If CStr(ViewState.Item("yeari")) = "" Then
                yeari = 0
            Else
                yeari = CInt(ViewState.Item("yeari"))
            End If

            If CStr(ViewState.Item("semanai")) = "" Then
                semanai = 0
            Else
                semanai = CInt(ViewState.Item("semanai"))
            End If

            If CStr(ViewState.Item("yearf")) = "" Then
                yearf = 0
            Else
                yearf = CInt(ViewState.Item("yearf"))
            End If

            If CStr(ViewState.Item("semanaf")) = "" Then
                semanaf = 0
            Else
                semanaf = CInt(ViewState.Item("semanaf"))
            End If

            If CStr(ViewState.Item("filtrarsemana")) = "" Then
                filtrarsemana = 0
            Else
                filtrarsemana = CInt(ViewState.Item("filtrarsemana"))
            End If


            Dim ds As New DataSet
            Dim objReporte As New Negocio.VentasPorArticuloView
            'Dim ds As DataSet = objReporte.CRVentasPorArticulos(CInt(vidempresa), CInt(vidtienda), CInt(vlinea), CInt(vsublinea), CInt(vprod), CStr((ViewState.Item("FechaInicio"))), CStr((ViewState.Item("FechaFin"))))
            ds = objReporte.CRVentasPorArticulosDetallladoValorizado(CInt(vidempresa), CInt(vidtienda), CInt(ViewState.Item("IdTipoExistencia")), CInt(vlinea), CInt(vsublinea), CInt(vprod), CStr((ViewState.Item("FechaInicio"))), CStr((ViewState.Item("FechaFin"))), yeari, semanai, yearf, semanaf, filtrarsemana)

            reporte = TipoReporteDetallado(vidempresa, vidtienda)
            reporte.SetDataSource(ds)

            If filtrarsemana = 1 Then

                Dim Ini As String = CStr(ViewState("yeari")) + " Semana " + CStr(ViewState("semanai"))
                Dim Fin As String = CStr(ViewState("yearf")) + " Semana " + CStr(ViewState("semanaf"))

                reporte.SetParameterValue("@FechaInicio", Ini)
                reporte.SetParameterValue("@FechaFin", Fin)

            Else
                reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
                reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
            End If

            CRViewer.ReportSource = reporte
            CRViewer.DataBind()


        Catch ex As Exception
            'objScript.mostrarMsjAlerta(Me, "Problemas en el reporte.")
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub
    Private Sub ReporteVentasxArticulosResumidoValorizado()
        Try
            Dim vidempresa, vidtienda, vlinea, vsublinea, vprod As Integer
            If CStr(ViewState.Item("IdEmpresa")) = "" Then
                vidempresa = 0
            Else
                vidempresa = CInt(ViewState.Item("IdEmpresa"))
            End If

            If CStr(ViewState.Item("idtienda")) = "" Then
                vidtienda = 0
            Else
                vidtienda = CInt(ViewState.Item("idtienda"))
            End If

            If CStr(ViewState.Item("idlinea")) = "" Then
                vlinea = 0
            Else
                vlinea = CInt(ViewState.Item("idlinea"))
            End If
            If CStr(ViewState.Item("idSublinea")) = "" Then
                vsublinea = 0
            Else
                vsublinea = CInt(ViewState.Item("idSublinea"))
            End If
            If CStr(ViewState.Item("idProducto")) = "" Then
                vprod = 0
            Else
                vprod = CInt(ViewState.Item("idProducto"))
            End If

            Dim yeari, yearf, semanai, semanaf, filtrarsemana As Integer

            If CStr(ViewState.Item("yeari")) = "" Then
                yeari = 0
            Else
                yeari = CInt(ViewState.Item("yeari"))
            End If

            If CStr(ViewState.Item("semanai")) = "" Then
                semanai = 0
            Else
                semanai = CInt(ViewState.Item("semanai"))
            End If

            If CStr(ViewState.Item("yearf")) = "" Then
                yearf = 0
            Else
                yearf = CInt(ViewState.Item("yearf"))
            End If

            If CStr(ViewState.Item("semanaf")) = "" Then
                semanaf = 0
            Else
                semanaf = CInt(ViewState.Item("semanaf"))
            End If

            If CStr(ViewState.Item("filtrarsemana")) = "" Then
                filtrarsemana = 0
            Else
                filtrarsemana = CInt(ViewState.Item("filtrarsemana"))
            End If


            Dim ds As New DataSet

            Dim objReporte As New Negocio.VentasPorArticuloView
            ds = objReporte.CRVentasPorArticulosResumidoValorizado(CInt(vidempresa), CInt(vidtienda), CInt(vlinea), CInt(vsublinea), CInt(vprod), CStr((ViewState.Item("FechaInicio"))), CStr((ViewState.Item("FechaFin"))), yeari, semanai, yearf, semanaf, filtrarsemana)

            reporte = Me.TipoReporteResumido(vidempresa, vidtienda)
            reporte.SetDataSource(ds)

            If filtrarsemana = 1 Then

                Dim Ini As String = CStr(ViewState("yeari")) + " Semana " + CStr(ViewState("semanai"))
                Dim Fin As String = CStr(ViewState("yearf")) + " Semana " + CStr(ViewState("semanaf"))

                reporte.SetParameterValue("@FechaInicio", Ini)
                reporte.SetParameterValue("@FechaFin", Fin)

            Else
                reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
                reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
            End If

            CRViewer.ReportSource = reporte
            CRViewer.DataBind()

        Catch ex As Exception
            'objScript.mostrarMsjAlerta(Me, "Problemas en el reporte.")
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub
    Private Function TipoReporteResumido(ByVal idempresa As Integer, ByVal idTienda As Integer) As ReportDocument

        If (idempresa <> 0) Then
            If (idTienda <> 0) Then '17
                reporte = New CR_VentasPorArticuloResumidoValorizado
            Else '18
                reporte = New CR_VentasPorArticuloResumidoValorizadoE
            End If
        Else
            If (idTienda <> 0) Then '17
                reporte = New CR_VentasPorArticuloResumidoValorizadoT
            Else '18
                reporte = New CR_VentasPorArticuloResumidoValorizadoN
            End If
        End If
        Return reporte
    End Function
    Private Function TipoReporteDetallado(ByVal idempresa As Integer, ByVal idTienda As Integer) As ReportDocument

        If (idempresa <> 0) Then
            If (idTienda <> 0) Then '17
                reporte = New CR_VentasPorArticuloDetalladoValorizado4
            Else '18
                reporte = New CR_VentasPorArticuloDetalladoValorizadoEE
            End If
        Else
            If (idTienda <> 0) Then '17
                reporte = New CR_VentasPorArticuloDetalladoValorizadoTT
            Else '18
                reporte = New CR_VentasPorArticuloDetalladoValorizadoNN
            End If
        End If
        Return reporte
    End Function
    Private Sub ReporteRankingClientesD()
        Try
            Dim vidempresa, vidtienda, vtipopersona, vtipooperacion, vidMoneda, nprimeros As Integer
            If CStr(ViewState.Item("IdEmpresa")) = "" Then
                vidempresa = 0
            Else
                vidempresa = CInt(ViewState.Item("IdEmpresa"))
            End If

            If CStr(ViewState.Item("idtienda")) = "" Then
                vidtienda = 0
            Else
                vidtienda = CInt(ViewState.Item("idtienda"))
            End If

            If CStr(ViewState.Item("idtipopersona")) = "" Then
                vtipopersona = 0
            Else
                vtipopersona = CInt(ViewState.Item("idtipopersona"))
            End If

            If CStr(ViewState.Item("idtipooperacion")) = "" Then
                vtipooperacion = 0
            Else
                vtipooperacion = CInt(ViewState.Item("idtipooperacion"))
            End If

            If CStr(ViewState.Item("idMoneda")) = "" Then
                vidMoneda = 0
            Else
                vidMoneda = CInt(ViewState.Item("idMoneda"))
            End If
            Dim vidlinea, vidsublinea As Integer




            If CStr(ViewState.Item("idlinea")) = "" Then
                vidlinea = 0
            Else
                vidlinea = CInt(ViewState.Item("idlinea"))
            End If
            If CStr(ViewState.Item("idsublinea")) = "" Then
                vidsublinea = 0
            Else
                vidsublinea = CInt(ViewState.Item("idsublinea"))
            End If


            If CStr(ViewState.Item("nprimeros")) = "" Then
                nprimeros = 0
            Else
                nprimeros = CInt(ViewState.Item("nprimeros"))
            End If

            Dim ds As New DataSet
            Dim objReporte As New Negocio.Cliente
            ds = objReporte.RankingClientes(CInt(vidempresa), CInt(vidtienda), CInt(vtipooperacion), CInt(vidMoneda), CInt(vtipopersona), CStr((ViewState.Item("FechaInicio"))), CStr((ViewState.Item("FechaFin"))), nprimeros, vidlinea, vidsublinea)
            'Dim reporte As ReportDocument
            'reporte = Me.TipoReporteRanking(vidempresa, vidtienda)
            reporte = New CR_RankingClientes
            reporte.SetDataSource(ds)
            reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
            reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
            CRViewer.ReportSource = reporte
            CRViewer.DataBind()

        Catch ex As Exception
            'objScript.mostrarMsjAlerta(Me, "Problemas en el reporte.")
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub
    Private Function TipoReporteRanking(ByVal idempresa As Integer, ByVal idTienda As Integer) As ReportDocument

        If (idempresa <> 0) Then
            If (idTienda <> 0) Then '
                reporte = New CR_RankingClientes
            Else '
                reporte = New CR_RankingClientes2
            End If
        Else
            If (idTienda <> 0) Then '
                reporte = New CR_RankingClientes3
            Else '
                reporte = New CR_RankingClientes4
            End If
        End If
        Return reporte
    End Function

    Private Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        reporte.Close()
        reporte.Dispose()
    End Sub
End Class