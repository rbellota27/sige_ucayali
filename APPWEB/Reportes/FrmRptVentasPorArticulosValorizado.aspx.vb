﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



Partial Public Class FrmRptVentasPorArticulosValorizado
    Inherits System.Web.UI.Page
    Private objscript As New ScriptManagerClass
    Private objCombo As New Combo
    Private objNegUsu As New Negocio.UsuarioView
    Private listaDetalle As List(Of Entidades.DetalleDocumentoView)
    Private listaDetalleRelacionado As List(Of Entidades.DetalleDocumento)
    Private listaDetalleDocRel_AddProd As List(Of Entidades.DetalleDocumento)
    Private listaProductoView As List(Of Entidades.ProductoView)
    Private listaDetalleProductov As List(Of Entidades.ProductoView)
    Private listaDocumentoRef As List(Of Entidades.Documento)
    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFrm()
        End If
    End Sub
    Private Sub inicializarFrm()
        Try
            btnMostrarReporte.Visible = True
            objCombo.LlenarCboPropietario(Me.cmbEmpresa, True)
            objCombo.LLenarCboTienda(Me.cmbTienda, True)
            'objCombo.LlenarCboTiendaxIdUsuario(Me.cmbTienda, , True)

            If objNegUsu.FunVerTodasTiendas(CInt(Session("IdUsuario")), 6) > 0 Then
                objCombo.LlenarCboTiendaxIdUsuario(Me.cmbTienda, CInt(Session("IdUsuario")), True)
            Else
                objCombo.LlenarCboTiendaxIdUsuario(Me.cmbTienda, CInt(Session("IdUsuario")), False)
            End If

            objCombo.LlenarCboYear(Me.idYearI, False)
            objCombo.LlenarCboSemanaOfYear(Me.idSemanaI, False)
            objCombo.LlenarCboYear(Me.idYearF, False)
            objCombo.LlenarCboSemanaOfYear(Me.idSemanaF, False)

            objCombo.llenarCboTipoExistencia(Me.cboTipoExistencia, True)
            objCombo.llenarCboLineaxTipoExistencia(Me.cmbLinea1, CInt(Me.cboTipoExistencia.SelectedValue), True)
            objCombo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea1, CInt(Me.cmbLinea1.SelectedValue), CInt(Me.cboTipoExistencia.SelectedValue), True)


            objCombo.LlenarCboLinea(Me.cmbLinea_AddProd, True)
            objCombo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            objCombo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)


            
            Dim FechaActual As Date = (New Negocio.FechaActual).SelectFechaActual
            txtFechaInicio.Text = Format(FechaActual, "dd/MM/yyyy")
            txtFechaFin.Text = Format(FechaActual, "dd/MM/yyyy")
            Me.hddFechaActual.Value = CStr(Format(FechaActual, "dd/MM/yyyy"))
            Me.cmbTienda.Enabled = True
            mostrarBtnProd(False)

            actualizarProgramacionSemana(FechaActual)

        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub

    Private Sub actualizarProgramacionSemana(ByVal Fecha As Date)

        Dim objCbo As New Combo
        Dim objProgramacionSemana As Entidades.ProgramacionPedido = (New Negocio.ProgramacionPedido).ProgramacionPedidoSelectxFecha(Fecha)

        If (objProgramacionSemana IsNot Nothing) Then
            If (Me.idYearI.Items.FindByValue(CStr(objProgramacionSemana.IdYear)) IsNot Nothing) Then
                Me.idYearI.SelectedValue = CStr(objProgramacionSemana.IdYear)
                objCbo.LlenarCboSemanaxIdYear(Me.idSemanaI, CInt(Me.idYearI.SelectedValue), False)
            End If

            If (Me.idSemanaI.Items.FindByValue(CStr(objProgramacionSemana.IdSemana)) IsNot Nothing) Then
                Me.idSemanaI.SelectedValue = CStr(objProgramacionSemana.IdSemana)
            End If

            If (Me.idYearF.Items.FindByValue(CStr(objProgramacionSemana.IdYear)) IsNot Nothing) Then
                Me.idYearF.SelectedValue = CStr(objProgramacionSemana.IdYear)
                objCbo.LlenarCboSemanaxIdYear(Me.idSemanaI, CInt(Me.idYearF.SelectedValue), False)
            End If

            If (Me.idSemanaF.Items.FindByValue(CStr(objProgramacionSemana.IdSemana)) IsNot Nothing) Then
                Me.idSemanaF.SelectedValue = CStr(objProgramacionSemana.IdSemana)
            End If


        End If


    End Sub


    'oculta Mo_Con_Detallado
    Private Sub RadioButtonList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioButtonList1.SelectedIndexChanged
        If (Me.RadioButtonList1.SelectedIndex = 0) Then
            'resumido no muestra la capa
            mostrarBtnProd(False)
            limpiarDetalleDocumento()
            ocultarLS(True)

        Else
            'detallado 
            mostrarBtnProd(True)
            ocultarLS(False)
            limpiatxt()

        End If
    End Sub
    Private Sub mostrarBtnProd(ByVal flag As Boolean)
        btnBProducto.Visible = flag
        
    End Sub
    Private Sub limpiatxt()
        
    End Sub

    Private Sub cmbLinea1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea1.SelectedIndexChanged
        objCombo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea1, CInt(Me.cmbLinea1.SelectedValue), CInt(Me.cboTipoExistencia.SelectedValue), True)
    End Sub


    'Private Sub btnBuscarProducto_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarProducto.Click
    '    buscarProductoXDescripccion()
    'End Sub
    'Private Sub buscarProductoXDescripccion()
    '    Try
    '        Dim nProducto As New Negocio.Producto
    '        Dim idLinea As Integer = 0
    '        Dim idSubLinea As Integer = 0
    '        If cmbLinea1.SelectedValue <> "0" Then idLinea = CInt(cmbLinea1.SelectedValue)
    '        If cmbSubLinea1.SelectedValue <> "0" Then idSubLinea = CInt(cmbSubLinea1.SelectedValue)
    '        Dim lista As List(Of Entidades.GrillaProducto_M) = nProducto.SelectUMPrincipalxIdSubLinea(idLinea, idSubLinea, txtDescripcionProducto.Text.Trim)
    '        DGVproductos.DataSource = lista
    '        DGVproductos.DataBind()
    '        Me.txtDescripcionProducto.Focus()
    '        If lista.Count = 0 Then
    '            objscript.mostrarMsjAlerta(Me, "No se hallaron registros")
    '        End If
    '    Catch ex As Exception
    '        objscript.mostrarMsjAlerta(Me, "Problemas en la búsqueda de productos.")
    '    End Try
    'End Sub
    ' Private Sub DGVproductos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVproductos.SelectedIndexChanged
    '    Session("idproductoValorizado") = CStr(Me.DGVproductos.SelectedRow.Cells(1).Text)
    '    Me.txtidProducto.Text = CStr(Me.DGVproductos.SelectedRow.Cells(1).Text)
    '    Me.txtProducto.Text = CStr(Me.DGVproductos.SelectedRow.Cells(2).Text)
    'End Sub

    Private Sub ocultarLS(ByVal flag As Boolean)

        Me.cmbLinea1.Enabled = flag
        Me.cmbSubLinea1.Enabled = flag
    End Sub

#Region "************************** BUSQUEDA PRODUCTO"
    Private Sub btnBuscarGrilla_AddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarGrilla_AddProd.Click
        BuscarProductoCatalogo(0)
    End Sub

    Protected Sub BuscarProductoCatalogo(ByVal opcion As Integer)
        Try

            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar

                    ViewState.Add("nombre_BuscarProducto", txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)
                    'ViewState.Add("IdAlmacen_BuscarProducto", Me.cboAlmacen.SelectedValue)
                    'ViewState.Add("IdEmpresa_BuscarProducto", Me.cboEmpresa.SelectedValue)
                    ViewState.Add("CodigoSubLinea_BuscarProducto", "")

            End Select

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")))

        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal codigoSubLinea As String, ByVal nomProducto As String, ByVal IdAlmacen As Integer, ByVal tipoMov As Integer, ByVal IdEmpresa As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select

        Dim codigoProducto As String = Me.txtCodigoProducto.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()
        'Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, CInt(Me.cboPais.SelectedValue), CInt(Me.cboFabricante.SelectedValue), CInt(Me.cboMarca.SelectedValue), CInt(Me.cboModelo.SelectedValue), CInt(Me.cboFormato.SelectedValue), CInt(Me.cboProveedor.SelectedValue), CInt(Me.cboEstilo.SelectedValue), CInt(Me.cboTransito.SelectedValue), CInt(Me.cboCalidad.SelectedValue))
        Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams_V2reporte(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, codigoProducto, tableTipoTabla)


        If listaProductoView.Count = 0 Then

            grilla.DataSource = Nothing
            grilla.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');    alert('No se hallaron registros.');        ", True)
            Return

        Else

            grilla.DataSource = listaProductoView
            grilla.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');           ", True)

        End If

    End Sub
    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            0, 1, CInt(ViewState("IdEmpresa_BuscarProducto")))
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            0, 2, CInt(ViewState("IdEmpresa_BuscarProducto")))
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            0, 3, CInt(ViewState("IdEmpresa_BuscarProducto")))
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub mostrarCapaStockPrecioxProducto(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            'Dim gvRow As GridViewRow = CType(CType(sender, ImageButton).NamingContainer, GridViewRow)
            'Me.lblProductoConsultarStockPrecioxProd.Text = HttpUtility.HtmlDecode(gvRow.Cells(2).Text)
            'Dim IdProducto As Integer = CInt(CType(gvRow.FindControl("hddIdProducto_Find"), HiddenField).Value)

            'Me.GV_ConsultarStockPrecioxProd.DataSource = (New Negocio.Catalogo).Producto_Consultar_Stock_Precio_Venta(IdProducto, 0, 0, 0, (New Negocio.FechaActual).SelectFechaActual, CInt(Me.cboEmpresa.SelectedValue))
            'Me.GV_ConsultarStockPrecioxProd.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa2('capaConsultarStockPrecioxProducto');   onCapa('capaBuscarProducto_AddProd');   ", True)

        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)


            objscript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region
#Region "************************** BUSQUEDA AVANZADA PRODUCTOS"

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()

            objscript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Protected Sub M_B_cmbSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objscript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()
            objscript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1

            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)

        Next

        Return dt

    End Function
#End Region

#Region "Add Producto"

    Private Sub DGV_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_AddProd.SelectedIndexChanged
        hddidproducto.Value = CStr(CType((Me.DGV_AddProd.Rows(DGV_AddProd.SelectedIndex).Cells(4).FindControl("hdfIdproducto")), HiddenField).Value)
        addProductoGrilla()

        If Not IsNothing(Me.cboTipoExistencia.Items.FindByValue("1")) Then
            Me.cboTipoExistencia.SelectedValue = "1"
        End If

        If Not IsNothing(Me.cmbLinea1.Items.FindByValue("0")) Then
            Me.cmbLinea1.SelectedValue = "0"
        End If

        If Not IsNothing(Me.cmbSubLinea1.Items.FindByValue("0")) Then
            Me.cmbSubLinea1.SelectedValue = "0"
        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "CierraCapaProd();", True)

    End Sub

    Private Sub setListaProductoView(ByVal lista As List(Of Entidades.ProductoView))
        'lista = New List(Of Entidades.ProductoView)
        Session.Remove("listaProductoView")
        Session.Add("listaProductoView", lista)
    End Sub
    Private Function getListaProductoView() As List(Of Entidades.ProductoView)
        Return CType(Session.Item("listaProductoView"), List(Of Entidades.ProductoView))
    End Function
    Private Sub addProductoGrilla()
        Try
            Dim lista As List(Of Entidades.ProductoView)
            If getListaProductoView() Is Nothing Then
                lista = New List(Of Entidades.ProductoView)
            Else
                lista = New List(Of Entidades.ProductoView)
            End If

            GV_Detalle.DataBind()

            Dim i As Integer = 0
            i = DGV_AddProd.SelectedIndex
            With DGV_AddProd.Rows(i)

                Dim obj As New Entidades.ProductoView
                With obj

                    .Id = CInt(CType((Me.DGV_AddProd.Rows(i).FindControl("hdfIdproducto")), HiddenField).Value)
                    .Prod_Codigo = HttpUtility.HtmlDecode(CStr(Me.DGV_AddProd.Rows(i).Cells(1).Text))
                    .Descripcion = HttpUtility.HtmlDecode(CStr(Me.DGV_AddProd.Rows(i).Cells(2).Text))
                    .UnidadMedida = HttpUtility.HtmlDecode(CStr(Me.DGV_AddProd.Rows(i).Cells(3).Text))

                End With
                lista.Add(obj)

            End With

            GV_Detalle.DataSource = lista
            GV_Detalle.DataBind()



        Catch ex As Exception
        Finally
        End Try
    End Sub

#End Region

#Region "Limpiar"
    Protected Sub btnLimpiarDetalleDocumento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarDetalleDocumento.Click
        limpiarDetalleDocumento()
    End Sub
    Private Sub limpiarDetalleDocumento()
        Try

            Me.listaDetalleProductov = New List(Of Entidades.ProductoView)

            setListaProductoView(Me.listaDetalleProductov)

            Me.GV_Detalle.DataSource = Nothing
            Me.GV_Detalle.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#End Region


    Private Sub cboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoExistencia.SelectedIndexChanged
        objCombo.llenarCboLineaxTipoExistencia(Me.cmbLinea1, CInt(Me.cboTipoExistencia.SelectedValue), True)
        objCombo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea1, CInt(Me.cmbLinea1.SelectedValue), CInt(Me.cboTipoExistencia.SelectedValue), True)
    End Sub
End Class