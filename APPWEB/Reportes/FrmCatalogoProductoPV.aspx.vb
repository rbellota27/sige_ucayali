﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmCatalogoProductoPV
    Inherits System.Web.UI.Page
    Dim objScript As New ScriptManagerClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFrm()
        End If
    End Sub
    Private Sub inicializarFrm()
        Try
            cargarDatosTienda(Me.cmbTienda)
            cargarDatosTipoPrecioPV(Me.cmbTipoPrecioVenta_SubLinea)
            cargarDatosLinea(Me.cmbLinea, "B")
            cargarDatosSubLinea(Me.cmbSubLinea, CInt(cmbLinea.SelectedValue), "B")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub
    Private Sub cargarDatosTienda(ByVal combo As DropDownList)
        Dim nTienda As New Negocio.Tienda
        Dim lista As List(Of Entidades.Tienda) = nTienda.SelectCbo
        Dim obj As New Entidades.Tienda
        obj.Id = 0
        obj.Nombre = "-----"
        lista.Insert(0, obj)
        combo.DataSource = lista
        combo.DataBind()
    End Sub
    Private Sub cargarDatosTipoPrecioPV(ByVal combo As DropDownList)
        Dim nTipoPrecioV As New Negocio.TipoPrecioV
        Dim lista As List(Of Entidades.TipoPrecioV) = nTipoPrecioV.SelectCbo
        Dim obj As New Entidades.TipoPrecioV
        obj.IdTipoPv = 0
        obj.Nombre = "-----"
        lista.Insert(0, obj)
        combo.DataSource = lista
        combo.DataBind()
    End Sub
    Private Sub cargarDatosLinea(ByVal combo As DropDownList, Optional ByVal opc As String = "")
        Dim nLinea As New Negocio.Linea
        Dim lista As List(Of Entidades.Linea) = nLinea.SelectAllActivo()
        If opc = "B" Then
            Dim obj As New Entidades.Linea
            obj.Id = 0
            obj.Descripcion = "-----"
            lista.Insert(0, obj)
        End If
        combo.DataSource = lista
        combo.DataBind()
    End Sub
    Private Sub cargarDatosSubLinea(ByVal combo As DropDownList, ByVal idLinea As Integer, Optional ByVal opc As String = "")
        Dim nSubLinea As New Negocio.SubLinea
        Dim lista As List(Of Entidades.SubLinea) = nSubLinea.SelectAllActivoxLinea(idLinea)
        If opc = "B" Then
            Dim obj As New Entidades.SubLinea
            obj.Id = 0
            obj.Nombre = "-----"
            lista.Insert(0, obj)
        End If
        combo.DataSource = lista
        combo.DataBind()
    End Sub

    Protected Sub cmbLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbLinea.SelectedIndexChanged
        Try
            cargarDatosSubLinea(Me.cmbSubLinea, CInt(cmbLinea.SelectedValue), "B")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub
End Class