<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmAperturaCierre_Caja.aspx.vb" Inherits="APPWEB.FrmAperturaCierre_Caja" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td class="TituloCelda">
                APERTURA / CIERRE DE CAJA
            </td>
        </tr>
        <tr align="center">
            <td>
                <table>
                    <tr>
                        <td class="Texto" style="font-weight: bold; text-align: right">
                            Empresa:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto" style="font-weight: bold; text-align: right">
                            Tienda:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto" style="font-weight: bold; text-align: right">
                            Caja:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboCaja" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto" style="font-weight: bold; text-align: right">
                            Fecha:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaMov" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                Width="100px"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaMov" runat="server" ClearMaskOnLostFocus="false"
                                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaMov">
                            </cc1:MaskedEditExtender>
                            <cc1:CalendarExtender ID="CalendarExtender_txtFechaMov" runat="server" Enabled="True"
                                Format="dd/MM/yyyy" TargetControlID="txtFechaMov">
                            </cc1:CalendarExtender>
                        </td>
                        <td>
                            <asp:Button ID="btnAceptar_MovCaja" runat="server" Text="Aceptar" Width="80px" ToolTip="Aceptar" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancelar_Apertura" Width="80px" runat="server" Text="Cancelar"
                                ToolTip="Cancelar" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="9">
                            <table>
                                <tr>
                                    <td>
                                        <asp:RadioButtonList ID="rdbOpcion_MovCaja" runat="server" RepeatDirection="Horizontal"
                                            CssClass="Texto" Font-Bold="true">
                                            <asp:ListItem Value="A" Selected="True">Aperturar caja</asp:ListItem>
                                            <asp:ListItem Value="C">Cerrar caja</asp:ListItem>
                                            <asp:ListItem Value="A">Reapertura</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnRegistrar" Width="80px" runat="server" Text="Registrar" ToolTip="Registrar"
                                            OnClientClick=" return ( onClick_Registrar() ); " />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_CajaAperturaCierre" runat="server">
                    <table width="100%">
                        <tr>
                            <td style="text-align: center">
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Ingrese Monto:
                                        </td>
                                        <td>
                                            <asp:GridView ID="GV_MontoApertura" runat="server" AutoGenerateColumns="False" Width="200px">
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <Columns>
                                                    <asp:BoundField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="25px"
                                                        HeaderText="" DataField="Simbolo" ItemStyle-HorizontalAlign="Center" ItemStyle-ForeColor="Red"
                                                        ItemStyle-Font-Bold="true">
                                                        <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                                        <ItemStyle Font-Bold="True" ForeColor="Red" Height="25px" HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-Height="25px" HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                                        <asp:HiddenField ID="hddIdMoneda" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>' />

                                                                        <asp:TextBox ID="txtMonto" runat="server" onblur="return(valBlur(event));" onfocus="return(aceptarFoco(this));"
                                                                            onKeypress="return(validarNumeroPuntoPositivo('event'));" TabIndex="500" Text="0" Width="80px"></asp:TextBox>
 
                                                        </ItemTemplate>
                                                        <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                                        <ItemStyle Height="25px" HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:GridView ID="GV_Detalle" runat="server" AutoGenerateColumns="False" Width="100%"
                    RowStyle-Height="25px">
                    <HeaderStyle CssClass="GrillaHeader" />
                    <Columns>
                        <asp:TemplateField HeaderText="Caja">
                            <ItemTemplate>
                                            <asp:Label ID="lblCaja" runat="server" Font-Bold="true" Text='<%# DataBinder.Eval(container.DataItem,"caja_Nombre") %>'></asp:Label>

                                            <asp:HiddenField ID="hddIdCaja_AperturaCierre" runat="server" Value='<%# DataBinder.Eval(container.DataItem,"IdCaja_AperturaCierre") %>' />

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Responsable" DataField="NomPersona" />
                        <asp:BoundField HeaderText="F. Apertura" DataField="cac_FechaApertura" DataFormatString="{0:dd/MM/yyyy hh:mm:ss}" />
                        <asp:TemplateField HeaderText="M. Apertura">
                            <ItemTemplate>
                                            <asp:Label ID="lblMonedaA" runat="server" Font-Bold="true" ForeColor="Red" Text='<%# DataBinder.Eval(container.DataItem,"mon_Simbolo") %>'></asp:Label>

                                            <asp:TextBox ID="txtcac_MontoApertura" runat="server" Font-Bold="true" Width="75px"
                                                onblur="return(valBlur(event));" onfocus="return(aceptarFoco(this));" onKeypress="return(validarNumeroPuntoPositivo('event'));"
                                                ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem,"cac_MontoApertura","{0:F3}") %>'></asp:TextBox>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="F. Cierre">
                            <ItemTemplate>
                                            <asp:TextBox ID="txtcac_FechaCierre" runat="server" ForeColor="Red" BackColor="LightYellow"
                                                onFocus=" return  ( onFocus_FechaCierre(this)  ); " Font-Bold="true" Width="70px"
                                                onblur="return(  valFecha(this) );" Text='<%# DataBinder.Eval(Container.DataItem,"cac_FechaCierre","{0:dd/MM/yyy}") %>'></asp:TextBox>

                                            <cc1:CalendarExtender ID="CalendarExtender_txtcac_FechaCierre" runat="server" Enabled="True"
                                                Format="dd/MM/yyyy" TargetControlID="txtcac_FechaCierre">
                                            </cc1:CalendarExtender>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="M. Cierre">
                            <ItemTemplate>
                                            <asp:Label ID="lblMonedaC" runat="server" Font-Bold="true" ForeColor="Red" Text='<%# DataBinder.Eval(container.DataItem,"mon_Simbolo") %>'></asp:Label>

                                            <asp:TextBox ID="txtcac_MontoCierre" runat="server" Font-Bold="true" Width="75px"
                                                onblur="return(valBlur(event));" onfocus="return(aceptarFoco(this));" onKeypress="return(validarNumeroPuntoPositivo('event'));"
                                                Text='<%# DataBinder.Eval(Container.DataItem,"cac_MontoCierre","{0:F3}") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Estado" DataField="ControlCaja" />
                        <asp:TemplateField HeaderText="Efectivo">
                            <ItemTemplate>
                                            <asp:Label ID="lblMonedaE" runat="server" Font-Bold="true" ForeColor="Red" Text='<%# DataBinder.Eval(container.DataItem,"mon_Simbolo") %>'></asp:Label>

                                            <asp:TextBox ID="txtTotalCajaEfectivo" runat="server" Font-Bold="true" Width="75px"
                                                ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem,"TotalCajaEfectivo","{0:F3}")  %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle CssClass="GrillaFooter" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">

        function onClick_Registrar() {
            var IdEmpresa = parseInt(document.getElementById('<%=cboEmpresa.ClientID %>').value);
            if (isNaN(IdEmpresa)) { IdEmpresa = 0; }
            if (IdEmpresa == 0) {
                alert('Seleccione una empresa.');
                return false;
            }

            var IdTienda = parseInt(document.getElementById('<%=cboTienda.ClientID %>').value);
            if (isNaN(IdTienda)) { IdTienda = 0; }
            if (IdTienda == 0) {
                alert('Seleccione una tienda.');
                return false;
            }

            var IdCaja = parseInt(document.getElementById('<%=cboCaja.ClientID %>').value);
            if (isNaN(IdCaja)) { IdCaja = 0; }
            if (IdCaja == 0) {
                alert('Seleccione una caja.');
                return false;
            }

            var Fecha = document.getElementById('<%=txtFechaMov.ClientID %>').value;
            var cadMonto = '';
            var cadIdMoneda = '';

            var GV_MontoApertura = document.getElementById('<%=GV_MontoApertura.ClientID %>');
            if (GV_MontoApertura != null) {

                var monto = 0;

                for (var i = 1; i < GV_MontoApertura.rows.length; i++) {
                    var rowElem = GV_MontoApertura.rows[i];

                    monto = parseFloat(rowElem.cells[1].children[1].value);
                    if (isNaN(monto)) { monto = 0; }

                    if (monto > 0) {
                        cadMonto = cadMonto + monto + ';';
                        cadIdMoneda = cadIdMoneda + rowElem.cells[1].children[0].value + ';';
                    }

                }

            }

            var GV_Detalle = document.getElementById('<%=GV_Detalle.ClientID %>');
            var rdbOpcion_MovCaja = document.getElementById('<%=rdbOpcion_MovCaja.ClientID %>');
            var ctrl_rdbOpcion_MovCaja = rdbOpcion_MovCaja.getElementsByTagName('input');

            for (var k = 0; k < ctrl_rdbOpcion_MovCaja.length; k++) {
                if (ctrl_rdbOpcion_MovCaja[k].checked == true && ctrl_rdbOpcion_MovCaja[k].value == 'C') { //Cierre
                    if (GV_Detalle == null) {
                        alert('La caja no tiene apertura.');
                        return false;
                    }
                }
            }
            return confirm('Desea continuar con la operación.');
        }
        
        function onFocus_FechaCierre(caja) {

            if (caja.value.length <= 0) {
                caja.value = document.getElementById('<%=txtFechaMov.ClientID %>').value;
                return false;
            }
            return true;
        }

        history.forward();
    
    </script>

</asp:Content>
