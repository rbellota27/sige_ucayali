<%@ Page Title="Caja - Recibo de Ingreso" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmReciboIngreso.aspx.vb" Inherits="APPWEB.FrmReciboIngreso" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr style="background-color: Yellow">
            <td align="center">
                <asp:Label ID="lblCaja" CssClass="LabelRojo" Font-Bold="true" runat="server" Text="Caja"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" OnClientClick="return(   valOnClickNuevo()  );" Width="80px"
                                runat="server" Text="Nuevo" ToolTip="Generar un Doc. Ajuste de Inventario desde un Doc. Toma de Inventario." />
                        </td>
                        <td>
                            <asp:Button ID="btnEditar" OnClientClick="return(  valOnClickEditar()  );" Width="80px"
                                runat="server" Text="Editar" ToolTip="Editar" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" OnClientClick="return(  valOnClickBuscar()  );"
                                Text="Buscar" ToolTip="Buscar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnAnular" OnClientClick="return(  valOnClickAnular()   );" Width="80px"
                                runat="server" Text="Anular" ToolTip="Anular Doc. Ajuste de Inventario" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" OnClientClick="return( valSaveDocumento()  );" Width="80px"
                                runat="server" Text="Guardar" ToolTip="Guardar" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimir" OnClientClick=" return(  valOnClickImprimir()  );  "
                                Width="80px" runat="server" Text="Imprimir" ToolTip="Imprimir Documento" />
                        </td>
                        <td>
                            <asp:Button ID="btnDocumentoRef" Width="120px" runat="server" Text="Documento Ref."
                                ToolTip="Buscar Documento de Referencia" OnClientClick=" return(  valOnClick_btnDocumentoRef()  ); " />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td style="width: 100%" align="right">
                            <table>
                                <tr>
                                    <td class="Texto">
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                RECIBO DE INGRESO
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cab" runat="server">
                    <table>
                        <tr>
                            <td class="Label">
                                Empresa:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa" Width="100%" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="Label">
                                Tienda:
                            </td>
                            <td style="width: 100px">
                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="Label">
                                Serie:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboSerie" Width="100%" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarDocumentoxCodigo">                                
                                <asp:TextBox ID="txtCodigoDocumento" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return(  valOnKeyPressCodigoDoc(e,this)  );"
                                    Width="100px"></asp:TextBox>
                                </asp:Panel>
                            </td>
                            <td>
                                <asp:ImageButton ID="btnBuscarDocumentoxCodigo" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                    ToolTip="Buscar Documento por [ Serie - N�mero ]." OnClientClick="return( valOnClickBuscarDocumentoxCodigo()   );" />
                            </td>
                            <td>
                                <asp:LinkButton ID="btnBusquedaAvanzado" runat="server" CssClass="Texto" Enabled="false"
                                    OnClientClick="return(  valOnClick_btnBusquedaAvanzado()  );" ToolTip="B�squeda Avanzada">Avanzado</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td class="Label">
                                Fecha Emisi�n:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaEmision">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaEmision">
                                </cc1:CalendarExtender>
                            </td>
                            <td class="Label">
                                Moneda:
                            </td>
                            <td>
                                <asp:DropDownList CssClass="LabelRojo" ID="cboMoneda" Width="100%" runat="server"
                                    Enabled="true" onClick="return(  valOnClickMonedaEmision()  );">
                                </asp:DropDownList>
                            </td>
                            <td class="Label">
                                Estado:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEstado" runat="server" Enabled="false">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                &nbsp;
                            </td>
                            <td>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="Label">
                                Caja:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboCaja" Width="100%" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="Label">
                                Operaci�n:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboTipoOperacion" Width="100%" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                CLIENTE
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cliente" runat="server">
                    <table>
                        <tr>
                            <td class="Texto" align="right">
                                Descripci�n:
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtDescripcionPersona" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft"
                                    Width="400px" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarPersona" CssClass="btnBuscar" OnClientClick="return( valBuscarPersona()  );"
                                    runat="server" Text="Buscar" ToolTip="Buscar Cliente" />
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" align="right">
                                D.N.I.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDNI" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>
                            </td>
                            <td align="right" class="Texto">
                                R.U.C.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRUC" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                DOCUMENTOS DE REFERENCIA
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_DocRef" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:GridView ID="GV_DocumentoRef" runat="server" AutoGenerateColumns="False" Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="NomTipoDocumento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red"
                                            HeaderText="Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="25px" />
                                        <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblNroDocumento" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                            <asp:HiddenField ID="hddIdDocumento0" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                                            <asp:HiddenField ID="hddIdEmpresa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />

                                                            <asp:HiddenField ID="hddIdAlmacen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />

                                                            <asp:HiddenField ID="hddIdPersona0" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Empresa" HeaderText="Empresa" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="Tienda" HeaderText="Tienda" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr class="SubTituloCelda">
            <td>
                DETALLE
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Detalle" runat="server">
                    <table id="TablaDetalle" width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Recibo de Ingreso por:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboMotivoReciboIngreso" AutoPostBack="true" CssClass="LabelRojo"
                                                Font-Bold="true" runat="server" ToolTip="Seleccione un Motivo del Recibo de Ingreso"
                                                onClick="return(  valSelectMotivoPago() );">
                                                <asp:ListItem Selected="True" Value="0">-- Seleccione un Motivo --</asp:ListItem>
                                                <asp:ListItem Value="1">Pago de deudas</asp:ListItem>
                                                <asp:ListItem Value="2">Otros</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblMessageHelp" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                Text="**** Seleccione los documentos a los cuales ingresar un Abono."></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_Deudas" runat="server" AutoGenerateColumns="false" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chbSelectDeuda" onClick="return(  valSelectDeudaAbono(event)  );" runat="server"
                                                                ToolTip="Seleccione el documento al cual realizar un abono." />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="NomTipoDocumento" HeaderText="Tipo" />
                                                    <asp:TemplateField HeaderText="Nro. Documento">
                                                        <ItemTemplate>
                                                                        <asp:HiddenField ID="hddIdMovCuenta" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMovCuenta")%>' />
 
                                                                            <asp:HiddenField ID="hddIdDocumentoDeuda" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                                                        <asp:Label ID="lblNroDocumentoDeuda" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"getNroDocumento")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="FechaEmision" NullDisplayText="---" DataFormatString="{0:dd/MM/yyyy}"
                                                        HeaderText="Fecha Emisi�n" />
                                                    <asp:BoundField DataField="getFechaVctoText" NullDisplayText="---" HeaderText="Fecha Vcto."
                                                        DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="NroDiasMora" HeaderText="D�as Vencidos" />
                                                    <asp:TemplateField HeaderText="Monto">
                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblMonedaMonto" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                                        <asp:Label ID="lblMonto" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MontoTotal","{0:F2}")%>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Saldo">
                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblMonedaSaldo" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                                        <asp:Label ID="lblSaldo" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"Saldo","{0:F2}")%>'></asp:Label> 
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Abono">
                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblMonedaAbono" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                                        <asp:TextBox ID="txtAbono" onFocus="return(  aceptarFoco(this)  );" onKeyup="return( calcularTotalAPagar()  );"
                                                                            ToolTip="Seleccione la deuda a la cual realizar el Abono." onKeypress="return(  valKeyPressAbono(event)  );"
                                                                            Width="100px" Font-Bold="true" runat="server"></asp:TextBox>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:ImageButton OnClick="btnVerHistorial_Click" ID="btnVerHistorialPago" Width="25px"
                                                                ToolTip="Ver Historial de Pagos." ImageUrl="~/Caja/iconos/afm_.gif" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnAddConcepto_Otros" Visible="false" Width="100px" runat="server"
                                                Text="Nuevo detalle" ToolTip="Agregar un nuevo detalle al documento." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_Otros" runat="server" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="true" SelectText="Quitar" ItemStyle-Width="65px"
                                                        ItemStyle-HorizontalAlign="Center" />
                                                    <asp:TemplateField HeaderText="Concepto" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                                        <asp:DropDownList ID="cboConcepto" runat="server" DataTextField="Nombre" DataValueField="Id"
                                                                            DataSource='<%#DataBinder.Eval(Container.DataItem,"ListaConcepto")%>'>
                                                                        </asp:DropDownList>
                                                                        <asp:TextBox ID="txtDescripcionConcepto" TabIndex="250" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                                            onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" Width="400px"
                                                                            ToolTip="Ingrese una Descripci�n Adicional" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Concepto")%>'></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Monto" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblMoneda" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>

                                                                        <asp:TextBox ID="txtAbono" TabIndex="260" onKeypress="return(   validarNumeroPuntoPositivo('event')   );"
                                                                            onblur="return( valBlur(event)  );" onKeyup="return( calcularTotalAPagar()  );" onFocus="return(  aceptarFoco(this) );"
                                                                            Width="90px" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Monto","{0:F2}")%>'></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cancelacion" runat="server">
                    <table id="Cancelacion" width="100%">
                        <tr>
                            <td class="TituloCelda">
                                DATOS DE CANCELACI�N
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="DatosCancelacionCab">
                                    <tr>
                                        <td class="Texto">
                                            Total a Cobrar:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblMonedaTotalAPagar" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                            Text="S/."></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtTotalAPagar" onKeypress="return(  false  );" onFocus="return(  valFocusCancelacion() );"
                                                            runat="server" CssClass="TextBox_ReadOnly" Width="100px" Text="0"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="SubTituloCelda">
                                CANCELACI�N
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_CP_Contado" runat="server" Width="100%">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="lblMedioPago_DC" CssClass="Texto" Font-Bold="true" runat="server"
                                                                Text="Medio de Pago:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboMedioPago" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                                AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <asp:Label ID="lblMonto_DC" CssClass="Texto" Font-Bold="true" runat="server" Text="Monto:"></asp:Label>
                                                        </td>
                                                        <td colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:DropDownList ID="cboMoneda_DatoCancelacion" runat="server" Width="100px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td>
                                                                    <asp:Panel ID="Panel7" runat="server" DefaultButton="btnAddDatoCancelacion">                                                                    
                                                                    <asp:TextBox ID="txtMonto_DatoCancelacion" onFocus=" return(  aceptarFoco(this)   ); "
                                                                    runat="server" onKeypress="  return(  valOnKeyPress_txtMonto_DatoCancelacion(event)  ); "
                                                                    Width="100px" Text="0"></asp:TextBox>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>                                                                                                                        
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnAddDatoCancelacion" runat="server" Text="Agregar" Width="70px"
                                                                OnClientClick=" return(   valOnClick_btnAddDatoCancelacion()  ); " />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnVer_NotaCredito" runat="server" Text="Buscar Nota Cr�dito" Width="145px"
                                                                OnClientClick=" return(   valOnClick_btnVer_NotaCredito()  ); " />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="lblPost_DC" CssClass="Texto" Font-Bold="true" runat="server" Text="Tipo Tarjeta"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboPost_DC" runat="server" Width="100%" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <asp:Label ID="lblTarjeta_DC" CssClass="Texto" Font-Bold="true" runat="server" Text="Tarjeta:"></asp:Label>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:DropDownList ID="cboTarjeta_DC" runat="server" Width="100%">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="lblBanco_DC" CssClass="Texto" Font-Bold="true" runat="server" Text="Banco:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboBanco" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="lblCuentaBancaria_DC" CssClass="Texto" Font-Bold="true" runat="server"
                                                                Text="Cuenta:"></asp:Label>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:DropDownList ID="cboCuentaBancaria" runat="server" Width="100%">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="lblNro_DC" runat="server" CssClass="Texto" Font-Bold="true" Text="N�mero:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtNro_DC" runat="server" onFocus=" return(  aceptarFoco(this)   ); "
                                                                onKeypress=" return(    valOnKeyPress_txtNro_DC(event)    ); " Width="90%"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="lblFechaACobrar_DC" CssClass="Texto" Font-Bold="true" runat="server"
                                                                Text="Fecha a Cobrar:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtFechaACobrar" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                                                                Width="100px"></asp:TextBox>
                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender6" runat="server" ClearMaskOnLostFocus="false"
                                                                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                                CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaACobrar">
                                                            </cc1:MaskedEditExtender>
                                                            <cc1:CalendarExtender ID="CalendarExtender5" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                                TargetControlID="txtFechaACobrar">
                                                            </cc1:CalendarExtender>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="GV_Cancelacion" runat="server" AutoGenerateColumns="false" Width="100%">
                                                    <Columns>
                                                        <asp:CommandField ButtonType="Link" SelectText="Quitar" ItemStyle-HorizontalAlign="Center"
                                                            ShowSelectButton="true" ItemStyle-Width="75px" />
                                                        <asp:TemplateField HeaderText="Monto" ItemStyle-Width="120px" HeaderStyle-Height="25px"
                                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblMonedaMonto" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMonedaDestino")%>'></asp:Label>

                                                                            <asp:Label ID="lblMonto" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MontoEquivalenteDestino","{0:F2}")%>'></asp:Label>

                                                                            <asp:HiddenField ID="hddIdMedioPago" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMedioPago")%>' />

                                                                            <asp:HiddenField ID="hddIdMonedaOrigen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="descripcionPagoCaja" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderText="Descripci�n" />
                                                    </Columns>
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td class="Texto" align="right">
                                                            Total Recibido:
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMonedaTotalRecibido" Text="-" runat="server" CssClass="LabelRojo"
                                                                            Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtTotalRecibido" onKeypress="return(  false  );" onFocus="return(  valFocusCancelacion() );"
                                                                            Width="90px" Text="0" CssClass="TextBox_ReadOnly" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Texto" align="right">
                                                            Faltante:
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMonedaFaltante" Text="-" runat="server" CssClass="LabelRojo" Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtFaltante" onKeypress="return(  false  );" onFocus="return(  valFocusCancelacion() );"
                                                                            Width="90px" Text="0" CssClass="TextBox_ReadOnly" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Texto" align="right">
                                                            Vuelto:
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMonedaVuelto" Text="-" runat="server" CssClass="LabelRojo" Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtVuelto" onKeypress="return(  false  );" onFocus="return(  valFocusCancelacion() );"
                                                                            Width="90px" Text="0" CssClass="TextBox_ReadOnly" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                OBSERVACIONES
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Obs" runat="server">
                    <asp:TextBox ID="txtObservaciones" Width="100%" Height="100px" TextMode="MultiLine"
                        runat="server" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                        onFocus="return ( onFocusTextTransform(this,configurarDatos) );"></asp:TextBox>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddFrmModo" runat="server" Value="0" />
                <asp:HiddenField ID="hddIdDocumento" runat="server" />
                <asp:HiddenField ID="hddIdTipoDocumento" Value="17" runat="server" />
                <asp:HiddenField ID="hddCodigoDocumento" runat="server" />
                <asp:HiddenField ID="hddIdPersona" runat="server" />
                <asp:HiddenField ID="hddIdMedioPagoPrincipal" runat="server" />
                <asp:HiddenField ID="hddIdMedioPagoInterfaz" runat="server" />
                <asp:HiddenField ID="hddPermiso_AddMedioPago" runat="server" Value="0" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
    <div id="capaDocumento_NotaCredito" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; background-color: white; z-index: 2;
        display: none; top: 480px; left: 25px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton8" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaDocumento_NotaCredito')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_NotaCredito" runat="server" ScrollBars="Horizontal" Width="100%">
                        <asp:GridView ID="GV_NotaCredito" runat="server" AutoGenerateColumns="false" Width="100%">
                            <Columns>
                                <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="50px" HeaderStyle-Height="25px" />
                                <asp:TemplateField HeaderText="Monto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblMonedaMontoRecibir_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                    <asp:TextBox ID="txtMontoRecibir_Find" Width="70px" Font-Bold="true" onKeypress="return(  validarNumeroPunto(event)  );"
                                                        onblur="return(  valBlur(event)   );" onFocus="return(   aceptarFoco(this)   );" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"ImporteTotal","{0:F2}")%>'></asp:TextBox>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Tipo" />
                                <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"Numero")%>'></asp:Label>

                                                    <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />

                                                    <asp:HiddenField ID="hddIdMonedaDocRef_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />

                                                    <asp:HiddenField ID="hddIdEstadoCancelacion_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEstadoCancelacion")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisi�n"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="FechaVenc" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Vcto."
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="Contador" DataFormatString="{0:F0}" HeaderText="Vigencia (D�as)"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Monto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblMonedaMonto_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                    <asp:Label ID="lblMonto_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F2}")%>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Saldo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblMonedaSaldo_Find" ForeColor="Red" runat="server" Font-Bold="true"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                    <asp:Label ID="lblSaldo_Find" runat="server" ForeColor="Red" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"ImporteTotal","{0:F2}")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="NomPropietario" HeaderText="Empresa" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomTienda" HeaderText="Tienda" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                        <asp:Button ID="Button1" runat="server" Text="Button" />
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaAbonos" style="border: 3px solid blue; padding: 8px; width: 700px; height: auto;
        position: absolute; top: 298px; left: 98px; background-color: white; z-index: 2;
        display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                        onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"
                        OnClientClick="return(offCapa('capaAbonos'));" />
                </td>
            </tr>
            <tr>
                <td style="width: 100%" class="TituloCelda">
                    Abonos
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:GridView ID="GV_Abonos" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                        PageSize="20" Width="100%">
                        <Columns>
                            <asp:BoundField DataField="NomTipoDocumento" HeaderText="Tipo" />
                            <asp:BoundField DataField="getNroDocumento" HeaderText="Nro. Documento" />
                            <asp:BoundField DataField="FechaEmision" HeaderText="Fecha Emisi�n" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:TemplateField HeaderText="Monto">
                                <ItemTemplate>
                                                <asp:Label ID="lblMoneda" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                <asp:Label ID="lblMontoAbonado" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F2}")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Selected="True" Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Value="J">Juridica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razon Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btBuscarPersonaGrillas">
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this,event));" runat="server"
                                                                            onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                                            Width="450px"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel3" runat="server" DefaultButton="btBuscarPersonaGrillas">
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero(event));" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel4" runat="server" DefaultButton="btBuscarPersonaGrillas">
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero(event));"
                                                                            MaxLength="11"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td>
                                                                        <%--<asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />--%>
                                                                            <asp:Button ID="btBuscarPersonaGrillas" runat="server" Text="Buscar" CssClass="btnBuscar"
                                                                        OnClientClick="this.disabled;this.value='Procesando...'" UseSubmitBehavior="false"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaBusquedaAvanzado" style="border: 3px solid blue; padding: 8px; width: 850px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 142px; left: 22px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton2" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaBusquedaAvanzado')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BusquedaAvanzado" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaInicio_BA" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaInicio_BA" runat="server"
                                                    ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                                    CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_BA">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaInicio_BA" runat="server" Enabled="True"
                                                    Format="dd/MM/yyyy" TargetControlID="txtFechaInicio_BA">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaFin_BA" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaFin_BA" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_BA">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaFin_BA" runat="server" Enabled="True"
                                                    Format="dd/MM/yyyy" TargetControlID="txtFechaFin_BA">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBusquedaAvanzado" Width="70px" ToolTip="Buscar Documentos" CssClass="btnBuscar"
                                                    runat="server" Text="Buscar" OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_BusquedaAvanzado" runat="server" AutoGenerateColumns="false"
                        Width="100%" AllowPaging="True">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50px" HeaderStyle-Height="25px">
                                <HeaderStyle Height="25px" Width="50px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:CommandField>
                            <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Tipo"></asp:BoundField>
                            <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblNroDocumento_BusquedaAvanzado" ForeColor="Red" Font-Bold="true"
                                                    runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                <asp:HiddenField ID="hddIdDocumento_BusquedaAvanzado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisi�n"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="DescripcionPersona" HeaderText="Cliente" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td class="LabelRojo" style="font-weight: bold">
                    *** El proceso de b�squeda realiza un filtrado de acuerdo al cliente seleccionado.
                </td>
            </tr>
        </table>
    </div>
    <div id="capaPermiso_AddMedioPago" style="border: 3px solid blue; padding: 8px; width: 350px;
        height: auto; position: absolute; top: 350px; left: 250px; background-color: white;
        z-index: 3; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton12" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaPermiso_AddMedioPago')   );" />
                </td>
            </tr>
            <tr>
                <td class="TituloCelda">
                    Autorizaci�n - Medio de Pago
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Usuario:
                            </td>
                            <td>
                                <asp:TextBox ID="txtUsuario_AddMedioPago" Width="150px" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Contrase�a:
                            </td>
                            <td>
                                <asp:TextBox ID="txtClave_AddMedioPago" Width="150px" runat="server" TextMode="Password"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Contrase�a:
                            </td>
                            <td>
                                <asp:TextBox ID="txtClave2_AddMedioPago" Width="150px" runat="server" TextMode="Password"> </asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnAceptar_AddMedioPago" OnClientClick=" return(  valOnClick_btnAceptar_AddMedioPago() ); "
                                    runat="server" Text="Aceptar" Width="80px" />
                            </td>
                            <td>
                                <asp:Button ID="btnCerrar_AddMedioPago" runat="server" Text="Cerrar" Width="80px"
                                    OnClientClick="return(    offCapa('capaPermiso_AddMedioPago')       );" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaDocumentosReferencia" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 100px; left: 30px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton6" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaDocumentosReferencia')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tipo Documento:
                            </td>
                            <td>
                                <asp:DropDownList ID="CboTipoDocumento" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right" style="text-align: left">
                    <asp:RadioButtonList ID="rdbBuscarDocRef" runat="server" RepeatDirection="Horizontal"
                        CssClass="Texto" AutoPostBack="true">
                        <asp:ListItem Value="0" Selected="True">Por Fecha de Emisi�n</asp:ListItem>
                        <asp:ListItem Value="1">Por Nro. Documento</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxFecha" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaInicio_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaFin_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender5" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaFin_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRef" Width="70px" ToolTip="Buscar Documentos" CssClass="btnBuscar"
                                                    runat="server" Text="Buscar" OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxCodigo" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Nro. Serie:
                                            </td>
                                            <td>
                                                <asp:Panel ID="Panel5" runat="server" DefaultButton="btnAceptarBuscarDocRefxCodigo">
                                                <asp:TextBox ID="txtSerie_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                                    </asp:Panel>
                                            </td>
                                            <td class="Texto">
                                                Nro. C�digo:
                                            </td>
                                            <td>
                                            <asp:Panel ID="Panel6" runat="server" DefaultButton="btnAceptarBuscarDocRefxCodigo">
                                                <asp:TextBox ID="txtCodigo_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                            </asp:Panel>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRefxCodigo" OnClientClick="return(  valOnClick_btnAceptarBuscarDocRefxCodigo()  );"
                                                    runat="server" Text="Buscar" Width="70px" ToolTip="Buscar Documentos" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_DocRef_Find" runat="server" ScrollBars="Horizontal" Width="100%">
                        <asp:GridView ID="GV_DocumentosReferencia_Find" runat="server" AutoGenerateColumns="false"
                            Width="100%">
                            <Columns>
                                <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="50px" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Tipo" />
                                <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                    <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                                    <asp:HiddenField ID="hddIdEmpresa_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />

                                                    <asp:HiddenField ID="hddIdAlmacen_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />

                                                    <asp:HiddenField ID="hddIdPersona_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisi�n"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DescripcionPersona" HeaderText="Descripci�n" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomAlmacen" HeaderText="Almac�n" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Doc. Ref." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddl_TipoDocumentoRef" DataSource='<%#DataBinder.Eval(Container.DataItem,"getTipoDocumentoRef")%>'
                                            DataTextField="DescripcionCorto" DataValueField="Id" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">
        //*************************   DATOS DE CANCELACION
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
        function valOnKeyPress_txtMonto_DatoCancelacion(e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) { // Enter
                document.getElementById('<%=btnAddDatoCancelacion.ClientID%>').focus();
            } else {
                return validarNumeroPuntoPositivo('event');
            }
            return true;
        }

        function valOnClick_btnAddDatoCancelacion() {
            var cboMoneda = document.getElementById('<%=cboMoneda_DatoCancelacion.ClientID%>');
            if (cboMoneda != null) {
                if (isNaN(parseInt(cboMoneda.value)) || parseInt(cboMoneda.value) <= 0 || cboMoneda.value.length <= 0) {
                    alert('Debe seleccionar una [ Moneda ].');
                    return false;
                }
            }
            var txtMonto = document.getElementById('<%=txtMonto_DatoCancelacion.ClientID%>');
            if (txtMonto != null) {
                if (isNaN(parseFloat(txtMonto.value)) || parseFloat(txtMonto.value) <= 0 || txtMonto.value.length <= 0) {
                    alert('Debe ingresar un valor v�lido.');
                    txtMonto.select();
                    txtMonto.focus();
                    return false;
                }
            }
            var cboPost = document.getElementById('<%=cboPost_DC.ClientID%>');
            if (cboPost != null) {
                if (isNaN(parseInt(cboPost.value)) || parseInt(cboPost.value) <= 0 || cboPost.value.length <= 0) {
                    alert('Debe seleccionar un [ Post ].');
                    return false;
                }
            }
            var cboTarjeta = document.getElementById('<%=cboTarjeta_DC.ClientID%>');
            if (cboTarjeta != null) {
                if (isNaN(parseInt(cboTarjeta.value)) || parseInt(cboTarjeta.value) <= 0 || cboTarjeta.value.length <= 0) {
                    alert('Debe seleccionar un [ Tipo de Tarjeta ].');
                    return false;
                }
            }
            var cboBanco = document.getElementById('<%=cboBanco.ClientID%>');
            if (cboBanco != null) {
                if (isNaN(parseInt(cboBanco.value)) || parseInt(cboBanco.value) <= 0 || cboBanco.value.length <= 0) {
                    alert('Debe seleccionar un [ Banco ].');
                    return false;
                }
            }
            var cboCuentaBancaria = document.getElementById('<%=cboCuentaBancaria.ClientID%>');
            if (cboCuentaBancaria != null) {
                if (isNaN(parseInt(cboCuentaBancaria.value)) || parseInt(cboCuentaBancaria.value) <= 0 || cboCuentaBancaria.value.length <= 0) {
                    alert('Debe seleccionar una [ Cuenta Bancaria ].');
                    return false;
                }
            }
            var txtNro = document.getElementById('<%=txtNro_DC.ClientID%>');
            if (txtNro != null) {
                if (txtNro.value.length <= 0) {
                    alert('Debe ingresar un valor v�lido.');
                    txtNro.select();
                    txtNro.focus();
                    return false;
                }
            }
            return true;
        }
        function valOnClick_btnVer_NotaCredito() {
            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }
            return true;
        }
        function valFocusCancelacion() {
            document.getElementById('<%=btnAddDatoCancelacion.ClientID%>').focus();
            return false;
        }
        function valOnKeyPress_txtNro_DC(e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) { //**************** Enter
                document.getElementById('<%=btnAddDatoCancelacion.ClientID%>').focus();
            }
            return true;
        }
        function valFocusCancelacion() {
            document.getElementById('<%=btnAddDatoCancelacion.ClientID%>').focus();
            return false;
        }
        //******************* FIN DATOS DE CANCELACION



        //************************** BUSQUEDA PERSONA
        //***************************************************** BUSQUEDA PERSONA

        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero(e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrillas.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(obj,e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) {
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btBuscarPersonaGrillas.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }

        function valSelectMotivoPago() {

            var IdPersona = document.getElementById('<%=hddIdPersona.ClientID %>').value;
            if (isNaN(IdPersona) || IdPersona.length <= 0) {
                alert('Seleccione una Persona.');
                return false;
            }
            return true;
        }
        function selectDeuda(e) {
            var e = e ? e : window.event;
            var event_element = e.target ? e.target : e.srcElement;
            var grilla = document.getElementById('<%=GV_Deudas.ClientID%>');
            if (grilla == null) {
                alert('No se hallaron registros.');
                return false;
            }            
            for (var i = 1; i < grilla.rows.length; i++) {
                var rowElem = grilla.rows[i];
                if (rowElem.cells[0].getElementsByTagName("INPUT")[0].id == event_element.id) {
                    rowElem.cells[8].children[1].value = '';
                    rowElem.cells[8].children[1].select();
                    rowElem.cells[8].children[1].focus();
                    return true;
                }
            }
            alert('Elemento no encontrado.');
            return false;
        }

        function valKeyPressAbono(e) {
            var e = e ? e : window.event;
            var event_element = e.target ? e.target : e.srcElement;

            var grilla = document.getElementById('<%=GV_Deudas.ClientID%>');
            if (grilla == null) {
                alert('No se hallaron registros.');
                return false;
            }
            for (var i = 1; i < grilla.rows.length; i++) {
                var rowElem = grilla.rows[i];
                if (rowElem.cells[8].children[1].id == event_element.id) {
                    var selectStatus = rowElem.cells[0].getElementsByTagName("INPUT")[0].checked;
                    if (selectStatus) {
                        return validarNumeroPuntoPositivo('event');
                    } else {
                        alert('Debe seleccionar el Documento al cual hacer el Abono.');
                        return false;
                    }
                }
            }
            alert('Elemento no encontrado.');
            return false;
        }




        function calcularDatosCancelacion() {
            var totalAPagar = parseFloat(document.getElementById('<%=txtTotalAPagar.ClientID%>').value);
            var totalRecibido = 0;

            var grilla = document.getElementById('<%=GV_Cancelacion.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    totalRecibido = totalRecibido + parseFloat(rowElem.cells[1].children[1].innerHTML);
                }
            }
            var faltante = 0;
            var vuelto = 0;
            if ((totalAPagar - totalRecibido) > 0) {
                faltante = Math.abs((totalAPagar - totalRecibido));
            } else {
                vuelto = Math.abs((totalAPagar - totalRecibido));
            }

            //********** Imprimimos valores
            document.getElementById('<%=txtTotalRecibido.ClientID%>').value = Format(totalRecibido, 2);
            document.getElementById('<%=txtFaltante.ClientID%>').value = Format(faltante, 2);
            document.getElementById('<%=txtVuelto.ClientID%>').value = Format(vuelto, 2);

            return false;
        }


        function valSelectDeudaAbono(e) {
            selectDeuda(e);
            calcularTotalAPagar();
            return true; //**** Retornamos true para que se tenga el check
        }


        function calcularTotalAPagar() {

            var cboMotivoReciboIngreso = document.getElementById('<%=cboMotivoReciboIngreso.ClientID%>');
            var totalAPagar = 0;
            switch (cboMotivoReciboIngreso.value) {
                case '0':  //********* Seleccionar Item                                
                    break;
                case '1':  //********** Pago deudas
                    var grilla = document.getElementById('<%=GV_Deudas.ClientID%>');
                    if (grilla != null) {
                        for (var i = 1; i < grilla.rows.length; i++) {
                            var rowElem = grilla.rows[i];
                            var selectStatus = rowElem.cells[0].getElementsByTagName("INPUT")[0].checked;
                            if (selectStatus) {
                                var abono = parseFloat(rowElem.cells[8].children[1].value);
                                if (isNaN(abono)) {
                                    abono = 0;
                                }
                                totalAPagar = totalAPagar + abono;
                            }
                        }
                    }
                    break;
                case '2':  //********** Otros
                    var grilla = document.getElementById('<%=GV_Otros.ClientID%>');
                    if (grilla != null) {
                        for (var i = 1; i < grilla.rows.length; i++) {
                            var rowElem = grilla.rows[i];
                            var abono = parseFloat(rowElem.cells[2].children[1].value);
                            if (isNaN(abono)) {
                                abono = 0;
                            }
                            totalAPagar = totalAPagar + abono;
                        }
                    }
                    break;
            }

            //********** Imprimimos valores
            document.getElementById('<%=txtTotalAPagar.ClientID%>').value = redondear(totalAPagar, 2);

            //********* calculamos los datos de cancelaci�n
            calcularDatosCancelacion();
            return false;
        }
        function valBuscarPersona() {
            var grilla = document.getElementById('<%=GV_Deudas.ClientID%>');
            if (grilla == null) {
                mostrarCapaPersona();
            } else {
                alert('No puede seleccionar otra persona. Motivo del Recibo de Ingreso actual : < PAGO DE DEUDAS >. Caso contrario seleccione otro motivo del Recibo de Ingreso.');
            }
            return false;
        }

        function valFocusPersona() {
            document.getElementById('<%=btnBuscarPersona.ClientID%>').focus();
            return false;
        }

        function valSaveDocumento() {

            //******** Validamos la cabecera

            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
            if (isNaN(cboEmpresa.value) || cboEmpresa.value.length <= 0) {
                alert('Debe seleccionar una Empresa');
                return false;
            }
            var cboTienda = document.getElementById('<%=cboTienda.ClientID%>');
            if (isNaN(cboTienda.value) || cboTienda.value.length <= 0) {
                alert('Debe seleccionar una Tienda');
                return false;
            }
            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(cboSerie.value) || cboSerie.value.length <= 0) {
                alert('Debe seleccionar una Serie');
                return false;
            }


            var cboCaja = document.getElementById('<%=cboCaja.ClientID%>');
            if (isNaN(cboCaja.value) || cboCaja.value.length <= 0) {
                alert('No tiene asignada una Caja.');
                return false;
            }

            var cboTipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID%>');
            if (isNaN(parseInt(cboTipoOperacion.value)) || cboTipoOperacion.value.length <= 0 || parseInt(cboTipoOperacion.value) <= 0) {
                alert('Debe seleccionar un Tipo de Operaci�n.');
                return false;
            }

            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(hddIdPersona.value) || hddIdPersona.value.length <= 0) {
                alert('Seleccione una Persona.');
                return false;
            }

            var grillaDeuda = document.getElementById('<%=GV_Deudas.ClientID%>');
            var grillaOtros = document.getElementById('<%=GV_Otros.ClientID%>');
            if (grillaDeuda == null && grillaOtros == null) {
                alert('Debe a�adir detalles al documento.');
                return false;
            }

            //******* Validamos que el abono sea menor o igual al saldo
            var grilla = document.getElementById('<%=GV_Deudas.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var selectStatus = rowElem.cells[0].children[0].children[0].status;
                    if (selectStatus) {
                        var abono = parseFloat(rowElem.cells[8].children[1].value);
                        var saldo = parseFloat(rowElem.cells[7].children[1].innerHTML);
                        var txtAbono = rowElem.cells[8].children[1];
                        if (isNaN(txtAbono.value) || parseFloat(txtAbono.value) <= 0 || txtAbono.value.length <= 0) {
                            alert('Debe ingresar un valor v�lido mayor a cero.');
                            txtAbono.select();
                            txtAbono.focus();
                            return false;
                        }
                        if (abono > saldo) {
                            alert('La cantidad ingresada supera el saldo pendiente.');
                            rowElem.cells[8].children[1].select();
                            rowElem.cells[8].children[1].focus();
                            return false;
                        }
                    }
                }
            }

            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(hddIdPersona.value) || hddIdPersona.value.length <= 0) {
                alert('Seleccione una Persona.');
                return false;
            }

            var txtTotalAPagar = document.getElementById('<%=txtTotalAPagar.ClientID%>');
            if (isNaN(txtTotalAPagar.value) || txtTotalAPagar.value.length <= 0 || parseFloat(txtTotalAPagar.value) <= 0) {
                alert('El Total a Pagar debe ser mayor a cero.');
                return false;
            }

            var hddIdMedioPagoPrincipal = document.getElementById('<%=hddIdMedioPagoPrincipal.ClientID%>');

            //********* Validamos que no Existe Vuelto si el medio de pago es fuera de EFECTIVO
            var grilla = document.getElementById('<%=GV_Cancelacion.ClientID %>');
            if (grilla != null) {
                var vuelto = parseFloat(document.getElementById('<%=txtVuelto.ClientID%>').value);
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];

                    if (parseInt(rowElem.cells[1].children[2].value) != (parseInt(hddIdMedioPagoPrincipal.value)) && vuelto > 0) {
                        alert('SE HAN INGRESADO MEDIOS DE PAGO QUE NO DEBEN GENERAR VUELTO. NO SE PERMITE LA OPERACI�N.');
                        return false;
                    }
                    if (parseFloat(rowElem.cells[1].children[1].innerHTML) <= 0) {
                        alert('No puede a�adirse una cancelaci�n con monto Negativo y/o CERO. Quite el dato de cancelaci�n.');
                        return false;
                    }
                }

                var txtTotalAPagar = document.getElementById('<%=txtTotalAPagar.ClientID%>');
                var txtTotalRecibido = document.getElementById('<%=txtTotalRecibido.ClientID%>');
                if (parseFloat(UnFormat(txtTotalRecibido.value, ',', '')) < parseFloat(txtTotalAPagar.value)) {
                    alert('El Total Recibido es menor al Total a Pagar. No se permite la Operaci�n.');
                    return false;
                }

            } else {
                alert('INGRESE UN DATO DE CANCELACI�N.');
                return false;
            }

            return confirm('Desea continuar con la Operaci�n ?');
        }

        function valOnClickNuevo() {

            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID %>');
            if (parseInt(hddFrmModo.value) == 0 || parseInt(hddFrmModo.value) == 4 || parseInt(hddFrmModo.value) == 5 || parseInt(hddFrmModo.value) == 6) {
                return true;
            }

            var grillaDeudas = document.getElementById('<%=GV_Deudas.ClientID %>');
            var grillaOtros = document.getElementById('<%=GV_Otros.ClientID %>');
            if (grillaDeudas == null && grillaOtros == null) {
                return true;
            }

            return (confirm('Est� seguro que desea generar un NUEVO Documento ?'));
        }


        function valOnClickEditar() {
            var cboEstadoDocumento = document.getElementById('<%=cboEstado.ClientID %>');
            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID %>');
            if (parseInt(cboEstadoDocumento.value) == 2 || parseInt(hddFrmModo.value) == 6) {
                alert('El Documento Recibo de Ingreso ha sido anulado. No puede habilitarse la edici�n del Documento.');
                return false;
            }

            var lblCaja = document.getElementById('<%=lblCaja.ClientID%>');
            if (lblCaja.innerText != 'Caja Aperturada') {
                alert('La caja no esta apertutada.\nNo se permite la operaci�n.');
                return false;
            }

            return (confirm('Antes de editar un Documento de Recibo de Ingreso, el Sistema revertir� todos los movimientos efectuados por el Documento. Est� seguro que desea continuar con el Proceso de Edici�n ?'));
        }


        function valOnClickAnular() {
            var cboEstadoDocumento = document.getElementById('<%=cboEstado.ClientID %>');
            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID %>');
            if (parseInt(cboEstadoDocumento.value) == 2 || parseInt(hddFrmModo.value) == 6) {
                alert('El Documento ya ha sido ANULADO. No procede la Anulaci�n del Documento.');
                return false;
            }
            return (confirm('Desea continuar con el Proceso de Anulaci�n del Documento Recibo de Ingreso ?'));
        }


        function valOnClickMonedaEmision() {
            var grillaDeudas = document.getElementById('<%=GV_Deudas.ClientID %>');
            var grillaOtros = document.getElementById('<%=GV_Otros.ClientID %>');
            var grillaCancelacion = document.getElementById('<%=GV_Cancelacion.ClientID %>');

            if (grillaDeudas != null || grillaOtros != null || grillaCancelacion != null) {
                alert('No puede seleccionar otra Moneda de Emisi�n ya que se ha ingresado Detalles de Documento y/o Datos de Cancelaci�n, si desea seleccionar otra Moneda de Emisi�n quite el detalle y/o los datos de cancelaci�n del Documento.');
                return false;
            }

            return true;
        }

        function valOnClickImprimir() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID %>');
            if (isNaN(parseInt(hddIdDocumento.value)) || parseInt(hddIdDocumento.value) <= 0 || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para Impresi�n.');
                return false;
            }
            //window.open('../../Caja/Reportes/Visor1.aspx?iReporte=1&IdDocumento=' + hddIdDocumento.value, 'Recibo_Ingreso', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            window.open('../../DocumentosMercantiles/VisorDocMercantiles.aspx?iReporte=17&IdDocumento=' + hddIdDocumento.value, 'Cotizacion', 'resizable=yes,width=1000,height=800,scrollbars=1', null);

            return false;
        }

        function valOnClickBuscar() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            var panel_Cab = document.getElementById('<%=Panel_Cab.ClientID %>');
            var btnBuscarDocumentoxCodigo = document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>');
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                panel_Cab.disabled = false;
                btnBuscarDocumentoxCodigo.disabled = false;
                btnBusquedaAvanzado.disabled = false;
                txtCodigoDocumento.readOnly = false;
                txtCodigoDocumento.disabled = false;
                txtCodigoDocumento.style.backgroundColor = 'Yellow';
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            return true;
        }

        function valOnClick_btnBusquedaAvanzado() {

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (btnBusquedaAvanzado.disabled == true) {  //**** Se habilita la B�squeda
                alert('Debe habilitar la B�squeda del Documento haciendo clic en el bot�n [ Buscar ]');
                return false;
            }

            onCapa('capaBusquedaAvanzado');
            return false;
        }
        function valOnKeyPressCodigoDoc(e,txtCodigoDocumento) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) {
                document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>').focus();
            } else {
                return onKeyPressEsNumero('event');
            }

        }
        function valOnClickBuscarDocumentoxCodigo() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                alert('Debe habilitar la B�squeda del Documento haciendo clic en el bot�n [ Buscar ]');
                return false;
            }

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }

            if (isNaN(parseInt(txtCodigoDocumento.value)) || txtCodigoDocumento.value.length <= 0) {
                alert('Debe ingresar un valor v�lido.');
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            document.getElementById('<%=hddCodigoDocumento.ClientID %>').value = txtCodigoDocumento.value;
            return true;
        }
        //***************************  PERFIL VS MEDIO DE PAGO
        function valOnClick_btnAceptar_AddMedioPago() {
            var txtUsuario = document.getElementById('<%=txtUsuario_AddMedioPago.ClientID %>');
            if (txtUsuario.value.length <= 0) {
                alert('CAMPO REQUERIDO. NO SE PERMITE LA OPERACI�N.');
                txtUsuario.select();
                txtUsuario.focus();
                return false;
            }
            var txtClave1 = document.getElementById('<%=txtClave_AddMedioPago.ClientID %>');
            if (txtClave1.value.length <= 0) {
                alert('CAMPO REQUERIDO. NO SE PERMITE LA OPERACI�N.');
                txtClave1.select();
                txtClave1.focus();
                return false;
            }
            var txtClave2 = document.getElementById('<%=txtClave2_AddMedioPago.ClientID %>');
            if (txtClave2.value.length <= 0) {
                alert('CAMPO REQUERIDO. NO SE PERMITE LA OPERACI�N.');
                txtClave2.select();
                txtClave2.focus();
                return false;
            }
            return true;
        }

        function verCapaPermiso_AddMedioPago() {
            onCapa('capaPermiso_AddMedioPago');
            document.getElementById('<%=txtUsuario_AddMedioPago.ClientID%>').select();
            document.getElementById('<%=txtUsuario_AddMedioPago.ClientID%>').focus();
            return false;
        }

        function valOnClick_btnDocumentoRef() {
            onCapa('capaDocumentosReferencia');
            var txtSerie = document.getElementById('<%=txtSerie_BuscarDocRef.ClientID %>');
            if (txtSerie != null) {
                txtSerie.select();
                txtSerie.focus();
            }
            return false;
        }
        function valOnClick_btnAceptarBuscarDocRefxCodigo() {
            var txtSerie = document.getElementById('<%=txtSerie_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtSerie.value)) || txtSerie.value.length <= 0) {
                alert('Ingrese un Nro. de Serie para la b�squeda.');
                txtSerie.select();
                txtSerie.focus();
                return false;
            }
            var txtCodigo = document.getElementById('<%=txtCodigo_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtCodigo.value)) || txtCodigo.value.length <= 0) {
                alert('Ingrese un Nro. de Documento para la b�squeda.');
                txtCodigo.select();
                txtCodigo.focus();
                return false;
            }
            return true;
        }
    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>
