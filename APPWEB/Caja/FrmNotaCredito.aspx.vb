﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmNotaCredito
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
    End Enum
    Dim listaDetalleconcepto As List(Of Entidades.DetalleConcepto)


    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            ConfigurarDatos()
            inicializarFrm()
        End If
        valAuxiliares()
    End Sub
    Protected Sub btnGEnerarFacElectronica_Click(sender As Object, e As EventArgs) Handles btnGEnerarFacElectronica.Click
        Dim bl As New Negocio.bl_estructuraFacturacion
        Try
            bl.fun_GENERAR_PRINT_FAC_ELECTRONICA(cboSerie.SelectedValue, Trim(Me.txtCodigoDocumento.Text))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION 
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {74, 75, 76, 77, 78, 122, 184})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumentoxCodigo.Enabled = True
        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumentoxCodigo.Enabled = False
        End If

        If listaPermisos(4) > 0 Then  '********* FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

        If listaPermisos(5) > 0 Then  '********* FECHA VCTO
            Me.txtFechaVcto.Enabled = True
        Else
            Me.txtFechaVcto.Enabled = False
        End If

        If listaPermisos(6) > 0 Then  '********* OPCION DE BUSQUEDA
            Me.chb_ConsiderarDocRef_NC.Enabled = True
        Else
            Me.chb_ConsiderarDocRef_NC.Enabled = False
        End If

    End Sub


    Private Sub valAuxiliares()
        Try

            '*********** Campos Nota de Crédito
            Me.lblMonedaIGV.Text = (Me.cboMoneda.SelectedItem.ToString)
            Me.lblMonedaSubTotal.Text = (Me.cboMoneda.SelectedItem.ToString)
            Me.lblMonedaTotal.Text = (Me.cboMoneda.SelectedItem.ToString)

            '*********** Campos Doc Ref
            Me.lblMonedaTotalDocRef.Text = (Me.cboMoneda.SelectedItem.ToString)
            Me.lblMonedaPercepcionDocRef.Text = (Me.cboMoneda.SelectedItem.ToString)
            Me.lblMonedaTotalGeneralDocRef.Text = (Me.cboMoneda.SelectedItem.ToString)

            '*************** Campo Detalle
            Me.lblMonedaMontoNotaCredito.Text = (Me.cboMoneda.SelectedItem.ToString)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub inicializarFrm()
        Try

            Dim objCbo As New Combo
            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)

                GenerarCodigoDocumento()

                .LlenarCboMoneda(Me.cboMoneda, False)
                .LLenarCboEstadoDocumento(Me.cboEstado)

                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.cboTipoDocumento.SelectedValue), True)
                .LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivo, CInt(Me.cboTipoOperacion.SelectedValue), True)

                .LlenarCboTiendaxIdUsuario(Me.cboTienda_DocRef, CInt(Session("IdUsuario")), True)

            End With

            Dim fechaActual As Date = (New Negocio.FechaActual).SelectFechaActual
            Me.txtFechaEmision.Text = Format(fechaActual, "dd/MM/yyyy")
            Me.txtFechaVcto.Text = Format(DateAdd(DateInterval.Day, 7, fechaActual), "dd/MM/yyyy")
            Me.txtFechaI.Text = Me.txtFechaEmision.Text
            Me.txtFechaF.Text = Me.txtFechaEmision.Text

            Me.txtFechaInicio_BA.Text = Me.txtFechaEmision.Text
            Me.txtFechaFin_BA.Text = Me.txtFechaEmision.Text


            Me.lblIGV_Tasa.Text = CStr(Math.Round((New Negocio.Impuesto).SelectTasaIGV * 100, 0))

            verFrm(FrmModo.Nuevo, False, True)

            ValidarPermisos()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub verFrm(ByVal FrmModo As Integer, ByVal limpiar As Boolean, ByVal initParametrosGral As Boolean)

        If (limpiar) Then
            LimpiarFrm()
        End If

        If (initParametrosGral) Then

            '******* FECHA DE VCTO
            Dim listaParametros() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {3})
            Me.txtFechaVcto.Text = Format(DateAdd(DateInterval.Day, listaParametros(0), CDate(Me.txtFechaEmision.Text)), "dd/MM/yyyy")

        End If

        Me.hddFrmModo.Value = CStr(FrmModo)
        ActualizarBotonesControl()

    End Sub


    Private Sub ActualizarBotonesControl()

        Select Case CInt(hddFrmModo.Value)

            Case FrmModo.Inicio '************* INICIO
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False


                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = True
                Me.Panel_DocReferencia.Enabled = False
                Me.Panel_Obs.Enabled = False
                Me.Panel_ListaProducto.Visible = False
                Me.Panel_MontoDetalle.Visible = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                Me.cboMoneda.Enabled = True


                Me.cboNotaCreditoModo.Enabled = True
                Me.cboGuiaRecepcion.Visible = False


            Case FrmModo.Nuevo '************** Nuevo
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False


                Me.Panel_Cliente.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Cab.Enabled = True
                Me.Panel_DocReferencia.Enabled = True
                Me.Panel_Obs.Enabled = True
                Me.Panel_ListaProducto.Visible = False
                Me.Panel_MontoDetalle.Visible = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                Me.cboMoneda.Enabled = True

                Me.cboNotaCreditoModo.Enabled = True

            Case FrmModo.Editar '*********************** Editar
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False


                Me.Panel_Cliente.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Cab.Enabled = True
                Me.Panel_DocReferencia.Enabled = True
                Me.Panel_Obs.Enabled = True

                If (Me.GV_Detalle.Rows.Count > 0) Then
                    Me.Panel_ListaProducto.Visible = True
                    Me.Panel_MontoDetalle.Visible = False
                    Me.cboNotaCreditoModo.SelectedValue = "0"
                Else
                    Me.Panel_ListaProducto.Visible = False
                    Me.Panel_MontoDetalle.Visible = True
                    Me.cboNotaCreditoModo.SelectedValue = "1"
                End If

                ''''concepto'''''''''''''''''
                If (Me.GV_Concepto.Rows.Count > 0) Then
                    Me.Panel_ListaProducto.Visible = True
                    Me.Panel_MontoDetalle.Visible = False
                    ' AQUI OTRA VEZ DANITZA
                    '    Me.cboNotaCreditoModo.SelectedValue = "0"
                    'Else
                    '    Me.Panel_ListaProducto.Visible = False
                    '    Me.Panel_MontoDetalle.Visible = True
                    '    Me.cboNotaCreditoModo.SelectedValue = "1"
                End If

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False

                Me.cboMoneda.Enabled = True


            Case FrmModo.Buscar_Doc '**************************** Buscar documento
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = False
                Me.txtCodigoDocumento.Focus()
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False


                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = True
                Me.Panel_DocReferencia.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboMoneda.Enabled = True

                Me.cboNotaCreditoModo.Enabled = True

            Case FrmModo.Documento_Buscar_Exito '********* documento hallado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True
                Me.btnImprimir.Visible = True



                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_DocReferencia.Enabled = False
                Me.Panel_Obs.Enabled = False

                If (Me.GV_Detalle.Rows.Count > 0) Then
                    Me.Panel_ListaProducto.Visible = True
                    Me.Panel_MontoDetalle.Visible = False

                    Me.cboNotaCreditoModo.SelectedValue = "0"
                Else
                    Me.Panel_ListaProducto.Visible = False
                    Me.Panel_MontoDetalle.Visible = True
                    Me.cboNotaCreditoModo.SelectedValue = "1"
                End If
                ''''''concepto'''''''''''''
                If (Me.GV_Concepto.Rows.Count > 0) Then
                    Me.Panel_ListaProducto.Visible = True
                    Me.Panel_MontoDetalle.Visible = False
                    'AQUI OTRA VEZ LA MALOGRO DANITZA
                    '    Me.cboNotaCreditoModo.SelectedValue = "0"
                    'Else
                    '    Me.Panel_ListaProducto.Visible = False
                    '    Me.Panel_MontoDetalle.Visible = True
                    '    Me.cboNotaCreditoModo.SelectedValue = "1"
                End If


                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


                Me.cboMoneda.Enabled = False

                Me.cboNotaCreditoModo.Enabled = True

            Case FrmModo.Documento_Save_Exito '********* documento guardado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False

                Me.btnImprimir.Visible = True

                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_DocReferencia.Enabled = False
                Me.Panel_Obs.Enabled = False

                If (Me.GV_Detalle.Rows.Count > 0) Then
                    Me.Panel_ListaProducto.Visible = True
                    Me.Panel_MontoDetalle.Visible = False

                Else
                    Me.Panel_ListaProducto.Visible = False
                    Me.Panel_MontoDetalle.Visible = True
                End If
                ''''''''''''Concepto'''''''''
                If (Me.GV_Concepto.Rows.Count > 0) Then
                    Me.Panel_ListaProducto.Visible = True
                    Me.Panel_MontoDetalle.Visible = False

                    'Else
                    '    Me.Panel_ListaProducto.Visible = False
                    '    Me.Panel_MontoDetalle.Visible = True
                End If


                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


                Me.cboMoneda.Enabled = False

                Me.cboNotaCreditoModo.Enabled = True

            Case FrmModo.Documento_Anular_Exito  '*********** Anulacion exitosa

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = True



                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_DocReferencia.Enabled = False
                Me.Panel_Obs.Enabled = False

                If (Me.GV_Detalle.Rows.Count > 0) Then
                    Me.Panel_ListaProducto.Visible = True
                    Me.Panel_MontoDetalle.Visible = False
                Else
                    Me.Panel_ListaProducto.Visible = False
                    Me.Panel_MontoDetalle.Visible = True
                End If

                '''''''''''concepto
                If (Me.GV_Concepto.Rows.Count > 0) Then
                    Me.Panel_ListaProducto.Visible = True
                    Me.Panel_MontoDetalle.Visible = False
                Else
                    Me.Panel_ListaProducto.Visible = False
                    Me.Panel_MontoDetalle.Visible = True
                End If


                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboMoneda.Enabled = False



        End Select
    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged

        Try


            Dim objCbo As New Combo

            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            GenerarCodigoDocumento()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        Try
            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged

        Try

            GenerarCodigoDocumento()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GenerarCodigoDocumento()


        Dim objCombo As New Combo
        objCombo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboTipoDocumento.SelectedValue))

        If (IsNumeric(Me.cboSerie.SelectedValue)) Then
            Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
        Else
            Me.txtCodigoDocumento.Text = ""
        End If

    End Sub


#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region



    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        objScript.onCapa(Me, "capaDocumentosReferencia")

    End Sub

    Protected Sub btnBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarDocRef.Click
        Try
            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()
            actualizarOpcionesBusquedaDocRef(1) '**** buscar doc ref x codigo
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub mostrarDocumentosRef_Find()
        ''VALIDACION DE NOTA DE CREDITO (POWERED BY GERSON)
        lblguardadata.Text = String.Empty
        Me.idlab.Visible = True
        Try

            Dim serie As Integer = 0
            Dim codigo As Integer = 0
            If (Me.txtSerie_BuscarDocRef.Text.Trim.Length > 0) Then serie = CInt(Me.txtSerie_BuscarDocRef.Text)
            If (Me.txtCodigo_BuscarDocRef.Text.Trim.Length > 0) Then codigo = CInt(Me.txtCodigo_BuscarDocRef.Text)

            Dim ValDocRelacionado As Boolean = Not Me.chb_ConsiderarDocRef_NC.Checked

            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.DocumentoNotaCredito).SelectDocReferenciaxParams(CInt(Me.hddIdPersona.Value), CDate(Me.txtFechaI.Text), CDate(Me.txtFechaF.Text), CInt(Me.cboTienda_DocRef.SelectedValue), serie, codigo, CInt(Me.rdbBuscarDocRef.SelectedValue), ValDocRelacionado, Me.cboTipoDocumento.SelectedValue)

            Dim listas As List(Of Entidades.DocumentoValid) = (New Negocio.ValidDocRelacionado).SelectDocRefValidNotaCredito(CInt(Me.hddIdPersona.Value), CDate(Me.txtFechaI.Text), CDate(Me.txtFechaF.Text), CInt(Me.cboTienda_DocRef.SelectedValue), serie, codigo, CInt(Me.rdbBuscarDocRef.SelectedValue), ValDocRelacionado, cboTipoDocumento.SelectedValue)

            If (listas.Count > 0) Then
                Me.gvDocRef.DataSource = listas
                Me.gvDocRef.DataBind()
            End If

            ''VALIDANDO SI HAY O.D O G.R

            For Each gvrow As GridViewRow In gvDocRef.Rows
                Dim lbl As Label = DirectCast(gvrow.FindControl("lblTipoDocumentoRef"), Label)

                If lbl IsNot Nothing And lbl.Text.Length >= 1 Then
                    lblguardadata.Text = "1"   '' SI HAY ORDEN DE DESPACHO O GUÍA DE REMISIÓN

                    If (cboGuiaRecepcion.Items.Count >= 1) Then  ''ESTA REFERENCIADO?. SI ES SI ..
                        lblguardadata.Text = "2"
                    Else
                        lblguardadata.Text = "1"                 ''SI ES NOOOO. 
                    End If

                Else
                    lblguardadata.Text = "0"   '' NO HAY 
                End If
            Next

            lblguardadata.Visible = True

            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged        
        cargarDocumentoReferencia()
    End Sub

    Private Sub cargarDocumentoReferencia()
        
        Try
            Try
                Me.hddIdDocumentoRef.Value = CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value
                Dim obj As New Negocio.Documento
                obj.validarNotaCredito(Me.hddIdDocumentoRef.Value, 0, Me.cboTipoOperacion.SelectedValue, Me.cboMotivo.SelectedValue, 0, "")
            Catch ex As Exception
                Throw ex
            End Try
            '********** DOCUMENTO REF
            Me.lblTipoDocumentoRef.Text = HttpUtility.HtmlDecode(Me.GV_DocumentosReferencia_Find.SelectedRow.Cells(1).Text)

            Me.lblNroDocumentoDocRef.Text = CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("lblNroDocumento_Find"), Label).Text
            Me.lblFechaEmisionDocRef.Text = HttpUtility.HtmlDecode(Me.GV_DocumentosReferencia_Find.SelectedRow.Cells(3).Text)

            Me.cboMoneda.SelectedValue = CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdMonedaDocRef_Find"), HiddenField).Value

            Me.lblTotalDocRef.Text = CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("lblTotal_Find"), Label).Text
            Me.lblPercepcionDocRef.Text = CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("lblPercepcion_Find"), Label).Text
            Me.lblTotalGeneralDocRef.Text = CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("lblTotalGeneral_Find"), Label).Text

            Me.lblDescuentoGlobalDocRef.Text = CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("lblDescuentoGlobal"), Label).Text

            Me.hddIdMonedaDocRef.Value = CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdMonedaDocRef_Find"), HiddenField).Value

            '*************** Detalle Documento
            actualizarModoDetalleNotaCredito(0, True) ' ****** LISTA TODOS LOS ARTICULOS


            valAuxiliares()


            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularMontos('0');", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Protected Sub btnQuitarDetalle_Click(ByVal sender As Object, ByVal e As EventArgs)

        QuitarProductoDetalle(CType(sender, ImageButton))
    End Sub
    Protected Sub btnQuitarDetalleconcepto_Click(ByVal sender As Object, ByVal e As EventArgs)

        QuitarProductoDetalleConcepto(CInt(CType(CType(sender, ImageButton).NamingContainer, GridViewRow).RowIndex))
    End Sub

    Protected Sub QuitarProductoDetalle(ByVal boton As ImageButton)

        Try

            Dim listaDetalle As List(Of Entidades.DetalleDocumento) = obtenerListaDetalle()

            Dim gvRow As GridViewRow = CType(boton.NamingContainer, GridViewRow)

            listaDetalle.RemoveAt(gvRow.RowIndex)

            Me.GV_Detalle.DataSource = listaDetalle
            Me.GV_Detalle.DataBind()



            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularMontos('0');", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Protected Sub QuitarProductoDetalleConcepto(ByVal Index As Integer)

        Try

            Me.listaDetalleconcepto = obtenerListaDetalleconcepto()

            If Me.listaDetalleconcepto(Index).IdConcepto = 10 Then

                Throw New Exception("CONCEPTO REFERENTE AL DESCUENTO GLOBAL. NO SE PERMITE LA OPERACIÓN.")
                Return

            End If

            If Me.listaDetalleconcepto(Index).IdDocumentoRef > 0 Then

                Throw New Exception("CONCEPTO REFERENTE A UN DOCUMENTO DE VENTA. NO SE PERMITE LA OPERACIÓN.")
                Return

            End If

            Me.listaDetalleconcepto.RemoveAt(Index)

            Me.GV_Concepto.DataSource = Me.listaDetalleconcepto
            Me.GV_Concepto.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularMontos('0');", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function obtenerListaDetalle() As List(Of Entidades.DetalleDocumento)

        Dim lista As New List(Of Entidades.DetalleDocumento)

        For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1

            Dim objDetalle As New Entidades.DetalleDocumento

            With objDetalle

                .IdDetalleDocumento = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("hddIdDetalleDocumento_Detalle"), HiddenField).Value)
                .IdDetalleAfecto = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("hddIdDetalleAfecto"), HiddenField).Value)
                .IdProducto = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("hddIdProducto"), HiddenField).Value)
                .CodigoProducto = CStr(CType(Me.GV_Detalle.Rows(i).FindControl("lblCodigoProducto"), Label).Text)
                .Cantidad = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtCantidadDetalle"), TextBox).Text)
                .UMedida = CStr(CType(Me.GV_Detalle.Rows(i).FindControl("lblUM_Detalle"), Label).Text)
                .NomProducto = CStr(Me.GV_Detalle.Rows(i).Cells(4).Text)
                .PrecioCD = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtPUnit_Detalle"), TextBox).Text)
                .Importe = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtImporte_Detalle"), TextBox).Text)
                .CantxAtender = CDec(Me.GV_Detalle.Rows(i).Cells(2).Text)
                .PrecioSD = CDec(Me.GV_Detalle.Rows(i).Cells(5).Text)
                .IdUnidadMedida = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("hddIdUnidadMedidaDetalle"), HiddenField).Value)
                .ProdNombreKit = CStr(Me.GV_Detalle.Rows(i).Cells(8).Text)
                .CaxAtender = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("hddcantxatender"), HiddenField).Value)
            End With



            lista.Add(objDetalle)

        Next

        Return lista

    End Function
    Private Function obtenerListaDetalleconcepto() As List(Of Entidades.DetalleConcepto)
        Dim lista1 As New List(Of Entidades.DetalleConcepto)

        For i As Integer = 0 To Me.GV_Concepto.Rows.Count - 1
            Dim objDetalle1 As New Entidades.DetalleConcepto

            With objDetalle1

                .Descripcion = CStr(Me.GV_Concepto.Rows(i).Cells(1).Text)
                .Monto = CDec(CType(Me.GV_Concepto.Rows(i).FindControl("txtConcepto"), TextBox).Text)
                .IdConcepto = CInt(CType(Me.GV_Concepto.Rows(i).FindControl("hddIdConcepto"), HiddenField).Value)
                .IdDocumentoRef = CInt(CType(Me.GV_Concepto.Rows(i).FindControl("hddIdDocumentoRef"), HiddenField).Value)

            End With
            lista1.Add(objDetalle1)
        Next
        Return lista1

    End Function

    Private Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBuscarDocRef.Click
        mostrarDocumentosRef_Find()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Try
            validaNCSegunTipoOperacion()
            Dim objDocValidar As New Negocio.Documento
            objDocValidar.validarNotaCredito(Me.hddIdDocumentoRef.Value, IIf(Me.cboGuiaRecepcion.SelectedValue = "", 0, Me.cboGuiaRecepcion.SelectedValue), Me.cboTipoOperacion.SelectedValue, Me.cboMotivo.SelectedValue, 0, "")
            RegistrarDocumento()
        Catch ex As Exception
            'Me.cboGuiaRecepcion.SelectedIndex = -1
            'Me.cboGuiaRecepcion.Visible = False
            'cboNotaCreditoModo.SelectedIndex = 0
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .Id = Nothing
                Case FrmModo.Editar
                    .Id = CInt(Me.hddIdDocumento.Value)
            End Select

            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .Serie = (Me.cboSerie.SelectedItem.ToString)
            .Codigo = (Me.txtCodigoDocumento.Text)
            .FechaEmision = CDate(Me.txtFechaEmision.Text)
            .IdTipoDocumento = CInt(Me.cboTipoDocumento.SelectedValue)
            .IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
            .IdMotivoT = CInt(Me.cboMotivo.SelectedValue)

            Try
                .FechaVenc = CDate(Me.txtFechaVcto.Text)
            Catch ex As Exception
                .FechaVenc = Nothing
            End Try


            .TotalLetras = (New Negocio.ALetras).Letras(CStr(Math.Round(CDec(Me.txtTotal.Text), 2, MidpointRounding.AwayFromZero)))
            Select Case CInt(Me.cboMoneda.SelectedValue)
                Case 1 '********** SOLES
                    .TotalLetras = .TotalLetras + "/100 SOLES"
                Case 2 '********** DOLARES
                    .TotalLetras = .TotalLetras + "/100 DÓLARES AMERICANOS"
            End Select

            .IdEstadoDoc = 1  '**************** ACTIVO
            .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
            .IdPersona = CInt(Me.hddIdPersona.Value)

            .Total = CDec(Me.txtTotal.Text)
            .TotalAPagar = CDec(Me.txtTotal.Text)
            .IGV = CDec(Me.txtIGV.Text)
            .SubTotal = CDec(Me.txtSubTotal.Text)
            .ImporteTotal = CDec(Me.txtTotal.Text)  '************** Tomado como El Monto variante de acuerdo a su aplicación.

            .IdUsuario = CInt(Session("IdUsuario"))

            If IsNumeric(Me.hddIdDocumentoRef.Value) Then
                .IdUsuarioComision = (New Negocio.Documento).DocumentoCabSelectxId(CInt(Me.hddIdDocumentoRef.Value)).IdUsuarioComision
            End If


            .IdEstadoCancelacion = 1
        End With

        Return objDocumento

    End Function


    Private Sub actualizarModoDetalleNotaCredito(ByVal Value As Integer, Optional ByVal Referencia As Boolean = False)
        Try

            '************ limpiamos los datos
            Me.cboGuiaRecepcion.Visible = False
            Me.cboGuiaRecepcion.Items.Clear()

            Me.GV_Detalle.DataSource = Nothing
            Me.GV_Detalle.DataBind()

            Me.GV_Concepto.DataSource = Nothing
            Me.GV_Concepto.DataBind()

            Me.txtMontoValorNotaCredito.Text = "0"
            Me.Panel_ListaProducto.Visible = False
            Me.Panel_MontoDetalle.Visible = False

            'If (Referencia) Then
            '    Select Case CInt(Value)
            '        Case 0 '************* POR LISTA DE PRODUCTOS

            '            Me.Panel_ListaProducto.Visible = True


            '            '*************** Detalle Documento
            '            Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumento(CInt(Me.hddIdDocumentoRef.Value))
            '            Dim listaDetalleconcepto As List(Of Entidades.DetalleConcepto) = (New Negocio.DetalleConcepto).SelectxIdDocumento(CInt(Me.hddIdDocumentoRef.Value))

            '            '***************************** Monedas
            '            Dim objUtil As New Negocio.Util
            '            Dim IdMonedaOrigen, IdMonedaDestino As Integer
            '            IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
            '            IdMonedaOrigen = CInt(Me.hddIdMonedaDocRef.Value)

            '            For i As Integer = 0 To listaDetalle.Count - 1

            '                listaDetalle(i).PrecioCD = Math.Round(objUtil.SelectValorxIdMonedaOxIdMonedaD(IdMonedaOrigen, IdMonedaDestino, listaDetalle(i).PrecioCD, "E"), 2)
            '                listaDetalle(i).IdDetalleAfecto = listaDetalle(i).IdDetalleDocumento
            '                listaDetalle(i).CantxAtender = listaDetalle(i).Cantidad
            '                listaDetalle(i).PrecioSD = listaDetalle(i).PrecioCD
            '                ''''''''''''
            '                'listaDetalleconcepto(i).Monto = listaDetalleconcepto(i).Monto


            '            Next

            '            Me.GV_Detalle.DataSource = listaDetalle
            '            Me.GV_Detalle.DataBind()

            '            Me.GV_Concepto.DataSource = listaDetalleconcepto
            '            Me.GV_Concepto.DataBind()

            '        Case 1 '************* POR MONTO FINAL

            '            Me.Panel_MontoDetalle.Visible = True
            '            Me.txtMontoValorNotaCredito.Text = Me.lblTotalDocRef.Text
            '            Me.txtRefrendo.Text = Me.cboTipoOperacion.SelectedItem.ToString


            '        Case 2 '************* Guia de Recepcion

            '            Me.Panel_ListaProducto.Visible = True
            '            Dim cbo As New Combo
            '            cbo.llenarCboRelacionDocumentoxIdTipoDocumento(cboGuiaRecepcion, CInt(Me.hddIdDocumentoRef.Value), 25)
            '            If cboGuiaRecepcion.Items.Count > 0 Then
            '                Me.cboGuiaRecepcion.Visible = True
            '                onChange_cboGuiaRecepcion(CInt(Me.cboGuiaRecepcion.SelectedValue))
            '            End If

            '    End Select

            'Else



            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo

                    Select Case CInt(Value)
                        Case 0 '************* POR LISTA DE PRODUCTOS
                            Me.Panel_ListaProducto.Visible = True

                            '*************** Detalle Documento
                            Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumento(CInt(Me.hddIdDocumentoRef.Value))
                            Me.listaDetalleconcepto = (New Negocio.DetalleConcepto).SelectxIdDocumento(CInt(Me.hddIdDocumentoRef.Value))

                            '***************************** Monedas
                            Dim objUtil As New Negocio.Util
                            Dim IdMonedaOrigen, IdMonedaDestino As Integer
                            IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                            IdMonedaOrigen = CInt(Me.hddIdMonedaDocRef.Value)

                            For i As Integer = 0 To listaDetalle.Count - 1

                                'listaDetalle(i).PrecioCD = Math.Round(objUtil.SelectValorxIdMonedaOxIdMonedaD(IdMonedaOrigen, IdMonedaDestino, listaDetalle(i).PrecioCD, "E"), 2)
                                listaDetalle(i).PrecioCD = IIf(listaDetalle(i).ComponenteKit, 0, Math.Round(objUtil.SelectValorxIdMonedaOxIdMonedaD(IdMonedaOrigen, IdMonedaDestino, listaDetalle(i).PrecioCD, "E"), 2))
                                listaDetalle(i).IdDetalleAfecto = listaDetalle(i).IdDetalleDocumento
                                listaDetalle(i).CantxAtender = listaDetalle(i).Cantidad
                                listaDetalle(i).PrecioSD = listaDetalle(i).PrecioCD
                                listaDetalle(i).CaxAtender = listaDetalle(i).CaxAtender
                            
                                'listaDetalle(i).ProdNombreKit = listaDetalle(i).ProdNombreKit
                                'listaDetalleconcepto(i).Monto = listaDetalleconcepto(i).Monto
                            Next

                            Me.GV_Detalle.DataSource = listaDetalle
                            Me.GV_Detalle.DataBind()

                            Me.GV_Concepto.DataSource = Me.listaDetalleconcepto
                            Me.GV_Concepto.DataBind()
                            If cboGuiaRecepcion.Items.Count > 0 Then
                                Me.cboGuiaRecepcion.Visible = True
                                Try
                                    Dim objDocValidar As New Negocio.Documento
                                    objDocValidar.validarNotaCredito(Me.hddIdDocumentoRef.Value, IIf(Me.cboGuiaRecepcion.SelectedValue = "", 0, Me.cboGuiaRecepcion.SelectedValue), Me.cboTipoOperacion.SelectedValue, Me.cboMotivo.SelectedValue, 0, "")
                                Catch ex As Exception
                                    Me.cboGuiaRecepcion.SelectedIndex = -1
                                    Me.cboGuiaRecepcion.Visible = False
                                    cboNotaCreditoModo.SelectedIndex = 0
                                    Throw ex
                                End Try
                                'onChange_cboGuiaRecepcion(CInt(Me.cboGuiaRecepcion.SelectedValue))
                            End If
                        Case 1 '************* POR MONTO FINAL

                            Me.Panel_MontoDetalle.Visible = True
                            Me.txtMontoValorNotaCredito.Text = Me.lblTotalDocRef.Text
                            Me.txtRefrendo.Text = Me.cboTipoOperacion.SelectedItem.ToString

                        Case 2 '************* POR PRODUCTOS RECEPCIONADAS

                            Me.Panel_ListaProducto.Visible = True
                            Dim cbo As New Combo
                            cbo.llenarCboRelacionDocumentoxIdTipoDocumento(cboGuiaRecepcion, CInt(Me.hddIdDocumentoRef.Value), 25)
                            If cboGuiaRecepcion.Items.Count > 0 Then
                                Me.cboGuiaRecepcion.Visible = True
                                Try
                                    Dim objDocValidar As New Negocio.Documento
                                    objDocValidar.validarNotaCredito(Me.hddIdDocumentoRef.Value, IIf(Me.cboGuiaRecepcion.SelectedValue = "", 0, Me.cboGuiaRecepcion.SelectedValue), Me.cboTipoOperacion.SelectedValue, Me.cboMotivo.SelectedValue, 0, "")
                                Catch ex As Exception
                                    Me.cboGuiaRecepcion.SelectedIndex = -1
                                    Me.cboGuiaRecepcion.Visible = False
                                    cboNotaCreditoModo.SelectedIndex = 0
                                    Throw ex
                                End Try
                                onChange_cboGuiaRecepcion(CInt(Me.cboGuiaRecepcion.SelectedValue))
                            End If

                            '*************** Detalle concepto
                            Me.listaDetalleconcepto = (New Negocio.DetalleConcepto).SelectxIdDocumento(CInt(Me.hddIdDocumentoRef.Value))

                            Me.GV_Concepto.DataSource = Me.listaDetalleconcepto
                            Me.GV_Concepto.DataBind()
                    End Select
                Case FrmModo.Editar

                    Select Case CInt(Value)
                        Case 0 '************* POR LISTA DE PRODUCTOS

                            Me.Panel_ListaProducto.Visible = True

                            '*************** Detalle Documento
                            Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumento(CInt(Me.hddIdDocumentoRef.Value))
                            Me.listaDetalleconcepto = (New Negocio.DetalleConcepto).SelectxIdDocumento(CInt(Me.hddIdDocumentoRef.Value))


                            '***************************** Monedas
                            Dim objUtil As New Negocio.Util
                            Dim IdMonedaOrigen, IdMonedaDestino As Integer
                            IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                            IdMonedaOrigen = CInt(Me.hddIdMonedaDocRef.Value)

                            For i As Integer = 0 To listaDetalle.Count - 1

                                listaDetalle(i).PrecioCD = Math.Round(objUtil.SelectValorxIdMonedaOxIdMonedaD(IdMonedaOrigen, IdMonedaDestino, listaDetalle(i).PrecioCD, "E"), 2)
                                listaDetalle(i).IdDetalleAfecto = listaDetalle(i).IdDetalleDocumento
                                listaDetalle(i).CantxAtender = listaDetalle(i).Cantidad
                                listaDetalle(i).PrecioSD = listaDetalle(i).PrecioCD
                                'listaDetalle(i).ProdNombreKit = listaDetalle(i).ProdNombreKit
                            Next

                            Me.GV_Detalle.DataSource = listaDetalle
                            Me.GV_Detalle.DataBind()

                            Me.GV_Concepto.DataSource = Me.listaDetalleconcepto
                            Me.GV_Concepto.DataBind()

                        Case 1 '************* POR MONTO FINAL

                            Me.Panel_MontoDetalle.Visible = True
                            Me.txtMontoValorNotaCredito.Text = Me.txtTotal.Text
                            Me.txtRefrendo.Text = Me.cboTipoOperacion.SelectedItem.ToString

                        Case 2 '************* POR PRODUCTOS RECEPCIONADAS

                            Me.Panel_ListaProducto.Visible = True
                            Dim cbo As New Combo
                            cbo.llenarCboRelacionDocumentoxIdTipoDocumento(cboGuiaRecepcion, CInt(Me.hddIdDocumentoRef.Value), 25)
                            If cboGuiaRecepcion.Items.Count > 0 Then
                                Me.cboGuiaRecepcion.Visible = True
                                onChange_cboGuiaRecepcion(CInt(Me.cboGuiaRecepcion.SelectedValue))
                            End If

                            '*************** Detalle concepto
                            Me.listaDetalleconcepto = (New Negocio.DetalleConcepto).SelectxIdDocumento(CInt(Me.hddIdDocumentoRef.Value))


                            Me.GV_Concepto.DataSource = Me.listaDetalleconcepto
                            Me.GV_Concepto.DataBind()

                    End Select

            End Select

            'Me.GV_Detalle.Enabled = False
            Dim valor As Integer = CInt(hddIdDocumentoRef.Value)
            Dim obj As Boolean = (New Negocio.DetalleDocumento).BuscaCheckNotCred(valor)


            If (lblguardadata.Text = "1") Then
                Me.cboNotaCreditoModo.SelectedValue = CStr(Value)
                If (cboGuiaRecepcion.Items.Count >= 1) Then  ''SI HAY GUÍA DE RECEPCIÓN.
                    lblguardadata.Text = "2"
                Else
                    lblguardadata.Text = "1"                 ''NO HAY GUÍA DE RECEPCIÓN. 
                End If
            ElseIf (lblguardadata.Text = "0" And obj = True) Then           ''no hay orden de despacho
                If (cboGuiaRecepcion.Items.Count >= 1) Then  ''SI HAY GUÍA DE RECEPCIÓN.
                    lblguardadata.Text = "4"
                ElseIf (obj = True) Then
                    lblguardadata.Text = "5"
                Else

                    lblguardadata.Text = "0"
                End If
                End If



                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularMontos('0');", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboNotaCreditoModo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNotaCreditoModo.SelectedIndexChanged
        actualizarModoDetalleNotaCredito(CInt(Me.cboNotaCreditoModo.SelectedValue))
    End Sub

    Private Function obtenerRelacionDocumento() As Entidades.RelacionDocumento

        Dim objRelacionDocumento As New Entidades.RelacionDocumento

        With objRelacionDocumento

            .IdDocumento1 = CInt(Me.hddIdDocumentoRef.Value)

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .IdDocumento2 = Nothing
                Case FrmModo.Editar
                    .IdDocumento2 = CInt(Me.hddIdDocumento.Value)
            End Select


        End With

        Return objRelacionDocumento

    End Function
    Private Function obtenerRelacionDocumentoGuia() As Entidades.RelacionDocumento

        Dim objRelDocGuia As Entidades.RelacionDocumento = Nothing

        If cboGuiaRecepcion.Items.Count > 0 Then
            objRelDocGuia = New Entidades.RelacionDocumento

            With objRelDocGuia

                .IdDocumento1 = CInt(Me.cboGuiaRecepcion.SelectedValue)

                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento2 = Nothing
                    Case FrmModo.Editar
                        .IdDocumento2 = CInt(Me.hddIdDocumento.Value)
                End Select


            End With
        End If

        Return objRelDocGuia

    End Function
    Private Function obtenerObservacion() As Entidades.Observacion

        Dim objObservacion As Entidades.Observacion = Nothing

        If (Me.txtObservaciones.Text.Trim.Length > 0) Then

            objObservacion = New Entidades.Observacion
            With objObservacion

                .Id = Nothing
                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento = Nothing
                    Case FrmModo.Editar
                        .IdDocumento = CInt(Me.hddIdDocumento.Value)
                End Select
                .Observacion = Me.txtObservaciones.Text.Trim

            End With

        End If

        Return objObservacion
    End Function
    Private Sub RegistrarDocumento()


        Try

            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()
            Dim listaDetalleDocumento As List(Of Entidades.DetalleDocumento) = obtenerListaDetalleDocumento_Save()
            Dim objRelacionDocumento As Entidades.RelacionDocumento = obtenerRelacionDocumento()
            Dim oRelDocGuia As Entidades.RelacionDocumento = obtenerRelacionDocumentoGuia()
            Dim objObservacion As Entidades.Observacion = obtenerObservacion()
            Dim ListaDetalleConcepto As List(Of Entidades.DetalleConcepto) = obtenerDetalleConcepto()

            ''Cambiando el valor de Cantidad por Atender a los productos compuestos.
            Dim valor As Integer = objRelacionDocumento.IdDocumento1
            Dim obj As Boolean = (New Negocio.DetalleDocumento).BuscaProdcompuesto(valor)

            ''Obteniendo la cantidad total de productos para actualizar la modificación hecha lineas arriba
            Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumento(CInt(Me.hddIdDocumentoRef.Value))

            ''Buscando y actualizando los valores de Cantidad xAtender cambiado lineas arriba.
            For Each i As Entidades.DetalleDocumento In listaDetalleDocumento
                For Each j As Entidades.DetalleDocumento In listaDetalle
                    If i.IdDetalleDocumento.Equals(j.IdDetalleDocumento) = True Then
                        i.CaxAtender = j.CaxAtender
                        Exit For
                    End If
                Next
            Next
            ''Actualizando la grilla con información actualizada.
            Me.GV_Detalle.DataSource = listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            'If (Me.cboMotivo.SelectedValue = "9" Or Me.cboMotivo.SelectedValue = "60000001") Then

            '    ''Buscando y validando si hay dispobible cantidad por atender.
            '    If (lblguardadata.Text = "1" Or lblguardadata.Text = "0" Or lblguardadata.Text = "3") Then

            '        For Each gvrowx As GridViewRow In GV_Detalle.Rows
            '            Dim lblx As HiddenField = DirectCast(gvrowx.FindControl("hddcantxatender"), HiddenField)
            '            If (CDec(lblx.Value) = 0) Then
            '                lblguardadata.Text = "3"
            '            End If
            '        Next
            '    End If

            '    If (lblguardadata.Text = "1") Then
            '        lblguardadata.Text = "2"
            '    End If

            '    If (lblguardadata.Text = "2" Or lblguardadata.Text = "0" Or lblguardadata.Text = "4") Then

            '        Select Case CInt(Me.hddFrmModo.Value)
            '            Case FrmModo.Nuevo
            '                objDocumento.Id = (New Negocio.DocumentoNotaCredito).RegistrarDocumentoNotaCredito(objDocumento, listaDetalleDocumento, objRelacionDocumento, objObservacion, ListaDetalleConcepto, oRelDocGuia)
            '            Case FrmModo.Editar
            '                objDocumento.Id = (New Negocio.DocumentoNotaCredito).ActualizarDocumentoNotaCredito(objDocumento, listaDetalleDocumento, objRelacionDocumento, objObservacion, ListaDetalleConcepto)
            '        End Select

            '        If (objDocumento.Id > 0) Then
            '            Me.hddIdDocumento.Value = CStr(objDocumento.Id)
            '            verFrm(FrmModo.Documento_Save_Exito, False, False)
            '            ''Agregar volver al estado normal a los Productos Compuestos.
            '            Dim objx As Boolean = (New Negocio.DetalleDocumento).CambiarProdcompuesto(valor)
            '            objScript.mostrarMsjAlerta(Me, "La Operación se realizó con Éxito.")
            '        Else
            '            Throw New Exception("Problemas en la Operación.")
            '        End If

            '    ElseIf (lblguardadata.Text = "1") Then
            '        Dim objx As Boolean = (New Negocio.DetalleDocumento).CambiarProdcompuesto(valor)
            '        objScript.mostrarMsjAlerta(Me, "Verifique el Documento de Recepción, ya que se ha generado un Documento de Despacho.")
            '    ElseIf (lblguardadata.Text = "5") Then
            '        If (Me.cboMotivo.SelectedValue = "9" Or Me.cboMotivo.SelectedValue = "60000001") Then
            '            Select Case CInt(Me.hddFrmModo.Value)
            '                Case FrmModo.Nuevo
            '                    objDocumento.Id = (New Negocio.DocumentoNotaCredito).RegistrarDocumentoNotaCredito(objDocumento, listaDetalleDocumento, objRelacionDocumento, objObservacion, ListaDetalleConcepto, oRelDocGuia)
            '                Case FrmModo.Editar
            '                    objDocumento.Id = (New Negocio.DocumentoNotaCredito).ActualizarDocumentoNotaCredito(objDocumento, listaDetalleDocumento, objRelacionDocumento, objObservacion, ListaDetalleConcepto)
            '            End Select
            '            Me.hddIdDocumento.Value = CStr(objDocumento.Id)
            '            verFrm(FrmModo.Documento_Save_Exito, False, False)
            '            ''Agregar volver al estado normal a los Productos Compuestos.
            '            Dim objx As Boolean = (New Negocio.DetalleDocumento).CambiarProdcompuesto(valor)
            '            objScript.mostrarMsjAlerta(Me, "La Operación se realizó con Éxito.")
            '        End If
            '    Else
            '        Dim objx As Boolean = (New Negocio.DetalleDocumento).CambiarProdcompuesto(valor)
            '        objScript.mostrarMsjAlerta(Me, "No se puede realizar una nota de credito a un producto que no tiene cantidad por Atender")
            '    End If

            'Else
            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    objDocumento.Id = (New Negocio.DocumentoNotaCredito).RegistrarDocumentoNotaCredito(objDocumento, listaDetalleDocumento, objRelacionDocumento, objObservacion, ListaDetalleConcepto, oRelDocGuia)
                Case FrmModo.Editar
                    objDocumento.Id = (New Negocio.DocumentoNotaCredito).ActualizarDocumentoNotaCredito(objDocumento, listaDetalleDocumento, objRelacionDocumento, objObservacion, ListaDetalleConcepto)
            End Select

            If (objDocumento.Id > 0) Then
                Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                verFrm(FrmModo.Documento_Save_Exito, False, False)
                ''Agregar volver al estado normal a los Productos Compuestos.
                Dim objx As Boolean = (New Negocio.DetalleDocumento).CambiarProdcompuesto(valor)

                objScript.mostrarMsjAlerta(Me, "La Operación se realizó con Éxito.")
            Else
                Throw New Exception("Problemas en la Operación.")
            End If
            'End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaDetalleDocumento_Save() As List(Of Entidades.DetalleDocumento)

        Dim listaDetalle As List(Of Entidades.DetalleDocumento) = obtenerListaDetalle()

        For i As Integer = 0 To listaDetalle.Count - 1

            listaDetalle(i).CantxAtender = Nothing

            If CInt(Me.cboNotaCreditoModo.SelectedValue) = 2 Then
                listaDetalle(i).IdDetalleAfecto = 0
            End If

        Next

        Return listaDetalle
    End Function
    Private Function obtenerDetalleConcepto() As List(Of Entidades.DetalleConcepto)

        Dim Lista_DetalleConcepto As List(Of Entidades.DetalleConcepto) = Nothing

        Select Case CInt(Me.cboNotaCreditoModo.SelectedValue)


            Case 1 '************** MONTO

                Dim objDetalleConcepto As New Entidades.DetalleConcepto
                With objDetalleConcepto
                    .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
                    .Monto = CDec(Me.txtMontoValorNotaCredito.Text)
                    .IdConcepto = Nothing
                    .Concepto = Me.txtRefrendo.Text
                End With

                Lista_DetalleConcepto = New List(Of Entidades.DetalleConcepto)
                Lista_DetalleConcepto.Add(objDetalleConcepto)

            Case Else  '************** Otros casos

                Lista_DetalleConcepto = obtenerListaDetalleconcepto()

        End Select
        Return Lista_DetalleConcepto
    End Function

    Protected Sub btnBuscarDocumentoxCodigo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumentoxCodigo.Click
        cargarDocumentoNotaCredito(CInt(Me.cboSerie.SelectedValue), CInt(Me.hddCodigoDocumento.Value), 0)
    End Sub
    Private Sub cargarDocumentoNotaCredito(ByVal IdSerie As Integer, ByVal codigo As Integer, ByVal IdDocumento As Integer)
        Try

            Dim objDocumento As Entidades.DocumentoNotaCredito = (New Negocio.DocumentoNotaCredito).DocumentoNotaCreditoSelectCab(IdSerie, codigo, IdDocumento)
            Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumento(objDocumento.Id)
            Me.listaDetalleconcepto = (New Negocio.DetalleConcepto).SelectxIdDocumento(objDocumento.Id)


            Dim objDetalleConcepto As Entidades.DetalleConcepto = obtenerDetalleConcepto_Load(objDocumento.Id)

            '************************  CARGAMOS LA CANTIDAD DE REFERENCIA
            For I As Integer = 0 To listaDetalle.Count - 1
                listaDetalle(I).CantxAtender = listaDetalle(I).CantidadDetalleAfecto
            Next

            cargarDocumentoNotaCreditoInterface(objDocumento, listaDetalle, objDetalleConcepto, listaDetalleconcepto)

            valAuxiliares()

            verFrm(FrmModo.Documento_Buscar_Exito, False, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function obtenerDetalleConcepto_Load(ByVal IdDocumento As Integer) As Entidades.DetalleConcepto

        Dim lista As List(Of Entidades.DetalleConcepto) = (New Negocio.DetalleConcepto).SelectxIdDocumento(IdDocumento)
        If (lista.Count = 1) Then
            Return lista(0)
        End If
        Return Nothing

    End Function

    Private Sub cargarDocumentoNotaCreditoInterface(ByVal objDocumento As Entidades.DocumentoNotaCredito, ByVal listaDetalle As List(Of Entidades.DetalleDocumento), ByVal objDetalleConcepto As Entidades.DetalleConcepto, ByVal Lista_DetalleConcepto As List(Of Entidades.DetalleConcepto))

        '*********** CABECERA
        Me.txtCodigoDocumento.Text = objDocumento.Codigo
        Me.txtFechaEmision.Text = Format(objDocumento.FechaEmision, "dd/MM/yyyy")
        Me.cboEstado.SelectedValue = CStr(objDocumento.IdEstadoDoc)



        If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumento.IdTipoOperacion)) IsNot Nothing) Then
            Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)
            OnChange_TipoOperacion()
        End If

        If Not Me.cboMotivo.Items.FindByValue(CStr(objDocumento.IdMotivoT)) Is Nothing Then
            Me.cboMotivo.SelectedValue = CStr(objDocumento.IdMotivoT)
        End If

        If (Me.cboMoneda.Items.FindByValue(CStr(objDocumento.IdMoneda)) IsNot Nothing) Then
            Me.cboMoneda.SelectedValue = CStr(objDocumento.IdMoneda)
        End If

        If (Me.cboEstadoCancelacion.Items.FindByValue(CStr(objDocumento.IdEstadoCancelacion)) IsNot Nothing) Then
            Me.cboEstadoCancelacion.SelectedValue = CStr(objDocumento.IdEstadoCancelacion)
        End If


        If (objDocumento.FechaVenc = Nothing) Then
            Me.txtFechaVcto.Text = ""
        Else
            Me.txtFechaVcto.Text = Format(objDocumento.FechaVenc, "dd/MM/yyyy")
        End If

        If objDocumento.IdEstadoDoc = 3 Then '' ******************* SALTEADO

            Dim listaParametros() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {3})
            Me.txtFechaVcto.Text = Format(DateAdd(DateInterval.Day, listaParametros(0), CDate(Me.txtFechaEmision.Text)), "dd/MM/yyyy")

        End If

        '************** CLIENTE
        Me.txtDescripcionPersona.Text = objDocumento.DescripcionPersona
        Me.txtRUC.Text = objDocumento.Ruc
        Me.txtDNI.Text = objDocumento.Dni

        '*************** DOCUMENTO REFERENCIA
        Me.lblTipoDocumentoRef.Text = objDocumento.TipoDocumentoRef
        Me.lblNroDocumentoDocRef.Text = objDocumento.DocRef_Serie + " - " + objDocumento.DocRef_Codigo
        Me.lblFechaEmisionDocRef.Text = Format(objDocumento.DocRef_FechaEmision, "dd/MM/yyyy")
        Me.lblTotalDocRef.Text = CStr(Math.Round(objDocumento.DocRef_Total, 2, MidpointRounding.AwayFromZero))
        Me.lblPercepcionDocRef.Text = CStr(Math.Round(objDocumento.DocRef_Percepcion, 2, MidpointRounding.AwayFromZero))
        Me.lblTotalGeneralDocRef.Text = CStr(Math.Round(objDocumento.DocRef_TotalAPagar, 2, MidpointRounding.AwayFromZero))

        '**************** DETALLE
        If (listaDetalle.Count > 0) Then
            Me.GV_Detalle.DataSource = listaDetalle
            Me.GV_Detalle.DataBind()
        Else
            Me.txtMontoValorNotaCredito.Text = CStr(Math.Round(objDocumento.TotalAPagar, 2, MidpointRounding.AwayFromZero))
            If (objDetalleConcepto IsNot Nothing) Then
                Me.txtRefrendo.Text = objDetalleConcepto.Concepto
            End If

        End If


        If (Lista_DetalleConcepto.Count > 0) Then

            Me.listaDetalleconcepto = Lista_DetalleConcepto
            Me.GV_Concepto.DataSource = Me.listaDetalleconcepto
            Me.GV_Concepto.DataBind()
        Else

        End If


        '******************** TOTALES
        Me.txtSubTotal.Text = CStr(Math.Round(objDocumento.SubTotal, 2, MidpointRounding.AwayFromZero))
        Me.txtIGV.Text = CStr(Math.Round(objDocumento.IGV, 2, MidpointRounding.AwayFromZero))
        Me.txtTotal.Text = CStr(Math.Round((objDocumento.TotalAPagar), 2, MidpointRounding.AwayFromZero))
        'Me.txtTotal.Text = Me.lblTotalDocRef.Text
        'Me.txtTotal.Text = CStr(Math.Round((objDocumento.Total), 2))

        '************** OBSERVACIONES
        Me.txtObservaciones.Text = objDocumento.Observacion

        '******************* HIDDEN
        Me.hddIdDocumento.Value = CStr(objDocumento.Id)
        Me.hddIdDocumentoRef.Value = CStr(objDocumento.IdDocumentoRef)
        Me.hddIdPersona.Value = CStr(objDocumento.IdPersona)
        Me.hddIdMonedaDocRef.Value = CStr(objDocumento.DocRef_IdMoneda)

        '******************* GUIAS
        Dim cbo As New Combo
        cbo.llenarCboRelacionDocumentoxIdTipoDocumento(cboGuiaRecepcion, objDocumento.Id, 25)
        If cboGuiaRecepcion.Items.Count > 0 Then
            cboNotaCreditoModo.SelectedValue = "2"
            cboGuiaRecepcion.Visible = True
        End If

    End Sub

    Private Sub LimpiarFrm()

        '************* CABECERA
        Me.txtCodigoDocumento.Text = ""
        Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.txtFechaVcto.Text = Format(DateAdd(DateInterval.Day, 7, (New Negocio.FechaActual).SelectFechaActual), "dd/MM/yyyy")
        Me.cboEstado.SelectedValue = "1"  '******* Activo por defecto
        Me.cboTipoOperacion.SelectedIndex = 0  '******* SIN TIPO DE OPERACION POR DEFECTO
        Me.hddIdDocumento.Value = ""
        Me.hddCodigoDocumento.Value = ""
        Me.cboGuiaRecepcion.Visible = False
        Me.cboGuiaRecepcion.Items.Clear()
        Me.cboEstadoCancelacion.SelectedIndex = 0
        '************ DATOS DEL CLIENTE
        Me.txtDescripcionPersona.Text = ""
        Me.txtDNI.Text = ""
        Me.txtRUC.Text = ""
        Me.hddIdPersona.Value = ""

        '********** DETALLE
        Me.GV_Detalle.DataSource = Nothing
        Me.GV_Detalle.DataBind()
        Me.GV_Concepto.DataSource = Nothing
        Me.GV_Concepto.DataBind()
        Me.txtMontoValorNotaCredito.Text = "0"
        Me.txtRefrendo.Text = ""

        '************* TOTALES DEL DOCUMENTO
        Me.txtSubTotal.Text = "0"
        Me.txtIGV.Text = "0"
        Me.txtTotal.Text = "0"

        '*************** DOCUMENTO DE REFERENCIA
        Me.lblTipoDocumentoRef.Text = ""
        Me.lblNroDocumentoDocRef.Text = ""
        Me.lblFechaEmisionDocRef.Text = ""
        Me.lblTotalDocRef.Text = "0"
        Me.lblPercepcionDocRef.Text = "0"
        Me.lblTotalGeneralDocRef.Text = "0"

        '***************** OBSERVACIONES
        Me.txtObservaciones.Text = ""

        '************ HIDDEN
        Me.hddIdDocumentoRef.Value = ""
        Me.hddIdMonedaDocRef.Value = ""

        '************* Otros
        Me.txtSerie_BuscarDocRef.Text = ""
        Me.txtCodigo_BuscarDocRef.Text = ""

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Try
            verFrm(FrmModo.Nuevo, True, True)
            GenerarCodigoDocumento()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        Try
            'validar doc a editar
            Dim listaPermisos() As Integer = (New Negocio.Util).ValidarEdicionDocxIdUsuarioxIdPermiso(CInt(Session("IdUsuario")), New Integer() {2101100012}, Me.cboSerie.SelectedItem.Text, Me.txtCodigoDocumento.Text, Me.cboTipoDocumento.SelectedValue)
            Select Case listaPermisos(0)
                Case 0
                    objScript.mostrarMsjAlerta(Me, "No está permitido editar el documento con fecha anterior a la de hoy")
                Case 1
                    verFrm(FrmModo.Editar, False, False)

                    '*************** CARGAMOS EL DETALLE
                    Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumento(CInt(Me.hddIdDocumento.Value))
                    Me.listaDetalleconcepto = (New Negocio.DetalleConcepto).SelectxIdDocumento(CInt(Me.hddIdDocumento.Value))


                    If (listaDetalle.Count > 0) Then

                        For i As Integer = 0 To listaDetalle.Count - 1
                            listaDetalle(i).CantxAtender = listaDetalle(i).CantidadDetalleAfecto
                        Next

                        Me.GV_Detalle.DataSource = listaDetalle
                        Me.GV_Detalle.DataBind()

                    End If
                    If (Me.listaDetalleconcepto.Count > 0) Then
                        Me.GV_Concepto.DataSource = Me.listaDetalleconcepto
                        Me.GV_Concepto.DataBind()
                    End If
            End Select

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        AnularDocumentoNotaCredito()
    End Sub
    Private Sub AnularDocumentoNotaCredito()
        Try
            'validar doc a editar
            Dim listaPermisos() As Integer = (New Negocio.Util).ValidarEdicionDocxIdUsuarioxIdPermiso(CInt(Session("IdUsuario")), New Integer() {2101100008}, Me.cboSerie.SelectedItem.Text, Me.txtCodigoDocumento.Text, Me.cboTipoDocumento.SelectedValue)
            Select Case listaPermisos(0)
                Case 1
                    Dim IdDocumento As Integer = (New Negocio.DocumentoNotaCredito).AnularDocumentoNotaCredito(CInt(Me.hddIdDocumento.Value))
                    If (IdDocumento > 0) Then
                        Me.cboEstado.SelectedValue = "2"  '*********** ANULADO
                        verFrm(FrmModo.Documento_Anular_Exito, False, False)
                        objScript.mostrarMsjAlerta(Me, "La Operación se realizó con Éxito.")
                    Else
                        Throw New Exception("Problemas en la Operación.")
                    End If

            End Select
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboMoneda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMoneda.SelectedIndexChanged
        actualizarMontos_MonedaEmision()
    End Sub
    Private Sub actualizarMontos_MonedaEmision()

        Try

            Dim IdMonedaOrigen, IdMonedaDestino As Integer
            IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)

            If (IdMonedaDestino = 1) Then  '*********** SOLES
                IdMonedaOrigen = 2
            ElseIf (IdMonedaDestino = 2) Then  '**************** DOLARES
                IdMonedaOrigen = 1
            Else
                Throw New Exception("La Moneda Origen difiere de la Configuración del Sistema.")
            End If

            Dim objUtil As New Negocio.Util

            With objUtil

                Me.lblTotalDocRef.Text = CStr(Math.Round(.SelectValorxIdMonedaOxIdMonedaD(IdMonedaOrigen, IdMonedaDestino, CDec(Me.lblTotalDocRef.Text), "E"), 2, MidpointRounding.AwayFromZero))
                Me.lblPercepcionDocRef.Text = CStr(Math.Round(.SelectValorxIdMonedaOxIdMonedaD(IdMonedaOrigen, IdMonedaDestino, CDec(Me.lblPercepcionDocRef.Text), "E"), 2, MidpointRounding.AwayFromZero))
                Me.lblTotalGeneralDocRef.Text = CStr(Math.Round(.SelectValorxIdMonedaOxIdMonedaD(IdMonedaOrigen, IdMonedaDestino, CDec(Me.lblTotalGeneralDocRef.Text), "E"), 2, MidpointRounding.AwayFromZero))

                Dim lista As List(Of Entidades.DetalleDocumento) = obtenerListaDetalle()
                Me.listaDetalleconcepto = obtenerListaDetalleconcepto()

                For i As Integer = 0 To lista.Count - 1
                    lista(i).PrecioSD = Math.Round(.SelectValorxIdMonedaOxIdMonedaD(IdMonedaOrigen, IdMonedaDestino, lista(i).PrecioSD, "E"), 2)
                    lista(i).PrecioCD = Math.Round(.SelectValorxIdMonedaOxIdMonedaD(IdMonedaOrigen, IdMonedaDestino, lista(i).PrecioCD, "E"), 2)
                Next

                Me.GV_Detalle.DataSource = lista
                Me.GV_Detalle.DataBind()

                Me.GV_Concepto.DataSource = Me.listaDetalleconcepto
                Me.GV_Concepto.DataBind()

                Me.txtSubTotal.Text = CStr(Math.Round(.SelectValorxIdMonedaOxIdMonedaD(IdMonedaOrigen, IdMonedaDestino, CDec(Me.txtSubTotal.Text), "E"), 2, MidpointRounding.AwayFromZero))
                Me.txtIGV.Text = CStr(Math.Round(.SelectValorxIdMonedaOxIdMonedaD(IdMonedaOrigen, IdMonedaDestino, CDec(Me.txtIGV.Text), "E"), 2, MidpointRounding.AwayFromZero))

                Me.txtTotal.Text = CStr(Math.Round(.SelectValorxIdMonedaOxIdMonedaD(IdMonedaOrigen, IdMonedaDestino, (CDec(Me.txtTotal.Text)), "E"), 2, MidpointRounding.AwayFromZero))

            End With

            valAuxiliares()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularMontos('0');", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnCargarArticuloAll_DocRef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCargarArticuloAll_DocRef.Click
        actualizarModoDetalleNotaCredito(0, True) ' ****** LISTA TODOS LOS ARTICULOS 
    End Sub

    Private Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Private Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))
            objScript.offCapa(Me, "capaPersona")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)

        Dim objPersonaView As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)
        If (objPersonaView IsNot Nothing) Then

            Me.txtDescripcionPersona.Text = objPersonaView.Descripcion
            Me.txtDNI.Text = objPersonaView.Dni
            Me.txtRUC.Text = objPersonaView.Ruc
            Me.hddIdPersona.Value = CStr(objPersonaView.IdPersona)

        End If

    End Sub
#Region "************************ BÚSQUEDA AVANZADO"

    Private Sub btnAceptarBusquedaAvanzado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBusquedaAvanzado.Click
        busquedaAvanzado()
    End Sub

    Private Sub busquedaAvanzado()

        Try

            Dim IdPersona As Integer = 0
            Try
                IdPersona = CInt(Me.hddIdPersona.Value)
            Catch ex As Exception
                IdPersona = 0
            End Try

            Dim IdSerie As Integer = CInt(Me.cboSerie.SelectedValue)
            Dim fechaInicio As Date = CDate(Me.txtFechaInicio_BA.Text)
            Dim fechaFin As Date = CDate(Me.txtFechaFin_BA.Text)

            Me.GV_BusquedaAvanzado.DataSource = (New Negocio.Documento).DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(CInt(Me.cboTipoDocumento.SelectedValue), IdSerie, IdPersona, fechaInicio, fechaFin)
            Me.GV_BusquedaAvanzado.DataBind()

            objScript.onCapa(Me, "capaBusquedaAvanzado")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub GV_BusquedaAvanzado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_BusquedaAvanzado.PageIndexChanging
        Me.GV_BusquedaAvanzado.PageIndex = e.NewPageIndex
        busquedaAvanzado()
    End Sub

    Protected Sub GV_BusquedaAvanzado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BusquedaAvanzado.SelectedIndexChanged
        Try
            cargarDocumentoNotaCredito(0, 0, CInt(CType(Me.GV_BusquedaAvanzado.SelectedRow.FindControl("hddIdDocumento_BusquedaAvanzado"), HiddenField).Value))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

#End Region

    Private Sub cboGuiaRecepcion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGuiaRecepcion.SelectedIndexChanged
        onChange_cboGuiaRecepcion(CInt(cboGuiaRecepcion.SelectedValue))
    End Sub
    Private Sub onChange_cboGuiaRecepcion(ByVal IdDocumento As Integer)

        Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdFact_NC(CInt(Me.hddIdDocumentoRef.Value), CInt(cboGuiaRecepcion.SelectedValue))
        Me.listaDetalleconcepto = (New Negocio.DetalleConcepto).SelectxIdDocumento(CInt(Me.hddIdDocumentoRef.Value))

        For i As Integer = 0 To listaDetalle.Count - 1            
            listaDetalle(i).PrecioCD = IIf(listaDetalle(i).ComponenteKit, 0, listaDetalle(i).PrecioCD)
            listaDetalle(i).CantxAtender = listaDetalle(i).Cantidad
            listaDetalle(i).PrecioSD = listaDetalle(i).PrecioCD
        Next

        If (lblguardadata.Text = "1") Then
            If (cboGuiaRecepcion.Items.Count > 1) Then  ''SI HAY GUÍA DE RECEPCIÓN.
                lblguardadata.Text = "2"
            Else
                lblguardadata.Text = "1"                 ''NO HAY GUÍA DE RECEPCIÓN. 
            End If

        End If

        Me.GV_Detalle.DataSource = listaDetalle
        Me.GV_Detalle.DataBind()

        Me.GV_Concepto.DataSource = Me.listaDetalleconcepto
        Me.GV_Concepto.DataBind()


    End Sub

    Private Sub cboTipoOperacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoOperacion.SelectedIndexChanged
        OnChange_TipoOperacion()
    End Sub
    Private Sub OnChange_TipoOperacion()
        Dim objCbo As New Combo
        With objCbo
            .LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivo, CInt(Me.cboTipoOperacion.SelectedValue), True)
        End With
    End Sub

    Private Sub GV_Concepto_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Concepto.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then

            If Me.listaDetalleconcepto(e.Row.RowIndex).IdConcepto = 10 Then

                CType(e.Row.FindControl("txtConcepto"), TextBox).Enabled = False

            End If

            If Me.listaDetalleconcepto(e.Row.RowIndex).IdDocumentoRef > 0 Then

                CType(e.Row.FindControl("txtConcepto"), TextBox).Enabled = False

            End If



        End If

    End Sub
    Protected Sub gvDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gvDocRef.SelectedIndexChanged
        cargarDocRefNotaCredito()
    End Sub

    ''CREAMOS EL METODO PARA LLENAR EL GV. DE VALIDACION DE NOTA DE CREDITO
    Private Sub cargarDocRefNotaCredito()
        Try

            Me.lblNroDocumentoDocRef.Text = CType(Me.gvDocRef.SelectedRow.FindControl("lblNroDocumento_Find"), Label).Text
            Me.lblTipoDocumentoRef.Text = CType(Me.gvDocRef.SelectedRow.FindControl("lblTipoDocumentoRef"), Label).Text

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", 1)  '******************** CLIENTE

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboMotivo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMotivo.SelectedIndexChanged
        Call validaNCSegunTipoOperacion()
    End Sub

    Private Sub validaNCSegunTipoOperacion()
        Dim obj As New Negocio.Documento
        Dim dt As New DataTable
        Try
            dt = obj.activarCamposNC(Me.cboTipoOperacion.SelectedValue, Me.cboMotivo.SelectedValue)
            If dt.Rows.Count > 0 Then
                Dim flagJalarRecepcion As Boolean = False
                Dim flagQuitarItems As Boolean = False
                Dim validaMontoTotal As Boolean = False

                flagJalarRecepcion = CBool(dt.Rows(0).Item("flagJalarRecepcion"))
                flagQuitarItems = CBool(dt.Rows(0).Item("flagQuitarItems"))
                validaMontoTotal = CBool(dt.Rows(0).Item("validaMontoTotal"))

                If Me.GV_Detalle.Rows.Count > 0 Then
                    If flagJalarRecepcion Then
                        If cboGuiaRecepcion.SelectedValue = "" Then
                            Throw New Exception("El motivo  " + Me.cboMotivo.SelectedItem.Text + " requiere que referencie una Guia de Recepción.\nOperación Incorrecta.")
                        End If
                    End If
                End If




                If ((cboMotivo.SelectedValue <> 2102158006) And (cboMotivo.SelectedValue <> 2102158001)) Then
                    If validaMontoTotal Then
                        Dim lblTotalDocRefe As Decimal = CDec(Me.lblTotalGeneralDocRef.Text)
                        Dim lblTotalDocTotalNC As Decimal = CDec(Me.txtTotal.Text)

                        'Redondeo de 0.02
                        'Dim diferencia As Decimal = (lblTotalDocRefe - lblTotalDocTotalNC)
                        'If (diferencia > 0 And diferencia <= 0.02) Then
                        '    Me.txtTotal.Text = Me.lblTotalGeneralDocRef.Text
                        '    lblTotalDocRefe = lblTotalDocTotalNC
                        'End If
                        'fin redondeo


                        If lblTotalDocRefe <> lblTotalDocTotalNC Then
                            Throw New Exception("El motivo  " + Me.cboMotivo.SelectedItem.Text + " requiere el monto total del documento de Referencia " + Me.lblTotalGeneralDocRef.Text + " coincida con el monto total de la NC.")
                        End If

                    End If
                End If

                If (cboMotivo.SelectedValue = 2102158001) Then
                    If validaMontoTotal Then
                        'Dim lblTotalDocRefe As Decimal = CDec(Me.lblTotalGeneralDocRef.Text)
                        Dim lblTotalDocRefe As Decimal = CDec(Me.lblTotalDocRef.Text)
                        Dim lblTotalDocTotalNC As Decimal = CDec(Me.txtTotal.Text)

                      
                        If lblTotalDocRefe <> lblTotalDocTotalNC Then
                            Throw New Exception("El motivo  " + Me.cboMotivo.SelectedItem.Text + " requiere el monto total del documento de Referencia " + Me.lblTotalGeneralDocRef.Text + " coincida con el monto total de la NC.")
                        End If

                    End If
                End If

                If (cboMotivo.SelectedValue = 2102158006) Then
                    If validaMontoTotal Then
                        Dim lblTotalDocRefe As Decimal = CDec(Me.lblTotalDocRef.Text)
                        Dim lblTotalDocTotalNC As Decimal = CDec(Me.txtTotal.Text)

                        'Redondeo de 0.02
                        'Dim diferencia2 As Decimal = (lblTotalDocRefe - lblTotalDocTotalNC)
                        'If (diferencia2 > 0 And diferencia2 <= 0.02) Then
                        '    Me.txtTotal.Text = Me.lblTotalGeneralDocRef.Text
                        '    lblTotalDocRefe = lblTotalDocTotalNC
                        'End If
                        'fin redondeo


                        If lblTotalDocRefe <> lblTotalDocTotalNC Then
                            Throw New Exception("El motivo  " + Me.cboMotivo.SelectedItem.Text + " requiere el monto total del documento de Referencia " + Me.lblTotalGeneralDocRef.Text + " coincida con el monto total de la NC.")
                        End If
                    End If
                End If





                Me.cboNotaCreditoModo.Enabled = flagJalarRecepcion
                Me.GV_Detalle.Enabled = flagQuitarItems
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
            Throw ex
        End Try

    End Sub

    Private Sub cboTipoDocumento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoDocumento.SelectedIndexChanged

        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboTipoDocumento.SelectedValue))
            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
End Class