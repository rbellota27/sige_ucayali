﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FrmReciboIngreso

    '''<summary>
    '''Control lblCaja.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblCaja As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control btnNuevo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnNuevo As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnEditar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnEditar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnBuscar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnAnular.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAnular As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnGuardar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnGuardar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnImprimir.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnImprimir As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnDocumentoRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnDocumentoRef As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control Panel_Cab.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_Cab As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control cboEmpresa.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboEmpresa As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control cboTienda.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboTienda As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control cboSerie.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboSerie As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control Panel1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel1 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtCodigoDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtCodigoDocumento As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnBuscarDocumentoxCodigo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscarDocumentoxCodigo As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control btnBusquedaAvanzado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBusquedaAvanzado As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control txtFechaEmision.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaEmision As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtFechaEmision_MaskedEditExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaEmision_MaskedEditExtender As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control txtFechaEmision_CalendarExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaEmision_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control cboMoneda.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboMoneda As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control cboEstado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboEstado As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control cboCaja.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboCaja As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control cboTipoOperacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboTipoOperacion As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control Panel_Cliente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_Cliente As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtDescripcionPersona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDescripcionPersona As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnBuscarPersona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscarPersona As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control txtDNI.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDNI As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtRUC.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtRUC As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control Panel_DocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_DocRef As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control GV_DocumentoRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GV_DocumentoRef As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control Panel_Detalle.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_Detalle As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control cboMotivoReciboIngreso.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboMotivoReciboIngreso As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control lblMessageHelp.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblMessageHelp As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control GV_Deudas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GV_Deudas As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control btnAddConcepto_Otros.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAddConcepto_Otros As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control GV_Otros.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GV_Otros As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control Panel_Cancelacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_Cancelacion As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control lblMonedaTotalAPagar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblMonedaTotalAPagar As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control txtTotalAPagar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtTotalAPagar As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control Panel_CP_Contado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_CP_Contado As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control lblMedioPago_DC.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblMedioPago_DC As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control cboMedioPago.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboMedioPago As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control lblMonto_DC.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblMonto_DC As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control cboMoneda_DatoCancelacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboMoneda_DatoCancelacion As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control Panel7.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel7 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtMonto_DatoCancelacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtMonto_DatoCancelacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnAddDatoCancelacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAddDatoCancelacion As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnVer_NotaCredito.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnVer_NotaCredito As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control lblPost_DC.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblPost_DC As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control cboPost_DC.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboPost_DC As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control lblTarjeta_DC.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTarjeta_DC As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control cboTarjeta_DC.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboTarjeta_DC As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control lblBanco_DC.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblBanco_DC As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control cboBanco.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboBanco As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control lblCuentaBancaria_DC.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblCuentaBancaria_DC As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control cboCuentaBancaria.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboCuentaBancaria As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control lblNro_DC.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblNro_DC As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control txtNro_DC.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtNro_DC As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control lblFechaACobrar_DC.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblFechaACobrar_DC As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control txtFechaACobrar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaACobrar As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control MaskedEditExtender6.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MaskedEditExtender6 As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control CalendarExtender5.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CalendarExtender5 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control GV_Cancelacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GV_Cancelacion As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control lblMonedaTotalRecibido.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblMonedaTotalRecibido As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control txtTotalRecibido.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtTotalRecibido As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control lblMonedaFaltante.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblMonedaFaltante As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control txtFaltante.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFaltante As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control lblMonedaVuelto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblMonedaVuelto As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control txtVuelto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtVuelto As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control Panel_Obs.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_Obs As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtObservaciones.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtObservaciones As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control hddFrmModo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddFrmModo As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddIdDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddIdDocumento As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddIdTipoDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddIdTipoDocumento As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddCodigoDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddCodigoDocumento As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddIdPersona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddIdPersona As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddIdMedioPagoPrincipal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddIdMedioPagoPrincipal As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddIdMedioPagoInterfaz.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddIdMedioPagoInterfaz As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddPermiso_AddMedioPago.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddPermiso_AddMedioPago As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddConfigurarDatos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddConfigurarDatos As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control ImageButton8.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ImageButton8 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control Panel_NotaCredito.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_NotaCredito As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control GV_NotaCredito.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GV_NotaCredito As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control Button1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Button1 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control ImageButton1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ImageButton1 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control GV_Abonos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GV_Abonos As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control btnCerrar_Capa.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnCerrar_Capa As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control pnlBusquedaPersona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlBusquedaPersona As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control rdbTipoPersona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents rdbTipoPersona As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''Control Panel2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel2 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control tbRazonApe.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbRazonApe As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control Panel3.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel3 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control tbbuscarDni.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbbuscarDni As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control Panel4.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel4 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control tbbuscarRuc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbbuscarRuc As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btBuscarPersonaGrillas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btBuscarPersonaGrillas As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control gvBuscar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents gvBuscar As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control btAnterior.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btAnterior As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btSiguiente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btSiguiente As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control tbPageIndex.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbPageIndex As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btIr.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btIr As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control tbPageIndexGO.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbPageIndexGO As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control ImageButton2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ImageButton2 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control Panel_BusquedaAvanzado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_BusquedaAvanzado As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtFechaInicio_BA.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaInicio_BA As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control MaskedEditExtender_txtFechaInicio_BA.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MaskedEditExtender_txtFechaInicio_BA As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control CalendarExtender_txtFechaInicio_BA.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CalendarExtender_txtFechaInicio_BA As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control txtFechaFin_BA.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaFin_BA As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control MaskedEditExtender_txtFechaFin_BA.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MaskedEditExtender_txtFechaFin_BA As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control CalendarExtender_txtFechaFin_BA.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CalendarExtender_txtFechaFin_BA As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control btnAceptarBusquedaAvanzado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAceptarBusquedaAvanzado As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control GV_BusquedaAvanzado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GV_BusquedaAvanzado As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control ImageButton12.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ImageButton12 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control txtUsuario_AddMedioPago.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtUsuario_AddMedioPago As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtClave_AddMedioPago.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtClave_AddMedioPago As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtClave2_AddMedioPago.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtClave2_AddMedioPago As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnAceptar_AddMedioPago.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAceptar_AddMedioPago As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnCerrar_AddMedioPago.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnCerrar_AddMedioPago As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control ImageButton6.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ImageButton6 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control CboTipoDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CboTipoDocumento As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control rdbBuscarDocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents rdbBuscarDocRef As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''Control Panel_BuscarDocRefxFecha.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_BuscarDocRefxFecha As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtFechaInicio_DocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaInicio_DocRef As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control MaskedEditExtender4.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MaskedEditExtender4 As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control CalendarExtender3.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CalendarExtender3 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control txtFechaFin_DocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaFin_DocRef As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control MaskedEditExtender5.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MaskedEditExtender5 As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control CalendarExtender4.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CalendarExtender4 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control btnAceptarBuscarDocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAceptarBuscarDocRef As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control Panel_BuscarDocRefxCodigo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_BuscarDocRefxCodigo As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control Panel5.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel5 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtSerie_BuscarDocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtSerie_BuscarDocRef As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control Panel6.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel6 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtCodigo_BuscarDocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtCodigo_BuscarDocRef As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnAceptarBuscarDocRefxCodigo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAceptarBuscarDocRefxCodigo As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control Panel_DocRef_Find.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_DocRef_Find As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control GV_DocumentosReferencia_Find.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GV_DocumentosReferencia_Find As Global.System.Web.UI.WebControls.GridView
End Class
