<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmNotaCreditoAplicacion.aspx.vb" Inherits="APPWEB.FrmNotaCreditoAplicacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnGuardar" runat="server" OnClientClick="return( valOnClick_btnGuardar()  );"
                                Text="Guardar" ToolTip="Guardar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnDeshacerAplicacion" OnClientClick="return(  valOnClick_btnDeshacerAplicacion()  );"
                                Width="260px" runat="server" Text="[ Deshacer Aplicaci�n de Nota de Cr�dito ]"
                                ToolTip="Deshace la aplicaci�n de la Nota de Cr�dito a TODOS los Documentos a Cr�dito a los cuales halla sido aplicado." />
                        </td>
                        <td>
                            <asp:Button ID="btnActualizarSaldoFavor" OnClientClick="return(  valOnClick_btnActualizarSaldoFavor()     );"
                                Width="290px" runat="server" Text="[ Actualizar Saldo a Favor de Nota de Cr�dito ]"
                                ToolTip="Permite actualizar el Saldo Restante de la Nota de Cr�dito a favor del Cliente a un valor menor o igual al monto original del documento." />
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                NOTA DE CR�DITO - APLICACI�N
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td align="right" class="Texto">
                            Empresa:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="right" class="Texto">
                            Tienda:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="right" class="Texto">
                            Caja:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboCaja" ForeColor="Red" Font-Bold="true" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                CLIENTE
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cliente" runat="server">
                    <table>
                        <tr>
                            <td align="right" class="Texto">
                                Descripci�n:
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtDescripcionPersona" runat="server" CssClass="TextBox_ReadOnlyLeft"
                                    ReadOnly="true" Width="400px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarPersona" runat="server" OnClientClick="return( valOnClick_btnBuscarPersona()  );" CssClass="btnBuscar"
                                    Text="Buscar" ToolTip="Buscar Cliente" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                D.N.I.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDNI" runat="server" CssClass="TextBox_ReadOnlyLeft" ReadOnly="true"></asp:TextBox>
                            </td>
                            <td align="right" class="Texto">
                                R.U.C.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRUC" runat="server" CssClass="TextBox_ReadOnlyLeft" ReadOnly="true"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnAddNotaCredito" OnClientClick="return(   valOnClick_btnAddNotaCredito()   );"
                                            Width="160px" runat="server" Text="Agregar Nota de Cr�dito" ToolTip="Agregar Nota de Cr�dito." />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_NotaCreditoApp" runat="server" Width="100%" ScrollBars="Horizontal">
                                <asp:GridView ID="GV_NotaCreditoApp" runat="server" AutoGenerateColumns="false" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Tipo" />
                                        <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblNroDocumento" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Numero")%>'></asp:Label>

                                                            <asp:HiddenField ID="hddIdDocumentoReferencia" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />

                                                            <asp:HiddenField ID="hddIdMonedaDocRef" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />

                                                            <asp:HiddenField ID="hddIdEstadoCancelacionDocRef" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEstadoCancelacion")%>' />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisi�n"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="FechaVenc" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Vcto."
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="Contador" DataFormatString="{0:F0}" HeaderText="Vigencia (D�as)"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderText="Monto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblMonedaMonto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                            <asp:Label ID="lblMonto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F2}")%>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Saldo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblMonedaSaldo" ForeColor="Red" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                            <asp:Label ID="lblSaldo" runat="server" ForeColor="Red" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"ImporteTotal","{0:F2}")%>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="NomPropietario" HeaderText="Empresa" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="NomTienda" HeaderText="Tienda" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" />
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="Texto" style="font-weight: bold">
                                        Saldo a Favor del Cliente:
                                    </td>
                                    <td>
                                        <asp:Label ID="lblMonedaSaldoAFavorCliente_NotaCredito" CssClass="LabelRojo" Font-Bold="true"
                                            runat="server" Text="-"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtSaldoAFavorCliente_NotaCredito" Font-Bold="true" ReadOnly="true"
                                            Width="90px" CssClass="TextBox_ReadOnly" runat="server" Text="0"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="TituloCelda">
                            APLICACI�N
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table>
                                <tr>
                                    <td class="Texto" style="font-weight: bold">
                                        Tipo de Aplicaci�n:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboTipoAplicacion" runat="server" AutoPostBack="True" onClick=" return(    valOnClick_cboTipoAplicacion()         ); ">
                                            <asp:ListItem Value="0" Selected="True">[ Seleccione un Tipo de Aplicaci�n ]</asp:ListItem>
                                            <asp:ListItem Value="1">Deudas Pendientes</asp:ListItem>
                                            <asp:ListItem Value="2">Contra efectivo de Caja</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_Deuda" runat="server">
                                <table style="width: 100%">
                                    <tr>
                                        <td class="SubTituloCelda">
                                            DEUDAS
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_Deudas" runat="server" AutoGenerateColumns="false" Width="100%">
                                                <Columns>
                                                    <asp:BoundField DataField="NomTipoDocumento" HeaderText="Tipo" />
                                                    <asp:TemplateField HeaderText="Nro. Documento">
                                                        <ItemTemplate>
                                                                        <asp:HiddenField ID="hddIdMovCuenta" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMovCuenta")%>' />

                                                                            <asp:HiddenField ID="hddIdDocumentoDeuda" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
   
                                                                        <asp:Label ID="lblNroDocumentoDeuda" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"getNroDocumento")%>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="FechaEmision" NullDisplayText="---" DataFormatString="{0:dd/MM/yyyy}"
                                                        HeaderText="Fecha Emisi�n" />
                                                    <asp:BoundField DataField="getFechaVctoText" NullDisplayText="---" HeaderText="Fecha Vcto."
                                                        DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="NroDiasMora" HeaderText="D�as Vencidos" />
                                                    <asp:TemplateField HeaderText="Monto">
                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblMonedaMonto" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                                        <asp:Label ID="lblMonto" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MontoTotal","{0:F2}")%>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Saldo">
                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblMonedaSaldo" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                                        <asp:Label ID="lblSaldo" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"Saldo","{0:F2}")%>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Abono">
                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblMonedaAbono" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                                        <asp:TextBox ID="txtAbono" onFocus="return(  aceptarFoco(this)  );" onKeyup="return( calcularAbonoTotal()  );"
                                                                            ToolTip="Ingrese el valor a abonar." onblur=" return(   valBlur(event)   ); " onKeypress="return( validarNumeroPuntoPositivo('event')   );"
                                                                            Width="70px" Text="0" Font-Bold="true" runat="server"></asp:TextBox>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:ImageButton OnClick="btnVerHistorial_Click" ID="btnVerHistorialPago" Width="25px"
                                                                ToolTip="Ver Historial de Pagos." ImageUrl="~/Caja/iconos/afm_.gif" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td class="Texto" style="font-weight: bold">
                                                        Total Abono:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblMonedaTotalAbono" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                            Text="-"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtTotalAbono" onFocus="return(  valOnFocus_txtTotalAbono()  );"
                                                            Font-Bold="true" Width="90px" CssClass="TextBox_ReadOnly" runat="server" Text="0"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_EfectivoCaja" runat="server">
                                <table width="100%">
                                    <tr>
                                        <td class="SubTituloCelda">
                                            CONTRA EFECTIVO DE CAJA
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td class="Texto" style="font-weight: bold">
                                                        Medio de Pago:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="cboMedioPago" runat="server" Enabled="false" Font-Bold="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="Texto" style="font-weight: bold">
                                                        Monto:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblMoneda_EfectivoCaja" runat="server" CssClass="LabelRojo" Font-Bold="true"
                                                            Text="-"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtEfectivoCaja" onKeyup="return( calcularAbonoTotal()  );" onFocus="return(  aceptarFoco(this)  );"
                                                            onblur=" return(   valBlur(event)   ); " onKeypress="return( validarNumeroPuntoPositivo('event')   );"
                                                            Width="70px" Text="0" Font-Bold="true" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                SALDOS RESTANTES
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto" style="font-weight: bold">
                            Saldo RESTANTE a favor del Cliente:
                        </td>
                        <td>
                            <asp:Label ID="lblMonedaSaldoRestante" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                Text="-"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSaldoRestante" onFocus="return(  valOnFocus_txtTotalAbono()  );"
                                Font-Bold="true" Width="90px" CssClass="TextBox_ReadOnly" runat="server" Text="0"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto" style="font-weight: bold">
                            Acci�n a Tomar:
                        </td>
                        <td class="LabelLeft">
                            <asp:RadioButtonList ID="rdbSaldoRestante" Font-Bold="true" runat="server" RepeatDirection="Vertical"
                                TextAlign="Right">
                                <asp:ListItem Value="0" Selected="True">Mantener Saldo Restante a Favor del Cliente</asp:ListItem>
                                <asp:ListItem Value="1">No Mantener Saldo Restante ( Considerar la Nota de Cr�dito Aplicada en su Totalidad )</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="LabelRojo" style="font-weight: bold">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="LabelRojo" style="font-weight: bold">
                *** La Opci�n [ Actualizar Saldo a Favor de Nota de Cr�dito ] Permite actualizar
                el Saldo Restante de la Nota de Cr�dito a favor del Cliente a un valor menor o igual
                al monto original del documento.
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdPersona" runat="server" />
                <asp:HiddenField ID="hddIdDocumentoNotaCredito" runat="server" />
                <asp:HiddenField ID="hddIdMoneda" runat="server" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
            </td>
        </tr>
    </table>
    <div id="capaDocumentosReferencia" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 258px; left: 17px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaDocumentosReferencia" runat="server" ToolTip="Cerrar"
                        ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaDocumentosReferencia')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tienda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTienda_DocRef" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Filtro:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboFiltro_NotaCredito" runat="server">
                                    <asp:ListItem Value="0" Selected="True">[ Notas de Cr�ditos Vigentes - Sin Aplicar ]</asp:ListItem>
                                    <asp:ListItem Value="1">[ Notas de Cr�ditos Aplicadas ]</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarDocRef_NotaCredito" runat="server" Text="Buscar" ToolTip="Buscar Nota de Cr�dito en la Tienda seleccionada." CssClass="btnBuscar" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_DocRef_Find" runat="server" ScrollBars="Horizontal" Width="100%">
                        <asp:GridView ID="GV_DocumentosReferencia_Find" runat="server" AutoGenerateColumns="false"
                            Width="100%" PageSize="40" AllowPaging="true">
                            <Columns>
                                <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="50px" />
                                <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Tipo" />
                                <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"Numero")%>'></asp:Label>

                                                    <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />

                                                    <asp:HiddenField ID="hddIdMonedaDocRef_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />

                                                    <asp:HiddenField ID="hddIdEstadoCancelacion_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEstadoCancelacion")%>' />

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisi�n"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="FechaVenc" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Vcto."
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="Contador" DataFormatString="{0:F0}" HeaderText="Vigencia (D�as)"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Monto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblMonedaMonto_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                    <asp:Label ID="lblMonto_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F2}")%>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Saldo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblMonedaSaldo_Find" ForeColor="Red" runat="server" Font-Bold="true"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                    <asp:Label ID="lblSaldo_Find" runat="server" ForeColor="Red" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"ImporteTotal","{0:F2}")%>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="NomPropietario" HeaderText="Empresa" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomTienda" HeaderText="Tienda" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaAbonos" style="border: 3px solid blue; padding: 8px; width: 700px; height: auto;
        position: absolute; top: 298px; left: 98px; background-color: white; z-index: 2;
        display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                        onmouseout="this.src='../Imagenes/Cerrar_B.JPG';" onmouseover="this.src='../Imagenes/Cerrar_A.JPG';"
                        OnClientClick="return(offCapa('capaAbonos'));" />
                </td>
            </tr>
            <tr>
                <td style="width: 100%" class="TituloCelda">
                    Abonos
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:GridView ID="GV_Abonos" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                        PageSize="20" Width="100%">
                        <Columns>
                            <asp:BoundField DataField="NomTipoDocumento" HeaderText="Tipo" />
                            <asp:BoundField DataField="getNroDocumento" HeaderText="Nro. Documento" />
                            <asp:BoundField DataField="FechaEmision" HeaderText="Fecha Emisi�n" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:TemplateField HeaderText="Monto">
                                <ItemTemplate>
                                                <asp:Label ID="lblMoneda" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                <asp:Label ID="lblMontoAbonado" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F2}")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaUpdateSaldo" style="border: 3px solid blue; padding: 8px; width: 250px;
        height: auto; position: absolute; top: 205px; left: 185px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton2" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaUpdateSaldo')   );" />
                </td>
            </tr>
            <tr>
                <td style="width: 100%" class="TituloCelda">
                    Nota de Cr�dito
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table width="100%">
                        <tr>
                            <td class="Texto" align="right">
                                Nota de Cr�dito:
                            </td>
                            <td>
                                <asp:Label ID="lblNroDocumento_UpdateSaldo" CssClass="Label" Font-Bold="true" runat="server"
                                    Text="Label"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" align="right">
                                Saldo Inicial:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblMoneda_UpdateSaldo" CssClass="Label" Font-Bold="true" runat="server"
                                                Text="-"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblSaldoOriginal_UpdateSaldo" CssClass="Label" Font-Bold="true" runat="server"
                                                Text="0"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" align="right">
                                Saldo Restante:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblMonedaSaldoRestante_UpdateSaldo" CssClass="LabelRojo" Font-Bold="true"
                                                runat="server" Text="-"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblSaldoRestante_UpdateSaldo" CssClass="LabelRojo" Font-Bold="true"
                                                runat="server" Text="0"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" align="right">
                                Nuevo Saldo:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblMonedaNuevoSaldo_UpdateSaldo" CssClass="LabelRojo" Font-Bold="true"
                                                runat="server" Text="-"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtNuevoSaldo_UpdateSaldo" onblur="return(   valBlur(event)   );" onKeypress="return(   validarNumeroPuntoPositivo('event')  );"
                                                onFocus="return( aceptarFoco(this)  );" Font-Bold="true" Width="90px" runat="server"
                                                Text="0"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center">
                                <asp:Button ID="btnGuardar_UpdateSaldo" OnClientClick="return(  valOnClick_btnGuardar_UpdateSaldo()   );"
                                    runat="server" Text="Guardar" ToolTip="Actualizar Saldo a Favor del Cliente." />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Selected="True" Value="J">Juridica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razon Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:Panel ID="Panel1" runat="server" DefaultButton="btBuscarPersonaGrillas">
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this));" runat="server" 
                                                                            onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" Width="450px"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel2" runat="server" DefaultButton="btBuscarPersonaGrillas">
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero());" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel3" runat="server" DefaultButton="btBuscarPersonaGrillas">
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero());"
                                                                            MaxLength="11"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td>
                                                                        <%--<asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />--%>
                                                                            <asp:Button ID="btBuscarPersonaGrillas" runat="server" Text="Buscar" CssClass="btnBuscar"
                                                                        OnClientClick="this.disabled;this.value='Procesando...'" UseSubmitBehavior="false"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaDeshacerAplicacion" style="border: 3px solid blue; padding: 8px; width: auto;
        height: auto; position: absolute; top: 112px; left: 274px; background-color: white;
        z-index: 2; display: none;">
        <table>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton9" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaConfiguracionFrm'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chb_AppDeudas" runat="server" Text="Deshacer aplicaciones a Deudas Pendientes."
                                    CssClass="Texto" Font-Bold="true" TextAlign="Right" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chb_AppEfectivoCaja" runat="server" Text="Deshacer aplicaciones contra Efectivo de Caja."
                                    CssClass="Texto" Font-Bold="true" TextAlign="Right" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnGuardar_DeshaceMov" runat="server" Text="Guardar" Width="90px"
                                                OnClientClick="return(  confirm('Desea continuar con la Operaci�n ?')   );" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCerrar_DeshaceMov" runat="server" Text="Cerrar" Width="90px" OnClientClick="return(  offCapa('capaDeshacerAplicacion')   );" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">
        //************************** BUSQUEDA PERSONA
        //***************************************************** BUSQUEDA PERSONA
        function valOnClick_btnBuscarPersona() {

            var grilla = document.getElementById('<%=GV_NotaCreditoApp.ClientID %>');
            if (grilla != null) {
                alert('SE HA INGRESADO UN DOCUMENTO < NOTA DE CR�DITO >. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            mostrarCapaPersona();
            return false;
        }
        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrillas.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(obj) {
            var key = event.keyCode;
            if (key == 13) {
                onBlurTextTransform(obj,configurarDatos);
                var boton = document.getElementById('<%=btBuscarPersonaGrillas.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }


        function valOnClick_btnAddNotaCredito() {
            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }
            onCapa('capaDocumentosReferencia');
            return false;
        }

        function calcularAbonoTotal() {
            var totalAbono = 0;
            var cboTipoAplicacion = document.getElementById('<%=cboTipoAplicacion.ClientID%>');
            switch (cboTipoAplicacion.value) {
                case '0':
                    //*************** NO se ha seleccionado ning�n tipo de aplicaci�n.
                    break;
                case '1':
                    //***************** DEUDAS
                    var grilla = document.getElementById('<%=GV_Deudas.ClientID%>');
                    if (grilla != null) {
                        for (var i = 1; i < grilla.rows.length; i++) {
                            var rowElem = grilla.rows[i];
                            var abono = parseFloat(rowElem.cells[7].children[1].value);
                            if (isNaN(abono)) {
                                abono = 0;
                            }
                            totalAbono = totalAbono + abono;
                        }
                    }
                    document.getElementById('<%=txtTotalAbono.ClientID%>').value = Format(totalAbono, 2);
                    break;
                case '2':
                    //************************    EFECTIVO DE CAJA
                    var txtEfectivoCaja = document.getElementById('<%=txtEfectivoCaja.ClientID%>');
                    if (txtEfectivoCaja != null) {
                        if (!(isNaN(parseFloat(txtEfectivoCaja.value)) || txtEfectivoCaja.value.length <= 0)) {
                            totalAbono = parseFloat(txtEfectivoCaja.value);
                        }
                    }
                    break;
            }



            var saldoRestante = 0;
            var totalSaldo = 0;
            totalSaldo = parseFloat(document.getElementById('<%=txtSaldoAFavorCliente_NotaCredito.ClientID%>').value);
            if (totalSaldo >= totalAbono) {
                saldoRestante = totalSaldo - totalAbono;
            }
            //********** Imprimimos valores
            document.getElementById('<%=txtSaldoRestante.ClientID%>').value = Format(saldoRestante, 2);
            return false;
        }

        function valOnFocus_txtTotalAbono() {
            document.getElementById('<%=btnAddNotaCredito.ClientID%>').focus();
            return false;
        }

        function valOnClick_cboTipoAplicacion() {
            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }
            var hddIdDocumentoNotaCredito = document.getElementById('<%=hddIdDocumentoNotaCredito.ClientID%>');
            if (isNaN(parseInt(hddIdDocumentoNotaCredito.value)) || hddIdDocumentoNotaCredito.value.length <= 0) {
                alert('Debe seleccionar un [ Documento Nota de Cr�dito ] para su aplicaci�n.');
                return false;
            }
            return true;
        }


        function valOnClick_btnGuardar() {
            var cad = '';
            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');

            var montoAplicacion = 0;

            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }
            var hddIdDocumentoNotaCredito = document.getElementById('<%=hddIdDocumentoNotaCredito.ClientID%>');
            if (isNaN(parseInt(hddIdDocumentoNotaCredito.value)) || hddIdDocumentoNotaCredito.value.length <= 0) {
                alert('Debe seleccionar un [ Documento Nota de Cr�dito ] para su aplicaci�n.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_Deudas.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var abono = parseFloat(rowElem.cells[7].children[1].value);
                    var saldo = parseFloat(rowElem.cells[6].children[1].innerHTML);
                    if (isNaN(abono)) {
                        abono = 0;
                    }
                    if (isNaN(saldo)) {
                        saldo = 0;
                    }
                    if (saldo < abono) {
                        alert('El monto abonado supera el saldo pendiente.');
                        rowElem.cells[7].children[1].select();
                        rowElem.cells[7].children[1].focus();
                        return false;
                    }
                }
            }
            var txtTotalSaldoAFavor = document.getElementById('<%=txtSaldoAFavorCliente_NotaCredito.ClientID%>');
            var txtTotalAbono = document.getElementById('<%=txtTotalAbono.ClientID%>');
            var txtEfectivoCaja = document.getElementById('<%=txtEfectivoCaja.ClientID%>');
            var txtSaldoRestante = document.getElementById('<%=txtSaldoRestante.ClientID%>');

            if (txtTotalAbono != null) {
                if (parseFloat(UnFormat(txtTotalAbono.value, ',', '')) > parseFloat(UnFormat(txtTotalSaldoAFavor.value, ',', ''))) {
                    alert('El Total Abonado supera el Saldo a favor del cliente.');
                    return false;
                }
                montoAplicacion = parseFloat(UnFormat(txtTotalAbono.value, ',', ''));
            }

            if (txtEfectivoCaja != null) {

                //************ VALIDAMOS EMPRESA TIENDA CAJA
                var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
                if (isNaN(parseInt(cboEmpresa.value)) || cboEmpresa.value.length <= 0) {
                    alert('DEBE SELECCIONAR UNA EMPRESA. NO SE PERMITE LA OPERACI�N.');
                    return false;
                }
                var cboTienda = document.getElementById('<%=cboTienda.ClientID%>');
                if (isNaN(parseInt(cboTienda.value)) || cboTienda.value.length <= 0) {
                    alert('DEBE SELECCIONAR UNA TIENDA. NO SE PERMITE LA OPERACI�N.');
                    return false;
                }
                var cboCaja = document.getElementById('<%=cboCaja.ClientID%>');
                if (isNaN(parseInt(cboCaja.value)) || cboCaja.value.length <= 0) {
                    alert('DEBE SELECCIONAR UNA CAJA. NO SE PERMITE LA OPERACI�N.');
                    return false;
                }

                if (parseFloat(txtEfectivoCaja.value) > parseFloat(txtTotalSaldoAFavor.value)) {
                    alert('EL MONTO DE LA APLICACI�N < CONTRA EFECTIVO DE CAJA > ES MAYOR AL SALDO A FAVOR DEL CLIENTE. NO SE PERMITE LA OPERACI�N.');
                    return false;
                }
                montoAplicacion = parseFloat(txtEfectivoCaja.value);
            }

            if (parseFloat(txtSaldoRestante.value) > parseFloat(txtTotalSaldoAFavor.value)) {
                alert('El Saldo Restante supera el Saldo Total a favor del cliente.');
                return false;
            }

            var lblMoneda = document.getElementById('<%=lblMonedaSaldoAFavorCliente_NotaCredito.ClientID%>').innerHTML;
            var rdbOpcion = document.getElementById('<%=rdbSaldoRestante.ClientID%>');
            var accion = '';
            if (rdbOpcion.cells[0].children[0].status) {
                accion = rdbOpcion.cells[0].children[1].innerHTML;
            } else if (rdbOpcion.cells[1].children[0].status) {
                accion = rdbOpcion.cells[1].children[1].innerHTML;
            } else if (rdbOpcion.cells[2].children[0].status) {
                accion = rdbOpcion.cells[2].children[1].innerHTML;
            } else {
                alert('No se tiene una [ Acci�n seleccionada a Realizar para el manejo del Saldo Restante. ]');
                return false;
            }

            cad = cad + 'Saldo a Favor del Cliente : ' + lblMoneda + ' ' + txtTotalSaldoAFavor.value + '\n';
            cad = cad + '              Total Aplicaci�n : ' + lblMoneda + ' ' + montoAplicacion + '\n';
            cad = cad + '                Saldo Restante : ' + lblMoneda + ' ' + txtSaldoRestante.value + '\n';
            cad = cad + 'Acci�n a Realizar [ Saldo Restante ] : ' + accion + '\n';
            cad = cad + 'Desea continuar con la Operaci�n ?'
            return confirm(cad);
        }

        function registrarAplicacion_NotaCredito(opcion, IdDocumentoNC) {
            calcularAbonoTotal();
            alert('La Operaci�n finaliz� con �xito.');
            /*
            if (opcion == '2') {
            window.open('FrmDocumento_Load.aspx?IdTipoDocumentoLoad=18&IdDocumentoRef=' + IdDocumentoNC , null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);                    
            }
            */
            return false;
        }
        function valOnClick_btnGuardar_UpdateSaldo() {
            var SaldoOriginal = parseFloat(document.getElementById('<%=lblSaldoOriginal_UpdateSaldo.ClientID%>').innerHTML);
            var SaldoNuevo = parseFloat(document.getElementById('<%=txtNuevoSaldo_UpdateSaldo.ClientID%>').value);
            if (isNaN(SaldoNuevo)) {
                alert('Ingrese un valor v�lido.');
                document.getElementById('<%=txtNuevoSaldo_UpdateSaldo.ClientID%>').select();
                document.getElementById('<%=txtNuevoSaldo_UpdateSaldo.ClientID%>').focus();
                return false;
            }
            if (SaldoOriginal < SaldoNuevo) {
                alert('El valor del [ Nuevo Saldo ] excede al Valor del [ Saldo Original ] del documento.');
                return false;
            }
            return confirm('Desea continuar con la Operaci�n ?');
        }
        function valOnClick_btnDeshacerAplicacion() {
            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }
            var hddIdDocumentoNotaCredito = document.getElementById('<%=hddIdDocumentoNotaCredito.ClientID%>');
            if (isNaN(parseInt(hddIdDocumentoNotaCredito.value)) || hddIdDocumentoNotaCredito.value.length <= 0) {
                alert('Debe seleccionar un [ Documento Nota de Cr�dito ] al cual hacer referencia.');
                return false;
            }
            onCapa('capaDeshacerAplicacion');
            return false;
        }
        function valOnClick_btnActualizarSaldoFavor() {
            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }
            var hddIdDocumentoNotaCredito = document.getElementById('<%=hddIdDocumentoNotaCredito.ClientID%>');
            if (isNaN(parseInt(hddIdDocumentoNotaCredito.value)) || hddIdDocumentoNotaCredito.value.length <= 0) {
                alert('Debe seleccionar un [ Documento Nota de Cr�dito ] al cual hacer referencia.');
                return false;
            }
            var cad = 'Se recomienda antes de Actualizar el saldo a favor del Cliente [ Deshacer la Aplicaci�n de la Nota de Cr�dito ]. Desea continuar con la Operaci�n ? ';
            return confirm(cad);
        }
    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>
