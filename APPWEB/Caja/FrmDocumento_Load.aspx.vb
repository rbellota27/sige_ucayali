﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmDocumento_Load
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then

            'Response.ContentType = "application/ms-excel"
            'Response.AddHeader("Content-Disposition", "attachment;filename=Informe.xls")

            Dim Opcion As Integer = CInt(Request.QueryString("IdTipoDocumentoLoad"))
            Dim IdDocumentoRef As Integer = CInt(Request.QueryString("IdDocumentoRef"))

            Select Case Opcion
                Case 1 '*************** RECIBO DE EGRESO - NOTA DE CREDITO
                    Dim IdMonedaNC As Integer = CInt(Request.QueryString("IdMonedaNC"))
                    Dim montoNC As Decimal = CDec(Request.QueryString("montoNC"))
                    Response.Redirect("~/Finanzas/FrmDocCancelacionCaja.aspx?IdDocumentoNC=" + CStr(IdDocumentoRef) + "&IdMonedaNC=" + CStr(IdMonedaNC) + "&montoNC=" + CStr(montoNC), True)
                Case 6  '************* GUIA DE REMISIÓN
                    Response.Redirect("~/Almacen1/FrmGuiaRemision.aspx?IdDocumentoRef=" + CStr(IdDocumentoRef), True)
                Case 18  '************** RECIBO DE EGRESO
                    Response.Redirect("~/Caja/frmReciboEgreso.aspx?IdDocumentoRef=" + CStr(IdDocumentoRef), True)
                Case 25  '************* GUIA DE RECEPCION
                    Response.Redirect("~/Almacen1/FrmGuiaRecepcion.aspx?IdDocumentoRef=" + CStr(IdDocumentoRef), True)
                Case 30  '************** COMPROBANTE DE PERCEPCION
                    Response.Redirect("~/Ventas/FrmComprobantePercepcion.aspx?IdDocumento=" + CStr(IdDocumentoRef), True)
            End Select
        End If
    End Sub


End Class