﻿<%@ Page Title="Caja - Recibo de Egreso" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmReciboEgreso.aspx.vb" Inherits="APPWEB.FrmReciboEgreso" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" OnClientClick="return(   valOnClickNuevo()  );" Width="80px"
                                runat="server" Text="Nuevo" ToolTip="Nuevo" />
                        </td>
                        <td>
                            <asp:Button ID="btnEditar" OnClientClick="return(  valOnClickEditar()  );" Width="80px"
                                runat="server" Text="Editar" ToolTip="Editar" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" OnClientClick="return(  valOnClickBuscar()  );"
                                Text="Buscar" ToolTip="Buscar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnAnular" OnClientClick="return(  valOnClickAnular()   );" Width="80px"
                                runat="server" Text="Anular" ToolTip="Anular" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" OnClientClick="return( valSaveDocumento()  );" Width="80px"
                                runat="server" Text="Guardar" ToolTip="Guardar" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimir" OnClientClick="return(  valOnClickImprimir()  );" Width="80px"
                                runat="server" Text="Imprimir" ToolTip="Imprimir Documento" />
                        </td>
                        <td>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td style="width: 100%" align="right">
                            <table>
                                <tr>
                                    <td class="Texto">
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                RECIBO DE EGRESO 
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cab" runat="server">
                    <table>
                        <tr>
                            <td class="Label">
                                Empresa:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa" Width="100%" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="Label">
                                Tienda:
                            </td>
                            <td style="width: 100px">
                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="Label">
                                Serie:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboSerie" Width="100%" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:TextBox ID="txtCodigoDocumento" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return(  valOnKeyPressCodigoDoc(this)  );"
                                    Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:ImageButton ID="btnBuscarDocumentoxCodigo" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                    ToolTip="Buscar Documento por [ Serie - Número ]." OnClientClick="return( valOnClickBuscarDocumentoxCodigo()   );" />
                            </td>
                            <td>
                                <asp:LinkButton ID="btnBusquedaAvanzado" runat="server" CssClass="Texto" Enabled="false"
                                    OnClientClick="return(  valOnClick_btnBusquedaAvanzado()  );" ToolTip="Búsqueda Avanzada">Avanzado</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td class="Label">
                                Fecha Emisión:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaEmision">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaEmision">
                                </cc1:CalendarExtender>
                            </td>
                            <td class="Label">
                                Moneda:
                            </td>
                            <td>
                                <asp:DropDownList CssClass="LabelRojo" ID="cboMoneda" Width="100%" runat="server"
                                    Enabled="true" onClick="return(  valOnClickMonedaEmision()  );">
                                </asp:DropDownList>
                            </td>
                            <td class="Label">
                                Estado:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEstado" runat="server" Enabled="false">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                &nbsp;
                            </td>
                            <td>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="Label">
                                Caja:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboCaja" Width="100%" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="Label">
                                Operación:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboTipoOperacion" Width="100%" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                DESTINATARIO
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cliente" runat="server">
                    <table>
                        <tr>
                            <td class="Texto" align="right">
                                Descripción:
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtDescripcionPersona" onFocus="return(   valFocusPersona()  );"
                                    onKeypress="return(false);" ReadOnly="true" Enabled="true" CssClass="TextBox_ReadOnlyLeft"
                                    Width="400px" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarPersona" OnClientClick="return( valBuscarPersona()  );"
                                    runat="server" Text="Buscar" ToolTip="Buscar Cliente" />
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" align="right">
                                D.N.I.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDNI" ReadOnly="true" onFocus="return(   valFocusPersona()  );"
                                    onKeypress="return(false);" CssClass="TextBox_ReadOnlyLeft" Enabled="true" runat="server"></asp:TextBox>
                            </td>
                            <td align="right" class="Texto">
                                R.U.C.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRUC" ReadOnly="true" onFocus="return(   valFocusPersona()  );"
                                    onKeypress="return(false);" CssClass="TextBox_ReadOnlyLeft" Enabled="true" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr class="TituloCelda">
            <td>
                DETALLE
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Detalle" runat="server">
                    <table id="TablaDetalle" width="100%">
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnAddConcepto_Otros" Width="100px" runat="server" Text="Nuevo detalle"
                                                ToolTip="Agregar un nuevo detalle al documento." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_Otros" runat="server" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="true" SelectText="Quitar" ItemStyle-Width="65px"
                                                        ItemStyle-HorizontalAlign="Center" />
                                                    <asp:TemplateField HeaderText="Concepto" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:DropDownList ID="cboConcepto" runat="server" DataTextField="Nombre" DataValueField="Id"
                                                                            DataSource='<%#DataBinder.Eval(Container.DataItem,"ListaConcepto")%>'>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtDescripcionConcepto" TabIndex="250" onFocus="return(  aceptarFoco(this) );"
                                                                            Width="400px" ToolTip="Ingrese una Descripción Adicional" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Concepto")%>'></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Monto" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMoneda" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAbono" TabIndex="260" onKeypress="return(   validarNumeroPuntoPositivo('event')   );"
                                                                            onblur="return( valBlur(event)  );" onKeyup="return( calcularTotalAPagar()  );" onFocus="return(  aceptarFoco(this) );"
                                                                            Width="90px" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Monto","{0:F2}")%>'></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cancelacion" runat="server">
                    <table id="Cancelacion" width="100%">
                        <tr>
                            <td class="TituloCelda">
                                DATOS DE MOVIMIENTO - CAJA
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="DatosCancelacionCab">
                                    <tr>
                                        <td class="Texto">
                                            Cantidad Total x Entregar:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblMonedaTotalAPagar" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                            Text="S/."></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtTotalAPagar" onKeypress="return(  false  );" onFocus="return(  valFocusCancelacion() );"
                                                            runat="server" CssClass="TextBox_ReadOnly" Width="100px" Text="0"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="SubTituloCelda">
                                MOV. CAJA
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_CP_Contado" runat="server" Width="100%">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="lblMedioPago_DC" CssClass="Texto" Font-Bold="true" runat="server"
                                                                Text="Medio de Pago:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboMedioPago" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                                AutoPostBack="True" Enabled="false">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <asp:Label ID="lblMonto_DC" CssClass="Texto" Font-Bold="true" runat="server" Text="Monto:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboMoneda_DatoCancelacion" runat="server" Width="100px">
                                                            </asp:DropDownList>
                                                            <asp:TextBox ID="txtMonto_DatoCancelacion" onFocus=" return(  aceptarFoco(this)   ); "
                                                                runat="server" onKeypress="  return(  valOnKeyPress_txtMonto_DatoCancelacion()  ); "
                                                                Width="80px" Text="0"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnAddDatoCancelacion" runat="server" Text="Agregar" Width="70px"
                                                                OnClientClick=" return(   valOnClick_btnAddDatoCancelacion()  ); " />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="GV_Cancelacion" runat="server" AutoGenerateColumns="false" Width="100%">
                                                    <Columns>
                                                        <asp:CommandField ButtonType="Link" SelectText="Quitar" ItemStyle-HorizontalAlign="Center"
                                                            ShowSelectButton="true" ItemStyle-Width="75px" />
                                                        <asp:TemplateField HeaderText="Monto" ItemStyle-Width="120px" HeaderStyle-Height="25px"
                                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblMonedaMonto" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMonedaDestino")%>'></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblMonto" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MontoEquivalenteDestino","{0:F2}")%>'></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddIdMedioPago" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMedioPago")%>' />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddIdMonedaOrigen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="descripcionPagoCaja" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderText="Descripción" />
                                                    </Columns>
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td class="Texto" align="right">
                                                            Total Entregado:
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMonedaTotalRecibido" Text="-" runat="server" CssClass="LabelRojo"
                                                                            Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtTotalRecibido" onKeypress="return(  false  );" onFocus="return(  valFocusCancelacion() );"
                                                                            Width="90px" Text="0" CssClass="TextBox_ReadOnly" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Texto" align="right">
                                                            Faltante:
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMonedaFaltante" Text="-" runat="server" CssClass="LabelRojo" Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtFaltante" onKeypress="return(  false  );" onFocus="return(  valFocusCancelacion() );"
                                                                            Width="90px" Text="0" CssClass="TextBox_ReadOnly" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                OBSERVACIONES
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Obs" runat="server">
                    <asp:TextBox ID="txtObservaciones" Width="100%" Height="100px" TextMode="MultiLine"
                        runat="server"></asp:TextBox>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdPersona" runat="server" />
                <asp:HiddenField ID="hddFrmModo" runat="server" Value="0" />
                <asp:HiddenField ID="hddIdDocumento" runat="server" />
                <asp:HiddenField ID="hddCodigoDocumento" runat="server" />
                <asp:HiddenField ID="hddIdTipoDocumento" runat="server" Value="18" />
                <asp:HiddenField ID="hddIdDocumentoRef" runat="server" />
                <asp:HiddenField ID="hddIdMedioPagoPrincipal" runat="server" />
                <asp:HiddenField ID="hddIdMedioPagoInterfaz" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Selected="True" Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Value="J">Juridica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razon Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda());" runat="server"
                                                                            Width="450px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero());" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero());"
                                                                            MaxLength="11"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='/Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="Página Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="Página Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaBusquedaAvanzado" style="border: 3px solid blue; padding: 8px; width: 850px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 142px; left: 22px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton2" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaBusquedaAvanzado')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BusquedaAvanzado" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaInicio_BA" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaInicio_BA" runat="server"
                                                    ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                                    CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_BA">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaInicio_BA" runat="server" Enabled="True"
                                                    Format="dd/MM/yyyy" TargetControlID="txtFechaInicio_BA">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaFin_BA" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaFin_BA" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_BA">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaFin_BA" runat="server" Enabled="True"
                                                    Format="dd/MM/yyyy" TargetControlID="txtFechaFin_BA">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBusquedaAvanzado" Width="70px" ToolTip="Buscar Documentos"
                                                    runat="server" Text="Buscar" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_BusquedaAvanzado" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50px" HeaderStyle-Height="25px">
                                <HeaderStyle Height="25px" Width="50px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:CommandField>
                            <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Tipo"></asp:BoundField>
                            <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblNroDocumento_BusquedaAvanzado" ForeColor="Red" Font-Bold="true"
                                                    runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdDocumento_BusquedaAvanzado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisión"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="DescripcionPersona" HeaderText="Cliente" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td class="LabelRojo" style="font-weight: bold">
                    *** El proceso de búsqueda realiza un filtrado de acuerdo al destinatario seleccionado.
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">

        //********************* BUSQUEDA DE PERSONA
        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }

        //********* FIN BUSQUEDA PERSONA


        function calcularDatosCancelacion() {
            var totalAPagar = parseFloat(document.getElementById('<%=txtTotalAPagar.ClientID%>').value);
            var totalEntregado = 0;
            var grilla = document.getElementById('<%=GV_Cancelacion.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    totalEntregado = totalEntregado + parseFloat(rowElem.cells[1].children[0].cells[1].children[0].innerHTML);
                }
            }
            var faltante = 0;
            var vuelto = 0;
            if ((totalAPagar - totalEntregado) > 0) {
                faltante = Math.abs((totalAPagar - totalEntregado));
            } else {
                vuelto = Math.abs((totalAPagar - totalEntregado));
            }

            //********** Imprimimos valores
            document.getElementById('<%=txtTotalRecibido.ClientID%>').value = Format(totalEntregado, 2);
            document.getElementById('<%=txtFaltante.ClientID%>').value = Format(faltante, 2);
            return false;
        }

        function calcularTotalAPagar() {

            var totalAPagar = 0;
            var grilla = document.getElementById('<%=GV_Otros.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var abono = parseFloat(rowElem.cells[2].children[0].cells[1].children[0].value);
                    if (isNaN(abono)) {
                        abono = 0;
                    }
                    totalAPagar = totalAPagar + abono;
                }
            }


            //********** Imprimimos valores
            document.getElementById('<%=txtTotalAPagar.ClientID%>').value = Format(totalAPagar, 2);

            //********* calculamos los datos de cancelación
            calcularDatosCancelacion();
            return false;
        }

        function valFocusCancelacion() {
            document.getElementById('<%=btnAddDatoCancelacion.ClientID%>').focus();
            return false;
        }

        function valSaveDocumento() {

            //******** Validamos la cabecera

            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
            if (isNaN(cboEmpresa.value) || cboEmpresa.value.length <= 0) {
                alert('Debe seleccionar una Empresa');
                return false;
            }
            var cboTienda = document.getElementById('<%=cboTienda.ClientID%>');
            if (isNaN(cboTienda.value) || cboTienda.value.length <= 0) {
                alert('Debe seleccionar una Tienda');
                return false;
            }
            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(cboSerie.value) || cboSerie.value.length <= 0) {
                alert('Debe seleccionar una Serie');
                return false;
            }
            var cboCaja = document.getElementById('<%=cboCaja.ClientID%>');
            if (isNaN(cboCaja.value) || cboCaja.value.length <= 0) {
                alert('No tiene asignada una Caja.');
                return false;
            }
            var cboTipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID%>');
            if (isNaN(parseInt(cboTipoOperacion.value)) || cboTipoOperacion.value.length <= 0 || parseInt(cboTipoOperacion.value) <= 0) {
                alert('Debe seleccionar un Tipo de Operación.');
                return false;
            }

            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(hddIdPersona.value) || hddIdPersona.value.length <= 0) {
                alert('Seleccione una Persona.');
                return false;
            }


            var grillaOtros = document.getElementById('<%=GV_Otros.ClientID%>');
            if (grillaOtros == null) {
                alert('Debe añadir detalles al documento.');
                return false;
            }



            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(hddIdPersona.value) || hddIdPersona.value.length <= 0) {
                alert('Seleccione una Persona.');
                return false;
            }

            var txtTotalAPagar = document.getElementById('<%=txtTotalAPagar.ClientID%>');
            if (isNaN(txtTotalAPagar.value) || txtTotalAPagar.value.length <= 0 || parseFloat(txtTotalAPagar.value) <= 0) {
                alert('El Total x Entregar debe ser mayor a cero.');
                return false;
            }

            var hddIdMedioPagoPrincipal = document.getElementById('<%=hddIdMedioPagoPrincipal.ClientID%>');


            //********* Validamos que no Existe Vuelto si el medio de pago es fuera de EFECTIVO
            var grilla = document.getElementById('<%=GV_Cancelacion.ClientID %>');
            if (grilla == null) {
                alert('NO SE HAN INGRESADO DATOS DE CANCELACIÓN. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }

            var txtFaltante = document.getElementById('<%=txtFaltante.ClientID %>');
            var txtTotalxEntregar = document.getElementById('<%=txtTotalAPagar.ClientID %>');
            var grillaCancelacion = document.getElementById('<%=GV_Cancelacion.ClientID %>');
            var cboMoneda = document.getElementById('<%=cboMoneda.ClientID %>');

            var txtTotal_DC = document.getElementById('<%=txtTotalRecibido.ClientID %>');

            if (parseFloat(txtTotal_DC.value) > parseFloat(txtTotalxEntregar.value)) {
                alert('EL TOTAL INGRESADO EN DATOS DE CANCELACIÓN SUPERA EL MONTO A ENTREGAR. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }

            if (grillaCancelacion == null) {
                return confirm('La Cantidad x Entregar del Recibo de Egreso es de ' + getCampoxValorCombo(cboMoneda, cboMoneda.value) + ' ' + txtTotalxEntregar.value + ' en EFECTIVO. Desea continuar con la operación ?');
            } else {
                if (parseFloat(txtFaltante.value) > 0) {
                    alert('No puede existir un faltante. No se puede continuar con la Operación.');
                    return false;
                }
            }
            return confirm('Desea continuar con la Operación ?');
        }


        function valOnClickNuevo() {

            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID %>');
            if (parseInt(hddFrmModo.value) == 0 || parseInt(hddFrmModo.value) == 4 || parseInt(hddFrmModo.value) == 5 || parseInt(hddFrmModo.value) == 6) {
                return true;
            }
            var grillaOtros = document.getElementById('<%=GV_Otros.ClientID %>');
            if (grillaOtros == null) {
                return true;
            }

            return (confirm('Está seguro que desea generar un NUEVO Documento ?'));
        }


        function valOnClickEditar() {
            var cboEstadoDocumento = document.getElementById('<%=cboEstado.ClientID %>');
            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID %>');
            if (parseInt(cboEstadoDocumento.value) == 2 || parseInt(hddFrmModo.value) == 6) {
                alert('El Documento Recibo de Egreso ha sido anulado. No puede habilitarse la edición del Documento.');
                return false;
            }

            return true;
        }


        function valOnClickAnular() {
            var cboEstadoDocumento = document.getElementById('<%=cboEstado.ClientID %>');
            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID %>');
            if (parseInt(cboEstadoDocumento.value) == 2 || parseInt(hddFrmModo.value) == 6) {
                alert('El Documento ya ha sido ANULADO. No procede la Anulación del Documento.');
                return false;
            }
            return (confirm('Desea continuar con el Proceso de Anulación del Documento Recibo de Egreso ?'));
        }


        function valOnClickMonedaEmision() {
            var grillaOtros = document.getElementById('<%=GV_Otros.ClientID %>');
            var grillaCancelacion = document.getElementById('<%=GV_Cancelacion.ClientID %>');

            if (grillaOtros != null || grillaCancelacion != null) {
                alert('No puede seleccionar otra Moneda de Emisión ya que se ha ingresado Detalles de Documento y/o Datos de Cancelación, si desea seleccionar otra Moneda de Emisión quite el detalle y/o los datos de cancelación del Documento.');
                return false;
            }

            return true;
        }

        function valOnClickImprimir() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID %>');
            if (isNaN(parseInt(hddIdDocumento.value)) || parseInt(hddIdDocumento.value) <= 0 || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ningún Documento para Impresión.');
                return false;
            }
            //window.open('../../Caja/Reportes/Visor1.aspx?iReporte=2&IdDocumento=' + hddIdDocumento.value, 'Recibo_Ingreso', 'resizable=yes,width=1000,height=800,scrollbars=1', null);

            window.open('../../DocumentosMercantiles/VisorDocMercantiles.aspx?iReporte=18&IdDocumento=' + hddIdDocumento.value, 'ReciboEgreso', 'resizable=yes,width=1000,height=800,scrollbars=1', null);


            return false;
        }
        function valOnClickBuscar() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            var panel_Cab = document.getElementById('<%=Panel_Cab.ClientID %>');
            var btnBuscarDocumentoxCodigo = document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>');
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la Búsqueda
                panel_Cab.disabled = false;
                btnBuscarDocumentoxCodigo.disabled = false;
                btnBusquedaAvanzado.disabled = false;
                txtCodigoDocumento.readOnly = false;
                txtCodigoDocumento.disabled = false;
                txtCodigoDocumento.style.backgroundColor = 'Yellow';
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            return true;
        }

        function valOnClick_btnBusquedaAvanzado() {

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (btnBusquedaAvanzado.disabled == true) {  //**** Se habilita la Búsqueda
                alert('Debe habilitar la Búsqueda del Documento haciendo clic en el botón [ Buscar ]');
                return false;
            }

            onCapa('capaBusquedaAvanzado');
            return false;
        }
        function valOnKeyPressCodigoDoc(txtCodigoDocumento) {
            var key = event.keyCode;
            if (key == 13) {
                document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>').focus();
            } else {
                return onKeyPressEsNumero('event');
            }

        }
        function valOnClickBuscarDocumentoxCodigo() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la Búsqueda
                alert('Debe habilitar la Búsqueda del Documento haciendo clic en el botón [ Buscar ]');
                return false;
            }

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }

            if (isNaN(parseInt(txtCodigoDocumento.value)) || txtCodigoDocumento.value.length <= 0) {
                alert('Debe ingresar un valor válido.');
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            document.getElementById('<%=hddCodigoDocumento.ClientID %>').value = txtCodigoDocumento.value;
            return true;
        }
        function valBuscarPersona() {
            mostrarCapaPersona();
            return false;
        }

        function valFocusPersona() {
            document.getElementById('<%=btnBuscarPersona.ClientID%>').focus();
            return false;
        }
        //*************************   DATOS DE CANCELACION

        function valOnKeyPress_txtMonto_DatoCancelacion() {
            if (event.keyCode == 13) { // Enter
                document.getElementById('<%=btnAddDatoCancelacion.ClientID%>').focus();
            } else {
                return validarNumeroPuntoPositivo('event');
            }
            return true;
        }

        function valOnClick_btnAddDatoCancelacion() {
            var cboMoneda = document.getElementById('<%=cboMoneda_DatoCancelacion.ClientID%>');
            if (cboMoneda != null) {
                if (isNaN(parseInt(cboMoneda.value)) || parseInt(cboMoneda.value) <= 0 || cboMoneda.value.length <= 0) {
                    alert('Debe seleccionar una [ Moneda ].');
                    return false;
                }
            }
            var txtMonto = document.getElementById('<%=txtMonto_DatoCancelacion.ClientID%>');
            if (txtMonto != null) {
                if (isNaN(parseInt(txtMonto.value)) || parseInt(txtMonto.value) <= 0 || txtMonto.value.length <= 0) {
                    alert('Debe ingresar un valor válido.');
                    txtMonto.select();
                    txtMonto.focus();
                    return false;
                }
            }
            return true;
        }
        function valOnClick_btnVer_NotaCredito() {
            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }
            return true;
        }
        function valFocusCancelacion() {
            document.getElementById('<%=btnAddDatoCancelacion.ClientID%>').focus();
            return false;
        }
        function valOnKeyPress_txtNro_DC() {
            if (event.keyCode == 13) { //**************** Enter
                document.getElementById('<%=btnAddDatoCancelacion.ClientID%>').focus();
            }
            return true;
        }
        function valFocusCancelacion() {
            document.getElementById('<%=btnAddDatoCancelacion.ClientID%>').focus();
            return false;
        }
        //******************* FIN DATOS DE CANCELACION
    
    
    </script>

</asp:Content>
