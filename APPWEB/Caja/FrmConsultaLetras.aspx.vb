﻿Imports Entidades
Imports Negocio
Imports general.librerias.serializador
Public Class FrmConsultaLetras
    Inherits System.Web.UI.Page

    Public ReadOnly Property SIdUsuario() As Integer
        Get
            Return CType(Session.Item("IdUsuario"), Integer)
        End Get
    End Property

    Private objScript As New ScriptManagerClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
       
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)
            Response.Cache.SetNoStore()

            Dim Usuario As Integer = CInt(Session("IdUsuario"))
            Me.TxtFechaInicio.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.TxtFechaFinal.Text = Me.TxtFechaInicio.Text

            Dim flag As String = Request.QueryString("flag")
            Select Case flag
                Case "llenarComboBanco"
                    Dim IdEstado As String = Request.QueryString("IdEstadoc")
                    Dim rpta As String = ""

                    Dim lista As New List(Of be_Bancos)
                    Try
                        lista = (New bl_Bancos).listarBancos(Convert.ToInt32(IdEstado))

                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    If lista.Count > 0 Then
                        Try
                            rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                        Catch ex As Exception
                            objScript.mostrarMsjAlerta(Me, ex.Message)
                        End Try
                        Response.Write(rpta)
                        Response.End()
                    Else
                        Response.Write(rpta)
                        Response.End()
                    End If


                Case "llenarComboTienda"
                    Dim IdEmpresa As String = Request.QueryString("IdEmpresa")
                    Dim rpta As String = ""

                    Dim lista As New List(Of be_Tienda)
                    Try
                        lista = (New bl_Tienda).selectTiendasN(Convert.ToInt32(IdEmpresa))
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    If lista.Count > 0 Then
                        Try
                            rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                        Catch ex As Exception
                            objScript.mostrarMsjAlerta(Me, ex.Message)
                        End Try
                        Response.Write(rpta)
                        Response.End()
                    Else
                        Response.Write(rpta)
                        Response.End()
                    End If
                Case "llenarcomboLetra"
                    Dim IdEstado As String = Request.QueryString("IdEstadoc")

                    Dim rpta As String = ""
                    Dim lista As New List(Of be_EstadoLetras)
                    Try
                        lista = (New bl_EstadoLetras).listarEstadoLetras(Convert.ToInt32(IdEstado))
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try
                    If lista.Count > 0 Then
                        Try
                            rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                        Catch ex As Exception
                            objScript.mostrarMsjAlerta(Me, ex.Message)
                        End Try
                        Response.Write(rpta)
                        Response.End()
                    Else
                        Response.Write(rpta)
                        Response.End()
                    End If

            Case "ExportarLetras"

                Dim Empresa As String = Request.QueryString("Empresa")
                Dim IdTienda As String = Request.QueryString("IdTienda")
                Dim IdEstado As String = Request.QueryString("IdEstado")
                Dim fechaInicio As String = Request.QueryString("fechaInicio")
                Dim FechaFinal As String = Request.QueryString("FechaFinal")

                Dim rpta As String = ""
                Dim lista As New List(Of ExportaLetras)
                Try
                    lista = (New bl_LetrasxCobrar).listarLetrasxExportar(Convert.ToInt32(IdTienda), IdEstado, fechaInicio, FechaFinal)
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try
                If lista.Count > 0 Then
                    Try
                        rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try
                    Response.Write(rpta)
                    Response.End()
                Else
                    Response.Write(rpta)
                    Response.End()

                End If
            Case "BuscarLetras"

                'Dim IdEstado As String = Request.QueryString("IdEstadoc")

                Dim Empresa As String = Request.QueryString("Empresa")
                Dim IdTienda As String = Request.QueryString("IdTienda")
                Dim IdEstado As String = Request.QueryString("IdEstado")
                Dim fechaInicio As String = Request.QueryString("fechaInicio")
                Dim FechaFinal As String = Request.QueryString("FechaFinal")

                Dim rpta As String = ""
                Dim lista As New List(Of be_LetrasxCobrar)
                Try
                    lista = (New bl_LetrasxCobrar).listarLetrasxCobrar(Convert.ToInt32(IdTienda), IdEstado, fechaInicio, FechaFinal)
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try
                If lista.Count > 0 Then
                    Try
                        rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try
                    Response.Write(rpta)
                    Response.End()
                Else
                    Response.Write(rpta)
                    Response.End()

                End If
                Case "BuscarEmpleados"
                    Dim Nombre As String = Request.QueryString("Nombre")
                    Dim lista As New List(Of be_UsuarioView)
                    Dim rpta As String = ""
                    Try
                        lista = (New bl_UsuarioView).SelectxEstadoxNombreUsuario(Nombre, 1)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)

                    End Try
                    If lista.Count > 0 Then
                        Try
                            rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                        Catch ex As Exception
                            objScript.mostrarMsjAlerta(Me, ex.Message)
                        End Try
                        Response.Write(rpta)
                        Response.End()
                    Else
                        Response.Write(rpta)
                        Response.End()

                End If

            Case "GuardarDetalleLetras"

                Dim IdDocumento As String = Request.QueryString("IdDocumento")
                Dim IdVendedor As String = Request.QueryString("IdVendedor")
                Dim FechaEntregaVend As String = Request.QueryString("FechaEntregaVend")
                Dim FechaEntregaLFirm As String = Request.QueryString("FechaEntregaLFirm")
                Dim FechaEnvioBanco As String = Request.QueryString("FechaEnvioBanco")
                Dim FechaCancelacion As String = Request.QueryString("FechaCancelacion")
                Dim FechaProtesto As String = Request.QueryString("FechaProtesto")
                Dim NroUnico As String = Request.QueryString("NroUnico")
                Dim IdBanco As String = Request.QueryString("IdBanco")
                Dim FechaPRogramado As String = Request.QueryString("FechaPRogramado")
                Dim FechaRenovacion As String = Request.QueryString("FechaRenovacion")
                Dim CondicionLetra As String = Request.QueryString("CondicionLetra")


                Dim lista As New List(Of be_LetrasxCobrar)
                Dim rpta As String = ""
                Try

                    Dim objeto22 As New Negocio.bl_LetrasxCobrar
                    objeto22 = (New bl_LetrasxCobrar).InsertUpdateLetras(IdDocumento, IdVendedor, FechaEntregaVend, FechaEntregaLFirm, FechaEnvioBanco, FechaCancelacion, FechaProtesto, NroUnico, IdBanco, FechaPRogramado, FechaRenovacion, Usuario, CondicionLetra)
                    Dim rptaGuardar As String
                    rptaGuardar = "ok"
                    Response.Write(rptaGuardar)
                    Response.End()
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)

                End Try
                'If lista.Count > 0 Then
                '    Try
                '        rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                '    Catch ex As Exception
                '        objScript.mostrarMsjAlerta(Me, ex.Message)
                '    End Try
                '    Response.Write(rpta)
                '    Response.End()
                'Else
                '    Response.Write(rpta)
                '    Response.End()

                'End If

        End Select

    End Sub

    'Protected Sub btnFiltroEmpleados_Click(sender As Object, e As EventArgs) Handles btnFiltroEmpleados.Click
    '    Dim obj As New ScriptManagerClass
    '    Try
    '        Dim objUV As New Negocio.UsuarioView
    '        gvFiltroEmpleados.DataSource = objUV.SelectxEstadoxNombre(Me.txtFiltroEmpleados.Text.Trim, 1)
    '        gvFiltroEmpleados.DataBind()

    '        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('CapaMantenimientoDetalle');", True)
    '        If gvFiltroEmpleados.Rows.Count = 0 Then
    '            obj.mostrarMsjAlerta(Me, "No se hallaron registros.")
    '        End If
    '    Catch ex As Exception
    '        obj.mostrarMsjAlerta(Me, "Problemas en la búsqueda.")
    '    End Try
    'End Sub

    'Protected Sub SelectCliente_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim lb As LinkButton = DirectCast(sender, LinkButton)
    '    Dim indice As Integer = DirectCast(lb.NamingContainer, GridViewRow).RowIndex
    '    Me.txtCodigoEmpleado.Text = Me.gvFiltroEmpleados.Rows(indice).Cells(1).Text
    '    Me.txtFiltroEmpleados.Text = Me.gvFiltroEmpleados.Rows(indice).Cells(2).Text
    '    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('CapaMantenimientoDetalle');", True)
    'End Sub
End Class