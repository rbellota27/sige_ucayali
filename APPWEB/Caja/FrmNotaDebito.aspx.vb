﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmNotaDebito
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass

    Private listaDocumentoRef As List(Of Entidades.Documento)
    Private listaDetalleConcepto As List(Of Entidades.DetalleConcepto)

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
    End Enum


#Region "************************ MANEJO DE SESSION"
    Private Function getListaDetalleConcepto() As List(Of Entidades.DetalleConcepto)
        Return CType(Session.Item("listaDetalleConcepto"), List(Of Entidades.DetalleConcepto))
    End Function
    Private Sub setListaDetalleConcepto(ByVal lista As List(Of Entidades.DetalleConcepto))
        Session.Remove("listaDetalleConcepto")
        Session.Add("listaDetalleConcepto", lista)
    End Sub
    Private Function getListaDocumentoRef() As List(Of Entidades.Documento)
        Return CType(Session.Item("listaDocumentoRef"), List(Of Entidades.Documento))
    End Function
    Private Sub setListaDocumentoRef(ByVal lista As List(Of Entidades.Documento))
        Session.Remove("listaDocumentoRef")
        Session.Add("listaDocumentoRef", lista)
    End Sub
#End Region

    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION / SELECCIONAR SALDO=0
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {83, 84, 85, 86, 87, 88})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumentoxCodigo.Enabled = True
        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumentoxCodigo.Enabled = False
        End If

        If listaPermisos(4) > 0 Then  '********* FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

        If (listaPermisos(5) > 0) Then
            Me.chb_CuentaxCobrar.Enabled = True
        Else
            Me.chb_CuentaxCobrar.Enabled = False
        End If
    End Sub

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            ConfigurarDatos()
            inicializarFrm()
        End If
        valAuxiliares()
    End Sub

    Private Sub valAuxiliares()
        Try

            If (Me.cboMoneda.SelectedItem IsNot Nothing) Then
                '*********** Campos Nota de Crédito
                Me.lblMonedaIGV.Text = (Me.cboMoneda.SelectedItem.ToString)
                Me.lblMonedaSubTotal.Text = (Me.cboMoneda.SelectedItem.ToString)
                Me.lblMonedaTotal.Text = (Me.cboMoneda.SelectedItem.ToString)
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub inicializarFrm()
        Try

            Dim objCbo As New Combo
            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.ddlTipoDocumento.SelectedValue))
                .LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)

                .LlenarCboMoneda(Me.cboMoneda, False)
                .LLenarCboEstadoDocumento(Me.cboEstado)
                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.ddlTipoDocumento.SelectedValue), True)
                .LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivo, CInt(Me.cboTipoOperacion.SelectedValue), True)
                'Llenar tipo documento
                .LLenarCboTipoDocumentoRefxIdTipoDocumento(cbotipodocumento, CInt(Me.ddlTipoDocumento.SelectedValue), 2, False)
                '----
                .LlenarCboRol(Me.cboRol, True)
                If (Me.cboRol.Items.FindByValue("1") IsNot Nothing) Then  '********** CLIENTE
                    Me.cboRol.SelectedValue = "1"
                End If

                '*************** DOCUMENTOS DE REFERENCIA
                .LlenarCboTiendaxIdUsuario(Me.cboTienda_DocRef, CInt(Session("IdUsuario")), True)
                If (Me.cboTienda_DocRef.Items.FindByValue(Me.cboTienda.SelectedValue) IsNot Nothing) Then
                    Me.cboTienda_DocRef.SelectedValue = Me.cboTienda.SelectedValue
                End If

            End With

            Dim fechaActual As Date = (New Negocio.FechaActual).SelectFechaActual
            Me.txtFechaEmision.Text = Format(fechaActual, "dd/MM/yyyy")
            Me.txtFechaI.Text = Me.txtFechaEmision.Text
            Me.txtFechaF.Text = Me.txtFechaEmision.Text
            Me.txtFechaInicio_BA.Text = Me.txtFechaEmision.Text
            Me.txtFechaFin_BA.Text = Me.txtFechaEmision.Text

            Me.lblIGV_Tasa.Text = CStr(Math.Round((New Negocio.Impuesto).SelectTasaIGV * 100, 0))
            Me.lblIGV_Tasaproducto.Text = CStr(Math.Round((New Negocio.Impuesto).SelectTasaIGV * 100, 0))

            verFrm(FrmModo.Nuevo, False, True, True, True, True)

            ValidarPermisos()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub verFrm(ByVal FrmModo As Integer, ByVal limpiar As Boolean, ByVal generarCodigo As Boolean, ByVal removeSession As Boolean, ByVal initSession As Boolean, ByVal initParametrosGral As Boolean)

        If (limpiar) Then
            LimpiarFrm()
        End If

        If (generarCodigo) Then
            GenerarCodigoDocumento()
        End If

        If (removeSession) Then
            Session.Remove("listaDetalleConcepto")
            Session.Remove("listaDocumentoRef")
        End If

        If (initSession) Then
            Me.listaDetalleConcepto = New List(Of Entidades.DetalleConcepto)
            Me.listaDocumentoRef = New List(Of Entidades.Documento)
            setListaDetalleConcepto(Me.listaDetalleConcepto)
            setListaDocumentoRef(Me.listaDocumentoRef)
        End If

        If (initParametrosGral) Then

            '******* FECHA DE VCTO
            Dim listaParametros() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {5})
            Me.txtFechaVcto.Text = Format(DateAdd(DateInterval.Day, listaParametros(0), CDate(Me.txtFechaEmision.Text)), "dd/MM/yyyy")

        End If

        Me.hddFrmModo.Value = CStr(FrmModo)
        ActualizarBotonesControl()

    End Sub


    Private Sub ActualizarBotonesControl()

        Select Case CInt(hddFrmModo.Value)

            Case FrmModo.Inicio '************* INICIO
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.btnBuscarDocRef.Visible = False

                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                Me.cboMoneda.Enabled = True

            Case FrmModo.Nuevo '************** Nuevo
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.btnBuscarDocRef.Visible = True

                Me.Panel_Cliente.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = True
                Me.Panel_Obs.Enabled = True

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                Me.cboMoneda.Enabled = True



            Case FrmModo.Editar '*********************** Editar
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.btnBuscarDocRef.Visible = True

                Me.Panel_Cliente.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = True
                Me.Panel_Obs.Enabled = True

                If (Me.GVProducto.Rows.Count > 0) Then
                    Me.Panel_ListaProducto.Visible = True
                Else
                    Me.Panel_ListaProducto.Visible = False
                End If

                ''''concepto'''''''''''''''''
                If (Me.GV_Concepto.Rows.Count > 0) Then
                    Me.Panel_ListaProducto.Visible = True
                Else
                    Me.Panel_ListaProducto.Visible = False
                End If

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False

                Me.cboMoneda.Enabled = True




            Case FrmModo.Buscar_Doc '**************************** Buscar documento
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = False
                Me.txtCodigoDocumento.Focus()
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.btnBuscarDocRef.Visible = False

                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboMoneda.Enabled = True



            Case FrmModo.Documento_Buscar_Exito '********* documento hallado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True
                Me.btnImprimir.Visible = True

                Me.btnBuscarDocRef.Visible = False

                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Obs.Enabled = False

                If (Me.GVProducto.Rows.Count > 0) Then
                    Me.Panel_ListaProducto.Visible = True
                Else
                    Me.Panel_ListaProducto.Visible = False
                End If

                ''''concepto'''''''''''''''''
                If (Me.GV_Concepto.Rows.Count > 0) Then
                    Me.Panel_ListaProducto.Visible = True
                Else
                    Me.Panel_ListaProducto.Visible = False
                End If

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


                Me.cboMoneda.Enabled = False


            Case FrmModo.Documento_Save_Exito '********* documento guardado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = True

                Me.btnBuscarDocRef.Visible = False

                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Obs.Enabled = False

                If (Me.GVProducto.Rows.Count > 0) Then
                    Me.Panel_ListaProducto.Visible = True
                Else
                    Me.Panel_ListaProducto.Visible = False
                End If

                ''''concepto'''''''''''''''''
                If (Me.GV_Concepto.Rows.Count > 0) Then
                    Me.Panel_ListaProducto.Visible = True
                Else
                    Me.Panel_ListaProducto.Visible = False
                End If

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


                Me.cboMoneda.Enabled = False



            Case FrmModo.Documento_Anular_Exito  '*********** Anulacion exitosa

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = True

                Me.btnBuscarDocRef.Visible = False

                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Obs.Enabled = False

                If (Me.GVProducto.Rows.Count > 0) Then
                    Me.Panel_ListaProducto.Visible = True
                Else
                    Me.Panel_ListaProducto.Visible = False
                End If

                ''''concepto'''''''''''''''''
                If (Me.GV_Concepto.Rows.Count > 0) Then
                    Me.Panel_ListaProducto.Visible = True
                Else
                    Me.Panel_ListaProducto.Visible = False
                End If

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboMoneda.Enabled = False


        End Select
    End Sub


    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged

        Try

            Dim objCbo As New Combo

            With objCbo

                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.ddlTipoDocumento.SelectedValue))
                .LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)

            End With

            GenerarCodigoDocumento()

            '************* Línea de Crédito
            If (Me.hddIdPersona.Value.Trim.Length > 0 And IsNumeric(Me.hddIdPersona.Value)) Then
                Me.GV_LineaCredito.DataSource = (New Negocio.CuentaPersona).SelectxParams(CInt(Me.hddIdPersona.Value), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
                Me.GV_LineaCredito.DataBind()
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged

        Try

            GenerarCodigoDocumento()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged

        Try

            Dim objCbo As New Combo

            With objCbo

                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.ddlTipoDocumento.SelectedValue))
                .LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)

            End With

            GenerarCodigoDocumento()

            '************* Línea de Crédito
            If (Me.hddIdPersona.Value.Trim.Length > 0 And IsNumeric(Me.hddIdPersona.Value)) Then
                Me.GV_LineaCredito.DataSource = (New Negocio.CuentaPersona).SelectxParams(CInt(Me.hddIdPersona.Value), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
                Me.GV_LineaCredito.DataBind()
            End If



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GenerarCodigoDocumento()

        If (IsNumeric(Me.cboSerie.SelectedValue)) Then
            Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
        Else
            Me.txtCodigoDocumento.Text = ""
        End If

    End Sub

    Protected Sub btnBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarDocRef.Click
        Try
            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

            actualizarOpcionesBusquedaDocRef(1) '**** buscar doc ref x codigo

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As Integer = 0
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdPersona As Integer = 0
            Dim IdTienda As Integer = 0

            If (Me.hddIdPersona.Value.Trim.Length > 0 And IsNumeric(Me.hddIdPersona.Value)) Then
                If (CInt(Me.hddIdPersona.Value) > 0) Then
                    IdPersona = CInt(Me.hddIdPersona.Value)
                Else
                    Throw New Exception("DEBE SELECCIONAR UNA PERSONA. NO SE PERMITE LA OPERACIÓN.")
                End If
            Else
                Throw New Exception("DEBE SELECCIONAR UNA PERSONA. NO SE PERMITE LA OPERACIÓN.")
            End If

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaI.Text)
                    fechafin = CDate(Me.txtFechaF.Text)
                    IdTienda = CInt(Me.cboTienda_DocRef.SelectedValue)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CInt(Me.txtCodigo_BuscarDocRef.Text)
            End Select

            '*************  IdTienda = 0  (BUSCA DE TODAS LAS TIENDAS)
            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).Documento_BuscarDocumentoRef(CInt(Me.cboEmpresa.SelectedValue), IdTienda, CInt(Me.hddIdPersona.Value), CInt(Me.ddlTipoDocumento.SelectedValue), serie, codigo, CInt(Me.cbotipodocumento.SelectedValue), fechaInicio, fechafin, False)

            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Throw New Exception("NO SE HALLARON REGISTROS.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged

        Try
            cargarDocumentoReferencia(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarDocumentoReferencia(ByVal IdDocumento As Integer)
        Try
            Me.hddIdDocumentoRef.Value = CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value
            Me.hddIdMonedaDocRef.Value = CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdMonedaDocRef_Find"), HiddenField).Value

            '*************** VALIDAMOS 
            Me.listaDocumentoRef = getListaDocumentoRef()
            For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

                If (Me.listaDocumentoRef(i).Id = IdDocumento) Then

                    Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO INGRESADO COMO DOCUMENTO DE REFERENCIA.")

                End If

            Next

            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)
            Me.listaDocumentoRef.Add(objDocumento)
            setListaDocumentoRef(Me.listaDocumentoRef)

            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            actualizarDetalleConcepto()

            Me.listaDetalleConcepto = getListaDetalleConcepto()
            Me.GV_Detalle.DataSource = Me.listaDetalleConcepto
            Me.GV_Detalle.DataBind()

            ''''''actualiza la nota de debito 
            actualizarModoDetalleNotaDebito(0, True)

            valAuxiliares()

           
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularMontos('0');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularMontos1('0');", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub actualizarDetalleConcepto()

        Me.listaDetalleConcepto = getListaDetalleConcepto()

        For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1

            Me.listaDetalleConcepto(i).IdMoneda = CInt(Me.cboMoneda.SelectedValue)
            Me.listaDetalleConcepto(i).Moneda = CStr(Me.cboMoneda.SelectedItem.ToString)
            Me.listaDetalleConcepto(i).Monto = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtAbono"), TextBox).Text)
            Me.listaDetalleConcepto(i).IdConcepto = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("cboConcepto"), DropDownList).SelectedValue)
            Me.listaDetalleConcepto(i).Concepto = CStr(CType(Me.GV_Detalle.Rows(i).FindControl("txtDescripcionConcepto"), TextBox).Text)

        Next

        setListaDetalleConcepto(Me.listaDetalleConcepto)

    End Sub

    Private Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBuscarDocRef.Click
        mostrarDocumentosRef_Find()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        RegistrarDocumento(CInt(Me.cboCondicionPago.SelectedValue))
    End Sub

    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .Id = Nothing
                Case FrmModo.Editar
                    .Id = CInt(Me.hddIdDocumento.Value)
            End Select

            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .Serie = (Me.cboSerie.SelectedItem.ToString)
            .Codigo = (Me.txtCodigoDocumento.Text)
            .FechaEmision = CDate(Me.txtFechaEmision.Text)
            .IdMotivoT = CInt(Me.cboMotivo.SelectedValue)

            Try
                .FechaVenc = CDate(Me.txtFechaVcto.Text)
            Catch ex As Exception
                .FechaVenc = Nothing
            End Try


            .IdTipoDocumento = CInt(Me.ddlTipoDocumento.SelectedValue)
            .IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
            .IdCaja = CInt(Me.cboCaja.SelectedValue)
            .TotalLetras = (New Negocio.ALetras).Letras(Me.txtTotal.Text) + "/100 " + (New Negocio.Moneda).SelectxId(CInt(Me.cboMoneda.SelectedValue))(0).Descripcion


            If (Me.chb_CuentaxCobrar.Checked) Then
                .IdEstadoCancelacion = 1 '********** POR PAGAR

            Else
                .IdEstadoCancelacion = 2 '********** CANCELADO
            End If


            .IdEstadoDoc = 1  '**************** ACTIVO
            .IdMoneda = CInt(Me.cboMoneda.SelectedValue)

            .IdPersona = CInt(Me.hddIdPersona.Value)

            .Total = CDec(Me.txtTotal.Text)
            .TotalAPagar = CDec(Me.txtTotal.Text)
            .IGV = CDec(Me.txtIGV.Text)
            .SubTotal = CDec(Me.txtSubTotal.Text)
            .ImporteTotal = CDec(Me.txtTotal.Text)
            .IdUsuario = CInt(Session("IdUsuario"))
            .IdCondicionPago = CInt(Me.cboCondicionPago.SelectedValue)

        End With

        Return objDocumento

    End Function

    Private Function obtenerObservacion() As Entidades.Observacion
        Dim objObservacion As Entidades.Observacion = Nothing

        If (Me.txtObservaciones.Text.Trim.Length > 0) Then

            objObservacion = New Entidades.Observacion

            With objObservacion

                .Id = Nothing
                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento = Nothing
                    Case FrmModo.Editar
                        .IdDocumento = CInt(Me.hddIdDocumento.Value)
                End Select
                .Observacion = Me.txtObservaciones.Text.Trim

            End With
        End If
        

        Return objObservacion
    End Function
    Private Sub RegistrarDocumento(ByVal Value As Integer)
        Try
            Select CInt(Value)
                Case 2
                    actualizarDetalleConcepto()

                    Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()
                    Me.listaDetalleConcepto = getListaDetalleConcepto()
                    Dim listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = obtenerListaRelacionDocumento()
                    Dim objObservacion As Entidades.Observacion = obtenerObservacion()
                    Dim objMovCuenta As Entidades.MovCuenta = obtenerMovCuenta()

                    Select Case CInt(Me.hddFrmModo.Value)
                        Case FrmModo.Nuevo
                            objDocumento.Id = (New Negocio.DocumentoNotaDebito).RegistrarDocumentoNotaDebito(objDocumento, Me.listaDetalleConcepto, listaRelacionDocumento, objObservacion, objMovCuenta)
                        Case FrmModo.Editar
                            objDocumento.Id = (New Negocio.DocumentoNotaDebito).ActualizarDocumentoNotaDebito(objDocumento, Me.listaDetalleConcepto, listaRelacionDocumento, objObservacion, objMovCuenta)
                    End Select

                    If (objDocumento.Id > 0) Then
                        Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                        verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False)
                        objScript.mostrarMsjAlerta(Me, "La Operación se realizó con Éxito.")
                    Else
                        Throw New Exception("Problemas en la Operación.")
                    End If
                Case 3
                    actualizarDetalleConcepto()

                    Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()
                    Me.listaDetalleConcepto = getListaDetalleConcepto()
                    Dim listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = obtenerListaRelacionDocumento()
                    Dim objObservacion As Entidades.Observacion = obtenerObservacion()
                    Dim objMovCuenta As Entidades.MovCuenta = Nothing

                    Select Case CInt(Me.hddFrmModo.Value)
                        Case FrmModo.Nuevo
                            objDocumento.Id = (New Negocio.DocumentoNotaDebito).RegistrarDocumentoNotaDebito(objDocumento, Me.listaDetalleConcepto, listaRelacionDocumento, objObservacion, objMovCuenta)
                        Case FrmModo.Editar
                            objDocumento.Id = (New Negocio.DocumentoNotaDebito).ActualizarDocumentoNotaDebito(objDocumento, Me.listaDetalleConcepto, listaRelacionDocumento, objObservacion, objMovCuenta)
                    End Select

                    If (objDocumento.Id > 0) Then
                        Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                        verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False)
                        objScript.mostrarMsjAlerta(Me, "La Operación se realizó con Éxito.")
                    Else
                        Throw New Exception("Problemas en la Operación.")
                    End If

            End Select
            Me.cboCondicionPago.SelectedValue = CStr(Value)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaRelacionDocumento() As List(Of Entidades.RelacionDocumento)

        Dim lista As New List(Of Entidades.RelacionDocumento)
        Me.listaDocumentoRef = getListaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            Dim objRelacionDocumento As New Entidades.RelacionDocumento
            With objRelacionDocumento

                .IdDocumento1 = Me.listaDocumentoRef(i).Id

            End With

            lista.Add(objRelacionDocumento)

        Next

        Return lista

    End Function
    Private Function obtenerMovCuenta() As Entidades.MovCuenta

        Dim objMovCuenta As Entidades.MovCuenta = Nothing

        If (Me.chb_CuentaxCobrar.Checked) Then

            objMovCuenta = New Entidades.MovCuenta

            With objMovCuenta

                .IdCuentaPersona = obtenerIdCuentaPersona()
                If (.IdCuentaPersona = 0) Then
                    Throw New Exception("EL CLIENTE NO POSEE LÍNEA DE CRÉDITO.")
                End If
                .IdPersona = CInt(Me.hddIdPersona.Value)
                .Monto = CDec(Me.txtTotal.Text)
                .Factor = 1  '********* AUMENTA LA CUENTA CORRIENTE DE LA PERSONA

                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento = Nothing
                    Case FrmModo.Editar
                        .IdDocumento = CInt(Me.hddIdDocumento.Value)
                End Select

                .IdMovCuentaTipo = 1 '********** CARGO
                .Saldo = CDec(Me.txtTotal.Text)

            End With

        End If

        Return objMovCuenta

    End Function

    Private Function obtenerIdCuentaPersona() As Integer
        Dim IdCuentaPersona As Integer = 0
        Dim total As Decimal = CDec(Me.txtTotal.Text)
        Dim saldoDisponible As Decimal = 0
        For i As Integer = 0 To Me.GV_LineaCredito.Rows.Count - 1

            If (CInt(CType(Me.GV_LineaCredito.Rows(i).FindControl("hddIdMoneda"), HiddenField).Value) = CInt(Me.cboMoneda.SelectedValue)) Then

                saldoDisponible = CDec(CType(Me.GV_LineaCredito.Rows(i).FindControl("lblDisponible"), Label).Text)

                If (total > saldoDisponible) Then
                    Throw New Exception("EL CLIENTE NO POSEE SALDO DISPONIBLE.")
                Else
                    IdCuentaPersona = CInt(CType(Me.GV_LineaCredito.Rows(i).FindControl("hddIdCuenta"), HiddenField).Value)
                    Return IdCuentaPersona
                End If


            End If


        Next

        Return IdCuentaPersona

    End Function

    Protected Sub btnBuscarDocumentoxCodigo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumentoxCodigo.Click
        cargarDocumentoNotaDebito(CInt(Me.cboSerie.SelectedValue), CInt(Me.hddCodigoDocumento.Value), Nothing)
    End Sub
    Private Sub cargarDocumentoNotaDebito(ByVal IdSerie As Integer, ByVal Codigo As Integer, ByVal IdDocumento As Integer)
        Try

            Dim objDocumento As Entidades.Documento = Nothing
            If (IdDocumento <> Nothing) Then
                objDocumento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)
            Else
                objDocumento = (New Negocio.Documento).SelectxIdSeriexCodigo(IdSerie, Codigo)
            End If


            Dim objlistadetalleproducto As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumento(objDocumento.Id)

            Me.listaDetalleConcepto = obtenerListaDetalleConcepto_LoadNotaDebito(objDocumento.Id)
            Me.listaDocumentoRef = obtenerListaDocumentoRef_LoadNotaDebito(objDocumento.Id)

            Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdPersona)
            Dim objObservaciones As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumento.Id)

            cargarDocumentoNotaDebitoInterface(objDocumento, Me.listaDetalleConcepto, objPersona, objObservaciones, Me.listaDocumentoRef,objlistadetalleproducto)

            '************************* CARGAMOS LA MONEDA
            valAuxiliares()

            setListaDetalleConcepto(Me.listaDetalleConcepto)
            setListaDocumentoRef(Me.listaDocumentoRef)

            verFrm(FrmModo.Documento_Buscar_Exito, False, False, False, False, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function obtenerListaDocumentoRef_LoadNotaDebito(ByVal IdDocumento As Integer) As List(Of Entidades.Documento)

        Dim lista As List(Of Entidades.RelacionDocumento) = (New Negocio.RelacionDocumento).RelacionDocumentoSelectxIdDocumento2xIdTipoDocumento(IdDocumento, CInt(Me.ddlTipoDocumento.SelectedValue))
        Dim listaDocumento As New List(Of Entidades.Documento)


        For i As Integer = 0 To lista.Count - 1

            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(lista(i).IdDocumento1)

            listaDocumento.Add(objDocumento)

        Next

        Return listaDocumento

    End Function
    Private Function obtenerListaDetalleConcepto_LoadNotaDebito(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleConcepto)

        Dim lista As List(Of Entidades.DetalleConcepto) = (New Negocio.DetalleConcepto).SelectxIdDocumento(IdDocumento)

        For i As Integer = 0 To lista.Count - 1

            lista(i).ListaConcepto = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(CInt(Me.ddlTipoDocumento.SelectedValue))

        Next

        Return lista

    End Function
    Private Function obtenerDetalleRecibo_Find(ByVal IdDocumento As Integer, ByVal moneda As String) As List(Of Entidades.DetalleReciboView_1)

        Dim lista As List(Of Entidades.DetalleRecibo) = (New Negocio.DetalleRecibo).DetalleReciboSelectxIdDocumento(IdDocumento)
        Dim listaReturn As New List(Of Entidades.DetalleReciboView_1)

        Dim listaConcepto As List(Of Entidades.Concepto) = (New Negocio.Concepto).SelectCboxTipoUso(2) '***** NOTA DE DEBITO

        For i As Integer = 0 To lista.Count - 1

            Dim obj As New Entidades.DetalleReciboView_1
            With obj

                .IdDocumento = lista(i).IdDocumento
                .IdDetalleRecibo = lista(i).IdDetalleRecibo
                .Moneda = moneda
                .Monto = lista(i).Monto
                .Concepto = lista(i).Concepto
                .IdConcepto = lista(i).IdConcepto
                .ListaConcepto = listaConcepto

            End With
            listaReturn.Add(obj)

        Next

        Return listaReturn

    End Function





    Private Sub cargarDocumentoNotaDebitoInterface(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objPersona As Entidades.PersonaView, ByVal objObservacion As Entidades.Observacion, ByVal listaDocumentoRef As List(Of Entidades.Documento), ByVal listaDetalleProducto As List(Of Entidades.DetalleDocumento))

        '*********** CABECERA
        Me.txtCodigoDocumento.Text = objDocumento.Codigo

        If (objDocumento.FechaEmision <> Nothing) Then
            Me.txtFechaEmision.Text = Format(objDocumento.FechaEmision, "dd/MM/yyyy")
        End If

        If (objDocumento.FechaVenc <> Nothing) Then
            Me.txtFechaVcto.Text = Format(objDocumento.FechaVenc, "dd/MM/yyyy")
        End If

        If (Me.cboEstado.Items.FindByValue(CStr(objDocumento.IdEstadoDoc)) IsNot Nothing) Then
            Me.cboEstado.SelectedValue = CStr(objDocumento.IdEstadoDoc)
        End If

        If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumento.IdTipoOperacion)) IsNot Nothing) Then
            Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)
        End If

        If Not Me.cboMotivo.Items.FindByValue(CStr(objDocumento.IdMotivoT)) Is Nothing Then
            Me.cboMotivo.SelectedValue = CStr(objDocumento.IdMotivoT)
        End If

        If (Me.cboMoneda.Items.FindByValue(CStr(objDocumento.IdMoneda)) IsNot Nothing) Then
            Me.cboMoneda.SelectedValue = CStr(objDocumento.IdMoneda)
        End If

        '************* DETALLE
        Me.GV_Detalle.DataSource = Me.listaDetalleConcepto
        Me.GV_Detalle.DataBind()


        If (listaDetalleProducto.Count > 0) Then
            Me.GVProducto.DataSource = listaDetalleProducto
            Me.GVProducto.DataBind()

        End If

        If (listaDetalleConcepto.Count > 0) Then
            Me.GV_Concepto.DataSource = listaDetalleConcepto
            Me.GV_Concepto.DataBind()
        End If


        '************** DOC REF
        Me.GV_DocumentoRef.DataSource = listaDocumentoRef
        Me.GV_DocumentoRef.DataBind()

        '************** CLIENTE
        If (objPersona IsNot Nothing) Then
            Me.txtDescripcionPersona.Text = objPersona.Descripcion
            Me.txtRUC.Text = objPersona.Ruc
            Me.txtDNI.Text = objPersona.Dni
            Me.hddIdPersona.Value = CStr(objPersona.IdPersona)

            Me.GV_LineaCredito.DataSource = (New Negocio.CuentaPersona).SelectxParams(CInt(Me.hddIdPersona.Value), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
            Me.GV_LineaCredito.DataBind()

        End If

        '******************** TOTALES
        Me.txtSubTotal.Text = CStr(Math.Round(objDocumento.SubTotal, 2))
        Me.txtIGV.Text = CStr(Math.Round(objDocumento.IGV, 2))
        Me.txtTotal.Text = CStr(Math.Round(objDocumento.TotalAPagar, 2))


        Me.txtSubTotalproducto.Text = CStr(Math.Round(objDocumento.SubTotal, 2))
        Me.txtIGVproducto.Text = CStr(Math.Round(objDocumento.IGV, 2))
        Me.txtTotalproducto.Text = CStr(Math.Round((objDocumento.TotalAPagar), 2))

        '************** OBSERVACIONES
        If (objObservacion IsNot Nothing) Then
            Me.txtObservaciones.Text = objObservacion.Observacion
        End If

        '******************* HIDDEN
        Me.hddIdDocumento.Value = CStr(objDocumento.Id)
        Me.hddIdPersona.Value = CStr(objDocumento.IdPersona)

    End Sub

    Private Sub LimpiarFrm()

        '************* CABECERA
        Me.txtCodigoDocumento.Text = ""
        Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.cboEstado.SelectedValue = "1"  '******* Activo por defecto
        Me.cboTipoOperacion.SelectedIndex = 0
        Me.hddIdDocumento.Value = ""
        Me.hddCodigoDocumento.Value = ""

        '************* DETALLE
        Me.GV_Detalle.DataSource = Nothing
        Me.GV_Detalle.DataBind()

        Me.GVProducto.DataSource = Nothing
        Me.GVProducto.DataBind()
        Me.GV_Concepto.DataSource = Nothing
        Me.GV_Concepto.DataBind()


        '************ DATOS DEL CLIENTE
        Me.txtDescripcionPersona.Text = ""
        Me.txtDNI.Text = ""
        Me.txtRUC.Text = ""
        Me.hddIdPersona.Value = ""

        Me.GV_LineaCredito.DataSource = Nothing
        Me.GV_LineaCredito.DataBind()

        '************* TOTALES DEL DOCUMENTO
        Me.txtSubTotal.Text = "0"
        Me.txtIGV.Text = "0"
        Me.txtTotal.Text = "0"
        Me.txtSubTotalproducto.Text = "0"
        Me.txtIGVproducto.Text = "0"
        Me.txtTotalproducto.Text = "0"
        '*************** DOCUMENTO DE REFERENCIA
        Me.GV_DocumentoRef.DataSource = Nothing
        Me.GV_DocumentoRef.DataBind()

        '***************** OBSERVACIONES
        Me.txtObservaciones.Text = ""

        '*********** Otros
        Me.txtSerie_BuscarDocRef.Text = ""
        Me.txtCodigo_BuscarDocRef.Text = ""

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Try
            verFrm(FrmModo.Nuevo, True, True, True, True, True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        Try

            verFrm(FrmModo.Editar, False, False, False, False, False)
            Me.GV_LineaCredito.DataSource = (New Negocio.CuentaPersona).SelectAllConsultaxIdPersona(CInt(Me.hddIdPersona.Value), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
            Me.GV_LineaCredito.DataBind()

            Me.listaDocumentoRef = getListaDocumentoRef()
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            Me.listaDetalleConcepto = getListaDetalleConcepto()
            Me.GV_Detalle.DataSource = Me.listaDetalleConcepto
            Me.GV_Detalle.DataBind()


            '*************** CARGAMOS EL DETALLE de productos
            Dim listaDetalleproducto As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumento(CInt(Me.hddIdDocumento.Value))
            Dim listaDetalleconcepto As List(Of Entidades.DetalleConcepto) = (New Negocio.DetalleConcepto).SelectxIdDocumento(CInt(Me.hddIdDocumento.Value))


            If (listaDetalleproducto.Count > 0) Then

                For i As Integer = 0 To listaDetalleproducto.Count - 1
                    listaDetalleproducto(i).CantxAtender = listaDetalleproducto(i).CantidadDetalleAfecto
                Next

                Me.GVProducto.DataSource = listaDetalleproducto
                Me.GVProducto.DataBind()
            End If


            If (listaDetalleconcepto.Count > 0) Then

                Me.GV_Concepto.DataSource = listaDetalleconcepto
                Me.GV_Concepto.DataBind()
            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        AnularDocumentoNotaDebito()
    End Sub

    Private Sub AnularDocumentoNotaDebito()
        Try

            If ((New Negocio.DocumentoNotaDebito).AnularDocumentoNotaDebito(CInt(Me.hddIdDocumento.Value))) Then
                Me.cboEstado.SelectedValue = "2"  '*********** ANULADO
                verFrm(FrmModo.Documento_Anular_Exito, False, False, False, False, False)
                objScript.mostrarMsjAlerta(Me, "La Operación se realizó con Éxito.")
            Else
                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboMoneda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMoneda.SelectedIndexChanged
        actualizarMontos_MonedaEmision()
    End Sub
    Private Sub actualizarMontos_MonedaEmision()

        Try

            actualizarDetalleConcepto()

            Me.listaDetalleConcepto = getListaDetalleConcepto()

            Me.GV_Detalle.DataSource = Me.listaDetalleConcepto
            Me.GV_Detalle.DataBind()

           
            '''''''''''''modificando las monedas para que pueda ser utilizado el producto''

            Dim IdMonedaOrigen, IdMonedaDestino As Integer
            IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)

            If (IdMonedaDestino = 1) Then  '*********** SOLES
                IdMonedaOrigen = 2
            ElseIf (IdMonedaDestino = 2) Then  '**************** DOLARES
                IdMonedaOrigen = 1
            Else
                Throw New Exception("La Moneda Origen difiere de la Configuración del Sistema.")
            End If

            Dim objUtil As New Negocio.Util

            With objUtil
                '''''''------detalle producto----------------'''
                Dim lista As List(Of Entidades.DetalleDocumento) = obtenerListaDetalle()
                Dim lista1 As List(Of Entidades.DetalleConcepto) = obtenerListaDetalleconcepto()

                For i As Integer = 0 To lista.Count - 1
                    lista(i).PrecioSD = Math.Round(.SelectValorxIdMonedaOxIdMonedaD(IdMonedaOrigen, IdMonedaDestino, lista(i).PrecioSD, "E"), 2)
                    lista(i).PrecioCD = Math.Round(.SelectValorxIdMonedaOxIdMonedaD(IdMonedaOrigen, IdMonedaDestino, lista(i).PrecioCD, "E"), 2)
                Next

                Me.GVProducto.DataSource = lista
                Me.GVProducto.DataBind()

                Me.GV_Concepto.DataSource = lista1
                Me.GV_Concepto.DataBind()

                Me.txtSubTotalproducto.Text = CStr(Math.Round(.SelectValorxIdMonedaOxIdMonedaD(IdMonedaOrigen, IdMonedaDestino, CDec(Me.txtSubTotalproducto.Text), "E"), 2))
                Me.txtIGVproducto.Text = CStr(Math.Round(.SelectValorxIdMonedaOxIdMonedaD(IdMonedaOrigen, IdMonedaDestino, CDec(Me.txtIGVproducto.Text), "E"), 2))

                Me.txtTotalproducto.Text = CStr(Math.Round(.SelectValorxIdMonedaOxIdMonedaD(IdMonedaOrigen, IdMonedaDestino, (CDec(Me.txtTotalproducto.Text)), "E"), 2))

            End With
            valAuxiliares()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularMontos('0');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularMontos1('0');", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function obtenerListaDetalle(ByVal cantFilas_Add As Integer) As List(Of Entidades.DetalleReciboView_1)


        '********** Obtengo la lista de conceptos 
        Dim listaAux As List(Of Entidades.Concepto) = (New Negocio.Concepto).SelectCboxTipoUso(2) '**** NOTA DE DEBITO

        Dim lista As New List(Of Entidades.DetalleReciboView_1)
        For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1

            Dim objDetalleRecibo As New Entidades.DetalleReciboView_1

            With objDetalleRecibo

                .Moneda = Me.cboMoneda.SelectedItem.ToString
                .Monto = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtAbono"), TextBox).Text)
                .Concepto = CType(Me.GV_Detalle.Rows(i).FindControl("txtDescripcionConcepto"), TextBox).Text
                .ListaConcepto = listaAux
                .IdConcepto = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("cboConcepto"), DropDownList).SelectedValue)

            End With

            lista.Add(objDetalleRecibo)

        Next

        For i As Integer = 0 To cantFilas_Add - 1

            Dim objDetalleRecibo As New Entidades.DetalleReciboView_1

            With objDetalleRecibo

                .Moneda = Me.cboMoneda.SelectedItem.ToString
                .Monto = 0
                .Concepto = ""
                .ListaConcepto = listaAux
                .IdConcepto = 0

            End With

            lista.Add(objDetalleRecibo)

        Next

        Return lista

    End Function

    Protected Sub btnAddDetalle_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddDetalle.Click
        addNuevoConcepto()
    End Sub
    Private Sub addNuevoConcepto()
        Try

            actualizarDetalleConcepto()

            Dim objDetalleConcepto As New Entidades.DetalleConcepto
            With objDetalleConcepto
                .ListaConcepto = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(CInt(Me.ddlTipoDocumento.SelectedValue))
                .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
                .Moneda = CStr(Me.cboMoneda.SelectedItem.ToString)
                .Monto = 0
                .Concepto = ""
                .IdConcepto = Nothing
            End With

            Me.listaDetalleConcepto = getListaDetalleConcepto()
            Me.listaDetalleConcepto.Add(objDetalleConcepto)
            setListaDetalleConcepto(Me.listaDetalleConcepto)

            Me.GV_Detalle.DataSource = Me.listaDetalleConcepto
            Me.GV_Detalle.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_Detalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Detalle.RowDataBound

        Try

            If (e.Row.RowType = DataControlRowType.DataRow) Then
                Dim cboConcepto As DropDownList = CType(e.Row.FindControl("cboConcepto"), DropDownList)
                cboConcepto.DataSource = Me.listaDetalleConcepto(e.Row.RowIndex).ListaConcepto
                cboConcepto.DataBind()
                If (Me.listaDetalleConcepto(e.Row.RowIndex).IdConcepto <> Nothing) Then
                    cboConcepto.SelectedValue = CStr(Me.listaDetalleConcepto(e.Row.RowIndex).IdConcepto)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Private Sub addPersona(ByVal IdPersona As Integer)


        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)

        If (objPersona IsNot Nothing) Then
            '************** DATOS
            Me.txtDescripcionPersona.Text = objPersona.getNombreParaMostrar
            Me.txtRUC.Text = objPersona.Ruc
            Me.txtDNI.Text = objPersona.Dni
            Me.hddIdPersona.Value = CStr(objPersona.IdPersona)

            '***************** ELIMINAMOS EL DOCUMENTO DE REFERENCIA
            Me.GV_DocumentoRef.DataSource = Nothing
            Me.GV_DocumentoRef.DataBind()

            '**************** LIMPIAMOS LA GRILLA DE BUSQUEDA DE DOCUMENTOS DE REFERENCIA
            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

            '************** LINEA DE CREDITO
            Me.GV_LineaCredito.DataSource = (New Negocio.CuentaPersona).SelectxParams(CInt(Me.hddIdPersona.Value), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
            Me.GV_LineaCredito.DataBind()

            '********************* RECALCULAMOS SALDOS
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " offCapa('capaPersona');  calcularMontos('0');", True)

        End If

    End Sub

    Private Sub GV_Detalle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Detalle.SelectedIndexChanged
        quitarConceptoDetalle()
    End Sub
    Private Sub quitarConceptoDetalle()
        Try

            actualizarDetalleConcepto()

            Me.listaDetalleConcepto = getListaDetalleConcepto()
            Me.listaDetalleConcepto.RemoveAt(Me.GV_Detalle.SelectedIndex)
            setListaDetalleConcepto(Me.listaDetalleConcepto)

            Me.GV_Detalle.DataSource = Me.listaDetalleConcepto
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularMontos('0');", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub
    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        objScript.onCapa(Me, "capaDocumentosReferencia")

    End Sub
    Private Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub


#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", Me.cboRol.SelectedValue)

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            addPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))
            objScript.offCapa(Me, "capaPersona")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

#End Region


#Region "************************ BÚSQUEDA AVANZADO"

    Private Sub btnAceptarBusquedaAvanzado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBusquedaAvanzado.Click
        busquedaAvanzado()
    End Sub

    Private Sub busquedaAvanzado()

        Try

            Dim IdPersona As Integer = 0
            Try
                IdPersona = CInt(Me.hddIdPersona.Value)
            Catch ex As Exception
                IdPersona = 0
            End Try

            Dim IdSerie As Integer = CInt(Me.cboSerie.SelectedValue)
            Dim fechaInicio As Date = CDate(Me.txtFechaInicio_BA.Text)
            Dim fechaFin As Date = CDate(Me.txtFechaFin_BA.Text)

            Me.GV_BusquedaAvanzado.DataSource = (New Negocio.Documento).DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(CInt(Me.ddlTipoDocumento.SelectedValue), IdSerie, IdPersona, fechaInicio, fechaFin)
            Me.GV_BusquedaAvanzado.DataBind()

            If (Me.GV_BusquedaAvanzado.Rows.Count <= 0) Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  onCapa('capaBusquedaAvanzado');  alert('No se hallaron registros.');    ", True)
            Else
                objScript.onCapa(Me, "capaBusquedaAvanzado")
            End If



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Protected Sub GV_BusquedaAvanzado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BusquedaAvanzado.SelectedIndexChanged
        Try

            cargarDocumentoNotaDebito(0, 0, CInt(CType(Me.GV_BusquedaAvanzado.SelectedRow.FindControl("hddIdDocumento_BusquedaAvanzado"), HiddenField).Value))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

#End Region

    
    Private Sub GV_DocumentoRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoRef.SelectedIndexChanged
        quitarDocumentoReferencia(Me.GV_DocumentoRef.SelectedIndex)
    End Sub
    Private Sub quitarDocumentoReferencia(ByVal Index As Integer)
        Try

            Me.listaDocumentoRef = getListaDocumentoRef()

            Me.listaDocumentoRef.RemoveAt(Index)

            setListaDocumentoRef(Me.listaDocumentoRef)

            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnQuitarDetalle_Click(ByVal sender As Object, ByVal e As EventArgs)

        QuitarProductoDetalle(CType(sender, ImageButton))
    End Sub
    Protected Sub btnQuitarDetalleconcepto_Click(ByVal sender As Object, ByVal e As EventArgs)

        QuitarProductoDetalleConcepto(CType(sender, ImageButton))
    End Sub

    Protected Sub QuitarProductoDetalle(ByVal boton As ImageButton)

        Try

            Dim listaDetalle As List(Of Entidades.DetalleDocumento) = obtenerListaDetalle()

            Dim gvRow As GridViewRow = CType(boton.NamingContainer, GridViewRow)

            listaDetalle.RemoveAt(gvRow.RowIndex)

            Me.GVProducto.DataSource = listaDetalle
            Me.GVProducto.DataBind()



            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularMontos1('0');", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Protected Sub QuitarProductoDetalleConcepto(ByVal boton As ImageButton)

        Try

            Dim listaDetalle1 As List(Of Entidades.DetalleConcepto) = obtenerListaDetalleconcepto()

            Dim gvRow As GridViewRow = CType(boton.NamingContainer, GridViewRow)

            listaDetalle1.RemoveAt(gvRow.RowIndex)

            Me.GV_Concepto.DataSource = listaDetalle1
            Me.GV_Concepto.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularMontos1('0');", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function obtenerListaDetalle() As List(Of Entidades.DetalleDocumento)

        Dim lista As New List(Of Entidades.DetalleDocumento)

        For i As Integer = 0 To Me.GVProducto.Rows.Count - 1

            Dim objDetalle As New Entidades.DetalleDocumento

            With objDetalle

                .IdDetalleDocumento = CInt(CType(Me.GVProducto.Rows(i).FindControl("hddIdDetalleDocumento_Detalle"), HiddenField).Value)
                .IdDetalleAfecto = CInt(CType(Me.GVProducto.Rows(i).FindControl("hddIdDetalleAfecto"), HiddenField).Value)
                .IdProducto = CInt(CType(Me.GVProducto.Rows(i).FindControl("hddIdProducto"), HiddenField).Value)
                .CodigoProducto = CStr(CType(Me.GVProducto.Rows(i).FindControl("lblCodigoProducto"), Label).Text)
                .Cantidad = CDec(CType(Me.GVProducto.Rows(i).FindControl("txtCantidadDetalle"), TextBox).Text)
                .UMedida = CStr(CType(Me.GVProducto.Rows(i).FindControl("lblUM_Detalle"), Label).Text)
                .NomProducto = CStr(Me.GVProducto.Rows(i).Cells(4).Text)
                .PrecioCD = CDec(CType(Me.GVProducto.Rows(i).FindControl("txtPUnit_Detalle"), TextBox).Text)
                .Importe = CDec(CType(Me.GVProducto.Rows(i).FindControl("txtImporte_Detalle"), TextBox).Text)
                .CantxAtender = CDec(Me.GVProducto.Rows(i).Cells(2).Text)
                .PrecioSD = CDec(Me.GVProducto.Rows(i).Cells(5).Text)
                .IdUnidadMedida = CInt(CType(Me.GVProducto.Rows(i).FindControl("hddIdUnidadMedidaDetalle"), HiddenField).Value)

            End With



            lista.Add(objDetalle)

        Next

        Return lista

    End Function
    Private Function obtenerListaDetalleconcepto() As List(Of Entidades.DetalleConcepto)
        Dim lista1 As New List(Of Entidades.DetalleConcepto)

        For i As Integer = 0 To Me.GV_Concepto.Rows.Count - 1
            Dim objDetalle1 As New Entidades.DetalleConcepto

            With objDetalle1

                .Descripcion = CStr(Me.GV_Concepto.Rows(i).Cells(2).Text)
                .Monto = CDec(CType(Me.GV_Concepto.Rows(i).FindControl("txtConcepto"), TextBox).Text)

            End With
            lista1.Add(objDetalle1)
        Next
        Return lista1

    End Function

    Private Sub btnCargarArticuloAll_DocRef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCargarArticuloAll_DocRef.Click
        actualizarModoDetalleNotaDebito(CInt(Me.cboNotaDebitoModo.SelectedValue), True)
    End Sub
    Private Sub actualizarModoDetalleNotaDebito(ByVal Value As Integer, Optional ByVal Referencia As Boolean = True)
        Try
            If (Referencia) Then
                Select Case CInt(Value)
                    Case 0 '************* POR LISTA DE PRODUCTOS

                        Me.Panel_ListaProducto.Visible = True


                        '*************** Detalle Documento
                        Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumento(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))
                        Dim listaDetalleconcepto As List(Of Entidades.DetalleConcepto) = (New Negocio.DetalleConcepto).SelectxIdDocumento(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))



                        ''''''detalle de los productos'''''

                        Dim objUtil As New Negocio.Util

                        '***************************** Monedas

                        Dim IdMonedaOrigen, IdMonedaDestino As Integer
                        IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                        'IdMonedaOrigen = CInt(Me.hddIdMonedaDocRef.Value)
                        IdMonedaOrigen = CInt(Me.cboMoneda.SelectedValue)
                       
                        For i As Integer = 0 To listaDetalle.Count - 1

                            listaDetalle(i).PrecioCD = Math.Round(objUtil.SelectValorxIdMonedaOxIdMonedaD(IdMonedaOrigen, IdMonedaDestino, listaDetalle(i).PrecioCD, "E"), 2)
                            listaDetalle(i).IdDetalleAfecto = listaDetalle(i).IdDetalleDocumento
                            listaDetalle(i).CantxAtender = listaDetalle(i).Cantidad
                            listaDetalle(i).PrecioSD = listaDetalle(i).PrecioCD
                            ''''''''''''
                            'listaDetalleconcepto(i).Monto = listaDetalleconcepto(i).Monto


                        Next

                        Me.GVProducto.DataSource = listaDetalle
                        Me.GVProducto.DataBind()

                        Me.GV_Concepto.DataSource = listaDetalleconcepto
                        Me.GV_Concepto.DataBind()


                End Select

            Else



                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo

                        Select Case CInt(Value)
                            Case 0 '************* POR LISTA DE PRODUCTOS

                                Me.Panel_ListaProducto.Visible = True


                                '*************** Detalle Documento
                                Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumento(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))
                                Dim listaDetalleconcepto As List(Of Entidades.DetalleConcepto) = (New Negocio.DetalleConcepto).SelectxIdDocumento(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))



                                For i As Integer = 0 To listaDetalle.Count - 1
                                    listaDetalle(i).CantxAtender = listaDetalle(i).Cantidad
                                    listaDetalle(i).PrecioSD = listaDetalle(i).PrecioCD
                                Next
                                Me.GVProducto.DataSource = listaDetalle
                                Me.GVProducto.DataBind()

                                Me.GV_Concepto.DataSource = listaDetalleconcepto
                                Me.GV_Concepto.DataBind()


                        End Select


                    Case FrmModo.Editar

                        Select Case CInt(Value)
                            Case 0 '************* POR LISTA DE PRODUCTOS

                                Me.Panel_ListaProducto.Visible = True

                                '*************** Detalle Documento
                                Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumento(CInt(Me.hddIdDocumento.Value))
                                Dim listaDetalleconcepto As List(Of Entidades.DetalleConcepto) = (New Negocio.DetalleConcepto).SelectxIdDocumento(CInt(Me.hddIdDocumento.Value))

                                Me.GVProducto.DataSource = listaDetalle
                                Me.GVProducto.DataBind()

                                Me.GV_Concepto.DataSource = listaDetalleconcepto
                                Me.GV_Concepto.DataBind()


                        End Select

                End Select

            End If

            Me.cboNotaDebitoModo.SelectedValue = CStr(Value)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularMontos1('0');", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
   
    Private Sub cboNotaDebitoModo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNotaDebitoModo.SelectedIndexChanged
        actualizarModoDetalleNotaDebito(CInt(Me.cboNotaDebitoModo.SelectedValue))
    End Sub

    Private Sub ddlTipoDocumento_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlTipoDocumento.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.ddlTipoDocumento.SelectedValue))
            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub cboTipoOperacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoOperacion.SelectedIndexChanged
        OnChange_TipoOperacion()
    End Sub
    Private Sub OnChange_TipoOperacion()
        Dim objCbo As New Combo
        With objCbo
            .LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivo, CInt(Me.cboTipoOperacion.SelectedValue), True)
        End With
    End Sub
End Class