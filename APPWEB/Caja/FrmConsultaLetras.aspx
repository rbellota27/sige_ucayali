﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmConsultaLetras.aspx.vb" Inherits="APPWEB.FrmConsultaLetras" %>

<%@ Register Assembly="APPWEB" Namespace="APPWEB" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 

    <table class="style1">
 
    <tr>
        <td style="height: 23px; color:white; text-align: center; background-color: #0000FF;">
        
            <strong style="text-align: center; background-color: #3333FF">CONSULTA DE LETRAS POR COBRAR</strong></tr>
    <tr>
        <td style="height: 12px">
            <table class="style1" style="background-color: #99CCFF">
                <tr>
                    <td style="width: 102px; text-align: right; color: #3333FF; height: 35px;">
                        <strong>Fecha Inicio :</strong></td>
                    <td style="width: 127px; height: 35px;">
                        <asp:TextBox ID="TxtFechaInicio" runat="server" style="margin-left: 0px" ></asp:TextBox>                                         
                                           <cc1:calendarextender ID="txtfecha_CalendarExtender" runat="server" 
                                           TargetControlID="TxtFechaInicio"  Format="dd/MM/yyyy"  /></td>
                    <td style="width: 6px; height: 35px;">
                        </td>
                    <td style="width: 107px; color: #3333FF; height: 35px;">
                        <strong>Fecha Final :</strong></td>
                    <td style="width: 135px; height: 35px;"><asp:TextBox ID="TxtFechaFinal" runat="server" style="margin-left: 0px" ></asp:TextBox>                                         
                                           <cc1:calendarextender ID="TxtFechaFinal_CalendarExtender" runat="server" 
                                           TargetControlID="TxtFechaFinal"  Format="dd/MM/yyyy"  /></td>
                    <td style="width: 2px; height: 35px;">
                        </td>
                    <td style="width: 117px; text-align: right; color: #3333FF; height: 35px;">
                        <strong>Estado Letras :</strong></td>
                    <td style="height: 35px">
                        <select id="cboEstadoLetras" name="D1" style="width: 233px">
                            <option></option>
                        </select></td>
                        <td style="width: 6px"></td>
                        <td style="width: 54px; color: #0000FF;"><strong>Tienda:</strong></td>
                        <td style="width: 251px">
                        <select id="cboTienda" name="D5" style="width: 233px">
                            <option></option>
                        </select></td>
                        <td></td>
                         <td>
                                        &nbsp;</td>
                </tr>
            
                <tr>
                    <td style="width: 102px">
                        &nbsp;</td>
                    <td style="width: 127px">
                                        &nbsp;</td>
                    <td style="width: 6px">
                        &nbsp;</td>
                    <td style="width: 107px">
                        &nbsp;</td>
                    <td style="width: 135px">
                        &nbsp;</td>
                    <td style="width: 2px">
                        &nbsp;</td>
                            <td style="width: 117px">
                                &nbsp;</td>
                            <td style="width: 26px">
                                        <input id="btnConsultar" style="width: 227px" type="button" 
                                            onclick="return btnConsultar_onclick(this)" 
                            value="Consultar Letras" /></td>
                            <td style="width: 6px">
                                &nbsp;</td>
                    <td style="width: 54px">
                        &nbsp;</td>
                    <td style="width: 251px">
                                        <input id="btnExportar" style="width: 126px" type="button" 
                                            onclick="return btnExportar_onclick(this)" 
                            value="Exportar" /></td>

                    <td></td>
                     <td></td>
                </tr>
            </table>
            </td>
    </tr>
 
    <tr>
        <td style="height: 12px">
       
        </td>
    </tr>
    <tr>
        <td>
            <div id="CapaRegistrosLetras"></div>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
    </tr>

   


</table>

 <div id="CapaMantenimientoDetalle" style="border: 3px solid blue; padding: 4px; width: 1050px;
            height: 550px; position: fixed; top: 120px; left:350px; background-color: #FFFFFF;
            z-index: 3; display:none;">
                   <table border="0" cellspacing="0px" style="width: 960px">
                       <tr>
                            <td style="height: 32px">
                                  <table border="0">
                                                <tr class="BarraTitulo">
                                                <td style="width:105%; background-color: #0066CC; text-align: center; height: 28px;"><span id="TituloPopup">
                                                    <span style="color: #FFFFFF"><strong>Detalle Letras x Cobrar</strong></span> </span></td>
                                                <td style="width:4%; height: 28px;"><img src='../ImagenesForm/imgcerrar.png' class="Iconoventanas" 
                                                        title="Cerrar Ventana" onclick="return cerrarVentana()"/></td>
                                                </tr>

                                  </table>
                            </td>
                        </tr>
                    </table>

                    <table class="style1" style="width: 93%">
                        <tr>
                            <td style="width: 312px; text-align: right; color: #0033CC; font-weight: 700;">
                                IdDocumento :</td>
                            <td class="style2" style="width: 28px">
                            
                                  

                                    &nbsp;</td>
                            <td style="width: 415px">
                            
                                  

                                    <table class="style1">
                                        <tr>
                                            <td style="width: 122px">
                            
                                  

                                    <input id="txtiddocumento" style="width: 126px" type="text" readonly/></td>
                                            <td style="width: 86px; text-align: right; font-weight: 700; color: #003399">
                                                Codigo:</td>
                                            <td>
                            
                                  

                                    <input id="TxtCodigoDoc" 
                                                    style="width: 151px; font-weight: 700; color: #0066CC; font-size: medium;" 
                                                    type="text" readonly/></td>
                                        </tr>
                                    </table>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td style="width: 181px">
                                &nbsp;</td>
                            <td>
                       
                          
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 312px; text-align: right; color: #0033CC;">
                                <strong>Vendedor Responsable :</strong></td>
                            <td class="style2" style="width: 28px">
                            
                            <input id="txtcodigo" style="width: 86px" type="text" readonly/></td>
                            <td style="width: 415px">
                               
                                <input id="txtFiltroEmpleado" style="width: 330px" type="text" /><input 
                                    id="btnbusqueda" type="button" value="Buscar" 
                                    onclick="return btnbusqueda_onclick(this)" style="width: 59px"/></td>
                            <td>
                                &nbsp;</td>
                            <td style="width: 181px">
                                &nbsp;</td>
                            <td>
                       
                          
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 312px; text-align: right; font-weight: 700; color: #0033CC;">
                                Fecha Entrega Letra al Vendedor :</td>
                            <td class="style2" style="width: 28px">
                                &nbsp;</td>
                            <td style="width: 415px">
                                 <asp:TextBox ID="TxtFechaEntregaVend" runat="server" style="margin-left: 0px" ></asp:TextBox>                                         
                                           <cc1:calendarextender ID="TxtFechaEntregaVend_CalendarExtender" runat="server" 
                                           TargetControlID="TxtFechaEntregaVend"  Format="dd/MM/yyyy"  /></td>
                            <td>
                                &nbsp;</td>
                            <td colspan="4" rowspan="11">
                                 
                                 <div id="CapaBusquedaPersona" style="border: 3px solid blue; padding: 4px; width: 500px; height: 500px; position: absolute; top: 0px; left:350px; background-color: #FFFFFF;z-index: 3; display:none;">
                                      
                                        <table>
                                             <tr class="BarraTitulo">
                                                <td style="width:96%; background-color: #0066CC; text-align: center; height: 28px;"></td>
                                                <td style="width:4%; height: 28px;"><img src='../ImagenesForm/imgcerrar.png' class="Iconoventanas" 
                                                        title="Cerrar Ventana" onclick="return cerrarVentanaPersona()"/></td>
                                                </tr>
                                                </table>
                                        <table>
                                        <tr>
                                        <td>
                                        <div id="Personas"></div>
                                        </td>
                                        </tr>
                                        </table>
                                     
                                   </div>
                               
                               </td>
                        </tr>
                        <tr>
                            <td style="width: 312px; text-align: right; color: #0033CC;">
                                <b>Fecha Entrega Letra Firmada :</b></td>
                            <td class="style2" style="width: 28px">
                                &nbsp;</td>
                            <td style="width: 415px">
                                 <asp:TextBox ID="TxtFechaEntregaLetFirm" runat="server" style="margin-left: 0px" ></asp:TextBox>                                         
                                           <cc1:calendarextender ID="TxtFechaEntregaLetFirm_CalendarExtender" runat="server" 
                                           TargetControlID="TxtFechaEntregaLetFirm"  Format="dd/MM/yyyy"  /></td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 312px; text-align: right; color: #0033CC;">
                                <b>Banco donde se deposita Letra :</b></td>
                            <td class="style2" style="width: 28px">
                                &nbsp;</td>
                            <td style="width: 415px">
                                <select id="cmbBanco" name="D2" style="width: 325px">
                                    <option></option>
                                </select></td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 312px; text-align: right; color: #0033CC;">
                                <b>Fecha Envio al Banco :</b></td>
                            <td class="style2" style="width: 28px">
                                &nbsp;</td>
                            <td style="width: 415px">
                               <asp:TextBox ID="TxtFechaEnvioBanco" runat="server" style="margin-left: 0px" ></asp:TextBox>                                         
                                           <cc1:calendarextender ID="TxtFechaEnvioBanco_CalendarExtender" runat="server" 
                                           TargetControlID="TxtFechaEnvioBanco"  Format="dd/MM/yyyy"  /></td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 312px; height: 23px; text-align: right; color: #0033CC;">
                                <b>Condicion de Letra :</b></td>
                            <td style="height: 23px; width: 28px">
                                </td>
                            <td style="height: 23px; width: 415px">
                                <select id="cmbCondicionLetra" name="D4" style="width: 321px">
                                  <option value="0">--TODOS---</option>
                                 <option value="1">En Cobranza</option>
                                 <option value="2">En Cartera</option>
                                 <option value="3">En Descuento</option>
                                 <option value="4">Renovacion</option>
                                </select></td>
                            <td style="height: 23px; ">
                                </td>
                        </tr>
                        <tr>
                            <td style="width: 312px; text-align: right; color: #0033CC;">
                                <b>Nro Unico :</b></td>
                            <td class="style2" style="width: 28px">
                                &nbsp;</td>
                            <td style="width: 415px">
                                <input id="TxtNroUnico" style="width: 127px" type="text" /></td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 312px; color: #0033CC; text-align: right;">
                                <b style="text-align: right">Fecha de Cancelacion :</b></td>
                            <td class="style2" style="width: 28px">
                                &nbsp;</td>
                            <td style="width: 415px">
                                <asp:TextBox ID="TxtFechaCancelacion" runat="server" style="margin-left: 0px" ></asp:TextBox>                                         
                                           <cc1:calendarextender ID="TxtFechaCancelacion_CalendarExtender" runat="server" 
                                           TargetControlID="TxtFechaCancelacion"  Format="dd/MM/yyyy"  /></td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 312px; color: #0033CC; text-align: right;">
                                <strong>Fecha de Renovación :</strong></td>
                            <td class="style2" style="width: 28px">
                                &nbsp;</td>
                            <td style="width: 415px">
                                <asp:TextBox ID="TxtFechaRenovacion" runat="server" style="margin-left: 0px" ></asp:TextBox>                                         
                                           <cc1:calendarextender ID="TxtFechaRenovacion_calendarextender" runat="server" 
                                           TargetControlID="TxtFechaRenovacion"  Format="dd/MM/yyyy"  /></td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 312px; height: 23px; text-align: right; color: #0033CC;">
                                <b>Fecha de Pago Programado :</b></td>
                            <td style="height: 23px; width: 28px">
                                </td>
                            <td style="height: 23px; width: 415px">
                                <asp:TextBox ID="TxtFechaProgramado" runat="server" style="margin-left: 0px" ></asp:TextBox>                                         
                                           <cc1:calendarextender ID="TxtFechaProgramado_calendarextender" runat="server" 
                                           TargetControlID="TxtFechaProgramado"  Format="dd/MM/yyyy"  /></td>
                            <td style="height: 23px; ">
                                </td>
                        </tr>
                        <tr>
                            <td style="width: 312px; text-align: right; color: #0033CC;">
                                <b style="text-align: right">Fecha de Protesto :</b></td>
                            <td class="style2" style="width: 28px">
                                &nbsp;</td>
                            <td style="width: 415px">
                                <asp:TextBox ID="TxtFechaProtesto" runat="server" style="margin-left: 0px" ></asp:TextBox>                                         
                                           <cc1:calendarextender ID="TxtFechaProtesto_CalendarExtender" runat="server" 
                                           TargetControlID="TxtFechaProtesto"  Format="dd/MM/yyyy"  /></td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 312px; text-align: right; color: #0033CC;">
                                <b>Recibo de Ingreso Relacionado :</b></td>
                            <td class="style2" style="width: 28px">
                                &nbsp;</td>
                            <td style="width: 415px">
                                <input id="TxtReciboIngreso" type="text" style="width: 468px" readonly /></td>
                            <td>
                                &nbsp;</td>
                        </tr>
                         <tr>
                            <td style="width: 312px; text-align: right; color: #0033CC;">
                                <strong>Facturas Relacionadas :</strong></td>
                            <td class="style2" style="width: 28px">
                                &nbsp;</td>
                            <td style="width: 415px">
                                <input id="TxtFacturasRelacionadas" type="text" style="width: 468px" readonly /></td>
                            <td>
                                &nbsp;</td>
                            <td style="width: 181px">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                         <tr>
                            <td style="width: 312px; text-align: right; color: #0033CC;">
                                <b>Observacion :</b></td>
                            <td class="style2" style="width: 28px">
                                &nbsp;</td>
                            <td style="width: 415px">
                                <input id="TxtObservacion" style="width: 390px" type="text" readonly /></td>
                            <td>
                                &nbsp;</td>
                            <td style="width: 181px">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                         <tr>
                            <td style="width: 312px; text-align: right; color: #0033CC;">
                                <b>Estado :</b></td>
                            <td class="style2" style="width: 28px">
                                &nbsp;</td>
                            <td style="width: 415px">
                                <select id="cmbEstadoLetra" name="D3" style="width: 314px" disabled="disabled">
                                    <option></option>
                                </select></td>
                            <td>
                                &nbsp;</td>
                            <td style="width: 181px">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                         <tr>
                            <td style="width: 312px">
                                &nbsp;</td>
                            <td class="style2" style="width: 28px">
                                &nbsp;</td>
                            <td style="width: 415px">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td style="width: 181px">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                         <tr>
                            <td style="width: 312px">
                                &nbsp;</td>
                            <td class="style2" style="width: 28px">
                                &nbsp;</td>
                            <td style="width: 415px">
                                <table class="style1">
                                    <tr>
                                        <td style="width: 80px">
                                <input id="btnGuardar"  onclick="return btnGuardar_onclick(this)"  style="width: 101px" 
                                                type="button" value="Guardar" /></td>
                                        <td style="width: 10px">
                                            &nbsp;</td>
                                        <td style="width: 82px">
                                            <input id="btnCancelar" style="width: 92px" type="button" value="Cancelar" /></td>
                                        <td style="width: 13px">
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                             </td>
                            <td>
                                &nbsp;</td>
                            <td style="width: 181px">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                         <tr>
                            <td style="width: 312px">
                                &nbsp;</td>
                            <td class="style2" style="width: 28px">
                                &nbsp;</td>
                            <td style="width: 415px">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td style="width: 181px">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                   </table>
                    <br />
   </div>

  <script type="text/javascript" language="javascript">

      window.onload = varios();


      function varios() {
          ConsultaCombos();
          ConsultaComboTienda();
          ConsultaComboBanco();
      }

      function ConsultaCombos() {

          var opcion = "llenarcomboLetra";
          var IdEstadoc = 1;
          var datacombob = new FormData();
          datacombob.append('flag', 'llenarcomboLetra');
          datacombob.append('IdEstadoc', IdEstadoc);

          var xhrrep = new XMLHttpRequest();
          xhrrep.open("POST", "FrmConsultaLetras.aspx?flag=" + opcion + "&IdEstadoc=" + IdEstadoc, true);
          xhrrep.onreadystatechange = function () {
              if (xhrrep.readyState == 4 && xhrrep.status == 200) {
                  mostrarListasCombos(xhrrep.responseText);
              }
          }
          xhrrep.send(datacombob);
          return false;
      }

      function mostrarListasCombosTienda(rptacombot) {
          if (rptacombot != "") {
              var listas3 = rptacombot;
              data3 = crearArray(listas3, "▼");
              listarcomboTienda();
          }
          return false;
      }

      function ConsultaComboBanco() {
          var opcion = "llenarComboBanco";
          var IdEstadoc = 1;

          var datacombob = new FormData();
          datacombob.append('flag', 'llenarComboBanco');
          datacombob.append('IdEstadoc', IdEstadoc);


          var xhrrep = new XMLHttpRequest();
          xhrrep.open("POST", "FrmConsultaLetras.aspx?flag=" + opcion + "&IdEstadoc=" + IdEstadoc, true);
          xhrrep.onreadystatechange = function () {
              if (xhrrep.readyState == 4 && xhrrep.status == 200) {
                  mostrarListasCombosBanco(xhrrep.responseText);
              }
          }

          xhrrep.send(datacombob);
          return false;

      }


      function mostrarListasCombosBanco(rptac) {

          if (rptac != "") {
              var listab = rptac;
              data4 = crearArray(listab, "▼");
              listarcombosBancos();
          }
          return false;

      }

      function listarcomboTienda() {
          var TipoServicio = [];
          var nRegistros = data3.length;
          var campos2;
          var TipoServ;
          for (var i = 0; i < nRegistros; i++) {
              campos2 = data3[i].split("|");
              {
                  TipoServ = campos2[0];
                  TipoServ += "|";
                  TipoServ += campos2[1];
                  TipoServicio.push(TipoServicio);
              }
          }
          crearCombo(data3, "cboTienda", "|");
      }


      function btnExportar_onclick(objeto) {
          ExportandoLetras(objeto);
      }
      function ConsultarRegistrosaExportar(objeto) {
          //  exportandoexcel();
          BusquedaLetras();
          return false;
      }

//      function ConsultarRegistrosaExportar(objeto) {
//          var opcion = "ConsultaDocumentosaExportar";
//          var fechaInicio = document.getElementById('<= TxtFechaInicio.ClientID%>').value;
//          var FechaFinal = document.getElementById('<= TxtFechaFin.ClientID%>').value;
//          var idtienda = document.getElementById("cboTienda").value;
//          var idAprobado = document.getElementById("cboAprobacion").value;
//          var idArea = document.getElementById("cboArea").value;

//          var parts = FechaFinal.split('/');
//          var mydate = parts[2];
//          var periodo = mydate;

//          var datarep = new FormData();
//          datarep.append('flag', 'ConsultaDocumentosaExportar');
//          datarep.append('fechaInicio', fechaInicio);
//          datarep.append('FechaFinal', FechaFinal);
//          datarep.append('idtienda', idtienda);
//          datarep.append('idAprobado', idAprobado);
//          datarep.append('idArea', idArea);

//          var xhr = new XMLHttpRequest();
//          xhr.open("POST", "FrmAprobacionPagoRequerimientos.aspx?flag=" + opcion + "&fechaInicio=" + fechaInicio + "&FechaFinal=" + FechaFinal + "&idtienda=" + idtienda + "&idAprobado=" + idAprobado + "&idArea=" + idArea, true);
//          xhr.onloadstart = function () { objeto.disabled = true; objeto.value = "Exportando..."; }
//          xhr.onloadend = function () { objeto.disabled = false; objeto.value = "Exportar a Excel"; }

//          xhr.onreadystatechange = function () {
//              if (xhr.readyState == 4 && xhr.status == 200) {
//                  fun_listaaExportar(xhr.responseText);
//              }
//          }
//          xhr.send(null);
//          return false;
//      }

      function fun_listaaExportar(listaExp)
       {
          filasexp = listaExp.split("▼");
          exportandoexcel();
       }

      function exportandoexcel() {
          crearTablaaExportar();
          var blob = new Blob([excel], { type: 'application/vnd.ms-excel' });
          if (navigator.appVersion.toString().indexOf('.NET') > 0) {
              window.navigator.msSaveBlob(blob, "Reporte.xls");
          }
          else {
              this.download = "Reporte.xls";
              this.href = window.URL.createObjectURL(blob);
          }
      }


      function crearTablaaExportar() {
          crearMatrizaExportar();
          excel = "<html><head><meta charset='UTF-8'></meta></head>"
          var nRegistros = matrizExp.length;
          if (nRegistros > 0) {

              //var cabeceras = ["IdDocumento", "doc_Serie", "doc_Codigo", "doc_FechaEmision", "doc_FechaVenc", "RUCODNI", "RazonSocial", "FacturasRelacionadas", "NroUnico", "doc_Total", "MontoCancelado", "Saldo", "Estado", "FechaEntregaVendedor", "FechaRecepcionLetraFirmada", "FechaEnvioBanco", "FechaPendientePago", "FechaRenovacion", "FechaEnProtesto", "FechaAnulacion", "FechaProgramada", "FechaCancelacion", "IdEstadoLetra", "idpersona", "RecibosIngreso", "IdVendedorR", "IdBanco", "EstadoLetra", "Nombre", "CondicionLetra", "Observacion","EstadoLetraCambio"];
              var cabeceras = ["EstadoProtesto","FechaEnProtesto","EstadoLetraCambio", "FechaCancelacion", "doc_FechaEmision", "doc_Codigo", "Total", "Saldo", "MontoCancelado", "RazonSocial", "doc_FechaVenc", "FacturasRelacionadas", "FechaEntregaVendedor", "Nombre", "FechaRecepcionLetraFirmada", "FechaEnvioBanco", "NroUnico", "CondicionLetra","RecibosIngreso", "Banco", "IdEstadoLetra"];


              var nCampos = cabeceras.length;
              excel += "<table><thead><tr>";
              for (var j = 0; j < nCampos; j++) 
              {
                  excel += "<th style='background-color:blue;color:white;'>";
                  excel += cabeceras[j];
                  excel += "</th>";
              }

              excel += "</tr></thead><tbody>";
              for (var i = 0; i < nRegistros; i++) {
                  excel += "<tr>";

                  for (var j = 0; j < nCampos; j++) {

                      if (matrizExp[i][20] == "1") {      
                          //  POR CANCELAR
                          //  contenido += "<tr bgcolor='#C4F9DA'>";
                          excel += "<td style='background-color:lightgrey;color:blue;'>";
                          excel += matrizExp[i][j];
                          excel += "</td>";
                      }

                      if (matrizExp[i][20] == "2") {  //POR CANCELAR
                          //  contenido += "<tr bgcolor='#C4F9DA'>";
                          excel += "<td style='background-color:lightgrey;color:blue;'>";
                          excel += matrizExp[i][j];
                          excel += "</td>";
                      }
                      if (matrizExp[i][20] == "3") {  //POR CANCELAR
                          //  contenido += "<tr bgcolor='#C4F9DA'>";
                          excel += "<td style='background-color:lightgrey;color:blue;'>";
                          excel += matrizExp[i][j];
                          excel += "</td>";
                      }
                      if (matrizExp[i][20] == "4") {  //POR CANCELAR
                          //  contenido += "<tr bgcolor='#C4F9DA'>";
                          excel += "<td style='background-color:lightgrey;color:blue;'>";
                          excel += matrizExp[i][j];
                          excel += "</td>";
                      }
                      if (matrizExp[i][20] == "5") {  //CANCELADAS  VERDES
                          //                          contenido += "<tr bgcolor='#C4F9DA'>";
                          excel += "<td style='background-color:mediumaquamarine;color:blue;'>";
                          excel += matrizExp[i][j];
                          excel += "</td>";
                      }
                      if (matrizExp[i][20] == "6") {  //RENEGOCIADAS  AMARILLAS
                          //                          contenido += "<tr bgcolor='#F3FAA3'>";
                          excel += "<td style='background-color:lemonchiffon;color:blue;'>";
                          excel += matrizExp[i][j];
                          excel += "</td>";
                      }
                      if (matrizExp[i][20] == "7") {  //PROTESTADAS ROJOS
                          //                          contenido += "<tr bgcolor='#FB2309'>";
                          excel += "<td style='background-color:orangered;color:blue;'>";
                          excel += matrizExp[i][j];
                          excel += "</td>";
                      }
                      if (matrizExp[i][20] == "8") {  // ANULADAS  MORADO PLOMIZO
//                          contenido += "<tr bgcolor='#6D7AAF'>";
                          excel += "<td style='background-color:mediumorchid;color:blue;'>";
//                          excel += "<td bgcolor='#6D7AAF';color:blue;'>";
                          excel += matrizExp[i][j];
                          excel += "</td>";
                      }
                  
                      
                  }
                  excel += "</tr>";
              }
              excel += "</tbody></table></body></html>";

          }
      }

      function crearMatrizaExportar() {
          matrizExp = [];
          var nRegistros = filasexp.length;
          var nCampos;
          var campos;
          var c = 0;
          var exito;
          var textos = document.getElementsByClassName("texto");
          var nTextos = textos.length;
          var texto;
          var x;
          for (var i = 0; i < nRegistros; i++) {
              exito = true;
              campos = filasexp[i].split("|");
              nCampos = campos.length;
              for (var j = 0; j < nTextos; j++) {
                  texto = textos[j];
                  x = j;

                  if (texto.value.length > 0) {
                      exito = campos[x].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                  }

                  if (!exito) break;

              }
              if (exito) {
                  matrizExp[c] = [];
                  for (var j = 0; j < nCampos; j++) {
                      if (isNaN(campos[j])) matrizExp[c][j] = campos[j];

                      else matrizExp[c][j] = campos[j];
                  }
                  c++;
              }
          }
      }



      function ConsultaComboTienda() {

          var opcion = "llenarComboTienda";
          var IdEmpresa = 1;
          var datacombo = new FormData();
          datacombo.append('flag', 'llenarComboTienda');
          datacombo.append('IdEmpresa', IdEmpresa);
          var xhrrep = new XMLHttpRequest();
          xhrrep.open("POST", "FrmConsultaLetras.aspx?flag=" + opcion + "&IdEmpresa=" + IdEmpresa, true);
          xhrrep.onreadystatechange = function () {
              if (xhrrep.readyState == 4 && xhrrep.status == 200) {
                  mostrarListasCombosTienda(xhrrep.responseText);
              }
          }
          xhrrep.send(datacombo);
          return false;
      }

      function mostrarListasCombos(rptacombo) {
          if (rptacombo != "") {
              var listas2 = rptacombo;
              data1 = crearArray(listas2, "▼");
              listarcombos();
          }
          return false;
      }



      function listarcombos() 
      {
          var EstadoLetra = [];
          var nRegistros = data1.length;
          var campos2;
          var EstLet;
          for (var i = 0; i < nRegistros; i++) {
              campos2 = data1[i].split("|");
              {
                  EstLet = campos2[0];
                  EstLet += "|";
                  EstLet += campos2[1];
                  EstadoLetra.push(EstadoLetra);
              }
          }
          crearCombo(data1, "cboEstadoLetras", "|");
          crearCombo(data1, "cmbEstadoLetra", "|");

      }

      function listarcombosBancos() {

          var EstadoLetra = [];
          var nRegistros = data4.length;
          var campos2;
          var EstLet;
          for (var i = 0; i < nRegistros; i++) {
              campos2 = data4[i].split("|");
              {
                  EstLet = campos2[0];
                  EstLet += "|"; 
                  EstLet += campos2[1];
                  EstadoLetra.push(EstadoLetra);
              }
          }
          crearCombo(data4, "cmbBanco", "|");

      }



      function crearCombo(lista, nombreCombo, separador, nombreItem, valorItem) {
         
          var contenido = "";
          if (nombreItem != null && valorItem != null) {
              contenido += "<option value='";
              contenido += valorItem;
              contenido += "'>";
              contenido += nombreItem;
              contenido += "</option>";
          }
          var nRegistros = lista.length;
          if (nRegistros > 0) {
              var campos;
              for (var i = 0; i < nRegistros; i++) {
                  campos = lista[i].split(separador);
                  contenido += "<option value='";
                  contenido += campos[0];
                  contenido += "'>";
                  contenido += campos[1];
                  contenido += "</option>";
              }
          }
          var cbo = document.getElementById(nombreCombo);
          if (cbo != null) cbo.innerHTML = contenido;

      }


      function crearArray(lista, separador) {
          var data = lista.split(separador);
          if (data.length === 1 && data[0] === "") data = [];
          return data;
      }


      function btnGuardar_onclick(Objeto) { 
         GuardarDetalleLetras(Objeto)
     }


      function btnConsultar_onclick(Objeto) {
          BusquedaLetras(Objeto)
      }
      function btnbusqueda_onclick(Objeto) { 
          BusquedaPersona(Objeto)
      }

      function GuardarDetalleLetras(objeto, flag) {
          //document.getElementById('CapaBusquedaPersona').style.display = 'block';
         
          var opcion = "GuardarDetalleLetras";
          //var Nombre = document.getElementById('txtFiltroEmpleado').value;

          var IdDocumento = document.getElementById('txtiddocumento').value;
          var IdVendedor = document.getElementById('txtcodigo').value;
          var NroUnico = document.getElementById('TxtNroUnico').value;
          var IdBanco = document.getElementById('cmbBanco').value;
          // <cc2:MsgBox1 runat="server"></cc2:MsgBox1>
          var CondicionLetra = document.getElementById('cmbCondicionLetra').value;   // 1- En Cobranza 2- En Cartera 3- Descuento
          
          var FechaEntregaVend = document.getElementById('<%= TxtFechaEntregaVend.ClientID%>').value;
          var FechaEntregaLFirm = document.getElementById('<%= TxtFechaEntregaLetFirm.ClientID%>').value;
          var FechaEnvioBanco = document.getElementById('<%= TxtFechaEnvioBanco.ClientID%>').value;
          var FechaCancelacion = document.getElementById('<%= TxtFechaCancelacion.ClientID%>').value;
          var FechaRenovacion = document.getElementById('<%= TxtFechaRenovacion.ClientID%>').value;
          var FechaPRogramado = document.getElementById('<%= TxtFechaProgramado.ClientID%>').value;
          var FechaProtesto = document.getElementById('<%= TxtFechaProtesto.ClientID%>').value;

//          var IdEstado = 1;
          var datadetalle = new FormData();
          datadetalle.append('flag', 'GuardarDetalleLetras');
          datadetalle.append('IdDocumento', IdDocumento);
          datadetalle.append('IdVendedor', IdVendedor);
          datadetalle.append('FechaEntregaVend', FechaEntregaVend);
          datadetalle.append('FechaEntregaLFirm', FechaEntregaLFirm);
          datadetalle.append('FechaEnvioBanco', FechaEnvioBanco);
          datadetalle.append('FechaCancelacion', FechaCancelacion);
          datadetalle.append('FechaProtesto', FechaProtesto);
          datadetalle.append('NroUnico', NroUnico);
          datadetalle.append('IdBanco', IdBanco);
          datadetalle.append('FechaPRogramado', FechaPRogramado);
          datadetalle.append('FechaRenovacion', FechaRenovacion);
          datadetalle.append('CondicionLetra', CondicionLetra);
          var xhrConsulta = new XMLHttpRequest();

          xhrConsulta.open("POST", "FrmConsultaLetras.aspx?flag=" + opcion + "&IdDocumento=" + IdDocumento + "&IdVendedor=" + IdVendedor + "&FechaEntregaVend=" + FechaEntregaVend + "&FechaEntregaLFirm=" + FechaEntregaLFirm + "&FechaEnvioBanco=" + FechaEnvioBanco + "&FechaCancelacion=" + FechaCancelacion + "&FechaProtesto=" + FechaProtesto + "&NroUnico=" + NroUnico + "&IdBanco=" + IdBanco + "&FechaPRogramado=" + FechaPRogramado + "&FechaRenovacion=" + FechaRenovacion + "&CondicionLetra=" + CondicionLetra, true); 
          xhrConsulta.onloadstart = function () { objeto.disabled = true; objeto.value = "Guardando..."; }
          xhrConsulta.onloadend = function () { objeto.disabled = false; objeto.value = "Guardar"; }
          xhrConsulta.onreadystatechange = function () {

              if (xhrConsulta.readyState == 4 && xhrConsulta.status == 200) {
                  (xhrConsulta.responseText);
                  if (xhrConsulta.toString = "ok") {
                      alert("Se guardo Correctamente");
                  }
              }
   

          }
          xhrConsulta.send(datadetalle);
          return false;
      }

      function BusquedaPersona(objeto, flag) {

          document.getElementById('CapaBusquedaPersona').style.display = 'block';
          var opcion = "BuscarEmpleados";
          var Nombre = document.getElementById('txtFiltroEmpleado').value;

          var IdEstado = 1;
          var datapersona = new FormData();

          datapersona.append('flag', 'BuscarEmpleados');
          datapersona.append('Nombre', Nombre);
          datapersona.append('IdEstado', IdEstado);
         
          var xhrConsulta = new XMLHttpRequest();

          xhrConsulta.open("POST", "FrmConsultaLetras.aspx?flag=" + opcion + "&Nombre=" + Nombre + "&IdEstado=" + IdEstado, true);
          xhrConsulta.onloadstart = function () { objeto.disabled = true; objeto.value = "Buscando..."; }
          xhrConsulta.onloadend = function () { objeto.disabled = false; objeto.value = "Buscar"; }
          xhrConsulta.onreadystatechange = function () {
             
              if (xhrConsulta.readyState == 4 && xhrConsulta.status == 200) {
                  fun_listaPersona(xhrConsulta.responseText);
              }
          }
          xhrConsulta.send(datapersona);
          return false;
      }

      function BusquedaLetras(objeto, flag) {

          var opcion = "BuscarLetras";
//        var IdEstado = null;
          var fechaInicio = document.getElementById('<%= TxtFechaInicio.ClientID%>').value;
          var FechaFinal = document.getElementById('<%= TxtFechaFinal.ClientID%>').value;
          var IdTienda = document.getElementById("cboTienda").value;
          var Empresa = '001';
//        var Estado = document.getElementById("cboEstadoLetras").value;  
          var IdEstado = document.getElementById("cboEstadoLetras").value;

          var dataMovimientos = new FormData();

          dataMovimientos.append('flag', 'BuscarLetras');
          dataMovimientos.append('Empresa', Empresa);
          dataMovimientos.append('IdEstado', IdEstado);
          dataMovimientos.append('fechaInicio', fechaInicio);
          dataMovimientos.append('FechaFinal', FechaFinal);
          dataMovimientos.append('IdTienda', IdTienda);
          var xhrGenera = new XMLHttpRequest();

          xhrGenera.open("POST", "FrmConsultaLetras.aspx?flag=" + opcion + "&Empresa=" + Empresa + "&IdEstado=" + IdEstado + "&fechaInicio=" + fechaInicio + "&FechaFinal=" + FechaFinal + "&IdTienda=" + IdTienda, true);


          xhrGenera.onloadstart = function () { objeto.disabled = true; objeto.value = "Procesando..."; }
          xhrGenera.onloadend = function () { objeto.disabled = false; objeto.value = "Consulta Letras"; }


          xhrGenera.onreadystatechange = function () {
              if (xhrGenera.readyState == 4 && xhrGenera.status == 200) {
                  fun_lista(xhrGenera.responseText);

//                  fun_listaaExportar(xhrGenera.responseText);

              }
          }
          xhrGenera.send(dataMovimientos);
          return false;

      }


      function ExportandoLetras(objeto, flag) {

          var opcion = "ExportarLetras";
     
          var fechaInicio = document.getElementById('<%= TxtFechaInicio.ClientID%>').value;
          var FechaFinal = document.getElementById('<%= TxtFechaFinal.ClientID%>').value;
          var IdTienda = document.getElementById("cboTienda").value;
          var Empresa = '001';
          //var Estado = document.getElementById("cboEstadoLetras").value;  
          var IdEstado = document.getElementById("cboEstadoLetras").value;

          var dataExportar = new FormData();

          dataExportar.append('flag', 'ExportarLetras');
          dataExportar.append('Empresa', Empresa);
          dataExportar.append('IdEstado', IdEstado);
          dataExportar.append('fechaInicio', fechaInicio);
          dataExportar.append('FechaFinal', FechaFinal);
          dataExportar.append('IdTienda', IdTienda);
          var xhtExportaLetras = new XMLHttpRequest();

          xhtExportaLetras.open("POST", "FrmConsultaLetras.aspx?flag=" + opcion + "&Empresa=" + Empresa + "&IdEstado=" + IdEstado + "&fechaInicio=" + fechaInicio + "&FechaFinal=" + FechaFinal + "&IdTienda=" + IdTienda, true);


          xhtExportaLetras.onloadstart = function () { objeto.disabled = true; objeto.value = "Exportando..."; }
          xhtExportaLetras.onloadend = function () { objeto.disabled = false; objeto.value = "Exportar"; }


          xhtExportaLetras.onreadystatechange = function () {
              if (xhtExportaLetras.readyState == 4 && xhtExportaLetras.status == 200)
              {
//                  fun_lista(xhtExportaLetras.responseText);

                  fun_listaaExportar(xhtExportaLetras.responseText);

              }
          }
          xhtExportaLetras.send(dataExportar);
          return false;

      }





      function fun_listaPersona(lista) {
          filas = lista.split("▼");

          crearTablaConsultaPersona();
          crearMatrizConsultaPersona();
          mostrarMatrizConsultaPersona();
          //configurarFiltrosPersona();

      }

      function fun_lista(lista) {
          filas = lista.split("▼");

           crearTablaConsulta();
           crearMatrizConsulta();
           mostrarMatrizConsulta();
           configurarFiltros();
       }

       function configurarFiltros() {
           var textos = document.getElementsByClassName("texto");
           var nTextos = textos.length;
           var texto;
           for (var j = 0; j < nTextos; j++) {
               texto = textos[j];
               texto.onkeyup = function () {
                   crearMatrizConsulta();
                   mostrarMatrizConsulta();
               }
           }
       }

       function mostrarMatrizConsulta() {
           var nRegistros = matrizConsulta.length;
           var contenido = "";
           if (nRegistros > 0) {
               var nCampos = matrizConsulta[0].length;
               for (var i = 0; i < nRegistros; i++) {

                  if (matrizConsulta[i][22] == "8") {  // ANULADAS  MORADO PLOMIZO
                      contenido += "<tr bgcolor='#6D7AAF'>";
                    }
                   if (matrizConsulta[i][22] == "5") {  //CANCELADAS  VERDES
                       contenido += "<tr bgcolor='#C4F9DA'>";
                   }
                   if (matrizConsulta[i][22] == "6") {  //RENEGOCIADAS  AMARILLAS
                       contenido += "<tr bgcolor='#F3FAA3'>"; 
                   }
                   if (matrizConsulta[i][22] == "7") {  //PROTESTADAS ROJOS
                       contenido += "<tr bgcolor='#FB2309'>";
                   }

//                   if (matrizConsulta[i][22] == "4") {  // NO INGRESADAS AZUL CLARO
//                       contenido += "<tr bgcolor='#1892F8'>";
//                   }


                   for (var j = 0; j < nCampos; j++) {
                       var cadenaid
                       var cadenareq
                       cadenaid = '' + matrizConsulta[i][1] + '';

                       //                       IdEstadoLetra
               

                       if (j == 0) {
                      
                           contenido += "<td>";
                           contenido += "<input id='btnmas' type='image' img src='../ImagenesForm/imgeditar.png' onclick='return MostrarDetalle(";
                           contenido += matrizConsulta[i][0] + ",";   // IdDocumento
                           contenido += '"' + matrizConsulta[i][8] + '"' + ",";  // NroUnico
                           contenido += '"' + matrizConsulta[i][25] + '"' + ","; // IdVendedor
                           contenido += '"' + matrizConsulta[i][13] + '"' + ","; // FechaEntregavendedor
                           contenido += '"' + matrizConsulta[i][14] + '"' + ","; // FechaEntLetFirmada
                           contenido += '"' + matrizConsulta[i][15] + '"' + ","; // FechaEnvioBanco
                           contenido += '"' + matrizConsulta[i][16] + '"' + ","; // FechaPendiente
                           contenido += '"' + matrizConsulta[i][17] + '"' + ","; // FechaRenovacion
                           contenido += '"' + matrizConsulta[i][18] + '"' + ","; // FechaProtesto
                           contenido += '"' + matrizConsulta[i][19] + '"' + ","; // FechaAnulacion
                           contenido += '"' + matrizConsulta[i][20] + '"' + ","; // FechaProgramado
                           contenido += '"' + matrizConsulta[i][21] + '"' + ","; // FechaCancelacion
                           contenido += '"' + matrizConsulta[i][26] + '"' + ","; // IdBanco
                           contenido += '"' + matrizConsulta[i][22] + '"' + ","; // IdEstado
                           contenido += '"' + matrizConsulta[i][24] + '"' + ","; // RecibosIngreso
                           contenido += '"' + matrizConsulta[i][7] + '"' + ","; // FacturasRelacionadas
                           contenido += '"' + matrizConsulta[i][28] + '"' + ","; // Nombre 
                           contenido += '"' + matrizConsulta[i][29] + '"' + ",";     // CondicionLetra
                           contenido += '"' + matrizConsulta[i][1] + '-' + matrizConsulta[i][2] + '"' + ",";   // CodigoDoc
                           contenido += '"' + matrizConsulta[i][30] + '"' ;   
                           contenido += ")' >";
                           contenido += "</td>";
                           contenido += "<td>"
                           contenido += matrizConsulta[i][j];
                           contenido += "</td>";
                       }

                       if (j > 0 && j < 8) {
                           contenido += "<td>";
                           contenido += matrizConsulta[i][j];
                           contenido += "</td>";
                       }

                       if (j > 7 && j < 11) {
                           contenido += "<td align='right'>";
                           contenido += matrizConsulta[i][j];
                           contenido += "</td>";
                       }
                       if (j == 11) {
                           contenido += "<td>";
                           contenido += matrizConsulta[i][j];
                           contenido += "</td>";
                       }

                       if (j == 12) {
                           contenido += "<td>";
                           contenido += matrizConsulta[i][j];
                           contenido += "</td>";
                       }

                   }
                   contenido += "</tr>";
               }
           }

           var tabla = document.getElementById("tbDocumentosConsulta");
           tabla.innerHTML = contenido;
       }

       function mostrarMatrizConsultaPersona() {
       
           var nRegistros = matrizConsultaPersona.length;
           var contenidopersona = "";
           if (nRegistros > 0) {
               var nCampos = matrizConsultaPersona[0].length;
               for (var i = 0; i < nRegistros; i++) {
                   for (var j = 0; j < nCampos; j++) {
                       var Personaid
                       var NombrePersona
                       Personaid = '' + matrizConsultaPersona[i][0] + '';
                       NombrePersona = '' + matrizConsultaPersona[i][1] + '';
                       if (j == 0) {
                           contenidopersona += "<tr class='FilaDatos'>";
                           contenidopersona += "<td>";

                           contenidopersona += "<input id='btnmostrarpersona' type='button' src='../ImagenesForm/imgeditar.png' onclick='return MostrarPersona(";

                           contenidopersona += '"' + Personaid + '",';
                           contenidopersona += '"' + NombrePersona + '"';
                           contenidopersona += ")' >";
                           contenidopersona += "</td>";

                           contenidopersona += "<td>"
                           contenidopersona += matrizConsultaPersona[i][j];
                           contenidopersona += "</td>";
                       }

                       if (j > 0) {
                           contenidopersona += "<td>";
                           contenidopersona += matrizConsultaPersona[i][j];
                           contenidopersona += "</td>";
                       }

                   }
                   contenidopersona += "</tr>";
               }
           }

           var tablapersona = document.getElementById("tbDocumentosConsultaPersona");
           tablapersona.innerHTML = contenidopersona;
       }



       function crearMatrizConsultaPersona() {
           matrizConsultaPersona = [];
           var nRegistros = filas.length;
           var nCampos;
           var campos;
           var c = 0;
           var exito;
           var textos = document.getElementsByClassName("texto");
           var nTextos = textos.length;
           var texto;
           var x;
           for (var i = 0; i < nRegistros; i++) {
               exito = true;
               campos = filas[i].split("|");
               nCampos = campos.length;
               for (var j = 0; j < nTextos; j++) {
                   texto = textos[j];
                   x = j;

                   if (x < 15) {
                       if (texto.value.length > 0) {
                           if (campos[x] != campos[0]) {
                               exito = campos[x - 1].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                           }
                       }
                   }


                   if (!exito) break;
               }
               if (exito) {
                   matrizConsultaPersona[c] = [];
                   for (var j = 0; j < nCampos; j++) {

                       if (isNaN(campos[j])) matrizConsultaPersona[c][j] = campos[j];
                       else matrizConsultaPersona[c][j] = campos[j];
                   }
                   c++;
               }
           }
       }

      function crearMatrizConsulta() {
          matrizConsulta = [];
          var nRegistros = filas.length;
          var nCampos;
          var campos;
          var c = 0;
          var exito;
          var textos = document.getElementsByClassName("texto");
          var nTextos = textos.length;
          var texto;
          var x;
          for (var i = 0; i < nRegistros; i++) {
              exito = true;
              campos = filas[i].split("|");
              nCampos = campos.length;
              for (var j = 0; j < nTextos; j++) {
                  texto = textos[j];
                  x = j;

                  if (x < 15) {
                      if (texto.value.length > 0) {
                          if (campos[x] != campos[0]) {
                              exito = campos[x-1].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                          }
                      }
                  }


                  if (!exito) break;
              }
              if (exito) {
                  matrizConsulta[c] = [];
                  for (var j = 0; j < nCampos ; j++) {

                      if (isNaN(campos[j])) matrizConsulta[c][j] = campos[j];

                      if (isNaN(campos[j])) matrizConsulta[c][9] = parseFloat(campos[9]).toFixed(2);
                      if (isNaN(campos[j])) matrizConsulta[c][10] = parseFloat(campos[10]).toFixed(2);
                      if (isNaN(campos[j])) matrizConsulta[c][11] = parseFloat(campos[11]).toFixed(2);
                        
                      else matrizConsulta[c][j] = campos[j];
                  }
                  c++;
              }
          }
      }

      function cerrarVentana() {
          document.getElementById('CapaMantenimientoDetalle').style.display = 'none';
          return false;
      }


      function cerrarVentanaPersona() {
          document.getElementById('CapaBusquedaPersona').style.display = 'none';
          return false;
      }


      function MostrarPersona(idpersona, nombre) 
      {
        
          document.getElementById('txtcodigo').value = idpersona;
          document.getElementById('txtFiltroEmpleado').value = nombre;
          document.getElementById('CapaBusquedaPersona').style.display = 'none';
      }

      function MostrarDetalle(iddocumento, NroUnico, IdVendedor, FechaEntregavendedor, FechaEntLetFirmada, FechaEnvioBanco, FechaPendiente, FechaRenovacion, FechaProtesto, FechaAnulacion, FechaProgramado, FechaCancelacion, IdBanco, IdEstado, RecibosIngreso, FacturasRelacionadas, Nombre, CondicionLetra,codigo,observacion) {

          document.getElementById('CapaMantenimientoDetalle').style.display = 'block';
          document.getElementById('txtiddocumento').value = iddocumento;
          document.getElementById('TxtNroUnico').value = NroUnico;
          document.getElementById('txtcodigo').value = IdVendedor;
          document.getElementById('<%= TxtFechaEntregaVend.ClientID%>').value = FechaEntregavendedor;
          document.getElementById('<%= TxtFechaEntregaLetFirm.ClientID%>').value = FechaEntLetFirmada;
          document.getElementById('<%= TxtFechaEnvioBanco.ClientID%>').value = FechaEnvioBanco;
          document.getElementById('<%= TxtFechaRenovacion.ClientID%>').value = FechaRenovacion;
          document.getElementById('<%= TxtFechaProgramado.ClientID%>').value = FechaProgramado;
          document.getElementById('<%= TxtFechaProtesto.ClientID%>').value = FechaProtesto;
          document.getElementById('<%= TxtFechaCancelacion.ClientID%>').value = FechaCancelacion;
          document.getElementById('TxtReciboIngreso').value = RecibosIngreso;
          document.getElementById('cmbBanco').value=IdBanco;
          document.getElementById('cmbEstadoLetra').value = IdEstado;
          document.getElementById('cmbCondicionLetra').value = CondicionLetra;
          document.getElementById('txtFiltroEmpleado').value = Nombre;
          document.getElementById('TxtFacturasRelacionadas').value = FacturasRelacionadas;
          document.getElementById('TxtCodigoDoc').value = codigo;
          document.getElementById('TxtObservacion').value = observacion;
          
           

          return false;

      }

      function crearTablaConsulta() {
          var nRegistros = filas.length;

          var cabeceras = [" ", "IdDocumento", "doc_Serie", "doc_Codigo", "doc_FechaEmision", "doc_FechaVenc", "RUCODNI", "RazonSocial", "FacturasRelacionadas", "NroUnico", "doc_Total", "MontoCancelado", "Saldo", "Estado"];
          var nCabeceras = cabeceras.length;
          var contenido = "<table><thead>";
          contenido += "<tr class='GrillaHeader'>";

          for (var j = 0; j < nCabeceras; j++) {
              contenido += "<th>";
              contenido += "<input type='text' class='texto' style='width:95%' placeholder='" + cabeceras[j] + "'/>";
              contenido += "</th>";
          }

          contenido += "</tr></thead><tbody id='tbDocumentosConsulta' class='GrillaRow'></tbody>";
          contenido += "</table>";
          var div = document.getElementById("CapaRegistrosLetras");
          div.innerHTML = contenido;
      }

      function crearTablaConsultaPersona() {
          var nRegistros = filas.length;

          var cabeceras = [" ", "Id", "NombreEmpleado"];
          var nCabeceras = cabeceras.length;
          var contenido = "<table><thead>";
          contenido += "<tr>";

          for (var j = 0; j < nCabeceras; j++) {
              contenido += "<th>";
              contenido += "<input type='text' class='texto' style='width:95%' placeholder='" + cabeceras[j] + "'/>";
              contenido += "</th>";
          }

          contenido += "</tr></thead><tbody id='tbDocumentosConsultaPersona' class='GrillaRow'></tbody>";
          contenido += "</table>";
          var div = document.getElementById("Personas");
          div.innerHTML = contenido;
      }

  </script>

</asp:Content>
