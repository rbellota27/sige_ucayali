﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FrmConsultaLetras

    '''<summary>
    '''Control TxtFechaInicio.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents TxtFechaInicio As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtfecha_CalendarExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtfecha_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control TxtFechaFinal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents TxtFechaFinal As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control TxtFechaFinal_CalendarExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents TxtFechaFinal_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control TxtFechaEntregaVend.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents TxtFechaEntregaVend As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control TxtFechaEntregaVend_CalendarExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents TxtFechaEntregaVend_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control TxtFechaEntregaLetFirm.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents TxtFechaEntregaLetFirm As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control TxtFechaEntregaLetFirm_CalendarExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents TxtFechaEntregaLetFirm_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control TxtFechaEnvioBanco.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents TxtFechaEnvioBanco As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control TxtFechaEnvioBanco_CalendarExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents TxtFechaEnvioBanco_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control TxtFechaCancelacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents TxtFechaCancelacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control TxtFechaCancelacion_CalendarExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents TxtFechaCancelacion_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control TxtFechaRenovacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents TxtFechaRenovacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control TxtFechaRenovacion_calendarextender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents TxtFechaRenovacion_calendarextender As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control TxtFechaProgramado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents TxtFechaProgramado As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control TxtFechaProgramado_calendarextender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents TxtFechaProgramado_calendarextender As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control TxtFechaProtesto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents TxtFechaProtesto As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control TxtFechaProtesto_CalendarExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents TxtFechaProtesto_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender
End Class
