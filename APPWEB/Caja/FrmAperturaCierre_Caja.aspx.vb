﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmAperturaCierre_Caja
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Private objCbo As New Combo
    Private ListaCajaAperturaCierre As List(Of Entidades.Caja_AperturaCierre)

#Region "Propiedades"

    Private Property Lista_CajaAperturaCierre() As List(Of Entidades.Caja_AperturaCierre)
        Get
            Return CType(Session.Item("listaDetalleCaja"), List(Of Entidades.Caja_AperturaCierre))
        End Get
        Set(ByVal value As List(Of Entidades.Caja_AperturaCierre))
            Session.Remove("listaDetalleCaja")
            Session.Add("listaDetalleCaja", value)
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        valOnLoad_Frm()

    End Sub
    Private Sub valOnLoad_Frm()
        Try

            If (Not Me.IsPostBack) Then
                inicializarFrm()
                ValidarPermisos()
                valOnClick_btnAceptar_MovCaja()
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub inicializarFrm()

        With objCbo
            .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
            .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            .LlenarCboCajaxIdTiendaxIdUsuarioNew(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)
        End With

        Me.txtFechaMov.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")

        '**************** CARGAMOS LA GRILLA MONEDA

        Dim ListaMontoApertura As New List(Of Entidades.Moneda)
        ListaMontoApertura.AddRange((New Negocio.Moneda).SelectxId(1)) ' ************* SOLES
        ListaMontoApertura.AddRange((New Negocio.Moneda).SelectxId(2)) ' ************* DOLARES

        Me.GV_MontoApertura.DataSource = ListaMontoApertura
        Me.GV_MontoApertura.DataBind()

    End Sub
    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION / FECHA VCTO / REGISTRAR CLIENTE / EDITAR CLIENTE / BUSCAR DOC REFERENCIA / BUSCAR MAESTRO DE OBRA  / SOLO PANEL DE CANCELACION
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {256, 257, 258})

        Me.rdbOpcion_MovCaja.Items(0).Selected = False
        Me.rdbOpcion_MovCaja.Items(1).Selected = False
        Me.rdbOpcion_MovCaja.Items(2).Selected = False

        If listaPermisos(0) > 0 Then  '********* Apertura
            Me.rdbOpcion_MovCaja.Items(0).Enabled = True
        Else
            Me.rdbOpcion_MovCaja.Items(0).Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* Cierre
            Me.rdbOpcion_MovCaja.Items(1).Enabled = True
        Else
            Me.rdbOpcion_MovCaja.Items(1).Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* REGISTRAR
            Me.rdbOpcion_MovCaja.Items(2).Enabled = True
        Else
            Me.rdbOpcion_MovCaja.Items(2).Enabled = False
        End If
    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        valOnChange_cboEmpresa()
    End Sub
    Private Sub valOnChange_cboEmpresa()
        Try

            With objCbo
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LlenarCboCajaxIdTiendaxIdUsuarioNew(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)
            End With
            valOnClick_btnAceptar_MovCaja()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub cboCaja_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboCaja.SelectedIndexChanged
        onChange_cboCaja()
    End Sub
    Private Sub onChange_cboCaja()
        valOnClick_btnAceptar_MovCaja()
    End Sub

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        valOnChange_cboTienda()
    End Sub
    Private Sub valOnChange_cboTienda()
        Try

            With objCbo
                .LlenarCboCajaxIdTiendaxIdUsuarioNew(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)

            End With
            valOnClick_btnAceptar_MovCaja()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptar_MovCaja_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptar_MovCaja.Click
        valOnClick_btnAceptar_MovCaja()
    End Sub
    Private Sub valOnClick_btnAceptar_MovCaja()
        Try

            Me.Panel_CajaAperturaCierre.Visible = True
            Me.cboEmpresa.Enabled = False
            Me.cboTienda.Enabled = False
            Me.cboCaja.Enabled = False
            Me.btnAceptar_MovCaja.Enabled = False
            Me.txtFechaMov.Enabled = False


            Me.ListaCajaAperturaCierre = (New Negocio.Caja_AperturaCierre).Caja_AperturaCierre_Select(0, CInt(cboEmpresa.SelectedValue), CInt(cboTienda.SelectedValue), CInt(Me.cboCaja.SelectedValue), 0, Me.txtFechaMov.Text.Trim)
            Me.Lista_CajaAperturaCierre = Me.ListaCajaAperturaCierre

            If ListaCajaAperturaCierre.Count = 0 Then
                If Me.rdbOpcion_MovCaja.Items(0).Enabled = True Then ' No tiene apertura
                    Me.rdbOpcion_MovCaja.Items(0).Selected = True
                End If
            Else

                Me.Panel_CajaAperturaCierre.Visible = False

                Me.rdbOpcion_MovCaja.Items(0).Enabled = False ' la apertura es unica
                Me.rdbOpcion_MovCaja.Items(0).Selected = False
                Me.rdbOpcion_MovCaja.Items(2).Selected = False


                If Not IsDate(Me.ListaCajaAperturaCierre(0).cac_FechaCierre) Then

                    Me.rdbOpcion_MovCaja.Items(2).Enabled = False
                    Me.rdbOpcion_MovCaja.Items(2).Enabled = False

                    If Me.rdbOpcion_MovCaja.Items(1).Enabled = True Then ' tiene apertura x defecto debe cerrar
                        Me.rdbOpcion_MovCaja.Items(1).Selected = True
                    End If

                Else

                    If Me.rdbOpcion_MovCaja.Items(2).Enabled = True Then ' entonces desea reaperturar
                        Me.rdbOpcion_MovCaja.Items(2).Selected = True
                    End If

                End If
            End If

            GV_Detalle.DataSource = Me.ListaCajaAperturaCierre
            GV_Detalle.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub



    Protected Sub btnCancelar_Apertura_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancelar_Apertura.Click

        valOnClick_btnCancelar_Apertura()
        ValidarPermisos()
    End Sub
    Private Sub valOnClick_btnCancelar_Apertura()

        Try

            Me.cboEmpresa.Enabled = True
            Me.cboTienda.Enabled = True
            Me.cboCaja.Enabled = True
            Me.btnAceptar_MovCaja.Enabled = True
            Me.txtFechaMov.Enabled = True
            Me.Panel_CajaAperturaCierre.Visible = False
            If GV_MontoApertura.Rows.Count > 0 Then
                CType(GV_MontoApertura.Rows(0).FindControl("txtMonto"), TextBox).Text = CStr(Decimal.Zero)
            End If
            Me.GV_Detalle.DataBind()



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub



    Protected Sub btnRegistrar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRegistrar.Click
        Try
            onClick_RegistrarCajaAperturaCierre()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub onClick_RegistrarCajaAperturaCierre()

        Dim negCaja_AperturaCierre As New Negocio.Caja_AperturaCierre
        Me.Actualizar_ListaListaCajaAperturaCierre()


        Me.ListaCajaAperturaCierre = Me.Lista_CajaAperturaCierre
        negCaja_AperturaCierre.Caja_AperturaCierre_Transaction(Me.ListaCajaAperturaCierre)

        valOnClick_btnAceptar_MovCaja()

        objScript.mostrarMsjAlerta(Me, "La operación finalizó con éxito.")

    End Sub

    Private Sub Actualizar_ListaListaCajaAperturaCierre()

        Me.ListaCajaAperturaCierre = Me.Lista_CajaAperturaCierre
        If IsNothing(Me.ListaCajaAperturaCierre) Then Me.ListaCajaAperturaCierre = New List(Of Entidades.Caja_AperturaCierre)

        If Me.ListaCajaAperturaCierre.Count > 0 Then

            For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1

                Me.ListaCajaAperturaCierre(i).cac_ControlCaja = Me.rdbOpcion_MovCaja.SelectedValue

                Select Case Me.ListaCajaAperturaCierre(i).cac_ControlCaja

                    Case "A" '  REAPERTURA                         

                        Me.ListaCajaAperturaCierre(i).cac_MontoApertura = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtcac_MontoApertura"), TextBox).Text)

                    Case "C" ' CIERRE

                        If Not IsDate(CType(Me.GV_Detalle.Rows(i).FindControl("txtcac_FechaCierre"), TextBox).Text) Then
                            Me.ListaCajaAperturaCierre(i).cac_FechaCierre = (New Negocio.FechaActual).SelectFechaActual
                        Else
                            Me.ListaCajaAperturaCierre(i).cac_FechaCierre = CDate(CType(Me.GV_Detalle.Rows(i).FindControl("txtcac_FechaCierre"), TextBox).Text)
                        End If


                        Me.ListaCajaAperturaCierre(i).cac_MontoCierre = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtcac_MontoCierre"), TextBox).Text)

                End Select

            Next

        Else

            ' APERTURA

            Dim obj As Entidades.Caja_AperturaCierre

            For Each row As GridViewRow In Me.GV_MontoApertura.Rows

                obj = New Entidades.Caja_AperturaCierre

                With obj

                    .IdCaja = CInt(cboCaja.SelectedValue)
                    .IdUsuario = CInt(Session("IdUsuario"))
                    .cac_FechaApertura = CDate(Me.txtFechaMov.Text)
                    .IdMoneda = CInt(CType(row.FindControl("hddIdMoneda"), HiddenField).Value)
                    .cac_MontoApertura = CDec(CType(row.FindControl("txtMonto"), TextBox).Text)
                    .cac_ControlCaja = Me.rdbOpcion_MovCaja.SelectedValue

                End With

                Me.ListaCajaAperturaCierre.Add(obj)
                obj = Nothing

            Next

        End If



        Me.Lista_CajaAperturaCierre = Me.ListaCajaAperturaCierre

    End Sub





    Private Sub GV_Detalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Detalle.RowDataBound
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then

                Select Case Me.ListaCajaAperturaCierre(e.Row.RowIndex).cac_ControlCaja

                    Case "A"

                    Case "C"

                End Select

            End If

        Catch ex As Exception

        End Try
    End Sub



End Class