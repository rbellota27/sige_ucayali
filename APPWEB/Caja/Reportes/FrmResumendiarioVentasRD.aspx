<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmResumendiarioVentasRD.aspx.vb" Inherits="APPWEB.FrmResumendiarioVentasRD" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
            <table style="width: 100%">
                <tr>
                    <td align="center" class="TituloCelda">
                    
                        Resumen Diario de Ventas</td>
                </tr>
                <tr>
                    <td>
                    
                        <asp:UpdatePanel ID="UpdatePanel_EmpresaTienda" runat="server">
                        <ContentTemplate>
                        <table>
                            <tr>
                                <td class="Label">
                        Empresa:
                        <asp:DropDownList ID="cmbEmpresa" runat="server" AutoPostBack="false">
                        </asp:DropDownList>
                        Tienda:
                        <asp:DropDownList ID="cboTienda" runat="server" >
                        </asp:DropDownList>
                                </td>
                                <td>
                                <asp:CheckBox  ID ="chbsResumen"  Text ="Solo Resumen"  runat ="server" CssClass ="Label" />
                                </td>
                            </tr>

                        </table>
                        </ContentTemplate>
                        </asp:UpdatePanel>                        
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="Label">
                                    Fecha Inicio:</td>
                                <td>                                
                                    <asp:TextBox ID="txtFechaInicio" CssClass="TextBoxReadOnly" runat="server"></asp:TextBox>                                
                                    <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" 
                                     TargetControlID="txtFechaInicio">
                                    </cc1:CalendarExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="Label">
                                Fecha Fin:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechaFin" CssClass="TextBoxReadOnly" runat="server"></asp:TextBox>
                                    <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" 
                                            TargetControlID="txtFechaFin">
                                    </cc1:CalendarExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                        <asp:ImageButton ID="btnAceptar" runat="server" 
                            ImageUrl="~/Imagenes/Aceptar_B.JPG" OnClientClick="return(verReporte());"
                            onmouseout="this.src='../../Imagenes/Aceptar_B.JPG';" 
                            onmouseover="this.src='../../Imagenes/Aceptar_A.JPG';" />
                                </td>
                            </tr>
                            </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1130px"></iframe>
                    </td>
                </tr>
                </table>
 
 <script  language ="javascript"  type ="text/javascript" >

     function verReporte() {
         var check = document.getElementById('<%= chbsResumen.ClientID%>');
         var resumen = 0;         
         if (check.checked==true ) {
             resumen = 1;
         }else{
         resumen = 0;
         }         
         var idempresa = document.getElementById('<%= cmbEmpresa.ClientID%>').value;
         var idTienda = document.getElementById('<%= cboTienda.ClientID%>').value;
         var fechaInicio = document.getElementById('<%= txtFechaInicio.ClientID%>').value;
         var fechaFin = document.getElementById('<%= txtFechaFin.ClientID%>').value;
         frame1.location.href = '../../Caja/Reportes/Visor1.aspx?iReporte=3&FechaInicio=' + fechaInicio + '&FechaFin=' + fechaFin + '&IdEmpresa=' + idempresa + '&IdTienda=' + idTienda +'&Resumen='+resumen ; 
         return  false  ;
     }
 
 </script>
</asp:Content>
