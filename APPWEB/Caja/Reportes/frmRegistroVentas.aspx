<%@ Page Title="" Language="vb" AutoEventWireup="false"   MasterPageFile="~/Principal.Master"   CodeBehind="frmRegistroVentas.aspx.vb" Inherits="APPWEB.frmRegistroVentas" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
            <table style="width: 100%">
                <tr>
                    <td align="center" class="TituloCelda">
                    
                        Registro de Ventas</td>
                </tr>
                <tr>
                    <td>
                    
                        <asp:UpdatePanel ID="UpdatePanel_EmpresaTienda" runat="server">
                        <ContentTemplate>
                        <table>
                            <tr>
                                <td class="Label">
                        Empresa:
                        <asp:DropDownList ID="cmbEmpresa" runat="server" AutoPostBack="false">
                        </asp:DropDownList>
                        Tienda:
                        <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="false" Enabled="true">
                        </asp:DropDownList>
                                </td>
                              <td class="Label">
                                <asp:RadioButtonList ID="rbtlistPerc" runat="server" 
                                  RepeatDirection ="Horizontal" AutoPostBack="True" >
                                  <asp:ListItem Value="0" Selected ="True">Todos</asp:ListItem >
                                  <asp:ListItem Value="1" >Con Percepcion</asp:ListItem>
                                  </asp:RadioButtonList>
                              </td>
                              <td  > 
                                  <asp:CheckBox ID="CheckBox1" runat="server" Text ="Por Cliente" class="Label"/>
                              </td>
                            </tr>
                            <tr >
                            <td colspan ="2" class="Label" align ="left" >
                            
                            </td>
                            <td align ="left" > 
                            <asp:CheckBox ID="CheckBox2" runat="server"  Text ="En  Soles"  Checked ="true" class="Label"   />
                            </td>
                            </tr>
                        </table>
                        </ContentTemplate>
                        </asp:UpdatePanel>                        
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="Label">
                                    Fecha Inicio:</td>
                                <td>                                
                                    <asp:TextBox ID="txtFechaInicio" CssClass="TextBoxReadOnly" runat="server"></asp:TextBox>                                
                                    <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" 
                                            TargetControlID="txtFechaInicio">
                                    </cc1:CalendarExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="Label">
                                Fecha Fin:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechaFin" CssClass="TextBoxReadOnly" runat="server"></asp:TextBox>
                                    <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" 
                                            TargetControlID="txtFechaFin">
                                    </cc1:CalendarExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                            <asp:ImageButton ID="imgbtnAceptar" runat="server" 
                            ImageUrl="~/Imagenes/Aceptar_B.JPG" OnClientClick="return(mostrarReporte(this));"
                            onmouseout="this.src='../../Imagenes/Aceptar_B.JPG';" 
                            onmouseover="this.src='../../Imagenes/Aceptar_A.JPG';" />
                                </td>
                            </tr>
                            </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1130px"></iframe>
                    </td>
                </tr>
                </table>
            <script language="javascript" type="text/javascript">

                function disable(button) {
                    setTimeout(function () {
                        if (Page_IsValid)
                            button.disabled = true;
                    }, 0);
                }

                function mostrarReporte(b) {
                    var fechaInicio = document.getElementById('<%= txtFechaInicio.ClientID%>').value;
                    var fechaFin = document.getElementById('<%= txtFechaFin.ClientID%>').value;
                    var IdEmpresa = document.getElementById('<%= cmbEmpresa.ClientID%>').value;
                    var IdTienda = document.getElementById('<%= cboTienda.ClientID%>').value;
                    var rdbList = document.getElementById('<%= rbtlistPerc.ClientID%>');
                    var boton = document.getElementById('#btnImprimir');

                    var TodosPercep = 0;
                    var radio = rdbList.getElementsByTagName("INPUT");
                    //alert(radio[0].checked);
                    if (radio[0].checked == true) {
                        TodosPercep = 0;
                    } else if (radio[1].checked == true) {
                        TodosPercep = 1;
                    } else {
                        TodosPercep = 0;
                    }
                    var check = document.getElementById('<%= CheckBox1.ClientID%>')

                    var clientes = 0;
                    if (check.status == true) {
                        var clientes = 0;
                    }
                    else {
                        var clientes = 1;
                    }
                    
                    var checkSol = document.getElementById('<%=CheckBox2.ClientID%>')
                    if (checkSol.status == true) {
                        var monBase = 1;
                    }
                    else {
                        var monBase = 0;
                    }

                    var NomEmpresa = '';
                    var NomTienda = '';

                    var cbo = document.getElementById('<%= cmbEmpresa.ClientID%>');
                    for (var i = 0; i < cbo.length; i++) {
                        if (IdEmpresa == cbo[i].value) {
                            NomEmpresa = cbo[i].text;
                            break;
                        }
                    }
                    cbo = document.getElementById('<%= cboTienda.ClientID%>');
                    for (var i = 0; i < cbo.length; i++) {
                        if (IdTienda == cbo[i].value) {
                            NomTienda = cbo[i].text;
                            break;
                        }
                    }

                    function fnchecktrue() {
                        document.getElementById('<%=CheckBox2.ClientID%>').status == true
                    }
                    frame1.location.href = '../../Reportes/visor.aspx?iReporte=13&FechaInicio=' + fechaInicio + '&FechaFin=' + fechaFin + '&IdEmpresa=' + IdEmpresa + '&IdTienda=' + IdTienda + '&NomEmpresa=' + NomEmpresa + '&NomTienda=' + NomTienda + '&Percepcion=' + TodosPercep + '&Cliente=' + clientes + '&monBase=' + monBase;                    
                    return false;
                }
                </script>
</asp:Content>
