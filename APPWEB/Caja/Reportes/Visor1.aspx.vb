﻿Imports CrystalDecisions.CrystalReports.Engine

Public Class Visor11
    Inherits System.Web.UI.Page
    Dim reporte As New ReportDocument
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Dim iReporte As String = Request.QueryString("iReporte")

            If iReporte Is Nothing Then
                iReporte = ""
            End If
            ViewState.Add("iReporte", iReporte)
            Select Case iReporte
                Case "1" '*** Recibo Ingreso
                    ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
                Case "2" '*** Recibo Egreso
                    ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
                Case "3"
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdTienda", Request.QueryString("IdTienda"))
                    ViewState.Add("Resumen", Request.QueryString("Resumen"))
                Case "4"
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdTienda", Request.QueryString("IdTienda"))
                    ViewState.Add("NomEmpresa", Request.QueryString("NomEmpresa"))
                    ViewState.Add("NomTienda", Request.QueryString("NomTienda"))
                    ViewState.Add("idcliente", Request.QueryString("idcliente"))
                    ViewState.Add("tipo", Request.QueryString("tipo"))
                    ViewState.Add("uit", Request.QueryString("uit"))
                    ViewState.Add("doc", Request.QueryString("doc"))
                Case "5", "6", "7" 'Cheque Egreso 'Letras' 'Caja 'cancelbancos
                    ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
                Case "19" 'Cheque Egreso
                    ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
                    ViewState.Add("Banco", Request.QueryString("Banco"))
                    ViewState.Add("moneda", Request.QueryString("moneda"))
                Case "20" 'Cheque Egreso
                    ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
                    ViewState.Add("Banco", Request.QueryString("Banco"))
                Case "21" 'Cheque Egreso BCP otro formato
                    ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
                    ViewState.Add("Banco", Request.QueryString("Banco"))
                    ViewState.Add("moneda", Request.QueryString("moneda"))
                    ViewState.Add("Formato", Request.QueryString("Formato"))
                    ' Formato = CStr(ViewState.Item("Formato"))
                Case "51" 'cancelbancos'
                    ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
                Case "8" 'Vista Previa'
                    ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
                Case "9" 'Impresion'
                    ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
            End Select
        End If

        mostrarReporte()
    End Sub
    Private Sub mostrarReporte()
        Dim objDocMer As New Negocio.DocumentosMercantiles

        Select Case ViewState.Item("iReporte").ToString
            Case "1"
                ReporteDocReciboIngreso()
            Case "2"
                ReporteDocReciboEgreso()
            Case "3"
                getMetdos()
            Case "4"
                ReporteDAOT()
            Case "5"
                ReporteChequeEgreso()
            Case "19"
                ReporteChequeEgreso()
            Case "6"
                ReporteLetras()
            Case "7"
                ReporteDocCancelCaja()
            Case "51"
                Dim idImp As Integer = objDocMer.crpImprimir(51)
                reportedoccancelbancos(idImp)
            Case "8"
                ReporteRequeGasto()
            Case "9"
                ReporteRequeGastoImpr()
            Case "20"
                ReporteChequeEgresoVoucher()
            Case "21"
                ReporteChequeEgreso()
        End Select
    End Sub
    ''AGREGADO PARA EL REPORTE DE REQUERIMIENTO DE GASTO IMPRESIÓN
    Private Sub ReporteRequeGastoImpr()
        Try
            reporte = New CR_RequeGastoV22
            reporte.PrintOptions.Dispose()
            reporte.SetDataSource((New Negocio.RequerimientoGasto).getDataSet_DocRequeGastoV2Impre(CInt(ViewState.Item("IdDocumento"))))
            CRViewer.ReportSource = reporte
        Catch ex As Exception
            Response.Write("<script>alert('" + ex.Message.Replace(vbCrLf, "").Replace(vbCr, "").Replace("'", "").Replace("""", "") + "');</script>")
        End Try
    End Sub
    ''AGREGADO PARA EL REPORTE DE REQUERIMIENTO DE GASTO VISTA PREVIA
    Private Sub ReporteRequeGasto()
        Try
            reporte = New CR_RequeGastoV22
            reporte.PrintOptions.Dispose()
            reporte.SetDataSource((New Negocio.RequerimientoGasto).getDataSet_DocRequeGastoV2(CInt(ViewState.Item("IdDocumento"))))
            CRViewer.ReportSource = reporte
            CRViewer.HasPrintButton = False
            CRViewer.HasExportButton = False
        Catch ex As Exception
            Response.Write("<script>alert('" + ex.Message.Replace(vbCrLf, "").Replace(vbCr, "").Replace("'", "").Replace("""", "") + "');</script>")
        End Try
    End Sub
    Private Sub ReporteDAOT()
        Try
            Dim vidempresa, tipo, doc As Integer
            Dim tienda, empresa As String
            Dim uit As Decimal
            vidempresa = CInt(ViewState.Item("IdEmpresa"))
            tienda = CStr(ViewState.Item("NomTienda"))
            empresa = CStr(ViewState.Item("NomTienda"))
            tipo = CInt(ViewState.Item("tipo"))
            uit = CDec(ViewState.Item("uit"))
            doc = CInt(ViewState.Item("doc"))

            Dim ds As New DataSet
            Dim objReporte As New Negocio.Cliente
            ds = objReporte.ReporteDetalleDAOT(CInt(ViewState("IdEmpresa")), CInt(ViewState.Item("IdTienda")), CInt(ViewState.Item("idcliente")), CStr(ViewState("FechaInicio")), CStr(ViewState("FechaFin")), tipo, uit, doc)

            If (tipo = 1) Then
                reporte = New CR_DetalladoDAOT
                reporte.SetDataSource(ds)
                reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
                reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
                reporte.SetParameterValue("@NomTienda", ViewState("NomTienda"))
                reporte.SetParameterValue("@NomEmpresa", ViewState("NomEmpresa"))
                CRViewer.ReportSource = reporte
                CRViewer.DataBind()

            Else
                reporte = New CR_ResumidoDAOT
                reporte.SetDataSource(ds)
                reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
                reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
                reporte.SetParameterValue("@NomTienda", ViewState("NomTienda"))
                reporte.SetParameterValue("@NomEmpresa", ViewState("NomEmpresa"))
                CRViewer.ReportSource = reporte
                CRViewer.DataBind()

            End If

        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub getMetdos()
        Dim soloResumen As Integer = CInt(ViewState.Item("Resumen"))
        If soloResumen = 1 Then
            reporteIngresoxTienda()
        Else
            ReporteResumenDiarioVentas()
        End If
    End Sub
    Private Sub ReporteDocReciboIngreso()
        Try
            reporte = New CR_ReciboIngreso
            Dim objReporte As New Negocio.Reportes
            reporte.SetDataSource(objReporte.getDataSetReciboIngreso(CInt(ViewState.Item("IdDocumento"))))
            CRViewer.ReportSource = reporte
        Catch ex As Exception
            Response.Write("<script>alert('" + ex.Message.Replace(vbCrLf, "").Replace(vbCr, "").Replace("'", "").Replace("""", "") + "');</script>")
        End Try
    End Sub

    Private Sub ReporteDocReciboEgreso()
        Try
            reporte = New CR_DocReciboEgreso
            reporte.SetDataSource((New Negocio.DocReciboCaja).getDataSet_DocReciboEgreso(CInt(ViewState.Item("IdDocumento"))))
            CRViewer.ReportSource = reporte
        Catch ex As Exception
            Response.Write("<script>alert('" + ex.Message.Replace(vbCrLf, "").Replace(vbCr, "").Replace("'", "").Replace("""", "") + "');</script>")
        End Try
    End Sub

    Private Sub ReporteResumenDiarioVentas()
        Try
            'Dim Reporte As New CR_ReporteResumenDiarioVentas
            reporte = New CR_RptResumenDiarioVentas
            Dim objReporte As New Negocio.Reportes
            Dim dttable As DataTable

            dttable = objReporte.getResumendiarioVentas(CInt(ViewState.Item("IdEmpresa")), CInt(ViewState.Item("IdTienda")), CStr(ViewState.Item("FechaInicio")), CStr(ViewState.Item("FechaFin"))).Tables("DT_ResumenDiarioVentas")

            'If dttable.Rows.Count > 0 Then
            'Dim fila As String = ""
            'For Each row As DataRow In dttable.Rows
            '    fila = row("Doc_FechaEmision").ToString
            'Next
            'Dim coll As DataColumn = Nothing
            'For Each col As DataColumn In dttable.Columns
            '    coll.ColumnName(0) = col.ColumnName(0)
            'Next
            'Reporte.SetDataSource(objReporte.getResumendiarioVentas(CInt(ViewState.Item("IdEmpresa")), CInt(ViewState.Item("IdTienda")), CStr(ViewState.Item("FechaInicio")), CStr(ViewState.Item("FechaFin"))))

            Reporte.SetDataSource(dttable)
            Reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
            Reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
            CRViewer.ReportSource = Reporte
            CRViewer.DataBind()
            'Else
            'Response.Write("<script>alert('no vienen  datos.');</script>")
            'End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub reporteIngresoxTienda()
        Try
            Dim vidempresa As Integer
            vidempresa = CInt(ViewState.Item("IdEmpresa"))

            Dim ds As New DataSet
            Dim objReporte As New Negocio.Cliente
            ds = objReporte.ReporteIngresosxTienda(CInt(ViewState("IdEmpresa")), CInt(ViewState.Item("IdTienda")), CStr(ViewState("FechaInicio")), CStr(ViewState("FechaFin")))
            reporte = New CR_IngresoxTienda
            reporte.SetDataSource(ds)
            reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
            reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
            CRViewer.ReportSource = reporte
            CRViewer.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub


    Private Sub ReporteChequeEgresoVoucher()
        Try
            Dim IdDocumento As Integer = 0
            IdDocumento = CInt(ViewState.Item("IdDocumento"))

            Dim objReporte As New Negocio.Reportes

            If (IdDocumento <> 0) Then
                reporte = New CR_ChequeVoucher
                reporte.SetDataSource(objReporte.getDataSetChequeImpresion(CInt(ViewState.Item("IdDocumento"))))
                CRViewer.ReportSource = reporte
            End If

        Catch ex As Exception
            Response.Write("<script>alert('" + ex.Message.Replace(vbCrLf, "").Replace(vbCr, "").Replace("'", "").Replace("""", "") + "');</script>")
        End Try
    End Sub

    Private Sub ReporteChequeEgresoVoucherV2()
        Try
            Dim IdDocumento As Integer = 0
            IdDocumento = CInt(ViewState.Item("IdDocumento"))

            Dim objReporte As New Negocio.Reportes

            If (IdDocumento <> 0) Then
                reporte = New CR_ChequeVoucherF2
                reporte.SetDataSource(objReporte.getDataSetChequeImpresionV2(CInt(ViewState.Item("IdDocumento"))))
                CRViewer.ReportSource = reporte
            End If

        Catch ex As Exception
            Response.Write("<script>alert('" + ex.Message.Replace(vbCrLf, "").Replace(vbCr, "").Replace("'", "").Replace("""", "") + "');</script>")
        End Try
    End Sub

    Private Sub ReporteChequeEgreso()
        Try
            Dim NombreBanco As String = ""
            Dim moneda As String = ""
            Dim IdFormato As String = ""
            NombreBanco = CStr(ViewState.Item("Banco"))
            moneda = CStr(ViewState.Item("moneda"))
            IdFormato = CStr(ViewState.Item("Formato"))
            ''Reporte Básico de Impresión
            'Dim Reporte As New CR_Cheque
            Dim objReporte As New Negocio.Reportes


            If (NombreBanco = "BANCO CONTINENTAL" And moneda.Trim = "US$" And IdFormato <> "2") Then
                reporte = New CR_ChequeBBVA
                reporte.SetDataSource(objReporte.getDataSetChequeImpresion(CInt(ViewState.Item("IdDocumento"))))
                CRViewer.ReportSource = reporte

            ElseIf (NombreBanco = "BANCO CONTINENTAL" And moneda.Trim = "S/." And IdFormato <> "2") Then
                reporte = New CR_ChequeBBVA_Soles
                reporte.SetDataSource(objReporte.getDataSetChequeImpresion(CInt(ViewState.Item("IdDocumento"))))
                CRViewer.ReportSource = reporte


            ElseIf (NombreBanco = "BANCO CONTINENTAL" And moneda.Trim = "US$" And IdFormato = "2") Then
                reporte = New CR_ChequeBBVAF2
                reporte.SetDataSource(objReporte.getDataSetChequeImpresionV2(CInt(ViewState.Item("IdDocumento"))))
                CRViewer.ReportSource = reporte
            ElseIf (NombreBanco = "BANCO CONTINENTAL" And moneda.Trim = "S/." And IdFormato = "2") Then
                reporte = New CR_ChequeBBVA_SolesF2
                reporte.SetDataSource(objReporte.getDataSetChequeImpresionV2(CInt(ViewState.Item("IdDocumento"))))
                CRViewer.ReportSource = reporte

            ElseIf (NombreBanco = "BANCO DE CREDITO DEL PERU" And IdFormato <> "2") Then
                reporte = New CR_ChequeBCP
                reporte.SetDataSource(objReporte.getDataSetChequeImpresion(CInt(ViewState.Item("IdDocumento"))))
                CRViewer.ReportSource = reporte
            ElseIf (NombreBanco = "BANCO DE CREDITO DEL PERU") And (IdFormato = "2") Then
                reporte = New CR_ChequeBCPF2
                reporte.SetDataSource(objReporte.getDataSetChequeImpresionV2(CInt(ViewState.Item("IdDocumento"))))
                CRViewer.ReportSource = reporte
            ElseIf (NombreBanco = "SCOTIABANK PERU") And (IdFormato <> "2") Then
                reporte = New CR_ChequeSCO
                reporte.SetDataSource(objReporte.getDataSetChequeImpresion(CInt(ViewState.Item("IdDocumento"))))
                CRViewer.ReportSource = reporte

            ElseIf (NombreBanco = "SCOTIABANK PERU") And (IdFormato = "2") Then
                reporte = New CR_ChequeSCOF2
                reporte.SetDataSource(objReporte.getDataSetChequeImpresion(CInt(ViewState.Item("IdDocumento"))))
                CRViewer.ReportSource = reporte
            End If
            'Reporte.SetDataSource(objReporte.getDataSetChequeImpresion(CInt(ViewState.Item("IdDocumento"))))
            'CRViewer.ReportSource = Reporte
        Catch ex As Exception
            Response.Write("<script>alert('" + ex.Message.Replace(vbCrLf, "").Replace(vbCr, "").Replace("'", "").Replace("""", "") + "');</script>")
        End Try
    End Sub
    Private Sub reportedoccancelbancos(ByVal tipoimpresion As Integer)
        Try
            Dim iddocumento As Integer = CInt(ViewState("IdDocumento"))
            Dim reporte As New ReportDocument
            Dim objreporte As New Negocio.Reportes
            Select Case tipoimpresion

                Case 1 Or 0 'estandar
                    reporte = New CR_DocCancelBancos
                    reporte.SetDataSource(objreporte.getDataSetDocCancelBancos(CInt(ViewState.Item("IdDocumento"))))
                    CRViewer.ReportSource = reporte
                Case 2 'personalizado
                    reporte = New CR_DocEgresos
                    reporte.SetDataSource(objreporte.getDataSetDocEgresos(CInt(ViewState.Item("IdDocumento"))))
                    CRViewer.ReportSource = reporte
            End Select
        Catch ex As Exception
            Response.Write("<script>alert('problemas en la carga de datos.');</script>")
        End Try
    End Sub
    'Private Sub ReporteDocCancelBancos()
    '    Try
    '        Dim reporte As New CR_DocCancelBancos
    '        Dim objReporte As New Negocio.Reportes
    '        reporte.SetDataSource(objReporte.getDataSetDocCancelBancos(CInt(ViewState.Item("IdDocumento"))))
    '        CRViewer.ReportSource = reporte
    '    Catch ex As Exception
    '        Response.Write("<script>alert('" + ex.Message.Replace(vbCrLf, "").Replace(vbCr, "").Replace("'", "").Replace("""", "") + "');</script>")
    '    End Try
    'End Sub
    Private Sub ReporteLetras()
        Try
            reporte = New CR_LetrasStd
            Dim objReporte As New Negocio.DocLetra
            reporte.SetDataSource(objReporte.getDataSet_CR_DocumentoLetraCab(CInt(ViewState.Item("IdDocumento"))))
            CRViewer.ReportSource = reporte
        Catch ex As Exception
            Response.Write("<script>alert('" + ex.Message.Replace(vbCrLf, "").Replace(vbCr, "").Replace("'", "").Replace("""", "") + "');</script>")
        End Try
    End Sub
    Private Sub ReporteDocCancelCaja()
        Try
            reporte = New CR_DocCancelCaja
            Dim objReporte As New Negocio.Reportes
            reporte.SetDataSource(objReporte.getDataSetDocCancelCaja(CInt(ViewState.Item("IdDocumento"))))
            CRViewer.ReportSource = reporte
        Catch ex As Exception
            Response.Write("<script>alert('" + ex.Message.Replace(vbCrLf, "").Replace(vbCr, "").Replace("'", "").Replace("""", "") + "');</script>")
        End Try
    End Sub

    Private Sub Visor11_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        reporte.Close()
        reporte.Dispose()
    End Sub
End Class