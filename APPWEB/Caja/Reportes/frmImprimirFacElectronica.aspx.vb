﻿Imports System.Net

Public Class frmImprimirFacElectronica
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim idSerie As Integer = 0
        Dim codigo As String = ""
        Dim idTipodocumento As Integer = 0
        idSerie = Request.QueryString("idSerie")
        codigo = Request.QueryString("codigo")
        idTipodocumento = Request.QueryString("idtipoDocumento")

        Dim path As String = ""
        Dim objetoPath As New Negocio.DocumentosMercantiles
        Dim idDocumento As Integer
        path = objetoPath.pathFacturaElectronica("O", idSerie, codigo, idTipodocumento)

        If path <> "" Then
            'Dim FilePath As String = Server.MapPath("javascript1-sample.pdf")
            Dim User As New WebClient()
            Dim FileBuffer As [Byte]() = User.DownloadData(path)
            If FileBuffer IsNot Nothing Then
                Response.ContentType = "application/pdf"
                Response.AddHeader("content-length", FileBuffer.Length.ToString())
                Response.BinaryWrite(FileBuffer)
                Response.Write("<script>")
                Response.Write("window.open('" & path & "','_blank')")
                Response.Write("</script>")
            End If
        Else
            objScript.mostrarMsjAlerta(Me, "No se encontró el archivo de facturación electrónica, intente en 10 segundos. Si el error persiste comunicarse con el área de sistemas.")
        End If
    End Sub

End Class