﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmReciboIngreso
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass

    Private ListaCancelacion As List(Of Entidades.PagoCaja)
    Private ListaDetalleConcepto As List(Of Entidades.DetalleConcepto)
    Private listaDocumentoRef As List(Of Entidades.Documento)

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
        AperturaCaja = 7
    End Enum

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Frm_OnLoad()
    End Sub

    Private Sub Frm_OnLoad()
        Try
            If (Not Me.IsPostBack) Then
                ConfigurarDatos()
                inicializarFrm()
                validarRequest()

                Validar_AperturaCierreCaja(False)

            End If
            visualizarMoneda()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub validarRequest()

        If (Request.QueryString("IdDocumentoRef") IsNot Nothing) Then

            Dim IdDocumentoRef As Integer = CInt(Request.QueryString("IdDocumentoRef"))
            cargarDocumentoReferencia(IdDocumentoRef)

        End If

    End Sub

    Private Sub visualizarMoneda()
        '************ Mostramos las Monedas
        Me.lblMonedaFaltante.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMonedaTotalAPagar.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMonedaTotalRecibido.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMonedaVuelto.Text = Me.cboMoneda.SelectedItem.ToString
    End Sub


    Private Sub inicializarFrm()
        Dim objCbo As New Combo
        With objCbo

            .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)

            .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)

            .LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)

            .LLenarCboEstadoDocumento(Me.cboEstado)
            .LlenarCboMoneda(Me.cboMoneda)
            .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.hddIdTipoDocumento.Value), True)

            '**************** DATOS DE CANCELACION
            .LlenarCboMedioPagoxIdTipoDocumento(Me.cboMedioPago, CInt(Me.hddIdTipoDocumento.Value))
            .LlenarCboBanco(Me.cboBanco, False)
            .LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)
            .LlenarCboMoneda(Me.cboMoneda_DatoCancelacion, False)

            ' cboPost_DC se llena el tipo de tarjeta no el POS fue cambiado el 19/05/2011
            '.LlenarCboPostxIdCaja(Me.cboPost_DC, CInt(Me.cboCaja.SelectedValue), False)
            .LlenarCboTipoTarjetaxIdCaja(Me.cboPost_DC, CInt(Me.cboCaja.SelectedValue), False)

            '---llenar TipoDocumento---
            .LLenarCboTipoDocumentoRefxIdTipoDocumento(CboTipoDocumento, CInt(hddIdTipoDocumento.Value), 2, False)


            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.txtFechaACobrar.Text = Me.txtFechaEmision.Text

            '*************** MEDIO DE PAGO PRINCIPAL
            Me.hddIdMedioPagoPrincipal.Value = CStr((New Negocio.MedioPago).SelectIdMedioPagoPrincipal())

            Me.txtFechaInicio_DocRef.Text = Me.txtFechaEmision.Text
            Me.txtFechaFin_DocRef.Text = Me.txtFechaEmision.Text
            actualizarOpcionesBusquedaDocRef(1, False)

            actualizarControlesMedioPago(1, CInt(Me.cboMedioPago.SelectedValue))

            ValidarPermisos()
            verFrm(FrmModo.Nuevo, True, True, True, True, True, True)

        End With

        actualizarOpcionesBusquedaDocRef(1, False)

    End Sub

    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION 
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {39, 40, 41, 42, 43})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumentoxCodigo.Enabled = True
        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumentoxCodigo.Enabled = False
        End If

        If listaPermisos(4) > 0 Then  '********* FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

    End Sub

    Private Function getListaCancelacion() As List(Of Entidades.PagoCaja)
        Return CType(Session.Item("ListaCancelacion"), List(Of Entidades.PagoCaja))
    End Function

    Private Sub setListaCancelacion(ByVal lista As List(Of Entidades.PagoCaja))
        Session.Remove("ListaCancelacion")
        Session.Add("ListaCancelacion", lista)
    End Sub

    Private Function getlistaDocumentoRef() As List(Of Entidades.Documento)
        Return CType(Session.Item("listaDocumentoRef"), List(Of Entidades.Documento))
    End Function
    Private Sub setlistaDocumentoRef(ByVal lista As List(Of Entidades.Documento))
        Session.Remove("listaDocumentoRef")
        Session.Add("listaDocumentoRef", lista)
    End Sub

    Private Sub GenerarCodigoDocumento()
        Try

            Dim objCombo As New Combo
            objCombo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))

            If (IsNumeric(Me.cboSerie.SelectedValue)) Then
                Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
            Else
                Me.txtCodigoDocumento.Text = ""
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try


            Dim objCbo As New Combo

            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)


            GenerarCodigoDocumento()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged


        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)


            GenerarCodigoDocumento()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        Try
            If (IsNumeric(Me.cboSerie.SelectedValue)) Then
                Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
            Else
                Me.txtCodigoDocumento.Text = ""
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


#Region "****************Buscar Personas Mantenimiento"
    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            Me.GV_Deudas.DataSource = Nothing
            Me.GV_Deudas.DataBind()

            Me.GV_Cancelacion.DataSource = Nothing
            Me.GV_Cancelacion.DataBind()

            Me.ListaCancelacion = New List(Of Entidades.PagoCaja)
            setListaCancelacion(Me.ListaCancelacion)

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "   offCapa('capaPersona');    calcularDatosCancelacion();    ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)

        Dim objPersonaView As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)
        If (objPersonaView IsNot Nothing) Then

            Me.txtDescripcionPersona.Text = objPersonaView.Descripcion
            Me.txtDNI.Text = objPersonaView.Dni
            Me.txtRUC.Text = objPersonaView.Ruc
            Me.hddIdPersona.Value = CStr(objPersonaView.IdPersona)

        End If
    End Sub
    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region



    Private Sub cboMotivoReciboIngreso_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMotivoReciboIngreso.SelectedIndexChanged

        actualizarMotivoPago(CInt(Me.cboMotivoReciboIngreso.SelectedValue))

    End Sub



    Private Sub actualizarMotivoPago(ByVal Opcion As Integer)
        Try

            '********* Limpiamos la grilla Deudas
            Me.GV_Deudas.DataSource = Nothing
            Me.GV_Deudas.DataBind()
            Me.GV_Otros.DataSource = Nothing
            Me.GV_Otros.DataBind()

            Me.lblMessageHelp.Visible = False
            Me.btnAddConcepto_Otros.Visible = False

            Select Case Opcion

                Case 0 '************ Seleccione una Opción

                Case 1 '************ Pago de deudas
                    Dim lista As List(Of Entidades.Documento_MovCuenta) = (New Negocio.MovCuenta).SelectDeudaxIdPersona(CInt(Me.hddIdPersona.Value), CInt(Me.cboMoneda.SelectedValue))
                    If (lista.Count <= 0) Then
                        Me.cboMotivoReciboIngreso.SelectedValue = "0"
                        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "alert('La Persona seleccionada no posee deudas.'); calcularTotalAPagar();", True)
                        Return
                    Else
                        Me.lblMessageHelp.Visible = True
                        Me.GV_Deudas.DataSource = lista
                        Me.GV_Deudas.DataBind()
                    End If

                Case 2 '************ Otros                    
                    Me.btnAddConcepto_Otros.Visible = True
                    Me.ListaDetalleConcepto = obtenerListaDetalleConcepto(True, 1)
                    Me.GV_Otros.DataSource = Me.ListaDetalleConcepto
                    Me.GV_Otros.DataBind()

            End Select

            '********** Recalculamos el total a pagar y los datos de cancelación
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularTotalAPagar();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnVerHistorial_Click(ByVal sender As Object, ByVal e As EventArgs)
        verHistorialAbonos(sender)
    End Sub
    Private Sub verHistorialAbonos(ByVal sender As Object)
        Try

            Dim IdMovCuenta As Integer = Nothing

            For i As Integer = 0 To Me.GV_Deudas.Rows.Count - 1

                If (CType(sender, ImageButton).ClientID = CType(Me.GV_Deudas.Rows(i).FindControl("btnVerHistorialPago"), ImageButton).ClientID) Then

                    IdMovCuenta = CInt(CType(Me.GV_Deudas.Rows(i).FindControl("hddIdMovCuenta"), HiddenField).Value)
                    Exit For

                End If

            Next

            If (IdMovCuenta = Nothing) Then
                Throw New Exception("No se halló el Registro. Error del Sistema.")
            Else

                '************ MOSTRAMOS EL HISTORIAL DE ABONOS
                Dim listaAbonos As List(Of Entidades.Documento_MovCuenta) = (New Negocio.MovCuenta).SelectAbonosxIdMovCuenta(IdMovCuenta)
                If (listaAbonos.Count <= 0) Then
                    Throw New Exception("No se hallaron abonos para el documento seleccionado.")
                Else
                    Me.GV_Abonos.DataSource = listaAbonos
                    Me.GV_Abonos.DataBind()
                    objScript.onCapa(Me, "capaAbonos")
                End If

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub limpiarControlesFrm(Optional ByVal limpiarDatosPersona As Boolean = True, Optional ByVal procesoEdicion As Boolean = False)


        If Not procesoEdicion Then
            Me.txtCodigoDocumento.Text = ""
            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.hddIdDocumento.Value = ""

        End If

        If limpiarDatosPersona Then
            Me.txtDescripcionPersona.Text = ""
            Me.txtDNI.Text = ""
            Me.txtRUC.Text = ""
            Me.hddIdPersona.Value = ""
        End If



        Me.txtFechaACobrar.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.txtFechaInicio_BA.Text = Me.txtFechaACobrar.Text
        Me.txtFechaFin_BA.Text = Me.txtFechaACobrar.Text
        Me.txtFaltante.Text = "0"
        Me.txtTotalAPagar.Text = "0"
        Me.txtTotalRecibido.Text = "0"
        Me.txtVuelto.Text = "0"

        '****************** DOCUMENTO REF
        Me.GV_DocumentoRef.DataSource = Nothing
        Me.GV_DocumentoRef.DataBind()

        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()

        Me.GV_Cancelacion.DataSource = Nothing
        Me.GV_Cancelacion.DataBind()
        Me.GV_Deudas.DataSource = Nothing
        Me.GV_Deudas.DataBind()
        Me.GV_Otros.DataSource = Nothing
        Me.GV_Otros.DataBind()
        Me.GV_BusquedaAvanzado.DataSource = Nothing
        Me.GV_BusquedaAvanzado.DataBind()
        Me.lblMessageHelp.Visible = False

        Me.txtObservaciones.Text = ""

        Me.hddCodigoDocumento.Value = ""
        Me.hddPermiso_AddMedioPago.Value = "0"

        actualizarControlesMedioPago(1, CInt(Me.hddIdMedioPagoPrincipal.Value))

    End Sub

    Private Sub verFrm(ByVal modoFrm As Integer, ByVal generarCodigoDoc As Boolean, ByVal limpiarSessionViewState As Boolean, ByVal initListasSession As Boolean, ByVal limpiarControles As Boolean, ByVal initMotivoReciboI As Boolean, ByVal limpiarDatosPersona As Boolean, Optional ByVal procesoEdicion As Boolean = False)

        If (limpiarControles) Then
            limpiarControlesFrm(limpiarDatosPersona, procesoEdicion)
        End If


        If (limpiarSessionViewState) Then
            Session.Remove("listaDocumentoRef")
            Session.Remove("ListaCancelacion")
            ViewState.Clear()
        End If


        If (initListasSession) Then
            Me.ListaCancelacion = New List(Of Entidades.PagoCaja)
            setListaCancelacion(Me.ListaCancelacion)

            Me.listaDocumentoRef = New List(Of Entidades.Documento)
            setlistaDocumentoRef(Me.listaDocumentoRef)
        End If

        If (generarCodigoDoc) Then
            GenerarCodigoDocumento()
        End If

        If (initMotivoReciboI) Then
            Me.cboMotivoReciboIngreso.SelectedValue = "0"
            Me.btnAddConcepto_Otros.Visible = False
        End If

        '************** MODO FRM
        hddFrmModo.Value = CStr(modoFrm)
        ActualizarBotonesControl()

    End Sub
    Private Sub ActualizarBotonesControl()

        Me.Panel_DocRef.Enabled = False

        Select Case CInt(hddFrmModo.Value)

            Case 0 '************* INICIO
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '******** Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cab.Enabled = False
                Me.Panel_Cancelacion.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboCaja.Enabled = True

                Me.cboMoneda.Enabled = True



            Case 1 '************** Nuevo
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '******** Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_Cancelacion.Enabled = True
                Me.Panel_Cliente.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Obs.Enabled = True
                Me.Panel_DocRef.Enabled = True

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboCaja.Enabled = True

                Me.cboMoneda.Enabled = True




            Case 2 '*********************** Editar
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '******** Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_Cancelacion.Enabled = True
                Me.Panel_Cliente.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Obs.Enabled = True
                Me.Panel_DocRef.Enabled = True

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboCaja.Enabled = True

                Me.cboMoneda.Enabled = True



            Case 3 '**************************** Buscar documento
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '******** Me.btnBuscarDocumentoxcodigo.Visible = True

                Me.txtCodigoDocumento.ReadOnly = False
                Me.txtCodigoDocumento.Focus()

                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False

                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_Cancelacion.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboCaja.Enabled = True

                Me.cboMoneda.Enabled = True



            Case 4 '********* documento hallado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                '********** Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True
                Me.btnImprimir.Visible = True

                Me.Panel_Cab.Enabled = False
                Me.Panel_Cancelacion.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboCaja.Enabled = False

                Me.cboMoneda.Enabled = False



            Case 5 '********* documento guardado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '********* Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = True

                Me.Panel_Cancelacion.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboCaja.Enabled = False

                Me.cboMoneda.Enabled = False



            Case 6  '*********** Anulacion exitosa

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '********* Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cancelacion.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboCaja.Enabled = False

                Me.cboMoneda.Enabled = False


        End Select
    End Sub

    Private Function obtenerDetalleConcepto_Save() As List(Of Entidades.DetalleConcepto)

        Dim lista As New List(Of Entidades.DetalleConcepto)

        Select Case CInt(Me.cboMotivoReciboIngreso.SelectedValue)

            Case 0 '********** Elegir motivo

            Case 1 '************ Pago deudas
                For i As Integer = 0 To Me.GV_Deudas.Rows.Count - 1

                    If (CType(Me.GV_Deudas.Rows(i).FindControl("chbSelectDeuda"), CheckBox).Checked) Then

                        Dim obj As New Entidades.DetalleConcepto

                        With obj
                            .Monto = CDec(CType(Me.GV_Deudas.Rows(i).FindControl("txtAbono"), TextBox).Text)
                            .Concepto = "Pago de " + Me.GV_Deudas.Rows(i).Cells(1).Text + " Nro. " + CType(Me.GV_Deudas.Rows(i).FindControl("lblNroDocumentoDeuda"), Label).Text
                            .IdMoneda = CInt(Me.cboMoneda.SelectedValue)

                            '******** Los documentos a los cuales se les va a afectar
                            .IdDocumentoRef = CInt(CType(Me.GV_Deudas.Rows(i).FindControl("hddIdDocumentoDeuda"), HiddenField).Value)
                            .IdMovCuentaRef = CInt(CType(Me.GV_Deudas.Rows(i).FindControl("hddIdMovCuenta"), HiddenField).Value)

                        End With

                        lista.Add(obj)

                    End If

                Next

            Case 2  '******** otros             

                lista = obtenerListaDetalleConcepto(False, Nothing)

        End Select

        Return lista

    End Function
    Private Function obtenerListaDetalleConcepto(ByVal generarNewLista As Boolean, ByVal cantidadInitNew As Integer) As List(Of Entidades.DetalleConcepto)

        Dim lista As New List(Of Entidades.DetalleConcepto)

        '********** Obtengo la lista de conceptos segun el tipo de Recibo
        Dim listaAux As List(Of Entidades.Concepto) = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(CInt(Me.hddIdTipoDocumento.Value))

        If (generarNewLista) Then  '********** Generamos la Lista de DetalleRecibo // Otros

            '************ Genero la lista
            For i As Integer = 0 To cantidadInitNew - 1

                Dim obj As New Entidades.DetalleConcepto

                obj.Moneda = Me.cboMoneda.SelectedItem.ToString
                obj.Monto = 0
                obj.Concepto = ""
                obj.ListaConcepto = listaAux
                obj.IdConcepto = Nothing
                lista.Add(obj)

            Next
        Else

            '**************** DEVOLVEMOS LA LISTA DEL DETALLE CONCEPTO (ACTUALIZAMOS)

            For i As Integer = 0 To Me.GV_Otros.Rows.Count - 1

                Dim obj As New Entidades.DetalleConcepto

                obj.Moneda = CType(Me.GV_Otros.Rows(i).FindControl("lblMoneda"), Label).Text
                obj.Monto = CDec(CType(Me.GV_Otros.Rows(i).FindControl("txtAbono"), TextBox).Text)
                obj.Concepto = CType(Me.GV_Otros.Rows(i).FindControl("txtDescripcionConcepto"), TextBox).Text
                obj.ListaConcepto = listaAux
                obj.IdConcepto = CInt(CType(Me.GV_Otros.Rows(i).FindControl("cboConcepto"), DropDownList).SelectedValue)
                lista.Add(obj)

            Next

        End If

        Return lista

    End Function

    Private Sub addNewDetalle_Otros()
        Try

            '********* Obtengo la Lista
            Me.ListaDetalleConcepto = obtenerListaDetalleConcepto(False, Nothing)

            '*********** Creo un Nuevo Detalle
            Dim listaAux As List(Of Entidades.Concepto) = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(CInt(Me.hddIdTipoDocumento.Value))


            '************ Creo el objeto y lo agrego a la Lista
            Dim obj As New Entidades.DetalleConcepto
            With obj
                .Moneda = Me.cboMoneda.SelectedItem.ToString
                .Monto = 0
                .Concepto = ""
                .ListaConcepto = listaAux
                .IdConcepto = Nothing
            End With
            Me.ListaDetalleConcepto.Add(obj)

            '************ Grilla
            Me.GV_Otros.DataSource = Me.ListaDetalleConcepto
            Me.GV_Otros.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_Otros_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Otros.RowDataBound

        Try

            If (e.Row.RowType = DataControlRowType.DataRow) Then


                Dim gvrow As GridViewRow = CType(e.Row.Cells(1).NamingContainer, GridViewRow)
                Dim combo As DropDownList = CType(gvrow.FindControl("cboConcepto"), DropDownList)

                If (Me.ListaDetalleConcepto(e.Row.RowIndex).IdConcepto <> Nothing) Then

                    combo.SelectedValue = CStr(Me.ListaDetalleConcepto(e.Row.RowIndex).IdConcepto)

                End If

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub GV_Otros_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Otros.SelectedIndexChanged
        eliminarDetalleRecibo_Otros(Me.GV_Otros.SelectedIndex)
    End Sub
    Private Sub eliminarDetalleRecibo_Otros(ByVal index As Integer)
        Try

            '****** Elimino el detalle
            Me.ListaDetalleConcepto = obtenerListaDetalleConcepto(False, Nothing)
            Me.ListaDetalleConcepto.RemoveAt(index)

            '******** Grilla
            Me.GV_Otros.DataSource = Me.ListaDetalleConcepto
            Me.GV_Otros.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      calcularTotalAPagar();       ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnAddConcepto_Otros_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddConcepto_Otros.Click
        addNewDetalle_Otros()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click

        If Validar_AperturaCierreCaja() Then
            registrarReciboIngreso()
        End If

    End Sub

    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            .IdUsuario = CInt(Session("IdUsuario"))
            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .Serie = Me.cboSerie.SelectedItem.ToString
            .Codigo = Me.txtCodigoDocumento.Text
            .IdTipoDocumento = CInt(Me.hddIdTipoDocumento.Value)
            .IdEstadoDoc = 1  '************ Siempre es ACTIVO
            .FechaEmision = CDate(Me.txtFechaEmision.Text)
            .IdCaja = CInt(Me.cboCaja.SelectedValue)
            .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
            .IdPersona = CInt(Me.hddIdPersona.Value)
            .TotalAPagar = CDec(Me.txtTotalAPagar.Text)
            .Total = CDec(Me.txtTotalAPagar.Text)
            .IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
            .IdCondicionPago = 1  '********* CONTADO
            .FechaCancelacion = (New Negocio.FechaActual).SelectFechaActual

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo  '******* Nuevo
                    .Id = Nothing
                Case FrmModo.Editar '***** Editar
                    .Id = CInt(Me.hddIdDocumento.Value)
            End Select

            .Vuelto = CDec(Me.txtVuelto.Text)
            .IdEstadoCancelacion = 2   '******** CANCELADO
            .TotalLetras = (New Aletras).Letras(CStr(objDocumento.TotalAPagar)) + " /100 " + (New Negocio.Moneda).SelectxId(.IdMoneda)(0).Descripcion

        End With

        Return objDocumento

    End Function
    Private Sub validarFrm()

        Me.ListaCancelacion = getListaCancelacion()
        For i As Integer = 0 To Me.ListaCancelacion.Count - 1
            If ((Me.ListaCancelacion(i).IdMedioPago <> CInt(Me.hddIdMedioPagoPrincipal.Value)) And CDec(Me.txtVuelto.Text) > 0) Then
                Throw New Exception("SE HAN INGRESADO MEDIOS DE PAGO QUE NO DEBEN GENERAR VUELTO. NO SE PERMITE LA OPERACIÓN.")
            End If
        Next

    End Sub
    Private Function obtenerListaMovBanco(ByVal lista As List(Of Entidades.PagoCaja), ByVal objDocumento As Entidades.Documento) As List(Of Entidades.MovBancoView)

        Dim listaMovBanco As New List(Of Entidades.MovBancoView)
        For i As Integer = 0 To lista.Count - 1

            Dim obj As New Entidades.MovBancoView
            With obj
                .IdBanco = lista(i).IdBanco
                .IdCuentaBancaria = lista(i).IdCuentaBancaria
                .Monto = lista(i).Efectivo
                .IdMoneda = lista(i).IdMoneda
                .IdMedioPago = lista(i).IdMedioPago
                .IdUsuario = objDocumento.IdUsuario
                .IdPersonaRef = objDocumento.IdPersona
                .IdCaja = objDocumento.IdCaja
                .IdTienda = objDocumento.IdTienda
                .FechaMov = objDocumento.FechaEmision

                Select Case (lista(i).IdMedioPagoInterfaz)
                    Case 3  '*********** CHEQUE
                        .NroOperacion = lista(i).NumeroCheque
                    Case Else
                        .NroOperacion = lista(i).NumeroOp
                End Select

                .IdTarjeta = lista(i).IdTarjeta
                .IdPost = lista(i).IdPost

            End With

            listaMovBanco.Add(obj)

        Next

        Return listaMovBanco

    End Function
    Private Sub registrarReciboIngreso()

        Try

            validarFrm()

            '****** cabecera Documento
            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()

            '******* Detalle CONCEPTO
            Me.ListaDetalleConcepto = obtenerDetalleConcepto_Save()
            '******** obtengo la lista pago caja a insertar
            Dim listaPagoCaja As List(Of Entidades.PagoCaja) = obtenerListaPagoCaja_Save()
            Dim objMovCaja As Entidades.MovCaja = obtenerMovCaja()

            Dim objObservaciones As Entidades.Observacion = obtenerObservaciones()

            Dim listaMovBanco As List(Of Entidades.MovBancoView) = obtenerListaMovBanco(listaPagoCaja, objDocumento)

            Dim listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = obtenerListaRelacionDocumento()

            Select Case CInt(Me.hddFrmModo.Value)
                Case 1 '******* NUEVO
                    Dim IdDocumento As Integer = (New Negocio.DocReciboCaja).ReciboCajaIngresoInsert(objDocumento, Me.ListaDetalleConcepto, objMovCaja, listaPagoCaja, objObservaciones, listaMovBanco, listaRelacionDocumento)

                    If (IdDocumento > 0) Then

                        verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False, False)
                        Me.hddIdDocumento.Value = CStr(IdDocumento)
                        objScript.mostrarMsjAlerta(Me, "El Documento se registró con éxito.")

                    Else
                        Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
                    End If


                Case 2 '******* EDITAR
                    If ((New Negocio.DocReciboCaja).ReciboCajaIngresoUpdate(objDocumento, Me.ListaDetalleConcepto, objMovCaja, listaPagoCaja, objObservaciones, listaMovBanco, listaRelacionDocumento) > 0) Then

                        verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False, False)

                        objScript.mostrarMsjAlerta(Me, "El Documento se actualizó con éxito.")
                    Else
                        Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
                    End If
            End Select

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function obtenerListaRelacionDocumento() As List(Of Entidades.RelacionDocumento)

        Dim listaRelacionDocumento As New List(Of Entidades.RelacionDocumento)

        Me.listaDocumentoRef = getlistaDocumentoRef()

        '************************ OBTENEMOS LOS DOCUMENTOS DE REFERENCIA SEGUN LA LISTA DE DOCUMENTOS DE REFERENCIA
        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            Dim obj As New Entidades.RelacionDocumento
            With obj

                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento2 = Nothing
                    Case FrmModo.Editar
                        .IdDocumento2 = CInt(Me.hddIdDocumento.Value)
                End Select

                .IdDocumento1 = Me.listaDocumentoRef(i).Id

            End With
            listaRelacionDocumento.Add(obj)

        Next

        '********************** OBTENEMOS LA LISTA DE DOCUMENTOS DE REFERENCIA SEGUN LOS PAGOS (DEUDAS)
        For i As Integer = 0 To Me.GV_Deudas.Rows.Count - 1

            If (CType(Me.GV_Deudas.Rows(i).FindControl("chbSelectDeuda"), CheckBox).Checked) Then

                Dim add As Boolean = True

                For j As Integer = 0 To listaRelacionDocumento.Count - 1

                    If (CInt(CType(Me.GV_Deudas.Rows(i).FindControl("hddIdDocumentoDeuda"), HiddenField).Value) = listaRelacionDocumento(j).IdDocumento1) Then
                        add = False
                        Exit For
                    End If

                Next

                If (add) Then

                    Dim obj As New Entidades.RelacionDocumento
                    With obj
                        Select Case CInt(Me.hddFrmModo.Value)
                            Case FrmModo.Nuevo
                                .IdDocumento2 = Nothing
                            Case FrmModo.Editar
                                .IdDocumento2 = CInt(Me.hddIdDocumento.Value)
                        End Select
                        .IdDocumento1 = CInt(CType(Me.GV_Deudas.Rows(i).FindControl("hddIdDocumentoDeuda"), HiddenField).Value)
                    End With
                    listaRelacionDocumento.Add(obj)

                End If

            End If

        Next

        Return listaRelacionDocumento

    End Function


    Private Function obtenerObservaciones() As Entidades.Observacion

        Dim objObservaciones As Entidades.Observacion = Nothing
        If (Me.txtObservaciones.Text.Trim.Length > 0) Then

            objObservaciones = New Entidades.Observacion
            objObservaciones.Observacion = Me.txtObservaciones.Text.Trim

        End If
        Return objObservaciones
    End Function

    Private Function obtenerListaPagoCaja_Save() As List(Of Entidades.PagoCaja)

        Me.ListaCancelacion = getListaCancelacion()

        If (Me.ListaCancelacion.Count = 0) Then  '**** Creamos un Pago Caja EFECTIVO

            Dim objPagoCaja As New Entidades.PagoCaja
            With objPagoCaja
                .Efectivo = CDec(Me.txtTotalAPagar.Text)
                .Factor = 1
                .IdMedioPago = CInt(Me.hddIdMedioPagoPrincipal.Value)
                .IdMedioPagoInterfaz = CInt(Me.hddIdMedioPagoInterfaz.Value)
                .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
                .IdTipoMovimiento = 1  '****** INGRESO

            End With

            Me.ListaCancelacion.Add(objPagoCaja)

        End If

        If (CDec(Me.txtVuelto.Text) > 0) Then  '********** Si existe vuelto

            For i As Integer = 0 To Me.ListaCancelacion.Count - 1

                If ((Me.ListaCancelacion(i).IdMedioPago = CInt(Me.hddIdMedioPagoPrincipal.Value)) And (Me.ListaCancelacion(i).IdMoneda = CInt(Me.cboMoneda.SelectedValue))) Then '******** Si es EFECTIVO y es la misma moneda de Emision
                    Me.ListaCancelacion(i).Vuelto = CDec(Me.txtVuelto.Text)
                    Return Me.ListaCancelacion
                End If

            Next

            '*********** Si no existe debemos crear un Medio de Pago para el Vuelto
            Dim objPagoCaja As New Entidades.PagoCaja
            With objPagoCaja

                '.IdMedioPagoInterfaz = CInt(Me.hddIdMedioPagoInterfaz.Value)
                .Factor = 1
                .IdMedioPago = CInt(Me.hddIdMedioPagoPrincipal.Value)
                .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
                .IdTipoMovimiento = 1  '****** INGRESO
                .Vuelto = CDec(Me.txtVuelto.Text)
                .Efectivo = CDec(Me.txtVuelto.Text) * -1

            End With

            Me.ListaCancelacion.Add(objPagoCaja)

            Return Me.ListaCancelacion

        End If


        Return Me.ListaCancelacion

    End Function
    Private Function obtenerMovCaja() As Entidades.MovCaja
        Dim objMovCaja As New Entidades.MovCaja

        With objMovCaja

            .IdCaja = CInt(Me.cboCaja.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdPersona = CInt(Session("IdUsuario"))
            .IdTipoMovimiento = 1   '************* Ingreso

        End With

        Return objMovCaja
    End Function

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        verFrm(FrmModo.Nuevo, True, True, True, True, True, True)
    End Sub

    Private Sub btnBuscarDocumentoxcodigo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarDocumentoxCodigo.Click
        Try
            buscarDocumento(CInt(Me.cboSerie.SelectedValue), CInt(Me.hddCodigoDocumento.Value), Nothing)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub cargarDocumentoCab(ByVal objDocumento As Entidades.Documento)

        Me.txtCodigoDocumento.Text = objDocumento.Codigo
        Me.txtFechaEmision.Text = Format(objDocumento.FechaEmision, "dd/MM/yyyy")
        Me.cboEstado.SelectedValue = CStr(objDocumento.IdEstadoDoc)
        Me.hddIdDocumento.Value = CStr(objDocumento.Id)
        Me.txtTotalAPagar.Text = CStr(Math.Round(objDocumento.TotalAPagar, 2))

        If (Me.cboMoneda.Items.FindByValue(CStr(objDocumento.IdMoneda)) IsNot Nothing) Then
            Me.cboMoneda.SelectedValue = CStr(objDocumento.IdMoneda)
        End If

        If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumento.IdTipoOperacion)) IsNot Nothing) Then
            Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)
        End If

    End Sub


    Private Sub buscarDocumento(ByVal IdSerie As Integer, ByVal codigo As Integer, ByVal IdDocumento As Integer)
        Try
            Dim objDocumento As Entidades.Documento = Nothing

            If (IdDocumento = Nothing) Then
                objDocumento = (New Negocio.Documento).SelectxIdSeriexCodigo(IdSerie, codigo)
            Else
                objDocumento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)
            End If

            '******** Si paso es xq si existe el documento
            verFrm(FrmModo.Documento_Buscar_Exito, False, True, True, True, True, True)

            cargarDocumentoCab(objDocumento)
            cargarPersona(objDocumento.IdPersona)

            Dim objObservaciones As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumento.Id)
            If (objObservaciones IsNot Nothing) Then
                Me.txtObservaciones.Text = objObservaciones.Observacion
            End If

            Me.ListaDetalleConcepto = (New Negocio.DetalleConcepto).SelectxIdDocumento(objDocumento.Id)
            Me.GV_Otros.DataSource = Me.ListaDetalleConcepto
            Me.GV_Otros.DataBind()


            Me.ListaCancelacion = obtenerListaCancelacion_Load(objDocumento.Id)
            setListaCancelacion(Me.ListaCancelacion)
            Me.GV_Cancelacion.DataSource = Me.ListaCancelacion
            Me.GV_Cancelacion.DataBind()

            '******************* RELACION DOCUMENTO
            Me.listaDocumentoRef = (New Negocio.RelacionDocumento).RelacionDocumento_SelectDocumentoRefxIdDocumento2(objDocumento.Id)
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            Validar_AperturaCierreCaja(False)

            '************ Calculamos los datos de cancelación
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularDatosCancelacion();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        If Validar_AperturaCierreCaja() Then
            HabilitarEdicionDocumento()
        End If
    End Sub
    Private Sub HabilitarEdicionDocumento()

        Try

            If (New Negocio.DocReciboCaja).DocumentoReciboIngresoDeshacerMov_Edicion(CInt(Me.hddIdDocumento.Value)) Then

                verFrm(FrmModo.Editar, False, True, True, True, True, False, True)
                objScript.mostrarMsjAlerta(Me, "El Proceso resultó con éxito.")

            Else
                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnular.Click

        anularDocumentoReciboIngreso(CInt(Me.hddIdDocumento.Value))

    End Sub
    Private Sub anularDocumentoReciboIngreso(ByVal IdDocumento As Integer)
        Try


            If ((New Negocio.DocReciboCaja).DocumentoReciboIngresoAnular(IdDocumento)) Then
                verFrm(FrmModo.Documento_Anular_Exito, False, False, False, False, False, False, False)
                objScript.mostrarMsjAlerta(Me, "El Proceso resultó con éxito.")
            End If



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

#Region "**************************** DATOS DE CANCELACION"

    Private Function obtenerListaCancelacion_Load(ByVal IdDocumento As Integer) As List(Of Entidades.PagoCaja)
        Dim lista As List(Of Entidades.PagoCaja) = (New Negocio.PagoCaja).SelectListaxIdDocumento(IdDocumento)

        For i As Integer = 0 To lista.Count - 1

            Select Case lista(i).IdMedioPagoInterfaz


                Case 1  '*********** EFECTIVO

                    lista(i).descripcionPagoCaja = " Medio Pago: [ " + lista(i).NomMedioPago + " ] Monto: [ " + lista(i).NomMoneda + " " + CStr(Math.Round(lista(i).Efectivo, 2)) + " ]"

                Case 2  '*********** BANCO  (NRO OPERACION)

                    lista(i).descripcionPagoCaja = " Medio Pago: [ " + lista(i).NomMedioPago + " ] Monto: [ " + lista(i).NomMoneda + " " + CStr(Math.Round(lista(i).Efectivo, 2)) + " ] Banco: [ " + lista(i).NomBanco + " ] Cuenta: [ " + lista(i).NumeroCuenta + " ] Nro Op: [ " + lista(i).NumeroOp + " ] "


                Case 3  '************** BANCO CHEQUE

                    Dim fecha As String = ""

                    If (lista(i).FechaACobrar <> Nothing) Then
                        fecha = Format(lista(i).FechaACobrar, "dd/MM/yyyy")
                    Else
                        fecha = "---"
                    End If

                    lista(i).descripcionPagoCaja = " Medio Pago: [ " + lista(i).NomMedioPago + " ] Monto: [ " + lista(i).NomMoneda + " " + CStr(Math.Round(lista(i).Efectivo, 2)) + " ] Banco: [ " + lista(i).NomBanco + " ] Fecha a Cobrar: [ " + fecha + " ]"


                Case 4 '************* TARJETA

                    lista(i).descripcionPagoCaja = " Medio Pago: [ " + lista(i).NomMedioPago + " ] Monto: [ " + lista(i).NomMoneda + " " + CStr(Math.Round(lista(i).Efectivo, 2)) + " ] Banco: [ " + lista(i).NomBanco + " ] Cuenta: [ " + lista(i).NumeroCuenta + " ]"

                Case 5   '*********** NOTA DE CREDITO

                    lista(i).descripcionPagoCaja = " Medio Pago: [ " + lista(i).NomMedioPago + " ] Monto: [ " + lista(i).NomMoneda + " " + CStr(Math.Round(lista(i).Efectivo, 2)) + " ] Nro. Documento: [ " + lista(i).NumeroOp + " ] "

                Case 6  '************* COMP RETENCION

                    lista(i).descripcionPagoCaja = " Medio Pago: [ " + lista(i).NomMedioPago + " ] Monto: [ " + lista(i).NomMoneda + " " + CStr(Math.Round(lista(i).Efectivo, 2)) + " ] Nro. Documento: [ " + lista(i).NumeroOp + " ] "

            End Select


        Next

        Return lista
    End Function

    Protected Sub cboMedioPago_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboMedioPago.SelectedIndexChanged
        Try
            actualizarControlesMedioPago(1, CInt(Me.cboMedioPago.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub actualizarControlesMedioPago(ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer)

        '************ Limpiar Datos de Cancelación
        Me.txtNro_DC.Text = ""
        Me.txtMonto_DatoCancelacion.Text = "0"
        Me.txtFechaACobrar.Text = ""
        Me.hddPermiso_AddMedioPago.Value = "0"

        Select Case IdCondicionPago
            Case 1  '**************** CONTADO
                Me.Panel_CP_Contado.Visible = True

                '*************** MEDIO DE PAGO
                Me.lblMonto_DC.Visible = False
                Me.txtMonto_DatoCancelacion.Visible = False
                Me.cboMoneda_DatoCancelacion.Visible = False
                Me.btnAddDatoCancelacion.Visible = False

                Me.lblPost_DC.Visible = False
                Me.cboPost_DC.Visible = False
                Me.lblTarjeta_DC.Visible = False
                Me.cboTarjeta_DC.Visible = False
                Me.lblBanco_DC.Visible = False
                Me.cboBanco.Visible = False
                Me.lblCuentaBancaria_DC.Visible = False
                Me.cboCuentaBancaria.Visible = False
                Me.lblNro_DC.Visible = False
                Me.txtNro_DC.Visible = False
                Me.lblFechaACobrar_DC.Visible = False
                Me.txtFechaACobrar.Visible = False

                Me.btnVer_NotaCredito.Visible = False

                Dim objMedioPago As Entidades.MedioPago = (New Negocio.MedioPago).SelectxId(IdMedioPago)(0)

                Me.cboBanco.Enabled = True
                Me.cboCuentaBancaria.Enabled = True

                Select Case objMedioPago.IdMedioPagoInterfaz

                    Case 1  '*************** EFECTIVO
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True

                    Case 2  '*************** BANCO
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        Me.lblBanco_DC.Visible = True
                        Me.cboBanco.Visible = True
                        Me.lblCuentaBancaria_DC.Visible = True
                        Me.cboCuentaBancaria.Visible = True
                        Me.lblNro_DC.Visible = True
                        Me.txtNro_DC.Visible = True

                        actualizarControles_BANCOS()

                    Case 3  '**************** BANCO CHEQUE
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        Me.lblBanco_DC.Visible = True
                        Me.cboBanco.Visible = True
                        Me.lblNro_DC.Visible = True
                        Me.txtNro_DC.Visible = True
                        Me.lblFechaACobrar_DC.Visible = True
                        Me.txtFechaACobrar.Visible = True

                        actualizarControles_BANCOS()

                    Case 4  '*********** TARJETA
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        Me.lblBanco_DC.Visible = True
                        Me.cboBanco.Visible = True
                        Me.lblCuentaBancaria_DC.Visible = True
                        Me.cboCuentaBancaria.Visible = True
                        Me.lblNro_DC.Visible = True
                        Me.txtNro_DC.Visible = True
                        Me.lblPost_DC.Visible = True
                        Me.cboPost_DC.Visible = True
                        Me.lblTarjeta_DC.Visible = True
                        Me.cboTarjeta_DC.Visible = True

                        '************ Configuramos los cambios por POST
                        actualizarControles_POST()
                        Me.cboBanco.Enabled = False
                        Me.cboCuentaBancaria.Enabled = False

                    Case 5  '************* NOTA CREDITO

                        Me.btnVer_NotaCredito.Visible = True

                    Case 6  '************** COMPROBANTE DE RETENCION

                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        Me.lblNro_DC.Visible = True
                        Me.txtNro_DC.Visible = True

                End Select

                Me.hddIdMedioPagoInterfaz.Value = CStr(objMedioPago.IdMedioPagoInterfaz)

            Case 2  '**************** CREDITO
                Me.Panel_CP_Contado.Visible = False

        End Select

    End Sub

    Protected Sub btnAddDatoCancelacion_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddDatoCancelacion.Click

        addDatoCancelacion()

    End Sub

    Private Sub addDatoCancelacion()
        Try

            If (CInt(Me.hddPermiso_AddMedioPago.Value) <= 0) Then
                If ((New Negocio.Perfil_MedioPago).Perfil_MedioPago_ValidarxIdMedioPagoxIdUsuario(CInt(Session("IdUsuario")), CInt(Me.cboMedioPago.SelectedValue)) <= 0) Then

                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  alert('NO POSEE LOS PERMISOS NECESARIOS PARA INGRESAR ESTE MEDIO DE PAGO. NO SE PERMITE LA OPERACIÒN.');     verCapaPermiso_AddMedioPago();  ", True)
                    Return

                End If
            End If


            Me.ListaCancelacion = getListaCancelacion()

            Dim objPagoCaja As New Entidades.PagoCaja

            objPagoCaja.IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
            objPagoCaja.Factor = 1
            objPagoCaja.IdTipoMovimiento = 1 '*************** INGRESO
            objPagoCaja.IdMedioPagoInterfaz = CInt(Me.hddIdMedioPagoInterfaz.Value)

            Select Case CInt(Me.hddIdMedioPagoInterfaz.Value)

                Case 1  '*********** EFECTIVO

                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.NomMonedaDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objPagoCaja.Efectivo = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)
                    objPagoCaja.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    objPagoCaja.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objPagoCaja.Efectivo, objPagoCaja.IdMoneda, objPagoCaja.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))
                    objPagoCaja.descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objPagoCaja.Efectivo, 2)) + " ]"

                Case 2  '*********** BANCO  (NRO OPERACION)

                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.Efectivo = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)
                    objPagoCaja.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)

                    objPagoCaja.NomMonedaDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objPagoCaja.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objPagoCaja.Efectivo, objPagoCaja.IdMoneda, objPagoCaja.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))

                    objPagoCaja.IdBanco = CInt(Me.cboBanco.SelectedValue)
                    objPagoCaja.IdCuentaBancaria = CInt(Me.cboCuentaBancaria.SelectedValue)

                    objPagoCaja.descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objPagoCaja.Efectivo, 2)) + " ] Banco: [ " + Me.cboBanco.SelectedItem.ToString + " ] Cuenta: [ " + Me.cboCuentaBancaria.SelectedItem.ToString + " ]"
                    objPagoCaja.NumeroOp = Me.txtNro_DC.Text

                Case 3  '************** BANCO CHEQUE

                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.Efectivo = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)

                    objPagoCaja.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    objPagoCaja.NomMonedaDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objPagoCaja.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objPagoCaja.Efectivo, objPagoCaja.IdMoneda, objPagoCaja.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))

                    objPagoCaja.IdBanco = CInt(Me.cboBanco.SelectedValue)

                    Dim fecha As String = ""
                    Try
                        objPagoCaja.FechaACobrar = CDate(Me.txtFechaACobrar.Text)
                        fecha = Me.txtFechaACobrar.Text
                    Catch ex As Exception
                        objPagoCaja.FechaACobrar = Nothing
                        fecha = "---"
                    End Try

                    objPagoCaja.descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objPagoCaja.Efectivo, 2)) + " ] Banco: [ " + Me.cboBanco.SelectedItem.ToString + " ] Fecha a Cobrar: [ " + fecha + " ]"
                    objPagoCaja.NumeroCheque = Me.txtNro_DC.Text

                Case 4 '************* TARJETA

                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.Efectivo = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)

                    objPagoCaja.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    objPagoCaja.NomMonedaDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objPagoCaja.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objPagoCaja.Efectivo, objPagoCaja.IdMoneda, objPagoCaja.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))
                    objPagoCaja.NumeroOp = CStr(Me.txtNro_DC.Text.Trim)

                    objPagoCaja.IdBanco = CInt(Me.cboBanco.SelectedValue)
                    objPagoCaja.IdCuentaBancaria = CInt(Me.cboCuentaBancaria.SelectedValue)
                    objPagoCaja.IdPost = (New Negocio.PostView).SelectIdPosxTipoTarjetaCaja(CInt(Me.cboTarjeta_DC.SelectedValue), CInt(Me.cboPost_DC.SelectedValue), CInt(Me.cboCaja.SelectedValue))
                    objPagoCaja.IdTarjeta = CInt(Me.cboTarjeta_DC.SelectedValue)
                    objPagoCaja.descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objPagoCaja.Efectivo, 2)) + " ] Banco: [ " + Me.cboBanco.SelectedItem.ToString + " ] Cuenta: [ " + Me.cboCuentaBancaria.SelectedValue.ToString + " ]"

                Case 5   '*********** NOTA DE CREDITO

                    '*************** NO SE UTILIZA

                Case 6   '************** COMP RETENCION


                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.Efectivo = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)

                    objPagoCaja.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    objPagoCaja.NomMonedaDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objPagoCaja.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objPagoCaja.Efectivo, objPagoCaja.IdMoneda, objPagoCaja.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))
                    objPagoCaja.NumeroOp = CStr(Me.txtNro_DC.Text.Trim)

                    objPagoCaja.descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objPagoCaja.Efectivo, 2)) + " ] "

            End Select

            Me.ListaCancelacion.Add(objPagoCaja)
            setListaCancelacion(Me.ListaCancelacion)

            Me.GV_Cancelacion.DataSource = Me.ListaCancelacion
            Me.GV_Cancelacion.DataBind()

            Me.txtMonto_DatoCancelacion.Text = "0"
            Me.txtNro_DC.Text = ""
            Me.txtFechaACobrar.Text = ""

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularDatosCancelacion(); ", True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub



    Protected Sub GV_Cancelacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_Cancelacion.SelectedIndexChanged
        quitarDatoCancelacion()
    End Sub
    Private Sub quitarDatoCancelacion()

        Try

            Me.ListaCancelacion = getListaCancelacion()
            Me.ListaCancelacion.RemoveAt(Me.GV_Cancelacion.SelectedRow.RowIndex)

            setListaCancelacion(Me.ListaCancelacion)

            Me.GV_Cancelacion.DataSource = Me.ListaCancelacion
            Me.GV_Cancelacion.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularDatosCancelacion(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Private Sub GV_NotaCredito_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_NotaCredito.SelectedIndexChanged
        agregarDatoCancelacion_NC()
    End Sub

    Private Sub agregarDatoCancelacion_NC()
        Try

            '************** VALIDAMOS SALDO
            Dim saldo As Decimal = CDec(CType(Me.GV_NotaCredito.SelectedRow.FindControl("lblSaldo_Find"), Label).Text)
            Dim monto As Decimal = CDec(CType(Me.GV_NotaCredito.SelectedRow.FindControl("txtMontoRecibir_Find"), TextBox).Text)
            Dim moneda As String = CStr(CType(Me.GV_NotaCredito.SelectedRow.FindControl("lblMonedaMontoRecibir_Find"), Label).Text)
            Dim IdDocumento As Integer = CInt(CType(Me.GV_NotaCredito.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value)
            Dim IdMonedaNC As Integer = CInt(CType(Me.GV_NotaCredito.SelectedRow.FindControl("hddIdMonedaDocRef_Find"), HiddenField).Value)
            Dim nroDocumento As String = CStr(CType(Me.GV_NotaCredito.SelectedRow.FindControl("lblNroDocumento_Find"), Label).Text)

            If (monto > saldo) Then
                Throw New Exception("El Monto Ingresado excede al saldo disponible [ " + moneda + " " + CStr(Math.Round(saldo, 2)) + " ].")
            Else
                '**************** Validamos el dato en Grilla de cancelación
                Me.ListaCancelacion = getListaCancelacion()
                For i As Integer = 0 To Me.ListaCancelacion.Count - 1
                    If (CInt(Me.ListaCancelacion(i).NumeroCheque) = IdDocumento) Then
                        Throw New Exception("La Nota de Crédito seleccionado ya se encuentra en [ Datos de Cancelación ].")
                    End If
                Next

                '**************** Agregamos el dato de cancelación
                Dim objPagoCaja As New Entidades.PagoCaja
                With objPagoCaja

                    .Efectivo = monto
                    .IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
                    .IdMoneda = CInt(IdMonedaNC)
                    .IdTipoMovimiento = 1   '******** Ingreso
                    .Factor = 1

                    .NumeroCheque = CStr(IdDocumento)

                    .NomMonedaDestino = Me.cboMoneda.SelectedItem.ToString
                    .IdMonedaDestino = CInt(cboMoneda.SelectedValue)
                    .MontoEquivalenteDestino = (New Negocio.Util).SelectValorxIdMonedaOxIdMonedaD(.IdMoneda, .IdMonedaDestino, .Efectivo, "E")
                    .descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objPagoCaja.Efectivo, 2)) + " ] Nro. Documento: [ " + nroDocumento + " ]"

                    .IdMedioPagoInterfaz = CInt(Me.hddIdMedioPagoInterfaz.Value)

                End With
                Me.ListaCancelacion.Add(objPagoCaja)
                setListaCancelacion(Me.ListaCancelacion)

                '************* Mostramos la grilla
                Me.GV_Cancelacion.DataSource = Me.ListaCancelacion
                Me.GV_Cancelacion.DataBind()

                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " offCapa('capaDocumento_NotaCredito');  calcularDatosCancelacion();", True)

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub mostrarDocumentosNotaCredito()
        Try

            If (CInt(Me.hddPermiso_AddMedioPago.Value) <= 0) Then
                If ((New Negocio.Perfil_MedioPago).Perfil_MedioPago_ValidarxIdMedioPagoxIdUsuario(CInt(Session("IdUsuario")), CInt(Me.cboMedioPago.SelectedValue)) <= 0) Then

                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  alert('NO POSEE LOS PERMISOS NECESARIOS PARA INGRESAR ESTE MEDIO DE PAGO. NO SE PERMITE LA OPERACIÒN.');     verCapaPermiso_AddMedioPago();   ", True)
                    Return

                End If
            End If



            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.DocumentoNotaCredito).DocumentoNotaCreditoSelectCabFind_Aplicacion(CInt(Me.hddIdPersona.Value), 0, 0)

            If (lista.Count > 0) Then

                Me.GV_NotaCredito.DataSource = lista
                Me.GV_NotaCredito.DataBind()
                objScript.onCapa(Me, "capaDocumento_NotaCredito")

            Else

                Throw New Exception("No se hallaron registros.")

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnVer_NotaCredito_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVer_NotaCredito.Click
        mostrarDocumentosNotaCredito()
    End Sub


    Private Sub cboPost_DC_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPost_DC.SelectedIndexChanged
        Try
            actualizarControles_POST()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub actualizarControles_POST()

        Dim objcbo As New Combo

        With objcbo
            'Cambio
            '.LlenarCboTarjetaxIdPost(Me.cboTarjeta_DC, CInt(Me.cboPost_DC.SelectedValue), False)
            .LlenarCboTarjetaxTipoTarjetaCaja(Me.cboTarjeta_DC, CInt(Me.cboPost_DC.SelectedValue), CInt(Me.cboCaja.SelectedValue))

            Dim objPost As Entidades.Post = (New Negocio.PostView).SelectxIdPost(CInt(Me.cboPost_DC.SelectedValue))

            '************* Banco Cuenta bancaria
            Me.cboBanco.SelectedValue = CStr(objPost.IdBanco)

            .LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)
            Me.cboCuentaBancaria.SelectedValue = CStr(objPost.IdCuentaBancaria)

        End With

    End Sub

    Private Sub actualizarControles_BANCOS()

        Dim objCbo As New Combo
        With objCbo
            .LlenarCboBanco(Me.cboBanco, False)
            .LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)
        End With

    End Sub

    Private Sub cboBanco_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBanco.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

#Region "************************ BÚSQUEDA AVANZADO"

    Private Sub btnAceptarBusquedaAvanzado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBusquedaAvanzado.Click
        busquedaAvanzado()
    End Sub

    Private Sub busquedaAvanzado()

        Try

            Dim IdPersona As Integer = 0
            Try
                IdPersona = CInt(Me.hddIdPersona.Value)
            Catch ex As Exception
                IdPersona = 0
            End Try

            Dim IdSerie As Integer = CInt(Me.cboSerie.SelectedValue)
            Dim fechaInicio As Date = CDate(Me.txtFechaInicio_BA.Text)
            Dim fechaFin As Date = CDate(Me.txtFechaFin_BA.Text)

            Me.GV_BusquedaAvanzado.DataSource = (New Negocio.Documento).DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(CInt(Me.hddIdTipoDocumento.Value), IdSerie, IdPersona, fechaInicio, fechaFin)
            Me.GV_BusquedaAvanzado.DataBind()

            If (Me.GV_BusquedaAvanzado.Rows.Count <= 0) Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  onCapa('capaBusquedaAvanzado');  alert('No se hallaron registros.');    ", True)
            Else
                objScript.onCapa(Me, "capaBusquedaAvanzado")
            End If



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub GV_BusquedaAvanzado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_BusquedaAvanzado.PageIndexChanging
        Me.GV_BusquedaAvanzado.PageIndex = e.NewPageIndex
        busquedaAvanzado()
    End Sub

    Protected Sub GV_BusquedaAvanzado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BusquedaAvanzado.SelectedIndexChanged
        Try
            '************  cargarDocumentoNotaCredito(0, 0, )
            buscarDocumento(0, 0, CInt(CType(Me.GV_BusquedaAvanzado.SelectedRow.FindControl("hddIdDocumento_BusquedaAvanzado"), HiddenField).Value))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

#End Region
    Private Sub btnAceptar_AddMedioPago_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar_AddMedioPago.Click
        validarLogeo_PermisoAddMedioPago()
    End Sub

    Private Sub validarLogeo_PermisoAddMedioPago()

        Try

            If (Me.txtClave_AddMedioPago.Text.Trim <> Me.txtClave2_AddMedioPago.Text.Trim) Then
                Throw New Exception("LAS CLAVES INGRESADAS NO COINCIDEN. NO SE PERMITE LA OPERACIÓN.")
            End If

            Dim IdUsuario As Integer = (New Negocio.Usuario).ValidarIdentidad(Me.txtUsuario_AddMedioPago.Text.Trim, Me.txtClave_AddMedioPago.Text.Trim)

            If (IdUsuario = Nothing) Then
                Throw New Exception("USUARIO Y/O CLAVE INCORRECTOS. NO SE PERMITE LA OPERACIÓN.")
            End If

            If ((New Negocio.Perfil_MedioPago).Perfil_MedioPago_ValidarxIdMedioPagoxIdUsuario(IdUsuario, CInt(Me.cboMedioPago.SelectedValue)) <= 0) Then

                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  alert('NO POSEE LOS PERMISOS NECESARIOS PARA INGRESAR ESTE MEDIO DE PAGO. NO SE PERMITE LA OPERACIÒN.');   verCapaPermiso_AddMedioPago();   ", True)
                Return

            End If

            Me.hddPermiso_AddMedioPago.Value = "1"
            objScript.mostrarMsjAlerta(Me, ("Se ha concedido la autorización para agregar el Medio de Pago < " + Me.cboMedioPago.SelectedItem.ToString + " >"))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click

        mostrarDocumentosRef_Find()

    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As String = ""
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdPersona As Integer = 0

            If (Me.hddIdPersona.Value.Trim.Length > 0 And IsNumeric(Me.hddIdPersona.Value)) Then
                If (CInt(Me.hddIdPersona.Value) > 0) Then
                    IdPersona = CInt(Me.hddIdPersona.Value)
                End If
            End If

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CStr(Me.txtCodigo_BuscarDocRef.Text)
            End Select

            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).Documento_BuscarDocumentoRef2(CInt(Me.cboEmpresa.SelectedValue), 0, _
                                                        IdPersona, CInt(Me.hddIdTipoDocumento.Value), serie, codigo, CInt(Me.CboTipoDocumento.SelectedValue), fechaInicio, fechafin, False)

            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged

        cargarDocumentoReferencia(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))

    End Sub

    Private Sub cargarDocumentoReferencia(ByVal IdDocumentoRef As Integer)

        Try

            validar_AddDocumentoRef(IdDocumentoRef)

            '***************** OBTENEMOS EL DOCUMENTO DE REFERENCIA
            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumentoRef)
            Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdPersona)

            '*************** GUARDAMOS EN SESSION
            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.listaDocumentoRef.Add(objDocumento)
            setlistaDocumentoRef(Me.listaDocumentoRef)

            '************** 
            cargarDocumentoRef_GUI(Me.listaDocumentoRef, objPersona)


            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

    Private Function validar_AddDocumentoRef(ByVal IdDocumentoRef As Integer) As Boolean

        Me.listaDocumentoRef = getlistaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            If (Me.listaDocumentoRef(i).Id = IdDocumentoRef) Then
                Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next

        Return True

    End Function

    Private Function obtenerListaDetalleDocumento_Ref(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)

        Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumento(IdDocumento)
        Dim objDocumentoRef As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)

        For i As Integer = (listaDetalle.Count - 1) To 0 Step -1

            If ((listaDetalle(i).IdTipoDocumento = 16 Or listaDetalle(i).IdTipoDocumento = 6)) Then  '**** SI EL DOCUMENTO DE REFERENCIA ES ORDEN DE COMPRA O ES UNA GUIA DE REMISION

                listaDetalle(i).Cantidad = listaDetalle(i).CantidadTransito

            End If

            If (listaDetalle(i).Cantidad = 0) Then

                listaDetalle.RemoveAt(i)

            Else

                listaDetalle(i).ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(listaDetalle(i).IdProducto)
                listaDetalle(i).IdDetalleAfecto = listaDetalle(i).IdDetalleDocumento
                listaDetalle(i).CantxAtender = Nothing
                listaDetalle(i).CantidadTransito = Nothing

            End If

        Next

        Return listaDetalle

    End Function
    Private Sub cargarDocumentoRef_GUI(ByVal listaDocumento As List(Of Entidades.Documento), ByVal objPersona As Entidades.PersonaView)

        '**************** LISTA DOC REFERENCIA
        Me.GV_DocumentoRef.DataSource = listaDocumento
        Me.GV_DocumentoRef.DataBind()

        '******************* PERSONA
        If (objPersona IsNot Nothing) Then

            With objPersona
                Me.txtDescripcionPersona.Text = .Descripcion
                Me.txtDNI.Text = .Dni
                Me.txtRUC.Text = .Ruc
                Me.hddIdPersona.Value = CStr(.IdPersona)
            End With

        End If


    End Sub

    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia")
        End If

    End Sub

    Private Sub GV_DocumentoRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoRef.SelectedIndexChanged
        quitarDocumentoRef(Me.GV_DocumentoRef.SelectedRow.RowIndex)
    End Sub
    Private Sub quitarDocumentoRef(ByVal index As Integer)

        Try

            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.listaDocumentoRef.RemoveAt(index)

            '********* GUARDAMOS EN SESSION
            setlistaDocumentoRef(Me.listaDocumentoRef)

            '************ GUI
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

#End Region


    Private Function Validar_AperturaCierreCaja(Optional ByVal showMessage As Boolean = True) As Boolean

        Dim objCombo As New Combo
        objCombo.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)
        If Me.cboCaja.Items.Count > 0 Then lblCaja.Text = (New Negocio.Caja_AperturaCierre).Caja_AperturaCierre_SelectEstado(CInt(Me.cboCaja.SelectedValue), Me.txtFechaEmision.Text.Trim)

        If Me.lblCaja.Text <> "Caja Aperturada" And showMessage Then
            objScript.mostrarMsjAlerta(Me, "La caja no esta apertutada.\nNo se permite la operación.")
            Return False
        End If

        Return True

    End Function

    Private Sub btBuscarPersonaGrillas_Click(sender As Object, e As System.EventArgs) Handles btBuscarPersonaGrillas.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", 0)  '******************** CLIENTE

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class