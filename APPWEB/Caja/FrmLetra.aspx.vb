﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmLetra
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    Private IdUsuario As Integer = 0

    '*************** ModoFrm
    '******** 0 : INICIO / nuevo - buscar
    '******** 1 : NUEVO / guardar - cancelar
    '******** 2 : EDITAR DOCUMENTO / guardar - cancelar
    '******** 3 : BUSCAR DOCUMENTO / buscar doc - habilitar busqueda
    '******** 4 : DOCUMENTO HALLADO CORRECTAMENTE / editar - anular - imprimir - despachar - remitir - cancelar
    '******** 5 : DOC. GUARDADO CORRECTAMENTE / imprimir - despachar - remitir - cancelar
    '******** 6: OCULTAR TODOS LOS BOTONES
    '**********************************************

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then
            ConfigurarDatos()
            inicializarFrm()
            validarPageRequest()
        End If

        '******** llenamos las monedas
        Me.lblMonedaMontoTotal.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMonedaTotalDocRef.Text = Me.cboMoneda.SelectedItem.ToString

    End Sub

    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION 
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {49, 50, 51, 52, 53})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumentoxCodigo.Enabled = True
        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumentoxCodigo.Enabled = False
        End If

        If listaPermisos(4) > 0 Then  '********* FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

    End Sub

    Private Sub validarPageRequest()
        Try

            If (Request.QueryString("IdDocumento") <> Nothing) Then

                cargarDocumentoLetra_FindxParams("4", CInt(Request.QueryString("IdDocumento")), 0, 0)

            Else

                verFrm("1", True, False)

            End If

            ValidarPermisos()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub inicializarFrm()
        Try

            '************** Obtenemos el Usuario desde session
            Try
                Me.IdUsuario = CInt(Session("IdUsuario"))
            Catch ex As Exception
                Me.IdUsuario = 0
            End Try

            Dim objCbo As New Combo
            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))

                .LlenarCboMoneda(Me.cboMoneda, False)

                .LlenarCboBanco(Me.cboBanco, True)
                .LlenarCboOficinaxIdBanco(Me.cboOficina, CInt(Me.cboBanco.SelectedValue), True)
                .LlenarCboCuentaBancaria(Me.cboNroCuenta, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), True)
                .LLenarCboEstadoDocumento(Me.cboEstado)
                .llenarCboTipoDocumentoxIdTipoDocumentos(Me.cboTipoDocumentoRef, "20", 2, False)
                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.hddIdTipoDocumento.Value), True)

                '************   ROL
                .LlenarCboRol(Me.cboRol, True)
                If (Me.cboRol.Items.FindByValue("1") IsNot Nothing) Then
                    Me.cboRol.SelectedValue = "1"  '************ CLIENTE
                End If

            End With

            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.txtFechaI.Text = Me.txtFechaEmision.Text
            Me.txtFechaF.Text = Me.txtFechaEmision.Text

            Me.txtFechaFin_DocRef.Text = Me.txtFechaEmision.Text
            Me.txtFechaInicio_DocRef.Text = Me.txtFechaEmision.Text

            '******** Agregamos un detalle Inicial
            addLetra_Detalle(1)

            actualizarOpcionesBusquedaDocRef(0, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub verFrm(ByVal modoFrm As String, ByVal generarCodigoDoc As Boolean, ByVal limpiarControles As Boolean)

        If (limpiarControles) Then
            limpiarControlesFrm()
        End If

        If (generarCodigoDoc) Then
            GenerarCodigoDocumento()
        End If

        '************** MODO FRM
        hddFrmModo.Value = modoFrm
        ActualizarBotonesControl()

    End Sub

    Private Sub limpiarControlesFrm()

        '********* CABECERA
        Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.cboEstado.SelectedValue = "1"  '****** ACTIVO

        '********* CLIENTE
        Me.txtDescripcionPersona.Text = ""
        Me.txtDNI.Text = ""
        Me.txtRUC.Text = ""
        Me.hddIdPersona.Value = ""

        '************ DOC REFERENCIA
        Me.GV_DocReferencia.DataSource = Nothing
        Me.GV_DocReferencia.DataBind()
        Me.GV_DocRef.DataBind()
        Me.txtTotalDocRef.Text = "0"

        '*********** LETRAS
        Me.GV_Letras.DataSource = Nothing
        Me.GV_Letras.DataBind()
        addLetra_Detalle(1)  '******** Agregamos un detalle Inicial
        Me.txtMontoTotal.Text = "0"

        '*********** CHECK
        Me.chbDC.Checked = False
        Me.chbProtestado.Checked = False
        Me.chbRenegociado.Checked = False
        Me.chbEnCartera.Checked = False
        Me.chbEnCobranza.Checked = False

        '************* AVAL
        Me.txtDescripcionAval.Text = ""
        Me.txtDNIAval.Text = ""
        Me.txtRUCAval.Text = ""
        Me.hddIdAval.Value = ""

        '*********** Auxiliares
        Me.hddIdDocumento.Value = ""
        Me.hddOpcionBuscarPersona.Value = "0"  '************ BUSQUEDA DE PERSONA
        Me.hddCodigoDocumento.Value = ""
        Me.hddContAmortizaciones.Value = ""

        '************ IMPRESION
        Me.GV_Imprimir.DataSource = Nothing
        Me.GV_Imprimir.DataBind()

        Me.GV_BusquedaAvanzado.DataSource = Nothing
        Me.GV_BusquedaAvanzado.DataBind()

    End Sub

    Private Sub GenerarCodigoDocumento()
        If (IsNumeric(Me.cboSerie.SelectedValue)) Then
            Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
        Else
            Me.txtCodigoDocumento.Text = ""
        End If
    End Sub

    Private Sub ActualizarBotonesControl()
        
        Me.btnBuscarPersona.Enabled = True
        Me.btnAddDocumentoReferencia.Enabled = True
        Me.btnAddLetra.Enabled = True

        Select Case CInt(hddFrmModo.Value)

            Case 0 '************* INICIO
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnRenegociar.Visible = False

                Me.Panel_Cabecera.Enabled = False
                Me.Panel_Aval.Enabled = False
                Me.Panel_Banco.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_DocReferencia.Enabled = False
                Me.Panel_Letras.Enabled = False


                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboMoneda.Enabled = True
                Me.cboTipoOperacion.Enabled = True

            Case 1 '************** Nuevo
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnRenegociar.Visible = False

                Me.Panel_Cabecera.Enabled = True
                Me.Panel_Aval.Enabled = True
                Me.Panel_Banco.Enabled = True
                Me.Panel_Cliente.Enabled = True
                Me.Panel_DocReferencia.Enabled = True
                Me.Panel_Letras.Enabled = True



                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboMoneda.Enabled = True
                Me.cboTipoOperacion.Enabled = True

            Case 2 '*********************** Editar
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnRenegociar.Visible = False

                Me.Panel_Cabecera.Enabled = True
                Me.Panel_Aval.Enabled = True
                Me.Panel_Banco.Enabled = True
                Me.Panel_Cliente.Enabled = True
                Me.Panel_DocReferencia.Enabled = True
                Me.Panel_Letras.Enabled = True



                If (CInt(Me.cboEstado.SelectedValue) <> 3) Then

                    Me.btnBuscarPersona.Enabled = False
                    Me.btnAddDocumentoReferencia.Enabled = False
                    Me.cboMoneda.Enabled = False
                Else

                    Me.cboMoneda.Enabled = True

                End If

                Me.btnAddLetra.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboTipoOperacion.Enabled = True

            Case 3 '**************************** Buscar documento
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = False
                Me.txtCodigoDocumento.Focus()
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnRenegociar.Visible = False

                Me.Panel_Cabecera.Enabled = True
                Me.Panel_Aval.Enabled = False
                Me.Panel_Banco.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_DocReferencia.Enabled = False
                Me.Panel_Letras.Enabled = False



                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboMoneda.Enabled = True
                Me.cboTipoOperacion.Enabled = True

            Case 4 '********* documento hallado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True
                Me.btnImprimir.Visible = True
                Me.btnRenegociar.Visible = True

                Me.Panel_Cabecera.Enabled = False
                Me.Panel_Aval.Enabled = False
                Me.Panel_Banco.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_DocReferencia.Enabled = False
                Me.Panel_Letras.Enabled = False



                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


                Me.cboMoneda.Enabled = False
                Me.cboTipoOperacion.Enabled = False

            Case 5 '********* documento guardado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnRenegociar.Visible = False
                Me.btnImprimir.Visible = True

                Me.Panel_Cabecera.Enabled = False
                Me.Panel_Aval.Enabled = False
                Me.Panel_Banco.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_DocReferencia.Enabled = False
                Me.Panel_Letras.Enabled = False




                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


                Me.cboMoneda.Enabled = False
                Me.cboTipoOperacion.Enabled = False

            Case 6  '*********** Anulacion exitosa

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnRenegociar.Visible = False

                Me.Panel_Cabecera.Enabled = False
                Me.Panel_Aval.Enabled = False
                Me.Panel_Banco.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_DocReferencia.Enabled = False
                Me.Panel_Letras.Enabled = False



                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboMoneda.Enabled = False
                Me.cboTipoOperacion.Enabled = False


            Case 7  '*********** RENEGOCIAR

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnRenegociar.Visible = False
                Me.btnAddLetra.Enabled = False

                Me.Panel_Cabecera.Enabled = True
                Me.Panel_Aval.Enabled = False
                Me.Panel_Banco.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_DocReferencia.Enabled = False
                Me.Panel_Letras.Enabled = True



                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboMoneda.Enabled = False
                Me.cboTipoOperacion.Enabled = True

        End Select
    End Sub

    Protected Sub btnQuitarLetra_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        quitarLetra(CType(sender, ImageButton))
    End Sub

    Protected Sub quitarLetra(ByVal btnQuitarLetra As ImageButton)
        Try

            If (CInt(Me.hddFrmModo.Value) = 2 Or CInt(Me.hddFrmModo.Value) = 7) Then
                Throw New Exception("NO SE PERMITE LA OPERACIÓN.")
            End If

            Dim gvRow As GridViewRow = CType(btnQuitarLetra.NamingContainer, GridViewRow)
            Dim lista As List(Of Entidades.DocLetra) = obtenerLetras_Detalle(0)

            lista.RemoveAt(gvRow.RowIndex)

            Me.GV_Letras.DataSource = lista
            Me.GV_Letras.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " calcularTotales(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnQuitarDocReferencia_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        quitarDocumentoReferencia(CType(sender, ImageButton))
    End Sub

    Protected Sub quitarDocumentoReferencia(ByVal btnQuitarDocumentoRef As ImageButton)
        Try


            If (CInt(Me.cboEstado.SelectedValue) <> 3 And CInt(Me.hddFrmModo.Value) = 2) Then  '**** EDITAR
                Throw New Exception("NO SE PERMITE LA OPERACIÓN.")
            End If

            Dim gvRow As GridViewRow = CType(btnQuitarDocumentoRef.NamingContainer, GridViewRow)
            Dim lista As List(Of Entidades.Documento_MovCuenta) = obtenerListaDocumentosReferencia(1) '*** Lista Detalle Doc Referencia

            lista.RemoveAt(gvRow.RowIndex)

            Me.GV_DocReferencia.DataSource = lista
            Me.GV_DocReferencia.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularTotales(); calcularTotalDocRef();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged

        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))

            GenerarCodigoDocumento()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try



    End Sub

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Try
            Dim objCombo As New Combo
            objCombo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        Try
            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub addLetra_Detalle(ByVal cantidad As Integer)
        Try

            Dim TotalDocumentoVenta As Decimal
            If IsNumeric(Me.txtTotalDocRef.Text) Then TotalDocumentoVenta = CDec(Me.txtTotalDocRef.Text)


            '********** Agregamos el nro de letras solicitados
            Dim lista As List(Of Entidades.DocLetra) = obtenerLetras_Detalle(0)

            Dim TotalLista As Integer = lista.Count

            For i As Integer = TotalLista To cantidad - 1

                Dim objDocumento As New Entidades.DocLetra
                With objDocumento

                    .NomMoneda = Me.cboMoneda.SelectedItem.ToString
                    .TotalAPagar = 0
                    .FechaVenc = (New Negocio.FechaActual).SelectFechaActual
                    .Observaciones = ""

                End With
                lista.Add(objDocumento)

            Next

            TotalLista = lista.Count

            If TotalDocumentoVenta > 0 Then
                For i As Integer = 0 To TotalLista - 1
                    With lista(i)
                        .TotalAPagar = TotalDocumentoVenta / TotalLista
                    End With
                Next
            End If

            Me.GV_Letras.DataSource = lista
            Me.GV_Letras.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " calcularTotales(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function obtenerLetras_Detalle(ByVal initRecorrido As Integer) As List(Of Entidades.DocLetra)

        Dim lista As New List(Of Entidades.DocLetra)

        For i As Integer = initRecorrido To Me.GV_Letras.Rows.Count - 1

            Dim objDocumento As New Entidades.DocLetra
            With objDocumento

                '*********************.NomMoneda = CType(Me.GV_Letras.Rows(i).FindControl("lblMoneda"), Label).Text
                .TotalAPagar = CDec(CType(Me.GV_Letras.Rows(i).FindControl("txtMonto"), TextBox).Text)
                .FechaVenc = CDate(CType(Me.GV_Letras.Rows(i).FindControl("txtFechaVcto"), TextBox).Text)
                .Observaciones = CType(Me.GV_Letras.Rows(i).FindControl("txtObservaciones"), TextBox).Text
                .NroDocumento = CType(Me.GV_Letras.Rows(i).FindControl("lblNroDocumento"), Label).Text

                If IsNumeric(CType(Me.GV_Letras.Rows(i).FindControl("hddIdDocumentoLetra"), HiddenField).Value) Then
                    .IdDocumento = CInt(CType(Me.GV_Letras.Rows(i).FindControl("hddIdDocumentoLetra"), HiddenField).Value)
                End If

                If IsNumeric(CType(Me.GV_Letras.Rows(i).FindControl("hddIdDocumento"), HiddenField).Value) Then
                    .Id = CInt(CType(Me.GV_Letras.Rows(i).FindControl("hddIdDocumento"), HiddenField).Value)
                End If


            End With
            lista.Add(objDocumento)

        Next

        Return lista

    End Function

    Private Sub cboBanco_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBanco.SelectedIndexChanged
        cargarDatosBanco(CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue))
    End Sub
    Private Sub cargarDatosBanco(ByVal IdBanco As Integer, ByVal IdEmpresa As Integer)
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboCuentaBancaria(Me.cboNroCuenta, IdBanco, IdEmpresa, True)
            objCbo.LlenarCboOficinaxIdBanco(Me.cboOficina, IdBanco, True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAddLetra_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddLetra.Click

        Dim CantidadLetra As Decimal = 0
        If IsNumeric(Me.txtNroLetra.Text) Then CantidadLetra = Math.Ceiling(CDec(Me.txtNroLetra.Text))
        addLetra_Detalle(CInt(CantidadLetra))
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click

        registrarDocumentoLetra()

    End Sub

    Private Function obtenerListaDocumentoCabLetra() As List(Of Entidades.DocLetra)

        Dim ListaDocumentoLetra As New List(Of Entidades.DocLetra)

        For i As Integer = 0 To GV_Letras.Rows.Count - 1

            '*********** Obtengo un documento letra x cada una de las filas
            Dim objDocumento As New Entidades.DocLetra

            With objDocumento

                Select Case CInt(Me.hddFrmModo.Value)
                    Case 1 '**** NUEVO
                        .Id = Nothing
                    Case 2 '**** EDITAR
                        '.Id = CInt(Me.hddIdDocumento.Value)
                        .Id = CInt(CType(Me.GV_Letras.Rows(i).FindControl("hddIdDocumento"), HiddenField).Value)
                    Case 7 '**** RENEGOCIAR

                        If IsNumeric(CType(Me.GV_Letras.Rows(i).FindControl("hddIdDocumento"), HiddenField).Value) Then
                            .Id = CInt(CType(Me.GV_Letras.Rows(i).FindControl("hddIdDocumento"), HiddenField).Value)
                        Else
                            .Id = 0
                        End If

                End Select

                .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
                .IdTienda = CInt(Me.cboTienda.SelectedValue)
                .IdSerie = CInt(Me.cboSerie.SelectedValue)
                .Serie = Me.cboSerie.SelectedItem.ToString
                .Codigo = Me.txtCodigoDocumento.Text
                .FechaEmision = CDate(Me.txtFechaEmision.Text)
                .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
                .IdEstadoDoc = 1 ' Estado Activo
                .IdTipoDocumento = CInt(Me.hddIdTipoDocumento.Value)
                .IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)


                .getLetraCambio.IdBanco = CInt(Me.cboBanco.SelectedValue)
                .getLetraCambio.IdOficina = CInt(Me.cboOficina.SelectedValue)
                .getLetraCambio.IdCuentaBancaria = CInt(Me.cboNroCuenta.SelectedValue)

                

                Try
                    .getLetraCambio.IdAval = CInt(Me.hddIdAval.Value)
                Catch ex As Exception
                    .getLetraCambio.IdAval = Nothing
                End Try

                .IdPersona = CInt(Me.hddIdPersona.Value)

                Try
                    If CInt(Me.cboBanco.SelectedValue) = 0 And CInt(Me.cboOficina.SelectedValue) = 0 And _
                       CInt(Me.cboNroCuenta.SelectedValue) = 0 And Me.chbDC.Checked = False And _
                       Me.chbProtestado.Checked = False And Me.chbRenegociado.Checked = False Then

                        Me.chbEnCartera.Checked = True
                        Me.chbEnCobranza.Checked = False

                    End If

                    If CInt(Me.cboBanco.SelectedValue) > 0 And CInt(Me.cboOficina.SelectedValue) > 0 And _
                       CInt(Me.cboNroCuenta.SelectedValue) > 0 And Me.chbDC.Checked = False And _
                       Me.chbProtestado.Checked = False And Me.chbRenegociado.Checked = False Then

                        Me.chbEnCartera.Checked = False
                        Me.chbEnCobranza.Checked = True

                    End If

                Catch ex As Exception

                End Try

                '********** Verificamos los Checks propios de la Letra
                If (CInt(Me.hddFrmModo.Value) = 7) Then '******* RENEGOCIAR

                    .getLetraCambio.lc_EnCartera = False
                    .getLetraCambio.DebitoCuenta = False
                    .getLetraCambio.lc_EnCobranza = False
                    .getLetraCambio.Protestado = False
                    .getLetraCambio.Renegociado = False

                Else

                    .getLetraCambio.lc_EnCartera = Me.chbEnCartera.Checked
                    .getLetraCambio.DebitoCuenta = Me.chbDC.Checked
                    .getLetraCambio.lc_EnCobranza = Me.chbEnCobranza.Checked
                    .getLetraCambio.Protestado = Me.chbProtestado.Checked
                    .getLetraCambio.Renegociado = Me.chbRenegociado.Checked

                End If


                '***** Datos independientes x Letras
                .Total = CDec(CType(Me.GV_Letras.Rows(i).FindControl("txtMonto"), TextBox).Text)
                .TotalAPagar = CDec(CType(Me.GV_Letras.Rows(i).FindControl("txtMonto"), TextBox).Text)
                .FechaVenc = CDate(CType(Me.GV_Letras.Rows(i).FindControl("txtFechaVcto"), TextBox).Text)
                .Observaciones = CType(Me.GV_Letras.Rows(i).FindControl("txtObservaciones"), TextBox).Text

                If IsNumeric(CType(Me.GV_Letras.Rows(i).FindControl("hddIdDocumentoLetra"), HiddenField).Value) Then
                    .IdDocumento = CInt(CType(Me.GV_Letras.Rows(i).FindControl("hddIdDocumentoLetra"), HiddenField).Value)
                End If

                .TotalLetras = (New Aletras).Letras(CStr(.TotalAPagar))

                .TotalLetras = (.TotalLetras + "/100 " + (New Negocio.Moneda).SelectxId(.IdMoneda)(0).Descripcion).ToUpper

                '****** MANEJO DE ESTADOS
                .IdCondicionPago = 2 '**** CREDITO

                '********** Datos EXTRA
                .NomMoneda = Me.cboMoneda.SelectedItem.ToString
                .NomTipoDocumento = "Letra"

            End With

            ListaDocumentoLetra.Add(objDocumento)

        Next



        Return ListaDocumentoLetra

    End Function


    Private Sub btnAddDocumentoReferencia_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddDocumentoReferencia.Click
        mostrarDocumentosCredito_Letras()
    End Sub
    Private Sub mostrarDocumentosCredito_Letras()
        Try

            '************ DEBE TRABAJAR SOLO CON LOS DOCUMENTOS A CREDITO LETRAS
            '********* x el momento obtengo todos
            Dim lista As List(Of Entidades.Documento_MovCuenta) = (New Negocio.MovCuenta).SelectDeudaxIdPersona_NoLetras(CInt(Me.hddIdPersona.Value), CInt(Me.cboMoneda.SelectedValue))
            If (lista.Count <= 0) Then
                objScript.mostrarMsjAlerta(Me, "No se hallaron registros.")
            Else
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub btnAddDocumentoReferencia_Find_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddDocumentoReferencia_Find.Click
        cargarDocumentosReferencia()
    End Sub
    Private Sub cargarDocumentosReferencia()
        Try

            Dim listaDocumentosRef As List(Of Entidades.Documento_MovCuenta) = obtenerListaDocumentosRef_Frm(obtenerListaDocumentosReferencia(1), obtenerListaDocumentosReferencia(0))


            Me.GV_DocReferencia.DataSource = listaDocumentosRef
            Me.GV_DocReferencia.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularTotales();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function obtenerListaDocumentosRef_Frm(ByVal listaDetalle As List(Of Entidades.Documento_MovCuenta), ByVal listaFind As List(Of Entidades.Documento_MovCuenta)) As List(Of Entidades.Documento_MovCuenta)

        '*************** VALIDAMOS
        For i As Integer = 0 To listaDetalle.Count - 1

            For j As Integer = 0 To listaFind.Count - 1

                If (listaDetalle(i).Id = listaFind(j).Id) Then
                    Throw New Exception("EL DOCUMENTO < " + listaDetalle(i).NomTipoDocumento + " " + listaDetalle(i).getNroDocumento + " > YA HA SIDO INGRESADO COMO DOCUMENTO DE REFERENCIA. NO SE PERMITE LA OPERACIÓN.")
                End If

            Next

        Next

        listaDetalle.AddRange(listaFind)

        Return listaDetalle

    End Function
    Private Function obtenerListaDocumentosReferencia(ByVal opcion As Integer) As List(Of Entidades.Documento_MovCuenta)

        Dim lista As New List(Of Entidades.Documento_MovCuenta)

        Select Case opcion
            Case 0 '********* FIND
                For i As Integer = 0 To Me.GV_DocumentosReferencia_Find.Rows.Count - 1

                    If CType(Me.GV_DocumentosReferencia_Find.Rows(i).FindControl("chbAddDocumentoRef_Find"), CheckBox).Checked Then

                        Dim objDocumento As New Entidades.Documento_MovCuenta
                        With objDocumento

                            .NomTipoDocumento = Me.GV_DocumentosReferencia_Find.Rows(i).Cells(1).Text
                            .NroDocumento = CType(Me.GV_DocumentosReferencia_Find.Rows(i).FindControl("lblNroDocumento_Find"), Label).Text
                            .IdMovCuenta = CInt(CType(Me.GV_DocumentosReferencia_Find.Rows(i).FindControl("hddIdMovCuentaReferencia_Find"), HiddenField).Value)
                            .Id = CInt(CType(Me.GV_DocumentosReferencia_Find.Rows(i).FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value)
                            .FechaEmision = CDate(Me.GV_DocumentosReferencia_Find.Rows(i).Cells(3).Text)

                            Try
                                .FechaVenc = CDate(Me.GV_DocumentosReferencia_Find.Rows(i).Cells(4).Text)
                            Catch ex As Exception
                                .FechaVenc = Nothing
                            End Try


                            .NomMoneda = CType(Me.GV_DocumentosReferencia_Find.Rows(i).FindControl("lblMonedaMontoTotal_Find"), Label).Text
                            .MontoTotal = CDec(CType(Me.GV_DocumentosReferencia_Find.Rows(i).FindControl("lblMontoTotal_Find"), Label).Text)
                            .Saldo = CDec(CType(Me.GV_DocumentosReferencia_Find.Rows(i).FindControl("lblSaldo_Find"), Label).Text)
                            .NuevoSaldo = .Saldo

                        End With
                        lista.Add(objDocumento)
                    End If

                Next

            Case 1 '********* Detalle Documentos Referencia

                For i As Integer = 0 To Me.GV_DocReferencia.Rows.Count - 1

                    Dim objDocumento As New Entidades.Documento_MovCuenta
                    With objDocumento

                        .NomTipoDocumento = Me.GV_DocReferencia.Rows(i).Cells(1).Text
                        .NroDocumento = CType(Me.GV_DocReferencia.Rows(i).FindControl("lblNroDocumento"), Label).Text


                        .IdMovCuenta = CInt(CType(Me.GV_DocReferencia.Rows(i).FindControl("hddIdMovCuentaReferencia"), HiddenField).Value)
                        .Id = CInt(CType(Me.GV_DocReferencia.Rows(i).FindControl("hddIdDocumentoReferencia"), HiddenField).Value)

                        .FechaEmision = CDate(Me.GV_DocReferencia.Rows(i).Cells(3).Text)

                        Try
                            .FechaVenc = CDate(Me.GV_DocReferencia.Rows(i).Cells(4).Text)
                        Catch ex As Exception
                            .FechaVenc = Nothing
                        End Try


                        .NomMoneda = CType(Me.GV_DocReferencia.Rows(i).FindControl("lblMonedaMontoTotal"), Label).Text
                        .MontoTotal = CDec(CType(Me.GV_DocReferencia.Rows(i).FindControl("lblMontoTotal"), Label).Text)
                        .Saldo = CDec(CType(Me.GV_DocReferencia.Rows(i).FindControl("lblSaldo"), Label).Text)
                        .NuevoSaldo = CDec(CType(Me.GV_DocReferencia.Rows(i).FindControl("txtSaldo"), TextBox).Text)

                    End With
                    lista.Add(objDocumento)

                Next

        End Select


        Return lista
    End Function

    Private Function obtenerListaDocLetra_Print(ByVal ListaDocumentoLetra As List(Of Entidades.DocLetra)) As List(Of Entidades.DocLetra)

        Dim listaLetras As New List(Of Entidades.DocLetra)

        For i As Integer = 0 To ListaDocumentoLetra.Count - 1

            Dim objDocLetra As Entidades.DocLetra = (New Negocio.DocLetra).DocumentoLetraSelectCabtxId_Print(ListaDocumentoLetra(i).Id)
            listaLetras.Add(objDocLetra)

        Next

        Return listaLetras

    End Function
    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        verFrm("1", True, True)
    End Sub




    Private Sub cargarDocumentoLetra_GUI(ByVal objDocumentoLetra As Entidades.DocLetra, ByVal listaDocRef As List(Of Entidades.Documento_MovCuenta), ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto))

        With objDocumentoLetra

            '********** Configuracion Documento
            If (Me.cboEmpresa.Items.FindByValue(CStr(.IdEmpresa)) IsNot Nothing) Then
                Me.cboEmpresa.SelectedValue = CStr(.IdEmpresa)
            Else
                Throw New Exception("EL DOCUMENTO SELECCIONADO POSEE UNA EMPRESA LA CUAL NO SE ENCUENTRA DISPONIBLE EN LA LISTA DESPLEGABLE. NO SE PERMITE LA OPERACIÓN.")
            End If

            Dim objCbo As New Combo
            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)

            If (Me.cboTienda.Items.FindByValue(CStr(.IdTienda)) IsNot Nothing) Then
                Me.cboTienda.SelectedValue = CStr(.IdTienda)
            Else
                Throw New Exception("EL DOCUMENTO SELECCIONADO POSEE UNA TIENDA LA CUAL NO SE ENCUENTRA DISPONIBLE EN LA LISTA DESPLEGABLE. NO SE PERMITE LA OPERACIÓN.")
            End If

            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
            If (Me.cboSerie.Items.FindByValue(CStr(.IdSerie)) IsNot Nothing) Then
                Me.cboSerie.SelectedValue = CStr(.IdSerie)
            Else
                Throw New Exception("EL DOCUMENTO SELECCIONADO POSEE UNA SERIE LA CUAL NO SE ENCUENTRA DISPONIBLE EN LA LISTA DESPLEGABLE. NO SE PERMITE LA OPERACIÓN.")
            End If

            Me.txtCodigoDocumento.Text = .Codigo

            If (.FechaEmision <> Nothing) Then
                Me.txtFechaEmision.Text = Format(.FechaEmision, "dd/MM/yyyy")
            End If

            If (Me.cboMoneda.Items.FindByValue(CStr(.IdMoneda)) IsNot Nothing) Then
                Me.cboMoneda.SelectedValue = CStr(.IdMoneda)
            End If

            If (Me.cboEstado.Items.FindByValue(CStr(.IdEstadoDoc)) IsNot Nothing) Then
                Me.cboEstado.SelectedValue = CStr(.IdEstadoDoc)
            End If

            If (Me.cboTipoOperacion.Items.FindByValue(CStr(.IdTipoOperacion)) IsNot Nothing) Then
                Me.cboTipoOperacion.SelectedValue = CStr(.IdTipoOperacion)
            End If

            '********** Letras
            Me.hddIdDocumento.Value = CStr(.Id)
            Me.hddContAmortizaciones.Value = CStr(.ContAmortizaciones)

            Dim listaLetras As List(Of Entidades.DocLetra) = (New Negocio.DocLetra).Anexar_Letra(objDocumentoLetra.Id)


            For i As Integer = 0 To listaLetras.Count - 1

                listaLetras(i) = (New Negocio.DocLetra).DocumentoLetraSelectCabxParams(listaLetras(i).Id, 0, 0)

            Next

            Me.GV_Letras.DataSource = listaLetras
            Me.GV_Letras.DataBind()

            '******** Doc Ref
            Me.GV_DocReferencia.DataSource = listaDocRef
            Me.GV_DocReferencia.DataBind()



            '********* Agencia Bancaria
            If (Me.cboBanco.Items.FindByValue(CStr(.getLetraCambio.IdBanco)) IsNot Nothing) Then
                Me.cboBanco.SelectedValue = CStr(.getLetraCambio.IdBanco)
            End If

            objCbo.LlenarCboOficinaxIdBanco(Me.cboOficina, CInt(Me.cboBanco.SelectedValue), True)

            If (Me.cboOficina.Items.FindByValue(CStr(.getLetraCambio.IdOficina)) IsNot Nothing) Then
                Me.cboOficina.SelectedValue = CStr(.getLetraCambio.IdOficina)
            End If

            objCbo.LlenarCboCuentaBancaria(Me.cboNroCuenta, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), True)

            If (Me.cboNroCuenta.Items.FindByValue(CStr(.getLetraCambio.IdCuentaBancaria)) IsNot Nothing) Then
                Me.cboNroCuenta.SelectedValue = CStr(.getLetraCambio.IdCuentaBancaria)
            End If

            If (.IdEstadoDoc <> 3) Then

                '********** Cliente
                If (.IdEstadoDoc <> 3) Then
                    Me.txtDescripcionPersona.Text = .getPersona_Cliente.Descripcion
                    Me.txtDNI.Text = .getPersona_Cliente.Dni
                    Me.txtRUC.Text = .getPersona_Cliente.Ruc
                    Me.hddIdPersona.Value = CStr(.getPersona_Cliente.IdPersona)
                End If

                '******* Flag Letra Cambio
                Me.chbDC.Checked = .getLetraCambio.DebitoCuenta
                Me.chbProtestado.Checked = .getLetraCambio.Protestado
                Me.chbRenegociado.Checked = .getLetraCambio.Renegociado
                Me.chbEnCartera.Checked = .getLetraCambio.lc_EnCartera
                Me.chbEnCobranza.Checked = .getLetraCambio.lc_EnCobranza

                '****** Datos del Aval
                Me.txtDescripcionAval.Text = .getPersona_Aval.Descripcion
                Me.txtDNIAval.Text = .getPersona_Aval.Dni
                Me.txtRUCAval.Text = .getPersona_Aval.Ruc
                Me.hddIdAval.Value = CStr(.getPersona_Aval.IdPersona)

            End If

        End With

    End Sub

    Private Sub btnBuscarDocumentoxCodigo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumentoxCodigo.Click
        cargarDocumentoLetra_FindxParams("4", 0, CInt(Me.cboSerie.SelectedValue), CInt(Me.hddCodigoDocumento.Value))
    End Sub

    Protected Sub btnEditar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEditar.Click
        cargarDocumentoLetra_FindxParams("2", CInt(Me.hddIdDocumento.Value), 0, 0)
    End Sub
    Private Sub cargarDocumentoLetra_FindxParams(ByVal FrmModo As String, ByVal IdDocumento As Integer, ByVal IdSerie As Integer, ByVal Codigo As Integer)

        Try

            '********** Obtenemos el Documento Letra
            Dim objDocumentoLetra As Entidades.DocLetra = (New Negocio.DocLetra).DocumentoLetraSelectCabxParams(IdDocumento, IdSerie, Codigo)
            Dim listaDocReferencia As List(Of Entidades.Documento_MovCuenta) = (New Negocio.DocLetra).DocumentoLetraSelectDocReferenciaxIdDocumentoLetra(objDocumentoLetra.Id, objDocumentoLetra.IdMoneda)
            Dim listaDetalleConcepto As List(Of Entidades.DetalleConcepto)

            If listaDocReferencia IsNot Nothing Then

                listaDetalleConcepto = (New Negocio.DetalleConcepto).SelectxIdDocumento(objDocumentoLetra.Id)
                Dim Index As Integer

                If listaDetalleConcepto IsNot Nothing Then

                    If listaDetalleConcepto.Count > 0 Then
                        For i As Integer = 0 To listaDocReferencia.Count - 1
                            Index = i
                            listaDocReferencia(i).NuevoSaldo = listaDetalleConcepto.Find(Function(ent As Entidades.DetalleConcepto) ent.IdDocumentoRef = listaDocReferencia(Index).Id).Monto
                        Next
                    End If

                End If

            End If


            limpiarControlesFrm() '********* Limpiamos antes de cargar los datos
            cargarDocumentoLetra_GUI(objDocumentoLetra, listaDocReferencia, listaDetalleConcepto) '**** Cargamos el Doc Letra en la GUI

            verFrm(FrmModo, False, False)

      

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " calcularTotales(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Private Sub registrarDocumentoLetra()
        Try

            Select Case CInt(Me.hddFrmModo.Value)
                Case 1 '******** NUEVO

                    '****** registramos los doc. letras / letra cambio / relacion documento / programamos los pagos            
                    Dim listaDocumentoRef As List(Of Entidades.Documento_MovCuenta) = obtenerListaDocumentosReferencia(1)
                    Dim listaDocumentoLetra As List(Of Entidades.DocLetra) = obtenerListaDocumentoCabLetra()

                    Dim ListaDocumentoLetra_Save As List(Of Entidades.DocLetra) = (New Negocio.DocLetra).registrarLetras(listaDocumentoLetra, listaDocumentoRef)

                    Dim listaDocumentoLetra_Print As List(Of Entidades.DocLetra) = obtenerListaDocLetra_Print(ListaDocumentoLetra)

                    Me.GV_Imprimir.DataSource = listaDocumentoLetra_Print
                    Me.GV_Imprimir.DataBind()

                Case 2 '****** EDITAR
                    '****** actualizamos el Doc. Letra / Letra Cambio / relación documento / pagos programados

                    Dim listaDocumentoRef As List(Of Entidades.Documento_MovCuenta) = obtenerListaDocumentosReferencia(1)
                    Dim listaDocumentoLetra As List(Of Entidades.DocLetra) = obtenerListaDocumentoCabLetra()

                    'ANTES
                    'Dim objDocumentoLetra As Entidades.DocLetra = obtenerListaDocumentoCabLetra()(0)  '*** Obtenemos el Documento para Edición.

                    If ((New Negocio.DocLetra).ActualizarLetras(listaDocumentoLetra, obtenerListaDocumentosReferencia(1))) Then


                        Dim listaDocumentoLetra_Print As List(Of Entidades.DocLetra) = obtenerListaDocLetra_Print(listaDocumentoLetra)

                        Me.GV_Imprimir.DataSource = listaDocumentoLetra_Print
                        Me.GV_Imprimir.DataBind()

                    Else
                        Throw New Exception("Problemas en la actualización del Documento.")
                    End If
                Case 7 '****** RENEGOCIAR

                    '******** Obtenemos una vez más los documentos de Referencia
                    Dim listaDocReferencia As List(Of Entidades.Documento_MovCuenta) = (New Negocio.DocLetra).DocumentoLetraSelectDocReferenciaxIdDocumentoLetra(CInt(Me.hddIdDocumento.Value), CInt(Me.cboMoneda.SelectedValue))
                    Me.GV_DocReferencia.DataSource = listaDocReferencia
                    Me.GV_DocReferencia.DataBind()

                    '********************** Renegociamos las Letras
                    Dim listaLetraCab As List(Of Entidades.DocLetra) = obtenerListaDocumentoCabLetra()

                    listaLetraCab.RemoveAll(Function(ent As Entidades.DocLetra) ent.Id > 0) '**** Removemos la Letra x Renegociar

                    Dim objLetraCambioxRenegociar As Entidades.LetraCambio = obtenerLetraCambioxRenegociar(CInt(Me.hddIdDocumento.Value))
                    objLetraCambioxRenegociar.Renegociado = True

                    Dim ListaDocumentoLetra As List(Of Entidades.DocLetra) = (New Negocio.DocLetra).DocumentoLetra_Renegociar(CInt(Me.hddIdDocumento.Value), objLetraCambioxRenegociar, listaLetraCab, obtenerListaDocumentosReferencia(1))
                    Dim listaDocumentoLetra_Print As List(Of Entidades.DocLetra) = obtenerListaDocLetra_Print(ListaDocumentoLetra)

                    Me.GV_Imprimir.DataSource = listaDocumentoLetra_Print
                    Me.GV_Imprimir.DataBind()

            End Select

            '******* mostramos la capa de impresion
            verFrm("5", False, False)
            objScript.onCapa(Me, "capaImprimir")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function obtenerLetraCambioxRenegociar(ByVal IdDocumento As Integer) As Entidades.LetraCambio

        Dim obj As New Entidades.LetraCambio
        With obj

            .IdDocumento = IdDocumento
            .DebitoCuenta = Me.chbDC.Checked
            .Renegociado = Me.chbRenegociado.Checked
            .Protestado = Me.chbProtestado.Checked
            .lc_EnCartera = Me.chbEnCartera.Checked
            .lc_EnCobranza = Me.chbEnCobranza.Checked
            .IdBanco = CInt(Me.cboBanco.SelectedValue)
            .IdCuentaBancaria = CInt(Me.cboNroCuenta.SelectedValue)
            .IdOficina = CInt(Me.cboOficina.SelectedValue)
            Try
                .IdAval = CInt(Me.hddIdAval.Value)
            Catch ex As Exception
                .IdAval = Nothing
            End Try


        End With
        Return obj

    End Function

    Protected Sub btnAnular_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAnular.Click
        anularDocumento(CInt(Me.hddIdDocumento.Value))
    End Sub
    Private Sub anularDocumento(ByVal IdDocumentoLetra As Integer)

        Try

            If ((New Negocio.DocLetra).DocumentoLetraAnularxIdDocumento(IdDocumentoLetra)) Then

                verFrm("6", False, False)
                Me.cboEstado.SelectedValue = "2"  '**** ANULADO
                objScript.mostrarMsjAlerta(Me, "El Documento se anuló con éxito.")

            Else
                Throw New Exception("Problemas en la anulación del Documento.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnAceptarRenegociar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarRenegociar.Click

        Dim IdDocumento As Integer = CInt(Me.hddIdDocumento.Value)

        '********** Obtenemos el Documento Letra
        Dim objDocumentoLetra As Entidades.DocLetra = (New Negocio.DocLetra).DocumentoLetraSelectCabxParams(IdDocumento, 0, 0)
        Dim listaDocReferencia As List(Of Entidades.Documento_MovCuenta) = (New Negocio.DocLetra).DocumentoLetraSelectDocReferenciaxIdDocumentoLetra(objDocumentoLetra.Id, objDocumentoLetra.IdMoneda)
        Dim listaDetalleConcepto As List(Of Entidades.DetalleConcepto)

        If listaDocReferencia IsNot Nothing Then

            listaDetalleConcepto = (New Negocio.DetalleConcepto).SelectxIdDocumento(objDocumentoLetra.Id)
            Dim Index As Integer

            If listaDetalleConcepto IsNot Nothing Then

                If listaDetalleConcepto.Count > 0 Then
                    For i As Integer = 0 To listaDocReferencia.Count - 1
                        Index = i
                        listaDocReferencia(i).NuevoSaldo = listaDetalleConcepto.Find(Function(ent As Entidades.DetalleConcepto) ent.IdDocumentoRef = listaDocReferencia(Index).Id).Monto
                    Next
                End If

            End If

        End If


        limpiarControlesFrm() '********* Limpiamos antes de cargar los datos
        cargarDocumentoLetra_GUI(objDocumentoLetra, listaDocReferencia, listaDetalleConcepto) '**** Cargamos el Doc Letra en la GUI

        aceptarRenegociarLetra(CInt(Me.txtCantidadLetrasxRenegociar.Text))

    End Sub
    Private Sub aceptarRenegociarLetra(ByVal cantidadLetrasxRenegociar As Integer)

        Try

            Dim listaLetras As List(Of Entidades.DocLetra) = generarListaLetrasRenegociar(obtenerLetras_Detalle(0), cantidadLetrasxRenegociar)
            Me.GV_Letras.DataSource = listaLetras
            Me.GV_Letras.DataBind()

            For i As Integer = 0 To Me.GV_Letras.Rows.Count - 1

                If IsNumeric(CType(Me.GV_Letras.Rows(i).FindControl("hddIdDocumento"), HiddenField).Value) Then

                    If CInt(CType(Me.GV_Letras.Rows(i).FindControl("hddIdDocumento"), HiddenField).Value) > 0 Then
                        Me.GV_Letras.Rows(i).BackColor = Drawing.Color.Yellow '** Color para la Letra a Renegociar
                        'Me.GV_Letras.Rows(i).Enabled = False '** Inhabilitamos la Letra a Renegociar
                    End If

                End If

            Next

            verFrm("7", False, False) '*** Renegociar
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  calcularTotales(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function generarListaLetrasRenegociar(ByVal listaLetras As List(Of Entidades.DocLetra), ByVal cantidadLetrasxRenegociar As Integer) As List(Of Entidades.DocLetra)

        Dim fechaActual As Date = (New Negocio.FechaActual).SelectFechaActual

        For i As Integer = 0 To cantidadLetrasxRenegociar - 1

            Dim objDocLetra As New Entidades.DocLetra
            With objDocLetra

                .NomMoneda = Me.cboMoneda.SelectedItem.ToString
                .TotalAPagar = 0
                .FechaVenc = fechaActual
                .Observaciones = ""

            End With
            listaLetras.Add(objDocLetra)
        Next

        Return listaLetras

    End Function


#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", Me.cboRol.SelectedValue)

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            '*************** CARGAMOS LA PERSONA
            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))

            Select Case CInt(Me.hddOpcionBuscarPersona.Value)
                Case 0

                    '*************** LIMPIAMOS LA GRILLA DOC REF
                    Me.GV_DocReferencia.DataSource = Nothing
                    Me.GV_DocReferencia.DataBind()

                    '***************** LIMPIAMOS LA GRILLA DE BUSQUEDA AVANZADA
                    Me.GV_BusquedaAvanzado.DataSource = Nothing
                    Me.GV_BusquedaAvanzado.DataBind()

                Case 1
                    '************** NO SE LIMPIA LA GRILLA

            End Select


            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     offCapa('capaPersona');          calcularTotales();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)

        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)

        If (objPersona IsNot Nothing) Then

            Select Case CInt(Me.hddOpcionBuscarPersona.Value)

                Case 0  '********************   DESTINATARIO

                    Me.txtDescripcionPersona.Text = objPersona.Descripcion
                    Me.txtDNI.Text = objPersona.Dni
                    Me.txtRUC.Text = objPersona.Ruc
                    Me.hddIdPersona.Value = CStr(objPersona.IdPersona)

                Case 1   '******************** AVAL

                    Me.txtDescripcionAval.Text = objPersona.Descripcion
                    Me.txtDNIAval.Text = objPersona.Dni
                    Me.txtRUCAval.Text = objPersona.Ruc
                    Me.hddIdAval.Value = CStr(objPersona.IdPersona)

            End Select

        End If

    End Sub

#End Region
#Region "************************ BÚSQUEDA AVANZADO"
    Private Sub btnAceptarBusquedaAvanzado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBusquedaAvanzado.Click
        busquedaAvanzado()
    End Sub

    Private Sub busquedaAvanzado()

        Try

            Dim IdPersona As Integer = 0
            Try
                IdPersona = CInt(Me.hddIdPersona.Value)
            Catch ex As Exception
                IdPersona = 0
            End Try

            Dim IdSerie As Integer = CInt(Me.cboSerie.SelectedValue)
            Dim fechaInicio As Date = CDate(Me.txtFechaI.Text)
            Dim fechaFin As Date = CDate(Me.txtFechaF.Text)

            Me.GV_BusquedaAvanzado.DataSource = (New Negocio.Documento).DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(CInt(Me.hddIdTipoDocumento.Value), IdSerie, IdPersona, fechaInicio, fechaFin)
            Me.GV_BusquedaAvanzado.DataBind()

            objScript.onCapa(Me, "capaBusquedaAvanzado")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub GV_BusquedaAvanzado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_BusquedaAvanzado.PageIndexChanging
        Me.GV_BusquedaAvanzado.PageIndex = e.NewPageIndex
        busquedaAvanzado()
    End Sub

    Protected Sub GV_BusquedaAvanzado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BusquedaAvanzado.SelectedIndexChanged
        Try

            cargarDocumentoLetra_FindxParams("4", CInt(CType(Me.GV_BusquedaAvanzado.SelectedRow.FindControl("hddIdDocumento_BusquedaAvanzado"), HiddenField).Value), 0, 0)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub



#End Region

    Private Sub cboMoneda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMoneda.SelectedIndexChanged
        Try

            Me.GV_DocReferencia.DataSource = Nothing
            Me.GV_DocReferencia.DataBind()

            '******** llenamos las monedas
            Me.lblMonedaMontoTotal.Text = Me.cboMoneda.SelectedItem.ToString
            Me.lblMonedaTotalDocRef.Text = Me.cboMoneda.SelectedItem.ToString

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub




#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"


    Private Function getLista_DocumentoRef() As List(Of Entidades.Documento)
        Return CType(Session.Item("listaDocumentoRef"), List(Of Entidades.Documento))
    End Function
    Private Sub setLista_DocumentoRef(ByVal lista As List(Of Entidades.Documento))
        Session.Remove("listaDocumentoRef")
        Session.Add("listaDocumentoRef", lista)
    End Sub

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click

        mostrarDocumentosRef_Find()

    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As Integer = 0
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdEmpresa As Integer = 0
            Dim IdPersona As Integer = 0

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CInt(Me.txtCodigo_BuscarDocRef.Text)
            End Select


            ''*************  IdTienda = 0  (BUSCA DE TODAS LAS TIENDAS)
            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).Documento_Buscar(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), IdPersona, 20, serie, codigo, fechaInicio, fechafin, False, 3)

            If (lista.Count > 0) Then
                Me.GV_DocRef.DataSource = lista
                Me.GV_DocRef.DataBind()
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            objScript.onCapa(Me, "DivDocRef")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub

    Private Sub GV_DocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocRef.SelectedIndexChanged

        cargarLetraSalteada(CInt(CType(Me.GV_DocRef.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))

    End Sub
    Private Sub cargarLetraSalteada(ByVal IdDocumentoRef As Integer)

        Try

            '***************** OBTENEMOS EL DOCUMENTO DE REFERENCIA
            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumentoRef)

            Dim lista As List(Of Entidades.DocLetra) = obtenerLetras_Detalle(0)
            Dim Index As Integer = CInt(Me.hddIndexLetra.Value)

            lista(Index).IdDocumento = objDocumento.Id
            lista(Index).NroDocumento = objDocumento.Serie + " - " + objDocumento.Codigo

            Me.GV_Letras.DataSource = lista
            Me.GV_Letras.DataBind()


            Me.GV_DocumentosReferencia_Find.DataBind()

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "DivDocRef")
        End If

    End Sub


#End Region

End Class