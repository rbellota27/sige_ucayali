<%@ Page Title="Consulta Letra 30.12.2011" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmComsultaPagoProgramado.aspx.vb" Inherits="APPWEB.FrmComsultaPagoProgramado" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td class="TituloCelda">
                CONSULTA DE LETRAS&nbsp;
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                DATOS DE CLIENTE
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto" align="right">
                            Descripci�n:
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="txtDescripcionPersona" ReadOnly="true" onKeypress="return(false);"
                                Enabled="true" CssClass="TextBox_ReadOnlyLeft" Width="400px" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtIdCliente" ReadOnly="true" onKeypress="return(false);" Enabled="true"
                                CssClass="TextBox_ReadOnlyLeft" Width="60px" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="btnBuscarPersona" OnClientClick="return( mostrarCapaPersona()  );" CssClass="btnBuscar"
                                runat="server" Text="Buscar" ToolTip="Buscar Cliente" />
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto" align="right">
                            D.N.I.:
                        </td>
                        <td>
                            <asp:TextBox ID="txtDNI" onFocus="return(   valOnFocus_Cliente()  );" onKeypress="return(false);"
                                CssClass="TextBox_ReadOnlyLeft" Enabled="true" runat="server"></asp:TextBox>
                        </td>
                        <td align="right" class="Texto">
                            R.U.C.:
                        </td>
                        <td>
                            <asp:TextBox ID="txtRUC" onFocus="return(   valOnFocus_Cliente()  );" onKeypress="return(false);"
                                CssClass="TextBox_ReadOnlyLeft" Enabled="true" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                LETRAS
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="Texto">
                                        Mostrar Letras:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboEstadoCancelacion" runat="server" Width="120px">
                                            <asp:ListItem Value="0">[ Todos ]</asp:ListItem>
                                            <asp:ListItem Value="1">Por Pagar</asp:ListItem>
                                            <asp:ListItem Value="2">Cancelado</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Texto">
                                        Top:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTopLetras" CssClass="TextBox_ReadOnlyLeft" onFocus=" return(   aceptarFoco(this)   ); "
                                            onKeypress="return(   validarNumeroPuntoPositivo('event')   );" onblur=" return(   valBlurClear('0',event)   ); "
                                            runat="server" Width="50px"></asp:TextBox>
                                    </td>
                                    <td>
                                        
                                        <asp:Button ID="btnAceptarVerLetras" runat="server" Text="Aceptar" ToolTip="Mostrar Letras"
                                            OnClientClick="return(  valOnClickMostrarLetras()   );" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelRojo" style="text-align: left; font-weight: bold">
                            *** Ingrese [ 0 ] en el campo [ Top ] para visualizar todos los registros.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_GV_Letras" runat="server" ScrollBars="Both" Width="100%">
                                <asp:GridView ID="GV_Letras" runat="server" AutoGenerateColumns="false" Width="100%">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-Height="20px">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnIrLetras" ToolTip="Ir al Formulario de Letras." ImageUrl="~/Imagenes/Arrow_Left.ICO"
                                                    runat="server" OnClientClick="return(  redireccionarFrmLetra(this)  );" OnClick="btnIrFrmLetra_Click" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-Height="20px">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnPagoProg" ToolTip="Editar Pago Programado" ImageUrl="~/Caja/iconos/billing_invoices.gif"
                                                    runat="server" Width="35px" OnClick="btnPagoProg_Click" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="NomTipoDocumento" HeaderText="Tipo" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblNroDocumento" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"getNroDocumento")%>'></asp:Label>

                                                            <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                                            <asp:HiddenField ID="hddIdEstadoCancelacion" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEstadoCancelacion")%>' />

                                                            <asp:HiddenField ID="hddIdPersona" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />

                                                            <asp:HiddenField ID="hddIdMovCuenta" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMovCuenta")%>' />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="getFechaEmisionText" HeaderText="Fec. Emisi�n" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="getFechaVctoText" HeaderText="Fec. Vcto." HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="getFechaPagoText" HeaderText="Fec. Pago" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderText="Monto Total" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblMonedaMontoTotal" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                            <asp:Label ID="lblMontoTotal" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F2}")%>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Saldo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblMonedaSaldo" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                            <asp:Label ID="lblSaldo" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SaldoMovCuenta","{0:F2}")%>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado Doc." ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                                        <asp:TemplateField HeaderText="Abono en Cuenta" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblDescDebitoCuenta" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"DescDebitoCuenta")%>'
                                                                Font-Bold="true"></asp:Label>

                                                            <asp:HiddenField ID="hddDebitoCuenta" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"DebitoCuenta")%>' />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Protestado" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblDescProtestado" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"DescProtestado")%>'
                                                                Font-Bold="true"></asp:Label>

                                                            <asp:HiddenField ID="hddProtestado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Protestado")%>' />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Renovado" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblDescRenegociado" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"DescRenegociado")%>'
                                                                Font-Bold="true"></asp:Label>

                                                            <asp:HiddenField ID="hddRenegociado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Renegociado")%>' />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdPersona" runat="server" />
                <asp:HiddenField ID="hddFechaActual" runat="server" />
            </td>
        </tr>
    </table>
    <div id="capaEditarLetra" style="border: 3px solid blue; padding: 8px; width: auto;
        height: auto; position: absolute; top: 230px; left: 221px; background-color: white;
        z-index: 2; display: none;">
        
        <table>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaEditarLetra'));" />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td class="Texto" align="right" style="font-weight: bold">
                                Letra:
                            </td>
                            <td>
                                <asp:Label ID="lblNroDocumento_Editar" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                    Text="-"></asp:Label>
                            </td>
                            <td>
                                  <asp:HiddenField ID="hddFechaActual_Editar" runat="server" />
                                <asp:HiddenField ID="hddIdDocumento_Editar" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="LabelLeft" style="font-weight: bold" align="left">
                                <asp:CheckBox ID="chbDebitoCuenta" runat="server" Text="Abono en Cuenta" 
                                    TextAlign="Right" />
                            </td>
                            <td class="Texto" align="right">
                               Nro UNICO:
                            </td>
                            <td>
                                <asp:TextBox ID="txtNroOperacion" Width="100px" Font-Bold="true" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="LabelLeft" style="font-weight: bold">
                                <asp:CheckBox ID="chbProtestado" runat="server" onClick="return(  valOnClickProtestado(this)  );"
                                    Text="Letra Protestada" />
                            </td>
                            <td style="color: #0000FF; text-align: right;" Width="150px">
                            Fecha Protesto :  
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaProtesto" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaProtesto_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" 
                                    TargetControlID="txtFechaProtesto">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaProtesto_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaProtesto">
                                </cc1:CalendarExtender>
                            </td>
                        </tr>
                        <tr>
                            <td class="LabelLeft" style="font-weight: bold" align="left">
                                <asp:CheckBox ID="chbLetraCancelada" runat="server" TextAlign="Right" Text="Letra Cancelada" />
                            </td>
                            <td class="Texto" align="right">
                                Fecha Cancelaci�n :
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaPago" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaPago_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaPago">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaPago_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaPago">
                                </cc1:CalendarExtender>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnGuardar_EditarLetra" ToolTip="Guardar los cambios" runat="server"
                                    Text="Guardar" OnClientClick="return(  valOnClickGuadar_EditarLetra()   );" Width="80px" />
                            </td>
                            <td>
                                <asp:Button ID="btnCerrar_EditarLetra" ToolTip="Cerrar" runat="server" Text="Cerrar"
                                    OnClientClick="return(  offCapa('capaEditarLetra')   );" Width="80px" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_DocumentoAbono" runat="server" AutoGenerateColumns="false" Width="100%"
                        HeaderStyle-Height="25px" RowStyle-Height="25px">
                        <Columns>
                            <asp:BoundField DataField="NomTipoDocumento" HeaderText="Tipo" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                            <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblSerie" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Serie")%>'></asp:Label>
                                                -
                                                <asp:Label ID="lblCodigo" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Codigo")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaEmision" HeaderText="Emisi�n" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:TemplateField HeaderText="Importe" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblMoneda" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                <asp:Label ID="lblMonto" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F3}")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Selected="True" Value="J">Juridica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razon Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarPersonaGrilla">                                                                        
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda());" runat="server"
                                                                            Width="450px"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel2" runat="server" DefaultButton="btnBuscarPersonaGrilla">    
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero());" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel3" runat="server" DefaultButton="btnBuscarPersonaGrilla">    
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero());"
                                                                            MaxLength="11"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button runat="server" Text="Buscar" CssClass="btnBuscar" ID="btnBuscarPersonaGrilla" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Rol:
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="cboRol" runat="server" Width="100%">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">
        //************************** BUSQUEDA PERSONA
        //***************************************************** BUSQUEDA PERSONA

        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btnBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btnBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }

        function valOnFocus_Cliente() {
            document.getElementById('<%=btnBuscarPersona.ClientID%>').focus();
            return false;
        }
        //*********************************************************************************** FIN BUSCAR CLIENTE

        function valEditarLetra_Select(boton) {
            var grilla = document.getElementById('<%=GV_Letras.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[0].id == boton.id) {


                        //****** VALIDAMOS LA PERSONA
                        var hddIdPersona_Grilla = rowElem.cells[3].children[3];
                        var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
                        if (parseInt(hddIdPersona_Grilla.value) != parseInt(hddIdPersona.value)) {
                            alert('Los Documentos mostrados NO corresponden a la Persona Seleccionada.');
                            return false;
                        }
                        document.getElementById('<%=lblNroDocumento_Editar.ClientID%>').innerHTML = rowElem.cells[3].children[0].innerHTML;
                        document.getElementById('<%=hddIdDocumento_Editar.ClientID%>').value = rowElem.cells[3].children[1].value;

                        var hddIdEstadoCancelacion = rowElem.cells[3].children[2];
                        if (parseInt(hddIdEstadoCancelacion.value) == 1) { //**** POR PAGAR
                            document.getElementById('<%=chbLetraCancelada.ClientID%>').status = false;
                            var fechaActual = new Date();
                            var dia = fechaActual.getDate();
                            var mes = fechaActual.getMonth() + 1;
                            var anno = fechaActual.getYear();
                            if (dia < 10) { dia = "0" + dia; }
                            if (mes < 10) { mes = "0" + mes; }
                            var fechaActualText = dia + "/" + mes + "/" + anno;
                            document.getElementById('<%=txtFechaPago.ClientID%>').value = fechaActualText;

                        } else {   //********** CANCELADO
                            document.getElementById('<%=chbLetraCancelada.ClientID%>').status = true;
                            document.getElementById('<%=txtFechaPago.ClientID%>').value = rowElem.cells[6].innerHTML;

                        }
                        var hddDebitoCuenta = rowElem.cells[11].children[1];
                        var hddProtestado = rowElem.cells[12].children[1];
                        var hddRenegociado = rowElem.cells[13].children[1];

                        if (hddDebitoCuenta.value == 'True') {
                            document.getElementById('<%=chbDebitoCuenta.ClientID%>').status = true;
                        } else {
                            document.getElementById('<%=chbDebitoCuenta.ClientID%>').status = false;
                        }

                        if (hddProtestado.value == 'True') {
                            document.getElementById('<%=chbProtestado.ClientID%>').status = true;
                        } else {
                            document.getElementById('<%=chbProtestado.ClientID%>').status = false;
                        }

                        onCapa('capaEditarLetra');

                        return false;
                    }
                }
            }
            return false;
        }
        function valOnClickMostrarLetras() {
            //        var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            //        if (isNaN(parseInt(hddIdPersona.value)) || parseInt(hddIdPersona.value) <= 0 || hddIdPersona.value.length<=0   ) {
            //            alert('Debe seleccionar un cliente.');
            //            mostrarCapaBuscarPersona();            
            //            return false;
            //        }

            var txtTop = document.getElementById('<%=txtTopLetras.ClientID%>');
            if (isNaN(parseInt(txtTop.value)) || parseInt(txtTop.value) < 0 || txtTop.value.length <= 0) {
                alert('Valor del campo [ Top ] inv�lido.');
                txtTop.select();
                txtTop.focus();
                return false;
            }

            return true;
        }

        function valOnClickGuadar_EditarLetra() 
        {
            var hddIdDocumento_EditarLetra = document.getElementById('<%=hddIdDocumento_Editar.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento_EditarLetra.value)) || parseInt(hddIdDocumento_EditarLetra.value) <= 0 || hddIdDocumento_EditarLetra.value.length <= 0) {
                alert('Debe seleccionar un Documento al cual hacer referencia.');
                return false;
            }
            return confirm('Desea continuar con la Operaci�n ?');
        }


        function redireccionarFrmLetra(boton) {
            var grilla = document.getElementById('<%=GV_Letras.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[0].children[0].id == boton.id) {

                        var hddIdDocumento = rowElem.cells[3].children[1];

                        //window.open('../../Caja/FrmLetra.aspx?IdDocumento=' + hddIdDocumento.value, '', 'resizable=yes,width=1000,height=800,scrollbars=1', null);                                        
                        window.open('FrmLetra.aspx?IdDocumento=' + hddIdDocumento.value, '_blank', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                        return false;
                    }
                }
            }
            return false;


        }
        function valOnClickProtestado(chbProtestado)
         {

            if (chbProtestado.status) {
                var hddIdDocumento = document.getElementById('<%=hddIdDocumento_Editar.ClientID%>');
                var grilla = document.getElementById('<%=GV_Letras.ClientID%>');
                if (grilla != null) {
                    for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                        var rowElem = grilla.rows[i];
                        if (parseInt(hddIdDocumento.value) == parseInt(rowElem.cells[3].children[1].value)) {

                           // var fechaActual = converToDate(document.getElementById('<%=txtFechaProtesto.ClientID%>').Value);
                            var fechaActual = converToDate(document.getElementById('<%=txtFechaProtesto.ClientID%>').value);
 
                            var fechaVcto = converToDate(rowElem.cells[5].innerHTML);

                            if (fechaActual < fechaVcto) 
                                {
                                    return confirm('La Fecha de Protesto [ ' + convertToDateString(fechaActual) + ' ] es menor que la Fecha de Vencimiento del Documento [ ' + convertToDateString(fechaVcto) + ' ]. Desea continuar ?');
                                } 
                            else
                                {
                                    return true;
                                }

                        }
                    }
                }

            }
            return true;
         }


    </script>

</asp:Content>
