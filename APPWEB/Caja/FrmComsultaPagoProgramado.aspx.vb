﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmComsultaPagoProgramado
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass

    Dim Usuario As Integer
    Public ReadOnly Property SIdUsuario() As Integer
        Get
            Return CType(Session.Item("IdUsuario"), Integer)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            inicializarFrm()
            ValidarPermisos()

        End If
        Usuario = CInt(Session("IdUsuario"))
    End Sub

    Private Sub ValidarPermisos()
        '**** DEBITO EN CUENTA / PROTESTADO / LETRA CANCELADA / GUARDAR
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {134, 135, 136, 137})

        If listaPermisos(0) > 0 Then  '********* DEBITO EN CUENTA
            Me.chbDebitoCuenta.Enabled = True
            Me.txtNroOperacion.Enabled = True
        Else
            Me.chbDebitoCuenta.Enabled = False
            Me.txtNroOperacion.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* PROTESTADO
            Me.chbProtestado.Enabled = True
        Else
            Me.chbProtestado.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* LETRA CANCELADA
            Me.chbLetraCancelada.Enabled = True
            Me.txtFechaPago.Enabled = True
        Else
            Me.chbLetraCancelada.Enabled = False
            Me.txtFechaPago.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* GUARDAR
            Me.btnGuardar_EditarLetra.Enabled = True
        Else
            Me.btnGuardar_EditarLetra.Enabled = False
        End If

    End Sub

    Private Sub inicializarFrm()
        Try

            Me.txtTopLetras.Text = "0"
            Me.hddFechaActual.Value = Format((New Negocio.FechaActual).SelectFechaActual(), "dd/MM/yyyy")

            Dim objCbo As New Combo
            objCbo.LlenarCboRol(Me.cboRol, True)

            If (Me.cboRol.Items.FindByValue("1") IsNot Nothing) Then
                Me.cboRol.SelectedValue = "1"
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
   

    Protected Sub btnAceptarVerLetras_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarVerLetras.Click

        Try

            Dim IdPersona As Integer = 0
            If (IsNumeric(Me.hddIdPersona.Value) And Me.hddIdPersona.Value.Trim.Length > 0) Then
                IdPersona = CInt(Me.hddIdPersona.Value)
            End If

            cargarConsultaLetras(IdPersona, CInt(Me.cboEstadoCancelacion.SelectedValue), CInt(Me.txtTopLetras.Text))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try



    End Sub
    Private Sub cargarConsultaLetras(ByVal IdPersona As Integer, ByVal IdEstadoCancelacion As Integer, ByVal top As Integer)



        Dim lista As List(Of Entidades.DocLetra) = (New Negocio.DocLetra).DocumentoLetraConsultarLetrasxIdEstadoCancelacion(IdPersona, IdEstadoCancelacion, top)

        If (lista.Count <= 0) Then
            Me.GV_Letras.DataSource = Nothing
            Me.GV_Letras.DataBind()
            Throw New Exception("No se hallaron registros.")
        Else
            Me.GV_Letras.DataSource = lista
            Me.GV_Letras.DataBind()

        End If

        GV_DocumentoAbono.DataBind()

    End Sub

    Private Sub btnGuardar_EditarLetra_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar_EditarLetra.Click
        registrar_EditarLetra()
    End Sub

    Private Sub registrar_EditarLetra()
        Try

            If (IsDate(Me.txtFechaPago.Text) And Me.txtFechaPago.Text.Trim.Length > 0 And Me.chbLetraCancelada.Checked = False) Then
                Throw New Exception("SE HA SELECCIONADO UNA FECHA DE PAGO PERO NO SE HA ACTIVADO LA CASILLA DE VERIFICACIÓN < Letra Cancelada >. NO SE PERMITE LA OPERACIÓN.")
            End If

            'If (Me.txtNroOperacion.Text.Trim.Length > 0 And Me.chbDebitoCuenta.Checked = False) Then
            '    Throw New Exception("SE HA INGRESADO UN NRO. DE OPERACIÓN PERO NO SE HA ACTIVADO LA CASILLA DE VERIFICACIÓN < Débito en Cuenta >. NO SE PERMITE LA OPERACIÓN.")
            'End If

            'If (Me.chbProtestado.Checked And Me.chbLetraCancelada.Checked) Then
            '    Throw New Exception("LA CASILLA DE VERIFICACIÓN < Letra Cancelada > y < Letra Protestada > HAN SIDO SELECCIONADAS. NO SE PERMITE LA OPERACIÓN.")
            'End If

            Dim fechaPago As Date = Nothing

            Try
                If chbProtestado.Checked = True Then
                    fechaPago = CDate(Me.txtFechaProtesto.Text)
                End If
                If chbLetraCancelada.Checked = True Then
                    fechaPago = CDate(Me.txtFechaPago.Text)

                End If
               
            Catch ex As Exception
                fechaPago = Nothing
            End Try

            If ((New Negocio.DocLetra).DocumentoLetra_EditarLetraxConsulta(CInt(Me.hddIdDocumento_Editar.Value), Me.chbLetraCancelada.Checked, Me.chbDebitoCuenta.Checked, Me.chbProtestado.Checked, fechaPago, Me.txtNroOperacion.Text, Usuario)) Then
                Me.GV_Letras.DataSource = Nothing
                Me.GV_Letras.DataBind()
                Me.GV_DocumentoAbono.DataBind()
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "alert('La Operación se realizó con éxito.'); offCapa('capaEditarLetra'); ", True)
            Else
                Throw New Exception("Problemas en la Operación.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnIrFrmLetra_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        redireccionarFrmLetra(sender)

    End Sub
    Private Sub redireccionarFrmLetra(ByVal sender As Object)

        Try

            Dim gvRow As GridViewRow = CType(CType(sender, ImageButton).NamingContainer, GridViewRow)

            Response.Redirect("FrmLetra.aspx?IdDocumento=" + CType(gvRow.FindControl("hddIdDocumento"), HiddenField).Value, True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            '*************** CARGAMOS LA PERSONA
            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))

            Me.GV_Letras.DataSource = Nothing
            Me.GV_Letras.DataBind()
            Me.GV_DocumentoAbono.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)

        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)

        If (objPersona IsNot Nothing) Then

            Me.txtDescripcionPersona.Text = objPersona.Descripcion
            Me.txtDNI.Text = objPersona.Dni
            Me.txtRUC.Text = objPersona.Ruc
            Me.txtIdCliente.Text = CStr(objPersona.IdPersona)
            Me.hddIdPersona.Value = CStr(objPersona.IdPersona)

        End If

    End Sub

    Protected Sub btnPagoProg_Click(ByVal sender As Object, ByVal e As EventArgs)

        Try

            mostrarCapaEditarLetra(CType(CType(sender, ImageButton).NamingContainer, GridViewRow))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub mostrarCapaEditarLetra(ByVal gvRow As GridViewRow)

        Dim IdDocumento As Integer = CInt(CType(gvRow.FindControl("hddIdDocumento"), HiddenField).Value)

        Dim objDocumento As Entidades.DocLetra = (New Negocio.DocLetra).DocumentoLetraSelectCabxParams(IdDocumento, 0, 0)

        Dim fechavencimiento As String
        fechavencimiento = (objDocumento.FechaVenc)

        'Me.hddFechaActual.Value = fechavencimiento
        'Me.hddFechaActual_Editar.Value = CStr(objDocumento.FechaVenc)
        'Me.hddFechaActual.Value = Format((New Negocio.FechaActual).SelectFechaActual(), "dd/MM/yyyy")

        Me.chbDebitoCuenta.Checked = objDocumento.getLetraCambio.DebitoCuenta
        Me.chbProtestado.Checked = objDocumento.getLetraCambio.Protestado

        Me.txtNroOperacion.Text = objDocumento.getLetraCambio.NroOperacion

        Select Case objDocumento.IdEstadoCancelacion
            Case 0
                Me.chbLetraCancelada.Checked = False
                Me.txtFechaPago.Text = ""
                Me.txtFechaProtesto.Text = ""

            Case 1 '*********** POR PAGAR
                Me.chbLetraCancelada.Checked = False
                Me.txtFechaPago.Text = ""
                Me.txtFechaProtesto.Text = ""

            Case 2 '************  CANCELADO
                Me.chbLetraCancelada.Checked = True
                If (objDocumento.FechaPago <> Nothing) Then
                    Me.txtFechaPago.Text = Format(objDocumento.FechaPago, "dd/MM/yyyy")
                End If
        End Select

        Me.hddIdDocumento_Editar.Value = CStr(objDocumento.Id)
        Me.lblNroDocumento_Editar.Text = objDocumento.NroDocumento

        Try

            Dim IdMovCuenta As Integer = CInt(CType(gvRow.FindControl("hddIdMovCuenta"), HiddenField).Value)

            Me.GV_DocumentoAbono.DataSource = (New Negocio.MovCuenta).SelectAbonosxIdMovCuenta(IdMovCuenta)
            Me.GV_DocumentoAbono.DataBind()

        Catch ex As Exception

        End Try

        objScript.onCapa(Me, "capaEditarLetra")

    End Sub
#End Region

    Private Sub btnBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", Me.cboRol.SelectedValue)

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class