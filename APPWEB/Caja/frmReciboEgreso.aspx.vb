﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmReciboEgreso
    Inherits System.Web.UI.Page


    Private objScript As New ScriptManagerClass
    Private ListaCancelacion As List(Of Entidades.PagoCaja)
    Private ListaDetalleConcepto As List(Of Entidades.DetalleConcepto)

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If (Not Me.IsPostBack) Then

            inicializarFrm()
            valParamsLoad()

        End If

        valAuxiliares()

    End Sub

    Private Sub valAuxiliares()
        '************ Mostramos las Monedas
        Me.lblMonedaFaltante.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMonedaTotalAPagar.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMonedaTotalRecibido.Text = Me.cboMoneda.SelectedItem.ToString
    End Sub

    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION 
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {44, 45, 46, 47, 48})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumentoxCodigo.Enabled = True
        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumentoxCodigo.Enabled = False
        End If

        If listaPermisos(4) > 0 Then  '********* FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

    End Sub

    Private Sub inicializarFrm()

        Try

            Dim objCbo As New Combo
            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)

                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.hddIdTipoDocumento.Value), True)

                Me.hddIdMedioPagoPrincipal.Value = CStr((New Negocio.MedioPago).SelectIdMedioPagoPrincipal)

                .LLenarCboEstadoDocumento(Me.cboEstado)
                .LlenarCboMoneda(Me.cboMoneda)
                .LlenarCboMoneda(Me.cboMoneda_DatoCancelacion)
                .LlenarCboMedioPagoxIdTipoDocumento(Me.cboMedioPago, CInt(Me.hddIdTipoDocumento.Value), False)

                Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
                Me.txtFechaInicio_BA.Text = Me.txtFechaEmision.Text
                Me.txtFechaFin_BA.Text = Me.txtFechaEmision.Text


                If (Me.cboMedioPago.Items.FindByValue(Me.hddIdMedioPagoPrincipal.Value) IsNot Nothing) Then
                    Me.cboMedioPago.SelectedValue = Me.hddIdMedioPagoPrincipal.Value
                End If
                actualizarControlesMedioPago(1, CInt(Me.cboMedioPago.SelectedValue))

                verFrm(FrmModo.Nuevo, True, True, True, True, True, True)

                ValidarPermisos()

            End With
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Private Sub valParamsLoad()
        'Try

        '    '************* MANEJO CONSIDERADO PARA DOCUMENTOS [ RECIBO DE EGRESO ]
        '    If (Request.QueryString("IdDocumentoRef") <> Nothing) Then

        '        Dim objDocumentoNotaCredito As Entidades.DocumentoNotaCredito = (New Negocio.DocumentoNotaCredito).DocumentoNotaCreditoSelectCab(0, 0, CInt(Request.QueryString("IdDocumentoRef")))

        '        '*************** CARGAMOS LOS DATOS DEL CLIENTE
        '        Me.txtDescripcionPersona.Text = objDocumentoNotaCredito.DescripcionPersona
        '        Me.txtRUC.Text = objDocumentoNotaCredito.Ruc
        '        Me.txtDNI.Text = objDocumentoNotaCredito.Dni
        '        Me.hddIdPersona.Value = CStr(objDocumentoNotaCredito.IdPersona)

        '        '************** AGREGAMOS EL MONTO DEL SALDO
        '        Me.GV_Otros.DataSource = Nothing
        '        Me.GV_Otros.DataBind()
        '        Me.cboMoneda.SelectedValue = CStr(objDocumentoNotaCredito.IdMoneda)
        '        Me.hddIdDocumentoRef.Value = CStr(objDocumentoNotaCredito.Id)
        '        addNewDetalle_Otros("E", objDocumentoNotaCredito.ImporteTotal)

        '        '************* Inhabilitamos los objetos
        '        Me.cboMoneda.Enabled = False
        '        Me.btnAddConcepto_Otros.Visible = False

        '        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularTotalAPagar();", True)

        '    End If

        'Catch ex As Exception
        '    objScript.mostrarMsjAlerta(Me, ex.Message)
        'End Try
    End Sub

    Private Function getListaCancelacion() As List(Of Entidades.PagoCaja)
        Return CType(Session.Item("ListaCancelacion"), List(Of Entidades.PagoCaja))
    End Function

    Private Sub setListaCancelacion(ByVal lista As List(Of Entidades.PagoCaja))
        Session.Remove("ListaCancelacion")
        Session.Add("ListaCancelacion", lista)
    End Sub

    Private Sub GenerarCodigoDocumento()
        Try

            Dim objCombo As New Combo
            objCombo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddidtipodocumento.value))

            If (IsNumeric(Me.cboSerie.SelectedValue)) Then
                Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
            Else
                Me.txtCodigoDocumento.Text = ""
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged

        Try


            Dim objCbo As New Combo

            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)


            GenerarCodigoDocumento()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged




        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)


            GenerarCodigoDocumento()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        Try
            If (IsNumeric(Me.cboSerie.SelectedValue)) Then
                Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
            Else
                Me.txtCodigoDocumento.Text = ""
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub limpiarControlesFrm(Optional ByVal limpiarDatosPersona As Boolean = True, Optional ByVal procesoEdicion As Boolean = False)


        If Not procesoEdicion Then
            Me.txtCodigoDocumento.Text = ""
            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.hddIdDocumento.Value = ""

        End If

        If limpiarDatosPersona Then
            Me.txtDescripcionPersona.Text = ""
            Me.txtDNI.Text = ""
            Me.txtRUC.Text = ""
            Me.hddIdPersona.Value = ""
        End If




        Me.txtFaltante.Text = "0"
        txtMonto_DatoCancelacion.Text = "0"
        Me.txtTotalAPagar.Text = "0"
        Me.txtTotalRecibido.Text = "0"


        Me.GV_Cancelacion.DataSource = Nothing
        Me.GV_Cancelacion.DataBind()

        Me.GV_Otros.DataSource = Nothing
        Me.GV_Otros.DataBind()


        Me.txtObservaciones.Text = ""

        Me.hddCodigoDocumento.Value = ""
        Me.hddIdDocumentoRef.Value = ""

    End Sub

    Private Sub verFrm(ByVal modoFrm As Integer, ByVal generarCodigoDoc As Boolean, ByVal limpiarSessionViewState As Boolean, ByVal initListasSession As Boolean, ByVal limpiarControles As Boolean, ByVal initMotivoReciboI As Boolean, ByVal limpiarDatosPersona As Boolean, Optional ByVal procesoEdicion As Boolean = False)

        If (limpiarControles) Then
            limpiarControlesFrm(limpiarDatosPersona, procesoEdicion)
        End If


        If (limpiarSessionViewState) Then
            Session.Remove("ListaCancelacion")
            ViewState.Clear()
        End If


        If (initListasSession) Then
            Me.ListaCancelacion = New List(Of Entidades.PagoCaja)
            setListaCancelacion(Me.ListaCancelacion)
        End If

        If (generarCodigoDoc) Then
            GenerarCodigoDocumento()
        End If

        '************** MODO FRM
        hddFrmModo.Value = CStr(modoFrm)
        ActualizarBotonesControl()

    End Sub
    Private Sub ActualizarBotonesControl()

        Select Case CInt(hddFrmModo.Value)

            Case 0 '************* INICIO
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '******** Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cancelacion.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboCaja.Enabled = True
                Me.cboTipoOperacion.Enabled = True

                Me.cboMoneda.Enabled = True

                Me.txtFechaEmision.Enabled = True

            Case 1 '************** Nuevo
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '******** Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False


                Me.Panel_Cancelacion.Enabled = True
                Me.Panel_Cliente.Enabled = True
                Me.Panel_Detalle.Enabled = True

                Me.Panel_Obs.Enabled = True

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboCaja.Enabled = True
                Me.cboTipoOperacion.Enabled = True
                Me.cboMoneda.Enabled = True

                Me.txtFechaEmision.Enabled = True


            Case 2 '*********************** Editar
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '******** Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cancelacion.Enabled = True
                Me.Panel_Cliente.Enabled = True
                Me.Panel_Detalle.Enabled = True

                Me.Panel_Obs.Enabled = True

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboCaja.Enabled = True
                Me.cboTipoOperacion.Enabled = True
                Me.cboMoneda.Enabled = True

                Me.txtFechaEmision.Enabled = True

            Case 3 '**************************** Buscar documento
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '******** Me.btnBuscarDocumentoxcodigo.Visible = True

                Me.txtCodigoDocumento.ReadOnly = False
                Me.txtCodigoDocumento.Focus()

                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False

                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False


                Me.Panel_Cancelacion.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False

                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                Me.cboCaja.Enabled = True
                Me.cboTipoOperacion.Enabled = True
                Me.cboMoneda.Enabled = True

                Me.txtFechaEmision.Enabled = True

            Case 4 '********* documento hallado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '********** Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True
                Me.btnImprimir.Visible = True

                Me.Panel_Cancelacion.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboCaja.Enabled = False
                Me.cboTipoOperacion.Enabled = False
                Me.cboMoneda.Enabled = False

                Me.txtFechaEmision.Enabled = False

            Case 5 '********* documento guardado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '********* Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = True

                Me.Panel_Cancelacion.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboCaja.Enabled = False
                Me.cboTipoOperacion.Enabled = False
                Me.cboMoneda.Enabled = False

                Me.txtFechaEmision.Enabled = False

            Case 6  '*********** Anulacion exitosa

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '********* Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cancelacion.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboCaja.Enabled = False
                Me.cboTipoOperacion.Enabled = False
                Me.cboMoneda.Enabled = False

                Me.txtFechaEmision.Enabled = False

        End Select
    End Sub

    Private Function obtenerListaDetalleRecibo(ByVal generarNewLista As Boolean, ByVal cantidadInitNew As Integer, ByVal tipoRecibo As String) As List(Of Entidades.DetalleReciboView_1)

        Dim lista As New List(Of Entidades.DetalleReciboView_1)

        '********** Obtengo la lista de conceptos segun el tipo de Recibo
        Dim listaAux As List(Of Entidades.Concepto) = Nothing
        Select Case tipoRecibo
            Case "I"  '******** Recibo Ingreso
                listaAux = (New Negocio.Concepto).SelectCboxTipoUso(1)
            Case "E" '******* Recibo Egreso
                listaAux = (New Negocio.Concepto).SelectCboxTipoUso(-1)
        End Select

        If (generarNewLista) Then  '********** Generamos la Lista de DetalleRecibo // Otros

            '************ Genero la lista
            For i As Integer = 0 To cantidadInitNew - 1

                Dim objDetalleRecibo As New Entidades.DetalleReciboView_1

                objDetalleRecibo.Moneda = Me.cboMoneda.SelectedItem.ToString
                objDetalleRecibo.Monto = 0
                objDetalleRecibo.Concepto = ""
                objDetalleRecibo.ListaConcepto = listaAux
                objDetalleRecibo.IdConcepto = Nothing
                lista.Add(objDetalleRecibo)

            Next
        Else

            '************* Obtenemos el detalle Recibo

            For i As Integer = 0 To Me.GV_Otros.Rows.Count - 1

                Dim objDetalleRecibo As New Entidades.DetalleReciboView_1

                objDetalleRecibo.Moneda = CType(Me.GV_Otros.Rows(i).FindControl("lblMoneda"), Label).Text
                objDetalleRecibo.Monto = CDec(CType(Me.GV_Otros.Rows(i).FindControl("txtAbono"), TextBox).Text)
                objDetalleRecibo.Concepto = CType(Me.GV_Otros.Rows(i).FindControl("txtDescripcionConcepto"), TextBox).Text
                objDetalleRecibo.ListaConcepto = listaAux
                objDetalleRecibo.IdConcepto = CInt(CType(Me.GV_Otros.Rows(i).FindControl("cboConcepto"), DropDownList).SelectedValue)
                lista.Add(objDetalleRecibo)

            Next

        End If

        Return lista

    End Function

    Private Sub addNewDetalle_Otros()
        Try

            '********* Obtengo la Lista
            Me.ListaDetalleConcepto = obtenerListaDetalleConcepto(False, Nothing)

            '*********** Creo un Nuevo Detalle
            Dim listaAux As List(Of Entidades.Concepto) = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(CInt(Me.hddIdTipoDocumento.Value))

            '************ Creo el objeto y lo agrego a la Lista
            Dim obj As New Entidades.DetalleConcepto
            With obj
                .Moneda = Me.cboMoneda.SelectedItem.ToString
                .Monto = 0
                .Concepto = ""
                .ListaConcepto = listaAux
                .IdConcepto = Nothing
            End With
            Me.ListaDetalleConcepto.Add(obj)

            '************ Grilla
            Me.GV_Otros.DataSource = Me.ListaDetalleConcepto
            Me.GV_Otros.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_Otros_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Otros.RowDataBound

        Try

            If (e.Row.RowType = DataControlRowType.DataRow) Then


                Dim gvrow As GridViewRow = CType(e.Row.Cells(1).NamingContainer, GridViewRow)
                Dim combo As DropDownList = CType(gvrow.FindControl("cboConcepto"), DropDownList)

                If (Me.ListaDetalleConcepto(e.Row.RowIndex).IdConcepto <> Nothing) Then

                    combo.SelectedValue = CStr(Me.ListaDetalleConcepto(e.Row.RowIndex).IdConcepto)

                End If

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub GV_Otros_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Otros.SelectedIndexChanged
        eliminarDetalleRecibo_Otros(Me.GV_Otros.SelectedIndex)
    End Sub
    Private Sub eliminarDetalleRecibo_Otros(ByVal index As Integer)
        Try

            '****** Elimino el detalle
            Me.ListaDetalleConcepto = obtenerListaDetalleConcepto(False, Nothing)
            Me.ListaDetalleConcepto.RemoveAt(index)

            '******** Grilla
            Me.GV_Otros.DataSource = Me.ListaDetalleConcepto
            Me.GV_Otros.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularTotalAPagar();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub



    Private Sub btnAddConcepto_Otros_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddConcepto_Otros.Click
        addNewDetalle_Otros()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        registrarReciboEgreso()
    End Sub
    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            .IdUsuario = CInt(Session("IdUsuario"))

            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .Serie = Me.cboSerie.SelectedItem.ToString
            .Codigo = Me.txtCodigoDocumento.Text
            .IdTipoDocumento = CInt(Me.hddIdTipoDocumento.Value)
            .IdEstadoDoc = 1  '************ Siempre es ACTIVO
            .FechaEmision = CDate(Me.txtFechaEmision.Text)
            .IdCaja = CInt(Me.cboCaja.SelectedValue)
            .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
            .IdPersona = CInt(Me.hddIdPersona.Value)
            .TotalAPagar = CDec(Me.txtTotalAPagar.Text)
            .Total = CDec(Me.txtTotalAPagar.Text)
            .FechaCancelacion = (New Negocio.FechaActual).SelectFechaActual
            .IdCondicionPago = 1  '********* CONTADO

            Select Case CInt(Me.hddFrmModo.Value)
                Case 1  '******* Nuevo
                    .Id = Nothing
                Case 2 '***** Editar
                    .Id = CInt(Me.hddIdDocumento.Value)
            End Select

            .IdEstadoCancelacion = 2   '******** Cancelado
            .IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
            .TotalLetras = (New Aletras).Letras(CStr(objDocumento.TotalAPagar)) + " /100 " + (New Negocio.Moneda).SelectxId(.IdMoneda)(0).Descripcion

        End With

        Return objDocumento

    End Function
    Private Sub validarFrm()

        If (CDec(Me.txtTotalAPagar.Text) > CDec(Me.txtTotalRecibido.Text)) Then

            Throw New Exception("EL TOTAL POR ENTREGAR SUPERA EL TOTAL INGRESADO EN DATOS DE CANCELACIÓN. NO SE PERMITE LA OPERACIÓN.")

        End If

        If (CDec(Me.txtTotalAPagar.Text) < CDec(Me.txtTotalRecibido.Text)) Then

            Throw New Exception("EL TOTAL INGRESADO EN DATOS DE CANCELACIÓN SUPERA EL TOTAL POR ENTREGAR. NO SE PERMITE LA OPERACIÓN.")

        End If

        If (Me.GV_Cancelacion.Rows.Count <= 0) Then

            Throw New Exception("NO SE HAN INGRESADO DATOS DE CANCELACIÓN. NO SE PERMITE LA OPERACIÓN.")

        End If

    End Sub
    Private Function obtenerRelacionDocumento() As Entidades.RelacionDocumento

        Dim objRelacionDocumento As Entidades.RelacionDocumento = Nothing
        If (Me.hddIdDocumentoRef.Value.Trim.Length > 0 And IsNumeric(Me.hddIdDocumentoRef.Value)) Then
            objRelacionDocumento.IdDocumento1 = CInt(Me.hddIdDocumentoRef.Value)
            objRelacionDocumento.IdDocumento2 = Nothing
        End If
        Return objRelacionDocumento
    End Function
    Private Sub registrarReciboEgreso()

        Try


            validarFrm()



            '****** cabecera Documento
            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()

            ''******* Documento Ref
            Dim objRelacionDocumento As Entidades.RelacionDocumento = Nothing

            '******* Detalle Concepto
            Dim listaDetalle As List(Of Entidades.DetalleConcepto) = obtenerListaDetalleConcepto(False, Nothing)

            '******** obtengo la lista pago caja a insertar
            Me.ListaCancelacion = getListaCancelacion()
            Dim listaPagoCaja As List(Of Entidades.PagoCaja) = Me.ListaCancelacion

            Dim objMovCaja As Entidades.MovCaja = obtenerMovCaja()

            '************* OBSERVACIONES
            Dim objObservaciones As Entidades.Observacion = obtenerObservaciones()

            Select Case CInt(Me.hddFrmModo.Value)
                Case 1 '******* NUEVO

                    Dim IdDocumento As Integer = (New Negocio.DocReciboCaja).ReciboCajaEgresoInsert(objDocumento, listaDetalle, objMovCaja, listaPagoCaja, objRelacionDocumento, objObservaciones)

                    If (IdDocumento > 0) Then

                        verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False, False)
                        Me.hddIdDocumento.Value = CStr(IdDocumento)
                        objScript.mostrarMsjAlerta(Me, "El Documento se registró con éxito.")
                    Else
                        Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
                    End If


                Case 2 '******* EDITAR
                    If ((New Negocio.DocReciboCaja).ReciboCajaEgresoUpdate(objDocumento, listaDetalle, objMovCaja, listaPagoCaja, objRelacionDocumento, objObservaciones) > 0) Then

                        verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False, False)

                        objScript.mostrarMsjAlerta(Me, "El Documento se actualizó con éxito.")
                    Else
                        Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
                    End If
            End Select

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function obtenerObservaciones() As Entidades.Observacion

        Dim obj As Entidades.Observacion = Nothing
        If (Me.txtObservaciones.Text.Trim.Length > 0) Then
            obj = New Entidades.Observacion
            obj.Observacion = Me.txtObservaciones.Text.Trim
        End If
        Return obj
    End Function

    Private Function obtenerMovCaja() As Entidades.MovCaja
        Dim objMovCaja As New Entidades.MovCaja

        With objMovCaja

            .IdCaja = CInt(Me.cboCaja.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdPersona = CInt(Session("IdUsuario"))
            .IdTipoMovimiento = 2   '************* EGRESO

        End With

        Return objMovCaja
    End Function

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        verFrm(FrmModo.Nuevo, True, True, True, True, True, True)
    End Sub

    Private Sub btnBuscarDocumentoxcodigo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarDocumentoxCodigo.Click
        buscarDocumento(CInt(Me.cboSerie.SelectedValue), CInt(Me.hddCodigoDocumento.Value), Nothing)
    End Sub

    Private Sub cargarDocumentoCab(ByVal objDocumento As Entidades.Documento)

        Me.txtCodigoDocumento.Text = objDocumento.Codigo
        Me.txtFechaEmision.Text = Format(objDocumento.FechaEmision, "dd/MM/yyyy")
        Me.cboEstado.SelectedValue = CStr(objDocumento.IdEstadoDoc)
        Me.hddIdDocumento.Value = CStr(objDocumento.Id)
        Me.txtTotalAPagar.Text = CStr(Math.Round(objDocumento.TotalAPagar, 2))

        If (Me.cboMoneda.Items.FindByValue(CStr(objDocumento.IdMoneda)) IsNot Nothing) Then
            Me.cboMoneda.SelectedValue = CStr(objDocumento.IdMoneda)
        End If
        If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumento.IdTipoOperacion)) IsNot Nothing) Then
            Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)
        End If

    End Sub

    Private Sub buscarDocumento(ByVal IdSerie As Integer, ByVal codigo As Integer, ByVal IdDocumento As Integer)
        Try

            Dim objDocumento As Entidades.Documento = Nothing

            If (IdDocumento = Nothing) Then
                objDocumento = (New Negocio.Documento).SelectxIdSeriexCodigo(IdSerie, codigo)
            Else
                objDocumento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)
            End If


            '******** Si paso es xq si existe el documento
            verFrm(FrmModo.Documento_Buscar_Exito, False, True, True, True, True, True)

            cargarDocumentoCab(objDocumento)

            cargarPersona(objDocumento.IdPersona)

            Dim objObservacion As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumento.Id)
            If (objObservacion IsNot Nothing) Then
                Me.txtObservaciones.Text = objObservacion.Observacion
            End If

            Me.ListaDetalleConcepto = obtenerListaDetalleConcepto_Load(objDocumento.Id)
            Me.GV_Otros.DataSource = Me.ListaDetalleConcepto
            Me.GV_Otros.DataBind()

            Me.ListaCancelacion = obtenerListaCancelacion_Load(objDocumento.Id)
            setListaCancelacion(Me.ListaCancelacion)
            Me.GV_Cancelacion.DataSource = Me.ListaCancelacion
            Me.GV_Cancelacion.DataBind()

            '************ Calculamos los datos de cancelación
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularDatosCancelacion();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaDetalleConcepto_Load(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleConcepto)

        Dim lista As List(Of Entidades.DetalleConcepto) = (New Negocio.DetalleConcepto).SelectxIdDocumento(IdDocumento)
        Dim listaConcepto As List(Of Entidades.Concepto) = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(CInt(Me.hddIdTipoDocumento.Value))

        For i As Integer = 0 To lista.Count - 1
            lista(i).ListaConcepto = listaConcepto
            lista(i).Moneda = Me.cboMoneda.SelectedItem.ToString
        Next

        Return lista
    End Function

    Private Function obtenerListaDetalleConcepto(ByVal generarNewLista As Boolean, ByVal cantidadInitNew As Integer) As List(Of Entidades.DetalleConcepto)

        Dim lista As New List(Of Entidades.DetalleConcepto)

        '********** Obtengo la lista de conceptos segun el tipo de Recibo
        Dim listaAux As List(Of Entidades.Concepto) = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(CInt(Me.hddIdTipoDocumento.Value))

        If (generarNewLista) Then  '********** Generamos la Lista de DetalleRecibo // Otros

            '************ Genero la lista
            For i As Integer = 0 To cantidadInitNew - 1

                Dim obj As New Entidades.DetalleConcepto

                obj.Moneda = Me.cboMoneda.SelectedItem.ToString
                obj.Monto = 0
                obj.Concepto = ""
                obj.ListaConcepto = listaAux
                obj.IdConcepto = Nothing
                lista.Add(obj)

            Next
        Else

            '**************** DEVOLVEMOS LA LISTA DEL DETALLE CONCEPTO (ACTUALIZAMOS)

            For i As Integer = 0 To Me.GV_Otros.Rows.Count - 1

                Dim obj As New Entidades.DetalleConcepto

                obj.Moneda = Me.cboMoneda.SelectedItem.ToString
                obj.Monto = CDec(CType(Me.GV_Otros.Rows(i).FindControl("txtAbono"), TextBox).Text)
                obj.Concepto = CType(Me.GV_Otros.Rows(i).FindControl("txtDescripcionConcepto"), TextBox).Text
                obj.ListaConcepto = listaAux
                obj.IdConcepto = CInt(CType(Me.GV_Otros.Rows(i).FindControl("cboConcepto"), DropDownList).SelectedValue)
                obj.IdMoneda = CInt(Me.cboMoneda.SelectedValue)
                lista.Add(obj)

            Next

        End If

        Return lista

    End Function

    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditar.Click

        Try
            verFrm(FrmModo.Editar, False, False, False, False, False, False, False)
            Me.GV_Cancelacion.DataSource = Nothing
            Me.GV_Cancelacion.DataBind()

            Me.ListaCancelacion = New List(Of Entidades.PagoCaja)
            setListaCancelacion(Me.ListaCancelacion)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularDatosCancelacion(); ", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
        
    End Sub

    Private Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnular.Click

        anularDocumentoReciboEgreso(CInt(Me.hddIdDocumento.Value))

    End Sub
    Private Sub anularDocumentoReciboEgreso(ByVal IdDocumento As Integer)
        Try


            If ((New Negocio.DocReciboCaja).ReciboCajaEgresoAnular(IdDocumento)) Then
                verFrm(FrmModo.Documento_Anular_Exito, False, False, False, False, False, False, False)
                objScript.mostrarMsjAlerta(Me, "El Proceso resultó con éxito.")
            Else
                objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", 1)  '******************** CLIENTE

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            Me.GV_Cancelacion.DataSource = Nothing
            Me.GV_Cancelacion.DataBind()

            Me.ListaCancelacion = New List(Of Entidades.PagoCaja)
            setListaCancelacion(Me.ListaCancelacion)

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "   offCapa('capaPersona');    calcularDatosCancelacion();    ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)

        Dim objPersonaView As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)
        If (objPersonaView IsNot Nothing) Then

            Me.txtDescripcionPersona.Text = objPersonaView.Descripcion
            Me.txtDNI.Text = objPersonaView.Dni
            Me.txtRUC.Text = objPersonaView.Ruc
            Me.hddIdPersona.Value = CStr(objPersonaView.IdPersona)

        End If
    End Sub
#End Region


#Region "************************ BÚSQUEDA AVANZADO"

    Private Sub btnAceptarBusquedaAvanzado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBusquedaAvanzado.Click
        busquedaAvanzado()
    End Sub

    Private Sub busquedaAvanzado()

        Try

            Dim IdPersona As Integer = 0
            Try
                IdPersona = CInt(Me.hddIdPersona.Value)
            Catch ex As Exception
                IdPersona = 0
            End Try

            Dim IdSerie As Integer = CInt(Me.cboSerie.SelectedValue)
            Dim fechaInicio As Date = CDate(Me.txtFechaInicio_BA.Text)
            Dim fechaFin As Date = CDate(Me.txtFechaFin_BA.Text)

            Me.GV_BusquedaAvanzado.DataSource = (New Negocio.Documento).DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(CInt(Me.hddIdTipoDocumento.Value), IdSerie, IdPersona, fechaInicio, fechaFin)
            Me.GV_BusquedaAvanzado.DataBind()

            If (Me.GV_BusquedaAvanzado.Rows.Count <= 0) Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  onCapa('capaBusquedaAvanzado');  alert('No se hallaron registros.');    ", True)
            Else
                objScript.onCapa(Me, "capaBusquedaAvanzado")
            End If



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Protected Sub GV_BusquedaAvanzado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BusquedaAvanzado.SelectedIndexChanged
        Try
            '************  cargarDocumentoNotaCredito(0, 0, )
            buscarDocumento(0, 0, CInt(CType(Me.GV_BusquedaAvanzado.SelectedRow.FindControl("hddIdDocumento_BusquedaAvanzado"), HiddenField).Value))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

#End Region

#Region "**************************** DATOS DE CANCELACION"

    Private Function obtenerListaCancelacion_Load(ByVal IdDocumento As Integer) As List(Of Entidades.PagoCaja)
        Dim lista As List(Of Entidades.PagoCaja) = (New Negocio.PagoCaja).SelectListaxIdDocumento(IdDocumento)

        For i As Integer = 0 To lista.Count - 1

            Select Case lista(i).IdMedioPagoInterfaz


                Case 1  '*********** EFECTIVO

                    lista(i).descripcionPagoCaja = " Medio Pago: [ " + lista(i).NomMedioPago + " ] Monto: [ " + lista(i).NomMoneda + " " + CStr(Math.Round(lista(i).Efectivo, 2)) + " ]"

            End Select


        Next

        Return lista

    End Function

    Protected Sub cboMedioPago_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboMedioPago.SelectedIndexChanged
        Try
            actualizarControlesMedioPago(1, CInt(Me.cboMedioPago.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub actualizarControlesMedioPago(ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer)

        '************ Limpiar Datos de Cancelación
        Me.txtMonto_DatoCancelacion.Text = "0"


        Select Case IdCondicionPago
            Case 1  '**************** CONTADO
                Me.Panel_CP_Contado.Visible = True

                '*************** MEDIO DE PAGO
                Me.lblMonto_DC.Visible = False
                Me.txtMonto_DatoCancelacion.Visible = False
                Me.cboMoneda_DatoCancelacion.Visible = False
                Me.btnAddDatoCancelacion.Visible = False


                Dim objMedioPago As Entidades.MedioPago = (New Negocio.MedioPago).SelectxId(IdMedioPago)(0)

                Select Case objMedioPago.IdMedioPagoInterfaz

                    Case 1  '*************** EFECTIVO
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True

                End Select

                Me.hddIdMedioPagoInterfaz.Value = CStr(objMedioPago.IdMedioPagoInterfaz)

            Case 2  '**************** CREDITO
                Me.Panel_CP_Contado.Visible = False

        End Select

    End Sub

    Protected Sub btnAddDatoCancelacion_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddDatoCancelacion.Click

        addDatoCancelacion()

    End Sub

    Private Sub addDatoCancelacion()
        Try

            Me.listaCancelacion = getListaCancelacion()

            Dim objPagoCaja As New Entidades.PagoCaja

            objPagoCaja.IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
            objPagoCaja.Factor = -1
            objPagoCaja.IdTipoMovimiento = 2 '*************** EGRESO
            objPagoCaja.IdMedioPagoInterfaz = CInt(Me.hddIdMedioPagoInterfaz.Value)

            Select Case CInt(Me.hddIdMedioPagoInterfaz.Value)

                Case 1  '*********** EFECTIVO

                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.NomMonedaDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objPagoCaja.Efectivo = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objPagoCaja.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)
                    objPagoCaja.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    objPagoCaja.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objPagoCaja.Efectivo, objPagoCaja.IdMoneda, objPagoCaja.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))
                    objPagoCaja.descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objPagoCaja.Efectivo, 2)) + " ]"

                    'Case 2  '*********** TARJETA

                    '    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    '    objPagoCaja.NomMonedaDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    '    objPagoCaja.Efectivo = CDec(Me.txtMonto_DatoCancelacion.Text)
                    '    objPagoCaja.EfectivoInicial = CDec(Me.txtMonto_DatoCancelacion.Text)
                    '    objPagoCaja.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)
                    '    objPagoCaja.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    '    objPagoCaja.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objPagoCaja.Efectivo, objPagoCaja.IdMoneda, objPagoCaja.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))
                    '    objPagoCaja.descripcionPagoCaja = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objPagoCaja.Efectivo, 2)) + " ]"



            End Select

            Me.listaCancelacion.Add(objPagoCaja)
            setListaCancelacion(Me.listaCancelacion)

            Me.GV_Cancelacion.DataSource = Me.listaCancelacion
            Me.GV_Cancelacion.DataBind()

            Me.txtMonto_DatoCancelacion.Text = "0"

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularDatosCancelacion(); ", True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Protected Sub GV_Cancelacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_Cancelacion.SelectedIndexChanged
        quitarDatoCancelacion()
    End Sub

    Private Sub quitarDatoCancelacion()

        Try

            Me.ListaCancelacion = getListaCancelacion()
            Me.ListaCancelacion.RemoveAt(Me.GV_Cancelacion.SelectedRow.RowIndex)

            setListaCancelacion(Me.ListaCancelacion)

            Me.GV_Cancelacion.DataSource = Me.ListaCancelacion
            Me.GV_Cancelacion.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularDatosCancelacion(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


#End Region


End Class