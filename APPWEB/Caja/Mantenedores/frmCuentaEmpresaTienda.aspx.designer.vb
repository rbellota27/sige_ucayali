﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmCuentaEmpresaTienda

    '''<summary>
    '''btGuardar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btGuardar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btCancelar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btCancelar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''dlempresa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dlempresa As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''dltienda control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dltienda As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''dlmoneda control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dlmoneda As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''btagregar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btagregar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''gvCuenta control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvCuenta As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''rb_estado control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rb_estado As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''gvregistros control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvregistros As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''hdd_operativo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdd_operativo As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdd_idusuario control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdd_idusuario As Global.System.Web.UI.WebControls.HiddenField
End Class
