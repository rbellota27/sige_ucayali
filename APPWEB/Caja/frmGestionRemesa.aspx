﻿<%@ Page Title="Gestion Remesa" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmGestionRemesa.aspx.vb" Inherits="APPWEB.frmGestionRemesa" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" ToolTip="Habilitar la creación de un nuevo documento"
                                Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClientClick="return( onclick_Buscar() );"
                                ToolTip="Habilitar controles de búsqueda" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnEditar" runat="server" Text="Editar" ToolTip="Habilitar la edición de documento"
                                Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClientClick=" return ( onClick_Guardar() ); "
                                ToolTip="Regitrar la información de documento" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnAnular" runat="server" Text="Anular" OnClientClick=" return ( onClick_Anular() ); "
                                ToolTip="Regitrar la información de documento" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscarDocumentoRef" runat="server" Text="Documento Ref." OnClientClick=" return ( onClick_BuscarDocumentoRef() ); "
                                ToolTip="Agregar un documento de referencia" />
                        </td>
                        <td>
                            <asp:Button ID="btnCheque" runat="server" Text="Cheques" OnClientClick=" return ( onClick_BuscarCheques() ); "
                                ToolTip="Agregar un cheque" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                gestion de Remesas
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto">
                            Empresa:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_Empresa" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto">
                            Tienda:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_Tienda" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto">
                            Serie:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_Serie" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox ID="tb_NroDocumento" CssClass="TextBox_ReadOnly" Font-Bold="true" MaxLength="12"
                                Width="85px" onKeyPress=" return ( onKeyPress_NroDocumento() );" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:ImageButton ID="Img_BuscarDocumento" OnClientClick="return ( onclick_BuscarDocumento() );"
                                runat="server" ImageUrl="~/Caja/iconos/ok.gif" ToolTip="Buscar documento por [ serie - número ]" />
                        </td>
                        <td>
                            <asp:LinkButton ID="lkb_Avanzado" runat="server" ToolTip="Búsqueda avanzada de documento"
                                CssClass="Texto" OnClientClick="return ( onClick_Avanzado() ) ;">Avanzado</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto">
                            Estado:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_EstadoDoc" runat="server" Enabled="false">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto">
                            Tipo Operaci&oacute;n:
                        </td>
                        <td colspan="6">
                            <asp:DropDownList ID="ddl_TipoOperacion" runat="server" Width="100%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto">
                            Emisi&oacute;n:
                        </td>
                        <td>
                            <asp:TextBox ID="tb_FechaEmision" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                Width="100px"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="MaskedEditExtender_tb_FechaEmision" runat="server" ClearMaskOnLostFocus="false"
                                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="tb_FechaEmision">
                            </cc1:MaskedEditExtender>
                            <cc1:CalendarExtender ID="CalendarExtender_tb_FechaEmision" runat="server" Enabled="True"
                                Format="dd/MM/yyyy" TargetControlID="tb_FechaEmision">
                            </cc1:CalendarExtender>
                        </td>
                        <td class="Texto">
                            Caja:
                        </td>
                        <td colspan="6">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddl_caja" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Texto">
                                        Fecha Caja:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tb_FechaCaja" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                            Width="100px"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="MaskedEditExtender_tb_FechaCaja" runat="server" ClearMaskOnLostFocus="false"
                                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                            CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                            CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="tb_FechaCaja">
                                        </cc1:MaskedEditExtender>
                                        <cc1:CalendarExtender ID="CalendarExtender_tb_FechaCaja" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" TargetControlID="tb_FechaCaja">
                                        </cc1:CalendarExtender>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="btnBuscarCajaCierre" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                            OnClientClick="return( onClick_BuscarCajaCierre()   );" ToolTip="Consultar Cierre de Caja" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="9">
                            <asp:GridView ID="GV_CajaAperuraCierre" runat="server" AutoGenerateColumns="False"
                                Width="100%" RowStyle-Height="25px">
                                <HeaderStyle CssClass="GrillaHeader" />
                                <Columns>
                                    <asp:BoundField HeaderText="Responsable Caja" DataField="NomPersona" />
                                    <asp:BoundField HeaderText="F. Apertura" DataField="cac_FechaApertura" DataFormatString="{0:dd/MM/yyyy hh:mm:ss}" />
                                    <asp:BoundField HeaderText="F. Cierre" DataField="cac_FechaCierre" DataFormatString="{0:dd/MM/yyyy hh:mm:ss}" />
                                    <asp:BoundField HeaderText="" DataField="mon_Simbolo" />
                                    <asp:BoundField HeaderText="M. Cierre Caja" DataField="cac_MontoCierre" DataFormatString="{0:F3}"
                                        ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true" />
                                </Columns>
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                Bancos
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Datos" runat="server" Width="100%">
                    <table>
                        <tr>
                            <td class="Texto">
                                Medio Pago:
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="ddl_MedioPago" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Tipo Concepto:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddl_TipoConceptoBanco" runat="server" Enabled="false">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Concepto Mov. Banco:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddl_ConceptoMovBanco" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Banco:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddl_Banco" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Cuenta Bancaria:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddl_CuentaBancaria" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Nro. Operaci&oacute;n:
                            </td>
                            <td>
                                <asp:TextBox ID="tb_NroOperacion" runat="server" Font-Bold="true" onKeyPress=" return ( onKeyPress_tb_NroOperacion(event) );"></asp:TextBox>
                            </td>
                            <%-- onkeypress="return valida(event)--%>
                            <td class="Texto">
                                Fecha Operacion:
                            </td>
                            <td>
                                <asp:TextBox ID="tb_FechaMov" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="MaskedEditExtender_tb_FechaMov" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="tb_FechaMov">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="CalendarExtender_tb_FechaMov" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="tb_FechaMov">
                                </cc1:CalendarExtender>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <table width="100%">
                                    <tr>
                                        <td class="TituloCeldaLeft">
                                            Documento Referencia
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:GridView ID="GV_DocumentoRef" runat="server" AutoGenerateColumns="False" Width="100%">
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <Columns>
                                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblTipoDocumento" Font-Bold="true" ForeColor="Red" runat="server"
                                                                            Text='<%#DataBinder.Eval(Container.DataItem,"NomTipoDocumento")%>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblNroDocumento" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:HiddenField ID="hddIdEmpresa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:HiddenField ID="hddIdAlmacen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:HiddenField ID="hddIdPersona" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="FechaEmision" HeaderText="Fecha Emisión" HeaderStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}" />
                                                    <asp:BoundField DataField="Empresa" HeaderText="Empresa" HeaderStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="Tienda" HeaderText="Tienda" HeaderStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:TemplateField HeaderText="Monto" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMontoMoneda" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblMonto" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F3}")%>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="TituloCeldaLeft">
                                            Cheque
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:GridView ID="GV_ChequeRef" runat="server" AutoGenerateColumns="false" Width="100%">
                                                <Columns>
                                                    <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" SelectText="Quitar"
                                                        ShowSelectButton="true" />
                                                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Nro Cheque" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:HiddenField ID="hddIdCheque" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCheque")%>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblBanco" runat="server" ForeColor="Red" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Banco")%>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblNroCheque" runat="server" ForeColor="Red" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Numero")%>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="FechaCobrar" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Fecha Cobrar" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMoneda" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblMonto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Monto","{0:F3}")%>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Moneda
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddl_Moneda" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            Monto:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tb_monto" runat="server" Width="65px" Font-Bold="true" onblur="return(valBlur(event));"
                                                onfocus="return(aceptarFoco(this));" onKeypress="return(validarNumeroPuntoPositivo('event'));">0</asp:TextBox>
                                        </td>
                                        <td class="Texto">
                                            Tipo Cambio:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tb_TipoCambio" runat="server" Width="65px" Font-Bold="true" onblur="return(valBlur(event));"
                                                onfocus="return(aceptarFoco(this));" onKeypress="return(validarNumeroPuntoPositivo('event'));">1</asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnAgregar" runat="server" Text="Agregar" OnClientClick=" return ( onClick_Agregar() ); "
                                                ToolTip="Agregar" Width="80px" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="TituloCeldaLeft">
                                Observaciones
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:TextBox ID="tb_Observacion" runat="server" Width="100%" MaxLength="200" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td class="SubTituloCelda">
                                Registro de Ingreso a Bancos
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_Remesas" runat="server" AutoGenerateColumns="False" Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                        <asp:TemplateField HeaderText="Concepto Mov. Banco" ItemStyle-Width="150px">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblConceptoMovBanco" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"ConceptoMovBanco") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdBanco" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdBanco") %>' />
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdCuentaBancaria" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdCuentaBancaria") %>' />
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdMedioPago" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdMedioPago") %>' />
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdPersonaRef" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdPersonaRef") %>' />
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdCaja" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdCaja") %>' />
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdTienda" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTienda") %>' />
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdConceptoMovBanco" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdConceptoMovBanco") %>' />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <ItemStyle Width="150px"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="F. Mov" DataField="FechaMov" DataFormatString="{0:dd/MM/yyy}" />
                                        <asp:BoundField HeaderText="Nro. Operación." DataField="NroOperacion" />
                                        <asp:BoundField HeaderText="Banco" DataField="Banco" />
                                        <asp:BoundField HeaderText="Cta. Bancaria" DataField="CuentaBancaria" />
                                        <asp:TemplateField HeaderText="Monto">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txtMonto" runat="server" Width="65px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                Text='<%# DataBinder.Eval(Container.DataItem,"Monto","{0:F3}") %>'></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Referencia">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Referencia") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddFrmModo" runat="server" Value="" />
                <asp:HiddenField ID="hddNroDocumento" runat="server" Value="" />
                <asp:HiddenField ID="hddIdDocumento" runat="server" Value="" />
            </td>
        </tr>
    </table>
    <div id="capaDocumentosReferencia" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 100px; left: 30px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton6" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaDocumentosReferencia')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tipo Documento:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddl_TipoDocumentoRef" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right" style="text-align: left">
                    <asp:RadioButtonList ID="rdbBuscarDocRef" runat="server" RepeatDirection="Horizontal"
                        CssClass="Texto" AutoPostBack="true">
                        <asp:ListItem Value="0" Selected="True">Por Fecha de Emisión</asp:ListItem>
                        <asp:ListItem Value="1">Por Nro. Documento</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxFecha" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaInicio_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaFin_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender5" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaFin_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRef" Width="70px" ToolTip="Buscar Documentos"
                                                    runat="server" Text="Buscar" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxCodigo" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Nro. Serie:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSerie_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                            </td>
                                            <td class="Texto">
                                                Nro. Código:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCodigo_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRefxCodigo" OnClientClick="return(  valOnClick_btnAceptarBuscarDocRefxCodigo()  );"
                                                    runat="server" Text="Buscar" Width="70px" ToolTip="Buscar Documentos" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_DocRef_Find" runat="server" ScrollBars="Horizontal" Width="100%">
                        <asp:GridView ID="GV_DocumentosReferencia_Find" runat="server" AutoGenerateColumns="false"
                            Width="100%">
                            <Columns>
                                <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="50px" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Tipo" />
                                <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                </td>
                                                <td>
                                                    <asp:HiddenField ID="hddIdPersona_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisión"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DescripcionPersona" HeaderText="Descripción" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Doc. Ref." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddl_TipoDocumentoRef" DataSource='<%#DataBinder.Eval(Container.DataItem,"getTipoDocumentoRef")%>'
                                            DataTextField="DescripcionCorto" DataValueField="Id" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaBusquedaAvanzado" style="border: 3px solid blue; padding: 8px; width: 850px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 142px; left: 22px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaDocumentosReferencia" runat="server" ToolTip="Cerrar"
                        ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaBusquedaAvanzado')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BusquedaAvanzado" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaI" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaI">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaI">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaF" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaF">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaF">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBusquedaAvanzado" Width="70px" ToolTip="Buscar Documentos"
                                                    runat="server" Text="Buscar" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_BusquedaAvanzado" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50px" HeaderStyle-Height="25px">
                                <HeaderStyle Height="25px" Width="50px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:CommandField>
                            <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Tipo"></asp:BoundField>
                            <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblNroDocumento_BusquedaAvanzado" ForeColor="Red" Font-Bold="true"
                                                    runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdDocumento_BusquedaAvanzado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisión"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="Tienda" HeaderText="Tienda" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="Caja" HeaderText="Caja" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />                            
                            <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td class="LabelRojo" style="font-weight: bold">
                    *** El proceso de búsqueda realiza un filtrado de acuerdo al cliente seleccionado.
                </td>
            </tr>
        </table>
    </div>
    <div id="capaCheques" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 100px; left: 30px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaCheques')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_cheques_Filtro" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tb_ChequeFechaIni" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MEEtb_ChequeFechaIni" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="tb_ChequeFechaIni">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CE_tb_ChequeFechaIni" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="tb_ChequeFechaIni">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tb_ChequeFechaFin" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender6" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="tb_ChequeFechaFin">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CE_tb_ChequeFechaFin" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="tb_ChequeFechaFin">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnBuscarCheque" Width="70px" ToolTip="Buscar Cheques" runat="server"
                                                    Text="Buscar" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_Cheque" runat="server" ScrollBars="Horizontal" Width="100%">
                        <asp:GridView ID="GV_Cheque" runat="server" AutoGenerateColumns="false" Width="100%">
                            <Columns>
                                <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" SelectText="Seleccionar"
                                    ShowSelectButton="true" />
                                <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Empresa/Tienda" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblEmpresa_Tienda" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Empresa_Tienda")%>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:HiddenField ID="hddIdCheque" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCheque")%>' />
                                                </td>
                                                <td>
                                                    <asp:HiddenField ID="hddIdMedioPago" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMedioPago")%>' />
                                                </td>
                                                <td>
                                                    <asp:HiddenField ID="hddIdMoneda" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                                </td>
                                                <td>
                                                    <asp:HiddenField ID="hddIdBanco" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdBanco")%>' />
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Cliente" HeaderStyle-HorizontalAlign="Center" HeaderText="Cliente"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="Banco" HeaderStyle-HorizontalAlign="Center" HeaderText="Banco"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NroCheque" HeaderStyle-HorizontalAlign="Center" HeaderText="Nro."
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="FechaMov" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Fecha Reg." ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="FechaCobrar" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Fecha Cobrar" ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblMoneda" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblMonto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Monto","{0:F3}")%>'></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="LabelRojo" style="font-weight: bold">
                    *** El proceso de búsqueda realiza un filtrado de acuerdo a la empresa y tienda
                    seleccionada.
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
        function onClick_BuscarCajaCierre() {
            return true;
        }

        function onClick_Agregar() {
            var IdMedioPago = parseInt(document.getElementById('<%=ddl_MedioPago.ClientID %>').value);
            if (isNaN(IdMedioPago)) { IdMedioPago = 0; }
            if (IdMedioPago == 0) {
                alert('SELECCIONE UN MEDIO PAGO. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }

            var IdConceptoMovBanco = parseInt(document.getElementById('<%=ddl_ConceptoMovBanco.ClientID %>').value);
            if (isNaN(IdConceptoMovBanco)) { IdConceptoMovBanco = 0; }
            if (IdConceptoMovBanco == 0) {
                alert('SELECCIONE UN CONCEPTO BANCO. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }

            var IdBanco = parseInt(document.getElementById('<%=ddl_Banco.ClientID %>').value);
            if (isNaN(IdBanco)) { IdBanco = 0; }
            if (IdBanco == 0) {
                alert('SELECCIONE UN BANCO. NO SE PERMITE LA OPERACION.');
                return false;
            }

            var IdCuentaBancaria = parseInt(document.getElementById('<%=ddl_CuentaBancaria.ClientID %>').value);
            if (isNaN(IdCuentaBancaria)) { IdCuentaBancaria = 0; }
            if (IdCuentaBancaria == 0) {
                alert('SELECCIONE UNA CUENTA BANCARIA. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }

            var txtNroOperacion = document.getElementById('<%=tb_NroOperacion.ClientID %>');
            if (txtNroOperacion.value.length <= 0) {
                alert('INGRESE EL NRO. DE OPERACIÓN');
                txtNroOperacion.select();
                txtNroOperacion.focus();
                return false;
            }



            return true;
        }

        function valida(e) {
            tecla = (document.all) ? e.keyCode : e.which;

            //Tecla de retroceso para borrar, siempre la permite
            if (tecla == 8) {
                return true;
            }

            // Patron de entrada, en este caso solo acepta numeros
            patron = /[0-9]/;
            tecla_final = String.fromCharCode(tecla);
            return patron.test(tecla_final);
        }

        function onKeyPress_tb_NroOperacion(e) 
        {
            tecla = (document.all) ? e.keyCode : e.which;
            //Tecla de retroceso para borrar, siempre la permite
            if (tecla == 8) {
                return true;
            }
            // Patron de entrada, en este caso solo acepta numeros
            patron = /[0-9]/;
            tecla_final = String.fromCharCode(tecla);
            return patron.test(tecla_final);
        }


        function onClick_Guardar() {

            var GV_Remesas = document.getElementById('<%=GV_Remesas.ClientID %>');
            if (GV_Remesas == null) {
                alert('GENERE REGISTROS DE BANCOS. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }

            return confirm('DESEA CONTINUAR CON LA OPERACIÓN.');
        }

        function onClick_Avanzado() {

            var lkb_Avanzado = document.getElementById('<%=lkb_Avanzado.ClientID %>');
            if (lkb_Avanzado.disabled == true) {
                alert('Habilite la búsqueda Avanzada [ boton buscar ]');
                return false;
            }

            onCapa('capaBusquedaAvanzado');

            return false;
        }

        function onClick_Anular() {
            return confirm('Desea continuar con el proceso de anulación del documento.')
        }

        function onclick_Buscar() {
            document.getElementById('<%=lkb_Avanzado.ClientID %>').disabled = false;
            var NroDocumento = document.getElementById('<%=tb_NroDocumento.ClientID %>');
            NroDocumento.readOnly = false;
            NroDocumento.focus();
            NroDocumento.select();
            return false;
        }

        function onKeyPress_NroDocumento() {
            var key = event.keyCode;
            if (key == 13) {
                document.getElementById('<%=Img_BuscarDocumento.ClientID %>').focus();
            } else {
                return onKeyPressEsNumero('event');
            }
        }
        function onclick_BuscarDocumento() {
            var NroDocumento = document.getElementById('<%=tb_NroDocumento.ClientID %>').value;
            document.getElementById('<%=hddNroDocumento.ClientID %>').value = NroDocumento;
            return true;
        }

        function onClick_BuscarDocumentoRef() {
            onCapa('capaDocumentosReferencia');
            return false;
        }

        function onClick_BuscarCheques() {
            onCapa('capaCheques');
            return false;
        }

        history.forward();
                
    </script>

</asp:Content>
