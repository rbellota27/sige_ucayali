﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'''''''''''''''''LUNES 15 MARZO 7:29 PM

Public Class Reportes
    Private objDaoReportes As New DAO.DAOReportes


    Public Function getResumidoGlobal(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Fecha As String) As DataSet
        Return objDaoReportes.getResumidoGlobal(IdEmpresa, IdTienda, Fecha)
    End Function

    Public Function getResumendiarioVentas(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal fechaI As String, ByVal fechaF As String) As DataSet
        Return objDaoReportes.getResumendiarioVentas(idempresa, idtienda, fechaI, fechaF)
    End Function

    Public Function getDataSetReciboIngreso(ByVal IdDocumento As Integer) As DataSet
        Return objDaoReportes.getDataSetReciboIngreso(IdDocumento)
    End Function

    Public Function getDataSetDocCotizacion(ByVal IdDocumento As Integer) As DataSet
        Return objDaoReportes.getDataSetDocCotizacion(IdDocumento)
    End Function
    Public Function getReporteDocCotizacion(ByVal IdDocumento As Integer, ByVal tipoimpresion As Integer) As DataSet
        Return objDaoReportes.getReporteDocCotizacion(IdDocumento, tipoimpresion)
    End Function
    Public Function getReporteComprobantePercepcion(ByVal IdDocumento As Integer) As DataSet
        Return objDaoReportes.getReporteComprobantePercepcion(IdDocumento)
    End Function



    Public Function getDataSetInvNOValorizado(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer) As DataSet
        Return objDaoReportes.getDataSetInvNOValorizado(IdEmpresa, IdAlmacen, IdLinea, IdSubLinea)
    End Function
    Public Function getDataSetResumenCXCResumen(ByVal IdEmpresa As Integer, ByVal idtienda As Integer) As DataSet
        Return objDaoReportes.getResumenCXCResumen(IdEmpresa, idtienda)
    End Function

    Public Function getDataSetCatalogoProducto_UM(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal UMedida As Integer) As DataSet
        Return objDaoReportes.getDataSetCatalogoProducto_UM(IdLinea, IdSubLinea, UMedida)
    End Function
    Public Function getDataSetMovCajaxParams(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idcaja As Integer, ByVal idmediopago As Integer, ByVal idtipomov As Integer, ByVal fechaI As Date, ByVal fechaF As Date) As DataSet
        Return objDaoReportes.getDataSetMovCajaxParams(idempresa, idtienda, idcaja, idmediopago, idtipomov, fechaI, fechaF)
    End Function
    Public Function getDataSetFactura(ByVal IdDocumento As Integer) As DataSet
        Return objDaoReportes.getDataSetFactura(IdDocumento)
    End Function
    Public Function getDataSetCatalogo() As DataSet
        Return objDaoReportes.getDataSetCatalogo
    End Function
    Public Function getDataSetInvInicial(ByVal idempresa As Integer, ByVal idalmacen As Integer, ByVal idlinea As Integer, ByVal idsublinea As Integer) As DataSet
        Return objDaoReportes.getDataSetInvInicial(idempresa, idalmacen, idlinea, idsublinea)
    End Function
    Public Function getDataSetDocInventarioInicial(ByVal idDocumento As Integer) As DataSet
        Return objDaoReportes.getDataSetDocInventarioInicial(idDocumento)
    End Function
    Public Function getDataSetBoleta(ByVal IdDocumento As Integer) As DataSet
        Return objDaoReportes.getDataSetBoleta(IdDocumento)
    End Function
    Public Function getDataSetCotizacion(ByVal IdDocumento As Integer) As DataSet
        Return objDaoReportes.getDataSetCotizacion(IdDocumento)
    End Function
    Public Function getDataSetPedidoCliente(ByVal IdDocumento As Integer) As DataSet
        Return objDaoReportes.getDataSetPedidoCliente(IdDocumento)
    End Function
    Public Function getDataSetOrdenDespacho(ByVal IdDocumento As Integer) As DataSet
        Return objDaoReportes.getDataSetOrdenDespacho(IdDocumento)
    End Function
    Public Function getDataSetDocGuiaRecepcion(ByVal IdDocumento As Integer) As DataSet
        Return objDaoReportes.getDataSetDocGuiaRecepcion(IdDocumento)
    End Function
    Public Function getDataSetDocGuiaRemision(ByVal IdDocumento As Integer) As DataSet
        Return objDaoReportes.getDataSetDocGuiaRemision(IdDocumento)
    End Function
    Public Function getDataSetMovCajaxParams2(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idtipodocumento As Integer, ByVal idcaja As Integer, ByVal idmediopago As Integer, ByVal idtipomov As Integer, ByVal fechaI As Date, ByVal fechaF As Date) As DataSet
        Return objDaoReportes.getDataSetMovCajaxParams2(idempresa, idtienda, idtipodocumento, idcaja, idmediopago, idtipomov, fechaI, fechaF)
    End Function
    Public Function getDataSetChequeEgreso(ByVal IdDocumento As Integer) As DataSet
        Return objDaoReportes.getDataSetChequeEgreso(IdDocumento)
    End Function
    Public Function getDataSetChequeImpresion(ByVal IdDocumento As Integer) As DataSet
        Return objDaoReportes.getDataSetChequeImpresion(IdDocumento)
    End Function

    'Public Function getDataSetChequeImpresionV2(ByVal IdDocumento As Integer) As DataSet
    '    Return objDaoReportes.getDataSetChequeImpresionV2(IdDocumento)
    'End Function
    Public Function getDataSetChequeImpresionV2(ByVal IdDocumento As Integer) As DataSet
        Return objDaoReportes.getDataSetChequeImpresionV2(IdDocumento)
    End Function
    Public Function getDataSetDocLetras(ByVal IdDocumento As Integer) As DataSet
        Return objDaoReportes.getDataSetDocCancelBancos(IdDocumento)
    End Function
    Public Function getDataSetDocCancelBancos(ByVal IdDocumento As Integer) As DataSet
        Return objDaoReportes.getDataSetDocCancelBancos(IdDocumento)
    End Function
    Public Function getDataSetDocEgresos(ByVal IdDocumento As Integer) As DataSet
        Return objDaoReportes.getDataSetDocEgresos(IdDocumento)
    End Function
    Public Function getDataSetDocCancelCaja(ByVal IdDocumento As Integer) As DataSet
        Return objDaoReportes.getDataSetDocCancelCaja(IdDocumento)
    End Function
    Public Function rptListarProductos(ByVal linea As Integer, ByVal sublinea As Integer, ByVal producto As Integer) As DataSet
        Return objDaoReportes.rptListarProductos(linea, sublinea, producto)
    End Function
End Class