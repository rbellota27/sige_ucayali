﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Web.UI.WebControls
Public Class ComboBox
    Public Sub llenarCboAlmacen(ByVal cbo As DropDownList)
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdAlmacen"
    End Sub
    Public Sub llenarCboTienda(ByVal cbo As DropDownList)
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
    End Sub
    Public Sub llenarComboBoxTipoExistencia(ByVal comboBox1 As DropDownList)
        comboBox1.DataTextField = "Nombre"
        comboBox1.DataValueField = "Id"
    End Sub
    Public Sub llenarComboBoxLinea(ByVal comboBox1 As DropDownList)
        comboBox1.DataTextField = "Descripcion"
        comboBox1.DataValueField = "Id"
    End Sub
    Public Sub llenarCboPropietario(ByVal cbo As DropDownList)
        cbo.DataTextField = "NombreComercial"
        cbo.DataValueField = "Id"
    End Sub
    Public Sub llenarComboBoxSubLinea(ByVal comboBox1 As DropDownList)
        comboBox1.DataTextField = "Nombre"
        comboBox1.DataValueField = "Id"
    End Sub
    Public Sub llenarComboBoxProveedor(ByVal comboBox1 As DropDownList)
        comboBox1.DataTextField = "NombreComercial"
        comboBox1.DataValueField = "IdPersona"
    End Sub
    Public Sub llenarComboBoxEstadoProd(ByVal comboBox1 As DropDownList)
        comboBox1.DataTextField = "Descripcion"
        comboBox1.DataValueField = "Id"
    End Sub
    Public Sub llenarComboBoxControlador(ByVal comboBox1 As DropDownList)
        comboBox1.DataTextField = "nombre"
        comboBox1.DataValueField = "IdPersona"
    End Sub
    Public Sub llenarComboBoxUMedida(ByVal comboBox1 As DropDownList)
        comboBox1.DataTextField = "DescripcionCorto"
        comboBox1.DataValueField = "Id"
    End Sub
    Public Sub llenarComboBoxTalla(ByVal comboBox1 As DropDownList)
        comboBox1.DataTextField = "Nombre"
        comboBox1.DataValueField = "Id"
    End Sub
    Public Sub llenarComboBoxTamanio(ByVal comboBox1 As DropDownList)
        comboBox1.DataTextField = "Nombre"
        comboBox1.DataValueField = "Id"
    End Sub
    Public Sub llenarComboBoxColor(ByVal comboBox1 As DropDownList)
        comboBox1.DataTextField = "Nombre"
        comboBox1.DataValueField = "Id"
    End Sub
    Public Sub llenarComboBoxSabor(ByVal comboBox1 As DropDownList)
        comboBox1.DataTextField = "Descripcion"
        comboBox1.DataValueField = "Id"
    End Sub
    Public Sub llenarComboBoxFormaProd(ByVal comboBox1 As DropDownList)
        comboBox1.DataTextField = "Nombre"
        comboBox1.DataValueField = "Id"
    End Sub
    Public Sub llenarComboBoxTipoProd(ByVal comboBox1 As DropDownList)
        comboBox1.DataTextField = "Nombre"
        comboBox1.DataValueField = "Id"
    End Sub
    Public Sub llenarComboBoxProducto(ByVal comboBox1 As DropDownList)
        comboBox1.DataTextField = "Descripcion"
        comboBox1.DataValueField = "Id"
    End Sub
    Public Sub llenarCboTipoOperacion(ByVal cbo As DropDownList, Optional ByVal addElemento As Boolean = False)
        Dim obj As New Negocio.TipoOperacion
        Dim lista As List(Of Entidades.TipoOperacion) = obj.SelectAllActivo()

        If addElemento Then
            Dim objTipoOperacion As New Entidades.TipoOperacion(0, "-----")
            lista.Insert(0, objTipoOperacion)
        End If

        cbo.DataValueField = "Id"
        cbo.DataTextField = "Nombre"
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub
    'Public Sub llenarCboBoxTipoOperacion(ByVal comboBox1 As DropDownList)
    '    comboBox1.DataTextField = "Nombre"
    '    comboBox1.DataTextField = "Id"
    'End Sub
End Class
