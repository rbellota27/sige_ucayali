﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Procedencia
    Private objDaoProcedencia As New DAO.DAOProcedencia
    Public Function SelectAllActivo_Cbo() As List(Of Entidades.Procedencia)
        Return objDaoProcedencia.SelectAllActivo_Cbo
    End Function
    Public Function PerfilUsuarioSelectxIdPersona(ByVal idpersona As Integer) As List(Of Entidades.Procedencia)
        Return objDaoProcedencia.PerfilUsuarioSelectxIdPersona(idpersona)
    End Function
    Public Function SelectAllActivoxIdPais(ByVal IdPais As String) As List(Of Entidades.Procedencia)
        Return objDaoProcedencia.SelectAllActivoxIdPais(IdPais)
    End Function
End Class
