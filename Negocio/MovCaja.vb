﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class MovCaja
    Dim obj As New DAO.DAOMovCaja
    Public Function IngresosDiariosxParams(ByVal IdTienda As Integer, ByVal IdEmpresa As Integer) As DataSet
        Return obj.IngresosDiariosxParams(IdTienda, IdEmpresa)
    End Function
    '********************************************************
    'Método fnInsTblDetalleRecibo, para obtener IdDetalleRecibo 
    'Autor   : Hans Fernando, Quispe Espinoza
    'Módulo  : Caja-Documento
    'Sistema : Indusfer
    'Empresa : Digrafic SRL
    'Fecha   : 8-Set-2009
    'Parametros : BEDetalleRecibo
    'Retorna : IdDetalleRecibo
    '********************************************************
    Public Function fnInsTblMovCaja(ByVal poBETblMovCaja As Entidades.MovCaja) As Integer
        Return obj.fnInsTblMovCaja(poBETblMovCaja)
    End Function
End Class
