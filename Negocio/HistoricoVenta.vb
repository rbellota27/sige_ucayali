﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




'************************   MARTES 20 ABRIL 2010 HORA 05_51 PM








Imports System.Data.SqlClient

Public Class HistoricoVenta

    Private objDaoHV As New DAO.DAOHistoricoVenta
    Private cn As SqlConnection
    Private tr As SqlTransaction

    Public Function HistoricoVenta_GenerarCargaDatosxAnioMes(ByVal Anio As Integer, ByVal Mes As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer) As Boolean
        Dim hecho As Boolean = False
        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            objDaoHV.HistoricoVenta_GenerarCargaDatosxAnioMes(Anio, Mes, IdEmpresa, IdTienda, IdLinea, IdSubLinea, cn, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return hecho
    End Function
End Class
