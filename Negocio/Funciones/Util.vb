﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient


Public Class Util
    Private obj As New DAO.DAOUtil


    Public Function fn_ProductoMedida(ByVal IdProducto As Integer, ByVal IdUnidadMedida As Integer, ByVal IdMagnitud As Integer, ByVal IdUmMagnitudDestino As Integer, ByVal Valor As Decimal) As Decimal
        Return obj.fn_ProductoMedida(IdProducto, IdUnidadMedida, IdMagnitud, IdUmMagnitudDestino, Valor)
    End Function

    Public Function valDocComprometerStock(ByVal IdDocumento As Integer) As Integer
        Return obj.valDocComprometerStock(IdDocumento)
    End Function

    Public Function fx_StockReal(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdProducto As Integer) As Decimal
        Return obj.fx_StockReal(IdEmpresa, IdAlmacen, IdProducto)
    End Function

    Public Function fx_StockDisponible(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdProducto As Integer) As Decimal
        Return obj.fx_StockDisponible(IdEmpresa, IdAlmacen, IdProducto)
    End Function

    Public Function fx_StockComprometidoxIdEmpresaxIdAlmacenxIdProducto(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdProducto As Integer) As Decimal
        Return obj.fx_StockComprometidoxIdEmpresaxIdAlmacenxIdProducto(IdEmpresa, IdAlmacen, IdProducto)
    End Function

    Public Function Fn_getStockTransito(ByVal IdAlmacen As Integer, ByVal IdProducto As Integer) As Decimal
        Return obj.Fn_getStockTransito(IdAlmacen, IdProducto)
    End Function

    Public Function fx_getCantidadxTono(ByVal idProducto As Integer, ByVal idTono As Integer) As Decimal
        Return obj.fx_getCantidadxTono(idProducto, idTono)
    End Function

    Public Function fx_getValorEquivalenteProducto(ByVal IdProducto As Integer, ByVal IdUM_Origen As Integer, ByVal IdUM_Destino As Integer, ByVal Cantidad As Decimal) As Decimal
        Return obj.fx_getValorEquivalenteProducto(IdProducto, IdUM_Origen, IdUM_Destino, Cantidad)
    End Function

    Public Function fx_getMagnitudProducto(ByVal IdProducto As Integer, ByVal IdUMProducto As Integer, ByVal Cantidad As Decimal, ByVal IdMagnitud As Integer, ByVal IdUMMagnitud_Destino As Integer) As Decimal
        Return obj.fx_getMagnitudProducto(IdProducto, IdUMProducto, Cantidad, IdMagnitud, IdUMMagnitud_Destino)
    End Function

    Public Function CalcularValorxTipoCambio(ByVal valor As Decimal, ByVal IdMonedaOrigen As Integer, ByVal IdMonedaDestino As Integer, ByVal codigo As String, ByVal fecha As Date) As Decimal
        Return obj.CalcularValorxTipoCambio(valor, IdMonedaOrigen, IdMonedaDestino, codigo, fecha)
    End Function

    Public Function fx_getCostoProducto(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdProducto As Integer, ByVal fechaFin As Date) As Decimal
        Return obj.fx_getCostoProducto(IdEmpresa, IdAlmacen, IdProducto, fechaFin)
    End Function
    Public Function fx_getParamVerInterfaceMaestroObra(ByVal idparam As Integer) As Integer
        Return obj.fx_getParamVerInterfaceMaestroObra(idparam)
    End Function
    Public Function fx_getValorParametroGeneralxIdparam(ByVal idparam As String) As String
        Return obj.fx_getValorParametroGeneralxIdparam(idparam)
    End Function

    Public Function fx_getCantidadMovAlmacenxIdDetalleDocumento(ByVal IdDetalleDocumento As Integer) As Decimal
        Return obj.fx_getCantidadMovAlmacenxIdDetalleDocumento(IdDetalleDocumento)
    End Function

    Public Function fx_StockRealxIdEmpresaxIdAlmacenxIdProductoxFechaFin(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdProducto As Integer, ByVal fechaFin As Date) As Decimal
        Return obj.fx_StockRealxIdEmpresaxIdAlmacenxIdProductoxFechaFin(IdEmpresa, IdAlmacen, IdProducto, fechaFin)
    End Function

    Public Function ValidarEdicionDocxIdUsuarioxIdPermiso(ByVal IdUsuario As Integer, ByVal IdPermiso() As Integer, ByVal doc_serie As Integer, _
                                                          ByVal doc_codigo As Integer, ByVal idTIpodocumento As Integer) As Integer()

        Dim lista(IdPermiso.Length - 1) As Integer
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Try
            cn.Open()
            For i As Integer = 0 To IdPermiso.Length - 1
                lista(i) = (New DAO.DAOUsuario).ValidarEdicionDocxIdUsuarioxIdPermiso(IdUsuario, IdPermiso(i), doc_serie, doc_codigo, idTIpodocumento, cn)
            Next
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return lista
    End Function

    Public Function ValidarPermisosxIdUsuario(ByVal IdUsuario As Integer, ByVal IdPermiso() As Integer) As Integer()

        Dim lista(IdPermiso.Length - 1) As Integer
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Try
            cn.Open()
            For i As Integer = 0 To IdPermiso.Length - 1
                lista(i) = (New DAO.DAOUsuario).ValidarPermisoxIdUsuarioxIdPermiso(IdUsuario, IdPermiso(i), cn)
            Next
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return lista
    End Function

    Public Function ValidarPermisosRolTipoPv(ByVal IdUsuario As Integer) As Integer

        Dim lista As Integer
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Try
            cn.Open()

            lista = (New DAO.DAOUsuario).ValidarPermisosRolTipoPv(IdUsuario, cn)

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return lista
    End Function



    Public Function ValidarPermisosxIdUsuarioxTipoOperacion(ByVal IdUsuario As Integer, ByVal CadenaIdPermiso As String) As List(Of Entidades.be_validacion)

        'Dim lista(IdPermiso.Length - 1) As Integer
        Dim lista As List(Of Entidades.be_validacion)
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Try
            cn.Open()
            lista = (New DAO.DAOUsuario).ValidarPermisoxIdUsuarioxIdPermisoxTipoOperacion(IdUsuario, CadenaIdPermiso, cn)
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return lista
    End Function

    Public Function SelectValorxIdMonedaOxIdMonedaD(ByVal IdMonedaOrigen As Integer, ByVal IdMonedaDestino As Integer, ByVal ValorAConvertir As Decimal, ByVal Codigo As String) As Decimal

        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Dim valor As Decimal = 0

        Try
            cn.Open()


            valor = obj.SelectValorxIdMonedaOxIdMonedaD(IdMonedaOrigen, IdMonedaDestino, ValorAConvertir, cn, Codigo)

        Catch ex As Exception

            Throw ex

        Finally

            If cn.State = ConnectionState.Open Then cn.Close()

        End Try

        Return valor

    End Function
    Public Function ValidarExistenciaxTablax2CamposForUpdate(ByVal nomTabla As String, ByVal nomCampo As String, ByVal valor As String, ByVal idCampo As String, ByVal idValor As Integer) As Integer
        Return obj.ValidarExistenciaxTablax2CamposForUpdate(nomTabla, nomCampo, valor, idCampo, idValor)
    End Function
    Public Function ValidarExistenciaxTablax1Campo(ByVal nomTabla As String, ByVal nomCampo As String, ByVal valor As String) As Integer
        Return obj.ValidarExistenciaxTablax1Campo(nomTabla, nomCampo, valor)
    End Function
    Public Function ValidarExistenciaxTablax4Campos(ByVal nomTabla As String, ByVal nomCampo1 As String, ByVal valor1 As String, ByVal nomCampo2 As String, ByVal valor2 As String, ByVal nomCampo3 As String, ByVal valor3 As String, ByVal nomCampo4 As String, ByVal valor4 As String) As Integer
        Return obj.ValidarExistenciaxTablax4Campos(nomTabla, nomCampo1, valor1, nomCampo2, valor2, nomCampo3, valor3, nomCampo4, valor4)
    End Function
    Public Function ValidarExistenciaxTablax3Campos(ByVal nomTabla As String, ByVal nomCampo1 As String, ByVal valor1 As String, ByVal nomCampo2 As String, ByVal valor2 As String, ByVal nomCampo3 As String, ByVal valor3 As String) As Integer
        Return obj.ValidarExistenciaxTablax3Campos(nomTabla, nomCampo1, valor1, nomCampo2, valor2, nomCampo3, valor3)
    End Function
    Public Function ValidarxDosParametros(ByVal nomTabla As String, ByVal nomCampo1 As String, ByVal valor1 As String, ByVal nomCampo2 As String, ByVal valor2 As String) As Integer
        Return obj.ValidarxDosParametros(nomTabla, nomCampo1, valor1, nomCampo2, valor2)
    End Function
    Public Function ValidarxDosParametrosEnteros(ByVal nomTabla As String, ByVal nomCampo1 As String, ByVal valor1 As Integer, ByVal nomCampo2 As String, ByVal valor2 As Integer) As Integer
        Return obj.ValidarxDosParametrosEnteros(nomTabla, nomCampo1, valor1, nomCampo2, valor2)
    End Function
    Public Function ValidarTiendaxUsuario(idUsuario As Integer) As String
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO.DAOUtil).ValidarTiendaxUsuario(cn, idUsuario)
    End Function
  
End Class
