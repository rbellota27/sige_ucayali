﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




'*********************   JUEVES 11 MARZO 2010 HORA 06_08 PM








Imports System.Data.SqlClient
Public Class MotivoT_TipoOperacion
    Private obj As New DAO.DAOMotivoT_TipoOperacion
    Public Function ActualizaTipoDocumento_TipoOperacion(ByVal UpdateMT_TO As List(Of Entidades.MotivoT_TipoOperacion)) As Boolean
        Return obj.ActualizaMotivoTraslado_TipoOperacion(UpdateMT_TO)
    End Function
    'Edita la lista TipoDocumento_TipoOperacion
    Public Function _MotivoT_TipoOperacioNSelectxIdMotivoT(ByVal Idtraslado As Integer) As List(Of Entidades.MotivoT_TipoOperacion)
        Return obj._MotivoT_TipoOperacionSelectxIdTraslado(Idtraslado)
    End Function
    Public Function MotivoT_TipoOperacionBorrar(ByVal IdTraslado As Integer, ByVal cn As SqlConnection, Optional ByVal tr As SqlTransaction = Nothing) As Boolean
        Return obj.DeletexIdMotivoT(cn, tr, IdTraslado)
    End Function
    Public Function SelectCboTipoOperacion() As List(Of Entidades.MotivoT_TipoOperacion)
        Return obj.SelectCboTipoOperacion
    End Function

    Public Function SelectCboxIdTipoOperacion(ByVal IdTipoOperacion As Integer) As List(Of Entidades.MotivoTraslado)
        Return obj.SelectCboxIdTipoOperacion(IdTipoOperacion)
    End Function
End Class
