﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'*********************   MIERCOLES 16 FEBRERO 2011
'*********************   VIERNES 23 ABRIL 2010



Public Class Kit

    Private objDaoKit As New DAO.DAOKit


    Public Function SelectComponentexIdKitDOC_OC(ByVal IdKit As Integer, ByVal IdDocumento As Integer) As List(Of Entidades.Kit)
        Return objDaoKit.SelectComponentexIdKitDOC_OC(IdKit, IdDocumento)
    End Function

    Public Function SelectComponentexIdKit_OC(ByVal IdKit As Integer, ByVal IdMoneda As Integer) As List(Of Entidades.Kit)
        Return objDaoKit.SelectComponentexIdKit_OC(IdKit, IdMoneda)
    End Function

    Public Function SelectComponenteDetallexParams(ByVal IdKit As Integer, ByVal IdUnidadMedidaKit As Integer, ByVal CantidadKit As Decimal, ByVal IdMonedaKit As Integer, ByVal IdTienda As Integer, ByVal IdTipoPV As Integer) As List(Of Entidades.Kit)
        Return objDaoKit.SelectComponenteDetallexParams(IdKit, IdUnidadMedidaKit, CantidadKit, IdMonedaKit, IdTienda, IdTipoPV)
    End Function

    Public Sub Kit_ActualizarxIdKitxIdComponente(ByVal objKit As Entidades.Kit)
        objDaoKit.Kit_ActualizarxIdKitxIdComponente(objKit)
    End Sub

    Public Function SelectComponentexIdKit(ByVal IdKit As Integer) As List(Of Entidades.Kit)
        Return objDaoKit.SelectComponentexIdKit(IdKit)
    End Function

    Public Function SelectComponentexIdKit2(ByVal IdKit As Integer) As List(Of Entidades.Kit)
        Return objDaoKit.SelectComponentexIdKit2(IdKit)
    End Function
End Class
