'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Collections.Generic
Public Class Area
    Dim obj As New DAO.DAOArea

    Public Function SelectCbo() As List(Of Entidades.Area)
        Return obj.SelectCbo
    End Function

    Public Function InsertaArea(ByVal area As Entidades.Area) As Boolean
        Return obj.InsertaArea(area)
    End Function
    Public Function ActualizaArea(ByVal area As Entidades.Area) As Boolean
        Return obj.ActualizaArea(area)
    End Function
    Public Function SelectAll() As List(Of Entidades.Area)
        Return obj.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Area)
        Return obj.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Area)
        Return obj.SelectAllInactivo
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Area)
        Return obj.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Area)
        Return obj.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Area)
        Return obj.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal idArea As Integer) As List(Of Entidades.Area)
        Return obj.SelectxId(idArea)
    End Function

    Public Function SelectCboxIdEmpresa(ByVal IdEmpresa As Integer) As List(Of Entidades.Area)
        Return obj.SelectCboxIdEmpresa(IdEmpresa)
    End Function
    Public Function SelectCboxIdTienda(ByVal IdTienda As Integer) As List(Of Entidades.Area)
        Return obj.SelectCboxIdTienda(IdTienda)
    End Function
    Public Function SelectAllxNombreActivoInactivo(ByVal nombre As String) As List(Of Entidades.Area)
        Return obj.SelectAllxNombreActivoInactivo(nombre)
    End Function

    Public Function SelectIdcentroCostoxIdarea(ByVal idarea As Integer) As String
        Return obj.SelectIdcentroCostoxIdarea(idarea)
    End Function

    Public Function AreaSelectxNombre(ByVal IdArea As Integer) As List(Of Entidades.Area)
        Return obj.AreaSelectxNombre(IdArea)
    End Function

#Region "Marzo del 2010"
    Public Function Area_Select_TiendaArea_UsuarioArea(ByVal idtienda As Integer, ByVal idusuario As Integer) As List(Of Entidades.Area)
        Return obj.Area_Select_TiendaArea_UsuarioArea(idtienda, idusuario)
    End Function

#End Region


End Class
