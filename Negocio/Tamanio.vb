﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Tamanio
    Private objDaoTamanio As New DAO.DAOTamanio
    Public Function InsertaTamanio(ByVal tamanio As Entidades.Tamanio) As Boolean
        Return objDaoTamanio.InsertaTamanio(tamanio)
    End Function
    Public Function ActualizaTamanio(ByVal tamanio As Entidades.Tamanio) As Boolean
        Return objDaoTamanio.ActualizaTamanio(tamanio)
    End Function
    Public Function SelectAllActivo_Cbo() As List(Of Entidades.Tamanio)
        Return objDaoTamanio.SelectAllActivo_Cbo
    End Function
    Public Function SelectAll() As List(Of Entidades.Tamanio)
        Return objDaoTamanio.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Tamanio)
        Return objDaoTamanio.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Tamanio)
        Return objDaoTamanio.SelectAllInactivo
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Tamanio)
        Return objDaoTamanio.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Tamanio)
        Return objDaoTamanio.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Tamanio)
        Return objDaoTamanio.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Tamanio)
        Return objDaoTamanio.SelectxId(id)
    End Function
End Class
