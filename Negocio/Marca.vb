﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Marca
    Private objDAOMarca As New DAO.DAOMarca
    Public Function InsertaMarca(ByVal marca As Entidades.Marca) As Boolean
        Return objDAOMarca.InsertaMarca(marca)
    End Function
    Public Function ActualizaMarca(ByVal marca As Entidades.Marca) As Boolean
        Return objDAOMarca.ActualizaMarca(marca)
    End Function
    Public Function SelectAll() As List(Of Entidades.Marca)
        Return objDAOMarca.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Marca)
        Return objDAOMarca.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Marca)
        Return objDAOMarca.SelectAllInactivo
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Marca)
        Return objDAOMarca.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Marca)
        Return objDAOMarca.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Marca)
        Return objDAOMarca.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Marca)
        Return objDAOMarca.SelectxId(id)
    End Function
End Class
