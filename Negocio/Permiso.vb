﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Chang Carnero, Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 03-Noviembre-2009
'Hora    : 05:30 pm
'*************************************************
Public Class Permiso
    Private obj As New DAO.DAOPermiso
    Private objConexion As New DAO.Conexion

    Public Function SelectxIdAreaxIdPerfil(ByVal IdArea As Integer, ByVal IdPerfil As Integer) As List(Of Entidades.Perfil_Permiso)
        Return obj.SelectxIdAreaxIdPerfil(IdArea, IdPerfil)
    End Function

    Public Function PermisoSelectxNombrePermiso(ByVal IdPerfil As Integer) As List(Of Entidades.Perfil)
        Return obj.PermisoSelectxNombrePermiso(IdPerfil)
    End Function
    Public Function SelectCboArea() As List(Of Entidades.Area)
        Return obj.SelectCboArea
    End Function
End Class
