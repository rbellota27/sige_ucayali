﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.






'************************   JUEVES 15 ABRIL 01_09 PM






Public Class Almacen
    Private objAlmacen As New DAO.DAOAlmacen

    Public Function AlmacenSelectActivoCentroDistribucionxIdEmpresa(ByVal IdEmpresa As Integer) As List(Of Entidades.Almacen)
        Return objAlmacen.AlmacenSelectActivoCentroDistribucionxIdEmpresa(IdEmpresa)
    End Function

    Public Function SelectAllActivoxIdTipoAlmacenxIdEmpresa(ByVal IdTipoAlmacen As Integer, ByVal IdEmpresa As Integer) As List(Of Entidades.Almacen)

        Return objAlmacen.SelectAllActivoxIdTipoAlmacenxIdEmpresa(IdTipoAlmacen, IdEmpresa)

    End Function


    Public Function SelectCboxIdTipoAlmacenxIdTienda(ByVal IdTipoAlmacen As Integer, ByVal IdTienda As Integer) As List(Of Entidades.Almacen)
        Return objAlmacen.SelectCboxIdTipoAlmacenxIdTienda(IdTipoAlmacen, IdTienda)
    End Function

    Public Function SelectAlmacenTAPrincipal() As List(Of Entidades.Almacen)
        Return objAlmacen.SelectAlmacenTAPrincipal()
    End Function

    Public Function SelectAlmacenPrincipalxIdTienda(ByVal idTienda As Integer) As List(Of Entidades.Almacen)
        Return objAlmacen.SelectAlmacenPrincipalxIdTienda(idTienda)
    End Function

    Public Function SelectCboxIdTienda(ByVal IdTienda As Integer) As List(Of Entidades.Almacen)
        Return objAlmacen.SelectCboxIdTienda(IdTienda)
    End Function

    Public Function SelectCboxIdTienda2(ByVal IdTienda As Integer) As List(Of Entidades.Almacen)
        Return objAlmacen.SelectCboxIdTienda2(IdTienda)
    End Function
    Public Function SelectCboxIdEmpresa(ByVal IdEmpresa As Integer) As List(Of Entidades.Almacen)
        Return objAlmacen.SelectCboxIdEmpresa(IdEmpresa)
    End Function
    Public Function SelectxId(ByVal IdAlmacen As Integer) As Entidades.Almacen
        Return objAlmacen.SelectxId(IdAlmacen)
    End Function
    Public Function SelectGrilla() As List(Of Entidades.Almacen)
        Return objAlmacen.SelectGrilla
    End Function
    Public Function SelectAllActivo(ByVal estado As Boolean) As List(Of Entidades.Almacen)
        Return objAlmacen.SelectAllActivo(estado)
    End Function
    Public Function SelectAll() As List(Of Entidades.Almacen)
        Return objAlmacen.SelectAll
    End Function
    Public Function InsertaAlmacen(ByVal almacen As Entidades.Almacen) As Boolean
        Try
            Return objAlmacen.InsertaAlmacen(almacen)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function ActualizaAlmacen(ByVal almacen As Entidades.Almacen) As Boolean
        Try
            Return objAlmacen.ActualizarAlmacen(almacen)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Almacen)
        Return objAlmacen.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Almacen)
        Return objAlmacen.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Almacen)
        Return objAlmacen.SelectInactivoxNombre(nombre)
    End Function

    Public Function SelectCbo() As List(Of Entidades.Almacen)
        Return objAlmacen.SelectCbo
    End Function

    Public Function SelectGrillaActivoxNombre(ByVal nombre As String) As List(Of Entidades.Almacen)
        Return objAlmacen.SelectGrillaActivoxNombre(nombre)
    End Function
    Public Function SelectGrillaActivoInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Almacen)
        Return objAlmacen.SelectGrillaActivoInactivoxNombre(nombre)
    End Function

#Region " ++++++++++++++++++++++++++++++ REPORTE ++++++++++++++"

    Public Function getDataSet_ProductoPorEntregar(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdDocumento As Integer, ByVal IdPersona As Integer) As DataSet
        Return objAlmacen.getDataSet_ProductoPorEntregar(IdEmpresa, IdAlmacen, IdDocumento, IdPersona)
    End Function

    Public Function getDataSet_ComercializacionPorMes(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdAlmacen As Integer, ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdProducto As Integer, ByVal FechaIni As String, ByVal FechaFin As String, ByVal TipoAlmacen As DataTable, ByVal IdDocumento As Integer, ByVal IdTiendaPrecio As Integer, ByVal AtributoProducto As DataTable) As DataSet
        Return objAlmacen.getDataSet_ComercializacionPorMes(IdEmpresa, IdTienda, IdAlmacen, IdTipoExistencia, IdLinea, IdSubLinea, IdProducto, FechaIni, FechaFin, TipoAlmacen, IdDocumento, IdTiendaPrecio, AtributoProducto)
    End Function

    Public Function getDataSet_ComercializacionPorSemana(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdAlmacen As Integer, ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdProducto As Integer, ByVal YearIni As Integer, ByVal SemanaIni As Integer, ByVal YearFin As Integer, ByVal SemanaFin As Integer, ByVal TipoAlmacen As DataTable, ByVal IdDocumento As Integer, ByVal IdTiendaPrecio As Integer, ByVal AtributoProducto As DataTable) As DataSet
        Return objAlmacen.getDataSet_ComercializacionPorSemana(IdEmpresa, IdTienda, IdAlmacen, IdTipoExistencia, IdLinea, IdSubLinea, IdProducto, YearIni, SemanaIni, YearFin, SemanaFin, TipoAlmacen, IdDocumento, IdTiendaPrecio, AtributoProducto)
    End Function

    Public Function getDataSet_CR_Comercializacion_Foshan02(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal FechaIni As String, ByVal FechaFin As String, ByVal YearIni As Integer, ByVal SemanaIni As Integer, ByVal YearFin As Integer, ByVal SemanaFin As Integer, ByVal FiltrarSemana As Integer, ByVal AtributoProducto As DataTable, ByVal TipoAlmacen As DataTable) As DataSet
        Return objAlmacen.getDataSet_CR_Comercializacion_Foshan02(IdEmpresa, IdTienda, IdTipoExistencia, IdLinea, IdSubLinea, FechaIni, FechaFin, YearIni, SemanaIni, YearFin, SemanaFin, FiltrarSemana, AtributoProducto, TipoAlmacen)
    End Function

    Public Function getDataSet_AlmacenSaldoxFecha(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdProducto As Integer, ByVal FechaFin As String, ByVal FiltrarSaldo As Integer, ByVal Saldo As Decimal, ByVal Tabla As DataTable) As DataSet
        Return objAlmacen.getDataSet_AlmacenSaldoxFecha(IdEmpresa, IdAlmacen, IdTipoExistencia, IdLinea, IdSubLinea, IdProducto, FechaFin, FiltrarSaldo, Saldo, Tabla)
    End Function

    Public Function getDataSet_ProductoEnTransito(ByVal IdAlmacen As Integer, ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdProducto As Integer) As DataSet
        Return objAlmacen.getDataSet_ProductoEnTransito(IdAlmacen, IdTipoExistencia, IdLinea, IdSubLinea, IdProducto)
    End Function

    Public Function getDataSet_ProductoComprometido(ByVal IdAlmacen As Integer, ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdProducto As Integer) As DataSet
        Return objAlmacen.getDataSet_ProductoComprometido(IdAlmacen, IdTipoExistencia, IdLinea, IdSubLinea, IdProducto)
    End Function

#End Region



End Class
