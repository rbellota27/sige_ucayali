﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Imports System.Data.SqlClient

Public Class SubLinea_TipoTablaValor
    Dim DAO As New DAO.DAOSubLinea_TipoTablaValor
    Dim objConexion As New DAO.Conexion

    Public Function ConfigTablaValor_Paginado(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal estado As Integer, ByVal idtipoexistencia As Integer, _
                                              ByVal PageIndex As Integer, ByVal PageSize As Integer) As List(Of Entidades.SubLinea_TipoTablaValor)
        Return DAO.ConfigTablaValor_Paginado(idlinea, idsublinea, estado, idtipoexistencia, PageIndex, PageSize)
    End Function

    Public Function GrabaSubLineaTipoTablaValor(ByVal idsublinea As Integer, ByVal IdTipoTabla As Integer, _
                                                ByVal lista As List(Of Entidades.SubLinea_TipoTablaValor)) As Boolean
        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim IdLinea As Integer = 0
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction
            'If Not DAO.AnularSubLineaTipoTablaValor(idsublinea, IdTipoTabla, cn, tr) Then
            '    Throw New Exception("Fracasó el Proceso de Inserción")
            'End If

            If Not DAO.GrabaSubLineaTipoTablaValor(lista, cn, tr) Then
                Throw New Exception("Fracasó el Proceso de Inserción")
            End If
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            Throw ex
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
    Public Function SelectAllxIdLineaxIdSubLineaxEstado(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal estado As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.SubLinea_TipoTablaValor)
        Return DAO.SelectAllxIdLineaxIdSubLineaxEstado(idlinea, idsublinea, estado, idtipoexistencia)
    End Function
    Public Function SelectxIdLineaxIdSubLinea(ByVal idlinea As Integer, ByVal idsublinea As Integer, Optional ByVal idTipoTabla As Integer = Nothing, Optional ByVal PageIndex As Integer = Nothing, Optional ByVal PageSize As Integer = Nothing, Optional ByVal descripcion As String = Nothing) As List(Of Entidades.SubLinea_TipoTablaValor)
        Return DAO.SelectxIdLineaxIdSubLinea(idlinea, idsublinea, idTipoTabla, PageIndex, PageSize, descripcion)
    End Function
    Public Function SelectxIdLineaxIdSubLineaxIdTipoTabla(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal idtipotabla As Integer, Optional ByVal textTabla As String = "") As List(Of Entidades.SubLinea_TipoTablaValor)
        Return DAO.SelectxIdLineaxIdSubLineaxIdTipoTabla(idlinea, idsublinea, idtipotabla, textTabla)
    End Function

    Public Function SelectxIdLineaxIdSubLineaxIdTipoTabla2(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal idtipotabla As Integer, Optional ByVal textTabla As String = "") As List(Of Entidades.SubLinea_TipoTablaValor)
        Return DAO.SelectxIdLineaxIdSubLineaxIdTipoTabla2(idlinea, idsublinea, idtipotabla, textTabla)
    End Function

    Public Function SelectxIdLineaxIdSubLineaxIdTipoTabla02(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal idtipotabla As Integer, ByVal idproducto As Integer) As List(Of Entidades.SubLinea_TipoTablaValor)
        Return DAO.SelectxIdLineaxIdSubLineaxIdTipoTabla02(idlinea, idsublinea, idtipotabla, idproducto)
    End Function
    Public Function SelectxIdLineaxIdSubLineaxIdTipoTabla(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal idtipotabla As Integer, ByVal idproducto As Integer) As List(Of Entidades.SubLinea_TipoTablaValor)
        Return DAO.SelectxIdLineaxIdSubLineaxIdTipoTabla(idlinea, idsublinea, idtipotabla, idproducto)
    End Function

    Public Function AnularSubLineaTipoTablaValor(ByVal lista As List(Of Entidades.SubLinea_TipoTablaValor)) As Boolean

        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim IdLinea As Integer = 0
        Dim hecho As Boolean = False

        Try
            cn.Open()
            tr = cn.BeginTransaction


            For i As Integer = 0 To lista.Count - 1

                DAO.AnularSubLineaTipoTablaValor(lista(i).IdSubLinea, lista(i).IdTipoTabla, lista(i).IdTipoTablaValor, cn, tr)

            Next


            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            Throw ex
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
End Class
