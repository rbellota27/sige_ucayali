﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class SubLinea
    Private objSubLinea As New DAO.DAOSubLinea


    Public Function fn_TotalPag_LineaxSubLinea(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal PageSize As Integer) As Integer
        Return objSubLinea.fn_TotalPag_LineaxSubLinea(IdLinea, IdSubLinea, PageSize)
    End Function


    Public Function InsertaSubLineaT(ByVal objSubLinea1 As Entidades.SubLinea, ByVal listaRegimen As List(Of Entidades.ProductoRegimen)) As Boolean
        Return objSubLinea.InsertaSubLineaT(objSubLinea1, listaRegimen)
    End Function
    Public Function ActualizaSubLineaT(ByVal objSubLinea1 As Entidades.SubLinea, ByVal listaRegimen As List(Of Entidades.ProductoRegimen)) As Boolean
        Return objSubLinea.ActualizaSubLineaT(objSubLinea1, listaRegimen)
    End Function
    Public Function SelectAll() As List(Of Entidades.SubLinea)
        Return objSubLinea.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.SubLinea)
        Return objSubLinea.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.SubLinea)
        Return objSubLinea.SelectAllInactivo
    End Function
    Public Function SelectActivoxLineaxTipoExistencia(ByVal idLinea As Integer, ByVal idTipoEx As Integer) As List(Of Entidades.SubLinea)
        Return objSubLinea.SelectActivoxLineaxTipoExistencia(idLinea, idTipoEx)
    End Function

    Public Function SelectActivoxLineaxTipoExistencia2(ByVal idLinea As Integer, ByVal idTipoEx As Integer) As List(Of Entidades.SubLinea)
        Return objSubLinea.SelectActivoxLineaxTipoExistencia2(idLinea, idTipoEx)
    End Function
    Public Function SelectAllActivoxLinea(ByVal idLinea As Integer) As List(Of Entidades.SubLinea)
        Return objSubLinea.SelectAllActivoxLinea(idLinea)
    End Function
    Public Function SelectxIdLineaxEstado(ByVal idlinea As Integer, ByVal estado As String) As List(Of Entidades.SubLinea)
        Return objSubLinea.SelectxIdLineaxEstado(idlinea, estado)
    End Function
    Public Function SelectxIdSubLinea(ByVal idsublinea As Integer) As Entidades.SubLinea
        Return objSubLinea.SelectxIdSubLinea(idsublinea)
    End Function
    Public Function SelectCboxIdLinea2(ByVal idLinea As Integer) As List(Of Entidades.SubLinea)
        Return objSubLinea.SelectCboxIdLinea2(idLinea)
    End Function

    Public Function SelectCboxIdLinea(ByVal idLinea As Integer) As List(Of Entidades.SubLinea)
        Return objSubLinea.SelectCboxIdLinea(idLinea)
    End Function


    Public Function SelectIdxCodigo(ByVal Codigo As Integer) As Integer
        Try
            Return objSubLinea.SelectIdxCodigo(Codigo)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectCodigoxIdSubLinea(ByVal IdSubLinea As Integer) As String
        Return objSubLinea.SelectCodigoxIdSubLinea(IdSubLinea)
    End Function
    Public Function SelectCbo() As List(Of Entidades.SubLinea)
        Return objSubLinea.SelectCbo
    End Function
    Public Function SelectActivoxLineaxTipoExistenciaCodigo(ByVal idLinea As Integer, ByVal idTipoEx As Integer) As List(Of Entidades.SubLinea)
        Return objSubLinea.SelectActivoxLineaxTipoExistenciaCodigo(idLinea, idTipoEx)
    End Function

    Public Function ValidadLongitud() As Entidades.SubLinea
        Return objSubLinea.ValidadLongitud()
    End Function
    Public Function ValidadNombrexIdLinea(ByVal idlinea As Integer, ByVal nombre As String) As Entidades.SubLinea
        Return objSubLinea.ValidadNombrexIdLinea(idlinea, nombre)
    End Function
End Class
