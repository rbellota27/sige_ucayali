﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class PersonaTipoAgente
    Private obj As New DAO.DAOPersonaTipoAgente

    Public Function PersonaTipoAgente_Select(ByVal IdPersona As Integer) As List(Of Entidades.PersonaTipoAgente)
        Return obj.PersonaTipoAgente_Select(IdPersona)
    End Function
    Public Function SelectxId(ByVal IdPersona As Integer) As Entidades.PersonaTipoAgente
        Return obj.SelectxId(IdPersona)
    End Function
    Public Function SelectIdAgenteTasaxIdPersona(ByVal IdPersona As Integer) As Entidades.TipoAgente
        Return obj.SelectIdAgenteTasaxIdPersona(IdPersona)
    End Function
End Class
