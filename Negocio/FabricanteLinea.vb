﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class FabricanteLinea
    Private objFabLinea As New DAO.DAOFabricanteLinea
    Public Function SelectAllActivoxIdLineaIdTipoExistencia(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal idpais As String) As List(Of Entidades.FabricanteLinea)
        Return objFabLinea.SelectAllActivoxIdLineaIdTipoExistencia(idlinea, idtipoexistencia, idpais)
    End Function
    Public Function SelectAllActivoxIdLineaIdTipoExistenciav2(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal idpais As String) As List(Of Entidades.FabricanteLinea)
        Return objFabLinea.SelectAllActivoxIdLineaIdTipoExistenciav2(idlinea, idtipoexistencia, idpais)
    End Function
    Public Function SelectAllActivoxIdLineaIdTipoExistencia(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.FabricanteLinea)
        Return objFabLinea.SelectAllActivoxIdLineaIdTipoExistencia(idlinea, idtipoexistencia)
    End Function
End Class
