﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'******************** MARTES 19 MAYO 2010 HORA 11_35 AM
Imports System.Data
Imports System.Data.SqlClient

Public Class CuentaBancaria
    Dim objDaoCtaBancaria As New DAO.DAOCuentaBancaria
    Dim objConexion As New DAO.Conexion

    Public Function VerificarsiExisteCB(ByVal IdBanco As Integer, ByVal IdPersona As Integer, ByVal IdMoneda As Integer) As Entidades.CuentaBancaria
        Return objDaoCtaBancaria.VerificarsiExisteCB(IdBanco, IdPersona, IdMoneda)
    End Function


    Public Function VerificarsiExisteCBDetraccion(ByVal IdBanco As Integer, ByVal IdPersona As Integer, ByVal IdMoneda As Integer) As Entidades.CuentaBancaria
        Return objDaoCtaBancaria.VerificarsiExisteCBDetraccion(IdBanco, IdPersona, IdMoneda)
    End Function


    Public Function SelectxIdBancoxIdCuentaBancaria(ByVal IdBanco As Integer, ByVal IdCuentaBancaria As Integer) As Entidades.CuentaBancaria
        Return objDaoCtaBancaria.SelectxIdBancoxIdCuentaBancaria(IdBanco, IdCuentaBancaria)
    End Function

    Public Function SelectActivoxIdPersona_DT(ByVal IdPersona As Integer) As DataTable
        Return objDaoCtaBancaria.SelectActivoxIdPersona_DT(IdPersona)
    End Function


    Public Function SelectCboBanco(ByVal IdEmpresa As Integer) As List(Of Entidades.Banco)
        Return objDaoCtaBancaria.SelectCboBanco(IdEmpresa)
    End Function

    Public Function SelectCboxIdBancoxIdPersona(ByVal IdBanco As Integer, ByVal IdPersona As Integer) As List(Of Entidades.CuentaBancaria)
        Return objDaoCtaBancaria.SelectCboxIdBancoxIdPersona(IdBanco, IdPersona)
    End Function
    Public Function SelectCboxIdBanco(ByVal IdBanco As Integer) As List(Of Entidades.CuentaBancaria)
        Return objDaoCtaBancaria.SelectCboxIdBanco(IdBanco)
    End Function
    Public Function DeleteCtaBancaria(ByVal idcuentabancaria As Integer) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim bool As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            objDaoCtaBancaria.DeleteCuentaBancaria(idcuentabancaria, cn, tr)
            tr.Commit()
            bool = True
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return bool
    End Function


    Public Function InsertaCuentaBancariaDetraccion(ByVal objCta As Entidades.CuentaBancaria, Optional ByVal esCuentaDetraccion As Boolean = False) As Boolean
        '************ 
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim bool As Boolean = False
        Try
            Dim objDaoCta As New DAO.DAOCuentaBancaria
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            '************** 
            objDaoCta.InsertaCuentaBancariaDetraccion(objCta, cn, tr, esCuentaDetraccion)
            bool = True
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            bool = False
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return bool
    End Function


    Public Function InsertaCuentaBancaria(ByVal objCta As Entidades.CuentaBancaria, Optional ByVal esCuentaDetraccion As Boolean = False) As Boolean
        '************ 
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim bool As Boolean = False
        Try
            Dim objDaoCta As New DAO.DAOCuentaBancaria
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            '************** 
            objDaoCta.InsertaCuentaBancaria(objCta, cn, tr, esCuentaDetraccion)
            bool = True
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            bool = False
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return bool
    End Function

    Public Function UpdateCuentaBancaria(ByVal objcta As Entidades.CuentaBancaria) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim bool As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            objDaoCtaBancaria.ActualizaCuentaBancaria(objcta, cn, tr)
            bool = True
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Return False
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return bool
    End Function
    Public Function SelectcuentaBancaria(ByVal idempresa As Integer, ByVal idbanco As Integer, ByVal estado As Int16, ByVal idmoneda As Int16) As List(Of Entidades.CuentaBancaria)
        Return objDaoCtaBancaria.SelectcuentaBancaria(idempresa, idbanco, estado, idmoneda)
    End Function

    Public Function ln_existeCuentaDetraccion(ByVal idProveedor As Integer) As Integer
        Return objDaoCtaBancaria.existeCuentaDetraccion(idProveedor)
    End Function

End Class










