﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'********************************************************
'Autor      : Chang Carnero, Edgar
'Módulo     : CondicionComercial
'Sistema    : Sanicenter
'Empresa    : Digrafic SRL
'Modificado : 20-Enero-2010
'Hora       : 01:00:00 pm
'********************************************************
Public Class CondicionComercial

    Private objDaoCondComercial As New DAO.DAOCondicionComercial
    Public Function SelectAllActivo() As List(Of Entidades.CondicionComercial)
        Return objDaoCondComercial.SelectAllActivo
    End Function
    Public Function SelectAll() As List(Of Entidades.CondicionComercial)
        Return objDaoCondComercial.SelectAllCondicionCoimercial
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.CondicionComercial)
        Return objDaoCondComercial.SelectAllInactivo
    End Function
    Public Function InsertaCondicionComercial(ByVal Estilo As Entidades.CondicionComercial) As Boolean
        Return objDaoCondComercial.InsertaCondicionComercial(Estilo)
    End Function
    Public Function ActualizaCondicionComercial(ByVal estilo As Entidades.CondicionComercial) As Boolean
        Return objDaoCondComercial.ActualizaCondicionComercial(estilo)
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.CondicionComercial)
        Return objDaoCondComercial.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.CondicionComercial)
        Return objDaoCondComercial.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.CondicionComercial)
        Return objDaoCondComercial.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.CondicionComercial)
        Return objDaoCondComercial.SelectxId(id)
    End Function
    'Public Function SelectxIdCondicionComercial(ByVal id As Integer) As Integer
    '    Return objDaoCondComercial.SelectxIdCondicionComercial(id)
    'End Function
    Public Function SelectxIdCondicionComercial(ByVal id As Integer) As Entidades.CondicionComercial
        Return objDaoCondComercial.SelectxIdCondicionComercial(id)
    End Function
End Class
