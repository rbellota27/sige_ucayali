﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'  30/09/2010
Public Class Usuario_Area
    Public obj As New DAO.DAOUsuario_Area

    Public Function ListarAreaxIdusuario(ByVal IdUsuario As Integer, Optional ByVal addIdArea As Integer = Nothing) As List(Of Entidades.Usuario_Area)
        Return obj.ListarAreaxIdusuario(IdUsuario, addIdArea)
    End Function

    Public Function InsertaUsuario_Area(ByVal Usuario_Area As Entidades.Usuario_Area) As Boolean
        Return obj.InsertaUsuario_Area(Usuario_Area)
    End Function
    Public Function ActualizaUsuario_Area(ByVal Usuario_Area As Entidades.Usuario_Area) As Boolean
        Return obj.ActualizaUsuario_Area(Usuario_Area)
    End Function


    Public Function SelectAll() As List(Of Entidades.Usuario_Area)
        Return obj.SelectAll()
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Usuario_Area)
        Return obj.SelectAllActivo()
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Usuario_Area)
        Return obj.SelectAllInactivo()
    End Function

    Public Function SelectxIdUsuario(ByVal IdUsuario As Integer) As List(Of Entidades.Usuario_Area)
        Return obj.SelectxIdUsuario(IdUsuario)
    End Function
    Public Function SelectxIdArea(ByVal IdArea As Integer) As List(Of Entidades.Usuario_Area)
        Return obj.SelectxIdArea(IdArea)
    End Function
End Class
