﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class TransitoLinea
    Private objDaoTransitoLinea As New DAO.DAOTransitoLinea
    Public Function SelectAllActivoxIdLineaIdTipoExistencia(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.TransitoLinea)
        Return objDaoTransitoLinea.SelectAllActivoxIdLineaIdTipoExistencia(idlinea, idtipoexistencia)
    End Function
    Public Function SelectAllActivoxIdLineaIdTipoExistenciav2(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.TransitoLinea)
        Return objDaoTransitoLinea.SelectAllActivoxIdLineaIdTipoExistenciav2(idlinea, idtipoexistencia)
    End Function
End Class
