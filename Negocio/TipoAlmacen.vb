﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'**************** '29Marzo 2010 01_00
Imports System.Data.SqlClient
Imports System.Data.SqlClient.SqlTransaction
Public Class TipoAlmacen
    Dim objDAOTipoAlmacen As New DAO.DAOTipoAlmacen
    Dim objConexion As DAO.Conexion
    Public Function ResumenIndicadoresGrillaIRILineaSubLineaAnio(ByVal Anio As String, ByVal MesInicial As String, ByVal MesFinal As String, ByVal CadenaTipoAlmacen As String, ByVal Linea As String, ByVal Sublinea As String, ByVal Pais As String) As List(Of Entidades.TipoAlmacen)
        Return objDAOTipoAlmacen.ResumenIndicadoresGrillaIRILineaSubLineaAnio(Anio, MesInicial, MesFinal, CadenaTipoAlmacen, Linea, Sublinea, Pais)
    End Function
    Public Function ResumenIndicadoresGrillaIRI(ByVal obj As Entidades.TipoAlmacen) As List(Of Entidades.TipoAlmacen)
        Return objDAOTipoAlmacen.ResumenIndicadoresGrillaIRI(obj)
    End Function
    Public Function ResumenIndicadoresLineasubLineaAnio(ByVal Anio As String, ByVal MesInicial As String, ByVal MesFinal As String, ByVal CadenaTipoAlmacen As String, ByVal Linea As String, ByVal Sublinea As String, ByVal Pais As String) As DataSet
        Return objDAOTipoAlmacen.ResumenIndicadoresLineasubLineaAnio(Anio, MesInicial, MesFinal, CadenaTipoAlmacen, Linea, Sublinea, Pais)
    End Function
    Public Function ResumenIndicadores(ByVal obj As Entidades.TipoAlmacen) As DataSet
        Return objDAOTipoAlmacen.ResumenIndicadores(obj)
    End Function
    Public Function ReporteRotacionInvxAnioxMesLineaSubLinea(ByVal cadenaalmacenado As String, ByVal anio As String, ByVal mes As String, ByVal Linea As String, ByVal SubLinea As String, ByVal Pais As String) As DataSet
        Return objDAOTipoAlmacen.ReporteRotacionInvxAnioxMesLineaSubLinea(cadenaalmacenado, anio, mes, Linea, SubLinea, Pais)
    End Function
    Public Function ReporteRotacionInvxAnioxMes(ByVal cadenaalmacenado As String, ByVal anio As String, ByVal mes As String) As DataSet
        Return objDAOTipoAlmacen.ReporteRotacionInvxAnioxMes(cadenaalmacenado, anio, mes)
    End Function
    Public Function SelectxTipoAlmacenxanioxmes(ByVal cadenaalmacenado As String, ByVal anio As String, ByVal mes As String) As List(Of Entidades.TipoAlmacen)
        Return objDAOTipoAlmacen.SelectxTipoAlmacenxanioxmes(cadenaalmacenado, anio, mes)
    End Function
    Public Function SelectxTipoAlmacenxanioxmesLineaSubLinea(ByVal cadenaalmacenado As String, ByVal anio As String, ByVal mes As String, ByVal Linea As String, ByVal SubLinea As String, ByVal Pais As String) As List(Of Entidades.TipoAlmacen)
        Return objDAOTipoAlmacen.SelectxTipoAlmacenxanioxmesLineaSubLinea(cadenaalmacenado, anio, mes, Linea, SubLinea, Pais)
    End Function
    Public Function SelectxIdTipoAlmacen() As List(Of Entidades.TipoAlmacen)
        Return objDAOTipoAlmacen.SelectxIdTipoAlmacen
    End Function
    Public Function SelectxIdTipoAlmacenAll(ByVal IdtipoAlmacen As Integer) As List(Of Entidades.TipoAlmacen)
        Try
            Return objDAOTipoAlmacen.SelectxIdTipoAlmacenAll(IdtipoAlmacen)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function RotacionInventarioAllLineaSubLineaAnio(ByVal Anio As String, ByVal MesInicial As String, ByVal MesFinal As String, _
                                                           ByVal CadenaTipoAlmacen As String, ByVal Linea As String, ByVal Sublinea As String, ByVal Pais As String, ByVal idTienda As Integer) As DataSet
        Return objDAOTipoAlmacen.RotacionInventarioAllLineaSubLineaAnio(Anio, MesInicial, MesFinal, CadenaTipoAlmacen, Linea, Sublinea, Pais, idTienda)
    End Function
    Public Function RotacioninventarioAll(ByVal obj As Entidades.TipoAlmacen) As DataSet
        Return objDAOTipoAlmacen.RotacioninventarioAll(obj)
    End Function

    Public Function SelectAllActivo() As List(Of Entidades.TipoAlmacen)
        Return objDAOTipoAlmacen.SelectAllActivo
    End Function

    Public Function SelectAllActivo2() As List(Of Entidades.TipoAlmacen)
        Return objDAOTipoAlmacen.SelectAllActivo2
    End Function

    Public Function insertTipoAlmacen(ByVal tAlmacen As Entidades.TipoAlmacen) As Boolean
        Return objDAOTipoAlmacen.InsertaTipoAlmacen(tAlmacen)
    End Function
    Public Function updateTipoAlmacen(ByVal tAlmacen As Entidades.TipoAlmacen) As Boolean
        Try
            Return objDAOTipoAlmacen.ActualizaTipoAlmacen(tAlmacen)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectActivoInactivo() As List(Of Entidades.TipoAlmacen)
        Return objDAOTipoAlmacen.SelectAllActivoInactivo
    End Function

End Class
