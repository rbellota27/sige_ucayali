﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Pos_TipoTarjeta
    Dim obj As New DAO.DAOPos_TipoTarjeta
    Public Function SelectxIdPos(ByVal IdPos As Integer) As List(Of Entidades.Pos_TipoTarjeta)
        Return obj.SelectxIdPos(IdPos)
    End Function

    Public Sub InsertTListxIdPos(ByVal L As List(Of Entidades.Pos_TipoTarjeta), _
    ByVal IdPos As Integer)
        obj.InsertTListxIdPos(L, IdPos, Nothing, Nothing)
    End Sub

    Public Function InsertT(ByVal x As Entidades.Pos_TipoTarjeta) As Integer
        Return obj.InsertT(x, Nothing, Nothing)
    End Function

    Public Function DeleteTxIdPos(ByVal IdPos As Integer) As Integer
        Return obj.DeleteTxIdPos(IdPos, Nothing, Nothing)
    End Function
End Class
