﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Juridica
    Private obj As New DAO.DAOJuridica
    Public Function SelectxId(ByVal IdPersona As Integer) As Entidades.Juridica
        Return obj.SelectxId(IdPersona)
    End Function

    Public Function Selectx_Id(ByVal IdPersona As Integer) As Entidades.Juridica
        Return obj.Selectx_Id(IdPersona)
    End Function

    'Public Function ActualizaJuridica(ByVal cn As SqlClient.SqlConnection, ByVal juridica As Entidades.Juridica) As Boolean
    '    'Return obj.ActualizaJuridica(juridica)
    'End Function
    Public Function SelectActivoxRazonSocial(ByVal razonSocial As String) As List(Of Entidades.Juridica)
        Return obj.SelectActivoxRazonSocial(razonSocial)
    End Function
End Class
