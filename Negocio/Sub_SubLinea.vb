﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'********************************************************
'Autor      : Chigne Bazan, Dany
'Módulo     : MotivoGasto
'Sistema    : Sanicenter
'Empresa    : Digrafic SRL
'Modificado : 24-Febrero-2010
'Hora       : 9:45:00 am
'********************************************************
Public Class Sub_SubLinea
    Private objDAOSub_SubLinea As New DAO.DAO_Sub_SubLinea
    Private obj As New DAO.DAO_Sub_SubLinea

    Public Function InsertaSub_SubLinea(ByVal Sub_SubLinea As Entidades.Sub_SubLinea) As Boolean
        Return objDAOSub_SubLinea.InsertaSub_SubLinea(Sub_SubLinea)
    End Function

    Public Function ActualizaSub_SubLinea(ByVal Sub_SubLinea As Entidades.Sub_SubLinea) As Boolean
        Return objDAOSub_SubLinea.ActualizaSub_SubLinea(Sub_SubLinea)
    End Function

    Public Function SelectAllSub_SubLinea() As List(Of Entidades.Sub_SubLinea)
        Return obj.SelectAllSub_SubLinea
    End Function

    Public Function SelectActivoSub_SubLinea() As List(Of Entidades.Sub_SubLinea)
        Return obj.SelectActivoSub_SubLinea
    End Function

    Public Function SelectInactivoSub_SubLinea() As List(Of Entidades.Sub_SubLinea)
        Return obj.SelectInactivoSub_SubLinea
    End Function

    Public Function SelectAllNombre(ByVal nombre As String) As List(Of Entidades.Sub_SubLinea)
        Return obj.SelectAllxNombre(nombre)
    End Function

    Public Function SelectNombreActivo(ByVal nombre As String) As List(Of Entidades.Sub_SubLinea)
        Return obj.SelectNombreActivo(nombre)
    End Function

    Public Function SelectNombreInactivo(ByVal nombre As String) As List(Of Entidades.Sub_SubLinea)
        Return obj.SelectNombreInactivo(nombre)
    End Function

    Public Function SelectId(ByVal Codigo As Integer) As List(Of Entidades.Sub_SubLinea)
        Return obj.SelectxId(Codigo)
    End Function

    Public Function SelectIdSubLinea(ByVal IdSubLinea As Integer) As List(Of Entidades.Sub_SubLinea)
        Return obj.SelectSub_SubLineaxSubLinea(IdSubLinea)
    End Function

    Public Function SelectAllActivoxIdLineav2(ByVal idsublinea As Integer) As List(Of Entidades.Sub_SubLinea)
        Return obj.SelectAllActivoxIdLineav2(idsublinea)
    End Function
End Class
