﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'Modificado 06-Abril-2010  3:50 PM
'*************************************************
'Autor   : Nagamine Molina, Jorge
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 10-Noviembre-2009
'Hora    : 03:40 pm
'*************************************************
'*************************************************
'Autor   : Chang Carnero, Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 10-Noviembre-2009
'Hora    : 01:00 pm
'*************************************************
Imports System.Data.SqlClient
Public Class Perfil
    Private obj As New DAO.DAOPerfil
    Private objPerfilTipoPV As New DAO.DAOPerfilTipoPV
    Private objPerfilPermiso As New DAO.DAOPerfil_Permiso
    Private objConexion As New DAO.Conexion
    'Public Function PerfilUpdate(ByVal objPerfil As Entidades.Perfil) As Boolean
    '    Return obj.PerfilUpdate(objPerfil)
    'End Function
    'Public Function PerfilInsert(ByVal objPerfil As Entidades.Perfil) As Boolean
    '    Return obj.PerfilInsert(objPerfil)
    'End Function
    Public Function SelectxIdPerfilxNombrexEstado(ByVal IdPerfil As Integer, ByVal Nombre As String, ByVal Estado As String, ByVal EstadoMasivo As String) As List(Of Entidades.Perfil)
        Return obj.SelectxIdPerfilxNombrexEstado(IdPerfil, Nombre, Estado, EstadoMasivo)
    End Function

    Public Function SelectCbo() As List(Of Entidades.Perfil)
        Return obj.SelectCbo()
    End Function
    Public Function SelectIdsxIdPersona(ByVal IdPersona As Integer) As List(Of Entidades.Perfil)
        Return obj.SelectIdsxIdPersona(IdPersona)
    End Function
    Public Function InsertaPerfilPermiso(ByVal ListaPermiso As List(Of Entidades.Perfil_Permiso), ByVal IdPerfil As Integer, ByVal IdArea As Integer) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)


            '************ Delete
            objPerfilPermiso.DeletexIdPerfilxIdArea(IdPerfil, IdArea, cn, tr)

            For i As Integer = 0 To ListaPermiso.Count - 1

                objPerfilPermiso.InsertaPerfil_Permiso(ListaPermiso(i), IdPerfil, cn, tr)


            Next

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function

    Public Function InsertaPerfilTipoPV(ByVal PerfilPV As Entidades.Perfil, _
                                        ByVal listaTPV As List(Of Entidades.PerfilTipoPV), _
                                        ByVal listaMP As List(Of Entidades.Perfil_MedioPago) _
                                        ) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction

            '************ Insertar Concepto
            Dim IdPerfil As Integer = obj.InsertaPerfil(PerfilPV, cn, tr)

            '************ Insertar Lista Tipo Documento
            Dim objDaoTipoPV As New DAO.DAOPerfilTipoPV
            'Dim objpERFIL As New Entidades.PerfilTipoPV
            objDaoTipoPV.InsertaListaPerfilTipoPV(cn, tr, listaTPV, IdPerfil)

            '************ Insertar Lista Tipo Documento
            Dim objDaoMP As New DAO.DAOPerfil_MedioPago
            'Dim objPerfilMP As New Entidades.Perfil_MedioPago
            objDaoMP.InsertListxIdPerfil(cn, tr, listaMP, IdPerfil)

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally

            If cn.State = ConnectionState.Open Then cn.Close()

        End Try
        Return hecho
    End Function
    Public Function PerfilTipoPVUpdate(ByVal TipoPV As Entidades.Perfil, _
                                        ByVal listaTPV As List(Of Entidades.PerfilTipoPV), _
                                        ByVal listaMP As List(Of Entidades.Perfil_MedioPago) _
                                        ) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            '*************   Actualiza Perfil  ********************
            If Not (obj.PerfilUpdate(TipoPV, cn, tr)) Then
                Throw New Exception
            End If

            '*************  Elimina ActualizaMotivoT_TipoOperacion ************************
            Dim obJDaoPerfilTipoPV As New DAO.DAOPerfilTipoPV
            If (Not obJDaoPerfilTipoPV.DeletexPerfilTipoPVBorrar(cn, tr, TipoPV.IdPerfil)) Then
                Throw New Exception
            End If
            '****************************** Insertar Lista  **************************************
            objPerfilTipoPV.InsertaListaPerfilTipoPV(cn, tr, listaTPV, TipoPV.IdPerfil)

            '*************  Elimina Medios de Pago************************
            Dim obJDaoPerfilMP As New DAO.DAOPerfil_MedioPago
            If (Not obJDaoPerfilMP.DeletexIdPerfil(cn, tr, TipoPV.IdPerfil)) Then
                Throw New Exception
            End If
            '****************************** Insertar Lista de Medios de Pago **************************************
            obJDaoPerfilMP.InsertListxIdPerfil(cn, tr, listaMP, TipoPV.IdPerfil)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
End Class
