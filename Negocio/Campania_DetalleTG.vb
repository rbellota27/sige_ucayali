﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class Campania_DetalleTG

    Dim obj As New DAO.DAOCampania_DetalleTG

    Public Sub Campania_DetalleTG_Transaction(ByVal IdCampania As Integer, ByVal Idproducto As Integer, ByVal IdCampaniaDetalle As Integer, ByVal listaCampania_DetalleTG As List(Of Entidades.Campania_DetalleTG))
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            obj.Campania_DetalleTGxDelete(IdCampania, 0, IdCampaniaDetalle, cn, tr)

            For Each eCampaniaDetalleTG As Entidades.Campania_DetalleTG In listaCampania_DetalleTG
                eCampaniaDetalleTG.IdCampania = IdCampania
                eCampaniaDetalleTG.IdCampaniaDetalle = IdCampaniaDetalle
                obj.Campania_DetalleTG_Transaction(eCampaniaDetalleTG, cn, tr)
            Next

            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

    End Sub

    Public Sub Campania_DetalleTG_TransactionALL(ByVal listaCampania_DetalleTG As List(Of Entidades.Campania_DetalleTG))
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            For Each eCampaniaDetalleTG As Entidades.Campania_DetalleTG In listaCampania_DetalleTG
                obj.Campania_DetalleTG_TransactionALL(eCampaniaDetalleTG, cn, tr)
            Next

            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

    End Sub

    Public Function Campania_DetalleTGxIdCampaniaDetalle(ByVal IdCampaniaDetalle As Integer) As List(Of Entidades.Campania_DetalleTG)
        Return obj.Campania_DetalleTGxIdCampaniaDetalle(IdCampaniaDetalle)
    End Function

    Public Function Campania_DetalleTGxIdCampania(ByVal IdProducto As Integer, ByVal IdCampania As Integer, ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSublinea As Integer, ByVal prod_codigo As String, ByVal prod_nombre As String, ByVal pageNumber As Integer, ByVal pageSize As Integer, ByVal tabla As DataTable) As List(Of Entidades.Campania_DetalleTG)
        Return obj.Campania_DetalleTGxIdCampania(IdProducto, IdCampania, IdTipoExistencia, IdLinea, IdSublinea, prod_codigo, prod_nombre, pageNumber, pageSize, tabla)
    End Function

End Class
