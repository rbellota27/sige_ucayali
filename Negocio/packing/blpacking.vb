﻿Imports System.Data.SqlClient
Public Class blpacking
    Dim objConexion As New DAO.Conexion

    Public Function EventoLoad(ByVal paramidSector As Integer, ByVal flagTransporte As Boolean, ByVal fecFiltro As Date, _
                                     ByVal idTransportista As Integer, ByVal turno As String)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        cn.Open()
        'Lista que contiene listas
        Dim listaTotal As New List(Of Object)

        'Lista de sectores
        Dim listaSector As New List(Of Entidades.be_Sector)
        listaSector = (New DAO.DAO_Sector_Objetos).DAO_ListarSector(cn)

        'Lista de Transportes
        Dim listaTransporte As New List(Of Entidades.be_transportista)
        listaTransporte = (New DAO.DAOVehiculo).SeleccionarTransporte(cn)

        'Lista de picking
        Dim listaPicking As New List(Of Entidades.be_picking)
        listaPicking = (New DAO.DAODespachos).DAO_ListaPicking(paramidSector, flagTransporte, fecFiltro, idTransportista, cn, turno)

        'Lista placa
        Dim listaPlaca As New List(Of Entidades.Vehiculo)
        listaPlaca = (New DAO.DAOVehiculo).seleccionarPlacasActivas(cn)
        cn.Close()
        listaTotal.Add(listaSector)
        listaTotal.Add(listaTransporte)
        listaTotal.Add(listaPicking)
        listaTotal.Add(listaPlaca)
        Return listaTotal
    End Function

    Public Function EventoClickBusqueda(ByVal paramidSector As Integer, ByVal flagTransporte As Boolean, ByVal fecFiltro As Date, _
                                     ByVal idTransportista As Integer, ByVal turno As String)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        cn.Open()
        'Lista que contiene listas
        Dim listaTotal As New List(Of Object)
        'Lista de picking
        Dim listaPicking As New List(Of Entidades.be_picking)
        listaPicking = (New DAO.DAODespachos).DAO_ListaPicking(paramidSector, flagTransporte, fecFiltro, idTransportista, cn, turno)

        listaTotal.Add(listaPicking)
        Return listaTotal
    End Function
End Class
