﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'29-01-2010 10:02 am se agrego selectcbo
Public Class Rol
    Private obj As New DAO.DAORol
    Public Function InsertaRol(ByVal rol As Entidades.Rol) As Boolean
        Return obj.InsertaRol(rol)
    End Function
    Public Function ActualizaRol(ByVal rol As Entidades.Rol) As Boolean
        Return obj.ActualizaRol(rol)
    End Function
    Public Function SelectAll() As List(Of Entidades.Rol)
        Return obj.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Rol)
        Return obj.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Rol)
        Return obj.SelectAllInactivo
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Rol)
        Return obj.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Rol)
        Return obj.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Rol)
        Return obj.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Rol)
        Return obj.SelectxId(id)
    End Function
    Public Function SelectCbo() As List(Of Entidades.Rol)
        Return obj.SelectCbo
    End Function
    Public Function SelectCboCliente() As List(Of Entidades.Rol)
        Return obj.SelectCboCliente
    End Function
    Public Function SelectCboProved() As List(Of Entidades.Rol)
        Return obj.SelectCboProved
    End Function
#Region "mantenimiento Persona"

    Public Function listarExisteRolEmpresa() As List(Of Entidades.Rol_Empresa)
        Return obj.listarExisteRolEmpresa()
    End Function

    Public Function listarRolxEmpresa(ByVal IdEmpresa As Integer) As List(Of Entidades.Rol_Empresa)
        Return obj.listarRolxEmpresa(IdEmpresa)
    End Function
    Public Function listarRolxEmpresaProveedor(ByVal IdEmpresa As Integer) As List(Of Entidades.Rol_Empresa)
        Return obj.listarRolxEmpresaProveedor(IdEmpresa)
    End Function



    Public Function listarRolxEmpresaUsuario(ByVal IdEmpresa As Integer, ByVal IdPersona As Integer) As List(Of Entidades.Rol_Empresa)
        Return obj.listarRolxEmpresaUsuario(IdEmpresa, IdPersona)
    End Function
    Public Function listarRolxEmpresaUsuarioProveedor(ByVal IdEmpresa As Integer, ByVal IdPersona As Integer) As List(Of Entidades.Rol_Empresa)
        Return obj.listarRolxEmpresaUsuarioProveedor(IdEmpresa, IdPersona)
    End Function


    Public Function listarMotivoBaja() As List(Of Entidades.MotivoBaja)
        Return obj.listarMotivoBaja
    End Function

    Public Function SelectxIdPersona(ByVal idPersona As Integer) As List(Of Entidades.RolPersona)
        Return obj.SelectxIdPersona(idPersona)
    End Function






#End Region

End Class
