﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class TipoDocumentoRef
    Private obj As New DAO.DAOTipoDocumentoRef
    Public Function InsertaTipoDocumentoRef(ByVal TipoDocumentoRef As Entidades.TipoDocumentoRef) As Boolean
        Return obj.InsertaTipoDocumentoRef(TipoDocumentoRef)
    End Function
    Public Function DeleteTipoDocumentoRef(ByVal TipoDocumentoRef As Entidades.TipoDocumentoRef) As Boolean
        Return obj.DeleteTipoDocumentoRef(TipoDocumentoRef)
    End Function
    Public Function SelectxIdTipoDoc(ByVal IdTipoDoc As Integer) As List(Of Entidades.TipoDocumentoRef)
        Return obj.SelectxIdTipoDoc(IdTipoDoc)
    End Function
    Public Function SelectxIdtipoDocRef(ByVal idTipoDocumento As Integer) As List(Of Entidades.TipoDocumento)
        Return obj.SelectxIdtipoDocRef(idTipoDocumento)
    End Function
End Class
