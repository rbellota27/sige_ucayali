﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



'*******************      VIERNES   19 MARZO 2010 HORA 10_22 AM

Imports System.Data.SqlClient
Public Class MovAlmacen
    Private obj As New DAO.DAOMovAlmacen
    Private cn As SqlConnection
    Private tr As SqlTransaction
    Private cmd As SqlCommand

    Public Function MovAlmacenUpdate_Cantidad_CostoxIdDetalleDocumento(ByVal Cantidad As Decimal, ByVal Costo As Decimal, ByVal IdDetalleDocumento As Integer) As Boolean

        Dim hecho As Boolean = False

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            obj.MovAlmacenUpdate_Cantidad_CostoxIdDetalleDocumento(Cantidad, Costo, IdDetalleDocumento, cn, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return hecho
    End Function

    Public Function SelectxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.MovAlmacen)
        Return obj.SelectxIdDocumento(IdDocumento)
    End Function
    Public Function SelectIdDocumentoInvInicial(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdTipoOperacion As Integer) As Integer
        Return obj.SelectIdDocumentoInvInicial(IdEmpresa, IdAlmacen, IdTipoOperacion)
    End Function
    Public Function RptMovAlmacen(ByVal idempresa As Integer, ByVal idorigen As Integer, ByVal iddestino As String, ByVal idlinea As String, ByVal idsublinea As String, ByVal fechaini As String, ByVal fechafin As String) As DataSet
        Return obj.RptMovAlmacen(idempresa, idorigen, iddestino, idlinea, idsublinea, fechaini, fechafin)
    End Function
    Public Function RptKardexMovAlmacen(ByVal idempresa As Integer, ByVal idalmacen As Integer, ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal idproducto As Integer, ByVal fechaini As String, ByVal fechafin As String) As DataSet
        Return obj.RptKardexMovAlmacen(idempresa, idalmacen, idlinea, idsublinea, idproducto, fechaini, fechafin)
    End Function
    Public Function RptMovAlmacenxDoc(ByVal idempresa As Integer, ByVal idorigen As Integer, ByVal iddestino As String, ByVal idlinea As String, ByVal idsublinea As String, ByVal fechaini As String, ByVal fechafin As String, ByVal idtipdoc As Integer) As DataSet
        Return obj.RptMovAlmacenxDoc(idempresa, idorigen, iddestino, idlinea, idsublinea, fechaini, fechafin, idtipdoc)
    End Function

    Public Function kardexporMetVal(ByVal idempresa As Integer, ByVal idalmacen As Integer, ByVal idproducto As Integer, ByVal fechainicio As String, ByVal fechafin As String) As DataSet
        Return obj.kardexporMetVal(idempresa, idalmacen, idproducto, fechainicio, fechafin)
    End Function

    Public Function CR_KardexProductoXMetVal(ByVal idempresa As Integer, ByVal idalmacen As Integer, ByVal idproducto As Integer, ByVal fechainicio As String, ByVal fechafin As String) As List(Of Entidades.Kardex)
        Return obj.CR_KardexProductoXMetVal(idempresa, idalmacen, idproducto, fechainicio, fechafin)
    End Function

    Public Function CR_KardexProductoXMetVal(ByVal idempresa As Integer, ByVal idalmacen As Integer, ByVal idproducto As Integer, ByVal fechainicio As String, ByVal fechafin As String, ByVal Igv As Decimal) As List(Of Entidades.Kardex)
        Return obj.CR_KardexProductoXMetVal(idempresa, idalmacen, idproducto, fechainicio, fechafin, Igv)
    End Function

End Class
