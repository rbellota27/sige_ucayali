﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'********************   JUEVES 10 JUNIO 2010 HORA 12_46 PM
Imports System.Data.SqlClient

Public Class Comisionista

    Private objDaoComisionista As New DAO.DAOComisionista
    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private tr As SqlTransaction = Nothing

    Public Function Comisionista_Select_Personas_Lista(ByVal IdComisionCab As Integer, ByVal IdEmpresa As Integer, _
                                               ByVal IdTienda As Integer, ByVal IdPerfil As Integer, ByVal IdRol As Integer, ByVal idConfigurado As Boolean) As List(Of Entidades.Comisionista)
        Return objDaoComisionista.Comisionista_Select_Personas_Lista(IdComisionCab, IdEmpresa, IdTienda, IdPerfil, IdRol, idConfigurado)
    End Function

    Public Function Comisionista_Select_Personas(ByVal IdComisionCab As Integer, ByVal IdEmpresa As Integer, _
                                                 ByVal IdTienda As Integer, ByVal IdPersona As Integer, _
                                                 ByVal IdPerfil As Integer, ByVal IdRol As Integer) As Entidades.Comisionista
        Return objDaoComisionista.Comisionista_Select_Personas(IdComisionCab, IdEmpresa, _
                                                 IdTienda, IdPersona, IdPerfil, IdRol)
    End Function

    Public Function Comisionista_Insert_por_grupo_Persona(ByVal ListaComisionista As List(Of Entidades.Comisionista)) As Integer
        Dim IdPersona As Integer = 0
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            For Each obj As Entidades.Comisionista In ListaComisionista
                IdPersona = (New DAO.DAOComisionista).Comisionista_Insert(obj, cn, tr)
            Next

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return IdPersona
    End Function

    Public Function Comisionista_Insert_por_grupo(ByVal ListaComisionista As List(Of Entidades.Comisionista)) As Integer
        Dim IdComisionCab As Integer = 0
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            For Each obj As Entidades.Comisionista In ListaComisionista
                IdComisionCab = (New DAO.DAOComisionista).Comisionista_Insert_por_grupo(obj, cn, tr)
            Next

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return IdComisionCab
    End Function

    Public Function Comisionista_SelectxParams(ByVal IdComisionCab As Integer) As List(Of Entidades.Comisionista)
        Return objDaoComisionista.Comisionista_SelectxParams(IdComisionCab)
    End Function


End Class
