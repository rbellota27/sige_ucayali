﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class ReporteImagen
    Private objDaoReporteImagen As New DAO.DAOReporteImagen

    Public Function InsertaReporteImagen(ByVal objReporteImagen As Entidades.ReporteImagen) As Boolean
        Return objDaoReporteImagen.InsertaImagenReporte(objReporteImagen)
    End Function
    Public Function ListNomImagen() As Entidades.ReporteImagen
        Return objDaoReporteImagen.ListNomImagen()
    End Function
    Public Function RptNomImagen() As DataSet
        Return objDaoReporteImagen.RptNomImagen()
    End Function
End Class
