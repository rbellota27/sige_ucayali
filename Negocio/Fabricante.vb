﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Fabricante
    Private objFab As New DAO.DAOFabricante
    Public Function InsertaFabricante(ByVal fabricante As Entidades.Fabricante) As Boolean
        Return objFab.InsertaFabricante(fabricante)
    End Function
    Public Function ActualizaFabricante(ByVal fabricante As Entidades.Fabricante) As Boolean
        Return objFab.ActualizaFabricante(fabricante)
    End Function
    Public Function SelectAll() As List(Of Entidades.Fabricante)
        Return objFab.SelectAll
    End Function

    Public Function SelectAllActivo() As List(Of Entidades.Fabricante)
        Return objFab.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Fabricante)
        Return objFab.SelectAllInactivo
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Fabricante)
        Return objFab.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Fabricante)
        Return objFab.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Fabricante)
        Return objFab.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Fabricante)
        Return objFab.SelectxId(id)
    End Function
    'Public Function SelectAllActivoxIdPais(ByVal IdPais As String) As List(Of Entidades.Fabricante)
    '    Return objFab.SelectAllActivoxIdPais(IdPais)
    'End Function
End Class
