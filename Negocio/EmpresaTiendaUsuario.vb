﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class EmpresaTiendaUsuario
    Private objDaoETU As New DAO.DAOEmpresaTiendaUsuario
    Public Function SelectEscalaTienda() As List(Of Entidades.Tienda)
        Return objDaoETU.SelectEscalaTienda
    End Function

    Public Function SelectTiendaObjetivo() As List(Of Entidades.Tienda)
        Return objDaoETU.SelectTiendaObjetivo
    End Function
    Public Function fn_GetTiendaPrincipal(ByVal IdUsuario As Integer) As String
        Return objDaoETU.fn_GetTiendaPrincipal(IdUsuario)
    End Function

    Public Function SelectEmpresaxIdUsuario(ByVal Idusuario As Integer) As List(Of Entidades.Propietario)
        Return objDaoETU.SelectEmpresaxIdUsuario(Idusuario)
    End Function
    Public Function SelectTiendaxIdEmpresaxIdUsuario(ByVal Idusuario As Integer, ByVal IdEmpresa As Integer) As List(Of Entidades.Tienda)
        Return objDaoETU.SelectTiendaxIdEmpresaxIdUsuario(Idusuario, IdEmpresa)
    End Function
    Public Function SelectTiendaxIdUsuario(ByVal Idusuario As Integer) As List(Of Entidades.Tienda)
        Return objDaoETU.SelectTiendaxIdUsuario(Idusuario)
    End Function
    Public Function SelectxIdUsuario(ByVal Idusuario As Integer) As List(Of Entidades.EmpresaTiendaUsuario)

        Return objDaoETU.SelectxIdUsuario(Idusuario)

    End Function
    Public Function RegistrarEmpresaTiendaUsuario(ByVal lista As List(Of Entidades.EmpresaTiendaUsuario), ByVal IdUsuario As Integer) As Boolean

        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False

        Try

            cn.Open()
            tr = cn.BeginTransaction

            objDaoETU.DeletexIdUsuario(IdUsuario, cn, tr)

            For i As Integer = 0 To lista.Count - 1

                lista(i).IdUsuario = IdUsuario
                objDaoETU.Insert(lista(i), cn, tr)

            Next

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function

End Class
