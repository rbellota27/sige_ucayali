﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class ConfigTipoCambio
    Private obj As New DAO.DAOConfigTipoCambio

    Private objConexion As New DAO.Conexion

    Public Function SelectAll() As List(Of Entidades.ConfigTipoCambio)

        Return obj.SelectAll

    End Function

    Public Function ConfigTipoCambioUpdate(ByVal lista As List(Of Entidades.ConfigTipoCambio)) As Boolean

        Dim hecho As Boolean = False
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction

            For i As Integer = 0 To lista.Count - 1

                obj.ConfigTipoCambioUpdate(lista(i), cn, tr)

            Next

            tr.Commit()
            hecho = True
        Catch ex As Exception

            tr.Rollback()
            hecho = False



        Finally

            If cn.State = ConnectionState.Open Then cn.Close()

        End Try

        Return hecho
    End Function

End Class
