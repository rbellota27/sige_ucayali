﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*********************   LUNES 24052010

Imports System.Data.SqlClient
Public Class CostoFletexPeso


    Private objDaoCostoFletexPeso As New DAO.DAOCostoFletexPeso
    Private cn As SqlConnection
    Private tr As SqlTransaction

    Public Function CostoFletexPeso_Delete(ByVal IdTiendaOrigen As Integer, ByVal IdTiendaDestino As Integer) As Boolean

        Dim hecho As Boolean = False

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            objDaoCostoFletexPeso.CostoFletexPeso_Delete(IdTiendaOrigen, IdTiendaDestino, tr)

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function

    Public Function CostoFletexPeso_Registrar(ByVal objCostoFletexPeso As Entidades.CostoFletexPeso) As Boolean

        Dim hecho As Boolean = False

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            objDaoCostoFletexPeso.CostoFletexPeso_Registrar(objCostoFletexPeso, cn, tr)

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function
    Public Function CostoFletexPeso_SelectxId(ByVal IdTiendaOrigen As Integer, ByVal IdTiendaDestino As Integer) As Entidades.CostoFletexPeso
        Return objDaoCostoFletexPeso.CostoFletexPeso_SelectxId(IdTiendaOrigen, IdTiendaDestino)
    End Function
    Public Function CostoFletexPeso_SelectxId_DT(ByVal IdTiendaOrigen As Integer, ByVal IdTiendaDestino As Integer) As DataTable
        Return objDaoCostoFletexPeso.CostoFletexPeso_SelectxId_DT(IdTiendaOrigen, IdTiendaDestino)
    End Function
End Class
