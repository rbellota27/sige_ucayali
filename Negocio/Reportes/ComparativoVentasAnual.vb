﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class ComparativoVentasAnual
    Dim obj As New DAO.DAOComparativoVentasAnual

    Public Function ComparativoVentasAnual(ByVal year As Integer, ByVal variable As String, ByVal IdPersona As Integer, ByVal idMoneda As Integer, ByVal idpropietario As Integer) As DataSet
        Return obj.ComparativoVentasAnual(year, variable, IdPersona, idMoneda, idpropietario)
    End Function


    Public Function reporteComisiones(ByVal fechainicio As String, ByVal fechafin As String, ByVal empresa As Integer, ByVal tienda As Integer, ByVal perfil As Integer, ByVal rol As Integer, ByVal usuario As Integer) As DataSet
        Return obj.reporteComisiones(fechainicio, fechafin, empresa, tienda, perfil, rol, usuario)
    End Function

End Class
