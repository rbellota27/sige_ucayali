﻿Imports DAO
Public Class LNValorizadoCajas
    Dim clsDatos As New DAOValorizadoCajas
    Public Function LN_ReturnDataTable(ByVal filtro As String, ByVal tabla As String) As DataTable
        Return clsDatos.DAO_CargarComboSecuencia(filtro, tabla)
    End Function

    Public Function LN_ReturnDataTableTonos(ByVal filtro As String, ByVal tabla As String, ByVal Cantidad As Decimal) As DataTable
        Return clsDatos.DAO_CargarComboSecuenciaTonos(filtro, tabla, Cantidad)
    End Function

    Public Function LN_ObtenerDatosGrilla(ByVal idTienda As Integer, ByVal fechaInicio As Date, ByVal fechaFinal As Date, ByVal topNumero As Integer, _
                                           ByVal opcion As Integer, ByVal opcFiltro As Integer, ByVal existencia As Integer, ByVal linea As Integer, _
                                           ByVal sublinea As Integer, ByVal pais As Integer, ByVal idFabrica As String, ByVal idTipoCliente As String) As DataTable
        Return clsDatos.DAO_ObtenerDatosGrilla(idTienda, fechaInicio, fechaFinal, topNumero, opcion, opcFiltro, existencia, linea, sublinea, pais, idFabrica, idTipoCliente)
    End Function
End Class
