﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Imports System.Data.Sql
Public Class TipoProducto
    Private objDaoTP As New DAO.DAOTipoProducto
    Private objcon As New DAO.Conexion
    Public Function SelectCbo() As List(Of Entidades.TipoProducto)
        Return objDaoTP.SelectCbo
    End Function

    Public Function selectTipoProductoAll(ByVal nombre As String) As List(Of Entidades.TipoProducto)
        Return objDaoTP.selectTipoProductoAll(nombre)
    End Function
    Public Function InsertaTipoProducto(ByVal tipoProducto As Entidades.TipoProducto) As Integer
        Dim cn As SqlConnection = objcon.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim resultado As Integer = 0
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim idtipoproducto As Integer = objDaoTP.InsertaTipoProducto(tipoProducto, cn, tr)
            tr.Commit()
            cn.Close()
            resultado = idtipoproducto
        Catch ex As Exception
            tr.Rollback()
            resultado = 0
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return resultado
    End Function
    Public Function UpdateTipoproducto(ByVal tipoProducto As Entidades.TipoProducto) As Boolean
        Dim cn As SqlConnection = objcon.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim resultado As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            If Not objDaoTP.UpdateTipoproducto(tipoProducto, cn, tr) Then
                Throw New Exception
            End If
            tr.Commit()
            cn.Close()
            resultado = True
        Catch ex As Exception
            tr.Rollback()
            resultado = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return resultado
    End Function

End Class
