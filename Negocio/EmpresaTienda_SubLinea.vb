﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'******************** MARTES 19 MAYO 2010 HORA 11_35 AM

Imports System.Data.SqlClient

Public Class EmpresaTienda_SubLinea

    Private cn As SqlConnection
    Private tr As SqlTransaction
    Private objDaoETS As New DAO.DAOEmpresaTienda_SubLinea

    Public Function DeletexParams(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdSubLinea As Integer) As Boolean
        Return objDaoETS.DeletexParams(IdEmpresa, IdTienda, IdSubLinea)
    End Function

    Public Function SelectxIdEmpresaxIdTiendaxIdSubLinea(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdSubLinea As Integer) As Entidades.EmpresaTienda_SubLinea
        Return objDaoETS.SelectxIdEmpresaxIdTiendaxIdSubLinea(IdEmpresa, IdTienda, IdSubLinea)
    End Function

    Public Function SelectxParams_DT(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer) As DataTable
        Return objDaoETS.SelectxParams_DT(IdEmpresa, IdTienda, IdLinea, IdSubLinea)
    End Function

    Public Function registrarEmpresaTienda_SubLinea(ByVal listaETS As List(Of Entidades.EmpresaTienda_SubLinea)) As Boolean

        Dim hecho As Boolean = False

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            For i As Integer = 0 To listaETS.Count - 1

                objDaoETS.EmpresaTienda_SubLinea_Registrar(listaETS(i), cn, tr)

            Next

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function

End Class
