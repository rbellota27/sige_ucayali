﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.





'*******************      VIERNES   19 MARZO 2010 HORA 10_22 AM











Public Class Empleado
    Private objDaoEmpleado As New DAO.DAOEmpleado

    Public Function EmpleadoSelectActivoxParams_Find(ByVal dni As String, ByVal ruc As String, _
                                    ByVal RazonApe As String, ByVal tipo As Integer, _
                                    ByVal pageindex As Integer, ByVal pagesize As Integer, _
                                    ByVal idrol As Integer, ByVal estado As Integer) As List(Of Entidades.PersonaView)
        Return objDaoEmpleado.EmpleadoSelectActivoxParams_Find(dni, ruc, RazonApe, tipo, pageindex, pagesize, idrol, estado)
    End Function



    Public Function SelectActivoxParams(ByVal opcion As Integer, ByVal textoBusqueda As String) As List(Of Entidades.Empleado)
        Return objDaoEmpleado.SelectActivoxParams(opcion, textoBusqueda)
    End Function

    Public Function SelectxIdPersona(ByVal IdPersona As Integer) As Entidades.Empleado
        Return objDaoEmpleado.SelectxIdPersona(IdPersona)
    End Function

    Public Function SelectxIdEmpleado(ByVal IdPersona As Integer) As Entidades.Empleado
        Return objDaoEmpleado.SelectxIdEmpleado(IdPersona)
    End Function

End Class
