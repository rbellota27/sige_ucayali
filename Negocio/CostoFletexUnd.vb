﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'*********************   LUNES 24052010
Imports System.Data.SqlClient

Public Class CostoFletexUnd

    Private objDaoCostoFletexUnd As New DAO.DAOCostoFletexUnd
    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private tr As SqlTransaction

    Public Function CostoFletexUnd_Delete(ByVal IdTiendaOrigen As Integer, ByVal IdTiendaDestino As Integer, ByVal IdProducto As Integer) As Boolean

        Dim hecho As Boolean = False

        Try

            cn.Open()
            tr = cn.BeginTransaction


            objDaoCostoFletexUnd.CostoFletexUnd_Delete(IdTiendaOrigen, IdTiendaDestino, IdProducto, cn, tr)

            tr.Commit()
            hecho = True

        Catch ex As Exception

            tr.Rollback()
            hecho = False
            Throw ex

        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function
    'Becker
    Public Function CostoFletexUnd_SelectxParams_DT(ByVal IdTiendaOrigen As Integer, ByVal IdTiendaDestino As Integer, ByVal IdProducto As Integer, _
                                                    ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, _
                                                    ByVal Producto As String, ByVal CodigoProd As String, ByVal IdTipoExistencia As Integer) As DataTable
        Return objDaoCostoFletexUnd.CostoFletexUnd_SelectxParams_DT(IdTiendaOrigen, IdTiendaDestino, IdProducto, IdLinea, IdSubLinea, _
                                                                    Producto, CodigoProd, IdTipoExistencia)
    End Function

    Public Function CostoFletexUnd_Registrar(ByVal lista As List(Of Entidades.CostoFletexUnd)) As Boolean

        Dim hecho As Boolean = False

        Try

            cn.Open()
            tr = cn.BeginTransaction

            For i As Integer = 0 To lista.Count - 1

                objDaoCostoFletexUnd.CostoFletexUnd_Registrar(lista(i), cn, tr)

            Next


            tr.Commit()
            hecho = True

        Catch ex As Exception

            tr.Rollback()
            hecho = False
            Throw ex

        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function
    'Becker
    Public Function CostoFletexUnd_SelectProductoBasexParams(ByVal IdTiendaOrigen As Integer, ByVal IdTiendaDestino As Integer, _
                                                             ByVal IdProducto As Integer, ByVal IdLinea As Integer, _
                                                             ByVal IdSubLinea As Integer, ByVal Producto As String, _
                                                             ByVal CodigoProd As String, ByVal IdTipoExistencia As Integer, ByVal tabla As DataTable) As List(Of Entidades.CostoFletexUnd)
        Return objDaoCostoFletexUnd.CostoFletexUnd_SelectProductoBasexParams(IdTiendaOrigen, IdTiendaDestino, IdProducto, _
                                                                              IdLinea, IdSubLinea, _
                                                                             Producto, CodigoProd, IdTipoExistencia, tabla)
    End Function

End Class
