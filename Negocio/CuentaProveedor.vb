﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'''''''    26 julio 2010 Hora 12:08 am

Imports System.Data.SqlClient
Public Class CuentaProveedor
    Private objConexion As New DAO.Conexion
    Private objCuentaProveedor As New DAO.DAOCuentaProveedor
    Dim cn As SqlConnection = objConexion.ConexionSIGE
    Dim tr As SqlTransaction = Nothing
    Public Function InsertaCuentaProv(ByVal CuentaProveedor As Entidades.CuentaProveedor) As Integer
        Return objCuentaProveedor.InsertaCuentaProveedor(CuentaProveedor)
    End Function

    Public Function ActualizaCuentaProv(ByVal CuentaProveedor As Entidades.CuentaProveedor) As Boolean
        Return objCuentaProveedor.ActualizarCuentaProveedor(CuentaProveedor)
    End Function

    Public Function DeletexIdProveedorCuentaProv(ByVal IdProveedor As Integer) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Return objCuentaProveedor.DeletexIdProveedorCuentaProveedor(IdProveedor, cn, tr)
    End Function

    Public Function SelectxIdProvCuentaProveedor(ByVal IdProveedor As Integer) As List(Of Entidades.CuentaProveedor)
        Return objCuentaProveedor.SelectxIdProveedorCuentaProveedor(IdProveedor)
    End Function
    Public Function SelectxIdProv_IdPropCuentaProveedor(ByVal IdProveedor As Integer, ByVal IdPropietario As Integer) As List(Of Entidades.CuentaProveedor)
        Return objCuentaProveedor.SelectxIdProv_IdPropCuentaProveedor(IdProveedor, IdPropietario)
    End Function

    Public Function SelectGrillaCxPResumenDocumento(ByVal IdProveedor As Integer, ByVal idmoneda As Integer) As List(Of Entidades.CxPResumenView)
        Return objCuentaProveedor.SelectGrillaCxPResumenDocumento(IdProveedor, idmoneda)
    End Function
    Public Function SelectxIdProveedor(ByVal IdProveedor As Integer) As List(Of Entidades.CuentaProveedor)
        Return objCuentaProveedor.SelectAllConsultaxIdProveedor(IdProveedor)
    End Function
    Public Function SelectAllConsultaxHistCtaProv(ByVal idctaPRoveedor As Integer) As List(Of Entidades.CuentaProveedor)
        Return objCuentaProveedor.SelectAllConsultaxHistCtaProv(idctaPRoveedor)
    End Function
    Public Function DocAsociadosCtaProvxPag(ByVal idCtaProv As Integer) As List(Of Entidades.CxCResumenView)
        Return objCuentaProveedor.DocAsociadosCtaProvxPag(idCtaProv)
    End Function
    Public Function GrabarCuentaProveedor(ByVal lista As List(Of Entidades.CuentaProveedor), ByVal idproveedor As Integer) As Boolean
        Dim hecho As Boolean = False
        '**************** obtengo la lista de registros a eliminar
        Dim listaOld As List(Of Entidades.CuentaProveedor) = objCuentaProveedor.SelectAllConsultaxIdProveedor(idproveedor)
        Dim i As Integer = listaOld.Count - 1
        While i >= 0
            Dim j As Integer = lista.Count - 1
            While j >= 0
                If listaOld.Item(i).IdCuentaProv = lista.Item(j).IdCuentaProv Then
                    listaOld.RemoveAt(i)
                    Exit While
                End If
                j = j - 1
            End While
            i = i - 1
        End While

        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            '************************ elimino los registros de caja usuario 
            For i = 0 To listaOld.Count - 1
                objCuentaProveedor.DeletexIdCtaProveedor(listaOld(i).IdCuentaProv, cn, tr)
            Next
            '************************** inserto/actualizo los registros
            Dim objDaoUtil As New DAO.DAOUtil
            For i = 0 To lista.Count - 1
                If objDaoUtil.ValidarExistenciaxTablax1Campo(cn, tr, "Cuentaproveedor", "IdCuentaProv", lista(i).IdCuentaProv.ToString) > 0 Then
                    '*********** actualizo
                    objCuentaProveedor.UpdateCuentaProveedorT(lista(i), cn, tr)
                Else
                    '*********** inserto
                    objCuentaProveedor.InsertaCuentaProveedorT(lista(i), cn, tr)
                End If
            Next
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function

    'Public Sub UpdateCuentaProveedorT(ByVal obj As Entidades.CuentaProveedor, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
    'End Sub
    Public Function DeletexCuentaPersonaxIdPersona(ByVal idproveedor As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Dim hecho As Boolean
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            objCuentaProveedor.DeletexCuentaProveedorxIdProveedor(idproveedor, cn, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho

    End Function
    Public Function DeletexIdCuentaProveedor(ByVal idcuentaProv As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Dim hecho As Boolean
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            objCuentaProveedor.DeletexIdCuentaProveedor(idcuentaProv, cn, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function

End Class
