﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.





'******************** MARTES 19 MAYO 2010 HORA 11_35 AM



Public Class DocReciboCaja
    Private objDaoDocReciboCaja As New DAO.DAODocReciboCaja

    '********** RECIBO EGRESO

    Public Function RPT_MovCajaxParams_DT(ByVal IdEmpresa As Integer, ByVal Idtienda As Integer, ByVal IdCaja As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date) As DataTable
        Return objDaoDocReciboCaja.RPT_MovCajaxParams_DT(IdEmpresa, Idtienda, IdCaja, FechaInicio, FechaFin)
    End Function
    Public Function SelectResumenMontoxSustentar_DT(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdCaja As Integer) As DataTable
        Return objDaoDocReciboCaja.SelectResumenMontoxSustentar_DT(IdEmpresa, IdTienda, IdCaja)
    End Function
    Public Function ReciboCajaEgresoInsert(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objMovCaja As Entidades.MovCaja, ByVal listaCancelacion As List(Of Entidades.PagoCaja), ByVal objRelacionDocumento As Entidades.RelacionDocumento, ByVal objObservaciones As Entidades.Observacion) As Integer
        Return objDaoDocReciboCaja.ReciboCajaEgresoInsert(objDocumento, listaDetalleConcepto, objMovCaja, listaCancelacion, objRelacionDocumento, objObservaciones)
    End Function
    Public Function ReciboCajaEgresoUpdate(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objMovCaja As Entidades.MovCaja, ByVal listaCancelacion As List(Of Entidades.PagoCaja), ByVal objRelacionDocumento As Entidades.RelacionDocumento, ByVal objObservaciones As Entidades.Observacion) As Integer
        Return objDaoDocReciboCaja.ReciboCajaEgresoUpdate(objDocumento, listaDetalleConcepto, objMovCaja, listaCancelacion, objRelacionDocumento, objObservaciones)
    End Function
    Public Function ReciboCajaEgresoAnular(ByVal IdDocumento As Integer) As Boolean
        Return objDaoDocReciboCaja.ReciboCajaEgresoAnular(IdDocumento)
    End Function
    Public Function getDataSet_DocReciboEgreso(ByVal IdDocumento As Integer) As DataSet

        Return objDaoDocReciboCaja.getDataSet_DocReciboEgreso(IdDocumento)

    End Function

    '********** RECIBO INGRESO
    Public Function ReciboCajaIngresoInsert(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objMovCaja As Entidades.MovCaja, ByVal listaCancelacion As List(Of Entidades.PagoCaja), ByVal objObservaciones As Entidades.Observacion, ByVal listaMovBanco As List(Of Entidades.MovBancoView), ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento)) As Integer
        Return objDaoDocReciboCaja.ReciboCajaIngresoInsert(objDocumento, listaDetalleConcepto, objMovCaja, listaCancelacion, objObservaciones, listaMovBanco, listaRelacionDocumento)
    End Function
    Public Function ReciboCajaIngresoUpdate(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objMovCaja As Entidades.MovCaja, ByVal listaCancelacion As List(Of Entidades.PagoCaja), ByVal objObservaciones As Entidades.Observacion, ByVal listaMovBanco As List(Of Entidades.MovBancoView), ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento)) As Integer
        Return objDaoDocReciboCaja.ReciboCajaIngresoUpdate(objDocumento, listaDetalleConcepto, objMovCaja, listaCancelacion, objObservaciones, listaMovBanco, listaRelacionDocumento)
    End Function
    Public Function DocumentoReciboIngresoDeshacerMov_Edicion(ByVal IdDocumento As Integer) As Boolean
        Return objDaoDocReciboCaja.DocumentoReciboIngresoDeshacerMov_Edicion(IdDocumento)
    End Function
    Public Function DocumentoReciboIngresoAnular(ByVal IdDocumento As Integer) As Boolean
        Return objDaoDocReciboCaja.DocumentoReciboIngresoAnular(IdDocumento)
    End Function

End Class
