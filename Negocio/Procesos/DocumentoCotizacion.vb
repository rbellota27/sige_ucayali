﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'************************   MARTES 01 06 2010 HORA 03_08 PM
Imports System.Data.SqlClient
Public Class DocumentoCotizacion
    Private objDaoDocumentoCotizacion As New DAO.DAODocumentoCotizacion
    Private objConexion As New DAO.Conexion
    Private cn As SqlConnection
    Private cmd As SqlCommand
    Private tr As SqlTransaction

    Public Function DocumentoVenta_SelectDetxIdDocumentoRef2(ByVal IdDocumentoRef As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Fecha As Date, ByVal IdTipoPV As Integer, ByVal IdPersona As Integer, ByVal IdMoneda As Integer, ByVal IdVendedor As Integer) As List(Of Entidades.DetalleDocumento)
        Return objDaoDocumentoCotizacion.DocumentoVenta_SelectDetxIdDocumentoRef2(IdDocumentoRef, IdEmpresa, IdTienda, Fecha, IdTipoPV, IdPersona, IdMoneda, IdVendedor)
    End Function

    Public Function DocumentoVenta_SelectDetxIdDocumentoRef3(ByVal IdDocumentoRef As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Fecha As Date, ByVal IdTipoPV As Integer, ByVal IdPersona As Integer, ByVal IdMoneda As Integer, ByVal IdVendedor As Integer) As List(Of Entidades.DetalleDocumento)
        Return objDaoDocumentoCotizacion.DocumentoVenta_SelectDetxIdDocumentoRef3(IdDocumentoRef, IdEmpresa, IdTienda, Fecha, IdTipoPV, IdPersona, IdMoneda, IdVendedor)
    End Function

    Public Function DocumentoVenta_SelectDetxIdDocumentoRef(ByVal IdDocumentoRef As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Fecha As Date, ByVal IdTipoPV As Integer, ByVal IdPersona As Integer, ByVal IdMoneda As Integer, ByVal IdVendedor As Integer) As List(Of Entidades.DetalleDocumento)
        Return objDaoDocumentoCotizacion.DocumentoVenta_SelectDetxIdDocumentoRef(IdDocumentoRef, IdEmpresa, IdTienda, Fecha, IdTipoPV, IdPersona, IdMoneda, IdVendedor)
    End Function

    Public Function anularCotizacion(ByVal IdDocumento As Integer) As Boolean
        Dim hecho As Boolean = False

        Try

            cn = objConexion.ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            '**************** ANULAMOS EL DOCUMENTO
            objDaoDocumentoCotizacion.DocumentoCotizacion_DeshacerMov(IdDocumento, False, False, False, False, False, False, False, False, False, True, cn, tr)

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function


    Public Function updateCotizacion(ByVal objDocumento As Entidades.Documento, ByVal objAnexoDocumento As Entidades.Anexo_Documento, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal listaAnexoDetalleDocumento As List(Of Entidades.Anexo_DetalleDocumento), ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objObservacion As Entidades.Observacion, ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal objMontoRegimenPercepcion As Entidades.MontoRegimen, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento)) As Integer

        Try

            cn = objConexion.ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            '**************** SE DESHACEN LOS MOVIMIENTOS
            objDaoDocumentoCotizacion.DocumentoCotizacion_DeshacerMov(objDocumento.Id, True, True, True, True, True, True, True, True, True, False, cn, tr)

            '************** ACTUALIZO LA CABECERA
            Dim objDaoDocumento As New DAO.DAODocumento
            objDaoDocumento.DocumentoUpdate(objDocumento, cn, tr)

            '*************** INSERTO ANEXO
            Dim objDaoAnexoDocumento As New DAO.DAOAnexo_Documento
            objAnexoDocumento.IdDocumento = objDocumento.Id
            objDaoAnexoDocumento.InsertAnexo_Documento(objAnexoDocumento, cn, tr)

            '************** INSERTO DETALLE DOCUMENTO
            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            For i As Integer = 0 To listaDetalleDocumento.Count - 1

                listaDetalleDocumento(i).IdDocumento = objDocumento.Id
                listaDetalleDocumento(i).IdDetalleDocumento = objDaoDetalleDocumento.InsertaDetalleDocumento(cn, listaDetalleDocumento(i), tr)

                listaAnexoDetalleDocumento(i).IdDocumento = listaDetalleDocumento(i).IdDocumento
                listaAnexoDetalleDocumento(i).IdDetalleDocumento = listaDetalleDocumento(i).IdDetalleDocumento
            Next

            '********************** INSERTO ANEXO DETALLE DOCUMENTO
            Dim objDaoAnexoDetalleDocumento As New DAO.DAOAnexo_DetalleDocumento
            For i As Integer = 0 To listaAnexoDetalleDocumento.Count - 1
                objDaoAnexoDetalleDocumento._Anexo_DetalleDocumentoInsert(listaAnexoDetalleDocumento(i), cn, tr)
            Next

            '******************** INSERTO DETALLE CONCEPTO
            Dim objDaoDetalleConcepto As New DAO.DAODetalleConcepto
            For i As Integer = 0 To listaDetalleConcepto.Count - 1

                listaDetalleConcepto(i).IdDocumento = objDocumento.Id
                objDaoDetalleConcepto.DetalleConceptoInsert(listaDetalleConcepto(i), cn, tr)

            Next

            '****************** OBSERVACION
            If (objObservacion IsNot Nothing) Then
                Dim objdaoObservacion As New DAO.DAOObservacion
                objObservacion.IdDocumento = objDocumento.Id
                objdaoObservacion.InsertaObservacionT(cn, tr, objObservacion)
            End If

            '****************** PUNTO PARTIDA
            If (objPuntoPartida IsNot Nothing) Then
                Dim objdaoPuntoPartida As New DAO.DAOPuntoPartida
                objPuntoPartida.IdDocumento = objDocumento.Id
                objdaoPuntoPartida.InsertaPuntoPartida(objPuntoPartida, cn, tr)
            End If

            '****************** PUNTO LLEGADA
            If (objPuntoLlegada IsNot Nothing) Then
                Dim objdaoPuntoLlegada As New DAO.DAOPuntoLlegada
                objPuntoLlegada.IdDocumento = objDocumento.Id
                objdaoPuntoLlegada.InsertaPuntoLlegadaT(cn, objPuntoLlegada, tr)
            End If

            '****************** PERCEPCION
            If (objMontoRegimenPercepcion IsNot Nothing) Then
                Dim objdaoMontoRegimen As New DAO.DAOMontoRegimen
                objMontoRegimenPercepcion.IdDocumento = objDocumento.Id
                objdaoMontoRegimen.InsertaMontoRegimen(cn, objMontoRegimenPercepcion, tr)
            End If


            '********************  INSERTO LISTA RELACION DOCUMENTO
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1
                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            Next
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function


    Public Function DocumentoCotizacionSelectDetalleConcepto(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleConcepto)
        Return objDaoDocumentoCotizacion.DocumentoCotizacionSelectDetalleConcepto(IdDocumento)
    End Function
    Public Function DocumentoCotizacionSelectCab22Factura(ByVal IdSerie As Integer, ByVal nroDocumento As String, ByVal IdDocumento As Integer) As Entidades.DocumentoCotizacion
        Return objDaoDocumentoCotizacion.DocumentoCotizacionSelectCab22Factura(IdSerie, nroDocumento, IdDocumento)
    End Function

    Public Function DocumentoCotizacionSelectCab(ByVal IdSerie As Integer, ByVal nroDocumento As Integer, ByVal IdDocumento As Integer) As Entidades.DocumentoCotizacion
        Return objDaoDocumentoCotizacion.DocumentoCotizacionSelectCab(IdSerie, nroDocumento, IdDocumento)
    End Function

    Public Function DocumentoCotizacionSelectDet(ByVal IdDocumento As Integer, Optional ByVal Consignacion As Boolean = False) As List(Of Entidades.DetalleDocumento)
        Return objDaoDocumentoCotizacion.DocumentoCotizacionSelectDet(IdDocumento, Consignacion)
    End Function

    Public Function registrarCotizacion(ByVal objDocumento As Entidades.Documento, ByVal objAnexoDocumento As Entidades.Anexo_Documento, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal listaAnexoDetalleDocumento As List(Of Entidades.Anexo_DetalleDocumento), ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objObservacion As Entidades.Observacion, ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal objMontoRegimenPercepcion As Entidades.MontoRegimen, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento)) As Integer

        Try

            cn = objConexion.ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            '************** INSERTO CABECERA
            objDocumento.Id = (New DAO.DAODocumento).InsertaDocumento(cn, objDocumento, tr)

            '*************** INSERTO ANEXO
            Dim objDaoAnexoDocumento As New DAO.DAOAnexo_Documento
            objAnexoDocumento.IdDocumento = objDocumento.Id
            objDaoAnexoDocumento.InsertAnexo_Documento(objAnexoDocumento, cn, tr)

            '************** INSERTO DETALLE DOCUMENTO
            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            For i As Integer = 0 To listaDetalleDocumento.Count - 1

                listaDetalleDocumento(i).IdDocumento = objDocumento.Id

                If objDocumento.IdTipoOperacion = 4 Then '' Consignacion a Cliente
                    listaDetalleDocumento(i).CantxAtender = listaDetalleDocumento(i).Cantidad
                End If

                listaDetalleDocumento(i).IdDetalleDocumento = objDaoDetalleDocumento.InsertaDetalleDocumento(cn, listaDetalleDocumento(i), tr)

                listaAnexoDetalleDocumento(i).IdDocumento = listaDetalleDocumento(i).IdDocumento
                listaAnexoDetalleDocumento(i).IdDetalleDocumento = listaDetalleDocumento(i).IdDetalleDocumento

            Next

            '********************** INSERTO ANEXO DETALLE DOCUMENTO
            Dim objDaoAnexoDetalleDocumento As New DAO.DAOAnexo_DetalleDocumento
            For i As Integer = 0 To listaAnexoDetalleDocumento.Count - 1

                objDaoAnexoDetalleDocumento._Anexo_DetalleDocumentoInsert(listaAnexoDetalleDocumento(i), cn, tr)

            Next


            '******************** INSERTO DETALLE CONCEPTO
            Dim objDaoDetalleConcepto As New DAO.DAODetalleConcepto
            For i As Integer = 0 To listaDetalleConcepto.Count - 1

                listaDetalleConcepto(i).IdDocumento = objDocumento.Id
                objDaoDetalleConcepto.DetalleConceptoInsert(listaDetalleConcepto(i), cn, tr)

            Next

            '****************** OBSERVACION
            If (objObservacion IsNot Nothing) Then
                Dim objdaoObservacion As New DAO.DAOObservacion
                objObservacion.IdDocumento = objDocumento.Id
                objdaoObservacion.InsertaObservacionT(cn, tr, objObservacion)
            End If

            '****************** PUNTO PARTIDA
            If (objPuntoPartida IsNot Nothing) Then
                Dim objdaoPuntoPartida As New DAO.DAOPuntoPartida
                objPuntoPartida.IdDocumento = objDocumento.Id
                objdaoPuntoPartida.InsertaPuntoPartida(objPuntoPartida, cn, tr)
            End If

            '****************** PUNTO LLEGADA
            If (objPuntoLlegada IsNot Nothing) Then
                Dim objdaoPuntoLlegada As New DAO.DAOPuntoLlegada
                objPuntoLlegada.IdDocumento = objDocumento.Id
                objdaoPuntoLlegada.InsertaPuntoLlegadaT(cn, objPuntoLlegada, tr)
            End If

            '****************** PERCEPCION
            If (objMontoRegimenPercepcion IsNot Nothing) Then
                Dim objdaoMontoRegimen As New DAO.DAOMontoRegimen
                objMontoRegimenPercepcion.IdDocumento = objDocumento.Id
                objdaoMontoRegimen.InsertaMontoRegimen(cn, objMontoRegimenPercepcion, tr)
            End If


            '********************  INSERTO LISTA RELACION DOCUMENTO
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1
                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            Next

            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function

    Public Function SelectValorEstimadoConcepto(ByVal IdConcepto As Integer, ByVal IdTipoDocumento As Integer, ByVal UbigeoOrigen As String, ByVal UbigeoDestino As String, ByVal IdMoneda_Destino As Integer, ByVal PesoTotal As Decimal, ByVal ImporteTotal As Decimal, ByVal Fecha As Date) As Decimal
        Return objDaoDocumentoCotizacion.SelectValorEstimadoConcepto(IdConcepto, IdTipoDocumento, UbigeoOrigen, UbigeoDestino, IdMoneda_Destino, PesoTotal, ImporteTotal, Fecha)
    End Function
    Public Function DocumentoCotizacion_ConsultarTipoPrecioxParams(ByVal IdTienda As Integer, ByVal IdProducto As Integer, ByVal IdMoneda_Destino As Integer, ByVal Fecha As Date) As List(Of Entidades.Catalogo)
        Return objDaoDocumentoCotizacion.DocumentoCotizacion_ConsultarTipoPrecioxParams(IdTienda, IdProducto, IdMoneda_Destino, Fecha)
    End Function

    Public Function DocumentoCotizacion_ConsultarTipoPrecioxParams2(ByVal IdTienda As Integer, ByVal IdProducto As Integer, ByVal IdMoneda_Destino As Integer, ByVal Fecha As Date) As List(Of Entidades.Catalogo)
        Return objDaoDocumentoCotizacion.DocumentoCotizacion_ConsultarTipoPrecioxParams2(IdTienda, IdProducto, IdMoneda_Destino, Fecha)
    End Function
    Public Function DocumentoCotizacion_ValSelectTipoPrecioPV(ByVal IdUsuario As Integer, ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer, ByVal fecha As Date, ByVal IdMoneda As Integer, ByVal IdTienda As Integer, ByVal IdCliente As Integer, ByVal IdTipoPV_Destino As Integer, ByVal Cad_IdProducto As String, ByVal Cad_IdUnidadMedida As String, ByVal Cad_Cantidad As String, ByVal IdTipoOperacion As Integer) As Entidades.Catalogo
        Return objDaoDocumentoCotizacion.DocumentoCotizacion_ValSelectTipoPrecioPV(IdUsuario, IdCondicionPago, IdMedioPago, fecha, IdMoneda, IdTienda, IdCliente, IdTipoPV_Destino, Cad_IdProducto, Cad_IdUnidadMedida, Cad_Cantidad, IdTipoOperacion)
    End Function
End Class
