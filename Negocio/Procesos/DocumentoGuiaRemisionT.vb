﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'**********************   VIERNES 25 06 2010

Imports System.Data.SqlClient

Public Class DocumentoGuiaRemisionT
    Private objDaoDocumentoGRT As New DAO.DAODocumentoGuiaRemisionT
    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private tr As SqlTransaction

    Public Function registrarDocumento(ByVal objDocumento As Entidades.Documento, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento), ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal objObservacion As Entidades.Observacion) As Integer


        Try

            cn.Open()
            tr = cn.BeginTransaction

            '*************** INSERTO CABECERA
            objDocumento.Id = (New DAO.DAODocumento).InsertaDocumento(cn, objDocumento, tr)

            '**************** INSERTO RELACION DOCUMENTO
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1
                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            Next

            '*************** INSERTO PUNTOPARTIDA
            Dim objDaoPuntoPartida As New DAO.DAOPuntoPartida
            If (objPuntoPartida IsNot Nothing) Then
                objPuntoPartida.IdDocumento = objDocumento.Id
                objDaoPuntoPartida.Registrar(objPuntoPartida, cn, tr)
            End If

            '*************** INSERTO PUNTO LLEGADA
            Dim objDaoPuntoLlegada As New DAO.DAOPuntoLlegada
            If (objPuntoLlegada IsNot Nothing) Then
                objPuntoLlegada.IdDocumento = objDocumento.Id
                objDaoPuntoLlegada.Registrar(objPuntoLlegada, cn, tr)
            End If

            '****************** INSERTO OBSERVACIONES
            Dim objDaoObservacion As New DAO.DAOObservacion
            If (objObservacion IsNot Nothing) Then
                objObservacion.IdDocumento = objDocumento.Id
                objDaoObservacion.Registrar(objObservacion, cn, tr)
            End If

            '*************** INSERT DETALLE DOCUMENTO
            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            For i As Integer = 0 To listaDetalleDocumento.Count - 1
                listaDetalleDocumento(i).IdDocumento = objDocumento.Id
                objDaoDetalleDocumento.Registrar(listaDetalleDocumento(i), cn, tr)
            Next

            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function
    Public Function actualizarDocumento(ByVal objDocumento As Entidades.Documento, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento), ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal objObservacion As Entidades.Observacion) As Integer

        Try

            Dim listaAux As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumento(objDocumento.Id)
            For i As Integer = listaAux.Count - 1 To 0 Step -1
                For x As Integer = listaDetalleDocumento.Count - 1 To 0 Step -1
                    If (listaAux(i).IdDetalleDocumento = listaDetalleDocumento(x).IdDetalleDocumento) Then
                        listaAux.RemoveAt(i)
                        Exit For
                    End If
                Next
            Next

            cn.Open()
            tr = cn.BeginTransaction


            '************** DESHACER MOV
            objDaoDocumentoGRT.DeshacerMov(objDocumento.Id, True, False, cn, tr)

            '*************** CABECERA
            Dim objDaoDocumento As New DAO.DAODocumento
            objDaoDocumento.DocumentoUpdate(objDocumento, cn, tr)

            '**************** RELACION DOCUMENTO
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1
                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            Next

            '*************** PUNTOPARTIDA
            Dim objDaoPuntoPartida As New DAO.DAOPuntoPartida
            If (objPuntoPartida IsNot Nothing) Then
                objPuntoPartida.IdDocumento = objDocumento.Id
                objDaoPuntoPartida.Registrar(objPuntoPartida, cn, tr)
            End If

            '*************** PUNTO LLEGADA
            Dim objDaoPuntoLlegada As New DAO.DAOPuntoLlegada
            If (objPuntoLlegada IsNot Nothing) Then
                objPuntoLlegada.IdDocumento = objDocumento.Id
                objDaoPuntoLlegada.Registrar(objPuntoLlegada, cn, tr)
            End If

            '****************** OBSERVACIONES
            Dim objDaoObservacion As New DAO.DAOObservacion
            If (objObservacion IsNot Nothing) Then
                objObservacion.IdDocumento = objDocumento.Id
                objDaoObservacion.Registrar(objObservacion, cn, tr)
            End If

            '*************** DETALLE DOCUMENTO
            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            For i As Integer = 0 To listaDetalleDocumento.Count - 1
                listaDetalleDocumento(i).IdDocumento = objDocumento.Id
                objDaoDetalleDocumento.Registrar(listaDetalleDocumento(i), cn, tr)
            Next

            '********************  ELIMINAR EL DETALLE NO UTILIZADO
            For i As Integer = 0 To listaAux.Count - 1
                objDaoDetalleDocumento.DeletexIdDetalleDocumento(listaAux(i).IdDetalleDocumento, cn, tr)
            Next

            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function
    Public Function anularDocumento(ByVal IdDocumento As Integer) As Boolean
        Dim hecho As Boolean = False
        Try

            cn.Open()
            tr = cn.BeginTransaction

            objDaoDocumentoGRT.DeshacerMov(IdDocumento, False, True, cn, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function
End Class
