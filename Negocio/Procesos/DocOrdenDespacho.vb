﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.





'*************   MARTES 27 ABRIL 2010 HORA 02_27 PM









Imports System.Data.SqlClient

Public Class DocOrdenDespacho
    Private obj As New DAO.DAODocOrdenDespacho
    Private objConexion As New DAO.Conexion
    Private cn As SqlConnection
    Private tr As SqlTransaction

    '*********************** VERSION 2
    Public Function registrarDocumento(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal objObservaciones As Entidades.Observacion, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento), ByVal Factor As Integer, ByVal MoverStockFisico As Boolean, ByVal ValidarDespacho As Boolean) As Integer

        cn = (New DAO.Conexion).ConexionSIGE
        Try

            '**** MoverStockFisico: MUEVE EL STOCK DE ALMACÉN
            '**** ValidarDespacho: valida el despacho ( cantidad, etc ) y actualiza el stock x atender afecto

            cn.Open()
            tr = cn.BeginTransaction

            '************* CABECERA
            objDocumento.Id = (New DAO.DAODocumento).InsertaDocumento(cn, objDocumento, tr)

            '************* DETALLE
            obj.DocumentoOrdenDespacho_DetalleInsert(cn, listaDetalleDocumento, tr, objDocumento.Id, Factor, MoverStockFisico, ValidarDespacho)

            '************ OBSERVACIONES
            If (objObservaciones IsNot Nothing) Then
                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservaciones)
            End If

            '************* DOCUMENTO REFERENCIA
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1
                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            Next

            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function
    Public Function DocumentoOrdenDespacho_GenerarDespachoAutomaticoxParams(ByVal IdAlmacen As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal IdUsuario As Integer, ByVal IdTipoDocumento_OD As Integer, ByVal IdTipoOperacion_OD As Integer, ByVal IdDocumento As Integer) As Boolean

        '***  Si IdDocumento=0 Generación Masiva de acuerdo a los parámetros ingresados
        '***  Si IdDocumento<>0 Generación Puntual
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try

            cn.Open()
            tr = cn.BeginTransaction

            obj.DocumentoOrdenDespacho_GenerarDespachoAutomaticoxParams(IdAlmacen, FechaInicio, FechaFin, IdUsuario, IdTipoDocumento_OD, IdTipoOperacion_OD, IdDocumento, cn, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return hecho
    End Function
 



    '************************** VERSION 1
    Public Function DocumentoOrdenDespacho_GenerarDespachoAutomatico(ByVal IdAlmacen As Integer, ByVal FechaFin As Date, ByVal IdUsuario As Integer, ByVal IdTipoDocumento_OD As Integer, ByVal IdTipoOperacion_OD As Integer, ByVal IdDocumento As Integer) As Boolean

        '***  Si IdDocumento=0 Generación Masiva de acuerdo a los parámetros ingresados
        '***  Si IdDocumento<>0 Generación Puntual
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try

            cn.Open()
            tr = cn.BeginTransaction

            obj.DocumentoOrdenDespacho_GenerarDespachoAutomatico(IdAlmacen, FechaFin, IdUsuario, IdTipoDocumento_OD, IdTipoOperacion_OD, IdDocumento, cn, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return hecho
    End Function
    Public Function SelectxIdDocumento(ByVal iddocumento As Integer) As Entidades.DocOrdenDespacho
        Return obj.SelectxIdDocumento(iddocumento)
    End Function
    Public Function InsertaOrdenDespacho(ByVal listaDetalle As List(Of Entidades.DetalleDocumento), ByVal objDocumento As Entidades.Documento, ByVal IdDocumentoOld As Integer, ByVal IdMetodoVal As Integer) As Integer
        'PROCEDIMIENTO INSERTA ORDEN DE DESPACHO Y ACTUALIZA CANT X ATENDER
        Return obj.InsertaOrdenDespacho(listaDetalle, objDocumento, IdDocumentoOld, IdMetodoVal)
    End Function
    Public Function SelectOrdenDespachoxIdSeriexCodigo(ByVal idserie As Integer, ByVal codigo As Integer) As Entidades.DocOrdenDespacho
        Return obj.SelectOrdenDespachoxIdSeriexCodigo(idserie, codigo)
    End Function
    Public Function SelectDetalleOrdenDespachoxIdDocumento(ByVal iddocumento As Integer) As List(Of Entidades.DetalleDocumento)
        Return obj.SelectDetalleOrdenDespachoxIdDocumento(iddocumento)
    End Function
    Public Function ActualizaOrdenDespacho(ByVal listaDetalle As List(Of Entidades.DetalleDocumento), ByVal objDocumento As Entidades.Documento) As Boolean
        Return obj.ActualizaOrdenDespacho(listaDetalle, objDocumento)
    End Function
    Public Function AnularxIdDocumento(ByVal iddocumento As Integer) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction

            obj.AnularxIdDocumento(iddocumento, cn, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function

End Class
