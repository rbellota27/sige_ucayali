﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 25-Set-2009
'Hora    : 09:00 pm

Public Class OrdenTrabajo

#Region "Variables"
    Dim objOT As DAO.DAOOrdenTrabajo
#End Region

#Region "Funciones"

    Public Function listarPersonaNaturalJuridica(ByVal nombre As String, ByVal dni As String, ByVal ruc As String, _
                                                 ByVal pageindex As Integer, ByVal pagesize As Integer) As List(Of Entidades.PersonaView)
        objOT = New DAO.DAOOrdenTrabajo
        Return objOT.listarPersonaNaturalJuridica(nombre, dni, ruc, pageindex, pagesize)
    End Function

    Public Function listarPersonaNaturalJuridicaX(ByVal idvehiculo As Integer, ByVal nombre As String, ByVal dni As String, ByVal ruc As String, _
                                                ByVal pageindex As Integer, ByVal pagesize As Integer) As List(Of Entidades.PersonaView)
        objOT = New DAO.DAOOrdenTrabajo
        Return objOT.listarPersonaNaturalJuridicaX(idvehiculo, nombre, dni, ruc, pageindex, pagesize)
    End Function

    Public Function llenarLineaxTipoExistencia(ByVal idexistencia As Integer) As List(Of Entidades.Linea)
        objOT = New DAO.DAOOrdenTrabajo
        Return objOT.llenarLineaxTipoExistencia(idexistencia)
    End Function

    Public Function listarLineaMarca(ByVal idlinea As Integer, ByVal mante As Integer) As List(Of Entidades.Marca)
        objOT = New DAO.DAOOrdenTrabajo
        Return objOT.listarLineaMarca(idlinea, mante)
    End Function

    Public Function listarModeloLineaMarca(ByVal idlineaMarca As Integer) As List(Of Entidades.ModeloLineaMarca)
        objOT = New DAO.DAOOrdenTrabajo
        Return objOT.listarModeloMarcaLinea(idlineaMarca)
    End Function

    Public Function inserta_vehiculoOT(ByVal obj As Entidades.VehiculoOt) As Integer
        objOT = New DAO.DAOOrdenTrabajo
        Return objOT.inserta_vehiculoOT(obj)
    End Function

    Public Function listarVehiculoxPlaca(ByVal placa As String) As Entidades.VehiculoOt
        objOT = New DAO.DAOOrdenTrabajo
        Return objOT.listarVehiculoxPlaca(placa)
    End Function

    Public Function listarDetalleServicio(ByVal idProducto As Integer, ByVal idtienda As Integer) As List(Of Entidades.Catalogo)
        objOT = New DAO.DAOOrdenTrabajo
        Return objOT.listarDetalleServicio(idProducto, idtienda)
    End Function

    Public Function insertarOrdenTrabajo(ByVal objDoc As Entidades.Documento, _
                                         ByVal objProdOt As List(Of Entidades.VehiculoOt), _
                                         ByVal obs As Entidades.Observacion, ByVal objFinanciamiento As Entidades.FinanciamientoOT) As Integer
        objOT = New DAO.DAOOrdenTrabajo
        Return objOT.insertarOrdenTrabajo(objDoc, objProdOt, obs, objFinanciamiento)
    End Function

    Public Function listarOrdenTrabajo(ByVal nomcli As String, ByVal doccli As String, ByVal placa As String, _
                                       ByVal nrodoc As String, ByVal idtienda As Integer, ByVal pageindex As Integer, _
                                       ByVal pagesize As Integer) As List(Of Entidades.OrdenTrabajoView)
        objOT = New DAO.DAOOrdenTrabajo
        Return objOT.listarOrdenTrabajo(nomcli, doccli, placa, nrodoc, idtienda, pageindex, pagesize)
    End Function

    Public Function listarOrdenTrabajo(ByVal idFinanciamientoOt As Integer) As Entidades.OrdenTrabajoView
        objOT = New DAO.DAOOrdenTrabajo
        Return objOT.listarOrdenTrabajo(idFinanciamientoOt)
    End Function

    Public Function listarProductoOT(ByVal idfinanciamientoOT As Integer) As List(Of Entidades.DetalleDocumentoView)
        objOT = New DAO.DAOOrdenTrabajo
        Return objOT.listarProductoOT(idfinanciamientoOT)
    End Function

    Public Function listarProductoOTL(ByVal idfinanciamientoOT As Integer, ByVal IdTienda As Integer, ByVal tipoPV As Integer) As List(Of Entidades.DetalleDocumentoView)
        objOT = New DAO.DAOOrdenTrabajo
        Return objOT.listarProductoOTL(idfinanciamientoOT, IdTienda, tipoPV)
    End Function

    Public Function listarOrdenTrabajoL(ByVal idtienda As Integer, ByVal pageindex As Integer, _
                                        ByVal pagesize As Integer) As List(Of Entidades.OrdenTrabajoView)
        objOT = New DAO.DAOOrdenTrabajo
        Return objOT.listarOrdenTrabajoL(idtienda, pageindex, pagesize)
    End Function

    Public Function ActualizarDocumento(ByVal objProductoOt As List(Of Entidades.VehiculoOt), _
                                        ByVal objObs As Entidades.Observacion, _
                                        ByVal objFinanciamientoOt As Entidades.FinanciamientoOT) As Boolean
        objOT = New DAO.DAOOrdenTrabajo
        Return objOT.ActualizarDocumento(objProductoOt, objObs, objFinanciamientoOt)
    End Function

    Function listarProductosLiquidacion(ByVal tipoPV As Integer, ByVal idtienda As Integer, ByVal idalmacen As Integer, _
                                        ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal descripcion As String, _
                                        ByVal codsublinea As Integer, ByVal pageIndex As Integer, _
                                        ByVal pageSize As Integer) As List(Of Entidades.Catalogo)
        objOT = New DAO.DAOOrdenTrabajo
        Return objOT.listarProductosLiquidacion(tipoPV, idtienda, idalmacen, idlinea, idsublinea, descripcion, codsublinea, pageIndex, pageSize)
    End Function

    Function insertarDetallesOrdenTrabajo(ByVal listProdusctos As List(Of Entidades.DetalleDocumento), _
                                          ByVal OBJDocumento As Entidades.Documento, ByVal moverAlmacen As Boolean, _
                                          ByVal comprometerStock As Boolean, ByVal operativo As Integer) As Boolean
        objOT = New DAO.DAOOrdenTrabajo
        Return objOT.insertarDetallesOrdenTrabajo(listProdusctos, OBJDocumento, moverAlmacen, comprometerStock, operativo)
    End Function


#End Region

End Class
