﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



'************************   MARTES 14 mayo 2010 HORA 05_51 PM







Imports System.Data.SqlClient

Public Class DocumentoOrdenPedidoSucursal

    Private cn As SqlConnection
    Private tr As SqlTransaction
    Private objDaoDOP As New DAO.DAOOrdenPedidoSucursal


    Public Function DetalleDocumento_Ref(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)
        Return objDaoDOP.DetalleDocumento_Ref(IdDocumento)
    End Function

    Public Function Documento_BuscarDocumentoRef(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPersona As Integer, ByVal IdTipoDocumento As Integer, ByVal Serie As Integer, ByVal Codigo As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, _
                                                ByVal IdtipoDocumentoRef As Integer, ByVal IdTipoOperacion As Integer) As List(Of Entidades.DocumentoView)
        Return objDaoDOP.Documento_BuscarDocumentoRef(IdEmpresa, IdTienda, IdPersona, IdTipoDocumento, Serie, Codigo, FechaInicio, FechaFin, IdtipoDocumentoRef, IdTipoOperacion)
    End Function
    Public Function Documento_BuscarDocumentoRef2(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPersona As Integer, ByVal IdTipoDocumento As Integer, ByVal Serie As Integer, ByVal Codigo As String, ByVal FechaInicio As Date, ByVal FechaFin As Date, _
                                                ByVal IdtipoDocumentoRef As Integer, ByVal IdTipoOperacion As Integer) As List(Of Entidades.DocumentoView)
        Return objDaoDOP.Documento_BuscarDocumentoRef2(IdEmpresa, IdTienda, IdPersona, IdTipoDocumento, Serie, Codigo, FechaInicio, FechaFin, IdtipoDocumentoRef, IdTipoOperacion)
    End Function


    Public Sub DocumentoOrdenPedidoSucursal_ValDocumentoRef(ByVal IdDocumentoRef As Integer)
        objDaoDOP.DocumentoOrdenPedidoSucursal_ValDocumentoRef(IdDocumentoRef)
    End Sub

    Public Function aprobarCantidadesOP(ByVal IdDocumento As Integer, ByVal listaAnexoDetalleDocumento As List(Of Entidades.Anexo_DetalleDocumento)) As Boolean

        Dim hecho As Boolean = False

        Try
            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            '********************  DESHACEMOS LOS CAMBIOS PREVIAMENTE EFECTUADOS
            objDaoDOP.DocumentoOrdenPedido_DeshacerCantidadAprobada(IdDocumento, cn, tr)

            '******************** INSERTAMOS LOS NUEVOS CAMBIOS (CANTIDADES)
            For i As Integer = 0 To listaAnexoDetalleDocumento.Count - 1

                objDaoDOP.DocumentoOrdenPedido_AprobarCantidadOPInsert(listaAnexoDetalleDocumento(i).IdDetalleDocumento, listaAnexoDetalleDocumento(i).CantidadAprobada, cn, tr)

            Next


            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function

    Public Function DocumentoOrdenPedidoSucursal_SelectDetalle_Aprobacion_DT(ByVal IdDocumento As Integer) As DataTable
        Return objDaoDOP.DocumentoOrdenPedidoSucursal_SelectDetalle_Aprobacion_DT(IdDocumento)
    End Function

    Public Function DocumentoOrdenPedido_SelectxParams_Aprobacion(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal EstadoAprob As Boolean, ByVal Serie As Integer, ByVal Codigo As Integer) As DataTable
        Return objDaoDOP.DocumentoOrdenPedido_SelectxParams_Aprobacion(IdEmpresa, IdAlmacen, FechaInicio, FechaFin, EstadoAprob, Serie, Codigo)
    End Function

    Public Function DocumentoOrdenPedidoSucursal_SelectDetalle(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)
        Return objDaoDOP.DocumentoOrdenPedidoSucursal_SelectDetalle(IdDocumento)
    End Function


    Private Sub AgregarComponente(ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal comprometerStock As Boolean, ByVal listaAnexoDetalleDocumento As List(Of Entidades.Anexo_DetalleDocumento))

        For i As Integer = 0 To listaDetalleDocumento.Count - 1

            If (listaDetalleDocumento(i).Kit) Then  '*********** KIT

                '****************** OBTENEMOS EL DETALLE DOCUMENTO
                Dim listaComponenteKit As List(Of Entidades.Kit) = (New Negocio.Kit).SelectComponenteDetallexParams(listaDetalleDocumento(i).IdProducto, listaDetalleDocumento(i).IdUnidadMedida, listaDetalleDocumento(i).Cantidad, Nothing, Nothing, Nothing)

                For j As Integer = 0 To listaComponenteKit.Count - 1

                    Dim objDetalleDocumento As New Entidades.DetalleDocumento
                    With objDetalleDocumento

                        .ComponenteKit = True
                        .IdDocumento = listaDetalleDocumento(i).IdDocumentoRef

                        .IdProducto = listaComponenteKit(j).IdComponente
                        .IdUnidadMedida = listaComponenteKit(j).IdUnidadMedida_Comp
                        .Cantidad = listaComponenteKit(j).CantidadTotal_Comp
                        .UMedida = listaComponenteKit(j).UnidadMedida_Comp

                    End With

                    '******************** AGREGAMOS EL COMPONENTE AL DETALLE
                    listaDetalleDocumento.Add(objDetalleDocumento)

                    If comprometerStock Then

                        Dim objAnexo_DetalleDocumento As New Entidades.Anexo_DetalleDocumento
                        With objAnexo_DetalleDocumento
                            .ComponenteKit = True
                            .IdKit = listaDetalleDocumento(i).IdProducto
                            .CantidadAprobada = listaDetalleDocumento(i).Cantidad
                        End With

                        listaAnexoDetalleDocumento.Add(objAnexo_DetalleDocumento)

                    End If

                Next



            End If


        Next


    End Sub

    Public Function registrarDocumento(ByVal objDocumento As Entidades.Documento, ByVal objAnexoDocumento As Entidades.Anexo_Documento, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal listaAnexoDetalleDocumento As List(Of Entidades.Anexo_DetalleDocumento), ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal objObservaciones As Entidades.Observacion, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento), ByVal moverAlmacen As Boolean, ByVal comprometerStock As Boolean, ByVal IdAlmacenPartida As Integer) As Integer


        Try
            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction


            '****************** INSERTO LA CABECERA
            objDocumento.Id = (New DAO.DAODocumento).InsertaDocumento(cn, objDocumento, tr)

            '***************** INSERTO EN ANEXO DOCUMENTO
            Dim objDaoAnexoDocumento As New DAO.DAOAnexo_Documento
            objAnexoDocumento.IdDocumento = objDocumento.Id
            objDaoAnexoDocumento.InsertAnexo_Documento(objAnexoDocumento, cn, tr, False)

            '******************** INSERTO EL DETALLE
            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            AgregarComponente(listaDetalleDocumento, comprometerStock, listaAnexoDetalleDocumento)
            objDaoDetalleDocumento.InsertaDetalleDocumento_MovAlmacen(cn, listaDetalleDocumento, tr, objDocumento.Id, moverAlmacen, comprometerStock, -1, IdAlmacenPartida)

            '***************** INSERTO EN ANEXO DETALLE DOCUMENTO
            Dim objDaoAnexoDetalleD As New DAO.DAOAnexo_DetalleDocumento
            For i As Integer = 0 To listaAnexoDetalleDocumento.Count - 1

                listaAnexoDetalleDocumento(i).IdDocumento = objDocumento.Id
                listaAnexoDetalleDocumento(i).IdDetalleDocumento = listaDetalleDocumento(i).IdDetalleDocumento

                objDaoAnexoDetalleD._Anexo_DetalleDocumentoInsert(listaAnexoDetalleDocumento(i), cn, tr)

            Next

            '********************* INSERTO EN PUNTO PARTIDA
            If (objPuntoPartida IsNot Nothing) Then
                Dim objDaoPuntoPartida As New DAO.DAOPuntoPartida
                objPuntoPartida.IdDocumento = objDocumento.Id
                objDaoPuntoPartida.InsertaPuntoPartida(objPuntoPartida, cn, tr)
            End If

            '********************* INSERTO EN PUNTO LLEGADA
            If (objPuntoLlegada IsNot Nothing) Then
                Dim objDaoPuntoLlegada As New DAO.DAOPuntoLlegada
                objPuntoLlegada.IdDocumento = objDocumento.Id
                objDaoPuntoLlegada.InsertaPuntoLlegadaT(cn, objPuntoLlegada, tr)
            End If

            '*********************** INSERTO EN RELACION DOCUMENTO
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1
                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            Next

            '********************** INSERTO OBSERVACIONES
            If (objObservaciones IsNot Nothing) Then
                Dim objDaoObservaciones As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservaciones.InsertaObservacionT(cn, tr, objObservaciones)
            End If

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function
    Public Function actualizarDocumento(ByVal objDocumento As Entidades.Documento, ByVal objAnexoDocumento As Entidades.Anexo_Documento, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal listaAnexoDetalleDocumento As List(Of Entidades.Anexo_DetalleDocumento), ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal objObservaciones As Entidades.Observacion, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento), ByVal moverAlmacen As Boolean, ByVal comprometerStock As Boolean, ByVal IdAlmacenPartida As Integer) As Integer

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            '*********************** DESHACER MOV
            objDaoDOP.DocumentoOrdenPedidoSucursal_DeshacerMov(objDocumento.Id, True, True, True, True, True, True, True, True, False, cn, tr)


            '****************** ACTUALIZAMOS LA CABECERA
            Dim objDaoDocumento As New DAO.DAODocumento
            objDaoDocumento.DocumentoUpdate(objDocumento, cn, tr)

            '***************** INSERTO EN ANEXO DOCUMENTO
            Dim objDaoAnexoDocumento As New DAO.DAOAnexo_Documento
            objAnexoDocumento.IdDocumento = objDocumento.Id
            objDaoAnexoDocumento.InsertAnexo_Documento(objAnexoDocumento, cn, tr, False)

            '******************** INSERTO EL DETALLE
            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            AgregarComponente(listaDetalleDocumento, comprometerStock, listaAnexoDetalleDocumento)
            objDaoDetalleDocumento.InsertaDetalleDocumento_MovAlmacen(cn, listaDetalleDocumento, tr, objDocumento.Id, moverAlmacen, comprometerStock, -1, IdAlmacenPartida)

            '***************** INSERTO EN ANEXO DETALLE DOCUMENTO
            Dim objDaoAnexoDetalleD As New DAO.DAOAnexo_DetalleDocumento
            For i As Integer = 0 To listaAnexoDetalleDocumento.Count - 1

                listaAnexoDetalleDocumento(i).IdDocumento = objDocumento.Id
                listaAnexoDetalleDocumento(i).IdDetalleDocumento = listaDetalleDocumento(i).IdDetalleDocumento

                objDaoAnexoDetalleD._Anexo_DetalleDocumentoInsert(listaAnexoDetalleDocumento(i), cn, tr)

            Next

            '********************* INSERTO EN PUNTO PARTIDA
            If (objPuntoPartida IsNot Nothing) Then
                Dim objDaoPuntoPartida As New DAO.DAOPuntoPartida
                objPuntoPartida.IdDocumento = objDocumento.Id
                objDaoPuntoPartida.InsertaPuntoPartida(objPuntoPartida, cn, tr)
            End If

            '********************* INSERTO EN PUNTO LLEGADA
            If (objPuntoLlegada IsNot Nothing) Then
                Dim objDaoPuntoLlegada As New DAO.DAOPuntoLlegada
                objPuntoLlegada.IdDocumento = objDocumento.Id
                objDaoPuntoLlegada.InsertaPuntoLlegadaT(cn, objPuntoLlegada, tr)
            End If

            '*********************** INSERTO EN RELACION DOCUMENTO
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1
                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            Next

            '********************** INSERTO OBSERVACIONES
            If (objObservaciones IsNot Nothing) Then
                Dim objDaoObservaciones As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservaciones.InsertaObservacionT(cn, tr, objObservaciones)
            End If

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function
    Public Function anularDocumento(ByVal iddocumento As Integer) As Boolean

        Dim hecho As Boolean = False
        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            objDaoDOP.DocumentoOrdenPedidoSucursal_DeshacerMov(iddocumento, False, True, False, False, False, False, True, True, True, cn, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function

    Public Function ReporteAprobacionPedido(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idalmacenDA As Integer, ByVal iddocumento As Integer, _
                     ByVal FechaInicio As String, ByVal Fechafin As String, ByVal yeari As Integer, ByVal semanai As Integer, _
ByVal yearf As Integer, ByVal semanaF As Integer, _
ByVal FiltraSemana As Integer, ByVal stocktransitoxtienda As Integer, ByVal tpAlmacen As DataTable) As DataSet


        Return objDaoDOP.ReporteAprobacionPedido(idempresa, idtienda, idalmacenDA, iddocumento, FechaInicio, Fechafin, yeari, semanai, yearf, semanaF, FiltraSemana, stocktransitoxtienda, tpAlmacen)


    End Function

    Public Function OrdenPedidoAprobacion(ByVal idempresa As Integer, ByVal iddocumento As Integer, _
                        ByVal FechaInicio As String, ByVal Fechafin As String, _
                        ByVal yeari As Integer, ByVal semanai As Integer, ByVal yearf As Integer, ByVal semanaF As Integer, _
                        ByVal FiltraSemana As Integer, ByVal tpAlmacen As DataTable) As DataSet

        Return objDaoDOP.OrdenPedidoAprobacion(idempresa, iddocumento, FechaInicio, Fechafin, yeari, semanai, yearf, semanaF, FiltraSemana, tpAlmacen)

    End Function



End Class
