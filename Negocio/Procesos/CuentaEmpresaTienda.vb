﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class CuentaEmpresaTienda

    Dim obj As New DAO.DAOCuentaEmpresaTienda

    Function SelectCuentaEmpresaTienda(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Idmoneda As Integer, ByVal estado As Integer) As List(Of Entidades.CuentaEmpresaTienda)
        Return obj.SelectCuentaEmpresaTienda(IdEmpresa, IdTienda, Idmoneda, estado)
    End Function
    Function SelectxIdEmpresaxIdTiendaxIdMoneda(ByVal IdEmpresa As Integer, _
    ByVal IdTienda As Integer, ByVal IdMoneda As Integer) As Entidades.CuentaEmpresaTienda
        Return obj.SelectxIdEmpresaxIdTiendaxIdMoneda(IdEmpresa, IdTienda, IdMoneda)
    End Function
    Public Function InsertarCuentaEmpresaTienda(ByVal lista As List(Of Entidades.CuentaEmpresaTienda), ByVal idsupervisor As Integer) As Boolean
        Return obj.InsertarCuentaEmpresaTienda(lista, idsupervisor)
    End Function

    Public Function UpdateCuentaEmpresaTienda(ByVal lista As List(Of Entidades.CuentaEmpresaTienda), ByVal idsupervisor As Integer) As Boolean

        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE

        cn.Open()

        Dim tr As SqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)

        Try

            For Each eCuentaEmpresaTienda As Entidades.CuentaEmpresaTienda In lista

                obj.UpdateCuentaEmpresaTienda(eCuentaEmpresaTienda, idsupervisor, cn, tr)

            Next

            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

        Return True

    End Function


End Class
