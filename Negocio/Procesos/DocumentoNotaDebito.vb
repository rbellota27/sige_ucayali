﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



'***********************  JUEVES 04 MARZO 2010 HORA 03_41 PM




Imports System.Data.SqlClient
Public Class DocumentoNotaDebito
    Private objDaoDocumentoNotaDebito As New DAO.DAODocumentoNotaDebito
    Private objConexion As New DAO.Conexion


    Public Function getDataSetDocumentoPrint(ByVal IdDocumento As Integer) As DataSet

        Dim ds As New DataSet
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Try

            Dim cmd As New SqlCommand("_CR_DocumentoNotaDebitoCab", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocumentoNotaDebitoCab")

            cmd = New SqlCommand("_CR_DocumentoNotaDebitoDet", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocumentoNotaDebitoDet")

        Catch ex As Exception
            Throw ex
        End Try

        Return ds

    End Function


    Public Function DocumentoNotaDebitoSelectCab(ByVal IdSerie As Integer, ByVal Codigo As Integer, ByVal IdDocumento As Integer) As Entidades.DocumentoNotaDebito
        Return objDaoDocumentoNotaDebito.DocumentoNotaDebitoSelectCab(IdSerie, Codigo, IdDocumento)
    End Function

    Public Function SelectDocReferenciaxParams(ByVal IdCliente As Integer, ByVal FechaI As Date, ByVal FechaF As Date, ByVal IdTienda As Integer, ByVal serie As Integer, ByVal codigo As Integer, ByVal opcion As Integer) As List(Of Entidades.DocumentoView)
        Return objDaoDocumentoNotaDebito.SelectDocReferenciaxParams(IdCliente, FechaI, FechaF, IdTienda, serie, codigo, opcion)
    End Function

    Public Function RegistrarDocumentoNotaDebito(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento), ByVal objObservacion As Entidades.Observacion, ByVal objMovCuenta As Entidades.MovCuenta) As Integer

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction

            '*************** Insertamos la cabecera
            Dim objDaoDocumento As New DAO.DAODocumento
            objDocumento.Id = objDaoDocumento.InsertaDocumento(cn, objDocumento, tr)

            '************* Insertamos el detalle
            Dim objDaoDetalleConcepto As New DAO.DAODetalleConcepto
            For i As Integer = 0 To listaDetalleConcepto.Count - 1
                listaDetalleConcepto(i).IdDocumento = objDocumento.Id
                objDaoDetalleConcepto.DetalleConceptoInsert(listaDetalleConcepto(i), cn, tr, False)
            Next

            '************* Insertamos Relacion Documento
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1
                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            Next

            '************** Insertamos Observacion
            If (objObservacion IsNot Nothing) Then
                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservacion.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservacion)
            End If

            '*************** Insertamos Mov Cuenta
            If (objMovCuenta IsNot Nothing) Then
                Dim objDaoMovCuenta As New DAO.DAOMovCuenta
                objMovCuenta.IdDocumento = objDocumento.Id
                objDaoMovCuenta.InsertaMovCuenta(cn, objMovCuenta, tr)
            End If
            Dim id As Integer = 0
            Dim obj As New DAO.DAODocumento
            id = objDocumento.Id
            tr.Commit()
            obj.crearFacturaElectronica(id, cn)
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function

    Public Function AnularDocumentoNotaDebito(ByVal IdDocumento As Integer) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try

            cn.Open()
            tr = cn.BeginTransaction

            '******************* DESHACEMOS LOS MOVIMIENTOS
            objDaoDocumentoNotaDebito.DocumentoNotaDebito_DeshacerMov(IdDocumento, False, True, True, True, False, cn, tr)

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho
    End Function

    Public Function ActualizarDocumentoNotaDebito(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento), ByVal objObservacion As Entidades.Observacion, ByVal objMovCuenta As Entidades.MovCuenta) As Integer

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction

            '******************* DESHACEMOS LOS MOVIMIENTOS
            objDaoDocumentoNotaDebito.DocumentoNotaDebito_DeshacerMov(objDocumento.Id, True, True, True, False, True, cn, tr)

            '*************** Actualizamos la cabecera
            Dim objDaoDocumento As New DAO.DAODocumento
            objDaoDocumento.DocumentoUpdate(objDocumento, cn, tr)

            '************* Insertamos el detalle
            Dim objDaoDetalleConcepto As New DAO.DAODetalleConcepto
            For i As Integer = 0 To listaDetalleConcepto.Count - 1
                listaDetalleConcepto(i).IdDocumento = objDocumento.Id
                objDaoDetalleConcepto.DetalleConceptoInsert(listaDetalleConcepto(i), cn, tr, False)
            Next

            '************* Insertamos Relacion Documento
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1
                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            Next

            '************** Insertamos Observacion
            If (objObservacion IsNot Nothing) Then
                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservacion.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservacion)
            End If

            '*************** Insertamos Mov Cuenta
            If (objMovCuenta IsNot Nothing) Then
                Dim objDaoMovCuenta As New DAO.DAOMovCuenta
                objMovCuenta.IdDocumento = objDocumento.Id
                objDaoMovCuenta.InsertaMovCuenta(cn, objMovCuenta, tr)
            End If


            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function



End Class
