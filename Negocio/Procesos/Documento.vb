﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.






'************************   MARTES 2501 2011 HORA 03_08 PM



'DetalleDocumentoSelectDetallexAtenderxIdDocumento

Imports System.Data.SqlClient
Public Class Documento

    Dim obj As New DAO.DAODocumento
    Dim objConexion As New DAO.Conexion
    Public Function VerificandoTiendaOPVP(ByVal IdTienda As Integer) As Integer
        Return obj.VerificandoTiendaOPVP(IdTienda)
    End Function
    Public Function SelectDetallexIdPase(ByVal IdPase As Integer) As List(Of Entidades.Documento)
        Return obj.SelectDetallexIdPase(IdPase)
    End Function
    Public Function EliminarDocumentoxIdDocumento(ByVal IdDocumento As String) As Boolean
        Return obj.EliminarDocumentoxIdDocumento(IdDocumento)
    End Function
    Public Function SelectPasesxIdEmpresa(ByVal IdEmpresa As Integer) As List(Of Entidades.Pase)
        Return obj.SelectPasesxIdEmpresa(IdEmpresa)
    End Function
    Public Function ValidarDocExterno(ByVal serie As String, ByVal codigo As String, ByVal IdTipoDocumento As Integer, ByVal IdPersona As Integer) As Entidades.Documento
        Return obj.ValidarDocExterno(serie, codigo, IdTipoDocumento, IdPersona)
    End Function
    Public Function SelectReporteDocExterno(ByVal serie As Integer, ByVal codigo As Integer, ByVal fechainicio As Date, ByVal fechafin As Date, ByVal IdTienda As Integer) As List(Of Entidades.Documento)
        Return obj.SelectReporteDocExterno(serie, codigo, fechainicio, fechafin, IdTienda)
    End Function
    Public Function SelectDocExternoxCodigoSerie(ByVal serie As String, ByVal codigo As String, ByVal fechainicio As Date, ByVal fechafin As Date, ByVal IdPersona As Integer) As List(Of Entidades.Documento)
        Return obj.SelectDocExternoxCodigoSerie(serie, codigo, fechainicio, fechafin, IdPersona)
    End Function
    Public Function SelectDocEOCxCodigoSerie(ByVal serie As String, ByVal codigo As String, ByVal fechainicio As Date, ByVal fechafin As Date, ByVal IdPersona As Integer) As List(Of Entidades.Documento)
        Return obj.SelectDocEOCxCodigoSerie(serie, codigo, fechainicio, fechafin, IdPersona)
    End Function
    Public Function SelectDocCancelacionBxIdRQ(ByVal IdDocumento As String) As List(Of Entidades.Documento)
        Return obj.SelectDocCancelacionBxIdRQ(IdDocumento)
    End Function

    Public Function SelectReciboEgresoxIdRQ(ByVal IdDocumento As String) As List(Of Entidades.Documento)
        Return obj.SelectReciboEgresoxIdRQ(IdDocumento)
    End Function

    Public Function SelectDocExtxIdRQinProgPago(ByVal IdDocumento As Integer) As List(Of Entidades.Documento)
        Return obj.SelectDocExtxIdRQinProgPago(IdDocumento)
    End Function
    Public Function SelectPermisosRQxIdPersona(ByVal IdUsuario As Integer) As Integer
        Return obj.SelectPermisosRQxIdPersona(IdUsuario)
    End Function
    Public Function SelectDocumentotoInsertRQ(ByVal IdDocumentoRef As Integer) As Entidades.Documento
        Return obj.SelectDocumentotoInsertRQ(IdDocumentoRef)
    End Function

    Public Function DocumentoOCSelect(ByVal objx As Entidades.Documento) As DataTable
        Return obj.DocumentoOCSelectDT(objx)
    End Function

    Public Function SelectDocumentoOCxIdDocRef(ByVal IdDocumentoRef As Integer) As Entidades.Documento
        Return obj.SelectDocumentoOCxIdDocRef(IdDocumentoRef)
    End Function
    Public Function DocumentoOCSelectList(ByVal serie As Integer, ByVal codigo As Integer, ByVal fechaini As Date, ByVal fechafin As Date) As List(Of Entidades.Documento)
        Return obj.DocumentoOCSelect(serie, codigo, fechaini, fechafin)
    End Function


    Public Function Documento_BuscarxTraza(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPersona As Integer, ByVal IdTipoDocumento As Integer, ByVal Serie As Integer, ByVal Codigo As String, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal val_FechaVcto As Boolean, ByVal PageIndex As Integer, ByVal PageSize As Integer, ByVal IdEstadoEnt As Integer, ByVal IdEstadoCan As Integer) As List(Of Entidades.DocumentoView)
        Return obj.Documento_BuscarxTraza(IdEmpresa, IdTienda, IdPersona, IdTipoDocumento, Serie, Codigo, FechaInicio, FechaFin, val_FechaVcto, PageIndex, PageSize, IdEstadoEnt, IdEstadoCan)
    End Function


    Public Function Documento_BuscarDocumentoRef_Externo(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPersona As Integer, ByVal IdTipoDocumento As Integer, ByVal Serie As String, _
                                                ByVal Codigo As String, ByVal IdTipoDocumentoRef As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, _
                                                ByVal val_FechaVcto As Boolean, _
                                                Optional ByVal val_CotizacionCanjeUnico As Boolean = False) As List(Of Entidades.DocumentoView)
        Return obj.Documento_BuscarDocumentoRef_Externo(IdEmpresa, IdTienda, IdPersona, IdTipoDocumento, Serie, Codigo, FechaInicio, FechaFin, val_FechaVcto, val_CotizacionCanjeUnico, IdTipoDocumentoRef)
    End Function
    Public Function SelectxIdTipoDocumentoxIdPersonaxSeriexCodigoExt(ByVal IdTipoDocumento As Integer, ByVal IdPersona As Integer, ByVal Serie As String, ByVal Codigo As String, ByVal pageIndex As Integer, ByVal pageSize As Integer) As List(Of Entidades.Documento)
        Return obj.SelectxIdTipoDocumentoxIdPersonaxSeriexCodigoExt(IdTipoDocumento, IdPersona, Serie, Codigo, pageIndex, pageSize)
    End Function
    Public Function SelectxIdTipoDocumentoxIdPersonaxSeriexCodigo(ByVal IdTipoDocumento As Integer, ByVal IdPersona As Integer, ByVal Serie As Integer, ByVal Codigo As Integer) As List(Of Entidades.Documento)
        Return obj.SelectxIdTipoDocumentoxIdPersonaxSeriexCodigo(IdTipoDocumento, IdPersona, Serie, Codigo)
    End Function
    Public Function Documento_BuscarDocumentoRef(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPersona As Integer, ByVal IdTipoDocumento As Integer, ByVal Serie As Integer, _
                                                 ByVal Codigo As Integer, ByVal IdTipoDocumentoRef As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, _
                                                 ByVal val_FechaVcto As Boolean, Optional ByVal val_CotizacionCanjeUnico As Boolean = False, Optional ByVal IdTipoOperacion As Integer = 0) As List(Of Entidades.DocumentoView)
        Return obj.Documento_BuscarDocumentoRef(IdEmpresa, IdTienda, IdPersona, IdTipoDocumento, Serie, Codigo, FechaInicio, FechaFin, val_FechaVcto, val_CotizacionCanjeUnico, IdTipoDocumentoRef, IdTipoOperacion)
    End Function
    Public Function Documento_BuscarDocumentoRef2(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPersona As Integer, ByVal IdTipoDocumento As Integer, ByVal Serie As Integer, _
                                               ByVal Codigo As String, ByVal IdTipoDocumentoRef As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, _
                                               ByVal val_FechaVcto As Boolean, Optional ByVal val_CotizacionCanjeUnico As Boolean = False, Optional ByVal IdTipoOperacion As Integer = 0) As List(Of Entidades.DocumentoView)
        Return obj.Documento_BuscarDocumentoRef2(IdEmpresa, IdTienda, IdPersona, IdTipoDocumento, Serie, Codigo, FechaInicio, FechaFin, val_FechaVcto, val_CotizacionCanjeUnico, IdTipoDocumentoRef, IdTipoOperacion)
    End Function
    Public Function _Documento_BuscarDocumentoRefDocExterno(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPersona As Integer, ByVal IdTipoDocumento As Integer, ByVal Serie As Integer, _
                                                ByVal Codigo As Integer, ByVal IdTipoDocumentoRef As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, _
                                                ByVal val_FechaVcto As Boolean, Optional ByVal val_CotizacionCanjeUnico As Boolean = False, Optional ByVal IdTipoOperacion As Integer = 0) As List(Of Entidades.DocumentoView)
        Return obj._Documento_BuscarDocumentoRefDocExterno(IdEmpresa, IdTienda, IdPersona, IdTipoDocumento, Serie, Codigo, FechaInicio, FechaFin, val_FechaVcto, val_CotizacionCanjeUnico, IdTipoDocumentoRef, IdTipoOperacion)
    End Function
    Public Function Documento_Buscar(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPersona As Integer, ByVal IdTipoDocumento As Integer, ByVal Serie As Integer, ByVal Codigo As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal val_FechaVcto As Boolean, Optional ByVal IdEstadoDoc As Integer = 1) As List(Of Entidades.DocumentoView)
        Return obj.Documento_Buscar(IdEmpresa, IdTienda, IdPersona, IdTipoDocumento, Serie, Codigo, FechaInicio, FechaFin, val_FechaVcto, IdEstadoDoc)
    End Function
    Public Function DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(ByVal IdTipoDocumento As Integer, ByVal IdSerie As Integer, ByVal IdPersona As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date) As List(Of Entidades.DocumentoView)
        Return obj.DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(IdTipoDocumento, IdSerie, IdPersona, FechaInicio, FechaFin)
    End Function
    Public Function AutoGenerarDocumentos_Salteados(ByVal IdSerie As Integer, ByVal CodigoInicio As Integer, ByVal CodigoFin As Integer, ByVal IdUsuario As Integer) As Boolean
        Return obj.AutoGenerarDocumentos_Salteados(IdSerie, CodigoInicio, CodigoFin, IdUsuario)
    End Function
    Public Function GenerarPaseVentasCompras(ByVal FechaInicioPase As String, ByVal FechaFinPase As String, ByVal Anio As String, ByVal IdUsuario As Integer) As Boolean
        Return obj.GenerarPaseVentasCompras(FechaInicioPase, FechaFinPase, Anio, IdUsuario)
    End Function
    Public Function VerificandoHistorialPases(ByVal FechaInicioPase As String, ByVal FechaFinPase As String, ByVal Anio As String) As Integer
        Return obj.VerificandoHistorialPases(FechaInicioPase, FechaFinPase, Anio)
    End Function
    Public Function VerificandoSinPasar(ByVal FechaInicioPase As String, ByVal FechaFinPase As String, ByVal Anio As String) As List(Of Entidades.Documento)
        Return obj.VerificandoSinPasar(FechaInicioPase, FechaFinPase, Anio)
    End Function
    Public Function GenerarNroDocumentoxIdSerie(ByVal IdSerie As Integer) As String
        Return obj.GenerarNroDocumentoxIdSerie(IdSerie)
    End Function
    Public Function registrarDocumentoVenta(ByVal objDocumento As Entidades.Documento, ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal objMovCaja As Entidades.MovCaja, ByVal objMovCuenta As Entidades.MovCuenta, ByVal listaCancelacion As List(Of Entidades.PagoCaja), ByVal listaVuelto As List(Of Entidades.PagoCaja), ByVal moverAlmacen As Boolean, ByVal comprometerStock As Boolean, ByVal separarAfectos_NoAfectos As Boolean, ByVal saltarCorrelativo As Boolean, ByVal separarxMontos As Boolean, ByVal modo As Integer, ByVal vistaPrevia As Boolean, ByVal comprometerPercepcion As Boolean, ByVal generarDespacho As Boolean) As List(Of Entidades.Documento)

        '************************* Valor Utilizado como default = true (caso Indusferr , False Caso Los Pinos)
        '************* Este valor no debe ser modificado, solo cuando se trate de Los Pinos
        Dim cobrarPercepcion As Boolean = Not comprometerPercepcion

        '***************** Valido si el Propietario es Agente PERCEPTOR
        If (New Negocio.Util).ValidarxDosParametros("PersonaTipoAgente", "IdPersona", CStr(objDocumento.IdEmpresa), "IdAgente", "1") <= 0 Then

            '************ NO es agente Percepto / caso Los Pinos
            saltarCorrelativo = False
            separarAfectos_NoAfectos = False
            cobrarPercepcion = False

        End If

        '****************** Declaramos la lista de documentos a registrar
        Dim listaDocumento As New List(Of Entidades.Documento)

        '***************** Declaramos variables de acceso a la BD
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim objDAODetalleDocumento As New DAO.DAODetalleDocumento
        Dim objDAODocumento As New DAO.DAODocumento

        Try            
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            '***************** Declaramos una lista que contendra las listas de los 
            '***************** detalles de los documentos a registrar
            Dim lista_listaDetalleDocumento As List(Of List(Of Entidades.DetalleDocumento))

            '****************** Realizamos la separacion por AFECTOS vs NO AFECTOS
            lista_listaDetalleDocumento = separarListaDetalleDocumento_Afecto_NoAfecto(listaDetalleDocumento, separarAfectos_NoAfectos)

            '************************ Ordenamos la lista_listaDetalleDocumento x importe
            lista_listaDetalleDocumento = ordenarListaDetalleDocumento(lista_listaDetalleDocumento)

            '****************** Realizamos la separación por Monto Maximo y Nro Filas del detalle
            lista_listaDetalleDocumento = separarListaDetalleDocumentoxMontoxNroFilasDetalleDoc(separarxMontos, lista_listaDetalleDocumento, objDocumento.IdTipoDocumento, objDocumento.IdEmpresa, objDocumento.IdMoneda)

            '****************** Hallamos los documentos junto con sus totales correspondientes
            listaDocumento = obtenerListaDocumentos(objDocumento, saltarCorrelativo, lista_listaDetalleDocumento, cobrarPercepcion)

            '********************** SI ES VISTA PREVIA
            If vistaPrevia Then

                Return obtenerListaVistaPrevia(listaDocumento)

            End If

            '***************** Obtenemos la ListaPagoCaja por Documento
            Dim lista_listaPagoCajaNew As List(Of List(Of Entidades.PagoCaja)) = obtenerListaPagoCajaxDocumento(listaDocumento, listaCancelacion, listaVuelto)

            '****************** Iniciamos el registro en la Base de Datos

            Select Case modo
                Case 1  '************ NUEVO
                    '******************** Insertamos los Documentos
                    For i As Integer = 0 To listaDocumento.Count - 1
                        listaDocumento(i).Id = objDAODocumento.InsertaDocumento_Venta(cn, listaDocumento(i), tr, True, comprometerPercepcion)
                    Next

                Case 2  '************* EDITAR
                    If listaDocumento.Count <> 1 Or lista_listaDetalleDocumento.Count <> 1 Then
                        Throw New Exception("SE HAN GENERADO MAS DE UN DOCUMENTO PARA EDICION")
                    End If
                    '********** Actualizo la cabecera
                    listaDocumento(0).CompPercepcion = comprometerPercepcion
                    objDAODocumento.DocumentoUpdate(listaDocumento(0), cn, tr)
                    '**************** DESHACER MOVIMIENTOS
                    objDAODocumento.DocumentoVenta_DeshacerMov(listaDocumento(0).Id, True, True, True, True, True, True, True, False, cn, tr)
            End Select
            '********************* Insertamos los detalles
            For i As Integer = 0 To lista_listaDetalleDocumento.Count - 1
                Dim salto As Integer = CInt(IIf(saltarCorrelativo = True, 2, 1))
                Dim ObjDocumentoAux As Entidades.Documento = listaDocumento(i * salto)
                objDAODetalleDocumento.InsertaDetalleDocumento_MovAlmacen(cn, lista_listaDetalleDocumento(i), tr, ObjDocumentoAux.Id _
                                                                          , moverAlmacen, comprometerStock, ObjDocumentoAux.FactorMov, ObjDocumentoAux.IdAlmacen)
                '***************** Insertamos el punto de llegada
                If objPuntoLlegada IsNot Nothing Then
                    Dim objDaoPuntoLlegada As New DAO.DAOPuntoLlegada
                    objPuntoLlegada.IdDocumento = ObjDocumentoAux.Id
                    objDaoPuntoLlegada.InsertaPuntoLlegadaT(cn, objPuntoLlegada, tr)
                End If
                '***************** Agregar EL Monto del Regimen: Percepción,Detracción, Retención, etc
                If ObjDocumentoAux.Percepcion > 0 Then
                    '********* 1 : Percepcion // Tabla RegimenOperacion
                    Dim objMontoRegimen As New Entidades.MontoRegimen(ObjDocumentoAux.Id, ObjDocumentoAux.Percepcion, 1)
                    Dim objDAOMontoRegimen As New DAO.DAOMontoRegimen
                    objDAOMontoRegimen.InsertaMontoRegimen(cn, objMontoRegimen, tr)
                End If

                If ObjDocumentoAux.Retencion > 0 Then
                    '********* 3 : Retencion // Tabla RegimenOperacion
                    Dim objMontoRegimen As New Entidades.MontoRegimen(ObjDocumentoAux.Id, ObjDocumentoAux.Retencion, 3)
                    Dim objDAOMontoRegimen As New DAO.DAOMontoRegimen
                    objDAOMontoRegimen.InsertaMontoRegimen(cn, objMontoRegimen, tr)
                End If

                '**************** Agrega el Movimiento de Caja
                If objMovCaja IsNot Nothing Then

                    objMovCaja.IdDocumento = ObjDocumentoAux.Id
                    Dim objDAOMovCaja As New DAO.DAOMovCaja
                    objDAOMovCaja.InsertaMovCaja(cn, objMovCaja, tr)
                    '******************** Insertamos los pago caja
                    Dim listaPagoCaja As List(Of Entidades.PagoCaja) = lista_listaPagoCajaNew(i)
                    Dim objDaoPagoCaja As New DAO.DAOPagoCaja
                    For j As Integer = 0 To listaPagoCaja.Count - 1
                        listaPagoCaja(j).IdDocumento = objMovCaja.IdDocumento
                        objDaoPagoCaja.InsertaPagoCaja(cn, listaPagoCaja(j), tr)
                        If (listaPagoCaja(j).IdMedioPago = 2) Then  '********** ACTUALIZAMOS SALDOS DE NOTA DE CREDITO
                            Dim objDaoDocumentoNotaCredito As New DAO.DAODocumentoNotaCredito
                            objDaoDocumentoNotaCredito.DocumentoNotaCredito_DisminuirSaldo_Ventas(CInt(listaPagoCaja(j).NumeroCheque), listaPagoCaja(j).Efectivo, cn, tr)

                            '******************** Insertamos el Documento relacionado
                            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
                            objDaoRelacionDocumento.InsertaRelacionDocumento(New Entidades.RelacionDocumento(CInt(listaPagoCaja(j).NumeroCheque), ObjDocumentoAux.Id), cn, tr)
                        End If
                    Next

                End If

                '********************** Insertamos el MovCuenta
                If objMovCuenta IsNot Nothing Then

                    Dim objDAOMovCuenta As New DAO.DAOMovCuenta

                    With objMovCuenta

                        .Monto = ObjDocumentoAux.TotalAPagar
                        .Saldo = ObjDocumentoAux.TotalAPagar
                        .IdDocumento = ObjDocumentoAux.Id

                    End With

                    objDAOMovCuenta.InsertaMovCuenta(cn, objMovCuenta, tr)

                End If




            Next

            '***************** GENERAMOS LAS ORDENES DE DESPACHO
            If (generarDespacho) Then
                Dim objDaoOrdenDespacho As New DAO.DAODocOrdenDespacho
                For i As Integer = 0 To listaDocumento.Count - 1

                    objDaoOrdenDespacho.DocumentoOrdenDespacho_GenerarDespachoAutomatico(listaDocumento(i).IdAlmacen, Nothing, listaDocumento(i).IdUsuario, 21, Nothing, listaDocumento(i).Id, cn, tr)

                Next
            End If

            tr.Commit()

        Catch ex As Exception

            tr.Rollback()
            Throw ex

        Finally

            If cn.State = ConnectionState.Open Then cn.Close()

        End Try

        Return listaDocumento

    End Function

    Public Function obtenerListaVistaPrevia(ByVal listaDocumento As List(Of Entidades.Documento)) As List(Of Entidades.Documento)

        Dim listaDocumentoAux As New List(Of Entidades.Documento)

        For i As Integer = 0 To listaDocumento.Count - 1

            If listaDocumento(i).IdEstadoDoc <> 3 Then

                listaDocumentoAux.Add(listaDocumento(i).getClone)

            End If

        Next

        Return listaDocumentoAux

    End Function
    ''QUITANDO CHECK
    Public Function QuitarCheckDocumento(ByVal IdDocumento As Integer) As Boolean
        Return obj.QuitarCheckDocumentoxIdDocumento(IdDocumento)
    End Function
    ''AGREGANDO CHECK
    Public Function AgregarCheckDocumentoxIdDocumento(ByVal IdDocumento As Integer) As Boolean
        Return obj.AgregarCheckDocumentoxIdDocumento(IdDocumento)
    End Function




    Public Function DeleteDocSalteadoxIdDocumento(ByVal IdDocumento As Integer) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            hecho = (New DAO.DAODocumento).DeleteDocSalteadoxIdDocumento(IdDocumento, cn, tr)
        Catch ex As Exception
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function

    Private Function obtenerListaPagoCajaxDocumento(ByVal listaDocumento As List(Of Entidades.Documento), ByVal listaCancelacion As List(Of Entidades.PagoCaja), ByVal listaVuelto As List(Of Entidades.PagoCaja)) As List(Of List(Of Entidades.PagoCaja))

        Dim lista_ListaPagoCaja As New List(Of List(Of Entidades.PagoCaja))
        Dim objUtil As New Negocio.Util

        For i As Integer = 0 To listaDocumento.Count - 1



            If listaDocumento(i).IdEstadoDoc <> 3 Then

                Dim totalAPagarNew As Decimal = listaDocumento(i).TotalAPagar

                ''**************** Si es agente RETENEDOR
                'If listaDocumento(i).IdTipoAgente = 2 Then

                '    '*************** Si es agente retenedor / Tabla TipoAgente
                '    totalAPagarNew = listaDocumento(i).TotalAPagar * (1 - (listaDocumento(i).TasaAgente / 100))

                'End If


                Dim listaAux As New List(Of Entidades.PagoCaja)

                If i = listaDocumento.Count - 1 Then

                    listaAux = unir_ListaCancelacion_ListaVuelto(listaDocumento(i), listaCancelacion, listaVuelto)
                    lista_ListaPagoCaja.Add(listaAux)
                    Return lista_ListaPagoCaja

                End If


                Dim j As Integer = 0

                While (j <= (listaCancelacion.Count - 1))


                    Dim Efectivo_Equivalente As Decimal = objUtil.SelectValorxIdMonedaOxIdMonedaD(listaCancelacion(j).IdMoneda, listaDocumento(i).IdMoneda, listaCancelacion(j).Efectivo, "E")


                    If totalAPagarNew < Efectivo_Equivalente Then

                        '************ Si el total a pagar es menor al efectivo
                        Dim objCancelacion As Entidades.PagoCaja = listaCancelacion(j).getClone

                        objCancelacion.Efectivo = objUtil.SelectValorxIdMonedaOxIdMonedaD(listaDocumento(i).IdMoneda, _
                                                                                          listaCancelacion(j).IdMoneda, totalAPagarNew, "E")
                        listaAux.Add(objCancelacion)

                        listaCancelacion(j).Efectivo = listaCancelacion(j).Efectivo - objCancelacion.Efectivo


                        Exit While

                    ElseIf totalAPagarNew = Efectivo_Equivalente Then

                        '************ Si el total a pagar es igual al efectivo
                        listaAux.Add(listaCancelacion(j).getClone)
                        listaCancelacion.RemoveAt(j)

                        Exit While

                    Else

                        listaAux.Add(listaCancelacion(j).getClone)
                        listaCancelacion.RemoveAt(j)

                        totalAPagarNew = totalAPagarNew - Efectivo_Equivalente

                        j = 0 '************** Reiniciamos el contador

                    End If


                End While

                lista_ListaPagoCaja.Add(listaAux)

            End If

        Next

        Return lista_ListaPagoCaja

    End Function
    Private Function unir_ListaCancelacion_ListaVuelto(ByVal objDocumento As Entidades.Documento, ByVal listaCancelacion As List(Of Entidades.PagoCaja), ByVal listaVuelto As List(Of Entidades.PagoCaja)) As List(Of Entidades.PagoCaja)

        If listaVuelto.Count = 0 Then

            '**************** No se ha dado vuelto en diferentes monedas
            For i As Integer = 0 To listaCancelacion.Count - 1

                If listaCancelacion(i).IdMedioPago = 1 And listaCancelacion(i).IdMoneda = objDocumento.IdMoneda Then

                    '************** Si es efectivo y el IdMoneda es el mismo que el IdMonedaEmision
                    listaCancelacion(i).Vuelto = objDocumento.Vuelto
                    Return listaCancelacion

                End If

            Next

            '**************** De no encontrarse, creo un nuevo objeto solo si el vuelto es >0

            If objDocumento.Vuelto > 0 Then

                Dim objPagoCaja As New Entidades.PagoCaja

                With objPagoCaja

                    .Factor = 1
                    .IdMedioPago = 1
                    .IdMoneda = objDocumento.IdMoneda
                    .IdTipoMovimiento = 1
                    .Vuelto = objDocumento.Vuelto

                End With

                listaCancelacion.Add(objPagoCaja)

            End If

        Else

            '******************** Se ha dado vuelto en otra, en la misma y/o varias monedas

            For i As Integer = 0 To listaVuelto.Count - 1

                Dim addPagoCaja As Boolean = True

                For j As Integer = 0 To listaCancelacion.Count - 1

                    If (listaVuelto(i).IdMedioPago = listaCancelacion(j).IdMedioPago) And (listaVuelto(i).IdMoneda = listaCancelacion(j).IdMoneda) Then

                        listaCancelacion(j).Vuelto = listaVuelto(i).Efectivo
                        addPagoCaja = False
                        Exit For

                    End If

                Next

                If addPagoCaja Then

                    '******************* Si no existe, agrego un nuevo PagoCaja
                    Dim objPagoCaja As New Entidades.PagoCaja

                    With objPagoCaja

                        .Factor = listaVuelto(i).Factor
                        .IdMedioPago = listaVuelto(i).IdMedioPago
                        .IdMoneda = listaVuelto(i).IdMoneda
                        .IdTipoMovimiento = listaVuelto(i).IdTipoMovimiento
                        .Vuelto = listaVuelto(i).Efectivo

                    End With

                    listaCancelacion.Add(objPagoCaja)

                End If


            Next

        End If

        Return listaCancelacion

    End Function
    Private Function ordenarListaDetalleDocumento(ByVal lista_ListaDetalleDocumento As List(Of List(Of Entidades.DetalleDocumento))) As List(Of List(Of Entidades.DetalleDocumento))

        Dim lista_ListaDetalleDocumentoRetorno As New List(Of List(Of Entidades.DetalleDocumento))

        For i As Integer = 0 To lista_ListaDetalleDocumento.Count - 1

            Dim lista As List(Of Entidades.DetalleDocumento) = lista_ListaDetalleDocumento(i)

            For j As Integer = 0 To lista.Count - 2

                For k As Integer = j + 1 To lista.Count - 1

                    If lista(j).Importe > lista(k).Importe Then

                        Dim aux As Entidades.DetalleDocumento = lista(j)
                        lista(j) = lista(k)
                        lista(k) = aux

                    End If

                Next

            Next

            lista_ListaDetalleDocumentoRetorno.Add(lista)

        Next

        Return lista_ListaDetalleDocumentoRetorno

    End Function
    Private Function separarListaDetalleDocumento_Afecto_NoAfecto(ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal separarAfectos_NoAfectos As Boolean) As List(Of List(Of Entidades.DetalleDocumento))

        '************ Declaramos la lista que contendrá la lista de afectos y no afectos
        Dim lista_ListaDetalleDocumento As New List(Of List(Of Entidades.DetalleDocumento))

        If separarAfectos_NoAfectos Then

            '************* Declaramos las listas de afectos y no afectos
            '************* SOLO TRABAJAMOS CON AFECTOS A PERCEPCION Y NO AFECTOS
            Dim listaAfecto As New List(Of Entidades.DetalleDocumento)
            Dim listaNOAfecto As New List(Of Entidades.DetalleDocumento)

            For i As Integer = 0 To listaDetalleDocumento.Count - 1

                If listaDetalleDocumento(i).TasaPercepcion > 0 Then

                    listaAfecto.Add(listaDetalleDocumento(i))

                Else

                    listaNOAfecto.Add(listaDetalleDocumento(i))

                End If

            Next

            '******************** Agregamos la Listas en el Orden :  NO Afecto  -   Afecto
            lista_ListaDetalleDocumento.Add(listaNOAfecto)
            lista_ListaDetalleDocumento.Add(listaAfecto)

        Else

            lista_ListaDetalleDocumento.Add(listaDetalleDocumento)

        End If

        Return lista_ListaDetalleDocumento

    End Function
    Private Function separarListaDetalleDocumentoxMontoxNroFilasDetalleDoc(ByVal separarxMonto As Boolean, ByVal lista_ListaDetalleDocumento As List(Of List(Of Entidades.DetalleDocumento)), ByVal IdTipoDocumento As Integer, ByVal IdEmpresa As Integer, ByVal IdMonedaDestino As Integer) As List(Of List(Of Entidades.DetalleDocumento))

        Dim lista_listaDetalleDocumentoRetorno As New List(Of List(Of Entidades.DetalleDocumento))
        Dim separarxMontoOld As Boolean = separarxMonto

        For i As Integer = 0 To lista_ListaDetalleDocumento.Count - 1

            separarxMonto = separarxMontoOld

            Dim lista As List(Of Entidades.DetalleDocumento) = lista_ListaDetalleDocumento(i)
            Dim listaAux As New List(Of Entidades.DetalleDocumento)
            Dim montoAcumulado As Decimal = 0

            '**************** Obtenemos los valores requeridos
            Dim objTipoDocumentoAE As Entidades.TipoDocumentoAE = (New Negocio.TipoDocumentoAE).SelectValoresxParams(IdEmpresa, IdTipoDocumento, IdMonedaDestino)

            If objTipoDocumentoAE Is Nothing Then
                Throw New Exception("No se ha registrado la capacidad de Números de Detalle para este documento.")
            End If

            Dim nroFilasDocumento As Integer = objTipoDocumentoAE.CantidadFilas
            Dim montoMaximo As Decimal = 0

            Select Case i

                Case 0

                    '**************** Primero es la lista de los NO AFECTO caso contrario puede contener a todos los elementos
                    montoMaximo = objTipoDocumentoAE.MontoMaximoNoAfecto

                Case 1

                    '******************* Luego viene la lista de los AFECTO PERCEPCION
                    montoMaximo = objTipoDocumentoAE.MontoMaximoAfecto

            End Select

            If montoMaximo = 0 Then

                montoMaximo = 999999 '************** damos un valor grande como monto maximo

            End If


            For j As Integer = 0 To lista.Count - 1

                Dim montoAcumuladoPrevio As Decimal = montoAcumulado + lista(j).Importe



                If separarxMonto Then

                    '************** Si el primer monto es mayor al maximo permitido, no separamos los documentos

                    If ((montoAcumuladoPrevio > montoMaximo) And (j = 0)) Then separarxMonto = False

                    If (listaAux.Count = nroFilasDocumento Or montoAcumuladoPrevio > montoMaximo) And (listaAux.Count > 0) Then

                        lista_listaDetalleDocumentoRetorno.Add(listaAux)

                        listaAux = New List(Of Entidades.DetalleDocumento)
                        montoAcumulado = 0

                    End If

                Else

                    If listaAux.Count = nroFilasDocumento Then

                        lista_listaDetalleDocumentoRetorno.Add(listaAux)

                        listaAux = New List(Of Entidades.DetalleDocumento)

                    End If

                End If

                listaAux.Add(lista(j))
                montoAcumulado = montoAcumulado + lista(j).Importe

                '**************** Si queda algun elemento lo añadimos
                If j = (lista.Count - 1) And listaAux.Count > 0 Then

                    lista_listaDetalleDocumentoRetorno.Add(listaAux)

                End If



            Next

        Next

        Return lista_listaDetalleDocumentoRetorno

    End Function
    Private Function obtenerListaDocumentos(ByVal objDocumento As Entidades.Documento, ByVal saltarCorrelativo As Boolean, ByVal lista_ListaDetalleDocumento As List(Of List(Of Entidades.DetalleDocumento)), ByVal cobrarPercepcion As Boolean) As List(Of Entidades.Documento)

        '******************* Obtenemos la descripción del total a pagar
        Dim totalALetras As String = ""

        Select Case objDocumento.IdMoneda

            Case 1
                '*********** SOLES
                totalALetras = "/100 SOLES"

            Case 2
                '*********** DOLARES
                totalALetras = "/100 DÓLARES AMERICANOS"
        End Select


        Dim listaDocumento As New List(Of Entidades.Documento)

        '*********************** Hallamos el nro de documentos a guardar
        Dim nroDocumentos As Integer = lista_ListaDetalleDocumento.Count

        If saltarCorrelativo Then

            nroDocumentos = lista_ListaDetalleDocumento.Count * 2 - 1

        End If

        '************************ Reservamos los espacios de guardado
        For i As Integer = 0 To nroDocumentos - 1

            listaDocumento.Add((New Entidades.Documento()))
            listaDocumento(i).IdEstadoDoc = 3 '******* Estado el cual indica que esta como PENDIENTE
            listaDocumento(i).IdSerie = objDocumento.IdSerie
            listaDocumento(i).Serie = objDocumento.Serie

        Next

        '************************* Recorremos el array de listas detalle
        For i As Integer = 0 To lista_ListaDetalleDocumento.Count - 1

            '**************** Declaramos la variables a utilizar
            Dim lista As List(Of Entidades.DetalleDocumento) = lista_ListaDetalleDocumento(i)
            Dim descuentoAcumulado As Decimal = 0
            Dim importeAcumulado As Decimal = 0
            Dim percepcionAcumulado As Decimal = 0

            Dim retencion As Decimal = 0

            For j As Integer = 0 To lista.Count - 1

                '******************* Obtenemos los totales
                descuentoAcumulado += lista(j).Descuento * lista(j).Cantidad
                importeAcumulado += lista(j).Importe
                percepcionAcumulado += (lista(j).TasaPercepcion / 100) * lista(j).Importe

            Next

            Dim objDocumentoNew As Entidades.Documento = objDocumento.getClone
            Dim Igv As Decimal = (New DAO.DAOImpuesto).SelectTasaIGV

            With objDocumentoNew

                .Descuento = Math.Round(descuentoAcumulado, 2)
                .ImporteTotal = Math.Round(importeAcumulado, 2)
                .Total = Math.Round(importeAcumulado, 2)
                .SubTotal = Math.Round(CDec(.Total / (Igv + 1)), 2)
                .IGV = Math.Round(.Total - .SubTotal, 2)

                '************* Si es retenedor y es factura
                If objDocumentoNew.IdTipoAgente = 2 And objDocumentoNew.IdTipoDocumento = 1 Then
                    percepcionAcumulado = 0
                    retencion = (objDocumentoNew.TasaAgente / 100) * objDocumentoNew.Total
                End If
                '************* Si es agente perceptor y es factura y el importe de percepcion es cero
                If objDocumentoNew.IdTipoAgente = 1 And objDocumentoNew.IdTipoDocumento = 1 And percepcionAcumulado > 0 Then
                    percepcionAcumulado = (objDocumentoNew.TasaAgente / 100) * objDocumentoNew.Total
                    retencion = 0
                End If

                '************** Si es Boleta 
                If objDocumentoNew.IdTipoDocumento = 3 And objDocumentoNew.Total < 700 Then
                    percepcionAcumulado = 0
                    retencion = 0
                End If

                If cobrarPercepcion = False Then
                    '************* SOLO CASO LOS PINOS
                    percepcionAcumulado = 0
                End If

                .TotalAPagar = Math.Round(.Total + percepcionAcumulado, 2)
                .Percepcion = Math.Round(percepcionAcumulado, 2)
                .Retencion = retencion
                .TotalLetras = (New ALetras).Letras(Math.Round(.TotalAPagar, 2).ToString) + totalALetras

            End With

            '******************* Obtenemos el salto de Documento
            Dim salto As Integer = CInt(IIf(saltarCorrelativo = True, 2, 1))

            listaDocumento(i * salto) = objDocumentoNew

        Next


        Return listaDocumento

    End Function

    Public Function DocumentoCabSelectxId(ByVal iddocumento As Integer) As Entidades.Documento
        Return obj.DocumentoCabSelectxId(iddocumento)
    End Function

    Public Function UpdateFechaEmision(ByVal IdDocumento As Integer, ByVal FechaEmisionNew As Date) As Boolean

        Dim hecho As Boolean = False
        Dim cn As SqlConnection = objConexion.ConexionSIGE


        Try

            cn.Open()

            obj.UpdateFechaEmision(IdDocumento, FechaEmisionNew, cn)
            hecho = True
        Catch ex As Exception
            hecho = False
        Finally

            If cn.State = ConnectionState.Open Then cn.Close()

        End Try
        Return hecho


    End Function

    Public Function InsertaDocumentoVentaT(ByVal Documento As Entidades.Documento, ByVal LDetalle As List(Of Entidades.DetalleDocumento), _
                               ByVal MontoRegimen As Entidades.MontoRegimen, ByVal LMovAlmacen As List(Of Entidades.MovAlmacen), ByVal PLlegada As Entidades.PuntoLlegada, _
                              Optional ByVal MovCaja As Entidades.MovCaja = Nothing, Optional ByVal listaPagoCaja As List(Of Entidades.PagoCaja) = Nothing, _
                               Optional ByVal listaMovCuenta As List(Of Entidades.MovCuenta) = Nothing) As Integer

        Return obj.InsertaDocumentoVentaT(Documento, LDetalle, MontoRegimen, LMovAlmacen, PLlegada, MovCaja, listaPagoCaja, listaMovCuenta)
    End Function
    Public Function InsertaDocumentoT(ByVal documento As Entidades.Documento, ByVal LDetalleGuia As List(Of Entidades.DetalleGuia), Optional ByVal afectaMovAlmacen As Boolean = False, Optional ByVal factorMov As Integer = 0, Optional ByVal IdAlmacen As Integer = 0, Optional ByVal IdMetValuacion As Integer = 0, Optional ByVal inventarioInicial As Boolean = False) As Integer
        Return obj.InsertaDocumentoT(documento, LDetalleGuia, afectaMovAlmacen, factorMov, IdAlmacen, IdMetValuacion, inventarioInicial)
    End Function
    Public Function SelectInvInicialIdDocxEmpresaxAlmacen(ByVal idempresa As Integer, ByVal idalmacen As Integer) As Integer
        Return obj.SelectInvInicialIdDocxEmpresaxAlmacen(idempresa, idalmacen)
    End Function
    Public Function SelectInvInicialCabxIdDocumento(ByVal iddocumento As Integer) As Entidades.Documento
        Return obj.SelectInvInicialCabxIdDocumento(iddocumento)
    End Function
    Public Function SelectIdDocumentoxIdSeriexCodigo(ByVal IdSerie As Integer, ByVal codigo As Integer) As Integer
        Return obj.SelectIdDocumentoxIdSeriexCodigo(IdSerie, codigo)
    End Function
    Public Function SelectxIdSeriexCodigo(ByVal IdSerie As Integer, ByVal codigo As Integer) As Entidades.Documento
        Try
            Return obj.SelectxIdSeriexCodigo(IdSerie, codigo)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function ActualizarDocumentoGuia(ByVal documento As Entidades.Documento, ByVal LDet As List(Of Entidades.DetalleGuia), ByVal IdDocumentoOld As Integer, Optional ByVal factorMov As Integer = 0, Optional ByVal IdAlmacen As Integer = 0, Optional ByVal IdMetValuacion As Integer = 0, Optional ByVal inventarioInicial As Boolean = False) As Integer
        Return obj.ActualizarDocumentoGuia(documento, LDet, IdDocumentoOld, factorMov, IdAlmacen, IdMetValuacion, False)
    End Function

    Public Function InsertDocumentoInvInicial(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleDoc As List(Of Entidades.DetalleDocumento), ByVal listaMovAlmacen As List(Of Entidades.MovAlmacen)) As Integer
        '************ devuelve el IdDocumento
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim IdDocumento As Integer = 0
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            '************ inserto el documento cabecera
            IdDocumento = obj.InsertaDocumento(cn, objDocumento, tr)

            '************** inserto el detalle del documento
            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            Dim objDaoMovAlmacen As New DAO.DAOMovAlmacen
            For i As Integer = 0 To listaDetalleDoc.Count - 1
                listaDetalleDoc(i).IdDocumento = IdDocumento
                Dim IdDetalleDocumento As Integer = objDaoDetalleDocumento.InsertaDetalleDocumento(cn, listaDetalleDoc.Item(i), tr)

                listaMovAlmacen(i).IdDocumento = IdDocumento
                listaMovAlmacen.Item(i).IdDetalleDocumento = IdDetalleDocumento
                objDaoMovAlmacen.InsertaMovAlmacen(cn, listaMovAlmacen(i), tr)
            Next
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            IdDocumento = -1
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return IdDocumento
    End Function

    Public Function UpdateDocumentoInvInicial(ByVal IdDocumento As Integer, ByVal IdAlmacen As Integer, ByVal IdSubLinea As Integer, ByVal listaDetalleDoc As List(Of Entidades.DetalleDocumento)) As Integer
        '************ devuelve el IdDocumento
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento


            objDaoDetalleDocumento.DocumentoInvInicialDeleteDetallexIdDocumentoxIdSubLinea(IdDocumento, IdSubLinea, cn, tr)
            objDaoDetalleDocumento.InsertaDetalleDocumento_MovAlmacen(cn, listaDetalleDoc, tr, IdDocumento, True, False, 1, IdAlmacen)

            tr.Commit()
            Return IdDocumento
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function
    Public Function AnulaDocumentoVentaT(ByVal IdDocumento As Integer) As Boolean

        'Dim IdDocumento As Integer = Nothing
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Using cn
            cn.Open()
            Dim T As SqlTransaction = cn.BeginTransaction(Data.IsolationLevel.Serializable)
            Try

                Dim daoMovAlmacen As New DAO.DAOMovAlmacen
                Dim daoPagoCaja As New DAO.DAOPagoCaja
                Dim daoMovCaja As New DAO.DAOMovCaja

                Dim daoUtil As New DAO.DAOUtil
                'Elimina el Movimiento de Almacen
                If daoUtil.ValidarExistenciaxTablax1Campo(cn, T, "MovAlmacen", "IdDocumento", CStr(IdDocumento)) > 0 Then
                    daoMovAlmacen.DeletexIdDocumento(cn, T, IdDocumento)
                End If
                'Elimina el Pago de Caja
                If daoUtil.ValidarExistenciaxTablax1Campo(cn, T, "PagoCaja", "IdDocumento", CStr(IdDocumento)) > 0 Then
                    daoPagoCaja.DeletexIdDocumento(cn, T, IdDocumento)
                    'Elimina el MOvimiento de Caja
                    daoMovCaja.DeletexIdDocumento(cn, T, IdDocumento)
                End If

                'Actualiza el Estado del Documento a Anulado
                obj.AnulaT(cn, T, IdDocumento)

                'Anula Documentos Relacionados como: Ordenes de Despacho y Guias
                Dim daoRelacionDocumento As New DAO.DAORelacionDocumento
                Dim RelacionDocumento As New Entidades.RelacionDocumento
                Dim LRelacionDocumento As New List(Of Entidades.RelacionDocumento)
                LRelacionDocumento = daoRelacionDocumento.SelectAllIdDoc2xIdDoc1(cn, T, IdDocumento)



                If LRelacionDocumento.Count > 0 Then
                    For Each RelacionDocumento In LRelacionDocumento
                        obj.AnulaT(cn, T, RelacionDocumento.IdDocumento2)
                        If daoUtil.ValidarExistenciaxTablax1Campo(cn, T, "MovAlmacen", "IdDocumento", CStr(RelacionDocumento.IdDocumento2)) > 0 Then
                            daoMovAlmacen.DeletexIdDocumento(cn, T, RelacionDocumento.IdDocumento2)
                        End If
                    Next
                End If

                T.Commit()
                Return True
            Catch ex As Exception
                T.Rollback()
                Return False
            End Try
        End Using
    End Function

    Public Function SelectActivoxIdDocumento(ByVal IdDocumento As Integer) As Entidades.Documento
        Return obj.SelectActivoxIdDocumento(IdDocumento)
    End Function

    Public Function UpdateDocumentoGuiaAlmacen(idUsuario As Integer, ByVal Documento As Entidades.Documento, ByVal listaDetalle As List(Of Entidades.DetalleDocumento), ByVal objObservacion As Entidades.Observacion, ByVal IdDocumentoRelacionado As Integer, ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal objTransportista As Entidades.Juridica, ByVal objChofer As Entidades.Chofer, ByVal objVehiculo As Entidades.Vehiculo, ByVal comprometeStock As Boolean, ByVal insertarMovAlmacen As Boolean) As Integer

        '************ Si actualiza correctamente devuelve el Id Documento actualizado
        '************ caso contrario devuelve -1

        '**************
        '***** La Guia de Remision solo es editada cuando no posee un documento de despacho
        '***** o despachos asociados

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction

            '************************ DESHACER MOVIMIENTOS
            Dim objDaoGuiaRemision As New DAO.DAODocGuiaRemision
            objDaoGuiaRemision.DocumentoGuiaRemision_DeshacerMov(Documento.Id, True, True, True, True, True, True, True, False, cn, tr)

            '****************** inserto el Transportista si es NUEVO
            If objTransportista IsNot Nothing Then

                Dim listaDocumentoI As New List(Of Entidades.DocumentoI)
                listaDocumentoI.Add(New Entidades.DocumentoI(Nothing, objTransportista.RUC, 3, Nothing))
                Dim objPersona As New Entidades.Persona
                objPersona.Estado = "1"

                Documento.IdTransportista = (New DAO.DAOPersona).InsertaPersonaT(cn, tr, objPersona, False, Nothing, True, objTransportista, False, Nothing, LDoid:=listaDocumentoI)

            End If


            '****************** inserto el Chofer si es NUEVO
            If objChofer IsNot Nothing Then
                Dim objNatural As New Entidades.Natural
                objNatural.Nombres = objChofer.Nombre

                Dim objPersona As New Entidades.Persona
                objPersona.Estado = "1"

                Documento.IdChofer = (New DAO.DAOPersona).InsertaPersonaT(cn, tr, objPersona, True, objNatural, False, Nothing, False, Nothing)

                objChofer.Id = Documento.IdChofer
                Dim objDaoChofer As New DAO.DAOChofer
                objDaoChofer.InsertaChofer(objChofer, cn, tr)
            End If

            '******************** inserto el Vehiculo si es NUEVO
            If objVehiculo IsNot Nothing Then
                Dim objProducto As New Entidades.Producto
                objProducto.IdEstadoProd = 1

                objVehiculo.IdProducto = (New DAO.DAOProducto).InsertaProductoTipoMercaderia(idUsuario, objProducto, cn, tr)

                Dim objDaoVehiculo As New DAO.DAOVehiculo
                objDaoVehiculo.InsertaVehiculo(objVehiculo, cn, tr)

                Documento.IdVehiculo = objVehiculo.IdProducto
            End If

            '*********************** ACTUALIZO LA CABECERA Cabecera
            obj.DocumentoUpdate(Documento, cn, tr)

            '********************** Observaciones
            Dim objDaoObservaciones As New DAO.DAOObservacion
            If objObservacion IsNot Nothing Then
                objObservacion.IdDocumento = Documento.Id
                objDaoObservaciones.InsertaObservacionT(cn, tr, objObservacion)
            End If



            '***************** Punto Partida
            Dim objDaoPuntoPartida As New DAO.DAOPuntoPartida
            If objPuntoPartida IsNot Nothing Then
                objPuntoPartida.IdDocumento = Documento.Id
                objDaoPuntoPartida.InsertaPuntoPartida(objPuntoPartida, cn, tr)
            End If


            '**************** PuntoLLegada
            Dim objDaoPuntoLlegada As New DAO.DAOPuntoLlegada
            If objPuntoLlegada IsNot Nothing Then
                objPuntoLlegada.IdDocumento = Documento.Id
                objDaoPuntoLlegada.InsertaPuntoLlegadaT(cn, objPuntoLlegada, tr)
            End If

            Dim objDaoDetalleDoc As New DAO.DAODetalleDocumento
            objDaoDetalleDoc.InsertaDetalleDocumento_MovAlmacen(cn, listaDetalle, tr, Documento.Id, insertarMovAlmacen, comprometeStock, Documento.FactorMov, Documento.IdAlmacen)

            tr.Commit()
        Catch ex As Exception

            tr.Rollback()
            Documento.Id = -1
            Throw ex
        Finally

            If cn.State = ConnectionState.Open Then cn.Close()

        End Try

        Return Documento.Id

    End Function

    Public Function DocumentoGuiaRemisionAnular(ByVal IdDocumento As Integer) As Boolean

        Dim hecho As Boolean = False
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try

            cn.Open()
            tr = cn.BeginTransaction

            Dim objDaoGuiaRemision As New DAO.DAODocGuiaRemision
            objDaoGuiaRemision.DocumentoGuiaRemision_DeshacerMov(IdDocumento, False, True, False, False, False, False, True, True, cn, tr)


            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

        Return hecho

    End Function

    Public Function actualizarGuiaRecepcion(ByVal modo As Integer, ByVal objDocumento As Entidades.Documento, ByVal listaDetalle As List(Of Entidades.DetalleDocumento), ByVal objObservacion As Entidades.Observacion, ByVal IdDocumentoRef As Integer) As Boolean

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction

            Dim objDaoDocumento As New DAO.DAODocumento
            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento

            '**************** DESHACEMOS LOS MOV
            Dim objDaoDocGuiaRecepcion As New DAO.DAODocGuiaRecepcion
            objDaoDocGuiaRecepcion.DocumentoGuiaRecepcion_DeshacerMov(objDocumento.Id, True, True, True, True, True, True, True, False, cn, tr)


            '************************* Actualizamos la cabecera
            objDaoDocumento.DocumentoUpdate(objDocumento, cn, tr)

            '*********************** Insertamos el detalledocumento
            objDaoDetalleDocumento.InsertaDetalleDocumento_MovAlmacen(cn, listaDetalle, tr, objDocumento.Id, True, False, 1, objDocumento.IdAlmacen)

            '********************* Insertamos las Observaciones
            Dim objDaoObservacion As New DAO.DAOObservacion
            If (objObservacion IsNot Nothing) Then objDaoObservacion.InsertaObservacionT(cn, tr, objObservacion)

            '********************* Documento Ref
            If (IdDocumentoRef <> 0) Then
                Dim objDaoRelacionDoc As New DAO.DAORelacionDocumento
                objDaoRelacionDoc.InsertaRelacionDocumento(New Entidades.RelacionDocumento(IdDocumentoRef, objDocumento.Id), cn, tr)
            End If


            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function

    Public Function AnularGuiaRecepcionxIdDocumento(ByVal IdDocumento As Integer) As Boolean

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False

        Try

            cn.Open()
            tr = cn.BeginTransaction

            obj.AnularGuiaRecepcionxIdDocumento(IdDocumento, cn, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho

    End Function

    Public Function SelectDocxDespachar(ByVal IdTienda As Integer) As List(Of Entidades.DocumentoView)

        Return obj.SelectDocxDespachar(IdTienda)

    End Function

    Public Function InsertDocumentoGuia_Almacen(idUsuario As Integer, ByVal objDocumento As Entidades.Documento, ByVal listaDetalleDoc As List(Of Entidades.DetalleDocumento), ByVal listaMovAlmacen As List(Of Entidades.MovAlmacen), Optional ByVal objObservaciones As Entidades.Observacion = Nothing, Optional ByVal IdDocumentoRelacionado As Integer = 0, Optional ByVal objPuntoPartida As Entidades.PuntoPartida = Nothing, Optional ByVal objPuntoLlegada As Entidades.PuntoLlegada = Nothing, Optional ByVal objTransportista As Entidades.Juridica = Nothing, Optional ByVal objChofer As Entidades.Chofer = Nothing, Optional ByVal objVehiculo As Entidades.Vehiculo = Nothing, Optional ByVal comprometer As Boolean = True) As Integer
        '************ devuelve el IdDocumento
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim IdDocumento As Integer = 0
        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            Dim objDaoMovAlmacen As New DAO.DAOMovAlmacen
            Dim objDaoProducto As New DAO.DAOProducto

            '****************** inserto el Transportista
            If objTransportista IsNot Nothing Then
                Dim listaDocumentoI As New List(Of Entidades.DocumentoI)
                listaDocumentoI.Add(New Entidades.DocumentoI(Nothing, objTransportista.RUC, 3, Nothing))
                Dim objPersona As New Entidades.Persona
                objPersona.Estado = "1"

                objDocumento.IdTransportista = (New DAO.DAOPersona).InsertaPersonaT(cn, tr, objPersona, False, Nothing, True, objTransportista, False, Nothing, LDoid:=listaDocumentoI)

            End If

            '****************** inserto el Chofer
            If objChofer IsNot Nothing Then
                Dim objNatural As New Entidades.Natural
                objNatural.Nombres = objChofer.Nombre

                Dim objPersona As New Entidades.Persona
                objPersona.Estado = "1"

                objDocumento.IdChofer = (New DAO.DAOPersona).InsertaPersonaT(cn, tr, objPersona, True, objNatural, False, Nothing, False, Nothing)

                objChofer.Id = objDocumento.IdChofer
                Dim objDaoChofer As New DAO.DAOChofer
                objDaoChofer.InsertaChofer(objChofer, cn, tr)
            End If

            '******************** inserto el Vehiculo
            If objVehiculo IsNot Nothing Then
                Dim objProducto As New Entidades.Producto
                objProducto.IdEstadoProd = 1

                objVehiculo.IdProducto = objDaoProducto.InsertaProductoTipoMercaderia(idUsuario, objProducto, cn, tr)

                Dim objDaoVehiculo As New DAO.DAOVehiculo
                objDaoVehiculo.InsertaVehiculo(objVehiculo, cn, tr)

                objDocumento.IdVehiculo = objVehiculo.IdProducto
            End If

            '************ inserto el documento cabecera
            IdDocumento = obj.InsertaDocumento(cn, objDocumento, tr)

            '************** inserto el detalle del documento y el mov. almacen

            For i As Integer = 0 To listaDetalleDoc.Count - 1
                listaDetalleDoc(i).IdDocumento = IdDocumento
                Dim IdDetalleDocumento As Integer = objDaoDetalleDocumento.InsertaDetalleDocumento(cn, listaDetalleDoc.Item(i), tr)

                If listaMovAlmacen.Count > 0 Then

                    listaMovAlmacen(i).IdDocumento = IdDocumento
                    listaMovAlmacen(i).IdDetalleDocumento = IdDetalleDocumento

                    If listaMovAlmacen(i).Factor <> 0 Then

                        '**Ă********* obtengo la cantidad equivalente para la UM Principal                
                        Dim cantEquivalente As Decimal = objDaoProducto.SelectEquivalenciaUMPrincipal(cn, tr, listaMovAlmacen(i).IdProducto, listaMovAlmacen(i).IdUnidadMedida, listaMovAlmacen(i).CantidadMov)
                        listaMovAlmacen(i).CantidadMov = cantEquivalente
                        listaMovAlmacen(i).CantxAtender = CDec(IIf(comprometer = True, cantEquivalente, Nothing))
                        objDaoMovAlmacen.InsertaMovAlmacen(cn, listaMovAlmacen(i), tr)
                    End If

                End If

            Next

            '***************** inserto las observaciones
            If objObservaciones IsNot Nothing Then
                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservaciones.IdDocumento = IdDocumento
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservaciones)
            End If

            '******************** inserto el documento relacionado
            If IdDocumentoRelacionado <> 0 Then
                Dim objDocRelacionado As New DAO.DAORelacionDocumento
                Dim obj As New Entidades.RelacionDocumento
                With obj
                    .IdDocumento1 = IdDocumentoRelacionado
                    .IdDocumento2 = IdDocumento
                End With
                objDocRelacionado.InsertaRelacionDocumento(obj, cn, tr)
            End If

            '*********************** inserto el Punto de Partida
            If objPuntoPartida IsNot Nothing Then
                Dim objDaoPuntoPartida As New DAO.DAOPuntoPartida
                objPuntoPartida.IdDocumento = IdDocumento
                objDaoPuntoPartida.InsertaPuntoPartida(objPuntoPartida, cn, tr)
            End If

            '************************ inserto el Punto de LLegada
            If objPuntoLlegada IsNot Nothing Then
                Dim objDaoPuntoLLegada As New DAO.DAOPuntoLlegada
                objPuntoLlegada.IdDocumento = IdDocumento
                objDaoPuntoLLegada.InsertaPuntoLlegadaT(cn, objPuntoLlegada, tr)
            End If

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            IdDocumento = -1
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return IdDocumento
    End Function
    Public Function SelectxIdDocumentoExternoLista(ByVal IdDocumento As String) As List(Of Entidades.Documento)
        Return obj.SelectxIdDocumentoExternoLista(IdDocumento)
    End Function
    Public Function SelectxIdDocumento(ByVal IdDocumento As Integer) As Entidades.Documento
        Return obj.SelectxIdDocumento(IdDocumento)
    End Function
    Public Function SelectxIdDocumentoDocReferencia(ByVal IdDocumento As Integer) As Entidades.Documento
        Return obj.SelectxIdDocumentoDocReferencia(IdDocumento)
    End Function
    Public Function SelectxIdDocumentoExterno(ByVal IdDocumento As String) As List(Of Entidades.Documento)
        Return obj.SelectxIdDocumentoExterno(IdDocumento)
    End Function
    Public Function SelectxIdDocumentoExternoOne(ByVal IdDocumento As String) As Entidades.Documento
        Return obj.SelectxIdDocumentoExternoOne(IdDocumento)
    End Function
    Public Function SelectxIdDocumentoExternotwo(ByVal IdDocumento As String) As Entidades.Documento
        Return obj.SelectxIdDocumentoExternotwo(IdDocumento)
    End Function

    Public Function SelectxIdDocumentoReferenciaCheckes(ByVal IdDocumento As Integer) As Entidades.Documento
        Return obj.SelectxIdDocumentoReferenciaCheckes(IdDocumento)
    End Function
    Public Function SelectxIdDocumentoProgramacionPago(ByVal IdDocumento As Integer) As Entidades.Documento
        Return obj.SelectxIdDocumentoProgramacionPago(IdDocumento)
    End Function

    Public Function FacturaSelectDet_Impresion(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)
        Return obj.FacturaSelectDet_Impresion(IdDocumento)
    End Function
    Public Function FacturaSelectCab_Impresion(ByVal IdDocumento As Integer) As Entidades.Documento
        Return obj.FacturaSelectCab_Impresion(IdDocumento)
    End Function


    Public Function DocumentoSelectDocxDespachar_V2(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, _
                                                    ByVal Serie As Integer, ByVal Codigo As Integer, ByVal IdPersona As Integer, _
                                                    ByVal IdTipoDocumento As Integer, ByVal IdTipoOperacion As Integer, ByVal IdTipoDocumentoRef As Integer) As List(Of Entidades.DocumentoView)
        Return obj.DocumentoSelectDocxDespachar_V2(IdEmpresa, IdAlmacen, FechaInicio, FechaFin, Serie, Codigo, IdPersona, IdTipoDocumento, IdTipoOperacion, IdTipoDocumentoRef)
    End Function

    Public Function DocumentoSelectDocxDespachar_V2_Detalle(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, _
                                                ByVal Serie As Integer, ByVal Codigo As Integer, ByVal IdPersona As Integer, _
                                                ByVal IdTipoDocumento As Integer, ByVal IdTipoOperacion As Integer, ByVal IdTipoDocumentoRef As Integer) As List(Of Entidades.DocumentoView)
        Return obj.DocumentoSelectDocxDespachar_V2_Detalle(IdEmpresa, IdAlmacen, FechaInicio, FechaFin, Serie, Codigo, IdPersona, IdTipoDocumento, IdTipoOperacion, IdTipoDocumentoRef)
    End Function

    Public Function DocumentoSelectDocxDespachar_picking(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, _
                                            ByVal Serie As Integer, ByVal Codigo As Integer, ByVal IdPersona As Integer, _
                                            ByVal IdTipoDocumento As Integer, ByVal IdTipoOperacion As Integer, ByVal IdTipoDocumentoRef As Integer) As List(Of Entidades.DocumentoView)
        Return obj.DocumentoSelectDocxDespachar_picking(IdEmpresa, IdAlmacen, FechaInicio, FechaFin, Serie, Codigo, IdPersona, IdTipoDocumento, IdTipoOperacion, IdTipoDocumentoRef)
    End Function


    ''Inserto el  punto de llegada
    Public Function InsertarPuntoLlegadax(ByVal objx As Entidades.PuntoLlegada) As Boolean

        Dim objPuntoLlegada As New Entidades.PuntoLlegada
        Dim objDaoPuntoLlegada As New DAO.DAODocumento


        objPuntoLlegada.IdDocumento = objx.IdDocumento
        objPuntoLlegada.IdTienda = objx.IdTienda
        objPuntoLlegada.IdAlmacen = objx.IdAlmacen
        objPuntoLlegada.pll_Direccion = objx.pll_Direccion
        objPuntoLlegada.pll_Ubigeo = objx.pll_Ubigeo

        Return objDaoPuntoLlegada.InsertaPuntoLlegadaT(objPuntoLlegada)

    End Function
    Public Function UpdateVendedorCotizacion(ByVal objx As Entidades.Documento) As Boolean

        Dim objDocumento As New Entidades.Documento
        Dim objDaoDocu As New DAO.DAODocumento


        objDocumento.IdDocumento = objx.IdDocumento
        objDocumento.IdUsuario = objx.IdUsuario
        objDocumento.IdUsuarioComision = objx.IdUsuarioComision

        Return objDaoDocu.UpdateVendedorCotizacion(objDocumento)

    End Function
    Public Function UpdateVendedorBolFact(ByVal objxa As Entidades.Documento) As Boolean

        Dim objDocumento As New Entidades.Documento
        Dim objDaoDocux As New DAO.DAODocumento

        objDocumento.IdDocumento = objxa.IdDocumento
        objDocumento.IdUsuarioComision = objxa.IdUsuarioComision

        Return objDaoDocux.UpdateVendedorBolFact(objDocumento)

    End Function
    Public Function EliminarPuntoLlegada(ByVal IdDocumento As Integer) As Boolean

        Return obj.EliminarPuntoLlegada(IdDocumento)

    End Function
    ''Obtengo el id de la cotizacion relacionada al documento
    Public Function SelectIdCotizacion(ByVal IdDocumento As Integer) As Integer

        Return obj.SelectIdCotizacion(IdDocumento)

    End Function

    Public Sub validarNotaCredito(ByVal idDocumento As Integer, ByVal idDocumentoRecep As Integer, ByVal idTipoOperacion As Integer, ByVal idMotivo As Integer, _
                                  ByVal paramNumerico As Decimal, ByVal paramString As String)
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Dim obj As New DAO.DAODocumento
        cn.Open()
        Try
            obj.validarNotaCredito(idDocumento, idDocumentoRecep, idTipoOperacion, idMotivo, paramNumerico, paramString, cn)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function activarCamposNC(ByVal idTipoOperacion As Integer, ByVal idMotivo As Integer) As DataTable
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Dim obj As New DAO.DAODocumento
        cn.Open()
        Try
            Return obj.activarCamposNC(idTipoOperacion, idMotivo, cn)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function crearArchivoFacContingencia(ByVal idMotivo As Integer, ByVal fecha As Date, ByVal ruta As String)
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO.DAODocumento).crearArchivoFacContingencia(idMotivo, fecha, cn, ruta)
    End Function
End Class
