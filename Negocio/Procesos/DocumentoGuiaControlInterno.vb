﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.






'*************    VIERNES 26 MARZO 2010 HORA 04_30 PM










Imports System.Data.SqlClient

Public Class DocumentoGuiaControlInterno

    Private cn As SqlConnection
    Private tr As SqlTransaction
    Private objDaoDocumentoGuiaCI As New DAO.DAODocumentoGuiaControlInterno

    Public Function DocumentoGuiaControlInterno_SelectDetalle(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)
        Return objDaoDocumentoGuiaCI.DocumentoGuiaControlInterno_SelectDetalle(IdDocumento)
    End Function

    Public Function registrarDocumento(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal objObservaciones As Entidades.Observacion, ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento), ByVal MoverStockFisico As Boolean) As Integer

        cn = (New DAO.Conexion).ConexionSIGE
        Try

            '**** MoverStockFisico: MUEVE EL STOCK DE ALMACÉN

            cn.Open()
            tr = cn.BeginTransaction

            '************* CABECERA
            objDocumento.Id = (New DAO.DAODocumento).InsertaDocumento(cn, objDocumento, tr)

            '************* DETALLE
            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            '******************* INSERTAMOS LA SALIDA DE PRODUCTOS (REMITENTE)
            If (objDocumento.IdEmpresa = objDocumento.IdRemitente And objPuntoPartida IsNot Nothing) Then
                If (objPuntoPartida.IdAlmacen <> Nothing) Then

                    '*********** INSERTAMOS
                    objDaoDetalleDocumento.InsertaDetalleDocumento_MovAlmacen(cn, listaDetalleDocumento, tr, objDocumento.Id, MoverStockFisico, False, -1, objPuntoPartida.IdAlmacen, objPuntoLlegada.IdAlmacen)

                End If
            End If
            '******************* INSERTAMOS EL INGRESO DE PRODUCTOS (DESTINATARIO)
            If (objDocumento.IdEmpresa = objDocumento.IdDestinatario And objPuntoLlegada IsNot Nothing) Then
                If (objPuntoLlegada.IdAlmacen <> Nothing) Then

                    '*********** INSERTAMOS
                    objDaoDetalleDocumento.InsertaDetalleDocumento_MovAlmacen(cn, listaDetalleDocumento, tr, objDocumento.Id, MoverStockFisico, False, 1, objPuntoLlegada.IdAlmacen)

                End If
            End If

            '************ PUNTO PARTIDA
            If (objPuntoPartida IsNot Nothing) Then
                Dim objDaoPuntoPartida As New DAO.DAOPuntoPartida
                objPuntoPartida.IdDocumento = objDocumento.Id
                objDaoPuntoPartida.InsertaPuntoPartida(objPuntoPartida, cn, tr)
            End If

            '************ PUNTO LLEGADA
            If (objPuntoLlegada IsNot Nothing) Then
                Dim objDaoPuntollegada As New DAO.DAOPuntoLlegada
                objPuntoLlegada.IdDocumento = objDocumento.Id
                objDaoPuntollegada.InsertaPuntoLlegadaT(cn, objPuntoLlegada, tr)
            End If

            '************* DOCUMENTO REFERENCIA
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1
                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            Next

            '************ OBSERVACIONES
            If (objObservaciones IsNot Nothing) Then
                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservaciones)
            End If

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function


    Public Function actualizarDocumento(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal objObservaciones As Entidades.Observacion, ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento), ByVal MoverStockFisico As Boolean) As Integer

        cn = (New DAO.Conexion).ConexionSIGE
        Try


            '**** MoverStockFisico: MUEVE EL STOCK DE ALMACÉN

            cn.Open()
            tr = cn.BeginTransaction

            '**************** DESHACER MOV
            objDaoDocumentoGuiaCI.DocumentoGuiaControlInterno_DeshacerMov(objDocumento.Id, True, True, True, True, True, True, False, cn, tr)

            '************* CABECERA
            Dim objDaoDocumento As New DAO.DAODocumento
            objDaoDocumento.DocumentoUpdate(objDocumento, cn, tr)

            '************* DETALLE
            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            '******************* INSERTAMOS LA SALIDA DE PRODUCTOS (REMITENTE)
            If (objDocumento.IdEmpresa = objDocumento.IdRemitente And objPuntoPartida IsNot Nothing) Then
                If (objPuntoPartida.IdAlmacen <> Nothing) Then

                    '*********** INSERTAMOS
                    objDaoDetalleDocumento.InsertaDetalleDocumento_MovAlmacen(cn, listaDetalleDocumento, tr, objDocumento.Id, MoverStockFisico, False, -1, objPuntoPartida.IdAlmacen)

                End If
            End If
            '******************* INSERTAMOS EL INGRESO DE PRODUCTOS (DESTINATARIO)
            If (objDocumento.IdEmpresa = objDocumento.IdDestinatario And objPuntoLlegada IsNot Nothing) Then
                If (objPuntoLlegada.IdAlmacen <> Nothing) Then

                    '*********** INSERTAMOS
                    objDaoDetalleDocumento.InsertaDetalleDocumento_MovAlmacen(cn, listaDetalleDocumento, tr, objDocumento.Id, MoverStockFisico, False, 1, objPuntoLlegada.IdAlmacen)

                End If
            End If

            '************ PUNTO PARTIDA
            If (objPuntoPartida IsNot Nothing) Then
                Dim objDaoPuntoPartida As New DAO.DAOPuntoPartida
                objPuntoPartida.IdDocumento = objDocumento.Id
                objDaoPuntoPartida.InsertaPuntoPartida(objPuntoPartida, cn, tr)
            End If

            '************ PUNTO LLEGADA
            If (objPuntoLlegada IsNot Nothing) Then
                Dim objDaoPuntollegada As New DAO.DAOPuntoLlegada
                objPuntoLlegada.IdDocumento = objDocumento.Id
                objDaoPuntollegada.InsertaPuntoLlegadaT(cn, objPuntoLlegada, tr)
            End If

            '************* DOCUMENTO REFERENCIA
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1
                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
            Next

            '************ OBSERVACIONES
            If (objObservaciones IsNot Nothing) Then
                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservaciones)
            End If

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function

    Public Function anularDocumento(ByVal IdDocumento As Integer) As Boolean
        Dim hecho As Boolean = False
        Try
            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            objDaoDocumentoGuiaCI.DocumentoGuiaControlInterno_DeshacerMov(IdDocumento, False, True, False, False, False, False, True, cn, tr)


            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return hecho
    End Function
End Class
