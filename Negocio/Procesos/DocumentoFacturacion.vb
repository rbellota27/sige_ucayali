﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




'**********************   VIERNES 25 06 2010















Imports System.Data.SqlClient
Public Class DocumentoFacturacion

    Private objDaoDocumento As DAO.DAODocumento
    Private objDaoDocumentoFacturacion As New DAO.DAODocumentoFacturacion
    Private objConexion As New DAO.Conexion
    Private cn As SqlConnection
    Private tr As SqlTransaction
    Private cmd As SqlCommand

    Public Function DocumentoSelectDocumentoAnticipo(ByVal IdPersona As Integer, ByVal IdMoneda_Destino As Integer) As List(Of Entidades.Documento)
        Return objDaoDocumentoFacturacion.DocumentoSelectDocumentoAnticipo(IdPersona, IdMoneda_Destino)
    End Function


    Public Function AnularDocumentoFacturacion(ByVal IdDocumento As Integer) As Boolean

        Dim hecho As Boolean = False
        cn = objConexion.ConexionSIGE

        Try

            cn.Open()
            tr = cn.BeginTransaction

            objDaoDocumentoFacturacion.DocumentoFacturacion_DeshacerMov(IdDocumento, True, False, False, False, True, False, False, False, True, True, False, False, True, False, True, cn, tr)

            hecho = True

            tr.Commit()
            cn.Close()
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function

    Public Function DocumentoFacturacionSelectPrintxIds(ByVal IdDocumentoInicio As Integer, ByVal IdDocumentoFin As Integer, ByVal IdSerie As Integer) As List(Of Entidades.Documento)
        Return objDaoDocumentoFacturacion.DocumentoFacturacionSelectPrintxIds(IdDocumentoInicio, IdDocumentoFin, IdSerie)
    End Function

    Public Function registrarDocumentoVenta(ByVal objDocumento As Entidades.Documento, ByVal objAnexoDocumento As Entidades.Anexo_Documento, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal objObservacion As Entidades.Observacion, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal objMovCaja As Entidades.MovCaja, ByVal objMovCuenta As Entidades.MovCuenta, ByVal listaCancelacion As List(Of Entidades.PagoCaja), ByVal listaVuelto As List(Of Entidades.PagoCaja), ByVal moverAlmacen As Boolean, ByVal comprometerStock As Boolean, ByVal separarAfectos_NoAfectos As Boolean, ByVal saltarCorrelativo As Boolean, ByVal separarxMontos As Boolean, ByVal vistaPrevia As Boolean, ByVal comprometerPercepcion As Boolean, ByVal listaMovBanco As List(Of Entidades.MovBancoView), ByVal useAlias_Random As Boolean, ByVal objRelacionDocumento As Entidades.RelacionDocumento, ByVal frmModo As Integer, Optional ByVal objMontoRegimenRetencion As Entidades.MontoRegimen = Nothing, Optional ByVal objMontoRegimenDetraccion As Entidades.MontoRegimen = Nothing, Optional ByVal listaCheque As List(Of Entidades.Cheque) = Nothing, Optional ByVal objAnexo_FacturaTransporte As Entidades.Anexo_FacturaTransporte = Nothing) As List(Of Entidades.Documento)

        '***********    frmModo = 1    NUEVO         //       frmModo = 2    EDITAR

        If objAnexoDocumento Is Nothing Then objAnexoDocumento = New Entidades.Anexo_Documento

        Dim objDocumentoOriginal As Entidades.Documento = objDocumento.getClone

        Dim cobrarPercepcion As Boolean = Not comprometerPercepcion

        Dim porcentIgv As Decimal = (New Negocio.Impuesto).SelectTasaIGV * 100

        '***************** Valido si el Propietario es Agente PERCEPTOR
        If (New Negocio.Util).ValidarxDosParametros("PersonaTipoAgente", "IdPersona", CStr(objDocumento.IdEmpresa), "IdAgente", "1") <= 0 Then

            '************ NO es agente Percepción
            cobrarPercepcion = False

        End If

        '****************** Declaramos la lista de documentos a registrar
        Dim listaDocumento As New List(Of Entidades.Documento)

        '***************** Factura No Afecta a I.G.V.
        If Not IsNothing(listaDetalleConcepto) Then
            For p As Integer = 0 To listaDetalleConcepto.Count - 1
                If listaDetalleConcepto(p).NoAfectoIGV Then
                    porcentIgv = 0
                End If
            Next
        End If



        Try



            '***************** Declaramos una lista que contendra las listas de los 
            '***************** detalles de los documentos a registrar
            Dim lista_listaDetalleDocumento As New List(Of List(Of Entidades.DetalleDocumento))

            '****************** Realizamos la separacion por AFECTOS vs NO AFECTOS
            lista_listaDetalleDocumento = separarListaDetalleDocumento_Afecto_NoAfecto(listaDetalleDocumento, separarAfectos_NoAfectos)

            '************************ Ordenamos la lista_listaDetalleDocumento x importe
            lista_listaDetalleDocumento = ordenarListaDetalleDocumento(lista_listaDetalleDocumento)

            '****************** Realizamos la separación por Monto Maximo y Nro Filas del detalle
            lista_listaDetalleDocumento = separarListaDetalleDocumentoxMontoxNroFilasDetalleDoc(separarxMontos, lista_listaDetalleDocumento, objDocumento.IdTipoDocumento, objDocumento.IdEmpresa, objDocumento.IdMoneda)

            '****************** Hallamos los documentos junto con sus totales correspondientes (NO SE CONSIDERA EL TOTAL DE LOS CONCEPTOS ADICIONALES)

            listaDocumento = obtenerListaDocumentos(objDocumento, saltarCorrelativo, lista_listaDetalleDocumento, cobrarPercepcion, porcentIgv, objAnexoDocumento.TotalConcepto)

            '****************** Agregamos el Total de Concepto al Primer Documento
            If (objAnexoDocumento IsNot Nothing) Then

                If (listaDocumento.Count <= 0) Then

                    objDocumentoOriginal.ImporteTotal = 0
                    objDocumentoOriginal.Total = 0
                    objDocumentoOriginal.TotalAPagar = 0

                    listaDocumento.Add(objDocumentoOriginal)

                End If

                If (lista_listaDetalleDocumento.Count <= 0) Then

                    lista_listaDetalleDocumento.Add(listaDetalleDocumento)

                End If

                For i As Integer = 0 To listaDocumento.Count - 1

                    If (listaDocumento(i).IdEstadoDoc = 1) Then

                        ' Esto es por el tema de anticipo (concepto negativo)
                        If listaDocumento(i).PorcentPercepcion > 0 And listaDocumento(i).Percepcion > 0 Then
                            listaDocumento(i).Percepcion = (listaDocumento(i).Total + objAnexoDocumento.TotalConceptoAbs) * listaDocumento(i).PorcentPercepcion
                        End If

                        listaDocumento(i).Total = listaDocumento(i).Total + objAnexoDocumento.TotalConcepto
                        If porcentIgv <= 0 Then
                            listaDocumento(i).SubTotal = listaDocumento(i).Total
                        Else
                            listaDocumento(i).SubTotal = listaDocumento(i).Total / (1 + (porcentIgv / 100))
                        End If

                        listaDocumento(i).IGV = listaDocumento(i).Total - listaDocumento(i).SubTotal

                        listaDocumento(i).TotalAPagar = listaDocumento(i).Total + listaDocumento(i).Percepcion
                        listaDocumento(i).TotalLetras = (New Negocio.ALetras).Letras(CStr(Math.Round(listaDocumento(i).TotalAPagar, 2, MidpointRounding.AwayFromZero))) + "/100 " + (New Negocio.Moneda).SelectxId(listaDocumento(i).IdMoneda)(0).Descripcion

                        Exit For

                    End If

                Next

            End If

            cn = objConexion.ConexionSIGE

            '********************** SI ES VISTA PREVIA
            If vistaPrevia Then

                Return obtenerListaVistaPrevia(listaDocumento)

            End If

            '***************** Obtenemos la ListaPagoCaja por Documento
            Dim lista_listaPagoCajaNew As List(Of List(Of Entidades.PagoCaja)) = obtenerListaPagoCajaxDocumento(listaDocumento, listaCancelacion, listaVuelto)


            '************************* FUNCIONALIDAD PARA KITS
            '*********** POR REFERENCIA TENEMOS EL < lista_listaDetalleDocumento >
            ActualizarListaDetalleDocumento_KIT(objDocumento.IdMoneda, objDocumento.IdTienda, objDocumento.IdTipoPV, lista_listaDetalleDocumento)

            '******************* INICIAMOS EL PROCESO EN LA BASE DE DATOS
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            objDaoDocumento = New DAO.DAODocumento
            Dim objDaoAnexoDocumento As New DAO.DAOAnexo_Documento
            Dim objDaoDetalleConcepto As New DAO.DAODetalleConcepto
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            Dim IdPersona As Integer = 0


            Select Case frmModo


                Case 1 '*********** NUEVO

                    For i As Integer = 0 To listaDocumento.Count - 1

                        If (i = 0) Then  '***** El Primer Documento no utiliza Alias
                            IdPersona = listaDocumento(i).IdPersona '******** Persona que origino toda la transaccion.
                            listaDocumento(i).Id = objDaoDocumento.InsertaDocumento(cn, listaDocumento(i), tr)
                        Else
                            If useAlias_Random Then
                                Dim IdAlias As Integer = (New DAO.DAOAlias).SelectIdPersonaAliasRandom(cn, tr)
                                If IdAlias > 0 Then
                                    listaDocumento(i).IdPersona = IdAlias
                                End If
                            End If
                            listaDocumento(i).Id = objDaoDocumento.InsertaDocumento(cn, listaDocumento(i), tr)
                        End If


                        '************************ INSERTAMOS EL ANEXO DOCUMENTO  (ACUMULAMOS LOS TOTALES)
                        If ((i = 0) And (objAnexoDocumento IsNot Nothing) And (listaDocumento(i).IdEstadoDoc = 1)) Then
                            objAnexoDocumento.IdDocumento = listaDocumento(i).Id
                            objDaoAnexoDocumento.InsertAnexo_Documento(objAnexoDocumento, cn, tr, False) '***** El Primer Documento no utiliza Alias
                            '*************** INSERTAMOS DETALLE CONCEPTO
                            objDaoDetalleConcepto.GrabarDetalleConceptoT(cn, listaDocumento(i).Id, listaDetalleConcepto, tr, True)

                        ElseIf ((objAnexoDocumento IsNot Nothing) And (listaDocumento(i).IdEstadoDoc = 1)) Then
                            If (useAlias_Random Or objAnexoDocumento.IdMaestroObra <> Nothing) Then
                                objAnexoDocumento.IdDocumento = listaDocumento(i).Id
                                objAnexoDocumento.TotalConcepto = 0
                                If useAlias_Random Then objAnexoDocumento.IdAlias = IdPersona
                                objDaoAnexoDocumento.InsertAnexo_Documento(objAnexoDocumento, cn, tr, useAlias_Random)
                            End If
                        End If

                        '************* Insertamos Relacion Documento   (Activo)
                        If ((listaDocumento(i).IdEstadoDoc = 1) And (objRelacionDocumento IsNot Nothing)) Then
                            objRelacionDocumento.IdDocumento2 = listaDocumento(i).Id
                            objDaoRelacionDocumento.InsertaRelacionDocumento(objRelacionDocumento, cn, tr)
                        End If

                        '******************** ANEXO FACTURA TRANSPORTE
                        If (objAnexo_FacturaTransporte IsNot Nothing) Then
                            objAnexo_FacturaTransporte.IdDocumento = listaDocumento(i).Id
                            objAnexo_FacturaTransporte.IdDocumento = (New DAO.DAOAnexo_FacturaTransporte).registrar(objAnexo_FacturaTransporte, cn, tr)
                        End If
                    Next

                Case 2 '****** EDITAR

                    If (listaDocumento.Count > 1) Then Throw New Exception("Se han generado más de 2 documentos en el proceso de Edición. No se permite la Operación.")

                    For i As Integer = 0 To listaDocumento.Count - 1

                        objDaoDocumentoFacturacion.DocumentoFacturacion_DeshacerMov(listaDocumento(i).Id, True, True, True, True, True, True, True, True, True, True, True, True, True, True, False, cn, tr)

                        objDaoDocumento.DocumentoUpdate(listaDocumento(i), cn, tr)

                        '************************ INSERTAMOS EL ANEXO DOCUMENTO  (ACUMULAMOS LOS TOTALES)
                        If ((i = 0) And (objAnexoDocumento IsNot Nothing) And (listaDocumento(i).IdEstadoDoc = 1)) Then
                            objAnexoDocumento.IdDocumento = listaDocumento(i).Id
                            objDaoAnexoDocumento.InsertAnexo_Documento(objAnexoDocumento, cn, tr, False) '***** El Primer Documento no utiliza Alias

                            '*************** INSERTAMOS DETALLE CONCEPTO
                            objDaoDetalleConcepto.GrabarDetalleConceptoT(cn, listaDocumento(i).Id, listaDetalleConcepto, tr, True)

                        ElseIf ((objAnexoDocumento IsNot Nothing) And (listaDocumento(i).IdEstadoDoc = 1)) Then
                            If (useAlias_Random Or objAnexoDocumento.IdMaestroObra <> Nothing) Then
                                objAnexoDocumento.IdDocumento = listaDocumento(i).Id
                                objAnexoDocumento.TotalConcepto = 0
                                objDaoAnexoDocumento.InsertAnexo_Documento(objAnexoDocumento, cn, tr, useAlias_Random)
                            End If
                        End If

                        '************* Insertamos Relacion Documento   (Activo)
                        If ((listaDocumento(i).IdEstadoDoc = 1) And (objRelacionDocumento IsNot Nothing)) Then
                            objRelacionDocumento.IdDocumento2 = listaDocumento(i).Id
                            objDaoRelacionDocumento.InsertaRelacionDocumento(objRelacionDocumento, cn, tr)
                        End If

                        '******************** ANEXO FACTURA TRANSPORTE
                        If (objAnexo_FacturaTransporte IsNot Nothing) Then
                            objAnexo_FacturaTransporte.IdDocumento = listaDocumento(i).Id
                            objAnexo_FacturaTransporte.IdDocumento = (New DAO.DAOAnexo_FacturaTransporte).registrar(objAnexo_FacturaTransporte, cn, tr)
                        End If

                    Next

            End Select



            '************************* VARIABLES DE PROCESO
            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            Dim objDaoAnexoDetalleDocumento As New DAO.DAOAnexo_DetalleDocumento
            Dim objDAOMontoRegimen As New DAO.DAOMontoRegimen
            Dim objDaoMovCuenta As New DAO.DAOMovCuenta
            Dim objDAOMovCaja As New DAO.DAOMovCaja
            Dim objDaoObservacion As New DAO.DAOObservacion
            Dim objDaoPuntoLlegada As New DAO.DAOPuntoLlegada
            Dim objDaoPuntoPartida As New DAO.DAOPuntoPartida

            '********************* Insertamos los detalles
            For i As Integer = 0 To lista_listaDetalleDocumento.Count - 1

                Dim salto As Integer = CInt(IIf(saltarCorrelativo = True, 2, 1))

                Dim ObjDocumentoAux As Entidades.Documento = listaDocumento(i * salto)

                objDaoDetalleDocumento.InsertaDetalleDocumento_MovAlmacen(cn, lista_listaDetalleDocumento(i), tr, ObjDocumentoAux.Id _
                                                                          , moverAlmacen, comprometerStock, ObjDocumentoAux.FactorMov, ObjDocumentoAux.IdAlmacen)

                '***************** INSERTAMOS EL ANEXO DETALLE DOCUMENTO
                For x As Integer = 0 To lista_listaDetalleDocumento(i).Count - 1
                    Dim objAnexoDetalleDocumento As New Entidades.Anexo_DetalleDocumento
                    With objAnexoDetalleDocumento
                        .IdDetalleDocumento = lista_listaDetalleDocumento(i)(x).IdDetalleDocumento
                        .IdDocumento = ObjDocumentoAux.Id
                        .IdTipoPV = lista_listaDetalleDocumento(i)(x).IdTipoPV

                        .ComponenteKit = lista_listaDetalleDocumento(i)(x).ComponenteKit
                        .IdKit = lista_listaDetalleDocumento(i)(x).IdKit

                        .IdCampania = lista_listaDetalleDocumento(i)(x).IdCampania
                        .IdProductoRef = lista_listaDetalleDocumento(i)(x).IdProductoAux


                    End With
                    objDaoAnexoDetalleDocumento._Anexo_DetalleDocumentoInsert(objAnexoDetalleDocumento, cn, tr)
                Next

                '***************** Eliminamos al maestro de obra de la cotizacion
                If objRelacionDocumento IsNot Nothing Then
                    If ObjDocumentoAux.DeleteIdMaestroObra Then
                        objDaoAnexoDocumento.FacturacionDeleteMaestro(objRelacionDocumento.IdDocumento1, cn, tr)
                    End If
                End If

                '***************** Insertamos el punto de llegada
                If objPuntoLlegada IsNot Nothing Then

                    objPuntoLlegada.IdDocumento = ObjDocumentoAux.Id
                    objDaoPuntoLlegada.InsertaPuntoLlegadaT(cn, objPuntoLlegada, tr)

                End If
                '***************** Insertamos el punto de PARTIDA
                If objPuntoPartida IsNot Nothing Then

                    objPuntoPartida.IdDocumento = ObjDocumentoAux.Id
                    objDaoPuntoPartida.InsertaPuntoPartida(objPuntoPartida, cn, tr)

                End If

                '***************** Agregar EL Monto del Regimen: Percepción,Detracción, Retención, etc
                If ObjDocumentoAux.Percepcion > 0 Then

                    '********* 1 : Percepcion // Tabla RegimenOperacion

                    Dim objMontoRegimen As New Entidades.MontoRegimen(ObjDocumentoAux.Id, ObjDocumentoAux.Percepcion, 1)
                    objDAOMontoRegimen.InsertaMontoRegimen(cn, objMontoRegimen, tr)

                    '************* INSERTAMOS EN DETALLE DOCUMENTO REGIMEN 
                    Dim objDaoDetalleDocumentoR As New DAO.DAODetalleDocumentoR
                    objDaoDetalleDocumentoR.InsertaDetalleDocumentoR(ObjDocumentoAux.Id, ObjDocumentoAux.Id, ObjDocumentoAux.IdMoneda, ObjDocumentoAux.FechaEmision, ObjDocumentoAux.Codigo, ObjDocumentoAux.Serie, ObjDocumentoAux.Total, ObjDocumentoAux.PorcentPercepcion * 100, 0, ObjDocumentoAux.Percepcion, ObjDocumentoAux.TotalAPagar, cn, tr)

                End If

                If (objMontoRegimenRetencion IsNot Nothing) Then

                    objMontoRegimenRetencion.IdDocumento = ObjDocumentoAux.Id
                    objDAOMontoRegimen.InsertaMontoRegimen(cn, objMontoRegimenRetencion, tr)

                End If

                If (objMontoRegimenDetraccion IsNot Nothing) Then

                    objMontoRegimenDetraccion.IdDocumento = ObjDocumentoAux.Id
                    objDAOMontoRegimen.InsertaMontoRegimen(cn, objMontoRegimenDetraccion, tr)

                End If

                '**************** Agrega el Movimiento de Caja
                If objMovCaja IsNot Nothing Then

                    objMovCaja.IdDocumento = ObjDocumentoAux.Id
                    objDAOMovCaja.InsertaMovCaja(cn, objMovCaja, tr)

                    '******************** Insertamos los pago caja

                    Dim listaPagoCaja As List(Of Entidades.PagoCaja) = lista_listaPagoCajaNew(i)

                    Dim objDaoPagoCaja As New DAO.DAOPagoCaja

                    For j As Integer = 0 To listaPagoCaja.Count - 1

                        listaPagoCaja(j).IdDocumento = objMovCaja.IdDocumento
                        objDaoPagoCaja.InsertaPagoCaja(cn, listaPagoCaja(j), tr)

                        If (listaPagoCaja(j).IdMedioPagoInterfaz = 5) Then  '********** ACTUALIZAMOS SALDOS DE NOTA DE CREDITO

                            Dim objDaoDocumentoNotaCredito As New DAO.DAODocumentoNotaCredito
                            objDaoDocumentoNotaCredito.DocumentoNotaCredito_DisminuirSaldo_Ventas(CInt(listaPagoCaja(j).NumeroCheque), listaPagoCaja(j).Efectivo, cn, tr)

                            '******************** Insertamos el Documento relacionado
                            objDaoRelacionDocumento.InsertaRelacionDocumento(New Entidades.RelacionDocumento(CInt(listaPagoCaja(j).NumeroCheque), ObjDocumentoAux.Id), cn, tr)

                        End If

                    Next

                End If

                '********************** Insertamos el MovCuenta
                If objMovCuenta IsNot Nothing Then

                    With objMovCuenta

                        .Monto = ObjDocumentoAux.TotalAPagar
                        .Saldo = ObjDocumentoAux.TotalAPagar
                        .IdDocumento = ObjDocumentoAux.Id

                    End With

                    objDaoMovCuenta.InsertaMovCuenta(cn, objMovCuenta, tr)

                End If

                '******************** OBSERVACIONES
                If (objObservacion IsNot Nothing) Then

                    objObservacion.IdDocumento = ObjDocumentoAux.Id
                    objDaoObservacion.InsertaObservacionT(cn, tr, objObservacion)

                End If

            Next


            Dim objDaoAnexoMovBanco As New DAO.DAOAnexo_MovBanco
            For i As Integer = 0 To listaMovBanco.Count - 1
                listaMovBanco(i).IdMovBanco = objDaoDocumentoFacturacion.MovBancoInsert_Venta(listaMovBanco(i), cn, tr) '******** INSERTAMOS MOV BANCO

                If listaMovBanco(i).IdMovBanco <> Nothing Then
                    For j As Integer = 0 To listaDocumento.Count - 1

                        If (listaDocumento(j).IdEstadoDoc = 1) Then '*********** ACTIVO
                            objDaoAnexoMovBanco.Anexo_MovBancoInsert(New Entidades.Anexo_MovBanco(listaMovBanco(i).IdMovBanco, listaMovBanco(i).IdCuentaBancaria, Nothing, listaDocumento(j).Id, listaMovBanco(i).IdBanco), cn, tr)
                        End If

                    Next
                End If


            Next


            Dim objDaoCheque As New DAO.DAOCheque
            Dim objDaoCheque_DocumentoRef As New DAO.DAOCheque_DocumentoRef
            If (listaCheque IsNot Nothing) Then
                For i As Integer = 0 To listaCheque.Count - 1

                    If (listaCheque(i).IdCheque = Nothing) Then  '************ NO HA SIDO REGISTRADO

                        listaCheque(i).IdCheque = objDaoCheque.Registrar(listaCheque(i), cn, tr)

                    End If

                    For j As Integer = 0 To listaDocumento.Count - 1

                        If (listaDocumento(j).IdEstadoDoc = 1) Then '*********** ACTIVO

                            Dim objCheque_DocumentoRef As New Entidades.Cheque_DocumentoRef
                            With objCheque_DocumentoRef
                                .IdCheque = listaCheque(i).IdCheque
                                .IdDocumentoRef = listaDocumento(j).Id
                            End With

                            objDaoCheque_DocumentoRef.Registrar(objCheque_DocumentoRef, cn, tr)
                        End If
                    Next
                Next
            End If

            ''***************** GENERAMOS LAS ORDENES DE DESPACHO
            'If (generarDespacho) Then
            '    Dim objDaoOrdenDespacho As New DAO.DAODocOrdenDespacho
            '    For i As Integer = 0 To listaDocumento.Count - 1

            '        objDaoOrdenDespacho.DocumentoOrdenDespacho_GenerarDespachoAutomatico(listaDocumento(i).IdAlmacen, Nothing, listaDocumento(i).IdUsuario, 21, Nothing, listaDocumento(i).Id, cn, tr)

            '    Next
            'End If
            Dim id As Integer = 0
            Dim obj As New DAO.DAODocumento
            For i As Integer = 0 To listaDocumento.Count - 1
                id = listaDocumento(i).Id
            Next

            tr.Commit()
            'Si termina correctamente la transaccion
            obj.crearFacturaElectronica(id, cn)

        Catch ex As Exception

            tr.Rollback()
            Throw ex

        Finally

            If cn.State = ConnectionState.Open Then cn.Close()

        End Try

        Return listaDocumento

    End Function

    Private Sub ActualizarListaDetalleDocumento_KIT(ByVal IdMoneda As Integer, ByVal IdTienda As Integer, ByVal IdTipoPV As Integer, ByVal lista_ListaDetalleDocumento As List(Of List(Of Entidades.DetalleDocumento)))

        For i As Integer = 0 To lista_ListaDetalleDocumento.Count - 1


            For x As Integer = 0 To lista_ListaDetalleDocumento(i).Count - 1

                If (lista_ListaDetalleDocumento(i)(x).Kit) Then  '*********** KIT

                    '****************** OBTENEMOS EL DETALLE DOCUMENTO
                    Dim listaComponenteKit As List(Of Entidades.Kit) = (New Negocio.Kit).SelectComponenteDetallexParams(lista_ListaDetalleDocumento(i)(x).IdProducto, lista_ListaDetalleDocumento(i)(x).IdUnidadMedida, lista_ListaDetalleDocumento(i)(x).Cantidad, IdMoneda, IdTienda, IdTipoPV)

                    For j As Integer = 0 To listaComponenteKit.Count - 1

                        Dim objDetalleDocumento As New Entidades.DetalleDocumento
                        With objDetalleDocumento

                            .ComponenteKit = True
                            .IdKit = lista_ListaDetalleDocumento(i)(x).IdProducto

                            .IdProducto = listaComponenteKit(j).IdComponente
                            .IdUnidadMedida = listaComponenteKit(j).IdUnidadMedida_Comp
                            .Cantidad = listaComponenteKit(j).CantidadTotal_Comp
                            .PrecioLista = listaComponenteKit(j).PrecioListaUnitEqKit_Comp
                            .PrecioCD = listaComponenteKit(j).PrecioUnitEqKit_Comp
                            .PrecioSD = listaComponenteKit(j).PrecioUnitEqKit_Comp
                            .Importe = .PrecioCD * .Cantidad
                            .UMedida = listaComponenteKit(j).UnidadMedida_Comp

                            .IdTipoPV = IdTipoPV

                        End With

                        '******************** AGREGAMOS EL COMPONENTE AL DETALLE
                        lista_ListaDetalleDocumento(i).Add(objDetalleDocumento)

                    Next

                End If


            Next

        Next

    End Sub

    Private Function obtenerListaPagoCajaxDocumento(ByVal listaDocumento As List(Of Entidades.Documento), ByVal listaCancelacion As List(Of Entidades.PagoCaja), ByVal listaVuelto As List(Of Entidades.PagoCaja)) As List(Of List(Of Entidades.PagoCaja))

        Dim lista_ListaPagoCaja As New List(Of List(Of Entidades.PagoCaja))
        Dim objUtil As New Negocio.Util

        For i As Integer = 0 To listaDocumento.Count - 1

            If listaDocumento(i).IdEstadoDoc <> 3 Then

                Dim totalAPagarNew As Decimal = listaDocumento(i).TotalAPagar

                Dim listaAux As New List(Of Entidades.PagoCaja)

                If i = listaDocumento.Count - 1 Then

                    listaAux = unir_ListaCancelacion_ListaVuelto(listaDocumento(i), listaCancelacion, listaVuelto)
                    lista_ListaPagoCaja.Add(listaAux)
                    Return lista_ListaPagoCaja

                End If

                Dim j As Integer = 0

                While (j <= (listaCancelacion.Count - 1))

                    Dim Efectivo_Equivalente As Decimal = objUtil.CalcularValorxTipoCambio(listaCancelacion(j).Efectivo, listaDocumento(i).IdMoneda, listaCancelacion(j).IdMoneda, "E", listaDocumento(i).FechaEmision)

                    If totalAPagarNew < Efectivo_Equivalente Then

                        '************ Si el total a pagar es menor al efectivo
                        Dim objCancelacion As Entidades.PagoCaja = listaCancelacion(j).getClone

                        objCancelacion.Efectivo = objUtil.CalcularValorxTipoCambio(totalAPagarNew, listaDocumento(i).IdMoneda, _
                                                                                          listaCancelacion(j).IdMoneda, "E", listaDocumento(i).FechaEmision)
                        listaAux.Add(objCancelacion)

                        listaCancelacion(j).Efectivo = listaCancelacion(j).Efectivo - objCancelacion.Efectivo


                        Exit While

                    ElseIf totalAPagarNew = Efectivo_Equivalente Then

                        '************ Si el total a pagar es igual al efectivo
                        listaAux.Add(listaCancelacion(j).getClone)
                        listaCancelacion.RemoveAt(j)

                        Exit While

                    Else

                        listaAux.Add(listaCancelacion(j).getClone)
                        listaCancelacion.RemoveAt(j)

                        totalAPagarNew = totalAPagarNew - Efectivo_Equivalente

                        j = 0 '************** Reiniciamos el contador

                    End If


                End While

                lista_ListaPagoCaja.Add(listaAux)

            End If

        Next

        Return lista_ListaPagoCaja

    End Function
    Private Function unir_ListaCancelacion_ListaVuelto(ByVal objDocumento As Entidades.Documento, ByVal listaCancelacion As List(Of Entidades.PagoCaja), ByVal listaVuelto As List(Of Entidades.PagoCaja)) As List(Of Entidades.PagoCaja)

        Dim IdMedioPagoPrincipal As Integer = (New Negocio.MedioPago).SelectIdMedioPagoPrincipal

        If listaVuelto.Count = 0 Then

            '**************** No se ha dado vuelto en diferentes monedas
            For i As Integer = 0 To listaCancelacion.Count - 1

                If listaCancelacion(i).IdMedioPago = IdMedioPagoPrincipal And listaCancelacion(i).IdMoneda = objDocumento.IdMoneda Then

                    '************** Si es efectivo y el IdMoneda es el mismo que el IdMonedaEmision
                    listaCancelacion(i).Vuelto = objDocumento.Vuelto
                    Return listaCancelacion

                End If

            Next

            '**************** De no encontrarse, creo un nuevo objeto solo si el vuelto es > 0

            If objDocumento.Vuelto > 0 Then

                Dim objPagoCaja As New Entidades.PagoCaja

                With objPagoCaja

                    .Factor = 1
                    .IdMedioPago = IdMedioPagoPrincipal
                    .IdMoneda = objDocumento.IdMoneda
                    .IdTipoMovimiento = 1
                    .Vuelto = objDocumento.Vuelto
                    .Efectivo = objDocumento.Vuelto * -1

                End With

                listaCancelacion.Add(objPagoCaja)

            End If

        Else

            '******************** Se ha dado vuelto en otra, en la misma y/o varias monedas

            For i As Integer = 0 To listaVuelto.Count - 1

                Dim addPagoCaja As Boolean = True

                For j As Integer = 0 To listaCancelacion.Count - 1

                    If (listaVuelto(i).IdMedioPago = listaCancelacion(j).IdMedioPago) And (listaVuelto(i).IdMoneda = listaCancelacion(j).IdMoneda) Then

                        listaCancelacion(j).Vuelto = listaVuelto(i).Efectivo
                        addPagoCaja = False
                        Exit For

                    End If

                Next

                If addPagoCaja Then

                    '******************* Si no existe, agrego un nuevo PagoCaja
                    Dim objPagoCaja As New Entidades.PagoCaja

                    With objPagoCaja

                        .Factor = listaVuelto(i).Factor
                        .IdMedioPago = listaVuelto(i).IdMedioPago
                        .IdMoneda = listaVuelto(i).IdMoneda
                        .IdTipoMovimiento = listaVuelto(i).IdTipoMovimiento
                        .Vuelto = listaVuelto(i).Efectivo
                        .Efectivo = objDocumento.Vuelto * -1

                    End With

                    listaCancelacion.Add(objPagoCaja)

                End If


            Next

        End If

        Return listaCancelacion

    End Function
    Private Function separarListaDetalleDocumento_Afecto_NoAfecto(ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal separarAfectos_NoAfectos As Boolean) As List(Of List(Of Entidades.DetalleDocumento))

        '************ Declaramos la lista que contendrá la lista de afectos y no afectos
        Dim lista_ListaDetalleDocumento As New List(Of List(Of Entidades.DetalleDocumento))

        If separarAfectos_NoAfectos Then

            '************* Declaramos las listas de afectos y no afectos
            '************* SOLO TRABAJAMOS CON AFECTOS A PERCEPCION Y NO AFECTOS
            Dim listaAfecto As New List(Of Entidades.DetalleDocumento)
            Dim listaNOAfecto As New List(Of Entidades.DetalleDocumento)

            For i As Integer = 0 To listaDetalleDocumento.Count - 1

                If listaDetalleDocumento(i).TasaPercepcion > 0 Then

                    listaAfecto.Add(listaDetalleDocumento(i))

                Else

                    listaNOAfecto.Add(listaDetalleDocumento(i))

                End If

            Next

            '******************** Agregamos la Listas en el Orden :  NO Afecto  -   Afecto
            lista_ListaDetalleDocumento.Add(listaNOAfecto)
            lista_ListaDetalleDocumento.Add(listaAfecto)

        Else

            lista_ListaDetalleDocumento.Add(listaDetalleDocumento)

        End If

        Return lista_ListaDetalleDocumento

    End Function
    Private Function ordenarListaDetalleDocumento(ByVal lista_ListaDetalleDocumento As List(Of List(Of Entidades.DetalleDocumento))) As List(Of List(Of Entidades.DetalleDocumento))

        Dim lista_ListaDetalleDocumentoRetorno As New List(Of List(Of Entidades.DetalleDocumento))

        For i As Integer = 0 To lista_ListaDetalleDocumento.Count - 1

            Dim lista As List(Of Entidades.DetalleDocumento) = lista_ListaDetalleDocumento(i)

            For j As Integer = 0 To lista.Count - 2

                For k As Integer = j + 1 To lista.Count - 1

                    If lista(j).Importe > lista(k).Importe Then

                        Dim aux As Entidades.DetalleDocumento = lista(j)
                        lista(j) = lista(k)
                        lista(k) = aux

                    End If

                Next

            Next

            lista_ListaDetalleDocumentoRetorno.Add(lista)

        Next

        Return lista_ListaDetalleDocumentoRetorno

    End Function
    Private Function separarListaDetalleDocumentoxMontoxNroFilasDetalleDoc(ByVal separarxMonto As Boolean, ByVal lista_ListaDetalleDocumento As List(Of List(Of Entidades.DetalleDocumento)), ByVal IdTipoDocumento As Integer, ByVal IdEmpresa As Integer, ByVal IdMonedaDestino As Integer) As List(Of List(Of Entidades.DetalleDocumento))

        Dim lista_listaDetalleDocumentoRetorno As New List(Of List(Of Entidades.DetalleDocumento))
        Dim separarxMontoOld As Boolean = separarxMonto

        For i As Integer = 0 To lista_ListaDetalleDocumento.Count - 1

            separarxMonto = separarxMontoOld

            Dim lista As List(Of Entidades.DetalleDocumento) = lista_ListaDetalleDocumento(i)
            Dim listaAux As New List(Of Entidades.DetalleDocumento)
            Dim montoAcumulado As Decimal = 0

            '**************** Obtenemos los valores requeridos
            Dim objTipoDocumentoAE As Entidades.TipoDocumentoAE = (New Negocio.TipoDocumentoAE).SelectValoresxParams(IdEmpresa, IdTipoDocumento, IdMonedaDestino)

            If objTipoDocumentoAE Is Nothing Then
                Throw New Exception("No se ha registrado la capacidad de Números de Detalle para este documento.")
            End If

            Dim nroFilasDocumento As Integer = objTipoDocumentoAE.CantidadFilas
            Dim montoMaximo As Decimal = 0

            Select Case i

                Case 0

                    '**************** Primero es la lista de los NO AFECTO caso contrario puede contener a todos los elementos
                    montoMaximo = objTipoDocumentoAE.MontoMaximoNoAfecto

                Case 1

                    '******************* Luego viene la lista de los AFECTO PERCEPCION
                    montoMaximo = objTipoDocumentoAE.MontoMaximoAfecto

            End Select

            If montoMaximo = 0 Then

                montoMaximo = 9999999 '************** damos un valor grande como monto maximo

            End If


            For j As Integer = 0 To lista.Count - 1

                Dim montoAcumuladoPrevio As Decimal = montoAcumulado + lista(j).Importe

                If separarxMonto Then

                    '************** Si el primer monto es mayor al maximo permitido, no separamos los documentos

                    If ((montoAcumuladoPrevio > montoMaximo) And (j = 0)) Then separarxMonto = False

                    If (listaAux.Count = nroFilasDocumento Or montoAcumuladoPrevio > montoMaximo) And (listaAux.Count > 0) Then

                        lista_listaDetalleDocumentoRetorno.Add(listaAux)

                        listaAux = New List(Of Entidades.DetalleDocumento)
                        montoAcumulado = 0

                    End If

                Else

                    If listaAux.Count = nroFilasDocumento Then

                        lista_listaDetalleDocumentoRetorno.Add(listaAux)

                        listaAux = New List(Of Entidades.DetalleDocumento)

                    End If

                End If

                listaAux.Add(lista(j))
                montoAcumulado = montoAcumulado + lista(j).Importe

                '**************** Si queda algun elemento lo añadimos
                If j = (lista.Count - 1) And listaAux.Count > 0 Then

                    lista_listaDetalleDocumentoRetorno.Add(listaAux)

                End If



            Next

        Next

        Return lista_listaDetalleDocumentoRetorno

    End Function
    Private Function obtenerListaDocumentos(ByVal objDocumento As Entidades.Documento, ByVal saltarCorrelativo As Boolean, ByVal lista_ListaDetalleDocumento As List(Of List(Of Entidades.DetalleDocumento)), ByVal cobrarPercepcion As Boolean, ByVal porcentIgv As Decimal, ByVal TotalConcepto As Decimal) As List(Of Entidades.Documento)


        Dim objTipoDocumentoAE As Entidades.TipoDocumentoAE = (New Negocio.TipoDocumentoAE).SelectValoresxParams(objDocumento.IdEmpresa, objDocumento.IdTipoDocumento, objDocumento.IdMoneda)

        '******************* Obtenemos la descripción del total a pagar
        Dim totalALetras As String = ""

        Select Case objDocumento.IdMoneda

            Case 1
                '*********** SOLES
                totalALetras = "/100 SOLES"

            Case 2
                '*********** DOLARES
                totalALetras = "/100 DÓLARES AMERICANOS"
        End Select


        Dim listaDocumento As New List(Of Entidades.Documento)

        '*********************** Hallamos el nro de documentos a guardar
        Dim nroDocumentos As Integer = lista_ListaDetalleDocumento.Count

        If saltarCorrelativo Then

            nroDocumentos = lista_ListaDetalleDocumento.Count * 2 - 1

        End If

        '************************ Reservamos los espacios de guardado
        For i As Integer = 0 To nroDocumentos - 1

            listaDocumento.Add((New Entidades.Documento()))
            listaDocumento(i).IdEstadoDoc = 3 '******* Estado el cual indica que esta como PENDIENTE
            listaDocumento(i).IdSerie = objDocumento.IdSerie
            listaDocumento(i).Serie = objDocumento.Serie
            listaDocumento(i).IdUsuario = objDocumento.IdUsuario
            listaDocumento(i).IdTipoDocumento = objDocumento.IdTipoDocumento
            listaDocumento(i).IdEmpresa = objDocumento.IdEmpresa
            listaDocumento(i).IdTienda = objDocumento.IdTienda

        Next

        '************************* Recorremos el array de listas detalle
        For i As Integer = 0 To lista_ListaDetalleDocumento.Count - 1

            '**************** Declaramos la variables a utilizar
            Dim lista As List(Of Entidades.DetalleDocumento) = lista_ListaDetalleDocumento(i)
            Dim descuentoAcumulado As Decimal = 0
            Dim importeAcumulado As Decimal = 0
            Dim percepcionAcumulado As Decimal = 0
            Dim retencion As Decimal = 0
            Dim TasaPercepcion As Decimal = 0
            Dim cantElem As Integer = 0 '******** agregue estas dos variables 
            Dim cantElemValido As Decimal = 0 '******** para hacerlo rapido ( PERCEPCION - BOLETA)

            For j As Integer = 0 To lista.Count - 1
                '******************* Obtenemos los totales
                descuentoAcumulado += lista(j).Descuento * lista(j).Cantidad
                importeAcumulado += Math.Round(lista(j).Importe, 2, MidpointRounding.AwayFromZero)

                If lista(j).Importe > 0 Then ' **************** ( PERCEPCION - BOLETA)
                    cantElem += 1
                    cantElemValido += lista(j).Cantidad
                End If


                'He comentado la percepcion es por el total del documento no por los items
                'percepcionAcumulado += (lista(j).TasaPercepcion / 100) * lista(j).Importe
                If lista(j).TasaPercepcion > 0 Then
                    TasaPercepcion = (lista(j).TasaPercepcion / 100)
                End If
            Next

            Dim objDocumentoNew As Entidades.Documento = objDocumento.getClone

            With objDocumentoNew

                ' Si el detalle no tiene percepcion no se compromete la percepcion
                If TasaPercepcion = 0 Then .CompPercepcion = False

                .Descuento = Math.Round(descuentoAcumulado, 4)
                .ImporteTotal = Math.Round(importeAcumulado, 4, MidpointRounding.AwayFromZero)
                .Total = Math.Round(importeAcumulado, 4, MidpointRounding.AwayFromZero)



                If (porcentIgv <= 0) Then
                    .SubTotal = .Total
                Else
                    .SubTotal = Math.Round(CDec(.Total / (1 + (porcentIgv / 100))), 4, MidpointRounding.AwayFromZero)
                End If
                .IGV = Math.Round(.Total - .SubTotal, 4, MidpointRounding.AwayFromZero)


                .PorcentPercepcion = TasaPercepcion
                percepcionAcumulado = .Total * TasaPercepcion

                '************* Si es retenedor y es factura
                If objDocumentoNew.IdTipoAgente = 2 And objDocumentoNew.IdTipoDocumento = 1 Then
                    percepcionAcumulado = 0
                    retencion = (objDocumentoNew.TasaAgente / 100) * objDocumentoNew.Total
                End If

                ''************** Si es Boleta 
                If objDocumentoNew.IdTipoDocumento = 3 Then
                    If ((objDocumento.ImporteTotal + TotalConcepto) <= objTipoDocumentoAE.MontoMaximoAfecto And objDocumentoNew.Total <= objTipoDocumentoAE.MontoMaximoAfecto) Or (cantElem = 1 And cantElemValido = 1 And percepcionAcumulado > 0) Then
                        percepcionAcumulado = 0
                        retencion = 0
                    End If
                End If

                If cobrarPercepcion = False Then
                    percepcionAcumulado = 0
                End If


                '.Percepcion = Math.Round(percepcionAcumulado, 2)
                .Percepcion = percepcionAcumulado
                .TotalAPagar = Math.Round(.Total + percepcionAcumulado, 2, MidpointRounding.AwayFromZero)

                .TotalLetras = (New ALetras).Letras(Math.Round(.TotalAPagar, 2, MidpointRounding.AwayFromZero).ToString) + totalALetras

            End With

            '******************* Obtenemos el salto de Documento
            Dim salto As Integer = CInt(IIf(saltarCorrelativo = True, 2, 1))

            listaDocumento(i * salto) = objDocumentoNew

        Next

        Return listaDocumento

    End Function
    Public Function obtenerListaVistaPrevia(ByVal listaDocumento As List(Of Entidades.Documento)) As List(Of Entidades.Documento)

        Dim listaDocumentoAux As New List(Of Entidades.Documento)

        For i As Integer = 0 To listaDocumento.Count - 1

            If listaDocumento(i).IdEstadoDoc <> 3 Then

                listaDocumentoAux.Add(listaDocumento(i).getClone)

            End If

        Next

        Return listaDocumentoAux

    End Function


End Class
