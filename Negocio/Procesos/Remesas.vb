﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class Remesas

    Dim cn As SqlConnection
    Dim objConexion As DAO.Conexion
    Dim tr As SqlTransaction = Nothing
    Dim objDaoRemesa As DAO.DAORemesa



    Public Sub Insert(ByVal objDocumento As Entidades.Documento, ByVal listaMovBanco As List(Of Entidades.MovBancoView))

        objConexion = New DAO.Conexion
        cn = objConexion.ConexionSIGE

        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            Dim DAOAnexo_MovBanco As DAO.DAOAnexo_MovBanco
            Dim DAOMovBanco_Cheque As DAO.DAOMovBanco_Cheque
            Dim DAORelacionDocumento As DAO.DAORelacionDocumento

            objDocumento.Id = (New DAO.DAODocumento).InsertaDocumento(cn, objDocumento, tr)

            For i As Integer = 0 To listaMovBanco.Count - 1

                listaMovBanco(i).IdMovBanco = (New DAO.DAOMovBanco).InsertT_GetID(listaMovBanco(i), cn, tr)

                DAOAnexo_MovBanco = New DAO.DAOAnexo_MovBanco
                DAOAnexo_MovBanco.Anexo_MovBancoInsert(New Entidades.Anexo_MovBanco(listaMovBanco(i).IdMovBanco, listaMovBanco(i).IdCuentaBancaria, Nothing, objDocumento.Id, listaMovBanco(i).IdDocumentoRef2, listaMovBanco(i).IdBanco), cn, tr)

                If listaMovBanco(i).IdDocumentoRef2 > 0 Then
                    DAORelacionDocumento = New DAO.DAORelacionDocumento

                    DAORelacionDocumento.InsertaRelacionDocumento(New Entidades.RelacionDocumento(listaMovBanco(i).IdDocumentoRef2, objDocumento.Id), cn, tr)

                End If

                If listaMovBanco(i).IdCheque > 0 Then
                    DAOMovBanco_Cheque = New DAO.DAOMovBanco_Cheque

                    DAOMovBanco_Cheque.MovBanco_Cheque_Transaction(New Entidades.MovBanco_Cheque(listaMovBanco(i).IdMovBanco, listaMovBanco(i).IdBanco, listaMovBanco(i).IdCuentaBancaria, listaMovBanco(i).IdCheque), cn, tr)

                End If


            Next

            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

    End Sub


    Public Sub Editar(ByVal objDocumento As Entidades.Documento, ByVal listaMovBanco As List(Of Entidades.MovBancoView))

        objConexion = New DAO.Conexion
        cn = objConexion.ConexionSIGE

        Try

            Dim DAOAnexo_MovBanco As DAO.DAOAnexo_MovBanco
            Dim DAOMovBanco_Cheque As DAO.DAOMovBanco_Cheque
            Dim DAORelacionDocumento As DAO.DAORelacionDocumento
            Dim DAORemesa As New DAO.DAORemesa

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            DAORemesa.Anular(objDocumento.Id, True, True, False, cn, tr)

            For i As Integer = 0 To listaMovBanco.Count - 1

                listaMovBanco(i).IdMovBanco = (New DAO.DAOMovBanco).InsertT_GetID(listaMovBanco(i), cn, tr)

                DAOAnexo_MovBanco = New DAO.DAOAnexo_MovBanco
                DAOAnexo_MovBanco.Anexo_MovBancoInsert(New Entidades.Anexo_MovBanco(listaMovBanco(i).IdMovBanco, listaMovBanco(i).IdCuentaBancaria, Nothing, objDocumento.Id, listaMovBanco(i).IdDocumentoRef2, listaMovBanco(i).IdBanco), cn, tr)

                If listaMovBanco(i).IdDocumentoRef2 > 0 Then
                    DAORelacionDocumento = New DAO.DAORelacionDocumento

                    DAORelacionDocumento.InsertaRelacionDocumento(New Entidades.RelacionDocumento(listaMovBanco(i).IdDocumentoRef2, objDocumento.Id), cn, tr)

                End If

                If listaMovBanco(i).IdCheque > 0 Then
                    DAOMovBanco_Cheque = New DAO.DAOMovBanco_Cheque

                    DAOMovBanco_Cheque.MovBanco_Cheque_Transaction(New Entidades.MovBanco_Cheque(listaMovBanco(i).IdMovBanco, listaMovBanco(i).IdBanco, listaMovBanco(i).IdCuentaBancaria, listaMovBanco(i).IdCheque), cn, tr)

                End If


            Next

            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

    End Sub


    Public Function Anular(ByVal IdDocumento As Integer) As Boolean

        objDaoRemesa = New DAO.DAORemesa
        objConexion = New DAO.Conexion
        cn = objConexion.ConexionSIGE

        Dim hecho As Boolean = False
        cn = objConexion.ConexionSIGE

        Try

            cn.Open()
            tr = cn.BeginTransaction

            objDaoRemesa.Anular(IdDocumento, True, True, True, cn, tr)

            hecho = True

            tr.Commit()
            cn.Close()
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function


End Class
