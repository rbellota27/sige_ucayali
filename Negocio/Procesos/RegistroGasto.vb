﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'viernes 12 de marzo
Public Class RegistroGasto


    Dim obj As New DAO.DAORegistroGasto

    Public Function InsertRegistroGasto(ByVal ObjEnt As Entidades.RegistroGasto.CabeceraRegistroGasto, _
                                         ByVal Lista As List(Of Entidades.RequerimientoGasto), _
                                         ByVal ListaCentroCosto As List(Of Entidades.Centro_costo), _
                                         ByVal objObs As Entidades.Observacion) As String
        Return obj.InsertRegistroGasto(ObjEnt, Lista, ListaCentroCosto, objObs)
    End Function

    Public Function UpdateRegistroGasto(ByVal ObjEnt As Entidades.RegistroGasto.CabeceraRegistroGasto, _
                                             ByVal Lista As List(Of Entidades.RequerimientoGasto), _
                                            ByVal ListaCentroCosto As List(Of Entidades.Centro_costo), _
                                            ByVal objObs As Entidades.Observacion) As String
        Return obj.UpdateRegistroGasto(ObjEnt, Lista, ListaCentroCosto, objObs)
    End Function

    Public Function selectRegistroGasto(ByVal idserie As Integer, ByVal codigo As Integer) As Entidades.RegistroGasto.CabeceraRegistroGasto
        Return obj.selectRegistroGasto(idserie, codigo)
    End Function

    Public Function selectListaRegistroGasto(ByVal IdDocumento As Integer) As List(Of Entidades.RequerimientoGasto)
        Return obj.selectListaRegistroGasto(IdDocumento)
    End Function

End Class
