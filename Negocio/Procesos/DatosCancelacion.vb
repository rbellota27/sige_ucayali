﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class DatosCancelacion
    Protected obj As New DAO.DAODatosCancelacion

    Public Function DeshacerMov_Edicion(ByVal IdDocumento As Integer) As Boolean
        Return obj.DeshacerMov_Edicion(IdDocumento)
    End Function
    Public Function Anular(ByVal IdDocumento As Integer) As Boolean
        Return obj.Anular(IdDocumento)
    End Function
    Public Function SelectListxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.DatosCancelacion)
        Return obj.SelectListxIdDocumento(IdDocumento)
    End Function
End Class
