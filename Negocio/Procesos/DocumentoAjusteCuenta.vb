﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.









'*********************   MIERCOLES 10 MARZO 2010 HORA 02_19 PM














Imports System.Data.SqlClient
Public Class DocumentoAjusteCuenta

    Private cn As SqlConnection
    Private tr As SqlTransaction
    Private cmd As SqlCommand

    Private objDaoDAC As New DAO.DAODocumentoAjusteCuenta

    Public Function DocumentoAjusteCuenta_SelectDetalle(ByVal IdDocumento As Integer) As List(Of Entidades.Documento_MovCuenta)
        Return objDaoDAC.DocumentoAjusteCuenta_SelectDetalle(IdDocumento)
    End Function

    Public Function DocumentoAjusteCuenta_SelectMovCuentaCargoxParams(ByVal IdUsuario As Integer, ByVal IdPersona As Integer, ByVal Serie As Integer, ByVal Codigo As Integer, ByVal FechaInicio As Date, _
                                                                    ByVal FechaFin As Date, ByVal IdTipoDocumento As Integer, ByVal IdtipoDocumentoRef As Integer) As List(Of Entidades.Documento_MovCuenta)
        Return objDaoDAC.DocumentoAjusteCuenta_SelectMovCuentaCargoxParams(IdUsuario, IdPersona, Serie, Codigo, FechaInicio, FechaFin, IdTipoDocumento, IdtipoDocumentoRef)
    End Function

    Public Function registrarDocumento(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objObservaciones As Entidades.Observacion, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento)) As Integer

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            '*********************** INSERTAMOS LA CABECERA
            objDocumento.Id = (New DAO.DAODocumento).InsertaDocumento(cn, objDocumento, tr)

            '********************** INSERTAMOS DETALLE CONCEPTO
            Dim objDaoDetalleConcepto As New DAO.DAODetalleConcepto
            Dim objDaoMovCuenta As New DAO.DAOMovCuenta
            For i As Integer = 0 To listaDetalleConcepto.Count - 1

                listaDetalleConcepto(i).IdDocumento = objDocumento.Id
                objDaoDetalleConcepto.DetalleConceptoInsert(listaDetalleConcepto(i), cn, tr, True)

                '******************* ACTUALIZAMOS MOV CUENTA
                objDaoMovCuenta.UpdateSaldoxIdMovCuenta(listaDetalleConcepto(i).IdMovCuentaRef, listaDetalleConcepto(i).Monto, cn, tr)

            Next


            '******************** RELACION DOCUMENTO
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1

                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)

            Next

            '******************** INSERTAMOS OBSERVACIONES
            If (objObservaciones IsNot Nothing) Then
                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservaciones)
            End If

            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function

    Public Function actualizarDocumento(ByVal objDocumento As Entidades.Documento, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal objObservaciones As Entidades.Observacion, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento)) As Integer

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction


            '****************** DESHACEMOS LOS MOV
            objDaoDAC.DocumentoAjusteCuenta_DeshacerMov(objDocumento.Id, True, True, True, True, True, False, cn, tr)


            '*********************** ACTUALIZAMOS LA CABECERA
            Dim objDaoDocumento As New DAO.DAODocumento
            objDaoDocumento.DocumentoUpdate(objDocumento, cn, tr)

            '********************** INSERTAMOS DETALLE CONCEPTO
            Dim objDaoDetalleConcepto As New DAO.DAODetalleConcepto
            Dim objDaoMovCuenta As New DAO.DAOMovCuenta
            For i As Integer = 0 To listaDetalleConcepto.Count - 1

                listaDetalleConcepto(i).IdDocumento = objDocumento.Id
                objDaoDetalleConcepto.DetalleConceptoInsert(listaDetalleConcepto(i), cn, tr, True)

                '******************* ACTUALIZAMOS MOV CUENTA
                objDaoMovCuenta.UpdateSaldoxIdMovCuenta(listaDetalleConcepto(i).IdMovCuentaRef, listaDetalleConcepto(i).Monto, cn, tr)

            Next


            '******************** RELACION DOCUMENTO
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            For i As Integer = 0 To listaRelacionDocumento.Count - 1

                listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)

            Next

            '******************** INSERTAMOS OBSERVACIONES
            If (objObservaciones IsNot Nothing) Then
                Dim objDaoObservacion As New DAO.DAOObservacion
                objObservaciones.IdDocumento = objDocumento.Id
                objDaoObservacion.InsertaObservacionT(cn, tr, objObservaciones)
            End If

            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function

    Public Function anularDocumento(ByVal IdDocumento As Integer) As Boolean

        Dim hecho As Boolean = False
        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()

            tr = cn.BeginTransaction


            objDaoDAC.DocumentoAjusteCuenta_DeshacerMov(IdDocumento, False, False, False, True, True, True, cn, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function
End Class
