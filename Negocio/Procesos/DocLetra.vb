﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Imports System.Data


Public Class DocLetra

    Private objConexion As New DAO.Conexion
    Private objDaoDocLetra As New DAO.DAODocLetra

    Public Function getDataSetDocumentoPrint(ByVal IdDocumento As Integer) As DataSet

        Dim ds As New DataSet
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Try

            Dim cmd As New SqlCommand("_CR_DocumentoLetraCab", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocumentoLetraCab")

        Catch ex As Exception
            Throw ex
        End Try

        Return ds

    End Function

    Public Function DocumentoLetra_Renegociar(ByVal IdDocumentoLetraxRenegociar As Integer, ByVal objLetraCambioLetraxRenegociar As Entidades.LetraCambio, ByVal listaLetrasCanje As List(Of Entidades.DocLetra), ByVal listaDocumentosRef As List(Of Entidades.Documento_MovCuenta)) As List(Of Entidades.DocLetra)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            ''******* Deshacemos los mov de la letra (Mov Cuenta,Pago prog)
            ''******* Actualizamos el estado de cancelación / el flag de Renegociado
            ''****** deshacemos los mov de los documentos de referencia
            ''******** eliminamos de relacion documento la relacion Letra / Doc referencia
            objDaoDocLetra.DocumentoLetra_DeshacerMov_Renegociar(IdDocumentoLetraxRenegociar, 0, cn, tr)
            'listaLetrasCanje(0).BaseSoles
            'objDaoDocLetra.DocumentoLetra_DeshacerMovimientos(IdDocumentoLetraxRenegociar, True, True, False, True, False, False, False, cn, tr, False, False, False)

            Dim objDaoLetraCambio As New DAO.DAOLetraCambio

            '***************** INSERTA LA LETRA CAMBIO DE LA LETRA X RENEGOCIAR
       
            objDaoLetraCambio.LetraCambioInsert(objLetraCambioLetraxRenegociar, cn, tr)


            '*********** Insertamos el Documento Letra , Letra Cambio y en la Tabla Mov Cuenta 
            For i As Integer = 0 To listaLetrasCanje.Count - 1

                listaLetrasCanje(i).Id = objDaoDocLetra.DocumentoLetraInsert(listaLetrasCanje(i), cn, tr)
                listaLetrasCanje(i).getLetraCambio.IdDocumento = listaLetrasCanje(i).Id
                objDaoLetraCambio.LetraCambioInsert(listaLetrasCanje(i).getLetraCambio, cn, tr)

            Next

            '*********************** INSERTO EN RELACION DOCUMENTO

            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            Dim obj As Entidades.RelacionDocumento

            For i As Integer = 0 To listaDocumentosRef.Count - 1

                For j As Integer = 0 To listaLetrasCanje.Count - 1

                    obj = New Entidades.RelacionDocumento
                    With obj
                        .IdDocumento1 = listaDocumentosRef(i).Id ' ********  DOCUMENTO VENTA
                        .IdDocumento2 = listaLetrasCanje(j).Id ' ****************   LETRA
                    End With
                    objDaoRelacionDocumento.InsertaRelacionDocumento(obj, cn, tr)

                Next

            Next


            '************ Insertamos Relación Documento y eliminamos de MovCuenta el Doc referencia
            For i As Integer = 0 To listaDocumentosRef.Count - 1

                For j As Integer = 0 To listaLetrasCanje.Count - 1

                    objDaoDocLetra.DocumentoLetra_CanjearDocReferenciaxLetra(listaDocumentosRef(i).IdMovCuenta, listaDocumentosRef(i).Id, listaLetrasCanje(j).Id, listaLetrasCanje(j).Saldo, cn, tr)

                Next

            Next

            '************ Insertamos la Relacion Documento entre las Letras de canje y la letra renegociada
            For i As Integer = 0 To listaLetrasCanje.Count - 1


                objDaoRelacionDocumento.InsertaRelacionDocumento(New Entidades.RelacionDocumento(IdDocumentoLetraxRenegociar, listaLetrasCanje(i).Id), cn, tr)



            Next

            For i As Integer = 0 To listaLetrasCanje.Count - 1


                objDaoRelacionDocumento.ActualizarMontos(New Entidades.RelacionDocumento(IdDocumentoLetraxRenegociar, listaLetrasCanje(i).Id), cn, tr)


            Next

           

            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return listaLetrasCanje


    End Function

    Public Function DocumentoLetraAnularxIdDocumento(ByVal IdDocumentoLetra As Integer) As Boolean

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            objDaoDocLetra.DocumentoLetra_DeshacerMovimientos(IdDocumentoLetra, True, True, False, False, True, False, False, cn, tr, True, True, True)

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            Throw ex
            hecho = False
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function

    Public Function DocumentoLetraSelectDocReferenciaxIdDocumentoLetra(ByVal IdDocumentoLetra As Integer, ByVal IdMonedaDestino As Integer) As List(Of Entidades.Documento_MovCuenta)
        Return objDaoDocLetra.DocumentoLetraSelectDocReferenciaxIdDocumentoLetra(IdDocumentoLetra, IdMonedaDestino)
    End Function

    Public Function DocumentoLetraSelectCabxParams(ByVal IdDocumento As Integer, ByVal IdSerie As Integer, ByVal Codigo As Integer) As Entidades.DocLetra
        Return objDaoDocLetra.DocumentoLetraSelectCabxParams(IdDocumento, IdSerie, Codigo)
    End Function

    Public Function DocumentoLetra_EditarLetraxConsulta(ByVal IdDocumento As Integer, ByVal LetraCancelada As Boolean, ByVal DebitoCuenta As Boolean, ByVal Protestado As Boolean, ByVal FechaPgo As Date, ByVal nroOperacion As String, ByVal Usuario As Integer) As Boolean

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False

        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            objDaoDocLetra.DocumentoLetra_EditarLetraxConsulta(IdDocumento, LetraCancelada, DebitoCuenta, Protestado, FechaPgo, nroOperacion, Usuario, cn, tr)
            objDaoDocLetra.DocumentoLetra_VerificarEstadoCancelacionDocReferencia(IdDocumento, cn, tr)

            tr.Commit()
            hecho = True

        Catch ex As Exception

            tr.Rollback()
            hecho = False
            Throw ex

        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho
    End Function

    Public Function DocumentoLetraConsultarLetrasxIdEstadoCancelacion(ByVal IdPersona As Integer, ByVal IdEstadoCancelacion As Integer, ByVal Top As Integer) As List(Of Entidades.DocLetra)
        Return (New DAO.DAODocLetra).DocumentoLetraConsultarLetrasxIdEstadoCancelacion(IdPersona, IdEstadoCancelacion, Top)
    End Function

    Public Function DocumentoLetraUpdate(ByVal objDocumentoLetra As Entidades.DocLetra, ByVal listaDocumentosRef As List(Of Entidades.Documento_MovCuenta)) As Boolean

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False

        Try

            cn.Open()
            tr = cn.BeginTransaction

            '*********** Deshacer los movimientos hecho x la Letra
            objDaoDocLetra.DocumentoLetra_DeshacerMovimientos(objDocumentoLetra.Id, True, True, True, True, False, True, True, cn, tr)

            '********** Actualizamos la Letra Cab
            objDaoDocLetra.DocumentoLetraUpdate(objDocumentoLetra, cn, tr)

            '********* Insertamos Letra Cambio
            Dim objDaoLetraCambio As New DAO.DAOLetraCambio
            objDocumentoLetra.getLetraCambio.IdDocumento = objDocumentoLetra.Id
            objDaoLetraCambio.LetraCambioInsert(objDocumentoLetra.getLetraCambio, cn, tr)

            ''************ Insertamos Relación Documento y eliminamos de MovCuenta el Doc referencia
            'For i As Integer = 0 To listaDocumentosRef.Count - 1

            '    objDaoDocLetra.DocumentoLetra_CanjearDocReferenciaxLetra(listaDocumentosRef(i).IdMovCuenta, listaDocumentosRef(i).Id, objDocumentoLetra.Id, cn, tr)
            'Next
            'objDaoDocLetra.DocumentoLetra_VerificarEstadoCancelacionDocReferencia(objDocumentoLetra.Id, cn, tr)


            '************ Insertamos Relación Documento y eliminamos de MovCuenta el Doc referencia
            If listaDocumentosRef IsNot Nothing Then
                For i As Integer = 0 To listaDocumentosRef.Count - 1

                    'For j As Integer = 0 To listaLetras.Count - 1

                    '    objDaoDocLetra.DocumentoLetra_CanjearDocReferenciaxLetra(listaDocumentosRef(i).IdMovCuenta, listaDocumentosRef(i).Id, listaLetras(j).Id, cn, tr)

                    'Next

                Next
            End If

            tr.Commit()

            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function

    Public Function registrarLetras(ByVal listaLetras As List(Of Entidades.DocLetra), ByVal listaDocumentosRef As List(Of Entidades.Documento_MovCuenta)) As List(Of Entidades.DocLetra)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            Dim objDaoDocLetra As New DAO.DAODocLetra
            Dim objDaoLetraCambio As New DAO.DAOLetraCambio

            '*********** Insertamos el Documento Letra , Letra Cambio y en la Tabla Mov Cuenta 
            For i As Integer = 0 To listaLetras.Count - 1



                If listaLetras(i).IdDocumento > 0 Then
                    listaLetras(i).Id = listaLetras(i).IdDocumento
                    objDaoDocLetra.DocumentoLetraUpdate(listaLetras(i), cn, tr)
                Else
                    listaLetras(i).Id = objDaoDocLetra.DocumentoLetraInsert(listaLetras(i), cn, tr)
                End If

                listaLetras(i).getLetraCambio.IdDocumento = listaLetras(i).Id
                objDaoLetraCambio.LetraCambioInsert(listaLetras(i).getLetraCambio, cn, tr)

            Next


            '*********************** INSERTO EN RELACION DOCUMENTO
          
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            Dim obj As Entidades.RelacionDocumento
            For i As Integer = 0 To listaDocumentosRef.Count - 1

                For j As Integer = 0 To listaLetras.Count - 1

                    obj = New Entidades.RelacionDocumento
                    With obj
                        .IdDocumento1 = listaDocumentosRef(i).Id ' ********  DOCUMENTO VENTA
                        .IdDocumento2 = listaLetras(j).Id ' ****************   LETRA
                    End With
                    objDaoRelacionDocumento.InsertaRelacionDocumento(obj, cn, tr)

                Next

            Next

            For i As Integer = 0 To listaDocumentosRef.Count - 1

                objDaoDocLetra.DocumentoLetra_CanjearDocReferenciaxLetra(listaDocumentosRef(i).IdMovCuenta, listaDocumentosRef(i).Id, 0, listaDocumentosRef(i).NuevoSaldo, cn, tr)

            Next

            '*********************** INSERTO EN CONCEPTO 
            Dim objDaoDetalleConcepto As New DAO.DAODetalleConcepto

            Dim ListaDetalleConcepto As List(Of Entidades.DetalleConcepto)
            Dim objDetalleConcepto As Entidades.DetalleConcepto

            For j As Integer = 0 To listaLetras.Count - 1

                ListaDetalleConcepto = New List(Of Entidades.DetalleConcepto)

                For i As Integer = 0 To listaDocumentosRef.Count - 1

                    objDetalleConcepto = New Entidades.DetalleConcepto
                    objDetalleConcepto.IdDocumentoRef = listaDocumentosRef(i).Id
                    objDetalleConcepto.Concepto = listaDocumentosRef(i).NomTipoDocumento + " Nro: " + listaDocumentosRef(i).NroDocumento + "  Importe: " + listaDocumentosRef(i).NomMoneda + " " + CStr(Math.Round(listaDocumentosRef(i).MontoTotal, 2)) + " Fecha: " + Format(listaDocumentosRef(i).FechaEmision, "dd/MM/yyyy")
                    objDetalleConcepto.Monto = listaDocumentosRef(i).NuevoSaldo

                    ListaDetalleConcepto.Add(objDetalleConcepto)

                Next

                objDaoDetalleConcepto.GrabarDetalleConceptoT(cn, listaLetras(j).Id, ListaDetalleConcepto, tr)

            Next

            tr.Commit()

        Catch ex As Exception

            tr.Rollback()
            Throw ex

        Finally

            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try

        Return listaLetras

    End Function

    Public Function DocumentoLetraSelectCabtxId_Print(ByVal IdDocumentoLetra As Integer) As Entidades.DocLetra
        Return (New DAO.DAODocLetra).DocumentoLetraSelectCabtxId_Print(IdDocumentoLetra)
    End Function

    Public Function Anexar_Letra(ByVal IdDocumentoLetra As Integer) As List(Of Entidades.DocLetra)
        Return (New DAO.DAODocLetra).Anexar_Letra(IdDocumentoLetra)
    End Function

    Public Function ActualizarLetras(ByVal listaLetras As List(Of Entidades.DocLetra), ByVal listaDocumentosRef As List(Of Entidades.Documento_MovCuenta)) As Boolean

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False

        Try

            cn.Open()
            tr = cn.BeginTransaction

            '*********** Deshacer los movimientos hecho x la Letra

            objDaoDocLetra.DocumentoLetra_DeshacerMovimientos(listaLetras(0).Id, True, True, True, True, False, True, True, cn, tr, True, True, True)

            Dim objDaoLetraCambio As New DAO.DAOLetraCambio
            For i As Integer = 0 To listaLetras.Count - 1

                '********** Actualizamos la Letra Cab

                If listaLetras(i).IdDocumento > 0 Then
                    listaLetras(i).Id = listaLetras(i).IdDocumento
                End If

                objDaoDocLetra.DocumentoLetraUpdate(listaLetras(i), cn, tr)

                listaLetras(i).getLetraCambio.IdDocumento = listaLetras(i).Id
                objDaoLetraCambio.LetraCambioInsert(listaLetras(i).getLetraCambio, cn, tr)

            Next

            '*********************** INSERTO EN RELACION DOCUMENTO

            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            Dim obj As Entidades.RelacionDocumento
            For i As Integer = 0 To listaDocumentosRef.Count - 1

                For j As Integer = 0 To listaLetras.Count - 1

                    obj = New Entidades.RelacionDocumento
                    With obj
                        .IdDocumento1 = listaDocumentosRef(i).Id ' ********  DOCUMENTO VENTA
                        .IdDocumento2 = listaLetras(j).Id ' ****************   LETRA
                    End With
                    objDaoRelacionDocumento.InsertaRelacionDocumento(obj, cn, tr)

                Next

            Next

            For i As Integer = 0 To listaDocumentosRef.Count - 1

                objDaoDocLetra.DocumentoLetra_CanjearDocReferenciaxLetra(listaDocumentosRef(i).IdMovCuenta, listaDocumentosRef(i).Id, 0, listaDocumentosRef(i).NuevoSaldo, cn, tr)

            Next

            '*********************** INSERTO EN CONCEPTO 
            Dim objDaoDetalleConcepto As New DAO.DAODetalleConcepto

            Dim ListaDetalleConcepto As List(Of Entidades.DetalleConcepto)
            Dim objDetalleConcepto As Entidades.DetalleConcepto

            For j As Integer = 0 To listaLetras.Count - 1

                ListaDetalleConcepto = New List(Of Entidades.DetalleConcepto)

                For i As Integer = 0 To listaDocumentosRef.Count - 1

                    objDetalleConcepto = New Entidades.DetalleConcepto
                    objDetalleConcepto.IdDocumentoRef = listaDocumentosRef(i).Id
                    objDetalleConcepto.Concepto = listaDocumentosRef(i).NomTipoDocumento + " Nro: " + listaDocumentosRef(i).NroDocumento + "  Importe: " + listaDocumentosRef(i).NomMoneda + " " + CStr(Math.Round(listaDocumentosRef(i).MontoTotal, 2)) + " Fecha: " + Format(listaDocumentosRef(i).FechaEmision, "dd/MM/yyyy")
                    objDetalleConcepto.Monto = listaDocumentosRef(i).NuevoSaldo

                    ListaDetalleConcepto.Add(objDetalleConcepto)

                Next

                objDaoDetalleConcepto.GrabarDetalleConceptoT(cn, listaLetras(j).Id, ListaDetalleConcepto, tr)

            Next



            tr.Commit()

            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function

    Public Function getDataSet_CR_DocumentoLetraCab(ByVal IdDocumento As Integer) As DataSet

        Return objDaoDocLetra.getDataSet_CR_DocumentoLetraCab(IdDocumento)

    End Function


End Class
