﻿Imports Negocio
Imports Entidades
Imports System.Data.SqlClient
Public Class LN_TRAZABILIDAD_DISTRIBUCION
    Dim objDatosTrazabilidad As New DAO.DAOTrazabilidad
    Private cn As SqlConnection
    Private tr As SqlTransaction
    Private cmd As SqlCommand

    Public Function LN_listarDocReferencia(ByVal serie As Integer, ByVal codigo As Integer, ByVal idTienda As Integer, ByVal fechaInicio As String, ByVal fechaFin As String) As DataTable
        Return objDatosTrazabilidad.DAO_listarDocReferencia(serie, codigo, idTienda, fechaInicio, fechaFin)
    End Function
    Public Function LN_listarProductos(ByVal idDocumento As Integer) As DataTable
        Return objDatosTrazabilidad.DAO_listarProductos(idDocumento)
    End Function

    Public Function LN_trazabilidad_GuardarKardex(ByVal idDocumentoReferencia As Integer, ByVal usuRegistro As String, ByVal obj As Entidades.Documento)
        Return objDatosTrazabilidad.DAO_trazabilidad_GuardarKardex(idDocumentoReferencia, usuRegistro, obj)
    End Function

    Public Function LN_trazabilidad_Ver_Registro(ByVal flag As String, ByVal serie As String, ByVal codigo As String, ByVal idempresa As Integer, ByVal idTienda As Integer, _
                                             ByVal idTipodocumento As Integer) As DataTable
        Return objDatosTrazabilidad.DAO_trazabilidad_Ver_Registro(flag, serie, codigo, idempresa, idTienda, idTipodocumento)
    End Function

    Public Sub LN_trazabilidad_GuardarSaldoKardex(ByVal idProducto As Integer, ByVal idUnidadMedida As Integer, ByVal idAfecto As Boolean, ByVal usuRegistro As String, _
                                                  ByVal idDocumento As Integer)
        objDatosTrazabilidad.DAO_trazabilidad_GuardarSaldoKardex(idProducto, idUnidadMedida, idAfecto, usuRegistro, idDocumento)
    End Sub

    Public Sub LN_trazabilidad_GuardarDetalleSaldoKardex(ByVal idProducto As Integer, ByVal idSector As Integer, ByVal cantidadSaldo As Decimal, _
                                                          ByVal idDocumento As Integer, ByVal idTono As Integer)
        objDatosTrazabilidad.DAO_trazabilidad_GuardarDetalleSaldoKardex(idProducto, idSector, cantidadSaldo, idDocumento, idTono)
    End Sub

    Public Function LN_obtenerIdDocumento(ByVal doc_serie As String, ByVal doc_codigo As String, ByVal idTipoDocumento As Integer) As Integer
        Return objDatosTrazabilidad.DAO_obtenerIdDocumento(doc_serie, doc_codigo, idTipoDocumento)
    End Function

    Public Function LN_Insert_Kardex_detallado(ByVal idProducto As Integer, ByVal idAlmacen As Integer, ByVal idSector As Integer, _
                                               ByVal cantidad As Decimal, ByVal idTono As Integer) As Boolean
        Return objDatosTrazabilidad.DAO_Insert_Kardex_detallado(idProducto, idAlmacen, idSector, cantidad, idTono)
    End Function

    Public Function LN_mantenimiento_Tonos(ByVal nom_tono As String, ByVal descTono As String, ByVal idProducto As Integer, ByVal idAlmacen As Integer, ByVal idSector As Integer) As Boolean
        cn = (New DAO.Conexion).ConexionSIGE
        Dim respuesta As Boolean
        Try
            cn.Open()
            tr = cn.BeginTransaction
            respuesta = (New DAO.DAOTrazabilidad).DAO_MANTENIMIENTO_TONO(nom_tono, descTono, idProducto, idAlmacen, idSector, cn, tr)
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            'Throw New e("Exception Occured")
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return respuesta
    End Function

    Public Function LN_Insert_Tono(ByVal nom_tono As String, ByVal descTono As String, ByVal idProducto As Integer) As Boolean
        cn = (New DAO.Conexion).ConexionSIGE
        Dim respuesta As Boolean
        Try
            cn.Open()
            tr = cn.BeginTransaction
            respuesta = objDatosTrazabilidad.DAO_insertTono(nom_tono, descTono, idProducto, cn, tr)
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            'Throw New e("Exception Occured")
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return respuesta
    End Function

    Public Function LN_trazabilidad_Select_Kardex(ByVal idSector As Integer, ByVal idAlmacen As Integer, ByVal prod_nombre As String, _
                                                         ByVal prod_codigo As String, ByVal idTipoExistencia As Integer, ByVal idLinea As Integer, _
                                                         ByVal idSublinea As Integer) As List(Of Entidades.be_productoStock)
        cn = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        Return objDatosTrazabilidad.DAO_trazabilidad_Select_Kardex(idSector, idAlmacen, prod_nombre, prod_codigo, idTipoExistencia, idLinea, idSublinea, cn)
        cn.Close()
    End Function

    Public Function LN_Update_Stock(ByVal idProducto As Integer, ByVal idAlmacen As Integer, ByVal idSector_antiguo As Integer, ByVal idSector_nuevo As Integer, _
                                            ByVal cantidad As Decimal, ByVal idTono As Integer) As Boolean
        Return (New DAO.DAOTrazabilidad).DAO_Update_Stock(idProducto, idAlmacen, idSector_antiguo, idSector_nuevo, cantidad, idTono)
    End Function
End Class
