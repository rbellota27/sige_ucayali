﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class Contabilidad
    Private objDaoContabilidad As New DAO.DAOContabilidad
    Private objConexion As New DAO.Conexion
    Public Function ReportePercepcion_PDT(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdTipoDocumento As Integer, ByVal IdMonedaDestino As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date) As String

        Return objDaoContabilidad.ReportePercepcion_PDT(IdEmpresa, IdTienda, IdTipoDocumento, IdMonedaDestino, FechaInicio, FechaFin)

    End Function

    Public Function get_DS_ReportePercepcion_PDT(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdTipoDocumento As Integer, ByVal IdMonedaDestino As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date) As DataSet
        Return objDaoContabilidad.get_DS_ReportePercepcion_PDT(IdEmpresa, IdTienda, IdTipoDocumento, IdMonedaDestino, FechaInicio, FechaFin)
    End Function


    Public Function ExportarContabilidad_Ventas(ByVal IdEmpresa As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date) As Boolean

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False

        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)


            objDaoContabilidad.ExpotarContabilidad_Ventas(IdEmpresa, FechaInicio, FechaFin, cn, tr)

            tr.Commit()
            hecho = True

        Catch ex As Exception

            tr.Rollback()
            hecho = False
            Throw ex

        Finally

            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try

        Return hecho

    End Function





End Class
