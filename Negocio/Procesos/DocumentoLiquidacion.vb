﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************   MARTES 27 ABRIL 2010 HORA 02_27 PM


Imports System.Data.SqlClient

Public Class DocumentoLiquidacion

    Private objDaoDocumentoLiq As New DAO.DAODocumentoLiquidacion

    Private cn As SqlConnection
    Private tr As SqlTransaction
    Private cmd As SqlCommand

    Public Function DocumentoLiquidacionPrint(ByVal IdDocumento As Integer) As DataSet

        Return objDaoDocumentoLiq.DocumentoLiquidacionPrint(IdDocumento)


    End Function

    Public Function DocumentoLiquidacion_SelectIdDocumentoRef(ByVal IdDocumento As Integer) As List(Of Entidades.RelacionDocumento)
        Return objDaoDocumentoLiq.DocumentoLiquidacion_SelectIdDocumentoRef(IdDocumento)
    End Function

    Public Function DocumentoLiquidacionSelectCabFind_Aplicacion(ByVal IdPersona As Integer) As List(Of Entidades.DocumentoView)
        Return objDaoDocumentoLiq.DocumentoLiquidacionSelectCabFind_Aplicacion(IdPersona)
    End Function


    Public Function DocumentoLiquidacion_BuscarDocRef(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPersona As Integer, ByVal Serie As Integer, ByVal Codigo As Integer, ByVal IdTipoDocumentoRef As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date) As List(Of Entidades.DocumentoView)
        Return objDaoDocumentoLiq.DocumentoLiquidacion_BuscarDocRef(IdEmpresa, IdTienda, IdPersona, Serie, Codigo, IdTipoDocumentoRef, FechaInicio, FechaFin)
    End Function

    Public Function registrarDocumentoLiquidacion(ByVal objDocumento As Entidades.Documento, ByVal objAnexoDocumento As Entidades.Anexo_Documento, ByVal listaDetalleDocumento_Ingreso As List(Of Entidades.DetalleDocumento), ByVal listaDetalleDocumento_Salida As List(Of Entidades.DetalleDocumento), ByVal objMovCaja As Entidades.MovCaja, ByVal listaPagoCaja As List(Of Entidades.PagoCaja), ByVal objRelacionDocumento As Entidades.RelacionDocumento, ByVal objObservacion As Entidades.Observacion, ByVal moverStockFisico_Ingreso As Boolean, ByVal moverStockFisico_Salida As Boolean, ByVal ComprometerStockSalida As Boolean) As Integer

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            '****************** INSERTAMOS LA CABECERA
            Dim objDaoDocumento As New DAO.DAODocumento
            objDocumento.Id = objDaoDocumento.InsertaDocumento(cn, objDocumento, tr)

            '****************** INSERTAMOS ANEXO DOCUMENTO
            If Not objAnexoDocumento Is Nothing Then

                Dim objDaoAnexoDocumento As New DAO.DAOAnexo_Documento
                objAnexoDocumento.IdDocumento = objDocumento.Id
                objDaoAnexoDocumento.InsertAnexo_Documento(objAnexoDocumento, cn, tr)

            End If

            '**************** INSERTAMOS EL DETALLE
            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            '********** INGRESO
            objDaoDetalleDocumento.InsertaDetalleDocumento_MovAlmacen(cn, listaDetalleDocumento_Ingreso, tr, objDocumento.Id, moverStockFisico_Ingreso, False, 1, objDocumento.IdAlmacen)
            '********** SALIDA
            If ComprometerStockSalida Then moverStockFisico_Salida = True
            objDaoDetalleDocumento.InsertaDetalleDocumento_MovAlmacen(cn, listaDetalleDocumento_Salida, tr, objDocumento.Id, moverStockFisico_Salida, ComprometerStockSalida, -1, objDocumento.IdAlmacen)



            '***************** INSERTAMOS MOV CAJA / PAGO CAJA
            If (objMovCaja IsNot Nothing) Then

                Dim objDaoMovCaja As New DAO.DAOMovCaja
                objMovCaja.IdDocumento = objDocumento.Id
                objDaoMovCaja.InsertaMovCaja(cn, objMovCaja, tr)

                '************ INSERTAMOS PAGO CAJA
                Dim objDaoPagoCaja As New DAO.DAOPagoCaja
                For i As Integer = 0 To listaPagoCaja.Count - 1
                    listaPagoCaja(i).IdDocumento = objDocumento.Id
                    objDaoPagoCaja.InsertaPagoCaja(cn, listaPagoCaja(i), tr)

                    If (listaPagoCaja(i).IdMedioPago = 10) Then  '*****   LIQUIDACION

                        objDaoDocumentoLiq.DocumentoLiquidacion_UpdateSaldo(CInt(listaPagoCaja(i).NumeroCheque), listaPagoCaja(i).Efectivo, -1, cn, tr)

                        Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
                        objDaoRelacionDocumento.InsertaRelacionDocumento((New Entidades.RelacionDocumento(CInt(listaPagoCaja(i).NumeroCheque), objDocumento.Id)), cn, tr)


                    End If

                Next

            End If

            '*************** INSERTAMOS OBS
            If (objObservacion IsNot Nothing) Then
                Dim objDaoObservaciones As New DAO.DAOObservacion
                objObservacion.IdDocumento = objDocumento.Id
                objDaoObservaciones.InsertaObservacionT(cn, tr, objObservacion)
            End If

            '**************** RELACION DOCUMENTO
            If (objRelacionDocumento IsNot Nothing) Then
                Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
                objRelacionDocumento.IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(objRelacionDocumento, cn, tr)

            End If

            tr.Commit()
            cn.Close()

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function


    Public Function actualizarDocumentoLiquidacion(ByVal objDocumento As Entidades.Documento, ByVal objAnexoDocumento As Entidades.Anexo_Documento, ByVal listaDetalleDocumento_Ingreso As List(Of Entidades.DetalleDocumento), ByVal listaDetalleDocumento_Salida As List(Of Entidades.DetalleDocumento), ByVal objMovCaja As Entidades.MovCaja, ByVal listaPagoCaja As List(Of Entidades.PagoCaja), ByVal objRelacionDocumento As Entidades.RelacionDocumento, ByVal objObservacion As Entidades.Observacion, ByVal moverStockFisico_Ingreso As Boolean, ByVal moverStockFisico_Salida As Boolean, ByVal ComprometerStock As Boolean) As Integer

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            '**************** DESHACER MOV
            objDaoDocumentoLiq.DocumentoLiquidacion_DeshacerMov(objDocumento.Id, True, True, True, True, True, True, True, False, cn, tr)

            '****************** ACTUALIZAMOS LA CABECERA
            Dim objDaoDocumento As New DAO.DAODocumento
            objDaoDocumento.DocumentoUpdate(objDocumento, cn, tr)

            '****************** INSERTAMOS ANEXO DOCUMENTO
            If Not objAnexoDocumento Is Nothing Then

                Dim objDaoAnexoDocumento As New DAO.DAOAnexo_Documento
                objAnexoDocumento.IdDocumento = objDocumento.Id
                objDaoAnexoDocumento.InsertAnexo_Documento(objAnexoDocumento, cn, tr)

            End If

            '**************** INSERTAMOS EL DETALLE
            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            '********** INGRESO
            objDaoDetalleDocumento.InsertaDetalleDocumento_MovAlmacen(cn, listaDetalleDocumento_Ingreso, tr, objDocumento.Id, moverStockFisico_Ingreso, False, 1, objDocumento.IdAlmacen)
            '********** SALIDA
            If ComprometerStock Then moverStockFisico_Salida = True
            objDaoDetalleDocumento.InsertaDetalleDocumento_MovAlmacen(cn, listaDetalleDocumento_Salida, tr, objDocumento.Id, moverStockFisico_Salida, ComprometerStock, -1, objDocumento.IdAlmacen)

            '***************** INSERTAMOS MOV CAJA / PAGO CAJA
            If (objMovCaja IsNot Nothing) Then

                Dim objDaoMovCaja As New DAO.DAOMovCaja
                objMovCaja.IdDocumento = objDocumento.Id
                objDaoMovCaja.InsertaMovCaja(cn, objMovCaja, tr)

                '************ INSERTAMOS PAGO CAJA
                Dim objDaoPagoCaja As New DAO.DAOPagoCaja
                For i As Integer = 0 To listaPagoCaja.Count - 1
                    listaPagoCaja(i).IdDocumento = objDocumento.Id
                    objDaoPagoCaja.InsertaPagoCaja(cn, listaPagoCaja(i), tr)

                    If (listaPagoCaja(i).IdMedioPago = 10) Then  '*****   LIQUIDACION

                        objDaoDocumentoLiq.DocumentoLiquidacion_UpdateSaldo(CInt(listaPagoCaja(i).NumeroCheque), listaPagoCaja(i).Efectivo, -1, cn, tr)

                        Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
                        objDaoRelacionDocumento.InsertaRelacionDocumento((New Entidades.RelacionDocumento(CInt(listaPagoCaja(i).NumeroCheque), objDocumento.Id)), cn, tr)


                    End If

                Next

            End If

            '*************** INSERTAMOS OBS
            If (objObservacion IsNot Nothing) Then
                Dim objDaoObservaciones As New DAO.DAOObservacion
                objObservacion.IdDocumento = objDocumento.Id
                objDaoObservaciones.InsertaObservacionT(cn, tr, objObservacion)
            End If

            '**************** RELACION DOCUMENTO
            If (objRelacionDocumento IsNot Nothing) Then
                Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
                objRelacionDocumento.IdDocumento2 = objDocumento.Id
                objDaoRelacionDocumento.InsertaRelacionDocumento(objRelacionDocumento, cn, tr)

            End If

            tr.Commit()
            cn.Close()

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function

    Public Function anularDocumentoLiquidacion(ByVal IdDocumento As Integer) As Boolean

        Dim hecho As Boolean = False

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            objDaoDocumentoLiq.DocumentoLiquidacion_DeshacerMov(IdDocumento, False, False, True, True, True, False, False, True, cn, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function


End Class

