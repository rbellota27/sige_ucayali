﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




'********************   JUEVES 10 JUNIO 2010 HORA 12_46 PM






Imports System.Data.SqlClient

Public Class DocCancelacion

    Protected obj As New DAO.DAODocCancelacion
    Private cn As SqlConnection
    Private tr As SqlTransaction

    Public Function actualizarSustento_Documento(ByVal IdDocumento As Integer, ByVal IdDetalleConcepto As Integer, ByVal listaDetalleConcepto_Anexo As List(Of Entidades.DetalleConcepto_Anexo), ByVal objObservaciones As Entidades.Observacion) As Boolean

        Dim hecho As Boolean = False
        cn = (New DAO.Conexion).ConexionSIGE

        Try

            cn.Open()
            tr = cn.BeginTransaction

            Dim objDaoDCA As New DAO.DAODetalleConcepto_Anexo

            '************* ELIMINAMOS EL SUSTENTO
            objDaoDCA.DetalleDocumento_Anexo_DeletexIdDetalleConcepto(IdDetalleConcepto, cn, tr)

            '******************* INSERTAMOS
            For i As Integer = 0 To listaDetalleConcepto_Anexo.Count - 1

                objDaoDCA.DetalleConcepto_Anexo_Insert(listaDetalleConcepto_Anexo(i), cn, tr)

            Next

            '***************** OBSERVACIONES
            Dim objDaoObservaciones As New DAO.DAOObservacion
            If (objObservaciones IsNot Nothing) Then

                objObservaciones.IdDocumento = IdDocumento
                objDaoObservaciones.Insert_Update(objObservaciones, cn, tr)

            Else

                objDaoObservaciones.DeletexIdDocumento(IdDocumento, cn, tr)

            End If


            obj.DocumentoReciboEgreso_UpdateMontoxSustentar(IdDocumento, cn, tr)

            Dim objReq As New DAO.DAORequerimientoGasto
            objReq.ValSustento_RelacionDocumento(IdDocumento, cn, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return hecho

    End Function
    Public Function UpdateSustentoxIdDocumento(ByVal objDocumento As Entidades.Documento) As Boolean
  
        obj.UpdateSustentoxIdDocumento(objDocumento)

    End Function



    Public Function UpdateMontoxSustentar_Liquidacion(ByVal listaAnexoDocumento As List(Of Entidades.Anexo_Documento)) As Boolean

        Dim hecho As Boolean = False

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction

            For i As Integer = 0 To listaAnexoDocumento.Count - 1

                obj.UpdateMontoxSustentar_Liquidacion(listaAnexoDocumento(i).IdDocumento, listaAnexoDocumento(i).MontoxSustentar, listaAnexoDocumento(i).Liquidado, cn, tr)

            Next

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function


    Public Function Sustento_Liq_SelectxParams_DT(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdCaja As Integer, ByVal Opcion_Sustento As Integer, ByVal Opcion_Liq As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal IdPersona As Integer) As DataTable
        Return obj.Sustento_Liq_SelectxParams_DT(IdEmpresa, IdTienda, IdCaja, Opcion_Sustento, Opcion_Liq, FechaInicio, FechaFin, IdPersona)
    End Function

    Public Function Insert(ByVal objDocumento As Entidades.Documento, _
    ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), _
    ByVal listaCancelacion As List(Of Entidades.DatosCancelacion), _
    ByVal objObservaciones As Entidades.Observacion, _
    ByVal listaMovBanco As List(Of Entidades.MovBancoView), _
    ByVal objMovCaja As Entidades.MovCaja, _
    ByVal listaPagoCaja As List(Of Entidades.PagoCaja), _
    Optional ByVal objAnexoDocumento As Entidades.Anexo_Documento = Nothing, _
    Optional ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = Nothing, _
    Optional ByVal objAsientoContable As Entidades.be_asientoContable = Nothing, Optional ByVal idCadenaRequerimiento As String = "")

        Return obj.Insert(objDocumento, listaDetalleConcepto, listaCancelacion, objObservaciones, listaMovBanco, objMovCaja, listaPagoCaja, objAnexoDocumento, listaRelacionDocumento, objAsientoContable, idCadenaRequerimiento)

    End Function

    Public Sub pruebaInsertAsiento(ByVal objAsientoContable As Entidades.be_asientoContable, ByVal idCadenaReq As String)
        Dim cn As SqlConnection
        cn = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        tr = cn.BeginTransaction(IsolationLevel.Serializable)
        Dim obj As New DAO.DAODocCancelacion
        ' obj.insertarAsientoContable(cn, tr, objAsientoContable, idCadenaReq)
    End Sub

    Public Function Update(ByVal objDocumento As Entidades.Documento, _
    ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), _
    ByVal listaCancelacion As List(Of Entidades.DatosCancelacion), _
    ByVal objObservaciones As Entidades.Observacion, _
    ByVal listaMovBanco As List(Of Entidades.MovBancoView), _
    ByVal objMovCaja As Entidades.MovCaja, _
    ByVal listaPagoCaja As List(Of Entidades.PagoCaja), Optional ByVal objAnexoDocumento As Entidades.Anexo_Documento = Nothing, Optional ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = Nothing) As Integer

        Return obj.Update(objDocumento, listaDetalleConcepto, listaCancelacion, objObservaciones, listaMovBanco, objMovCaja, listaPagoCaja, objAnexoDocumento, listaRelacionDocumento)

    End Function

End Class
