﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class ApproveOC

    Dim objDAOApproveOC As New DAO.DAOApproveOC

    Public Function OrdenCompra_MovCuentaPorPagar(ByVal obj As Entidades.Documento) As Boolean
        Return objDAOApproveOC.OrdenCompra_MovCuentaPorPagar(obj)
    End Function
    Public Function OrdenCompra_Approve(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdArea As Integer, _
                                        ByVal Aprobacion As Integer, ByVal IdSerie As Integer, ByVal Fechaini As String, ByVal Fechafin As String) As List(Of Entidades.Documento)

        Return objDAOApproveOC.OrdenCompra_Approve(IdEmpresa, IdTienda, IdArea, Aprobacion, IdSerie, Fechaini, Fechafin)

    End Function


    Public Function CancelarStockTransito(ByVal obj As Entidades.Documento) As Boolean
        Return objDAOApproveOC.CancelarStockTransito(obj)
    End Function

End Class
