﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'viernes 12 de marzo
Public Class DocOrdenPedido

    Private obj As New DAO.DAODocOrdenPedido

#Region "Mantenimiento"

    Public Function SelectOrdenPedido(ByVal codigo As Integer, ByVal idserie As Integer) As Entidades.Documento
        Return obj.SelectOrdenPedido(codigo, idserie)
    End Function

    Public Function findDocumentoRef(ByVal serie As String, ByVal codigo As String, _
                                     ByVal pageindex As Integer, ByVal pagesize As Integer, _
                                     ByVal Idtienda As Integer, ByVal IdEmpresa As Integer, _
                                     ByVal idalmacen As Integer) As List(Of Entidades.DocumentoView)
        Return obj.findDocumentoRef(serie, codigo, pageindex, pagesize, Idtienda, IdEmpresa, idalmacen)
    End Function

    Public Function InsertOrdenPedido(ByVal ObjDocumento As Entidades.Documento, ByVal listaDetalle As List(Of Entidades.DetalleDocumento), _
                                      ByVal obs As Entidades.Observacion, ByVal comprometerStock As Boolean, ByVal objProgPedido As Entidades.ProgramacionPedido) As String
        Return obj.InsertOrdenPedido(ObjDocumento, listaDetalle, obs, comprometerStock, objProgPedido)
    End Function

    Public Function UpdateOrdenPedido(ByVal objDocumento As Entidades.Documento, ByVal listaDetalle As List(Of Entidades.DetalleDocumento), _
                                     ByVal objObs As Entidades.Observacion, ByVal comprometerStock As Boolean, ByVal objProgPedido As Entidades.ProgramacionPedido) As Boolean
        Return obj.UpdateOrdenPedido(objDocumento, listaDetalle, objObs, comprometerStock, objProgPedido)
    End Function

#End Region

#Region "otros"
    Public Function OrdenPedidoxSelectDetalleEntresucursales(ByVal IdDocumento As Integer, _
                                                            ByVal idAlmacen As Integer) As List(Of Entidades.DetalleDocumento)
        Return obj.OrdenPedidoxSelectDetalleEntresucursales(IdDocumento, idAlmacen)
    End Function

    Public Function OrdenPedidoConsultarRelacionDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.Documento)
        Return obj.OrdenPedidoConsultarRelacionDocumento(IdDocumento)
    End Function

    Public Function getDataSetOrdenPedido(ByVal idDocumento As Integer) As DataSet
        Return obj.getDataSetOrdenPedido(idDocumento)
    End Function

    Public Function ListarProductosOrdenPedido(ByVal idalmacen As Integer, ByVal descripcion As String, _
     ByVal linea As Integer, ByVal sublinea As Integer, ByVal codsubLinea As Integer, _
     ByVal pageindex As Integer, ByVal pagesize As Integer, ByVal idalmacen1 As Integer) As List(Of Entidades.Catalogo)
        Return obj.ListarProductosOrdenPedido(idalmacen, descripcion, linea, sublinea, codsubLinea, pageindex, pagesize, idalmacen1)
    End Function


    Public Function OrdenPedidoxSelectxIdDocumento(ByVal idDocumento As Integer, ByVal IdAlmacen As Integer, _
                                                                                    ByVal idAlmacen2 As Integer) As List(Of Entidades.DetalleDocumento)
        Return obj.OrdenPedidoxSelectxIdDocumento(idDocumento, IdAlmacen, idAlmacen2)
    End Function

    Public Function OrdenPedidoConsultarStock(ByVal idalmacen As Integer, ByVal idproducto As Integer) As Decimal
        Return obj.OrdenPedidoConsultarStock(idalmacen, idproducto)
    End Function


#End Region

#Region "1era semana de marzo"

    Public Function OrdenPedido_SemanaActual(ByVal fecha As Date) As Integer
        Return obj.OrdenPedido_SemanaActual(fecha)
    End Function

    Public Function OrdenPedido_ProgramacionPedido(ByVal fecha As Date, _
                                                  ByVal idEmpresa As Integer, ByVal idTienda As Integer, _
                                                  ByVal idAlmacen As Integer) As String
        Return obj.OrdenPedido_ProgramacionPedido(fecha, idEmpresa, idTienda, idAlmacen)
    End Function

    Public Function OrdenPedido_Validar_ProgramacionPedido(ByVal fecha As Date, _
                                                 ByVal idEmpresa As Integer, ByVal idTienda As Integer, _
                                                 ByVal idAlmacen As Integer) As String
        Return obj.OrdenPedido_Validar_ProgramacionPedido(fecha, idEmpresa, idTienda, idAlmacen)
    End Function

#End Region

#Region "Reportes"
    Public Function DetalleOrdenPedido(ByVal idempresa As Integer, ByVal idalmacen As Integer, ByVal year As Integer, ByVal semana As Integer, ByVal fechainicio As String, ByVal fechafin As String, ByVal fsemana As Integer, ByVal tipodocs As Integer) As DataSet

        Return obj.DetalleOrdenPedido(idempresa, idalmacen, year, semana, fechainicio, fechafin, fsemana, tipodocs)

    End Function
#End Region


End Class
