﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Modelo
    Private objDAOModelo As New DAO.DAOModelo

    Public Function InsertaModelo(ByVal Modelo As Entidades.Modelo) As Boolean
        Return objDAOModelo.InsertaModelo(Modelo)
    End Function
    Public Function ActualizaModelo(ByVal Modelo As Entidades.Modelo) As Boolean
        Return objDAOModelo.ActualizaModelo(Modelo)
    End Function
    Public Function SelectAll() As List(Of Entidades.Modelo)
        Return objDAOModelo.SelectAllModelo
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Modelo)
        Return objDAOModelo.SelectAllActivoModelo
    End Function

    Public Function SelectAllInactivoModelo() As List(Of Entidades.Modelo)
        Return objDAOModelo.SelectAllInactivoModelo
    End Function

    Public Function SelectAllxNombre(ByVal Nombre As String) As List(Of Entidades.Modelo)
        Return objDAOModelo.SelectAllxNombre(Nombre)
    End Function

    Public Function SelectAllxNombreInactivo(ByVal Nombre As String) As List(Of Entidades.Modelo)
        Return objDAOModelo.SelectAllxNombreInactivo(Nombre)
    End Function
    Public Function SelectAllxNombreActivo(ByVal Nombre As String) As List(Of Entidades.Modelo)
        Return objDAOModelo.SelectAllxNombreActivo(Nombre)
    End Function
    Public Function SelectxId(ByVal Codigo As Integer) As List(Of Entidades.Modelo)
        Return objDAOModelo.SelectxId(Codigo)
    End Function

End Class
