﻿Imports DAO
Imports Entidades
Imports System.Data.SqlClient
Public Class bl_reposicionStock8
    Dim cn As SqlConnection = Nothing
    Dim tr As SqlTransaction = Nothing
    Public Function listar_reposicionStock_datatable(ByVal fechainicial As Date, ByVal fechafinal As Date, ByVal idTienda As Integer, _
                                          ByVal idEmpresa As Integer, ByVal idalmacencomparativo As String)
        cn = (New DAO.Conexion).ConexionSIGE
        Return (New DAO.DAO_ReposicionStock).lista_reposicionStock_datatable(cn, fechainicial, fechafinal, idTienda, idEmpresa, idalmacencomparativo)
    End Function
    Public Function listar_reposicionStock_lista(ByVal fechainicial As Date, ByVal fechafinal As Date, ByVal idTienda As Integer, _
                                          ByVal idEmpresa As Integer, ByVal idalmacencomparativo As String) As List(Of Entidades.be_listaReponerStock)
        cn = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO.DAO_ReposicionStock).lista_reposicionStock_lista(cn, fechainicial, fechafinal, idTienda, idEmpresa, idalmacencomparativo)
    End Function

    Public Function listar_reposicionStock_listaReportetop(ByVal fechainicial As Date, ByVal fechafinal As Date, ByVal idTienda As Integer, _
                                       ByVal idEmpresa As Integer, ByVal idalmacencomparativo As String) As List(Of Entidades.be_listaReponerStock8)
        cn = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO.DAO_ReposicionStock).listar_reposicionStock_listaReportetop(cn, fechainicial, fechafinal, idTienda, idEmpresa, idalmacencomparativo)
    End Function
    Public Function listarObjetosReposicionStock(idusuario As Integer) As be_reponerStockListarObjetos
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO_ReposicionStock).ListarObjetosGenerales(cn, idusuario)
        cn.Close()
    End Function

    Public Function CrearCotizacion(ByVal idUsuario As Integer, lista As List(Of Entidades.be_reposicionStock))
        Dim retornar As Boolean = False
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        tr = cn.BeginTransaction(IsolationLevel.Serializable)
        Dim idDocumento As Integer = 0
        Dim idDetalleDocumento As Integer = 0
        Try
            idDocumento = (New DAO_ReposicionStock).CrearCabeceraCotizacion(tr, cn, idUsuario, idDocumento)

            idDetalleDocumento = (New DAO_ReposicionStock).CrearDetalleCotizacion(tr, cn, idUsuario, lista, idDocumento)
            tr.Commit()
            retornar = True
        Catch ex As Exception
            retornar = False
            tr.Rollback()
        End Try
        Return retornar
    End Function
End Class
