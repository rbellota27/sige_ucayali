﻿Imports DAO
Imports Entidades
Imports System.Data.SqlClient
Public Class bl_Pagosdocumentos
    Dim cn As SqlConnection


    Public Function selectDocumentos(ByVal idEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdArea As Integer, ByVal Opcion_Aprobacion As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal monto As Decimal) As List(Of be_PagosDocumentos)

        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Dim lista As New List(Of Entidades.be_PagosDocumentos)
            lista = (New DAO_Pagosdocumentos).selectDocumentos(cn, idEmpresa, IdTienda, IdArea, Opcion_Aprobacion, FechaInicio, FechaFin, monto)
            'tr.Commit()
            Return lista
        Catch ex As Exception
            Throw ex
            'tr.Rollback()
            Throw ex
        End Try
    End Function


    Public Function selectDocumentosPagadosaExportar(ByVal idEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdArea As Integer, ByVal Opcion_Aprobacion As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal monto As Decimal) As List(Of be_PagosDocumentosExport)

        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Dim lista As New List(Of Entidades.be_PagosDocumentosExport)
            lista = (New DAO_Pagosdocumentos).selectDocumentosPagadosaExportar(cn, idEmpresa, IdTienda, IdArea, Opcion_Aprobacion, FechaInicio, FechaFin, monto)
            'tr.Commit()
            Return lista
        Catch ex As Exception
            Throw ex
            'tr.Rollback()
            Throw ex
        End Try
    End Function
    Public Function selectDocumentosPagados(ByVal idEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdArea As Integer, ByVal Opcion_Aprobacion As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal monto As Decimal) As List(Of be_PagosDocumentos)

        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Dim lista As New List(Of Entidades.be_PagosDocumentos)
            lista = (New DAO_Pagosdocumentos).selectDocumentosPagados(cn, idEmpresa, IdTienda, IdArea, Opcion_Aprobacion, FechaInicio, FechaFin, monto)
            'tr.Commit()
            Return lista
        Catch ex As Exception
            Throw ex
            'tr.Rollback()
            Throw ex
        End Try
    End Function

    Public Function selectSumarTotales(ByVal idEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdArea As Integer, ByVal Opcion_Aprobacion As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal monto As Decimal) As List(Of be_PagosDocumentos)

        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Dim lista As New List(Of Entidades.be_PagosDocumentos)
            lista = (New DAO_Pagosdocumentos).selectSumarTotales(cn, idEmpresa, IdTienda, IdArea, Opcion_Aprobacion, FechaInicio, FechaFin, monto)
            'tr.Commit()
            Return lista
        Catch ex As Exception
            Throw ex
            'tr.Rollback()
            Throw ex
        End Try
    End Function

    Public Function UpdateDocumentoReprogramados(ByVal Fecha As String, ByVal iddocumento As Integer) As List(Of be_PagosDocumentos)

        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Dim lista As New List(Of Entidades.be_PagosDocumentos)
            lista = (New DAO_Pagosdocumentos).UpdateDocumentoReprogramados(cn, Fecha, iddocumento)

            Return lista
        Catch ex As Exception
            Throw ex

            Throw ex
        End Try
    End Function


    'Public Function UpdateDocumentoReprogramadosF(ByVal iddocumento As Integer, ByVal Fecha As String) As List(Of be_PagosDocumentos)

    '    cn = (New Conexion).ConexionSIGE
    '    cn.Open()
    '    Try
    '        Dim lista As New List(Of Entidades.be_PagosDocumentos)
    '        lista = (New DAO_Pagosdocumentos).UpdateDocumentoReprogramadosF(cn, iddocumento, Fecha)

    '        Return lista
    '    Catch ex As Exception
    '        Throw ex

    '        Throw ex
    '    End Try
    'End Function


    Public Function UpdateDocumento(ByVal iddocumento As Integer, ByVal idrequerimiento As Integer) As List(Of be_PagosDocumentos)

        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Dim lista As New List(Of Entidades.be_PagosDocumentos)
            lista = (New DAO_Pagosdocumentos).UpdateDocumento(cn, iddocumento, idrequerimiento)
            'tr.Commit()
            Return lista
        Catch ex As Exception
            Throw ex
            'tr.Rollback()
            Throw ex
        End Try
    End Function


    Public Function UpdateDocumentoRetencion(ByVal iddocumento As Integer, ByVal idrequerimiento As Integer) As List(Of be_PagosDocumentos)

        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Dim lista As New List(Of Entidades.be_PagosDocumentos)
            lista = (New DAO_Pagosdocumentos).UpdateDocumentoRetencion(cn, iddocumento, idrequerimiento)
            'tr.Commit()
            Return lista
        Catch ex As Exception
            Throw ex
            'tr.Rollback()
            Throw ex
        End Try
    End Function

End Class

