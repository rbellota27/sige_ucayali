﻿Imports DAO
Imports Entidades
Imports System.Data.SqlClient

Public Class bl_RequerimientosProgramados
    Dim cn As SqlConnection

    Public Function listarprogramaciones(ByVal fechaInicio As String, ByVal fechaFinal As String) As List(Of be_RequerimientoProgramados)
        cn = (New Conexion).ConexionSIGE
        cn.Open()

        Try
            Dim lista As New List(Of Entidades.be_RequerimientoProgramados)
            lista = (New DAO_RequerimientosProgramados).listarprogramaciones(cn, fechaInicio, fechaFinal)

            Return lista
        Catch ex As Exception
            Throw ex

            Throw ex
        End Try
    End Function

    Public Function listarprogramacionesEstado(ByVal fechaInicio As String, ByVal fechaFinal As String, ByVal Iddocumento As Integer, ByVal IdUsuario As Integer) As List(Of be_RequerimientoProgramados)
        cn = (New Conexion).ConexionSIGE
        cn.Open()

        Try
            Dim lista As New List(Of Entidades.be_RequerimientoProgramados)
            lista = (New DAO_RequerimientosProgramados).listarprogramacionesEstado(cn, fechaInicio, fechaFinal, Iddocumento, IdUsuario)

            Return lista
        Catch ex As Exception
            Throw ex

            Throw ex
        End Try
    End Function

    Public Function listarprogramacionesEstadoReprogramado(ByVal fechaInicio As String, ByVal fechaFinal As String, ByVal FechaCambio As String, ByVal Iddocumento As Integer) As List(Of be_RequerimientoProgramados)
        cn = (New Conexion).ConexionSIGE
        cn.Open()

        Try
            Dim lista As New List(Of Entidades.be_RequerimientoProgramados)
            lista = (New DAO_RequerimientosProgramados).listarprogramacionesEstadoReprogramado(cn, fechaInicio, fechaFinal, FechaCambio, Iddocumento)

            Return lista
        Catch ex As Exception
            Throw ex

            Throw ex
        End Try
    End Function

    Public Function listarmonto(ByVal fechaInicio As String, ByVal fechaFinal As String, ByVal Iddocumento As Integer, ByVal Usuario As Integer) As List(Of be_MontoProgramado)
        cn = (New Conexion).ConexionSIGE
        cn.Open()

        Try
            Dim lista As New List(Of Entidades.be_MontoProgramado)
            lista = (New DAO_RequerimientosProgramados).listarmonto(cn, fechaInicio, fechaFinal, Iddocumento, Usuario)

            Return lista
        Catch ex As Exception
            Throw ex

            Throw ex
        End Try
    End Function

    'Public Function listarDetraccionesMasivasEstados(ByVal iddocumento As String, ByVal estado As String) As List(Of be_detraccionMasiva)
    '    cn = (New Conexion).ConexionSIGE
    '    cn.Open()

    '    Try
    '        Dim lista As New List(Of Entidades.be_detraccionMasiva)
    '        lista = (New DAO_PagoDetraccionesMasivas).listarDetraccionesMasivasEstados(cn, iddocumento, estado)

    '        Return lista
    '    Catch ex As Exception
    '        Throw ex

    '        Throw ex
    '    End Try
    'End Function

    Public Function AnularCheques(ByVal iddocumento As Integer) As List(Of be_RequerimientoProgramados)

        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Dim lista As New List(Of Entidades.be_RequerimientoProgramados)
            lista = (New DAO_RequerimientosProgramados).AnularCheque(cn, iddocumento)

            Return lista
        Catch ex As Exception
            Throw ex

            Throw ex
        End Try
    End Function


End Class




