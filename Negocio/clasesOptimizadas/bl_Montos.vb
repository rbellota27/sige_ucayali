﻿Imports DAO
Imports Entidades
Imports System.Data.SqlClient
Public Class bl_Montos


    Dim cn As SqlConnection

    Public Function selectSumarTotales(ByVal idEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdArea As Integer, ByVal Opcion_Aprobacion As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal monto As Decimal) As List(Of be_Montos)

        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Dim lista As New List(Of Entidades.be_Montos)
            lista = (New DAO_Montos).selectSumarTotales(cn, idEmpresa, IdTienda, IdArea, Opcion_Aprobacion, FechaInicio, FechaFin, monto)
            'tr.Commit()
            Return lista
        Catch ex As Exception
            Throw ex
            'tr.Rollback()
            Throw ex
        End Try
    End Function




End Class
