﻿Imports DAO
Imports Entidades
Imports System.Data.SqlClient
Public Class bl_FrmGenerales
    Dim cn As SqlConnection = Nothing
    Public Function cls_frmCancelarProvisiones(ByVal idUsuario As Integer) As be_frmCargaPrincipal
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO_FrmGenerales).cls_frmCancelarProvisiones(cn, idUsuario)
    End Function

    Public Function listarTrabajadores() As DataTable
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO_Programaciones).listarTrabajadores(cn)
    End Function

    Public Function reportePicking(ByVal idSerie As Integer, ByVal codigo As String, ByVal idTipodocumento As Integer) As DataTable
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO_FrmGenerales).reportePicking(cn, idSerie, codigo, idTipodocumento)
    End Function

    Public Function detectarConexionSOLUCONT() As String
        Dim mensaje As String = ""
        Try
            cn = (New Conexion).ConexionSOLUCONT
            cn.Open()
        Catch ex As System.Data.SqlClient.SqlException
            mensaje = ex.Number
        End Try
        Return mensaje
    End Function
End Class
