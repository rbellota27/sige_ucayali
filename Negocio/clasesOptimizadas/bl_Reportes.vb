﻿Imports System.Data.SqlClient
Imports DAO
Imports Entidades
Public Class bl_Reportes
    Dim cn As SqlConnection = Nothing
    Public Function reporteDespacho_clienteFinal(ByVal flag As Boolean, ByVal fechaInicio As Date, fechaFinal As Date) As DataTable
        cn = (New Conexion).ConexionSIGE        
        Return (New DAO_Reportes).rpt_despacho_clienteFinal(cn, flag, fechaInicio, fechaFinal)
    End Function

    Public Function reporteDespacho_sanicenter_subcontratado(ByVal flag As Boolean, ByVal fechaInicio As Date, fechaFinal As Date) As DataTable
        cn = (New Conexion).ConexionSIGE
        Return (New DAO_Reportes).rpt_despacho_Sanicenter_subContratado(cn, flag, fechaInicio, fechaFinal)
    End Function

    Public Function rptGenerico(ByVal IdUSuario As Integer) As List(Of be_reportes)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO_Reportes).rptGenerico(cn, IdUSuario)
    End Function

    Public Function rptFiltros(ByVal idReporte As Integer, ByVal IdUSuario As Integer) As DataTable
        cn = (New Conexion).ConexionSIGE
        Return (New DAO_Reportes).rptFiltros(cn, idReporte, IdUSuario)
    End Function
    Public Function ejecutaScripts(ByVal script As String) As DataTable
        cn = (New Conexion).ConexionSIGE
        Return (New DAO_Reportes).ejecutaScripts(cn, script)
    End Function

    Public Function ejecutaProceduresReportes(ByVal idReporte As Integer, ByVal parametros As String) As DataTable
        cn = (New Conexion).ConexionSIGE
        Return (New DAO_Reportes).ejecutaProceduresReportes(cn, idReporte, parametros)
    End Function
End Class
