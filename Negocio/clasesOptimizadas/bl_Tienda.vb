﻿Imports DAO
Imports Entidades
Imports System.Data.SqlClient
Public Class bl_Tienda
    Dim cn As SqlConnection

    Public Function selectTiendas(ByVal idEmpresa As Integer) As List(Of be_Tienda)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Dim lista As New List(Of Entidades.be_Tienda)
            lista = (New DAO_Tiendas).selectTiendas(cn, idEmpresa)
            'tr.Commit()
            Return lista
        Catch ex As Exception
            Throw ex
            'tr.Rollback()
            Throw ex
        End Try
    End Function


    Public Function selectTiendasN(ByVal idEmpresa As Integer) As List(Of be_Tienda)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Dim lista As New List(Of Entidades.be_Tienda)
            lista = (New DAO_Tiendas).selectTiendasN(cn, idEmpresa)
            'tr.Commit()
            Return lista
        Catch ex As Exception
            Throw ex
            'tr.Rollback()
            Throw ex
        End Try
    End Function

End Class
