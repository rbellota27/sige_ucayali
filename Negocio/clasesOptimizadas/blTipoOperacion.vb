﻿Imports DAO
Imports Entidades
Imports System.Data.SqlClient
Public Class blTipoOperacion
    Dim cn As SqlConnection

    Public Function listarTipoOperacion(ByVal IdMotivotraslado As Integer) As List(Of be_TipoOperacion)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        ' tr = cn.BeginTransaction(IsolationLevel.Serializable)
        Try
            Dim lista As New List(Of Entidades.be_TipoOperacion)
            lista = (New DAO_TipoOperacion).listarTipoOperacion(cn, IdMotivotraslado)
            'tr.Commit()
            Return lista
        Catch ex As Exception
            Throw ex
            'tr.Rollback()
            Throw ex
        End Try
    End Function

End Class
