﻿Imports DAO
Imports Entidades
Imports System.Data.SqlClient

Public Class blPagoDetraccionesMasivas
    Dim cn As SqlConnection

    Public Function listarDetraccionesMasivas(ByVal fechaInicio As String, ByVal fechaFinal As String, _
                                           ByVal anio As String) As List(Of be_detraccionMasiva)
        cn = (New Conexion).ConexionSIGE
        cn.Open()

        Try
            Dim lista As New List(Of Entidades.be_detraccionMasiva)
            lista = (New DAO_PagoDetraccionesMasivas).listarDetraccionesMasivas(cn, fechaInicio, fechaFinal, anio)

            Return lista
        Catch ex As Exception
            Throw ex

            Throw ex
        End Try
    End Function



    Public Function listaDetraccionesporLote(ByVal lote As String, ByVal anio As String) As List(Of be_detraccionMasiva)
        cn = (New Conexion).ConexionSIGE
        cn.Open()

        Try
            Dim lista As New List(Of Entidades.be_detraccionMasiva)
            lista = (New DAO_PagoDetraccionesMasivas).listaDetraccionesporLote(cn, lote, anio)

            Return lista
        Catch ex As Exception
            Throw ex

            Throw ex
        End Try
    End Function

    Public Function listarDetraccionesMasivasEstados(ByVal iddocumento As String, ByVal estado As String) As List(Of be_detraccionMasiva)
        cn = (New Conexion).ConexionSIGE
        cn.Open()

        Try
            Dim lista As New List(Of Entidades.be_detraccionMasiva)
            lista = (New DAO_PagoDetraccionesMasivas).listarDetraccionesMasivasEstados(cn, iddocumento, estado)

            Return lista
        Catch ex As Exception
            Throw ex

        End Try
    End Function

    Public Function listarDetraccionesMasivasMontosEstados(ByVal iddocumento As String, ByVal MontroN As Integer, ByVal estado As String) As List(Of be_detraccionMasiva)
        cn = (New Conexion).ConexionSIGE
        cn.Open()

        Try
            Dim lista As New List(Of Entidades.be_detraccionMasiva)
            lista = (New DAO_PagoDetraccionesMasivas).listarDetraccionesMasivasMontosEstados(cn, iddocumento, MontroN, estado)

            Return lista
        Catch ex As Exception
            Throw ex

            Throw ex
        End Try
    End Function




End Class




