﻿
Imports DAO
Imports Entidades
Imports System.Data.SqlClient
Public Class bl_EstadoLetras
    Dim cn As SqlConnection


    Public Function listarEstadoLetras(ByVal IdEstado As Integer) As List(Of be_EstadoLetras)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        Try
            Dim lista As New List(Of Entidades.be_EstadoLetras)
            lista = (New DAO_EstadoLetras).ListarEstadoLetras(cn, IdEstado)
            'tr.Commit()
            Return lista
        Catch ex As Exception
            Throw ex
            'tr.Rollback()
            Throw ex
        End Try
    End Function
End Class
