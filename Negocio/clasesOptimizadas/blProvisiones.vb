﻿Imports System.Data.SqlClient
Public Class blProvisiones    
    Dim cn As SqlConnection
    Public Function listarDocumentosProvisionados(ByVal docSerie As String, ByVal docCodigo As String, ByVal fecInicio As Date, _
                                                  ByVal fecFin As Date, ByVal flagFiltro As Integer, ByVal nroruc As String) As List(Of Entidades.Documento)
        cn = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO.DAO_Provisiones).listaProvisiones(cn, docSerie, docCodigo, fecInicio, fecFin, flagFiltro, nroruc)
        cn.Close()
    End Function

    Public Function listarDocumentosProvisionadosV2(ByVal docSerie As String, ByVal docCodigo As String, ByVal fecInicio As Date, _
                                              ByVal fecFin As Date, ByVal flagFiltro As Integer, ByVal nroruc As String, ByVal nrovoucher As String) As List(Of Entidades.Documento)
        cn = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO.DAO_Provisiones).listarDocumentosProvisionadosV2(cn, docSerie, docCodigo, fecInicio, fecFin, flagFiltro, nroruc, nrovoucher)
        cn.Close()
    End Function


    Public Function listarRelacionRequerimento_Provision(ByVal idRequerimiento As Integer) As List(Of Entidades.Documento)
        cn = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO.DAO_Provisiones).listarRelacionRequetimento_Provision(cn, idRequerimiento)
        cn.Close()
    End Function
End Class
