﻿Imports DAO
Imports Entidades
Imports System.Data.SqlClient
Public Class bl_estructuraFacturacion
    Public Shared Function estructuraFacturacion(ByVal idTipoDocumento) As List(Of be_facturacionElectronica)
        Dim cn As SqlConnection
        cn = (New Conexion).ConexionSIGE()
        cn.Open()
        Return (New DAO_FacturacionElectronica).estructuraFacturacion(cn, idTipoDocumento)
    End Function

    Public Function fun_GENERAR_PRINT_FAC_ELECTRONICA(idSerie As Integer, codigo As String)
        Dim cn As SqlConnection
        cn = (New Conexion).ConexionSIGE()
        cn.Open()
        Return (New DAO_FacturacionElectronica).fun_GENERAR_PRINT_FAC_ELECTRONICA(cn, idSerie, codigo)
    End Function

    Public Function fun_GENERAR_PRINT_RETENCION_ELECTRONICA(IdDocumento As Integer)
        Dim cn As SqlConnection
        cn = (New Conexion).ConexionSIGE()
        cn.Open()
        Return (New DAO_FacturacionElectronica).fun_GENERAR_PRINT_RETENCION_ELECTRONICA(cn, IdDocumento)
    End Function

End Class
