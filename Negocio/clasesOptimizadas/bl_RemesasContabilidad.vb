﻿Imports DAO
Imports Entidades
Imports System.Data.SqlClient

Public Class bl_RemesasContabilidad
    Dim cn As SqlConnection


    Public Function listarRemesasContabilidad(ByVal Empresa As String, ByVal Moneda As Integer, ByVal banco As Integer, ByVal fechaInicio As String, ByVal FechaFinal As String, ByVal FechaAsiento As String) As List(Of be_RemesasContabilidad)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        ' tr = cn.BeginTransaction(IsolationLevel.Serializable)
        Try
            Dim lista As New List(Of Entidades.be_RemesasContabilidad)
            lista = (New DAO_RemesasContabilidad).listarRemesasContabilidad(cn, Empresa, Moneda, banco, fechaInicio, FechaFinal, FechaAsiento)
            'tr.Commit()
            Return lista
        Catch ex As Exception
            Throw ex
            'tr.Rollback()
            Throw ex
        End Try
    End Function


    '   UpdateDocumentoEgreso SP_Marcar_Remesas_Contabilidad

    Public Function UpdateDocumentoEgreso(ByVal IdMovBanco As Integer, ByVal FechaAsiento As String) As List(Of be_RemesasContabilidad)
        cn = (New Conexion).ConexionSIGE
        cn.Open()
        ' tr = cn.BeginTransaction(IsolationLevel.Serializable)
        Try
            Dim lista As New List(Of Entidades.be_RemesasContabilidad)
            lista = (New DAO_RemesasContabilidad).UpdateDocumentoEgreso(cn, IdMovBanco, FechaAsiento)
            'tr.Commit()
            Return lista
        Catch ex As Exception
            Throw ex
            'tr.Rollback()
            Throw ex
        End Try
    End Function


    'Public Sub CreacionAsientosContable(ByVal idProgramacion As Integer, ByVal nroOperacion As String, ByVal montoDeposito As Decimal, _
    '                        ByVal idbanco As Integer, ByVal fechaPago As Date, ByVal idPersona As Integer)
    '    cn = (New Conexion).ConexionSIGE
    '    cn.Open()
    '    Try
    '        Dim obj As New DAO_Programaciones

    '        obj.CreacionAsientosContable(cn, idProgramacion, nroOperacion, montoDeposito, idbanco, fechaPago, idPersona)

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub


    Public Function CrearAsientoRemesas(ByVal periodocanc As Integer, ByVal Banco As Integer, ByVal Moneda As Integer, ByVal FechaAsiento As String, ByVal idUsuario As Integer) As String
        cn = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO.DAO_Programaciones).CreacionAsientosContable(cn, periodocanc, Banco, Moneda, FechaAsiento, idUsuario)
    End Function

End Class
