﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'**************** LUNES 17 05 2010 HORA 06 00 PM
Imports System.Data.SqlClient

Public Class MedioPago
    Dim obj As New DAO.DAOMedioPago
    Dim objMedioP_CondicionP As New DAO.DAOMedioP_CondicionP
    Dim objconexion As New DAO.Conexion
    Dim objMedioPago As New DAO.DAOMedioPago



    Public Function MedioPago_MovBanco(ByVal IdTipoConceptoBanco As Integer) As List(Of Entidades.MedioPago)
        Return obj.MedioPago_MovBanco(IdTipoConceptoBanco)
    End Function




#Region "Mantenimiento"
    Public Function InsertarMedioPago_ListaTipoDocumento(ByVal objEnt As Entidades.MedioPago, _
                ByVal ListaTipoDoc_MedioP As List(Of Entidades.TipoDocumento_MedioPago)) As Boolean
        Return obj.InsertarMedioPago_ListaTipoDocumento(objEnt, ListaTipoDoc_MedioP)
    End Function
    Public Function ActualizarMedioPago_ListaTipoDoc(ByVal objEnt As Entidades.MedioPago, _
                    ByVal ListaTipoDoc_MedioP As List(Of Entidades.TipoDocumento_MedioPago)) As Boolean
        Return obj.ActualizarMedioPago_ListaTipoDoc(objEnt, ListaTipoDoc_MedioP)
    End Function
    Public Function SelectCboMpInterfaz() As List(Of Entidades.MedioPago)
        Return obj.SelectCboMpInterfaz
    End Function
    Public Function ListarTipoDoc_MedioP(ByVal idMedioPago As Integer) As List(Of Entidades.TipoDocumento_MedioPago)
        Return obj.ListarTipoDoc_MedioP(idMedioPago)
    End Function
#End Region

    Public Function SelectxIdTipoDocumentoxIdCondicionPago(ByVal IdTipoDocumento As Integer, ByVal IdCondicionPago As Integer) As List(Of Entidades.MedioPago)
        Return obj.SelectxIdTipoDocumentoxIdCondicionPago(IdTipoDocumento, IdCondicionPago)
    End Function

    Public Function SelectIdMedioPagoPrincipal() As Integer
        Return objMedioPago.SelectIdMedioPagoPrincipal
    End Function
    Public Function _CondicionPago_MedioPagoSelectxIdCondicionPago(ByVal IdMedioP As Integer) As List(Of Entidades.MedioPago_CondicionPago)
        Return objMedioP_CondicionP._CondicionPago_MedioPagoSelectxIdCondicionPago(IdMedioP)
        'Return Nothing
    End Function
    Public Function SelectCbo() As List(Of Entidades.MedioPago)
        Return obj.SelectCbo
    End Function
    Public Function SelectAll() As List(Of Entidades.MedioPago)
        Return obj.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.MedioPago)
        Return obj.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.MedioPago)
        Return obj.SelectAllInactivo
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.MedioPago)
        Return obj.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.MedioPago)
        Return obj.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.MedioPago)
        Return obj.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.MedioPago)
        Return obj.SelectxId(id)
    End Function
    Public Function SelectxCodSunat(ByVal codSunat As String) As List(Of Entidades.MedioPago)
        Return obj.SelectxCodSunat(codSunat)
    End Function

    'Public Function InsertaMedioP_CondicionP(ByVal MedioP As Entidades.CondicionPago, ByVal lista As List(Of Entidades.MedioPago_CondicionPago)) As Boolean
    '    Dim cn As SqlConnection = objconexion.ConexionSIGE
    '    Dim tr As SqlTransaction = Nothing
    '    Dim hecho As Boolean = False
    '    Try
    '        cn.Open()
    '        tr = cn.BeginTransaction

    '        '************ Insertar Motivo Traslado
    '        Dim IdCondicionP As Integer = obj.InsertaMedioPago_CP(MedioP, cn, tr)

    '        '************ Insertar Lista
    '        Dim objDaoMedioPago_CondicionPago As New DAO.DAOMedioP_CondicionP
    '        Dim objCondicionP As New Entidades.MedioPago_CondicionPago
    '        objDaoMedioPago_CondicionPago.InsertaListaCondicionPago(cn, tr, lista, IdCondicionP)

    '        tr.Commit()
    '        hecho = True

    '    Catch ex As Exception
    '        tr.Rollback()
    '        hecho = False
    '    Finally

    '        If cn.State = ConnectionState.Open Then cn.Close()

    '    End Try
    '    Return hecho
    'End Function

    'Public Function ActualizaMedioPago_CondicionP(ByVal CondicionP As Entidades.CondicionPago, ByVal lista As List(Of Entidades.MedioPago_CondicionPago)) As Boolean
    '    Dim cn As SqlConnection = objconexion.ConexionSIGE
    '    Dim tr As SqlTransaction = Nothing
    '    Dim hecho As Boolean = False
    '    Try
    '        cn.Open()
    '        tr = cn.BeginTransaction(IsolationLevel.Serializable)
    '        '*************   Actualiza Condición de Pago  ********************
    '        If Not (obj.ActualizaMedioPago_condicionP(CondicionP, cn, tr)) Then
    '            Throw New Exception
    '        End If

    '        '*************  Elimina ActualizaMotivoT_TipoOperacion ************************
    '        Dim obJDaoMedioPago As New DAO.DAOMedioP_CondicionP
    '        If (Not obJDaoMedioPago.DeletexCondicionPago_MedioPagoBorrar(cn, tr, CondicionP.Id)) Then
    '            Throw New Exception
    '        End If
    '        '****************************** Insertar Lista  **************************************
    '        obJDaoMedioPago.InsertaListaCondicionPago(cn, tr, lista, CondicionP.Id)
    '        tr.Commit()
    '        hecho = True
    '    Catch ex As Exception
    '        tr.Rollback()
    '        hecho = False
    '    Finally
    '        If cn.State = ConnectionState.Open Then cn.Close()
    '    End Try
    '    Return hecho
    'End Function
    'Edita la lista CondicionPago_MedioPago
End Class
