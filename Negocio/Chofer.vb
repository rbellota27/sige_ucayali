﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'**************** VIERNES 19 FEB 2010 HORA 02_58 PM








Public Class Chofer
    Private obj As New DAO.DAOChofer

    Public Function ChoferSelectActivoxParams(ByVal dni As String, ByVal ruc As String, _
                                      ByVal RazonApe As String, ByVal tipo As Integer, _
                                      ByVal pageindex As Integer, ByVal pagesize As Integer, _
                                      ByVal estado As Integer) As List(Of Entidades.PersonaView)
        Return obj.ChoferSelectActivoxParams(dni, ruc, RazonApe, tipo, pageindex, pagesize, estado)
    End Function


    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Chofer)
        Return obj.SelectActivoxNombre(nombre)
    End Function

    Public Function listarChoferxID(ByVal idChofer As Integer) As Entidades.Chofer
        Return obj.listarChoferxID(idChofer)
    End Function

    Public Function LN_ActualizaChofer(ByVal chofer As Entidades.Chofer) As Boolean
        Return obj.ActualizaChofer(chofer)
    End Function

    Public Function LN_InsertarChofer(ByVal chofer As Entidades.Chofer) As Boolean
        Return obj.InsertaChofer(chofer)
    End Function
End Class
