﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*****************  VIERNES 22 ENERO 2010 HORA 03_54 PM

Public Class TipoPrecioV
    Private obj As New DAO.DAOTipoPrecioV

    Public Function SelectIdTipoPrecioVDefault() As Integer
        Return obj.SelectIdTipoPrecioVDefault()
    End Function

    Public Function InsertaTipoPrecioV(ByVal tipopreciov As Entidades.TipoPrecioV) As Boolean
        Return obj.InsertaTipoPrecioV(tipopreciov)
    End Function
    Public Function ActualizaTipoPrecioV(ByVal tipopreciov As Entidades.TipoPrecioV) As Boolean
        Return obj.ActualizaTipoPrecioV(tipopreciov)
    End Function
    Public Function SelectAll() As List(Of Entidades.TipoPrecioV)
        Return obj.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.TipoPrecioV)
        Return obj.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.TipoPrecioV)
        Return obj.SelectAllInactivo
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.TipoPrecioV)
        Return obj.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoPrecioV)
        Return obj.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoPrecioV)
        Return obj.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.TipoPrecioV)
        Return obj.SelectxId(id)
    End Function
    Public Function SelectCbo() As List(Of Entidades.TipoPrecioV)
        Return obj.SelectCbo
    End Function

    Public Function SelectCbo2() As List(Of Entidades.TipoPrecioV)
        Return obj.SelectCbo2
    End Function
    Public Function SelectCbo1() As List(Of Entidades.TipoPrecioV)
        Return obj.SelectCbo1
    End Function
End Class
