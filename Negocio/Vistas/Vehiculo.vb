﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Chang Carnero, Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 29-Octubre-2009
'Hora    : 06:00 am
'*************************************************
Imports System.Data.SqlClient
Public Class Vehiculo
    Private obj As New DAO.DAOVehiculo

    Public Function SelectAllVehiculos() As List(Of Entidades.Vehiculo)
        Return obj.SelectAllVehiculos
    End Function
    Public Function SelectAllVehiculosXtipoexistenciaXlineaXsublinea() As List(Of Entidades.Vehiculo)
        Return obj.SelectAllVehiculosXtipoexistenciaXlineaXsublinea
    End Function

    Public Function SelectActivoxNroPlaca(ByVal placa As String) As List(Of Entidades.Vehiculo)
        Return obj.SelectActivoxNroPlaca(placa)
    End Function

    Public Function SeleccionarTransportistas() As DataTable
        Return obj.SeleccionarTransportistas()
    End Function

    Public Function SeleccionarTransportistas_prueba(ByVal inicial As Integer, ByVal final As Integer) As DataSet
        Return obj.SeleccionarTransportistas_prueba(inicial, final)
    End Function

    Public Function SelectxIdVehiculo(ByVal IdVehiculo As Integer) As Entidades.Vehiculo
        Return obj.SelectxIdVehiculo(IdVehiculo)
    End Function

    Public Function SelecFiltradoGrilla(ByVal Placa As String) As List(Of Entidades.Vehiculo)
        Return obj.SelecFiltradoGrilla(Placa)
    End Function

    Public Function InsertaEstadoVehiculo(ByVal EstadoVehiculo As Entidades.Vehiculo) As Boolean
        Return obj.InsertaEstadoVehiculo(EstadoVehiculo)
    End Function

    Public Function SelectCboEstadoVehiculo() As List(Of Entidades.EstadoComboVehiculo)
        Return obj.SelectCboEstadoVehiculo
    End Function
    Public Function InsertaIdProductoVehiculo(ByVal EstadoIdProducto As Entidades.Vehiculo) As Boolean
        Return obj.InsertaIdProductoVehiculo(EstadoIdProducto)
    End Function
    Public Function ActualizaVehiculo(ByVal IdProducto As Entidades.Vehiculo) As Boolean
        Return obj.ActualizaVehiculo(IdProducto)
    End Function
    Public Function SelectCboTipoExistenciaxIdVehiculo() As List(Of Entidades.Vehiculo)
        Return obj.SelectCboTipoExistencia()
    End Function
    Public Function InsertaVehiculoValidadoxPlaca(ByVal producto As Entidades.Vehiculo) As Integer
        Return obj.InsertaVehiculoValidadoxPlaca(producto)
    End Function
    Public Function ActualizaVehiculoValidadoxPlaca(ByVal producto As Entidades.Vehiculo) As Integer
        Return obj.ActualizaVehiculoValidadoxPlaca(producto)
    End Function
    Public Function DeleteVehiculoxIdDocumento(ByVal IdDocumento As Integer) As Integer
        Return obj.DeleteVehiculoxIdDocumento(IdDocumento)
    End Function
    Public Function SelectGrillaVehiculo_Buscar(ByVal Placa As String, ByVal Modelo As String, ByVal Constancia As String, ByVal EstadoProd As String) As List(Of Entidades.Vehiculo)
        Return obj.SelectGrillaVehiculo_Buscar(Placa, Modelo, Constancia, EstadoProd)
    End Function
End Class
