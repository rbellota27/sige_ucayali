﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class TipoDocRef_TipoDoc_View
    Dim obj As New DAO.DAOTipoDocRef_TipoDoc_View
    Public Function SelectxIdTipoDoc(ByVal IdTipoDoc As Integer) As List(Of Entidades.TipoDocRef_TipoDoc_View)
        Return obj.SelectxIdTipoDoc(IdTipoDoc)
    End Function

    Public Sub InsertaListaTipoDocumentoRef(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal lista As List(Of Entidades.TipoDocRef_TipoDoc_View), ByVal IdTipoDocumento As Integer)
        obj.InsertaListaTipoDocumentoRef(cn, tr, lista, IdTipoDocumento)
    End Sub

    Public Function DeletexIdDocumento(ByVal idDocumento As Integer) As Boolean
        obj.DeletexIdDocumento(idDocumento)
    End Function
End Class
