﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.
Imports System.Data.SqlClient
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel

Public Class MovBancoView
    Private obj As New DAO.DAOMovBancoView
    Private cn As SqlConnection
    Private tr As SqlTransaction
    Public Function SelectMovBancosxId(ByVal IdMovBanco As Integer) As Entidades.MovBancoView
        Return obj.SelectMovBancosxId(IdMovBanco)
    End Function

    Public Function SelectxIdBancoxIdCuentaBancaria(ByVal IdBanco As Integer, ByVal IdCuentaBancaria As Integer) As Entidades.MovBancoView
        Return obj.SelectxIdBancoxIdCuentaBancaria(IdBanco, IdCuentaBancaria)
    End Function
    Public Function SelectMovBancosxFechas(ByVal FechaInicial As Date, ByVal FechaFinal As Date, ByVal NroOperacion As String, ByVal IdBanco As Integer, ByVal IdCuentaBancaria As Integer) As List(Of Entidades.MovBancoView)
        Return obj.SelectMovBancosxFechas(FechaInicial, FechaFinal, NroOperacion, IdBanco, IdCuentaBancaria)
    End Function

    Public Function SelectExcelttoGridView(ByVal namearchivo As String, ByVal nom_hoja As String, ByVal IdEstructuraBanco As Integer) As List(Of Entidades.MovBancoView)
        Return obj.SelectExcelttoGridView(namearchivo, nom_hoja, IdEstructuraBanco)
    End Function
    Public Function SelectxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.MovBancoView)
        Return obj.SelectxIdDocumento(IdDocumento)
    End Function
    Public Function SelectUltimoIdentificadorExport() As Integer
        Return obj.SelectUltimoIdentificadorExport()
    End Function
    Public Function SelectAll() As List(Of Entidades.MovBancoView)
        Return obj.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.MovBancoView)
        Return obj.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.MovBancoView)
        Return obj.SelectAllInactivo
    End Function
    Public Function SelectxId(ByVal Id As Integer) As Entidades.MovBancoView
        Return obj.SelectxId(Id)
    End Function

    Public Function SelectInsercionExcel(ByVal IdentificadorExcel As Integer) As List(Of Entidades.MovBancoView)
        Return obj.SelectInsercionExcel(IdentificadorExcel)
    End Function

    Public Function InsertT(ByVal x As Entidades.MovBancoView) As Boolean
        Return obj.InsertT(CType(x, Entidades.MovBanco))
    End Function

    Public Function UpdateT(ByVal x As Entidades.MovBancoView) As Boolean
        Return obj.UpdateT(CType(x, Entidades.MovBanco))
    End Function

    Public Function InsertAll(ByVal x As Entidades.MovBancoView, Optional ByVal Lista As List(Of Entidades.Anexo_MovBanco) = Nothing) As Boolean
        Return obj.InsertAll(CType(x, Entidades.MovBanco), Lista)
    End Function
    Public Function Read(ByVal tie_Estado As Integer) As List(Of Entidades.Tienda)
        tie_Estado = 1
        Dim Lista As List(Of Entidades.Tienda) = obj.SelectTiendaExcelImport(tie_Estado)
        Return Lista
    End Function

    Public Function InsertMovBancoExcel(ByVal Lista As List(Of Entidades.MovBancoView)) As Boolean
        'Return obj.InsertAll(CType(x, Entidades.MovBanco), Lista)
        Try
            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            tr = cn.BeginTransaction
            For i As Integer = 0 To Lista.Count - 1
                obj.InsertMovBancoExcel(Lista(i), cn, tr)
            Next
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
    End Function

    Public Function UpdateAll(ByVal x As Entidades.MovBancoView, Optional ByVal Lista As List(Of Entidades.Anexo_MovBanco) = Nothing) As Boolean
        Return obj.UpdateAll(CType(x, Entidades.MovBanco), Lista)
    End Function

    Private P As New Entidades.MovBancoView
    'P: Entidad que va a guardar los parametros que se van a utilizar en las funciones
    'Es una variable auxiliar

    Private Function FiltrarEntidadxParametros(ByVal x As Entidades.MovBancoView) As Boolean
        'x va a ser el objeto a filtrar

        If (x.IdMovBanco = P.IdMovBanco Or P.IdMovBanco = Nothing Or P.IdMovBanco = 0) _
        And (x.IdBanco = P.IdBanco Or P.IdBanco = Nothing Or P.IdBanco = 0) _
        And (x.IdCuentaBancaria = P.IdCuentaBancaria Or P.IdCuentaBancaria = Nothing Or P.IdCuentaBancaria = 0) _
        And (x.IdTipoConceptoBanco = P.IdTipoConceptoBanco Or (P.IdTipoConceptoBanco = Nothing) Or (P.IdTipoConceptoBanco = 0)) _
        And (x.IdConceptoMovBanco = P.IdConceptoMovBanco Or (P.IdConceptoMovBanco = Nothing) Or (P.IdConceptoMovBanco = 0)) _
        And (x.NroOperacion.ToLower.Trim.StartsWith(P.NroOperacion.ToLower.Trim) Or P.NroOperacion = "") _
        And (x.EstadoMov = P.EstadoMov Or (IsNothing(P.EstadoMov))) Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function FiltrarLista(ByVal filtro As Entidades.MovBancoView, _
    ByVal List As List(Of Entidades.MovBancoView)) As List(Of Entidades.MovBancoView)

        P = filtro
        Return List.FindAll(AddressOf FiltrarEntidadxParametros)

    End Function

    Public Function FiltrarListaxRangoFechas(ByVal filtro As Entidades.MovBancoView, _
       ByVal List As List(Of Entidades.MovBancoView), _
       ByVal fecha_ini As Date, ByVal fecha_fin As Date) As List(Of Entidades.MovBancoView)

        Dim L2, L3 As List(Of Entidades.MovBancoView)
        L3 = New List(Of Entidades.MovBancoView)
        Dim i As Integer

        P = filtro

        L2 = List.FindAll(AddressOf FiltrarEntidadxParametros)
        For i = 0 To L2.Count - 1
            If L2(i).FechaMov >= fecha_ini And L2(i).FechaMov <= fecha_fin Then
                L3.Add(L2(i))
            End If
        Next
        Return L3

    End Function

    Public Function ValidarEntidadxNombre(ByVal filtro As Entidades.MovBancoView, ByVal List As List(Of Entidades.MovBancoView)) As Boolean
        'X es el valor a verificar, 

        Dim x As New Entidades.MovBancoView
        x.IdMovBanco = filtro.IdMovBanco
        x.NroOperacion = filtro.NroOperacion

        Dim List2 As List(Of Entidades.MovBancoView)
        List2 = ValidarLista(x, List)

        If List2.Count > 0 Then 'Existen registros  
            Return False
        Else
            Return True
        End If

    End Function

    Private Function ValidarEntidadxParametros(ByVal x As Entidades.MovBancoView) As Boolean
        'Se utiliza a la hora de insertar o eliminar
        'x va a ser el objeto a filtrar
        'En este caso no se cuenta el registro analizado
        If (x.IdMovBanco <> P.IdMovBanco) _
        And (x.NroOperacion.ToLower.Trim = P.NroOperacion.ToLower.Trim Or P.NroOperacion = "") Then
            'And (x.Estado = P.Estado Or (Not P.Estado.HasValue))
            Return True
        Else
            Return False
        End If
    End Function
    Public Function ValidarLista(ByVal filtro As Entidades.MovBancoView, ByVal List As List(Of Entidades.MovBancoView)) As List(Of Entidades.MovBancoView)
        P = filtro
        Return List.FindAll(AddressOf ValidarEntidadxParametros)

    End Function


End Class