﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Public Class PostView
    Private obj As New DAO.DAOPostView

    Public Function SelectIdPosxTipoTarjetaCaja(ByVal IdTarjeta As Integer, ByVal IdTipoTarjeta As Integer, ByVal IdCaja As Integer) As Integer
        Return obj.SelectIdPosxTipoTarjetaCaja(IdTarjeta, IdTipoTarjeta, IdCaja)
    End Function

    Public Function SelectxIdPost(ByVal IdPost As Integer) As Entidades.Post
        Return obj.SelectxIdPost(IdPost)
    End Function

    Public Function SelectCboxIdCaja(ByVal IdCaja As Integer) As List(Of Entidades.Post)
        Return obj.SelectCboxIdCaja(IdCaja)
    End Function

    Public Function SelectAll() As List(Of Entidades.PostView)
        Return obj.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.PostView)
        Return obj.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.PostView)
        Return obj.SelectAllInactivo
    End Function

    Public Function SelectxIdBanco(ByVal IdBanco As Integer) As List(Of Entidades.PostView)
        Return obj.SelectxIdBanco(IdBanco)
    End Function

    Public Function SelectxIdCuentaBancaria(ByVal IdCuentaBancaria As Integer) As List(Of Entidades.PostView)
        Return obj.SelectxIdCuentaBancaria(IdCuentaBancaria)
    End Function

    Public Function SelectxIdBancoxIdCuentaBancaria(ByVal IdBanco As Integer, _
        ByVal IdCuentaBancaria As Integer) As List(Of Entidades.PostView)
        Return obj.SelectxIdBancoxIdCuentaBancaria(IdBanco, IdCuentaBancaria)
    End Function

    Public Function InsertT_AddListPos_TipoTarjeta(ByVal x As Entidades.Post, _
    ByVal L As List(Of Entidades.Pos_TipoTarjeta)) As Integer
        Return obj.InsertT_AddListPos_TipoTarjeta(CType(x, Entidades.Post), L, Nothing, Nothing)
    End Function
    Public Function UpdateT_AddListPos_TipoTarjeta(ByVal x As Entidades.Post, _
    ByVal L As List(Of Entidades.Pos_TipoTarjeta)) As Integer
        Return obj.UpdateT_AddListPos_TipoTarjeta(CType(x, Entidades.Post), L, Nothing, Nothing)
    End Function
    Public Function InsertT(ByVal x As Entidades.PostView) As Boolean
        Return obj.InsertT(CType(x, Entidades.Post))
    End Function

    Public Function UpdateT(ByVal x As Entidades.PostView) As Boolean
        Return obj.UpdateT(CType(x, Entidades.Post))
    End Function

    Private P As New Entidades.PostView
    'P: Entidad que va a guardar los parametros que se van a utilizar en las funciones
    'Es una variable auxiliar

    Private Function FiltrarEntidadxParametros(ByVal x As Entidades.PostView) As Boolean
        'x va a ser el objeto a filtrar

        If (x.Id = P.Id Or P.Id = 0) _
        And (x.Descripcion.ToLower.Trim.StartsWith(P.Descripcion.ToLower.Trim) Or P.Descripcion = "") _
        And (x.Identificador.ToLower.Trim.StartsWith(P.Identificador.ToLower.Trim) Or P.Identificador = "") _
        And (x.IdBanco = P.IdBanco Or P.IdBanco = 0) _
        And (x.IdCuentaBancaria = P.IdCuentaBancaria Or P.IdCuentaBancaria = 0) _
        And (x.Estado = P.Estado Or (Not P.Estado.HasValue)) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function FiltrarLista(ByVal filtro As Entidades.PostView, ByVal List As List(Of Entidades.PostView)) As List(Of Entidades.PostView)
        P = filtro
        Return List.FindAll(AddressOf FiltrarEntidadxParametros)

    End Function

    Public Function ValidarEntidadxNombre(ByVal filtro As Entidades.PostView, ByVal List As List(Of Entidades.PostView)) As Boolean
        'X es el valor a verificar, 

        Dim x As New Entidades.PostView
        x.Id = filtro.Id
        x.Nombre = filtro.Nombre

        Dim List2 As List(Of Entidades.PostView)
        List2 = ValidarLista(x, List)

        If List2.Count > 0 Then 'Existen registros  
            Return False
        Else
            Return True
        End If


    End Function

    Private Function ValidarEntidadxParametros(ByVal x As Entidades.PostView) As Boolean
        'Se utiliza a la hora de insertar o eliminar
        'x va a ser el objeto a filtrar
        'En este caso no se cuenta el registro analizado
        If (x.Id <> P.Id) _
        And (x.Nombre.ToLower.Trim = P.Nombre.ToLower.Trim Or P.Nombre = "") Then
            'And (x.Estado = P.Estado Or (Not P.Estado.HasValue))
            Return True
        Else
            Return False
        End If
    End Function
    Public Function ValidarLista(ByVal filtro As Entidades.PostView, ByVal List As List(Of Entidades.PostView)) As List(Of Entidades.PostView)
        P = filtro
        Return List.FindAll(AddressOf ValidarEntidadxParametros)

    End Function

End Class
