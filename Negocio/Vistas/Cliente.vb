﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'08_febrero_2010
Public Class Cliente
    Dim obj As New DAO.DAOCliente
    Public Function ReporteDetalleDAOT(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idpersona As Integer, ByVal fechaInicio As String, ByVal fechafin As String, ByVal tipo As Integer, ByVal uit As Decimal, ByVal doc As Integer) As DataSet
        Return obj.ReporteDetalleDAOT(idempresa, idtienda, idpersona, fechaInicio, fechafin, tipo, uit, doc)
    End Function
    Public Function PersonaUpdate_Ventas(ByVal objPersonaView As Entidades.PersonaView) As Boolean
        Return obj.PersonaUpdate_Ventas(objPersonaView)
    End Function

    Public Function PersonaUpdate_MO(ByVal objPersonaView As Entidades.PersonaView) As Boolean
        Return obj.PersonaUpdate_MO(objPersonaView)
    End Function
    Public Function SelectxId_Ventas(ByVal IdPersona As Integer, ByVal idempresa As Integer) As Entidades.PersonaView
        Return obj.SelectxId_Ventas(IdPersona, idempresa)
    End Function

    Public Function SelectxId(ByVal IdPersona As Integer) As Entidades.PersonaView
        Return obj.SelectxId(IdPersona)
    End Function
    Public Function SelectxDni(ByVal Dni As String) As Entidades.PersonaView
        Try
            If obj.SelectxDni(Dni).Estado = CStr(1) Then
                Return obj.SelectxDni(Dni)
            Else
                Throw New Exception("El Cliente esta Inactivo")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectxRuc(ByVal Ruc As String) As Entidades.PersonaView
        Try
            If obj.SelectxRuc(Ruc).Estado = CStr(1) Then
                Return obj.SelectxRuc(Ruc)
            Else
                Throw New Exception("El Cliente esta Inactivo")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectxNombre(ByVal Nombre As String) As List(Of Entidades.PersonaView)
        Return obj.SelectxNombre(Nombre)
    End Function
    Public Function SelectxNombreComercial(ByVal NombreComercial As String) As List(Of Entidades.PersonaView)
        Return obj.SelectxNombreComercial(NombreComercial)
    End Function
    Public Function SelectxRazonSocial(ByVal RazonSocial As String) As List(Of Entidades.PersonaView)
        Return obj.SelectxRazonSocial(RazonSocial)
    End Function
    Public Function SelectAll() As List(Of Entidades.PersonaView)
        Return obj.SelectAll
    End Function
    Public Function SelectGrillaCliente() As List(Of Entidades.PersonaView)
        Return obj.SelectGrillaCliente
    End Function
    Public Function SelectGrillaClientexNombre(ByVal Nombre As String, ByVal Opcion As Integer) As List(Of Entidades.PersonaView)
        Return obj.SelectGrillaClientexNombre(Nombre, Opcion)
    End Function
    Public Function SelectGrillaClientexRazonSocial(ByVal nombre As String) As List(Of Entidades.PersonaView)
        Return obj.SelectGrillaClientexRazonSocial(nombre)
    End Function
    Public Function SelectGrillaClientexDNI(ByVal numero As String) As List(Of Entidades.PersonaView)
        Return obj.SelectGrillaClientexDNI(numero)
    End Function
    Public Function SelectGrillaClientexRUC(ByVal numero As String) As List(Of Entidades.PersonaView)
        Return obj.SelectGrillaClientexRUC(numero)
    End Function
    Public Function SelectGrillaClientexIdPersona(ByVal codigo As String) As List(Of Entidades.PersonaView)
        Return obj.SelectGrillaClientexIdPersona(codigo)
    End Function
    Public Function SelectGrillaClientexEstado(ByVal estado As String) As List(Of Entidades.PersonaView)
        Return obj.SelectGrillaClientexEstado(estado)
    End Function
    Public Function DataSetCxCResumenCliente(ByVal idEmpresa As Integer, ByVal idtienda As Integer, ByVal idmoneda As Integer) As DataSet
        Return obj.DataSetCxCResumenCliente(idEmpresa, idtienda, idmoneda)
    End Function
    Public Function DataSetCxCResumenDocumento(ByVal idEmpresa As Integer, ByVal idtienda As Integer, ByVal idmoneda As Integer, ByVal idpersona As Integer) As DataSet
        Return obj.DataSetCxCResumenDocumento(idEmpresa, idtienda, idmoneda, idpersona)
    End Function
    Public Function RankingClientes(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idCondicionpago As Integer, ByVal idmoneda As Integer, ByVal idTipopersona As Integer, ByVal fechaInicio As String, ByVal fechafin As String, ByVal nprimeros As Integer, ByVal idlinea As Integer, ByVal idsublinea As Integer) As DataSet
        Return obj.RankingClientes(idempresa, idtienda, idCondicionpago, idmoneda, idTipopersona, fechaInicio, fechafin, nprimeros, idlinea, idsublinea)
    End Function
    Public Function DetalleGuiasRemision(ByVal idempresa As Integer, ByVal idAlmacenOrigen As Integer, ByVal idAlmacenDestino As Integer, ByVal fechaInicio As String, ByVal fechafin As String, ByVal vmercaTransito As Integer, ByVal IdTipoOperacion As Integer) As DataSet
        Return obj.DetalleGuiasRemision(idempresa, idAlmacenOrigen, idAlmacenDestino, fechaInicio, fechafin, vmercaTransito, IdTipoOperacion)
    End Function

    Public Function ReporteIngresosxTienda(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal fechaInicio As String, ByVal fechafin As String) As DataSet
        Return obj.ReporteIngresosxTienda(idempresa, idtienda, fechaInicio, fechafin)
    End Function

    Public Function DetalleCxC(ByVal idpersona As Integer, ByVal vopcion As Integer, ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal FechaIni As String, ByVal FechaFin As String) As DataSet
        Return obj.DetalleCxC(idpersona, vopcion, idempresa, idtienda, FechaIni, FechaFin)
    End Function
    Public Function DetalleCxCDocCliente(ByVal idpersona As Integer, ByVal vopcion As Integer, ByVal idempresa As Integer, ByVal idtienda As Integer) As DataSet
        Return obj.DetalleCxCDocCliente(idpersona, vopcion, idempresa, idtienda)
    End Function

    Public Function VentasXTienda(ByVal idempresa As Integer, ByVal idLinea As Integer, ByVal idSublinea As Integer, ByVal idProducto As Integer, ByVal fechaInicio As String, ByVal fechafin As String) As DataSet
        Return obj.VentasXTienda(idempresa, idLinea, idSublinea, idProducto, fechaInicio, fechafin)
    End Function
    Public Function VentasXTiendaExcel(ByVal idempresa As Integer, ByVal idLinea As Integer, ByVal idSublinea As Integer, ByVal idProducto As Integer, ByVal fechaInicio As String, ByVal fechafin As String) As DataSet
        Return obj.VentasXTiendaRSet(idempresa, idLinea, idSublinea, idProducto, fechaInicio, fechafin)
    End Function

End Class
