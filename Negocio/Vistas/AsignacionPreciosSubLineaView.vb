﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class AsignacionPreciosSubLineaView
    Private obj As New DAO.DAOAsignacionPreciosSubLineaView
    Public Function SelectxIdSubLinea(ByVal IdSubLinea As Integer) As List(Of Entidades.AsignacionPreciosSubLineaView)
        'FUNCION UTILIZADA PARA EL REGISTRO DE UN NUEVO PRECIO DE VENTA POR SUBLINEA
        Return obj.SelectxIdSubLinea(IdSubLinea)
    End Function
End Class
