﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

''22_02_2010
Public Class VentasPorArticuloView
    Dim obj As New DAO.DAOVentasPorArticulo
    Public Function CRVentasPorArticulos(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idlinea As Integer, ByVal idSublinea As Integer, ByVal idProducto As Integer, ByVal fechainicio As String, ByVal fechafin As String, ByVal yeari As Integer, ByVal semanai As Integer, ByVal yearf As Integer, ByVal semanaf As Integer, ByVal filtrarsemana As Integer) As DataSet
        Return obj.CRVentasPorArticulos(idempresa, idtienda, idlinea, idSublinea, idProducto, fechainicio, fechafin, yeari, semanai, yearf, semanaf, filtrarsemana)
        '14
    End Function
    Public Function CRVentasPorArticulosResumido(ByVal idEmpresa As Integer, ByVal idtienda As Integer, ByVal idlinea As Integer, ByVal idSublinea As Integer, ByVal idProducto As Integer, ByVal fechainicio As String, ByVal fechafin As String, ByVal yeari As Integer, ByVal semanai As Integer, ByVal yearf As Integer, ByVal semanaf As Integer, ByVal filtrarsemana As Integer) As DataSet
        '15
        Return obj.CRVentasPorArticulosResumido(idEmpresa, idtienda, idlinea, idSublinea, idProducto, fechainicio, fechafin, yeari, semanai, yearf, semanaf, filtrarsemana)
    End Function
    Public Function CRVentasPorArticulosDetallladoValorizado(ByVal idEmpresa As Integer, ByVal idtienda As Integer, ByVal IdTipoExistencia As Integer, ByVal idlinea As Integer, ByVal idSublinea As Integer, ByVal idProducto As Integer, ByVal fechainicio As String, ByVal fechafin As String, ByVal yeari As Integer, ByVal semanai As Integer, ByVal yearf As Integer, ByVal semanaf As Integer, ByVal filtrarsemana As Integer) As DataSet

        Return obj.CRVentasPorArticulosDetallladoValorizado(idEmpresa, idtienda, IdTipoExistencia, idlinea, idSublinea, idProducto, fechainicio, fechafin, yeari, semanai, yearf, semanaf, filtrarsemana)
    End Function
    Public Function CRVentasPorArticulosResumidoValorizado(ByVal idEmpresa As Integer, ByVal idtienda As Integer, ByVal idlinea As Integer, ByVal idSublinea As Integer, ByVal idProducto As Integer, ByVal fechainicio As String, ByVal fechafin As String, ByVal yeari As Integer, ByVal semanai As Integer, ByVal yearf As Integer, ByVal semanaf As Integer, ByVal filtrarsemana As Integer) As DataSet

        Return obj.CRVentasPorArticulosResumidoValorizado(idEmpresa, idtienda, idlinea, idSublinea, idProducto, fechainicio, fechafin, yeari, semanai, yeari, semanaf, filtrarsemana)
    End Function

    Public Function ReporteListaPrecios(ByVal idtienda As Integer, ByVal idlinea As Integer, ByVal idSublinea As Integer, ByVal codSublinea As Integer, ByVal idproducto As Integer, ByVal descripcion As String, ByVal idtipomedida As Integer, ByVal idmoneda As Integer, ByVal Tabla As DataTable) As DataSet
        Return obj.ReporteListaPrecios(idtienda, idlinea, idSublinea, codSublinea, idproducto, descripcion, idtipomedida, idmoneda, Tabla)
    End Function

    Public Function CRVtasProductoProm(ByVal idEmpresa As Integer, ByVal idtienda As Integer, ByVal idDocumento As Integer, ByVal ListaProducto As List(Of Entidades.ProductoView), ByVal fechainicio As String, ByVal fechafin As String, ByVal yeari As Integer, ByVal semanai As Integer, ByVal yearf As Integer, ByVal semanaf As Integer, ByVal filtrarsemana As Integer, ByVal idpersona As Integer) As DataSet
        Return obj.CRVtasProductoProm(idEmpresa, idtienda, idDocumento, ListaProducto, fechainicio, fechafin, yeari, semanai, yearf, semanaf, filtrarsemana, idpersona)
    End Function

End Class
