﻿Imports DAO
Imports System.Data.SqlClient
Public Class LN_Despachos
    Dim clsDatos As New DAODespachos

    Public Function eventoLoad(ByVal filtro As String, ByVal flag As String, ByVal condicion As String, _
                                             ByVal tienda As Integer, ByVal serie As Integer, ByVal documento As Integer, _
                                             ByVal fechaEmision As Date, ByVal check As Boolean)
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Dim listaObjetos As New List(Of Object)

        Dim lista As New List(Of Entidades.be_cronograma)
        cn.Open()
        'Cronograma por atender
        lista = (New DAO.DAODespachos).DAO_ListarDatosGenerales(filtro, flag, condicion, tienda, serie, documento, fechaEmision, check, cn)

        'Carga una lista con los objetos ayudante || chofer || placas
        listaObjetos = (New DAO.DAODespachos).DAO_eventoLoad(cn)
        listaObjetos.Add(lista)
        cn.Close()
        Return listaObjetos
    End Function

    Public Function eventoBuscarProgramacion(ByVal filtro As String, ByVal flag As String, ByVal condicion As String, _
                                             ByVal tienda As Integer, ByVal serie As Integer, ByVal documento As Integer, _
                                             ByVal fechaEmision As Date, ByVal check As Boolean)
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Dim lista As New List(Of Entidades.be_cronograma)
        cn.Open()
        lista = (New DAO.DAODespachos).DAO_ListarDatosGenerales(filtro, flag, condicion, tienda, serie, documento, fechaEmision, check, cn)
        cn.Close()
        Return lista
    End Function


    Public Function eventoFiltrarGuias(ByVal filtro As String, ByVal flag As String, ByVal condicion As String, _
                                             ByVal tienda As Integer, ByVal serie As Integer, ByVal documento As Integer, _
                                             ByVal fechaEmision As Date, ByVal check As Boolean)
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Dim lista As New List(Of Entidades.be_guiaRemision)
        cn.Open()
        lista = (New DAO.DAODespachos).DAO_FiltrarGuias(filtro, flag, condicion, tienda, serie, documento, fechaEmision, check, cn)
        cn.Close()
        Return lista
    End Function

    Public Function LN_ListarVehiculos(ByVal identificador As Integer, ByVal filtro As String) As DataTable
        Return clsDatos.ListarVehiculos(identificador, filtro)
    End Function

    Public Function LN_ListarDatosGenerales(ByVal filtro As String, ByVal dt As DataTable, ByVal flag As String, ByVal condicion As String, _
                                             ByVal tienda As Integer, ByVal serie As Integer, ByVal documento As Integer, ByVal fechaEmision As Date, ByVal check As Boolean)
        Return clsDatos.DAO_ListarDatosGenerales(filtro, dt, flag, condicion, tienda, serie, documento, fechaEmision, check)
    End Function

    Public Function LN_UpdateProgramacion(ByVal idCronograma As String, ByVal id_anulacion As Integer, ByVal comentario As String)
        Return clsDatos.DAO_UpdateProgramacion(idCronograma, id_anulacion, comentario)
    End Function

    Public Function LN_InsertarGuias(ByVal peso As Decimal, ByVal fechaInicioDespacho As DateTime, ByVal capacidadAlcanzada As Decimal, ByVal cantidadDoc As Integer, ByVal idEstadoDespacho As Integer, _
                                     ByVal idDocumentoRelacion As String, ByVal entidades As Entidades.Vehiculo, ByVal flag As String, ByVal idCronogramaDespacho As Integer, _
                                     ByVal idDocumentoDeselecion As String, ByVal nroVuelta As Integer)
        Return clsDatos.DAO_InsertarGuias(peso, fechaInicioDespacho, capacidadAlcanzada, cantidadDoc, idEstadoDespacho, idDocumentoRelacion, entidades, _
                                          flag, idCronogramaDespacho, idDocumentoDeselecion, nroVuelta)
    End Function

    Public Function LN_ListarPersonasVehiculo()
        Return clsDatos.DAO_ListarPersonasVehiculo()
    End Function

    Public Function DAO_UPDATE_FEC_DESPACHOS(ByVal idDocumento As String, ByVal fecha As DateTime)
        Return clsDatos.DAO_UPDATE_FEC_DESPACHOS(idDocumento, fecha)
    End Function

    Public Function DAO_UPDATE_PESO_DESPACHOS(ByVal idDocumento As String, ByVal peso As Decimal)
        Return clsDatos.DAO_UPDATE_PESO_DESPACHOS(idDocumento, peso)
    End Function

    Public Function LN_DataSetTipados(ByVal dt As DataTable, ByVal flag As String, ByVal condicion As String, ByVal tienda As Integer, ByVal serie As Integer, _
                                      ByVal documento As Integer, ByVal fechaEmision As Date, ByVal check As Boolean)
        Return clsDatos.DAO_DataSetTipados(dt, flag, condicion, tienda, serie, documento, fechaEmision, check)
    End Function

    Public Function LN_Reporte(ByVal fecInicio As Date, ByVal fecFin As Date, ByVal turno As String, ByVal placa As String, idTienda As String)
        Return clsDatos.DAO_Reporte(fecInicio, fecFin, turno, placa, idTienda)
    End Function

    Public Function LN_Reporte_Distribucion(ByVal idDocumento As Integer)
        Return clsDatos.DAO_Reporte_Distribucion(idDocumento)
    End Function

    Public Function LN_Controladores(ByVal idDocumentos As String, ByVal flag As String, ByVal idusuario As String, ByVal flagFiltro As Boolean, ByVal fechaFiltro As Date, _
                                      ByVal Trasnsportista As Integer)
        Return clsDatos.DAO_Controladores(idDocumentos, flag, idusuario, flagFiltro, fechaFiltro, Trasnsportista)
    End Function

    Public Function LN_ListaPicking(ByVal paramidSector As Integer, ByVal flagTransporte As Boolean, ByVal fecFiltro As Date, _
                                    ByVal idTransportista As Integer, ByVal turno As String) As List(Of Entidades.be_picking)
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Return clsDatos.DAO_ListaPicking(paramidSector, flagTransporte, fecFiltro, idTransportista, cn, turno)
    End Function

    Public Function LN_Controladores_UPDATE(ByVal id_despacho_controlador As Integer, ByVal idAlmacen As Integer, ByVal idProducto As Integer, ByVal idSector As Integer, _
                                             ByVal cantidadDespachada As Decimal, ByVal idUnidadEntrada As Integer, ByVal idUnidadSalida As Integer, ByVal idTono As Integer)
        Return clsDatos.DAO_Controladores_UPDATE(id_despacho_controlador, idAlmacen, idProducto, idSector, cantidadDespachada, idUnidadEntrada, idUnidadSalida, idTono)
    End Function

    Public Sub LN_BuscarDocumentoGenearl(ByVal nroSerie As Integer, ByVal nroDocumento As Integer, idtienda As String)
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        clsDatos.DAO_BuscarDocumentoGeneral(cn, nroSerie, nroDocumento, idtienda)
        cn.Close()
    End Sub
End Class
