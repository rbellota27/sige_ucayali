﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

''''''''''''''''''''''MIRAYA ANAMARIA, EDGAR 02/02/2010 4:57 pm
Public Class Caja_PostView

    Private obj As New DAO.DAOCaja_PostView

    Public Function SelectAll() As List(Of Entidades.Caja_PostView)
        Return obj.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Caja_PostView)
        Return obj.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Caja_PostView)
        Return obj.SelectAllInactivo
    End Function

    Public Function InsertT(ByVal x As Entidades.Caja_PostView) As Boolean
        Return obj.InsertT(CType(x, Entidades.Caja_Post))
    End Function

    Public Function UpdateT(ByVal x As Entidades.Caja_PostView) As Boolean
        Return obj.UpdateT(CType(x, Entidades.Caja_Post))
    End Function


    Private P As New Entidades.Caja_PostView
    'P: Entidad que va a guardar los parametros que se van a utilizar en las funciones
    'Es una variable auxiliar

#Region "Filtro por llaves primarias"



    Private Function FiltrarEntidadxParametros(ByVal x As Entidades.Caja_PostView) As Boolean
        'x va a ser el objeto a filtrar

        If (P.IdPost = 0 Or x.IdPost = P.IdPost) _
        And (P.IdCaja = 0 Or x.IdCaja = P.IdCaja) _
        And (P.IdTienda = 0 Or x.IdTienda = P.IdTienda) _
        And (P.IdBanco = 0 Or x.IdBanco = P.IdBanco) _
        And (P.IdCuentaBancaria = 0 Or x.IdCuentaBancaria = P.IdCuentaBancaria) _
        And ((Not P.Estado.HasValue) Or x.Estado = P.Estado) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function FiltrarLista(ByVal filtro As Entidades.Caja_PostView, ByVal List As List(Of Entidades.Caja_PostView)) As List(Of Entidades.Caja_PostView)
        P = filtro
        Return List.FindAll(AddressOf FiltrarEntidadxParametros)

    End Function

    Public Function ValidarEntidad(ByVal filtro As Entidades.Caja_PostView, ByVal List As List(Of Entidades.Caja_PostView)) As Boolean
        Dim x As New Entidades.Caja_PostView
        x.IdPost = filtro.IdPost
        x.IdCaja = filtro.IdCaja

        Dim List2 As List(Of Entidades.Caja_PostView)
        List2 = FiltrarLista(x, List)
        If List2.Count > 0 Then 'Existen registros con esas llaves primarias 
            Return False
        Else
            Return True
        End If

    End Function
#End Region

#Region "Filtro de datos por Post y Tienda"
    Private Function FiltrarEntidadxPostxTienda(ByVal x As Entidades.Caja_PostView) As Boolean
        'x va a ser el objeto a filtrar
        'p es el filtro

        If (x.IdPost = P.IdPost) _
        And (x.IdTienda <> P.IdTienda) _
        And (x.Estado = P.Estado) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function ValidarListaxPostxTienda(ByVal filtro As Entidades.Caja_PostView, ByVal List As List(Of Entidades.Caja_PostView)) As List(Of Entidades.Caja_PostView)
        P = filtro
        Return List.FindAll(AddressOf FiltrarEntidadxPostxTienda)

    End Function

    Public Function ValidarEntidadxPostxTienda(ByVal filtro As Entidades.Caja_PostView, ByVal List As List(Of Entidades.Caja_PostView)) As Boolean
        'un post puede estar asociado a varias cajas, pero de una misma tienda
        'Se debe verificar que un registro caja post nuevo cumpla con esa regla

        Dim x As New Entidades.Caja_PostView
        x.IdPost = filtro.IdPost
        x.IdCaja = filtro.IdCaja
        x.IdTienda = filtro.IdTienda
        x.Estado = True

        Dim List2 As List(Of Entidades.Caja_PostView)
        List2 = ValidarListaxPostxTienda(x, List)
        If List2.Count > 0 Then 'Existen registros con ese post y otra tienda 
            Return False
        Else
            Return True
        End If

    End Function

#End Region

End Class
