﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'***************** MIERCOLES 17 FEB 2010 HORA 10_53 PM


'MIRAYA ANAMARIA, EDGAR 02/02/2010 8:19 pm
Public Class TarjetaView
    Private obj As New DAO.DAOTarjetaView

    Public Function SelectCboxTipoTarjetaCaja(ByVal IdTipoTarjeta As Integer, ByVal IdCaja As Integer) As List(Of Entidades.Tarjeta)

        Return obj.SelectCboxTipoTarjetaCaja(IdTipoTarjeta, IdCaja)

    End Function

    Public Function SelectCboxIdPost(ByVal IdPost As Integer) As List(Of Entidades.Tarjeta)

        Return obj.SelectCboxIdPost(IdPost)

    End Function

    Public Function SelectAll() As List(Of Entidades.TarjetaView)
        Return obj.SelectAll
    End Function

    Public Function SelectAllActivo() As List(Of Entidades.TarjetaView)
        Return obj.SelectAllActivo
    End Function

    Public Function SelectAllInactivo() As List(Of Entidades.TarjetaView)
        Return obj.SelectAllInactivo
    End Function
    Public Function SelectxIdTipoTarjeta(ByVal Id As Integer) As List(Of Entidades.TarjetaView)
        Return obj.SelectxIdTipoTarjeta(Id)
    End Function
    Public Function InsertaTarjeta(ByVal x As Entidades.TarjetaView) As Boolean
        Return obj.InsertaTarjeta(CType(x, Entidades.Tarjeta))
    End Function
    Public Function ActualizaTarjeta(ByVal x As Entidades.TarjetaView) As Boolean
        Return obj.ActualizaTarjeta(CType(x, Entidades.Tarjeta))
    End Function

    Private P As New Entidades.TarjetaView
    'P: Entidad que va a guardar los parametros que se van a utilizar en las funciones
    'Es una variable auxiliar

    Private Function FiltrarEntidadxParametros(ByVal x As Entidades.TarjetaView) As Boolean
        'x va a ser el objeto a filtrar

        If (x.Id = P.Id Or P.Id = 0) _
        And (x.Nombre.ToLower.Trim.StartsWith(P.Nombre.ToLower.Trim) Or P.Nombre = "") _
        And (x.IdTipoTarjeta = P.IdTipoTarjeta Or P.IdTipoTarjeta = 0) _
        And (x.Estado = P.Estado Or (Not P.Estado.HasValue)) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function FiltrarLista(ByVal filtro As Entidades.TarjetaView, ByVal List As List(Of Entidades.TarjetaView)) As List(Of Entidades.TarjetaView)
        P = filtro
        Return List.FindAll(AddressOf FiltrarEntidadxParametros)

    End Function

    Public Function ValidarEntidadxNombre(ByVal filtro As Entidades.TarjetaView, ByVal List As List(Of Entidades.TarjetaView)) As Boolean
        'X es el valor a verificar, 

        Dim x As New Entidades.TarjetaView
        x.Id = filtro.Id
        x.Nombre = filtro.Nombre
        x.IdTipoTarjeta = filtro.IdTipoTarjeta

        Dim List2 As List(Of Entidades.TarjetaView)
        List2 = ValidarLista(x, List)

        If List2.Count > 0 Then 'Existen registros  
            Return False
        Else
            Return True
        End If


    End Function
    Private Function ValidarEntidadxParametros(ByVal x As Entidades.TarjetaView) As Boolean
        'Se utiliza a la hora de insertar o eliminar
        'x va a ser el objeto a filtrar
        'En este caso no se cuenta el registro analizado
        If (x.Id <> P.Id) _
        And (x.Nombre.ToLower.Trim = P.Nombre.ToLower.Trim Or P.Nombre = "") _
        And (x.IdTipoTarjeta = P.IdTipoTarjeta) Then
            'And (x.Estado = P.Estado Or (Not P.Estado.HasValue))
            Return True
        Else
            Return False
        End If
    End Function
    Public Function ValidarLista(ByVal filtro As Entidades.TarjetaView, ByVal List As List(Of Entidades.TarjetaView)) As List(Of Entidades.TarjetaView)
        P = filtro
        Return List.FindAll(AddressOf ValidarEntidadxParametros)

    End Function


End Class
