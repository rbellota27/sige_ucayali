﻿Public Class Proveedor
    Private objDAOProveedor As New DAO.DAOProveedor

    Public Function ListarProveedorOrdenCompra(ByVal idproveedor As Integer) As Entidades.PersonaView
        Return objDAOProveedor.ListarProveedorOrdenCompra(idproveedor)
    End Function

    Public Function SelectCbo() As List(Of Entidades.PersonaView)
        Return objDAOProveedor.SelectCbo
    End Function
    Public Function SelectCboImportado() As List(Of Entidades.PersonaView)
        Return objDAOProveedor.SelectCboImportado
    End Function
    Public Function SelectGrillaProveedor() As List(Of Entidades.PersonaView)
        Return objDAOProveedor.SelectGrillaProveedor
    End Function
    Public Function SelectGrillaProveedorxNombre(ByVal nombre As String) As List(Of Entidades.PersonaView)
        Return objDAOProveedor.SelectGrillaProveedorxNombre(nombre)
    End Function
    Public Function SelectGrillaProveedorxRazonSocial(ByVal nombre As String) As List(Of Entidades.PersonaView)
        Return objDAOProveedor.SelectGrillaProveedorxRazonSocial(nombre)
    End Function
    Public Function SelectGrillaProveedorxDNI(ByVal numero As String) As List(Of Entidades.PersonaView)
        Return objDAOProveedor.SelectGrillaProveedorxDNI(numero)
    End Function
    Public Function SelectGrillaProveedorxRUC(ByVal numero As String) As List(Of Entidades.PersonaView)
        Return objDAOProveedor.SelectGrillaProveedorxRUC(numero)
    End Function
    Public Function SelectGrillaProveedorxIdPersona(ByVal codigo As String) As List(Of Entidades.PersonaView)
        Return objDAOProveedor.SelectGrillaProveedorxIdPersona(codigo)
    End Function
    Public Function SelectGrillaProveedorxEstado(ByVal estado As String) As List(Of Entidades.PersonaView)
        Return objDAOProveedor.SelectGrillaProveedorxEstado(estado)
    End Function
End Class
