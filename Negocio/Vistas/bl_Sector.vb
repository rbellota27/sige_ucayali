﻿Imports System.Data.SqlClient
Public Class bl_Sector
    Dim objConexion As New DAO.Conexion
    Dim objSector As DAO.DAO_Sector_Objetos

    Public Function bl_Sector() As List(Of Entidades.be_Sector)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        cn.Open()
        objSector = New DAO.DAO_Sector_Objetos
        Return objSector.DAO_ListarSector(cn)
    End Function
End Class
