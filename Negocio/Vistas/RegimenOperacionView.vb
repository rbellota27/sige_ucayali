﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class RegimenOperacionView
    Dim obj As New DAO.DAORegimenOperacionView
    Public Function SelectAll() As List(Of Entidades.RegimenOperacionView)
        Return obj.SelectAll()
    End Function

    Private P As New Entidades.RegimenOperacionView
    'P: Entidad que va a guardar los parametros que se van a utilizar en las funciones
    'Es una variable auxiliar

    Private Function FiltrarEntidadxParametros(ByVal x As Entidades.RegimenOperacionView) As Boolean
        'x va a ser el objeto a filtrar

        If (x.IdRegimen = P.IdRegimen Or P.IdRegimen = 0) _
        And (x.IdTipoOperacion = P.IdTipoOperacion Or P.IdTipoOperacion = 0) _
        And (x.CuentaContable.StartsWith(P.CuentaContable) Or P.CuentaContable = "") _
        And (x.Estado = P.Estado Or P.Estado = "2") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function FiltrarLista(ByVal filtro As Entidades.RegimenOperacionView, ByVal List As List(Of Entidades.RegimenOperacionView)) As List(Of Entidades.RegimenOperacionView)
        P = filtro
        Return List.FindAll(AddressOf FiltrarEntidadxParametros)

    End Function

    Public Function ValidarEntidad(ByVal filtro As Entidades.RegimenOperacionView, ByVal List As List(Of Entidades.RegimenOperacionView)) As Boolean
        'X es el valor a verificar, se analizan sus llaves primarias, en este caso IdRegimen y IdTipoOperacion
        'Aqui se pone las otras variables en su forma general, para encontrar todos los resultados 
        Dim x As New Entidades.RegimenOperacionView
        x.IdRegimen = filtro.IdRegimen
        x.IdTipoOperacion = filtro.IdTipoOperacion
        x.CuentaContable = ""
        x.Estado = "2" 'Todos los estados

        Dim List2 As List(Of Entidades.RegimenOperacionView)
        List2 = FiltrarLista(x, List)
        If List2.Count > 0 Then 'Existen registros con esas llaves primarias 
            Return False
        Else
            Return True
        End If

    End Function


End Class
