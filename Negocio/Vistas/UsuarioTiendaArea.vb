﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class UsuarioTiendaArea
    Private obj As New DAO.DAOUsuarioTiendaArea
    Private objConexion As New DAO.Conexion
    Public Function SelectActivoDistinctTiendaxIdPersonaxIdPerfil(ByVal IdPersona As Integer, ByVal IdPerfil As Integer) As List(Of Entidades.UsuarioTiendaArea)
        Return obj.SelectActivoDistinctTiendaxIdPersonaxIdPerfil(IdPersona, IdPerfil)
    End Function
    Public Function RegistrarUsuarioTiendaArea(ByVal lista As List(Of Entidades.UsuarioTiendaArea), ByVal idPersona As Integer) As Boolean
        Dim hecho As Boolean = False
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            '************* elimino todos los registros segun el idpersona
            obj.DeletexIdPersona(cn, tr, idPersona)

            '******************* inserto los registros
            Dim objCajaUsuario As New DAO.DAOCajaUsuario

            For i As Integer = 0 To lista.Count - 1
                obj.Insert(cn, tr, lista(i))
            Next
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
    Public Function SelectAllxIdPersona(ByVal idpersona As Integer) As List(Of Entidades.UsuarioTiendaArea)
        Return obj.SelectAllxIdPersona(idpersona)
    End Function
End Class
