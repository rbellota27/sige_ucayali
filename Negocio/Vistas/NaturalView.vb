﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class NaturalView
    Private obj As New DAO.DAONaturalView
    Public Function _NaturalViewSelectAll() As List(Of Entidades.NaturalView)
        Return obj._NaturalViewSelectAll()
    End Function
    Public Function _NaturalViewSelectAllxNombre(ByVal nombre As String) As List(Of Entidades.NaturalView)
        Return obj._NaturalViewSelectAllxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As Entidades.NaturalView
        Return obj.SelectxId(id)
    End Function
    Public Function SelectxDNI(ByVal dni As String) As Entidades.NaturalView
        Return obj.SelectxDNI(dni)
    End Function
End Class
