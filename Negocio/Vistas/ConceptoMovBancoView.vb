﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class ConceptoMovBancoView
    Private obj As New DAO.DAOConceptoMovBancoView

    Public Function SelectAll() As List(Of Entidades.ConceptoMovBancoView)
        Return obj.SelectAll
    End Function

    Public Function SelectAllActivo() As List(Of Entidades.ConceptoMovBancoView)
        Return obj.SelectAllActivo
    End Function

    Public Function SelectAllInactivo() As List(Of Entidades.ConceptoMovBancoView)
        Return obj.SelectAllInactivo
    End Function
    Public Function SelectxIdTipoConceptoBanco(ByVal Id As Integer) As List(Of Entidades.ConceptoMovBancoView)
        Return obj.SelectxIdTipoConceptoBanco(Id)
    End Function
    Public Function InsertT(ByVal x As Entidades.ConceptoMovBancoView) As Boolean
        Return obj.InsertT(CType(x, Entidades.ConceptoMovBanco))
    End Function

    Public Function UpdateT(ByVal x As Entidades.ConceptoMovBancoView) As Boolean
        Return obj.UpdateT(CType(x, Entidades.ConceptoMovBanco))
    End Function

    Private P As New Entidades.ConceptoMovBancoView
    'P: Entidad que va a guardar los parametros que se van a utilizar en las funciones
    'Es una variable auxiliar

    Private Function FiltrarEntidadxParametros(ByVal x As Entidades.ConceptoMovBancoView) As Boolean
        'x va a ser el objeto a filtrar

        If (x.Id = P.Id Or P.Id = 0) _
        And (x.Descripcion.ToLower.Trim.StartsWith(P.Descripcion.ToLower.Trim) Or P.Descripcion = "") _
        And (x.DescripcionBreve.ToLower.Trim.StartsWith(P.DescripcionBreve.ToLower.Trim) Or P.DescripcionBreve = "") _
        And (x.IdTipoConceptoBanco = P.IdTipoConceptoBanco Or P.IdTipoConceptoBanco = 0) _
        And (x.EstadoAutoAprobacion = P.EstadoAutoAprobacion Or (Not P.EstadoAutoAprobacion.HasValue)) _
        And (x.Estado = P.Estado Or (Not P.Estado.HasValue)) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function FiltrarLista(ByVal filtro As Entidades.ConceptoMovBancoView, ByVal List As List(Of Entidades.ConceptoMovBancoView)) As List(Of Entidades.ConceptoMovBancoView)
        P = filtro
        Return List.FindAll(AddressOf FiltrarEntidadxParametros)

    End Function

    Public Function ValidarEntidadxNombre(ByVal filtro As Entidades.ConceptoMovBancoView, ByVal List As List(Of Entidades.ConceptoMovBancoView)) As Boolean
        'X es el valor a verificar, 

        Dim x As New Entidades.ConceptoMovBancoView
        x.Id = filtro.Id
        x.Nombre = filtro.Nombre

        Dim List2 As List(Of Entidades.ConceptoMovBancoView)
        List2 = ValidarLista(x, List)

        If List2.Count > 0 Then 'Existen registros  
            Return False
        Else
            Return True
        End If


    End Function
    Private Function ValidarEntidadxParametros(ByVal x As Entidades.ConceptoMovBancoView) As Boolean
        'Se utiliza a la hora de insertar o eliminar
        'x va a ser el objeto a filtrar
        'En este caso no se cuenta el registro analizado
        If (x.Id <> P.Id) _
        And (x.Nombre.ToLower.Trim = P.Nombre.ToLower.Trim Or P.Nombre = "") Then
            'And (x.Estado = P.Estado Or (Not P.Estado.HasValue))
            Return True
        Else
            Return False
        End If
    End Function
    Public Function ValidarLista(ByVal filtro As Entidades.ConceptoMovBancoView, ByVal List As List(Of Entidades.ConceptoMovBancoView)) As List(Of Entidades.ConceptoMovBancoView)
        P = filtro
        Return List.FindAll(AddressOf ValidarEntidadxParametros)

    End Function


End Class
