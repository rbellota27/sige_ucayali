﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class ProductoUM
    Private objDAOProdUM As New DAO.DAOProductoUM

    Public Function SelectxIdProductoxIdUM(ByVal idproducto As Integer, ByVal idunidadmedida As Integer) As Entidades.ProductoUM
        Return objDAOProdUM.SelectxIdProductoxIdUM(idproducto, idunidadmedida)
    End Function

    Public Function InsertaProductoUM(ByVal productoum As Entidades.ProductoUM) As Boolean
        Return objDAOProdUM.InsertaProductoUM(productoum)
    End Function
    Public Function ActualizaProductoUM(ByVal productoum As Entidades.ProductoUM) As Boolean
        Return objDAOProdUM.ActualizaProductoUM(productoum)
    End Function
    Public Function SelectxIdProducto(ByVal idProducto As Integer) As List(Of Entidades.ProductoUM)
        Return objDAOProdUM.SelectxIdProducto(idProducto)
    End Function
    Public Function SelectIdUMPrincipalxIdProducto(ByVal IdProducto As Integer) As Integer
        Return objDAOProdUM.SelectIdUMPrincipalxIdProducto(IdProducto)
    End Function
    Public Function SelectAll() As List(Of Entidades.ProductoUM)
        Return objDAOProdUM.SelectAll()
    End Function
    Public Function SelectEquivalenciaxIdProductoxIdUnidadMedida(ByVal idProducto As Integer, ByVal IdUnidadMedida As Integer) As Decimal
        Return objDAOProdUM.SelectEquivalenciaxIdProductoxIdUnidadMedida(idProducto, IdUnidadMedida)
    End Function
    Public Function ProductoUMUpdatePorcentRetazo(ByVal listaProductoUM As List(Of Entidades.ProductoUM)) As Boolean
        Dim objConexion As New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            For i As Integer = 0 To listaProductoUM.Count - 1
                With listaProductoUM.Item(i)
                    If Not objDAOProdUM.ProductoUMUpdatePorcentRetazo(.IdProducto, .IdUnidadMedida, .PorcentRetazo, cn, tr) Then
                        Throw New Exception
                    End If
                End With

                ''***************** inserto y/o actualizo los pv de retazos
                'Dim objDaoProductoTipoPV As New DAO.DAOProductoTipoPV
                'Dim objDaoUtil As New DAO.DAOUtil
                'Dim lista As List(Of Entidades.ProductoTipoPV) = objDaoProductoTipoPV.SelectUmPrincipalxIdProducto(listaProductoUM(i).IdProducto, cn, tr)
                'For x As Integer = 0 To lista.Count - 1
                '    '******************** creo el obj a registrar
                '    Dim objProdTipoPV As New Entidades.ProductoTipoPV
                '    With objProdTipoPV
                '        .IdProducto = lista(x).IdProducto
                '        .IdTipoPv = lista(x).IdTipoPv
                '        .PUtilVariable = 0

                '        .Valor = (lista(x).Valor * (1 + listaProductoUM(i).PorcentRetazo / 100)) * lista(x).Equivalencia

                '        .Utilidad = .Valor - (lista(x).PrecioCompraxUMPrincipal * lista(x).Equivalencia)

                '        If lista(x).PrecioCompraxUMPrincipal = 0 Then
                '            .PUtilFijo = 100
                '        Else
                '            .PUtilFijo = (.Utilidad / (lista(x).PrecioCompraxUMPrincipal * lista(x).Equivalencia)) * 100
                '        End If

                '        .Estado = lista(x).Estado
                '        .IdUsuario = Nothing
                '        .IdTienda = lista(x).IdTienda
                '        .IdUnidadMedida = listaProductoUM(i).IdUnidadMedida
                '        .IdMoneda = lista(x).IdMoneda
                '    End With

                '    '********************** verifico la existencia
                '    If objDaoUtil.ValidarExistenciaxTablax4Campos("ProductoTipoPV", "IdTienda", CStr(lista(x).IdTienda), "IdProducto", CStr(lista(x).IdProducto), "IdUnidadMedida", CStr(listaProductoUM(i).IdUnidadMedida), "IdTipoPV", CStr(lista(x).IdTipoPv)) > 0 Then
                '        '************* actualizo
                '        objDaoProductoTipoPV.ActualizaProductoTipoPV(objProdTipoPV, cn, tr)
                '    Else
                '        '*************** inserto
                '        objDaoProductoTipoPV.InsertaProductoTipoPV(cn, tr, objProdTipoPV)
                '    End If

                'Next
            Next
            tr.Commit()
            Return True
        Catch ex As Exception
            tr.Rollback()
            Return False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

End Class
