﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Ubicacion
    Private objDAOUb As New DAO.DAOUbicacion
    Public Function InsertaUbicacion(ByVal ubicacion As Entidades.Ubicacion) As Boolean
        Return objDAOUb.InsertaUbicacion(ubicacion)
    End Function
    Public Function ActualizaUbicacion(ByVal ubicacion As Entidades.Ubicacion) As Boolean
        Return objDAOUb.ActualizaUbicacion(ubicacion)
    End Function
    Public Function SelectxIdProducto(ByVal IdProducto As Integer) As List(Of Entidades.Ubicacion)
        Return objDAOUb.SelectxIdProducto(IdProducto)
    End Function
    Public Function SelectxIdUbicacion(ByVal IdUbicacion As Integer) As List(Of Entidades.Ubicacion)
        Return objDAOUb.SelectxIdUbicacion(IdUbicacion)
    End Function
    Public Function SelectAll() As List(Of Entidades.Ubicacion)
        Return objDAOUb.SelectAll
    End Function
End Class
