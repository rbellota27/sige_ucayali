﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************** LUNES 18 ENERO 2010 HORA 11_51 AM

Public Class Tarjeta
    Private objDaoTarjeta As New DAO.DAOTarjeta
    Public Function SelectAllActivo() As List(Of Entidades.Tarjeta)
        Return objDaoTarjeta.SelectAllActivo
    End Function
    Public Function SelectAll() As List(Of Entidades.Tarjeta)
        Return objDaoTarjeta.SelectAll
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Tarjeta)
        Return objDaoTarjeta.SelectAllInactivo
    End Function
    Public Function InsertaTarjeta(ByVal Estilo As Entidades.Tarjeta) As Boolean
        Return objDaoTarjeta.InsertaTarjeta(Estilo)
    End Function
    Public Function ActualizaTarjeta(ByVal estilo As Entidades.Tarjeta) As Boolean
        Return objDaoTarjeta.ActualizaTarjeta(estilo)
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Tarjeta)
        Return objDaoTarjeta.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Tarjeta)
        Return objDaoTarjeta.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Tarjeta)
        Return objDaoTarjeta.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Tarjeta)
        Return objDaoTarjeta.SelectxId(id)
    End Function
End Class
