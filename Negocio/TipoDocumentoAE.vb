﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Chang Carnero, Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 11-Noviembre-2009
'Hora    : 03:00 pm
'*************************************************
Public Class TipoDocumentoAE
    Private obj As New DAO.DAOTipoDocumentoAE

    Public Function SelectxIds(ByVal IdEmpresa As Integer, ByVal IdTipoDocumento As Integer, ByVal IdArea As Integer) As Entidades.TipoDocumentoAE
        Return obj.SelectxIds(IdEmpresa, IdTipoDocumento, IdArea)
    End Function
    Public Function SelectValoresxParams(ByVal IdEmpresa As Integer, ByVal IdTipoDocumento As Integer, ByVal IdMonedaDestino As Integer) As Entidades.TipoDocumentoAE
        Return obj.SelectValoresxParams(IdEmpresa, IdTipoDocumento, IdMonedaDestino)
    End Function
    Public Function SelectAllTipoDocumentoAE() As List(Of Entidades.TipoDocumentoAE)
        Return obj.SelectAllTipoDocumentoAE
    End Function
    Public Function InsertaTipoDocumentoAE(ByVal TipoDocumentoAE As Entidades.TipoDocumentoAE) As String
        Return obj.InsertaTipoDocumentoAE(TipoDocumentoAE)
    End Function
    Public Function ActualizaTipoDocumentoAE(ByVal TipoDocumentoAE As Entidades.TipoDocumentoAE) As Integer
        Return obj.ActualizaTipoDocumentoAE(TipoDocumentoAE)
    End Function
    Public Function EliminaxEmpresaxAreaxTipoDocumento(ByVal IdEmpresa As Integer, ByVal IdArea As Integer _
                                          , ByVal IdTipoDocumento As Integer) As Integer
        Return obj.EliminarxEmpresaxAreaxTipoDocumento(IdEmpresa, IdArea, IdTipoDocumento)
    End Function
    Public Function BuscarTipoDocumentoxIdArea(ByVal IdArea As Integer, ByVal IdEmpresa As Integer) As List(Of Entidades.TipoDocumentoAE)
        Return obj.TipoDocumentoAEBuscarxIdArea(IdArea, IdEmpresa)
    End Function
End Class
