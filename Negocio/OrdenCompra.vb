﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class OrdenCompra
    Dim Obj As New DAO.DAOOrdenCompra
    Dim objConexion As New DAO.Conexion
    Dim objDoc As New DAO.DAODocumento

    Public Function Validar_CosteoImportacion(ByVal IdDocumento As Integer) As Nullable(Of Integer)
        Return Obj.Validar_CosteoImportacion(IdDocumento)
    End Function

    Public Function OrdenCompra_Datos_Contacto(ByVal IdPersona As Integer) As List(Of Entidades.PersonaView)
        Return Obj.OrdenCompra_Datos_Contacto(IdPersona)
    End Function

    Public Function CuentaProveedor_SelectActivo(ByVal IdProveedor As Integer, ByVal IdPropietario As Integer) As List(Of Entidades.CuentaProveedor)
        Return Obj.CuentaProveedor_SelectActivo(IdProveedor, IdPropietario)
    End Function

    Public Function ComprasxProveedor(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idpersona As Integer, ByVal fechaInicio As String, ByVal fechafin As String, ByVal tipo As Integer, ByVal linea As Integer, ByVal sublinea As Integer) As DataSet
        Return Obj.ComprasxProveedor(idempresa, idtienda, idpersona, fechaInicio, fechafin, tipo, linea, SubLinea)
    End Function
    Public Function ventasxVendedor(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idpersona As Integer, ByVal fechaInicio As String, ByVal fechafin As String, ByVal tipo As Integer, ByVal linea As Integer, ByVal sublinea As Integer) As DataSet
        Return Obj.ventasxVendedor(idempresa, idtienda, idpersona, fechaInicio, fechafin, tipo, Linea, SubLinea)
    End Function

    Public Function RankingProvedores(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idCondicionpago As Integer, ByVal idmoneda As Integer, ByVal idTipopersona As Integer, ByVal fechaInicio As String, ByVal fechafin As String, ByVal nprimeros As Integer, ByVal idlinea As Integer, ByVal idsublinea As Integer) As DataSet
        Return Obj.RankingProvedores(idempresa, idtienda, idCondicionpago, idmoneda, idTipopersona, fechaInicio, fechafin, nprimeros, idlinea, idsublinea)
    End Function

    Public Function SelectOrdenCompra(ByVal idserie As Integer, ByVal codigo As Integer, Optional ByVal IdDocumento As Integer = 0) As Entidades.Documento
        Return Obj.SelectOrdenCompra(idserie, codigo, IdDocumento)
    End Function

    Public Function listarOrdenesCompraxFiltro(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdProveedor As Integer, ByVal FechaIni As String, ByVal FechaFin As String, ByVal IdEstadoCan As Integer, ByVal IdEstadoEnt As Integer, ByVal PageIndex As Integer, ByVal PageSize As Integer) As List(Of Entidades.DocumentoView)
        Return Obj.listarOrdenesCompraxFiltro(IdEmpresa, IdTienda, IdProveedor, FechaIni, FechaFin, IdEstadoCan, IdEstadoEnt, PageIndex, PageSize)
    End Function

    Public Function getDataSetOrdenCompra(ByVal iddocumento As Integer) As DataSet
        Return Obj.getDataSetOrdenCompra(iddocumento)
    End Function

    Public Function getDataSetComparativo(ByVal year As Integer, _
                            ByVal columna As String, ByVal idproveedor As Integer, _
                            ByVal idmoneda As Integer, ByVal idpropietario As Integer) As DataSet
        Return Obj.getDataSetComparativo(year, columna, idproveedor, idmoneda, idpropietario)
    End Function

    Public Function ListarPrecioXidMoneda(ByVal idmoneda As Integer, ByVal idproducto As Integer) As Decimal
        Return Obj.ListarPrecioXidMoneda(idmoneda, idproducto)
    End Function
    Public Function listarContactoxIdPersona(ByVal idPersona As Integer, Optional ByVal addIdusuario As Integer = 0) As List(Of Entidades.PersonaView)
        Return Obj.listarContactoxIdPersona(idPersona, addIdusuario)
    End Function

    Public Function listarPersonaJuridicaxRolxEmpresa(ByVal NomPersona As String, ByVal Ruc As String, ByVal Tipo As Integer, ByVal IdRol As Integer, ByVal Condicion As Integer, ByVal Estado As Integer, ByVal PageIndex As Integer, ByVal PageSize As Integer, Optional ByVal Dni As String = "") As List(Of Entidades.PersonaView)
        Return Obj.listarPersonaJuridicaxRolxEmpresa(NomPersona, Ruc, Tipo, IdRol, Condicion, Estado, PageIndex, PageSize, Dni)
    End Function

    Function listarProductosProveedorOrdenCompra(ByVal idempresa As Integer, ByVal idproveedor As Integer, ByVal IdAgenteProveedor As Integer, _
          ByVal descripcion As String, ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal codsublinea As Integer, _
          ByVal idmoneda As Integer, ByVal pageIndex As Integer, ByVal pageSize As Integer, ByVal verTodo As Boolean, _
          ByVal IdArea As Integer, ByVal tabla As DataTable, ByVal codigoProducto As String, ByVal codigoProveedor As String, ByVal incluyeigv As Boolean) As List(Of Entidades.Catalogo)
        Return Obj.listarProductosProveedorOrdenCompra(idempresa, idproveedor, IdAgenteProveedor, descripcion, idlinea, idsublinea, codsublinea, idmoneda, pageIndex, pageSize, verTodo, IdArea, tabla, codigoProducto, codigoProveedor, incluyeigv)
    End Function
    Public Function listarProductosProveedorOrdenCompraCodBarraFab(ByVal idempresa As Integer, _
           ByVal idproveedor As Integer, ByVal IdAgenteProveedor As Integer _
           , ByVal idmoneda As Integer, _
           ByVal IdArea As Integer, _
           ByVal IncluyeIgv As Boolean, ByVal CodbarraFab As String) As List(Of Entidades.Catalogo)
        Return Obj.listarProductosProveedorOrdenCompraCodBarraFab(idempresa, idproveedor, IdAgenteProveedor, idmoneda, IdArea, IncluyeIgv, CodbarraFab)
    End Function

    Function listarDireccionAlmacen() As List(Of Entidades.Almacen)
        Return Obj.listarDireccionAlmacen()
    End Function

    Function InsertarOrdenCompra(ByVal objDocumento As Entidades.Documento, _
                                 ByVal listaDetalle As List(Of Entidades.DetalleDocumento), _
                                 ByVal obs As Entidades.Observacion, _
                                 ByVal objMonto As Entidades.MontoRegimen, _
                                 ByVal listaCondicionComercial As List(Of Entidades.CondicionComercial)) _
                                 As String
        Return Obj.InsertarOrdenCompra(objDocumento, listaDetalle, obs, objMonto, listaCondicionComercial)
    End Function

    Function listarCondicionOrdenCompra(ByVal idDocumento As Integer) As List(Of Entidades.CondicionComercial)
        Dim objCC As New Negocio.CondicionPago
        Return objCC.SelectAllCondicionesComerciales(idDocumento)
    End Function

    Function listarProductoPedidoSucursal(ByVal idtienda As Integer, ByVal idlinea As Integer, ByVal idsublinea As Integer, _
                                          ByVal descripcion As String, ByVal codsublinea As Integer, _
                                          ByVal pageIndex As Integer, ByVal pageSize As Integer) As List(Of Entidades.Catalogo)
        Return Obj.listarProductoPedidoSucursal(idtienda, idlinea, idsublinea, descripcion, codsublinea, pageIndex, pageSize)
    End Function

    Function listarPreciosCompra(ByVal idproducto As Integer) As List(Of Entidades.DetalleDocumento)
        Return Obj.listarPreciosCompra(idproducto)
    End Function

    Function listarObservacionDocumento(ByVal iddocumento As Integer) As Entidades.Observacion
        Return (New DAO.DAOObservacion).listarObservacionDocumento(iddocumento)
    End Function

    Public Function DetalleDocumentoViewSelectxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumentoView)
        Return Obj.DetalleDocumentoViewSelectxIdDocumento(IdDocumento)
    End Function

    Public Function updateDocumento(ByVal objDocumento As Entidades.Documento, ByVal ListaDetalle As List(Of Entidades.DetalleDocumento), _
                                    ByVal objObs As Entidades.Observacion, ByVal objMonto As Entidades.MontoRegimen, ByVal listaCondicionComercial As List(Of Entidades.CondicionComercial)) As Boolean
        Return Obj.UpdateDocumento(objDocumento, ListaDetalle, objObs, objMonto, listaCondicionComercial)
    End Function

    Public Sub AnularOrdenCompra(ByVal iddocumento As Integer)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction

        cn.Open()
        tr = cn.BeginTransaction(IsolationLevel.Serializable)
        Try
            Using cn

                Obj.Anular(iddocumento, False, False, False, False, True, True, True, cn, tr)

                tr.Commit()
            End Using
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try


    End Sub
    Public Function getReporteVentasStock(ByVal idEmpresa As Integer, ByVal idtienda As Integer, ByVal idlinea As Integer, ByVal idSublinea As Integer, ByVal fechainicio As String, ByVal fechafin As String, ByVal yeari As Integer, ByVal semanai As Integer, ByVal yearf As Integer, ByVal semanaf As Integer, ByVal filtrarsemana As Integer, ByVal ltProd As List(Of Entidades.ProductoView), ByVal ltTAlmacen As List(Of Entidades.TipoAlmacen), ByVal stockxtienda As Short) As DataSet
        Return Obj.getReporteVentasStock(idEmpresa, idtienda, idlinea, idSublinea, fechainicio, fechafin, yeari, semanai, yearf, semanaf, filtrarsemana, ltProd, ltTAlmacen, stockxtienda)
    End Function

End Class
