﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Impuesto
    Dim obj As New DAO.DAOImpuesto

    Public Function impuesto_select(Optional ByVal IdImpuesto As Integer = Nothing, _
                                    Optional ByVal IdEstado As String = Nothing, Optional ByVal Descripcion As String = Nothing) As List(Of Entidades.Impuesto)
        Return obj.impuesto_select(IdImpuesto, IdEstado, Descripcion)
    End Function

    Public Function SelectTasaIGV() As Decimal
        Return obj.SelectTasaIGV
    End Function

    Public Function InsertaImpuesto(ByVal impuesto As Entidades.Impuesto) As Boolean
        Return obj.InsertaImpuesto(impuesto)
    End Function

    Public Function ActualizaImpuesto(ByVal impuesto As Entidades.Impuesto) As Boolean
        Return obj.ActualizaImpuesto(impuesto)
    End Function

End Class
