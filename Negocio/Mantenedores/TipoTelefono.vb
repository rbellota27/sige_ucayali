﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class TipoTelefono
    Dim obj As New DAO.DAOTipoTelefono
    Public Function InsertaTipoTelefono(ByVal tipotelefono As Entidades.TipoTelefono) As Boolean
        Return obj.InsertaTipoTelefono(tipotelefono)
    End Function
    Public Function ActualizaTipoTelefono(ByVal tipotelefono As Entidades.TipoTelefono) As Boolean
        Return obj.ActualizaTipoTelefono(tipotelefono)
    End Function
    Public Function SelectCbo() As List(Of Entidades.TipoTelefono)
        Return obj.SelectCbo
    End Function
    Public Function SelectAll() As List(Of Entidades.TipoTelefono)
        Return obj.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.TipoTelefono)
        Return obj.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.TipoTelefono)
        Return obj.SelectAllInactivo
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.TipoTelefono)
        Return obj.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoTelefono)
        Return obj.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoTelefono)
        Return obj.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.TipoTelefono)
        Return obj.SelectxId(id)
    End Function
End Class
