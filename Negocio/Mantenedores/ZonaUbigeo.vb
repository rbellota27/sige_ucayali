﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'********************  JUEVES 11 FEB 2010 HORA 10_00 AM


Public Class ZonaUbigeo
    Private objDaoZonaUbigeo As New DAO.DAOZonaUbigeo
    Public Function SelectxEstadoxNombre(ByVal estado As String) As List(Of Entidades.ZonaUbigeo)
        Return objDaoZonaUbigeo.SelectxEstadoxNombre(estado)
    End Function
    Public Function InsertaZonaubigeo(ByVal zonaubigeo As Entidades.ZonaUbigeo) As Boolean
        Return objDaoZonaUbigeo.InsertaZonaUbigeo(zonaubigeo)
    End Function
    Public Function EliminaZonaubigeo(ByVal zonaubigeo As Entidades.ZonaUbigeo) As Boolean
        Return objDaoZonaUbigeo.EliminaZonaUbigeo(zonaubigeo)
    End Function
    Public Function ActualizaZonaUbigeo(ByVal zonaubigeo As Entidades.ZonaUbigeo) As Boolean
        Return objDaoZonaUbigeo.ActualizaZonaUbigeo(zonaubigeo)
    End Function
    Public Function SelectxIdZona(ByVal IdZona As Integer) As Entidades.ZonaUbigeo
        Return objDaoZonaUbigeo.SelectxIdZona(IdZona)
    End Function
    Public Function SelectCbo() As List(Of Entidades.ZonaUbigeo)
        Return objDaoZonaUbigeo.SelectCbo()
    End Function
End Class
