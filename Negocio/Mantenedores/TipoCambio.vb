﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class TipoCambio

    Dim obj As New DAO.DAOTipoCambio
    Public Function SelectVigentexIdMoneda(ByVal IdMoneda As Integer) As Entidades.TipoCambio
        Return obj.SelectVigentexIdMoneda(IdMoneda)
    End Function
    Public Function SelectOficial(ByVal IdMoneda As Integer) As Entidades.TipoCambio
        Try
            Return obj.SelectOficial(IdMoneda)
        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Function SelectComercial(ByVal IdMoneda As Integer) As Entidades.TipoCambio
        Try
            Return obj.SelectComercial(IdMoneda)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectTipoCambioActualxIdMoneda(ByVal IdMoneda As Integer) As Entidades.TipoCambio
        Return obj.SelectTipoCambioActualxIdMoneda(IdMoneda)
    End Function
    Public Function InsertaTipoCambio(ByVal tipocambio As Entidades.TipoCambio) As Boolean
        Return obj.InsertaTipoCambio(tipocambio)
    End Function
    Public Function ActualizaTipoCambio(ByVal tipocambio As Entidades.TipoCambio) As Boolean
        Return obj.ActualizaTipoCambio(tipocambio)
    End Function
    Public Function SelectxIdMoneda(ByVal IdMoneda As Integer) As List(Of Entidades.TipoCambio)
        Return obj.SelectxIdMoneda(IdMoneda)
    End Function

    Public Function SelectTipoCambioBuscarFecha(ByVal IdMoneda As Integer, ByVal Fecha As String) As List(Of Entidades.TipoCambio)
        Return obj.SelectTipoCambioBuscarFecha(CInt(IdMoneda), CStr(Fecha))
    End Function


End Class
