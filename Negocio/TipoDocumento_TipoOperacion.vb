﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Chang Carnero, Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 01-Oct-2009
'Hora    : 01:16 pm
'*************************************************
Imports System.Data.SqlClient
Public Class TipoDocumento_TipoOperacion
    Private obj As New DAO.DAOTipoDocumento_TipoOperacion
    'Public Function SelectCboTipoOperacion(ByVal IdDocumento As Integer) As List(Of Entidades.TipoDocumento_TipoOperacion)
    '    Return obj.SelectCboTipoOperacion(IdDocumento)
    'End Function
    Public Function SelectCboTipoOperacion() As List(Of Entidades.TipoDocumento_TipoOperacion)
        Return obj.SelectCboTipoOperacion
    End Function
    Public Function ActualizaTipoDocumento_TipoOperacion(ByVal UpdateTD_TO As List(Of Entidades.TipoDocumento_TipoOperacion)) As Boolean
        Return obj.ActualizaTipoDocumento_TipoOperacion(UpdateTD_TO)
    End Function
    'Edita la lista TipoDocumento_TipoOperacion
    Public Function _TipoDocumento_TipoOperacionSelectxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.TipoDocumento_TipoOperacion)
        Return obj._TipoDocumento_TipoOperacionSelectxIdDocumento(IdDocumento)
    End Function
    Public Function TipoDocumento_TipoOperacionBorrar(ByVal IdDocumento As Integer, ByVal cn As SqlConnection, Optional ByVal tr As SqlTransaction = Nothing) As Boolean
        Return obj.DeletexIdDocumento(cn, tr, IdDocumento)
    End Function
End Class
