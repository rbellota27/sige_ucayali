﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Public Class RelacionDocumento
    Private objDaoRelacionDocumento As New DAO.DAORelacionDocumento


    Public Function Documento_Referencia(ByVal IdDocumento As Integer) As List(Of Entidades.RelacionDocumento)
        Return objDaoRelacionDocumento.Documento_Referencia(IdDocumento)
    End Function

    Public Function SelectxIdDocumentoxIdTipoDocumento(ByVal IdDocumento As Integer, ByVal IdTipoDocumento As Integer) As List(Of Entidades.Documento)
        Return objDaoRelacionDocumento.SelectxIdDocumentoxIdTipoDocumento(IdDocumento, IdTipoDocumento)
    End Function

    Public Function RelacionDocumento_SelectDocumentoRefxIdDocumento2(ByVal IdDocumento2 As Integer) As List(Of Entidades.Documento)
        Return objDaoRelacionDocumento.RelacionDocumento_SelectDocumentoRefxIdDocumento2(IdDocumento2)
    End Function

    Public Function RelacionDocumentoSelectxIdDocumento2xIdTipoDocumento(ByVal IdDocumento2 As Integer, ByVal IdTipoDocumento As Integer) As List(Of Entidades.RelacionDocumento)
        Return objDaoRelacionDocumento.RelacionDocumentoSelectxIdDocumento2xIdTipoDocumento(IdDocumento2, IdTipoDocumento)
    End Function
End Class
