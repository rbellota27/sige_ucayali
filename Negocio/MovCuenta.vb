﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class MovCuenta
    Private obj As New DAO.DAOMovCuenta
    Public Function SelectDeudaxIdPersona(ByVal idPersona As Integer) As List(Of Entidades.MovCuenta)
        Return obj.SelectDeudaxIdPersona(idPersona)
    End Function
    '********************************************************
    'Método Din, para obtener Cuenta por cualquier campo
    'Autor   : Hans Fernando, Quispe Espinoza
    'Módulo  : Caja-Documento
    'Sistema : Indusfer
    'Empresa : Digrafic SRL
    'Fecha   : 3-Set-2009
    'Parametros : psWhere, psOrder
    'Retorna : Lista de tipo MovCuenta
    '********************************************************
    Public Function fnSelTblMovCuentaDin(ByVal psWhere As String, ByVal psOrder As String) As List(Of Entidades.MovCuenta)
        Return obj.fnSelTblMovCuentaDin(psWhere, psOrder)
    End Function
    '********************************************************
    'Método Update, para actualizar Saldo Pendiente
    'Autor   : Hans Fernando, Quispe Espinoza
    'Módulo  : Caja-Documento
    'Sistema : Indusfer
    'Empresa : Digrafic SRL
    'Fecha   : 5-Set-2009
    'Parametros : poBEMovCuenta
    'Retorna : NULL
    '********************************************************
    Public Function fnUdpTblMovCuenta(ByVal poBEMovCuenta As Entidades.MovCuenta) As Integer
        Return obj.fnUdpTblMovCuenta(poBEMovCuenta)
    End Function
    '********************************************************
    'Método Insertar, para Insertar Saldo Pendiente
    'Autor   : Hans Fernando, Quispe Espinoza
    'Módulo  : Caja-Documento
    'Sistema : Indusfer
    'Empresa : Digrafic SRL
    'Fecha   : 7-Set-2009
    'Parametros : poBEMovCuenta
    'Retorna : IdMovCuenta
    '********************************************************
    Public Function fnInsTblMovCuenta(ByVal poBEMovCuenta As Entidades.MovCuenta) As Integer
        Return obj.fnInsTblMovCuenta(poBEMovCuenta)
    End Function
    Public Function SelectDeudaxIdPersona(ByVal IdPersona As Integer, ByVal IdMonedaDestino As Integer) As List(Of Entidades.Documento_MovCuenta)
        Return obj.SelectDeudaxIdPersona(IdPersona, IdMonedaDestino)
    End Function

    Public Function SelectDeudaxIdPersona_NoLetras(ByVal IdPersona As Integer, ByVal IdMonedaDestino As Integer) As List(Of Entidades.Documento_MovCuenta)
        Return obj.SelectDeudaxIdPersona_NoLetras(IdPersona, IdMonedaDestino)
    End Function

    Public Function SelectAbonosxIdMovCuenta(ByVal IdMovCuenta As Integer) As List(Of Entidades.Documento_MovCuenta)
        Return obj.SelectAbonosxIdMovCuenta(IdMovCuenta)
    End Function
End Class
