﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class TipoTablaValor
    Dim DAO As New DAO.DAOTipoTablaValor

    Public Function TipoTablaValorSelectAllxIdTipoTablaxEstado_Paginado( _
               ByVal idtipotabla As Integer, ByVal descripcion As String, _
               ByVal estado As Integer, ByVal PageIndex As Integer, _
               ByVal PageSize As Integer) As List(Of Entidades.TipoTablaValor)
        Return DAO.TipoTablaValorSelectAllxIdTipoTablaxEstado_Paginado(idtipotabla, _
                        descripcion, estado, PageIndex, PageSize)
    End Function

    Public Function InsertaTipoTablaValor(ByVal obj As Entidades.TipoTablaValor) As Boolean
        Return DAO.InsertaTipoTablaValor(obj)
    End Function
    Public Function ActualizaTipoTablaValor(ByVal obj As Entidades.TipoTablaValor) As Boolean
        Return DAO.ActualizaTipoTablaValor(obj)
    End Function
    Public Function SelectAllxIdTipoTablaxEstado(ByVal idtipotabla As Integer, ByVal estado As Integer) As List(Of Entidades.TipoTablaValor)
        Return DAO.SelectAllxIdTipoTablaxEstado(idtipotabla, estado)
    End Function
    Public Function SelectAllxIdTipoTablaxEstado(ByVal idsublinea As Integer, ByVal idtipotabla As Integer, ByVal estado As Integer) As List(Of Entidades.TipoTablaValor)
        Return DAO.SelectAllxIdTipoTablaxEstado(idsublinea, idtipotabla, estado)
    End Function
    Public Function SelectAllxIdTipoTablaxnombre(ByVal idsublinea As Integer, ByVal idtipotabla As Integer, _
                                                 ByVal nombre As String, ByVal PageIndex As Integer, ByVal PageSize As Integer) As List(Of Entidades.TipoTablaValor)
        Return DAO.SelectAllxIdTipoTablaxnombre(idsublinea, idtipotabla, nombre, PageIndex, PageSize)
    End Function
    Public Function GenerarCodigo(ByVal idtipotabla As Integer) As Entidades.TipoTablaValor
        Return DAO.GenerarCodigo(idtipotabla)
    End Function
End Class
