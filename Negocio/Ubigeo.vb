﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'**********************   VIERNES 25 06 2010

Public Class Ubigeo
    Private obj As New DAO.DAOUbigeo
    Public Function SelectxCodDeptoxCodProvxCodDistrito(ByVal CodDepto As String, ByVal CodProv As String, ByVal CodDist As String) As Entidades.Ubigeo
        Return obj.SelectxCodDeptoxCodProvxCodDistrito(CodDepto, CodProv, CodDist)
    End Function

    Public Function SelectAllDepartamentos() As List(Of Entidades.Ubigeo)
        Return obj.SelectAllDepartamentos
    End Function
    Public Function SelectAllProvinciasxCodDpto(ByVal CodDpto As String) As List(Of Entidades.Ubigeo)
        Return obj.SelectAllProvinciasxCodDpto(CodDpto)
    End Function
    Public Function SelectAllDistritosxCodDptoxCodProv(ByVal CodDpto As String, ByVal CodProv As String) As List(Of Entidades.Ubigeo)
        Return obj.SelectAllDistritosxCodDptoxCodProv(CodDpto, CodProv)
    End Function
    Public Function ActualizaUbigeoZona(ByVal EntUbigeo As Entidades.Ubigeo) As Boolean
        Return obj.ActualizaUbigeoZona(EntUbigeo)
    End Function
    Public Function SelectAllDatos() As DataTable
        Return obj.SelectAllDatos()
    End Function

End Class
