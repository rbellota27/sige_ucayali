﻿Imports DAO
Public Class LN_Sector
    Dim obj As New DAO_Sector
    Public Function LN_InsertaSector(ByVal sector As Entidades.Sector) As Boolean
        Return obj.InsertaSector(sector)
    End Function

    Public Function LN_ActualizarSector(ByVal sector As Entidades.Sector) As Boolean
        Return obj.ActualizarSector(sector)
    End Function

    Public Function LN_InsertaRelacion_Sector_Sublinea(ByVal sector As Entidades.Sector) As Boolean
        Return obj.InsertaRelacion_Sector_Sublinea(sector)
    End Function

    Public Function LN_listarSectores(ByVal idDocumento As Integer, ByVal idProducto As Integer) As DataSet
        Return obj.DAO_listarSectores(idDocumento, idProducto)
    End Function
End Class
