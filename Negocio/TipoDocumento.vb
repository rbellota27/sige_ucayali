﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



'**************************   MARTES 11 MAYO 2010 HORA 10_45 AM

Imports System.Data.SqlClient
Public Class TipoDocumento
    Private obj As New DAO.DAOTipoDocumento
    Private objconexion As New DAO.Conexion
    Dim objTipoDocumento As New DAO.DAOTipoDocumento

    

    Public Function SelectTipoDocumentoxIdTipoDocumento(ByVal IdTipoDocumento As String) As List(Of Entidades.TipoDocumento)
        Return obj.SelectTipoDocumentoxIdTipoDocumento(IdTipoDocumento)
    End Function

    Public Function SelectTipoDocumentoExternoActivo() As List(Of Entidades.TipoDocumento)
        Return obj.SelectTipoDocumentoExternoActivo
    End Function

    Public Function ActualizaTipoDocumento(ByVal tipodocumento As Entidades.TipoDocumento, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        'Public Function ActualizaTipoDocumento(ByVal tipodocumento As Entidades.TipoDocumento) As Boolean
        Return objTipoDocumento.ActualizaTipoDocumento(tipodocumento, cn, tr)
    End Function

    Public Function SelectxId(ByVal idTipoDocumento As Integer) As Entidades.TipoDocumento
        Return objTipoDocumento.SelectxId(idTipoDocumento)
    End Function
    Public Function SelectAll() As List(Of Entidades.TipoDocumento)
        Return objTipoDocumento.SelectAll
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.TipoDocumento)
        Return objTipoDocumento.SelectAllxNombre(nombre)
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.TipoDocumento)
        Return objTipoDocumento.SelectAllActivo()
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoDocumento)
        Return objTipoDocumento.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.TipoDocumento)
        Return objTipoDocumento.SelectAllInactivo
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoDocumento)
        Return objTipoDocumento.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxCodSunat(ByVal codSunat As String) As List(Of Entidades.TipoDocumento)
        Return objTipoDocumento.SelectxCodSunat(codSunat)
    End Function
    Public Function SelectCboxIdEmpresaxIdArea(ByVal IdEmpresa As Integer, ByVal IdArea As Integer) As List(Of Entidades.TipoDocumento)
        Return objTipoDocumento.SelectCboxIdEmpresaxIdArea(IdEmpresa, IdArea)
    End Function
    ''AGREGADO 10/07/14
    Public Function SelectCboTipoDocumentoProgPago() As List(Of Entidades.TipoDocumento)
        Return objTipoDocumento.SelectCboTipoDocumentoProgPago
    End Function
    Public Function SelecttipodocumentosCaja() As List(Of Entidades.TipoDocumento)
        Return objTipoDocumento.SelectCboTipoDocumentosCaja()
    End Function

    Public Function SelecttipodocumentoInd() As List(Of Entidades.TipoDocumento)
        Return objTipoDocumento.SelectCboTipoDocumentoIndep()
    End Function

    Public Function SelectCbo() As List(Of Entidades.TipoDocumento)
        Return objTipoDocumento.SelectCbo
    End Function
    Public Function SelectExportarContaxIdTiPoDocumento(ByVal IdTipoDocumento As Integer) As String
        Return objTipoDocumento.SelectExportarContaxIdTiPoDocumento(IdTipoDocumento)
    End Function

    Public Function ActualizaTipoDocTipoOpera(ByVal TipoDoc As Entidades.TipoDocumento, _
    ByVal lista As List(Of Entidades.TipoDocumento_TipoOperacion), _
    Optional ByVal listaTDocRef As List(Of Entidades.TipoDocRef_TipoDoc_View) = Nothing) As Boolean
        Dim cn As SqlConnection = objconexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            '*************  Actualiza TipoDocumento  ********************
            If Not (obj.ActualizaTipoDocumento(TipoDoc, cn, tr)) Then
                Throw New Exception
            End If

            '*************  Elimina TipoDocumento_TipoOperacion ************************
            Dim obJDaoTipoDocumento_TipoOperacion As New DAO.DAOTipoDocumento_TipoOperacion
            If (Not obJDaoTipoDocumento_TipoOperacion.DeletexIdDocumento(cn, tr, TipoDoc.Id)) Then
                Throw New Exception
            End If
            '****************************** Insertar Lista  **************************************
            obJDaoTipoDocumento_TipoOperacion.InsertaTipoDocumento_TipoOperacion(cn, tr, lista, TipoDoc.Id)


            '************ Insertar listaTDocRef
            If Not listaTDocRef Is Nothing Then
                Dim obj As New DAO.DAOTipoDocRef_TipoDoc_View
                obj.DeletexIdDocumento(TipoDoc.Id)
                obj.InsertaListaTipoDocumentoRef(cn, tr, listaTDocRef, TipoDoc.Id)
            End If
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
    Public Function InsertaTipoDocumento(ByVal TipoDocumento As Entidades.TipoDocumento, _
    ByVal lista As List(Of Entidades.TipoDocumento_TipoOperacion), _
    Optional ByVal listaTDocRef As List(Of Entidades.TipoDocRef_TipoDoc_View) = Nothing) As Boolean

        Dim cn As SqlConnection = objconexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction

            '************ Insertar Magnitud
            Dim IdDocumento As Integer = obj.InsertaTipoDocumento(TipoDocumento, cn, tr)

            '************ Insertar Lista
            Dim objDaoTipoDocumento As New DAO.DAOTipoDocumento_TipoOperacion
            Dim objBEMagnitud As New Entidades.TipoDocumento_TipoOperacion
            objDaoTipoDocumento.InsertaTipoDocumento_TipoOperacion(cn, tr, lista, IdDocumento)

            '************ Insertar listaTDocRef
            If Not listaTDocRef Is Nothing Then
                Dim obj As New DAO.DAOTipoDocRef_TipoDoc_View
                obj.DeletexIdDocumento(IdDocumento)
                obj.InsertaListaTipoDocumentoRef(cn, tr, listaTDocRef, IdDocumento)
            End If

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally

            If cn.State = ConnectionState.Open Then cn.Close()

        End Try
        Return hecho
    End Function


    Public Function SelectCboTipoOperacion() As List(Of Entidades.TipoDocumento_TipoOperacion)
        Return obj.SelectCboTipoOperacion
    End Function
End Class
