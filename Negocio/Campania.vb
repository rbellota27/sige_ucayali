﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'******************   LUNES 31 MAYO 2010 HORA 03_23 PM

Imports System.Data.SqlClient

Public Class Campania

    Private objDaoCampania As New DAO.DAOCampania
    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private tr As SqlTransaction

    Public Function Campania_SelectxParams(ByVal IdCampania As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Descripcion As String, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal Estado As Boolean) As DataTable
        Return objDaoCampania.Campania_SelectxParams(IdCampania, IdEmpresa, IdTienda, Descripcion, FechaInicio, FechaFin, Estado)
    End Function
    Public Function Campania_SelectxParams2(ByVal IdCampania As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Descripcion As String, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal Estado As Boolean) As DataTable
        Return objDaoCampania.Campania_SelectxParams2(IdCampania, IdEmpresa, IdTienda, Descripcion, FechaInicio, FechaFin, Estado)
    End Function

    Public Function Campania_SelectxIdCampania(ByVal IdCampania As Integer) As Entidades.Campania
        Return objDaoCampania.Campania_SelectxIdCampania(IdCampania)
    End Function

    Public Function registrarCampania(ByVal objCampania As Entidades.Campania, ByVal IdUsuario As Integer, ByVal listaCampania_Detalle As List(Of Entidades.Campania_Detalle)) As Boolean

        Dim hecho As Boolean = False

        Try

            '******************* VALIDAMOS EL DETALLE
            Dim listaAux As New List(Of Entidades.Campania_Detalle)
            If (objCampania.IdCampania <> Nothing) Then

                listaAux = (New DAO.DAOCampania_Detalle).Campania_Detalle_SelectxIdCampania(objCampania.IdCampania)

                For i As Integer = listaAux.Count - 1 To 0 Step -1

                    For j As Integer = listaCampania_Detalle.Count - 1 To 0 Step -1

                        If (listaAux(i).IdCampania = listaCampania_Detalle(j).IdCampania And listaAux(i).IdProducto = listaCampania_Detalle(j).IdProducto And listaAux(i).IdUnidadMedida = listaCampania_Detalle(j).IdUnidadMedida) Then

                            listaAux.RemoveAt(i)
                            Exit For

                        End If

                    Next

                Next

            End If

            cn.Open()
            tr = cn.BeginTransaction

            '************** REGISTRO LA CABECERA
            objCampania.IdCampania = objDaoCampania.Campania_Registrar(objCampania, IdUsuario, cn, tr)

            '**************** REGISTRO EL DETALLE
            Dim objDaoCampania_Detalle As New DAO.DAOCampania_Detalle
            For i As Integer = 0 To listaCampania_Detalle.Count - 1

                listaCampania_Detalle(i).IdCampania = objCampania.IdCampania
                objDaoCampania_Detalle.Campania_Detalle_Registrar(cn, tr, listaCampania_Detalle(i))

            Next

            '******************* ELIMINAMOS LOS DATOS
            For i As Integer = 0 To listaAux.Count - 1

                objDaoCampania_Detalle.Campania_Detalle_DeletexParams(listaAux(i).IdCampania, listaAux(i).IdProducto, listaAux(i).IdUnidadMedida, cn, tr)

            Next


            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function
End Class
