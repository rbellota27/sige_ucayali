﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DsctoVolVtas

    Dim objDAODsctoVolVtas As New DAO.DAODsctoVolVtas
    Dim cn As SqlConnection
    Dim cmd As SqlCommand
    Dim tr As SqlTransaction

    Public Function DsctoVolVtas_Registrar(ByVal ListaDsctoVolVtas As List(Of Entidades.DsctoVolVtas)) As Boolean


        cn = (New DAO.Conexion).ConexionSIGE

        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            For Each obj As Entidades.DsctoVolVtas In ListaDsctoVolVtas

                objDAODsctoVolVtas.DsctoVolVtas_Transaction(obj, cn, tr)

            Next

            tr.Commit()

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        End Try

        Return True

    End Function

    Public Function DsctoVolVtas_UpdateEst(ByVal ListaDsctoVolVtas As List(Of Entidades.DsctoVolVtas)) As String

        cn = (New DAO.Conexion).ConexionSIGE
        Dim Mensaje As String = ""
        Dim Operacion As Boolean
        Dim Ok As Integer = 0

        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            For Each obj As Entidades.DsctoVolVtas In ListaDsctoVolVtas

                Operacion = objDAODsctoVolVtas.DsctoVolVtas_UpdateEst(obj, cn, tr)

                If Operacion Then
                    Ok += 1
                End If

            Next

            tr.Commit()

            Mensaje = "Nro de Transacción " + CStr(ListaDsctoVolVtas.Count) + " cantidad de registros actualizados " + CStr(Ok)

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        End Try

        Return Mensaje

    End Function

    Public Function DsctoVolVtas_UpdateEstG(ByVal ListaDsctoVolVtas As List(Of Entidades.DsctoVolVtas)) As String

        cn = (New DAO.Conexion).ConexionSIGE
        Dim Mensaje As String = ""
        Dim Operacion As Boolean
        Dim Ok As Integer = 0

        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            For Each obj As Entidades.DsctoVolVtas In ListaDsctoVolVtas

                Operacion = objDAODsctoVolVtas.DsctoVolVtas_UpdateEstG(obj, cn, tr)

                If Operacion Then
                    Ok += 1
                End If

            Next

            tr.Commit()

            Mensaje = "Nro de Transacción " + CStr(ListaDsctoVolVtas.Count) + " cantidad de registros actualizados " + CStr(Ok)

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        End Try

        Return Mensaje

    End Function

    Public Function DsctoVolVtas_SelectNew(ByVal fecInicio As Date, ByVal fecFinal As Date, ByVal cantMinima As Integer, ByVal cantMaxima As Integer, ByVal IdTienda As Integer, ByVal IdTipoPV As Integer, ByVal Dscto As Decimal, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdProducto As Integer, ByVal CodigoProducto As String, ByVal Producto As String, ByVal tabla As DataTable, ByVal PageIndex As Integer, ByVal PageSize As Integer) As List(Of Entidades.DsctoVolVtas)

        Return objDAODsctoVolVtas.DsctoVolVtas_SelectNew(fecInicio, fecFinal, cantMinima, cantMaxima, IdTienda, IdTipoPV, Dscto, IdLinea, IdSubLinea, IdProducto, CodigoProducto, Producto, tabla, PageIndex, PageSize)

    End Function

    Public Function DsctoVolVtas_SelectD(ByVal objDscto As Entidades.DsctoVolVtas, ByVal tabla As DataTable, ByVal PageIndex As Integer, ByVal PageSize As Integer) As List(Of Entidades.DsctoVolVtas)
        Return objDAODsctoVolVtas.DsctoVolVtas_SelectD(objDscto, tabla, PageIndex, PageSize)
    End Function

    Public Function DsctoVolVtas_SelectG(ByVal objDscto As Entidades.DsctoVolVtas, ByVal PageIndex As Integer, ByVal PageSize As Integer) As List(Of Entidades.DsctoVolVtas)
        Return objDAODsctoVolVtas.DsctoVolVtas_SelectG(objDscto, PageIndex, PageSize)
    End Function


    Public Function fx_DsctoVolVtas(ByVal IdUsuario As Integer, ByVal IdTienda As Integer, ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer, ByVal IdProducto As Integer, ByVal Cantidad As Decimal, ByVal Fecha As String) As Decimal

        Dim DsctoVolVtas As Decimal = 0

        cn = (New DAO.Conexion).ConexionSIGE
        cmd = New SqlCommand
        cmd.Connection = cn
        cmd.CommandText = "SELECT [dbo].[fx_DsctoVolVtas] ( " + CStr(IdUsuario) + "," + CStr(IdTienda) + "," + CStr(IdCondicionPago) + "," + CStr(IdMedioPago) + "," + CStr(IdProducto) + "," + CStr(Cantidad) + ",'" + CStr(Fecha) + "')"

        Try

            cn.Open()
            DsctoVolVtas = CDec(cmd.ExecuteScalar())

        Catch ex As Exception
            DsctoVolVtas = 0
        End Try

        Return DsctoVolVtas

    End Function

    Public Function Facturacion(ByVal objDscto As Entidades.DsctoVolVtas) As List(Of Entidades.DsctoVolVtas)
        Return objDAODsctoVolVtas.Facturacion(objDscto)
    End Function
End Class
