﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

' fecha : 09-Nov-2009

Public Class DocumentoI
    Private obj As New DAO.DAODocumentoI

#Region "otros"
    Public Function SelectxIdPersona(ByVal IdPersona As Integer) As Entidades.DocumentoI
        Return obj.SelectxIdPersona(IdPersona)
    End Function
    Public Function fnSelTblDocumentoDin(ByVal psWhere As String, ByVal psOrder As String) As List(Of Entidades.Documento)
        Dim loDocumento As New DAO.DAODocumento
        Return loDocumento.fnSelTblDocumentoDin(psWhere, psOrder)
    End Function
    Public Function fnInsTblDocumento(ByVal documento As Entidades.Documento) As Integer
        Dim poDLDocumento As New DAO.DAODocumento
        Return poDLDocumento.fnInsTblDocumento(documento)
    End Function
#End Region

#Region "mantenimiento Persona"

    Public Function Selectx_IdPersona(ByVal IdPersona As Integer) As List(Of Entidades.DocumentoI)
        'Return obj.Selectx_IdPersona(IdPersona)
        Return (New DAO.DAODocumentoI).Selectx_IdPersona(IdPersona)
    End Function

    Public Function listarTipoDocumento() As List(Of Entidades.TipoDocumentoI)
        Return obj.listarTipoDocumento()
    End Function

#End Region

End Class
