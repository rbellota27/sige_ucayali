﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DocumentosMercantiles
    Private docMercantiles As New DAO.DAODocumentosMercantiles

    Public Function pathFacturaElectronica(ByVal direccion As String, ByVal idSerie As Integer, ByVal doc_Codigo As String, ByVal idTipoDocumento As Integer) As String
        Dim cn As SqlConnection
        cn = (New DAO.Conexion).ConexionSIGE
        cn.Open()
        Return (New DAO.DAODocumentosMercantiles).pathFacturaElectronica(direccion, idSerie, doc_Codigo, idTipoDocumento, cn)
    End Function

    Public Function RptCosteoImportacion(ByVal IdDocumento As Integer) As DataSet
        Return docMercantiles.RptCosteoImportacion(IdDocumento)
    End Function


    Public Function RptCosteoProyectadoImportacion(ByVal IdDocumento As Integer) As DataSet
        Return docMercantiles.RptCosteoProyectadoImportacion(IdDocumento)
    End Function

    Public Function ReporteDocCotizacion(ByVal IdDocumento As Integer) As DataSet
        Return docMercantiles.getReporteDocCotizacion(IdDocumento)
    End Function
    Public Function ReporteDocReciboI(ByVal IdDocumento As Integer) As DataSet
        Return docMercantiles.RptDocReciboI(IdDocumento)
    End Function
    Public Function ReporteDocReciboE(ByVal IdDocumento As Integer) As DataSet
        Return docMercantiles.RptDocReciboE(IdDocumento)
    End Function
    Public Function RptDocNotaCredito(ByVal IdDocumento As Integer) As DataSet
        Return docMercantiles.RptDocNotaCredito(IdDocumento)
    End Function
    Public Function getDataSetOrdenCompra(ByVal IdDocumento As Integer) As DataSet
        Return docMercantiles.getDataSetOrdenCompra(IdDocumento)
    End Function
    Public Function getDataSetOrdenCompraNomComercial(ByVal IdDocumento As Integer) As DataSet
        Return docMercantiles.getDataSetOrdenCompraNomComercial(IdDocumento)
    End Function
    Public Function rptDocFactBol(ByVal iddocumento As Integer, ByVal tipoimpresion As Integer) As DataSet
        Return docMercantiles.rptDocFactBol(iddocumento, tipoimpresion)
    End Function
    Public Function rptDocNotaDebito(ByVal iddocumento As Integer) As DataSet
        Return docMercantiles.rptDocNotaDebito(iddocumento)
    End Function
    Public Function rptDocPedidoCliente(ByVal iddocumento As Integer) As DataSet
        Return docMercantiles.rptDocPedidoCliente(iddocumento)
    End Function
    Public Function rptDocControlInterno(ByVal iddocumento As Integer) As DataSet
        Return docMercantiles.rptDocControlInterno(iddocumento)
    End Function

    Public Function rptDocGuias(ByVal iddocumento As Integer) As DataSet
        Return docMercantiles.rptDocGuias(iddocumento)
    End Function

    Public Function rptGuiaxTono(ByVal iddocumento As Integer) As DataSet
        Return docMercantiles.rptGuiasxTonos(iddocumento)
    End Function

    Public Function getReporteDocCotizacion(ByVal iddoducmento As Integer) As DataSet
        Return docMercantiles.getReporteDocCotizacion(iddoducmento)
    End Function
    Public Function ImpDocsEjm(ByVal IdTipoDocs As Integer) As Integer
        Return docMercantiles.ImpDocsEjm(IdTipoDocs)
    End Function
    Public Function crpImprimir(ByVal idtipodocumento As Integer) As Int16
        Return docMercantiles.crpImprimir(idtipodocumento)
    End Function

End Class
