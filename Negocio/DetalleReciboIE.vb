﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Nagamine Molina, Jorge
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 20-Octubre-2009
'Hora    : 01:00 pm
'*************************************************

'********************************************************
'Método Din, para obtener DetalleRecibo por cualquier campo
'Autor   : Caro Freyre, verly
'Módulo  : Caja-Documento
'Sistema : Indusfer
'Empresa : Digrafic SRL
'Fecha   : 01-Oct-2009
'Parametros : psWhere, psOrder
'Retorna : Lista de tipo DetalleRecibo
'********************************************************

Public Class DetalleReciboIE
    Dim poDLTblDetalleRecibo As New DAO.DAODetalleRecibo
    Public Function fnInsTblDetalleRecibo(ByVal poBETblDetalleRecibo As Entidades.DetalleRecibo) As Integer
        Return poDLTblDetalleRecibo.fnInsTblDetalleRecibo(poBETblDetalleRecibo)
    End Function

    Public Function fnSelTblDetalleReciboDin(ByVal psWhere As String, ByVal psOrder As String) As List(Of Entidades.DetalleRecibo)
        Return poDLTblDetalleRecibo.fnSelTblDetalleReciboDin(psWhere, psOrder)
    End Function

    Public Function DocumentoReciboIngresoSelectDet(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleReciboView_1)
        Return poDLTblDetalleRecibo.DocumentoReciboIngresoSelectDet(IdDocumento)
    End Function

    Public Function DocumentoReciboEgresoSelectDet(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleReciboView_1)
        Return poDLTblDetalleRecibo.DocumentoReciboEgresoSelectDet(IdDocumento)
    End Function
End Class
