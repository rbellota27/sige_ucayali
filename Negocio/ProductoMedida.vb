﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*****************  VIERNES 22 ENERO 2010 HORA 03_54 PM
Public Class ProductoMedida
    Private objDaoProductoMedida As New DAO.DAOProductoMedida
    Public Function getMagnitudProducto(ByVal IdProducto As Integer, ByVal IdUMProducto As Integer, ByVal Cantidad As Decimal, ByVal IdMagnitud As Integer, ByVal IdUMMagnitud_Destino As Integer) As Decimal
        Return objDaoProductoMedida.getMagnitudProducto(IdProducto, IdUMProducto, Cantidad, IdMagnitud, IdUMMagnitud_Destino)
    End Function

End Class
