﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Chang Carnero, Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 05-Oct-2009
'Hora    : 04:00 pm
'*************************************************
Imports System.Data.SqlClient
Public Class Banco
    Dim objBanco As New DAO.DAOBanco
    Dim objconnnection As New DAO.Conexion




    Public Function Banco_CuentaBancaria(ByVal IdPersona As Integer) As List(Of Entidades.Banco)
        Return objBanco.Banco_CuentaBancaria(IdPersona)
    End Function



    Public Function InsertaBanco(ByVal banco As Entidades.Banco, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Return CBool(objBanco.InsertaBanco(banco, cn, tr))
    End Function
    Public Function ActualizaBanco(ByVal Banco As Entidades.Banco, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        'Public Function ActualizaBanco(ByVal Banco As Entidades.Banco) As Boolean
        Return objBanco.ActualizaBanco(Banco, cn, tr)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Banco)
        Return objBanco.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectAll() As List(Of Entidades.Banco)
        Return objBanco.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Banco)
        Return objBanco.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Banco)
        Return objBanco.SelectAllInactivo
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Banco)
        Return objBanco.SelectAllxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Banco)
        Return objBanco.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxCodSunat(ByVal codSunat As String) As List(Of Entidades.Banco)
        Return objBanco.SelectxCodSunat(codSunat)
    End Function
    Public Function SelectxId(ByVal idbanco As Integer) As List(Of Entidades.Banco)
        Return objBanco.SelectxId(idbanco)
    End Function
    Public Function SelectCbo() As List(Of Entidades.Banco)
        Return objBanco.SelectCbo
    End Function

    Public Function ln_selectCuentaDetraccion() As List(Of Entidades.Banco)
        Return objBanco.selectCuentaDetraccion()
    End Function
    'Public Function InsertaBanco(ByVal Banco As Entidades.Banco, ByVal lista As List(Of Entidades.Oficina)) As Boolean
    '    Dim cn As SqlConnection = objconnnection.ConexionSIGE
    '    Dim tr As SqlTransaction = Nothing
    '    Dim hecho As Boolean = False
    '    Try
    '        cn.Open()
    '        tr = cn.BeginTransaction

    '        '************ Insertar Banco
    '        Dim IdBanco As Integer = objBanco.InsertaBanco(Banco, cn, tr)

    '        '************ Insertar Lista DGV_OFICINA
    '        Dim objDaoBanco As New DAO.DAOOficina
    '        Dim objBEBanco As New Entidades.Oficina
    '        objDaoBanco.InsertaOficina(cn, tr, lista, IdBanco)

    '        tr.Commit()
    '        hecho = True

    '    Catch ex As Exception
    '        tr.Rollback()
    '        hecho = False
    '    Finally

    '        If cn.State = ConnectionState.Open Then cn.Close()

    '    End Try
    '    Return hecho
    'End Function
    Public Function InsertaBanco(ByVal objban As Entidades.Banco) As Boolean
        objconnnection = New DAO.Conexion
        Dim cn As SqlConnection = objconnnection.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim IdBanco As Integer = 0
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            IdBanco = objBanco.InsertaBanco(objban, cn, tr)
            If IdBanco = -1 Then
                Throw New Exception("Fracasó el proceso de Inserción.")
            End If
            Dim objDaoBancoOfici As New DAO.DAOOficina
            
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex

        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
    Public Function InsertaBancoOficina(ByVal objban As Entidades.Banco, ByVal listaOficina As List(Of Entidades.Oficina)) As Boolean
        objconnnection = New DAO.Conexion
        Dim cn As SqlConnection = objconnnection.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim IdBanco As Integer = 0
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            IdBanco = objBanco.InsertaBanco(objban, cn, tr)
            If IdBanco = -1 Then
                Throw New Exception("Fracasó el proceso de Inserción.")
            End If
            Dim objDaoBancoOfici As New DAO.DAOOficina
            If Not objDaoBancoOfici.InsertaListaOficina(IdBanco, listaOficina, cn, tr) Then
                Throw New Exception("Fracasó el Proceso de Inserción")
            End If
          
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex

        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
    'Public Function ActualizaBancoOficina(ByVal objBan As Entidades.Banco, ByVal listaOficina As List(Of Entidades.Oficina), ByVal listaLetraCambio As List(Of Entidades.LetraCambio)) As Boolean
    Public Function ActualizaBancoOficina(ByVal objBan As Entidades.Banco, ByVal listaOficina As List(Of Entidades.Oficina)) As Boolean
        '************Obtengo los item a eliminar
        Dim objOficina As New Negocio.Oficina
        Dim listaOficina_Editar As List(Of Entidades.Oficina) = objOficina.SelectxIdBanco(objBan.Id)
        Dim i As Integer = listaOficina_Editar.Count - 1
        While i > 0
            Dim j As Integer = listaOficina.Count - 1
            While j >= 0
                If listaOficina_Editar.Item(i).Id = listaOficina.Item(j).Id Then
                    listaOficina_Editar.RemoveAt(i)
                    Exit While
                End If
                j = j - 1
            End While
            i = i - 1
        End While
        objconnnection = New DAO.Conexion
        Dim cn As SqlConnection = objconnnection.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction

            If Not objBanco.ActualizaBanco(objBan, cn, tr) Then
                Throw New Exception("Fracasó el proceso de Actualización.")
            End If

            Dim objDaoBancoOfic As New DAO.DAOOficina
            'elimina la lista vieja

            For x As Integer = 0 To listaOficina_Editar.Count - 1
                With listaOficina_Editar.Item(x)
                    objDaoBancoOfic.DeletexIdBancoxIdOficina(.IdBanco, .Id, cn, tr)
                End With
            Next
            'inserta la lista nueva
            If Not objDaoBancoOfic.InsertaListaOficina(objBan.Id, listaOficina, cn, tr) Then
                'If Not objDaoBancoOfic.InsertaOficina(cn, tr, listaOficina, objBan.Id) Then
                Throw New Exception("Fracasó el proceso de Actualización88888888")
            End If
            'Dim objDaoBanOficina As New DAO.DAOLetraCambio
            'Dim objBancoOficina As Entidades.LetraCambio
            'If Not objDaoBanOficina.DeletexIdBanco(objBan.Id, cn, tr) Then
            '    Throw New Exception("Fracasó el proceso de Actualización.")
            'End If
            'For Each objBancoOficina In listaLetraCambio
            '    objBancoOficina.IdBanco = objBan.Id
            '    objDaoBanOficina.InsertaLetraCambio(cn, tr, objBancoOficina)
            'Next
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
End Class
