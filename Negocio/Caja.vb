﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




'*********************   VIERNES 14 MAYO 2010 HORA 04_38 PM


Imports System.Data.SqlClient
Imports System.Data.SqlClient.SqlTransaction
Public Class Caja
    Dim daoCaja As New DAO.DAOCaja
    Private objConexion As DAO.Conexion


    Public Function SelectResumenxParams(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdCaja As Integer, ByVal IdMedioPago As Integer) As DataTable
        Return daoCaja.SelectResumenxParams(IdEmpresa, IdTienda, IdCaja, IdMedioPago)
    End Function

    Public Function SelectCboxIdTienda(ByVal idtienda As Integer) As List(Of Entidades.Caja)
        Return daoCaja.SelectCboxIdTienda(idtienda)
    End Function
    Public Function SelectIdCajaxIdPersona(ByVal IdPersona As Integer) As Integer
        Return daoCaja.SelectIdCajaxIdPersona(IdPersona)
    End Function
    Public Function SelectAll() As List(Of Entidades.Caja)
        Return daoCaja.SelectAll
    End Function
    Public Function SelectGrillaxIdTienda(ByVal idtienda As Integer) As List(Of Entidades.Caja)
        Return daoCaja.SelectGrillaxIdTienda(idtienda)
    End Function
    Public Function SelectCajaxId(ByVal IdCaja As Integer) As Entidades.Caja
        Return daoCaja.SelectCajaxId(IdCaja)
    End Function
    Public Function SelectAllActivoInactivo() As List(Of Entidades.Caja)
        Return daoCaja.SelectAllActivoInactivo
    End Function
    Public Function GrabarCaja(ByVal caja As List(Of Entidades.Caja)) As Boolean
        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction
            Dim objDaoCaja As New DAO.DAOCaja
            objDaoCaja.GrabaCajaT(cn, caja, tr)
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function


#Region " ++++++++++++++++++++++++++++++ REPORTE ++++++++++++++ "

    Public Function getDataSet_LiquidacionCaja(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdCaja As Integer, ByVal FechaIni As String, ByVal FechaFin As String) As DataSet
        Return daoCaja.getDataSet_LiquidacionCaja(IdEmpresa, IdTienda, IdCaja, FechaIni, FechaFin)
    End Function


#End Region

End Class
