﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Transito
    Private objDaoTransito As New DAO.DAOTransito
    Public Function SelectAllActivo() As List(Of Entidades.Transito)
        Return objDaoTransito.SelectAllActivo
    End Function
    Public Function SelectAll() As List(Of Entidades.Transito)
        Return objDaoTransito.SelectAll
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Transito)
        Return objDaoTransito.SelectAllInactivo
    End Function
    Public Function InsertaTransito(ByVal Estilo As Entidades.Transito) As Boolean
        Return objDaoTransito.InsertaTransito(Estilo)
    End Function
    Public Function ActualizaTransito(ByVal estilo As Entidades.Transito) As Boolean
        Return objDaoTransito.ActualizaTransito(estilo)
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Transito)
        Return objDaoTransito.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Transito)
        Return objDaoTransito.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Transito)
        Return objDaoTransito.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Transito)
        Return objDaoTransito.SelectxId(id)
    End Function
End Class
