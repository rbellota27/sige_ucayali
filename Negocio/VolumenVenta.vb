﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*****************  VIERNES 22 ENERO 2010 HORA 03_54 PM
Imports System.Data.SqlClient

Public Class VolumenVenta
    Dim DAOVolumenVenta As New DAO.DAOVolumenVenta
    Dim DAOProducto_VolVenta As New DAO.DAOProducto_VolVenta
    Public Function SelectxIdProductoxIdTipoPVxIdTienda(ByVal IdProducto As Integer, _
    ByVal IdTipoPV As Integer, ByVal IdTienda As Integer) As List(Of Entidades.Producto_VolVenta)
        Return DAOProducto_VolVenta.SelectxIdProductoxIdTipoPVxIdTienda(IdProducto, IdTipoPV, IdTienda)
    End Function
    Public Function InsertaVolumenVenta(ByVal VolumenVenta As Entidades.VolumenVenta) As Boolean
        Return DAOVolumenVenta.InsertaVolumenVenta(VolumenVenta)
    End Function

    Public Function ActualizaVolumenVenta(ByVal VolumenVenta As Entidades.VolumenVenta) As Boolean
        Return DAOVolumenVenta.ActualizaVolumenVenta(VolumenVenta)
    End Function
    Public Function DeleteVolumenVenta(ByVal VolumenVenta As Entidades.VolumenVenta) As Boolean
        Return DAOVolumenVenta.DeleteVolumenVenta(VolumenVenta)
    End Function
    Public Function DeleteVolumenVentaxParametros(ByVal IdLinea As Integer, _
    ByVal IdSubLinea As Integer, ByVal IdTipoPV As Integer, ByVal IdTienda As Integer) As Boolean
        Return DAOVolumenVenta.DeleteVolumenVentaxParametros(IdLinea, IdSubLinea, IdTipoPV, IdTienda)
    End Function
    Public Function SelectxLineaxSubLineaxTipoPVxTienda(ByVal IdLinea As Integer, _
    ByVal IdSubLinea As Integer, ByVal IdTipoPV As Integer, ByVal IdTienda As Integer, _
    ByVal IdTipoExistencia As Integer, ByVal tabla As DataTable) As List(Of Entidades.Producto_VolVenta)
        Return DAOProducto_VolVenta.SelectxLineaxSubLineaxTipoPVxTienda(IdLinea, IdSubLinea, IdTipoPV, IdTienda, IdTipoExistencia, tabla)
    End Function



    Public Function InsertaVolumenVentaxParametros(ByVal ListaVolumenVenta As List(Of Entidades.Producto_VolVenta)) As Boolean
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            For Each obj As Entidades.Producto_VolVenta In ListaVolumenVenta
                DAOVolumenVenta.InsertaVolumenVentaxParametros(obj, cn, tr)
            Next

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

        Return True

    End Function

End Class
