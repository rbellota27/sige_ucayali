﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************** MARTES 26 ENERO 2010 HORA 03_44 PM
'*************** MARTES 28 ENERO 2010 HORA 01_09 pm
Imports System.Data.SqlClient

Public Class ParametroGeneral

    'Private objScript As New ScriptManagerClass 'para mostrar mensaje de error
    Private objDaoParametroGeneral As New DAO.DAOParametroGeneral
    Private obj As New DAO.DAOParametroGeneral
    Private objConexion As New DAO.Conexion
    Public Function SelectCboArea() As List(Of Entidades.Area)
        Return obj.SelectCboArea
    End Function


    Public Function SelectxIdAreaxIdParametro(ByVal IdArea As Integer) As List(Of Entidades.ParametroGeneral)
        Return obj.SelectxIdAreaxIdParametro(IdArea)
    End Function


    Public Function getValorParametroGeneral(ByVal IdParametroGeneral() As Integer) As Decimal()
        Dim lista(IdParametroGeneral.Length - 1) As Decimal
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE

        Try
            cn.Open()

            For i As Integer = 0 To IdParametroGeneral.Length - 1
                lista(i) = objDaoParametroGeneral.ParametroGeneralSelectActivoValorxIdParametro(IdParametroGeneral(i), cn)
            Next

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return lista

    End Function
    Public Function ParametrosGeneralSelectxNombre(ByVal IdParametro As Integer, ByVal IdArea As Integer) As List(Of Entidades.ParametroGeneral)
        Return obj.SelectxIdAreaxIdParametro(IdParametro)
    End Function

    Public Function InsertaParametroGeneral(ByVal ListaParametroGeneral As List(Of Entidades.ParametroGeneral), ByVal IdArea As Integer) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            '************ Delete
            'obj.DeletexIdPerfilxIdArea(IdPerfil, IdArea, cn, tr)

            For i As Integer = 0 To ListaParametroGeneral.Count - 1 'desde 0 hasta tamaño de lista
                ListaParametroGeneral(i).IdParametro = CInt(objDaoParametroGeneral.InsertaParametroGeneral(ListaParametroGeneral(i), IdArea, cn, tr))
                'obj.InsertaParametroGeneral(ListaParametroGeneral(i), IdArea, cn, tr)
                'ListaParametroGeneral(i).getLetraCambio.IdDocumento = listaLetras(i).Id

            Next
            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function

    Public Function ActualizaParametroGeneral(ByVal ListaParametroGeneral As List(Of Entidades.ParametroGeneral), ByVal IdArea As Integer) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            '************ Delete
            'obj.DeletexIdPerfilxIdArea(IdPerfil, IdArea, cn, tr)
            For i As Integer = 0 To ListaParametroGeneral.Count - 1

                ListaParametroGeneral(i).IdParametro = CInt(objDaoParametroGeneral.ActualizaParametroGeneral(ListaParametroGeneral(i), IdArea, cn, tr))
                'obj.ActualizaParametroGeneral(ListaParametroGeneral(i), IdArea, cn, tr)

            Next

            tr.Commit()
            hecho = True

        Catch ex As Exception
            ' objScript.mostrarMsjAlerta(Me, ex.Message)
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function
End Class
