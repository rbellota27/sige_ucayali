﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class TipoTabla
    Dim DAO As New DAO.DAOTipoTabla

    Public Function TipoTablaSelectAllxEstado_Paginado(ByVal Descripcion As String, _
                           ByVal Estado As Integer, ByVal PageIndex As Integer, _
                           ByVal PageSize As Integer) As List(Of Entidades.TipoTabla)
        Return DAO.TipoTablaSelectAllxEstado_Paginado(Descripcion, Estado, PageIndex, PageSize)
    End Function


    Public Function InsertaTipoTabla(ByVal obj As Entidades.TipoTabla) As Boolean
        Return DAO.InsertaTipoTabla(obj)
    End Function
    Public Function ActualizarTipoTabla(ByVal obj As Entidades.TipoTabla) As Boolean
        Return DAO.ActualizarTipoTabla(obj)
    End Function
    Public Function SelectAllTipoTablaxEstado(ByVal Estado As Integer) As List(Of Entidades.TipoTabla)
        Return DAO.SelectAllTipoTablaxEstado(Estado)
    End Function
    'select TIPO tabla de el Tab Configuracion Tabla por valor
    Public Function SelectAllTipoTablaxLineaSub(ByVal Estado As Integer, ByVal Linea As Integer, ByVal Sublinea As Integer) As List(Of Entidades.TipoTabla)
        Return DAO.SelectAllTipoTablaxLineaSub(Estado, Linea, Sublinea)
    End Function
    Public Function SelectTipoTablaAllActivoxId(ByVal idtipotabla As Integer) As Entidades.TipoTabla
        Return DAO.SelectTipoTablaAllActivoxId(idtipotabla)
    End Function
    Public Function LongitudTablasGeneral(ByVal idtipotabla As Integer, ByVal idlinea As Integer, ByVal idsublinea As Integer) As Entidades.TipoTabla
        Return DAO.LongitudTablasGeneral(idtipotabla, idlinea, idsublinea)
    End Function

    Public Function ValidadNombres(ByVal nomTabla As String, ByVal columna As String, ByVal valor As String) As Entidades.TipoTabla
        Return DAO.ValidadNombres(nomTabla, columna, valor)
    End Function
    Public Function ValidadNombres(ByVal nomTabla As String, ByVal columna As String, ByVal valor As String, ByVal columna2 As String, ByVal valor2 As String) As Entidades.TipoTabla
        Return DAO.ValidadNombres(nomTabla, columna, valor, columna2, valor2)
    End Function
End Class
