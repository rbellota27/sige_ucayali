﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'********************   JUEVES 10 JUNIO 2010 HORA 12_46 PM

Public Class RestriccionComision_Cliente

    Private objDaoRestriccion_Cliente As New DAO.DAORestriccion_Cliente

    Public Function RestriccionComision_Cliente_SelectxParams(ByVal IdComisionCab As Integer) As List(Of Entidades.Restriccion_Cliente)
        Return objDaoRestriccion_Cliente.RestriccionComision_Cliente_SelectxParams(IdComisionCab)
    End Function

End Class
