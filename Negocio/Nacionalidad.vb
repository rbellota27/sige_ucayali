﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Nacionalidad
    Private obj As New DAO.DAONacionalidad
    Public Function InsertaNacionalidad(ByVal nacionalidad As Entidades.Nacionalidad) As Boolean
        Return obj.InsertaNacionalidad(nacionalidad)
    End Function
    Public Function ActualizaNacionalidad(ByVal nacionalidad As Entidades.Nacionalidad) As Boolean
        Return obj.ActualizaNacionalidad(nacionalidad)
    End Function
    Public Function SelectAll() As List(Of Entidades.Nacionalidad)
        Return obj.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Nacionalidad)
        Return obj.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Nacionalidad)
        Return obj.SelectAllInactivo
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Nacionalidad)
        Return obj.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Nacionalidad)
        Return obj.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Nacionalidad)
        Return obj.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Nacionalidad)
        Return obj.SelectxId(id)
    End Function
    Public Function SelectCbo() As List(Of Entidades.Nacionalidad)
        Return obj.SelectCbo
    End Function
End Class
