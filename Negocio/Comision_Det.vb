﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Imports DAO
Public Class Comision_Det

    Dim objDAOComision_Det As New DAO.DAOComision_Det
    Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Dim tr As SqlTransaction = Nothing

    Public Sub Registrar(ByVal ListaComisionDet As List(Of Entidades.Comision_Det))
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            For Each obj As Entidades.Comision_Det In ListaComisionDet
                objDAOComision_Det.Registrar(obj, cn, tr)
            Next

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

    End Sub

    Public Function Comision_SelectProducto_AddDetallexParams(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal Tabla_IdProducto As DataTable, ByVal IdTienda As Integer, ByVal IdComisionCab As Integer, ByVal FiltrarComsion As Boolean, ByVal Descripcion As String, ByVal CodigoProducto As String, Optional ByVal PageIndex As Integer = 0, Optional ByVal PageSize As Integer = 0) As List(Of Entidades.Comision_Det)
        Return objDAOComision_Det.Comision_SelectProducto_AddDetallexParams(IdLinea, IdSubLinea, Tabla_IdProducto, IdTienda, IdComisionCab, FiltrarComsion, Descripcion, CodigoProducto, PageIndex, PageSize)
    End Function


    Public Function CR_DetalleComision(ByVal IdComisionCab As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdVendedor As Integer, ByVal FechaIni As String, ByVal FechaFin As String) As DataSet
        Return objDAOComision_Det.CR_DetalleComision(IdComisionCab, IdEmpresa, IdTienda, IdVendedor, FechaIni, FechaFin)
    End Function


    Public Function CR_ComisionxVendedor(ByVal IdComisionCab As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdVendedor As Integer, ByVal FechaIni As String, ByVal FechaFin As String, ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdProducto As Integer, ByVal Tabla As DataTable) As DataSet
        Return objDAOComision_Det.CR_ComisionxVendedor(IdComisionCab, IdEmpresa, IdTienda, IdVendedor, FechaIni, FechaFin, IdTipoExistencia, IdLinea, IdSubLinea, IdProducto, Tabla)
    End Function

End Class
