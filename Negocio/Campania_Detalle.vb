﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'******************   LUNES 31 MAYO 2010 HORA 03_23 PM

Imports System.Data.SqlClient

Public Class Campania_Detalle

    Private objDaoCampania_Detalle As New DAO.DAOCampania_Detalle
    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private tr As SqlTransaction


    Public Function Campania_Detalle_Select(ByVal IdCampania As Integer, _
                                            ByVal IdTipoExistencia As Integer, _
                                            ByVal IdLinea As Integer, _
                                            ByVal IdSubLinea As Integer, _
                                            ByVal prod_codigo As String, _
                                            ByVal prod_nombre As String, _
                                             ByVal PageNumber As Integer, _
                                            ByVal PageSize As Integer, _
                                            ByVal tabla As DataTable) As List(Of Entidades.Campania_Detalle)
        Return objDaoCampania_Detalle.Campania_Detalle_Select(IdCampania, IdTipoExistencia, IdLinea, IdSubLinea, prod_codigo, prod_nombre, PageNumber, PageSize, tabla)
    End Function
    Public Function Campania_Detalle_Vigente(ByVal IdCampania As Integer, ByVal IdProducto As Integer, ByVal IdUnidadMedida As Integer) As Entidades.Campania_Detalle
        Return objDaoCampania_Detalle.Campania_Detalle_Vigente(IdCampania, IdProducto, IdUnidadMedida)
    End Function

    Public Function Campania_Detalle_Vigente_SelectxParams_DT(ByVal IdCampania As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdProducto As Integer, ByVal IdUnidadMedida As Integer, ByVal Fecha As Date) As DataTable
        Return objDaoCampania_Detalle.Campania_Detalle_Vigente_SelectxParams_DT(IdCampania, IdEmpresa, IdTienda, IdProducto, IdUnidadMedida, Fecha)
    End Function

    Public Function Campania_Detalle_Vigente_SelectxParams_DT2(ByVal IdCampania As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdProducto As Integer, ByVal IdUnidadMedida As Integer, ByVal Fecha As Date) As DataTable
        Return objDaoCampania_Detalle.Campania_Detalle_Vigente_SelectxParams_DT2(IdCampania, IdEmpresa, IdTienda, IdProducto, IdUnidadMedida, Fecha)
    End Function

    Public Function Campania_Detalle_SelectxIdCampania(ByVal IdCampania As Integer) As List(Of Entidades.Campania_Detalle)
        Return objDaoCampania_Detalle.Campania_Detalle_SelectxIdCampania(IdCampania)
    End Function

    Public Function Campania_SelectProducto_AddDetallexParams(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal Tabla_IdProducto As DataTable, ByVal IdTienda As Integer, ByVal IdTipoPV As Integer) As List(Of Entidades.Campania_Detalle)
        Return objDaoCampania_Detalle.Campania_SelectProducto_AddDetallexParams(IdLinea, IdSubLinea, Tabla_IdProducto, IdTienda, IdTipoPV)
    End Function

    Public Function Campania_Detalle_DeletexParams(ByVal IdCampania As Integer, ByVal IdProducto As Integer, ByVal IdUnidadMedida As Integer) As Boolean

        Dim hecho As Boolean = False

        Try

            cn.Open()
            tr = cn.BeginTransaction

            objDaoCampania_Detalle.Campania_Detalle_DeletexParams(IdCampania, IdProducto, IdUnidadMedida, cn, tr)

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function
End Class
