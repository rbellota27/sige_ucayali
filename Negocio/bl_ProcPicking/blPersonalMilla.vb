﻿Imports DAO
Imports Entidades
Imports System.Data.SqlClient
Public Class blPersonalMilla
    Dim cn As SqlConnection

    Public Function GuardarPersonal(ByVal IdPersonal As Integer, ByVal NombrePersonal As Integer) As List(Of bePersonalMilla)
        cn = (New Conexion).ConexionSIGE
        cn.Open()

        Try
            Dim lista As New List(Of Entidades.bePersonalMilla)
            lista = (New DAOListarPersonal).GuardarPersonal(cn, IdPersonal, NombrePersonal)

            Return lista
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
