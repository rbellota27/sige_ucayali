﻿Imports DAO
Imports Entidades
Imports System.Data.SqlClient

Public Class blPersonal

    Dim cn As SqlConnection


    Public Function listarPersonal() As List(Of bePersonal)

        cn = (New Conexion).ConexionSIGE
        cn.Open()

        Try
            Dim lista As New List(Of Entidades.bePersonal)
            lista = (New DAOListarPersonal).listarPersonal(cn)

            Return lista
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function listaPersMilla() As List(Of bePersonalMilla)
        cn = (New Conexion).ConexionSIGE
        cn.Open()

        Try
            Dim lista As New List(Of Entidades.bePersonalMilla)
            lista = (New DAOListarPersonal).listaPersMilla(cn)

            Return lista
        Catch ex As Exception
            Throw ex
        End Try
    End Function





End Class
