﻿Imports DAO
Imports Entidades
Imports System.Data.SqlClient
Public Class blTipoEstanteria
    Dim cn As SqlConnection


    Public Function listarTipoEstanteria() As List(Of beTipoEstanteria)
        cn = (New Conexion).ConexionSIGE
        cn.Open()

        Try
            Dim lista As New List(Of Entidades.beTipoEstanteria)
            lista = (New DAOTipoEstanteria).ListarTipoEstanteria(cn)

            Return lista
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class








