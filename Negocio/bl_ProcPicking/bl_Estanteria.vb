﻿Imports DAO
Imports Entidades
Imports System.Data.SqlClient

Public Class bl_Estanteria

    Dim cn As SqlConnection


    Public Function listarEstanteria() As List(Of be_Estanteria)
        cn = (New Conexion).ConexionSIGE
        cn.Open()

        Try
            Dim lista As New List(Of Entidades.be_Estanteria)
            lista = (New DAOListarEstanteria).listarEstanterias(cn)

            Return lista
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
