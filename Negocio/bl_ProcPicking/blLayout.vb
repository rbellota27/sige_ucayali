﻿

Imports DAO
Imports Entidades
Imports System.Data.SqlClient
Public Class blLayout
    Dim cn As SqlConnection

    Public Function ListarLayout(ByVal IdAlmacen As Integer, ByVal IdZona As Integer) As List(Of beLayout)
        cn = (New Conexion).ConexionSIGE
        cn.Open()

        Try
            Dim lista As New List(Of Entidades.beLayout)
            lista = (New DAOLayout).ListarLayout(cn, IdAlmacen, IdZona)

            Return lista
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class






