﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class UnidadMedida
    Private objDAOUM As New DAO.DAOUnidadMedida
    Public Function InsertaUnidadMedida(ByVal unidadmedida As Entidades.UnidadMedida) As Boolean
        Return objDAOUM.InsertaUnidadMedida(unidadmedida)
    End Function
    Public Function ActualizaUnidadMedida(ByVal unidadmedida As Entidades.UnidadMedida) As Boolean
        Return objDAOUM.ActualizaUnidadMedida(unidadmedida)
    End Function
    Public Function SelectAll() As List(Of Entidades.UnidadMedida)
        Return objDAOUM.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.UnidadMedida)
        Return objDAOUM.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.UnidadMedida)
        Return objDAOUM.SelectAllInactivo
    End Function
    Public Function SelectCbo() As List(Of Entidades.UnidadMedida)
        Return objDAOUM.SelectCbo
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.UnidadMedida)
        Return objDAOUM.SelectAllxNombre(nombre)
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.UnidadMedida)
        Return objDAOUM.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.UnidadMedida)
        Return objDAOUM.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.UnidadMedida)
        Return objDAOUM.SelectxId(id)
    End Function
    Public Function SelectxCodSunat(ByVal cod As String) As List(Of Entidades.UnidadMedida)
        Return objDAOUM.SelectxCodSunat(cod)
    End Function
    Public Function SelectNombreCortoxIdUnidadMedida(ByVal IdUnidadMedida As Integer) As String
        Return objDAOUM.SelectNombreCortoxIdUnidadMedida(IdUnidadMedida)
    End Function
End Class
