﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'LUNES 01 DE FEBRERO
Imports System.Data.SqlClient
Imports System.Data.SqlClient.SqlTransaction
Public Class Linea
    Dim objDAOLinea As New DAO.DAOLinea
    Dim objConexion As DAO.Conexion

    'Public Function InsertaLinea(ByVal linea As Entidades.Linea) As Boolean
    'Return objDAOLinea.InsertaLinea(linea)
    'End Function
    Public Function InsertaLinea(ByVal objLinea As Entidades.Linea, ByVal Lista_Linea_Area As List(Of Entidades.Linea_Area)) As Boolean
        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim IdLinea As Integer = 0
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction
            IdLinea = objDAOLinea.InsertaLinea(objLinea, cn, tr)
            If IdLinea = -1 Then
                Throw New Exception("Fracasó el proceso de Inserción.")
            End If
            'PAIS LINEA
            'Dim objDAOPaisLinea As New DAO.DAOPaisLinea
            'If Not objDAOPaisLinea.GrabaPaisLineaT(IdLinea, cn, listaPaisLinea, tr) Then
            '    Throw New Exception("Fracasó el Proceso de Inserción")
            'End If
            'FABRICANTE LINEA
            'Dim objDAOFabricanteLinea As New DAO.DAOFabricanteLinea
            'If Not objDAOFabricanteLinea.GrabaFabricanteLineaT(IdLinea, cn, listaFabricanteLinea, tr) Then
            '    Throw New Exception("Fracasó el Proceso de Inserción")
            'End If
            'MARCA LINEA
            'Dim objDAOMarcaLinea As New DAO.DAOMarcaLinea
            'If Not objDAOMarcaLinea.GrabaMarcaLineaT(IdLinea, cn, listaMarcaLinea, tr) Then
            '    Throw New Exception("Fracasó el Proceso de Inserción")
            'End If
            'MODELO LINEA
            'Dim objDAOModeloLinea As New DAO.DAOModeloLinea
            'If Not objDAOModeloLinea.GrabaModeloLineaT(IdLinea, cn, listaModeloLinea, tr) Then
            '    Throw New Exception("Fracasó el Proceso de Inserción")
            'End If
            'FORMATO LINEA
            'Dim objDAOFormatoLinea As New DAO.DAOFormatoLinea
            'If Not objDAOFormatoLinea.GrabaFormatoLineaT(IdLinea, cn, listaFormatoLinea, tr) Then
            '    Throw New Exception("Fracasó el Proceso de Inserción")
            'End If
            'PROVEEDOR LINEA
            'Dim objDAOProveedorLinea As New DAO.DAOProveedorLinea
            'If Not objDAOProveedorLinea.GrabaProveedorLineaT(IdLinea, cn, listaProveedorLinea, tr) Then
            '    Throw New Exception("Fracasó el Proceso de Inserción")
            'End If

            'ESTILO LINEA
            'Dim objDAOEstiloLinea As New DAO.DAOEstiloLinea
            'If Not objDAOEstiloLinea.GrabaEstiloLineaT(IdLinea, cn, listaEstiloLinea, tr) Then
            '    Throw New Exception("Fracasó el Proceso de Inserción")
            'End If
            ''TRANSITO LINEA
            'Dim objDAOTransitoLinea As New DAO.DAOTransitoLinea
            'If Not objDAOTransitoLinea.GrabaTransitoLineaT(IdLinea, cn, listaTransitoLinea, tr) Then
            '    Throw New Exception("Fracasó el Proceso de Inserción")
            'End If
            ''CALIDAD LINEA 
            'Dim objDAOCalidadLinea As New DAO.DAOCalidadLinea
            'If Not objDAOCalidadLinea.GrabaCalidadLineaT(IdLinea, cn, listaCalidadLinea, tr) Then
            '    Throw New Exception("Fracasó el Proceso de Inserción")
            'End If
            ''COLOR LINEA
            'Dim objColorLinea As New DAO.DAOColor_Linea
            'If Not objColorLinea.GrabaColorLineaT(IdLinea, cn, listaColorLinea, tr) Then
            '    Throw New Exception("Fracasó el Proceso de Inserción")
            'End If
            'LINEA AREA
            Dim objDAOLineaAerea As New DAO.DAOLinea_Area
            objDAOLineaAerea.InsertLinea_Area(cn, IdLinea, Lista_Linea_Area, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            Throw ex
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
    Public Function ActualizaLinea(ByVal objLinea As Entidades.Linea, ByVal lista_Linea_Area As List(Of Entidades.Linea_Area)) As Boolean
        objConexion = New DAO.Conexion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction
            If Not objDAOLinea.ActualizaLinea(objLinea, cn, tr) Then
                Throw New Exception("Fracasó el proceso de Actualización.")
            End If
            ''PAIS LINEA
            'Dim objDAOPaisLinea As New DAO.DAOPaisLinea
            'If Not objDAOPaisLinea.GrabaPaisLineaT(objLinea.Id, cn, listaPaisLinea, tr) Then
            '    Throw New Exception("Fracasó el proceso de Actualización.")
            'End If
            ''FABRICANTE LINEA
            'Dim objDAOFabricanteLinea As New DAO.DAOFabricanteLinea
            'If Not objDAOFabricanteLinea.GrabaFabricanteLineaT(objLinea.Id, cn, listaFabricanteLinea, tr) Then
            '    Throw New Exception("Fracasó el Proceso de Inserción")
            'End If
            ''MARCA LINEA
            'Dim objDAOMarcaLinea As New DAO.DAOMarcaLinea
            'If Not objDAOMarcaLinea.GrabaMarcaLineaT(objLinea.Id, cn, listaMarcaLinea, tr) Then
            '    Throw New Exception("Fracasó el proceso de Actualización.")
            'End If
            ''MODELO LINEA
            'Dim objDAOModeloLinea As New DAO.DAOModeloLinea
            'If Not objDAOModeloLinea.GrabaModeloLineaT(objLinea.Id, cn, listaModeloLinea, tr) Then
            '    Throw New Exception("Fracasó el proceso de Actualización.")
            'End If
            ''FORMATO LINEA
            'Dim objDAOFormatoLinea As New DAO.DAOFormatoLinea
            'If Not objDAOFormatoLinea.GrabaFormatoLineaT(objLinea.Id, cn, listaFormatoLinea, tr) Then
            '    Throw New Exception("Fracasó el proceso de Actualización.")
            'End If
            ''PROVEEDOR LINEA
            ''Dim objDAOProveedorLinea As New DAO.DAOProveedorLinea
            ''If Not objDAOProveedorLinea.GrabaProveedorLineaT(objLinea.Id, cn, listaProveedorLinea, tr) Then
            ''    Throw New Exception("Fracasó el proceso de Actualización.")
            ''End If
            ''ESTILO LINEA
            'Dim objDAOEstiloLinea As New DAO.DAOEstiloLinea
            'If Not objDAOEstiloLinea.GrabaEstiloLineaT(objLinea.Id, cn, listaEstiloLinea, tr) Then
            '    Throw New Exception("Fracasó el proceso de Actualización.")
            'End If
            ''TRANSITO LINEA
            'Dim objDAOTransitoLinea As New DAO.DAOTransitoLinea
            'If Not objDAOTransitoLinea.GrabaTransitoLineaT(objLinea.Id, cn, listaTransitoLinea, tr) Then
            '    Throw New Exception("Fracasó el proceso de Actualización.")
            'End If
            ''CALIDAD LINEA
            'Dim objDAOCalidadLinea As New DAO.DAOCalidadLinea
            'If Not objDAOCalidadLinea.GrabaCalidadLineaT(objLinea.Id, cn, listaCalidadLinea, tr) Then
            '    Throw New Exception("Fracasó el proceso de Actualización.")
            'End If
            ''COLOR LINEA
            'Dim objColorLinea As New DAO.DAOColor_Linea
            'If Not objColorLinea.GrabaColorLineaT(objLinea.Id, cn, listaColorLinea, tr) Then
            '    Throw New Exception("Fracasó el Proceso de Inserción")
            'End If
            'LINEA AREA
            Dim objDAOLineaArea As New DAO.DAOLinea_Area
            objDAOLineaArea.UpdateLinea_Area(cn, objLinea.Id, lista_Linea_Area, tr)

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
    'Public Function ActualizaLinea(ByVal linea As Entidades.Linea) As Boolean
    '    Return objDAOLinea.ActualizaLinea(linea)
    'End Function
    Public Function SelectAll() As List(Of Entidades.Linea)
        Return objDAOLinea.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Linea)
        Return objDAOLinea.SelectAllActivo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Linea)
        Return objDAOLinea.SelectAllInactivo
    End Function
    Public Function SelectActivoxTipoExistencia(ByVal idTipoEx As Integer) As List(Of Entidades.Linea)
        Return objDAOLinea.SelectActivoxTipoExistencia(idTipoEx)
    End Function

    Public Function SelectxIdLineaxEstado(ByVal idlinea As Integer, ByVal estado As String, _
                                          ByVal idexistencia As Integer) As List(Of Entidades.Linea)
        Return objDAOLinea.SelectxIdLineaxEstado(idlinea, estado, idexistencia)
    End Function

    Public Function SelectCboLineaxIdtipoexistencia(ByVal idexistencia As Integer) As List(Of Entidades.Linea)
        objDAOLinea = New DAO.DAOLinea
        Return objDAOLinea.SelectCboLineaxIdtipoexistencia(idexistencia)
    End Function

    Public Function SelectCboLineaxIdtipoexistencia2(ByVal idexistencia As Integer) As List(Of Entidades.Linea)
        objDAOLinea = New DAO.DAOLinea
        Return objDAOLinea.SelectCboLineaxIdtipoexistencia2(idexistencia)
    End Function

    Public Function SelectFlagActivoxIdLinea(ByVal idlinea As Integer) As Entidades.Linea
        Return objDAOLinea.SelectFlagActivoxIdLinea(idlinea)
    End Function
    Public Function SelectAtributoxIdLineaxEstado(ByVal idlinea As Integer, _
                                          ByVal idexistencia As Integer) As List(Of Entidades.Linea)
        Return objDAOLinea.SelectAtributoxIdLineaxEstado(idlinea, idexistencia)
    End Function
    Public Function SelectValidarIdLineaxProducto(ByVal idlinea As Integer) As Entidades.Linea
        Return objDAOLinea.SelectValidarIdLineaxProducto(idlinea)
    End Function
    Public Function SelectActivoxTipoExistenciaCodigo(ByVal idTipoEx As Integer) As List(Of Entidades.Linea)
        Return objDAOLinea.SelectActivoxTipoExistenciaCodigo(idTipoEx)
    End Function

    Public Function ValidadLongitud() As Entidades.Linea
        Return objDAOLinea.ValidadLongitud()
    End Function

End Class
