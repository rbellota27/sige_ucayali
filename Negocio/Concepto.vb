﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************** LUNES 18 ENERO 2010 HORA 11_51 AM

'********************************************************
'Autor      : Chang Carnero, Edgar
'Módulo     : Concepto
'Sistema    : Sanicenter
'Empresa    : Digrafic SRL
'Modificado : 15-Enero-2010
'Hora       : 01:00:00 pm
'********************************************************
Imports System.Data.SqlClient
Public Class Concepto
    Private objDaoConcepto As New DAO.DAOConcepto
    Private objDaoCpto_TipoDoc As New DAO.DAOConcepto_TipoDocumento
    Private objconexion As New DAO.Conexion
    Private obj As New DAO.DAOConcepto
    'Public Function InsertaConcepto(ByVal concepto As Entidades.Concepto) As Boolean
    '    Return objDaoConcepto.InsertaConcepto(concepto)
    'End Function
    'Public Function ActualizaConcepto(ByVal concepto As Entidades.Concepto) As Boolean
    '    Return objDaoConcepto.ActualizaConcepto(concepto)
    'End Function
    Public Function SelectAll() As List(Of Entidades.Concepto)
        Return objDaoConcepto.SelectAll
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Concepto)
        Return objDaoConcepto.SelectAllActivo
    End Function
    Public Function SelectCbo() As List(Of Entidades.Concepto)
        Return objDaoConcepto.SelectCbo
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Concepto)
        Return objDaoConcepto.SelectAllInactivo
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Concepto)
        Return objDaoConcepto.SelectActivoxNombre(nombre)
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Concepto)
        Return objDaoConcepto.SelectInactivoxNombre(nombre)
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Concepto)
        Return objDaoConcepto.SelectAllxNombre(nombre)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Concepto)
        Return objDaoConcepto.SelectxId(id)
    End Function
    Public Function CargarConceptosxFactura() As List(Of Entidades.Concepto)
        Return objDaoConcepto.CargarConceptosxFactura()
    End Function
    Public Function SelectCboxTipoUso(ByVal TipoUso As Integer) As List(Of Entidades.Concepto)
        Return objDaoConcepto.SelectCboxTipoUso(TipoUso)
    End Function
    Public Function InsertaCpto_TipoDoc(ByVal TipoDoc As Entidades.Concepto, _
                                        ByVal lista As List(Of Entidades.Concepto_TipoDocumento), _
                                        ByVal ListTiendaConcepto As List(Of Entidades.Tienda_Concepto)) As Boolean
        Dim cn As SqlConnection = objconexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction

            '************ Insertar Concepto
            Dim IdConcepto As Integer = obj.InsertaConcepto(TipoDoc, cn, tr)

            '************ Insertar Lista Tipo Documento
            Dim objDaoTipoDocumento_Cpto As New DAO.DAOConcepto_TipoDocumento
            Dim objConcepto As New Entidades.Concepto_TipoDocumento
            objDaoTipoDocumento_Cpto.InsertaListaConcepto_TipoDocumento(cn, tr, lista, IdConcepto)

            '*********** Inserta Lista Tienda_Concepto
            Dim objTienda_Concepto As New DAO.DAOTienda_Concepto
            objTienda_Concepto.InsertTienda_Concepto(IdConcepto, ListTiendaConcepto, cn, tr)

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally

            If cn.State = ConnectionState.Open Then cn.Close()

        End Try
        Return hecho
    End Function
    Public Function ActualizaConcepto_TipoDocumento(ByVal Concepto As Entidades.Concepto, _
                                                    ByVal lista As List(Of Entidades.Concepto_TipoDocumento), _
                                                    ByVal ListTiendaConcepto As List(Of Entidades.Tienda_Concepto)) As Boolean
        Dim cn As SqlConnection = objconexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            '*************   Actualiza Condición de Pago  ********************
            If Not (obj.ActualizaConcepto(Concepto, cn, tr)) Then
                Throw New Exception
            End If

            '*************  Elimina Concepto _ Lista TipoDocumento ************************
            Dim obJDaoTipoDocumento As New DAO.DAOConcepto_TipoDocumento
            If (Not obJDaoTipoDocumento.DeletexCpto_TipoDoc(cn, tr, Concepto.Id)) Then
                Throw New Exception
            End If
            '****************************** Insertar Lista  **************************************
            obJDaoTipoDocumento.InsertaListaConcepto_TipoDocumento(cn, tr, lista, Concepto.Id)

            Dim objTienda_Concepto As New DAO.DAOTienda_Concepto
            '*********************** Elimina Tienda_Concepto *****************************
            objTienda_Concepto.UpdateTienda_Concepto(Concepto.Id, cn, tr)

            '*********************** Insertar Lista Tienda_Concepto **********************************
            objTienda_Concepto.InsertTienda_Concepto(Concepto.Id, ListTiendaConcepto, cn, tr)
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
    Public Function _Concepto_TipoDocumentoEditarxIdConcepto(ByVal IdConcepto As Integer) As List(Of Entidades.Concepto_TipoDocumento)
        Return objDaoCpto_TipoDoc._Concepto_TipoDocumentoEditarxIdConcepto(IdConcepto)
        'Return Nothing
    End Function
    Public Function listarMoneda() As List(Of Entidades.Concepto_TipoDocumento)
        Return objDaoCpto_TipoDoc.listarMoneda()
    End Function
End Class
