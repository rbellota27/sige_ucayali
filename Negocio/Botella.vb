﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



'******************     SABADO 03 JULIO 2010


Imports System.Data.SqlClient

Public Class Botella

    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private tr As SqlTransaction
    Private objDaoBotella As New DAO.DAOBotella

    Public Function Select_CboContenido() As DataTable
        Return objDaoBotella.Select_CboContenido()
    End Function

    Public Function SelectBotellaDisponiblexParams(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdContenido As Integer) As DataTable
        Return objDaoBotella.SelectBotellaDisponiblexParams(IdEmpresa, IdTienda, IdContenido)
    End Function

    Public Function SelectPendientesDevolucionCliente(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdCliente As Integer, ByVal NroBotella As String) As DataTable
        Return objDaoBotella.SelectPendientesDevolucionCliente(IdEmpresa, IdTienda, IdCliente, NroBotella)
    End Function

    Public Function registrarIngresoBotella(ByVal listaBotella As List(Of Entidades.Botella), ByVal listaMovBotella As List(Of Entidades.MovBotella)) As Boolean

        Dim hecho As Boolean = False

        Try

            cn.Open()
            tr = cn.BeginTransaction

            For i As Integer = 0 To listaBotella.Count - 1
                listaBotella(i).IdBotella = objDaoBotella.registrar(listaBotella(i), cn, tr)
            Next

            Dim objDaoMovBotella As New DAO.DAOMovBotella
            For i As Integer = 0 To listaMovBotella.Count - 1

                listaMovBotella(i).IdBotella = listaBotella(i).IdBotella
                objDaoMovBotella.Insert(listaMovBotella(i), cn, tr)

            Next

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function

    Public Function registrarIngresoBotellaxDevolucionCliente(ByVal listaMovBotella As List(Of Entidades.MovBotella)) As Boolean

        Dim hecho As Boolean = False

        Try

            cn.Open()
            tr = cn.BeginTransaction

            Dim objDaoMovBotella As New DAO.DAOMovBotella
            For i As Integer = 0 To listaMovBotella.Count - 1
                objDaoMovBotella.Insert(listaMovBotella(i), cn, tr)
            Next

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function

    Public Function registrarSalida_BotellaDisponible(ByVal listaMovBotella As List(Of Entidades.MovBotella)) As Boolean

        Dim hecho As Boolean = False

        Try

            cn.Open()
            tr = cn.BeginTransaction

            Dim objDaoMovBotella As New DAO.DAOMovBotella
            For i As Integer = 0 To listaMovBotella.Count - 1
                objDaoMovBotella.Insert(listaMovBotella(i), cn, tr)
            Next

            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function
End Class
