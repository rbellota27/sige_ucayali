'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOFabricante
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaFabricante(ByVal fabricante As Entidades.Fabricante) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}

        ArrayParametros(0) = New SqlParameter("@fab_NombreLargo", SqlDbType.VarChar)
        ArrayParametros(0).Value = fabricante.NombreLargo
        ArrayParametros(1) = New SqlParameter("@fab_NombreCorto", SqlDbType.VarChar)
        ArrayParametros(1).Value = fabricante.NombreCorto
        ArrayParametros(2) = New SqlParameter("@fab_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = fabricante.Estado

        ArrayParametros(3) = New SqlParameter("@fab_codigo", SqlDbType.Char, 2)
        ArrayParametros(3).Value = fabricante.fab_codigo

        ArrayParametros(4) = New SqlParameter("@IdPais", SqlDbType.Char, 4)
        ArrayParametros(4).Value = fabricante.IdPais

        Dim cmd As New SqlCommand("_FabricanteInsert", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return True
    End Function
    Public Function ActualizaFabricante(ByVal fabricante As Entidades.Fabricante) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
        ArrayParametros(0) = New SqlParameter("@fab_NombreLargo", SqlDbType.VarChar)
        ArrayParametros(0).Value = fabricante.NombreLargo
        ArrayParametros(1) = New SqlParameter("@fab_NombreCorto", SqlDbType.VarChar)
        ArrayParametros(1).Value = fabricante.NombreCorto
        ArrayParametros(2) = New SqlParameter("@fab_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = fabricante.Estado
        ArrayParametros(3) = New SqlParameter("@fab_codigo", SqlDbType.Char, 2)
        ArrayParametros(3).Value = fabricante.fab_codigo
        ArrayParametros(4) = New SqlParameter("@IdPais", SqlDbType.Char, 4)
        ArrayParametros(4).Value = fabricante.IdPais
        ArrayParametros(5) = New SqlParameter("@IdFabricante", SqlDbType.Int)
        ArrayParametros(5).Value = fabricante.Id
        Return HDAO.Update(cn, "_FabricanteUpdate", ArrayParametros)
    End Function
    Public Function SelectAll() As List(Of Entidades.Fabricante)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_FabricanteSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Fabricante)
                Do While lector.Read
                    Dim objFab As New Entidades.Fabricante
                    objFab.Estado = CStr(lector.Item("fab_Estado"))
                    objFab.Id = CInt(lector.Item("IdFabricante"))
                    objFab.NombreCorto = CStr(IIf(IsDBNull(lector.Item("fab_NombreCorto")) = True, "", lector.Item("fab_NombreCorto")))
                    objFab.NombreLargo = CStr(IIf(IsDBNull(lector.Item("fab_NombreLargo")) = True, "", lector.Item("fab_NombreLargo")))
                    objFab.fab_codigo = CStr(IIf(IsDBNull(lector.Item("fab_codigo")) = True, "", lector.Item("fab_codigo")))
                    'SE AGREGO ENTIDADES DE PAIS
                    objFab.IdPais = CStr(IIf(IsDBNull(lector.Item("IdPais")) = True, "", lector.Item("IdPais")))
                    objFab.NomPais = CStr(IIf(IsDBNull(lector.Item("pa_Nombre")) = True, "", lector.Item("pa_Nombre")))

                    Lista.Add(objfab)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAllActivo() As List(Of Entidades.Fabricante)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_FabricanteSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Fabricante)
                Do While lector.Read
                    Dim objFab As New Entidades.Fabricante
                    objFab.Estado = CStr(lector.Item("fab_Estado"))
                    objFab.Id = CInt(lector.Item("IdFabricante"))
                    objFab.NombreCorto = CStr(IIf(IsDBNull(lector.Item("fab_NombreCorto")) = True, "", lector.Item("fab_NombreCorto")))
                    objFab.NombreLargo = CStr(IIf(IsDBNull(lector.Item("fab_NombreLargo")) = True, "", lector.Item("fab_NombreLargo")))

                    objFab.fab_codigo = CStr(IIf(IsDBNull(lector.Item("fab_codigo")) = True, "", lector.Item("fab_codigo")))

                    'SE AGREGO ENTIDADES DE PAIS
                    objFab.IdPais = CStr(IIf(IsDBNull(lector.Item("IdPais")) = True, "", lector.Item("IdPais")))
                    objFab.NomPais = CStr(IIf(IsDBNull(lector.Item("pa_Nombre")) = True, "", lector.Item("pa_Nombre")))

                    Lista.Add(objFab)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Fabricante)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_FabricanteSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Fabricante)
                Do While lector.Read
                    Dim objFab As New Entidades.Fabricante
                    objFab.Estado = CStr(lector.Item("fab_Estado"))
                    objFab.Id = CInt(lector.Item("IdFabricante"))
                    objFab.NombreCorto = CStr(IIf(IsDBNull(lector.Item("fab_NombreCorto")) = True, "", lector.Item("fab_NombreCorto")))
                    objFab.NombreLargo = CStr(IIf(IsDBNull(lector.Item("fab_NombreLargo")) = True, "", lector.Item("fab_NombreLargo")))
                    objFab.fab_codigo = CStr(IIf(IsDBNull(lector.Item("fab_codigo")) = True, "", lector.Item("fab_codigo")))
                    'SE AGREGO ENTIDADES DE PAIS
                    objFab.IdPais = CStr(IIf(IsDBNull(lector.Item("IdPais")) = True, "", lector.Item("IdPais")))
                    objFab.NomPais = CStr(IIf(IsDBNull(lector.Item("pa_Nombre")) = True, "", lector.Item("pa_Nombre")))

                    Lista.Add(objFab)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Fabricante)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_FabricanteSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Fabricante)
                Do While lector.Read
                    Dim objFab As New Entidades.Fabricante
                    objFab.Estado = CStr(lector.Item("fab_Estado"))
                    objFab.Id = CInt(lector.Item("IdFabricante"))
                    objFab.NombreCorto = CStr(IIf(IsDBNull(lector.Item("fab_NombreCorto")) = True, "", lector.Item("fab_NombreCorto")))
                    objFab.NombreLargo = CStr(IIf(IsDBNull(lector.Item("fab_NombreLargo")) = True, "", lector.Item("fab_NombreLargo")))

                    objFab.fab_codigo = CStr(IIf(IsDBNull(lector.Item("fab_codigo")) = True, "", lector.Item("fab_codigo")))
                    'SE AGREGO ENTIDADES DE PAIS
                    objFab.IdPais = CStr(IIf(IsDBNull(lector.Item("IdPais")) = True, "", lector.Item("IdPais")))
                    objFab.NomPais = CStr(IIf(IsDBNull(lector.Item("pa_Nombre")) = True, "", lector.Item("pa_Nombre")))

                    Lista.Add(objFab)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Fabricante)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_FabricanteSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Fabricante)
                Do While lector.Read
                    Dim objFab As New Entidades.Fabricante
                    objFab.Estado = CStr(lector.Item("fab_Estado"))
                    objFab.Id = CInt(lector.Item("IdFabricante"))
                    objFab.NombreCorto = CStr(IIf(IsDBNull(lector.Item("fab_NombreCorto")) = True, "", lector.Item("fab_NombreCorto")))
                    objFab.NombreLargo = CStr(IIf(IsDBNull(lector.Item("fab_NombreLargo")) = True, "", lector.Item("fab_NombreLargo")))
                    objFab.fab_codigo = CStr(IIf(IsDBNull(lector.Item("fab_codigo")) = True, "", lector.Item("fab_codigo")))
                    'SE AGREGO ENTIDADES DE PAIS
                    objFab.IdPais = CStr(IIf(IsDBNull(lector.Item("IdPais")) = True, "", lector.Item("IdPais")))
                    objFab.NomPais = CStr(IIf(IsDBNull(lector.Item("pa_Nombre")) = True, "", lector.Item("pa_Nombre")))

                    Lista.Add(objFab)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Fabricante)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_FabricanteSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Fabricante)
                Do While lector.Read
                    Dim objFab As New Entidades.Fabricante
                    objFab.Estado = CStr(lector.Item("fab_Estado"))
                    objFab.Id = CInt(lector.Item("IdFabricante"))
                    objFab.NombreCorto = CStr(IIf(IsDBNull(lector.Item("fab_NombreCorto")) = True, "", lector.Item("fab_NombreCorto")))
                    objFab.NombreLargo = CStr(IIf(IsDBNull(lector.Item("fab_NombreLargo")) = True, "", lector.Item("fab_NombreLargo")))
                    objFab.fab_codigo = CStr(IIf(IsDBNull(lector.Item("fab_codigo")) = True, "", lector.Item("fab_codigo")))
                    'SE AGREGO ENTIDADES DE PAIS
                    objFab.IdPais = CStr(IIf(IsDBNull(lector.Item("IdPais")) = True, "", lector.Item("IdPais")))
                    objFab.NomPais = CStr(IIf(IsDBNull(lector.Item("pa_Nombre")) = True, "", lector.Item("pa_Nombre")))

                    Lista.Add(objFab)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Fabricante)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_FabricanteSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Fabricante)
                Do While lector.Read
                    Dim objFab As New Entidades.Fabricante
                    objFab.Estado = CStr(lector.Item("fab_Estado"))
                    objFab.Id = CInt(lector.Item("IdFabricante"))
                    objFab.NombreCorto = CStr(IIf(IsDBNull(lector.Item("fab_NombreCorto")) = True, "", lector.Item("fab_NombreCorto")))
                    objFab.NombreLargo = CStr(IIf(IsDBNull(lector.Item("fab_NombreLargo")) = True, "", lector.Item("fab_NombreLargo")))
                    objFab.fab_codigo = CStr(IIf(IsDBNull(lector.Item("fab_codigo")) = True, "", lector.Item("fab_codigo")))
                    'SE AGREGO ENTIDADES DE PAIS
                    objFab.IdPais = CStr(IIf(IsDBNull(lector.Item("IdPais")) = True, "", lector.Item("IdPais")))
                    objFab.NomPais = CStr(IIf(IsDBNull(lector.Item("pa_Nombre")) = True, "", lector.Item("pa_Nombre")))

                    Lista.Add(objFab)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    'Public Function SelectAllActivoxIdPais(ByVal idpais As String) As List(Of Entidades.Fabricante)
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim cmd As New SqlCommand("_FabricanteSelectAllActivoxIdPais", cn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.AddWithValue("@IdPais", idpais)
    '    Dim lector As SqlDataReader
    '    Try
    '        Using cn
    '            cn.Open()
    '            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
    '            Dim Lista As New List(Of Entidades.Fabricante)
    '            Do While lector.Read
    '                Dim objFab As New Entidades.Fabricante
    '                objFab.Estado = CStr(lector.Item("fab_Estado"))
    '                objFab.Id = CInt(lector.Item("IdFabricante"))
    '                objFab.NombreCorto = CStr(IIf(IsDBNull(lector.Item("fab_NombreCorto")) = True, "", lector.Item("fab_NombreCorto")))
    '                objFab.NombreLargo = CStr(IIf(IsDBNull(lector.Item("fab_NombreLargo")) = True, "", lector.Item("fab_NombreLargo")))
    '                'objFab.IdPais = CStr(IIf(IsDBNull(lector.Item("IdPais")) = True, "", lector.Item("IdPais")))
    '                Lista.Add(objFab)
    '            Loop
    '            lector.Close()
    '            Return Lista
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
End Class
