'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOTipoTienda
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaTipoTienda(ByVal tipotienda As Entidades.TipoTienda) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@tipt_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = tipotienda.Nombre
        ArrayParametros(1) = New SqlParameter("@tipt_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = tipotienda.Estado
        Return HDAO.Insert(cn, "_TipoTiendaInsert", ArrayParametros)
    End Function
    Public Function ActualizaTipoTienda(ByVal tipotienda As Entidades.TipoTienda) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@tipt_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = tipotienda.Nombre
        ArrayParametros(1) = New SqlParameter("@tipt_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = tipotienda.Estado
        ArrayParametros(2) = New SqlParameter("@IdTipoTienda", SqlDbType.Int)
        ArrayParametros(2).Value = tipotienda.Id
        Return HDAO.Update(cn, "_TipoTiendaUpdate", ArrayParametros)
    End Function
    Public Function SelectAll() As List(Of Entidades.TipoTienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTiendaSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoTienda)
                Do While lector.Read
                    Dim obj As New Entidades.TipoTienda
                    obj.Estado = CStr(lector.Item("tipt_Estado"))
                    obj.Nombre = CStr(lector.Item("tipt_Nombre"))
                    obj.Id = CInt(lector.Item("IdTipoTienda"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.TipoTienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTiendaSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoTienda)
                Do While lector.Read
                    Dim obj As New Entidades.TipoTienda
                    obj.Estado = CStr(lector.Item("tipt_Estado"))
                    obj.Nombre = CStr(lector.Item("tipt_Nombre"))
                    obj.Id = CInt(lector.Item("IdTipoTienda"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.TipoTienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTiendaSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoTienda)
                Do While lector.Read
                    Dim obj As New Entidades.TipoTienda
                    obj.Estado = CStr(lector.Item("tipt_Estado"))
                    obj.Nombre = CStr(lector.Item("tipt_Nombre"))
                    obj.Id = CInt(lector.Item("IdTipoTienda"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.TipoTienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTiendaSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoTienda)
                Do While lector.Read
                    Dim obj As New Entidades.TipoTienda
                    obj.Estado = CStr(lector.Item("tipt_Estado"))
                    obj.Nombre = CStr(lector.Item("tipt_Nombre"))
                    obj.Id = CInt(lector.Item("IdTipoTienda"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoTienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTiendaSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoTienda)
                Do While lector.Read
                    Dim obj As New Entidades.TipoTienda
                    obj.Estado = CStr(lector.Item("tipt_Estado"))
                    obj.Nombre = CStr(lector.Item("tipt_Nombre"))
                    obj.Id = CInt(lector.Item("IdTipoTienda"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoTienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTiendaSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoTienda)
                Do While lector.Read
                    Dim obj As New Entidades.TipoTienda
                    obj.Estado = CStr(lector.Item("tipt_Estado"))
                    obj.Nombre = CStr(lector.Item("tipt_Nombre"))
                    obj.Id = CInt(lector.Item("IdTipoTienda"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.TipoTienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTiendaSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoTienda)
                Do While lector.Read
                    Dim obj As New Entidades.TipoTienda
                    obj.Estado = CStr(lector.Item("tipt_Estado"))
                    obj.Nombre = CStr(lector.Item("tipt_Nombre"))
                    obj.Id = CInt(lector.Item("IdTipoTienda"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class