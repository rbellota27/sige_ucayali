﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'********************************************************
'Autor      : Chang Carnero, Edgar
'Módulo     : Concepto
'Sistema    : Sanicenter
'Empresa    : Digrafic SRL
'Modificado : 19-Enero-2010
'Hora       : 01:00:00 pm
'********************************************************
Imports System.Data.SqlClient
Public Class DAOPerfilTipoPV
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New DAO.Conexion
    Public Sub InsertaListaPerfilTipoPV(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal lista As List(Of Entidades.PerfilTipoPV), ByVal IdPerfil As Integer)
        Dim fila As New Entidades.PerfilTipoPV
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@IdPerfil", SqlDbType.Int)
        ArrayParametros(0).Value = IdPerfil
        'For Each ----> Para Cada Uno
        For Each fila In lista
            ArrayParametros(1) = New SqlParameter("@IdTipoPV", SqlDbType.Int)
            ArrayParametros(1).Value = fila.IdTipoPV
            ArrayParametros(2) = New SqlParameter("@PerPV_Estado", SqlDbType.Char)
            ArrayParametros(2).Value = IIf(fila.VerEstado = True, "1", "0")
            HDAO.InsertaT(cn, "_PerfilTipoPVInsert", ArrayParametros, tr)
        Next
    End Sub
    'Para EDITAR Perfil - PerfilTipoPV(Lista)
    Public Function _PerfilTipoPVEditarxIdPerfil(ByVal IdPerfil As Integer) As List(Of Entidades.PerfilTipoPV)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PerfilTipoPVSelectxIdPerfil", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("IdPerfil", IdPerfil)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.PerfilTipoPV)
                Do While lector.Read
                    Dim obj As New Entidades.PerfilTipoPV
                    obj.IdPerfil = CInt(lector.Item("IdPerfil"))
                    obj.IdTipoPV = CInt(lector.Item("IdTipoPV"))
                    obj.NombreTipoPV = CStr(lector.Item("pv_Nombre"))
                    obj.PerPV_Estado = CChar(IIf(IsDBNull(lector.Item("perpv_estado")) = True, "1", lector("perpv_estado")))
                    lista.Add(obj)
                Loop
                lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw New Exception
        End Try
    End Function
    Public Function DeletexPerfilTipoPVBorrar(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal IdPerfil As Integer) As Boolean
        Try
            Dim cmd As New SqlCommand("_PerfilTipoPVBorrar", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdPerfil", IdPerfil)
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
  
End Class