﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOTienda_Concepto

    Dim objConexion As New DAO.Conexion

    Public Function InsertTienda_Concepto(ByVal idConcepto As Integer, _
                                          ByVal Lista As List(Of Entidades.Tienda_Concepto), _
                                          ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Try
            For Each obj As Entidades.Tienda_Concepto In Lista
                Dim parametros() As SqlParameter = New SqlParameter(4) {}
                parametros(0) = New SqlParameter("@IdConcepto", SqlDbType.Int)
                parametros(0).Value = idConcepto
                parametros(1) = New SqlParameter("@IdTienda", SqlDbType.Int)
                parametros(1).Value = IIf(obj.IdTienda = Nothing, DBNull.Value, obj.IdTienda)
                parametros(2) = New SqlParameter("@IdMoneda", SqlDbType.Int)
                parametros(2).Value = IIf(obj.IdMoneda = Nothing, DBNull.Value, obj.IdMoneda)
                parametros(3) = New SqlParameter("@tie_monto", SqlDbType.Int)
                parametros(3).Value = IIf(obj.tie_monto = Nothing, DBNull.Value, obj.tie_monto)
                parametros(4) = New SqlParameter("@tie_estado", SqlDbType.Int)
                parametros(4).Value = IIf(obj.tie_estado = Nothing, DBNull.Value, obj.tie_estado)
                Dim cmd As New SqlCommand("_InsertTienda_Concepto", cn, tr)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(parametros)
                Try
                    cmd.ExecuteNonQuery()
                Catch ex As Exception
                Finally

                End Try

            Next
        Catch ex As Exception
            Throw ex
        End Try
        Return True
    End Function

    Public Function UpdateTienda_Concepto(ByVal idconcepto As Integer, _
                                          ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Try
            Dim cmd As New SqlCommand("_UpdateTienda_Concepto", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idconcepto", idconcepto)
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return True
    End Function


    Public Function SelectTienda_Concepto(ByVal idConcepto As Integer) As List(Of Entidades.Tienda_Concepto)
        Dim Lista As New List(Of Entidades.Tienda_Concepto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim lector As SqlDataReader
        Try
            cn.Open()
            Dim cmd As New SqlCommand("_SelectTienda_Concepto", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdConcepto", idConcepto)
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.Tienda_Concepto
                With obj
                    .IdTienda = CInt(lector("IdTienda"))
                    .IdConcepto = CInt(lector("IdConcepto"))
                    .IdMoneda = CInt(lector("IdMoneda"))
                    .tie_estado = CBool(IIf(IsDBNull(lector("tie_estado")), False, lector("tie_estado")))
                    .tie_monto = CDec(IIf(IsDBNull(lector("tie_monto")), False, lector("tie_monto")))
                    .cadMoneda = CStr(IIf(IsDBNull(lector("cadMoneda")), False, lector("cadMoneda")))
                    .cadTienda = CStr(IIf(IsDBNull(lector("cadTienda")), False, lector("cadTienda")))
                End With
                Lista.Add(obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return Lista
    End Function

End Class
