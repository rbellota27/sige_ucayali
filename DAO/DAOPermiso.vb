﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Chang Carnero, Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 09-Noviembre-2009
'Hora    : 01:00 pm
'*************************************************
Imports System.Data.SqlClient
Public Class DAOPermiso
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion


    Public Function SelectxIdAreaxIdPerfil(ByVal IdArea As Integer, ByVal IdPerfil As Integer) As List(Of Entidades.Perfil_Permiso)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PermisoSelectxIdAreaxIdPerfil", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdArea", IdArea)
        cmd.Parameters.AddWithValue("@IdPerfil", IdPerfil)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Perfil_Permiso)
                Do While lector.Read
                    Dim obj As New Entidades.Perfil_Permiso

                    obj.IdPermiso = CInt(IIf(IsDBNull(lector("IdPermiso")) = True, 0, lector("IdPermiso")))
                    obj.Estado = CBool(IIf(IsDBNull(lector("Cont")) = True, 0, lector("Cont")))
                    obj.DescPermiso = CStr(IIf(IsDBNull(lector("perm_Descripcion")) = True, 0, lector("perm_Descripcion")))


                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function





    Public Function SelectCboArea() As List(Of Entidades.Area)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PermisoSelectCboArea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Area)
                Do While lector.Read
                    Dim obj As New Entidades.Area
                    obj.Id = CInt(IIf(IsDBNull(lector("IdArea")) = True, 0, lector("IdArea")))
                    obj.DescripcionCorta = CStr(IIf(IsDBNull(lector("ar_NombreCorto")) = True, "", lector("ar_NombreCorto")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function







    Public Function PermisoSelectxNombrePermiso(ByVal IdPerfil As Integer) As List(Of Entidades.Perfil)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PermisoSelectxNombrePermiso", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPerfil", IdPerfil)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Perfil)
                Do While lector.Read
                    Dim objPermiso As New Entidades.Perfil
                    objPermiso.IdPermiso = CInt(IIf(IsDBNull(lector.Item("IdPermiso")) = True, 0, lector.Item("IdPermiso")))
                    objPermiso.perm_Descripcion = CStr(IIf(IsDBNull(lector.Item("perm_Descripcion")) = True, "---", lector.Item("perm_Descripcion")))
                    objPermiso.Estado = CStr(IIf(IsDBNull(lector.Item("Estado")) = True, "", lector.Item("Estado")))
                    Lista.Add(objPermiso)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
