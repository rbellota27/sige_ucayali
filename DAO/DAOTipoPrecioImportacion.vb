﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOTipoPrecioImportacion
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

    Public Function Insert(ByVal obj As Entidades.TipoPrecioImportacion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim Prm() As SqlParameter = New SqlParameter(2) {}
        Prm(0) = New SqlParameter("@Nombre_TPImp", SqlDbType.VarChar)
        Prm(0).Value = obj.Nombre
        Prm(1) = New SqlParameter("@Nombre_CortoTPImp", SqlDbType.VarChar)
        Prm(1).Value = obj.NomCorto
        Prm(2) = New SqlParameter("@Estado_TPImp", SqlDbType.Bit)
        Prm(2).Value = obj.Estado
        Return HDAO.Insert(cn, "_TipoPrecioImportacionInsert", Prm)
    End Function
    Public Function Update(ByVal obj As Entidades.TipoPrecioImportacion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim Prm() As SqlParameter = New SqlParameter(3) {}
        Prm(0) = New SqlParameter("@Nombre_TPImp", SqlDbType.VarChar)
        Prm(0).Value = obj.Nombre
        Prm(1) = New SqlParameter("@Nombre_CortoTPImp", SqlDbType.VarChar)
        Prm(1).Value = obj.NomCorto
        Prm(2) = New SqlParameter("@Estado_TPImp", SqlDbType.Bit)
        Prm(2).Value = obj.Estado
        Prm(3) = New SqlParameter("@IdTPImp", SqlDbType.Int)
        Prm(3).Value = obj.Id
        Return HDAO.Update(cn, "_TipoPrecioImportacionUpdate", Prm)
    End Function

    Public Function SelectAll() As List(Of Entidades.TipoPrecioImportacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoPrecioImportacionSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoPrecioImportacion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoPrecioImportacion
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTPImp")) = True, 0, lector.Item("IdTPImp")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre_TPImp")) = True, "", lector.Item("Nombre_TPImp")))
                    obj.NomCorto = CStr(IIf(IsDBNull(lector.Item("Nombre_CortoTPImp")) = True, "", lector.Item("Nombre_CortoTPImp")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("Estado_TPImp")) = True, 0, lector.Item("Estado_TPImp")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.TipoPrecioImportacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoPrecioImportacionSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoPrecioImportacion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoPrecioImportacion
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTPImp")) = True, 0, lector.Item("IdTPImp")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre_TPImp")) = True, "", lector.Item("Nombre_TPImp")))
                    obj.NomCorto = CStr(IIf(IsDBNull(lector.Item("Nombre_CortoTPImp")) = True, "", lector.Item("Nombre_CortoTPImp")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("Estado_TPImp")) = True, 0, lector.Item("Estado_TPImp")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.TipoPrecioImportacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoPrecioImportacionSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoPrecioImportacion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoPrecioImportacion
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTPImp")) = True, 0, lector.Item("IdTPImp")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre_TPImp")) = True, "", lector.Item("Nombre_TPImp")))
                    obj.NomCorto = CStr(IIf(IsDBNull(lector.Item("Nombre_CortoTPImp")) = True, "", lector.Item("Nombre_CortoTPImp")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("Estado_TPImp")) = True, 0, lector.Item("Estado_TPImp")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.TipoPrecioImportacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoPrecioImportacionSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoPrecioImportacion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoPrecioImportacion
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTPImp")) = True, 0, lector.Item("IdTPImp")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre_TPImp")) = True, "", lector.Item("Nombre_TPImp")))
                    obj.NomCorto = CStr(IIf(IsDBNull(lector.Item("Nombre_CortoTPImp")) = True, "", lector.Item("Nombre_CortoTPImp")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("Estado_TPImp")) = True, 0, lector.Item("Estado_TPImp")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoPrecioImportacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoPrecioImportacionSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoPrecioImportacion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoPrecioImportacion
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTPImp")) = True, 0, lector.Item("IdTPImp")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre_TPImp")) = True, "", lector.Item("Nombre_TPImp")))
                    obj.NomCorto = CStr(IIf(IsDBNull(lector.Item("Nombre_CortoTPImp")) = True, "", lector.Item("Nombre_CortoTPImp")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("Estado_TPImp")) = True, 0, lector.Item("Estado_TPImp")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoPrecioImportacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoPrecioImportacionSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoPrecioImportacion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoPrecioImportacion
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTPImp")) = True, 0, lector.Item("IdTPImp")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre_TPImp")) = True, "", lector.Item("Nombre_TPImp")))
                    obj.NomCorto = CStr(IIf(IsDBNull(lector.Item("Nombre_CortoTPImp")) = True, "", lector.Item("Nombre_CortoTPImp")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("Estado_TPImp")) = True, 0, lector.Item("Estado_TPImp")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal Id As Integer) As List(Of Entidades.TipoPrecioImportacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoPrecioImportacionSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTPImp", Id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoPrecioImportacion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoPrecioImportacion
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTPImp")) = True, 0, lector.Item("IdTPImp")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre_TPImp")) = True, "", lector.Item("Nombre_TPImp")))
                    obj.NomCorto = CStr(IIf(IsDBNull(lector.Item("Nombre_CortoTPImp")) = True, "", lector.Item("Nombre_CortoTPImp")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("Estado_TPImp")) = True, 0, lector.Item("Estado_TPImp")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCbo() As List(Of Entidades.TipoPrecioImportacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoPrecioImportacionSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoPrecioImportacion)
                Do While lector.Read
                    Dim obj As New Entidades.TipoPrecioImportacion

                    obj.Id = CInt(lector.Item("Id"))
                    obj.Nombre = CStr(lector.Item("NombreCorto"))

                    Lista.Add(obj)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

End Class
