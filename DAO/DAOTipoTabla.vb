﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOTipoTabla
    Dim Helper As New HelperDAO
    Dim objConexion As New Conexion

    Public Function TipoTablaSelectAllxEstado_Paginado(ByVal Descripcion As String, _
                            ByVal Estado As Integer, ByVal PageIndex As Integer, _
                            ByVal PageSize As Integer) As List(Of Entidades.TipoTabla)

        Descripcion = Descripcion.Replace("*", "%")

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTablaSelectAllxEstado_Paginado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@tt_nombre", Descripcion)
        cmd.Parameters.AddWithValue("@tt_Estado", Estado)
        cmd.Parameters.AddWithValue("@pageIndex", PageIndex)
        cmd.Parameters.AddWithValue("@pageSize", PageSize)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoTabla)
                Do While lector.Read
                    Dim obj As New Entidades.TipoTabla

                    obj.IdTipoTabla = CInt(IIf(IsDBNull(lector.Item("IdTipoTabla")), 0, lector.Item("IdTipoTabla")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("tt_Nombre")), "", lector.Item("tt_Nombre")))
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("tt_Abv")) = True, "", lector.Item("tt_Abv")))
                    obj.Longitud = CInt(IIf(IsDBNull(lector.Item("tt_Longitud")) = True, 0, lector.Item("tt_Longitud")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("tt_Estado")) = True, 0, lector.Item("tt_Estado")))

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function InsertaTipoTabla(ByVal obj As Entidades.TipoTabla) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@tt_Nombre", SqlDbType.VarChar, 150)
        ArrayParametros(0).Value = obj.Nombre
        ArrayParametros(1) = New SqlParameter("@tt_Abv", SqlDbType.VarChar, 150)
        ArrayParametros(1).Value = obj.Abv
        ArrayParametros(2) = New SqlParameter("@tt_Longitud", SqlDbType.TinyInt)
        ArrayParametros(2).Value = obj.Longitud
        ArrayParametros(3) = New SqlParameter("@tt_Estado", SqlDbType.VarChar, 50)
        ArrayParametros(3).Value = obj.Estado
        Dim cmd As New SqlCommand("_TipoTabla_Insert", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return True
    End Function
    Public Function ActualizarTipoTabla(ByVal obj As Entidades.TipoTabla) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@IdTipoTabla", SqlDbType.VarChar, 50)
        ArrayParametros(0).Value = obj.IdTipoTabla
        ArrayParametros(1) = New SqlParameter("@tt_Nombre", SqlDbType.VarChar, 150)
        ArrayParametros(1).Value = obj.Nombre
        ArrayParametros(2) = New SqlParameter("@tt_Abv", SqlDbType.VarChar, 150)
        ArrayParametros(2).Value = obj.Abv
        ArrayParametros(3) = New SqlParameter("@tt_Longitud", SqlDbType.TinyInt)
        ArrayParametros(3).Value = obj.Longitud
        ArrayParametros(4) = New SqlParameter("@tt_Estado", SqlDbType.VarChar, 50)
        ArrayParametros(4).Value = obj.Estado
        Dim cmd As New SqlCommand("_TipoTabla_Update", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return True
    End Function
    Public Function SelectAllTipoTablaxEstado(ByVal Estado As Integer) As List(Of Entidades.TipoTabla)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTablaSelectAllxEstado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@tt_Estado", Estado)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoTabla)
                Do While lector.Read
                    Dim obj As New Entidades.TipoTabla
                    obj.IdTipoTabla = CInt(IIf(IsDBNull(lector.Item("IdTipoTabla")), 0, lector.Item("IdTipoTabla")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("tt_Nombre")), "", lector.Item("tt_Nombre")))
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("tt_Abv")) = True, "", lector.Item("tt_Abv")))
                    obj.Longitud = CInt(IIf(IsDBNull(lector.Item("tt_Longitud")) = True, 0, lector.Item("tt_Longitud")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("tt_Estado")) = True, 0, lector.Item("tt_Estado")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    'CONFIGURACION TABLA POR VALOR  TIPO TABLA select POR Linea  y SubLinea
    Public Function SelectAllTipoTablaxLineaSub(ByVal Estado As Integer, ByVal Linea As Integer, ByVal Sublinea As Integer) As List(Of Entidades.TipoTabla)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SelectAllTipoTablaxLineaSub", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Estado", Estado)
        cmd.Parameters.AddWithValue("@Linea", Linea)
        cmd.Parameters.AddWithValue("@Sublinea", Sublinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoTabla)
                Do While lector.Read
                    Dim obj As New Entidades.TipoTabla
                    obj.IdTipoTabla = CInt(IIf(IsDBNull(lector.Item("IdTipoTabla")), 0, lector.Item("IdTipoTabla")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("tt_Nombre")), "", lector.Item("tt_Nombre")))
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("tt_Abv")) = True, "", lector.Item("tt_Abv")))
                    obj.Longitud = CInt(IIf(IsDBNull(lector.Item("tt_Longitud")) = True, 0, lector.Item("tt_Longitud")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("tt_Estado")) = True, 0, lector.Item("tt_Estado")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectTipoTablaAllActivoxId(ByVal idtipotabla As Integer) As Entidades.TipoTabla
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTablaSelectActivoLongitudxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoTabla", idtipotabla)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim obj As Entidades.TipoTabla = Nothing
                If lector.Read Then
                    obj = New Entidades.TipoTabla
                    obj.IdTipoTabla = CInt(IIf(IsDBNull(lector.Item("IdTipoTabla")) = True, 0, lector.Item("IdTipoTabla")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("tt_Nombre")) = True, "", lector.Item("tt_Nombre")))
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("tt_Abv")) = True, "", lector.Item("tt_Abv")))
                    obj.Longitud = CInt(IIf(IsDBNull(lector.Item("tt_Longitud")) = True, 0, lector.Item("tt_Longitud")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("tt_Estado")) = True, 0, lector.Item("tt_Estado")))
                End If
                lector.Close()
                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function LongitudTablasGeneral(ByVal idtipotabla As Integer, ByVal idlinea As Integer, ByVal idsublinea As Integer) As Entidades.TipoTabla
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("Longitud_Tablas", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoTabla", idtipotabla)
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim obj As Entidades.TipoTabla = Nothing
                If lector.Read Then
                    obj = New Entidades.TipoTabla
                    obj.Longitud = CInt(IIf(IsDBNull(lector.Item("Longitud")) = True, 0, lector.Item("Longitud")))
                End If
                lector.Close()
                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function ValidadNombres(ByVal nomTabla As String, ByVal columna As String, ByVal valor As String) As Entidades.TipoTabla
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTabla_Validacion", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Tabla", nomTabla)
        cmd.Parameters.AddWithValue("@Columna", columna)
        cmd.Parameters.AddWithValue("@Valor", valor)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim obj As Entidades.TipoTabla = Nothing
                If lector.Read Then
                    obj = New Entidades.TipoTabla
                    obj.Item = CInt(IIf(IsDBNull(lector.Item("Item")) = True, 0, lector.Item("Item")))
                End If
                lector.Close()
                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function ValidadNombres(ByVal nomTabla As String, ByVal columna As String, ByVal valor As String, ByVal columna2 As String, ByVal valor2 As String) As Entidades.TipoTabla
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTabla_ValidacionxTabla", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Tabla", nomTabla)
        cmd.Parameters.AddWithValue("@Columna", columna)
        cmd.Parameters.AddWithValue("@Valor", valor)
        cmd.Parameters.AddWithValue("@Columna2", columna2)
        cmd.Parameters.AddWithValue("@Valor2", valor2)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim obj As Entidades.TipoTabla = Nothing
                If lector.Read Then
                    obj = New Entidades.TipoTabla
                    obj.Item = CInt(IIf(IsDBNull(lector.Item("Item")) = True, 0, lector.Item("Item")))
                End If
                lector.Close()
                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
