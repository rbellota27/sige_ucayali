'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

' Martes, 25 de Enero de 2011


Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAODetalleDocumento
    Inherits DAO.DAOMantenedor

    Private HDAO As New DAO.HelperDAO
    Private reader As SqlDataReader
    Private objDaoMantenedor As New DAO.DAOMantenedor


    Public Function fn_TotalPag_DetalleDocumento(ByVal IdDocumento As Integer, ByVal PageSize As Integer) As Integer


        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim TotalPagina As Integer = 0

        Try

            cn.Open()

            TotalPagina = CInt(SqlHelper.ExecuteScalar(cn, CommandType.Text, " select [dbo].[fn_TotalPag_DetalleDocumento](" + CStr(IdDocumento) + "," + CStr(PageSize) + ") "))


        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return TotalPagina

    End Function



    Public Function SelectxIdFact_NC(ByVal IdFactura As Integer, ByVal IdGuiaRecepcion As Integer) As List(Of Entidades.DetalleDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DetalleDocumentoSelectxIdFact_NC", cn)
        cmd.CommandTimeout = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdFactura", IdFactura)
        cmd.Parameters.AddWithValue("@IdGuiaRecepcion", IdGuiaRecepcion)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.DetalleDocumento
                    With obj
                        .Cantidad = CDec(IIf(IsDBNull(lector.Item("dc_Cantidad")) = True, 0, lector.Item("dc_Cantidad")))
                        .CantxAtender = CDec(IIf(IsDBNull(lector.Item("dc_CantxAtender")) = True, 0, lector.Item("dc_CantxAtender")))
                        .Descuento = CDec(IIf(IsDBNull(lector.Item("dc_Descuento")) = True, 0, lector.Item("dc_Descuento")))
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .Importe = CDec(IIf(IsDBNull(lector.Item("dc_Importe")) = True, 0, lector.Item("dc_Importe")))
                        .PrecioCD = CDec(IIf(IsDBNull(lector.Item("dc_PrecioCD")) = True, 0, lector.Item("dc_PrecioCD")))
                        .PrecioSD = CDec(IIf(IsDBNull(lector.Item("dc_PrecioSD")) = True, 0, lector.Item("dc_PrecioSD")))
                        .TasaDetraccion = CDec(IIf(IsDBNull(lector.Item("dc_TasaDetraccion")) = True, 0, lector.Item("dc_TasaDetraccion")))
                        .TasaPercepcion = CDec(IIf(IsDBNull(lector.Item("dc_TasaPercepcion")) = True, 0, lector.Item("dc_TasaPercepcion")))
                        .UMedida = CStr(IIf(IsDBNull(lector.Item("dc_UMedida")) = True, "", lector.Item("dc_UMedida")))
                        .Utilidad = CDec(IIf(IsDBNull(lector.Item("dc_Utilidad")) = True, 0, lector.Item("dc_Utilidad")))
                        .NomProducto = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                        .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")) = True, "", lector.Item("prod_Codigo")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .IdUnidadMedidaPrincipal = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedidaPrincipal")) = True, 0, lector.Item("IdUnidadMedidaPrincipal")))
                        .IdDetalleDocumento = CInt(IIf(IsDBNull(lector.Item("IdDetalleDocumento")) = True, 0, lector.Item("IdDetalleDocumento")))
                        .IdDetalleAfecto = CInt(IIf(IsDBNull(lector.Item("IdDetalleAfecto")) = True, 0, lector.Item("IdDetalleAfecto")))
                        '.IdDocumentoRef = CInt(IIf(IsDBNull(lector.Item("IdDocumentoRef")) = True, 0, lector.Item("IdDocumentoRef")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        '.CantidadDetalleAfecto = CDec(IIf(IsDBNull(lector.Item("CantidadDetalleAfecto")) = True, 0, lector.Item("CantidadDetalleAfecto")))
                        ''AGREGAR   '.ProdNombreKit = CStr(IIf(IsDBNull(lector.Item("codcompuesto")) = True, "", lector.Item("codcompuesto")))
                        .CantidadTransito = UCDec(lector("dc_CantidadTransito"))
                        .Moneda = UCStr(lector("Moneda"))

                        .Peso = objDaoMantenedor.UCDec(lector("dc_Peso"))
                        .UmPeso = objDaoMantenedor.UCStr(lector("dc_UmPeso"))
                        .IdUnidadMedida_Peso = objDaoMantenedor.UCInt(lector("IdUnidadMedida_Peso"))

                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Sub DeletexIdDetalleDocumento(ByVal IdDetalleDocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(0) {}
        p(0) = objDaoMantenedor.getParam(IdDetalleDocumento, "@IdDetalleDocumento", SqlDbType.Int)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DetalleDocumento_DeletexIdDetalleDocumento", p)

    End Sub

    Public Function Registrar(ByVal objDetalleDocumento As Entidades.DetalleDocumento, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer

        Dim p() As SqlParameter = New SqlParameter(18) {}
        p(0) = objDaoMantenedor.getParam(objDetalleDocumento.IdDetalleDocumento, "@IdDetalleDocumento", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(objDetalleDocumento.IdDocumento, "@IdDocumento", SqlDbType.Int)
        p(2) = objDaoMantenedor.getParam(objDetalleDocumento.IdProducto, "@IdProducto", SqlDbType.Int)
        p(3) = objDaoMantenedor.getParam(objDetalleDocumento.IdUnidadMedida, "@IdUnidadMedida", SqlDbType.Int)
        p(4) = objDaoMantenedor.getParam(objDetalleDocumento.Cantidad, "@dc_Cantidad", SqlDbType.Decimal)
        p(5) = objDaoMantenedor.getParam(objDetalleDocumento.CantxAtender, "@dc_CantxAtender", SqlDbType.Decimal)
        p(6) = objDaoMantenedor.getParam(objDetalleDocumento.UMedida, "@dc_UMedida", SqlDbType.VarChar)
        p(7) = objDaoMantenedor.getParam(objDetalleDocumento.PrecioSD, "@dc_PrecioSD", SqlDbType.Decimal)
        p(8) = objDaoMantenedor.getParam(objDetalleDocumento.Descuento, "@dc_Descuento", SqlDbType.Decimal)
        p(9) = objDaoMantenedor.getParam(objDetalleDocumento.PrecioCD, "@dc_PrecioCD", SqlDbType.Decimal)
        p(10) = objDaoMantenedor.getParam(objDetalleDocumento.Importe, "@dc_Importe", SqlDbType.Decimal)
        p(11) = objDaoMantenedor.getParam(objDetalleDocumento.Utilidad, "@dc_Utilidad", SqlDbType.Decimal)
        p(12) = objDaoMantenedor.getParam(objDetalleDocumento.TasaPercepcion, "@dc_TasaPercepcion", SqlDbType.Decimal)
        p(13) = objDaoMantenedor.getParam(objDetalleDocumento.TasaDetraccion, "@dc_TasaDetraccion", SqlDbType.Decimal)
        p(14) = objDaoMantenedor.getParam(objDetalleDocumento.Peso, "@dc_Peso", SqlDbType.Decimal)
        p(15) = objDaoMantenedor.getParam(objDetalleDocumento.UmPeso, "@dc_UmPeso", SqlDbType.VarChar)
        p(16) = objDaoMantenedor.getParam(objDetalleDocumento.IdDetalleAfecto, "@IdDetalleAfecto", SqlDbType.Int)
        p(17) = objDaoMantenedor.getParam(objDetalleDocumento.DetalleGlosa, "@dc_DetalleGlosa", SqlDbType.VarChar)
        p(18) = objDaoMantenedor.getParam(objDetalleDocumento.IdUnidadMedida_Peso, "@IdUnidadMedida_Peso", SqlDbType.Int)

        Return CInt(SqlHelper.ExecuteScalar(tr, CommandType.StoredProcedure, "_DetalleDocumento_Registrar", p))

    End Function

    Public Function SelectComponenteKitxIdKitxIdDocumento(ByVal IdKit As Integer, ByVal IdDocumento As Integer) As List(Of Entidades.Kit)

        Dim lista As New List(Of Entidades.Kit)

        Try

            Dim p() As SqlParameter = New SqlParameter(1) {}
            p(0) = MyBase.getParam(IdKit, "@IdKit", SqlDbType.Int)
            p(1) = MyBase.getParam(IdDocumento, "@IdDocumento", SqlDbType.Int)

            MyBase.Cn = objConexion.ConexionSIGE
            MyBase.Cn.Open()

            reader = SqlHelper.ExecuteReader(MyBase.Cn, CommandType.StoredProcedure, "_DetalleDocumento_SelectComponenteKitxIdKitxIdDocumento", p)

            While (reader.Read)

                Dim objKit As New Entidades.Kit
                With objKit

                    .IdKit = MyBase.UCInt(reader("IdKit"))
                    .IdComponente = MyBase.UCInt(reader("IdComponente"))
                    .Cantidad_Comp = MyBase.UCDec(reader("CantidadComponente"))
                    .IdUnidadMedida_Comp = MyBase.UCInt(reader("IdUnidadMedidaComponente"))
                    .Componente = MyBase.UCStr(reader("Componente"))
                    .CodigoProd_Comp = MyBase.UCStr(reader("CodigoProducto_Comp"))
                    .UnidadMedida_Comp = MyBase.UCStr(reader("UnidadMedida"))

                    .IdMonedaPrecio_Comp = MyBase.UCInt(reader("IdMoneda"))
                    .Precio_Comp = MyBase.UCDec(reader("PrecioComponente"))
                    .MonedaPrecio_Comp = MyBase.UCStr(reader("MonedaComponente"))

                End With

                lista.Add(objKit)

            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (MyBase.Cn.State = ConnectionState.Open) Then MyBase.Cn.Close()
        End Try

        Return lista

    End Function

    Public Sub InsertaDetalleDocumento_MovAlmacen(ByVal cn As SqlConnection, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal tr As SqlTransaction, ByVal IdDocumento As Integer, Optional ByVal InsertaMovAlmacen As Boolean = False, Optional ByVal ComprometeStock As Boolean = False, Optional ByVal Factor As Integer = 0, Optional ByVal IdAlmacen As Integer = 0, Optional ByVal IdAlmacenllegada As Integer = 0)

        For i As Integer = 0 To listaDetalleDocumento.Count - 1

            Dim detalledocumento As Entidades.DetalleDocumento = listaDetalleDocumento(i)
            '****************
            Dim lista As New List(Of Entidades.be_tonoXProducto)
            lista = listaDetalleDocumento(i).listaTonos
            Dim dt As New DataTable
            dt.Columns.Add("idTono")
            dt.Columns.Add("nombre")
            dt.Columns.Add("cantidad")
            If lista IsNot Nothing Then
                For h As Integer = 0 To lista.Count - 1
                    Dim dr As DataRow = dt.NewRow
                    dr("idTono") = lista.Item(h).idTono
                    dr("nombre") = ""
                    dr("cantidad") = lista.Item(h).cantidadTono
                    dt.Rows.Add(dr)
                Next
            End If
            '*******************
            Dim ArrayParametros() As SqlParameter = New SqlParameter(28) {}

            ArrayParametros(0) = New SqlParameter("@IdDetalleDocumento", SqlDbType.Int)
            ArrayParametros(0).Value = DBNull.Value
            ArrayParametros(0).Direction = ParameterDirection.Output
            ArrayParametros(1) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            ArrayParametros(1).Value = IdDocumento
            ArrayParametros(2) = New SqlParameter("@IdProducto", SqlDbType.Int)
            ArrayParametros(2).Value = detalledocumento.IdProducto
            ArrayParametros(3) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
            ArrayParametros(3).Value = detalledocumento.IdUnidadMedida
            ArrayParametros(4) = New SqlParameter("@dc_Cantidad", SqlDbType.Decimal)
            ArrayParametros(4).Value = IIf(detalledocumento.Cantidad = Nothing, DBNull.Value, detalledocumento.Cantidad)
            ArrayParametros(5) = New SqlParameter("@dc_CantxAtender", SqlDbType.Decimal)
            ArrayParametros(5).Value = IIf(detalledocumento.CantxAtender = Nothing, DBNull.Value, detalledocumento.CantxAtender)
            ArrayParametros(6) = New SqlParameter("@dc_UMedida", SqlDbType.VarChar)
            ArrayParametros(6).Value = IIf(detalledocumento.UMedida = Nothing, DBNull.Value, detalledocumento.UMedida)
            ArrayParametros(7) = New SqlParameter("@dc_PrecioSD", SqlDbType.Decimal)
            ArrayParametros(7).Value = IIf(detalledocumento.PrecioSD = Nothing, DBNull.Value, detalledocumento.PrecioSD)
            ArrayParametros(8) = New SqlParameter("@dc_Descuento", SqlDbType.Decimal)
            ArrayParametros(8).Value = IIf(detalledocumento.Descuento = Nothing, DBNull.Value, detalledocumento.Descuento)
            ArrayParametros(9) = New SqlParameter("@dc_PrecioCD", SqlDbType.Decimal)
            ArrayParametros(9).Value = IIf(detalledocumento.PrecioCD = Nothing, DBNull.Value, detalledocumento.PrecioCD)
            ArrayParametros(10) = New SqlParameter("@dc_Importe", SqlDbType.Decimal)
            ArrayParametros(10).Value = IIf(detalledocumento.Importe = Nothing, DBNull.Value, detalledocumento.Importe)
            ArrayParametros(11) = New SqlParameter("@dc_Utilidad", SqlDbType.Decimal)
            ArrayParametros(11).Value = IIf(detalledocumento.Utilidad = Nothing, DBNull.Value, detalledocumento.Utilidad)
            ArrayParametros(12) = New SqlParameter("@dc_TasaPercepcion", SqlDbType.Decimal)
            ArrayParametros(12).Value = IIf(detalledocumento.TasaPercepcion = Nothing, DBNull.Value, detalledocumento.TasaPercepcion)
            ArrayParametros(13) = New SqlParameter("@dc_TasaDetraccion", SqlDbType.Decimal)
            ArrayParametros(13).Value = IIf(detalledocumento.TasaDetraccion = Nothing, DBNull.Value, detalledocumento.TasaDetraccion)
            ArrayParametros(14) = New SqlParameter("@dc_Peso", SqlDbType.Decimal)
            ArrayParametros(14).Value = IIf(detalledocumento.Peso = Nothing, DBNull.Value, detalledocumento.Peso)
            ArrayParametros(15) = New SqlParameter("@dc_UmPeso", SqlDbType.VarChar)
            ArrayParametros(15).Value = IIf(detalledocumento.UmPeso = Nothing, DBNull.Value, detalledocumento.UmPeso)
            ArrayParametros(16) = New SqlParameter("@IdDetalleAfecto", SqlDbType.Int)
            ArrayParametros(16).Value = IIf(detalledocumento.IdDetalleAfecto = Nothing, DBNull.Value, detalledocumento.IdDetalleAfecto)

            ArrayParametros(17) = New SqlParameter("@dc_DetalleGlosa", SqlDbType.VarChar)
            ArrayParametros(17).Value = IIf(detalledocumento.DetalleGlosa = Nothing, DBNull.Value, detalledocumento.DetalleGlosa)

            ArrayParametros(18) = New SqlParameter("@InsertaMovAlmacen", SqlDbType.Bit)
            ArrayParametros(18).Value = IIf(InsertaMovAlmacen = Nothing, False, InsertaMovAlmacen)

            ArrayParametros(19) = New SqlParameter("@ComprometeStock", SqlDbType.Bit)
            ArrayParametros(19).Value = IIf(ComprometeStock = Nothing, False, ComprometeStock)

            ArrayParametros(20) = New SqlParameter("@Factor", SqlDbType.Int)
            ArrayParametros(20).Value = IIf(Factor = Nothing, 0, Factor)

            ArrayParametros(21) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
            ArrayParametros(21).Value = IIf(IdAlmacen = Nothing, DBNull.Value, IdAlmacen)

            ArrayParametros(22) = New SqlParameter("@dc_PrecioLista", SqlDbType.Decimal)
            ArrayParametros(22).Value = IIf(detalledocumento.PrecioLista = Nothing, DBNull.Value, detalledocumento.PrecioLista)
            ArrayParametros(23) = New SqlParameter("@IdUsuarioSupervisor", SqlDbType.Int)
            ArrayParametros(23).Value = IIf(detalledocumento.IdUsuarioSupervisor = Nothing, DBNull.Value, detalledocumento.IdUsuarioSupervisor)

            ArrayParametros(24) = New SqlParameter("@ma_CostoMovIngreso", SqlDbType.Decimal)
            ArrayParametros(24).Value = IIf(detalledocumento.CostoMovIngreso = Nothing, DBNull.Value, detalledocumento.CostoMovIngreso)

            ArrayParametros(25) = New SqlParameter("@idSector", SqlDbType.Int)
            ArrayParametros(25).Value = IIf(detalledocumento.idSector = Nothing, DBNull.Value, detalledocumento.idSector)

            ArrayParametros(26) = New SqlParameter("@type_tono", SqlDbType.Structured)
            ArrayParametros(26).Value = dt

            ArrayParametros(27) = New SqlParameter("@idAlmacenDEstino", SqlDbType.Int)
            ArrayParametros(27).Value = IIf(IdAlmacenllegada = Nothing, DBNull.Value, IdAlmacenllegada)

            ArrayParametros(28) = New SqlParameter("@idTono", SqlDbType.Int)
            ArrayParametros(28).Value = IIf(detalledocumento.idTono = Nothing, DBNull.Value, detalledocumento.idTono)

            listaDetalleDocumento(i).IdDetalleDocumento = HDAO.InsertaTParameterOutPut(cn, "_DetalleDocumentoInsert_MovAlmacen_v2", ArrayParametros, tr)

        Next

    End Sub
    Public Function SelectxIdDocumentoconcepto(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DetalleDocumentoSelectxIdDocumentoConceptos", cn)
        cmd.CommandTimeout = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.DetalleDocumento
                    With obj

                        .IdDetalleAfecto = CInt(IIf(IsDBNull(lector.Item("IdDetalleAfecto")) = True, 0, lector.Item("IdDetalleAfecto")))
                        .desconcepto = CStr(IIf(IsDBNull(lector.Item("dr_Concepto")) = True, "", lector.Item("dr_Concepto")))
                        .montoconcepto = CDec(IIf(IsDBNull(lector.Item("dr_Monto")) = True, 0, lector.Item("dr_Monto")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxIdDocumentoValidacionTransito(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DetalleDocumentoSelectxIdDocumentoCantTransito_v2", cn)
        cmd.CommandTimeout = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.DetalleDocumento
                    With obj
                        .Cantidad = CDec(IIf(IsDBNull(lector.Item("dc_Cantidad")) = True, 0, lector.Item("dc_Cantidad")))
                        .CantxAtender = CDec(IIf(IsDBNull(lector.Item("dc_CantxAtender")) = True, 0, lector.Item("dc_CantxAtender")))
                        .Descuento = CDec(IIf(IsDBNull(lector.Item("dc_Descuento")) = True, 0, lector.Item("dc_Descuento")))
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .Importe = CDec(IIf(IsDBNull(lector.Item("dc_Importe")) = True, 0, lector.Item("dc_Importe")))
                        .PrecioCD = CDec(IIf(IsDBNull(lector.Item("dc_PrecioCD")) = True, 0, lector.Item("dc_PrecioCD")))
                        .PrecioSD = CDec(IIf(IsDBNull(lector.Item("dc_PrecioSD")) = True, 0, lector.Item("dc_PrecioSD")))
                        .TasaDetraccion = CDec(IIf(IsDBNull(lector.Item("dc_TasaDetraccion")) = True, 0, lector.Item("dc_TasaDetraccion")))
                        .TasaPercepcion = CDec(IIf(IsDBNull(lector.Item("dc_TasaPercepcion")) = True, 0, lector.Item("dc_TasaPercepcion")))
                        .UMedida = CStr(IIf(IsDBNull(lector.Item("dc_UMedida")) = True, "", lector.Item("dc_UMedida")))
                        .Utilidad = CDec(IIf(IsDBNull(lector.Item("dc_Utilidad")) = True, 0, lector.Item("dc_Utilidad")))
                        .NomProducto = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                        .CaxAtender = CDec(IIf(IsDBNull(lector.Item("CaxAtender")) = True, 0, lector.Item("CaxAtender")))
                        .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")) = True, "", lector.Item("prod_Codigo")))
                        .ProdNombreKit = CStr(IIf(IsDBNull(lector.Item("codcompuesto")) = True, "", lector.Item("codcompuesto")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .IdUnidadMedidaPrincipal = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedidaPrincipal")) = True, 0, lector.Item("IdUnidadMedidaPrincipal")))
                        .IdDetalleDocumento = CInt(IIf(IsDBNull(lector.Item("IdDetalleDocumento")) = True, 0, lector.Item("IdDetalleDocumento")))
                        .IdDetalleAfecto = CInt(IIf(IsDBNull(lector.Item("IdDetalleAfecto")) = True, 0, lector.Item("IdDetalleAfecto")))
                        .IdDocumentoRef = CInt(IIf(IsDBNull(lector.Item("IdDocumentoRef")) = True, 0, lector.Item("IdDocumentoRef")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .CantidadDetalleAfecto = CDec(IIf(IsDBNull(lector.Item("CantidadDetalleAfecto")) = True, 0, lector.Item("CantidadDetalleAfecto")))
                        .CantidadTransito = CDec(IIf(IsDBNull(lector.Item("dc_CantidadTransito")) = True, 0, lector.Item("dc_CantidadTransito")))
                        .Moneda = UCStr(lector("Moneda"))
                        .Peso = objDaoMantenedor.UCDec(lector("dc_Peso"))
                        .UmPeso = objDaoMantenedor.UCStr(lector("dc_UmPeso"))
                        .IdUnidadMedida_Peso = objDaoMantenedor.UCInt(lector("IdUnidadMedida_Peso"))
                        .Kit = objDaoMantenedor.UCBool(lector("prod_Kit"))
                        .IdKit = objDaoMantenedor.UCInt(lector("IdKit"))
                        .kit_Cantidad_Comp = objDaoMantenedor.UCInt(lector("kit_Cantidad_Comp"))
                        .idSector = CInt(IIf(IsDBNull(lector.Item("id_sector")) = True, 0, lector.Item("id_sector")))

                        .ComponenteKit = CBool(IIf(IsDBNull(lector("add_ComponenteKit")) = True, False, lector("add_ComponenteKit")))

                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function SelectxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DetalleDocumentoSelectxIdDocumento", cn)
        cmd.CommandTimeout = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.DetalleDocumento
                    With obj
                        .Cantidad = CDec(IIf(IsDBNull(lector.Item("dc_Cantidad")) = True, 0, lector.Item("dc_Cantidad")))
                        .CantxAtender = CDec(IIf(IsDBNull(lector.Item("dc_CantxAtender")) = True, 0, lector.Item("dc_CantxAtender")))
                        .Descuento = CDec(IIf(IsDBNull(lector.Item("dc_Descuento")) = True, 0, lector.Item("dc_Descuento")))
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .Importe = CDec(IIf(IsDBNull(lector.Item("dc_Importe")) = True, 0, lector.Item("dc_Importe")))
                        .PrecioCD = CDec(IIf(IsDBNull(lector.Item("dc_PrecioCD")) = True, 0, lector.Item("dc_PrecioCD")))
                        .PrecioSD = CDec(IIf(IsDBNull(lector.Item("dc_PrecioSD")) = True, 0, lector.Item("dc_PrecioSD")))
                        .TasaDetraccion = CDec(IIf(IsDBNull(lector.Item("dc_TasaDetraccion")) = True, 0, lector.Item("dc_TasaDetraccion")))
                        .TasaPercepcion = CDec(IIf(IsDBNull(lector.Item("dc_TasaPercepcion")) = True, 0, lector.Item("dc_TasaPercepcion")))
                        .UMedida = CStr(IIf(IsDBNull(lector.Item("dc_UMedida")) = True, "", lector.Item("dc_UMedida")))
                        .Utilidad = CDec(IIf(IsDBNull(lector.Item("dc_Utilidad")) = True, 0, lector.Item("dc_Utilidad")))
                        .NomProducto = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                        .CaxAtender = CDec(IIf(IsDBNull(lector.Item("CaxAtender")) = True, 0, lector.Item("CaxAtender")))
                        .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")) = True, "", lector.Item("prod_Codigo")))
                        .ProdNombreKit = CStr(IIf(IsDBNull(lector.Item("codcompuesto")) = True, "", lector.Item("codcompuesto")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .IdUnidadMedidaPrincipal = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedidaPrincipal")) = True, 0, lector.Item("IdUnidadMedidaPrincipal")))
                        .IdDetalleDocumento = CInt(IIf(IsDBNull(lector.Item("IdDetalleDocumento")) = True, 0, lector.Item("IdDetalleDocumento")))
                        .IdDetalleAfecto = CInt(IIf(IsDBNull(lector.Item("IdDetalleAfecto")) = True, 0, lector.Item("IdDetalleAfecto")))
                        .IdDocumentoRef = CInt(IIf(IsDBNull(lector.Item("IdDocumentoRef")) = True, 0, lector.Item("IdDocumentoRef")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .CantidadDetalleAfecto = CDec(IIf(IsDBNull(lector.Item("CantidadDetalleAfecto")) = True, 0, lector.Item("CantidadDetalleAfecto")))
                        .idSector = CInt(IIf(IsDBNull(lector.Item("id_sector")) = True, 0, lector.Item("id_sector")))
                        .CantidadTransito = CDec(IIf(IsDBNull(lector.Item("dc_CantidadTransito")) = True, 0, lector.Item("dc_CantidadTransito")))
                        .Moneda = UCStr(lector("Moneda"))

                        .Peso = objDaoMantenedor.UCDec(lector("dc_Peso"))
                        .UmPeso = objDaoMantenedor.UCStr(lector("dc_UmPeso"))
                        .IdUnidadMedida_Peso = objDaoMantenedor.UCInt(lector("IdUnidadMedida_Peso"))
                        .Kit = objDaoMantenedor.UCBool(lector("prod_Kit"))
                        .IdKit = objDaoMantenedor.UCInt(lector("IdKit"))
                        .kit_Cantidad_Comp = objDaoMantenedor.UCInt(lector("kit_Cantidad_Comp"))

                        .ComponenteKit = CBool(IIf(IsDBNull(lector("add_ComponenteKit")) = True, False, lector("add_ComponenteKit")))

                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function BuscaProdcompuestoo(ByVal IdDocumento As Integer) As Boolean
        Dim hecho As Boolean = False
        Dim resultado As Integer = 0
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_BuscaProdcompuesto", cn)
        cmd.CommandTimeout = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cn.Open()
        Try
            If cmd.ExecuteNonQuery() <= 0 Then
                Throw New Exception
            End If
            hecho = True
        Catch ex As Exception
            hecho = False
        Finally

        End Try
        Return hecho
    End Function
    Public Function CambiarProdcompuesto(ByVal IdDocumento As Integer) As Boolean
        Dim hecho As Boolean = False
        Dim resultado As Integer = 0
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CambiarProdcompuesto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cn.Open()
        Try
            If cmd.ExecuteNonQuery() <= 0 Then
                Throw New Exception
            End If
            hecho = True
        Catch ex As Exception
            hecho = False
        Finally

        End Try
        Return hecho
    End Function
    Public Function BuscaCheckNotCred(ByVal IdDocumento As Integer) As Boolean
        Dim hecho As Boolean = False
        Dim resultado As Integer = 0
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_BuscaCheckNotCred", cn)
        cmd.CommandTimeout = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cn.Open()
        Try
            If cmd.ExecuteNonQuery() >= 1 Then
                Throw New Exception
            End If
            hecho = True
        Catch ex As Exception
            hecho = False
        Finally

        End Try
        Return hecho
    End Function




    ''DOCUMENTO REFERENCIA + DESCRIPCI�N DEL PRODUCTO COMPUESTO

    Public Function DetalleDocumentoRefxGuiaRecep(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("[_DetalleDocumentoRefxGuiaRecep_v2]", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleDocumento)
                If (lector.HasRows = True) Then


                    Do While lector.Read
                        Dim obj As New Entidades.DetalleDocumento
                        With obj
                            .idSector = CInt(IIf(IsDBNull(lector.Item("idSector")) = True, 0, lector.Item("idSector")))
                            .Cantidad = CDec(IIf(IsDBNull(lector.Item("dc_Cantidad")) = True, 0, lector.Item("dc_Cantidad")))
                            .CantxAtender = CDec(IIf(IsDBNull(lector.Item("dc_CantxAtender")) = True, 0, lector.Item("dc_CantxAtender")))
                            .Descuento = CDec(IIf(IsDBNull(lector.Item("dc_Descuento")) = True, 0, lector.Item("dc_Descuento")))
                            .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                            .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                            .Importe = CDec(IIf(IsDBNull(lector.Item("dc_Importe")) = True, 0, lector.Item("dc_Importe")))
                            .PrecioCD = CDec(IIf(IsDBNull(lector.Item("dc_PrecioCD")) = True, 0, lector.Item("dc_PrecioCD")))
                            .PrecioSD = CDec(IIf(IsDBNull(lector.Item("dc_PrecioSD")) = True, 0, lector.Item("dc_PrecioSD")))
                            .TasaDetraccion = CDec(IIf(IsDBNull(lector.Item("dc_TasaDetraccion")) = True, 0, lector.Item("dc_TasaDetraccion")))
                            .TasaPercepcion = CDec(IIf(IsDBNull(lector.Item("dc_TasaPercepcion")) = True, 0, lector.Item("dc_TasaPercepcion")))
                            .UMedida = CStr(IIf(IsDBNull(lector.Item("dc_UMedida")) = True, "", lector.Item("dc_UMedida")))
                            .Utilidad = CDec(IIf(IsDBNull(lector.Item("dc_Utilidad")) = True, 0, lector.Item("dc_Utilidad")))
                            .NomProducto = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                            .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")) = True, "", lector.Item("prod_Codigo")))
                            .ProdNombreKit = CStr(IIf(IsDBNull(lector.Item("codcompuesto")) = True, "", lector.Item("codcompuesto")))
                            .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                            .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                            .IdUnidadMedidaPrincipal = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedidaPrincipal")) = True, 0, lector.Item("IdUnidadMedidaPrincipal")))
                            .IdDetalleDocumento = CInt(IIf(IsDBNull(lector.Item("IdDetalleDocumento")) = True, 0, lector.Item("IdDetalleDocumento")))
                            .IdDetalleAfecto = CInt(IIf(IsDBNull(lector.Item("IdDetalleAfecto")) = True, 0, lector.Item("IdDetalleAfecto")))
                            .IdDocumentoRef = CInt(IIf(IsDBNull(lector.Item("IdDocumentoRef")) = True, 0, lector.Item("IdDocumentoRef")))
                            .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                            .CantidadDetalleAfecto = CDec(IIf(IsDBNull(lector.Item("CantidadDetalleAfecto")) = True, 0, lector.Item("CantidadDetalleAfecto")))

                            .CantidadTransito = UCDec(lector("dc_CantidadTransito"))
                            .Moneda = UCStr(lector("Moneda"))

                            .Peso = objDaoMantenedor.UCDec(lector("dc_Peso"))
                            .UmPeso = objDaoMantenedor.UCStr(lector("dc_UmPeso"))
                            .IdUnidadMedida_Peso = objDaoMantenedor.UCInt(lector("IdUnidadMedida_Peso"))
                            .Kit = objDaoMantenedor.UCBool(lector("prod_Kit"))
                            .IdKit = objDaoMantenedor.UCInt(lector("IdKit"))
                            .kit_Cantidad_Comp = objDaoMantenedor.UCInt(lector("kit_Cantidad_Comp"))

                        End With
                        Lista.Add(obj)
                    Loop
                Else
                    Lista = Nothing
                End If
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectCantxAtenderNoCeroxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DetalleDocumentoSelectCantxAtenderNoCeroxIdDocumento", cn)
        cmd.CommandTimeout = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.DetalleDocumento
                    With obj
                        .Cantidad = CDec(IIf(IsDBNull(lector.Item("dc_Cantidad")) = True, 0, lector.Item("dc_Cantidad")))
                        .CantxAtender = CDec(IIf(IsDBNull(lector.Item("dc_CantxAtender")) = True, 0, lector.Item("dc_CantxAtender")))
                        '.Descuento = CDec(IIf(IsDBNull(lector.Item("dc_Descuento")) = True, 0, lector.Item("dc_Descuento")))
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        '.Importe = CDec(IIf(IsDBNull(lector.Item("dc_Importe")) = True, 0, lector.Item("dc_Importe")))
                        '.PrecioCD = CDec(IIf(IsDBNull(lector.Item("dc_PrecioCD")) = True, 0, lector.Item("dc_PrecioCD")))
                        '.PrecioSD = CDec(IIf(IsDBNull(lector.Item("dc_PrecioSD")) = True, 0, lector.Item("dc_PrecioSD")))
                        '.TasaDetraccion = CDec(IIf(IsDBNull(lector.Item("dc_TasaDetraccion")) = True, 0, lector.Item("dc_TasaDetraccion")))
                        '.TasaPercepcion = CDec(IIf(IsDBNull(lector.Item("dc_TasaPercepcion")) = True, 0, lector.Item("dc_TasaPercepcion")))
                        .UMedida = CStr(IIf(IsDBNull(lector.Item("dc_UMedida")) = True, "", lector.Item("dc_UMedida")))
                        '.Utilidad = CDec(IIf(IsDBNull(lector.Item("dc_Utilidad")) = True, 0, lector.Item("dc_Utilidad")))
                        .NomProducto = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .IdUnidadMedidaPrincipal = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedidaPrincipal")) = True, 0, lector.Item("IdUnidadMedidaPrincipal")))
                        .IdDetalleDocumento = CInt(IIf(IsDBNull(lector.Item("IdDetalleDocumento")) = True, 0, lector.Item("IdDetalleDocumento")))
                        .CantADespachar = CDec(IIf(IsDBNull(lector.Item("dc_CantxAtender")) = True, 0, lector.Item("dc_CantxAtender")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function InsertaLDetalleDocumentoT(ByVal Cn As SqlConnection, ByVal Ldetalledocumento As List(Of Entidades.DetalleDocumento), _
                                            ByVal T As SqlTransaction) As Integer
        Dim DetalleDocumento As New Entidades.DetalleDocumento
        For Each DetalleDocumento In Ldetalledocumento

            Dim ArrayParametros() As SqlParameter = New SqlParameter(15) {}

            ArrayParametros(0) = New SqlParameter("@IdDetalleDocumento", SqlDbType.Int)
            ArrayParametros(0).Direction = ParameterDirection.Output
            ArrayParametros(1) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            ArrayParametros(1).Value = DetalleDocumento.IdDocumento
            ArrayParametros(2) = New SqlParameter("@IdProducto", SqlDbType.Int)
            ArrayParametros(2).Value = DetalleDocumento.IdProducto
            ArrayParametros(3) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
            ArrayParametros(3).Value = DetalleDocumento.IdUnidadMedida
            ArrayParametros(4) = New SqlParameter("@dc_Cantidad", SqlDbType.Decimal)
            ArrayParametros(4).Value = IIf(DetalleDocumento.Cantidad = Nothing, DBNull.Value, DetalleDocumento.Cantidad)
            ArrayParametros(5) = New SqlParameter("@dc_CantxAtender", SqlDbType.Decimal)
            ArrayParametros(5).Value = IIf(DetalleDocumento.CantxAtender = Nothing, DBNull.Value, DetalleDocumento.CantxAtender)
            ArrayParametros(6) = New SqlParameter("@dc_UMedida", SqlDbType.VarChar)
            ArrayParametros(6).Value = IIf(DetalleDocumento.UMedida = Nothing, DBNull.Value, DetalleDocumento.UMedida)
            ArrayParametros(7) = New SqlParameter("@dc_PrecioSD", SqlDbType.Decimal)
            ArrayParametros(7).Value = IIf(DetalleDocumento.PrecioSD = Nothing, DBNull.Value, DetalleDocumento.PrecioSD)
            ArrayParametros(8) = New SqlParameter("@dc_Descuento", SqlDbType.Decimal)
            ArrayParametros(8).Value = IIf(DetalleDocumento.Descuento = Nothing, DBNull.Value, DetalleDocumento.Descuento)
            ArrayParametros(9) = New SqlParameter("@dc_PrecioCD", SqlDbType.Decimal)
            ArrayParametros(9).Value = IIf(DetalleDocumento.PrecioCD = Nothing, DBNull.Value, DetalleDocumento.PrecioCD)
            ArrayParametros(10) = New SqlParameter("@dc_Importe", SqlDbType.Decimal)
            ArrayParametros(10).Value = IIf(DetalleDocumento.Importe = Nothing, DBNull.Value, DetalleDocumento.Importe)
            ArrayParametros(11) = New SqlParameter("@dc_Utilidad", SqlDbType.Decimal)
            ArrayParametros(11).Value = IIf(DetalleDocumento.Utilidad = Nothing, DBNull.Value, DetalleDocumento.Utilidad)
            ArrayParametros(12) = New SqlParameter("@dc_TasaPercepcion", SqlDbType.Decimal)
            ArrayParametros(12).Value = IIf(DetalleDocumento.TasaPercepcion = Nothing, DBNull.Value, DetalleDocumento.TasaPercepcion)
            ArrayParametros(13) = New SqlParameter("@dc_TasaDetraccion", SqlDbType.Decimal)
            ArrayParametros(13).Value = IIf(DetalleDocumento.TasaDetraccion = Nothing, DBNull.Value, DetalleDocumento.TasaDetraccion)
            ArrayParametros(14) = New SqlParameter("@dc_Peso", SqlDbType.Decimal)
            ArrayParametros(14).Value = IIf(DetalleDocumento.Peso = Nothing, DBNull.Value, DetalleDocumento.Peso)
            ArrayParametros(15) = New SqlParameter("@dc_UmPeso", SqlDbType.VarChar)
            ArrayParametros(15).Value = IIf(DetalleDocumento.UmPeso = Nothing, DBNull.Value, DetalleDocumento.UmPeso)
            Return HDAO.InsertaTParameterOutPut(Cn, "_DetalleDocumentoInsert", ArrayParametros, T)
        Next
    End Function
    Public Function InsertaDetalleDocumento(ByVal cn As SqlConnection, ByVal detalledocumento As Entidades.DetalleDocumento, ByVal T As SqlTransaction) As Integer
        Dim ArrayParametros() As SqlParameter = New SqlParameter(24) {}
        ArrayParametros(0) = New SqlParameter("@IdDetalleDocumento", SqlDbType.Int)
        ArrayParametros(0).Direction = ParameterDirection.Output
        ArrayParametros(1) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(1).Value = detalledocumento.IdDocumento
        ArrayParametros(2) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(2).Value = detalledocumento.IdProducto
        ArrayParametros(3) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
        ArrayParametros(3).Value = detalledocumento.IdUnidadMedida
        ArrayParametros(4) = New SqlParameter("@dc_Cantidad", SqlDbType.Decimal)
        ArrayParametros(4).Value = IIf(detalledocumento.Cantidad = Nothing, DBNull.Value, detalledocumento.Cantidad)
        ArrayParametros(5) = New SqlParameter("@dc_CantxAtender", SqlDbType.Decimal)
        ArrayParametros(5).Value = IIf(detalledocumento.CantxAtender = Nothing, DBNull.Value, detalledocumento.CantxAtender)
        ArrayParametros(6) = New SqlParameter("@dc_UMedida", SqlDbType.VarChar)
        ArrayParametros(6).Value = IIf(detalledocumento.UMedida = Nothing, DBNull.Value, detalledocumento.UMedida)
        ArrayParametros(7) = New SqlParameter("@dc_PrecioSD", SqlDbType.Decimal)
        ArrayParametros(7).Value = IIf(detalledocumento.PrecioSD = Nothing, DBNull.Value, detalledocumento.PrecioSD)
        ArrayParametros(8) = New SqlParameter("@dc_Descuento", SqlDbType.Decimal)
        ArrayParametros(8).Value = IIf(detalledocumento.Descuento = Nothing, DBNull.Value, detalledocumento.Descuento)
        ArrayParametros(9) = New SqlParameter("@dc_PrecioCD", SqlDbType.Decimal)
        ArrayParametros(9).Value = IIf(detalledocumento.PrecioCD = Nothing, DBNull.Value, detalledocumento.PrecioCD)
        ArrayParametros(10) = New SqlParameter("@dc_Importe", SqlDbType.Decimal)
        ArrayParametros(10).Value = IIf(detalledocumento.Importe = Nothing, DBNull.Value, detalledocumento.Importe)
        ArrayParametros(11) = New SqlParameter("@dc_Utilidad", SqlDbType.Decimal)
        ArrayParametros(11).Value = IIf(detalledocumento.Utilidad = Nothing, DBNull.Value, detalledocumento.Utilidad)
        ArrayParametros(12) = New SqlParameter("@dc_TasaPercepcion", SqlDbType.Decimal)
        ArrayParametros(12).Value = IIf(detalledocumento.TasaPercepcion = Nothing, DBNull.Value, detalledocumento.TasaPercepcion)
        ArrayParametros(13) = New SqlParameter("@dc_TasaDetraccion", SqlDbType.Decimal)
        ArrayParametros(13).Value = IIf(detalledocumento.TasaDetraccion = Nothing, DBNull.Value, detalledocumento.TasaDetraccion)
        ArrayParametros(14) = New SqlParameter("@dc_Peso", SqlDbType.Decimal)
        ArrayParametros(14).Value = IIf(detalledocumento.Peso = Nothing, DBNull.Value, detalledocumento.Peso)
        ArrayParametros(15) = New SqlParameter("@dc_UmPeso", SqlDbType.VarChar)
        ArrayParametros(15).Value = IIf(detalledocumento.UmPeso = Nothing, DBNull.Value, detalledocumento.UmPeso)
        ArrayParametros(16) = New SqlParameter("@IdDetalleAfecto", SqlDbType.Int)
        ArrayParametros(16).Value = IIf(detalledocumento.IdDetalleAfecto = Nothing, DBNull.Value, detalledocumento.IdDetalleAfecto)

        ArrayParametros(17) = New SqlParameter("@dc_DetalleGlosa", SqlDbType.VarChar)
        ArrayParametros(17).Value = IIf(detalledocumento.DetalleGlosa = Nothing, DBNull.Value, detalledocumento.DetalleGlosa)

        ArrayParametros(18) = New SqlParameter("@dc_PrecioLista", SqlDbType.Decimal)
        ArrayParametros(18).Value = IIf(detalledocumento.PrecioLista = Nothing, DBNull.Value, detalledocumento.PrecioLista)
        ArrayParametros(19) = New SqlParameter("@IdUsuarioSupervisor", SqlDbType.Int)
        ArrayParametros(19).Value = IIf(detalledocumento.IdUsuarioSupervisor = Nothing, DBNull.Value, detalledocumento.IdUsuarioSupervisor)

        ArrayParametros(20) = New SqlParameter("@dc_CantidadTransito", SqlDbType.Decimal)
        ArrayParametros(20).Value = IIf(detalledocumento.dc_CantidadTransito = Nothing, DBNull.Value, detalledocumento.dc_CantidadTransito)
        ArrayParametros(21) = New SqlParameter("@dc_PrecioSinIGV", SqlDbType.Decimal)
        ArrayParametros(21).Value = IIf(detalledocumento.dc_PrecioSinIGV = Nothing, DBNull.Value, detalledocumento.dc_PrecioSinIGV)
        ArrayParametros(22) = New SqlParameter("dc_Descuento1", SqlDbType.Decimal)
        ArrayParametros(22).Value = IIf(detalledocumento.dc_Descuento1 = Nothing, DBNull.Value, detalledocumento.dc_Descuento1)
        ArrayParametros(23) = New SqlParameter("@dc_Descuento2", SqlDbType.Decimal)
        ArrayParametros(23).Value = IIf(detalledocumento.dc_Descuento2 = Nothing, DBNull.Value, detalledocumento.dc_Descuento2)
        ArrayParametros(24) = New SqlParameter("@dc_Descuento3", SqlDbType.Decimal)
        ArrayParametros(24).Value = IIf(detalledocumento.dc_Descuento3 = Nothing, DBNull.Value, detalledocumento.dc_Descuento3)


        Return HDAO.InsertaTParameterOutPut(cn, "_DetalleDocumentoInsert", ArrayParametros, T)
    End Function
    Public Function ActualizaDetalleDocumento(ByVal detalledocumento As Entidades.DetalleDocumento) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(11) {}
        ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = detalledocumento.IdDocumento
        ArrayParametros(1) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(1).Value = detalledocumento.IdProducto
        ArrayParametros(2) = New SqlParameter("@dc_Cantidad", SqlDbType.Decimal)
        ArrayParametros(2).Value = detalledocumento.Cantidad
        ArrayParametros(3) = New SqlParameter("@dc_CantxAtender", SqlDbType.Decimal)
        ArrayParametros(3).Value = detalledocumento.CantxAtender
        ArrayParametros(4) = New SqlParameter("@dc_UMedida", SqlDbType.VarChar)
        ArrayParametros(4).Value = detalledocumento.UMedida
        ArrayParametros(5) = New SqlParameter("@dc_PrecioSD", SqlDbType.Decimal)
        ArrayParametros(5).Value = detalledocumento.PrecioSD
        ArrayParametros(6) = New SqlParameter("@dc_Descuento", SqlDbType.Decimal)
        ArrayParametros(6).Value = detalledocumento.Descuento
        ArrayParametros(7) = New SqlParameter("@dc_PrecioCD", SqlDbType.Decimal)
        ArrayParametros(7).Value = detalledocumento.PrecioCD
        ArrayParametros(8) = New SqlParameter("@dc_Importe", SqlDbType.Decimal)
        ArrayParametros(8).Value = detalledocumento.Importe
        ArrayParametros(9) = New SqlParameter("@dc_Utilidad", SqlDbType.Decimal)
        ArrayParametros(9).Value = detalledocumento.Utilidad
        ArrayParametros(10) = New SqlParameter("@dc_TasaPercepcion", SqlDbType.Decimal)
        ArrayParametros(10).Value = detalledocumento.TasaPercepcion
        ArrayParametros(11) = New SqlParameter("@dc_TasaDetraccion", SqlDbType.Decimal)
        ArrayParametros(11).Value = detalledocumento.TasaDetraccion
        Return HDAO.Update(cn, "_DetalleDocumentoUpdate", ArrayParametros)
    End Function
    Public Function ActualizaDetalleDocumento(ByVal detalledocumento As Entidades.DetalleDocumento, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Dim ArrayParametros() As SqlParameter = New SqlParameter(15) {}
        ArrayParametros(0) = New SqlParameter("@IdDetalleDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = detalledocumento.IdDetalleDocumento
        ArrayParametros(1) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(1).Value = detalledocumento.IdDocumento
        ArrayParametros(2) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(2).Value = detalledocumento.IdProducto
        ArrayParametros(3) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
        ArrayParametros(3).Value = detalledocumento.IdUnidadMedida
        ArrayParametros(4) = New SqlParameter("@dc_Cantidad", SqlDbType.Decimal)
        ArrayParametros(4).Value = IIf(detalledocumento.Cantidad = Nothing, DBNull.Value, detalledocumento.Cantidad)
        ArrayParametros(5) = New SqlParameter("@dc_CantxAtender", SqlDbType.Decimal)
        ArrayParametros(5).Value = IIf(detalledocumento.CantxAtender = Nothing, DBNull.Value, detalledocumento.CantxAtender)
        ArrayParametros(6) = New SqlParameter("@dc_UMedida", SqlDbType.VarChar)
        ArrayParametros(6).Value = IIf(detalledocumento.UMedida = Nothing, DBNull.Value, detalledocumento.UMedida)
        ArrayParametros(7) = New SqlParameter("@dc_PrecioSD", SqlDbType.Decimal)
        ArrayParametros(7).Value = IIf(detalledocumento.PrecioSD = Nothing, DBNull.Value, detalledocumento.PrecioSD)
        ArrayParametros(8) = New SqlParameter("@dc_Descuento", SqlDbType.Decimal)
        ArrayParametros(8).Value = IIf(detalledocumento.Descuento = Nothing, DBNull.Value, detalledocumento.Descuento)
        ArrayParametros(9) = New SqlParameter("@dc_PrecioCD", SqlDbType.Decimal)
        ArrayParametros(9).Value = IIf(detalledocumento.PrecioCD = Nothing, DBNull.Value, detalledocumento.PrecioCD)
        ArrayParametros(10) = New SqlParameter("@dc_Importe", SqlDbType.Decimal)
        ArrayParametros(10).Value = IIf(detalledocumento.Importe = Nothing, DBNull.Value, detalledocumento.Importe)
        ArrayParametros(11) = New SqlParameter("@dc_Utilidad", SqlDbType.Decimal)
        ArrayParametros(11).Value = IIf(detalledocumento.Utilidad = Nothing, DBNull.Value, detalledocumento.Utilidad)
        ArrayParametros(12) = New SqlParameter("@dc_TasaPercepcion", SqlDbType.Decimal)
        ArrayParametros(12).Value = IIf(detalledocumento.TasaPercepcion = Nothing, DBNull.Value, detalledocumento.TasaPercepcion)
        ArrayParametros(13) = New SqlParameter("@dc_TasaDetraccion", SqlDbType.Decimal)
        ArrayParametros(13).Value = IIf(detalledocumento.TasaDetraccion = Nothing, DBNull.Value, detalledocumento.TasaDetraccion)
        ArrayParametros(14) = New SqlParameter("@dc_Peso", SqlDbType.Decimal)
        ArrayParametros(14).Value = IIf(detalledocumento.Peso = Nothing, DBNull.Value, detalledocumento.Peso)
        ArrayParametros(15) = New SqlParameter("@dc_UmPeso", SqlDbType.VarChar)
        ArrayParametros(15).Value = IIf(detalledocumento.UmPeso = Nothing, DBNull.Value, detalledocumento.UmPeso)
        Dim cmd As New SqlCommand("_DetalleDocumentoUpdate", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        If cmd.ExecuteNonQuery <= 0 Then
            '******** Throw New Exception
        End If
        Return True
    End Function
    Public Function UpdateCantxAtenderxIdDetalleDocumento(ByVal IdDetalleDocumento As Integer, ByVal cantxatender As Decimal, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Dim cmd As New SqlCommand("_DetalleDocumentoUpdateCantxAtenderxIdDetalleDocumento", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDetalleDocumento", IdDetalleDocumento)
        cmd.Parameters.AddWithValue("@CantxAtender", cantxatender)
        Try
            If cmd.ExecuteNonQuery = 0 Then
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxIdSubLineaxIdDocumento(ByVal idsublinea As Integer, ByVal iddocumento As Integer, ByVal Prod_Codigo As String, _
                                                         ByVal Prod_Nombre As String, ByVal Tabla As DataTable) As List(Of Entidades.DetalleDocumentoView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_InventarioInicialSelectxIdSubLineaxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)
        cmd.Parameters.AddWithValue("@prod_Codigo", IIf(Prod_Codigo = Nothing, DBNull.Value, Prod_Codigo))
        cmd.Parameters.AddWithValue("@prod_Nombre", IIf(Prod_Nombre = Nothing, DBNull.Value, Prod_Nombre))
        cmd.Parameters.AddWithValue("@Tabla", Tabla)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleDocumentoView)
                Do While lector.Read
                    Dim obj As New Entidades.DetalleDocumentoView
                    With obj
                        .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")) = True, "", lector.Item("prod_Codigo")))
                        .Cantidad = CDec(IIf(IsDBNull(lector.Item("Cantidad")) = True, 0, lector.Item("Cantidad")))
                        .Descripcion = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .IdUMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .UM = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                        .IdDetalleDocumento = CInt(IIf(IsDBNull(lector.Item("IdDetalleDocumento")) = True, 0, lector.Item("IdDetalleDocumento")))

                        .NomMoneda = CStr(IIf(IsDBNull(lector.Item("MonedaBase")) = True, "", lector.Item("MonedaBase")))
                        .PrecioCD = CDec(IIf(IsDBNull(lector.Item("CostoProducto")) = True, 0, lector.Item("CostoProducto")))

                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function DetalleDocumentoViewSelectxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumentoView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DetalleDocumentoViewSelectxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleDocumentoView)
                Do While lector.Read
                    Dim obj As New Entidades.DetalleDocumentoView
                    With obj
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .IdDetalleDocumento = CInt(IIf(IsDBNull(lector.Item("IdDetalleDocumento")) = True, 0, lector.Item("IdDetalleDocumento")))
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .UM = CStr(IIf(IsDBNull(lector.Item("dc_UMedida")) = True, "", lector.Item("dc_UMedida")))
                        .Cantidad = CDec(IIf(IsDBNull(lector.Item("dc_Cantidad")) = True, 0, lector.Item("dc_Cantidad")))
                        .IdUMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .PrecioSD = CDec(IIf(IsDBNull(lector.Item("dc_PrecioSD")) = True, 0, lector.Item("dc_PrecioSD")))
                        .PrecioCD = CDec(IIf(IsDBNull(lector.Item("dc_PrecioCD")) = True, 0, lector.Item("dc_PrecioCD")))
                        .Importe = CDec(IIf(IsDBNull(lector.Item("dc_Importe")) = True, 0, lector.Item("dc_Importe")))
                        .Descripcion = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                        .IdUMPrincipal = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedidaPrincipal")) = True, 0, lector.Item("IdUnidadMedidaPrincipal")))
                        .NomUMPrincipal = CStr(IIf(IsDBNull(lector.Item("NomUnidadMedidaPrincipal")) = True, "", lector.Item("NomUnidadMedidaPrincipal")))
                        .Percepcion = CDec(IIf(IsDBNull(lector.Item("dc_TasaPercepcion")) = True, 0, lector.Item("dc_TasaPercepcion")))
                        .Detraccion = CDec(IIf(IsDBNull(lector.Item("dc_TasaDetraccion")) = True, 0, lector.Item("dc_TasaDetraccion")))

                        .Glosa = CStr(IIf(IsDBNull(lector.Item("dc_DetalleGlosa")) = True, "", lector.Item("dc_DetalleGlosa")))
                        .StockDisponible = CDec(IIf(IsDBNull(lector.Item("StockDisponible")) = True, 0, lector.Item("StockDisponible")))
                        .IdTienda_PV = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectIdsxIdDocumento(ByVal iddocumento As Integer) As List(Of Entidades.DetalleDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DetalleDocumentoSelectIdxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.DetalleDocumento
                    With obj
                        .IdDetalleDocumento = CInt(IIf(IsDBNull(lector.Item("IdDetalleDocumento")) = True, 0, lector.Item("IdDetalleDocumento")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCantxAtenderxIdDetalleDocumento(ByVal iddetalledocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Decimal
        Dim cmd As New SqlCommand("_DetalleDocumentoSelectCantxAtenderxIdDetalleDocumento", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDetalleDocumento", iddetalledocumento)
        Dim lector As SqlDataReader = cmd.ExecuteReader
        Dim cantxAtender As Decimal = 0
        If lector.Read Then
            cantxAtender = CDec(IIf(IsDBNull(lector.Item("dc_CantxAtender")) = True, 0, lector.Item("dc_CantxAtender")))
        End If
        lector.Close()
        Return cantxAtender
    End Function

    Public Function DeletexIdDocumento(ByVal IdDocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean

        Dim cmd As New SqlCommand("_DetalleDocumentoDeletexIdDocumento", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

        If cmd.ExecuteNonQuery <= 0 Then
            Return False
        End If

        Return True
    End Function

    Public Function MovAlmacen_DetalleDocumentoDeletexIdDocumento(ByVal IdDocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean

        Dim cmd As New SqlCommand("_MovAlmacen_DetalleDocumentoDeletexIdDocumento", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Try
            If cmd.ExecuteNonQuery <= 0 Then
                Return False
            End If
        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return True
    End Function

    Public Sub DocumentoInvInicialDeleteDetallexIdDocumentoxIdSubLinea(ByVal IdDocumento As Integer, ByVal IdSubLinea As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As New SqlCommand("_DocumentoInvInicialDeleteDetallexIdDocumentoxIdSubLinea", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Sub


    '******************** OBTIENE EL DETALLE DOCUMENTO EL CUAL TIENE CANTIDAD X ATENDER > 0
    Public Function DetalleDocumentoSelectDetallexAtenderxIdDocumento(ByVal IdDocumento As Integer, ByVal IdTipoOperacion As Integer) As List(Of Entidades.DetalleDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DetalleDocumentoSelectDetallexAtenderxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.DetalleDocumento
                    With obj
                        .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")) = True, "", lector.Item("prod_Codigo")))
                        .Cantidad = CDec(IIf(IsDBNull(lector.Item("dc_Cantidad")) = True, 0, lector.Item("dc_Cantidad")))
                        .CantxAtender = CDec(IIf(IsDBNull(lector.Item("dc_CantxAtender")) = True, 0, lector.Item("dc_CantxAtender")))
                        .Descuento = CDec(IIf(IsDBNull(lector.Item("dc_Descuento")) = True, 0, lector.Item("dc_Descuento")))
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .Importe = CDec(IIf(IsDBNull(lector.Item("dc_Importe")) = True, 0, lector.Item("dc_Importe")))
                        .PrecioCD = CDec(IIf(IsDBNull(lector.Item("dc_PrecioCD")) = True, 0, lector.Item("dc_PrecioCD")))
                        .PrecioSD = CDec(IIf(IsDBNull(lector.Item("dc_PrecioSD")) = True, 0, lector.Item("dc_PrecioSD")))
                        .TasaDetraccion = CDec(IIf(IsDBNull(lector.Item("dc_TasaDetraccion")) = True, 0, lector.Item("dc_TasaDetraccion")))
                        .TasaPercepcion = CDec(IIf(IsDBNull(lector.Item("dc_TasaPercepcion")) = True, 0, lector.Item("dc_TasaPercepcion")))
                        .Utilidad = CDec(IIf(IsDBNull(lector.Item("dc_Utilidad")) = True, 0, lector.Item("dc_Utilidad")))
                        .NomProducto = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .UMedida = CStr(IIf(IsDBNull(lector.Item("dc_UMedida")) = True, "", lector.Item("dc_UMedida")))
                        .IdUnidadMedidaPrincipal = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedidaPrincipal")) = True, 0, lector.Item("IdUnidadMedidaPrincipal")))
                        .UnidadMedidaPrincipal = CStr(IIf(IsDBNull(lector.Item("UnidadMedidaPrincipal")) = True, "", lector.Item("UnidadMedidaPrincipal")))

                        .IdDetalleDocumento = CInt(IIf(IsDBNull(lector.Item("IdDetalleDocumento")) = True, 0, lector.Item("IdDetalleDocumento")))
                        .IdDetalleAfecto = CInt(IIf(IsDBNull(lector.Item("IdDetalleAfecto")) = True, 0, lector.Item("IdDetalleAfecto")))
                        .NroDocumento = CStr(IIf(IsDBNull(lector.Item("NroDocumento")) = True, "", lector.Item("NroDocumento")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function DetalleDocumentoSelectDetallexAtenderxIdDocumentoDetalle(ByVal IdDocumento As Integer, ByVal IdTipoOperacion As Integer) As List(Of Entidades.DetalleDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DetalleDocumentoSelectDetallexAtenderxIdDocumento_Detalle", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.DetalleDocumento
                    With obj
                        .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")) = True, "", lector.Item("prod_Codigo")))
                        .Cantidad = CDec(IIf(IsDBNull(lector.Item("dc_Cantidad")) = True, 0, lector.Item("dc_Cantidad")))
                        .CantxAtender = CDec(IIf(IsDBNull(lector.Item("dc_CantxAtender")) = True, 0, lector.Item("dc_CantxAtender")))
                        .Descuento = CDec(IIf(IsDBNull(lector.Item("dc_Descuento")) = True, 0, lector.Item("dc_Descuento")))
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .Importe = CDec(IIf(IsDBNull(lector.Item("dc_Importe")) = True, 0, lector.Item("dc_Importe")))
                        .PrecioCD = CDec(IIf(IsDBNull(lector.Item("dc_PrecioCD")) = True, 0, lector.Item("dc_PrecioCD")))
                        .PrecioSD = CDec(IIf(IsDBNull(lector.Item("dc_PrecioSD")) = True, 0, lector.Item("dc_PrecioSD")))
                        .TasaDetraccion = CDec(IIf(IsDBNull(lector.Item("dc_TasaDetraccion")) = True, 0, lector.Item("dc_TasaDetraccion")))
                        .TasaPercepcion = CDec(IIf(IsDBNull(lector.Item("dc_TasaPercepcion")) = True, 0, lector.Item("dc_TasaPercepcion")))

                        .Utilidad = CDec(IIf(IsDBNull(lector.Item("dc_Utilidad")) = True, 0, lector.Item("dc_Utilidad")))
                        .NomProducto = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .UMedida = CStr(IIf(IsDBNull(lector.Item("dc_UMedida")) = True, "", lector.Item("dc_UMedida")))

                        .IdUnidadMedidaPrincipal = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedidaPrincipal")) = True, 0, lector.Item("IdUnidadMedidaPrincipal")))
                        .UnidadMedidaPrincipal = CStr(IIf(IsDBNull(lector.Item("UnidadMedidaPrincipal")) = True, "", lector.Item("UnidadMedidaPrincipal")))

                        .IdDetalleDocumento = CInt(IIf(IsDBNull(lector.Item("IdDetalleDocumento")) = True, 0, lector.Item("IdDetalleDocumento")))
                        .IdDetalleAfecto = CInt(IIf(IsDBNull(lector.Item("IdDetalleAfecto")) = True, 0, lector.Item("IdDetalleAfecto")))
                        .NroDocumento = CStr(IIf(IsDBNull(lector.Item("NroDocumento")) = True, "", lector.Item("NroDocumento")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

End Class

