'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOAsignacion
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaAsignacion(ByVal asignacion As Entidades.Asignacion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(6) {}
        ArrayParametros(0) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(0).Value = asignacion.IdAsignacion
        ArrayParametros(1) = New SqlParameter("@IdTipoPv", SqlDbType.Int)
        ArrayParametros(1).Value = asignacion.IdTipoPv
        ArrayParametros(2) = New SqlParameter("@as_Valor", SqlDbType.Decimal)
        ArrayParametros(2).Value = asignacion.Valor
        ArrayParametros(3) = New SqlParameter("@as_PercentUtil", SqlDbType.Decimal)
        ArrayParametros(3).Value = asignacion.PercentUtil
        ArrayParametros(4) = New SqlParameter("@as_Fecha", SqlDbType.DateTime)
        ArrayParametros(4).Value = asignacion.Fecha
        ArrayParametros(5) = New SqlParameter("@as_Utilidad", SqlDbType.Decimal)
        ArrayParametros(5).Value = asignacion.Utilidad
        ArrayParametros(6) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        ArrayParametros(6).Value = asignacion.IdUsuario
        Return HDAO.Insert(cn, "_AsignacionInsert", ArrayParametros)
    End Function
    Public Function ActualizaAsignacion(ByVal asignacion As Entidades.Asignacion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(7) {}
        ArrayParametros(0) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(0).Value = asignacion.IdProducto
        ArrayParametros(1) = New SqlParameter("@IdTipoPv", SqlDbType.Int)
        ArrayParametros(1).Value = asignacion.IdTipoPv
        ArrayParametros(2) = New SqlParameter("@as_Valor", SqlDbType.Decimal)
        ArrayParametros(2).Value = asignacion.Valor
        ArrayParametros(3) = New SqlParameter("@as_PercentUtil", SqlDbType.Decimal)
        ArrayParametros(3).Value = asignacion.PercentUtil
        ArrayParametros(4) = New SqlParameter("@as_Fecha", SqlDbType.DateTime)
        ArrayParametros(4).Value = asignacion.Fecha
        ArrayParametros(5) = New SqlParameter("@as_Utilidad", SqlDbType.Decimal)
        ArrayParametros(5).Value = asignacion.Utilidad
        ArrayParametros(6) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        ArrayParametros(6).Value = asignacion.IdUsuario
        ArrayParametros(7) = New SqlParameter("@IdAsignacion", SqlDbType.Int)
        ArrayParametros(7).Value = asignacion.IdAsignacion
        Return HDAO.Update(cn, "_AsignacionUpdate", ArrayParametros)
    End Function
End Class
