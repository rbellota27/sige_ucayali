﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



'********************  MIERCOLES 07 ABRIL 2010 HORA 11_13 AM






Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class DAOPerfil_MedioPago
    Inherits DAOMantenedor

    Dim HDAO As New DAO.HelperDAO

    Public Function Perfil_MedioPago_ValidarxIdMedioPagoxIdUsuario(ByVal IdUsuario As Integer, ByVal IdMedioPago As Integer) As Integer

        Dim cont As Integer = 0
        Dim cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
        Try

            Dim p() As SqlParameter = New SqlParameter(1) {}

            p(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
            p(0).Value = IdUsuario
            p(1) = New SqlParameter("@IdMedioPago", SqlDbType.Int)
            p(1).Value = IdMedioPago

            cn.Open()
            cont = CInt(SqlHelper.ExecuteScalar(cn, CommandType.StoredProcedure, "_Perfil_MedioPago_ValidarxIdMedioPagoxIdUsuario", p))

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return cont

    End Function


    Public Sub InsertListxIdPerfil(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal lista As List(Of Entidades.Perfil_MedioPago), ByVal IdPerfil As Integer)
        For i As Integer = 0 To lista.Count - 1
            lista(i).IdPerfil = IdPerfil
        Next
        InsertList(cn, tr, lista)
    End Sub

    Public Sub InsertList(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal lista As List(Of Entidades.Perfil_MedioPago))
        For i As Integer = 0 To lista.Count - 1
            Insert(cn, tr, lista(i))
        Next
    End Sub

    Public Sub Insert(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal obj As Entidades.Perfil_MedioPago)

        Dim p() As SqlParameter = New SqlParameter(2) {}
        p(0) = getParam(obj.IdPerfil, "@IdPerfil", SqlDbType.Int)
        p(1) = getParam(obj.IdMedioPago, "@IdMedioPago", SqlDbType.Int)
        p(2) = getParam(obj.Estado, "@pmp_Estado", SqlDbType.Bit)
        HDAO.InsertaT(cn, "_Perfil_MedioPagoInsert", p, tr)

    End Sub

    Public Function SelectxIdPerfil(ByVal IdPerfil As Integer) As List(Of Entidades.Perfil_MedioPago)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Perfil_MedioPagoSelectxIdPerfil", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("IdPerfil", IdPerfil)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.Perfil_MedioPago)
                Do While lector.Read
                    Dim obj As New Entidades.Perfil_MedioPago
                    obj.IdPerfil = UCInt(lector.Item("IdPerfil"))
                    obj.IdMedioPago = UCInt(lector.Item("IdMedioPago"))
                    obj.MedioPago = UCStr(lector.Item("mp_nombre"))
                    obj.Estado = UCBool(lector.Item("pmp_estado"))

                    lista.Add(obj)
                Loop
                lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw New Exception
        End Try
    End Function

    Public Function DeletexIdPerfil(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal IdPerfil As Integer) As Boolean
        Try
            Dim cmd As New SqlCommand("_Perfil_MedioPagoDeletexIdPerfil", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdPerfil", IdPerfil)
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function


End Class
