﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOProveedorLinea
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectAllActivoxIdLineaIdTipoExistencia(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.ProveedorLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProveedorLineaSelectAllActivoxIdLineaIdTipoExistencia", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idtipoexistencia)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ProveedorLinea)
                Do While lector.Read
                    Dim obj As New Entidades.ProveedorLinea
                    obj.IdProveedor = CInt(lector.Item("IdProveedor"))
                    obj.NomProveedor = CStr(lector.Item("NomProveedor"))
                    obj.IdLinea = CInt(lector.Item("IdLinea"))
                    obj.IdTipoExistencia = CInt(lector.Item("IdTipoExistencia"))
                    obj.Estado = CBool(lector.Item("pl_Estado"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GrabaProveedorLineaT(ByVal IdLinea As Integer, ByVal cn As SqlConnection, ByVal listaProveedorLinea As List(Of Entidades.ProveedorLinea), ByVal T As SqlTransaction) As Boolean
        Dim cmd As SqlCommand
        Try
            For i As Integer = 0 To listaProveedorLinea.Count - 1
                If T IsNot Nothing Then
                    cmd = New SqlCommand("InsUpd_ProveedorLinea", cn, T)
                Else
                    cmd = New SqlCommand("InsUpd_ProveedorLinea", cn)
                End If
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@IdProveedor", listaProveedorLinea.Item(i).IdProveedor)
                cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
                cmd.Parameters.AddWithValue("@IdTipoExistencia", listaProveedorLinea.Item(i).IdTipoExistencia)
                cmd.Parameters.AddWithValue("@plEstado", listaProveedorLinea.Item(i).Estado)
                cmd.Parameters.AddWithValue("@plOrden", listaProveedorLinea.Item(i).Orden)
                Dim cont As Integer = cmd.ExecuteNonQuery
                If cmd.ExecuteNonQuery = 0 Then
                    Return False
                End If
            Next
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class
