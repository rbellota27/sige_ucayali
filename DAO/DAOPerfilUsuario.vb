﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOPerfilUsuario
    Private objConexion As New Conexion
    Public Function PerfilUsuarioSelectxIdPersona(ByVal idpersona As Integer) As List(Of Entidades.PerfilUsuario)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PerfilUsuarioSelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("IdPersona", idpersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PerfilUsuario)
                Do While lector.Read
                    Dim obj As New Entidades.PerfilUsuario(CInt(lector.Item("IdPersona")), CInt(lector.Item("IdPerfil")), CStr(lector.Item("perf_Nombre")), CBool(lector.Item("perfu_Estado")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectCboxIdPersona(ByVal idpersona As Integer) As List(Of Entidades.PerfilUsuario)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PerfilUsuarioSelectCboxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("IdPersona", idpersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PerfilUsuario)
                Do While lector.Read
                    Dim obj As New Entidades.PerfilUsuario(Nothing, CInt(lector.Item("IdPerfil")), CStr(lector.Item("perf_Nombre")), Nothing)
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Sub DeletexIdPersona(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal Idpersona As Integer)
        Dim cmd As New SqlCommand("_PerfilUsuarioDeletexIdPersona", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", Idpersona)
        cmd.ExecuteNonQuery()
    End Sub
    Public Function DeletexIdPersonaxIdPerfil(ByVal cn As SqlConnection, ByVal obj As Entidades.PerfilUsuario, ByVal tr As SqlTransaction) As Boolean
        Dim cmd As New SqlCommand("_PerfilUsuarioDeletexIdPerfilxIdPersona", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idperfil", obj.IdPerfil)
        cmd.Parameters.AddWithValue("@IdPersona", obj.IdPersona)
        If cmd.ExecuteNonQuery = 0 Then
            Throw New Exception
        End If
        Return True
    End Function
    Public Function Insert(ByVal cn As SqlConnection, ByVal obj As Entidades.PerfilUsuario, ByVal tr As SqlTransaction) As Boolean
        Dim cmd As New SqlCommand("_PerfilUsuarioInsert", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idperfil", obj.IdPerfil)
        cmd.Parameters.AddWithValue("@IdPersona", obj.IdPersona)
        cmd.Parameters.AddWithValue("@Estado", obj.Estado)
        If cmd.ExecuteNonQuery = 0 Then
            Throw New Exception
        End If
        Return True
    End Function
    Public Function Update(ByVal cn As SqlConnection, ByVal obj As Entidades.PerfilUsuario, ByVal tr As SqlTransaction) As Boolean
        Dim cmd As New SqlCommand("_PerfilUsuarioUpdate", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idperfil", obj.IdPerfil)
        cmd.Parameters.AddWithValue("@IdPersona", obj.IdPersona)
        cmd.Parameters.AddWithValue("@Estado", obj.Estado)
        If cmd.ExecuteNonQuery = 0 Then
            Throw New Exception
        End If
        Return True
    End Function
    Public Function SelectxIdPersona(ByVal idpersona As Integer) As List(Of Entidades.TiendaAreaPerfilView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TiendaAreaPerfilViewSelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idpersona", idpersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.TiendaAreaPerfilView)
                While lector.Read
                    Dim obj As New Entidades.TiendaAreaPerfilView
                    obj.IdArea = CInt(IIf(IsDBNull(lector.Item("IdArea")) = True, 0, lector.Item("IdArea")))
                    obj.IdPerfil = CInt(IIf(IsDBNull(lector.Item("IdPerfil")) = True, 0, lector.Item("IdPerfil")))
                    obj.IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                    obj.NomArea = CStr(IIf(IsDBNull(lector.Item("ar_NombreLargo")) = True, "", lector.Item("ar_NombreLargo")))
                    obj.NomPerfil = CStr(IIf(IsDBNull(lector.Item("perf_Nombre")) = True, "", lector.Item("perf_Nombre")))
                    obj.NomTienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))
                    lista.Add(obj)
                End While
                lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
