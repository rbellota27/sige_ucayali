﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOCalidadLinea
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectAllActivoxIdLineaIdTipoExistencia(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.CalidadLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CalidadLineaSelectAllActivoxIdLineaIdTipoExistencia", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idtipoexistencia)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CalidadLinea)
                Do While lector.Read
                    Dim objCalidadLinea As New Entidades.CalidadLinea
                    objCalidadLinea.IdCalidad = CInt((lector.Item("IdCalidad")))
                    objCalidadLinea.NomCalidad = CStr(lector.Item("NomCalidad"))
                    objCalidadLinea.Abv = CStr(lector.Item("NomCorto"))
                    objCalidadLinea.IdLinea = CInt(lector.Item("IdLinea"))
                    objCalidadLinea.IdTipoExistencia = CInt(lector.Item("IdTipoExistencia"))
                    objCalidadLinea.Estado = CBool(lector.Item("cl_Estado"))
                    Lista.Add(objCalidadLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAllActivoxIdLineaIdTipoExistenciav2(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.CalidadLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CalidadLineaSelectAllActivoxIdLineaIdTipoExistenciav2", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idtipoexistencia)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CalidadLinea)
                Do While lector.Read
                    Dim objCalidadLinea As New Entidades.CalidadLinea
                    objCalidadLinea.IdCalidadOrden = CStr(lector.Item("IdCalidad"))
                    objCalidadLinea.NomCalidad = CStr(lector.Item("NomCalidad"))
                    objCalidadLinea.Abv = CStr(lector.Item("NomCorto"))
                    Lista.Add(objCalidadLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function GrabaCalidadLineaT(ByVal IdLinea As Integer, ByVal cn As SqlConnection, ByVal listaCalidadLinea As List(Of Entidades.CalidadLinea), ByVal T As SqlTransaction) As Boolean
        Dim cmd As SqlCommand
        Try
            For i As Integer = 0 To listaCalidadLinea.Count - 1
                If T IsNot Nothing Then
                    cmd = New SqlCommand("InsUpd_CalidadLinea", cn, T)
                Else
                    cmd = New SqlCommand("InsUpd_CalidadLinea", cn)
                End If
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
                cmd.Parameters.AddWithValue("@IdCalidad", listaCalidadLinea.Item(i).IdCalidad)
                cmd.Parameters.AddWithValue("@IdTipoExistencia", listaCalidadLinea.Item(i).IdTipoExistencia)
                cmd.Parameters.AddWithValue("@clEstado", listaCalidadLinea.Item(i).Estado)
                cmd.Parameters.AddWithValue("@clOrden", listaCalidadLinea.Item(i).Orden)
                Dim cont As Integer = cmd.ExecuteNonQuery
                If cmd.ExecuteNonQuery = 0 Then
                    Return False
                End If
            Next
            Return True
        Catch ex As Exception
            Return False
        Finally

        End Try
    End Function
End Class
