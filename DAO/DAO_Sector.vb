﻿Imports Entidades
Imports System.Data.Sql
Imports System.Data.SqlClient
Public Class DAO_Sector
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaSector(ByVal sector As Entidades.Sector) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@NOM_SECTOR", SqlDbType.VarChar)
        ArrayParametros(0).Value = sector.nombre
        ArrayParametros(1) = New SqlParameter("@DESC_SECTOR", SqlDbType.VarChar)
        ArrayParametros(1).Value = sector.descripcion
        ArrayParametros(2) = New SqlParameter("@IDPERSONA", SqlDbType.Int)
        ArrayParametros(2).Value = sector.idControlador
        ArrayParametros(3) = New SqlParameter("@ESTADO", SqlDbType.Int)
        ArrayParametros(3).Value = sector.estado
        Return HDAO.Insert(cn, "SP_SECTOR_INSERT", ArrayParametros)
    End Function

    Public Function ActualizarSector(ByVal sector As Entidades.Sector) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SP_SECTOR_UPDATE")
        With cmd
            .CommandType = CommandType.StoredProcedure
            .Connection = cn

            .Parameters.Add(New SqlParameter("@IDSECTOR", SqlDbType.Int, 40, ParameterDirection.Input)).Value = sector.idSector
            .Parameters.Add(New SqlParameter("@NOMBRE", SqlDbType.NVarChar, 50, ParameterDirection.Input)).Value = sector.nombre
            .Parameters.Add(New SqlParameter("@DESCRIPCION", SqlDbType.NVarChar, 150, ParameterDirection.Input)).Value = sector.descripcion
            .Parameters.Add(New SqlParameter("@IDCONTROLADOR", SqlDbType.Int, 40, ParameterDirection.Input)).Value = sector.idControlador
            .Parameters.Add(New SqlParameter("@ESTADO", SqlDbType.Int, 1, ParameterDirection.Input)).Value = sector.estado
            Using cn
                Try
                    cn.Open()
                    If (.ExecuteNonQuery) = 0 Then
                        Throw New Exception("Fracasó la Operación Update")
                    End If
                    cn.Close()
                    Return True
                Catch ex As Exception
                    Return False
                End Try
            End Using
        End With            

    End Function

    Public Function InsertaRelacion_Sector_Sublinea(ByVal sector As Entidades.Sector) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@ID_SECTOR", SqlDbType.Int)
        ArrayParametros(0).Value = sector.idSector
        ArrayParametros(1) = New SqlParameter("@ID_SUBLINEA", SqlDbType.Int)
        ArrayParametros(1).Value = sector.idSublineas
        Return HDAO.Insert(cn, "SP__INSERT_RELACION_SECTOR_SUBLINEA", ArrayParametros)
    End Function

    Public Function DAO_listarSectores(ByVal idDocumento As Integer, ByVal idProducto As Integer) As DataSet
        Dim cn = objConexion.ConexionSIGE()
        Dim cmd As New SqlCommand("SP_TRAZABILIDAD_DISTRIBUCION")
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@IDDOCUMENTO", idDocumento)
        cmd.Parameters.AddWithValue("@IDPRDUCTO", idProducto)
        Using cmd
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet()
            cn.Open()
            Try
                da.Fill(ds)
                Return ds
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                cn.Close()
            End Try
        End Using
    End Function
End Class

