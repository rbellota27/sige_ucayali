﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'********************   JUEVES 10 JUNIO 2010 HORA 12_46 PM

Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAORestriccion_Cliente

    Private objDaoMantenedor As New DAO.DAOMantenedor
    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private reader As SqlDataReader

    Public Sub DeletexIdComisionCabxIdPersona(ByVal IdComisionCab As Integer, ByVal IdPersona As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim P() As SqlParameter = New SqlParameter(1) {}
        P(0) = objDaoMantenedor.getParam(IdComisionCab, "@IdComisionCab", SqlDbType.Int)
        P(1) = objDaoMantenedor.getParam(IdPersona, "@IdPersona", SqlDbType.Int)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_RestriccionComision_Cliente_DeletexIdComisionCabxIdPersona", P)

    End Sub

    Public Function Registrar(ByVal objRestriccionCom_Cliente As Entidades.Restriccion_Cliente, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer

        Dim p() As SqlParameter = New SqlParameter(2) {}
        p(0) = objDaoMantenedor.getParam(objRestriccionCom_Cliente.IdCliente, "@IdCliente", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(objRestriccionCom_Cliente.IdComisionCab, "@IdComisionCab", SqlDbType.Int)
        p(2) = objDaoMantenedor.getParam(objRestriccionCom_Cliente.Estado, "@com_Estado", SqlDbType.Bit)

        Return CInt(SqlHelper.ExecuteScalar(tr, CommandType.StoredProcedure, "_RestriccionComision_Cliente_Registrar", p))

    End Function

    Public Function RestriccionComision_Cliente_SelectxParams(ByVal IdComisionCab As Integer) As List(Of Entidades.Restriccion_Cliente)

        Dim listaRestriccion_Cliente As New List(Of Entidades.Restriccion_Cliente)

        Try

            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = objDaoMantenedor.getParam(IdComisionCab, "@IdComisionCab", SqlDbType.Int)

            cn.Open()
            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_RestriccionComision_Cliente_SelectxParams", p)
            While (reader.Read)

                Dim objRestriccion_Cliente As New Entidades.Restriccion_Cliente

                With objRestriccion_Cliente

                    .IdComisionCab = objDaoMantenedor.UCInt(reader("IdComisionCab"))
                    .IdCliente = objDaoMantenedor.UCInt(reader("IdCliente"))
                    .IdPersona = .IdCliente
                    .Estado = objDaoMantenedor.UCBool(reader("Estado"))
                    .Descripcion = objDaoMantenedor.UCStr(reader("Descripcion"))
                    .Dni = objDaoMantenedor.UCStr(reader("Dni"))
                    .Empresa = objDaoMantenedor.UCStr(reader("Empresa"))
                    .Tienda = objDaoMantenedor.UCStr(reader("Tienda"))
                    .Perfil = objDaoMantenedor.UCStr(reader("Perfil"))

                End With

                listaRestriccion_Cliente.Add(objRestriccion_Cliente)

            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return listaRestriccion_Cliente

    End Function

End Class
