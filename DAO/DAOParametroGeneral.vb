﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************** MARTES 26 ENERO 2010 HORA 03_44 PM
Imports System.Data.SqlClient
Public Class DAOParametroGeneral
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Dim cmd As SqlCommand
    Private ListaParametroGeneral As List(Of Entidades.ParametroGeneral)

    Public Function InsertaParametroGeneral(ByVal ParametroGeneral As Entidades.ParametroGeneral, ByVal IdArea As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        'Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}

        ArrayParametros(0) = New SqlParameter("@par_Descripcion", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(ParametroGeneral.par_Descripcion = Nothing, DBNull.Value, ParametroGeneral.par_Descripcion)

        ArrayParametros(1) = New SqlParameter("@par_Valor", SqlDbType.Decimal)
        ArrayParametros(1).Value = IIf(ParametroGeneral.par_valor = Nothing, DBNull.Value, ParametroGeneral.par_valor)

        ArrayParametros(2) = New SqlParameter("@par_Estado", SqlDbType.Bit)
        ArrayParametros(2).Value = IIf(ParametroGeneral.par_Estado = Nothing, DBNull.Value, ParametroGeneral.par_Estado)

        ArrayParametros(3) = New SqlParameter("@IdArea", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(ParametroGeneral.IdArea = Nothing, DBNull.Value, ParametroGeneral.IdArea)

        HDAO.InsertaT(cn, "_ParametroGeneralInsert", ArrayParametros, tr)
    End Function

    Public Function ActualizaParametroGeneral(ByVal ParametroGeneral As Entidades.ParametroGeneral, ByVal IdArea As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        'Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}

        ArrayParametros(0) = New SqlParameter("@par_Descripcion", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(ParametroGeneral.par_Descripcion = Nothing, DBNull.Value, ParametroGeneral.par_Descripcion)

        ArrayParametros(1) = New SqlParameter("@par_Valor", SqlDbType.Decimal)
        ArrayParametros(1).Value = IIf(ParametroGeneral.par_valor = Nothing, DBNull.Value, ParametroGeneral.par_valor)

        ArrayParametros(2) = New SqlParameter("@par_Estado", SqlDbType.Bit)
        ArrayParametros(2).Value = IIf(ParametroGeneral.par_Estado = Nothing, DBNull.Value, ParametroGeneral.par_Estado)

        ArrayParametros(3) = New SqlParameter("@IdArea", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(ParametroGeneral.IdArea = Nothing, DBNull.Value, ParametroGeneral.IdArea)

        ArrayParametros(4) = New SqlParameter("@IdParametro", SqlDbType.Int)
        ArrayParametros(4).Value = IIf(ParametroGeneral.IdParametro = Nothing, DBNull.Value, ParametroGeneral.IdParametro)

        'HDAO.Update(cn, "_ParametroGeneralUpdate", ArrayParametros)

        HDAO.InsertaT(cn, "_ParametroGeneralUpdate", ArrayParametros, tr)
    End Function

    Public Function SelectCboArea() As List(Of Entidades.Area)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ParametroGeneralSelectCboArea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Area)
                Do While lector.Read
                    Dim obj As New Entidades.Area
                    obj.Id = CInt(IIf(IsDBNull(lector("IdArea")) = True, 0, lector("IdArea")))
                    obj.DescripcionCorta = CStr(IIf(IsDBNull(lector("ar_NombreCorto")) = True, "", lector("ar_NombreCorto")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ParametroGeneralSelectActivoValorxIdParametro(ByVal Idparametro As Integer, ByVal cn As SqlConnection) As Decimal

        cmd = New SqlCommand("_ParametroGeneralSelectActivoValorxIdParametro", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdParametro", Idparametro)

        Return CDec(cmd.ExecuteScalar)

    End Function

    Public Function SelectxIdAreaxIdParametro(ByVal IdArea As Integer) As List(Of Entidades.ParametroGeneral)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ParametroGeneralxIdArea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdArea", IdArea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ParametroGeneral)
                Do While lector.Read
                    Dim obj As New Entidades.ParametroGeneral

                    obj.IdParametro = CInt(IIf(IsDBNull(lector("IdParametro")) = True, 0, lector("IdParametro")))
                    obj.par_Descripcion = CStr(IIf(IsDBNull(lector("par_Descripcion")) = True, 0, lector("par_Descripcion")))
                    obj.par_valor = CDec(IIf(IsDBNull(lector("par_valor")) = True, 0, lector("par_valor")))
                    obj.par_Estado = CBool(IIf(IsDBNull(lector("par_Estado")) = True, 0, lector("par_Estado")))
                    obj.IdArea = CInt(IIf(IsDBNull(lector("IdArea")) = True, 0, lector("IdArea")))

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
