'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.







'*********************   VIERNES 26 FEB 2010   HORA  02_40 PM







Imports System.Data.SqlClient

Public Class DAOPagoCaja
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Sub InsertaPagoCaja(ByVal cn As SqlConnection, ByVal pagocaja As Entidades.PagoCaja, ByVal T As SqlTransaction)
        'Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(16) {}

        ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = pagocaja.IdDocumento
        ArrayParametros(1) = New SqlParameter("@IdMedioPago", SqlDbType.Int)
        ArrayParametros(1).Value = pagocaja.IdMedioPago
        ArrayParametros(2) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        ArrayParametros(2).Value = pagocaja.IdMoneda
        ArrayParametros(3) = New SqlParameter("@IdTipoMovimiento", SqlDbType.Int)
        ArrayParametros(3).Value = pagocaja.IdTipoMovimiento
        ArrayParametros(4) = New SqlParameter("@pc_Monto", SqlDbType.Decimal)
        ArrayParametros(4).Value = pagocaja.getMonto
        ArrayParametros(5) = New SqlParameter("@pc_Factor", SqlDbType.Decimal)
        ArrayParametros(5).Value = pagocaja.Factor
        ArrayParametros(6) = New SqlParameter("@pc_Efectivo", SqlDbType.Decimal)
        ArrayParametros(6).Value = IIf(pagocaja.getEfectivo = Nothing, DBNull.Value, pagocaja.getEfectivo)
        ArrayParametros(7) = New SqlParameter("@pc_Vuelto", SqlDbType.Decimal)
        ArrayParametros(7).Value = IIf(pagocaja.Vuelto = Nothing, DBNull.Value, pagocaja.Vuelto)
        ArrayParametros(8) = New SqlParameter("@pc_Donacion", SqlDbType.Decimal)
        ArrayParametros(8).Value = IIf(pagocaja.Donacion = Nothing, DBNull.Value, pagocaja.Donacion)
        ArrayParametros(9) = New SqlParameter("@pc_Redondeo", SqlDbType.Decimal)
        ArrayParametros(9).Value = IIf(pagocaja.Redondeo = Nothing, DBNull.Value, pagocaja.Redondeo)
        ArrayParametros(10) = New SqlParameter("@pc_NumeroOp", SqlDbType.VarChar)
        ArrayParametros(10).Value = IIf(pagocaja.NumeroOp = Nothing, DBNull.Value, pagocaja.NumeroOp)
        ArrayParametros(11) = New SqlParameter("@pc_NumeroCheque", SqlDbType.VarChar)
        ArrayParametros(11).Value = IIf(pagocaja.NumeroCheque = Nothing, DBNull.Value, pagocaja.NumeroCheque)
        ArrayParametros(12) = New SqlParameter("@pc_FechaACobrar", SqlDbType.Decimal)
        ArrayParametros(12).Value = IIf(pagocaja.FechaACobrar = Nothing, DBNull.Value, pagocaja.FechaACobrar)
        ArrayParametros(13) = New SqlParameter("@IdBanco", SqlDbType.Int)
        ArrayParametros(13).Value = IIf(pagocaja.IdBanco = Nothing, DBNull.Value, pagocaja.IdBanco)
        ArrayParametros(14) = New SqlParameter("@IdCuentaBancaria", SqlDbType.Int)
        ArrayParametros(14).Value = IIf(pagocaja.IdCuentaBancaria = Nothing, DBNull.Value, pagocaja.IdCuentaBancaria)
        ArrayParametros(15) = New SqlParameter("@IdPost", SqlDbType.Int)
        ArrayParametros(15).Value = IIf(pagocaja.IdPost = Nothing, DBNull.Value, pagocaja.IdPost)
        ArrayParametros(16) = New SqlParameter("@IdTarjeta", SqlDbType.Int)
        ArrayParametros(16).Value = IIf(pagocaja.IdTarjeta = Nothing, DBNull.Value, pagocaja.IdTarjeta)
        HDAO.InsertaT(cn, "_PagoCajaInsert", ArrayParametros, T)

    End Sub
    'Public Function ActualizaPagoCaja(ByVal pagocaja As Entidades.PagoCaja) As Boolean
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim ArrayParametros() As SqlParameter = New SqlParameter(11) {}

    '    ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
    '    ArrayParametros(0).Value = pagocaja.IdDocumento
    '    ArrayParametros(1) = New SqlParameter("@IdMedioPago", SqlDbType.Int)
    '    ArrayParametros(1).Value = pagocaja.IdMedioPago
    '    ArrayParametros(2) = New SqlParameter("@IdTipoMovimiento", SqlDbType.Int)
    '    ArrayParametros(2).Value = pagocaja.IdTipoMovimiento
    '    ArrayParametros(3) = New SqlParameter("@pc_Monto", SqlDbType.Decimal)
    '    ArrayParametros(3).Value = pagocaja.Monto
    '    ArrayParametros(4) = New SqlParameter("@pc_Efectivo", SqlDbType.Decimal)
    '    ArrayParametros(4).Value = pagocaja.Efectivo
    '    ArrayParametros(4) = New SqlParameter("@pc_Efectivo", SqlDbType.Decimal)
    '    ArrayParametros(4).Value = IIf(pagocaja.Efectivo = Nothing, DBNull.Value, pagocaja.Efectivo)
    '    ArrayParametros(5) = New SqlParameter("@pc_Vuelto", SqlDbType.Decimal)
    '    ArrayParametros(5).Value = IIf(pagocaja.Vuelto = Nothing, DBNull.Value, pagocaja.Vuelto)
    '    ArrayParametros(6) = New SqlParameter("@pc_Donacion", SqlDbType.Decimal)
    '    ArrayParametros(6).Value = IIf(pagocaja.Donacion = Nothing, DBNull.Value, pagocaja.Donacion)
    '    ArrayParametros(7) = New SqlParameter("@pc_Redondeo", SqlDbType.Decimal)
    '    ArrayParametros(7).Value = IIf(pagocaja.Redondeo = Nothing, DBNull.Value, pagocaja.Redondeo)
    '    ArrayParametros(8) = New SqlParameter("@pc_NumeroOp", SqlDbType.VarChar)
    '    ArrayParametros(8).Value = IIf(pagocaja.NumeroOp = Nothing, DBNull.Value, pagocaja.NumeroOp)
    '    ArrayParametros(9) = New SqlParameter("@pc_FechaACobrar", SqlDbType.Decimal)
    '    ArrayParametros(9).Value = IIf(pagocaja.FechaACobrar = Nothing, DBNull.Value, pagocaja.FechaACobrar)
    '    ArrayParametros(10) = New SqlParameter("@IdBanco", SqlDbType.Int)
    '    ArrayParametros(10).Value = IIf(pagocaja.IdBanco = Nothing, DBNull.Value, pagocaja.IdBanco)
    '    ArrayParametros(11) = New SqlParameter("@IdCuentaBancaria", SqlDbType.Int)
    '    ArrayParametros(11).Value = IIf(pagocaja.IdCuentaBancaria = Nothing, DBNull.Value, pagocaja.IdCuentaBancaria)


    'End Function 
    Public Sub DeletexIdDocumento(ByVal cn As SqlConnection, ByVal T As SqlTransaction, ByVal IdDocumento As Integer)

        Dim cmd As New SqlCommand("_PagoCajaDeletexIdDocumento", cn, T)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.ExecuteNonQuery()

    End Sub
    Public Function fnInsertaPagoCaja(ByVal pagocaja As Entidades.PagoCaja) As Integer
        'Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(14) {}

        ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = pagocaja.IdDocumento
        ArrayParametros(1) = New SqlParameter("@IdMedioPago", SqlDbType.Int)
        ArrayParametros(1).Value = pagocaja.IdMedioPago
        ArrayParametros(2) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        ArrayParametros(2).Value = pagocaja.IdMoneda
        ArrayParametros(3) = New SqlParameter("@IdTipoMovimiento", SqlDbType.Int)
        ArrayParametros(3).Value = pagocaja.IdTipoMovimiento
        ArrayParametros(4) = New SqlParameter("@pc_Monto", SqlDbType.Decimal)
        ArrayParametros(4).Value = pagocaja.getMonto
        ArrayParametros(5) = New SqlParameter("@pc_Factor", SqlDbType.Decimal)
        ArrayParametros(5).Value = pagocaja.Factor
        ArrayParametros(6) = New SqlParameter("@pc_Efectivo", SqlDbType.Decimal)
        ArrayParametros(6).Value = IIf(pagocaja.getEfectivo = Nothing, DBNull.Value, pagocaja.getEfectivo)
        ArrayParametros(7) = New SqlParameter("@pc_Vuelto", SqlDbType.Decimal)
        ArrayParametros(7).Value = IIf(pagocaja.Vuelto = Nothing, DBNull.Value, pagocaja.Vuelto)
        ArrayParametros(8) = New SqlParameter("@pc_Donacion", SqlDbType.Decimal)
        ArrayParametros(8).Value = IIf(pagocaja.Donacion = Nothing, DBNull.Value, pagocaja.Donacion)
        ArrayParametros(9) = New SqlParameter("@pc_Redondeo", SqlDbType.Decimal)
        ArrayParametros(9).Value = IIf(pagocaja.Redondeo = Nothing, DBNull.Value, pagocaja.Redondeo)
        ArrayParametros(10) = New SqlParameter("@pc_NumeroOp", SqlDbType.VarChar)
        ArrayParametros(10).Value = IIf(pagocaja.NumeroOp = Nothing, DBNull.Value, pagocaja.NumeroOp)
        ArrayParametros(11) = New SqlParameter("@pc_NumeroCheque", SqlDbType.VarChar)
        ArrayParametros(11).Value = IIf(pagocaja.NumeroCheque = Nothing, DBNull.Value, pagocaja.NumeroCheque)
        ArrayParametros(12) = New SqlParameter("@pc_FechaACobrar", SqlDbType.Decimal)
        ArrayParametros(12).Value = IIf(pagocaja.FechaACobrar = Nothing, DBNull.Value, pagocaja.FechaACobrar)
        ArrayParametros(13) = New SqlParameter("@IdBanco", SqlDbType.Int)
        ArrayParametros(13).Value = IIf(pagocaja.IdBanco = Nothing, DBNull.Value, pagocaja.IdBanco)
        ArrayParametros(14) = New SqlParameter("@IdCuentaBancaria", SqlDbType.Int)
        ArrayParametros(14).Value = IIf(pagocaja.IdCuentaBancaria = Nothing, DBNull.Value, pagocaja.IdCuentaBancaria)
        Dim _objConexion As New Conexion
        Return Convert.ToInt32(Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(_objConexion.ConexionSIGE.ConnectionString.ToString(), CommandType.StoredProcedure, "_PagoCajaInsert", ArrayParametros))
    End Function
    Public Function SelectxIdDocumento(ByVal IdDocumento As Integer) As Entidades.PagoCaja
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PagoCajaSelectxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim PagoCaja As New Entidades.PagoCaja
                If lector.Read Then
                    With PagoCaja
                        .IdMedioPago = CInt(IIf(IsDBNull(lector.Item("IdMedioPago")) = True, 0, lector.Item("IdMedioPago")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .Monto = CDec(IIf(IsDBNull(lector.Item("pc_Monto")) = True, 0, lector.Item("pc_Monto")))
                        .Efectivo = CDec(IIf(IsDBNull(lector.Item("pc_Efectivo")) = True, 0, lector.Item("pc_Efectivo")))
                        .Vuelto = CDec(IIf(IsDBNull(lector.Item("pc_Vuelto")) = True, 0, lector.Item("pc_Vuelto")))
                        .NumeroOp = CStr(IIf(IsDBNull(lector.Item("pc_NumeroOp")) = True, "", lector.Item("pc_NumeroOp")))
                        .NumeroCheque = CStr(IIf(IsDBNull(lector.Item("pc_NumeroCheque")) = True, "", lector.Item("pc_NumeroCheque")))
                        .FechaACobrar = CDate(IIf(IsDBNull(lector.Item("pc_FechaACobrar")) = True, Nothing, lector.Item("pc_FechaACobrar")))
                        .IdBanco = CInt(IIf(IsDBNull(lector.Item("IdBanco")) = True, 0, lector.Item("IdBanco")))
                        .IdCuentaBancaria = CInt(IIf(IsDBNull(lector.Item("IdCuentaBancaria")) = True, 0, lector.Item("IdCuentaBancaria")))
                    End With
                End If
                Return PagoCaja
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectListaxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.PagoCaja)

        Dim lista As New List(Of Entidades.PagoCaja)
        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Try
            Dim cmd As New SqlCommand("_PagoCajaSelectxIdDocumento", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            cn.Open()
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            While lector.Read

                Dim obj As New Entidades.PagoCaja
                With obj

                    .IdDocumento = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                    .IdMedioPago = CInt(IIf(IsDBNull(lector("IdMedioPago")) = True, 0, lector("IdMedioPago")))
                    .IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")) = True, 0, lector("IdMoneda")))
                    .IdTipoMovimiento = CInt(IIf(IsDBNull(lector("IdTipoMovimiento")) = True, 0, lector("IdTipoMovimiento")))


                    .Monto = CDec(IIf(IsDBNull(lector("pc_Monto")) = True, 0, lector("pc_Monto")))
                    .Factor = CInt(IIf(IsDBNull(lector("pc_Factor")) = True, 0, lector("pc_Factor")))
                    .Efectivo = CDec(IIf(IsDBNull(lector("pc_Efectivo")) = True, 0, lector("pc_Efectivo")))
                    .Vuelto = CDec(IIf(IsDBNull(lector("pc_Vuelto")) = True, 0, lector("pc_Vuelto")))
                    .Donacion = CDec(IIf(IsDBNull(lector("pc_Donacion")) = True, 0, lector("pc_Donacion")))


                    .Redondeo = CDec(IIf(IsDBNull(lector("pc_Redondeo")) = True, 0, lector("pc_Redondeo")))
                    .NumeroOp = CStr(IIf(IsDBNull(lector("pc_NumeroOp")) = True, "", lector("pc_NumeroOp")))
                    .NumeroCheque = CStr(IIf(IsDBNull(lector("pc_NumeroCheque")) = True, "", lector("pc_NumeroCheque")))
                    .FechaACobrar = CDate(IIf(IsDBNull(lector("pc_FechaACobrar")) = True, Nothing, lector("pc_FechaACobrar")))

                    .IdBanco = CInt(IIf(IsDBNull(lector("IdBanco")) = True, 0, lector("IdBanco")))
                    .IdCuentaBancaria = CInt(IIf(IsDBNull(lector("IdCuentaBancaria")) = True, 0, lector("IdCuentaBancaria")))


                    .NomMedioPago = CStr(IIf(IsDBNull(lector("mp_Nombre")) = True, "", lector("mp_Nombre")))
                    .NomMoneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")) = True, "", lector("mon_Simbolo")))
                    .NomBanco = CStr(IIf(IsDBNull(lector("ban_Nombre")) = True, "", lector("ban_Nombre")))
                    .EfectivoInicial = CDec(IIf(IsDBNull(lector("pc_Efectivo")) = True, 0, lector("pc_Efectivo")))

                    .NomMonedaDestino = CStr(IIf(IsDBNull(lector("NomMonedaDestino")) = True, "", lector("NomMonedaDestino")))
                    .IdMonedaDestino = CInt(IIf(IsDBNull(lector("IdMonedaDestino")) = True, 0, lector("IdMonedaDestino")))
                    .MontoEquivalenteDestino = CDec(IIf(IsDBNull(lector("MontoEquivalenteDestino")) = True, 0, lector("MontoEquivalenteDestino")))
                    .MontoEquivalenteVuelto = CDec(IIf(IsDBNull(lector("MontoEquivalenteVuelto")) = True, 0, lector("MontoEquivalenteVuelto")))
                    .IdMedioPagoInterfaz = CInt(IIf(IsDBNull(lector("IdMedioPagoInterfaz")) = True, 0, lector("IdMedioPagoInterfaz")))

                End With

                lista.Add(obj)

            End While
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return lista
    End Function
End Class
