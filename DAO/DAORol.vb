'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'29-01-2010 Se agrego SelectCbo

Imports System.Data.SqlClient

Public Class DAORol
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaRol(ByVal rol As Entidades.Rol) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@rol_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = rol.Nombre
        ArrayParametros(1) = New SqlParameter("@rol_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = rol.Estado
        Return HDAO.Insert(cn, "_RolInsert", ArrayParametros)
    End Function
    Public Function ActualizaRol(ByVal rol As Entidades.Rol) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@rol_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = rol.Nombre
        ArrayParametros(1) = New SqlParameter("@rol_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = rol.Estado
        ArrayParametros(2) = New SqlParameter("@IdRol", SqlDbType.Int)
        ArrayParametros(2).Value = rol.Id
        Return HDAO.Update(cn, "_RolUpdate", ArrayParametros)
    End Function
    Public Function SelectAll() As List(Of Entidades.Rol)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RolSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Rol)
                Do While lector.Read
                    Dim obj As New Entidades.Rol
                    obj.Id = CInt(lector.Item("IdRol"))
                    obj.Estado = CStr(lector.Item("rol_Estado"))
                    obj.Nombre = CStr(lector.Item("rol_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Rol)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RolSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Rol)
                Do While lector.Read
                    Dim obj As New Entidades.Rol
                    obj.Id = CInt(lector.Item("IdRol"))
                    obj.Estado = CStr(lector.Item("rol_Estado"))
                    obj.Nombre = CStr(lector.Item("rol_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Rol)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RolSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Rol)
                Do While lector.Read
                    Dim obj As New Entidades.Rol
                    obj.Id = CInt(lector.Item("IdRol"))
                    obj.Estado = CStr(lector.Item("rol_Estado"))
                    obj.Nombre = CStr(lector.Item("rol_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Rol)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RolSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Rol)
                Do While lector.Read
                    Dim obj As New Entidades.Rol
                    obj.Id = CInt(lector.Item("IdRol"))
                    obj.Estado = CStr(lector.Item("rol_Estado"))
                    obj.Nombre = CStr(lector.Item("rol_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Rol)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RolSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Rol)
                Do While lector.Read
                    Dim obj As New Entidades.Rol
                    obj.Id = CInt(lector.Item("IdRol"))
                    obj.Estado = CStr(lector.Item("rol_Estado"))
                    obj.Nombre = CStr(lector.Item("rol_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Rol)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RolSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Rol)
                Do While lector.Read
                    Dim obj As New Entidades.Rol
                    obj.Id = CInt(lector.Item("IdRol"))
                    obj.Estado = CStr(lector.Item("rol_Estado"))
                    obj.Nombre = CStr(lector.Item("rol_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Rol)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RolSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Rol)
                Do While lector.Read
                    Dim obj As New Entidades.Rol
                    obj.Id = CInt(lector.Item("IdRol"))
                    obj.Estado = CStr(lector.Item("rol_Estado"))
                    obj.Nombre = CStr(lector.Item("rol_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectCboProved() As List(Of Entidades.Rol)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RolSelectCboProveedor", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Rol)
                Do While lector.Read
                    Dim obj As New Entidades.Rol
                    obj.Id = CInt(lector.Item("Id"))
                    obj.Nombre = CStr(lector.Item("Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectCbo() As List(Of Entidades.Rol)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RolSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Rol)
                Do While lector.Read
                    Dim obj As New Entidades.Rol
                    obj.Id = CInt(lector.Item("Id"))
                    obj.Nombre = CStr(lector.Item("Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectCboCliente() As List(Of Entidades.Rol)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RolSelectCboCliente", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Rol)
                Do While lector.Read
                    Dim obj As New Entidades.Rol
                    obj.Id = CInt(lector.Item("Id"))
                    obj.Nombre = CStr(lector.Item("Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function


#Region "mantenimiento Persona"

    Public Function listarExisteRolEmpresa() As List(Of Entidades.Rol_Empresa)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                Dim cmd As New SqlCommand("_listarExisteRolEmpresa", cn)
                cmd.CommandType = CommandType.StoredProcedure
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Rol_Empresa)
                Do While lector.Read
                    Dim obj As New Entidades.Rol_Empresa
                    obj.IdEmpresa = CInt(lector("IdEmpresa"))
                    obj.Empresa = CStr(IIf(IsDBNull(lector("Empresa")), "", lector("Empresa")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function
    Public Function listarRolxEmpresaUsuarioProveedor(ByVal idempresa As Integer, ByVal idPersona As Integer) As List(Of Entidades.Rol_Empresa)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                Dim cmd As New SqlCommand("_listarRolxEmpresausuarioProveedor", cn)
                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.AddWithValue("@idempresa", idempresa)
                cmd.Parameters.AddWithValue("@idpersona", idPersona)

                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Rol_Empresa)

                Do While lector.Read
                    Dim obj As New Entidades.Rol_Empresa
                    obj.IdRol = CInt(lector("IdRol"))
                    obj.Rol = CStr(IIf(IsDBNull(lector("Rol")), "", lector("Rol")))


                    'obj.IdPersona = CInt(lector("IdPersona"))
                    'obj.Rol = CStr(IIf(IsDBNull(lector("Rol")), "", lector("Rol")))


                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function listarRolxEmpresaUsuario(ByVal idempresa As Integer, ByVal idPersona As Integer) As List(Of Entidades.Rol_Empresa)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                Dim cmd As New SqlCommand("_listarRolxEmpresausuario", cn)
                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.AddWithValue("@idempresa", idempresa)
                cmd.Parameters.AddWithValue("@idpersona", idPersona)
            
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Rol_Empresa)
               
                Do While lector.Read
                    Dim obj As New Entidades.Rol_Empresa
                    obj.IdRol = CInt(lector("IdRol"))
                    obj.Rol = CStr(IIf(IsDBNull(lector("Rol")), "", lector("Rol")))


                    'obj.IdPersona = CInt(lector("IdPersona"))
                    'obj.Rol = CStr(IIf(IsDBNull(lector("Rol")), "", lector("Rol")))


                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function
    Public Function listarRolxEmpresaProveedor(ByVal idempresa As Integer) As List(Of Entidades.Rol_Empresa)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                Dim cmd As New SqlCommand("_listarRolxEmpresaProveedor", cn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@idempresa", idempresa)



                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Rol_Empresa)
                Do While lector.Read
                    Dim obj As New Entidades.Rol_Empresa
                    obj.IdRol = CInt(lector("IdRol"))
                    obj.Rol = CStr(IIf(IsDBNull(lector("Rol")), "", lector("Rol")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function listarRolxEmpresa(ByVal idempresa As Integer) As List(Of Entidades.Rol_Empresa)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                Dim cmd As New SqlCommand("_listarRolxEmpresa", cn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@idempresa", idempresa)



                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Rol_Empresa)
                Do While lector.Read
                    Dim obj As New Entidades.Rol_Empresa
                    obj.IdRol = CInt(lector("IdRol"))
                    obj.Rol = CStr(IIf(IsDBNull(lector("Rol")), "", lector("Rol")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function listarMotivoBaja() As List(Of Entidades.MotivoBaja)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                Dim cmd As New SqlCommand("_listarMotivoBaja", cn)
                cmd.CommandType = CommandType.StoredProcedure
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MotivoBaja)
                Do While lector.Read
                    Dim obj As New Entidades.MotivoBaja
                    obj.Id = CInt(lector("Id"))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector("Descripcion")), "", lector("Descripcion")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function


    Public Function SelectxIdPersona(ByVal idPersona As Integer) As List(Of Entidades.RolPersona)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_listarRolPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idpersona", idPersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.RolPersona)
                Do While lector.Read
                    Dim obj As New Entidades.RolPersona
                    With obj
                        .IdPersona = CInt(lector("Idpersona"))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector("IdEmpresa")), 0, lector("IdEmpresa")))
                        .IdRol = CInt(IIf(IsDBNull(lector("IdRol")), 0, lector("IdRol")))
                        .Estado = CBool(IIf(IsDBNull(lector("rp_Estado")), False, lector("rp_Estado")))
                        .FechaAlta = CDate(IIf(IsDBNull(lector("rp_FechaAlta")), Nothing, lector("rp_FechaAlta")))
                        .FechaBaja = CDate(IIf(IsDBNull(lector("rp_FechaBaja")), Nothing, lector("rp_FechaBaja")))
                        .IdMotivoBaja = CInt(IIf(IsDBNull(lector("IdMotivoBaja")), 0, lector("IdMotivoBaja")))
                        Lista.Add(obj)
                    End With
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function



#End Region

End Class
