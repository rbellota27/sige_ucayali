'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.






'**************************   MARTES 11 MAYO 2010 HORA 10_45 AM








Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data



Public Class DAOTipoDocumento
    Inherits DAOMantenedor

    Private HDAO As New DAO.HelperDAO
    Private reader As SqlDataReader


  

    Public Function SelectTipoDocumentoExternoActivo() As List(Of Entidades.TipoDocumento)


        Dim lista As New List(Of Entidades.TipoDocumento)

        Try

            MyBase.Cn = (New DAO.Conexion).ConexionSIGE
            MyBase.Cn.Open()

            reader = SqlHelper.ExecuteReader(MyBase.Cn, CommandType.StoredProcedure, "_TipoDocumentoSelectTipoDocumentoExternoActivo")

            While (reader.Read)

                Dim obj As New Entidades.TipoDocumento
                With obj
                    .Id = MyBase.UCInt(reader("IdTipoDocumento"))
                    .Descripcion = MyBase.UCStr(reader("tdoc_NombreLargo"))
                End With
                lista.Add(obj)


            End While
            reader.Close()


        Catch ex As Exception
            Throw ex
        Finally
            If (MyBase.Cn.State = ConnectionState.Open) Then MyBase.Cn.Close()
        End Try

        Return lista

    End Function


    Public Function InsertaTipoDocumento(ByVal tipodocumento As Entidades.TipoDocumento, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer
        'Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(7) {}
        ArrayParametros(0) = New SqlParameter("@tdoc_NombreLargo", SqlDbType.VarChar)
        ArrayParametros(0).Value = tipodocumento.Descripcion
        ArrayParametros(1) = New SqlParameter("@tdoc_NombreCorto", SqlDbType.VarChar)
        ArrayParametros(1).Value = tipodocumento.DescripcionCorto
        ArrayParametros(2) = New SqlParameter("@tdoc_CodigoSunat", SqlDbType.VarChar)
        ArrayParametros(2).Value = tipodocumento.CodigoSunat
        ArrayParametros(3) = New SqlParameter("@tdoc_ExportarConta", SqlDbType.Char)
        ArrayParametros(3).Value = tipodocumento.ExportarConta
        ArrayParametros(4) = New SqlParameter("@tdoc_Estado", SqlDbType.Char)
        ArrayParametros(4).Value = tipodocumento.Estado
        ArrayParametros(5) = New SqlParameter("@tdoc_Externo", SqlDbType.Bit)
        ArrayParametros(5).Value = tipodocumento.Externo
        ArrayParametros(6) = New SqlParameter("@tdoc_Cancelable", SqlDbType.Bit)
        ArrayParametros(6).Value = tipodocumento.Cancelable
        ArrayParametros(7) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        ArrayParametros(7).Direction = ParameterDirection.Output
        'Return HDAO.Insert(cn, "_TipoDocumentoInsert", ArrayParametros)
        Dim cmd As New SqlCommand("_TipoDocumentoInsert", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        cmd.ExecuteNonQuery()
        Return CInt(cmd.Parameters("@IdTipoDocumento").Value)
    End Function

    Public Function ActualizaTipoDocumento(ByVal tipodocumento As Entidades.TipoDocumento, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        'Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(7) {}
        ArrayParametros(0) = New SqlParameter("@tdoc_NombreLargo", SqlDbType.VarChar)
        ArrayParametros(0).Value = tipodocumento.Descripcion
        ArrayParametros(1) = New SqlParameter("@tdoc_NombreCorto", SqlDbType.VarChar)
        ArrayParametros(1).Value = tipodocumento.DescripcionCorto
        ArrayParametros(2) = New SqlParameter("@tdoc_CodigoSunat", SqlDbType.VarChar)
        ArrayParametros(2).Value = tipodocumento.CodigoSunat
        ArrayParametros(3) = New SqlParameter("@tdoc_ExportarConta", SqlDbType.Char)
        ArrayParametros(3).Value = tipodocumento.ExportarConta
        ArrayParametros(4) = New SqlParameter("@tdoc_Estado", SqlDbType.Char)
        ArrayParametros(4).Value = tipodocumento.Estado
        ArrayParametros(5) = New SqlParameter("@tdoc_Externo", SqlDbType.Bit)
        ArrayParametros(5).Value = tipodocumento.Externo
        ArrayParametros(6) = New SqlParameter("@tdoc_Cancelable", SqlDbType.Bit)
        ArrayParametros(6).Value = tipodocumento.Cancelable
        ArrayParametros(7) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        ArrayParametros(7).Value = IIf(tipodocumento.Id = Nothing, DBNull.Value, tipodocumento.Id)
        'Return HDAO.Update(cn, "_TipoDocumentoUpdate", ArrayParametros)
        Dim cmd As New SqlCommand("_TipoDocumentoUpdate", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        If (cmd.ExecuteNonQuery = 0) Then
            Return False
        End If
        Return True
    End Function
    Public Function getTipoDocumento(ByVal lector As SqlDataReader) As Entidades.TipoDocumento

        Dim Obj As New Entidades.TipoDocumento
        With Obj
            .Id = UCInt(lector("IdTipoDocumento"))
            .Descripcion = UCStr(lector("tdoc_NombreLargo"))
            .DescripcionCorto = UCStr(lector("tdoc_NombreCorto"))
            .CodigoSunat = UCStr(lector("tdoc_CodigoSunat"))
            .ExportarConta = UCStr(lector("tdoc_ExportarConta"))
            .Externo = UCBool(lector("tdoc_Externo"))
            .Cancelable = UCBool(lector("tdoc_Cancelable"))
            .Estado = UCStr(lector("tdoc_Estado"))
        End With
        Return Obj
    End Function
    ''AGREGADO 10/07/14
    Public Function SelectCboTipoDocumentoProgPago() As List(Of Entidades.TipoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SelectcboTipoDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDocumento)
                Do While lector.Read
                    Dim objTipoDoc As New Entidades.TipoDocumento
                    objTipoDoc.Id = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                    objTipoDoc.Descripcion = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreLargo")) = True, "", lector.Item("tdoc_NombreLargo")))
                    Lista.Add(objTipoDoc)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function getListTipoDocumento(ByVal lector As SqlDataReader) As List(Of Entidades.TipoDocumento)
        Dim L As New List(Of Entidades.TipoDocumento)
        Do While lector.Read
            L.Add(getTipoDocumento(lector))
        Loop
        Return L
    End Function


    Public Function SelectAll() As List(Of Entidades.TipoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure

        Try
            Using cn
                cn.Open()

                Return getListTipoDocumento(cmd.ExecuteReader(Data.CommandBehavior.CloseConnection))
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.TipoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Try
            Using cn
                cn.Open()
                Return getListTipoDocumento(cmd.ExecuteReader(Data.CommandBehavior.CloseConnection))
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.TipoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            Using cn
                cn.Open()
                Return getListTipoDocumento(cmd.ExecuteReader(Data.CommandBehavior.CloseConnection))
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Try
            Using cn
                cn.Open()
                Return getListTipoDocumento(cmd.ExecuteReader(Data.CommandBehavior.CloseConnection))
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.TipoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            Using cn
                cn.Open()
                Return getListTipoDocumento(cmd.ExecuteReader(Data.CommandBehavior.CloseConnection))
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Try
            Using cn
                cn.Open()
                Return getListTipoDocumento(cmd.ExecuteReader(Data.CommandBehavior.CloseConnection))
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxCodSunat(ByVal codSunat As String) As List(Of Entidades.TipoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoSelectxCodSunat", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Cod", codSunat)
        Try
            Using cn
                cn.Open()
                Return getListTipoDocumento(cmd.ExecuteReader(Data.CommandBehavior.CloseConnection))
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function



    Public Function SelectCboxIdEmpresaxIdArea(ByVal IdEmpresa As Integer, ByVal IdArea As Integer) As List(Of Entidades.TipoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoSelectCboxIdEmpresaxIdArea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdArea", IdArea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDocumento)
                Do While lector.Read
                    Dim TipoDocumento As New Entidades.TipoDocumento
                    TipoDocumento.Id = CInt(lector.Item("IdTipoDocumento"))
                    TipoDocumento.DescripcionCorto = CStr(lector.Item("tdoc_NombreCorto"))
                    Lista.Add(TipoDocumento)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxId(ByVal idTipoDocumento As Integer) As Entidades.TipoDocumento
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", idTipoDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                lector.Read()
                Return getTipoDocumento(lector)
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCboTipoDocumentoIndep() As List(Of Entidades.TipoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoInd", cn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDocumento)
                Do While lector.Read
                    Dim TipoDocumento As New Entidades.TipoDocumento
                    TipoDocumento.Id = CInt(lector.Item("IdTipoDocumento"))
                    TipoDocumento.DescripcionCorto = CStr(lector.Item("tdoc_NombreCorto"))
                    Lista.Add(TipoDocumento)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCbo() As List(Of Entidades.TipoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDocumento)
                Do While lector.Read
                    Dim TipoDocumento As New Entidades.TipoDocumento
                    TipoDocumento.Id = CInt(lector.Item("IdTipoDocumento"))
                    TipoDocumento.Descripcion = CStr(lector.Item("tdoc_NombreLargo"))
                    Lista.Add(TipoDocumento)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectExportarContaxIdTiPoDocumento(ByVal IdTipoDocumento As Integer) As String
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentoSelectExportarContaxIdTiPoDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim ExportarConta As String = Nothing
                If lector.Read = True Then
                    ExportarConta = CStr(IIf(IsDBNull(lector.Item("tdoc_ExportarConta")) = True, "0", lector.Item("tdoc_ExportarConta")))
                End If
                Return ExportarConta
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectTipoDocumentoxIdTipoDocumento(ByVal IdTipoDocumento As String) As List(Of Entidades.TipoDocumento)
        Dim lista As New List(Of Entidades.TipoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SelectTipoDocumentoxIdTipoDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
        Dim lector As SqlDataReader
        Try

            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)

                Do While lector.Read
                    Dim obj As New Entidades.TipoDocumento
                    With obj
                        .Id = CInt(lector.Item("IdTipoDocumento"))
                        .DescripcionCorto = CStr(lector.Item("tdoc_NombreCorto"))
                        .Descripcion = CStr(lector.Item("tdoc_NombreLargo"))

                    End With
                    lista.Add(obj)

                Loop
            End Using

        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return lista
    End Function
    Public Function SelectCboTipoOperacion() As List(Of Entidades.TipoDocumento_TipoOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("TipoDocumento_TipoOperacionSelectCboTipoOreacion", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDocumento_TipoOperacion)
                Do While lector.Read
                    Dim TipoDocumento As New Entidades.TipoDocumento_TipoOperacion
                    TipoDocumento.IdTipoDocumento = CInt(lector.Item("IdTipoDocumento"))
                    TipoDocumento.Nombre = CStr(lector.Item("Nombre"))
                    Lista.Add(TipoDocumento)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCboTipoDocumentosCaja() As List(Of Entidades.TipoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoDocumentosCaja", cn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDocumento)
                Do While lector.Read
                    Dim TipoDocumento As New Entidades.TipoDocumento
                    TipoDocumento.Id = CInt(lector.Item("IdTipoDocumento"))
                    TipoDocumento.DescripcionCorto = CStr(lector.Item("tdoc_NombreCorto"))
                    Lista.Add(TipoDocumento)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
