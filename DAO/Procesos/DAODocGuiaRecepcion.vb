﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class DAODocGuiaRecepcion
    Dim HDAO As New DAO.HelperDAO
    Private objConexion As New DAO.Conexion
    'Public Function ValidandoCantidadTransitoDocReferencia(ByVal IdDocumento As Integer) As Decimal
    '    Dim CantidadTransito As Decimal
    '    Dim p() As SqlParameter = New SqlParameter(0) {}
    '    p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
    '    p(0).Value = IdDocumento
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Try
    '        cn.Open()
    '        CantidadTransito = CDec(SqlHelper.ExecuteScalar(cn, CommandType.StoredProcedure, "_DocumentoGuiaRecepcionSelectCostoProducto", p))
    '    Catch ex As Exception
    '        Throw ex
    '    Finally
    '        If (cn.State = ConnectionState.Open) Then cn.Close()
    '    End Try
    '    Return CantidadTransito
    'End Function


    Public Function DocumentoGuiaRecepcionSelectCostoProducto(ByVal IdProducto As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal Fecha As Date, ByVal IdDocumentoRef As Integer) As Decimal

        Dim costoProducto As Decimal

        Dim p() As SqlParameter = New SqlParameter(4) {}

        p(0) = New SqlParameter("@IdProducto", SqlDbType.Int)
        p(0).Value = IdProducto

        p(1) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        p(1).Value = IdEmpresa

        p(2) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        p(2).Value = IdAlmacen

        p(3) = New SqlParameter("@IdDocumentoRef", SqlDbType.Int)
        p(3).Value = IdDocumentoRef

        p(4) = New SqlParameter("@Fecha", SqlDbType.Date)
        p(4).Value = Fecha

        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Try

            cn.Open()

            costoProducto = CDec(SqlHelper.ExecuteScalar(cn, CommandType.StoredProcedure, "_DocumentoGuiaRecepcionSelectCostoProducto", p))

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return costoProducto

    End Function
    Public Function DocumentoGuiaRecepcion_BuscarDocumentoRef2(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdAlmacen As Integer, _
                                                            ByVal IdPersona As Integer, ByVal IdTipoDocumento As Integer, _
                                                            ByVal Serie As Integer, ByVal Codigo As String, _
                                                            ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal TipoDocumentoRef As Integer, ByVal IdTipoOperacion As Integer) As List(Of Entidades.DocumentoView)

        Dim lista As New List(Of Entidades.DocumentoView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoGuiaRecepcion_BuscarDocumentoRef", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdTipoDocumentoRef", TipoDocumentoRef)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
        cmd.Parameters.AddWithValue("@Serie", Serie)
        cmd.Parameters.AddWithValue("@Codigo", Codigo)
        cmd.Parameters.AddWithValue("@FechaFin", IIf(FechaFin = Nothing, DBNull.Value, FechaFin))
        cmd.Parameters.AddWithValue("@FechaInicio", IIf(FechaInicio = Nothing, DBNull.Value, FechaInicio))
        cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion)

        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                While (lector.Read)

                    Dim objDocumento As New Entidades.DocumentoView
                    With objDocumento

                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                        .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, 0, lector.Item("doc_Total")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, 0, lector.Item("doc_TotalAPagar")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("edoc_Nombre")) = True, "", lector.Item("edoc_Nombre")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                        .Empresa = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))
                        .RUC = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .DNI = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                        .NroDocumento = .Serie + " - " + .Codigo
                        .NomAlmacen = CStr(IIf(IsDBNull(lector.Item("Almacen")) = True, "", lector.Item("Almacen")))
                        .strTipoDocumentoRef = CStr(IIf(IsDBNull(lector.Item("CadTipoDocumentoRef")) = True, "", lector.Item("CadTipoDocumentoRef")))
                    End With

                    lista.Add(objDocumento)

                End While

                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function DocumentoGuiaRecepcion_BuscarDocumentoRef(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdAlmacen As Integer, _
                                                              ByVal IdPersona As Integer, ByVal IdTipoDocumento As Integer, _
                                                              ByVal Serie As Integer, ByVal Codigo As Integer, _
                                                              ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal TipoDocumentoRef As Integer, ByVal IdTipoOperacion As Integer) As List(Of Entidades.DocumentoView)

        Dim lista As New List(Of Entidades.DocumentoView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoGuiaRecepcion_BuscarDocumentoRef", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdTipoDocumentoRef", TipoDocumentoRef)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
        cmd.Parameters.AddWithValue("@Serie", Serie)
        cmd.Parameters.AddWithValue("@Codigo", Codigo)
        cmd.Parameters.AddWithValue("@FechaFin", IIf(FechaFin = Nothing, DBNull.Value, FechaFin))
        cmd.Parameters.AddWithValue("@FechaInicio", IIf(FechaInicio = Nothing, DBNull.Value, FechaInicio))
        cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion)

        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                While (lector.Read)

                    Dim objDocumento As New Entidades.DocumentoView
                    With objDocumento

                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                        .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, 0, lector.Item("doc_Total")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, 0, lector.Item("doc_TotalAPagar")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("edoc_Nombre")) = True, "", lector.Item("edoc_Nombre")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                        .Empresa = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))
                        .RUC = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .DNI = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                        .NroDocumento = .Serie + " - " + .Codigo
                        .NomAlmacen = CStr(IIf(IsDBNull(lector.Item("Almacen")) = True, "", lector.Item("Almacen")))
                        .strTipoDocumentoRef = CStr(IIf(IsDBNull(lector.Item("CadTipoDocumentoRef")) = True, "", lector.Item("CadTipoDocumentoRef")))
                    End With

                    lista.Add(objDocumento)

                End While

                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Sub DocumentoSolicitudPago_DeshacerMov(ByVal IdDocumento As Integer, ByVal deleteRelacionDoc As Boolean, ByVal deleteObservaciones As Boolean, ByVal deleteDetalleConcepto As Boolean, ByVal Anular As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As New SqlCommand("_DocumentoSolicitudPago_DeshacerMov", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0

        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@deleteRelacionDoc", deleteRelacionDoc)
        cmd.Parameters.AddWithValue("@deleteObservaciones", deleteObservaciones)
        cmd.Parameters.AddWithValue("@DeleteDetalleConcepto", deleteDetalleConcepto)
        cmd.Parameters.AddWithValue("@anular", Anular)
        cmd.ExecuteNonQuery()

    End Sub
    Public Sub DocumentoGuiaRecepcion_DeshacerMov(ByVal IdDocumento As Integer, ByVal deleteDetalleDocumento As Boolean, ByVal deleteMovAlmacen As Boolean, ByVal deleteSaldoKardex As Boolean, ByVal deleteDetalleMovAlmacen As Boolean, ByVal deleteRelacionDoc As Boolean, ByVal deleteObservaciones As Boolean, ByVal DeletePuntoPartida As Boolean, ByVal Anular As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As New SqlCommand("_DocumentoGuiaRecepcion_DeshacerMov", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0

        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@deleteDetalleDocumento", deleteDetalleDocumento)
        cmd.Parameters.AddWithValue("@deleteMovAlmacen", deleteMovAlmacen)
        cmd.Parameters.AddWithValue("@deleteSaldoKardex", deleteSaldoKardex)
        cmd.Parameters.AddWithValue("@deleteDetalleMovAlmacen", deleteDetalleMovAlmacen)
        cmd.Parameters.AddWithValue("@deleteRelacionDoc", deleteRelacionDoc)
        cmd.Parameters.AddWithValue("@deleteObservaciones", deleteObservaciones)
        cmd.Parameters.AddWithValue("@DeletePuntoPartida", DeletePuntoPartida)
        cmd.Parameters.AddWithValue("@Anular", Anular)
        cmd.ExecuteNonQuery()

    End Sub




    Public Function DocumentoGuiaRecepcionSelectCab(ByVal IdSerie As Integer, ByVal Codigo As Integer, ByVal IdDocumento As Integer) As Entidades.DocGuiaRecepcion

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim objDocumento As New Entidades.DocGuiaRecepcion

        Try

            cn.Open()

            Dim cmd As New SqlCommand("_DocumentoGuiaRecepcionSelectCab", cn)
            cmd.CommandTimeout = 0
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdSerie", IdSerie)
            cmd.Parameters.AddWithValue("@Codigo", Codigo)
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            Dim lector As SqlDataReader
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)

            If (lector.Read) Then
                With objDocumento

                    .Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                    .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                    .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))
                    .IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")) = True, 0, lector("IdPersona")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(lector("IdEstadoDoc")) = True, 0, lector("IdEstadoDoc")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(lector("IdTipoOperacion")) = True, 0, lector("IdTipoOperacion")))
                    .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")) = True, 0, lector("IdTienda")))
                    .IdSerie = CInt(IIf(IsDBNull(lector("IdSerie")) = True, 0, lector("IdSerie")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector("IdEmpresa")) = True, 0, lector("IdEmpresa")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector("IdTipoDocumento")) = True, 0, lector("IdTipoDocumento")))
                    .Observacion = CStr(IIf(IsDBNull(lector("ob_Observacion")) = True, "", lector("ob_Observacion")))
                    .DescripcionPersona = CStr(IIf(IsDBNull(lector("DescripcionPersona")) = True, "", lector("DescripcionPersona")))
                    .Ruc = CStr(IIf(IsDBNull(lector("Ruc")) = True, "", lector("Ruc")))
                    .Dni = CStr(IIf(IsDBNull(lector("Dni")) = True, "", lector("Dni")))
                    .IdDocumentoRef = CInt(IIf(IsDBNull(lector("IdDocumentoReferencia")) = True, 0, lector("IdDocumentoReferencia")))
                    .TipoDocumentoRef = CStr(IIf(IsDBNull(lector("TipoDocumentoRef")) = True, "", lector("TipoDocumentoRef")))
                    .DocRef_Serie = CStr(IIf(IsDBNull(lector("DocRef_Serie")) = True, "", lector("DocRef_Serie")))
                    .DocRef_Codigo = CStr(IIf(IsDBNull(lector("DocRef_Codigo")) = True, "", lector("DocRef_Codigo")))
                    .DocRef_FechaEmision = CDate(IIf(IsDBNull(lector("DocRef_FechaEmision")) = True, Nothing, lector("DocRef_FechaEmision")))
                    .IdAlmacen = CInt(IIf(IsDBNull(lector("IdAlmacen")) = True, 0, lector("IdAlmacen")))
                End With
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            lector.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try

        Return objDocumento

    End Function

    Public Sub DocumentoGuiaRecepcion_VerificarRegistro(ByVal IdDocumento As Integer, ByVal TipoUso As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(1) {}

        p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        p(0).Value = IdDocumento

        p(1) = New SqlParameter("@TipoUso", SqlDbType.Int)
        p(1).Value = TipoUso
        Try
            SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoGuiaRecepcion_VerificarRegistro", p)
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Sub


    Public Function InsertaDocumentoGuiaRecepcion(ByVal cn As SqlConnection, ByVal documento As Entidades.Documento, ByVal T As SqlTransaction) As Integer

        Dim ArrayParametros() As SqlParameter = New SqlParameter(23) {}
        ArrayParametros(0) = New SqlParameter("@doc_Codigo", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(documento.Codigo = Nothing, DBNull.Value, documento.Codigo)
        ArrayParametros(1) = New SqlParameter("@doc_Serie", SqlDbType.VarChar)
        ArrayParametros(1).Value = IIf(documento.Serie = Nothing, DBNull.Value, documento.Serie)
        ArrayParametros(2) = New SqlParameter("@doc_FechaEmision", SqlDbType.DateTime)
        ArrayParametros(2).Value = IIf(documento.FechaEmision = Nothing, DBNull.Value, documento.FechaEmision)
        ArrayParametros(3) = New SqlParameter("@doc_FechaIniTraslado", SqlDbType.DateTime)
        ArrayParametros(3).Value = IIf(documento.FechaIniTraslado = Nothing, DBNull.Value, documento.FechaIniTraslado)
        'ArrayParametros(4) = New SqlParameter("@doc_FechaRegistro", SqlDbType.DateTime)
        'ArrayParametros(4).Value = IIf(documento.FechaRegistro = Nothing, DBNull.Value, documento.FechaRegistro)
        ArrayParametros(4) = New SqlParameter("@doc_FechaAentregar", SqlDbType.DateTime)
        ArrayParametros(4).Value = IIf(documento.FechaAEntregar = Nothing, DBNull.Value, documento.FechaAEntregar)
        ArrayParametros(5) = New SqlParameter("@doc_FechaEntrega", SqlDbType.DateTime)
        ArrayParametros(5).Value = IIf(documento.FechaEntrega = Nothing, DBNull.Value, documento.FechaEntrega)
        ArrayParametros(6) = New SqlParameter("@doc_FechaVenc", SqlDbType.DateTime)
        ArrayParametros(6).Value = IIf(documento.FechaVenc = Nothing, DBNull.Value, documento.FechaVenc)
        ArrayParametros(7) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(7).Value = IIf(documento.IdPersona = Nothing, DBNull.Value, documento.IdPersona)
        ArrayParametros(8) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        ArrayParametros(8).Value = IIf(documento.IdUsuario = Nothing, DBNull.Value, documento.IdUsuario)
        ArrayParametros(9) = New SqlParameter("@IdTransportista", SqlDbType.Int)
        ArrayParametros(9).Value = IIf(documento.IdTransportista = Nothing, DBNull.Value, documento.IdTransportista)
        ArrayParametros(10) = New SqlParameter("@IdRemitente", SqlDbType.Int)
        ArrayParametros(10).Value = IIf(documento.IdRemitente = Nothing, DBNull.Value, documento.IdRemitente)
        ArrayParametros(11) = New SqlParameter("@IdDestinatario", SqlDbType.Int)
        ArrayParametros(11).Value = IIf(documento.IdDestinatario = Nothing, DBNull.Value, documento.IdDestinatario)
        ArrayParametros(12) = New SqlParameter("@IdEstadoDoc", SqlDbType.Int)
        ArrayParametros(12).Value = IIf(documento.IdEstadoDoc = Nothing, DBNull.Value, documento.IdEstadoDoc)
        ArrayParametros(13) = New SqlParameter("@LugarEntrega", SqlDbType.Int)
        ArrayParametros(13).Value = IIf(documento.LugarEntrega = Nothing, DBNull.Value, documento.LugarEntrega)
        ArrayParametros(14) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        ArrayParametros(14).Value = IIf(documento.IdTipoOperacion = Nothing, DBNull.Value, documento.IdTipoOperacion)
        ArrayParametros(15) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(15).Value = IIf(documento.IdTienda = Nothing, DBNull.Value, documento.IdTienda)
        ArrayParametros(16) = New SqlParameter("@IdSerie", SqlDbType.Int)
        ArrayParametros(16).Value = IIf(documento.IdSerie = Nothing, DBNull.Value, documento.IdSerie)
        ArrayParametros(17) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(17).Value = IIf(documento.IdEmpresa = Nothing, DBNull.Value, documento.IdEmpresa)
        ArrayParametros(18) = New SqlParameter("@IdChofer", SqlDbType.Int)
        ArrayParametros(18).Value = IIf(documento.IdChofer = Nothing, DBNull.Value, documento.IdChofer)
        ArrayParametros(19) = New SqlParameter("@IdMotivoT", SqlDbType.Int)
        ArrayParametros(19).Value = IIf(documento.IdMotivoT = Nothing, DBNull.Value, documento.IdMotivoT)
        ArrayParametros(20) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        ArrayParametros(20).Value = IIf(documento.IdTipoDocumento = Nothing, DBNull.Value, documento.IdTipoDocumento)
        ArrayParametros(21) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(21).Direction = ParameterDirection.Output
        ArrayParametros(22) = New SqlParameter("@IdEstadoEnt", SqlDbType.Int)
        ArrayParametros(22).Value = IIf(documento.IdEstadoEntrega = Nothing, DBNull.Value, documento.IdEstadoEntrega)
        ArrayParametros(23) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        ArrayParametros(23).Value = IIf(documento.IdAlmacen = Nothing, DBNull.Value, documento.IdAlmacen)
        Try
            Return HDAO.InsertaTParameterOutPut(cn, "_DocumentoInsert_Guiarecepcion", ArrayParametros, T)
        Catch ex As Exception
            Throw ex
        Finally

        End Try


    End Function

    Public Function SelectxIdDocumentoGuiarecepcion(ByVal IdDocumento As Integer) As Entidades.Documento
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoSelectxIdDocumentoGuiarecepcion", cn)
        cmd.CommandTimeout = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader()

                If lector.HasRows = False Then
                    Throw New Exception("No existe documento con Ese código")
                Else
                    lector.Read()
                End If
                Dim Documento As New Entidades.Documento
                With Documento
                    .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                    .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                    .FechaIniTraslado = CDate(IIf(IsDBNull(lector.Item("doc_FechaIniTraslado")) = True, Nothing, lector.Item("doc_FechaIniTraslado")))
                    .FechaRegistro = CDate(IIf(IsDBNull(lector.Item("doc_FechaRegistro")) = True, Nothing, lector.Item("doc_FechaRegistro")))
                    .FechaAEntregar = CDate(IIf(IsDBNull(lector.Item("doc_FechaAentregar")) = True, Nothing, lector.Item("doc_FechaAentregar")))
                    .FechaEntrega = CDate(IIf(IsDBNull(lector.Item("doc_FechaEntrega")) = True, Nothing, lector.Item("doc_FechaEntrega")))
                    .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                    .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    .IdUsuario = CInt(IIf(IsDBNull(lector.Item("IdUsuario")) = True, 0, lector.Item("IdUsuario")))
                    .IdTransportista = CInt(IIf(IsDBNull(lector.Item("IdTransportista")) = True, 0, lector.Item("IdTransportista")))
                    .IdRemitente = CInt(IIf(IsDBNull(lector.Item("IdRemitente")) = True, 0, lector.Item("IdRemitente")))
                    .IdDestinatario = CInt(IIf(IsDBNull(lector.Item("IdDestinatario")) = True, 0, lector.Item("IdDestinatario")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                    .LugarEntrega = CInt(IIf(IsDBNull(lector.Item("LugarEntrega")) = True, 0, lector.Item("LugarEntrega")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                    .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                    .IdChofer = CInt(IIf(IsDBNull(lector.Item("IdChofer")) = True, 0, lector.Item("IdChofer")))
                    .IdMotivoT = CInt(IIf(IsDBNull(lector.Item("IdMotivoT")) = True, 0, lector.Item("IdMotivoT")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                    .IdEstadoEntrega = CInt(IIf(IsDBNull(lector.Item("IdEstadoEnt")) = True, 0, lector.Item("IdEstadoEnt")))
                    .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    .NomAlmacen = CStr(IIf(IsDBNull(lector.Item("NomAlmacen")) = True, "", lector.Item("NomAlmacen")))
                    .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                    .PoseeOrdenDespacho = CBool(IIf(IsDBNull(lector.Item("PoseeOrdenDespacho")) = True, 0, lector.Item("PoseeOrdenDespacho")))
                    .PoseeGRecepcion = CBool(IIf(IsDBNull(lector.Item("PoseeGRecepcion")) = True, 0, lector.Item("PoseeGRecepcion")))
                    .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                    .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                    .Empresa = CStr(IIf(IsDBNull(lector.Item("Empresa")) = True, "", lector.Item("Empresa")))
                    .Tienda = CStr(IIf(IsDBNull(lector.Item("Tienda")) = True, "", lector.Item("Tienda")))
                    .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("EstadoDocumento")) = True, "", lector.Item("EstadoDocumento")))
                    .NroDocumento = .Serie + " - " + .Codigo
                    .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                    .CantidadTransito = CDec(IIf(IsDBNull(lector.Item("dc_CantidadTransito")) = True, 0, lector.Item("dc_CantidadTransito")))
                End With
                Return Documento
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
