﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'**********************   VIERNES 25 06 2010
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAODocumentoGuiaRemisionT

    Private objDaoMantenedor As New DAO.DAOMantenedor

    Public Sub DeshacerMov(ByVal IdDocumento As Integer, ByVal deleteRelacionDocumento As Boolean, ByVal Anular As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(2) {}
        p(0) = objDaoMantenedor.getParam(IdDocumento, "@IdDocumento", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(deleteRelacionDocumento, "@deleteRelacionDocumento", SqlDbType.Bit)
        p(2) = objDaoMantenedor.getParam(Anular, "@Anular", SqlDbType.Bit)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoGuiaRemisionT_DeshacerMov", p)

    End Sub

End Class
