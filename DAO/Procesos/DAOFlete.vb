﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

' viernes 12 de febrero 2010
Imports System.Data.SqlClient

Public Class DAOFlete

    Dim objConexion As New DAO.Conexion

    Public Function deleteFlete(ByVal idflete As Integer) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim cmd As New SqlCommand("_deleteFlete", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idflete", idflete)
            cmd.ExecuteNonQuery()
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return True
    End Function

    Public Function SelectFlete() As List(Of Entidades.Flete)
        Dim Lista As New List(Of Entidades.Flete)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SelectFlete", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            cn.Open()
            lector = cmd.ExecuteReader
            Do While lector.Read
                Dim obj As New Entidades.Flete
                With obj
                    .IdFlete = CInt(lector("IdFlete"))
                    .strInicio = CStr(IIf(IsDBNull(lector("strInicio")), "---", lector("strInicio")))
                    .strDestino = CStr(IIf(IsDBNull(lector("strDestino")), "---", lector("strDestino")))
                    .strMoneda = CStr(IIf(IsDBNull(lector("strMoneda")), "---", lector("strMoneda")))
                    .pp_UbigeoInicio = CStr(IIf(IsDBNull(lector("pp_UbigeoInicio")), "---", lector("pp_UbigeoInicio")))
                    .pp_UbigeoDestino = CStr(IIf(IsDBNull(lector("pp_UbigeoDestino")), "---", lector("pp_UbigeoDestino")))
                    .IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")), 0, lector("IdMoneda")))
                    .fle_CostoxKilo = CDec(IIf(IsDBNull(lector("fle_CostoxPeso")), 0, lector("fle_CostoxPeso")))
                    .fle_CostoFijo = CDec(IIf(IsDBNull(lector("fle_CostoFijo")), 0, lector("fle_CostoFijo")))
                End With
                Lista.Add(obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function


    Public Function InsertFlete(ByVal obj As Entidades.Flete) As Integer
        Dim parametros() As SqlParameter = New SqlParameter(4) {}
        parametros(0) = New SqlParameter("@pp_UbigeoInicio", SqlDbType.Char)
        parametros(0).Value = IIf(obj.pp_UbigeoInicio = Nothing, DBNull.Value, obj.pp_UbigeoInicio)
        parametros(1) = New SqlParameter("@pp_UbigeoDestino", SqlDbType.Char)
        parametros(1).Value = IIf(obj.pp_UbigeoDestino = Nothing, DBNull.Value, obj.pp_UbigeoDestino)
        parametros(2) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        parametros(2).Value = IIf(obj.IdMoneda = Nothing, DBNull.Value, obj.IdMoneda)
        parametros(3) = New SqlParameter("@fle_CostoxPeso", SqlDbType.Decimal)
        parametros(3).Value = IIf(obj.fle_CostoxKilo = Nothing, DBNull.Value, obj.fle_CostoxKilo)
        parametros(4) = New SqlParameter("@fle_CostoFijo", SqlDbType.Decimal)
        parametros(4).Value = IIf(obj.fle_CostoFijo = Nothing, DBNull.Value, obj.fle_CostoFijo)

        Dim cod% = 0
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim cmd As New SqlCommand("_InsertFlete", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(parametros)
            cod = CInt(cmd.ExecuteScalar)
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return cod
    End Function

    Public Function UpdateFlete(ByVal obj As Entidades.Flete) As Boolean
        Dim parametros() As SqlParameter = New SqlParameter(5) {}
        parametros(0) = New SqlParameter("@pp_UbigeoInicio", SqlDbType.Char)
        parametros(0).Value = IIf(obj.pp_UbigeoInicio = Nothing, DBNull.Value, obj.pp_UbigeoInicio)
        parametros(1) = New SqlParameter("@pp_UbigeoDestino", SqlDbType.Char)
        parametros(1).Value = IIf(obj.pp_UbigeoDestino = Nothing, DBNull.Value, obj.pp_UbigeoDestino)
        parametros(2) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        parametros(2).Value = IIf(obj.IdMoneda = Nothing, DBNull.Value, obj.IdMoneda)
        parametros(3) = New SqlParameter("@fle_CostoxPeso", SqlDbType.Decimal)
        parametros(3).Value = IIf(obj.fle_CostoxKilo = Nothing, DBNull.Value, obj.fle_CostoxKilo)
        parametros(4) = New SqlParameter("@fle_CostoFijo", SqlDbType.Decimal)
        parametros(4).Value = IIf(obj.fle_CostoFijo = Nothing, DBNull.Value, obj.fle_CostoFijo)
        parametros(5) = New SqlParameter("@IdFlete", SqlDbType.Int)
        parametros(5).Value = IIf(obj.IdFlete = Nothing, DBNull.Value, obj.IdFlete)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim cmd As New SqlCommand("_UpdateFlete", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(parametros)
            cmd.ExecuteNonQuery()
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return True
    End Function



End Class
