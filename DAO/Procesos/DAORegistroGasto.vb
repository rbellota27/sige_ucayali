﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'viernes 12 de marzo
Imports System.Data.SqlClient
Public Class DAORegistroGasto

    Private cn As SqlConnection
    Private cmd As SqlCommand
    Private lector As SqlDataReader
    Private tr As SqlTransaction
    Private objconexion As New DAO.Conexion
    Private objDAOObs As DAOObservacion

#Region "Mantenimiento"

    Public Function InsertRegistroGasto(ByVal Obj As Entidades.RegistroGasto.CabeceraRegistroGasto, _
                                            ByVal Lista As List(Of Entidades.RequerimientoGasto), _
                                            ByVal ListaCentroCosto As List(Of Entidades.Centro_costo), _
                                            ByVal objObs As Entidades.Observacion) As String
        Dim cad As String
        Dim paramentros() As SqlParameter = New SqlParameter(12) {}
        paramentros(0) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        paramentros(0).Value = IIf(Obj.IdEmpresa = Nothing, DBNull.Value, Obj.IdEmpresa)
        paramentros(1) = New SqlParameter("@IdTienda", SqlDbType.Int)
        paramentros(1).Value = IIf(Obj.IdTienda = Nothing, DBNull.Value, Obj.IdTienda)
        paramentros(2) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        paramentros(2).Value = IIf(Obj.IdTipoDocumento = Nothing, DBNull.Value, Obj.IdTipoDocumento)
        paramentros(3) = New SqlParameter("@IdSerie", SqlDbType.Int)
        paramentros(3).Value = IIf(Obj.IdSerie = Nothing, DBNull.Value, Obj.IdSerie)
        paramentros(4) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        paramentros(4).Value = IIf(Obj.IdTipoOperacion = Nothing, DBNull.Value, Obj.IdTipoOperacion)
        paramentros(5) = New SqlParameter("@doc_FechaEmision", SqlDbType.DateTime)
        paramentros(5).Value = IIf(Obj.FechaEmision = Nothing, DBNull.Value, Obj.FechaEmision)
        paramentros(6) = New SqlParameter("@IdArea", SqlDbType.Int)
        paramentros(6).Value = IIf(Obj.IdArea = Nothing, DBNull.Value, Obj.IdArea)
        paramentros(7) = New SqlParameter("@IdMedioPago", SqlDbType.Int)
        paramentros(7).Value = IIf(Obj.IdMedioPagoCredito = Nothing, DBNull.Value, Obj.IdMedioPagoCredito)
        paramentros(8) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        paramentros(8).Value = IIf(Obj.IdUsuario = Nothing, DBNull.Value, Obj.IdUsuario)
        paramentros(9) = New SqlParameter("@IdPersona", SqlDbType.Int)
        paramentros(9).Value = IIf(Obj.IdPersona = Nothing, DBNull.Value, Obj.IdPersona)
        paramentros(10) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        paramentros(10).Value = IIf(Obj.IdMoneda = Nothing, DBNull.Value, Obj.IdMoneda)
        paramentros(11) = New SqlParameter("@doc_Total", SqlDbType.Decimal)
        paramentros(11).Value = IIf(Obj.TotalAPagar = Nothing, DBNull.Value, Obj.TotalAPagar)
        paramentros(12) = New SqlParameter("@IdCentroCosto", SqlDbType.VarChar)
        paramentros(12).Value = IIf(Obj.IdCentroCosto = Nothing, DBNull.Value, Obj.IdCentroCosto)

        cn = objconexion.ConexionSIGE
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            cmd = New SqlCommand("_InsertRegistroGasto", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(paramentros)
            cad = cmd.ExecuteScalar.ToString

            Dim iddocumento() As String = cad.Split(CChar(","))

            Dim ObjDaoRequerimiento As New DAO.DAORequerimientoGasto
            ObjDaoRequerimiento.InsertDetRequerimientoGasto(CInt(iddocumento(0)), Lista, cn, tr)

            Dim objDaoDocRelacionado As New DAO.DAORelacionDocumento
            Dim objEntDocRelacionado As New Entidades.RelacionDocumento
            objEntDocRelacionado.IdDocumento1 = CInt(Obj.IdDocRelacionado)
            objEntDocRelacionado.IdDocumento2 = CInt(iddocumento(0))

            objDaoDocRelacionado.InsertaRelacionDocumento(objEntDocRelacionado, cn, tr)

            Dim objDaoCentroCosto As New DAO.DAOCentro_Costo
            objDaoCentroCosto.GastoPorCentroCosto_Insert(cn, tr, CInt(iddocumento(0)), ListaCentroCosto)

            objDAOObs = New DAOObservacion
            objObs.IdDocumento = CInt(iddocumento(0))
            objDAOObs.InsertaObservacionT(cn, tr, objObs)

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return cad
    End Function

    Public Function UpdateRegistroGasto(ByVal Obj As Entidades.RegistroGasto.CabeceraRegistroGasto, _
                                             ByVal Lista As List(Of Entidades.RequerimientoGasto), _
                                             ByVal ListaCentroCosto As List(Of Entidades.Centro_costo), _
                                             ByVal objObs As Entidades.Observacion) As String
        Dim cad As String = String.Empty
        Dim parametros() As SqlParameter = New SqlParameter(8) {}
        parametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        parametros(0).Value = IIf(Obj.Id = Nothing, DBNull.Value, Obj.Id)
        parametros(1) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        parametros(1).Value = IIf(Obj.IdTipoOperacion = Nothing, DBNull.Value, Obj.IdTipoOperacion)
        parametros(2) = New SqlParameter("@doc_FechaEmision", SqlDbType.DateTime)
        parametros(2).Value = IIf(Obj.FechaEmision = Nothing, DBNull.Value, Obj.FechaEmision)
        parametros(3) = New SqlParameter("@IdArea", SqlDbType.Int)
        parametros(3).Value = IIf(Obj.IdArea = Nothing, DBNull.Value, Obj.IdArea)
        parametros(4) = New SqlParameter("@IdMedioPago", SqlDbType.Int)
        parametros(4).Value = IIf(Obj.IdMedioPagoCredito = Nothing, DBNull.Value, Obj.IdMedioPagoCredito)
        parametros(5) = New SqlParameter("@IdPersona", SqlDbType.Int)
        parametros(5).Value = IIf(Obj.IdPersona = Nothing, DBNull.Value, Obj.IdPersona)
        parametros(6) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        parametros(6).Value = IIf(Obj.IdMoneda = Nothing, DBNull.Value, Obj.IdMoneda)
        parametros(7) = New SqlParameter("@doc_Total", SqlDbType.Decimal)
        parametros(7).Value = IIf(Obj.TotalAPagar = Nothing, DBNull.Value, Obj.TotalAPagar)
        parametros(8) = New SqlParameter("@IdCentroCosto", SqlDbType.VarChar)
        parametros(8).Value = IIf(Obj.IdCentroCosto = Nothing, DBNull.Value, Obj.IdCentroCosto)

        cn = objconexion.ConexionSIGE
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            cmd = New SqlCommand("_UpdateRegistroGasto", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(parametros)
            cad = cmd.ExecuteScalar.ToString

            Dim ObjDaoRequerimiento As New DAO.DAORequerimientoGasto
            ObjDaoRequerimiento.InsertDetRequerimientoGasto(Obj.Id, Lista, cn, tr)

            Dim objDaoDocRelacionado As New DAO.DAORelacionDocumento
            Dim objEntDocRelacionado As New Entidades.RelacionDocumento
            objEntDocRelacionado.IdDocumento1 = CInt(Obj.IdDocRelacionado)
            objEntDocRelacionado.IdDocumento2 = Obj.Id
            objDaoDocRelacionado.UpdateRelacionDocumento(objEntDocRelacionado, cn, tr)

            Dim objDaoCentroCosto As New DAO.DAOCentro_Costo
            objDaoCentroCosto.GastoPorCentroCosto_Insert(cn, tr, Obj.Id, ListaCentroCosto)


            objDAOObs = New DAOObservacion
            objObs.IdDocumento = Obj.Id
            objDAOObs.ActualizaObservacion(objObs)

            tr.Commit()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return cad
    End Function


#End Region



#Region "Otros"

    Public Function selectRegistroGasto(ByVal idserie As Integer, ByVal codigo As Integer) As Entidades.RegistroGasto.CabeceraRegistroGasto
        Dim obj As New Entidades.RegistroGasto.CabeceraRegistroGasto
        cn = objconexion.ConexionSIGE
        cmd = New SqlCommand("_selectRegistroGasto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idserie", idserie)
        cmd.Parameters.AddWithValue("@codigo", codigo)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If lector.HasRows = True Then
                If lector.Read Then
                    With obj
                        .Id = CInt(lector("IdDocumento"))
                        .IdDocRelacionado = CInt(lector("IdDocRelacionado"))
                        .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")), "", lector("doc_Codigo")))
                        .codigoRef = CStr(IIf(IsDBNull(lector("doc_CodigoRef")), "", lector("doc_CodigoRef")))
                        .serieRef = CStr(IIf(IsDBNull(lector("doc_SerieRef")), "", lector("doc_SerieRef")))
                        .IdSerie = CInt(IIf(IsDBNull(lector("IdSerie")), 0, lector("IdSerie")))
                        .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")), "", lector("doc_Serie")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector("IdEstadoDoc")), 0, lector("IdEstadoDoc")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")), Nothing, lector("doc_FechaEmision")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")), 0, lector("doc_TotalAPagar")))
                        .doc_Total2 = CDec(IIf(IsDBNull(lector("doc_Total2")), 0, lector("doc_Total2")))
                        .IdPersona = CInt(lector("Idpersona"))
                        .NombrePersona = CStr(IIf(IsDBNull(lector("nombres")), "", lector("nombres")))
                        .Ruc = CStr(IIf(IsDBNull(lector("Ruc")), "---", lector("Ruc")))
                        .Dni = CStr(IIf(IsDBNull(lector("Dni")), "---", lector("Dni")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")), 0, lector("IdMoneda")))
                        .Idmoneda2 = CInt(IIf(IsDBNull(lector("Idmoneda2")), 0, lector("Idmoneda2")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector("IdEmpresa")), 0, lector("IdEmpresa")))
                        .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")), 0, lector("IdTienda")))
                        .IdArea = CInt(IIf(IsDBNull(lector("IdArea")), 0, lector("IdArea")))
                        .IdMedioPagoCredito = CInt(IIf(IsDBNull(lector("IdMedioPago")), 0, lector("IdMedioPago")))
                        .IdTipoOperacion = CInt(IIf(IsDBNull(lector("IdTipoOperacion")), 0, lector("IdTipoOperacion")))
                        .IdCentroCosto = CStr(IIf(IsDBNull(lector("IdCentroCosto")), "", lector("IdCentroCosto")))
                        .FechaCancelacion = CDate(IIf(IsDBNull(lector("doc_FechaCancelacion")), Nothing, lector("doc_FechaCancelacion")))
                        .IdUsuarioSupervisor = CInt(IIf(IsDBNull(lector("IdUsuarioSupervisor")), 0, lector("IdUsuarioSupervisor")))
                        .NomSupervisor = CStr(lector("nomSupervisor"))
                    End With
                End If
            Else
                Throw New Exception("No se encuentra el documento")
            End If
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return obj
    End Function

    Public Function selectListaRegistroGasto(ByVal IdDocumento As Integer) As List(Of Entidades.RequerimientoGasto)
        Dim Lista As New List(Of Entidades.RequerimientoGasto)
        cn = objconexion.ConexionSIGE
        cmd = New SqlCommand("_selectListaRegistroGasto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.RequerimientoGasto
                With obj
                    .IdDocumento = CInt(lector("IdDocumento"))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector("IdTipoDocumentoRef")), 0, lector("IdTipoDocumentoRef")))
                    .NroDocumento = CStr(IIf(IsDBNull(lector("dg_NroDocumento")), "", lector("dg_NroDocumento")))
                    .Descripcion = CStr(IIf(IsDBNull(lector("dr_Concepto")), "", lector("dr_Concepto")))
                    .IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")), 0, lector("IdMoneda")))
                    .Monto = CDec(IIf(IsDBNull(lector("dr_Monto")), 0, lector("dr_Monto")))
                    .IdConcepto = CInt(IIf(IsDBNull(lector("IdConcepto")), 0, lector("IdConcepto")))
                    .IdMotivo = CInt(IIf(IsDBNull(lector("IdMotGasto")), 0, lector("IdMotGasto")))
                End With
                Lista.Add(obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function

#End Region


End Class
