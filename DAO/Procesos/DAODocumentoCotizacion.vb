﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports Microsoft.ApplicationBlocks.Data

'************************   MARTES 01 06 2010 HORA 03_08 PM



Imports System.Data.SqlClient
Public Class DAODocumentoCotizacion
    Private objConexion As New DAO.Conexion
    Private cmd As SqlCommand
    Private tr As SqlTransaction
    Private cn As SqlConnection
    Private objDaoMantenedor As New DAO.DAOMantenedor

    'DocumentoVenta_SelectDetxIdDocumentoRef3
    Public Function DocumentoVenta_SelectDetxIdDocumentoRef3(ByVal IdDocumentoRef As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Fecha As Date, ByVal IdTipoPV As Integer, ByVal IdPersona As Integer, ByVal IdMoneda As Integer, ByVal IdVendedor As Integer) As List(Of Entidades.DetalleDocumento)

        Dim lista As New List(Of Entidades.DetalleDocumento)

        Try

            Dim p() As SqlParameter = New SqlParameter(7) {}
            p(0) = objDaoMantenedor.getParam(IdDocumentoRef, "@IdDocumentoRef", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(Fecha, "@Fecha", SqlDbType.Date)
            p(4) = objDaoMantenedor.getParam(IdTipoPV, "@IdTipoPV", SqlDbType.Int)
            p(5) = objDaoMantenedor.getParam(IdPersona, "@IdPersona", SqlDbType.Int)
            p(6) = objDaoMantenedor.getParam(IdMoneda, "@IdMoneda", SqlDbType.Int)
            p(7) = objDaoMantenedor.getParam(IdVendedor, "@IdVendedor", SqlDbType.Int)

            cn = objConexion.ConexionSIGE
            cn.Open()
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_DocumentoVenta_SelectDetxIdDocumentoRef3", p)

            While (reader.Read)

                Dim objDetalleDocumento As New Entidades.DetalleDocumento
                With objDetalleDocumento

                    .IdProducto = objDaoMantenedor.UCInt(reader("IdProducto"))
                    .IdUnidadMedida = objDaoMantenedor.UCInt(reader("IdUnidadMedida"))
                    .Cantidad = objDaoMantenedor.UCDec(reader("Cantidad"))
                    .PrecioLista = objDaoMantenedor.UCDec(reader("PrecioLista"))
                    .PrecioSD = objDaoMantenedor.UCDec(reader("PrecioLista"))
                    .Descuento = objDaoMantenedor.UCDec(0)
                    .PorcentDcto = objDaoMantenedor.UCDec(0)
                    .PrecioCD = objDaoMantenedor.UCDec(reader("PrecioLista"))
                    .Importe = .PrecioCD * .Cantidad
                    .TasaPercepcion = objDaoMantenedor.UCDec(reader("PorcentPercepcion"))
                    .DetalleGlosa = ""
                    .IdUsuarioSupervisor = Nothing
                    .IdTipoPV = objDaoMantenedor.UCInt(reader("IdTipoPv"))
                    .NomProducto = objDaoMantenedor.UCStr(reader("Producto"))
                    .CodigoProducto = objDaoMantenedor.UCStr(reader("CodigoProducto"))

                    .PorcentDctoMaximo = objDaoMantenedor.UCDec(reader("PorcentDctoMax"))
                    .PrecioBaseDcto = objDaoMantenedor.UCStr(reader("PrecioBaseDcto"))
                    .VolumenVentaMin = objDaoMantenedor.UCDec(reader("VolumenMinVenta"))
                    .pvComercial = objDaoMantenedor.UCDec(reader("PrecioComercial"))

                    .ExisteCampania_Producto = objDaoMantenedor.UCBool(reader("ExisteCampaniaProducto"))
                    .IdCampania = objDaoMantenedor.UCInt(reader("IdCampania"))
                    .IdProductoAux = objDaoMantenedor.UCInt(reader("IdProductoRef"))

                End With

                lista.Add(objDetalleDocumento)

            End While

            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista

    End Function


    Public Function DocumentoVenta_SelectDetxIdDocumentoRef2(ByVal IdDocumentoRef As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Fecha As Date, ByVal IdTipoPV As Integer, ByVal IdPersona As Integer, ByVal IdMoneda As Integer, ByVal IdVendedor As Integer) As List(Of Entidades.DetalleDocumento)

        Dim lista As New List(Of Entidades.DetalleDocumento)

        Try

            Dim p() As SqlParameter = New SqlParameter(7) {}
            p(0) = objDaoMantenedor.getParam(IdDocumentoRef, "@IdDocumentoRef", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(Fecha, "@Fecha", SqlDbType.Date)
            p(4) = objDaoMantenedor.getParam(IdTipoPV, "@IdTipoPV", SqlDbType.Int)
            p(5) = objDaoMantenedor.getParam(IdPersona, "@IdPersona", SqlDbType.Int)
            p(6) = objDaoMantenedor.getParam(IdMoneda, "@IdMoneda", SqlDbType.Int)
            p(7) = objDaoMantenedor.getParam(IdVendedor, "@IdVendedor", SqlDbType.Int)

            cn = objConexion.ConexionSIGE
            cn.Open()
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_DocumentoVenta_SelectDetxIdDocumentoRef2", p)

            While (reader.Read)

                Dim objDetalleDocumento As New Entidades.DetalleDocumento
                With objDetalleDocumento

                    .IdProducto = objDaoMantenedor.UCInt(reader("IdProducto"))
                    .IdUnidadMedida = objDaoMantenedor.UCInt(reader("IdUnidadMedida"))
                    .Cantidad = objDaoMantenedor.UCDec(reader("Cantidad"))
                    .PrecioLista = objDaoMantenedor.UCDec(reader("PrecioLista"))
                    .PrecioSD = objDaoMantenedor.UCDec(reader("PrecioLista"))
                    .Descuento = objDaoMantenedor.UCDec(0)
                    .PorcentDcto = objDaoMantenedor.UCDec(0)
                    .PrecioCD = objDaoMantenedor.UCDec(reader("PrecioLista"))
                    .Importe = .PrecioCD * .Cantidad
                    .TasaPercepcion = objDaoMantenedor.UCDec(reader("PorcentPercepcion"))
                    .DetalleGlosa = ""
                    .IdUsuarioSupervisor = Nothing
                    .IdTipoPV = objDaoMantenedor.UCInt(reader("IdTipoPv"))
                    .NomProducto = objDaoMantenedor.UCStr(reader("Producto"))
                    .CodigoProducto = objDaoMantenedor.UCStr(reader("CodigoProducto"))

                    .PorcentDctoMaximo = objDaoMantenedor.UCDec(reader("PorcentDctoMax"))
                    .PrecioBaseDcto = objDaoMantenedor.UCStr(reader("PrecioBaseDcto"))
                    .VolumenVentaMin = objDaoMantenedor.UCDec(reader("VolumenMinVenta"))
                    .pvComercial = objDaoMantenedor.UCDec(reader("PrecioComercial"))

                    .ExisteCampania_Producto = objDaoMantenedor.UCBool(reader("ExisteCampaniaProducto"))
                    .IdCampania = objDaoMantenedor.UCInt(reader("IdCampania"))
                    .IdProductoAux = objDaoMantenedor.UCInt(reader("IdProductoRef"))

                End With

                lista.Add(objDetalleDocumento)

            End While

            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista

    End Function

    Public Function DocumentoVenta_SelectDetxIdDocumentoRef(ByVal IdDocumentoRef As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Fecha As Date, ByVal IdTipoPV As Integer, ByVal IdPersona As Integer, ByVal IdMoneda As Integer, ByVal IdVendedor As Integer) As List(Of Entidades.DetalleDocumento)

        Dim lista As New List(Of Entidades.DetalleDocumento)

        Try

            Dim p() As SqlParameter = New SqlParameter(7) {}
            p(0) = objDaoMantenedor.getParam(IdDocumentoRef, "@IdDocumentoRef", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(Fecha, "@Fecha", SqlDbType.Date)
            p(4) = objDaoMantenedor.getParam(IdTipoPV, "@IdTipoPV", SqlDbType.Int)
            p(5) = objDaoMantenedor.getParam(IdPersona, "@IdPersona", SqlDbType.Int)
            p(6) = objDaoMantenedor.getParam(IdMoneda, "@IdMoneda", SqlDbType.Int)
            p(7) = objDaoMantenedor.getParam(IdVendedor, "@IdVendedor", SqlDbType.Int)

            cn = objConexion.ConexionSIGE
            cn.Open()
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_DocumentoVenta_SelectDetxIdDocumentoRef", p)

            While (reader.Read)

                Dim objDetalleDocumento As New Entidades.DetalleDocumento
                With objDetalleDocumento

                    .IdProducto = objDaoMantenedor.UCInt(reader("IdProducto"))
                    .IdUnidadMedida = objDaoMantenedor.UCInt(reader("IdUnidadMedida"))
                    .Cantidad = objDaoMantenedor.UCDec(reader("Cantidad"))
                    .PrecioLista = objDaoMantenedor.UCDec(reader("PrecioLista"))
                    .PrecioSD = objDaoMantenedor.UCDec(reader("PrecioLista"))
                    .Descuento = objDaoMantenedor.UCDec(0)
                    .PorcentDcto = objDaoMantenedor.UCDec(0)
                    .PrecioCD = objDaoMantenedor.UCDec(reader("PrecioLista"))
                    .Importe = .PrecioCD * .Cantidad
                    .TasaPercepcion = objDaoMantenedor.UCDec(reader("PorcentPercepcion"))
                    .DetalleGlosa = ""
                    .IdUsuarioSupervisor = Nothing
                    .IdTipoPV = objDaoMantenedor.UCInt(reader("IdTipoPv"))
                    .NomProducto = objDaoMantenedor.UCStr(reader("Producto"))
                    .CodigoProducto = objDaoMantenedor.UCStr(reader("CodigoProducto"))

                    .PorcentDctoMaximo = objDaoMantenedor.UCDec(reader("PorcentDctoMax"))
                    .PrecioBaseDcto = objDaoMantenedor.UCStr(reader("PrecioBaseDcto"))
                    .VolumenVentaMin = objDaoMantenedor.UCDec(reader("VolumenMinVenta"))
                    .pvComercial = objDaoMantenedor.UCDec(reader("PrecioComercial"))

                    .ExisteCampania_Producto = objDaoMantenedor.UCBool(reader("ExisteCampaniaProducto"))
                    .IdCampania = objDaoMantenedor.UCInt(reader("IdCampania"))
                    .IdProductoAux = objDaoMantenedor.UCInt(reader("IdProductoRef"))

                End With

                lista.Add(objDetalleDocumento)

            End While

            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista

    End Function

    Public Sub DocumentoCotizacion_DeshacerMov(ByVal IdDocumento As Integer, ByVal DeleteAnexoDocumento As Boolean, ByVal DeleteDetalleDocumento As Boolean, ByVal DeleteAnexoDetalle As Boolean, ByVal DeleteDetalleConcepto As Boolean, ByVal DeleteMontoRegimen As Boolean, ByVal DeleteObservacion As Boolean, ByVal DeletePuntoPartida As Boolean, ByVal DeletePuntoLlegada As Boolean, ByVal deleteRelacionDocumento As Boolean, ByVal Anular As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        cmd = New SqlCommand("_DocumentoCotizacion_DeshacerMov", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@DeleteAnexoDocumento", DeleteAnexoDocumento)
        cmd.Parameters.AddWithValue("@DeleteDetalleDocumento", DeleteDetalleDocumento)
        cmd.Parameters.AddWithValue("@DeleteAnexoDetalle", DeleteAnexoDetalle)
        cmd.Parameters.AddWithValue("@DeleteDetalleConcepto", DeleteDetalleConcepto)
        cmd.Parameters.AddWithValue("@DeleteMontoRegimen", DeleteMontoRegimen)
        cmd.Parameters.AddWithValue("@DeleteObservacion", DeleteObservacion)
        cmd.Parameters.AddWithValue("@DeletePuntoPartida", DeletePuntoPartida)
        cmd.Parameters.AddWithValue("@DeletePuntoLlegada", DeletePuntoLlegada)
        cmd.Parameters.AddWithValue("@deleteRelacionDocumento", deleteRelacionDocumento)
        cmd.Parameters.AddWithValue("@Anular", Anular)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try


    End Sub


    Public Function DocumentoCotizacionSelectDetalleConcepto(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleConcepto)

        Dim lista As New List(Of Entidades.DetalleConcepto)

        Try

            cn = objConexion.ConexionSIGE
            cmd = New SqlCommand("_DocumentoCotizacionSelectDetalleConcepto", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            cn.Open()
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            While (lector.Read)

                Dim objDetalleConcepto As New Entidades.DetalleConcepto
                With objDetalleConcepto

                    .IdDocumento = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                    .IdDetalleConcepto = CInt(IIf(IsDBNull(lector("IdDetalleConcepto")) = True, 0, lector("IdDetalleConcepto")))
                    .Concepto = CStr(IIf(IsDBNull(lector("dr_Concepto")) = True, "", lector("dr_Concepto")))
                    .IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")) = True, 0, lector("IdMoneda")))
                    .Monto = CDec(IIf(IsDBNull(lector("dr_Monto")) = True, 0, lector("dr_Monto")))
                    .IdConcepto = CInt(IIf(IsDBNull(lector("IdConcepto")) = True, 0, lector("IdConcepto")))
                    .Moneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")) = True, "", lector("mon_Simbolo")))
                    .IdDocumentoRef = CInt(IIf(IsDBNull(lector("IdDocumentoRef")) = True, 0, lector("IdDocumentoRef")))
                    .ConceptoAdelanto = CBool(IIf(IsDBNull(lector("con_ConceptoAdelanto")) = True, 0, lector("con_ConceptoAdelanto")))

                    .PorcentDetraccion = objDaoMantenedor.UCDec(lector("con_PorcentDetraccion"))
                    .MontoMinDetraccion = objDaoMantenedor.UCDec(lector("con_MontoMinDetraccion"))
                    .NoAfectoIGV = objDaoMantenedor.UCBool(lector("con_NoAfectoIGV"))

                End With

                lista.Add(objDetalleConcepto)

            End While

            lector.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista


    End Function


    Public Function DocumentoCotizacionSelectDet(ByVal IdDocumento As Integer, ByVal Consignacion As Boolean) As List(Of Entidades.DetalleDocumento)

        Dim lista As New List(Of Entidades.DetalleDocumento)

        Try

            cn = objConexion.ConexionSIGE
            cmd = New SqlCommand("_DocumentoCotizacionSelectDet", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            cmd.Parameters.AddWithValue("@Consignacion", Consignacion)

            cn.Open()
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            While (lector.Read)

                Dim objDetalleDocumento As New Entidades.DetalleDocumento
                With objDetalleDocumento

                    .IdDetalleDocumento = CInt(IIf(IsDBNull(lector("IdDetalleDocumento")) = True, 0, lector("IdDetalleDocumento")))
                    .IdDetalleAfecto = CInt(IIf(IsDBNull(lector("IdDetalleAfecto")) = True, 0, lector("IdDetalleAfecto")))
                    .IdDocumento = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                    .IdProducto = CInt(IIf(IsDBNull(lector("IdProducto")) = True, 0, lector("IdProducto")))
                    .IdUnidadMedida = CInt(IIf(IsDBNull(lector("IdUnidadMedida")) = True, 0, lector("IdUnidadMedida")))
                    .Cantidad = CDec(IIf(IsDBNull(lector("dc_Cantidad")) = True, 0, lector("dc_Cantidad")))
                    .PrecioLista = CDec(IIf(IsDBNull(lector("dc_PrecioLista")) = True, 0, lector("dc_PrecioLista")))
                    .PrecioSD = CDec(IIf(IsDBNull(lector("dc_PrecioSD")) = True, 0, lector("dc_PrecioSD")))
                    .Descuento = CDec(IIf(IsDBNull(lector("dc_Descuento")) = True, 0, lector("dc_Descuento")))
                    .PorcentDcto = CDec(IIf(IsDBNull(lector("PorcentDcto")) = True, 0, lector("PorcentDcto")))
                    .PrecioCD = CDec(IIf(IsDBNull(lector("dc_PrecioCD")) = True, 0, lector("dc_PrecioCD")))
                    .Importe = CDec(IIf(IsDBNull(lector("dc_Importe")) = True, 0, lector("dc_Importe")))
                    .TasaPercepcion = CDec(IIf(IsDBNull(lector("dc_TasaPercepcion")) = True, 0, lector("dc_TasaPercepcion")))
                    .DetalleGlosa = CStr(IIf(IsDBNull(lector("dc_DetalleGlosa")) = True, "", lector("dc_DetalleGlosa")))
                    .IdUsuarioSupervisor = CInt(IIf(IsDBNull(lector("IdUsuarioSupervisor")) = True, 0, lector("IdUsuarioSupervisor")))
                    .IdTipoPV = CInt(IIf(IsDBNull(lector("IdTipoPV")) = True, 0, lector("IdTipoPV")))
                    .NomProducto = CStr(IIf(IsDBNull(lector("prod_Nombre")) = True, "", lector("prod_Nombre")))
                    .CodigoProducto = CStr(IIf(IsDBNull(lector("prod_Codigo")) = True, "", lector("prod_Codigo")))
                    .PorcentDctoMaximo = CDec(IIf(IsDBNull(lector("PorcentDctoMax")) = True, 0, lector("PorcentDctoMax")))
                    .PrecioBaseDcto = CStr(IIf(IsDBNull(lector("PrecioBaseDcto")) = True, "", lector("PrecioBaseDcto")))
                    .VolumenVentaMin = CDec(IIf(IsDBNull(lector("VolumenVentaMin")) = True, 0, lector("VolumenVentaMin")))
                    .pvComercial = CDec(IIf(IsDBNull(lector("PrecioComercial")) = True, 0, lector("PrecioComercial")))
                    .ComponenteKit = CBool(IIf(IsDBNull(lector("add_ComponenteKit")) = True, False, lector("add_ComponenteKit")))
                    .IdKit = CInt(IIf(IsDBNull(lector("IdKit")) = True, 0, lector("IdKit")))
                    .Kit = CBool(IIf(IsDBNull(lector("prod_Kit")) = True, False, lector("prod_Kit")))
                    .IdCampania = objDaoMantenedor.UCInt(lector("IdCampania"))
                    .ExisteCampania_Producto = objDaoMantenedor.UCBool(lector("ExisteCampania_Producto"))

                    .IdProductoAux = objDaoMantenedor.UCInt(lector("IdProductoRef"))
                End With

                lista.Add(objDetalleDocumento)

            End While

            lector.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista

    End Function

    Public Function DocumentoCotizacionSelectCab(ByVal IdSerie As Integer, ByVal nroDocumento As Integer, ByVal IdDocumento As Integer) As Entidades.DocumentoCotizacion

        Dim objDocumentoCotizacion As New Entidades.DocumentoCotizacion

        Try

            cn = objConexion.ConexionSIGE
            cmd = New SqlCommand("_DocumentoCotizacionSelectCab", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdSerie", IdSerie)
            cmd.Parameters.AddWithValue("@NroDocumento", nroDocumento)
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            cn.Open()
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            If (lector.Read) Then

                With objDocumentoCotizacion

                    .getObjTipoAgente.Tasa = CDec(IIf(IsDBNull(lector("TasaAgente")) = True, 0, lector("TasaAgente")))
                    .Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                    .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                    .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))
                    .FechaAEntregar = CDate(IIf(IsDBNull(lector("doc_FechaAentregar")) = True, Nothing, lector("doc_FechaAentregar")))
                    .FechaVenc = CDate(IIf(IsDBNull(lector("doc_FechaVenc")) = True, Nothing, lector("doc_FechaVenc")))
                    .ImporteTotal = CDec(IIf(IsDBNull(lector("doc_ImporteTotal")) = True, 0, lector("doc_ImporteTotal")))
                    .Descuento = CDec(IIf(IsDBNull(lector("doc_Descuento")) = True, 0, lector("doc_Descuento")))
                    .SubTotal = CDec(IIf(IsDBNull(lector("doc_SubTotal")) = True, 0, lector("doc_SubTotal")))
                    .IGV = CDec(IIf(IsDBNull(lector("doc_Igv")) = True, 0, lector("doc_Igv")))
                    .getObjImpuesto.Tasa = CDec(IIf(IsDBNull(lector("tasaIGV")) = True, 0, lector("tasaIGV")))
                    .Total = CDec(IIf(IsDBNull(lector("doc_Total")) = True, 0, lector("doc_Total")))
                    .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")) = True, 0, lector("doc_TotalAPagar")))
                    .Percepcion = CDec(IIf(IsDBNull(lector("Percepcion")) = True, 0, lector("Percepcion")))
                    .CompPercepcion = objDaoMantenedor.UCBool(lector("doc_CompPercepcion"))

                    .IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")) = True, 0, lector("IdPersona")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(lector("IdEstadoDoc")) = True, 0, lector("IdEstadoDoc")))
                    .IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")) = True, 0, lector("IdMoneda")))
                    .IdCondicionPago = CInt(IIf(IsDBNull(lector("IdCondicionPago")) = True, 0, lector("IdCondicionPago")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(lector("IdTipoOperacion")) = True, 0, lector("IdTipoOperacion")))
                    .IdMotivoT = CInt(IIf(IsDBNull(lector("IdMotivoT")) = True, 0, lector("IdMotivoT")))
                    .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")) = True, 0, lector("IdTienda")))
                    .IdCaja = CInt(IIf(IsDBNull(lector("IdCaja")) = True, 0, lector("IdCaja")))
                    .IdSerie = CInt(IIf(IsDBNull(lector("IdSerie")) = True, 0, lector("IdSerie")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector("IdEmpresa")) = True, 0, lector("IdEmpresa")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector("IdTipoDocumento")) = True, 0, lector("IdTipoDocumento")))
                    .IdAlmacen = CInt(IIf(IsDBNull(lector("IdAlmacen")) = True, 0, lector("IdAlmacen")))
                    .IdTipoPV = CInt(IIf(IsDBNull(lector("IdTipoPV")) = True, 0, lector("IdTipoPV")))
                    .getObjAnexoDocumento.IdMedioPago = CInt(IIf(IsDBNull(lector("IdMedioPago")) = True, 0, lector("IdMedioPago")))
                    .getObjAnexoDocumento.NroDiasVigencia = CDec(IIf(IsDBNull(lector("anex_NroDiasVigencia")) = True, 0, lector("anex_NroDiasVigencia")))
                    .getObjAnexoDocumento.TotalConcepto = CDec(IIf(IsDBNull(lector("anex_TotalConcepto")) = True, 0, lector("anex_TotalConcepto")))
                    .getObjAnexoDocumento.anex_DescuentoGlobal = CDec(IIf(IsDBNull(lector("anex_DescuentoGlobal")) = True, 0, lector("anex_DescuentoGlobal")))

                    .IdArea = CInt(IIf(IsDBNull(lector("IdArea")) = True, 0, lector("IdArea")))
                    .getObjPersonaView.ApPaterno = CStr(IIf(IsDBNull(lector("nat_Apepat")) = True, "", lector("nat_Apepat")))
                    .getObjPersonaView.ApMaterno = CStr(IIf(IsDBNull(lector("nat_Apemat")) = True, "", lector("nat_Apemat")))
                    .getObjPersonaView.Nombres = CStr(IIf(IsDBNull(lector("nat_Nombres")) = True, "", lector("nat_Nombres")))

                    .getObjPersonaView.TipoPersona = CInt(IIf(IsDBNull(lector("TipoPersona")) = True, "0", lector("TipoPersona")))
                    .getObjPersonaView.RazonSocial = CStr(IIf(IsDBNull(lector("jur_Rsocial")) = True, "", lector("jur_Rsocial")))
                    .getObjPersonaView.Dni = CStr(IIf(IsDBNull(lector("Dni")) = True, "", lector("Dni")))
                    .getObjPersonaView.Ruc = CStr(IIf(IsDBNull(lector("Ruc")) = True, "", lector("Ruc")))
                    .getObjPersonaView.Direccion = CStr(IIf(IsDBNull(lector("DireccionFiscal")) = True, "", lector("DireccionFiscal")))
                    .getObjTipoAgente.IdAgente = CInt(IIf(IsDBNull(lector("IdAgente")) = True, 0, lector("IdAgente")))
                    .getObjPersonaView.SujetoARetencion = objDaoMantenedor.UCBool(lector("SujetoARetencion"))
                    .getObjPersonaView.IdRol = CInt(IIf(IsDBNull(lector("IdRol")) = True, 0, lector("IdRol")))

                    .getObjMaestroObra.IdPersona = CInt(IIf(IsDBNull(lector("IdMaestroObra")) = True, 0, lector("IdMaestroObra")))
                    .getObjMaestroObra.Descripcion = CStr(IIf(IsDBNull(lector("MaestroObra_Descripcion")) = True, "", lector("MaestroObra_Descripcion")))
                    .getObjMaestroObra.Dni = CStr(IIf(IsDBNull(lector("MaestroObra_Dni")) = True, "", lector("MaestroObra_Dni")))
                    .getObjMaestroObra.Ruc = CStr(IIf(IsDBNull(lector("MaestroObra_Ruc")) = True, "", lector("MaestroObra_Ruc")))

                    .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))
                    .NomTipoOperacion = CStr(IIf(IsDBNull(lector("top_Nombre")) = True, "", lector("top_Nombre")))
                    .DescripcionPersona = CStr(IIf(IsDBNull(lector("DescripcionPersona")) = True, "", lector("DescripcionPersona")))
                    .NomCondicionPago = CStr(IIf(IsDBNull(lector("cp_Nombre")) = True, "", lector("cp_Nombre")))
                    .MedioPago = CStr(IIf(IsDBNull(lector("mp_Nombre")) = True, "", lector("mp_Nombre")))

                    .IdDocumentoRef = CInt(IIf(IsDBNull(lector("IdDocumentoRef")) = True, 0, lector("IdDocumentoRef")))
                    .IdUsuarioComision = CInt(IIf(IsDBNull(lector("IdUsuarioComision")) = True, 0, lector("IdUsuarioComision")))
                    .IdMedioPagoCredito = CInt(IIf(IsDBNull(lector("IdMedioPagoCredito")) = True, 0, lector("IdMedioPagoCredito")))

                    .Retencion = objDaoMantenedor.UCDec(lector("Retencion"))
                    .Detraccion = objDaoMantenedor.UCDec(lector("Detraccion"))

                End With

            Else

                If (Not (IdSerie = Nothing And nroDocumento = Nothing And IdDocumento = Nothing)) Then
                    Throw New Exception("No se hallaron registros.")
                End If

            End If

            lector.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumentoCotizacion

    End Function

    Public Function DocumentoCotizacionSelectCab22Factura(ByVal IdSerie As Integer, ByVal nroDocumento As String, ByVal IdDocumento As Integer) As Entidades.DocumentoCotizacion

        Dim objDocumentoCotizacion As New Entidades.DocumentoCotizacion

        Try

            cn = objConexion.ConexionSIGE
            cmd = New SqlCommand("_DocumentoCotizacionSelectCab22", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdSerie", IdSerie)
            cmd.Parameters.AddWithValue("@NroDocumento", nroDocumento)
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            cn.Open()
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            If (lector.Read) Then

                With objDocumentoCotizacion

                    .getObjTipoAgente.Tasa = CDec(IIf(IsDBNull(lector("TasaAgente")) = True, 0, lector("TasaAgente")))
                    .Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                    .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                    .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))
                    .FechaAEntregar = CDate(IIf(IsDBNull(lector("doc_FechaAentregar")) = True, Nothing, lector("doc_FechaAentregar")))
                    .FechaVenc = CDate(IIf(IsDBNull(lector("doc_FechaVenc")) = True, Nothing, lector("doc_FechaVenc")))
                    .ImporteTotal = CDec(IIf(IsDBNull(lector("doc_ImporteTotal")) = True, 0, lector("doc_ImporteTotal")))
                    .Descuento = CDec(IIf(IsDBNull(lector("doc_Descuento")) = True, 0, lector("doc_Descuento")))
                    .SubTotal = CDec(IIf(IsDBNull(lector("doc_SubTotal")) = True, 0, lector("doc_SubTotal")))
                    .IGV = CDec(IIf(IsDBNull(lector("doc_Igv")) = True, 0, lector("doc_Igv")))
                    .getObjImpuesto.Tasa = CDec(IIf(IsDBNull(lector("tasaIGV")) = True, 0, lector("tasaIGV")))
                    .Total = CDec(IIf(IsDBNull(lector("doc_Total")) = True, 0, lector("doc_Total")))
                    .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")) = True, 0, lector("doc_TotalAPagar")))
                    .Percepcion = CDec(IIf(IsDBNull(lector("Percepcion")) = True, 0, lector("Percepcion")))
                    .CompPercepcion = objDaoMantenedor.UCBool(lector("doc_CompPercepcion"))

                    .IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")) = True, 0, lector("IdPersona")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(lector("IdEstadoDoc")) = True, 0, lector("IdEstadoDoc")))
                    .IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")) = True, 0, lector("IdMoneda")))
                    .IdCondicionPago = CInt(IIf(IsDBNull(lector("IdCondicionPago")) = True, 0, lector("IdCondicionPago")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(lector("IdTipoOperacion")) = True, 0, lector("IdTipoOperacion")))
                    .IdMotivoT = CInt(IIf(IsDBNull(lector("IdMotivoT")) = True, 0, lector("IdMotivoT")))
                    .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")) = True, 0, lector("IdTienda")))
                    .IdCaja = CInt(IIf(IsDBNull(lector("IdCaja")) = True, 0, lector("IdCaja")))
                    .IdSerie = CInt(IIf(IsDBNull(lector("IdSerie")) = True, 0, lector("IdSerie")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector("IdEmpresa")) = True, 0, lector("IdEmpresa")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector("IdTipoDocumento")) = True, 0, lector("IdTipoDocumento")))
                    .IdAlmacen = CInt(IIf(IsDBNull(lector("IdAlmacen")) = True, 0, lector("IdAlmacen")))
                    .IdTipoPV = CInt(IIf(IsDBNull(lector("IdTipoPV")) = True, 0, lector("IdTipoPV")))
                    .getObjAnexoDocumento.IdMedioPago = CInt(IIf(IsDBNull(lector("IdMedioPago")) = True, 0, lector("IdMedioPago")))
                    .getObjAnexoDocumento.NroDiasVigencia = CDec(IIf(IsDBNull(lector("anex_NroDiasVigencia")) = True, 0, lector("anex_NroDiasVigencia")))
                    .getObjAnexoDocumento.TotalConcepto = CDec(IIf(IsDBNull(lector("anex_TotalConcepto")) = True, 0, lector("anex_TotalConcepto")))
                    .getObjAnexoDocumento.anex_DescuentoGlobal = CDec(IIf(IsDBNull(lector("anex_DescuentoGlobal")) = True, 0, lector("anex_DescuentoGlobal")))

                    .IdArea = CInt(IIf(IsDBNull(lector("IdArea")) = True, 0, lector("IdArea")))
                    .getObjPersonaView.ApPaterno = CStr(IIf(IsDBNull(lector("nat_Apepat")) = True, "", lector("nat_Apepat")))
                    .getObjPersonaView.ApMaterno = CStr(IIf(IsDBNull(lector("nat_Apemat")) = True, "", lector("nat_Apemat")))
                    .getObjPersonaView.Nombres = CStr(IIf(IsDBNull(lector("nat_Nombres")) = True, "", lector("nat_Nombres")))

                    .getObjPersonaView.TipoPersona = CInt(IIf(IsDBNull(lector("TipoPersona")) = True, "0", lector("TipoPersona")))
                    .getObjPersonaView.RazonSocial = CStr(IIf(IsDBNull(lector("jur_Rsocial")) = True, "", lector("jur_Rsocial")))
                    .getObjPersonaView.Dni = CStr(IIf(IsDBNull(lector("Dni")) = True, "", lector("Dni")))
                    .getObjPersonaView.Ruc = CStr(IIf(IsDBNull(lector("Ruc")) = True, "", lector("Ruc")))
                    .getObjPersonaView.Direccion = CStr(IIf(IsDBNull(lector("DireccionFiscal")) = True, "", lector("DireccionFiscal")))
                    .getObjTipoAgente.IdAgente = CInt(IIf(IsDBNull(lector("IdAgente")) = True, 0, lector("IdAgente")))
                    .getObjPersonaView.SujetoARetencion = objDaoMantenedor.UCBool(lector("SujetoARetencion"))
                    .getObjPersonaView.IdRol = CInt(IIf(IsDBNull(lector("IdRol")) = True, 0, lector("IdRol")))

                    .getObjMaestroObra.IdPersona = CInt(IIf(IsDBNull(lector("IdMaestroObra")) = True, 0, lector("IdMaestroObra")))
                    .getObjMaestroObra.Descripcion = CStr(IIf(IsDBNull(lector("MaestroObra_Descripcion")) = True, "", lector("MaestroObra_Descripcion")))
                    .getObjMaestroObra.Dni = CStr(IIf(IsDBNull(lector("MaestroObra_Dni")) = True, "", lector("MaestroObra_Dni")))
                    .getObjMaestroObra.Ruc = CStr(IIf(IsDBNull(lector("MaestroObra_Ruc")) = True, "", lector("MaestroObra_Ruc")))

                    .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))
                    .NomTipoOperacion = CStr(IIf(IsDBNull(lector("top_Nombre")) = True, "", lector("top_Nombre")))
                    .DescripcionPersona = CStr(IIf(IsDBNull(lector("DescripcionPersona")) = True, "", lector("DescripcionPersona")))
                    .NomCondicionPago = CStr(IIf(IsDBNull(lector("cp_Nombre")) = True, "", lector("cp_Nombre")))
                    .MedioPago = CStr(IIf(IsDBNull(lector("mp_Nombre")) = True, "", lector("mp_Nombre")))

                    .IdDocumentoRef = CInt(IIf(IsDBNull(lector("IdDocumentoRef")) = True, 0, lector("IdDocumentoRef")))
                    .IdUsuarioComision = CInt(IIf(IsDBNull(lector("IdUsuarioComision")) = True, 0, lector("IdUsuarioComision")))
                    .IdMedioPagoCredito = CInt(IIf(IsDBNull(lector("IdMedioPagoCredito")) = True, 0, lector("IdMedioPagoCredito")))

                    .Retencion = objDaoMantenedor.UCDec(lector("Retencion"))
                    .Detraccion = objDaoMantenedor.UCDec(lector("Detraccion"))

                End With

            Else

                If (Not (IdSerie = Nothing And nroDocumento = Nothing And IdDocumento = Nothing)) Then
                    Throw New Exception("No se hallaron registros.")
                End If

            End If

            lector.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumentoCotizacion

    End Function
    Public Function DocumentoCotizacion_ValSelectTipoPrecioPV(ByVal IdUsuario As Integer, ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer, ByVal fecha As Date, ByVal IdMoneda As Integer, ByVal IdTienda As Integer, ByVal IdCliente As Integer, ByVal IdTipoPV_Destino As Integer, ByVal Cad_IdProducto As String, ByVal Cad_IdUnidadMedida As String, ByVal Cad_Cantidad As String, ByVal IdTipoOperacion As Integer) As Entidades.Catalogo

        Dim objCatalogo As New Entidades.Catalogo

        Try

            cn = objConexion.ConexionSIGE
            cn.Open()
            cmd = New SqlCommand("_DocumentoCotizacion_ValSelectTipoPrecioPV", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario)
            cmd.Parameters.AddWithValue("@IdCliente", IdCliente)
            cmd.Parameters.AddWithValue("@IdTipoPV_Destino", IdTipoPV_Destino)
            cmd.Parameters.AddWithValue("@Cad_IdProducto", Cad_IdProducto)
            cmd.Parameters.AddWithValue("@Cad_IdUnidadMedida", Cad_IdUnidadMedida)
            cmd.Parameters.AddWithValue("@Cad_Cantidad", Cad_Cantidad)
            cmd.Parameters.AddWithValue("@IdCondicionPago", IdCondicionPago)
            cmd.Parameters.AddWithValue("@IdMedioPago", IdMedioPago)
            cmd.Parameters.AddWithValue("@fecha", fecha)
            cmd.Parameters.AddWithValue("@IdMoneda", IdMoneda)
            cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
            cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion)




            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If (lector.Read) Then
                objCatalogo.PrecioLista = CDec(IIf(IsDBNull(lector("PrecioLista")) = True, 0, lector("PrecioLista")))
                objCatalogo.pvComercial = CDec(IIf(IsDBNull(lector("PrecioComercial")) = True, 0, lector("PrecioComercial")))
                objCatalogo.PorcentDctoMaximo = CDec(IIf(IsDBNull(lector("PorcentDcto")) = True, 0, lector("PorcentDcto")))
                objCatalogo.PrecioBaseDcto = CStr(IIf(IsDBNull(lector("PrecioBaseDcto")) = True, "", lector("PrecioBaseDcto")))
                objCatalogo.VolumenVenta = CDec(IIf(IsDBNull(lector("VolumenVentaMin_Eq")) = True, 0, lector("VolumenVentaMin_Eq")))
            Else
                Throw New Exception("EL PRODUCTO SELECCIONADO NO POSEE UN PRECIO DE LISTA.")
            End If

            cn.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objCatalogo

    End Function


    Public Function SelectValorEstimadoConcepto(ByVal IdConcepto As Integer, ByVal IdTipoDocumento As Integer, ByVal UbigeoOrigen As String, ByVal UbigeoDestino As String, ByVal IdMoneda_Destino As Integer, ByVal PesoTotal As Decimal, ByVal ImporteTotal As Decimal, ByVal Fecha As Date) As Decimal

        Dim valor As Decimal = 0

        Try

            cn = objConexion.ConexionSIGE
            cn.Open()

            cmd = New SqlCommand("_DocumentoCotizacionSelectValorEstimadoConcepto", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdConcepto", IdConcepto)
            cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
            cmd.Parameters.AddWithValue("@UbigeoOrigen", UbigeoOrigen)
            cmd.Parameters.AddWithValue("@UbigeoDestino", UbigeoDestino)
            cmd.Parameters.AddWithValue("@IdMoneda_Destino", IdMoneda_Destino)
            cmd.Parameters.AddWithValue("@PesoTotal", PesoTotal)
            cmd.Parameters.AddWithValue("@ImporteTotal", ImporteTotal)
            cmd.Parameters.AddWithValue("@Fecha", Fecha)
            valor = CDec(cmd.ExecuteScalar)

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return valor

    End Function

    Public Function DocumentoCotizacion_ConsultarTipoPrecioxParams(ByVal IdTienda As Integer, ByVal IdProducto As Integer, ByVal IdMoneda_Destino As Integer, ByVal Fecha As Date) As List(Of Entidades.Catalogo)

        Dim lista As New List(Of Entidades.Catalogo)
        Try
            cn = objConexion.ConexionSIGE
            cn.Open()
            cmd = New SqlCommand("_DocumentoCotizacion_ConsultarTipoPrecioxParams", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
            cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
            cmd.Parameters.AddWithValue("@IdMoneda_Destino", IdMoneda_Destino)
            cmd.Parameters.AddWithValue("@Fecha", Fecha)

            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            While (lector.Read)

                Dim obj As New Entidades.Catalogo
                With obj

                    .NomTipoPV = CStr(IIf(IsDBNull(lector("pv_Nombre")) = True, "", lector("pv_Nombre")))
                    .SimbMoneda = CStr(IIf(IsDBNull(lector("Moneda_Destino")) = True, "", lector("Moneda_Destino")))
                    .PrecioLista = CDec(IIf(IsDBNull(lector("PrecioLista")) = True, 0, lector("PrecioLista")))
                    .cadenaUM = CStr(IIf(IsDBNull(lector("um_NombreCorto")) = True, "", lector("um_NombreCorto")))

                End With
                lista.Add(obj)

            End While

            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista

    End Function


    Public Function DocumentoCotizacion_ConsultarTipoPrecioxParams2(ByVal IdTienda As Integer, ByVal IdProducto As Integer, ByVal IdMoneda_Destino As Integer, ByVal Fecha As Date) As List(Of Entidades.Catalogo)

        Dim lista As New List(Of Entidades.Catalogo)
        Try
            cn = objConexion.ConexionSIGE2
            cn.Open()
            cmd = New SqlCommand("_DocumentoCotizacion_ConsultarTipoPrecioxParams", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
            cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
            cmd.Parameters.AddWithValue("@IdMoneda_Destino", IdMoneda_Destino)
            cmd.Parameters.AddWithValue("@Fecha", Fecha)

            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            While (lector.Read)

                Dim obj As New Entidades.Catalogo
                With obj

                    .NomTipoPV = CStr(IIf(IsDBNull(lector("pv_Nombre")) = True, "", lector("pv_Nombre")))
                    .SimbMoneda = CStr(IIf(IsDBNull(lector("Moneda_Destino")) = True, "", lector("Moneda_Destino")))
                    .PrecioLista = CDec(IIf(IsDBNull(lector("PrecioLista")) = True, 0, lector("PrecioLista")))
                    .cadenaUM = CStr(IIf(IsDBNull(lector("um_NombreCorto")) = True, "", lector("um_NombreCorto")))

                End With
                lista.Add(obj)

            End While

            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista

    End Function

End Class
