﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



'***************************   MARTES 06 DE ABRIL 2010 12_02 PM




Imports System.Data.SqlClient
Public Class DAODocumentoCompPercepcion

    Private objConexion As New DAO.Conexion

    Public Function DocumentoPercepcionPrint(ByVal IdDocumento As Integer) As DataSet

        Dim ds As New DataSet
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim da As SqlDataAdapter
        Dim cmd As SqlCommand
        Try
            cmd = New SqlCommand("_CR_DocumentoCompPercepcionCab", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocumentoCompPercepcionCab")

            cmd = New SqlCommand("_CR_DocumentoCompPercepcionDet", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocumentoCompPercepcionDet")

        Catch ex As Exception

            Throw ex
        Finally

        End Try

        Return ds

    End Function



    Public Function DocumentoCompPercepcion_GenerarxIdDocumentoRef(ByVal IdDocumentoRef As Integer, ByVal IdMonedaDestino As Integer) As Entidades.DocumentoCompPercepcion

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoCompPercepcion_GenerarxIdDocumentoRef", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumentoRef", IdDocumentoRef)
        cmd.Parameters.AddWithValue("@IdMonedaDestino", IdMonedaDestino)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim obj As New Entidades.DocumentoCompPercepcion
                If lector.Read Then

                    With obj

                        obj.getTipoDocumento.CodigoSunat = CStr(IIf(IsDBNull(lector("tdoc_CodigoSunat")) = True, "", lector("tdoc_CodigoSunat")))
                        obj.getTipoDocumento.DescripcionCorto = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))
                        obj.Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                        obj.Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                        obj.Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                        obj.FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))
                        obj.IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")) = True, 0, lector("IdMoneda")))
                        obj.NomMoneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")) = True, "", lector("mon_Simbolo")))
                        obj.TotalSoles = CDec(IIf(IsDBNull(lector("Total_Soles")) = True, 0, lector("Total_Soles")))
                        obj.TotalDolares = CDec(IIf(IsDBNull(lector("Total_Dolares")) = True, 0, lector("Total_Dolares")))
                        obj.TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")) = True, 0, lector("doc_TotalAPagar")))
                        obj.NomCondicionPago = CStr(IIf(IsDBNull(lector("cp_Nombre")) = True, "", lector("cp_Nombre")))
                        obj.IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")) = True, 0, lector("IdPersona")))
                        obj.PorcentPercepcion = CDec(IIf(IsDBNull(lector("PorcentPercepcion")) = True, 0, lector("PorcentPercepcion")))

                        obj.MonedaDestino = CStr(IIf(IsDBNull(lector("MonedaDestino")) = True, "", lector("MonedaDestino")))
                        obj.TotalAPagarEq = CDec(IIf(IsDBNull(lector("doc_TotalAPagar_Eq")) = True, 0, lector("doc_TotalAPagar_Eq")))
                        obj.TipoCambio = CDec(IIf(IsDBNull(lector("TipoCambio")) = True, 0, lector("TipoCambio")))
                        obj.Percepcion = CDec(IIf(IsDBNull(lector("ImportePercepcion")) = True, 0, lector("ImportePercepcion")))

                        obj.ImporteTotal = CDec(IIf(IsDBNull(lector("ImporteTotal")) = True, 0, lector("ImporteTotal")))
                        obj.NroDocumento = .Serie + " - " + .Codigo

                        obj.DescripcionPersona = CStr(IIf(IsDBNull(lector("DescPersona")) = True, "", lector("DescPersona")))
                        obj.Ruc = CStr(IIf(IsDBNull(lector("Ruc")) = True, "", lector("Ruc")))
                        obj.Dni = CStr(IIf(IsDBNull(lector("Dni")) = True, "", lector("Dni")))

                    End With

                Else

                    Throw New Exception("No se hallaron registros para el Documento de Referencia. Verificar si el Documento está hábil para la Generación del Doc. Comprobante de Percepción.")

                End If
                lector.Close()
                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function





    Public Sub DocumentoCompPercepcion_DeshacerMov(ByVal IdDocumento As Integer, ByVal DeletePagoCaja As Boolean, ByVal DeleteMovCaja As Boolean, ByVal DeleteMovCuenta As Boolean, ByVal DeleteDetalleDocR As Boolean, ByVal DeleteRelacionDoc As Boolean, ByVal UpdateEstadoDoc_Anular As Boolean, ByVal ValAbonos As Boolean, ByVal ValActivoToDeshacer As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As New SqlCommand("_DocumentoCompPercepcion_DeshacerMov", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@DeletePagoCaja", DeletePagoCaja)
        cmd.Parameters.AddWithValue("@DeleteMovCaja", DeleteMovCaja)
        cmd.Parameters.AddWithValue("@DeleteMovCuenta", DeleteMovCuenta)
        cmd.Parameters.AddWithValue("@DeleteDetalleDocR", DeleteDetalleDocR)
        cmd.Parameters.AddWithValue("@DeleteRelacionDoc", DeleteRelacionDoc)
        cmd.Parameters.AddWithValue("@UpdateEstadoDoc_Anular", UpdateEstadoDoc_Anular)
        cmd.Parameters.AddWithValue("@ValAbonos", ValAbonos)
        cmd.Parameters.AddWithValue("@ValActivoToDeshacer", ValActivoToDeshacer)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try


    End Sub



    Public Function DocumentoCompPercepcionSelectDetalle(ByVal IdDocumento As Integer) As List(Of Entidades.DocumentoCompPercepcion)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoCompPercepcionSelectDetalle", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DocumentoCompPercepcion)
                Do While lector.Read
                    Dim obj As New Entidades.DocumentoCompPercepcion
                    With obj


                        obj.Id = CInt(IIf(IsDBNull(lector("IdDocumentoRef")) = True, 0, lector("IdDocumentoRef")))
                        obj.TotalAPagar = CDec(IIf(IsDBNull(lector("dcr_TotalDocumento")) = True, 0, lector("dcr_TotalDocumento")))
                        obj.NroDocumento = CStr(IIf(IsDBNull(lector("NroDocumentoRef")) = True, "", lector("NroDocumentoRef")))
                        obj.FechaEmision = CDate(IIf(IsDBNull(lector("dcr_FechaEmision")) = True, Nothing, lector("dcr_FechaEmision")))
                        obj.TipoCambio = CDec(IIf(IsDBNull(lector("dcr_TipoCambio")) = True, 0, lector("dcr_TipoCambio")))
                        obj.PorcentPercepcion = CDec(IIf(IsDBNull(lector("dcr_Porcentaje")) = True, 0, lector("dcr_Porcentaje")))
                        obj.Percepcion = CDec(IIf(IsDBNull(lector("dcr_Importe")) = True, 0, lector("dcr_Importe")))
                        obj.ImporteTotal = CDec(IIf(IsDBNull(lector("dcr_ImporteFinal")) = True, 0, lector("dcr_ImporteFinal")))
                        obj.IdMoneda = CInt(IIf(IsDBNull(lector("IdMonedaDocRef")) = True, 0, lector("IdMonedaDocRef")))
                        obj.NomMoneda = CStr(IIf(IsDBNull(lector("MonedaOrigen")) = True, "", lector("MonedaOrigen")))
                        obj.getTipoDocumento.DescripcionCorto = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))
                        obj.getTipoDocumento.CodigoSunat = CStr(IIf(IsDBNull(lector("tdoc_CodigoSunat")) = True, "", lector("tdoc_CodigoSunat")))
                        obj.MonedaDestino = CStr(IIf(IsDBNull(lector("MonedaDestino")) = True, "", lector("MonedaDestino")))

                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function





    Public Function DocumentoCompPercepcionSelectCab(ByVal IdSerie As Integer, ByVal Codigo As Integer, ByVal IdDocumento As Integer) As Entidades.DocumentoCompPercepcion

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim objDocumento As Entidades.DocumentoCompPercepcion = Nothing
        Try

            cn.Open()

            Dim cmd As New SqlCommand("_DocumentoComprobantePercepcionSelectCab", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdSerie", IdSerie)
            cmd.Parameters.AddWithValue("@Codigo", Codigo)
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            Dim dr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            If (dr.Read) Then
                objDocumento = New Entidades.DocumentoCompPercepcion

                With objDocumento

                    .Id = CInt(IIf(IsDBNull(dr("IdDocumento")), 0, dr("IdDocumento")))
                    .Codigo = CStr(IIf(IsDBNull(dr("doc_Codigo")), "", dr("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(dr("doc_Serie")), "", dr("doc_Serie")))
                    .FechaEmision = CDate(IIf(IsDBNull(dr("doc_FechaEmision")), Nothing, dr("doc_FechaEmision")))
                    .FechaVenc = CDate(IIf(IsDBNull(dr("doc_FechaVenc")), Nothing, dr("doc_FechaVenc")))
                    .TotalAPagar = CDec(IIf(IsDBNull(dr("doc_TotalAPagar")), 0, dr("doc_TotalAPagar")))
                    .IdPersona = CInt(IIf(IsDBNull(dr("IdPersona")), 0, dr("IdPersona")))
                    .DescripcionPersona = CStr(IIf(IsDBNull(dr("DescPersona")), "", dr("DescPersona")))
                    .Ruc = CStr(IIf(IsDBNull(dr("RUC")), "", dr("RUC")))
                    .Dni = CStr(IIf(IsDBNull(dr("DNI")), "", dr("DNI")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(dr("IdEstadoDoc")), 0, dr("IdEstadoDoc")))
                    .IdCondicionPago = CInt(IIf(IsDBNull(dr("IdCondicionPago")), 0, dr("IdCondicionPago")))
                    .IdMoneda = CInt(IIf(IsDBNull(dr("IdMoneda")), 0, dr("IdMoneda")))
                    .IdTienda = CInt(IIf(IsDBNull(dr("IdTienda")), 0, dr("IdTienda")))
                    .IdSerie = CInt(IIf(IsDBNull(dr("IdSerie")), 0, dr("IdSerie")))
                    .IdEmpresa = CInt(IIf(IsDBNull(dr("IdEmpresa")), 0, dr("IdEmpresa")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(dr("IdTipoOperacion")), 0, dr("IdTipoOperacion")))

                    .IdTipoDocumento = CInt(IIf(IsDBNull(dr("IdTipoDocumento")), 0, dr("IdTipoDocumento")))
                    .IdEstadoCancelacion = CInt(IIf(IsDBNull(dr("IdEstadoCan")), 0, dr("IdEstadoCan")))
                    .IdCaja = CInt(IIf(IsDBNull(dr("IdCaja")), 0, dr("IdCaja")))

                    .ContAbonos = CInt(IIf(IsDBNull(dr("ContAbonos")), 0, dr("ContAbonos")))
                End With

            Else

                Throw New Exception("El Documento NO EXISTE.")

            End If

            dr.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento

    End Function

    Public Function DocumentoCompPercepcionSelectDocReferencia_Find(ByVal IdPersona As Integer, ByVal IdMonedaDestino As Integer) As List(Of Entidades.DocumentoCompPercepcion)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoCompPercepcionSelectDocReferencia_Find", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdMonedaDestino", IdMonedaDestino)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DocumentoCompPercepcion)
                Do While lector.Read
                    Dim obj As New Entidades.DocumentoCompPercepcion
                    With obj

                        obj.getTipoDocumento.CodigoSunat = CStr(IIf(IsDBNull(lector("tdoc_CodigoSunat")) = True, "", lector("tdoc_CodigoSunat")))
                        obj.getTipoDocumento.DescripcionCorto = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))
                        obj.Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                        obj.Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                        obj.Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                        obj.FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))
                        obj.IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")) = True, 0, lector("IdMoneda")))
                        obj.NomMoneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")) = True, "", lector("mon_Simbolo")))
                        obj.TotalSoles = CDec(IIf(IsDBNull(lector("Total_Soles")) = True, 0, lector("Total_Soles")))
                        obj.TotalDolares = CDec(IIf(IsDBNull(lector("Total_Dolares")) = True, 0, lector("Total_Dolares")))
                        obj.TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")) = True, 0, lector("doc_TotalAPagar")))
                        obj.NomCondicionPago = CStr(IIf(IsDBNull(lector("cp_Nombre")) = True, "", lector("cp_Nombre")))
                        obj.IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")) = True, 0, lector("IdPersona")))
                        obj.PorcentPercepcion = CDec(IIf(IsDBNull(lector("PorcentPercepcion")) = True, 0, lector("PorcentPercepcion")))

                        obj.MonedaDestino = CStr(IIf(IsDBNull(lector("MonedaDestino")) = True, "", lector("MonedaDestino")))
                        obj.TotalAPagarEq = CDec(IIf(IsDBNull(lector("doc_TotalAPagar_Eq")) = True, 0, lector("doc_TotalAPagar_Eq")))
                        obj.TipoCambio = CDec(IIf(IsDBNull(lector("TipoCambio")) = True, 0, lector("TipoCambio")))
                        obj.Percepcion = CDec(IIf(IsDBNull(lector("ImportePercepcion")) = True, 0, lector("ImportePercepcion")))

                        obj.ImporteTotal = CDec(IIf(IsDBNull(lector("ImporteTotal")) = True, 0, lector("ImporteTotal")))
                        obj.NroDocumento = .Serie + " - " + .Codigo
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function


    Public Function DocumentoCompPercepcionAnticipo(ByVal IdDocumento As Integer, ByVal IdMonedaDestino As Integer, ByVal PorcentPercepcion As Decimal) As List(Of Entidades.DocumentoCompPercepcion)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("[_DocumentoCompPercepcionAnticipo]", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@IdMonedaDestino", IdMonedaDestino)
        cmd.Parameters.AddWithValue("@PorcentPercepcion", PorcentPercepcion)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DocumentoCompPercepcion)
                Do While lector.Read
                    Dim obj As New Entidades.DocumentoCompPercepcion
                    With obj

                        obj.getTipoDocumento.CodigoSunat = CStr(IIf(IsDBNull(lector("tdoc_CodigoSunat")) = True, "", lector("tdoc_CodigoSunat")))
                        obj.getTipoDocumento.DescripcionCorto = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))
                        obj.Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                        obj.Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                        obj.Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                        obj.FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))
                        obj.IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")) = True, 0, lector("IdMoneda")))
                        obj.NomMoneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")) = True, "", lector("mon_Simbolo")))
                        obj.TotalSoles = CDec(IIf(IsDBNull(lector("Total_Soles")) = True, 0, lector("Total_Soles")))
                        obj.TotalDolares = CDec(IIf(IsDBNull(lector("Total_Dolares")) = True, 0, lector("Total_Dolares")))
                        obj.TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")) = True, 0, lector("doc_TotalAPagar")))
                        obj.NomCondicionPago = CStr(IIf(IsDBNull(lector("cp_Nombre")) = True, "", lector("cp_Nombre")))
                        obj.IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")) = True, 0, lector("IdPersona")))
                        obj.PorcentPercepcion = CDec(IIf(IsDBNull(lector("PorcentPercepcion")) = True, 0, lector("PorcentPercepcion")))

                        obj.MonedaDestino = CStr(IIf(IsDBNull(lector("MonedaDestino")) = True, "", lector("MonedaDestino")))
                        obj.TotalAPagarEq = CDec(IIf(IsDBNull(lector("doc_TotalAPagar_Eq")) = True, 0, lector("doc_TotalAPagar_Eq")))
                        obj.TipoCambio = CDec(IIf(IsDBNull(lector("TipoCambio")) = True, 0, lector("TipoCambio")))
                        obj.Percepcion = CDec(IIf(IsDBNull(lector("ImportePercepcion")) = True, 0, lector("ImportePercepcion")))

                        obj.ImporteTotal = CDec(IIf(IsDBNull(lector("ImporteTotal")) = True, 0, lector("ImporteTotal")))
                        obj.NroDocumento = .Serie + " - " + .Codigo
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function


    Public Function DocumentoCompPercepcion_NC(ByVal IdDocumento As Integer, ByVal IdMonedaDestino As Integer, ByVal PorcentPercepcion As Decimal) As List(Of Entidades.DocumentoCompPercepcion)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("[dbo].[_DocumentoCompPercepcion_NC]", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@IdMonedaDestino", IdMonedaDestino)
        cmd.Parameters.AddWithValue("@Percepcion", PorcentPercepcion)


        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DocumentoCompPercepcion)
                Do While lector.Read
                    Dim obj As New Entidades.DocumentoCompPercepcion
                    With obj

                        obj.getTipoDocumento.CodigoSunat = CStr(IIf(IsDBNull(lector("tdoc_CodigoSunat")) = True, "", lector("tdoc_CodigoSunat")))
                        obj.getTipoDocumento.DescripcionCorto = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))
                        obj.Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                        obj.Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                        obj.Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                        obj.FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))
                        obj.IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")) = True, 0, lector("IdMoneda")))
                        obj.NomMoneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")) = True, "", lector("mon_Simbolo")))
                        obj.TotalSoles = CDec(IIf(IsDBNull(lector("Total_Soles")) = True, 0, lector("Total_Soles")))
                        obj.TotalDolares = CDec(IIf(IsDBNull(lector("Total_Dolares")) = True, 0, lector("Total_Dolares")))
                        obj.TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")) = True, 0, lector("doc_TotalAPagar")))
                        obj.NomCondicionPago = CStr(IIf(IsDBNull(lector("cp_Nombre")) = True, "", lector("cp_Nombre")))
                        obj.IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")) = True, 0, lector("IdPersona")))
                        obj.PorcentPercepcion = CDec(IIf(IsDBNull(lector("PorcentPercepcion")) = True, 0, lector("PorcentPercepcion")))

                        obj.MonedaDestino = CStr(IIf(IsDBNull(lector("MonedaDestino")) = True, "", lector("MonedaDestino")))
                        obj.TotalAPagarEq = CDec(IIf(IsDBNull(lector("doc_TotalAPagar_Eq")) = True, 0, lector("doc_TotalAPagar_Eq")))
                        obj.TipoCambio = CDec(IIf(IsDBNull(lector("TipoCambio")) = True, 0, lector("TipoCambio")))
                        obj.Percepcion = CDec(IIf(IsDBNull(lector("ImportePercepcion")) = True, 0, lector("ImportePercepcion")))

                        obj.ImporteTotal = CDec(IIf(IsDBNull(lector("ImporteTotal")) = True, 0, lector("ImporteTotal")))
                        obj.NroDocumento = .Serie + " - " + .Codigo
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function



End Class

