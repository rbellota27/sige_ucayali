﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOContabilidad

    Private objConexion As New DAO.Conexion


    Public Sub ExpotarContabilidad_Ventas(ByVal IdEmpresa As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Try
            Dim cmd As New SqlCommand("_DocumentosExportaContabilidad", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
            cmd.Parameters.AddWithValue("@DESDE", Format(FechaInicio, "dd/MM/yyyy"))
            cmd.Parameters.AddWithValue("@HASTA", Format(FechaFin, "dd/MM/yyyy"))

            cmd.ExecuteNonQuery()
        Catch ex As Exception
        Finally

        End Try
    End Sub

    Public Function get_DS_ReportePercepcion_PDT(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdTipoDocumento As Integer, ByVal IdMonedaDestino As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date) As DataSet

        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE()
        Try

            Dim cmd As New SqlCommand("_A_Contabilidad_Rpt_Percepcion_SUNAT", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
            cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
            cmd.Parameters.AddWithValue("@FechaInicio", FechaInicio)
            cmd.Parameters.AddWithValue("@FechaFin", FechaFin)
            cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
            cmd.Parameters.AddWithValue("@IdMonedaDestino", IdMonedaDestino)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_PercepionPDT")

        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return ds

    End Function

    Public Function ReportePercepcion_PDT(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdTipoDocumento As Integer, ByVal IdMonedaDestino As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date) As String

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cad As String = ""

        Try

            cn.Open()

            Dim cmd As New SqlCommand("_A_Contabilidad_Rpt_Percepcion_SUNAT", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
            cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
            cmd.Parameters.AddWithValue("@FechaInicio", FechaInicio)
            cmd.Parameters.AddWithValue("@FechaFin", FechaFin)
            cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)
            cmd.Parameters.AddWithValue("@IdMonedaDestino", IdMonedaDestino)


            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Dim firstRow As Boolean = True

            While (lector.Read)


                If Not firstRow Then
                    cad = cad + vbCrLf
                End If

                cad = cad + CStr(IIf(IsDBNull(lector("CodTipoDocumentoI")) = True, "", lector("CodTipoDocumentoI"))).Trim + "|" + _
                        CStr(IIf(IsDBNull(lector("NroDocumentoI")) = True, "", lector("NroDocumentoI"))).Trim + "|" + _
                        CStr(IIf(IsDBNull(lector("RazonSocial")) = True, "", lector("RazonSocial"))).Trim + "|" + _
                        CStr(IIf(IsDBNull(lector("ApPaterno")) = True, "", lector("ApPaterno"))).Trim + "|" + _
                        CStr(IIf(IsDBNull(lector("ApMaterno")) = True, "", lector("ApMaterno"))).Trim + "|" + _
                        CStr(IIf(IsDBNull(lector("Nombre")) = True, "", lector("Nombre"))).Trim + "|" + _
                        CStr(IIf(IsDBNull(lector("SerieCompPercepcion")) = True, "", lector("SerieCompPercepcion"))).Trim + "|" + _
                        CStr(IIf(IsDBNull(lector("NroCompPercepcion")) = True, "", lector("NroCompPercepcion"))).Trim + "|" + _
                        CStr(IIf(IsDBNull(lector("FechaEmisionCompPercepcion")) = True, "", lector("FechaEmisionCompPercepcion"))).Trim + "|" + _
                        CStr(IIf(IsDBNull(lector("GeneraCreditoFiscal")) = True, "", lector("GeneraCreditoFiscal"))).Trim + "|" + _
                        CStr(IIf(IsDBNull(lector("VentaxTerremoto")) = True, "", lector("VentaxTerremoto"))).Trim + "|" + _
                        CStr(IIf(IsDBNull(lector("AgentePerceptor")) = True, "", lector("AgentePerceptor"))).Trim + "|" + _
                        CStr(IIf(IsDBNull(lector("MontoPagoComprobantePercepcion")) = True, "", lector("MontoPagoComprobantePercepcion"))).Trim + "|" + _
                        CStr(IIf(IsDBNull(lector("tdoc_CodigoSunat")) = True, "", lector("tdoc_CodigoSunat"))).Trim + "|" + _
                        CStr(IIf(IsDBNull(lector("SerieCompPago")) = True, "", lector("SerieCompPago"))).Trim + "|" + _
                        CStr(IIf(IsDBNull(lector("NroCompPago")) = True, "", lector("NroCompPago"))).Trim + "|" + _
                        CStr(IIf(IsDBNull(lector("FechaEmisionCompPago")) = True, "", lector("FechaEmisionCompPago"))).Trim + "|" + _
                        CStr(IIf(IsDBNull(lector("MontoTotalComprobantePago")) = True, "", lector("MontoTotalComprobantePago"))).Trim + "|"

                firstRow = False

            End While

            lector.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try


        Return cad




    End Function




End Class
