﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Public Class DAODocCancelacion
    Inherits DAO.DAODocumento

    Private DBUtil As New DAOMantenedor
    Private objConexion As New DAO.Conexion
    Private objDaoMantenedor As New DAO.DAOMantenedor

    '*************** DOCUMENTO CANCELACION 


    Public Sub DocumentoReciboEgreso_UpdateMontoxSustentar(ByVal IdDocumentoRG As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Try
            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = objDaoMantenedor.getParam(IdDocumentoRG, "@IdDocumento", SqlDbType.Int)

            SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "[_DocumentoReciboEgreso_UpdateMontoxSustentar]", p)
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub

    Public Sub UpdateMontoxSustentar_Liquidacion(ByVal IdDocumento As Integer, ByVal MontoxSustentar As Decimal, ByVal Liquidado As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Try
            Dim p() As SqlParameter = New SqlParameter(2) {}
            p(0) = objDaoMantenedor.getParam(IdDocumento, "@IdDocumento", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(MontoxSustentar, "@MontoxSustentar", SqlDbType.Decimal)
            p(2) = objDaoMantenedor.getParam(Liquidado, "@Liquidado", SqlDbType.Bit)

            SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoCancelacionCaja_UpdateMontoxSustentar_Liquidacion", p)

        Catch ex As Exception
            Throw ex
        Finally

        End Try


    End Sub



    Public Function Sustento_Liq_SelectxParams_DT(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdCaja As Integer, ByVal Opcion_Sustento As Integer, ByVal Opcion_Liq As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal IdPersona As Integer) As DataTable

        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Try

            Dim p() As SqlParameter = New SqlParameter(7) {}

            p(0) = objDaoMantenedor.getParam(Opcion_Sustento, "@Opcion_Sustento", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(IdCaja, "@IdCaja", SqlDbType.Int)
            p(4) = objDaoMantenedor.getParam(FechaInicio, "@FechaInicio", SqlDbType.Date)
            p(5) = objDaoMantenedor.getParam(FechaFin, "@FechaFin", SqlDbType.Date)
            p(6) = objDaoMantenedor.getParam(Opcion_Liq, "@Opcion_Liq", SqlDbType.Int)
            p(7) = objDaoMantenedor.getParam(IdPersona, "@IdPersona", SqlDbType.Int)

            Dim cmd As New SqlCommand("_DocumentoReciboEgreso_Sustento_Liq_SelectxParams", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_ReciboEgreso")

        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return ds.Tables("DT_ReciboEgreso")

    End Function

    Public Function insertarAsientoContable(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal objAsientoContable As Entidades.be_asientoContable, _
                                       ByVal idCadenaRequerimiento As String, ByVal idDocumentoGenerado As Integer) As String
        Dim nroVoucher As String = String.Empty
        Using cmd As New SqlCommand("SP_CREAR_ASIENTOCONTABLE_V4", cn, tr)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                .Parameters.Add(New SqlParameter("@idCadenaRequerimiento", SqlDbType.NVarChar)).Value = idCadenaRequerimiento
                .Parameters.Add(New SqlParameter("@IdEmpresa", SqlDbType.Int)).Value = objAsientoContable.idEmpresa
                .Parameters.Add(New SqlParameter("@fechaEmision", SqlDbType.VarChar)).Value = objAsientoContable.fechaEmision
                .Parameters.Add(New SqlParameter("@idUsuario", SqlDbType.NVarChar)).Value = objAsientoContable.usuarioCreacion
                .Parameters.Add(New SqlParameter("@idBanco", SqlDbType.Int)).Value = objAsientoContable.idBanco
                .Parameters.Add(New SqlParameter("@idMoneda", SqlDbType.Int)).Value = objAsientoContable.idMoneda
                .Parameters.Add(New SqlParameter("@ctaCancelacion", SqlDbType.NVarChar)).Value = objAsientoContable.ctaCancelacion
                .Parameters.Add(New SqlParameter("@glosa", SqlDbType.NVarChar)).Value = objAsientoContable.glosa
                .Parameters.Add(New SqlParameter("@idproveedor", SqlDbType.Int)).Value = objAsientoContable.idproveedor
                .Parameters.Add(New SqlParameter("@importepagado", SqlDbType.Decimal)).Value = objAsientoContable.importeAPagar
                .Parameters.Add(New SqlParameter("@idProgramacion", SqlDbType.Int)).Value = objAsientoContable.idProgramacion
                .Parameters.Add(New SqlParameter("@idDocumentoCabecera", SqlDbType.Int)).Value = idDocumentoGenerado
            End With
            Try
                'cmd.ExecuteNonQuery()
                Dim rdr As SqlDataReader = cmd.ExecuteReader()
                If rdr IsNot Nothing Then
                    Dim ordinal As Integer = rdr.GetOrdinal("doc_NroVoucherConta")
                    While rdr.Read()
                        nroVoucher = rdr.GetString(ordinal)
                    End While
                    rdr.Close()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Using
        Return nroVoucher
    End Function

    '********* Inserta el Documento CANCELACION  y retorna el IdDocumento
    Public Function Insert(ByVal objDocumento As Entidades.Documento, _
    ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), _
    ByVal listaCancelacion As List(Of Entidades.DatosCancelacion), _
    ByVal objObservaciones As Entidades.Observacion, _
    ByVal listaMovBanco As List(Of Entidades.MovBancoView), _
    ByVal objMovCaja As Entidades.MovCaja, _
    ByVal listaPagoCaja As List(Of Entidades.PagoCaja), ByVal objAnexoDocumento As Entidades.Anexo_Documento,
    ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento),
    ByVal objAsientoContable As Entidades.be_asientoContable, ByVal idCadenaRequerimiento As String) As Integer

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            'qwe
            '************* Insertamos la cabecera
            objDocumento.Id = MyBase.InsertaDocumento(cn, objDocumento, tr)

            '************* Insertamos el DETALLE CONCEPTO
            InsertListDetalleConceptoxIdDocumento_Cancelacion(listaDetalleConcepto, objDocumento.Id, cn, tr)


            UpdateDocumentoSustento(listaDetalleConcepto, objDocumento.Id, cn, tr)


            '*********** Insertamos la cancelación - Mov Caja - Pago Caja
            If (objMovCaja IsNot Nothing) Then

                '*********** Inserto Mov Caja
                InsertMovCajaxIdDocumento(objMovCaja, objDocumento.Id, cn, tr)

                '*********** Inserto Lista Cancelacion
                If (listaPagoCaja IsNot Nothing) Then
                    InsertListCancelacion_PagoCaja(listaPagoCaja, objDocumento.Id, cn, tr)
                End If
            End If

            InsertListCancelacion_DatosCancelacion(listaCancelacion, objDocumento.Id, cn, tr)


            'Dim Dao As DAODatosCancelacion = New DAODatosCancelacion()
            'For i As Integer = 0 To listaCancelacion.Count - 1
            '    listaCancelacion(i).IdDocumento = objDocumento.Id
            'Next
            'Dim DAOInsertaDatosCancelacion As Boolean = Dao.InsertaDatosCancelacion(listaCancelacion)


            If listaMovBanco IsNot Nothing Then

                Dim objDaoAnexoMovBanco As New DAO.DAOAnexo_MovBanco
                'Dim objDaoDocumentoFacturacion As New DAO.DAODocumentoFacturacion
                Dim objDaoMovBanco As New DAO.DAOMovBancoView

                For i As Integer = 0 To listaMovBanco.Count - 1
                    listaMovBanco(i).IdMovBanco = objDaoMovBanco.InsertT_GetID(listaMovBanco(i), cn, tr) '******** INSERTAMOS MOV BANCO
                    'listaMovBanco(i).IdMovBanco = objDaoDocumentoFacturacion.MovBancoInsert_Venta(listaMovBanco(i), cn, tr) '******** INSERTAMOS MOV BANCO

                    If (listaMovBanco(i).IdMovBanco <> Nothing) Then
                        objDaoAnexoMovBanco.Anexo_MovBancoInsert(New Entidades.Anexo_MovBanco(listaMovBanco(i).IdMovBanco, listaMovBanco(i).IdCuentaBancaria, Nothing, objDocumento.Id, listaMovBanco(i).IdBanco), cn, tr)
                    End If

                Next
            End If

            '******************* INSERTAMOS ANEXO DOCUMENTO
            If (objAnexoDocumento IsNot Nothing) Then
                Dim objDaoAnexoDocumento As New DAO.DAOAnexo_Documento
                objAnexoDocumento.IdDocumento = objDocumento.Id
                objDaoAnexoDocumento.InsertAnexo_Documento(objAnexoDocumento, cn, tr, False)
            End If

            '**************** INSERTAMOS RELACION DOCUMENTO
            If (listaRelacionDocumento IsNot Nothing) Then
                Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
                For i As Integer = 0 To listaRelacionDocumento.Count - 1
                    listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                    objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)
                Next
            End If

            If (objObservaciones IsNot Nothing) Then
                InsertObservacion(objObservaciones, objDocumento.Id, cn, tr)
            End If
            If objAsientoContable IsNot Nothing Then
                objDocumento.nroVoucher = insertarAsientoContable(cn, tr, objAsientoContable, idCadenaRequerimiento, objDocumento.Id)
            End If
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function

    '********* Actualiza el Documento CANCELACION y retorna el IdDocumento
    Public Function Update(ByVal objDocumento As Entidades.Documento, _
    ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), _
    ByVal listaCancelacion As List(Of Entidades.DatosCancelacion), _
    ByVal objObservaciones As Entidades.Observacion, _
    ByVal listaMovBanco As List(Of Entidades.MovBancoView), _
    ByVal objMovCaja As Entidades.MovCaja, _
    ByVal listaPagoCaja As List(Of Entidades.PagoCaja), ByVal objAnexoDocumento As Entidades.Anexo_Documento, ByVal listaRelacionDocumento As List(Of Entidades.RelacionDocumento)) As Integer

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            '************ Deshacemos lo hecho (DetalleRecibo / MovCaja / PagoCaja) 
            'Me.DocumentoReciboEgresoDeshacerMov(objDocumento.Id, True, True, True, True, True, False, cn, tr)
            Dim objDC As New DAODatosCancelacion
            objDC.DeshacerMov(objDocumento.Id, True, True, True, True, True, True, True, True, True, True, False, cn, tr)

            '************* ACTUALIZAMOS la cabecera
            MyBase.DocumentoUpdate(objDocumento, cn, tr)

            '************* Insertamos el DETALLE CONCEPTO
            InsertListDetalleConceptoxIdDocumento_Cancelacion(listaDetalleConcepto, objDocumento.Id, cn, tr)

            UpdateDocumentoSustento(listaDetalleConcepto, objDocumento.Id, cn, tr)
            '*********** Insertamos la cancelación - Mov Caja - Pago Caja
            If (objMovCaja IsNot Nothing) Then

                '*********** Inserto Mov Caja               
                InsertMovCajaxIdDocumento(objMovCaja, objDocumento.Id, cn, tr)
                '*********** Inserto Lista Cancelacion
                If (listaPagoCaja IsNot Nothing) Then
                    InsertListCancelacion_PagoCaja(listaPagoCaja, objDocumento.Id, cn, tr)
                End If

            End If

            InsertListCancelacion_DatosCancelacion(listaCancelacion, objDocumento.Id, cn, tr)

            'Dim Dao As DAODatosCancelacion = New DAODatosCancelacion()

            'For i As Integer = 0 To listaCancelacion.Count - 1
            '    listaCancelacion(i).IdDocumento = objDocumento.Id
            'Next
            'Dim DAOInsertaDatosCancelacion As Boolean = Dao.InsertaDatosCancelacion(listaCancelacion)
            '



            If listaMovBanco IsNot Nothing Then

                Dim objDaoAnexoMovBanco As New DAO.DAOAnexo_MovBanco
                'Dim objDaoDocumentoFacturacion As New DAO.DAODocumentoFacturacion
                Dim objDaoMovBanco As New DAO.DAOMovBancoView

                For i As Integer = 0 To listaMovBanco.Count - 1
                    listaMovBanco(i).IdMovBanco = objDaoMovBanco.InsertT_GetID(listaMovBanco(i), cn, tr) '******** INSERTAMOS MOV BANCO
                    'listaMovBanco(i).IdMovBanco = objDaoDocumentoFacturacion.MovBancoInsert_Venta(listaMovBanco(i), cn, tr) '******** INSERTAMOS MOV BANCO

                    If (listaMovBanco(i).IdMovBanco <> Nothing) Then
                        objDaoAnexoMovBanco.Anexo_MovBancoInsert(New Entidades.Anexo_MovBanco(listaMovBanco(i).IdMovBanco, listaMovBanco(i).IdCuentaBancaria, Nothing, objDocumento.Id, listaMovBanco(i).IdBanco), cn, tr)
                    End If
                Next
            End If
            'End If

            '******************* INSERTAMOS ANEXO DOCUMENTO
            If (objAnexoDocumento IsNot Nothing) Then
                Dim objDaoAnexoDocumento As New DAO.DAOAnexo_Documento
                objAnexoDocumento.IdDocumento = objDocumento.Id
                objDaoAnexoDocumento.InsertAnexo_Documento(objAnexoDocumento, cn, tr, False)
            End If

            If (listaRelacionDocumento IsNot Nothing) Then

                Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento

                For i As Integer = 0 To listaRelacionDocumento.Count - 1

                    listaRelacionDocumento(i).IdDocumento2 = objDocumento.Id
                    objDaoRelacionDocumento.InsertaRelacionDocumento(listaRelacionDocumento(i), cn, tr)

                Next

            End If

            If (objObservaciones IsNot Nothing) Then
                InsertObservacion(objObservaciones, objDocumento.Id, cn, tr)
            End If

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function

    'ini Detalle Concepto
    Public Sub InsertListDetalleConceptoxIdDocumento_Cancelacion(ByVal list As List(Of Entidades.DetalleConcepto), ByVal IdDocumento As Integer, ByVal cnx As SqlConnection, ByVal trx As SqlTransaction)
        Try
            For i As Integer = 0 To list.Count - 1
                list(i).IdDocumento = IdDocumento
                Me.InsertDetalleConcepto_Cancelacion_v2(list(i), cnx, trx)
            Next
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub

    'ini Detalle Concepto
    Public Sub InsertListDetalleConceptoxIdDocumento_Cancelacion_v2(ByVal list As List(Of Entidades.DetalleConcepto), ByVal IdDocumento As Integer, ByVal cnx As SqlConnection, ByVal trx As SqlTransaction)
        Try
            For i As Integer = 0 To list.Count - 1
                list(i).IdDocumento = IdDocumento
                Me.InsertDetalleConcepto_Cancelacion_v2(list(i), cnx, trx)
            Next
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub


    Public Sub UpdateDocumentoSustento(ByVal list As List(Of Entidades.DetalleConcepto), ByVal IdDocumento As Integer, ByVal cnx As SqlConnection, ByVal trx As SqlTransaction)
        Try
            For i As Integer = 0 To list.Count - 1
                list(i).IdDocumento = IdDocumento
                Me.UpdateDocumentoSustentox(list(i), cnx, trx)
            Next
        Catch ex As Exception
            Throw ex
        Finally            
        End Try
    End Sub

    Public Sub UpdateDocumentoSustentoMovCuenta(ByVal list As List(Of Entidades.MovCuentaPorPagar), ByVal IdDocumento As Integer, ByVal cnx As SqlConnection, ByVal trx As SqlTransaction)
        Try
            For i As Integer = 0 To list.Count - 1
                Me.UpdateDocumentoSustentoMovCuenta(list(i), cnx, trx)
            Next
        Catch ex As Exception
            Throw ex
        Finally            
        End Try
    End Sub
    Public Sub UpdateDocumentoSustentox(ByVal obj As Entidades.DetalleConcepto, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(1) {}
        p(0) = objDaoMantenedor.getParam(obj.IdDocumentoRef, "@IdDocumento", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(obj.ResultSustento, "@flagSustento", SqlDbType.Int)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoCancelacionCaja_UpdateSustentoDocumento", p)

    End Sub
    Public Sub UpdateDocumentoSustentoMovCuenta(ByVal obj As Entidades.MovCuentaPorPagar, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(1) {}
        p(0) = objDaoMantenedor.getParam(obj.IdDocumento, "@IdDocumento", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(obj.FlagSustento, "@flagSustento", SqlDbType.Int)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoCancelacionCaja_UpdateSustentoDocumento", p)

    End Sub
    Public Sub UpdateSustentoxIdDocumento(ByVal obj As Entidades.Documento)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try
            cn.Open()
            tr = cn.BeginTransaction
            Dim p() As SqlParameter = New SqlParameter(1) {}
            p(0) = objDaoMantenedor.getParam(obj.IdDocumento, "@IdDocumento", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(obj.FactorMov, "@flagSustento", SqlDbType.Int)
            SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoCancelacionCaja_UpdateSustentoDocumento", p)
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
        Finally

        End Try
        cn.Close()
    End Sub

    Public Sub InsertDetalleConcepto_Cancelacion_v2(ByVal obj As Entidades.DetalleConcepto, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim param() As SqlParameter = New SqlParameter(6) {}
        With DBUtil
            param(0) = .getParam(obj.IdDocumento, "@IdDocumento", SqlDbType.Int)
            param(1) = .getParam(obj.Concepto, "@dr_Concepto", SqlDbType.VarChar)
            param(2) = .getParam(obj.IdMoneda, "@IdMonedaDetalle", SqlDbType.Int)
            param(3) = .getParam(obj.Monto, "@dr_Monto", SqlDbType.Decimal)
            param(4) = .getParam(obj.IdConcepto, "@IdConcepto", SqlDbType.Int)
            param(5) = .getParam(obj.IdDocumentoRef, "@IdDocumentoAfecto", SqlDbType.Int)
            param(6) = .getParam(obj.IdMovCuentaRef, "@IdMovCtaPPAfecto", SqlDbType.Int)
        End With

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoCancelacion_InsertDetalleConcepto_v2", param)

    End Sub
    Public Sub InsertDetalleConcepto_Cancelacion(ByVal obj As Entidades.DetalleConcepto, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim param() As SqlParameter = New SqlParameter(6) {}
        With DBUtil
            param(0) = .getParam(obj.IdDocumento, "@IdDocumento", SqlDbType.Int)
            param(1) = .getParam(obj.Concepto, "@dr_Concepto", SqlDbType.VarChar)
            param(2) = .getParam(obj.IdMoneda, "@IdMonedaDetalle", SqlDbType.Int)
            param(3) = .getParam(obj.Monto, "@dr_Monto", SqlDbType.Decimal)
            param(4) = .getParam(obj.IdConcepto, "@IdConcepto", SqlDbType.Int)
            param(5) = .getParam(obj.IdDocumentoRef, "@IdDocumentoAfecto", SqlDbType.Int)
            param(6) = .getParam(obj.IdMovCuentaRef, "@IdMovCtaPPAfecto", SqlDbType.Int)
        End With

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoCancelacion_InsertDetalleConcepto", param)

    End Sub
    'fin

End Class
