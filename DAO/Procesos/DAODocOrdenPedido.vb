﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Imports System.Data.SqlClient
Public Class DAODocOrdenPedido
    Dim objConexion As New Conexion

#Region "Mantenimeinto"

    Public Function SelectOrdenPedido(ByVal codigo As Integer, ByVal idserie As Integer) As Entidades.Documento
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SelectOrdenPedido", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@codigo", codigo)
        cmd.Parameters.AddWithValue("@serie", idserie)
        Dim lector As SqlDataReader = Nothing
        Dim obj As New Entidades.Documento
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If lector.HasRows = False Then
                Throw New Exception("No existe documento con Ese código")
            End If
            If lector.Read Then
                With obj
                    .Id = CInt(lector("IdDocumento"))
                    .Codigo = CStr(lector("doc_Codigo"))
                    .Serie = CStr(lector("doc_Serie"))
                    .FechaEmision = CDate(lector("doc_FechaEmision"))
                    .IdSerie = CInt(lector("IdSerie"))
                    .IdEmpresa = CInt(lector("IdEmpresa"))
                    .IdTienda = CInt(lector("IdTienda"))
                    .IdAlmacen = CInt(lector("IdAlmacen"))
                    .IdTipoOperacion = CInt(lector("IdTipoOperacion"))
                    .IdEstadoDoc = CInt(lector("IdEstadoDoc"))
                    .IdDocRelacionado = CInt(IIf(IsDBNull(lector("IdDocumento1")), 0, lector("IdDocumento1")))
                    .NomTipoDocumento = CStr(IIf(IsDBNull(lector("doc_Codigoref")), "", lector("doc_Codigoref"))) 'Codigo ref 
                    .NomTipoOperacion = CStr(IIf(IsDBNull(lector("doc_Serieref")), "", lector("doc_Serieref"))) 'Serie ref 
                    .IdEstadoEntrega = CInt(IIf(IsDBNull(lector("IdEstadoEnt")), 0, lector("IdEstadoEnt"))) ' estado de entrega del documento relacionado
                    .IdEstadoCancelacion = CInt(IIf(IsDBNull(lector("EstadoEntrega")), 0, lector("EstadoEntrega"))) 'estado de entrega del documento orden de pedido X se considero al ultimo momento
                    .NomEmpleado = CStr(IIf(IsDBNull(lector("tipodocumento")), "", lector("tipodocumento"))) 'TipoDocumento 
                    .NomEmpresaTomaInv = CStr(IIf(IsDBNull(lector("persona")), "", lector("persona"))) 'persona
                End With
            End If
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return obj
    End Function

    Public Function findDocumentoRef(ByVal serie As String, ByVal codigo As String, _
                                     ByVal pageindex As Integer, ByVal pagesize As Integer, _
                                     ByVal IdTienda As Integer, ByVal IdEmpresa As Integer, _
                                     ByVal idalmacen As Integer) As List(Of Entidades.DocumentoView)
        Dim parametros() As SqlParameter = New SqlParameter(6) {}
        parametros(0) = New SqlParameter("@codigo", SqlDbType.VarChar)
        parametros(0).Value = codigo
        parametros(1) = New SqlParameter("@serie", SqlDbType.VarChar)
        parametros(1).Value = serie
        parametros(2) = New SqlParameter("@pageindex", SqlDbType.Int)
        parametros(2).Value = pageindex
        parametros(3) = New SqlParameter("@pageSize", SqlDbType.Int)
        parametros(3).Value = pagesize
        parametros(4) = New SqlParameter("@idtienda", SqlDbType.Int)
        parametros(4).Value = IdTienda
        parametros(5) = New SqlParameter("@idempresa", SqlDbType.Int)
        parametros(5).Value = IdEmpresa
        parametros(6) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        parametros(6).Value = idalmacen

        Dim lector As SqlDataReader = Nothing
        Dim Lista As New List(Of Entidades.DocumentoView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_selectDocRef", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametros)

        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.DocumentoView
                With obj
                    .IdDocumento = CInt(lector("IdDocumento"))
                    .Codigo = CStr(lector("doc_Codigo"))
                    .Serie = CStr(lector("doc_Serie"))
                    .FechaEmision = CStr(IIf(IsDBNull(lector("doc_FechaEmision")), Nothing, lector("doc_FechaEmision")))
                    .RazonSocial = CStr(IIf(IsDBNull(lector("Nombre")), "---", lector("Nombre")))
                    .RUC = CStr(IIf(IsDBNull(lector("RUC")), "---", lector("RUC")))
                    .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")), "---", lector("tdoc_NombreCorto")))
                End With
                Lista.Add(obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function


    Public Function getDataSetOrdenPedido(ByVal idDocumento As Integer) As DataSet
        Dim daPedido As SqlDataAdapter
        Dim daDetPedido As SqlDataAdapter
        Dim daProgPedido As SqlDataAdapter
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_SelectDocPedido", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", idDocumento)

            daPedido = New SqlDataAdapter(cmd)
            daPedido.Fill(ds, "DT_OrdenPedido")

            cmd.CommandText = "_CR_DetOrdenPedido"
            daDetPedido = New SqlDataAdapter(cmd)
            daDetPedido.Fill(ds, "DT_DetOrdenPedido")

            cmd.CommandText = "_ListarDocProgPedidoxIdDocumento"
            daProgPedido = New SqlDataAdapter(cmd)
            daProgPedido.Fill(ds, "DT_ProgPedido")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try

        Return ds
    End Function

    Public Function insertBDOrdenPedido(ByVal obj As Entidades.Documento, ByVal cn As SqlConnection, _
                            ByVal tr As SqlTransaction) As String
        Dim parametro() As SqlParameter = New SqlParameter(8) {}

        parametro(0) = New SqlParameter("@doc_FechaEmision", SqlDbType.DateTime)
        parametro(0).Value = IIf(obj.FechaEmision = Nothing, DBNull.Value, obj.FechaEmision)

        parametro(1) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        parametro(1).Value = IIf(obj.IdUsuario = Nothing, DBNull.Value, obj.IdUsuario)

        parametro(2) = New SqlParameter("@IdEstadoDoc", SqlDbType.Int)
        parametro(2).Value = IIf(obj.IdEstadoDoc = Nothing, DBNull.Value, obj.IdEstadoDoc)

        parametro(3) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        parametro(3).Value = IIf(obj.IdTipoOperacion = Nothing, DBNull.Value, obj.IdTipoOperacion)

        parametro(4) = New SqlParameter("@IdTienda", SqlDbType.Int)
        parametro(4).Value = IIf(obj.IdTienda = Nothing, DBNull.Value, obj.IdTienda)

        parametro(5) = New SqlParameter("@IdSerie", SqlDbType.Int)
        parametro(5).Value = IIf(obj.IdSerie = Nothing, DBNull.Value, obj.IdSerie)

        parametro(6) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        parametro(6).Value = IIf(obj.IdEmpresa = Nothing, DBNull.Value, obj.IdEmpresa)

        parametro(7) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        parametro(7).Value = IIf(obj.IdTipoDocumento = Nothing, DBNull.Value, obj.IdTipoDocumento)

        parametro(8) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        parametro(8).Value = IIf(obj.IdAlmacen = Nothing, False, obj.IdAlmacen)

        parametro(9) = New SqlParameter("@IdEstadoEnt", SqlDbType.Int)
        parametro(9).Value = IIf(obj.IdEstadoEntrega = Nothing, DBNull.Value, obj.IdEstadoEntrega)

        Dim cmd As New SqlCommand("_InsertOrdenPedido", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametro)
        Try
            Return cmd.ExecuteScalar().ToString
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function

    Public Function InsertOrdenPedido(ByVal ObjDocumento As Entidades.Documento, _
                                      ByVal listaDetalle As List(Of Entidades.DetalleDocumento), _
                                      ByVal obs As Entidades.Observacion, _
                                      ByVal comprometerstock As Boolean, ByVal objProgPedido As Entidades.ProgramacionPedido) As String
        Dim cad As String
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        cn.Open()
        Dim tr As SqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)
        Try
            Dim ObjDaoDetalleDocumento As New DAO.DAODetalleDocumento

            cad = Me.insertBDOrdenPedido(ObjDocumento, cn, tr)
            Dim idxcodigo() As String = cad.Split(CChar(","))

            Dim objPuntoPartida As New Entidades.PuntoPartida
            Dim objDaoPuntoPartida As New DAO.DAOPuntoPartida
            Dim objPuntoLLegada As New Entidades.PuntoLlegada
            Dim objDaoPuntoLLegada As New DAO.DAOPuntoLlegada

            With objPuntoPartida
                .IdDocumento = CInt(idxcodigo(0))
                .IdTienda = ObjDocumento.IdTiendaSucursal
                .IdAlmacen = ObjDocumento.LugarEntrega
            End With
            With objPuntoLLegada
                .IdDocumento = CInt(idxcodigo(0))
                .IdTienda = ObjDocumento.IdTienda
                .IdAlmacen = ObjDocumento.IdAlmacen
            End With

            objDaoPuntoPartida.InsertaPuntoPartida(objPuntoPartida, cn, tr)
            objDaoPuntoLLegada.InsertaPuntoLlegadaT(cn, objPuntoLLegada, tr)


            Select Case ObjDocumento.IdTipoOperacion
                Case 18 'PEDIDO ENTRE SUCURSALES

                    Dim objDocRef As New DAO.DAORelacionDocumento
                    Dim objEntRelacionDoc As New Entidades.RelacionDocumento
                    With objEntRelacionDoc
                        .IdDocumento1 = ObjDocumento.IdDocRelacionado
                        .IdDocumento2 = CInt(idxcodigo(0))
                    End With
                    objDocRef.InsertaRelacionDocumento(objEntRelacionDoc, cn, tr)

                    ObjDaoDetalleDocumento.InsertaDetalleDocumento_MovAlmacen(cn, listaDetalle, tr, CInt(idxcodigo(0)) _
                        , True, comprometerstock, 1, ObjDocumento.LugarEntrega)


                Case 19 'PEDIDO AL CENTRO DISTRIBUIDOR

                    For i As Integer = 0 To listaDetalle.Count - 1
                        With listaDetalle.Item(i)
                            .IdDocumento = CInt(idxcodigo(0))
                            .IdDetalleDocumento = ObjDaoDetalleDocumento.InsertaDetalleDocumento(cn, listaDetalle(i), tr)
                        End With
                    Next
                    Dim objDAOProg As New DAO.DAOProgramacionPedido
                    objProgPedido.IdDocumento = CInt(idxcodigo(0))
                    objDAOProg.InsertDocumentoProgramacionPedido(objProgPedido, cn, tr)

            End Select

            Dim objObservacion As New DAO.DAOObservacion
            obs.IdDocumento = CInt(idxcodigo(0))
            objObservacion.InsertaObservacionT(cn, tr, obs)

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return cad
    End Function

    Public Function UpdateOrdenPedido(ByVal objDocumento As Entidades.Documento, ByVal listaDetalle As List(Of Entidades.DetalleDocumento), _
                                     ByVal objObs As Entidades.Observacion, _
                                     ByVal comprometerStock As Boolean, ByVal objProgPedido As Entidades.ProgramacionPedido) As Boolean
        Dim ObjDaoDocumento As New DAO.DAODocumento
        Dim ObjDaoDetalleDocumento As New DAO.DAODetalleDocumento
        Dim ObjPuntoPartida As New DAO.DAOPuntoPartida
        Dim iddoc As Integer = -1
        Dim ObjDocRef As New DAO.DAORelacionDocumento
        Dim objObservaciones As New DAO.DAOObservacion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        cn.Open()
        Dim tr As SqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)
        Try
            iddoc = CInt(objDocumento.Id)
            '**********************ACTUALIZO LA CABECERA DEL DOCUMENTO
            ObjDaoDocumento.DocumentoUpdate(objDocumento, cn, tr)

            '***********************ACTUALIZO EL PUNTO DE PARTIDA
            Dim objEntPuntoPartida As New Entidades.PuntoPartida
            With objEntPuntoPartida
                .IdTienda = objDocumento.IdTiendaSucursal
                .IdAlmacen = objDocumento.LugarEntrega
                .IdDocumento = iddoc
            End With
            ObjPuntoPartida.UpdatePuntoPartida(objEntPuntoPartida, cn, tr)


            Select Case objDocumento.IdTipoOperacion
                Case 18 'Entre sucursales
                    '**********************ACTUALIZO LA RELACION DOCUMENTO
                    Dim objEntDocRef As New Entidades.RelacionDocumento
                    objEntDocRef.IdDocumento1 = objDocumento.IdDocRelacionado
                    objEntDocRef.IdDocumento2 = iddoc
                    ObjDocRef.UpdateRelacionDocumento(objEntDocRef, cn, tr)

                    '**********************BORRO LOS DETALLE DEL DOCUMENTO Y LOS MOV. ALMACEN
                    ObjDaoDocumento.MovAlmacen_ExtornoxIdDocumento(iddoc, cn, tr)
                    '**********************INSERTO LOS NUEVOS DETALLE DEL DOCUMENTO Y LOS MOV. ALMACEN
                    ObjDaoDetalleDocumento.InsertaDetalleDocumento_MovAlmacen(cn, listaDetalle, tr, iddoc _
                     , True, comprometerStock, 1, objDocumento.LugarEntrega)

                Case 19 'Centro distribuidor
                    '**********************BORRRO LOS DETALLE DEL DOCUMENTO
                    Me.DeleteDetallexID(iddoc, cn, tr)
                    '**********************INSERTO LOS NUEVOS DETALLES
                    For i As Integer = 0 To listaDetalle.Count - 1
                        With listaDetalle.Item(i)
                            .IdDocumento = iddoc
                            .IdDetalleDocumento = ObjDaoDetalleDocumento.InsertaDetalleDocumento(cn, listaDetalle(i), tr)
                        End With
                    Next
                    '**********************ACTUALIZO LA PROGRAMACION DE SEMANA
                    'Dim objDAOProg As New DAO.DAOProgramacionPedido
                    'objProgPedido.IdDocumento = iddoc
                    'objDAOProg.UpdateDocumentoProgramacionPedido(objProgPedido, cn, tr)

            End Select

            '**************************ACTUALIZO LAS OBSERVACIONES
            objObs.IdDocumento = iddoc
            objObservaciones.ActualizaObservacion(objObs)

            tr.Commit()
            Return True

        Catch ex As Exception
            tr.Rollback()
            iddoc = -1
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Sub DeleteDetallexID(ByVal iddocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Dim cmd As New SqlCommand("_deleteDetallexID", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@iddocumento", iddocumento)
        Try
            Using cmd
                If cmd.ExecuteNonQuery = 0 Then
                    Throw New Exception("Fracaso la Operaccion")
                End If
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub


#End Region

#Region "Otros"
    Public Function ListarProductosOrdenPedido(ByVal idalmacen As Integer, ByVal descripcion As String, _
        ByVal linea As Integer, ByVal sublinea As Integer, ByVal codsubLinea As Integer, _
        ByVal pageindex As Integer, ByVal pagesize As Integer, ByVal almacen1 As Integer) As List(Of Entidades.Catalogo)

        Dim parametros() As SqlParameter = New SqlParameter(7) {}
        parametros(0) = New SqlParameter("@idalmacen", SqlDbType.Int)
        parametros(0).Value = idalmacen
        parametros(1) = New SqlParameter("@descripcion", SqlDbType.VarChar)
        parametros(1).Value = descripcion
        parametros(2) = New SqlParameter("@linea", SqlDbType.Int)
        parametros(2).Value = linea
        parametros(3) = New SqlParameter("@sublinea", SqlDbType.Int)
        parametros(3).Value = sublinea
        parametros(4) = New SqlParameter("@codsublinea", SqlDbType.Int)
        parametros(4).Value = codsubLinea
        parametros(5) = New SqlParameter("@pageindex", SqlDbType.Int)
        parametros(5).Value = pageindex
        parametros(6) = New SqlParameter("@pageSize", SqlDbType.Int)
        parametros(6).Value = pagesize
        parametros(7) = New SqlParameter("@idalmacen1", SqlDbType.Int)
        parametros(7).Value = almacen1

        Dim Lista As New List(Of Entidades.Catalogo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim lector As SqlDataReader
        Try
            Dim cmd As New SqlCommand("_ListarProductosOrdenPedido", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(parametros)

            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.Catalogo
                With obj
                    .IdProducto = CInt(lector("IdProducto"))
                    .Descripcion = CStr(IIf(IsDBNull(lector("prod_Nombre")), "---", lector("prod_Nombre")))
                    .NomLinea = CStr(IIf(IsDBNull(lector("lin_Nombre")), "---", lector("lin_Nombre")))
                    .NomSubLinea = CStr(IIf(IsDBNull(lector("sl_Nombre")), "---", lector("sl_Nombre")))
                    .cadenaUM = CStr(IIf(IsDBNull(lector("cadenaUM")), "---", lector("cadenaUM")))
                    .StockDisponibleN = CDec(IIf(IsDBNull(lector("stock")), 0, lector("stock")))
                    .StockAReal = CDec(IIf(IsDBNull(lector("stock1")), 0, lector("stock1")))
                End With
                Lista.Add(obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

        Return Lista

    End Function

    Public Function OrdenPedidoxSelectDetalleEntresucursales(ByVal IdDocumento As Integer, _
                                                             ByVal idAlmacen As Integer) As List(Of Entidades.DetalleDocumento)

        Dim Lista As New List(Of Entidades.DetalleDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_OrdenPedidoxSelectDetalleEntresucursales", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@IdAlmacen", idAlmacen)

        Dim lector As SqlDataReader
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.DetalleDocumento
                With obj
                    .IdProducto = CInt(lector("IdProducto"))
                    .StockDisponibleN = CDec(IIf(IsDBNull(lector("StockDisponible")), 0, lector("StockDisponible")))
                    .Stock = CDec(IIf(IsDBNull(lector("stock")), 0, lector("stock")))
                    .NomProducto = CStr(lector("prod_Nombre"))
                    ' .cadenaUM = CStr(IIf(IsDBNull(lector("cadenaUM")), 0, lector("cadenaUM")))
                    .UMedida = CStr(IIf(IsDBNull(lector("dc_UMedida")), 0, lector("dc_UMedida")))
                    .IdUnidadMedida = CInt(IIf(IsDBNull(lector("idunidadmedida")), 0, lector("idunidadmedida")))
                    .IdUnidadMedidaPrincipal = CInt(IIf(IsDBNull(lector("IdUnidadMedidaPrincipal")), 0, lector("IdUnidadMedidaPrincipal")))

                    .CantxAtender = CDec(IIf(IsDBNull(lector("pendiente")), 0, lector("pendiente")))
                    .cantSolicitada = CDec(IIf(IsDBNull(lector("dc_Cantidad")), 0, lector("dc_Cantidad")))
                End With
                Lista.Add(obj)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function

    Public Function OrdenPedidoxSelectxIdDocumento(ByVal idDocumento As Integer, ByVal IdAlmacen As Integer, _
                                                   ByVal IdAlmacen2 As Integer) As List(Of Entidades.DetalleDocumento)
        Dim Lista As New List(Of Entidades.DetalleDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_OrdenPedidoxSelectxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", idDocumento)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdAlmacen2", IdAlmacen2)

        Dim lector As SqlDataReader
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.DetalleDocumento
                With obj
                    .IdProducto = CInt(lector("IdProducto"))
                    .StockDisponibleN = CDec(IIf(IsDBNull(lector("StockDisponible")), 0, lector("StockDisponible")))
                    .NomProducto = CStr(lector("prod_Nombre"))
                    ' .cadenaUM = CStr(IIf(IsDBNull(lector("cadenaUM")), 0, lector("cadenaUM")))
                    .UMedida = CStr(IIf(IsDBNull(lector("dc_UMedida")), 0, lector("dc_UMedida")))
                    .IdUnidadMedida = CInt(IIf(IsDBNull(lector("idunidadmedida")), 0, lector("idunidadmedida")))
                    .IdUnidadMedidaPrincipal = CInt(IIf(IsDBNull(lector("IdUnidadMedidaPrincipal")), 0, lector("IdUnidadMedidaPrincipal")))
                    .CantxAtender = CDec(IIf(IsDBNull(lector("dc_CantxAtender")), 0, lector("dc_CantxAtender")))
                    .Cantidad = CDec(IIf(IsDBNull(lector("dc_Cantidad")), 0, lector("dc_Cantidad")))
                    .Stock = CDec(IIf(IsDBNull(lector("stock")), 0, lector("stock")))
                    .cantSolicitada = CDec(IIf(IsDBNull(lector("dc_CantxAtender")), 0, lector("dc_CantxAtender")))
                End With
                Lista.Add(obj)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function

    Public Function OrdenPedidoConsultarStock(ByVal idalmacen As Integer, ByVal idproducto As Integer) As Decimal
        Dim stock As Decimal
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_OrdenPedidoConsultarStockxIdProducto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idAlmacen", idalmacen)
        cmd.Parameters.AddWithValue("@idProducto", idproducto)
        Try
            cn.Open()
            stock = CDec(cmd.ExecuteScalar)
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return stock
    End Function

    Public Function OrdenPedidoConsultarRelacionDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.Documento)
        Dim Lista As New List(Of Entidades.Documento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_OrdenPedidoConsultarRelacionDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

        Dim lector As SqlDataReader
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.Documento
                With obj
                    .Id = CInt(lector("IdDocumento"))
                    .Codigo = CStr(lector("doc_Codigo"))
                    .Serie = CStr(lector("doc_Serie"))
                    .FechaEmision = CDate(lector("doc_FechaEmision"))
                    .NomAlmacen = CStr(lector("alm_Nombre"))
                End With
                Lista.Add(obj)
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function

#End Region

#Region "marzo del 2010"

    Public Function OrdenPedido_SemanaActual(ByVal fecha As Date) As Integer
        Dim idsemana As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_OrdenPedido_SemanaActual", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@fecha", fecha)
        Try
            cn.Open()
            idsemana = CInt(cmd.ExecuteScalar())
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return idsemana
    End Function

    Public Function OrdenPedido_ProgramacionPedido(ByVal fecha As Date, _
                                                   ByVal idEmpresa As Integer, ByVal idTienda As Integer, _
                                                   ByVal idAlmacen As Integer) As String

        Dim cad As String = String.Empty
        Dim parametros() As SqlParameter = New SqlParameter(3) {}
        parametros(0) = New SqlParameter("@fecha", SqlDbType.DateTime)
        parametros(0).Value = fecha
        parametros(1) = New SqlParameter("@idempresa", SqlDbType.Int)
        parametros(1).Value = idEmpresa
        parametros(2) = New SqlParameter("@idTienda", SqlDbType.Int)
        parametros(2).Value = idTienda
        parametros(3) = New SqlParameter("@idAlmacen", SqlDbType.Int)
        parametros(3).Value = idAlmacen

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_OrdenPedido_ProgramacionPedido", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametros)
        Try
            cn.Open()
            cad = cmd.ExecuteScalar.ToString
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return cad
    End Function

    Public Function OrdenPedido_Validar_ProgramacionPedido(ByVal fecha As Date, _
                                                   ByVal idEmpresa As Integer, ByVal idTienda As Integer, _
                                                   ByVal idAlmacen As Integer) As String
        Dim cad$
        Dim parametros() As SqlParameter = New SqlParameter(3) {}
        parametros(0) = New SqlParameter("@fecha", SqlDbType.DateTime)
        parametros(0).Value = fecha
        parametros(1) = New SqlParameter("@idempresa", SqlDbType.Int)
        parametros(1).Value = idEmpresa
        parametros(2) = New SqlParameter("@idTienda", SqlDbType.Int)
        parametros(2).Value = idTienda
        parametros(3) = New SqlParameter("@idAlmacen", SqlDbType.Int)
        parametros(3).Value = idAlmacen

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_OrdenPedido_Validar_ProgramacionPedido", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametros)
        Try
            cn.Open()
            cad = cmd.ExecuteScalar.ToString
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return cad
    End Function



#End Region

#Region "Reportes"
    Public Function DetalleOrdenPedido(ByVal idempresa As Integer, ByVal idalmacen As Integer, ByVal year As Integer, ByVal semana As Integer, ByVal fechainicio As String, ByVal fechafin As String, ByVal fsemana As Integer, ByVal tipodocs As Integer) As DataSet

        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Dim daD As SqlDataAdapter
        Dim ds As New DataSet
        Try

            Dim cmdD As New SqlCommand("[_CR_DetallePedidosInternos]", cn)
            cmdD.CommandType = CommandType.StoredProcedure
            cmdD.Parameters.AddWithValue("@idEmpresa", idempresa)
            cmdD.Parameters.AddWithValue("@idalmacen", idalmacen)
            cmdD.Parameters.AddWithValue("@tipoDocs", tipodocs)
            cmdD.Parameters.AddWithValue("@year", year)
            cmdD.Parameters.AddWithValue("@semana", semana)
            cmdD.Parameters.AddWithValue("@fechainicio", fechainicio)
            cmdD.Parameters.AddWithValue("@fechafin", fechafin)
            cmdD.Parameters.AddWithValue("@filtrarsemana ", fsemana)

            daD = New SqlDataAdapter(cmdD)
            daD.Fill(ds, "DT_PedidosInterno")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds

    End Function
#End Region

End Class
