﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




'*********************   MIERCOLES  24/03/2010 HORA 05_59 PM












Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOTomaInventario
    Private objConexion As New DAO.Conexion
    Private HDAO As New DAO.HelperDAO

    Private cn As SqlConnection
    Private cmd As SqlCommand
    Private tr As SqlTransaction
    Private reader As SqlDataReader
    '****************************** VERSIÓN II < TOMA DE INVENTARIO >
    Public Sub DocumentoTomaInventario_GenerarConsolidado_DetalleInsert(ByVal IdDocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As SqlCommand
        Dim p() As SqlParameter = New SqlParameter(0) {}

        p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        p(0).Value = IdDocumento

        cmd = New SqlCommand
        cmd.Connection = cn
        cmd.CommandText = "_DocumentoTomaInventario_GenerarConsolidado_DetalleInsert"
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Transaction = tr
        cmd.CommandTimeout = 0
        cmd.Parameters.AddRange(p)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try


    End Sub


    Public Function DocumentoTomaInventario_SelectDocRef(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdAlmacen As Integer, ByVal Serie As Integer, ByVal Codigo As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date) As List(Of Entidades.DocumentoView)

        Dim lista As New List(Of Entidades.DocumentoView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoTomaInventario_SelectDocRef", cn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@Serie", Serie)
        cmd.Parameters.AddWithValue("@Codigo", Codigo)
        cmd.Parameters.AddWithValue("@FechaFin", IIf(FechaFin = Nothing, DBNull.Value, FechaFin))
        cmd.Parameters.AddWithValue("@FechaInicio", IIf(FechaInicio = Nothing, DBNull.Value, FechaInicio))


        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                While (lector.Read)

                    Dim objDocumento As New Entidades.DocumentoView
                    With objDocumento

                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .FechaEntrega = CDate(IIf(IsDBNull(lector.Item("FechaEntrega")) = True, Nothing, lector.Item("FechaEntrega")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                        .Empresa = CStr(IIf(IsDBNull(lector.Item("Empresa")) = True, "", lector.Item("Empresa")))
                        .NomAlmacen = CStr(IIf(IsDBNull(lector.Item("Almacen")) = True, "", lector.Item("Almacen")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("Tienda")) = True, "", lector.Item("Tienda")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("TipoDocumento")) = True, "", lector.Item("TipoDocumento")))
                        .NroDocumento = .Serie + " - " + .Codigo

                    End With

                    lista.Add(objDocumento)

                End While

                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function DocumentoTomaInventario_CargarProductoDetallexParams(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdProducto As Integer, ByVal IdDocumento As Integer, ByVal FechaTomaInv As Date, ByVal PageIndex As Integer, ByVal PageSize As Integer, ByVal CodigoProducto As String) As List(Of Entidades.DetalleDocumento)

        Dim lista As New List(Of Entidades.DetalleDocumento)
        Dim cmd As SqlCommand
        Try

            Dim p() As SqlParameter = New SqlParameter(9) {}

            p(0) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
            p(0).Value = IdEmpresa

            p(1) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
            p(1).Value = IdAlmacen

            p(2) = New SqlParameter("@IdLinea", SqlDbType.Int)
            p(2).Value = IdLinea

            p(3) = New SqlParameter("@IdSubLinea", SqlDbType.Int)
            p(3).Value = IdSubLinea

            p(4) = New SqlParameter("@IdProducto", SqlDbType.Int)
            p(4).Value = IdProducto

            p(5) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            p(5).Value = IdDocumento

            p(6) = New SqlParameter("@FechaTomaInv", SqlDbType.Date)
            p(6).Value = FechaTomaInv

            p(7) = New SqlParameter("@PageIndex", SqlDbType.Int)
            p(7).Value = PageIndex

            p(8) = New SqlParameter("@PageSize", SqlDbType.Int)
            p(8).Value = PageSize

            p(9) = New SqlParameter("@CodigoProducto", SqlDbType.VarChar)
            p(9).Value = CodigoProducto

            cn = objConexion.ConexionSIGE

            cn.Open()

            cmd = New SqlCommand
            cmd.Connection = cn
            cmd.CommandText = "_DocumentoTomaInventario_CargarProductoDetallexParams"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            cmd.Parameters.AddRange(p)

            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            While (reader.Read)

                Dim obj As New Entidades.DetalleDocumento

                With obj

                    .IdProducto = CInt(IIf(IsDBNull(reader("IdProducto")) = True, 0, reader("IdProducto")))
                    .IdUnidadMedida = CInt(IIf(IsDBNull(reader("IdUnidadMedida")) = True, 0, reader("IdUnidadMedida")))
                    .NomProducto = CStr(IIf(IsDBNull(reader("Producto")) = True, "", reader("Producto")))
                    .UMedida = CStr(IIf(IsDBNull(reader("UnidadMedida")) = True, "", reader("UnidadMedida")))
                    .Cantidad = CDec(IIf(IsDBNull(reader("Cantidad")) = True, 0, reader("Cantidad")))
                    .CantxAtender = CDec(IIf(IsDBNull(reader("Cantidad_Sistema")) = True, 0, reader("Cantidad_Sistema")))
                    .CodigoProducto = CStr(IIf(IsDBNull(reader("CodigoProducto")) = True, "", reader("CodigoProducto")))

                    ' ********** dc_Descuento1 -- > ALMACEN EL VALOR DE LA CANTIDA UM1
                    .dc_Descuento1 = CDec(IIf(IsDBNull(reader("dc_Descuento1")) = True, 0, reader("dc_Descuento1")))

                    .UM1 = CStr(IIf(IsDBNull(reader("UM1")) = True, "", reader("UM1")))
                    .Equivalencia1 = CDec(IIf(IsDBNull(reader("Equivalencia1")) = True, 0, reader("Equivalencia1")))

                    ' ********** dc_Descuento2 -- > ALMACEN EL VALOR DE LA CANTIDA UM2
                    .dc_Descuento2 = CDec(IIf(IsDBNull(reader("dc_Descuento2")) = True, 0, reader("dc_Descuento2")))

                    .UM2 = CStr(IIf(IsDBNull(reader("UM2")) = True, "", reader("UM2")))
                    .Equivalencia2 = CDec(IIf(IsDBNull(reader("Equivalencia2")) = True, 0, reader("Equivalencia2")))


                    .DetalleGlosa = CStr(IIf(IsDBNull(reader("dc_DetalleGlosa")) = True, "", reader("dc_DetalleGlosa")))

                End With

                lista.Add(obj)

            End While

            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista

    End Function
    Public Sub DocumentoTomaInventario_RegistrarDetalle(ByVal objDetalleDocumento As Entidades.DetalleDocumento, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(8) {}

        p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        p(0).Value = objDetalleDocumento.IdDocumento

        p(1) = New SqlParameter("@IdProducto", SqlDbType.Int)
        p(1).Value = objDetalleDocumento.IdProducto

        p(2) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
        p(2).Value = objDetalleDocumento.IdUnidadMedida

        p(3) = New SqlParameter("@UnidadMedida", SqlDbType.VarChar)
        p(3).Value = objDetalleDocumento.UMedida

        p(4) = New SqlParameter("@Cantidad", SqlDbType.Decimal)
        p(4).Value = objDetalleDocumento.Cantidad

        p(5) = New SqlParameter("@Cantidad_Sistema", SqlDbType.Decimal)
        p(5).Value = objDetalleDocumento.CantxAtender

        p(6) = New SqlParameter("@dc_Descuento1", SqlDbType.Decimal)
        p(6).Value = objDetalleDocumento.dc_Descuento1

        p(7) = New SqlParameter("@dc_Descuento2", SqlDbType.Decimal)
        p(7).Value = objDetalleDocumento.dc_Descuento2

        p(8) = New SqlParameter("@dc_DetalleGlosa", SqlDbType.VarChar)
        p(8).Value = objDetalleDocumento.DetalleGlosa

        cmd = New SqlCommand
        cmd.Connection = cn
        cmd.CommandText = "_DocumentoTomaInventario_RegistrarDetalle"
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Transaction = tr
        cmd.CommandTimeout = 0
        cmd.Parameters.AddRange(p)

        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try


    End Sub
    Public Sub DocumentoTomaInventario_DeshacerMov(ByVal IdDocumento As Integer, ByVal DeleteDetalleDocumento As Boolean, ByVal DeleteRelacionDocumento As Boolean, ByVal DeleteDocumento_Persona As Boolean, ByVal DeleteObservacion As Boolean, ByVal Anular As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(5) {}

        p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        p(0).Value = IdDocumento

        p(1) = New SqlParameter("@DeleteDetalleDocumento", SqlDbType.Bit)
        p(1).Value = DeleteDetalleDocumento

        p(2) = New SqlParameter("@DeleteRelacionDocumento", SqlDbType.Bit)
        p(2).Value = DeleteRelacionDocumento

        p(3) = New SqlParameter("@DeleteDocumento_Persona", SqlDbType.Bit)
        p(3).Value = DeleteDocumento_Persona

        p(4) = New SqlParameter("@DeleteObservacion", SqlDbType.Bit)
        p(4).Value = DeleteObservacion

        p(5) = New SqlParameter("@Anular", SqlDbType.Bit)
        p(5).Value = Anular

        cmd = New SqlCommand
        cmd.Connection = cn
        cmd.CommandText = "_DocumentoTomaInventario_DeshacerMov"
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Transaction = tr
        cmd.CommandTimeout = 0
        cmd.Parameters.AddRange(p)

        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try



    End Sub
    Public Function DocumentoTomaInventario_FiltroDetalle(ByVal IdDocumento As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodigoSubLinea As String, ByVal Producto As String, _
                                            ByVal CodigoProducto As String, ByVal IdtipoExistencia As Integer, ByVal pageIndex As Integer, ByVal pageSize As Integer) As List(Of Entidades.DetalleDocumento)

        Dim lista As New List(Of Entidades.DetalleDocumento)

        Try

            Dim p() As SqlParameter = New SqlParameter(8) {}

            p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            p(0).Value = IdDocumento

            p(1) = New SqlParameter("@IdLinea", SqlDbType.Int)
            p(1).Value = IdLinea

            p(2) = New SqlParameter("@IdSubLinea", SqlDbType.Int)
            p(2).Value = IdSubLinea

            p(3) = New SqlParameter("@Producto", SqlDbType.VarChar)
            p(3).Value = Producto

            p(4) = New SqlParameter("@CodigoSubLinea", SqlDbType.VarChar)
            p(4).Value = CodigoSubLinea

            p(5) = New SqlParameter("@CodigoProducto", SqlDbType.VarChar)
            p(5).Value = CodigoProducto

            p(6) = New SqlParameter("@IdtipoExistencia", SqlDbType.Int)
            p(6).Value = IdtipoExistencia

            p(7) = New SqlParameter("@PageIndex", SqlDbType.Int)
            p(7).Value = pageIndex

            p(8) = New SqlParameter("@PageSize", SqlDbType.Int)
            p(8).Value = pageSize

            cn = objConexion.ConexionSIGE
            cn.Open()

            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_DocumentoTomaInventario_FiltroDetalle", p)

            While (reader.Read)

                Dim obj As New Entidades.DetalleDocumento

                With obj

                    .IdProducto = CInt(IIf(IsDBNull(reader("IdProducto")) = True, 0, reader("IdProducto")))
                    .IdUnidadMedida = CInt(IIf(IsDBNull(reader("IdUnidadMedida")) = True, 0, reader("IdUnidadMedida")))
                    .NomProducto = CStr(IIf(IsDBNull(reader("Producto")) = True, "", reader("Producto")))
                    .UMedida = CStr(IIf(IsDBNull(reader("UnidadMedida")) = True, "", reader("UnidadMedida")))
                    .Cantidad = CDec(IIf(IsDBNull(reader("Cantidad")) = True, 0, reader("Cantidad")))
                    .CantxAtender = CDec(IIf(IsDBNull(reader("Cantidad_Sistema")) = True, 0, reader("Cantidad_Sistema")))
                    .CodigoProducto = CStr(IIf(IsDBNull(reader("CodigoProducto")) = True, "", reader("CodigoProducto")))
                    .DetalleGlosa = CStr(IIf(IsDBNull(reader("dc_DetalleGlosa")) = True, "", reader("dc_DetalleGlosa")))
                    .UM1 = CStr(IIf(IsDBNull(reader("UM1")) = True, "", reader("UM1")))
                    .UM2 = CStr(IIf(IsDBNull(reader("UM2")) = True, "", reader("UM2")))
                    .dc_Descuento1 = CDec(IIf(IsDBNull(reader("dc_Descuento1")) = True, 0, reader("dc_Descuento1")))
                    .dc_Descuento2 = CDec(IIf(IsDBNull(reader("dc_Descuento2")) = True, 0, reader("dc_Descuento2")))
                    .Equivalencia1 = CDec(IIf(IsDBNull(reader("Equivalencia1")) = True, 0, reader("Equivalencia1")))
                    .Equivalencia2 = CDec(IIf(IsDBNull(reader("Equivalencia2")) = True, 0, reader("Equivalencia2")))

                End With

                lista.Add(obj)

            End While

            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista


    End Function
    Public Function DocumentoTomaInventario_FiltroDetalle_Consolidado(ByVal IdDocumento As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, _
                                            ByVal CodigoSubLinea As String, ByVal Producto As String, ByVal CodigoProducto As String, ByVal IdtipoExistencia As Integer, _
                                            ByVal pageIndex As Integer, ByVal PageSize As Integer) As DataSet

        Dim ds As New DataSet
        Dim da As SqlDataAdapter = Nothing

        Try

            Dim p() As SqlParameter = New SqlParameter(8) {}

            p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            p(0).Value = IdDocumento

            p(1) = New SqlParameter("@IdLinea", SqlDbType.Int)
            p(1).Value = IdLinea

            p(2) = New SqlParameter("@IdSubLinea", SqlDbType.Int)
            p(2).Value = IdSubLinea

            p(3) = New SqlParameter("@Producto", SqlDbType.VarChar)
            p(3).Value = Producto

            p(4) = New SqlParameter("@CodigoSubLinea", SqlDbType.VarChar)
            p(4).Value = CodigoSubLinea

            p(5) = New SqlParameter("@CodigoProducto", SqlDbType.VarChar)
            p(5).Value = CodigoProducto

            p(6) = New SqlParameter("@IdTipoExistencia", SqlDbType.Int)
            p(6).Value = IdtipoExistencia

            p(7) = New SqlParameter("@PageIndex", SqlDbType.Int)
            p(7).Value = pageIndex

            p(8) = New SqlParameter("@PageSize", SqlDbType.Int)
            p(8).Value = PageSize


            cn = objConexion.ConexionSIGE

            cmd = New SqlCommand("_DocumentoTomaInventario_FiltroDetalle_Consolidado", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            cmd.Parameters.AddRange(p)

            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Detalle")

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return ds


    End Function










    '*****************************   VERSIÓN I < PROCESO DE TOMA DE INVENTARIO / AJUSTE DE INVENTARIO >

    Public Function SelectLineaxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.Linea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocTomaInventarioSelectLineaxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Linea)
                Do While lector.Read
                    Dim objLinea As New Entidades.Linea
                    objLinea.Id = CInt(lector.Item("IdLinea"))
                    objLinea.Descripcion = CStr(IIf(IsDBNull(lector("lin_Nombre")) = True, "", lector("lin_Nombre")))
                    Lista.Add(objLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectSubLineaxIdDocumentoxIdLinea(ByVal IdDocumento As Integer, ByVal IdLinea As Integer) As List(Of Entidades.SubLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocTomaInventarioSelectSubLineaxIdDocumentoxIdLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea)
                Do While lector.Read
                    Dim objSubLinea As New Entidades.SubLinea
                    objSubLinea.Id = CInt(lector.Item("IdSubLInea"))
                    objSubLinea.Nombre = CStr(IIf(IsDBNull(lector("sl_Nombre")) = True, "", lector("sl_Nombre")))
                    Lista.Add(objSubLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectDetallexIdDocumentoxIdSubLinea(ByVal IdDocumento As Integer, ByVal IdSubLinea As Integer) As List(Of Entidades.DetalleDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocTomaInventarioSelectDetallexIdDocumentoxIdSubLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleDocumento)
                Do While lector.Read
                    Dim objDetalleDoc As New Entidades.DetalleDocumento
                    With objDetalleDoc

                        .IdDetalleDocumento = CInt(IIf(IsDBNull(lector("IdDetalleDocumento")) = True, 0, lector("IdDetalleDocumento")))
                        .IdProducto = CInt(IIf(IsDBNull(lector("IdProducto")) = True, 0, lector("IdProducto")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector("IdUnidadMedida")) = True, 0, lector("IdUnidadMedida")))
                        .IdDocumento = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                        .Cantidad = CDec(IIf(IsDBNull(lector("dc_Cantidad")) = True, 0, lector("dc_Cantidad")))


                        .CantxAtender = CDec(IIf(IsDBNull(lector("dc_CantxAtender")) = True, 0, lector("dc_CantxAtender")))





                        .NomProducto = CStr(IIf(IsDBNull(lector("prod_Nombre")) = True, "", lector("prod_Nombre")))
                        .UMedida = CStr(IIf(IsDBNull(lector("dc_UMedida")) = True, "", lector("dc_UMedida")))

                    End With
                    Lista.Add(objDetalleDoc)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxIdDocumento(ByVal IdDocumento As Integer) As Entidades.Documento

        Dim objDocumento As Entidades.Documento = Nothing

        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Try

            Dim cmd As New SqlCommand("_DocumentoTomaInvSelectxIdDocumento", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            cn.Open()

            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            If (lector.Read) Then

                objDocumento = New Entidades.Documento
                With objDocumento

                    .Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                    .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                    .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))
                    .FechaEntrega = CDate(IIf(IsDBNull(lector("doc_FechaEntrega")) = True, Nothing, lector("doc_FechaEntrega")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(lector("IdEstadoDoc")) = True, 0, lector("IdEstadoDoc")))
                    .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")) = True, 0, lector("IdTienda")))
                    .IdSerie = CInt(IIf(IsDBNull(lector("IdSerie")) = True, 0, lector("IdSerie")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector("IdEmpresa")) = True, 0, lector("IdEmpresa")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector("IdTipoDocumento")) = True, 0, lector("IdTipoDocumento")))
                    .IdAlmacen = CInt(IIf(IsDBNull(lector("IdAlmacen")) = True, 0, lector("IdAlmacen")))
                    .NomAlmacen = CStr(IIf(IsDBNull(lector("alm_Nombre")) = True, "", lector("alm_Nombre")))
                    .NomEmpleado = CStr(IIf(IsDBNull(lector("NomEmpleado")) = True, "", lector("NomEmpleado")))

                    .NomEmpresaTomaInv = CStr(IIf(IsDBNull(lector("NomEmpresaTomaInv")) = True, "", lector("NomEmpresaTomaInv")))
                    .IdDestinatario = CInt(IIf(IsDBNull(lector("IdEmpresaTomaInv")) = True, 0, lector("IdEmpresaTomaInv")))

                    .NomEstadoDocumento = CStr(IIf(IsDBNull(lector("edoc_Nombre")) = True, "", lector("edoc_Nombre")))
                End With

            End If

            lector.Close()

        Catch ex As Exception

            objDocumento = Nothing

        Finally

            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try


        Return objDocumento

    End Function
    Public Function SelectxIdSeriexCodigo(ByVal IdSerie As Integer, ByVal codigo As Integer) As Entidades.Documento

        Dim objDocumento As Entidades.Documento = Nothing

        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Try

            Dim cmd As New SqlCommand("_DocumentoTomaInvSelectxIdSeriexCodigo", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdSerie", IdSerie)
            cmd.Parameters.AddWithValue("@Codigo", codigo)

            cn.Open()

            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            If (lector.Read) Then

                objDocumento = New Entidades.Documento
                With objDocumento

                    .Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                    .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                    .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))
                    .FechaEntrega = CDate(IIf(IsDBNull(lector("doc_FechaEntrega")) = True, Nothing, lector("doc_FechaEntrega")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(lector("IdEstadoDoc")) = True, 0, lector("IdEstadoDoc")))
                    .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")) = True, 0, lector("IdTienda")))
                    .IdSerie = CInt(IIf(IsDBNull(lector("IdSerie")) = True, 0, lector("IdSerie")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector("IdEmpresa")) = True, 0, lector("IdEmpresa")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector("IdTipoDocumento")) = True, 0, lector("IdTipoDocumento")))
                    .IdAlmacen = CInt(IIf(IsDBNull(lector("IdAlmacen")) = True, 0, lector("IdAlmacen")))
                    .NomAlmacen = CStr(IIf(IsDBNull(lector("alm_Nombre")) = True, "", lector("alm_Nombre")))
                    .NomEmpleado = CStr(IIf(IsDBNull(lector("NomEmpleado")) = True, "", lector("NomEmpleado")))

                    .NomEmpresaTomaInv = CStr(IIf(IsDBNull(lector("NomEmpresaTomaInv")) = True, "", lector("NomEmpresaTomaInv")))
                    .IdDestinatario = CInt(IIf(IsDBNull(lector("IdEmpresaTomaInv")) = True, 0, lector("IdEmpresaTomaInv")))

                    .PoseeDocRelacionado = CStr(IIf(IsDBNull(lector("PoseeDocAjusteInv")) = True, "0", lector("PoseeDocAjusteInv")))
                End With

            End If

            lector.Close()

        Catch ex As Exception

            objDocumento = Nothing

        Finally

            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try


        Return objDocumento

    End Function
    Public Function getDataSet_DocAjusteInventario(ByVal IdDocumento As Integer, ByVal opcion As Integer) As DataSet

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ds As New DataSet
        Try

            '************** Cabecera
            Dim cmd As New SqlCommand("_CR_DocumentoAjusteInvCabxIdDocumento", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocumentoAjusteInvCab")


            '************ Doc Toma Inventario Relacionado
            cmd = New SqlCommand("_CR_DocumentoAjusteInvSelectDocTomaInvxIdDocumento", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocumentoAjusteInv_DocTomaInv")


            '************ Detalle
            cmd = New SqlCommand("_CR_DocumentoAjusteInventarioDetxIdDocumento", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            cmd.Parameters.AddWithValue("@Opcion", opcion)

            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocumentoAjusteInventarioDet")

        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return ds
    End Function
    Public Function getDataSet_DocTomaInvFormato(ByVal IdDocumento As Integer, ByVal opcion As String) As DataSet

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ds As New DataSet
        Try

            '************** Cabecera
            Dim cmd As New SqlCommand("_CR_DocTomaInventarioFormatoCab", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocTomaInventarioFormatoCab")

            '************ Detalle
            cmd = New SqlCommand("_CR_DocTomaInventarioFormatoDet", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            cmd.Parameters.AddWithValue("@Opcion", opcion)
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocTomaInventarioFormatoDet")


            '************ Encargados
            cmd = New SqlCommand("_Documento_PersonaSelectxIdDocumento", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Doc_Persona")




        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return ds
    End Function
    Public Function RecalcularSaldosDocTomaInv(ByVal IdDocumento As Integer, ByVal FechaFin As Date, Optional ByVal NroDias_Add As Integer = 0) As Boolean

        Dim hecho As Boolean = False
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try
            cn.Open()
            tr = cn.BeginTransaction

            Dim cmd As New SqlCommand("_DocumentoRecalcularSaldosDocTomaInv", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            cmd.Parameters.AddWithValue("@FechaFin", FechaFin)
            cmd.Parameters.AddWithValue("@NroDias_Add", NroDias_Add)

            If (cmd.ExecuteNonQuery <= 0) Then
                Throw New Exception
            End If

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            Throw ex
            hecho = False
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return hecho
    End Function
    Public Function GenerarDocTomaInventario(ByVal IdDocumento As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal FechaFin As Date, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean

        Try

            Dim cmd As New SqlCommand("_DocumentoGenerarTomaInventario", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
            cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
            cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
            cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
            cmd.Parameters.AddWithValue("@FechaFin", FechaFin)

            If cmd.ExecuteNonQuery <= 0 Then
                Return False
            End If

            Return True

        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function
    Public Function SelectDetallexIdDocumentoxIdSubLinea_Ajuste(ByVal IdDocumentoTomaInv As Integer, ByVal IdSubLinea As Integer, ByVal IdMonedaDestino As Integer) As List(Of Entidades.DetalleDocumentoTomaInventario)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocTomaInventarioSelectDetallexIdDocumentoxIdSubLinea_Ajuste", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumentoTomaInv)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@IdMonedaDestino", IdMonedaDestino)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleDocumentoTomaInventario)
                Do While lector.Read
                    Dim obj As New Entidades.DetalleDocumentoTomaInventario
                    With obj

                        .IdProducto = CInt(IIf(IsDBNull(lector("IdProducto")) = True, 0, lector("IdProducto")))
                        .IdDetalleDocumento = CInt(IIf(IsDBNull(lector("IdDetalleDocumento")) = True, 0, lector("IdDetalleDocumento")))
                        .IdDocumento = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector("IdUnidadMedida")) = True, 0, lector("IdUnidadMedida")))


                        .CantidadTomaInv = CDec(IIf(IsDBNull(lector("dc_Cantidad")) = True, 0, lector("dc_Cantidad")))
                        .CantidadSistema = CDec(IIf(IsDBNull(lector("dc_CantxAtender")) = True, 0, lector("dc_CantxAtender")))
                        .UMedida = CStr(IIf(IsDBNull(lector("dc_UMedida")) = True, "", lector("dc_UMedida")))
                        .NomProducto = CStr(IIf(IsDBNull(lector("prod_Nombre")) = True, "", lector("prod_Nombre")))

                        .Faltante = CDec(IIf(IsDBNull(lector("Faltante")) = True, 0, lector("Faltante")))
                        .Sobrante = CDec(IIf(IsDBNull(lector("Sobrante")) = True, 0, lector("Sobrante")))
                        .CostoUnit = CDec(IIf(IsDBNull(lector("CostoUnit")) = True, 0, lector("CostoUnit")))

                        .CostoFaltante = CDec(IIf(IsDBNull(lector("CostoFaltante")) = True, 0, lector("CostoFaltante")))
                        .CostoSobrante = CDec(IIf(IsDBNull(lector("CostoSobrante")) = True, 0, lector("CostoSobrante")))

                        .CantidadAjuste = CDec(IIf(IsDBNull(lector("CantidadAjuste")) = True, 0, lector("CantidadAjuste")))
                        .DescEstado = CStr(IIf(IsDBNull(lector("DescEstado")) = True, "", lector("DescEstado")))
                        .Ajustado = CStr(IIf(IsDBNull(lector("Estado")) = True, 0, lector("Estado")))

                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function InsertaDetalleDocumentoAjusteInventario(ByVal cn As SqlConnection, ByVal detalledocumento As Entidades.DetalleDocumento, ByVal T As SqlTransaction) As Integer
        Dim ArrayParametros() As SqlParameter = New SqlParameter(9) {}
        ArrayParametros(0) = New SqlParameter("@IdDetalleDocumento", SqlDbType.Int)
        ArrayParametros(0).Direction = ParameterDirection.Output
        ArrayParametros(1) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(1).Value = detalledocumento.IdDocumento
        ArrayParametros(2) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(2).Value = detalledocumento.IdProducto
        ArrayParametros(3) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
        ArrayParametros(3).Value = detalledocumento.IdUnidadMedida
        ArrayParametros(4) = New SqlParameter("@dc_Cantidad", SqlDbType.Decimal)
        ArrayParametros(4).Value = IIf(detalledocumento.Cantidad = Nothing, DBNull.Value, detalledocumento.Cantidad)
        ArrayParametros(5) = New SqlParameter("@dc_UMedida", SqlDbType.VarChar)
        ArrayParametros(5).Value = IIf(detalledocumento.UMedida = Nothing, DBNull.Value, detalledocumento.UMedida)
        ArrayParametros(6) = New SqlParameter("@dc_Peso", SqlDbType.Decimal)
        ArrayParametros(6).Value = IIf(detalledocumento.Peso = Nothing, DBNull.Value, detalledocumento.Peso)
        ArrayParametros(7) = New SqlParameter("@dc_UmPeso", SqlDbType.VarChar)
        ArrayParametros(7).Value = IIf(detalledocumento.UmPeso = Nothing, DBNull.Value, detalledocumento.UmPeso)
        ArrayParametros(8) = New SqlParameter("@IdDetalleAfecto", SqlDbType.Int)
        ArrayParametros(8).Value = IIf(detalledocumento.IdDetalleAfecto = Nothing, DBNull.Value, detalledocumento.IdDetalleAfecto)

        '********************* utilizado para el mov Almacen
        ArrayParametros(9) = New SqlParameter("@Factor", SqlDbType.Int)
        ArrayParametros(9).Value = detalledocumento.Factor

        Return HDAO.InsertaTParameterOutPut(cn, "_DetalleDocumentoAjusteInventarioInsert", ArrayParametros, T)
    End Function
    Public Function SelectDocAjusteInventarioxIdDocTomaInv(ByVal IdDocumentoTomaInv As Integer) As Entidades.Documento

        Dim objDocumento As Entidades.Documento = Nothing

        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Try

            Dim cmd As New SqlCommand("_DocumentoAjusteInventarioSelectxIdDocTomaInv", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumentoTomaInv", IdDocumentoTomaInv)

            cn.Open()

            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            If (lector.Read) Then

                objDocumento = New Entidades.Documento
                With objDocumento

                    .Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                    .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                    .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))
                    .FechaEntrega = CDate(IIf(IsDBNull(lector("doc_FechaEntrega")) = True, Nothing, lector("doc_FechaEntrega")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(lector("IdEstadoDoc")) = True, 0, lector("IdEstadoDoc")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(lector("IdTipoOperacion")) = True, 0, lector("IdTipoOperacion")))


                    .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")) = True, 0, lector("IdTienda")))
                    .IdSerie = CInt(IIf(IsDBNull(lector("IdSerie")) = True, 0, lector("IdSerie")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector("IdEmpresa")) = True, 0, lector("IdEmpresa")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector("IdTipoDocumento")) = True, 0, lector("IdTipoDocumento")))
                    .IdAlmacen = CInt(IIf(IsDBNull(lector("IdAlmacen")) = True, 0, lector("IdAlmacen")))

                End With

            End If

            lector.Close()

        Catch ex As Exception

            objDocumento = Nothing

        Finally

            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try


        Return objDocumento

    End Function
    Public Function EliminarProcesoxIdProductoxIdDocumentoAjusteInv(ByVal IdDocAjusteInv As Integer, ByVal IdProducto As Integer) As Boolean

        Dim hecho As Boolean = False
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction

            Dim cmd As New SqlCommand("_DocumentoAjusteInvEliminarProcesoxIdProductoxIdDocumentoAjusteInv", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
            cmd.Parameters.AddWithValue("@IdDocAjusteInv", IdDocAjusteInv)

            cmd.ExecuteNonQuery()
            tr.Commit()
            hecho = True

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

        Return hecho

    End Function
    Public Function SelectDocAjusteInventarioxIdSeriexCodigo(ByVal IdSerie As Integer, ByVal Codigo As Integer) As Entidades.Documento

        Dim objDocumento As Entidades.Documento = Nothing

        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Try

            Dim cmd As New SqlCommand("_DocumentoAjusteInventarioSelectxIdSeriexCodigo", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdSerie", IdSerie)
            cmd.Parameters.AddWithValue("@Codigo", Codigo)

            cn.Open()

            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            If (lector.Read) Then

                objDocumento = New Entidades.Documento
                With objDocumento

                    .Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                    .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                    .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))
                    .FechaEntrega = CDate(IIf(IsDBNull(lector("doc_FechaEntrega")) = True, Nothing, lector("doc_FechaEntrega")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(lector("IdEstadoDoc")) = True, 0, lector("IdEstadoDoc")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(lector("IdTipoOperacion")) = True, 0, lector("IdTipoOperacion")))


                    .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")) = True, 0, lector("IdTienda")))
                    .IdSerie = CInt(IIf(IsDBNull(lector("IdSerie")) = True, 0, lector("IdSerie")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector("IdEmpresa")) = True, 0, lector("IdEmpresa")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector("IdTipoDocumento")) = True, 0, lector("IdTipoDocumento")))
                    .IdAlmacen = CInt(IIf(IsDBNull(lector("IdAlmacen")) = True, 0, lector("IdAlmacen")))

                    .IdDocRelacionado = CInt(IIf(IsDBNull(lector("IdDocTomaInventario")) = True, 0, lector("IdDocTomaInventario")))

                End With

            End If

            lector.Close()

        Catch ex As Exception

            objDocumento = Nothing

        Finally

            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try


        Return objDocumento

    End Function
    Public Function DocumentoAjusteInventarioAnularxIdDocumento(ByVal IdDocumento As Integer) As Boolean

        Dim hecho As Boolean = False
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try

            cn.Open()
            tr = cn.BeginTransaction

            Dim cmd As New SqlCommand("_DocumentoAjusteInventarioAnularxIdDocumento", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            cmd.ExecuteNonQuery()

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            Throw New Exception("Problema en la carga de datos.")
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function
    Public Function DocumentoTomaInventarioSelectxIdAlmacen(ByVal IdAlmacen As Integer) As List(Of Entidades.Documento)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoTomaInventarioSelectxIdAlmacen", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Documento)
                Do While lector.Read
                    Dim objDocumento As New Entidades.Documento
                    With objDocumento

                        .Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                        .Codigo = CStr(IIf(IsDBNull(lector("NroDocumento")) = True, "", lector("NroDocumento")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))
                        .FechaEntrega = CDate(IIf(IsDBNull(lector("doc_FechaEntrega")) = True, Nothing, lector("doc_FechaEntrega")))
                        .NomAlmacen = CStr(IIf(IsDBNull(lector("alm_Nombre")) = True, "", lector("alm_Nombre")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))
                        .NomEmpresaTomaInv = CStr(IIf(IsDBNull(lector("DescEmpresa")) = True, "", lector("DescEmpresa")))

                    End With

                    Lista.Add(objDocumento)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try


    End Function
    Public Sub DocumentTomaInventarioAnularxIdDocumento(ByVal IdDocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As New SqlCommand("_DocumentoTomaInventarioAnularxIdDocumento", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub
End Class
