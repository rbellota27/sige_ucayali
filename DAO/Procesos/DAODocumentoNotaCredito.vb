﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.










'***************************   MARTES 06 DE ABRIL 2010 12_02 PM






Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAODocumentoNotaCredito

    Private objConexion As New DAO.Conexion



    Public Sub Registrar_DetalleDocumento(ByVal ListaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal IdDocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim P() As SqlParameter
        Dim cmd As New SqlCommand("[dbo].[_DetalleDocumentoInsert_NC]", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure

        For Each obj As Entidades.DetalleDocumento In ListaDetalleDocumento

            P = New SqlParameter(9) {}

            P(0) = New SqlParameter("@IdDetalleDocumento", SqlDbType.Int)
            P(0).Direction = ParameterDirection.Output

            P(1) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            P(1).Value = IdDocumento

            P(2) = New SqlParameter("@IdProducto", SqlDbType.Int)
            P(2).Value = obj.IdProducto

            P(3) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
            P(3).Value = obj.IdUnidadMedida

            P(4) = New SqlParameter("@dc_UMedida", SqlDbType.VarChar)
            P(4).Value = obj.UMedida

            P(5) = New SqlParameter("@dc_Cantidad", SqlDbType.Decimal)
            P(5).Value = obj.Cantidad

            P(6) = New SqlParameter("@dc_PrecioSD", SqlDbType.Decimal)
            P(6).Value = obj.PrecioSD

            P(7) = New SqlParameter("@dc_PrecioCD", SqlDbType.Decimal)
            P(7).Value = obj.PrecioCD

            P(8) = New SqlParameter("@dc_Importe", SqlDbType.Decimal)
            P(8).Value = obj.Importe

            P(9) = New SqlParameter("@IdDetalleAfecto", SqlDbType.Int)
            P(9).Value = obj.IdDetalleAfecto

            cmd.Parameters.AddRange(P)            
            cmd.ExecuteNonQuery()
            obj.IdDetalleDocumento = CInt(cmd.Parameters(0).Value)
            cmd.Parameters.Clear()

        Next



    End Sub


    Public Sub DocumentoNC_Deshacer_MovFact(ByVal IdDocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        '********************** ANULA EL STOCK COMPROMETIDO DEL DOCUMENTO DE REFERENCIA SI NO POSEE GUIA DE REMISION O ORDEN DE DESPACHO
        Dim P() As SqlParameter = New SqlParameter(0) {}
        P(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        P(0).Value = IdDocumento

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoNC_Deshacer_MovFact", P)

    End Sub

    Public Sub DocumentoNotaCredito_ValidarDocumentoNC_Rel(ByVal IdDocumentoRef As Integer, ByVal IdMoneda As Integer, ByVal Monto_NC As Decimal, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        '********************** VALIDA SI EL DOCUMENTO DE REFERENCIA POSEE NOTAS DE CREDITO ASOCIADOS Y SI EL MONTO TOTAL SUPERA EL TOTAL DEL DOCUMENTO REF

        Dim p() As SqlParameter = New SqlParameter(2) {}

        p(0) = New SqlParameter("@IdDocumentoRef", SqlDbType.Int)
        p(0).Value = IdDocumentoRef

        p(1) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        p(1).Value = IdMoneda

        p(2) = New SqlParameter("@Monto_NC", SqlDbType.Decimal)
        p(2).Value = Monto_NC

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoNotaCredito_ValidarDocumentoNC_Rel", p)

    End Sub

    Public Sub DocumentoNotaCredito_AplicacionEfectivoCaja(ByVal IdDocumento As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdCaja As Integer, ByVal IdUsuario As Integer, ByVal IdTipoMov As Integer, ByVal IdMedioPago As Integer, ByVal IdMoneda As Integer, ByVal Factor As Integer, ByVal Monto As Decimal, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As New SqlCommand("_DocumentoNotaCredito_AplicacionEfectivoCaja", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdCaja", IdCaja)
        cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario)
        cmd.Parameters.AddWithValue("@IdTipoMov", IdTipoMov)
        cmd.Parameters.AddWithValue("@IdMedioPago", IdMedioPago)
        cmd.Parameters.AddWithValue("@IdMoneda", IdMoneda)
        cmd.Parameters.AddWithValue("@Factor", Factor)
        cmd.Parameters.AddWithValue("@Monto", Monto)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Sub

    Public Sub DocumentoNotaCredito_DisminuirSaldo_Ventas(ByVal IdDocumento As Integer, ByVal monto As Decimal, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As New SqlCommand("_DocumentoNotaCredito_DisminuirSaldo_Ventas", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@Monto", monto)

        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Sub

    Public Sub DocumentoNotaCredito_UpdateSaldoFavorCliente(ByVal IdDocumento As Integer, ByVal monto As Decimal, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As New SqlCommand("_DocumentoNotaCredito_UpdateSaldoFavorCliente", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@Monto", monto)

        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Sub

    Public Sub DocumentoNotaCreditoAplicacion_DeshacerAplicacion(ByVal IdDocumento As Integer, ByVal deshacer_AppDeudas As Boolean, ByVal deshacer_AppEfectivoCaja As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As New SqlCommand("_DocumentoNotaCreditoAplicacion_DeshacerAplicacion", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@deshacer_AppDeudas", deshacer_AppDeudas)
        cmd.Parameters.AddWithValue("@deshacer_AppEfectivoCaja", deshacer_AppEfectivoCaja)

        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Sub

    Public Sub DocumentoNotaCreditoAplicacion_ValSaldoRestante(ByVal IdDocumentoNotaCredito As Integer, ByVal Opcion As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)


        Dim cmd As New SqlCommand("_DocumentoNotaCreditoAplicacion_ValSaldoRestante", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumentoNotaCredito)
        cmd.Parameters.AddWithValue("@Opcion", Opcion)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Sub

    Public Sub DocumentoNotaCredito_AplicacionDeudas(ByVal IdDocumentoNotaCredito As Integer, ByVal IdDocumentoRef As Integer, ByVal Abono As Decimal, ByVal IdMovCuentaDocRef As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)


        Dim cmd As New SqlCommand("_DocumentoNotaCredito_AplicacionDeudas", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumentoNotaCredito", IdDocumentoNotaCredito)
        cmd.Parameters.AddWithValue("@IdDocumentoRef", IdDocumentoRef)
        cmd.Parameters.AddWithValue("@Abono", Abono)
        cmd.Parameters.AddWithValue("@IdMovCuentaDocRef", IdMovCuentaDocRef)

        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Sub

    Public Function DocumentoNotaCreditoSelectCabFind_Aplicacion(ByVal IdPersona As Integer, ByVal IdTienda As Integer, ByVal Filtro As Integer) As List(Of Entidades.DocumentoView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoNotaCreditoSelectCabFind_Aplicacion", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@Filtro", Filtro)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DocumentoView)
                Do While lector.Read
                    Dim obj As New Entidades.DocumentoView
                    With obj
                        .IdDocumento = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))
                        .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                        .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector("doc_FechaEmision")) = True, "", Format(lector("doc_FechaEmision"), "dd/MM/yyyy")))
                        .FechaVenc = CDate(IIf(IsDBNull(lector("doc_FechaVenc")) = True, Nothing, lector("doc_FechaVenc")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")) = True, 0, lector("doc_TotalAPagar")))
                        .ImporteTotal = CDec(IIf(IsDBNull(lector("doc_ImporteTotal")) = True, 0, lector("doc_ImporteTotal")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector("IdEstadoDoc")) = True, 0, lector("IdEstadoDoc")))
                        .IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")) = True, 0, lector("IdPersona")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")) = True, 0, lector("IdMoneda")))
                        .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")) = True, 0, lector("IdTienda")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector("IdEmpresa")) = True, 0, lector("IdEmpresa")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector("IdTipoDocumento")) = True, 0, lector("IdTipoDocumento")))
                        .IdEstadoCancelacion = CInt(IIf(IsDBNull(lector("IdEstadoCan")) = True, 0, lector("IdEstadoCan")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")) = True, "", lector("mon_Simbolo")))
                        .NomEstadoCancelacion = CStr(IIf(IsDBNull(lector("ecan_Nombre")) = True, "", lector("ecan_Nombre")))
                        .NomPropietario = CStr(IIf(IsDBNull(lector("per_NComercial")) = True, "", lector("per_NComercial")))
                        .NomTienda = CStr(IIf(IsDBNull(lector("tie_Nombre")) = True, "", lector("tie_Nombre")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector("edoc_Nombre")) = True, "", lector("edoc_Nombre")))
                        .Contador = CInt(IIf(IsDBNull(lector("DiasVigencia")) = True, 0, lector("DiasVigencia")))
                        .Numero = .Serie + " - " + .Codigo
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function

    Public Sub DocumentoNotaCreditoDeshacerMov(ByVal IdDocumento As Integer, ByVal DeleteRelacionDocumento As Boolean, ByVal DeleteDetalleDocumento As Boolean, ByVal DeleteObservacion As Boolean, ByVal deleteDetalleConcepto As Boolean, ByVal Anular As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        'Dim cmd As New SqlCommand("_DocumentoNotaCreditoDeshacerMov", cn, tr)
        Dim cmd As New SqlCommand("_DocumentoNotaCreditoDeshacerMov_V2", cn, tr)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@DeleteRelacionDocumento", DeleteRelacionDocumento)
        cmd.Parameters.AddWithValue("@DeleteDetalleDocumento", DeleteDetalleDocumento)
        cmd.Parameters.AddWithValue("@DeleteObservacion", DeleteObservacion)
        cmd.Parameters.AddWithValue("@DeleteDetalleConcepto", deleteDetalleConcepto)
        cmd.Parameters.AddWithValue("@Anular", Anular)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Sub

    Public Function SelectDocReferenciaxParams(ByVal IdCliente As Integer, ByVal FechaI As Date, ByVal FechaF As Date, _
                                               ByVal IdTienda As Integer, ByVal serie As Integer, ByVal codigo As Integer, _
                                               ByVal opcion As Integer, ByVal ValDocRelacionado As Boolean, _
                                               ByVal idTipoDocumento As Integer) As List(Of Entidades.DocumentoView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoNotaCreditoSelectDocReferenciaxParams_v2", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdCliente", IdCliente)
        cmd.Parameters.AddWithValue("@FechaI", FechaI)
        cmd.Parameters.AddWithValue("@FechaF", FechaF)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@Serie", serie)
        cmd.Parameters.AddWithValue("@Codigo", codigo)
        cmd.Parameters.AddWithValue("@Opcion", opcion)
        cmd.Parameters.AddWithValue("@ValDocRelacionado", ValDocRelacionado)
        cmd.Parameters.AddWithValue("@idTipoDocumento", idTipoDocumento)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DocumentoView)
                Do While lector.Read
                    Dim obj As New Entidades.DocumentoView
                    With obj
                        .IdDocumento = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))
                        .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                        .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector("doc_FechaEmision")) = True, "", Format(lector("doc_FechaEmision"), "dd/MM/yyyy")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")) = True, "", lector("mon_Simbolo")))
                        .Total = CDec(IIf(IsDBNull(lector("doc_Total")) = True, 0, lector("doc_Total")))
                        .Percepcion = CDec(IIf(IsDBNull(lector("Percepcion")) = True, 0, lector("Percepcion")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")) = True, 0, lector("doc_TotalAPagar")))
                        .NomPropietario = CStr(IIf(IsDBNull(lector("per_NComercial")) = True, "", lector("per_NComercial")))
                        .NomTienda = CStr(IIf(IsDBNull(lector("tie_Nombre")) = True, "", lector("tie_Nombre")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector("edoc_Nombre")) = True, "", lector("edoc_Nombre")))
                        .NomCondicionPago = CStr(IIf(IsDBNull(lector("cp_Nombre")) = True, "", lector("cp_Nombre")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector("IdEstadoDoc")) = True, 0, lector("IdEstadoDoc")))
                        .IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")) = True, 0, lector("IdPersona")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")) = True, 0, lector("IdMoneda")))
                        .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")) = True, 0, lector("IdTienda")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector("IdEmpresa")) = True, 0, lector("IdEmpresa")))
                        .IdCondicionPago = CInt(IIf(IsDBNull(lector("IdCondicionPago")) = True, 0, lector("IdCondicionPago")))
                        .Numero = .Serie + " - " + .Codigo
                        .strTipoDocumentoRef = CStr(IIf(IsDBNull(lector("CadTipoDocumentoRef")) = True, "", lector("CadTipoDocumentoRef")))
                        .anex_DescuentoGlobal = CDec(IIf(IsDBNull(lector("anex_DescuentoGlobal")) = True, 0, lector("anex_DescuentoGlobal")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function DocumentoNotaCreditoSelectCab(ByVal IdSerie As Integer, ByVal Codigo As Integer, ByVal IdDocumento As Integer) As Entidades.DocumentoNotaCredito

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim objDocumento As New Entidades.DocumentoNotaCredito

        Try

            cn.Open()

            Dim cmd As New SqlCommand("_DocumentoNotaCreditoSelectCab", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdSerie", IdSerie)
            cmd.Parameters.AddWithValue("@Codigo", Codigo)
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            Dim lector As SqlDataReader
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)

            If (lector.Read) Then
                With objDocumento

                    .Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                    .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                    .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))
                    .FechaVenc = CDate(IIf(IsDBNull(lector("doc_FechaVenc")) = True, Nothing, lector("doc_FechaVenc")))
                    .ImporteTotal = CDec(IIf(IsDBNull(lector("doc_ImporteTotal")) = True, 0, lector("doc_ImporteTotal")))
                    .SubTotal = CDec(IIf(IsDBNull(lector("doc_SubTotal")) = True, 0, lector("doc_SubTotal")))
                    .IGV = CDec(IIf(IsDBNull(lector("doc_Igv")) = True, 0, lector("doc_Igv")))
                    .Total = CDec(IIf(IsDBNull(lector("doc_Total")) = True, 0, lector("doc_Total")))
                    .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")) = True, 0, lector("doc_TotalAPagar")))
                    .IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")) = True, 0, lector("IdPersona")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(lector("IdEstadoDoc")) = True, 0, lector("IdEstadoDoc")))
                    .IdEstadoCancelacion = CInt(IIf(IsDBNull(lector("IdEstadoCan")) = True, 1, lector("IdEstadoCan"))) '******* POR APLICAR
                    .IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")) = True, 0, lector("IdMoneda")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(lector("IdTipoOperacion")) = True, 0, lector("IdTipoOperacion")))
                    .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")) = True, 0, lector("IdTienda")))
                    .IdSerie = CInt(IIf(IsDBNull(lector("IdSerie")) = True, 0, lector("IdSerie")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector("IdEmpresa")) = True, 0, lector("IdEmpresa")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector("IdTipoDocumento")) = True, 0, lector("IdTipoDocumento")))
                    .Observacion = CStr(IIf(IsDBNull(lector("ob_Observacion")) = True, "", lector("ob_Observacion")))
                    .NomMoneda = CStr(IIf(IsDBNull(lector("DescMoneda")) = True, "", lector("DescMoneda")))
                    .DescripcionPersona = CStr(IIf(IsDBNull(lector("DescripcionPersona")) = True, "", lector("DescripcionPersona")))
                    .Ruc = CStr(IIf(IsDBNull(lector("Ruc")) = True, "", lector("Ruc")))
                    .Dni = CStr(IIf(IsDBNull(lector("Dni")) = True, "", lector("Dni")))
                    .IdDocumentoRef = CInt(IIf(IsDBNull(lector("IdDocumentoReferencia")) = True, 0, lector("IdDocumentoReferencia")))
                    .TipoDocumentoRef = CStr(IIf(IsDBNull(lector("TipoDocumentoRef")) = True, "", lector("TipoDocumentoRef")))
                    .DocRef_Serie = CStr(IIf(IsDBNull(lector("DocRef_Serie")) = True, "", lector("DocRef_Serie")))
                    .DocRef_Codigo = CStr(IIf(IsDBNull(lector("DocRef_Codigo")) = True, "", lector("DocRef_Codigo")))
                    .DocRef_FechaEmision = CDate(IIf(IsDBNull(lector("DocRef_FechaEmision")) = True, Nothing, lector("DocRef_FechaEmision")))
                    .DocRef_Total = CDec(IIf(IsDBNull(lector("DocRef_Total")) = True, 0, lector("DocRef_Total")))
                    .DocRef_TotalAPagar = CDec(IIf(IsDBNull(lector("DocRef_TotalAPagar")) = True, 0, lector("DocRef_TotalAPagar")))
                    .DocRef_Percepcion = CDec(IIf(IsDBNull(lector("DocRef_Percepcion")) = True, 0, lector("DocRef_Percepcion")))

                    .DocRef_IdMoneda = CInt(IIf(IsDBNull(lector("DocRef_IdMoneda")) = True, 0, lector("DocRef_IdMoneda")))

                    .IdMotivoT = CInt(IIf(IsDBNull(lector("IdMotivoT")) = True, 0, lector("IdMotivoT")))

                End With
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            lector.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try

        Return objDocumento

    End Function


End Class
