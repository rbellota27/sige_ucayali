﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'*******************      VIERNES   19 MARZO 2010 HORA 10_22 AM

Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAODocumentoFacturacion

    Private objConexion As New DAO.Conexion
    Private tr As SqlTransaction
    Private cn As SqlConnection
    Private cmd As SqlCommand
    Private lector As SqlDataReader


    Public Function DocumentoSelectDocumentoAnticipo(ByVal IdPersona As Integer, ByVal IdMoneda_Destino As Integer) As List(Of Entidades.Documento)

        Dim listaDocumento As New List(Of Entidades.Documento)

        Try

            cn = objConexion.ConexionSIGE

            Dim param() As SqlParameter = New SqlParameter(1) {}
            param(0) = New SqlParameter("@IdPersona", SqlDbType.Int)
            param(0).Value = IdPersona
            param(1) = New SqlParameter("@IdMoneda_Destino", SqlDbType.Int)
            param(1).Value = IdMoneda_Destino

            cn.Open()

            Dim lector As SqlDataReader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_DocumentoSelectDocumentoAnticipo", param)
            While (lector.Read)

                Dim objDocumento As New Entidades.Documento
                With objDocumento

                    .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))
                    .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                    .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                    .Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                    .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))
                    .NomMoneda = CStr(IIf(IsDBNull(lector("MonedaDestino")) = True, "", lector("MonedaDestino")))
                    .TotalAPagar = CDec(IIf(IsDBNull(lector("MontoAnticipoEq")) = True, 0, lector("MontoAnticipoEq")))
                    .NomEstadoDocumento = CStr(IIf(IsDBNull(lector("edoc_Nombre")) = True, "", lector("edoc_Nombre")))
                    .NroDocumento = .Serie + " - " + .Codigo
                End With
                listaDocumento.Add(objDocumento)
            End While
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return listaDocumento

    End Function





    Public Sub DocumentoFacturacion_DeshacerMov(ByVal IdDocumento As Integer, ByVal DeleteRelacionDocumento As Boolean, ByVal DeleteAnexoDocumento As Boolean, ByVal DeleteDetalleDocumento As Boolean, ByVal DeleteAnexoDetalleDocumento As Boolean, ByVal DeleteMovAlmacen As Boolean, ByVal DeletePuntoPartida As Boolean, ByVal DeletePuntoLlegada As Boolean, ByVal DeleteDetalleConcepto As Boolean, ByVal DeleteMovCaja As Boolean, ByVal DeletePagoCaja As Boolean, ByVal DeleteObservacion As Boolean, ByVal DeleteMontoRegimen As Boolean, ByVal DeleteMovCuenta As Boolean, ByVal DeleteDetalleDocumentoR As Boolean, ByVal Anular As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Dim param() As SqlParameter = New SqlParameter(15) {}
        param(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        param(0).Value = IdDocumento
        param(1) = New SqlParameter("@DeleteRelacionDocumento", SqlDbType.Int)
        param(1).Value = DeleteRelacionDocumento
        param(2) = New SqlParameter("@DeleteAnexoDocumento", SqlDbType.Int)
        param(2).Value = DeleteAnexoDocumento
        param(3) = New SqlParameter("@DeleteDetalleDocumento", SqlDbType.Int)
        param(3).Value = DeleteDetalleDocumento
        param(4) = New SqlParameter("@DeleteAnexoDetalleDocumento", SqlDbType.Int)
        param(4).Value = DeleteAnexoDetalleDocumento
        param(5) = New SqlParameter("@DeleteMovAlmacen", SqlDbType.Int)
        param(5).Value = DeleteMovAlmacen
        param(6) = New SqlParameter("@DeletePuntoPartida", SqlDbType.Int)
        param(6).Value = DeletePuntoPartida
        param(7) = New SqlParameter("@DeletePuntoLlegada", SqlDbType.Int)
        param(7).Value = DeletePuntoLlegada
        param(8) = New SqlParameter("@DeleteDetalleConcepto", SqlDbType.Int)
        param(8).Value = DeleteDetalleConcepto
        param(9) = New SqlParameter("@DeleteMovCaja", SqlDbType.Int)
        param(9).Value = DeleteMovCaja
        param(10) = New SqlParameter("@DeletePagoCaja", SqlDbType.Int)
        param(10).Value = DeletePagoCaja
        param(11) = New SqlParameter("@DeleteObservacion", SqlDbType.Int)
        param(11).Value = DeleteObservacion
        param(12) = New SqlParameter("@DeleteMontoRegimen", SqlDbType.Int)
        param(12).Value = DeleteMontoRegimen
        param(13) = New SqlParameter("@DeleteMovCuenta", SqlDbType.Int)
        param(13).Value = DeleteMovCuenta
        param(14) = New SqlParameter("@Anular", SqlDbType.Int)
        param(14).Value = Anular
        param(15) = New SqlParameter("@DeleteDetalleDocumentoR", SqlDbType.Int)
        param(15).Value = DeleteDetalleDocumentoR

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoFacturacion_DeshacerMov", param)

    End Sub






    Public Function DocumentoFacturacionSelectPrintxIds(ByVal IdDocumentoInicio As Integer, ByVal IdDocumentoFin As Integer, ByVal IdSerie As Integer) As List(Of Entidades.Documento)

        Dim lista As New List(Of Entidades.Documento)
        Try

            cn = objConexion.ConexionSIGE
            cn.Open()

            cmd = New SqlCommand("_DocumentoFacturacionSelectPrintxIds", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumentoInicio", IdDocumentoInicio)
            cmd.Parameters.AddWithValue("@IdDocumentoFin", IdDocumentoFin)
            cmd.Parameters.AddWithValue("@IdSerie", IdSerie)

            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            While (lector.Read)

                Dim objDocumento As New Entidades.Documento

                With objDocumento

                    .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                    .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                    .NroDocumento = CStr(IIf(IsDBNull(lector("NroDocumento")) = True, "", lector("NroDocumento")))
                    .Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                    .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))
                    .NomMoneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")) = True, "", lector("mon_Simbolo")))
                    .Total = CDec(IIf(IsDBNull(lector("doc_Total")) = True, 0, lector("doc_Total")))
                    .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")) = True, 0, lector("doc_TotalAPagar")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(lector("IdEstadoDoc")) = True, 0, lector("IdEstadoDoc")))
                    .Percepcion = CDec(IIf(IsDBNull(lector("Percepcion")) = True, 0, lector("Percepcion")))

                End With

                lista.Add(objDocumento)

            End While
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        
        Return lista

    End Function


    Public Function MovBancoInsert_Venta(ByVal objMovBanco As Entidades.MovBancoView, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer

        Dim resul As Integer

        Dim param() As SqlParameter = New SqlParameter(12) {}
        param(0) = New SqlParameter("@IdMedioPago", SqlDbType.Int)
        param(0).Value = IIf(objMovBanco.IdMedioPago = Nothing, DBNull.Value, objMovBanco.IdMedioPago)
        param(1) = New SqlParameter("@IdBanco", SqlDbType.Int)
        param(1).Value = IIf(objMovBanco.IdBanco = Nothing, DBNull.Value, objMovBanco.IdBanco)
        param(2) = New SqlParameter("@IdCuentaBancaria", SqlDbType.Int)
        param(2).Value = IIf(objMovBanco.IdCuentaBancaria = Nothing, DBNull.Value, objMovBanco.IdCuentaBancaria)
        param(3) = New SqlParameter("@Monto", SqlDbType.Decimal)
        param(3).Value = IIf(objMovBanco.Monto = Nothing, DBNull.Value, objMovBanco.Monto)
        param(4) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        param(4).Value = IIf(objMovBanco.IdMoneda = Nothing, DBNull.Value, objMovBanco.IdMoneda)
        param(5) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        param(5).Value = IIf(objMovBanco.IdUsuario = Nothing, DBNull.Value, objMovBanco.IdUsuario)
        param(6) = New SqlParameter("@IdPersonaRef", SqlDbType.Int)
        param(6).Value = IIf(objMovBanco.IdPersonaRef = Nothing, DBNull.Value, objMovBanco.IdPersonaRef)
        param(7) = New SqlParameter("@IdCaja", SqlDbType.Int)
        param(7).Value = IIf(objMovBanco.IdCaja = Nothing, DBNull.Value, objMovBanco.IdCaja)
        param(8) = New SqlParameter("@IdTienda", SqlDbType.Int)
        param(8).Value = IIf(objMovBanco.IdTienda = Nothing, DBNull.Value, objMovBanco.IdTienda)
        param(9) = New SqlParameter("@FechaMov", SqlDbType.Date)
        param(9).Value = IIf(objMovBanco.FechaMov = Nothing, DBNull.Value, objMovBanco.FechaMov)
        param(10) = New SqlParameter("@NroOperacion", SqlDbType.VarChar)
        param(10).Value = IIf(objMovBanco.NroOperacion = Nothing, DBNull.Value, objMovBanco.NroOperacion)
        param(11) = New SqlParameter("@IdTarjeta", SqlDbType.Int)
        param(11).Value = IIf(objMovBanco.IdTarjeta = Nothing, DBNull.Value, objMovBanco.IdTarjeta)
        param(12) = New SqlParameter("@IdPost", SqlDbType.Int)
        param(12).Value = IIf(objMovBanco.IdPost = Nothing, DBNull.Value, objMovBanco.IdPost)

        resul = Convert.ToInt32(SqlHelper.ExecuteScalar(tr, CommandType.StoredProcedure, "_MovBancoInsert_Venta", param))

        Return (resul)

    End Function

End Class
