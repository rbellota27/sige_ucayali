﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



Imports System.Data.SqlClient
Imports System.Data

Public Class DAODocLetra

    Private objConexionSIGE As New DAO.Conexion

    Public Sub DocumentoLetra_DeshacerMov_Renegociar(ByVal IdDocumentoLetra As Integer, ByVal MontoRenegociado As Decimal, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Try
            Dim cmd As New SqlCommand("_DocumentoLetra_DeshacerMov_Renegociar_", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumentoLetra", IdDocumentoLetra)
            cmd.Parameters.AddWithValue("@MontoRenegociado", MontoRenegociado)
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally
        End Try

    End Sub

    Public Sub DocumentoLetraAnularxIdDocumento(ByVal IdDocumentoLetra As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Try
            Dim cmd As New SqlCommand("_DocumentoLetraAnularxIdDocumento", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumentoLetra", IdDocumentoLetra)

            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally
        End Try    

    End Sub

    Public Sub DocumentoLetra_DeshacerMovimientos(ByVal IdDocumentoLetra As Integer, ByVal DeleteMovCuenta As Boolean, ByVal DeletePagoProgramado As Boolean, ByVal DeleteObservaciones As Boolean, ByVal DeleteLetraCambio As Boolean, ByVal Anular As Boolean, ByVal DeleteRelacionDocumento As Boolean, ByVal DeleteDetalleConcepto As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction, Optional ByVal valEstado_DC As Boolean = True, Optional ByVal valEstado_Renegociado As Boolean = True, Optional ByVal valEstado_Protestado As Boolean = True)
        Try
            Dim cmd As New SqlCommand("_DocumentoLetra_DeshacerMovimientos", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumentoLetra", IdDocumentoLetra)
            cmd.Parameters.AddWithValue("@DeleteMovCuenta", DeleteMovCuenta)
            cmd.Parameters.AddWithValue("@DeletePagoProgramado", DeletePagoProgramado)
            cmd.Parameters.AddWithValue("@DeleteObservaciones", DeleteObservaciones)
            cmd.Parameters.AddWithValue("@DeleteLetraCambio", DeleteLetraCambio)
            cmd.Parameters.AddWithValue("@Anular", Anular)
            cmd.Parameters.AddWithValue("@valEstado_DC", valEstado_DC)
            cmd.Parameters.AddWithValue("@valEstado_Protestado", valEstado_Protestado)
            cmd.Parameters.AddWithValue("@valEstado_Renegociado", valEstado_Renegociado)

            cmd.Parameters.AddWithValue("@DeleteRelacionDocumento", valEstado_Renegociado)
            cmd.Parameters.AddWithValue("@DeleteDetalleConcepto", valEstado_Renegociado)

            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally
        End Try        

    End Sub

    Public Sub DocumentoLetraUpdate(ByVal objDocumentoLetra As Entidades.DocLetra, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Try
            Dim p() As SqlParameter = New SqlParameter(10) {}

            p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            p(0).Value = IIf(objDocumentoLetra.Id = Nothing, DBNull.Value, objDocumentoLetra.Id)
            p(1) = New SqlParameter("@doc_FechaEmision", SqlDbType.DateTime)
            p(1).Value = IIf(objDocumentoLetra.FechaEmision = Nothing, DBNull.Value, objDocumentoLetra.FechaEmision)
            p(2) = New SqlParameter("@doc_FechaVenc", SqlDbType.DateTime)
            p(2).Value = IIf(objDocumentoLetra.FechaVenc = Nothing, DBNull.Value, objDocumentoLetra.FechaVenc)
            p(3) = New SqlParameter("@doc_TotalAPagar", SqlDbType.Decimal)
            p(3).Value = IIf(objDocumentoLetra.TotalAPagar = Nothing, DBNull.Value, objDocumentoLetra.TotalAPagar)
            p(4) = New SqlParameter("@doc_TotalLetras", SqlDbType.Xml)
            p(4).Value = IIf(objDocumentoLetra.TotalLetras = Nothing, DBNull.Value, objDocumentoLetra.TotalLetras)
            p(5) = New SqlParameter("@IdPersona", SqlDbType.Int)
            p(5).Value = IIf(objDocumentoLetra.IdPersona = Nothing, DBNull.Value, objDocumentoLetra.IdPersona)
            p(6) = New SqlParameter("@IdUsuario", SqlDbType.Int)
            p(6).Value = IIf(objDocumentoLetra.IdUsuario = Nothing, DBNull.Value, objDocumentoLetra.IdUsuario)
            p(7) = New SqlParameter("@IdEstadoDoc", SqlDbType.Int)
            p(7).Value = IIf(objDocumentoLetra.IdEstadoDoc = Nothing, DBNull.Value, objDocumentoLetra.IdEstadoDoc)
            p(8) = New SqlParameter("@IdMoneda", SqlDbType.Int)
            p(8).Value = IIf(objDocumentoLetra.IdMoneda = Nothing, DBNull.Value, objDocumentoLetra.IdMoneda)
            p(9) = New SqlParameter("@Observaciones", SqlDbType.VarChar)
            p(9).Value = IIf(objDocumentoLetra.Observaciones = Nothing, DBNull.Value, objDocumentoLetra.Observaciones)
            p(10) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
            p(10).Value = IIf(objDocumentoLetra.IdTipoOperacion = Nothing, DBNull.Value, objDocumentoLetra.IdTipoOperacion)

            Dim cmd As New SqlCommand("_DocumentoLetraUpdate", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally
        End Try       

    End Sub

    Public Function DocumentoLetraSelectDocReferenciaxIdDocumentoLetra(ByVal IdDocumentoLetra As Integer, ByVal IdMonedaDestino As Integer) As List(Of Entidades.Documento_MovCuenta)
        Dim cn As SqlConnection = objConexionSIGE.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoLetraSelectDocReferenciaxIdDocumentoLetra", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumentoLetra)
        cmd.Parameters.AddWithValue("@IdMonedaDestino", IdMonedaDestino)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Documento_MovCuenta)
                Do While lector.Read
                    Dim obj As New Entidades.Documento_MovCuenta
                    With obj


                        .Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                        .MontoTotal = CDec(IIf(IsDBNull(lector("MontoTotal")) = True, 0, lector("MontoTotal")))
                        .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                        .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))
                        .FechaVenc = CDate(IIf(IsDBNull(lector("doc_FechaVenc")) = True, Nothing, lector("doc_FechaVenc")))
                        .NroDiasMora = CInt(IIf(IsDBNull(lector("NroDiasMora")) = True, 0, lector("NroDiasMora")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector("MonedaDestino")) = True, "", lector("MonedaDestino")))
                        .NroDocumento = .Serie + " - " + .Codigo
                        .Saldo = CDec(IIf(IsDBNull(lector("mcu_Saldo")) = True, 0, lector("mcu_Saldo")))

                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Function

    Public Function DocumentoLetraSelectCabxParams(ByVal IdDocumento As Integer, ByVal IdSerie As Integer, ByVal Codigo As Integer) As Entidades.DocLetra

        Dim cn As SqlConnection = objConexionSIGE.ConexionSIGE
        Dim objDocumentoLetra As Entidades.DocLetra = Nothing
        Try

            cn.Open()

            Dim cmd As New SqlCommand("_DocumentoLetraSelectCabxParams", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            cmd.Parameters.AddWithValue("@IdSerie", IdSerie)
            cmd.Parameters.AddWithValue("@Codigo", Codigo)

            Dim dr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            If (dr.Read) Then
                objDocumentoLetra = New Entidades.DocLetra

                With objDocumentoLetra

                    .Id = CInt(IIf(IsDBNull(dr("IdDocumento")), 0, dr("IdDocumento")))
                    .Codigo = CStr(IIf(IsDBNull(dr("doc_Codigo")), "", dr("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(dr("doc_Serie")), "", dr("doc_Serie")))
                    .FechaEmision = CDate(IIf(IsDBNull(dr("doc_FechaEmision")), Nothing, dr("doc_FechaEmision")))
                    .FechaVenc = CDate(IIf(IsDBNull(dr("doc_FechaVenc")), Nothing, dr("doc_FechaVenc")))
                    .TotalAPagar = CDec(IIf(IsDBNull(dr("doc_TotalAPagar")), 0, dr("doc_TotalAPagar")))
                    .getPersona_Cliente.IdPersona = CInt(IIf(IsDBNull(dr("IdPersona")), 0, dr("IdPersona")))
                    .getPersona_Cliente.Descripcion = CStr(IIf(IsDBNull(dr("DescPersona")), "", dr("DescPersona")))
                    .getPersona_Cliente.Ruc = CStr(IIf(IsDBNull(dr("RUC_Persona")), "", dr("RUC_Persona")))
                    .getPersona_Cliente.Dni = CStr(IIf(IsDBNull(dr("DNI_Persona")), "", dr("DNI_Persona")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(dr("IdEstadoDoc")), 0, dr("IdEstadoDoc")))
                    .IdMoneda = CInt(IIf(IsDBNull(dr("IdMoneda")), 0, dr("IdMoneda")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(dr("IdTipoOperacion")), 0, dr("IdTipoOperacion")))
                    .IdTienda = CInt(IIf(IsDBNull(dr("IdTienda")), 0, dr("IdTienda")))
                    .IdSerie = CInt(IIf(IsDBNull(dr("IdSerie")), 0, dr("IdSerie")))
                    .IdEmpresa = CInt(IIf(IsDBNull(dr("IdEmpresa")), 0, dr("IdEmpresa")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(dr("IdTipoDocumento")), 0, dr("IdTipoDocumento")))
                    .IdEstadoCancelacion = CInt(IIf(IsDBNull(dr("IdEstadoCan")), 0, dr("IdEstadoCan")))
                    .NomMoneda = CStr(IIf(IsDBNull(dr("mon_Simbolo")), "", dr("mon_Simbolo")))
                    .Observaciones = CStr(IIf(IsDBNull(dr("ob_Observacion")), "", dr("ob_Observacion")))
                    .getPersona_Aval.IdPersona = CInt(IIf(IsDBNull(dr("IdAval")), 0, dr("IdAval")))
                    .getPersona_Aval.Descripcion = CStr(IIf(IsDBNull(dr("DescAval")), "", dr("DescAval")))
                    .getPersona_Aval.Ruc = CStr(IIf(IsDBNull(dr("RUC_Aval")), "", dr("RUC_Aval")))
                    .getPersona_Aval.Dni = CStr(IIf(IsDBNull(dr("DNI_Aval")), "", dr("DNI_Aval")))
                    .getLetraCambio.IdBanco = CInt(IIf(IsDBNull(dr("IdBanco")), 0, dr("IdBanco")))
                    .getLetraCambio.IdCuentaBancaria = CInt(IIf(IsDBNull(dr("IdCuentaBancaria")), 0, dr("IdCuentaBancaria")))
                    .getLetraCambio.IdOficina = CInt(IIf(IsDBNull(dr("IdOficina")), 0, dr("IdOficina")))
                    .getLetraCambio.DebitoCuenta = CBool(IIf(IsDBNull(dr("lc_DebitoCuenta")), 0, dr("lc_DebitoCuenta")))
                    .getLetraCambio.Renegociado = CBool(IIf(IsDBNull(dr("lc_Renegociado")), 0, dr("lc_Renegociado")))
                    .getLetraCambio.Protestado = CBool(IIf(IsDBNull(dr("lc_Protestado")), 0, dr("lc_Protestado")))
                    .getLetraCambio.lc_EnCartera = CBool(IIf(IsDBNull(dr("lc_EnCartera")), False, dr("lc_EnCartera")))
                    .getLetraCambio.lc_EnCobranza = CBool(IIf(IsDBNull(dr("lc_EnCobraza")), False, dr("lc_EnCobraza")))

                    .IdCondicionPago = CInt(IIf(IsDBNull(dr("IdCondicionPago")), 0, dr("IdCondicionPago")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(dr("IdTipoOperacion")), 0, dr("IdTipoOperacion")))
                    .ContAmortizaciones = CInt(IIf(IsDBNull(dr("ContAmortizaciones")), 0, dr("ContAmortizaciones")))

                    .FechaPago = CDate(IIf(IsDBNull(dr("FechaPago")), Nothing, dr("FechaPago")))
                    .getLetraCambio.NroOperacion = CStr(IIf(IsDBNull(dr("lc_NroOperacion")), "", dr("lc_NroOperacion")))
                    .NroDocumento = .Serie + " - " + .Codigo

                End With

            Else

                Throw New Exception("El Documento NO EXISTE.")

            End If

            dr.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumentoLetra

    End Function

    Public Sub DocumentoLetra_EditarLetraxConsulta(ByVal IdDocumento As Integer, ByVal LetraCancelada As Boolean, ByVal DebitoCuenta As Boolean, ByVal Protestado As Boolean, ByVal FechaPgo As Date, ByVal nroOperacion As String, ByVal Usuario As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Try
            Dim cmd As New SqlCommand("_DocumentoLetra_EditarLetraxConsultaV1", cn, tr)

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            cmd.Parameters.AddWithValue("@LetraCancelada", LetraCancelada)
            cmd.Parameters.AddWithValue("@DebitoCuenta", DebitoCuenta)
            cmd.Parameters.AddWithValue("@Protestado", Protestado)

            Dim param As New SqlParameter("@FechaPgo", SqlDbType.DateTime)
            param.Value = IIf(FechaPgo = Nothing, DBNull.Value, FechaPgo)

            Dim param1 As New SqlParameter("@NroOperacion", SqlDbType.VarChar)
            param1.Value = IIf(nroOperacion = Nothing, DBNull.Value, nroOperacion)

            cmd.Parameters.AddWithValue("@Usuario", Usuario)

            cmd.Parameters.Add(param)
            cmd.Parameters.Add(param1)
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
        End Try

    End Sub

    Public Sub DocumentoLetra_VerificarEstadoCancelacionDocReferencia(ByVal IdDocumentoLetra As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Try
            Dim cmd As New SqlCommand("_DocumentoLetra_VerificarEstadoCancelacionDocReferencia", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumentoLetra", IdDocumentoLetra)            
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    Public Function DocumentoLetraConsultarLetrasxIdEstadoCancelacion(ByVal IdPersona As Integer, ByVal IdEstadoCancelacion As Integer, ByVal Top As Integer) As List(Of Entidades.DocLetra)
        Dim cn As SqlConnection = objConexionSIGE.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoLetraConsultarLetrasxIdEstadoCancelacion", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdEstadoCancelacion", IdEstadoCancelacion)
        cmd.Parameters.AddWithValue("@Top", Top)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DocLetra)
                Do While lector.Read
                    Dim obj As New Entidades.DocLetra
                    With obj
                        .Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                        .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                        .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))
                        .FechaVenc = CDate(IIf(IsDBNull(lector("doc_FechaVenc")) = True, Nothing, lector("doc_FechaVenc")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")) = True, "", lector("mon_Simbolo")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")) = True, 0, lector("doc_TotalAPagar")))
                        .NomEstadoCancelacion = CStr(IIf(IsDBNull(lector("ecan_Nombre")) = True, "", lector("ecan_Nombre")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector("edoc_Nombre")) = True, "", lector("edoc_Nombre")))
                        .IdEstadoCancelacion = CInt(IIf(IsDBNull(lector("IdEstadoCan")) = True, 0, lector("IdEstadoCan")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector("IdEstadoDoc")) = True, 0, lector("IdEstadoDoc")))
                        .IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")) = True, 0, lector("IdPersona")))
                        .FechaPago = CDate(IIf(IsDBNull(lector("pp_FechaPago")) = True, Nothing, lector("pp_FechaPago")))
                        .DebitoCuenta = CBool(IIf(IsDBNull(lector("lc_DebitoCuenta")) = True, 0, lector("lc_DebitoCuenta")))
                        .Renegociado = CBool(IIf(IsDBNull(lector("lc_Renegociado")) = True, 0, lector("lc_Renegociado")))
                        .Protestado = CBool(IIf(IsDBNull(lector("lc_Protestado")) = True, 0, lector("lc_Protestado")))
                        .DescDebitoCuenta = CStr(IIf(IsDBNull(lector("DescDebitoCuenta")) = True, "", lector("DescDebitoCuenta")))
                        .DescProtestado = CStr(IIf(IsDBNull(lector("DescProtestado")) = True, "", lector("DescProtestado")))
                        .DescRenegociado = CStr(IIf(IsDBNull(lector("DescRenegociado")) = True, "", lector("DescRenegociado")))
                        .IdMovCuenta = CInt(IIf(IsDBNull(lector("IdMovCuenta")) = True, 0, lector("IdMovCuenta")))
                        .MontoMovCuenta = CDec(IIf(IsDBNull(lector("mcu_Monto")) = True, 0, lector("mcu_Monto")))
                        .SaldoMovCuenta = CDec(IIf(IsDBNull(lector("mcu_Saldo")) = True, 0, lector("mcu_Saldo")))

                    End With

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Sub DocumentoLetra_CanjearDocReferenciaxLetra(ByVal IdMovCuentaDocRef As Integer, ByVal IdDocumentoRef As Integer, ByVal IdDocumentoLetra As Integer, ByVal AbonoxLetra As Decimal, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Try
            Dim cmd As New SqlCommand("_DocumentoLetra_CanjearDocReferenciaxLetra", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0

            cmd.Parameters.AddWithValue("@IdMovCuentaDocRef", IdMovCuentaDocRef)
            cmd.Parameters.AddWithValue("@IdDocumentoRef", IdDocumentoRef)
            cmd.Parameters.AddWithValue("@IdDocumentoLetra", IdDocumentoLetra)
            cmd.Parameters.AddWithValue("@Monto", AbonoxLetra)

            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally            
        End Try

    End Sub

    Public Function DocumentoLetraInsert(ByVal objDocLetra As Entidades.DocLetra, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer
        Try
            Dim p() As SqlParameter = New SqlParameter(18) {}

            p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            p(0).Direction = ParameterDirection.Output

            p(1) = New SqlParameter("@doc_Serie", SqlDbType.VarChar)
            p(1).Value = IIf(objDocLetra.Serie = Nothing, DBNull.Value, objDocLetra.Serie)

            p(2) = New SqlParameter("@doc_Codigo", SqlDbType.VarChar)
            p(2).Value = IIf(objDocLetra.Codigo = Nothing, DBNull.Value, objDocLetra.Codigo)

            p(3) = New SqlParameter("@doc_FechaEmision", SqlDbType.DateTime)
            p(3).Value = IIf(objDocLetra.FechaEmision = Nothing, DBNull.Value, objDocLetra.FechaEmision)

            p(4) = New SqlParameter("@doc_FechaVenc", SqlDbType.DateTime)
            p(4).Value = IIf(objDocLetra.FechaVenc = Nothing, DBNull.Value, objDocLetra.FechaVenc)

            p(5) = New SqlParameter("@doc_TotalAPagar", SqlDbType.Decimal)
            p(5).Value = IIf(objDocLetra.TotalAPagar = Nothing, DBNull.Value, objDocLetra.TotalAPagar)

            p(6) = New SqlParameter("@IdPersona", SqlDbType.Int)
            p(6).Value = IIf(objDocLetra.IdPersona = Nothing, DBNull.Value, objDocLetra.IdPersona)

            p(7) = New SqlParameter("@IdUsuario", SqlDbType.Int)
            p(7).Value = IIf(objDocLetra.IdUsuario = Nothing, DBNull.Value, objDocLetra.IdUsuario)

            p(8) = New SqlParameter("@IdEstadoDoc", SqlDbType.Int)
            p(8).Value = IIf(objDocLetra.IdEstadoDoc = Nothing, DBNull.Value, objDocLetra.IdEstadoDoc)

            p(9) = New SqlParameter("@IdCondicionPago", SqlDbType.Int)
            p(9).Value = IIf(objDocLetra.IdCondicionPago = Nothing, DBNull.Value, objDocLetra.IdCondicionPago)

            p(10) = New SqlParameter("@IdMoneda", SqlDbType.Int)
            p(10).Value = IIf(objDocLetra.IdMoneda = Nothing, DBNull.Value, objDocLetra.IdMoneda)

            p(11) = New SqlParameter("@IdTienda", SqlDbType.Int)
            p(11).Value = IIf(objDocLetra.IdTienda = Nothing, DBNull.Value, objDocLetra.IdTienda)

            p(12) = New SqlParameter("@IdSerie", SqlDbType.Int)
            p(12).Value = IIf(objDocLetra.IdSerie = Nothing, DBNull.Value, objDocLetra.IdSerie)

            p(13) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
            p(13).Value = IIf(objDocLetra.IdEmpresa = Nothing, DBNull.Value, objDocLetra.IdEmpresa)

            p(14) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
            p(14).Value = IIf(objDocLetra.IdTipoDocumento = Nothing, DBNull.Value, objDocLetra.IdTipoDocumento)


            p(15) = New SqlParameter("@IdEstadoCan", SqlDbType.Int)
            p(15).Value = IIf(objDocLetra.IdEstadoCancelacion = Nothing, DBNull.Value, objDocLetra.IdEstadoCancelacion)

            p(16) = New SqlParameter("@doc_TotalLetras", SqlDbType.Xml)
            p(16).Value = IIf(objDocLetra.TotalLetras = Nothing, DBNull.Value, objDocLetra.TotalLetras)

            p(17) = New SqlParameter("@Observaciones", SqlDbType.VarChar)
            p(17).Value = IIf(objDocLetra.Observaciones = Nothing, DBNull.Value, objDocLetra.Observaciones)

            p(18) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
            p(18).Value = IIf(objDocLetra.IdTipoOperacion = Nothing, DBNull.Value, objDocLetra.IdTipoOperacion)


            Dim cmd As New SqlCommand("_DocumentoLetraInsert", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            cmd.ExecuteNonQuery()
            Return CInt(cmd.Parameters("@IdDocumento").Value)
        Catch ex As Exception
            Throw ex
        Finally

        End Try        
    End Function

    Public Function DocumentoLetraSelectCabtxId_Print(ByVal IdDocumentoLetra As Integer) As Entidades.DocLetra

        Dim cn As SqlConnection = objConexionSIGE.ConexionSIGE
        Dim objDocLetra As Entidades.DocLetra = Nothing

        Try

            Dim cmd As New SqlCommand("_DocumentoLetraSelectCabtxId_Print", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumentoLetra", IdDocumentoLetra)

            cn.Open()

            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If (lector.Read) Then

                objDocLetra = New Entidades.DocLetra
                With objDocLetra

                    .Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                    .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                    .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))

                    .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))

                    .FechaVenc = CDate(IIf(IsDBNull(lector("doc_FechaVenc")) = True, Nothing, lector("doc_FechaVenc")))
                    .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")) = True, 0, lector("doc_TotalAPagar")))
                    .IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")) = True, 0, lector("IdMoneda")))
                    .NomMoneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")) = True, "", lector("mon_Simbolo")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector("IdTipoDocumento")) = True, 0, lector("IdTipoDocumento")))
                    .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))

                End With

            Else

                Throw New Exception("El Documento Letra con IdDocumento = " + CStr(IdDocumentoLetra) + " no existe.")

            End If

            lector.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocLetra

    End Function

    Public Function Anexar_Letra(ByVal IdDocumentoLetra As Integer) As List(Of Entidades.DocLetra)

        Dim cn As SqlConnection = objConexionSIGE.ConexionSIGE
        Dim objDocLetra As Entidades.DocLetra = Nothing
        Dim ListaDocLetra As List(Of Entidades.DocLetra)


        Try

            Dim cmd As New SqlCommand("_Anexar_Letra", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumentoLetra", IdDocumentoLetra)

            cn.Open()

            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
         
            ListaDocLetra = New List(Of Entidades.DocLetra)

            Do While lector.Read

                objDocLetra = New Entidades.DocLetra
                With objDocLetra

                    .Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))

                End With
                ListaDocLetra.Add(objDocLetra)

            Loop

            lector.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return ListaDocLetra

    End Function


    Public Function getDataSet_CR_DocumentoLetraCab(ByVal IdDocumento As Integer) As DataSet
        Dim cn As New SqlConnection
        cn = objConexionSIGE.ConexionSIGE
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As New SqlCommand("_CR_DocumentoLetraCab", objConexionSIGE.ConexionSIGE)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

        Try

            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "cabecera")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function


End Class
