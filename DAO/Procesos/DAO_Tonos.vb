﻿Imports System.Data.SqlClient
Public Class DAO_Tonos
    Dim objConexion As New Conexion

    Public Function SelectUnidadMedidaxProducto(ByVal prod_codigo As String, ByVal cn As SqlConnection) As List(Of Entidades.UnidadMedida)
        Dim lista As List(Of Entidades.UnidadMedida) = Nothing
        Using cmd As New SqlCommand("SP_UNIDADMEDIDA_X_PRODUCTO", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@prod_codigo", prod_codigo)
            End With
            Dim lector As SqlDataReader
            Try
                cn.Open()
                lector = cmd.ExecuteReader(Data.IsolationLevel.Serializable)
                If lector IsNot Nothing Then
                    lista = New List(Of Entidades.UnidadMedida)
                    Dim unidadMedida As Integer = lector.GetOrdinal("um_NombreCorto")
                    Dim equivalencia As Integer = lector.GetOrdinal("pum_Equivalencia")
                    Dim estado As Integer = lector.GetOrdinal("pum_UnidadPrincipal")
                    Dim objUnidadMedida As New Entidades.UnidadMedida
                    While lector.Read()
                        objUnidadMedida = New Entidades.UnidadMedida()
                        With objUnidadMedida
                            .DescripcionCorto = lector.GetString(unidadMedida)
                            .pumEquivalencia = lector.GetDecimal(equivalencia)
                            .Estado = lector.GetBoolean(estado)
                        End With
                        lista.Add(objUnidadMedida)
                    End While
                    lector.Close()
                End If
                Return lista
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    Public Function SelectTonosxProducto_Editar(ByVal idDocumento As Integer, ByVal idDetalleDocumento As Integer, _
                                         ByVal cn As SqlConnection) As List(Of Entidades.be_tonoXProducto)
        Dim lista As List(Of Entidades.be_tonoXProducto) = Nothing
        Using cmd As New SqlCommand("SP_SELECT_TONO_X_PRODUCTO_EDITAR", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@idDocumento", idDocumento)
                .Parameters.AddWithValue("@idDetalleDocumento", idDetalleDocumento)
            End With
            Dim lector As SqlDataReader
            Try
                cn.Open()
                lector = cmd.ExecuteReader(Data.IsolationLevel.Serializable)
                If lector IsNot Nothing Then
                    lista = New List(Of Entidades.be_tonoXProducto)
                    Dim idTono As Integer = lector.GetOrdinal("idTono")
                    Dim idProductosss As Integer = lector.GetOrdinal("idproducto")
                    Dim nomTono As Integer = lector.GetOrdinal("NOM_TONO")
                    Dim cantidad As Integer = lector.GetOrdinal("cantidad")
                    Dim objTonos As New Entidades.be_tonoXProducto
                    While lector.Read()
                        objTonos = New Entidades.be_tonoXProducto()
                        objTonos.idTono = lector.GetInt32(idTono)
                        objTonos.idProducto = lector.GetInt32(idProductosss)
                        objTonos.nomtono = lector.GetString(nomTono)
                        objTonos.cantidadTono = lector.GetDecimal(cantidad)
                        lista.Add(objTonos)
                    End While
                    lector.Close()
                End If
                Return lista
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    Public Function SelectTonosxProducto_Nuevo(ByVal idProducto As Integer, ByVal idAlmacen As Integer, idDocumento As Integer, _
                                         ByVal cn As SqlConnection) As List(Of Entidades.be_tonoXProducto)
        Dim lista As List(Of Entidades.be_tonoXProducto) = Nothing
        Using cmd As New SqlCommand("SP_SELECT_TONO_X_PRODUCTO_NUEVO_V2", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@ID_PRODUCTO", idProducto)
                .Parameters.AddWithValue("@ID_ALMACEN", idAlmacen)
                .Parameters.AddWithValue("@idDocumento", idDocumento)
            End With
            Dim lector As SqlDataReader
            Try
                cn.Open()
                lector = cmd.ExecuteReader(Data.IsolationLevel.Serializable)
                If lector IsNot Nothing Then
                    lista = New List(Of Entidades.be_tonoXProducto)
                    Dim idTono As Integer = lector.GetOrdinal("idTono")
                    Dim idSector As Integer = lector.GetOrdinal("idsector")
                    Dim idProductosss As Integer = lector.GetOrdinal("idproducto")
                    Dim idAlmacennn As Integer = lector.GetOrdinal("idalmacen")
                    Dim nomTono As Integer = lector.GetOrdinal("NOM_TONO")
                    Dim descTono As Integer = lector.GetOrdinal("DESC_TONO")
                    Dim cantidad As Integer = lector.GetOrdinal("cantidad")
                    Dim objTonos As New Entidades.be_tonoXProducto
                    While lector.Read()
                        objTonos = New Entidades.be_tonoXProducto()
                        objTonos.idTono = lector.GetInt32(idTono)
                        objTonos.idSector = lector.GetInt32(idSector)
                        objTonos.idProducto = lector.GetInt32(idProductosss)
                        objTonos.idAlmacen = lector.GetInt32(idAlmacennn)
                        objTonos.nomtono = lector.GetString(nomTono)
                        objTonos.descTono = lector.GetString(descTono)
                        objTonos.cantidadTono = lector.GetDecimal(cantidad)
                        lista.Add(objTonos)
                    End While
                    lector.Close()
                End If

                Return lista
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    Public Function DAO_ListarTono(ByVal cn As SqlConnection, ByVal idProducto As Integer, ByVal idDocumentoRef As Integer, ByVal idUnidadMedida As Integer) As List(Of Entidades.be_tonoXProducto)
        Dim lista As List(Of Entidades.be_tonoXProducto) = Nothing
        Using cmd As New SqlCommand("SP_SELECT_TONO_X_PRODUCTO_V2")
            With cmd
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@ID_PRODUCTO", SqlDbType.Int, 200, ParameterDirection.Input)).Value = idProducto
                .Parameters.Add(New SqlParameter("@ID_DOCUMENTO", SqlDbType.Int, 50, ParameterDirection.Input)).Value = idDocumentoRef
                .Parameters.Add(New SqlParameter("@ID_UNIDADMEDIDA", SqlDbType.Int, 50, ParameterDirection.Input)).Value = idUnidadMedida
            End With
            Dim lector As SqlDataReader
            Try
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.SingleResult)
                If lector IsNot Nothing Then
                    lista = New List(Of Entidades.be_tonoXProducto)
                    Dim idTono As Integer = lector.GetOrdinal("ID_REL_TONO_PROD")
                    Dim nom_tono As Integer = lector.GetOrdinal("NOM_TONO")
                    Dim cantidad As Integer = lector.GetOrdinal("cantidad")
                    Dim objSector As Entidades.be_tonoXProducto
                    While lector.Read()
                        objSector = New Entidades.be_tonoXProducto()
                        With objSector
                            .idTono = lector.GetInt32(idTono)
                            .nomtono = lector.GetString(nom_tono)
                            .cantidadTono = lector.GetDecimal(cantidad)
                            lista.Add(objSector)
                        End With
                    End While
                    lector.Close()
                End If
                Return lista
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    Public Function SelectTonosxProducto_Editar_GuiaRecepcion(ByVal idDocumento As Integer, ByVal cn As SqlConnection) As List(Of Entidades.be_tonoXProducto)
        Dim lista As List(Of Entidades.be_tonoXProducto) = Nothing
        Using cmd As New SqlCommand("SP_DROPDOWNLIST_FILTRO", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@FITLRO", idDocumento)
                .Parameters.AddWithValue("@TABLA", "TBL_DISTRIB_SALDO_KARDEX_DETALLE_V2")
            End With
            Dim lector As SqlDataReader
            Try
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.SingleResult)
                If lector IsNot Nothing Then
                    lista = New List(Of Entidades.be_tonoXProducto)
                    Dim idTono As Integer = lector.GetOrdinal("idTono")
                    Dim nomTono As Integer = lector.GetOrdinal("NOM_TONO")
                    Dim cantidadSaldo As Integer = lector.GetOrdinal("CantidadSaldo")
                    Dim objTonos As New Entidades.be_tonoXProducto
                    While lector.Read()
                        objTonos = New Entidades.be_tonoXProducto()
                        objTonos.idTono = lector.GetInt32(idTono)
                        objTonos.nomtono = lector.GetString(nomTono)
                        objTonos.cantidadTono = lector.GetDecimal(cantidadSaldo)
                        lista.Add(objTonos)
                    End While
                    lector.Close()
                End If
                Return lista
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    Public Function ListarTonosxProducto(ByVal idProducto As Integer, ByVal cn As SqlConnection) As List(Of Entidades.be_tonoXProducto)
        Dim lista As List(Of Entidades.be_tonoXProducto) = Nothing
        Using cmd As New SqlCommand("SP_DROPDOWNLIST_FILTRO", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@FITLRO", idProducto)
                .Parameters.AddWithValue("@TABLA", "LISTAR_TONO_X_PRODUCTO")
            End With
            Dim lector As SqlDataReader
            Try
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.SingleResult)
                If lector IsNot Nothing Then
                    lista = New List(Of Entidades.be_tonoXProducto)
                    Dim idTono As Integer = lector.GetOrdinal("ID_REL_TONO_PROD")
                    Dim nomTono As Integer = lector.GetOrdinal("NOM_TONO")
                    Dim objTonos As New Entidades.be_tonoXProducto
                    While lector.Read()
                        objTonos = New Entidades.be_tonoXProducto()
                        objTonos.idTono = lector.GetInt32(idTono)
                        objTonos.nomtono = lector.GetString(nomTono)
                        lista.Add(objTonos)
                    End While
                    lector.Close()
                End If
                Return lista
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    Public Function ListarTonosxProducto_y_cantidades(ByVal idProducto As Integer, ByVal idAlmacen As Integer, ByVal cn As SqlConnection) As List(Of Entidades.be_tonoXProducto)
        Dim lista As List(Of Entidades.be_tonoXProducto) = Nothing
        Using cmd As New SqlCommand("SP_LISTA_TONO_CANTIDAD_X_PRODUCTO", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@idProducto", idProducto)
                .Parameters.AddWithValue("@idAlmacen", idAlmacen)
            End With
            Dim lector As SqlDataReader
            Try
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.SingleResult)
                If lector IsNot Nothing Then
                    lista = New List(Of Entidades.be_tonoXProducto)
                    Dim idTono As Integer = lector.GetOrdinal("ID_REL_TONO_PROD")
                    Dim nomTono As Integer = lector.GetOrdinal("NOM_TONO")
                    Dim objTonos As New Entidades.be_tonoXProducto
                    While lector.Read()
                        objTonos = New Entidades.be_tonoXProducto()
                        objTonos.idTono = lector.GetInt32(idTono)
                        objTonos.nomtono = lector.GetString(nomTono)
                        lista.Add(objTonos)
                    End While
                    lector.Close()
                End If
                Return lista
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    Public Function eliminarTono(ByVal idProducto As Integer, ByVal idTono As Integer, ByVal idAlmacen As Integer, ByVal cn As SqlConnection)

        Using cmd As New SqlCommand("SP_ELIMINAR_TONO", cn)
            Dim retorno As Integer
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add(New SqlParameter("@idProducto", DbType.Int32)).Value = idProducto
            cmd.Parameters.Add(New SqlParameter("@idTono", DbType.Int32)).Value = idTono
            cmd.Parameters.Add(New SqlParameter("@idAlmacen", DbType.Int32)).Value = idAlmacen
            Try
                cn.Open()
                retorno = cmd.ExecuteNonQuery()
            Catch ex As Exception
                Throw ex
            End Try
            Return retorno
        End Using
    End Function
End Class
