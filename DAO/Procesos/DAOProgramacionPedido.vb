﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.






'************************   JUEVES 15 ABRIL 01_09 PM









Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class DAOProgramacionPedido
    Dim objConexion As New DAO.Conexion
    Private cn As SqlConnection
    Private reader As SqlDataReader
    Private tr As SqlTransaction

    Public Function ProgramacionPedidoSelectxIdYearxIdSemana(ByVal IdYear As Integer, ByVal IdSemana As Integer) As Entidades.ProgramacionPedido

        cn = (New DAO.Conexion).ConexionSIGE
        Dim objProgramacionPedido As Entidades.ProgramacionPedido = Nothing
        Try

            Dim p() As SqlParameter = New SqlParameter(1) {}
            p(0) = New SqlParameter("@IdYear", SqlDbType.Int)
            p(0).Value = IdYear
            p(1) = New SqlParameter("@IdSemana", SqlDbType.Int)
            p(1).Value = IdSemana

            cn.Open()
            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_ProgramacionPedidoSelectxIdYearxIdSemana", p)
            If (reader.Read) Then

                objProgramacionPedido = New Entidades.ProgramacionPedido

                With objProgramacionPedido
                    .IdSemana = CInt(IIf(IsDBNull(reader("IdSemana")) = True, 0, reader("IdSemana")))
                    .IdYear = CInt(IIf(IsDBNull(reader("IdYear")) = True, 0, reader("IdYear")))
                    .cal_Mes = CInt(IIf(IsDBNull(reader("cal_Mes")) = True, 0, reader("cal_Mes")))
                    .cal_FechaIni = CDate(IIf(IsDBNull(reader("cal_FechaIni")) = True, Nothing, reader("cal_FechaIni")))
                    .cal_FechaFin = CDate(IIf(IsDBNull(reader("cal_FechaFin")) = True, Nothing, reader("cal_FechaFin")))
                    .cal_DiasUtil = CInt(IIf(IsDBNull(reader("cal_DiasUtil")) = True, 0, reader("cal_DiasUtil")))
                    .cal_Estado = CBool(IIf(IsDBNull(reader("cal_Estado")) = True, 0, reader("cal_Estado")))
                End With

            End If
            reader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return objProgramacionPedido

    End Function

    Public Function ProgramacionPedidoSelectxFecha(ByVal fecha As Date) As Entidades.ProgramacionPedido

        cn = (New DAO.Conexion).ConexionSIGE
        Dim objProgramacionPedido As Entidades.ProgramacionPedido = Nothing
        Try

            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = New SqlParameter("@Fecha", SqlDbType.Date)
            p(0).Value = fecha

            cn.Open()
            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_ProgramacionPedidoSelectxFecha", p)
            If (reader.Read) Then

                objProgramacionPedido = New Entidades.ProgramacionPedido

                With objProgramacionPedido
                    .IdSemana = CInt(IIf(IsDBNull(reader("IdSemana")) = True, 0, reader("IdSemana")))
                    .IdYear = CInt(IIf(IsDBNull(reader("IdYear")) = True, 0, reader("IdYear")))
                    .cal_Mes = CInt(IIf(IsDBNull(reader("cal_Mes")) = True, 0, reader("cal_Mes")))
                    .cal_FechaIni = CDate(IIf(IsDBNull(reader("cal_FechaIni")) = True, Nothing, reader("cal_FechaIni")))
                    .cal_FechaFin = CDate(IIf(IsDBNull(reader("cal_FechaFin")) = True, Nothing, reader("cal_FechaFin")))
                    .cal_DiasUtil = CInt(IIf(IsDBNull(reader("cal_DiasUtil")) = True, 0, reader("cal_DiasUtil")))
                    .cal_Estado = CBool(IIf(IsDBNull(reader("cal_Estado")) = True, 0, reader("cal_Estado")))
                End With

            End If
            reader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return objProgramacionPedido

    End Function


    Public Function ListarProgramacionPedido(ByVal idyear As Integer) As String
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ListarProgramacionPedido", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idyear", idyear)
        Try
            cn.Open()
            Return cmd.ExecuteScalar.ToString
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function InsertProgramacionPedido(ByVal obj As Entidades.ProgramacionPedido) As Boolean
        Dim parametros() As SqlParameter = New SqlParameter(2) {}
        parametros(0) = New SqlParameter("@IdYear", SqlDbType.Int)
        parametros(0).Value = IIf(obj.IdYear = Nothing, DBNull.Value, obj.IdYear)
        parametros(1) = New SqlParameter("@cal_FechaIni", SqlDbType.DateTime)
        parametros(1).Value = IIf(obj.cal_FechaIni = Nothing, DBNull.Value, obj.cal_FechaIni)
        parametros(2) = New SqlParameter("@dias", SqlDbType.Int)
        parametros(2).Value = IIf(obj.cal_DiasUtil = Nothing, DBNull.Value, obj.cal_DiasUtil)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim cmd As New SqlCommand("_InsertProgramacionPedido", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(parametros)
            cmd.ExecuteNonQuery()
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

        Return True
    End Function

    Public Function UpdateProgramacionPedido(ByVal obj As Entidades.ProgramacionPedido) As Boolean
        Dim parametros() As SqlParameter = New SqlParameter(6) {}
        parametros(0) = New SqlParameter("@IdYear", SqlDbType.Int)
        parametros(0).Value = IIf(obj.IdYear = Nothing, DBNull.Value, obj.IdYear)
        parametros(1) = New SqlParameter("@IdSemana", SqlDbType.Int)
        parametros(1).Value = IIf(obj.IdSemana = Nothing, DBNull.Value, obj.IdSemana)
        parametros(2) = New SqlParameter("@cal_Mes", SqlDbType.Int)
        parametros(2).Value = IIf(obj.cal_Mes = Nothing, DBNull.Value, obj.cal_Mes)
        parametros(3) = New SqlParameter("@cal_FechaIni", SqlDbType.DateTime)
        parametros(3).Value = IIf(obj.cal_FechaIni = Nothing, DBNull.Value, obj.cal_FechaIni)
        parametros(4) = New SqlParameter("@cal_FechaFin", SqlDbType.DateTime)
        parametros(4).Value = IIf(obj.cal_FechaFin = Nothing, DBNull.Value, obj.cal_FechaFin)
        parametros(5) = New SqlParameter("@cal_DiasUtil", SqlDbType.Int)
        parametros(5).Value = IIf(obj.cal_DiasUtil = Nothing, DBNull.Value, obj.cal_DiasUtil)
        parametros(6) = New SqlParameter("@cal_Estado", SqlDbType.Bit)
        parametros(6).Value = IIf(obj.cal_Estado = Nothing, DBNull.Value, obj.cal_Estado)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim cmd As New SqlCommand("_UpdateProgramacionPedido", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(parametros)
            cmd.ExecuteNonQuery()
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

        Return True
    End Function

    Public Function ListarIdSemanaxIdYear(ByVal idyear As Integer) As String
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ListarIdSemanaxIdYear", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idyear", idyear)
        Dim cad As String = String.Empty
        Try
            cn.Open()
            cad = cmd.ExecuteScalar.ToString
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return cad
    End Function

    Public Function ListarProgramacionMantenimiento(ByVal idyear As Integer, ByVal idsemana As Integer) As Entidades.ProgramacionPedido
        Dim obj As New Entidades.ProgramacionPedido
        Dim lector As SqlDataReader
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ListarProgramacionMantenimiento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idyear", idyear)
        cmd.Parameters.AddWithValue("@idsemana", idsemana)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If lector.Read Then
                With obj
                    .cal_Mes = CInt(IIf(IsDBNull(lector("cal_Mes")), 0, lector("cal_Mes")))
                    .cal_FechaIni = CDate(IIf(IsDBNull(lector("cal_FechaIni")), Nothing, lector("cal_FechaIni")))
                    .cal_FechaFin = CDate(IIf(IsDBNull(lector("cal_FechaFin")), Nothing, lector("cal_FechaFin")))
                    .cal_DiasUtil = CInt(IIf(IsDBNull(lector("cal_DiasUtil")), 0, lector("cal_DiasUtil")))
                    .cal_Estado = CBool(IIf(IsDBNull(lector("cal_Estado")), False, lector("cal_Estado")))
                End With
            End If
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return obj
    End Function

    Public Function ListarProgPedido_A_Documento(ByVal idyear As Integer, _
                    ByVal idTienda As Integer, ByVal idAlmacen As Integer) As String
        Dim cad$
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ListarProgPedido_A_Documento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idyear", idyear)
        cmd.Parameters.AddWithValue("@idTienda", idTienda)
        cmd.Parameters.AddWithValue("@idAlmacen", idAlmacen)
        Try
            cn.Open()
            cad = cmd.ExecuteScalar.ToString()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return cad
    End Function
#Region "LLENA  COMBO"

    Public Function GetYear() As List(Of Entidades.ProgramacionPedido)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SelectYear", cn)
        cmd.CommandType = CommandType.StoredProcedure
        'cmd.Parameters.AddWithValue("@IdCaja", IdCaja)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ProgramacionPedido)
                Do While lector.Read
                    Dim obj As New Entidades.ProgramacionPedido
                    With obj
                        .IdYear = CInt(IIf(IsDBNull(lector("idyear")) = True, 0, lector("idyear")))
                    End With

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function GetSemanaDYear() As List(Of Entidades.ProgramacionPedido)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("selectSemanaYear", cn)
        cmd.CommandType = CommandType.StoredProcedure
        'cmd.Parameters.AddWithValue("@IdCaja", IdCaja)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ProgramacionPedido)
                Do While lector.Read
                    Dim obj As New Entidades.ProgramacionPedido
                    With obj
                        .IdSemana = CInt(IIf(IsDBNull(lector("IdSemana")) = True, 0, lector("IdSemana")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try


    End Function

    Public Function ProgramacionPedidoSelectYear() As List(Of Entidades.ProgramacionPedido)

        cn = (New DAO.Conexion).ConexionSIGE
        Dim lista As New List(Of Entidades.ProgramacionPedido)
        Try

            cn.Open()
            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_ProgramacionPedidoSelectYear")
            While (reader.Read)

                Dim obj As New Entidades.ProgramacionPedido
                With obj
                    .IdYear = CInt(IIf(IsDBNull(reader("IdYear")) = True, 0, reader("IdYear")))
                End With
                lista.Add(obj)

            End While
            reader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return lista

    End Function


    Public Function ProgramacionPedidoSelectSemanaxYear(ByVal IdYear As Integer) As List(Of Entidades.ProgramacionPedido)

        cn = (New DAO.Conexion).ConexionSIGE
        Dim lista As New List(Of Entidades.ProgramacionPedido)
        Try

            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = New SqlParameter("@IdYear", SqlDbType.Int)
            p(0).Value = IdYear

            cn.Open()
            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_ProgramacionPedidoSelectSemanaxYear", p)
            While (reader.Read)

                Dim obj As New Entidades.ProgramacionPedido
                With obj
                    .IdSemana = CInt(IIf(IsDBNull(reader("IdSemana")) = True, 0, reader("IdSemana")))
                End With
                lista.Add(obj)

            End While
            reader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return lista

    End Function

#End Region


#Region "DocumentoProgramacionPedido"

    Public Function InsertDocumentoProgramacionPedido(ByVal obj As Entidades.ProgramacionPedido, _
                                        ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Try
            Dim cmd As New SqlCommand("_InsertDocumentoProgramacionPedido", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", obj.IdDocumento)
            cmd.Parameters.AddWithValue("@IdYear", obj.IdYear)
            cmd.Parameters.AddWithValue("@IdSemana", obj.IdSemana)
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return True
    End Function

    Public Function UpdateDocumentoProgramacionPedido(ByVal obj As Entidades.ProgramacionPedido, _
                                    ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Try
            Dim cmd As New SqlCommand("_UpdateDocumentoProgramacionPedido", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", obj.IdDocumento)
            cmd.Parameters.AddWithValue("@IdYear", obj.IdYear)
            cmd.Parameters.AddWithValue("@IdSemana", obj.IdSemana)
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return True
    End Function

    Public Function ListarDocProgPedidoxIdDocumento(ByVal idDocumento As Integer) As Entidades.ProgramacionPedido
        Dim obj As Entidades.ProgramacionPedido
        Dim lector As SqlDataReader
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ListarDocProgPedidoxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", idDocumento)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If lector.Read Then
                obj = New Entidades.ProgramacionPedido
                With obj
                    .IdDocumento = CInt(IIf(IsDBNull(lector("IdDocumento")), 0, lector("IdDocumento")))
                    .IdYear = CInt(IIf(IsDBNull(lector("IdYear")), 0, lector("IdYear")))
                    .IdSemana = CInt(IIf(IsDBNull(lector("IdSemana")), 0, lector("IdSemana")))
                    .CadIdsemana = CStr(IIf(IsDBNull(lector("CadIdsemana")), "", lector("CadIdsemana")))
                    .cal_Mes = CInt(IIf(IsDBNull(lector("cal_Mes")), 0, lector("cal_Mes")))
                    .cal_FechaIni = CDate(IIf(IsDBNull(lector("cal_FechaIni")), Nothing, lector("cal_FechaIni")))
                    .cal_FechaFin = CDate(IIf(IsDBNull(lector("cal_FechaFin")), Nothing, lector("cal_FechaFin")))
                    .cal_DiasUtil = CInt(IIf(IsDBNull(lector("cal_DiasUtil")), 0, lector("cal_DiasUtil")))
                End With
            End If
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return obj
    End Function

#End Region


End Class
