﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOApproveOC

    Dim objConexion As New Conexion

    Public Function OrdenCompra_MovCuentaPorPagar(ByVal obj As Entidades.Documento) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim cmd As New SqlCommand("_OrdenCompra_MovCuentaPorPagar", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", obj.Id)
            cmd.Parameters.AddWithValue("@IdUsuario", IIf(obj.IdUsuarioSupervisor = Nothing, DBNull.Value, obj.IdUsuarioSupervisor))
            cmd.Parameters.AddWithValue("@doc_FechaVenc", IIf(obj.FechaVenc = Nothing, DBNull.Value, obj.FechaVenc))
            cmd.Parameters.AddWithValue("@anex_FechaAprob", IIf(obj.FechaAprobacion = Nothing, DBNull.Value, obj.FechaAprobacion))
            cmd.Parameters.AddWithValue("@anex_Aprobar", IIf(obj.Aprobar = Nothing, False, obj.Aprobar))
            cmd.ExecuteNonQuery()
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return True
    End Function

    Public Function OrdenCompra_Approve(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdArea As Integer, _
                                        ByVal Aprobacion As Integer, ByVal IdSerie As Integer, ByVal Fechaini As String, ByVal Fechafin As String) As List(Of Entidades.Documento)
        Dim Lista As New List(Of Entidades.Documento)
        Dim lector As SqlDataReader = Nothing
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_OrdenCompra_Approve", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("IdArea", IdArea)
        cmd.Parameters.AddWithValue("Aprobacion", Aprobacion)
        cmd.Parameters.AddWithValue("@IdSerie", IdSerie)
        cmd.Parameters.AddWithValue("Fechaini", Fechaini)
        cmd.Parameters.AddWithValue("Fechafin", Fechafin)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                If lector.HasRows Then
                    Dim obj As New Entidades.Documento
                    With obj
                        .Id = CInt(lector("IdDocumento"))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")), "", lector("tdoc_NombreCorto")))
                        .NroDocumento = CStr(IIf(IsDBNull(lector("NumDocumento")), "", lector("NumDocumento")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")), Nothing, lector("doc_FechaEmision")))
                        .FechaCancelacion = CDate(IIf(IsDBNull(lector("doc_FechaCancelacion")), Nothing, lector("doc_FechaCancelacion")))
                        .FechaVenc = CDate(IIf(IsDBNull(lector("doc_FechaVenc")), Nothing, lector("doc_FechaVenc")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")), "", lector("mon_Simbolo")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")), 0, lector("doc_TotalAPagar")))
                        .DescripcionPersona = CStr(IIf(IsDBNull(lector("NomProveedor")), "", lector("NomProveedor")))
                        .Aprobar = CBool(IIf(IsDBNull(lector("anex_Aprobar")), False, lector("anex_Aprobar")))
                        .NomEmpleado = CStr(IIf(IsDBNull(lector("NomSupervisor")), "", lector("NomSupervisor")))
                        .FechaAprobacion = CDate(IIf(IsDBNull(lector("anex_FechaAprob")), Nothing, lector("anex_FechaAprob")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector("IdEmpresa")), 0, lector("IdEmpresa")))
                        .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")), 0, lector("IdTienda")))
                        .IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")), 0, lector("IdPersona")))
                        .IdUsuario = CInt(IIf(IsDBNull(lector("IdUsuario")), 0, lector("IdUsuario")))
                        .IdRemitente = CInt(IIf(IsDBNull(lector("IdRemitente")), 0, lector("IdRemitente")))
                        .IdDestinatario = CInt(IIf(IsDBNull(lector("IdDestinatario")), 0, lector("IdDestinatario")))
                        .IdDocRelacionado = CInt(IIf(IsDBNull(lector("IdDocumentoRef")), 0, lector("IdDocumentoRef")))
                    End With
                    Lista.Add(obj)
                End If
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function


    Public Function CancelarStockTransito(ByVal obj As Entidades.Documento) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim cmd As New SqlCommand("_OrdenCompra_CancelarTransito", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            cmd.Parameters.AddWithValue("@IdEmpresa", obj.IdEmpresa)
            cmd.Parameters.AddWithValue("@IdTienda", IIf(obj.IdTienda = Nothing, DBNull.Value, obj.IdTienda))
            cmd.Parameters.AddWithValue("@IdSerie", IIf(obj.IdSerie = Nothing, DBNull.Value, obj.IdSerie))
            cmd.Parameters.AddWithValue("@FechaIni", IIf(obj.FechaEmision = Nothing, DBNull.Value, obj.FechaEmision))
            cmd.Parameters.AddWithValue("@FechaFin", IIf(obj.FechaVenc = Nothing, DBNull.Value, obj.FechaVenc))
            cmd.ExecuteNonQuery()

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

        Return True
    End Function

End Class
