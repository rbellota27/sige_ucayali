﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAODocOrdenDespacho
    Private objConexion As New Conexion


    '************************* VERSION 2
    Public Sub DocumentoOrdenDespacho_DetalleInsert(ByVal cn As SqlConnection, ByVal listaDetalleDocumento As List(Of Entidades.DetalleDocumento), ByVal tr As SqlTransaction, ByVal IdDocumento As Integer, ByVal Factor As Integer, ByVal MoverStockFisico As Boolean, ByVal ValidarDespacho As Boolean)

        For i As Integer = 0 To listaDetalleDocumento.Count - 1

            Dim detalledocumento As Entidades.DetalleDocumento = listaDetalleDocumento(i)

            Dim ArrayParametros() As SqlParameter = New SqlParameter(9) {}

            ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            ArrayParametros(0).Value = IdDocumento
            ArrayParametros(1) = New SqlParameter("@IdProducto", SqlDbType.Int)
            ArrayParametros(1).Value = detalledocumento.IdProducto
            ArrayParametros(2) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
            ArrayParametros(2).Value = detalledocumento.IdUnidadMedida
            ArrayParametros(3) = New SqlParameter("@dc_Cantidad", SqlDbType.Decimal)
            ArrayParametros(3).Value = IIf(detalledocumento.Cantidad = Nothing, DBNull.Value, detalledocumento.Cantidad)
            ArrayParametros(4) = New SqlParameter("@dc_UMedida", SqlDbType.VarChar)
            ArrayParametros(4).Value = IIf(detalledocumento.UMedida = Nothing, DBNull.Value, detalledocumento.UMedida)
            ArrayParametros(5) = New SqlParameter("@IdDetalleAfecto", SqlDbType.Int)
            ArrayParametros(5).Value = IIf(detalledocumento.IdDetalleAfecto = Nothing, DBNull.Value, detalledocumento.IdDetalleAfecto)
            ArrayParametros(6) = New SqlParameter("@dc_DetalleGlosa", SqlDbType.VarChar)
            ArrayParametros(6).Value = IIf(detalledocumento.DetalleGlosa = Nothing, DBNull.Value, detalledocumento.DetalleGlosa)
            ArrayParametros(7) = New SqlParameter("@Factor", SqlDbType.Int)
            ArrayParametros(7).Value = Factor
            ArrayParametros(8) = New SqlParameter("@MoverStockFisico", SqlDbType.Bit)
            ArrayParametros(8).Value = MoverStockFisico
            ArrayParametros(9) = New SqlParameter("@ValidarDespacho", SqlDbType.Bit)
            ArrayParametros(9).Value = ValidarDespacho

            SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoGuiaRemision_DetalleInsert", ArrayParametros)

        Next

    End Sub
    Public Sub DocumentoOrdenDespacho_GenerarDespachoAutomaticoxParams(ByVal IdAlmacen As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal IdUsuario As Integer, ByVal IdTipoDocumento_OD As Integer, ByVal IdTipoOperacion_OD As Integer, ByVal IdDocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        '***  Si IdDocumento=0 Generación Masiva de acuerdo a los parámetros ingresados
        '***  Si IdDocumento<>0 Generación Puntual
        Dim cmd As New SqlCommand("_DocumentoOrdenDespacho_GenerarDespachoAutomaticoxParams", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdAlmacen", IIf(IdAlmacen = Nothing, DBNull.Value, IdAlmacen))
        cmd.Parameters.AddWithValue("@FechaInicio", IIf(FechaInicio = Nothing, DBNull.Value, FechaInicio))
        cmd.Parameters.AddWithValue("@FechaFin", IIf(FechaFin = Nothing, DBNull.Value, FechaFin))
        cmd.Parameters.AddWithValue("@IdUsuario", IIf(IdUsuario = Nothing, DBNull.Value, IdUsuario))
        cmd.Parameters.AddWithValue("@IdTipoDocumento_OD", IIf(IdTipoDocumento_OD = Nothing, DBNull.Value, IdTipoDocumento_OD))
        cmd.Parameters.AddWithValue("@IdTipoOperacion_OD", IIf(IdTipoOperacion_OD = Nothing, DBNull.Value, IdTipoOperacion_OD))
        cmd.Parameters.AddWithValue("@IdDocumento", IIf(IdDocumento = Nothing, DBNull.Value, IdDocumento))
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try



    End Sub








    '**************************** VERSION 1
    Public Sub DocumentoOrdenDespacho_GenerarDespachoAutomatico(ByVal IdAlmacen As Integer, ByVal FechaFin As Date, ByVal IdUsuario As Integer, ByVal IdTipoDocumento_OD As Integer, ByVal IdTipoOperacion_OD As Integer, ByVal IdDocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        '***  Si IdDocumento=0 Generación Masiva de acuerdo a los parámetros ingresados
        '***  Si IdDocumento<>0 Generación Puntual
        Dim cmd As New SqlCommand("_DocumentoOrdenDespacho_GenerarDespachoAutomatico", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdAlmacen", IIf(IdAlmacen = Nothing, DBNull.Value, IdAlmacen))
        cmd.Parameters.AddWithValue("@FechaFin", IIf(FechaFin = Nothing, DBNull.Value, FechaFin))
        cmd.Parameters.AddWithValue("@IdUsuario", IIf(IdUsuario = Nothing, DBNull.Value, IdUsuario))
        cmd.Parameters.AddWithValue("@IdTipoDocumento_OD", IIf(IdTipoDocumento_OD = Nothing, DBNull.Value, IdTipoDocumento_OD))
        cmd.Parameters.AddWithValue("@IdTipoOperacion_OD", IIf(IdTipoOperacion_OD = Nothing, DBNull.Value, IdTipoOperacion_OD))
        cmd.Parameters.AddWithValue("@IdDocumento", IIf(IdDocumento = Nothing, DBNull.Value, IdDocumento))
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try


    End Sub
    Public Sub DeleteDetalle_Movimiento(ByVal iddocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Dim cmd As New SqlCommand("_OrdenDespachoDeleteDetalle_Movimiento", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub
    Public Sub AnularxIdDocumento(ByVal iddocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        '***** Repone los stocks y elimina los mov almacen de la orden de despacho
        Dim cmd As New SqlCommand("_OrdenDespachoAnularxIdDocumento", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub
    Public Function SelectxIdDocumento(ByVal iddocumento As Integer) As Entidades.DocOrdenDespacho
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoOrdenDespachoCabSelectxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim obj As Entidades.DocOrdenDespacho = Nothing
                If lector.Read Then
                    obj = New Entidades.DocOrdenDespacho
                    With obj
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                        .Dni = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdSerie = CInt(IIf(IsDBNull(lector.Item("IdSerie")) = True, 0, lector.Item("IdSerie")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .NomCliente = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                        .Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                    End With
                End If
                lector.Close()
                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function ActualizaOrdenDespacho(ByVal listaDetalle As List(Of Entidades.DetalleDocumento), ByVal objDocumento As Entidades.Documento) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        cn.Open()
        Dim tr As SqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)
        Try
            Dim objDaoMovAlmacen As New DAO.DAOMovAlmacen
            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            Dim objDaoDocumento As New DAO.DAODocumento
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            Dim comprometido As Boolean = False

            '************ elimino el detalle y los mov de almacen, repongo las cant x atender
            DeleteDetalle_Movimiento(objDocumento.Id, cn, tr)

            For i As Integer = 0 To listaDetalle.Count - 1
                With listaDetalle.Item(i)
                    '********** actualizo el detalle documento
                    .CantxAtender = objDaoDetalleDocumento.SelectCantxAtenderxIdDetalleDocumento(.IdDetalleAfecto, cn, tr)
                    If Not objDaoDetalleDocumento.UpdateCantxAtenderxIdDetalleDocumento(.IdDetalleAfecto, .CantxAtender - .CantADespachar, cn, tr) Then Throw New Exception

                    '*********** obtengo la cantidad equivalente en unidad principal
                    Dim objDaoProducto As New DAO.DAOProducto
                    Dim cantidadEquivalente As Decimal = objDaoProducto.SelectEquivalenciaUMPrincipal(cn, tr, .IdProducto, .IdUnidadMedida, .CantxAtender - .CantADespachar)

                    '*********** actualizo el mov almacen con la cantidad equivalente a la Unidad Principal
                    objDaoMovAlmacen.UpdateCantxAtenderxIdDetalleDocumento(.IdDetalleDocumento, cantidadEquivalente, True, cn, tr)

                    '************* Inserto el detalle documento de la Orden de despacho
                    listaDetalle(i).Cantidad = listaDetalle(i).CantADespachar
                    listaDetalle(i).CantxAtender = Nothing
                    Dim IdDetalleDoc As Integer = objDaoDetalleDocumento.InsertaDetalleDocumento(cn, listaDetalle(i), tr)

                    '****************** Inserto el MOv almacen producido por la Orden de Despacho
                    Dim objMovAlmacen As New Entidades.MovAlmacen
                    objMovAlmacen.IdProducto = .IdProducto
                    objMovAlmacen.IdDocumento = .IdDocumento
                    objMovAlmacen.IdUnidadMedida = .IdUnidadMedida
                    objMovAlmacen.IdUMPrincipal = .IdUnidadMedidaPrincipal
                    objMovAlmacen.IdDetalleDocumento = IdDetalleDoc
                    objMovAlmacen.CantidadMov = objDaoProducto.SelectEquivalenciaUMPrincipal(cn, tr, .IdProducto, .IdUnidadMedida, .CantADespachar)
                    objMovAlmacen.Comprometido = False
                    objMovAlmacen.Factor = -1
                    objMovAlmacen.IdAlmacen = .IdAlmacen
                    objMovAlmacen.IdEmpresa = objDocumento.IdEmpresa
                    objMovAlmacen.IdMetodoV = Nothing
                    objMovAlmacen.IdTienda = objDocumento.IdTienda
                    objMovAlmacen.IdTipoOperacion = 17
                    objMovAlmacen.UMPrincipal = .UMedida
                    objDaoMovAlmacen.InsertaMovAlmacen(cn, objMovAlmacen, tr)
                End With
            Next
            tr.Commit()
            Return True
        Catch ex As Exception
            tr.Rollback()
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return False
    End Function
    Public Function InsertaOrdenDespacho(ByVal listaDetalle As List(Of Entidades.DetalleDocumento), ByVal objDocumento As Entidades.Documento, ByVal IdDocumentoOld As Integer, ByVal IdMetodoVal As Integer) As Integer
        Dim iddocumento As Integer = -1
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        cn.Open()
        Dim tr As SqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)
        Try
            Dim objDaoMovAlmacen As New DAO.DAOMovAlmacen
            Dim objDaoDetalleDocumento As New DAO.DAODetalleDocumento
            Dim objDaoDocumento As New DAO.DAODocumento
            Dim objDaoRelacionDocumento As New DAO.DAORelacionDocumento
            Dim comprometido As Boolean = False
            iddocumento = objDaoDocumento.InsertaDocumento(cn, objDocumento, tr)
            For i As Integer = 0 To listaDetalle.Count - 1
                With listaDetalle.Item(i)
                    .IdDocumento = iddocumento
                    .CantxAtender = objDaoDetalleDocumento.SelectCantxAtenderxIdDetalleDocumento(.IdDetalleDocumento, cn, tr)
                    '********** actualizo el detalle documento
                    If Not objDaoDetalleDocumento.UpdateCantxAtenderxIdDetalleDocumento(.IdDetalleDocumento, .CantxAtender - .CantADespachar, cn, tr) Then Throw New Exception

                    '*********** obtengo la cantidad equivalente en unidad principal
                    Dim objDaoProducto As New DAO.DAOProducto
                    Dim cantidadEquivalente As Decimal = objDaoProducto.SelectEquivalenciaUMPrincipal(cn, tr, .IdProducto, .IdUnidadMedida, .CantxAtender - .CantADespachar)

                    '*********** actualizao el mov almacen con la cantidad equivalente a la Unidad Principal
                    objDaoMovAlmacen.UpdateCantxAtenderxIdDetalleDocumento(.IdDetalleDocumento, cantidadEquivalente, True, cn, tr)

                    '************* Inserto el detalle documento de la Orden de despacho
                    listaDetalle(i).Cantidad = listaDetalle(i).CantADespachar
                    listaDetalle(i).CantxAtender = Nothing
                    listaDetalle(i).IdDetalleAfecto = listaDetalle(i).IdDetalleDocumento
                    Dim IdDetalleDoc As Integer = objDaoDetalleDocumento.InsertaDetalleDocumento(cn, listaDetalle(i), tr)

                    '****************** Inserto el MOv almacen producido por la Orden de Despacho
                    Dim objMovAlmacen As New Entidades.MovAlmacen
                    objMovAlmacen.IdProducto = .IdProducto
                    objMovAlmacen.IdDocumento = .IdDocumento
                    objMovAlmacen.IdUnidadMedida = .IdUnidadMedida
                    objMovAlmacen.IdUMPrincipal = .IdUnidadMedidaPrincipal
                    objMovAlmacen.IdDetalleDocumento = IdDetalleDoc
                    objMovAlmacen.CantidadMov = objDaoProducto.SelectEquivalenciaUMPrincipal(cn, tr, .IdProducto, .IdUnidadMedida, .CantADespachar)
                    objMovAlmacen.Comprometido = False
                    objMovAlmacen.Factor = -1
                    objMovAlmacen.IdAlmacen = .IdAlmacen
                    objMovAlmacen.IdEmpresa = objDocumento.IdEmpresa
                    objMovAlmacen.IdMetodoV = IdMetodoVal
                    objMovAlmacen.IdTienda = objDocumento.IdTienda
                    objMovAlmacen.IdTipoOperacion = 17
                    objMovAlmacen.UMPrincipal = .UMedida
                    objDaoMovAlmacen.InsertaMovAlmacen(cn, objMovAlmacen, tr)

                End With
            Next
            objDaoRelacionDocumento.InsertaRelacionDocumento(New Entidades.RelacionDocumento(IdDocumentoOld, iddocumento), cn, tr)
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            iddocumento = -1
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return iddocumento
    End Function
    Public Function SelectOrdenDespachoxIdSeriexCodigo(ByVal idserie As Integer, ByVal codigo As Integer) As Entidades.DocOrdenDespacho
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SelectOrdenDespachoxIdSeriexCodigo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Codigo", codigo)
        cmd.Parameters.AddWithValue("@IdSerie", idserie)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim obj As Entidades.DocOrdenDespacho = Nothing
                If lector.Read Then
                    obj = New Entidades.DocOrdenDespacho
                    With obj
                        .NomComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .NomRSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                        .NomCliente = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                        .Dni = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                        .Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                    End With
                End If
                lector.Close()
                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectDetalleOrdenDespachoxIdDocumento(ByVal iddocumento As Integer) As List(Of Entidades.DetalleDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SelectDetalleOrdenDespachoxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.DetalleDocumento
                    With obj
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .UMedida = CStr(IIf(IsDBNull(lector.Item("dc_UMedida")) = True, "", lector.Item("dc_UMedida")))
                        .NomProducto = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                        .IdDetalleDocumento = CInt(IIf(IsDBNull(lector.Item("IdDetalleDocumento")) = True, 0, lector.Item("IdDetalleDocumento")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .IdDetalleAfecto = CInt(IIf(IsDBNull(lector.Item("IdDetalleAfecto")) = True, 0, lector.Item("IdDetalleAfecto")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                        .IdUnidadMedidaPrincipal = CInt(IIf(IsDBNull(lector.Item("ma_IdUMPrincipal")) = True, 0, lector.Item("ma_IdUMPrincipal")))

                        '********** obtengo las cantidades
                        .Cantidad = CDec(IIf(IsDBNull(lector.Item("CantidadTotal")) = True, 0, lector.Item("CantidadTotal")))
                        .CantxAtender = CDec(IIf(IsDBNull(lector.Item("CantidadxAtender")) = True, 0, lector.Item("CantidadxAtender")))
                        .CantADespachar = CDec(IIf(IsDBNull(lector.Item("dc_Cantidad")) = True, 0, lector.Item("dc_Cantidad")))

                        .CantADespacharOriginal = CDec(IIf(IsDBNull(lector.Item("dc_Cantidad")) = True, 0, lector.Item("dc_Cantidad")))

                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

End Class

