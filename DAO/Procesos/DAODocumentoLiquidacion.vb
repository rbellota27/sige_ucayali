﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



'*************   MARTES 27 ABRIL 2010 HORA 02_27 PM






Imports System.Data.SqlClient


Public Class DAODocumentoLiquidacion

    Private cn As SqlConnection
    Private tr As SqlTransaction
    Private cmd As SqlCommand
    Private objConexion As New DAO.Conexion

    Public Function DocumentoLiquidacionPrint(ByVal IdDocumento As Integer) As DataSet


        Dim ds As New DataSet

        cn = (New DAO.Conexion).ConexionSIGE

        cmd = New SqlCommand("_DocumentoLiquidacionPrint_SelectCab", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

        Dim da As New SqlDataAdapter(cmd)
        da.Fill(ds, "_DocumentoLiquidacionPrint_SelectCab")

        cmd = New SqlCommand("_DocumentoLiquidacionPrint_SelectDet", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

        da = New SqlDataAdapter(cmd)
        Try

        Catch ex As Exception
        Finally

        End Try
        da.Fill(ds, "DT_DocumentoLiquidacionPrint_SelectDet")

        Return ds

    End Function


    Public Function DocumentoLiquidacion_DeshacerMov(ByVal IdDocumento As Integer, ByVal DeleteAnexoDocumento As Boolean, ByVal DeleteDetalleDocumento As Boolean, ByVal DeleteMovAlmacen As Boolean, ByVal DeleteMovCaja As Boolean, ByVal DeletePagoCaja As Boolean, ByVal DeleteObservacion As Boolean, ByVal DeleteRelacionDocumento As Boolean, ByVal Anular As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean

        cmd = New SqlCommand("_DocumentoLiquidacion_DeshacerMov", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@DeleteAnexoDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@DeleteDetalleDocumento", DeleteDetalleDocumento)
        cmd.Parameters.AddWithValue("@DeleteMovAlmacen", DeleteMovAlmacen)
        cmd.Parameters.AddWithValue("@DeleteMovCaja", DeleteMovCaja)
        cmd.Parameters.AddWithValue("@DeletePagoCaja", DeletePagoCaja)
        cmd.Parameters.AddWithValue("@DeleteObservacion", DeleteObservacion)
        cmd.Parameters.AddWithValue("@DeleteRelacionDocumento", DeleteRelacionDocumento)
        cmd.Parameters.AddWithValue("@Anular", Anular)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function



    Public Function DocumentoLiquidacion_SelectIdDocumentoRef(ByVal IdDocumento As Integer) As List(Of Entidades.RelacionDocumento)

        Dim lista As New List(Of Entidades.RelacionDocumento)
        cn = (New DAO.Conexion).ConexionSIGE

        Try

            cmd = New SqlCommand("_DocumentoLiquidacion_SelectIdDocumentoRef", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            cn.Open()
            Using cn

                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

                While (lector.Read)

                    Dim obj As New Entidades.RelacionDocumento

                    obj.IdDocumento1 = CInt(lector("IdDocumento1"))
                    obj.IdDocumento2 = CInt(lector("IdDocumento2"))

                    lista.Add(obj)

                End While

            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return lista

    End Function


    Public Sub DocumentoLiquidacion_UpdateSaldo(ByVal IdDocumentoLiquidacion As Integer, ByVal Monto As Decimal, ByVal Factor As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        cmd = New SqlCommand("_DocumentoLiquidacion_UpdateSaldo", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumentoLiquidacion)
        cmd.Parameters.AddWithValue("@Monto", Monto)
        cmd.Parameters.AddWithValue("@Factor", Factor)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try


    End Sub


    Public Function DocumentoLiquidacionSelectCabFind_Aplicacion(ByVal IdPersona As Integer) As List(Of Entidades.DocumentoView)

        cn = (New DAO.Conexion).ConexionSIGE
        cmd = New SqlCommand("_DocumentoLiquidacionSelectCabFind_Aplicacion", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DocumentoView)
                Do While lector.Read
                    Dim obj As New Entidades.DocumentoView
                    With obj
                        .IdDocumento = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))
                        .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                        .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector("doc_FechaEmision")) = True, "", Format(lector("doc_FechaEmision"), "dd/MM/yyyy")))
                        .FechaVenc = CDate(IIf(IsDBNull(lector("doc_FechaVenc")) = True, Nothing, lector("doc_FechaVenc")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")) = True, 0, lector("doc_TotalAPagar")))
                        .ImporteTotal = CDec(IIf(IsDBNull(lector("doc_ImporteTotal")) = True, 0, lector("doc_ImporteTotal")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector("IdEstadoDoc")) = True, 0, lector("IdEstadoDoc")))
                        .IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")) = True, 0, lector("IdPersona")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")) = True, 0, lector("IdMoneda")))
                        .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")) = True, 0, lector("IdTienda")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector("IdEmpresa")) = True, 0, lector("IdEmpresa")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector("IdTipoDocumento")) = True, 0, lector("IdTipoDocumento")))
                        .IdEstadoCancelacion = CInt(IIf(IsDBNull(lector("IdEstadoCan")) = True, 0, lector("IdEstadoCan")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")) = True, "", lector("mon_Simbolo")))
                        .NomEstadoCancelacion = CStr(IIf(IsDBNull(lector("ecan_Nombre")) = True, "", lector("ecan_Nombre")))
                        .NomPropietario = CStr(IIf(IsDBNull(lector("per_NComercial")) = True, "", lector("per_NComercial")))
                        .NomTienda = CStr(IIf(IsDBNull(lector("tie_Nombre")) = True, "", lector("tie_Nombre")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector("edoc_Nombre")) = True, "", lector("edoc_Nombre")))
                        .Contador = CInt(IIf(IsDBNull(lector("DiasVigencia")) = True, 0, lector("DiasVigencia")))
                        .Numero = .Serie + " - " + .Codigo
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function


    Public Function DocumentoLiquidacion_BuscarDocRef(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPersona As Integer, ByVal Serie As Integer, ByVal Codigo As Integer, ByVal IdTipoDocumentoRef As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date) As List(Of Entidades.DocumentoView)

        Dim lista As New List(Of Entidades.DocumentoView)

        cn = (New DAO.Conexion).ConexionSIGE

        cmd = New SqlCommand("_DocumentoLiquidacion_BuscarDocRef", cn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@Serie", Serie)
        cmd.Parameters.AddWithValue("@Codigo", Codigo)
        cmd.Parameters.AddWithValue("@TipoDocumentoRef", IdTipoDocumentoRef)
        cmd.Parameters.AddWithValue("@FechaFin", IIf(FechaFin = Nothing, DBNull.Value, FechaFin))
        cmd.Parameters.AddWithValue("@FechaInicio", IIf(FechaInicio = Nothing, DBNull.Value, FechaInicio))

        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                While (lector.Read)

                    Dim objDocumento As New Entidades.DocumentoView
                    With objDocumento

                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                        .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, 0, lector.Item("doc_Total")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, 0, lector.Item("doc_TotalAPagar")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("edoc_Nombre")) = True, "", lector.Item("edoc_Nombre")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                        .Empresa = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))

                        .RUC = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .DNI = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                        .Numero = .Serie + " - " + .Codigo
                        .strTipoDocumentoRef = CStr(IIf(IsDBNull(lector.Item("CadTipoDocumentoRef")) = True, "", lector.Item("CadTipoDocumentoRef")))
                    End With

                    lista.Add(objDocumento)

                End While

                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


End Class

