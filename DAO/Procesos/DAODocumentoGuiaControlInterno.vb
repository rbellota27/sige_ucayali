﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.






'*************    VIERNES 26 MARZO 2010 HORA 04_30 PM











Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class DAODocumentoGuiaControlInterno

    Private cn As SqlConnection
    Private reader As SqlDataReader

    Public Sub DocumentoGuiaControlInterno_DeshacerMov(ByVal IdDocumento As Integer, ByVal DeleteDetalleDocumento As Boolean, ByVal DeleteMovAlmacen As Boolean, ByVal DeletePuntoPartida As Boolean, ByVal DeletePuntoLlegada As Boolean, ByVal DeleteObservacion As Boolean, ByVal deleteRelacionDoc As Boolean, ByVal Anular As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Using cmd As New SqlCommand("_DocumentoGuiaControlInterno_DeshacerMov", cn, tr)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                .Parameters.Add(New SqlParameter("@IdDocumento", SqlDbType.Int)).Value = IdDocumento
                .Parameters.Add(New SqlParameter("@DeleteMovAlmacen", SqlDbType.Bit)).Value = DeleteMovAlmacen
                .Parameters.Add(New SqlParameter("@DeleteDetalleDocumento", SqlDbType.Bit)).Value = DeleteDetalleDocumento
                .Parameters.Add(New SqlParameter("@DeletePuntoPartida", SqlDbType.Bit)).Value = DeletePuntoPartida
                .Parameters.Add(New SqlParameter("@DeletePuntoLlegada", SqlDbType.Bit)).Value = DeletePuntoLlegada
                .Parameters.Add(New SqlParameter("@DeleteObservacion", SqlDbType.Bit)).Value = DeleteObservacion
                .Parameters.Add(New SqlParameter("@deleteRelacionDoc", SqlDbType.Bit)).Value = deleteRelacionDoc
                .Parameters.Add(New SqlParameter("@Anular", SqlDbType.Bit)).Value = Anular
            End With
            Try
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Sub

    Public Function DocumentoGuiaControlInterno_SelectDetalle(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)

        Dim lista As New List(Of Entidades.DetalleDocumento)
        Try

            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            p(0).Value = IdDocumento

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()

            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_DocumentoGuiaControlInterno_SelectDetalle", p)

            While (reader.Read)

                Dim obj As New Entidades.DetalleDocumento
                With obj

                    .IdProducto = CInt(IIf(IsDBNull(reader("IdProducto")) = True, 0, reader("IdProducto")))
                    .CodigoProducto = CStr(IIf(IsDBNull(reader("Codigo")) = True, 0, reader("Codigo")))
                    .IdUnidadMedida = CInt(IIf(IsDBNull(reader("IdUnidadMedida")) = True, 0, reader("IdUnidadMedida")))
                    .Cantidad = CDec(IIf(IsDBNull(reader("Cantidad")) = True, 0, reader("Cantidad")))
                    .NomProducto = CStr(IIf(IsDBNull(reader("Producto")) = True, "", reader("Producto")))
                    .Kit = CBool(IIf(IsDBNull(reader("prod_Kit")) = True, False, reader("prod_Kit")))
                    .idTono = CInt(IIf(IsDBNull(reader("idTono")) = True, False, reader("idTono")))
                    lista.Add(obj)

                End With

            End While

            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista

    End Function

End Class
