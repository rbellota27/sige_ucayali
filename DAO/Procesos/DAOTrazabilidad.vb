﻿Imports Entidades
Imports System.Data.SqlClient
Public Class DAOTrazabilidad
    Inherits Conexion
    'Dim tr As SqlTransaction = Nothing    

    Public Function DAO_listarDocReferencia(ByVal serie As Integer, ByVal codigo As Integer, ByVal idTienda As Integer, ByVal fechaInicio As String, ByVal fechaFin As String) As DataTable
        Dim cn = ConexionSIGE()
        Dim cmd As New SqlCommand("SP_TRAZABILIDAD_DOC_REF")
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = cn
        With cmd
            .Parameters.AddWithValue("@DOC_SERIE", serie)
            .Parameters.AddWithValue("@DOC_CODIGO", codigo)
            .Parameters.AddWithValue("@IDTIENDA", idTienda)
            .Parameters.AddWithValue("@FECHA_INICIO", fechaInicio)
            .Parameters.AddWithValue("@FECHA_FIN", fechaFin)
        End With
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        Using cmd
            cn.Open()
            Try
                da.Fill(dt)
                Return dt
            Catch ex As Exception
                Throw ex
            Finally
                cn.Close()
            End Try
        End Using
    End Function

    Public Function DAO_listarProductos(ByVal idDocumento As Integer) As DataTable
        Dim cn = ConexionSIGE()
        Dim cmd As New SqlCommand("SP_TRAZABILIDAD_LISTAR_PRODUCTOS")
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = cn
        With cmd
            .Parameters.AddWithValue("@IDDOCUMENTO", idDocumento)
        End With
        Using cmd
            Dim da As New SqlDataAdapter(cmd)
            Dim dt As New DataTable
            cn.Open()
            Try
                da.Fill(dt)
                Return dt
            Catch ex As Exception
                Throw ex
            Finally
                cn.Close()
            End Try
        End Using
    End Function

   

    Public Function DAO_trazabilidad_GuardarKardex(ByVal idDocumentoReferencia As Integer, ByVal usuRegistro As String, ByVal obj As Entidades.Documento)
        Dim cn = ConexionSIGE()
        cn.Open()
        'tr = cn.BeginTransaction(IsolationLevel.Serializable)
        Dim idDcoumento As String = String.Empty
        Using cmd As New SqlCommand("SP_TRAZABILIDAD_INSERT_CABECERA")
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Connection = cn
                '.Transaction = tr

                .Parameters.Add("@IdDocumento", SqlDbType.Int, 50)
                .Parameters("@IdDocumento").Direction = ParameterDirection.Output
                .Parameters.AddWithValue("@IDDOCUMENTO_REFERENCIA", idDocumentoReferencia)
                .Parameters.AddWithValue("@DOC_SERIE", obj.Serie)
                .Parameters.AddWithValue("@DOC_CODIGO", obj.Codigo)
                .Parameters.AddWithValue("@DOC_FECHAEMISION", obj.FechaEmision)
                .Parameters.AddWithValue("@ID_TIENDA", obj.IdTienda)
                .Parameters.AddWithValue("@ID_EMPRESA", obj.IdEmpresa)
                .Parameters.AddWithValue("@ID_TIPODOCUMENTO", obj.IdTipoDocumento)
                .Parameters.AddWithValue("@USU_REG", usuRegistro)
            End With
            Try
                cmd.ExecuteScalar()
                idDcoumento = CStr(IIf(IsDBNull(cmd.Parameters("@IdDocumento").Value), "", cmd.Parameters("@IdDocumento").Value))
                Return idDcoumento
                'tr.Commit()
            Catch ex As Exception
                'tr.Rollback()
                Throw ex
            Finally
                cn.Close()
            End Try
        End Using
    End Function

    Public Function DAO_trazabilidad_Ver_Registro(ByVal flag As String, ByVal serie As String, ByVal codigo As String, ByVal idempresa As Integer, ByVal idTienda As Integer, _
                                             ByVal idTipodocumento As Integer) As DataTable
        Using cn As New SqlConnection(cadenaConexion)
            Dim dt_cabecera As New DataTable
            Dim dt_doc_ref As New DataTable
            Dim cmd As New SqlCommand("SP_TRAZABILIDAD_DISTRIBUCION_NUEVO")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cn
            With cmd
                .Parameters.AddWithValue("@FLAG", flag)
                .Parameters.AddWithValue("@DOC_SERIE", serie)
                .Parameters.AddWithValue("@DOC_CODIGO", codigo)
                .Parameters.AddWithValue("@ID_EMPRESA", idempresa)
                .Parameters.AddWithValue("@ID_TIENDA", idTienda)
                .Parameters.AddWithValue("@ID_TIPO_DOC", idTipodocumento)
            End With
            Try
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt_doc_ref) 'carga referencia
                Return dt_doc_ref
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    Public Sub DAO_trazabilidad_GuardarSaldoKardex(ByVal idProducto As Integer, ByVal idUnidadMedida As Integer, ByVal idAfecto As Boolean, ByVal usuRegistro As String, _
                                                   ByVal idDocumento As Integer)
        Dim cn = ConexionSIGE()
        cn.Open()
        'tr = cn.BeginTransaction(IsolationLevel.Serializable)
        Using cmd As New SqlCommand("SP_TRAZABILIDAD_INSERT_SALDO_KARDEX")
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Connection = cn
                '.Transaction = tr

                .Parameters.AddWithValue("@IDPRODUCTO", idProducto)
                .Parameters.AddWithValue("@IDUNIDADMEDIDA", idUnidadMedida)
                .Parameters.AddWithValue("@AFECTO", idAfecto)
                .Parameters.AddWithValue("@USU_REG", usuRegistro)
                .Parameters.AddWithValue("@IDDOCUMENTO", idDocumento)
            End With
            Try
                cmd.ExecuteNonQuery()
                ' tr.Commit()
            Catch ex As Exception
                'tr.Rollback()
                Throw ex
            Finally
                cn.Close()
            End Try
        End Using
    End Sub

    Public Sub DAO_trazabilidad_GuardarDetalleSaldoKardex(ByVal idProducto As Integer, ByVal idSector As Integer, ByVal cantidadSaldo As Decimal, _
                                                          ByVal idDocumento As Integer, ByVal idTono As Integer)
        Dim cn = ConexionSIGE()
        cn.Open()
        'tr = cn.BeginTransaction(IsolationLevel.Serializable)
        Using cmd As New SqlCommand("SP_TRAZABILIDAD_INSERT_SALDO_KARDEX_DETALLE")
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Connection = cn
                '.Transaction = tr

                .Parameters.AddWithValue("@IDSECTOR", idSector)
                .Parameters.AddWithValue("@IDPRODUCTO", idProducto)
                .Parameters.AddWithValue("@CANTIDASALDO", cantidadSaldo)
                .Parameters.AddWithValue("@IDDOCUMENTO", idDocumento)
                .Parameters.AddWithValue("@IDTONO", idTono)
            End With
            Try
                cmd.ExecuteNonQuery()
                ' tr.Commit()
            Catch ex As Exception
                'tr.Rollback()
                Throw ex
            Finally
                cn.Close()
            End Try
        End Using
    End Sub

    Public Function DAO_obtenerIdDocumento(ByVal doc_serie As String, ByVal doc_codigo As String, ByVal idTipoDocumento As Integer) As Integer
        Dim idDocumento As String = String.Empty
        Using con As SqlConnection = ConexionSIGE()
            Dim cmd As New SqlCommand("SP_TRAZABILIDAR_VALIDAR_ID_DOCUMENTO")
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Connection = con

                .Parameters.Add(New SqlParameter("@IDDOCUMENTO", SqlDbType.Int, 30, ParameterDirection.Output)).Direction = ParameterDirection.Output
                .Parameters.Add(New SqlParameter("@DOC_SERIE", SqlDbType.VarChar, 20, ParameterDirection.Input)).Value = doc_serie
                .Parameters.Add(New SqlParameter("@DOC_CODIGO", SqlDbType.VarChar, 100, ParameterDirection.Input)).Value = doc_codigo
                .Parameters.Add(New SqlParameter("@IDTIPODOCUMENTO", SqlDbType.Int, 30, ParameterDirection.Input)).Value = idTipoDocumento

                con.Open()
                Try
                    .ExecuteScalar()
                Catch ex As Exception
                    Throw ex
                Finally
                    con.Close()
                End Try
                idDocumento = CStr(IIf(IsDBNull(cmd.Parameters("@IDDOCUMENTO").Value), 0, cmd.Parameters("@IDDOCUMENTO").Value))
                Return idDocumento
            End With
        End Using
    End Function

    Public Function DAO_Insert_Kardex_detallado(ByVal idProducto As Integer, ByVal idAlmacen As Integer, ByVal idSector As Integer, _
                                                ByVal cantidad As Decimal, ByVal idTono As Integer) As Boolean
        Dim idDocumento As String = String.Empty
        Using con As SqlConnection = ConexionSIGE()
            Dim cmd As New SqlCommand("SP_TRAZABILIDAD_INSERT_KARDEX_DETALLADO")
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Connection = con

                .Parameters.Add(New SqlParameter("@IDPRODUCTO", SqlDbType.Int, 30, ParameterDirection.Input)).Value = idProducto
                .Parameters.Add(New SqlParameter("@IDALMACEN", SqlDbType.Int, 30, ParameterDirection.Input)).Value = idAlmacen
                .Parameters.Add(New SqlParameter("@IDSECTOR", SqlDbType.Int, 30, ParameterDirection.Input)).Value = idSector
                .Parameters.Add(New SqlParameter("@CANTIDAD", SqlDbType.Decimal, 18, ParameterDirection.Input)).Value = cantidad
                .Parameters.Add(New SqlParameter("@IDTONO", SqlDbType.Int, 18, ParameterDirection.Input)).Value = idTono

                con.Open()
                Try
                    If .ExecuteNonQuery = 0 Then
                        Throw New Exception("Fracasó la operación de actualizar Stock.")
                    End If
                    Return True
                Catch ex As Exception
                    Throw New Exception(ex.Message)
                    Return False
                Finally
                    con.Close()
                End Try
            End With
        End Using
    End Function

    Public Function DAO_MANTENIMIENTO_TONO(ByVal nom_tono As String, ByVal descTono As String, ByVal idProducto As Integer, ByVal idAlmacen As Integer, ByVal idSector As Integer, _
                                   ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Dim idDocumento As String = String.Empty
        Using cmd As New SqlCommand("SP_TRAZABILIDAD_MANTENIMIENTO_STOCK")
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Connection = cn
                .Transaction = tr

                .Parameters.Add(New SqlParameter("@NOM_TONO", SqlDbType.VarChar, 150, ParameterDirection.Input)).Value = nom_tono
                .Parameters.Add(New SqlParameter("@DESC_TONO", SqlDbType.VarChar, 200, ParameterDirection.Input)).Value = descTono
                .Parameters.Add(New SqlParameter("@IDALMACEN", SqlDbType.Int, 50, ParameterDirection.Input)).Value = idAlmacen
                .Parameters.Add(New SqlParameter("@IDPRODUCTO", SqlDbType.Int, 50, ParameterDirection.Input)).Value = idProducto
                .Parameters.Add(New SqlParameter("@IDSECTOR", SqlDbType.Int, 50, ParameterDirection.Input)).Value = idSector
                Try
                    If .ExecuteNonQuery = 0 Then
                        Throw New Exception("Fracasó la operación.")
                    End If
                    Return True
                Catch ex As Exception
                    Throw New Exception(ex.Message)
                    Return False
                End Try
            End With
        End Using
    End Function

    Public Function DAO_insertTono(ByVal nom_tono As String, ByVal descTono As String, ByVal idProducto As Integer, _
                                   ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Dim idDocumento As String = String.Empty
        Using cmd As New SqlCommand("SP_TRAZABILIDAD_INSERT_TONO_V1")
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Connection = cn
                .Transaction = tr

                .Parameters.Add(New SqlParameter("@NOM_TONO", SqlDbType.VarChar, 150, ParameterDirection.Input)).Value = nom_tono
                .Parameters.Add(New SqlParameter("@DESC_TONO", SqlDbType.VarChar, 200, ParameterDirection.Input)).Value = descTono
                .Parameters.Add(New SqlParameter("@IDPRODUCTO", SqlDbType.Int, 50, ParameterDirection.Input)).Value = idProducto
                Try
                    If .ExecuteNonQuery = 0 Then
                        Throw New Exception("Fracasó la operación.")
                    End If
                    Return True
                Catch ex As Exception
                    Throw New Exception(ex.Message)
                    Return False
                End Try
            End With
        End Using
    End Function

    Public Function DAO_trazabilidad_Select_Kardex(ByVal idSector As Integer, ByVal idAlmacen As Integer, ByVal prod_nombre As String, _
                                                         ByVal prod_codigo As String, ByVal idTipoExistencia As Integer, ByVal idLinea As Integer, _
                                                         ByVal idSublinea As Integer, ByVal cn As SqlConnection) As List(Of Entidades.be_productoStock)
        Dim lista As List(Of Entidades.be_productoStock) = Nothing
        Dim dt As New DataTable

        Using cmd As New SqlCommand("SP_TRAZABILIDAD_SELECT_STOCK")

            With cmd
                .CommandType = CommandType.StoredProcedure
                .Connection = cn

                .Parameters.Add(New SqlParameter("@ID_SECTOR", SqlDbType.Int, 150, ParameterDirection.Input)).Value = idSector
                .Parameters.Add(New SqlParameter("@ID_ALMACEN", SqlDbType.Int, 200, ParameterDirection.Input)).Value = idAlmacen
                .Parameters.Add(New SqlParameter("@PROD_NOMBRE", SqlDbType.VarChar, 200, ParameterDirection.Input)).Value = prod_nombre
                .Parameters.Add(New SqlParameter("@PROD_CODIGO", SqlDbType.VarChar, 150, ParameterDirection.Input)).Value = prod_codigo
                .Parameters.Add(New SqlParameter("@ID_TIPO_EXISTENCIA", SqlDbType.Int, 150, ParameterDirection.Input)).Value = idTipoExistencia
                .Parameters.Add(New SqlParameter("@ID_LINEA", SqlDbType.Int, 150, ParameterDirection.Input)).Value = idLinea
                .Parameters.Add(New SqlParameter("@ID_SUB_LINEA", SqlDbType.Int, 150, ParameterDirection.Input)).Value = idSublinea
                Try
                    Dim rdr As SqlDataReader = .ExecuteReader(CommandBehavior.SingleResult)
                    If rdr IsNot Nothing Then
                        lista = New List(Of Entidades.be_productoStock)

                        Dim _idproDucto As Integer = rdr.GetOrdinal("idproducto")
                        Dim _idAlmacen As Integer = rdr.GetOrdinal("idalmacen")
                        Dim _idsector As Integer = rdr.GetOrdinal("idsector")
                        Dim _idTono As Integer = rdr.GetOrdinal("idTono")
                        Dim _tex_Nombre As Integer = rdr.GetOrdinal("tex_Nombre")
                        Dim _lin_Nombre As Integer = rdr.GetOrdinal("lin_Nombre")
                        Dim _sl_Nombre As Integer = rdr.GetOrdinal("sl_Nombre")
                        Dim _prod_codigo As Integer = rdr.GetOrdinal("prod_Codigo")
                        Dim _prod_Nombre As Integer = rdr.GetOrdinal("prod_Nombre")
                        Dim _almacen As Integer = rdr.GetOrdinal("tie_Nombre")
                        Dim _nom_sector As Integer = rdr.GetOrdinal("nom_sector")
                        Dim _stock_milla As Integer = rdr.GetOrdinal("stock_milla")
                        Dim _cantidad As Integer = rdr.GetOrdinal("cantidad")
                        Dim _NOM_TONO As Integer = rdr.GetOrdinal("NOM_TONO")
                        Dim _um_NombreCorto As Integer = rdr.GetOrdinal("um_NombreCorto")

                        Dim objetoProducto As Entidades.be_productoStock = Nothing
                        While rdr.Read()
                            objetoProducto = New Entidades.be_productoStock
                            With objetoProducto
                                .idProducto = rdr.GetInt32(_idproDucto)
                                .idalmacen = rdr.GetInt32(_idAlmacen)
                                .idsector = rdr.GetInt32(_idsector)
                                .idTono = rdr.GetInt32(_idTono)
                                .existencia = rdr.GetString(_tex_Nombre)
                                .linea = rdr.GetString(_lin_Nombre)
                                .sublinea = rdr.GetString(_sl_Nombre)
                                .codSige = rdr.GetString(_prod_codigo)
                                .producto = rdr.GetString(_prod_Nombre)
                                .almacen = rdr.GetString(_almacen)
                                .sector = rdr.GetString(_nom_sector)
                                .stockKardex = rdr.GetDecimal(_stock_milla)
                                .stockTono = rdr.GetString(_cantidad)
                                .tonoDisponible = rdr.GetString(_NOM_TONO)
                                .UnidadMedida = rdr.GetString(_um_NombreCorto)
                            End With
                            lista.Add(objetoProducto)
                        End While
                        rdr.Close()
                    End If
                    Return lista
                Catch ex As Exception
                    Throw New Exception(ex.Message)
                Finally
                End Try
            End With
        End Using
    End Function

    Public Function DAO_Update_Stock(ByVal idProducto As Integer, ByVal idAlmacen As Integer, ByVal idSector_antiguo As Integer, ByVal idSector_nuevo As Integer, _
                                                ByVal cantidad As Decimal, ByVal idTono As Integer) As Boolean
        Dim idDocumento As String = String.Empty
        Using con As SqlConnection = ConexionSIGE()
            Dim cmd As New SqlCommand("SP_TRAZABILIDAD_UPDATE_STOCK")
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Connection = con

                .Parameters.Add(New SqlParameter("@IDPRODUCTO", SqlDbType.Int, 30, ParameterDirection.Input)).Value = idProducto
                .Parameters.Add(New SqlParameter("@IDALMACEN", SqlDbType.Int, 30, ParameterDirection.Input)).Value = idAlmacen
                .Parameters.Add(New SqlParameter("@IDSECTOR_ANTIGUO", SqlDbType.Int, 30, ParameterDirection.Input)).Value = idSector_antiguo
                .Parameters.Add(New SqlParameter("@IDSECTOR_NUEVO", SqlDbType.Int, 30, ParameterDirection.Input)).Value = idSector_nuevo
                .Parameters.Add(New SqlParameter("@CANTIDAD", SqlDbType.Decimal, 18, ParameterDirection.Input)).Value = cantidad
                .Parameters.Add(New SqlParameter("@IDTONO", SqlDbType.Int, 18, ParameterDirection.Input)).Value = idTono

                con.Open()
                Try
                    If .ExecuteNonQuery = 0 Then
                        Throw New Exception("Fracasó la operación de actualizar Stock.")
                    End If
                    Return True
                Catch ex As Exception
                    Throw New Exception(ex.Message)
                    Return False
                Finally
                    con.Close()
                End Try
            End With
        End Using
    End Function
End Class
