﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOConfiguracionDescuentoAvanzado

    Private objConexion As New DAO.Conexion
    Private tr As SqlTransaction = Nothing
    Private cmd As SqlCommand
    Private lector As SqlDataReader
    Private cn As SqlConnection

    Public Function getPrecioBaseDcto(ByVal IdUsuario As Integer, ByVal IdTipoPV As Integer, ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer) As String

        Dim precioBaseDcto As String = ""
        cn = objConexion.ConexionSIGE

        Try

            cn.Open()
            cmd = New SqlCommand("     select dbo.fx_getPrecioBaseDcto_UsuarioxCondPagoxMedioPagoxTipoPV(" + CStr(IdUsuario) + "," + CStr(IdCondicionPago) + "," + CStr(IdMedioPago) + "," + CStr(IdTipoPV) + ")    ", cn)
            cmd.CommandType = CommandType.Text

            precioBaseDcto = CStr(cmd.ExecuteScalar)
            cn.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return precioBaseDcto

    End Function
    Public Function getPorcentDctoMax(ByVal IdUsuario As Integer, ByVal IdTipoPV As Integer, ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer) As Decimal

        Dim porcentDcto As Decimal = 0
        cn = objConexion.ConexionSIGE

        Try
            cn.Open()
            cmd = New SqlCommand("select dbo.fx_getPorcentDctoMax_UsuarioxCondPagoxMedioPagoxTipoPV(" + CStr(IdUsuario) + "," + CStr(IdCondicionPago) + "," + CStr(IdMedioPago) + "," + CStr(IdTipoPV) + ")    ", cn)
            cmd.CommandType = CommandType.Text

            porcentDcto = CDec(cmd.ExecuteScalar)
            cn.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return porcentDcto

    End Function
    Public Function getPorcentDctoMaxAvz(ByVal IdUsuario As Integer, ByVal IdTipoPV As Integer, ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer, ByVal IdTipoOperacion As Integer, ByVal CodigoProd As String) As Decimal

        Dim porcentDcto As Decimal = 0
        cn = objConexion.ConexionSIGE

        Try
            cn.Open()
            'cmd = New SqlCommand("select dbo.fx_getPorcentDctoMax_UsuarioxCondPagoxMedioPagoxTipoPV_CbMediopago(" + CStr(IdUsuario) + "," + CStr(IdCondicionPago) + "," + CStr(IdMedioPago) + "," + CStr(IdTipoPV) + ")    ", cn)
            ',@IdTipoOperacion int,@IdProducto int, @idlinea int ,@IdSublinea int,@idpais int
            cmd = New SqlCommand("select dbo.fx_getPorcentDctoMax_UsuarioxCondPagoxMedioPagoxTipoPV_CbMediopagoCodigo(" + CStr(IdUsuario) + "," + CStr(IdCondicionPago) + "," + CStr(IdMedioPago) + "," + CStr(IdTipoPV) + "," + CStr(IdTipoOperacion) + ",'" + (CodigoProd) + "')    ", cn)
            cmd.CommandType = CommandType.Text
            porcentDcto = CDec(cmd.ExecuteScalar)
            cn.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return porcentDcto

    End Function


    Public Function InsertConfiguracionDescuentoAvanzado(ByVal objEnt As Entidades.ConfiguracionDescuentoAvanzado) As Integer
        Dim parametros() As SqlParameter = New SqlParameter(12) {}
        parametros(0) = New SqlParameter("@IdMedioPago", SqlDbType.Int)
        parametros(0).Value = IIf(objEnt.IdMedioPago = Nothing, DBNull.Value, objEnt.IdMedioPago)
        parametros(1) = New SqlParameter("@IdPerfil", SqlDbType.Int)
        parametros(1).Value = IIf(objEnt.IdPerfil = Nothing, DBNull.Value, objEnt.IdPerfil)
        parametros(2) = New SqlParameter("@IdTipoPV", SqlDbType.Int)
        parametros(2).Value = IIf(objEnt.IdTipoPV = Nothing, DBNull.Value, objEnt.IdTipoPV)
        parametros(3) = New SqlParameter("@IdCondicionPago", SqlDbType.Int)
        parametros(3).Value = IIf(objEnt.IdCondicionPago = Nothing, DBNull.Value, objEnt.IdCondicionPago)
        parametros(4) = New SqlParameter("@desc_Porcentaje", SqlDbType.Decimal)
        parametros(4).Value = IIf(objEnt.desc_Porcentaje = Nothing, DBNull.Value, objEnt.desc_Porcentaje)
        parametros(5) = New SqlParameter("@desc_Estado", SqlDbType.Char)
        parametros(5).Value = IIf(objEnt.desc_Estado = Nothing, DBNull.Value, objEnt.desc_Estado)
        parametros(6) = New SqlParameter("@desc_PrecioBaseDscto", SqlDbType.VarChar)
        parametros(6).Value = IIf(objEnt.Desc_PrecioBaseDscto = Nothing, DBNull.Value, objEnt.Desc_PrecioBaseDscto)
        parametros(7) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        parametros(7).Value = IIf(objEnt.IdTipoOperacion = Nothing, DBNull.Value, objEnt.IdTipoOperacion)
        parametros(8) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        parametros(8).Value = IIf(objEnt.IdUsuario = Nothing, DBNull.Value, objEnt.IdUsuario)
        parametros(9) = New SqlParameter("@IdExistencia", SqlDbType.Int)
        parametros(9).Value = IIf(objEnt.IdExistencia = Nothing, DBNull.Value, objEnt.IdExistencia)
        parametros(10) = New SqlParameter("@IdLinea", SqlDbType.Int)
        parametros(10).Value = IIf(objEnt.IdLinea = Nothing, DBNull.Value, objEnt.IdLinea)
        parametros(11) = New SqlParameter("@CodigoP", SqlDbType.Int)
        parametros(11).Value = IIf(objEnt.CodigoP = Nothing, DBNull.Value, objEnt.CodigoP)
        parametros(12) = New SqlParameter("@idpais", SqlDbType.Int)
        parametros(12).Value = IIf(objEnt.idpais = Nothing, DBNull.Value, objEnt.idpais)

        Dim cod% = 0
        cn = objConexion.ConexionSIGE
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            cmd = New SqlCommand("_InsertConfiguracionDescuentoavanzado", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(parametros)
            cod = CInt(cmd.ExecuteScalar)
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return cod
    End Function



    Public Function InsertConfiguracionDescuento(ByVal objEnt As Entidades.ConfiguracionDescuentoAvanzado) As Integer
        Dim parametros() As SqlParameter = New SqlParameter(6) {}
        parametros(0) = New SqlParameter("@IdMedioPago", SqlDbType.Int)
        parametros(0).Value = IIf(objEnt.IdMedioPago = Nothing, DBNull.Value, objEnt.IdMedioPago)
        parametros(1) = New SqlParameter("@IdPerfil", SqlDbType.Int)
        parametros(1).Value = IIf(objEnt.IdPerfil = Nothing, DBNull.Value, objEnt.IdPerfil)
        parametros(2) = New SqlParameter("@IdTipoPV", SqlDbType.Int)
        parametros(2).Value = IIf(objEnt.IdTipoPV = Nothing, DBNull.Value, objEnt.IdTipoPV)
        parametros(3) = New SqlParameter("@IdCondicionPago", SqlDbType.Int)
        parametros(3).Value = IIf(objEnt.IdCondicionPago = Nothing, DBNull.Value, objEnt.IdCondicionPago)
        parametros(4) = New SqlParameter("@desc_Porcentaje", SqlDbType.Decimal)
        parametros(4).Value = IIf(objEnt.desc_Porcentaje = Nothing, DBNull.Value, objEnt.desc_Porcentaje)
        parametros(5) = New SqlParameter("@desc_Estado", SqlDbType.Char)
        parametros(5).Value = IIf(objEnt.desc_Estado = Nothing, DBNull.Value, objEnt.desc_Estado)
        parametros(6) = New SqlParameter("@desc_PrecioBaseDscto", SqlDbType.VarChar)
        parametros(6).Value = IIf(objEnt.Desc_PrecioBaseDscto = Nothing, DBNull.Value, objEnt.Desc_PrecioBaseDscto)

        Dim cod% = 0
        cn = objConexion.ConexionSIGE
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            cmd = New SqlCommand("_InsertConfiguracionDescuento", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(parametros)
            cod = CInt(cmd.ExecuteScalar)
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return cod
    End Function

    Public Function UpdateConfiguracionDescuento(ByVal objEnt As Entidades.ConfiguracionDescuentoAvanzado) As Boolean
        Dim parametros() As SqlParameter = New SqlParameter(7) {}
        parametros(0) = New SqlParameter("@IdMedioPago", SqlDbType.Int)
        parametros(0).Value = IIf(objEnt.IdMedioPago = Nothing, DBNull.Value, objEnt.IdMedioPago)
        parametros(1) = New SqlParameter("@IdPerfil", SqlDbType.Int)
        parametros(1).Value = IIf(objEnt.IdPerfil = Nothing, DBNull.Value, objEnt.IdPerfil)
        parametros(2) = New SqlParameter("@IdTipoPV", SqlDbType.Int)
        parametros(2).Value = IIf(objEnt.IdTipoPV = Nothing, DBNull.Value, objEnt.IdTipoPV)
        parametros(3) = New SqlParameter("@IdCondicionPago", SqlDbType.Int)
        parametros(3).Value = IIf(objEnt.IdCondicionPago = Nothing, DBNull.Value, objEnt.IdCondicionPago)
        parametros(4) = New SqlParameter("@desc_Porcentaje", SqlDbType.Decimal)
        parametros(4).Value = IIf(objEnt.desc_Porcentaje = Nothing, DBNull.Value, objEnt.desc_Porcentaje)
        parametros(5) = New SqlParameter("@desc_Estado", SqlDbType.Char)
        parametros(5).Value = IIf(objEnt.desc_Estado = Nothing, DBNull.Value, objEnt.desc_Estado)
        parametros(6) = New SqlParameter("@IdDescuento", SqlDbType.Int)
        parametros(6).Value = IIf(objEnt.IdDescuento = Nothing, DBNull.Value, objEnt.IdDescuento)
        parametros(7) = New SqlParameter("@desc_PrecioBaseDscto", SqlDbType.VarChar)
        parametros(7).Value = IIf(objEnt.Desc_PrecioBaseDscto = Nothing, DBNull.Value, objEnt.Desc_PrecioBaseDscto)


        cn = objConexion.ConexionSIGE
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            cmd = New SqlCommand("_UpdateConfiguracionDescuento", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(parametros)
            cmd.ExecuteNonQuery()
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return True
    End Function


    Public Function SelectConfiguracionDescuento(ByVal estado As Integer, ByVal idMedPago As Integer, ByVal idPerfil As Integer, ByVal idTipoPv As Integer, ByVal idcondicionPago As Integer, ByVal desc_PrecioBaseDscto As String) As List(Of Entidades.ConfiguracionDescuentoAvanzado)
        Dim Lista As New List(Of Entidades.ConfiguracionDescuentoAvanzado)
        cn = objConexion.ConexionSIGE
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            cmd = New SqlCommand("_SelectConfiguracionDescuento", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@estado", estado)
            cmd.Parameters.AddWithValue("@idmediopago", idMedPago)
            cmd.Parameters.AddWithValue("@idPerfil", idPerfil)
            cmd.Parameters.AddWithValue("@idTipoPv", idTipoPv)
            cmd.Parameters.AddWithValue("@idcondicionPago", idcondicionPago)
            cmd.Parameters.AddWithValue("@desc_PrecioBaseDscto", desc_PrecioBaseDscto)



            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.ConfiguracionDescuentoAvanzado
                With obj

                    .IdDescuento = CInt(lector("IdDescuento"))
                    .IdMedioPago = CInt(IIf(IsDBNull(lector("IdMedioPago")), 0, lector("IdMedioPago")))
                    .IdPerfil = CInt(IIf(IsDBNull(lector("IdPerfil")), 0, lector("IdPerfil")))
                    .IdTipoPV = CInt(IIf(IsDBNull(lector("IdTipoPV")), 0, lector("IdTipoPV")))
                    .IdCondicionPago = CInt(IIf(IsDBNull(lector("IdCondicionPago")), 0, lector("IdCondicionPago")))
                    .MedioPago = CStr(IIf(IsDBNull(lector("MedioPago")), "---", lector("MedioPago")))
                    .TipoPrecioV = CStr(IIf(IsDBNull(lector("TipoPrecioV")), "---", lector("TipoPrecioV")))
                    .CondicionPago = CStr(IIf(IsDBNull(lector("CondicionPago")), "---", lector("CondicionPago")))
                    .Perfil = CStr(IIf(IsDBNull(lector("Perfil")), 0, lector("Perfil")))
                    .desc_Porcentaje = CDec(IIf(IsDBNull(lector("desc_Porcentaje")), 0, lector("desc_Porcentaje")))
                    .desc_FechaRegistro = CDate(IIf(IsDBNull(lector("desc_FechaRegistro")), "---", lector("desc_FechaRegistro")))
                    .desc_Estado = CStr(IIf(IsDBNull(lector("desc_Estado")), "---", lector("desc_Estado")))
                    .Desc_PrecioBaseDscto = CStr(IIf(IsDBNull(lector("desc_PrecioBaseDcto")), "", lector("desc_PrecioBaseDcto")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(lector("IdTipoOperacion")), 0, lector("IdTipoOperacion")))
                    .idpais = CInt(IIf(IsDBNull(lector("IdPais")), 0, lector("IdPais")))
                    .IdLinea = CInt(IIf(IsDBNull(lector("IdLinea")), 0, lector("IdLinea")))
                    .IdUsuario = CInt(IIf(IsDBNull(lector("IdUsuario")), 0, lector("IdUsuario")))
                    .NUsuario = CStr(IIf(IsDBNull(lector("NUsuario")), "", lector("NUsuario")))


                End With
                Lista.Add(obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function



    Public Function SelectConfiguracionDescuentoAvanzado(ByVal estado As Integer, ByVal idMedPago As Integer, ByVal idPerfil As Integer, ByVal idTipoPv As Integer, ByVal idcondicionPago As Integer, ByVal desc_PrecioBaseDscto As String) As List(Of Entidades.ConfiguracionDescuentoAvanzado)
        Dim Lista As New List(Of Entidades.ConfiguracionDescuentoAvanzado)
        cn = objConexion.ConexionSIGE
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            cmd = New SqlCommand("_SelectConfiguracionDescuentoAvanzado", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@estado", estado)
            cmd.Parameters.AddWithValue("@idmediopago", idMedPago)
            cmd.Parameters.AddWithValue("@idPerfil", idPerfil)
            cmd.Parameters.AddWithValue("@idTipoPv", idTipoPv)
            cmd.Parameters.AddWithValue("@idcondicionPago", idcondicionPago)
            cmd.Parameters.AddWithValue("@desc_PrecioBaseDscto", desc_PrecioBaseDscto)



            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.ConfiguracionDescuentoAvanzado
                With obj

                    .IdDescuento = CInt(lector("IdDescuento"))
                    .IdMedioPago = CInt(IIf(IsDBNull(lector("IdMedioPago")), 0, lector("IdMedioPago")))
                    .IdPerfil = CInt(IIf(IsDBNull(lector("IdPerfil")), 0, lector("IdPerfil")))
                    .IdTipoPV = CInt(IIf(IsDBNull(lector("IdTipoPV")), 0, lector("IdTipoPV")))
                    .IdCondicionPago = CInt(IIf(IsDBNull(lector("IdCondicionPago")), 0, lector("IdCondicionPago")))
                    .MedioPago = CStr(IIf(IsDBNull(lector("MedioPago")), "---", lector("MedioPago")))
                    .TipoPrecioV = CStr(IIf(IsDBNull(lector("TipoPrecioV")), "---", lector("TipoPrecioV")))
                    .CondicionPago = CStr(IIf(IsDBNull(lector("CondicionPago")), "---", lector("CondicionPago")))
                    .Perfil = CStr(IIf(IsDBNull(lector("Perfil")), 0, lector("Perfil")))
                    .desc_Porcentaje = CDec(IIf(IsDBNull(lector("desc_Porcentaje")), 0, lector("desc_Porcentaje")))
                    .desc_FechaRegistro = CDate(IIf(IsDBNull(lector("desc_FechaRegistro")), "---", lector("desc_FechaRegistro")))
                    .desc_Estado = CStr(IIf(IsDBNull(lector("desc_Estado")), "---", lector("desc_Estado")))
                    .Desc_PrecioBaseDscto = CStr(IIf(IsDBNull(lector("desc_PrecioBaseDcto")), "", lector("desc_PrecioBaseDcto")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(lector("IdTipoOperacion")), 0, lector("IdTipoOperacion")))
                    .idpais = CInt(IIf(IsDBNull(lector("IdPais")), 0, lector("IdPais")))
                    .IdLinea = CInt(IIf(IsDBNull(lector("IdLinea")), 0, lector("IdLinea")))
                    .IdUsuario = CInt(IIf(IsDBNull(lector("IdUsuario")), 0, lector("IdUsuario")))
                    .NUsuario = CStr(IIf(IsDBNull(lector("NUsuario")), "", lector("NUsuario")))

                    .TipoOperacion = CStr(IIf(IsDBNull(lector("TipoOperacion")), "", lector("TipoOperacion")))





                End With
                Lista.Add(obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function

End Class
