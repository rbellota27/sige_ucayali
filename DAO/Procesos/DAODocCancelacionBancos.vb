﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Public Class DAODocCancelacionBancos
    Inherits DAO.DAODocumento

    Private DBUtil As New DAOMantenedor
    Private objConexion As New DAO.Conexion



    '*************** DOCUMENTO CANCELACION BANCOS



    '********* Inserta el Documento CANCELACION Banco y retorna el IdDocumento
    Public Function Insert(ByVal objDocumento As Entidades.Documento, _
    ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), _
    ByVal listaCancelacion As List(Of Entidades.DatosCancelacion), _
    ByVal objObservaciones As Entidades.Observacion, _
    ByVal listaMovBanco As List(Of Entidades.MovBancoView)) As Integer
        'ByVal objMovCaja As Entidades.MovCaja, _

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            '************* Insertamos la cabecera
            objDocumento.Id = MyBase.InsertaDocumento(cn, objDocumento, tr)

            '************* Insertamos el DETALLE CONCEPTO
            InsertListDetalleConceptoxIdDocumento_CancelacionBancos(listaDetalleConcepto, objDocumento.Id, cn, tr)

            '*********** Insertamos la cancelación - Mov Caja - Pago Caja
            'If (objMovCaja IsNot Nothing) Then

            '    '*********** Inserto Mov Caja
            '    InsertMovCajaxIdDocumento(objMovCaja, objDocumento.Id, cn, tr)

            '    '*********** Inserto Lista Cancelacion
            '    InsertListCancelacion_DatosCancelacion(listaCancelacion, objDocumento.Id, cn, tr)

            'End If
            InsertListCancelacion_DatosCancelacion(listaCancelacion, objDocumento.Id, cn, tr)

            InsertObservacion(objObservaciones, objDocumento.Id, cn, tr)


            Dim objDaoAnexoMovBanco As New DAO.DAOAnexo_MovBanco
            Dim objDaoDocumentoFacturacion As New DAO.DAODocumentoFacturacion
            For i As Integer = 0 To listaMovBanco.Count - 1
                listaMovBanco(i).IdMovBanco = objDaoDocumentoFacturacion.MovBancoInsert_Venta(listaMovBanco(i), cn, tr) '******** INSERTAMOS MOV BANCO

                If (listaMovBanco(i).IdMovBanco <> Nothing) Then
                    objDaoAnexoMovBanco.Anexo_MovBancoInsert(New Entidades.Anexo_MovBanco(listaMovBanco(i).IdMovBanco, listaMovBanco(i).IdCuentaBancaria, Nothing, objDocumento.Id, listaMovBanco(i).IdBanco), cn, tr)
                End If

            Next

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function

    '********* Actualiza el Documento CANCELACION BANCO y retorna el IdDocumento
    Public Function Update(ByVal objDocumento As Entidades.Documento, _
    ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), _
    ByVal listaCancelacion As List(Of Entidades.DatosCancelacion), _
    ByVal objObservaciones As Entidades.Observacion, _
    ByVal listaMovBanco As List(Of Entidades.MovBancoView)) As Integer
        'ByVal objMovCaja As Entidades.MovCaja, _

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing

        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            '************ Deshacemos lo hecho (DetalleRecibo / MovCaja / PagoCaja) 
            'Me.DocumentoReciboEgresoDeshacerMov(objDocumento.Id, True, True, True, True, True, False, cn, tr)
            Dim objDC As New DAODatosCancelacion
            objDC.DeshacerMov(objDocumento.Id, True, True, True, True, True, True, True, True, True, False, cn, tr)

            '************* ACTUALIZAMOS la cabecera
            MyBase.DocumentoUpdate(objDocumento, cn, tr)

            '************* Insertamos el DETALLE CONCEPTO
            InsertListDetalleConceptoxIdDocumento_CancelacionBancos(listaDetalleConcepto, objDocumento.Id, cn, tr)

            '*********** Insertamos la cancelación - Mov Caja - Pago Caja
            'If (objMovCaja IsNot Nothing) Then

            '    '*********** Inserto Mov Caja               
            '    InsertMovCajaxIdDocumento(objMovCaja, objDocumento.Id, cn, tr)
            '    '*********** Inserto Lista Cancelacion
            '    InsertListCancelacion_PagoCaja(listaCancelacion, objDocumento.Id, cn, tr)

            'End If
            InsertListCancelacion_DatosCancelacion(listaCancelacion, objDocumento.Id, cn, tr)

            InsertObservacion(objObservaciones, objDocumento.Id, cn, tr)
            ''Si el tipo documento es cheque - egreso (idtipodocumento=42), no debe haber movimientos bancarios
            ''porque ya se realizo los mismos en la creación de ese cheque.
            'If objDocumento.IdTipoDocumento <> 42 Then

            Dim objDaoAnexoMovBanco As New DAO.DAOAnexo_MovBanco
            Dim objDaoDocumentoFacturacion As New DAO.DAODocumentoFacturacion
            For i As Integer = 0 To listaMovBanco.Count - 1
                listaMovBanco(i).IdMovBanco = objDaoDocumentoFacturacion.MovBancoInsert_Venta(listaMovBanco(i), cn, tr) '******** INSERTAMOS MOV BANCO

                If (listaMovBanco(i).IdMovBanco <> Nothing) Then
                    objDaoAnexoMovBanco.Anexo_MovBancoInsert(New Entidades.Anexo_MovBanco(listaMovBanco(i).IdMovBanco, listaMovBanco(i).IdCuentaBancaria, Nothing, objDocumento.Id, listaMovBanco(i).IdBanco), cn, tr)
                End If
            Next

            'End If
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objDocumento.Id

    End Function

    'ini Detalle Concepto
    Public Sub InsertListDetalleConceptoxIdDocumento_CancelacionBancos(ByVal list As List(Of Entidades.DetalleConcepto), ByVal IdDocumento As Integer, ByVal cnx As SqlConnection, ByVal trx As SqlTransaction)
        Try
            For i As Integer = 0 To list.Count - 1
                list(i).IdDocumento = IdDocumento
                Me.InsertDetalleConcepto_CancelacionBancos(list(i), cnx, trx)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub InsertDetalleConcepto_CancelacionBancos(ByVal obj As Entidades.DetalleConcepto, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim param() As SqlParameter = New SqlParameter(6) {}
        With DBUtil
            param(0) = .getParam(obj.IdDocumento, "@IdDocumento", SqlDbType.Int)
            param(1) = .getParam(obj.Concepto, "@dr_Concepto", SqlDbType.VarChar)
            param(2) = .getParam(obj.IdMoneda, "@IdMoneda", SqlDbType.Int)
            param(3) = .getParam(obj.Monto, "@dr_Monto", SqlDbType.Decimal)
            param(4) = .getParam(obj.IdConcepto, "@IdConcepto", SqlDbType.Int)
            param(5) = .getParam(obj.IdDocumentoRef, "@IdDocumentoAfecto", SqlDbType.Int)
            param(6) = .getParam(obj.IdMovCuentaRef, "@IdMovCtaPPAfecto", SqlDbType.Int)
        End With

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoCancelacionBancos_InsertDetalleConcepto", param)

    End Sub
    'fin



End Class
