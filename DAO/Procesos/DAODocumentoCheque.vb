﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




'**************************   MARTES 11 MAYO 2010 HORA 10_45 AM









Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAODocumentoCheque
    Inherits DAOMantenedor

    Public Sub DocumentoCheque_DeshacerMov(ByVal IdDocumento As Integer, ByVal deleteRelacionDocumento As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(1) {}
        p(0) = MyBase.getParam(IdDocumento, "@IdDocumento", SqlDbType.Int)
        p(1) = MyBase.getParam(deleteRelacionDocumento, "@deleteRelacionDocumento", SqlDbType.Bit)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoCheque_DeshacerMov", p)

    End Sub

    Public Function SelectxParams(ByVal x As Entidades.DocumentoCheque) As List(Of Entidades.DocumentoCheque)
        Cn = objConexion.ConexionSIGE()
        Dim Prm() As SqlParameter
        Prm = GetVectorParametros(CType(x, Entidades.DocumentoCheque), modo_query.Show)
        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()

                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "_DocumentoChequeSelectxParams", Prm)

                Dim Lista As New List(Of Entidades.DocumentoCheque)
                With lector
                    Do While .Read

                        Dim obj As New Entidades.DocumentoCheque

                        obj.Id = UCInt(.Item("IdDocumento"))
                        obj.ChequeNumero = UCStr(.Item("ChequeNumero"))
                        obj.IdBeneficiario = UCInt(.Item("IdBeneficiario"))
                        obj.Beneficiario = UCStr(.Item("Beneficiario"))
                        obj.Monto = UCDec(.Item("Monto"))
                        obj.IdMoneda = UCInt(.Item("IdMoneda"))
                        obj.Serie = UCStr(.Item("doc_Serie"))
                        obj.FechaACobrar = UCDate(.Item("FechaACobrar"))
                        obj.FechaEmision = UCDate(.Item("fechaemision"))
                        obj.FechaRegistro = UCDate(.Item("FechaRegistro"))
                        obj.IdSerieCheque = UCInt(.Item("IdSerieCheque"))

                        obj.IdEstadoDoc = UCInt(.Item("IdEstadoDoc"))
                        obj.IdEstadoEntrega = UCInt(.Item("IdEstadoEnt"))
                        obj.IdEstadoCancelacion = UCInt(.Item("IdEstadoCan"))

                        obj.NomEstadoDocumento = UCStr(.Item("NomEstadoDocumento"))
                        obj.NomEstadoEntregado = UCStr(.Item("NomEstadoEntregado"))
                        obj.NomEstadoCancelacion = UCStr(.Item("NomEstadoCancelacion"))
                        obj.TotalLetras = UCStr(.Item("doc_TotalLetras"))

                        obj.SerieCheque = (New DAOSerieChequeView).SelectxId(obj.IdSerieCheque)
                        obj.AnexoDoc = (New DAOAnexo_Documento).SelectxIdDocumento(obj.Id)
                        'Estos datos estan relacionados con la funcion clone, 
                        'si cambian estos datos, se debe cambiar tb esa funcion, se encuentra en la entidad

                        Lista.Add(obj)

                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAll() As List(Of Entidades.DocumentoCheque)

        Dim x As New Entidades.DocumentoCheque

        Return SelectxParams(x)

    End Function

    Public Function SelectxId(ByVal id As Integer) As Entidades.DocumentoCheque

        Dim x As New Entidades.DocumentoCheque
        x.Id = id

        Return SelectxParams(x).Item(0)

    End Function

    Public Function SelectxEstadoCancelacion(ByVal Cancelado As Boolean) As List(Of Entidades.DocumentoCheque)

        Dim x As New Entidades.DocumentoCheque
        If Cancelado Then
            x.IdEstadoCancelacion = 2
        Else
            x.IdEstadoCancelacion = 1
        End If

        Return SelectxParams(x)

    End Function

    Public Function SelectxIdBeneficiario(ByVal IdBeneficiario As Integer) As List(Of Entidades.DocumentoCheque)

        Dim x As New Entidades.DocumentoCheque

        x.IdBeneficiario = IdBeneficiario

        Return SelectxParams(x)

    End Function

    Public Function SelectxIdBeneficiarioxEstadoCancelacion(ByVal IdBeneficiario As Integer, ByVal Cancelado As Boolean) As List(Of Entidades.DocumentoCheque)

        Dim x As New Entidades.DocumentoCheque

        x.IdBeneficiario = IdBeneficiario
        If Cancelado Then
            x.IdEstadoCancelacion = 2
        Else
            x.IdEstadoCancelacion = 1
        End If
        Return SelectxParams(x)

    End Function

    Public Function SelectxIdBeneficiarioxEstadoCancelacion_Out_DatosCancelacion(ByVal IdBeneficiario As Integer, ByVal Cancelado As Boolean) As List(Of Entidades.DocumentoCheque)
        Dim IdEstadoCancelacion As Integer
        If Cancelado Then
            IdEstadoCancelacion = 2
        Else
            IdEstadoCancelacion = 1
        End If
        Return SelectxIdBeneficiarioxEstadoCancelacion_Out_DatosCancelacion(IdBeneficiario, IdEstadoCancelacion)

    End Function

    Public Function SelectxIdBeneficiarioxEstadoCancelacion_Out_DatosCancelacion(ByVal IdBeneficiario As Integer, ByVal IdEstadoCancelacion As Integer) As List(Of Entidades.DocumentoCheque)
        Cn = objConexion.ConexionSIGE()
        Dim Prm() As SqlParameter = New SqlParameter(1) {}
        Prm(0) = getParam(IdBeneficiario, "@IdBeneficiario", SqlDbType.Int)
        Prm(1) = getParam(IdEstadoCancelacion, "@IdEstadoCan", SqlDbType.Int)

        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()

                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, _
                "_DocumentoChequeSelectxIdBeneficiarioxEstadoCancelacion-DatosCancelacion", Prm)

                Dim Lista As New List(Of Entidades.DocumentoCheque)
                With lector
                    Do While .Read

                        Dim obj As New Entidades.DocumentoCheque

                        obj.Id = UCInt(.Item("IdDocumento"))
                        obj.ChequeNumero = UCStr(.Item("ChequeNumero"))
                        obj.IdBeneficiario = UCInt(.Item("IdBeneficiario"))
                        obj.Beneficiario = UCStr(.Item("Beneficiario"))
                        obj.Monto = UCDec(.Item("Monto"))
                        obj.IdMoneda = UCInt(.Item("IdMoneda"))
                        obj.Serie = UCStr(.Item("doc_Serie"))
                        obj.FechaACobrar = UCDate(.Item("FechaACobrar"))
                        obj.FechaEmision = UCDate(.Item("fechaemision"))
                        obj.FechaRegistro = UCDate(.Item("FechaRegistro"))
                        obj.IdSerieCheque = UCInt(.Item("IdSerieCheque"))

                        obj.IdEstadoDoc = UCInt(.Item("IdEstadoDoc"))
                        obj.IdEstadoEntrega = UCInt(.Item("IdEstadoEnt"))
                        obj.IdEstadoCancelacion = UCInt(.Item("IdEstadoCan"))

                        obj.NomEstadoDocumento = UCStr(.Item("NomEstadoDocumento"))
                        obj.NomEstadoEntregado = UCStr(.Item("NomEstadoEntregado"))
                        obj.NomEstadoCancelacion = UCStr(.Item("NomEstadoCancelacion"))
                        obj.TotalLetras = UCStr(.Item("doc_TotalLetras"))

                        obj.SerieCheque = (New DAOSerieChequeView).SelectxId(obj.IdSerieCheque)
                        obj.AnexoDoc = (New DAOAnexo_Documento).SelectxIdDocumento(obj.Id)
                        'Estos datos estan relacionados con la funcion clone, 
                        'si cambian estos datos, se debe cambiar tb esa funcion, se encuentra en la entidad

                        Lista.Add(obj)

                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function GetVectorParametros(ByVal x As Entidades.DocumentoCheque, ByVal op As modo_query) As SqlParameter()

        Dim Prm() As SqlParameter

        Select Case op
            Case modo_query.Show ', modo_query.Update, modo_query.Insert
                Prm = New SqlParameter(11) {}

                Prm(0) = getParam(x.Id, "@IdDocumento", SqlDbType.Int)
                Prm(1) = getParam(x.ChequeNumero, "@ChequeNumero", SqlDbType.VarChar)
                Prm(2) = getParam(x.IdBeneficiario, "@IdBeneficiario", SqlDbType.Int)
                Prm(3) = getParam(x.Beneficiario, "@Beneficiario", SqlDbType.VarChar)
                Prm(4) = getParam(x.Serie, "@doc_Serie", SqlDbType.VarChar)
                Prm(5) = getParam(x.FechaACobrar, "@FechaACobrar", SqlDbType.Date)
                Prm(6) = getParam(x.FechaEmision, "@fechaemision", SqlDbType.Date)
                Prm(7) = getParam(x.FechaRegistro, "@FechaRegistro", SqlDbType.Date)
                Prm(8) = getParam(x.IdSerieCheque, "@IdSerieCheque", SqlDbType.Int)
                Prm(9) = getParam(x.IdEstadoDoc, "@IdEstadoDoc", SqlDbType.Int)
                Prm(10) = getParam(x.IdEstadoEntrega, "@IdEstadoEnt", SqlDbType.Int)
                Prm(11) = getParam(x.IdEstadoCancelacion, "@IdEstadoCan", SqlDbType.Int)

                'Case modo_query.Delete
            Case Else
                Prm = Nothing
        End Select

        Return Prm
    End Function


    Public Function GenerarDocCodigoChequexIdSerieCheque(ByVal IdSerieCheque As Integer) As String

        Cn = objConexion.ConexionSIGE()

        Dim Prm() As SqlParameter = New SqlParameter(0) {}

        Prm(0) = getParam(IdSerieCheque, "@IdSerieCheque", SqlDbType.Int)

        Try
            Using Cn
                Cn.Open()
                Return CStr(SqlHelper.ExecuteScalar(Cn, CommandType.StoredProcedure, "_DocumentoChequeGenerarDocCodigoChequexIdSerieCheque", Prm))
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function

    Public Function ValidarNumeroCheque(ByVal IdDocumento As Integer, ByVal IdSerieCheque As Integer, ByVal ChequeNumero As String) As Boolean
        Dim validar As Boolean = 0
        Cn = objConexion.ConexionSIGE()
        Using cmd As New SqlCommand("_DocumentoChequeValidarNumeroCheque", Cn)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                .Parameters.Add(New SqlParameter("@IdDocumento", SqlDbType.Int)).Value = IdDocumento
                .Parameters.Add(New SqlParameter("@IdSerieCheque", SqlDbType.Int)).Value = IdSerieCheque
                .Parameters.Add(New SqlParameter("@ChequeNumero", SqlDbType.VarChar)).Value = ChequeNumero
            End With
            Try
                Cn.Open()
                validar = cmd.ExecuteScalar()
            Catch ex As Exception
                Throw ex
            End Try
        End Using
        Return validar


        'Dim Prm() As SqlParameter = New SqlParameter(2) {}

        'Prm(0) = getParam(IdDocumento, "@IdDocumento", SqlDbType.Int)
        'Prm(1) = getParam(IdSerieCheque, "@IdSerieCheque", SqlDbType.Int)
        'Prm(2) = getParam(ChequeNumero, "@ChequeNumero", SqlDbType.VarChar)

        'Try
        '    Using Cn
        '        Cn.Open()
        '        Return CBool(SqlHelper.ExecuteScalar(Cn, CommandType.StoredProcedure, "_DocumentoChequeValidarNumeroCheque", Prm))
        '    End Using
        'Catch ex As Exception
        '    Throw ex
        'Finally

        'End Try

    End Function

    Public Function DAO_EmitirCheque(ByVal cn As SqlConnection, ByVal IdMovCtaPP As Integer) As List(Of Entidades.DocumentoCheque)
        Dim lista As List(Of Entidades.DocumentoCheque) = Nothing
        Using cmd As New SqlCommand("SP_CHEQUE_EMITIR", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@IdMovCtaPP", DbType.Int32)).Value = IdMovCtaPP
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.DocumentoCheque)
                Dim idDocumento As Integer = rdr.GetOrdinal("idDocumento")
                Dim ban_Nombre As Integer = rdr.GetOrdinal("ban_Nombre")
                Dim cb_numero As Integer = rdr.GetOrdinal("cb_numero")
                Dim doc_Serie As Integer = rdr.GetOrdinal("doc_Serie")
                Dim ChequeNumero As Integer = rdr.GetOrdinal("ChequeNumero")
                Dim mon_Simbolo As Integer = rdr.GetOrdinal("mon_Simbolo")
                Dim Monto As Integer = rdr.GetOrdinal("Monto")
                Dim Beneficiario As Integer = rdr.GetOrdinal("Beneficiario")
                Dim FechaACobrar As Integer = rdr.GetOrdinal("FechaACobrar")
                Dim FechaEmision As Integer = rdr.GetOrdinal("FechaEmision")
                Dim NomEstadoDocumento As Integer = rdr.GetOrdinal("NomEstadoDocumento")
                Dim NomEstadoEntregado As Integer = rdr.GetOrdinal("NomEstadoEntregado")
                Dim NomEstadoCancelacion As Integer = rdr.GetOrdinal("NomEstadoCancelacion")
                Dim objetoDocumentoCheque As Entidades.DocumentoCheque = Nothing
                While rdr.Read()
                    objetoDocumentoCheque = New Entidades.DocumentoCheque
                    With objetoDocumentoCheque
                        .IdDocumento = rdr.GetInt32(idDocumento)
                        .banco_nombre = rdr.GetValue(ban_Nombre)
                        .ctaBancaria_nombre = rdr.GetString(cb_numero)
                        .Serie = rdr.GetString(doc_Serie)
                        .ChequeNumero = rdr.GetString(ChequeNumero)
                        .NomMoneda = rdr.GetString(mon_Simbolo)
                        .Monto = rdr.GetDecimal(Monto)
                        .Beneficiario = rdr.GetString(Beneficiario)
                        .FechaACobrar = rdr.GetDateTime(FechaACobrar)
                        .FechaEmision = rdr.GetDateTime(FechaEmision)
                        .NomEstadoDocumento = rdr.GetString(NomEstadoDocumento)
                        .NomEstadoEntregado = rdr.GetString(NomEstadoEntregado)
                        .NomEstadoCancelacion = rdr.GetString(NomEstadoCancelacion)
                    End With
                    lista.Add(objetoDocumentoCheque)
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function
End Class
