﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.







'*******************      VIERNES   19 MARZO 2010 HORA 10_22 AM


Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAODocumentoInvInicial

    Private cn As SqlConnection
    Private tr As SqlTransaction
    Private cmd As SqlCommand
    Private reader As SqlDataReader

    Public Function DocumentoInventarioInicialSelectIdDocumentoxParams(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdTipoDocumento As Integer) As Integer

        Dim IdDocumento As Integer = Nothing

        Try

            Dim p() As SqlParameter = New SqlParameter(2) {}

            p(0) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
            p(0).Value = IdEmpresa
            p(1) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
            p(1).Value = IdAlmacen
            p(2) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
            p(2).Value = IdTipoDocumento

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()

            IdDocumento = CInt(SqlHelper.ExecuteScalar(cn, CommandType.StoredProcedure, "_DocumentoInventarioInicialSelectIdDocumentoxParams", p))

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return IdDocumento

    End Function

    Public Sub DocumentoInvInicial_RegistrarDetalle(ByVal objDetalle As Entidades.DetalleDocumento, ByVal IdDocumento As Integer, ByVal InsertaMovAlmacen As Boolean, ByVal Factor As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdTipoOperacion As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)


        Dim p() As SqlParameter = New SqlParameter(10) {}

        p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        p(0).Value = IdDocumento

        p(1) = New SqlParameter("@IdProducto", SqlDbType.Int)
        p(1).Value = objDetalle.IdProducto

        p(2) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
        p(2).Value = objDetalle.IdUnidadMedida

        p(3) = New SqlParameter("@dc_Cantidad", SqlDbType.Decimal)
        p(3).Value = objDetalle.Cantidad

        p(4) = New SqlParameter("@dc_UMedida", SqlDbType.VarChar)
        p(4).Value = objDetalle.UMedida

        p(5) = New SqlParameter("@InsertaMovAlmacen", SqlDbType.Bit)
        p(5).Value = InsertaMovAlmacen

        p(6) = New SqlParameter("@Factor", SqlDbType.Int)
        p(6).Value = Factor

        p(7) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        p(7).Value = IdEmpresa

        p(8) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        p(8).Value = IdAlmacen

        p(9) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        p(9).Value = IdTipoOperacion

        p(10) = New SqlParameter("@CostoProducto", SqlDbType.Decimal)
        p(10).Value = objDetalle.PrecioCD

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoInvInicial_RegistrarDetalle", p)

    End Sub


End Class
