﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.







'**************** LUNES 17 05 2010 HORA 06 00 PM





Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAORequerimientoGasto

    Private cn As SqlConnection
    Private cmd As SqlCommand
    Private lector As SqlDataReader
    Private tr As SqlTransaction
    Private objconexion As New DAO.Conexion
    Private objDAOObs As DAOObservacion
    Private objDaoMantenedor As New DAO.DAOMantenedor

    '****************************   VERSION 2
    Public Sub ValSustento_RelacionDocumento(ByVal IdDocumentoRG As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(0) {}
        p(0) = objDaoMantenedor.getParam(IdDocumentoRG, "@IdDocumento_RG", SqlDbType.Int)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoRequerimientoGasto_ValSustento_RelacionDocumento", p)


    End Sub


    Public Function fx_getTotalSustentadoxIdDocumentoReqGasto(ByVal IdDocumento_RG As Integer) As Decimal

        Dim totalSustentado As Decimal = 0

        Try

            cn = objconexion.ConexionSIGE
            cn.Open()

            cmd = New SqlCommand("  select dbo.fx_getTotalSustentadoxIdDocumentoReqGasto(" + CStr(IdDocumento_RG) + ")  ", cn)
            cmd.CommandType = CommandType.Text

            totalSustentado = CDec(cmd.ExecuteScalar)

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return totalSustentado

    End Function

    Public Sub DocumentoRequerimientoGasto_UpdateMontoxSustentar(ByVal IdDocumentoRG As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(0) {}
        p(0) = objDaoMantenedor.getParam(IdDocumentoRG, "@IdDocumentoRG", SqlDbType.Int)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoRequerimientoGasto_UpdateMontoxSustentar", p)


    End Sub
    Public Sub DeshacerDocumentoRequerimientoGasto(ByVal IdDocumento As Integer, ByVal DeleteAnexoDocumento As Boolean, ByVal DeleteDetalleConcepto As Boolean, ByVal DeleteMovCuentaCxP As Boolean, ByVal DeleteObservacion As Boolean, ByVal Anular As Boolean, ByVal listaRelacionDocumento As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(6) {}
        p(0) = objDaoMantenedor.getParam(IdDocumento, "@IdDocumento", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(DeleteAnexoDocumento, "@DeleteAnexoDocumento", SqlDbType.Bit)
        p(2) = objDaoMantenedor.getParam(DeleteDetalleConcepto, "@DeleteDetalleConcepto", SqlDbType.Bit)
        p(3) = objDaoMantenedor.getParam(DeleteMovCuentaCxP, "@DeleteMovCuentaCxP", SqlDbType.Bit)
        p(4) = objDaoMantenedor.getParam(DeleteObservacion, "@DeleteObservacion", SqlDbType.Bit)
        p(5) = objDaoMantenedor.getParam(Anular, "@Anular", SqlDbType.Bit)
        p(6) = objDaoMantenedor.getParam(listaRelacionDocumento, "@DeleteRelacionDoc", SqlDbType.Bit)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DeshacerDocumentoRequerimientoGasto", p)

    End Sub
    Public Function DocumentoRequerimientoGasto_SelectAprobacion(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdArea As Integer, ByVal Opcion_Aprobacion As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal MontoTot As Decimal) As DataTable

        Dim ds As New DataSet

        Try

            Dim p() As SqlParameter = New SqlParameter(6) {}
            p(0) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdArea, "@IdArea", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(Opcion_Aprobacion, "@Opcion_Aprobacion", SqlDbType.Int)
            p(4) = objDaoMantenedor.getParam(FechaInicio, "@FechaInicio", SqlDbType.Date)
            p(5) = objDaoMantenedor.getParam(FechaFin, "@FechaFin", SqlDbType.Date)
            p(6) = objDaoMantenedor.getParam(MontoTot, "@monto", SqlDbType.Decimal)

            Dim cmd As New SqlCommand("_DocumentoRequerimientoGasto_SelectAprobacion", objconexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocumentoReqGasto")

        Catch ex As Exception
            Throw ex
        End Try

        Return ds.Tables("DT_DocumentoReqGasto")

    End Function



   

    Public Function getDataSet_DocRequeGastoV2(ByVal IdDocumento As Integer) As DataSet

        Dim cn As SqlConnection = objconexion.ConexionSIGE
        Dim DS_RequeGastoV2 As New DataSet
        Try
            Dim da As SqlDataAdapter = Nothing

            '*********** CABECERA
            Dim cmd As New SqlCommand("CR_DocRequeGastoV2", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            da = New SqlDataAdapter(cmd)
            da.Fill(DS_RequeGastoV2, "DT_DocRequeGastosV2")

            '********* DETALLE CONCEPTO
            cmd.CommandText = "CR_DocRequeGastoV2xCuerpo"
            da = New SqlDataAdapter(cmd)
            da.Fill(DS_RequeGastoV2, "DT_Cuerpo")

            ' ''************ DETALLE DATOS CANCELACION
            'cmd.CommandText = "CR_DocRequeGastoV2xObs"
            'da = New SqlDataAdapter(cmd)
            'da.Fill(DS_RequeGastoV2, "DT_Observacion")

        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return DS_RequeGastoV2

    End Function













#Region "Mantenimiento"

    Public Function InsertRequerimientoGasto(ByVal Obj As Entidades.RequerimientoGasto.CabeceraRequerimiento, _
                                             ByVal Lista As List(Of Entidades.RequerimientoGasto), ByVal objObs As Entidades.Observacion) As String
        Dim cad As String
        Dim paramentros() As SqlParameter = New SqlParameter(15) {}
        paramentros(0) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        paramentros(0).Value = IIf(Obj.IdEmpresa = Nothing, DBNull.Value, Obj.IdEmpresa)
        paramentros(1) = New SqlParameter("@IdTienda", SqlDbType.Int)
        paramentros(1).Value = IIf(Obj.IdTienda = Nothing, DBNull.Value, Obj.IdTienda)
        paramentros(2) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        paramentros(2).Value = IIf(Obj.IdTipoDocumento = Nothing, DBNull.Value, Obj.IdTipoDocumento)
        paramentros(3) = New SqlParameter("@IdSerie", SqlDbType.Int)
        paramentros(3).Value = IIf(Obj.IdSerie = Nothing, DBNull.Value, Obj.IdSerie)
        paramentros(4) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        paramentros(4).Value = IIf(Obj.IdTipoOperacion = Nothing, DBNull.Value, Obj.IdTipoOperacion)
        paramentros(5) = New SqlParameter("@doc_FechaEmision", SqlDbType.DateTime)
        paramentros(5).Value = IIf(Obj.FechaEmision = Nothing, DBNull.Value, Obj.FechaEmision)
        paramentros(6) = New SqlParameter("@IdArea", SqlDbType.Int)
        paramentros(6).Value = IIf(Obj.IdArea = Nothing, DBNull.Value, Obj.IdArea)
        paramentros(7) = New SqlParameter("@IdMedioPago", SqlDbType.Int)
        paramentros(7).Value = IIf(Obj.IdMedioPagoCredito = Nothing, DBNull.Value, Obj.IdMedioPagoCredito)
        paramentros(8) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        paramentros(8).Value = IIf(Obj.IdUsuario = Nothing, DBNull.Value, Obj.IdUsuario)
        paramentros(9) = New SqlParameter("@IdPersona", SqlDbType.Int)
        paramentros(9).Value = IIf(Obj.IdPersona = Nothing, DBNull.Value, Obj.IdPersona)
        paramentros(10) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        paramentros(10).Value = IIf(Obj.IdMoneda = Nothing, DBNull.Value, Obj.IdMoneda)
        paramentros(11) = New SqlParameter("@doc_Total", SqlDbType.Decimal)
        paramentros(11).Value = IIf(Obj.TotalAPagar = Nothing, DBNull.Value, Obj.TotalAPagar)
        paramentros(12) = New SqlParameter("@IdCentroCosto", SqlDbType.VarChar)
        paramentros(12).Value = IIf(Obj.IdCentroCosto = Nothing, DBNull.Value, Obj.IdCentroCosto)
        paramentros(13) = New SqlParameter("@idusuarioSupervisor", SqlDbType.Int)
        paramentros(13).Value = IIf(Obj.IdUsuarioSupervisor = Nothing, DBNull.Value, Obj.IdUsuarioSupervisor)
        paramentros(14) = New SqlParameter("@anex_Aprobar", SqlDbType.Bit)
        paramentros(14).Value = Obj.anex_Aprobar
        paramentros(15) = New SqlParameter("@enviarCuenta", SqlDbType.Bit)
        paramentros(15).Value = Obj.EnviarTablaCuenta

        cn = objconexion.ConexionSIGE
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            cmd = New SqlCommand("_InsertRequerimientoGasto", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(paramentros)
            cad = cmd.ExecuteScalar.ToString

            Dim iddocumento() As String = cad.Split(CChar(","))

            InsertDetRequerimientoGasto(CInt(iddocumento(0)), Lista, cn, tr)

            objDAOObs = New DAOObservacion
            objObs.IdDocumento = CInt(iddocumento(0))
            objDAOObs.InsertaObservacionT(cn, tr, objObs)

            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return cad
    End Function

    Public Function UpdateRequerimientoGasto(ByVal Obj As Entidades.RequerimientoGasto.CabeceraRequerimiento, _
                                             ByVal Lista As List(Of Entidades.RequerimientoGasto), ByVal objObs As Entidades.Observacion) As String
        Dim cad As String = String.Empty
        Dim parametros() As SqlParameter = New SqlParameter(11) {}
        parametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        parametros(0).Value = IIf(Obj.Id = Nothing, DBNull.Value, Obj.Id)
        parametros(1) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        parametros(1).Value = IIf(Obj.IdTipoOperacion = Nothing, DBNull.Value, Obj.IdTipoOperacion)
        parametros(2) = New SqlParameter("@doc_FechaEmision", SqlDbType.DateTime)
        parametros(2).Value = IIf(Obj.FechaEmision = Nothing, DBNull.Value, Obj.FechaEmision)
        parametros(3) = New SqlParameter("@IdArea", SqlDbType.Int)
        parametros(3).Value = IIf(Obj.IdArea = Nothing, DBNull.Value, Obj.IdArea)
        parametros(4) = New SqlParameter("@IdMedioPago", SqlDbType.Int)
        parametros(4).Value = IIf(Obj.IdMedioPagoCredito = Nothing, DBNull.Value, Obj.IdMedioPagoCredito)
        parametros(5) = New SqlParameter("@IdPersona", SqlDbType.Int)
        parametros(5).Value = IIf(Obj.IdPersona = Nothing, DBNull.Value, Obj.IdPersona)
        parametros(6) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        parametros(6).Value = IIf(Obj.IdMoneda = Nothing, DBNull.Value, Obj.IdMoneda)
        parametros(7) = New SqlParameter("@doc_Total", SqlDbType.Decimal)
        parametros(7).Value = IIf(Obj.TotalAPagar = Nothing, DBNull.Value, Obj.TotalAPagar)
        parametros(8) = New SqlParameter("@IdCentroCosto", SqlDbType.VarChar)
        parametros(8).Value = IIf(Obj.IdCentroCosto = Nothing, DBNull.Value, Obj.IdCentroCosto)
        parametros(9) = New SqlParameter("@IdUsuarioSupervisor", SqlDbType.Int)
        parametros(9).Value = IIf(Obj.IdUsuarioSupervisor = Nothing, DBNull.Value, Obj.IdUsuarioSupervisor)
        parametros(10) = New SqlParameter("@anex_Aprobar", SqlDbType.Bit)
        parametros(10).Value = Obj.anex_Aprobar
        parametros(11) = New SqlParameter("@enviarcuenta", SqlDbType.Bit)
        parametros(11).Value = Obj.EnviarTablaCuenta

        cn = objconexion.ConexionSIGE
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            cmd = New SqlCommand("_UpdateRequerimientoGasto", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(parametros)
            cad = cmd.ExecuteScalar.ToString

            InsertDetRequerimientoGasto(Obj.Id, Lista, cn, tr)

            objDAOObs = New DAOObservacion
            objObs.IdDocumento = Obj.Id
            objDAOObs.ActualizaObservacion(objObs)

            tr.Commit()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return cad
    End Function

    Public Sub InsertDetRequerimientoGasto(ByVal idDocumento As Integer, ByVal Lista As List(Of Entidades.RequerimientoGasto), _
                                            ByVal cnx As SqlConnection, ByVal trx As SqlTransaction)
        For Each obj As Entidades.RequerimientoGasto In Lista
            Dim parametros() As SqlParameter = New SqlParameter(8) {}
            parametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            parametros(0).Value = idDocumento
            parametros(1) = New SqlParameter("@IdConcepto", SqlDbType.Int)
            parametros(1).Value = IIf(obj.IdConcepto = Nothing, DBNull.Value, obj.IdConcepto)
            parametros(2) = New SqlParameter("@dr_Concepto", SqlDbType.VarChar)
            parametros(2).Value = IIf(obj.Descripcion = Nothing, DBNull.Value, obj.Descripcion)
            parametros(3) = New SqlParameter("@IdMotGasto", SqlDbType.Int)
            parametros(3).Value = IIf(obj.IdMotivo = Nothing, DBNull.Value, obj.IdMotivo)
            parametros(4) = New SqlParameter("@IdTipoDocumentoRef", SqlDbType.Int)
            parametros(4).Value = IIf(obj.IdTipoDocumento = Nothing, DBNull.Value, obj.IdTipoDocumento)
            parametros(5) = New SqlParameter("@dg_NroDocumento", SqlDbType.VarChar)
            parametros(5).Value = IIf(obj.NroDocumento = Nothing, DBNull.Value, obj.NroDocumento)
            parametros(6) = New SqlParameter("@IdMoneda", SqlDbType.Int)
            parametros(6).Value = IIf(obj.IdMoneda = Nothing, DBNull.Value, obj.IdMoneda)
            parametros(7) = New SqlParameter("@dr_Monto", SqlDbType.Decimal)
            parametros(7).Value = IIf(obj.Monto = Nothing, DBNull.Value, obj.Monto)
            parametros(8) = New SqlParameter("@IdProveedor", SqlDbType.Decimal)
            parametros(8).Value = IIf(obj.IdProveedor = Nothing, DBNull.Value, obj.IdProveedor)

            cmd = New SqlCommand("_InsertDetRequerimientoGasto", cnx, trx)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(parametros)
            cmd.ExecuteNonQuery()
        Next
    End Sub


#End Region


#Region "Marzo del 2010"

    Public Function RequerimientoGasto_Select_Aprobar(ByVal idempresa As Integer, _
                                                      ByVal Idtienda As Integer, ByVal idarea As Integer, _
                                                      ByVal autorizadas As Boolean) As List(Of Entidades.RequerimientoGasto.CabeceraRequerimiento)
        Dim Lista As New List(Of Entidades.RequerimientoGasto.CabeceraRequerimiento)
        cn = objconexion.ConexionSIGE
        cmd = New SqlCommand("_RequerimientoGasto_Select_Aprobar", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idempresa", idempresa)
        cmd.Parameters.AddWithValue("@Idtienda", Idtienda)
        cmd.Parameters.AddWithValue("@idarea", idarea)
        cmd.Parameters.AddWithValue("@autorizadas", autorizadas)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.RequerimientoGasto.CabeceraRequerimiento
                With obj
                    .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")), "", lector("doc_Codigo")))
                    .IdSerie = CInt(IIf(IsDBNull(lector("IdSerie")), 0, lector("IdSerie")))
                    .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")), "", lector("doc_Serie")))
                    .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")), Nothing, lector("doc_FechaEmision")))
                    .Total = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")), 0, lector("doc_TotalAPagar")))
                    .NombrePersona = CStr(IIf(IsDBNull(lector("beneficiario")), "", lector("beneficiario")))
                    .NomSupervisor = CStr(IIf(IsDBNull(lector("supervisor")), "", lector("supervisor")))
                    .anex_Aprobar = CBool(IIf(IsDBNull(lector("anex_Aprobar")), False, lector("anex_Aprobar")))
                End With
                Lista.Add(obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function


    Public Function selectRequerimientoGasto(ByVal idserie As Integer, ByVal codigo As Integer) As Entidades.RequerimientoGasto.CabeceraRequerimiento
        Dim obj As New Entidades.RequerimientoGasto.CabeceraRequerimiento
        cn = objconexion.ConexionSIGE
        cmd = New SqlCommand("_selectRequerimientoGasto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idserie", idserie)
        cmd.Parameters.AddWithValue("@codigo", codigo)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If lector.HasRows = True Then
                If lector.Read Then
                    With obj
                        .Id = CInt(lector("IdDocumento"))
                        .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")), "", lector("doc_Codigo")))
                        .IdSerie = CInt(IIf(IsDBNull(lector("IdSerie")), 0, lector("IdSerie")))
                        .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")), "", lector("doc_Serie")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")), Nothing, lector("doc_FechaEmision")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")), 0, lector("doc_TotalAPagar")))
                        .IdPersona = CInt(lector("Idpersona"))
                        .NombrePersona = CStr(IIf(IsDBNull(lector("beneficiario")), "", lector("beneficiario")))
                        .Ruc = CStr(IIf(IsDBNull(lector("Ruc")), "---", lector("Ruc")))
                        .Dni = CStr(IIf(IsDBNull(lector("Dni")), "---", lector("Dni")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")), 0, lector("IdMoneda")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector("IdEmpresa")), 0, lector("IdEmpresa")))
                        .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")), 0, lector("IdTienda")))
                        .IdArea = CInt(IIf(IsDBNull(lector("IdArea")), 0, lector("IdArea")))
                        .IdMedioPagoCredito = CInt(IIf(IsDBNull(lector("IdMedioPago")), 0, lector("IdMedioPago")))
                        .IdTipoOperacion = CInt(IIf(IsDBNull(lector("IdTipoOperacion")), 0, lector("IdTipoOperacion")))
                        .IdCentroCosto = CStr(IIf(IsDBNull(lector("IdCentroCosto")), "", lector("IdCentroCosto")))
                        'fecha de aprobacion
                        .FechaCancelacion = CDate(IIf(IsDBNull(lector("doc_FechaCancelacion")), Nothing, lector("doc_FechaCancelacion")))
                        .NomSupervisor = CStr(lector("nomSupervisor"))
                        .anex_Aprobar = CBool(IIf(IsDBNull(lector("anex_Aprobar")), False, lector("anex_Aprobar")))
                        .IdDocRelacionado = CInt(IIf(IsDBNull(lector("IdDocumento2")), 0, lector("IdDocumento2")))
                    End With
                End If
            Else
                Throw New Exception("No se hallaron registros")
            End If
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return obj
    End Function

    Public Function selectListaRequerimientoGasto(ByVal IdDocumento As Integer) As List(Of Entidades.RequerimientoGasto)
        Dim Lista As New List(Of Entidades.RequerimientoGasto)
        cn = objconexion.ConexionSIGE
        cmd = New SqlCommand("_SelectListaRequerimientoGasto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.RequerimientoGasto
                With obj
                    .IdDocumento = CInt(lector("IdDocumento"))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector("IdTipoDocumentoRef")), 0, lector("IdTipoDocumentoRef")))
                    .NroDocumento = CStr(IIf(IsDBNull(lector("dg_NroDocumento")), "", lector("dg_NroDocumento")))
                    .Descripcion = CStr(IIf(IsDBNull(lector("dr_Concepto")), "", lector("dr_Concepto")))
                    .IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")), 0, lector("IdMoneda")))
                    .Monto = CDec(IIf(IsDBNull(lector("dr_Monto")), 0, lector("dr_Monto")))
                    .IdConcepto = CInt(IIf(IsDBNull(lector("IdConcepto")), 0, lector("IdConcepto")))
                    .IdMotivo = CInt(IIf(IsDBNull(lector("IdMotGasto")), 0, lector("IdMotGasto")))
                    .IdProveedor = CInt(IIf(IsDBNull(lector("IdProveedor")), 0, lector("IdProveedor")))
                    .NomPersona = CStr(IIf(IsDBNull(lector("NomPersona")), "", lector("NomPersona")))
                End With
                Lista.Add(obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function

#End Region


End Class
