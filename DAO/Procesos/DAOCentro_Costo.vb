﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Imports System.Data.SqlClient
Public Class DAOCentro_Costo

    Private objConexion As New DAO.Conexion
    Private cn As SqlConnection
    Private cmd As SqlCommand
    Private tr As SqlTransaction = Nothing
    Private lector As SqlDataReader = Nothing
    Private Lista_Centro_costo As List(Of Entidades.Centro_costo)
    Private Obj_Centro_costo As Entidades.Centro_costo

#Region "Marzo del 2010"

    Public Function CentroCosto_idTienda_IdUsuario_IdArea(ByVal idtienda As Integer, _
                                                          ByVal idusuario As Integer, ByVal idarea As Integer) As String
        Dim Cad$
        Dim parametros() As SqlParameter = New SqlParameter(2) {}
        parametros(0) = New SqlParameter("@idTienda", SqlDbType.Int)
        parametros(1) = New SqlParameter("@idUsuario", SqlDbType.Int)
        parametros(2) = New SqlParameter("@idArea", SqlDbType.Int)
        parametros(0).Value = idtienda
        parametros(1).Value = idusuario
        parametros(2).Value = idarea

        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_CentroCosto_idTienda_IdUsuario_IdArea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametros)
        Try
            cn.Open()
            Cad = cmd.ExecuteScalar().ToString()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Cad
    End Function

    Public Sub GastoPorCentroCosto_Insert(ByVal cnx As SqlConnection, ByVal trx As SqlTransaction, _
                                          ByVal iddocumento As Integer, ByVal Lista As List(Of Entidades.Centro_costo))
        For Each obj As Entidades.Centro_costo In Lista
            cmd = New SqlCommand("_GastoPorCentroCosto_Insert", cnx, trx)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdCentroCosto", obj.codigo)
            cmd.Parameters.AddWithValue("@gcc_monto", obj.monto)
            cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)
            cmd.ExecuteNonQuery()
        Next
    End Sub

    Public Function GastoPorCentroCosto_Select_IdDocumento(ByVal idDocumento As Integer) As List(Of Entidades.Centro_costo)
        Lista_Centro_costo = New List(Of Entidades.Centro_costo)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_GastoPorCentroCosto_Select_IdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", idDocumento)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Obj_Centro_costo = New Entidades.Centro_costo
                With Obj_Centro_costo
                    .codigo = CStr(lector("IdCentroCosto"))
                    .strCod_UniNeg = CStr(IIf(IsDBNull(lector("strCod_UniNeg")), "---", lector("strCod_UniNeg")))
                    .strCod_DepFunc = CStr(IIf(IsDBNull(lector("strCod_DepFunc")), "---", lector("strCod_DepFunc")))
                    .strCod_SubCodigo2 = CStr(IIf(IsDBNull(lector("strCod_SubCodigo2")), "---", lector("strCod_SubCodigo2")))
                    .strCod_SubCodigo3 = CStr(IIf(IsDBNull(lector("strCod_SubCodigo3")), "---", lector("strCod_SubCodigo3")))
                    .strCod_SubCodigo4 = CStr(IIf(IsDBNull(lector("strCod_SubCodigo4")), "---", lector("strCod_SubCodigo4")))
                    .monto = CDec(IIf(IsDBNull(lector("gcc_monto")), 0, lector("gcc_monto")))
                End With
                Lista_Centro_costo.Add(Obj_Centro_costo)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista_Centro_costo
    End Function

#End Region


    Public Function Insert_Centro_Costo(ByVal obj As Entidades.Centro_costo) As Boolean
        Dim parametros() As SqlParameter = New SqlParameter(7) {}
        parametros(0) = New SqlParameter("@Cod_UniNeg", SqlDbType.Char)
        parametros(1) = New SqlParameter("@Cod_DepFunc", SqlDbType.Char)
        parametros(2) = New SqlParameter("@Cod_SubCodigo2", SqlDbType.Char)
        parametros(3) = New SqlParameter("@Cod_SubCodigo3", SqlDbType.Char)
        parametros(4) = New SqlParameter("@Cod_SubCodigo4", SqlDbType.Char)
        parametros(5) = New SqlParameter("@Nombre_CC", SqlDbType.VarChar)
        parametros(6) = New SqlParameter("@Estado_CC", SqlDbType.Bit)
        parametros(7) = New SqlParameter("@codigo", SqlDbType.Char)

        parametros(0).Value = IIf(obj.Cod_UniNeg = Nothing, DBNull.Value, obj.Cod_UniNeg)
        parametros(1).Value = IIf(obj.Cod_DepFunc = Nothing, DBNull.Value, obj.Cod_DepFunc)
        parametros(2).Value = IIf(obj.Cod_SubCodigo2 = Nothing, DBNull.Value, obj.Cod_SubCodigo2)
        parametros(3).Value = IIf(obj.Cod_SubCodigo3 = Nothing, DBNull.Value, obj.Cod_SubCodigo3)
        parametros(4).Value = IIf(obj.Cod_SubCodigo4 = Nothing, DBNull.Value, obj.Cod_SubCodigo4)
        parametros(5).Value = IIf(obj.Nombre_CC = Nothing, DBNull.Value, obj.Nombre_CC)
        parametros(6).Value = IIf(obj.Estado_CC = Nothing, False, obj.Estado_CC)
        parametros(7).Value = IIf(obj.codigo = Nothing, DBNull.Value, obj.codigo)

        Dim IdCentro_Costo As Integer = 0

        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_Insert_Centro_Costo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametros)
        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return True
    End Function

    Public Function Update_Centro_Costo(ByVal obj As Entidades.Centro_costo) As Boolean
        Dim parametros() As SqlParameter = New SqlParameter(7) {}
        parametros(0) = New SqlParameter("@Cod_UniNeg", SqlDbType.Char)
        parametros(1) = New SqlParameter("@Cod_DepFunc", SqlDbType.Char)
        parametros(2) = New SqlParameter("@Cod_SubCodigo2", SqlDbType.Char)
        parametros(3) = New SqlParameter("@Cod_SubCodigo3", SqlDbType.Char)
        parametros(4) = New SqlParameter("@Cod_SubCodigo4", SqlDbType.Char)
        parametros(5) = New SqlParameter("@Nombre_CC", SqlDbType.VarChar)
        parametros(6) = New SqlParameter("@Estado_CC", SqlDbType.Bit)
        parametros(7) = New SqlParameter("@codigo", SqlDbType.Char)

        parametros(0).Value = IIf(obj.Cod_UniNeg = Nothing, DBNull.Value, obj.Cod_UniNeg)
        parametros(1).Value = IIf(obj.Cod_DepFunc = Nothing, DBNull.Value, obj.Cod_DepFunc)
        parametros(2).Value = IIf(obj.Cod_SubCodigo2 = Nothing, DBNull.Value, obj.Cod_SubCodigo2)
        parametros(3).Value = IIf(obj.Cod_SubCodigo3 = Nothing, DBNull.Value, obj.Cod_SubCodigo3)
        parametros(4).Value = IIf(obj.Cod_SubCodigo4 = Nothing, DBNull.Value, obj.Cod_SubCodigo4)
        parametros(5).Value = IIf(obj.Nombre_CC = Nothing, DBNull.Value, obj.Nombre_CC)
        parametros(6).Value = IIf(obj.Estado_CC = Nothing, False, obj.Estado_CC)
        parametros(7).Value = IIf(obj.codigo = Nothing, DBNull.Value, obj.codigo)

        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_Update_Centro_Costo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametros)
        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return True
    End Function
    Public Function BuscarAutogeneradoXCodSubArea3(ByVal CodUnidadNegocio As String, ByVal CodsubCodigo As String, ByVal CodSubArea1 As String, ByVal CodSubArea2 As String) As String
        Dim Autogenerado As String = ""
        Dim parametros() As SqlParameter = New SqlParameter(3) {}
        parametros(0) = New SqlParameter("@Cod_UniNeg", SqlDbType.VarChar)
        parametros(1) = New SqlParameter("@cod_depfunc", SqlDbType.VarChar)
        parametros(2) = New SqlParameter("@Cod_SubCodigo2", SqlDbType.VarChar)
        parametros(3) = New SqlParameter("@Cod_SubCodigo3", SqlDbType.VarChar)

        parametros(0).Value = IIf(CodUnidadNegocio = "00", DBNull.Value, CodUnidadNegocio)
        parametros(1).Value = IIf(CodsubCodigo = "00", DBNull.Value, CodsubCodigo)
        parametros(2).Value = IIf(CodSubArea1 = "00", DBNull.Value, CodSubArea1)
        parametros(3).Value = IIf(CodSubArea2 = "00", DBNull.Value, CodSubArea2)
        'parametros(4).Value = IIf(obj.Cod_SubCodigo4 = "00", DBNull.Value, obj.Cod_SubCodigo4)
        'parametros(5).Value = IIf(obj.Estado = Nothing, False, obj.Estado)


        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("SelectAutogeneradoxCodSubArea3", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametros)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            lector.Read()
            Autogenerado = CStr(lector("Autogenerado"))

            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Autogenerado
    End Function

    Public Function BuscarAutogeneradoXCodSubArea2(ByVal CodUnidadNegocio As String, ByVal CodsubCodigo As String, ByVal CodSubArea1 As String) As String
        Dim Autogenerado As String = ""
        Dim parametros() As SqlParameter = New SqlParameter(2) {}
        parametros(0) = New SqlParameter("@Cod_UniNeg", SqlDbType.VarChar)
        parametros(1) = New SqlParameter("@cod_depfunc", SqlDbType.VarChar)
        parametros(2) = New SqlParameter("@Cod_SubCodigo2", SqlDbType.VarChar)

        parametros(0).Value = IIf(CodUnidadNegocio = "00", DBNull.Value, CodUnidadNegocio)
        parametros(1).Value = IIf(CodsubCodigo = "00", DBNull.Value, CodsubCodigo)
        parametros(2).Value = IIf(CodSubArea1 = "00", DBNull.Value, CodSubArea1)
        'parametros(3).Value = IIf(obj.Cod_SubCodigo3 = "00", DBNull.Value, obj.Cod_SubCodigo3)
        'parametros(4).Value = IIf(obj.Cod_SubCodigo4 = "00", DBNull.Value, obj.Cod_SubCodigo4)
        'parametros(5).Value = IIf(obj.Estado = Nothing, False, obj.Estado)


        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("SelectAutogeneradoxCodSubArea2", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametros)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            lector.Read()
            Autogenerado = CStr(lector("Autogenerado"))

            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Autogenerado
    End Function
    Public Function BuscarAutogeneradoXCodSubArea1(ByVal CodUnidadNegocio As String, ByVal CodsubCodigo As String) As String
        Dim Autogenerado As String = ""
        Dim parametros() As SqlParameter = New SqlParameter(1) {}
        parametros(0) = New SqlParameter("@Cod_UniNeg", SqlDbType.VarChar)
        parametros(1) = New SqlParameter("@cod_depfunc", SqlDbType.VarChar)

        parametros(0).Value = IIf(CodUnidadNegocio = "00", DBNull.Value, CodUnidadNegocio)
        parametros(1).Value = IIf(CodsubCodigo = "00", DBNull.Value, CodsubCodigo)
        'parametros(2).Value = IIf(CodsubCodigo = "00", DBNull.Value, CodsubCodigo)
        'parametros(3).Value = IIf(obj.Cod_SubCodigo3 = "00", DBNull.Value, obj.Cod_SubCodigo3)
        'parametros(4).Value = IIf(obj.Cod_SubCodigo4 = "00", DBNull.Value, obj.Cod_SubCodigo4)
        'parametros(5).Value = IIf(obj.Estado = Nothing, False, obj.Estado)


        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("SelectAutogeneradoxCodSubcodigo2", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametros)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            lector.Read()
            Autogenerado = CStr(lector("Autogenerado"))
            '.Cod_DepFunc = CStr(lector("Cod_DepFunc"))
            '.Cod_SubCodigo2 = CStr(lector("Cod_SubCodigo2"))
            '.Cod_SubCodigo3 = CStr(lector("Cod_SubCodigo3"))
            '.Cod_SubCodigo4 = CStr(lector("Cod_SubCodigo4"))
            '.strCod_UniNeg = CStr(IIf(IsDBNull(lector("strCod_UniNeg")), "---", lector("strCod_UniNeg")))
            '.strCod_DepFunc = CStr(IIf(IsDBNull(lector("strCod_DepFunc")), "---", lector("strCod_DepFunc")))
            '.strCod_SubCodigo2 = CStr(IIf(IsDBNull(lector("strCod_SubCodigo2")), "---", lector("strCod_SubCodigo2")))
            '.strCod_SubCodigo3 = CStr(IIf(IsDBNull(lector("strCod_SubCodigo3")), "---", lector("strCod_SubCodigo3")))
            '.strCod_SubCodigo4 = CStr(IIf(IsDBNull(lector("strCod_SubCodigo4")), "---", lector("strCod_SubCodigo4")))
            '.Estado_CC = CBool(IIf(IsDBNull(lector("Estado_CC")), False, lector("Estado_CC")))


            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Autogenerado
    End Function
    Public Function BuscarAutogeneradoXCodSubcodigo(ByVal CodUnidadNegocio As String) As String
        Dim Autogenerado As String = ""
        Dim parametros() As SqlParameter = New SqlParameter(0) {}
        parametros(0) = New SqlParameter("@Cod_UniNeg", SqlDbType.VarChar)
        parametros(0).Value = IIf(CodUnidadNegocio = "00", DBNull.Value, CodUnidadNegocio)
        'parametros(1).Value = IIf(obj.Cod_DepFunc = "00", DBNull.Value, obj.Cod_DepFunc)
        'parametros(2).Value = IIf(obj.Cod_SubCodigo2 = "00", DBNull.Value, obj.Cod_SubCodigo2)
        'parametros(3).Value = IIf(obj.Cod_SubCodigo3 = "00", DBNull.Value, obj.Cod_SubCodigo3)
        'parametros(4).Value = IIf(obj.Cod_SubCodigo4 = "00", DBNull.Value, obj.Cod_SubCodigo4)
        'parametros(5).Value = IIf(obj.Estado = Nothing, False, obj.Estado)


        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("SelectAutogeneradoxCodSubcodigo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametros)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            lector.Read()
            Autogenerado = CStr(lector("Autogenerado"))
            '.Cod_DepFunc = CStr(lector("Cod_DepFunc"))
            '.Cod_SubCodigo2 = CStr(lector("Cod_SubCodigo2"))
            '.Cod_SubCodigo3 = CStr(lector("Cod_SubCodigo3"))
            '.Cod_SubCodigo4 = CStr(lector("Cod_SubCodigo4"))
            '.strCod_UniNeg = CStr(IIf(IsDBNull(lector("strCod_UniNeg")), "---", lector("strCod_UniNeg")))
            '.strCod_DepFunc = CStr(IIf(IsDBNull(lector("strCod_DepFunc")), "---", lector("strCod_DepFunc")))
            '.strCod_SubCodigo2 = CStr(IIf(IsDBNull(lector("strCod_SubCodigo2")), "---", lector("strCod_SubCodigo2")))
            '.strCod_SubCodigo3 = CStr(IIf(IsDBNull(lector("strCod_SubCodigo3")), "---", lector("strCod_SubCodigo3")))
            '.strCod_SubCodigo4 = CStr(IIf(IsDBNull(lector("strCod_SubCodigo4")), "---", lector("strCod_SubCodigo4")))
            '.Estado_CC = CBool(IIf(IsDBNull(lector("Estado_CC")), False, lector("Estado_CC")))


            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Autogenerado
    End Function
    Public Function BuscarAutogeneradoXCodUniNegocio(ByVal CodUnidadNegocio As String) As String
        Dim Autogenerado As String = ""
        'Dim parametros() As SqlParameter = New SqlParameter(0) {}
        'parametros(0) = New SqlParameter("@Cod_UniNeg", SqlDbType.VarChar)
        'parametros(0).Value = IIf(obj.Cod_UniNeg = "00", DBNull.Value, obj.Cod_UniNeg)
        'parametros(1).Value = IIf(obj.Cod_DepFunc = "00", DBNull.Value, obj.Cod_DepFunc)
        'parametros(2).Value = IIf(obj.Cod_SubCodigo2 = "00", DBNull.Value, obj.Cod_SubCodigo2)
        'parametros(3).Value = IIf(obj.Cod_SubCodigo3 = "00", DBNull.Value, obj.Cod_SubCodigo3)
        'parametros(4).Value = IIf(obj.Cod_SubCodigo4 = "00", DBNull.Value, obj.Cod_SubCodigo4)
        'parametros(5).Value = IIf(obj.Estado = Nothing, False, obj.Estado)


        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("SelectAutogeneradoxCodUniNegocio", cn)
        cmd.CommandType = CommandType.StoredProcedure
        'cmd.Parameters.AddRange(parametros)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            lector.Read()


            Autogenerado = CStr(lector("Autogenerado"))
            '.Cod_DepFunc = CStr(lector("Cod_DepFunc"))
            '.Cod_SubCodigo2 = CStr(lector("Cod_SubCodigo2"))
            '.Cod_SubCodigo3 = CStr(lector("Cod_SubCodigo3"))
            '.Cod_SubCodigo4 = CStr(lector("Cod_SubCodigo4"))
            '.strCod_UniNeg = CStr(IIf(IsDBNull(lector("strCod_UniNeg")), "---", lector("strCod_UniNeg")))
            '.strCod_DepFunc = CStr(IIf(IsDBNull(lector("strCod_DepFunc")), "---", lector("strCod_DepFunc")))
            '.strCod_SubCodigo2 = CStr(IIf(IsDBNull(lector("strCod_SubCodigo2")), "---", lector("strCod_SubCodigo2")))
            '.strCod_SubCodigo3 = CStr(IIf(IsDBNull(lector("strCod_SubCodigo3")), "---", lector("strCod_SubCodigo3")))
            '.strCod_SubCodigo4 = CStr(IIf(IsDBNull(lector("strCod_SubCodigo4")), "---", lector("strCod_SubCodigo4")))
            '.Estado_CC = CBool(IIf(IsDBNull(lector("Estado_CC")), False, lector("Estado_CC")))


            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Autogenerado
    End Function


    Public Function Listar_centro_costo(ByVal obj As Entidades.Centro_costo) As List(Of Entidades.Centro_costo)
        Listar_centro_costo = New List(Of Entidades.Centro_costo)
        Dim parametros() As SqlParameter = New SqlParameter(5) {}
        parametros(0) = New SqlParameter("@Cod_UniNeg", SqlDbType.Char)
        parametros(1) = New SqlParameter("@Cod_DepFunc", SqlDbType.Char)
        parametros(2) = New SqlParameter("@Cod_SubCodigo2", SqlDbType.Char)
        parametros(3) = New SqlParameter("@Cod_SubCodigo3", SqlDbType.Char)
        parametros(4) = New SqlParameter("@Cod_SubCodigo4", SqlDbType.Char)
        parametros(5) = New SqlParameter("@estado", SqlDbType.Int)

        parametros(0).Value = IIf(obj.Cod_UniNeg = "00", DBNull.Value, obj.Cod_UniNeg)
        parametros(1).Value = IIf(obj.Cod_DepFunc = "00", DBNull.Value, obj.Cod_DepFunc)
        parametros(2).Value = IIf(obj.Cod_SubCodigo2 = "00", DBNull.Value, obj.Cod_SubCodigo2)
        parametros(3).Value = IIf(obj.Cod_SubCodigo3 = "00", DBNull.Value, obj.Cod_SubCodigo3)
        parametros(4).Value = IIf(obj.Cod_SubCodigo4 = "00", DBNull.Value, obj.Cod_SubCodigo4)
        parametros(5).Value = IIf(obj.Estado = Nothing, False, obj.Estado)


        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_Listar_centro_costo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametros)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Obj_Centro_costo = New Entidades.Centro_costo
                With Obj_Centro_costo
                    .Cod_UniNeg = CStr(lector("Cod_UniNeg"))
                    .Cod_DepFunc = CStr(lector("Cod_DepFunc"))
                    .Cod_SubCodigo2 = CStr(lector("Cod_SubCodigo2"))
                    .Cod_SubCodigo3 = CStr(lector("Cod_SubCodigo3"))
                    .Cod_SubCodigo4 = CStr(lector("Cod_SubCodigo4"))
                    .strCod_UniNeg = CStr(IIf(IsDBNull(lector("strCod_UniNeg")), "---", lector("strCod_UniNeg")))
                    .strCod_DepFunc = CStr(IIf(IsDBNull(lector("strCod_DepFunc")), "---", lector("strCod_DepFunc")))
                    .strCod_SubCodigo2 = CStr(IIf(IsDBNull(lector("strCod_SubCodigo2")), "---", lector("strCod_SubCodigo2")))
                    .strCod_SubCodigo3 = CStr(IIf(IsDBNull(lector("strCod_SubCodigo3")), "---", lector("strCod_SubCodigo3")))
                    .strCod_SubCodigo4 = CStr(IIf(IsDBNull(lector("strCod_SubCodigo4")), "---", lector("strCod_SubCodigo4")))
                    .Estado_CC = CBool(IIf(IsDBNull(lector("Estado_CC")), False, lector("Estado_CC")))
                    .IdCentroCosto = CInt(IIf(IsDBNull(lector("IdCentroCosto")), False, lector("IdCentroCosto")))
                End With
                Listar_centro_costo.Add(Obj_Centro_costo)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Listar_centro_costo
    End Function

    Public Function Listar_Cod_UniNeg_Centro_Costo(ByVal estado As Integer) As List(Of Entidades.Centro_costo)
        Lista_Centro_costo = New List(Of Entidades.Centro_costo)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_Listar_Cod_UniNeg_Centro_Costo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@estado", estado)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Obj_Centro_costo = New Entidades.Centro_costo
                With Obj_Centro_costo
                    .Cod_UniNeg = CStr(lector("Cod_UniNeg"))
                    .Nombre_CC = CStr(lector("Nombre_CC"))
                End With
                Lista_Centro_costo.Add(Obj_Centro_costo)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista_Centro_costo
    End Function

    Public Function Listar_Cod_DepFunc_Centro_costo(ByVal Cod_UniNeg As String, ByVal estado As Integer) As List(Of Entidades.Centro_costo)
        Lista_Centro_costo = New List(Of Entidades.Centro_costo)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_Listar_Cod_DepFunc_Centro_costo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Cod_UniNeg", Cod_UniNeg)
        cmd.Parameters.AddWithValue("@estado", estado)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Obj_Centro_costo = New Entidades.Centro_costo
                With Obj_Centro_costo
                    .Cod_DepFunc = CStr(lector("Cod_DepFunc"))
                    .Nombre_CC = CStr(lector("Nombre_CC"))
                End With
                Lista_Centro_costo.Add(Obj_Centro_costo)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista_Centro_costo
    End Function

    Public Function Listar_Cod_SubCodigo2_Centro_costo(ByVal Cod_UniNeg As String, _
                                                       ByVal Cod_DepFunc As String, _
                                                       ByVal estado As Integer) As List(Of Entidades.Centro_costo)
        Lista_Centro_costo = New List(Of Entidades.Centro_costo)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_Listar_Cod_SubCodigo2_Centro_costo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Cod_UniNeg", Cod_UniNeg)
        cmd.Parameters.AddWithValue("@Cod_DepFunc", Cod_DepFunc)
        cmd.Parameters.AddWithValue("@estado", estado)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Obj_Centro_costo = New Entidades.Centro_costo
                With Obj_Centro_costo
                    .Cod_SubCodigo2 = CStr(lector("Cod_SubCodigo2"))
                    .Nombre_CC = CStr(lector("Nombre_CC"))
                End With
                Lista_Centro_costo.Add(Obj_Centro_costo)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista_Centro_costo
    End Function

    Public Function Listar_Cod_SubCodigo3_Centro_costo(ByVal Cod_UniNeg As String, _
                                                      ByVal Cod_DepFunc As String, _
                                                      ByVal Cod_SubCodigo2 As String, _
                                                      ByVal estado As Integer) As List(Of Entidades.Centro_costo)
        Lista_Centro_costo = New List(Of Entidades.Centro_costo)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_Listar_Cod_SubCodigo3_Centro_costo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Cod_UniNeg", Cod_UniNeg)
        cmd.Parameters.AddWithValue("@Cod_DepFunc", Cod_DepFunc)
        cmd.Parameters.AddWithValue("@Cod_SubCodigo2", Cod_SubCodigo2)
        cmd.Parameters.AddWithValue("@estado", estado)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Obj_Centro_costo = New Entidades.Centro_costo
                With Obj_Centro_costo
                    .Cod_SubCodigo3 = CStr(lector("Cod_SubCodigo3"))
                    .Nombre_CC = CStr(lector("Nombre_CC"))
                End With
                Lista_Centro_costo.Add(Obj_Centro_costo)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista_Centro_costo
    End Function

    Public Function Listar_Cod_SubCodigo4_Centro_costo(ByVal Cod_UniNeg As String, _
                                                      ByVal Cod_DepFunc As String, _
                                                      ByVal Cod_SubCodigo2 As String, _
                                                      ByVal Cod_SubCodigo3 As String, _
                                                      ByVal estado As Integer) As List(Of Entidades.Centro_costo)
        Lista_Centro_costo = New List(Of Entidades.Centro_costo)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_Listar_Cod_SubCodigo4_Centro_costo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Cod_UniNeg", Cod_UniNeg)
        cmd.Parameters.AddWithValue("@Cod_DepFunc", Cod_DepFunc)
        cmd.Parameters.AddWithValue("@Cod_SubCodigo2", Cod_SubCodigo2)
        cmd.Parameters.AddWithValue("@Cod_SubCodigo3", Cod_SubCodigo3)
        cmd.Parameters.AddWithValue("@estado", estado)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Obj_Centro_costo = New Entidades.Centro_costo
                With Obj_Centro_costo
                    .Cod_SubCodigo4 = CStr(lector("Cod_SubCodigo4"))
                    .Nombre_CC = CStr(lector("Nombre_CC"))
                End With
                Lista_Centro_costo.Add(Obj_Centro_costo)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista_Centro_costo
    End Function

End Class
