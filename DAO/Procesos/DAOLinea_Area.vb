﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOLinea_Area

    Private cn As SqlConnection
    Private cmd As SqlCommand
    Private lector As SqlDataReader = Nothing
    Private tr As SqlTransaction = Nothing
    Private objConexion As New DAO.Conexion
    Private Lista As List(Of Entidades.Linea_Area)
    Private Obj As Entidades.Linea_Area

    Public Function ListarLineaxArea(ByVal idArea As Integer) As List(Of Entidades.Linea)
        Dim ListaLinea As New List(Of Entidades.Linea)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_ListarLineaxArea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idarea", idArea)
        Try
            cn.Open()
            lector = cmd.ExecuteReader
            Do While lector.Read
                Dim ObjLinea As New Entidades.Linea
                With ObjLinea
                    .Id = CInt(lector("IdLinea"))
                    .Descripcion = CStr(lector("lin_Nombre"))
                End With
                ListaLinea.Add(ObjLinea)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return ListaLinea
    End Function

    Public Function ListarTipoexistenciaxArea(ByVal idArea As Integer) As List(Of Entidades.TipoExistencia)
        Dim ListaTipoexistencia As New List(Of Entidades.TipoExistencia)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_ListarTipoexistenciaxArea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idarea", idArea)
        Try
            cn.Open()
            lector = cmd.ExecuteReader
            Do While lector.Read
                Dim ObjTipoexistencia As New Entidades.TipoExistencia
                With ObjTipoexistencia
                    .Id = CInt(lector("IdTipoExistencia"))
                    .Nombre = CStr(lector("tex_Nombre"))
                End With
                ListaTipoexistencia.Add(ObjTipoexistencia)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return ListaTipoexistencia
    End Function
    Public Function ListarLineaxTipoexistenciaxArea(ByVal idArea As Integer, ByVal idTipoExistencia As Integer) As List(Of Entidades.Linea)
        Dim ListaLinea As New List(Of Entidades.Linea)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_ListarLineaxTipoexistenciaxArea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idarea", idArea)
        cmd.Parameters.AddWithValue("@idtipoexistencia", idTipoExistencia)
        Try
            cn.Open()
            lector = cmd.ExecuteReader
            Do While lector.Read
                Dim ObjLinea As New Entidades.Linea
                With ObjLinea
                    .Id = CInt(lector("IdLinea"))
                    .Descripcion = CStr(lector("lin_Nombre"))
                End With
                ListaLinea.Add(ObjLinea)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return ListaLinea
    End Function
    Public Function SelectLinea_Area_TipoExist(ByVal idTipoExistencia As Integer) As List(Of Entidades.Linea_Area)
        Lista = New List(Of Entidades.Linea_Area)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_SelectLinea_Area_TipoExist", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idTipoExistencia)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Obj = New Entidades.Linea_Area
                With Obj
                    .IdArea = CInt(lector("IdArea"))
                    .LAEstado = CBool(IIf(IsDBNull(lector("LAEstado")), False, lector("LAEstado")))
                End With
                Lista.Add(Obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function

    Public Function SelectLinea_Area(ByVal idLinea As Integer) As List(Of Entidades.Linea_Area)
        Lista = New List(Of Entidades.Linea_Area)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_SelectLinea_Area", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idLinea)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Obj = New Entidades.Linea_Area
                With Obj
                    .IdArea = CInt(lector("IdArea"))
                    .IdTipoExistencia = CInt(lector("IdTipoExistencia"))
                    .IdLinea = CInt(lector("IdLinea"))
                    .LAEstado = CBool(IIf(IsDBNull(lector("LAEstado")), False, lector("LAEstado")))
                End With
                Lista.Add(Obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function

    Public Function Insert_TipoExist_Linea_Area(ByVal cnx As SqlConnection, ByVal IdTipoExistencia As Integer, ByVal Lista As List(Of Entidades.Linea_Area), ByVal trx As SqlTransaction) As Boolean
        Try
            Dim borrar As Integer = 0
            If Lista.Count = 0 Then
                Dim parametros() As SqlParameter = New SqlParameter(2) {}
                parametros(0) = New SqlParameter("@IdTipoExistencia", SqlDbType.Int)
                parametros(0).Value = IdTipoExistencia
                parametros(1) = New SqlParameter("@IdArea", SqlDbType.Int)
                parametros(1).Value = DBNull.Value
                parametros(2) = New SqlParameter("@LAEstado", SqlDbType.Int)
                parametros(2).Value = DBNull.Value
                parametros(2) = New SqlParameter("@borrar", SqlDbType.Int)
                parametros(2).Value = DBNull.Value

                cmd = New SqlCommand("_Insert_TipoExist_Linea_Area", cnx, trx)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(parametros)
                cmd.ExecuteNonQuery()
            End If
            For Each obj As Entidades.Linea_Area In Lista
                Dim parametros() As SqlParameter = New SqlParameter(3) {}
                parametros(0) = New SqlParameter("@IdTipoExistencia", SqlDbType.Int)
                parametros(0).Value = IdTipoExistencia
                parametros(1) = New SqlParameter("@IdArea", SqlDbType.Int)
                parametros(1).Value = IIf(obj.IdArea = Nothing, DBNull.Value, obj.IdArea)
                parametros(2) = New SqlParameter("@LAEstado", SqlDbType.Int)
                parametros(2).Value = IIf(obj.LAEstado = Nothing, DBNull.Value, obj.LAEstado)
                parametros(3) = New SqlParameter("@borrar", SqlDbType.Int)
                parametros(3).Value = IIf(borrar = 0, DBNull.Value, borrar)

                cmd = New SqlCommand("_Insert_TipoExist_Linea_Area", cnx, trx)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(parametros)
                borrar = CInt(cmd.ExecuteScalar)
            Next
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return True
    End Function


    Public Function InsertLinea_Area(ByVal cnx As SqlConnection, ByVal IdLinea As Integer, ByVal Lista As List(Of Entidades.Linea_Area), ByVal trx As SqlTransaction) As Boolean
        Try
            For Each obj As Entidades.Linea_Area In Lista
                Dim parametros() As SqlParameter = New SqlParameter(2) {}
                parametros(0) = New SqlParameter("@IdLinea", SqlDbType.Int)
                parametros(0).Value = IdLinea
                parametros(1) = New SqlParameter("@IdArea", SqlDbType.Int)
                parametros(1).Value = IIf(obj.IdArea = Nothing, DBNull.Value, obj.IdArea)
                parametros(2) = New SqlParameter("@LAEstado", SqlDbType.Int)
                parametros(2).Value = IIf(obj.LAEstado = Nothing, DBNull.Value, obj.LAEstado)

                cmd = New SqlCommand("_InsertLinea_Area", cnx, trx)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(parametros)
                cmd.ExecuteNonQuery()
            Next
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return True
    End Function


    Public Function UpdateLinea_Area(ByVal cnx As SqlConnection, ByVal idLinea As Integer, ByVal Lista As List(Of Entidades.Linea_Area), ByVal trx As SqlTransaction) As Boolean
        Try
            Dim borrartodo As Integer = 0

            If Lista.Count = 0 Then
                cmd = New SqlCommand("_deleteLinea_Area", cnx, trx)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@IdLinea", idLinea)
                cmd.ExecuteNonQuery()
            End If
            For Each obj As Entidades.Linea_Area In Lista
                Dim parametros() As SqlParameter = New SqlParameter(3) {}
                parametros(0) = New SqlParameter("@IdLinea", SqlDbType.Int)
                parametros(0).Value = idLinea
                parametros(1) = New SqlParameter("@IdArea", SqlDbType.Int)
                parametros(1).Value = IIf(obj.IdArea = Nothing, DBNull.Value, obj.IdArea)
                parametros(2) = New SqlParameter("@LAEstado", SqlDbType.Int)
                parametros(2).Value = IIf(obj.LAEstado = Nothing, DBNull.Value, obj.LAEstado)
                parametros(3) = New SqlParameter("@update", SqlDbType.Int)
                parametros(3).Value = IIf(borrartodo = 0, DBNull.Value, borrartodo)

                cmd = New SqlCommand("_UpdateLinea_Area", cnx, trx)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(parametros)
                borrartodo = CInt(cmd.ExecuteScalar())
            Next
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return True
    End Function

End Class
