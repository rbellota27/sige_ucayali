﻿Imports System.Data.SqlClient
Public Class DAO_Sector_Objetos
    Public Function DAO_ListarSector(ByVal cn As SqlConnection) As List(Of Entidades.be_Sector)
        Dim lista As List(Of Entidades.be_Sector) = Nothing
        Using cmd As New SqlCommand("SP_DROPDOWNLIST_FILTRO")
            With cmd
                .Connection = cn
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@FITLRO", SqlDbType.VarChar, 200, ParameterDirection.Input)).Value = ""
                .Parameters.Add(New SqlParameter("@TABLA", SqlDbType.VarChar, 50, ParameterDirection.Input)).Value = "TBL_ALMACEN_SECTOR"
            End With
            Dim lector As SqlDataReader
            Try
                lector = cmd.ExecuteReader(CommandBehavior.SingleResult)
                If lector IsNot Nothing Then
                    lista = New List(Of Entidades.be_Sector)
                    Dim idSector As Integer = lector.GetOrdinal("id_sector")
                    Dim nom_sector As Integer = lector.GetOrdinal("nom_sector")
                    Dim objSector As Entidades.be_Sector
                    While lector.Read()
                        objSector = New Entidades.be_Sector()
                        With objSector
                            .idSector = lector.GetInt32(idSector)
                            .nomSector = lector.GetString(nom_sector)
                            lista.Add(objSector)
                        End With
                    End While
                    lector.Close()
                End If
                Return lista
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function
End Class
