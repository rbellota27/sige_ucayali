﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOProductoTipoPVRetazo
    Private objConexion As New Conexion
    Public Function SelectRetazoxSubLineaxTiendaxTipoPV(ByVal idsublinea As Integer, ByVal idtienda As Integer, ByVal idtipopv As Integer) As List(Of Entidades.ProductoTipoPVRetazo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoTipoPVSelectRetazoxSubLineaxTiendaxTipoPV", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        cmd.Parameters.AddWithValue("@IdTienda", idtienda)
        cmd.Parameters.AddWithValue("@IdTipoPv", idtipopv)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ProductoTipoPVRetazo)
                Do While lector.Read
                    Dim obj As New Entidades.ProductoTipoPVRetazo
                    With obj

                        .IdProducto = CInt(IIf(IsDBNull(lector("IdProducto")) = True, 0, lector("IdProducto")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector("IdUMRetazo")) = True, 0, lector("IdUMRetazo")))
                        .PrecioFinal = CDec(IIf(IsDBNull(lector("PVRetazo")) = True, 0, lector("PVRetazo")))
                        .Estado = CStr(IIf(IsDBNull(lector("ppv_Estado")) = True, 1, lector("ppv_Estado")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector("IdMonedaPV_UMPrincipal")) = True, 0, lector("IdMonedaPV_UMPrincipal")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector("NomMonedaPV_UMPrincipal")) = True, "", lector("NomMonedaPV_UMPrincipal")))
                        .NomUMPrincipal = CStr(IIf(IsDBNull(lector("NomUMPrincipal")) = True, 0, lector("NomUMPrincipal")))
                        .PV_UMPrincipal = CDec(IIf(IsDBNull(lector("PV_UMPrincipal")) = True, 0, lector("PV_UMPrincipal")))
                        .PorcentAdicionalRetazo = CDec(IIf(IsDBNull(lector("pum_PorcentRetazo")) = True, 0, lector("pum_PorcentRetazo")))
                        .Equivalencia = CDec(IIf(IsDBNull(lector("pum_Equivalencia")) = True, 0, lector("pum_Equivalencia")))
                        .NomUnidadMedida = CStr(IIf(IsDBNull(lector("NomUMRetazo")) = True, "", lector("NomUMRetazo")))
                        .NomProducto = CStr(IIf(IsDBNull(lector("prod_Nombre")) = True, "", lector("prod_Nombre")))
                        .PrecioCompra = CDec(IIf(IsDBNull(lector("PC_UMPrincipal")) = True, 0, lector("PC_UMPrincipal")))
                        .IdMonedaPC_UMPrincipal = CInt(IIf(IsDBNull(lector("IdMonedaPC_UMPrincipal")) = True, 0, lector("IdMonedaPC_UMPrincipal")))
                        .NomMonedaPC_UMPrincipal = CStr(IIf(IsDBNull(lector("NomMonedaPC_UMPrincipal")) = True, "", lector("NomMonedaPC_UMPrincipal")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectPorcentRetazoxIdSubLinea(ByVal idsublinea As Integer) As List(Of Entidades.ProductoTipoPVRetazo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoUMViewSelectPorcentRetazoxIdSubLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ProductoTipoPVRetazo)
                Do While lector.Read
                    Dim obj As New Entidades.ProductoTipoPVRetazo
                    With obj
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .NomUnidadMedida = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                        .Equivalencia = CDec(IIf(IsDBNull(lector.Item("pum_Equivalencia")) = True, 0, lector.Item("pum_Equivalencia")))
                        .Estado = CStr(IIf(IsDBNull(lector.Item("pum_Estado")) = True, "0", lector.Item("pum_Estado")))
                        .PorcentAdicionalRetazo = CDec(IIf(IsDBNull(lector.Item("pum_PorcentRetazo")) = True, 0, lector.Item("pum_PorcentRetazo")))
                        .NomProducto = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                        .IdSubLInea = CInt(IIf(IsDBNull(lector.Item("IdSubLInea")) = True, 0, lector.Item("IdSubLInea")))
                        .PrecioCompra = CDec(IIf(IsDBNull(lector.Item("prod_PrecioCompra")) = True, 0, lector.Item("prod_PrecioCompra")))
                        .NomUMPrincipal = CStr(IIf(IsDBNull(lector.Item("UMPrincipal")) = True, "", lector.Item("UMPrincipal")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
