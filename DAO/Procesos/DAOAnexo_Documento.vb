﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOAnexo_Documento
    Inherits DAOMantenedor

    Private cmd As SqlCommand
    'Dim ObjConexion As New Conexion    'Ya esta definido en la clase padre
    Dim HDAO As New DAO.HelperDAO
    Dim reader As SqlDataReader

    Public Function UpdateProgramacionAprobacion(ByVal IdDocumento As Integer) As Entidades.Anexo_Documento

        Cn = objConexion.ConexionSIGE()

        Dim Prm As SqlParameter = getParam(IdDocumento, "@IdDocumento", SqlDbType.Int)

        Try
            reader = SqlHelper.ExecuteReader(MyBase.Cn, CommandType.StoredProcedure, "Aprobacion_ProgramacionPago", Prm)
        

        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function
    Public Function SelectTonosxProducto_Nuevo(ByVal idProducto As Integer, ByVal idAlmacen As Integer, idDocumento As Integer, _
                                          ByVal cn As SqlConnection) As List(Of Entidades.be_tonoXProducto)
        Dim lista As List(Of Entidades.be_tonoXProducto) = Nothing
        Using cmd As New SqlCommand("SP_SELECT_TONO_X_PRODUCTO_NUEVO_V2", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@ID_PRODUCTO", idProducto)
                .Parameters.AddWithValue("@ID_ALMACEN", idAlmacen)
                .Parameters.AddWithValue("@idDocumento", idDocumento)
            End With
            Dim lector As SqlDataReader
            Try
                cn.Open()
                lector = cmd.ExecuteReader(Data.IsolationLevel.Serializable)
                If lector IsNot Nothing Then
                    lista = New List(Of Entidades.be_tonoXProducto)
                    Dim idTono As Integer = lector.GetOrdinal("idTono")
                    Dim idSector As Integer = lector.GetOrdinal("idsector")
                    Dim idProductosss As Integer = lector.GetOrdinal("idproducto")
                    Dim idAlmacennn As Integer = lector.GetOrdinal("idalmacen")
                    Dim nomTono As Integer = lector.GetOrdinal("NOM_TONO")
                    Dim descTono As Integer = lector.GetOrdinal("DESC_TONO")
                    Dim cantidad As Integer = lector.GetOrdinal("cantidad")
                    Dim objTonos As New Entidades.be_tonoXProducto
                    While lector.Read()
                        objTonos = New Entidades.be_tonoXProducto()
                        objTonos.idTono = lector.GetInt32(idTono)
                        objTonos.idSector = lector.GetInt32(idSector)
                        objTonos.idProducto = lector.GetInt32(idProductosss)
                        objTonos.idAlmacen = lector.GetInt32(idAlmacennn)
                        objTonos.nomtono = lector.GetString(nomTono)
                        objTonos.descTono = lector.GetString(descTono)
                        objTonos.cantidadTono = lector.GetDecimal(cantidad)
                        lista.Add(objTonos)
                    End While
                    lector.Close()
                End If

                Return lista
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function
    'Public Function MarcarProgramacionAprobacion() As Boolean
    '    Try
    '        ' cn = objConexion.ConexionSIGE()
    '        'cn.Open()
    '        'Dim Prm As SqlParameter = getParam(IdDocumento, "@IdDocumento", SqlDbType.Int)

    '        'Dim Arrayprm() As SqlParameter = New SqlParameter(1) {}
    '        'Arrayprm(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
    '        'Arrayprm(0).Value = IdDocumento

    '        'Arrayprm(1) = New SqlParameter("@estado", SqlDbType.Int)
    '        'Arrayprm(1).Value = estado


    '        'HDAO.Update(cn, "MarcarAprobacion_ProgramacionPago", Arrayprm)
    '        'cn.Close()

    '--
    '        Dim cn As SqlConnection = objConexion.ConexionSIGE
    '        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
    '        ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
    '        ArrayParametros(0).Value = objAnexoDoc.IdDocumento
    '        ArrayParametros(1) = New SqlParameter("@estado", SqlDbType.Int)
    '        ArrayParametros(1).Value = objAnexoDoc.estado
    '        Return HDAO.Update(cn, "MarcarAprobacion_ProgramacionPago", ArrayParametros)
    '        --




    '    Catch ex As Exception
    '        Throw ex
    '    Finally

    '    End Try

    'End Function

    Public Function MarcarProgramacionAprobacion(ByVal IdDocumento As Integer, ByVal estado As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Try
            Dim retornar As Integer = 0
            Using cmd As New SqlCommand("MarcarAprobacion_ProgramacionPago", cn, tr)
                cmd.CommandType = CommandType.StoredProcedure
                With cmd
                    .Parameters.Add(New SqlParameter("@IdDocumento", SqlDbType.Int)).Value = IdDocumento
                    .Parameters.Add(New SqlParameter("@estado", SqlDbType.Int)).Value = estado
                End With
                Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleRow)
                If rdr IsNot Nothing Then
                    While rdr.Read()
                        retornar = CBool(rdr.GetInt32(0))
                    End While
                End If
                rdr.Close()
            End Using
            Return retornar
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function Anexo_DocumentoSelectxIdDocumento(ByVal IdDocumento As Integer) As Entidades.Anexo_Documento

        Dim objAnexo As Entidades.Anexo_Documento = Nothing

        Try

            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            p(0).Value = IdDocumento

            MyBase.Cn = (New DAO.Conexion).ConexionSIGE
            MyBase.Cn.Open()

            reader = SqlHelper.ExecuteReader(MyBase.Cn, CommandType.StoredProcedure, "_Anexo_DocumentoSelectxIdDocumento", p)

            If (reader.Read) Then
                objAnexo = New Entidades.Anexo_Documento
                With objAnexo


                    .IdDocumento = MyBase.UCInt(reader("IdDocumento"))
                    .doc_Importacion = MyBase.UCBool(reader("doc_Importacion"))
                    .IdMedioPago = MyBase.UCInt(reader("IdMedioPago"))
                    .IdUsuarioSupervisor = MyBase.UCInt(reader("IdUsuarioSupervisor"))
                    .PrecImportacion = MyBase.UCStr(reader("anex_PrecImportacion"))
                    .NroContenedores = MyBase.UCStr(reader("anex_NroContenedores"))
                    .NroDiasVigencia = MyBase.UCDec(reader("anex_NroDiasVigencia"))
                    .IdArea = MyBase.UCInt(reader("IdArea"))
                    .TotalConcepto = MyBase.UCDec(reader("anex_TotalConcepto"))
                    .IdAlias = MyBase.UCInt(reader("IdAlias"))
                    .IdCentroCosto = MyBase.UCInt(reader("IdCentroCosto"))
                    .IdMaestroObra = MyBase.UCInt(reader("IdMaestroObra"))
                    .IdTPImp = MyBase.UCInt(reader("IdTPImp"))
                    .IdSerieCheque = MyBase.UCInt(reader("IdSerieCheque"))
                    .Aprobar = MyBase.UCBool(reader("anex_Aprobar"))
                    .IdYear = MyBase.UCInt(reader("IdYear"))
                    .IdSemana = MyBase.UCInt(reader("IdSemana"))
                    .FechaAprobacion = MyBase.UCDate(reader("anex_FechaAprob"))
                    .MontoAfectoIgv = MyBase.UCDec(reader("anex_MontoAfectoIgv"))
                    .MontoNoAfectoIgv = MyBase.UCDec(reader("anex_MontoNoAfectoIgv"))
                    .MontoISC = MyBase.UCDec(reader("montoISC"))
                    .MontoOtroTributo = MyBase.UCDec(reader("montoOtroTributo"))
                    .MontoxSustentar = MyBase.UCDec(reader("anex_MontoxSustentar"))
                    .EstadoRequerimiento = MyBase.UCInt(reader("est_reque"))
                    '.RetencionHonorarios = MyBase.UCDec(reader("RetencionHonorarios"))
                    .Banco = MyBase.UCStr(reader("Banco"))
                    .IdCuentaBancaria = MyBase.UCInt(reader("idCuentaBancaria"))
                    .NroCuentaBancaria = MyBase.UCStr(reader("CuentaBancaria"))
                    .Codigo = MyBase.UCStr(reader("anex_Codigo"))
                    .Serie = MyBase.UCStr(reader("anex_Serie"))
                    .NroOperacion = MyBase.UCStr(reader("anex_NroOperacion"))
                    .cb_SwiftBancario = MyBase.UCStr(reader("cb_SwiftBancario"))
                    .cb_CuentaInterbancaria = MyBase.UCStr(reader("cb_CuentaInterbancaria"))
                End With

            End If
            reader.Close()

        Catch ex As Exception

            Throw ex

        Finally

            If (MyBase.Cn.State = ConnectionState.Open) Then MyBase.Cn.Close()


        End Try

        Return objAnexo

    End Function
    Public Function InsertAnexo_Documento2(ByVal objEnt As Entidades.Anexo_Documento, _
                                         ByVal cn As SqlConnection, ByVal tr As SqlTransaction, _
                                         Optional ByVal useAlias_Random As Boolean = False) As Boolean

        Dim prm() As SqlParameter
        prm = GetVectorParametros2(objEnt, modo_query.Insert, useAlias_Random)

        Try
            cmd = New SqlCommand("_InsertAnexo_Documento2", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(prm)

            cmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return True
    End Function



    Public Function InsertAnexo_Documento(ByVal objEnt As Entidades.Anexo_Documento, _
                                          ByVal cn As SqlConnection, ByVal tr As SqlTransaction, _
                                          Optional ByVal useAlias_Random As Boolean = False) As Boolean
        Dim prm() As SqlParameter
        prm = GetVectorParametros(objEnt, modo_query.Insert, useAlias_Random)

        Try
            cmd = New SqlCommand("_InsertAnexo_Documento", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(prm)
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return True
    End Function

    Public Sub FacturacionDeleteMaestro(ByVal IdCotizacion As Integer, ByVal sqlcn As SqlConnection, ByVal sqltr As SqlTransaction)

        Try
            cmd = New SqlCommand("_FacturacionDeleteMaestro", sqlcn, sqltr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdCotizacion", IdCotizacion)

            cmd.ExecuteNonQuery()


        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub

    Public Function InsertAnexoDocT(ByVal cn As SqlConnection, ByVal vIdDocumento As Integer, ByVal objAnexoDoc As Entidades.Anexo_Documento, Optional ByVal T As SqlTransaction = Nothing) As Boolean
        Try
            Dim Arrayprm() As SqlParameter = New SqlParameter(4) {}
            Arrayprm(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            Arrayprm(0).Value = vIdDocumento

            Arrayprm(1) = New SqlParameter("@doc_Importacion", SqlDbType.Bit)
            Arrayprm(1).Value = objAnexoDoc.doc_Importacion

            Arrayprm(2) = New SqlParameter("@IdTPImp", SqlDbType.Int)
            Arrayprm(2).Value = objAnexoDoc.IdTPImp

            Arrayprm(3) = New SqlParameter("@anex_NroContenedores", SqlDbType.VarChar, 20)
            Arrayprm(3).Value = objAnexoDoc.NroContenedores

            'Aqui se guardar
            Arrayprm(4) = New SqlParameter("@anex_MontoxSustentar", SqlDbType.Decimal)
            Arrayprm(4).Value = objAnexoDoc.MontoxSustentar



            HDAO.InsertaT(cn, "_AnexoDocumentoInsert", Arrayprm, T)
            Return True
        Catch ex As Exception
            Throw ex
            Return False
        Finally

        End Try
    End Function
    Public Function ActualizaAnexoDocT(ByVal cn As SqlConnection, ByVal vIdDocumento As Integer, ByVal objAnexoDoc As Entidades.Anexo_Documento, Optional ByVal T As SqlTransaction = Nothing) As Boolean
        Try
            Dim Arrayprm() As SqlParameter = New SqlParameter(3) {}
            Arrayprm(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            Arrayprm(0).Value = vIdDocumento

            Arrayprm(1) = New SqlParameter("@doc_Importacion", SqlDbType.Bit)
            Arrayprm(1).Value = objAnexoDoc.doc_Importacion

            'Arrayprm(2) = New SqlParameter("@anex_PrecImportacion", SqlDbType.VarChar, 10)
            'Arrayprm(2).Value = objAnexoDoc.PrecImportacion
            Arrayprm(2) = New SqlParameter("@IdTPImp", SqlDbType.Int)
            Arrayprm(2).Value = objAnexoDoc.IdTPImp

            Arrayprm(3) = New SqlParameter("@anex_NroContenedores", SqlDbType.VarChar, 20)
            Arrayprm(3).Value = objAnexoDoc.NroContenedores

            HDAO.UpdateT(cn, "_AnexoDocumentoUpdate", Arrayprm, T)
            Return True
        Catch ex As Exception
            Throw ex
            Return False
        Finally

        End Try
    End Function

    Public Function DeletexIdDocumento(ByVal IdDocumento As Integer, ByVal cn As SqlConnection, Optional ByVal T As SqlTransaction = Nothing) As Boolean
        Try
            Dim Arrayprm() As SqlParameter
            Arrayprm = New SqlParameter(0) {}

            Arrayprm(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            Arrayprm(0).Value = getParamValueInt(IdDocumento)

            SqlHelper.ExecuteNonQuery(T, CommandType.StoredProcedure, "_AnexoDocumentoDeletexIdDocumento", Arrayprm)

            Return True
        Catch ex As Exception
            Throw ex
            Return False
        Finally

        End Try
    End Function

    Public Function SelectxIdDocumento(ByVal IdDocumento As Integer) As Entidades.Anexo_Documento
        'Cuando se modifique esta funcion, se debe actualizar tambien el metodo clone, que esta en la entidad
        Cn = objConexion.ConexionSIGE()

        Dim Prm As SqlParameter = getParam(IdDocumento, "@IdDocumento", SqlDbType.Int)
        'Dim Prm() As SqlParameter
        'Prm = New SqlParameter(0) {}
        'Prm(0) = getParam(IdDocumento, "@IdDocumento", SqlDbType.Int)

        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()

                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "_AnexoDocumentoSelectxIdDocumento", Prm)
                Dim obj As New Entidades.Anexo_Documento
                With lector
                    If .Read Then

                        obj.IdDocumento = IdDocumento
                        obj.doc_Importacion = UCBool(.Item("doc_Importacion"))
                        obj.IdMedioPago = UCInt(.Item("IdMedioPago"))
                        obj.IdUsuarioSupervisor = UCInt(.Item("IdUsuarioSupervisor"))
                        obj.PrecImportacion = UCStr(.Item("anex_PrecImportacion"))
                        obj.NroContenedores = UCStr(.Item("anex_NroContenedores"))
                        obj.NroDiasVigencia = UCInt(.Item("anex_NroDiasVigencia"))
                        obj.IdArea = UCInt(.Item("IdArea"))
                        obj.TotalConcepto = UCDec(.Item("anex_TotalConcepto"))

                        obj.IdAlias = UCInt(.Item("IdAlias"))
                        obj.IdCentroCosto = UCInt(.Item("IdCentroCosto"))
                        obj.IdMaestroObra = UCInt(.Item("IdMaestroObra"))
                        obj.IdTPImp = UCInt(.Item("IdTPImp"))

                        obj.IdSerieCheque = UCInt(.Item("IdSerieCheque"))

                        obj.Aprobar = UCBool(.Item("anex_Aprobar"))
                        obj.IdYear = UCInt(.Item("IdYear"))
                        obj.IdSemana = UCInt(.Item("IdSemana"))
                        obj.NroOperacion = UCStr(.Item("anex_NroOperacion"))

                    End If
                    .Close()
                End With

                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function GetVectorParametros2(ByVal x As Entidades.Anexo_Documento, ByVal op As modo_query, Optional ByVal useAlias_Random As Boolean = False) As SqlParameter()

        Dim Prm() As SqlParameter

        Select Case op
            Case modo_query.Update, modo_query.Insert

                If op = modo_query.Insert Then
                    Prm = New SqlParameter(33) {}
                    Prm(33) = getParam(useAlias_Random, "@useAlias_Random", SqlDbType.Bit)
                Else
                    Prm = New SqlParameter(32) {}
                End If

                Prm(0) = getParam(x.IdDocumento, "@IdDocumento", SqlDbType.Int)
                Prm(1) = New SqlParameter("@doc_Importacion", SqlDbType.Bit)
                Prm(1).Value = IIf(x.doc_Importacion = Nothing, DBNull.Value, x.doc_Importacion)
                'Prm(1) = getParam(x.doc_Importacion, "@doc_Importacion", SqlDbType.Bit)
                Prm(2) = getParam(x.IdMedioPago, "@IdMedioPago", SqlDbType.Int)
                Prm(3) = getParam(x.IdUsuarioSupervisor, "@IdUsuarioSupervisor", SqlDbType.Int)
                Prm(4) = getParam(x.PrecImportacion, "@anex_PrecImportacion", SqlDbType.Char)
                Prm(5) = getParam(x.NroContenedores, "@anex_NroContenedores", SqlDbType.VarChar)
                Prm(6) = getParam(x.NroDiasVigencia, "@anex_NroDiasVigencia", SqlDbType.Decimal)
                Prm(7) = getParam(x.TotalConcepto, "@anex_TotalConcepto", SqlDbType.Decimal)
                Prm(8) = getParam(x.IdArea, "@IdArea", SqlDbType.Int)
                Prm(9) = getParam(x.IdAlias, "@IdAlias", SqlDbType.Int)
                Prm(10) = getParam(x.IdMaestroObra, "@IdMaestroObra", SqlDbType.Int)
                Prm(11) = getParam(x.IdTPImp, "@IdTPImp", SqlDbType.Int)
                Prm(12) = getParam(x.IdSerieCheque, "@IdSerieCheque", SqlDbType.Int)
                Prm(13) = getParam(x.Aprobar, "@Aprobar", SqlDbType.Bit)
                Prm(14) = getParam(x.IdYear, "@IdYear", SqlDbType.Int)
                Prm(15) = getParam(x.IdSemana, "@IdSemana", SqlDbType.Int)
                Prm(16) = getParam(x.NroOperacion, "@anex_NroOperacion", SqlDbType.VarChar)
                Prm(17) = getParam(x.FechaAprobacion, "@anex_FechaAprobacion", SqlDbType.DateTime)

                Prm(18) = getParam(x.MontoAfectoIgv, "@anex_MontoAfectoIgv", SqlDbType.Decimal)
                Prm(19) = getParam(x.MontoNoAfectoIgv, "@anex_MontoNoAfectoIgv", SqlDbType.Decimal)

                Prm(20) = getParam(x.Liquidado, "@anex_Liquidado", SqlDbType.Bit)
                Prm(21) = getParam(x.MontoxSustentar, "@anex_MontoxSustentar", SqlDbType.Decimal)
                Prm(22) = getParam(x.Codigo, "@anex_Codigo", SqlDbType.VarChar)
                Prm(23) = getParam(x.Serie, "@anex_Serie", SqlDbType.VarChar)
                Prm(24) = getParam(x.MontoISC, "@montoISC", SqlDbType.Decimal)
                Prm(25) = getParam(x.MontoOtroTributo, "@montoOtroTributo", SqlDbType.Decimal)
                Prm(26) = getParam(x.anex_DescuentoGlobal, "@anex_DescuentoGlobal", SqlDbType.Decimal)
                Prm(27) = getParam(x.EstadoRequerimiento, "@est_reque", SqlDbType.Int)

                Prm(28) = getParam(x.Banco, "@banco", SqlDbType.VarChar)
                Prm(29) = getParam(x.IdCuentaBancaria, "@idCuentabancaria", SqlDbType.Int)
                Prm(30) = getParam(x.NroCuentaBancaria, "@Cuentabancaria", SqlDbType.VarChar)

                Prm(31) = getParam(x.cb_CuentaInterbancaria, "@cb_CuentaInterbancaria", SqlDbType.VarChar)
                Prm(32) = getParam(x.cb_SwiftBancario, "@cb_SwiftBancario", SqlDbType.VarChar)
                'Prm(26) = getParam(x.RetencionHonorarios, "@RetencionHonorariros", SqlDbType.Decimal)

                'Case modo_query.Delete
                '    Prm = New SqlParameter(0) {}
                '    Prm(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
                '    Prm(0).Value = getParamValueInt(x.IdDocumento)
            Case Else
                Prm = Nothing
        End Select

        Return Prm
    End Function


    Public Function GetVectorParametros(ByVal x As Entidades.Anexo_Documento, ByVal op As modo_query, Optional ByVal useAlias_Random As Boolean = False) As SqlParameter()

        Dim Prm() As SqlParameter

        Select Case op
            Case modo_query.Update, modo_query.Insert

                If op = modo_query.Insert Then
                    Prm = New SqlParameter(28) {}
                    Prm(28) = getParam(useAlias_Random, "@useAlias_Random", SqlDbType.Bit)
                Else
                    Prm = New SqlParameter(27) {}
                End If

                Prm(0) = getParam(x.IdDocumento, "@IdDocumento", SqlDbType.Int)
                Prm(1) = New SqlParameter("@doc_Importacion", SqlDbType.Bit)
                Prm(1).Value = IIf(x.doc_Importacion = Nothing, DBNull.Value, x.doc_Importacion)
                'Prm(1) = getParam(x.doc_Importacion, "@doc_Importacion", SqlDbType.Bit)
                Prm(2) = getParam(x.IdMedioPago, "@IdMedioPago", SqlDbType.Int)
                Prm(3) = getParam(x.IdUsuarioSupervisor, "@IdUsuarioSupervisor", SqlDbType.Int)
                Prm(4) = getParam(x.PrecImportacion, "@anex_PrecImportacion", SqlDbType.Char)
                Prm(5) = getParam(x.NroContenedores, "@anex_NroContenedores", SqlDbType.VarChar)
                Prm(6) = getParam(x.NroDiasVigencia, "@anex_NroDiasVigencia", SqlDbType.Decimal)
                Prm(7) = getParam(x.TotalConcepto, "@anex_TotalConcepto", SqlDbType.Decimal)
                Prm(8) = getParam(x.IdArea, "@IdArea", SqlDbType.Int)
                Prm(9) = getParam(x.IdAlias, "@IdAlias", SqlDbType.Int)
                Prm(10) = getParam(x.IdMaestroObra, "@IdMaestroObra", SqlDbType.Int)
                Prm(11) = getParam(x.IdTPImp, "@IdTPImp", SqlDbType.Int)
                Prm(12) = getParam(x.IdSerieCheque, "@IdSerieCheque", SqlDbType.Int)
                Prm(13) = getParam(x.Aprobar, "@Aprobar", SqlDbType.Bit)
                Prm(14) = getParam(x.IdYear, "@IdYear", SqlDbType.Int)
                Prm(15) = getParam(x.IdSemana, "@IdSemana", SqlDbType.Int)
                Prm(16) = getParam(x.NroOperacion, "@anex_NroOperacion", SqlDbType.VarChar)
                Prm(17) = getParam(x.FechaAprobacion, "@anex_FechaAprobacion", SqlDbType.DateTime)

                Prm(18) = getParam(x.MontoAfectoIgv, "@anex_MontoAfectoIgv", SqlDbType.Decimal)
                Prm(19) = getParam(x.MontoNoAfectoIgv, "@anex_MontoNoAfectoIgv", SqlDbType.Decimal)

                Prm(20) = getParam(x.Liquidado, "@anex_Liquidado", SqlDbType.Bit)
                Prm(21) = getParam(x.MontoxSustentar, "@anex_MontoxSustentar", SqlDbType.Decimal)
                Prm(22) = getParam(x.Codigo, "@anex_Codigo", SqlDbType.VarChar)
                Prm(23) = getParam(x.Serie, "@anex_Serie", SqlDbType.VarChar)
                Prm(24) = getParam(x.MontoISC, "@montoISC", SqlDbType.Decimal)
                Prm(25) = getParam(x.MontoOtroTributo, "@montoOtroTributo", SqlDbType.Decimal)
                Prm(26) = getParam(x.anex_DescuentoGlobal, "@anex_DescuentoGlobal", SqlDbType.Decimal)
                Prm(27) = getParam(x.EstadoRequerimiento, "@est_reque", SqlDbType.Int)
                'Prm(26) = getParam(x.RetencionHonorarios, "@RetencionHonorariros", SqlDbType.Decimal)

                'Case modo_query.Delete
                '    Prm = New SqlParameter(0) {}
                '    Prm(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
                '    Prm(0).Value = getParamValueInt(x.IdDocumento)
            Case Else
                Prm = Nothing
        End Select

        Return Prm
    End Function

End Class
