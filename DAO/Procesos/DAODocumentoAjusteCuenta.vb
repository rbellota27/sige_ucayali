﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAODocumentoAjusteCuenta

    Private cn As SqlConnection
    Private tr As SqlTransaction
    Private reader As SqlDataReader

    Public Function DocumentoAjusteCuenta_SelectMovCuentaCargoxParams(ByVal IdUsuario As Integer, ByVal IdPersona As Integer, _
                            ByVal Serie As Integer, ByVal Codigo As Integer, ByVal FechaInicio As Date, _
                            ByVal FechaFin As Date, ByVal IdTipoDocumento As Integer, ByVal IdTipoDocumentoRef As Integer) As List(Of Entidades.Documento_MovCuenta)

        Dim Lista As New List(Of Entidades.Documento_MovCuenta)

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            Dim param() As SqlParameter = New SqlParameter(7) {}

            param(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
            param(0).Value = IdUsuario
            param(1) = New SqlParameter("@IdPersona", SqlDbType.Int)
            param(1).Value = IdPersona
            param(2) = New SqlParameter("@Serie", SqlDbType.Int)
            param(2).Value = Serie
            param(3) = New SqlParameter("@Codigo", SqlDbType.Int)
            param(3).Value = Codigo
            param(4) = New SqlParameter("@FechaInicio", SqlDbType.Date)
            param(4).Value = (IIf(FechaInicio = Nothing, DBNull.Value, FechaInicio))
            param(5) = New SqlParameter("@FechaFin", SqlDbType.Date)
            param(5).Value = (IIf(FechaFin = Nothing, DBNull.Value, FechaFin))
            param(6) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
            param(6).Value = IdTipoDocumento
            param(7) = New SqlParameter("@IdTipoDocumentoRef", SqlDbType.Int)
            param(7).Value = IdTipoDocumentoRef

            cn.Open()

            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_DocumentoAjusteCuenta_SelectMovCuentaCargoxParams", param)

            While (reader.Read)
                Dim obj As New Entidades.Documento_MovCuenta
                With obj

                    .IdMovCuenta = CInt(IIf(IsDBNull(reader("IdMovCuenta")) = True, 0, reader("IdMovCuenta")))
                    .Id = CInt(IIf(IsDBNull(reader("IdDocumento")) = True, 0, reader("IdDocumento")))
                    .MontoTotal = CDec(IIf(IsDBNull(reader("MontoTotal")) = True, 0, reader("MontoTotal")))
                    .Saldo = CDec(IIf(IsDBNull(reader("Saldo")) = True, 0, reader("Saldo")))
                    .Serie = CStr(IIf(IsDBNull(reader("doc_Serie")) = True, "", reader("doc_Serie")))
                    .Codigo = CStr(IIf(IsDBNull(reader("doc_Codigo")) = True, "", reader("doc_Codigo")))
                    .NomTipoDocumento = CStr(IIf(IsDBNull(reader("tdoc_NombreCorto")) = True, "", reader("tdoc_NombreCorto")))
                    .FechaEmision = CDate(IIf(IsDBNull(reader("doc_FechaEmision")) = True, Nothing, reader("doc_FechaEmision")))
                    .FechaVenc = CDate(IIf(IsDBNull(reader("doc_FechaVenc")) = True, Nothing, reader("doc_FechaVenc")))
                    .NroDiasMora = CInt(IIf(IsDBNull(reader("NroDiasMora")) = True, 0, reader("NroDiasMora")))
                    .NomMoneda = CStr(IIf(IsDBNull(reader("Moneda_Origen")) = True, "", reader("Moneda_Origen")))
                    .IdMoneda = CInt(IIf(IsDBNull(reader("IdMoneda_Origen")) = True, 0, reader("IdMoneda_Origen")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(reader("IdTipoDocumento")) = True, 0, reader("IdTipoDocumento")))
                    .strTipoDocumentoRef = CStr(IIf(IsDBNull(reader.Item("CadTipoDocumentoRef")) = True, "", reader.Item("CadTipoDocumentoRef")))
                End With
                Lista.Add(obj)
            End While
            reader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return Lista
    End Function

    Public Function DocumentoAjusteCuenta_SelectDetalle(ByVal IdDocumento As Integer) As List(Of Entidades.Documento_MovCuenta)

        Dim Lista As New List(Of Entidades.Documento_MovCuenta)

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            Dim param() As SqlParameter = New SqlParameter(0) {}
            param(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            param(0).Value = IdDocumento

            cn.Open()

            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_DocumentoAjusteCuenta_SelectDetalle", param)

            While (reader.Read)
                Dim obj As New Entidades.Documento_MovCuenta
                With obj

                    .IdMovCuenta = CInt(IIf(IsDBNull(reader("IdMovCuenta")) = True, 0, reader("IdMovCuenta")))
                    .Id = CInt(IIf(IsDBNull(reader("IdDocumento")) = True, 0, reader("IdDocumento")))
                    .MontoTotal = CDec(IIf(IsDBNull(reader("MontoTotal")) = True, 0, reader("MontoTotal")))
                    .Saldo = CDec(IIf(IsDBNull(reader("Saldo")) = True, 0, reader("Saldo")))
                    .Serie = CStr(IIf(IsDBNull(reader("doc_Serie")) = True, "", reader("doc_Serie")))
                    .Codigo = CStr(IIf(IsDBNull(reader("doc_Codigo")) = True, "", reader("doc_Codigo")))
                    .NomTipoDocumento = CStr(IIf(IsDBNull(reader("tdoc_NombreCorto")) = True, "", reader("tdoc_NombreCorto")))
                    .FechaEmision = CDate(IIf(IsDBNull(reader("doc_FechaEmision")) = True, Nothing, reader("doc_FechaEmision")))
                    .FechaVenc = CDate(IIf(IsDBNull(reader("doc_FechaVenc")) = True, Nothing, reader("doc_FechaVenc")))
                    .NroDiasMora = CInt(IIf(IsDBNull(reader("NroDiasMora")) = True, 0, reader("NroDiasMora")))
                    .NomMoneda = CStr(IIf(IsDBNull(reader("Moneda_Origen")) = True, "", reader("Moneda_Origen")))
                    .IdMoneda = CInt(IIf(IsDBNull(reader("IdMoneda_Origen")) = True, 0, reader("IdMoneda_Origen")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(reader("IdTipoDocumento")) = True, 0, reader("IdTipoDocumento")))

                    .IdEstadoDoc = CInt(IIf(IsDBNull(reader("IdEstadoDoc")) = True, 0, reader("IdEstadoDoc")))
                    .NomEstadoDocumento = CStr(IIf(IsDBNull(reader("edoc_Nombre")) = True, "", reader("edoc_Nombre")))

                    .Descuento = .Saldo  '*********** EN LA VARIABLE DCTO ALMACENAMOS EL SALDO ACTUAL
                    .Utilidad = CDec(IIf(IsDBNull(reader("SaldoAnterior")) = True, 0, reader("SaldoAnterior"))) '********* EN LA VARIABLE UTILIDAD ALMACENAMOS EL SALDO ORIGINAL

                End With
                Lista.Add(obj)
            End While
            reader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return Lista
    End Function

    Public Sub DocumentoAjusteCuenta_DeshacerMov(ByVal IdDocumento As Integer, ByVal DeleteDetalleConcepto As Boolean, ByVal DeleteObservacion As Boolean, ByVal DeleteRelacionDocumento As Boolean, ByVal ValMovPosteriores As Boolean, ByVal RestablecerSaldos As Boolean, ByVal Anular As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(6) {}

        p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        p(0).Value = IdDocumento

        p(1) = New SqlParameter("@DeleteDetalleConcepto", SqlDbType.Int)
        p(1).Value = DeleteDetalleConcepto

        p(2) = New SqlParameter("@DeleteObservacion", SqlDbType.Int)
        p(2).Value = DeleteObservacion

        p(3) = New SqlParameter("@DeleteRelacionDocumento", SqlDbType.Int)
        p(3).Value = DeleteRelacionDocumento

        p(4) = New SqlParameter("@RestablecerSaldos", SqlDbType.Int)
        p(4).Value = RestablecerSaldos

        p(5) = New SqlParameter("@ValMovPosteriores", SqlDbType.Int)
        p(5).Value = ValMovPosteriores

        p(6) = New SqlParameter("@Anular", SqlDbType.Int)
        p(6).Value = Anular

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DocumentoAjusteCuenta_DeshacerMov", p)

    End Sub



End Class
