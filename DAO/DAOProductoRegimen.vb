'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOProductoRegimen
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaProductoRegimen(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal productoregimen As Entidades.ProductoRegimen) As Boolean
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@reg_Id", SqlDbType.Int)
        ArrayParametros(0).Value = productoregimen.reg_Id
        ArrayParametros(1) = New SqlParameter("@IdSubLInea", SqlDbType.Int)
        ArrayParametros(1).Value = productoregimen.IdSubLInea
        ArrayParametros(2) = New SqlParameter("@pr_Tasa", SqlDbType.Decimal)
        ArrayParametros(2).Value = IIf(productoregimen.Tasa = Nothing, DBNull.Value, productoregimen.Tasa)
        HDAO.InsertaT(cn, "_ProductoRegimenInsert", ArrayParametros, tr)
    End Function
    Public Function ActualizaProductoRegimen(ByVal productoregimen As Entidades.ProductoRegimen) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@reg_Id", SqlDbType.Int)
        ArrayParametros(0).Value = productoregimen.reg_Id
        ArrayParametros(1) = New SqlParameter("@IdSubLInea", SqlDbType.Int)
        ArrayParametros(1).Value = productoregimen.IdSubLInea
        ArrayParametros(2) = New SqlParameter("@pr_Tasa", SqlDbType.Decimal)
        ArrayParametros(2).Value = productoregimen.Tasa
        Return HDAO.Update(cn, "_ProductoRegimenUpdate", ArrayParametros)
    End Function
    Public Sub DeletexIdSubLinea(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal idsublinea As Integer)
        Dim cmd As New SqlCommand("_ProductoRegimenDeletexIdSubLinea", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        cmd.ExecuteNonQuery()
    End Sub
End Class
