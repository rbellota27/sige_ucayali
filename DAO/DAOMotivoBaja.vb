'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOMotivoBaja
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaMotivoBaja(ByVal motivobaja As Entidades.MotivoBaja) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@mt_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = motivobaja.Descripcion
        ArrayParametros(1) = New SqlParameter("@mt_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = motivobaja.Estado
        Return HDAO.Insert(cn, "_MotivoBajaInsert", ArrayParametros)
    End Function
    Public Function ActualizaMotivoBaja(ByVal motivobaja As Entidades.MotivoBaja) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@mt_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = motivobaja.Descripcion
        ArrayParametros(1) = New SqlParameter("@mt_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = motivobaja.Estado
        ArrayParametros(2) = New SqlParameter("@IdMotivoBaja", SqlDbType.Int)
        ArrayParametros(2).Value = motivobaja.Id
        Return HDAO.Update(cn, "_MotivoBajaUpdate", ArrayParametros)
    End Function
    Public Function SelectAll() As List(Of Entidades.MotivoBaja)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoBajaSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MotivoBaja)
                Do While lector.Read
                    Dim obj As New Entidades.MotivoBaja
                    obj.Descripcion = CStr(lector.Item("mt_Nombre"))
                    obj.Estado = CStr(lector.Item("mt_Estado"))
                    obj.Id = CInt(lector.Item("IdMotivoBaja"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.MotivoBaja)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoBajaSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MotivoBaja)
                Do While lector.Read
                    Dim obj As New Entidades.MotivoBaja
                    obj.Descripcion = CStr(lector.Item("mt_Nombre"))
                    obj.Estado = CStr(lector.Item("mt_Estado"))
                    obj.Id = CInt(lector.Item("IdMotivoBaja"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.MotivoBaja)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoBajaSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MotivoBaja)
                Do While lector.Read
                    Dim obj As New Entidades.MotivoBaja
                    obj.Descripcion = CStr(lector.Item("mt_Nombre"))
                    obj.Estado = CStr(lector.Item("mt_Estado"))
                    obj.Id = CInt(lector.Item("IdMotivoBaja"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.MotivoBaja)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoBajaSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MotivoBaja)
                Do While lector.Read
                    Dim obj As New Entidades.MotivoBaja
                    obj.Descripcion = CStr(lector.Item("mt_Nombre"))
                    obj.Estado = CStr(lector.Item("mt_Estado"))
                    obj.Id = CInt(lector.Item("IdMotivoBaja"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.MotivoBaja)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoBajaSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MotivoBaja)
                Do While lector.Read
                    Dim obj As New Entidades.MotivoBaja
                    obj.Descripcion = CStr(lector.Item("mt_Nombre"))
                    obj.Estado = CStr(lector.Item("mt_Estado"))
                    obj.Id = CInt(lector.Item("IdMotivoBaja"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.MotivoBaja)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoBajaSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MotivoBaja)
                Do While lector.Read
                    Dim obj As New Entidades.MotivoBaja
                    obj.Descripcion = CStr(lector.Item("mt_Nombre"))
                    obj.Estado = CStr(lector.Item("mt_Estado"))
                    obj.Id = CInt(lector.Item("IdMotivoBaja"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.MotivoBaja)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoBajaSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MotivoBaja)
                Do While lector.Read
                    Dim obj As New Entidades.MotivoBaja
                    obj.Descripcion = CStr(lector.Item("mt_Nombre"))
                    obj.Estado = CStr(lector.Item("mt_Estado"))
                    obj.Id = CInt(lector.Item("IdMotivoBaja"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCbo() As List(Of Entidades.MotivoBaja)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoBajaSelect_cbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MotivoBaja)
                Do While lector.Read
                    Dim obj As New Entidades.MotivoBaja
                    obj.Descripcion = CStr(lector.Item("mt_Nombre"))
                    'obj.Estado = CStr(lector.Item("mt_Estado"))
                    obj.Id = CInt(lector.Item("IdMotivoBaja"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
