﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Imports Entidades

Public Class DAOConfigImpresion
    Dim objConexion As New DAO.Conexion
    Dim cn As SqlConnection = objConexion.ConexionSIGE
    Public Function SelectAll(ByVal idTipodocumento As Integer, ByVal idTipoimpresion As Integer) As List(Of Entidades.DocsImpresionView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idTipoDocumento", idTipodocumento)
        cmd.Parameters.AddWithValue("@idtipoimpresion", idTipoimpresion)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DocsImpresionView)
                Do While lector.Read
                    Dim obj As New Entidades.DocsImpresionView

                    obj.IdImpresion = CInt(IIf(IsDBNull(lector.Item("IdImpresion")) = True, 0, lector.Item("IdImpresion")))
                    obj.IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("idtipodocumento")) = True, 0, lector.Item("idtipodocumento")))
                    obj.Tp_Nombre = CStr(IIf(IsDBNull(lector.Item("Impresion")) = True, "", lector.Item("Impresion")))
                    obj.Docs = CStr(IIf(IsDBNull(lector.Item("Docs")) = True, "", lector.Item("Docs")))
                    obj.Timp = CStr(IIf(IsDBNull(lector.Item("timp")) = True, 0, lector.Item("timp")))
                    obj.TimpNombre = CStr(IIf(IsDBNull(lector.Item("TimpNombre")) = True, "", lector.Item("TimpNombre")))

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function llenaCbo() As List(Of Entidades.DocsImpresionView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("LlenaCboTipoImpresion", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DocsImpresionView)
                Do While lector.Read
                    Dim obj As New Entidades.DocsImpresionView

                    obj.IdImpresion = CInt(IIf(IsDBNull(lector.Item("IdImpresion")) = True, 0, lector.Item("IdImpresion")))
                    obj.Tp_Nombre = CStr(IIf(IsDBNull(lector.Item("ti_Nombre")) = True, "", lector.Item("ti_Nombre")))

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function InsertConfigImpresion(ByVal objImp As Entidades.DocsImpresionView) As Boolean

        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@idTDocuemnto", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(objImp.IdTipoDocumento = Nothing, DBNull.Value, objImp.IdTipoDocumento)
        ArrayParametros(1) = New SqlParameter("@idTimpresion", SqlDbType.Int)
        ArrayParametros(1).Value = IIf(objImp.IdImpresion = Nothing, DBNull.Value, objImp.IdImpresion)
        ArrayParametros(2) = New SqlParameter("@timp", SqlDbType.VarChar, 250)
        ArrayParametros(2).Value = IIf(objImp.Timp = Nothing, DBNull.Value, objImp.Timp)

        Dim cmd As New SqlCommand("_TipoImpresionInsertUpdate", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return True
    End Function
    'Public Sub insertarDocCondicionComercial(ByVal cn As SqlConnection, ByVal T As SqlTransaction, ByVal objTImp As Entidades.CondicionComercial)
    '    Dim cmd As New SqlCommand("_TipoImpresionInsertUpdate", cn, T)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    Try
    '        cmd.Parameters.AddWithValue("@iddocumento", iddocumento)
    '        cmd.Parameters.AddWithValue("@idtipodoc", idtipodoc)
    '        cmd.Parameters.AddWithValue("@idcondicion", idcondicion.Id)

    '        Using cmd
    '            If cmd.ExecuteNonQuery = 0 Then
    '                Throw New Exception("Fracaso la Operaccion")
    '            End If
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub
End Class
