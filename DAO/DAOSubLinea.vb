'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAOSubLinea
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion



    Public Function fn_TotalPag_LineaxSubLinea(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal PageSize As Integer) As Integer


        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim TotalPagina As Integer = 0

        Try

            cn.Open()

            TotalPagina = CInt(SqlHelper.ExecuteScalar(cn, CommandType.Text, " select dbo.fn_TotalPag_LineaxSubLinea(" + CStr(IdLinea) + "," + CStr(IdSubLinea) + "," + CStr(PageSize) + ") "))


        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return TotalPagina

    End Function



    Public Function InsertaSubLinea(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal sublinea As Entidades.SubLinea) As Integer
        Dim ArrayParametros() As SqlParameter = New SqlParameter(15) {}
        ArrayParametros(0) = New SqlParameter("@sl_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(sublinea.Nombre = Nothing, DBNull.Value, sublinea.Nombre)
        ArrayParametros(1) = New SqlParameter("@sl_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = IIf(sublinea.Estado = Nothing, DBNull.Value, sublinea.Estado)
        ArrayParametros(2) = New SqlParameter("@IdLinea", SqlDbType.Int)
        ArrayParametros(2).Value = IIf(sublinea.IdLinea = Nothing, DBNull.Value, sublinea.IdLinea)
        ArrayParametros(3) = New SqlParameter("@IdTipoExistencia", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(sublinea.IdTipoExistencia = Nothing, DBNull.Value, sublinea.IdTipoExistencia)
        ArrayParametros(4) = New SqlParameter("@sl_AfectoIgv", SqlDbType.Char)
        ArrayParametros(4).Value = IIf(sublinea.AfectoIgv = Nothing, DBNull.Value, sublinea.AfectoIgv)
        ArrayParametros(5) = New SqlParameter("@sl_AfectoPercepcion", SqlDbType.Char)
        ArrayParametros(5).Value = IIf(sublinea.AfectoPercepcion = Nothing, DBNull.Value, sublinea.AfectoPercepcion)
        ArrayParametros(6) = New SqlParameter("@sl_AfectoDetraccion", SqlDbType.Char)
        ArrayParametros(6).Value = IIf(sublinea.AfectoDetraccion = Nothing, DBNull.Value, sublinea.AfectoDetraccion)
        ArrayParametros(7) = New SqlParameter("@sl_CtaDebeCompra", SqlDbType.VarChar)
        ArrayParametros(7).Value = IIf(sublinea.CtaDebeCompra = Nothing, DBNull.Value, sublinea.CtaDebeCompra)
        ArrayParametros(8) = New SqlParameter("@sl_CtaHaberCompra", SqlDbType.VarChar)
        ArrayParametros(8).Value = IIf(sublinea.CtaHaberCompra = Nothing, DBNull.Value, sublinea.CtaHaberCompra)
        ArrayParametros(9) = New SqlParameter("@sl_CostoCompra", SqlDbType.VarChar)
        ArrayParametros(9).Value = IIf(sublinea.CostoCompra = Nothing, DBNull.Value, sublinea.CostoCompra)
        ArrayParametros(10) = New SqlParameter("@sl_CtaDebeVenta", SqlDbType.VarChar)
        ArrayParametros(10).Value = IIf(sublinea.CtaDebeVenta = Nothing, DBNull.Value, sublinea.CtaDebeVenta)
        ArrayParametros(11) = New SqlParameter("@sl_CtaHaberVenta", SqlDbType.VarChar)
        ArrayParametros(11).Value = IIf(sublinea.CtaHaberVenta = Nothing, DBNull.Value, sublinea.CtaHaberVenta)
        ArrayParametros(12) = New SqlParameter("@sl_CostoVenta", SqlDbType.VarChar)
        ArrayParametros(12).Value = IIf(sublinea.CostoVenta = Nothing, DBNull.Value, sublinea.CostoVenta)
        ArrayParametros(13) = New SqlParameter("@sl_Codigo", SqlDbType.VarChar)
        ArrayParametros(13).Value = IIf(sublinea.CodigoSubLinea = Nothing, DBNull.Value, sublinea.CodigoSubLinea)

        ArrayParametros(14) = New SqlParameter("@sl_Longitud", SqlDbType.TinyInt)
        ArrayParametros(14).Value = IIf(sublinea.Longitud = Nothing, DBNull.Value, sublinea.Longitud)

        ArrayParametros(15) = New SqlParameter("@Retorno", SqlDbType.Int)
        ArrayParametros(15).Direction = ParameterDirection.Output

        Return HDAO.InsertaTParameterOutPut(cn, "_SubLineaInsert", ArrayParametros, tr)
    End Function
    Public Sub ActualizaSubLinea(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal sublinea As Entidades.SubLinea)
        Dim cmd As New SqlCommand("_SubLineaUpdate", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        Dim ArrayParametros() As SqlParameter = New SqlParameter(15) {}
        ArrayParametros(0) = New SqlParameter("@sl_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(sublinea.Nombre = Nothing, DBNull.Value, sublinea.Nombre)
        ArrayParametros(1) = New SqlParameter("@sl_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = IIf(sublinea.Estado = Nothing, DBNull.Value, sublinea.Estado)
        ArrayParametros(2) = New SqlParameter("@IdLinea", SqlDbType.Int)
        ArrayParametros(2).Value = IIf(sublinea.IdLinea = Nothing, DBNull.Value, sublinea.IdLinea)
        ArrayParametros(3) = New SqlParameter("@IdTipoExistencia", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(sublinea.IdTipoExistencia = Nothing, DBNull.Value, sublinea.IdTipoExistencia)
        ArrayParametros(4) = New SqlParameter("@sl_AfectoIgv", SqlDbType.Char)
        ArrayParametros(4).Value = IIf(sublinea.AfectoIgv = Nothing, DBNull.Value, sublinea.AfectoIgv)
        ArrayParametros(5) = New SqlParameter("@sl_AfectoPercepcion", SqlDbType.Char)
        ArrayParametros(5).Value = IIf(sublinea.AfectoPercepcion = Nothing, DBNull.Value, sublinea.AfectoPercepcion)
        ArrayParametros(6) = New SqlParameter("@sl_AfectoDetraccion", SqlDbType.Char)
        ArrayParametros(6).Value = IIf(sublinea.AfectoDetraccion = Nothing, DBNull.Value, sublinea.AfectoDetraccion)
        ArrayParametros(7) = New SqlParameter("@sl_CtaDebeCompra", SqlDbType.VarChar)
        ArrayParametros(7).Value = IIf(sublinea.CtaDebeCompra = Nothing, DBNull.Value, sublinea.CtaDebeCompra)
        ArrayParametros(8) = New SqlParameter("@sl_CtaHaberCompra", SqlDbType.VarChar)
        ArrayParametros(8).Value = IIf(sublinea.CtaHaberCompra = Nothing, DBNull.Value, sublinea.CtaHaberCompra)
        ArrayParametros(9) = New SqlParameter("@sl_CostoCompra", SqlDbType.VarChar)
        ArrayParametros(9).Value = IIf(sublinea.CostoCompra = Nothing, DBNull.Value, sublinea.CostoCompra)
        ArrayParametros(10) = New SqlParameter("@sl_CtaDebeVenta", SqlDbType.VarChar)
        ArrayParametros(10).Value = IIf(sublinea.CtaDebeVenta = Nothing, DBNull.Value, sublinea.CtaDebeVenta)
        ArrayParametros(11) = New SqlParameter("@sl_CtaHaberVenta", SqlDbType.VarChar)
        ArrayParametros(11).Value = IIf(sublinea.CtaHaberVenta = Nothing, DBNull.Value, sublinea.CtaHaberVenta)
        ArrayParametros(12) = New SqlParameter("@sl_CostoVenta", SqlDbType.VarChar)
        ArrayParametros(12).Value = IIf(sublinea.CostoVenta = Nothing, DBNull.Value, sublinea.CostoVenta)
        ArrayParametros(13) = New SqlParameter("@sl_Codigo", SqlDbType.VarChar)
        ArrayParametros(13).Value = IIf(sublinea.CodigoSubLinea = Nothing, DBNull.Value, sublinea.CodigoSubLinea)

        ArrayParametros(14) = New SqlParameter("@IdSubLInea", SqlDbType.Int)
        ArrayParametros(14).Value = sublinea.Id

        ArrayParametros(15) = New SqlParameter("@sl_Longitud", SqlDbType.TinyInt)
        ArrayParametros(15).Value = IIf(sublinea.Longitud = Nothing, DBNull.Value, sublinea.Longitud)

        cmd.Parameters.AddRange(ArrayParametros)
        If cmd.ExecuteNonQuery() = 0 Then
            Throw New Exception
        End If
    End Sub
    Public Function InsertaSubLineaT(ByVal objSubLinea As Entidades.SubLinea, ByVal listaRegimen As List(Of Entidades.ProductoRegimen)) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim id As Integer = Me.InsertaSubLinea(cn, tr, objSubLinea)
            Dim objDaoProdRegimen As New DAO.DAOProductoRegimen
            For i As Integer = 0 To listaRegimen.Count - 1
                listaRegimen.Item(i).IdSubLInea = id
                listaRegimen.Item(i).Tasa = listaRegimen.Item(i).Tasa / 100
                objDaoProdRegimen.InsertaProductoRegimen(cn, tr, listaRegimen.Item(i))
            Next
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
    Public Function ActualizaSubLineaT(ByVal objSubLinea As Entidades.SubLinea, ByVal listaRegimen As List(Of Entidades.ProductoRegimen)) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Me.ActualizaSubLinea(cn, tr, objSubLinea)
            Dim objDaoProdRegimen As New DAO.DAOProductoRegimen
            objDaoProdRegimen.DeletexIdSubLinea(cn, tr, objSubLinea.Id)
            For i As Integer = 0 To listaRegimen.Count - 1
                listaRegimen.Item(i).IdSubLInea = objSubLinea.Id
                listaRegimen.Item(i).Tasa = listaRegimen.Item(i).Tasa / 100
                objDaoProdRegimen.InsertaProductoRegimen(cn, tr, listaRegimen.Item(i))
            Next
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function
    Public Function SelectAll() As List(Of Entidades.SubLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SubLineaSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea)
                Do While lector.Read
                    Dim objSubLinea As New Entidades.SubLinea
                    objSubLinea.AfectoDetraccion = CStr(IIf(IsDBNull(lector.Item("sl_AfectoDetraccion")) = True, "0", lector.Item("sl_AfectoDetraccion")))
                    objSubLinea.AfectoIgv = CStr(IIf(IsDBNull(lector.Item("sl_AfectoIgv")) = True, "0", lector.Item("sl_AfectoIgv")))
                    objSubLinea.AfectoPercepcion = CStr(IIf(IsDBNull(lector.Item("sl_AfectoPercepcion")) = True, "0", lector.Item("sl_AfectoPercepcion")))
                    objSubLinea.CostoCompra = CStr(IIf(IsDBNull(lector.Item("sl_CostoCompra")) = True, "0", lector.Item("sl_CostoCompra")))
                    objSubLinea.CostoVenta = CStr(IIf(IsDBNull(lector.Item("sl_CostoVenta")) = True, "0", lector.Item("sl_CostoVenta")))
                    objSubLinea.CtaDebeCompra = CStr(IIf(IsDBNull(lector.Item("sl_CtaDebeCompra")) = True, "0", lector.Item("sl_CtaDebeCompra")))
                    objSubLinea.CtaDebeVenta = CStr(IIf(IsDBNull(lector.Item("sl_CtaDebeVenta")) = True, "0", lector.Item("sl_CtaDebeVenta")))
                    objSubLinea.CtaHaberCompra = CStr(IIf(IsDBNull(lector.Item("sl_CtaHaberCompra")) = True, "0", lector.Item("sl_CtaHaberCompra")))
                    objSubLinea.CtaHaberVenta = CStr(IIf(IsDBNull(lector.Item("sl_CtaHaberVenta")) = True, "0", lector.Item("sl_CtaHaberVenta")))
                    objSubLinea.Estado = CStr(IIf(IsDBNull(lector.Item("sl_Estado")) = True, "0", lector.Item("sl_Estado")))
                    objSubLinea.Id = CInt(IIf(IsDBNull(lector.Item("IdSubLInea")) = True, 0, lector.Item("IdSubLInea")))
                    objSubLinea.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    objSubLinea.IdTipoExistencia = CInt(IIf(IsDBNull(lector.Item("IdTipoExistencia")) = True, 0, lector.Item("IdTipoExistencia")))
                    objSubLinea.Nombre = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")) = True, "0", lector.Item("sl_Nombre")))
                    Lista.Add(objSubLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectxIdSubLinea(ByVal idsublinea As Integer) As Entidades.SubLinea
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SubLineaSelectxIdSubLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim objSubLinea As New Entidades.SubLinea
                If lector.Read Then
                    objSubLinea.AfectoDetraccion = CStr(IIf(IsDBNull(lector.Item("sl_AfectoDetraccion")) = True, "0", lector.Item("sl_AfectoDetraccion")))
                    objSubLinea.AfectoIgv = CStr(IIf(IsDBNull(lector.Item("sl_AfectoIgv")) = True, "0", lector.Item("sl_AfectoIgv")))
                    objSubLinea.AfectoPercepcion = CStr(IIf(IsDBNull(lector.Item("sl_AfectoPercepcion")) = True, "0", lector.Item("sl_AfectoPercepcion")))
                    objSubLinea.CostoCompra = CStr(IIf(IsDBNull(lector.Item("sl_CostoCompra")) = True, "0", lector.Item("sl_CostoCompra")))
                    objSubLinea.CostoVenta = CStr(IIf(IsDBNull(lector.Item("sl_CostoVenta")) = True, "0", lector.Item("sl_CostoVenta")))
                    objSubLinea.CtaDebeCompra = CStr(IIf(IsDBNull(lector.Item("sl_CtaDebeCompra")) = True, "0", lector.Item("sl_CtaDebeCompra")))
                    objSubLinea.CtaDebeVenta = CStr(IIf(IsDBNull(lector.Item("sl_CtaDebeVenta")) = True, "0", lector.Item("sl_CtaDebeVenta")))
                    objSubLinea.CtaHaberCompra = CStr(IIf(IsDBNull(lector.Item("sl_CtaHaberCompra")) = True, "0", lector.Item("sl_CtaHaberCompra")))
                    objSubLinea.CtaHaberVenta = CStr(IIf(IsDBNull(lector.Item("sl_CtaHaberVenta")) = True, "0", lector.Item("sl_CtaHaberVenta")))
                    objSubLinea.Estado = CStr(IIf(IsDBNull(lector.Item("sl_Estado")) = True, "0", lector.Item("sl_Estado")))
                    objSubLinea.Id = CInt(IIf(IsDBNull(lector.Item("IdSubLInea")) = True, 0, lector.Item("IdSubLInea")))
                    objSubLinea.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    objSubLinea.IdTipoExistencia = CInt(IIf(IsDBNull(lector.Item("IdTipoExistencia")) = True, 0, lector.Item("IdTipoExistencia")))
                    objSubLinea.Nombre = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")) = True, "0", lector.Item("sl_Nombre")))
                    objSubLinea.CodigoSubLinea = CStr(IIf(IsDBNull(lector.Item("sl_Codigo")) = True, "", lector.Item("sl_Codigo")))
                    objSubLinea.NroOrdenLinea = CInt(IIf(IsDBNull(lector.Item("l_NroOrden")) = True, 0, lector.Item("l_NroOrden")))
                    objSubLinea.ParteNombreLinea = CBool(IIf(IsDBNull(lector.Item("l_ParteNombre")) = True, False, lector.Item("l_ParteNombre")))
                    objSubLinea.NroOrdenSubLinea = CInt(IIf(IsDBNull(lector.Item("sl_NroOrden")) = True, 0, lector.Item("sl_NroOrden")))
                    objSubLinea.ParteNombreSubLinea = CBool(IIf(IsDBNull(lector.Item("sl_ParteNombre")) = True, False, lector.Item("sl_ParteNombre")))
                    objSubLinea.CodigoLinea = CStr(IIf(IsDBNull(lector.Item("lin_Codigo")) = True, "", lector.Item("lin_Codigo")))
                    objSubLinea.NomLinea = CStr(IIf(IsDBNull(lector.Item("lin_Nombre")) = True, "", lector.Item("lin_Nombre")))
                    objSubLinea.TipoUsoSubLinea = CStr(IIf(IsDBNull(lector.Item("sl_TipoUso")) = True, "", lector.Item("sl_TipoUso")))
                    objSubLinea.TipoUsoLinea = CStr(IIf(IsDBNull(lector.Item("l_TipoUso")) = True, "", lector.Item("l_TipoUso")))
                    objSubLinea.Longitud = CInt(IIf(IsDBNull(lector.Item("sl_Longitud")) = True, 0, lector.Item("sl_Longitud")))
                End If
                lector.Close()
                Return objSubLinea
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.SubLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SubLineaSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea)
                Do While lector.Read
                    Dim objSubLinea As New Entidades.SubLinea
                    objSubLinea.AfectoDetraccion = CStr(IIf(IsDBNull(lector.Item("sl_AfectoDetraccion")) = True, "", lector.Item("sl_AfectoDetraccion")))
                    objSubLinea.AfectoIgv = CStr(IIf(IsDBNull(lector.Item("sl_AfectoIgv")) = True, "", lector.Item("sl_AfectoIgv")))
                    objSubLinea.AfectoPercepcion = CStr(IIf(IsDBNull(lector.Item("sl_AfectoPercepcion")) = True, "", lector.Item("sl_AfectoPercepcion")))
                    objSubLinea.CostoCompra = CStr(IIf(IsDBNull(lector.Item("sl_CostoCompra")) = True, "", lector.Item("sl_CostoCompra")))
                    objSubLinea.CostoVenta = CStr(IIf(IsDBNull(lector.Item("sl_CostoVenta")) = True, "", lector.Item("sl_CostoVenta")))
                    objSubLinea.CtaDebeCompra = CStr(IIf(IsDBNull(lector.Item("sl_CtaDebeCompra")) = True, "", lector.Item("sl_CtaDebeCompra")))
                    objSubLinea.CtaDebeVenta = CStr(IIf(IsDBNull(lector.Item("sl_CtaDebeVenta")) = True, "", lector.Item("sl_CtaDebeVenta")))
                    objSubLinea.CtaHaberCompra = CStr(IIf(IsDBNull(lector.Item("sl_CtaHaberCompra")) = True, "", lector.Item("sl_CtaHaberCompra")))
                    objSubLinea.CtaHaberVenta = CStr(IIf(IsDBNull(lector.Item("sl_CtaHaberVenta")) = True, "", lector.Item("sl_CtaHaberVenta")))
                    objSubLinea.Estado = CStr(IIf(IsDBNull(lector.Item("sl_Estado")) = True, "", lector.Item("sl_Estado")))
                    objSubLinea.Id = CInt(IIf(IsDBNull(lector.Item("IdSubLInea")) = True, 0, lector.Item("IdSubLInea")))
                    objSubLinea.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    objSubLinea.IdTipoExistencia = CInt(IIf(IsDBNull(lector.Item("IdTipoExistencia")) = True, 0, lector.Item("IdTipoExistencia")))
                    objSubLinea.Nombre = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")) = True, "", lector.Item("sl_Nombre")))
                    Lista.Add(objSubLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.SubLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SubLineaSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea)
                Do While lector.Read
                    Dim objSubLinea As New Entidades.SubLinea
                    objSubLinea.AfectoDetraccion = CStr(IIf(IsDBNull(lector.Item("sl_AfectoDetraccion")) = True, "", lector.Item("sl_AfectoDetraccion")))
                    objSubLinea.AfectoIgv = CStr(IIf(IsDBNull(lector.Item("sl_AfectoIgv")) = True, "", lector.Item("sl_AfectoIgv")))
                    objSubLinea.AfectoPercepcion = CStr(IIf(IsDBNull(lector.Item("sl_AfectoPercepcion")) = True, "", lector.Item("sl_AfectoPercepcion")))
                    objSubLinea.CostoCompra = CStr(IIf(IsDBNull(lector.Item("sl_CostoCompra")) = True, "", lector.Item("sl_CostoCompra")))
                    objSubLinea.CostoVenta = CStr(IIf(IsDBNull(lector.Item("sl_CostoVenta")) = True, "", lector.Item("sl_CostoVenta")))
                    objSubLinea.CtaDebeCompra = CStr(IIf(IsDBNull(lector.Item("sl_CtaDebeCompra")) = True, "", lector.Item("sl_CtaDebeCompra")))
                    objSubLinea.CtaDebeVenta = CStr(IIf(IsDBNull(lector.Item("sl_CtaDebeVenta")) = True, "", lector.Item("sl_CtaDebeVenta")))
                    objSubLinea.CtaHaberCompra = CStr(IIf(IsDBNull(lector.Item("sl_CtaHaberCompra")) = True, "", lector.Item("sl_CtaHaberCompra")))
                    objSubLinea.CtaHaberVenta = CStr(IIf(IsDBNull(lector.Item("sl_CtaHaberVenta")) = True, "", lector.Item("sl_CtaHaberVenta")))
                    objSubLinea.Estado = CStr(IIf(IsDBNull(lector.Item("sl_Estado")) = True, "", lector.Item("sl_Estado")))
                    objSubLinea.Id = CInt(IIf(IsDBNull(lector.Item("IdSubLInea")) = True, 0, lector.Item("IdSubLInea")))
                    objSubLinea.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    objSubLinea.IdTipoExistencia = CInt(IIf(IsDBNull(lector.Item("IdTipoExistencia")) = True, 0, lector.Item("IdTipoExistencia")))
                    objSubLinea.Nombre = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")) = True, "", lector.Item("sl_Nombre")))
                    Lista.Add(objSubLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectActivoxLineaxTipoExistencia(ByVal idLinea As Integer, ByVal idTipoEx As Integer) As List(Of Entidades.SubLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SubLineaSelectActivoxLineaxTipoEx", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idLinea)
        cmd.Parameters.AddWithValue("@IdTipoEx", idTipoEx)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea)
                Do While lector.Read
                    Dim objSubLinea As New Entidades.SubLinea
                    objSubLinea.AfectoDetraccion = CStr(IIf(IsDBNull(lector.Item("sl_AfectoDetraccion")) = True, "", lector.Item("sl_AfectoDetraccion")))
                    objSubLinea.AfectoIgv = CStr(IIf(IsDBNull(lector.Item("sl_AfectoIgv")) = True, "", lector.Item("sl_AfectoIgv")))
                    objSubLinea.AfectoPercepcion = CStr(IIf(IsDBNull(lector.Item("sl_AfectoPercepcion")) = True, "", lector.Item("sl_AfectoPercepcion")))
                    objSubLinea.CostoCompra = CStr(IIf(IsDBNull(lector.Item("sl_CostoCompra")) = True, "", lector.Item("sl_CostoCompra")))
                    objSubLinea.CostoVenta = CStr(IIf(IsDBNull(lector.Item("sl_CostoVenta")) = True, "", lector.Item("sl_CostoVenta")))
                    objSubLinea.CtaDebeCompra = CStr(IIf(IsDBNull(lector.Item("sl_CtaDebeCompra")) = True, "", lector.Item("sl_CtaDebeCompra")))
                    objSubLinea.CtaDebeVenta = CStr(IIf(IsDBNull(lector.Item("sl_CtaDebeVenta")) = True, "", lector.Item("sl_CtaDebeVenta")))
                    objSubLinea.CtaHaberCompra = CStr(IIf(IsDBNull(lector.Item("sl_CtaHaberCompra")) = True, "", lector.Item("sl_CtaHaberCompra")))
                    objSubLinea.CtaHaberVenta = CStr(IIf(IsDBNull(lector.Item("sl_CtaHaberVenta")) = True, "", lector.Item("sl_CtaHaberVenta")))
                    objSubLinea.Estado = CStr(IIf(IsDBNull(lector.Item("sl_Estado")) = True, "", lector.Item("sl_Estado")))
                    objSubLinea.Id = CInt(IIf(IsDBNull(lector.Item("IdSubLInea")) = True, 0, lector.Item("IdSubLInea")))
                    objSubLinea.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    objSubLinea.IdTipoExistencia = CInt(IIf(IsDBNull(lector.Item("IdTipoExistencia")) = True, 0, lector.Item("IdTipoExistencia")))
                    objSubLinea.Nombre = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")) = True, "", lector.Item("sl_Nombre")))
                    Lista.Add(objSubLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function SelectActivoxLineaxTipoExistencia2(ByVal idLinea As Integer, ByVal idTipoEx As Integer) As List(Of Entidades.SubLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE2
        Dim cmd As New SqlCommand("_SubLineaSelectActivoxLineaxTipoEx", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idLinea)
        cmd.Parameters.AddWithValue("@IdTipoEx", idTipoEx)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea)
                Do While lector.Read
                    Dim objSubLinea As New Entidades.SubLinea
                    objSubLinea.AfectoDetraccion = CStr(IIf(IsDBNull(lector.Item("sl_AfectoDetraccion")) = True, "", lector.Item("sl_AfectoDetraccion")))
                    objSubLinea.AfectoIgv = CStr(IIf(IsDBNull(lector.Item("sl_AfectoIgv")) = True, "", lector.Item("sl_AfectoIgv")))
                    objSubLinea.AfectoPercepcion = CStr(IIf(IsDBNull(lector.Item("sl_AfectoPercepcion")) = True, "", lector.Item("sl_AfectoPercepcion")))
                    objSubLinea.CostoCompra = CStr(IIf(IsDBNull(lector.Item("sl_CostoCompra")) = True, "", lector.Item("sl_CostoCompra")))
                    objSubLinea.CostoVenta = CStr(IIf(IsDBNull(lector.Item("sl_CostoVenta")) = True, "", lector.Item("sl_CostoVenta")))
                    objSubLinea.CtaDebeCompra = CStr(IIf(IsDBNull(lector.Item("sl_CtaDebeCompra")) = True, "", lector.Item("sl_CtaDebeCompra")))
                    objSubLinea.CtaDebeVenta = CStr(IIf(IsDBNull(lector.Item("sl_CtaDebeVenta")) = True, "", lector.Item("sl_CtaDebeVenta")))
                    objSubLinea.CtaHaberCompra = CStr(IIf(IsDBNull(lector.Item("sl_CtaHaberCompra")) = True, "", lector.Item("sl_CtaHaberCompra")))
                    objSubLinea.CtaHaberVenta = CStr(IIf(IsDBNull(lector.Item("sl_CtaHaberVenta")) = True, "", lector.Item("sl_CtaHaberVenta")))
                    objSubLinea.Estado = CStr(IIf(IsDBNull(lector.Item("sl_Estado")) = True, "", lector.Item("sl_Estado")))
                    objSubLinea.Id = CInt(IIf(IsDBNull(lector.Item("IdSubLInea")) = True, 0, lector.Item("IdSubLInea")))
                    objSubLinea.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    objSubLinea.IdTipoExistencia = CInt(IIf(IsDBNull(lector.Item("IdTipoExistencia")) = True, 0, lector.Item("IdTipoExistencia")))
                    objSubLinea.Nombre = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")) = True, "", lector.Item("sl_Nombre")))
                    Lista.Add(objSubLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllActivoxLinea(ByVal idLinea As Integer) As List(Of Entidades.SubLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SubLineaSelectAllActivoxLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idLinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea)
                Do While lector.Read
                    Dim objSubLinea As New Entidades.SubLinea
                    objSubLinea.AfectoDetraccion = CStr(IIf(IsDBNull(lector.Item("sl_AfectoDetraccion")) = True, "", lector.Item("sl_AfectoDetraccion")))
                    objSubLinea.AfectoIgv = CStr(IIf(IsDBNull(lector.Item("sl_AfectoIgv")) = True, "", lector.Item("sl_AfectoIgv")))
                    objSubLinea.AfectoPercepcion = CStr(IIf(IsDBNull(lector.Item("sl_AfectoPercepcion")) = True, "", lector.Item("sl_AfectoPercepcion")))
                    objSubLinea.CostoCompra = CStr(IIf(IsDBNull(lector.Item("sl_CostoCompra")) = True, "", lector.Item("sl_CostoCompra")))
                    objSubLinea.CostoVenta = CStr(IIf(IsDBNull(lector.Item("sl_CostoVenta")) = True, "", lector.Item("sl_CostoVenta")))
                    objSubLinea.CtaDebeCompra = CStr(IIf(IsDBNull(lector.Item("sl_CtaDebeCompra")) = True, "", lector.Item("sl_CtaDebeCompra")))
                    objSubLinea.CtaDebeVenta = CStr(IIf(IsDBNull(lector.Item("sl_CtaDebeVenta")) = True, "", lector.Item("sl_CtaDebeVenta")))
                    objSubLinea.CtaHaberCompra = CStr(IIf(IsDBNull(lector.Item("sl_CtaHaberCompra")) = True, "", lector.Item("sl_CtaHaberCompra")))
                    objSubLinea.CtaHaberVenta = CStr(IIf(IsDBNull(lector.Item("sl_CtaHaberVenta")) = True, "", lector.Item("sl_CtaHaberVenta")))
                    objSubLinea.Estado = CStr(IIf(IsDBNull(lector.Item("sl_Estado")) = True, "", lector.Item("sl_Estado")))
                    objSubLinea.Id = CInt(IIf(IsDBNull(lector.Item("IdSubLInea")) = True, 0, lector.Item("IdSubLInea")))
                    objSubLinea.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    objSubLinea.IdTipoExistencia = CInt(IIf(IsDBNull(lector.Item("IdTipoExistencia")) = True, 0, lector.Item("IdTipoExistencia")))
                    objSubLinea.Nombre = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")) = True, "", lector.Item("sl_Nombre")))
                    Lista.Add(objSubLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectCboxIdLinea(ByVal idLinea As Integer) As List(Of Entidades.SubLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SubLineaSelectCboxIdLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idLinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea)
                Do While lector.Read
                    Dim objSubLinea As New Entidades.SubLinea
                    objSubLinea.Id = CInt(IIf(IsDBNull(lector.Item("IdSubLInea")) = True, 0, lector.Item("IdSubLInea")))
                    objSubLinea.Nombre = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")) = True, "", lector.Item("sl_Nombre")))
                    Lista.Add(objSubLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectCboxIdLinea2(ByVal idLinea As Integer) As List(Of Entidades.SubLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE2
        Dim cmd As New SqlCommand("_SubLineaSelectCboxIdLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idLinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea)
                Do While lector.Read
                    Dim objSubLinea As New Entidades.SubLinea
                    objSubLinea.Id = CInt(IIf(IsDBNull(lector.Item("IdSubLInea")) = True, 0, lector.Item("IdSubLInea")))
                    objSubLinea.Nombre = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")) = True, "", lector.Item("sl_Nombre")))
                    Lista.Add(objSubLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectxIdLineaxEstado(ByVal idlinea As Integer, ByVal estado As String) As List(Of Entidades.SubLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SubLineaSelectxIdLineaxEstado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@Estado", estado)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea)
                Do While lector.Read
                    Dim obj As New Entidades.SubLinea
                    obj.Id = CInt(lector.Item("IdSubLInea"))
                    obj.Nombre = CStr(lector.Item("sl_Nombre"))
                    obj.Estado = CStr(lector.Item("sl_Estado"))
                    obj.NomLinea = CStr(lector.Item("lin_Nombre"))
                    obj.CodigoSubLinea = CStr(IIf(IsDBNull(lector.Item("sl_Codigo")) = True, "", lector.Item("sl_Codigo")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectIdxCodigo(ByVal Codigo As Integer) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SublineaSelectIdxCodigo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@sl_Codigo", Codigo)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                lector.Read()
                If lector.HasRows = False Then
                    Throw New Exception("No existe Sublinea con ese C�digo")
                End If
                Dim IdSublinea As Integer = CInt(lector.Item("IdSubLInea"))
                Return IdSublinea
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectCodigoxIdSubLinea(ByVal IdSubLinea As Integer) As String
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SublineaSelectCodigoxIdSubLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                lector.Read()
                Dim Codigo As String = CStr(iif(IsDBNull(lector.Item("sl_Codigo")) = True, "", lector.Item("sl_Codigo")))
                Return Codigo
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectCbo() As List(Of Entidades.SubLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SubLineaSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea)
                Do While lector.Read
                    Dim objSubLinea As New Entidades.SubLinea
                    objSubLinea.Id = CInt(IIf(IsDBNull(lector.Item("IdSubLInea")) = True, 0, lector.Item("IdSubLInea")))
                    objSubLinea.Nombre = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")) = True, "", lector.Item("sl_Nombre")))
                    Lista.Add(objSubLinea)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectActivoxLineaxTipoExistenciaCodigo(ByVal idLinea As Integer, ByVal idTipoEx As Integer) As List(Of Entidades.SubLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SubLineaSelectActivoxLineaxTipoExCodigo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idLinea)
        cmd.Parameters.AddWithValue("@IdTipoEx", idTipoEx)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea)
                Do While lector.Read
                    Dim objSubLinea As New Entidades.SubLinea
                    objSubLinea.AfectoDetraccion = CStr(IIf(IsDBNull(lector.Item("sl_AfectoDetraccion")) = True, "", lector.Item("sl_AfectoDetraccion")))
                    objSubLinea.AfectoIgv = CStr(IIf(IsDBNull(lector.Item("sl_AfectoIgv")) = True, "", lector.Item("sl_AfectoIgv")))
                    objSubLinea.AfectoPercepcion = CStr(IIf(IsDBNull(lector.Item("sl_AfectoPercepcion")) = True, "", lector.Item("sl_AfectoPercepcion")))
                    objSubLinea.CostoCompra = CStr(IIf(IsDBNull(lector.Item("sl_CostoCompra")) = True, "", lector.Item("sl_CostoCompra")))
                    objSubLinea.CostoVenta = CStr(IIf(IsDBNull(lector.Item("sl_CostoVenta")) = True, "", lector.Item("sl_CostoVenta")))
                    objSubLinea.CtaDebeCompra = CStr(IIf(IsDBNull(lector.Item("sl_CtaDebeCompra")) = True, "", lector.Item("sl_CtaDebeCompra")))
                    objSubLinea.CtaDebeVenta = CStr(IIf(IsDBNull(lector.Item("sl_CtaDebeVenta")) = True, "", lector.Item("sl_CtaDebeVenta")))
                    objSubLinea.CtaHaberCompra = CStr(IIf(IsDBNull(lector.Item("sl_CtaHaberCompra")) = True, "", lector.Item("sl_CtaHaberCompra")))
                    objSubLinea.CtaHaberVenta = CStr(IIf(IsDBNull(lector.Item("sl_CtaHaberVenta")) = True, "", lector.Item("sl_CtaHaberVenta")))
                    objSubLinea.Estado = CStr(IIf(IsDBNull(lector.Item("sl_Estado")) = True, "", lector.Item("sl_Estado")))
                    objSubLinea.CodigoSubLinea = CStr(IIf(IsDBNull(lector.Item("IdSubLInea")) = True, "", lector.Item("IdSubLInea")))
                    objSubLinea.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    objSubLinea.IdTipoExistencia = CInt(IIf(IsDBNull(lector.Item("IdTipoExistencia")) = True, 0, lector.Item("IdTipoExistencia")))
                    objSubLinea.Nombre = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")) = True, "", lector.Item("sl_Nombre")))
                    Lista.Add(objSubLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ValidadLongitud() As Entidades.SubLinea
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SubLineaValidadLongitud", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim objSubLinea As New Entidades.SubLinea
                If lector.Read Then
                    objSubLinea.Longitud = CInt(IIf(IsDBNull(lector.Item("sl_Longitud")) = True, 0, lector.Item("sl_Longitud")))
                    objSubLinea.Cantidad = CInt(IIf(IsDBNull(lector.Item("Cantidad")) = True, 0, lector.Item("Cantidad")))
                End If
                lector.Close()
                Return objSubLinea
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ValidadNombrexIdLinea(ByVal idlinea As Integer, ByVal nombre As String) As Entidades.SubLinea
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SubLinea_Validacion", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@sl_Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim objSubLinea As New Entidades.SubLinea
                If lector.Read Then
                    objSubLinea.Cantidad = CInt(IIf(IsDBNull(lector.Item("Item")) = True, 0, lector.Item("Item")))
                End If
                lector.Close()
                Return objSubLinea
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function



End Class
