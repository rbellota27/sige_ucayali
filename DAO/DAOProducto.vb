'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

' 11/02/2011

Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class DAOProducto
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectProductoxCodigoxDescripcion(ByVal CodigoProducto As String, ByVal NombreProducto As String) As List(Of Entidades.ProductoView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim lista As New List(Of Entidades.ProductoView)

        Try

            Dim cmd As New SqlCommand("SelectProductoxCodigoDescripcion", cn)
            cmd.CommandType = CommandType.StoredProcedure
            NombreProducto = NombreProducto.Replace("*", "%")
            cmd.Parameters.AddWithValue("@ProdNombre", NombreProducto)
            cmd.Parameters.AddWithValue("@CodigoProducto", CodigoProducto)
            cmd.CommandTimeout = 0
            cn.Open()
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If (lector.HasRows = True) Then
                While (lector.Read)
                    Dim obj As New Entidades.ProductoView
                    With obj
                        .Descripcion = CStr(IIf(IsDBNull(lector("prod_Nombre")), "", lector("prod_Nombre")))
                        .Id = CInt(IIf(IsDBNull(lector("IdProducto")), 0, lector("IdProducto")))
                        .Codigo = CStr(IIf(IsDBNull(lector("prod_codigo")), "", lector("prod_codigo")))
                    End With
                    lista.Add(obj)
                End While
            Else
                lista = Nothing
            End If
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return lista

    End Function

    Public Function fx_Get_CodProd_Lin_SubLin_AutoGen(ByVal IdSublinea As Integer, ByVal Longitud As Integer) As String
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand
        cmd.CommandText = "SELECT [dbo].[fx_Get_CodProd_Lin_SubLin_AutoGen](" + CStr(IdSublinea) + "," + CStr(Longitud) + ")"
        cmd.CommandType = CommandType.Text
        cmd.CommandTimeout = 0
        cmd.Connection = cn
        Try
            cn.Open()
            cmd.ExecuteScalar()
            Return cmd.ExecuteScalar().ToString()
        Catch ex As Exception
            Throw ex
        End Try

        Return Nothing
    End Function


    Public Function ReporteAlmacen(ByVal Linea As Integer, ByVal SubLinea As Integer, ByVal Fecha As String, ByVal Tienda As Integer, ByVal IdAlmacen As Integer) As List(Of Entidades.ReporteAlmacen)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("ReporteAlmacen", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdLinea", Linea)
        cmd.Parameters.AddWithValue("@IdSubLInea", SubLinea)
        cmd.Parameters.AddWithValue("@fecha", Fecha)
        cmd.Parameters.AddWithValue("@IdTienda", Tienda)
        cmd.Parameters.AddWithValue("@idalmacen", IdAlmacen)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ReporteAlmacen)
                Do While lector.Read
                    Dim objGrillaProd As New Entidades.ReporteAlmacen
                    objGrillaProd.IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                    objGrillaProd.CodigoSige = CStr(IIf(IsDBNull(lector.Item("Cod_SIGE")) = True, "", lector.Item("Cod_SIGE")))
                    objGrillaProd.Pais = CStr(IIf(IsDBNull(lector.Item("Pais")) = True, "", lector.Item("Pais")))
                    objGrillaProd.NombreProducto = CStr(IIf(IsDBNull(lector.Item("Prod_Nombre")) = True, "", lector.Item("Prod_Nombre")))
                    objGrillaProd.Cantidad = CDec(IIf(IsDBNull(lector.Item("CANTIDAD")) = True, 0, lector.Item("CANTIDAD")))
                    objGrillaProd.Costo = CDec(IIf(IsDBNull(lector.Item("COSTO")) = True, 0, lector.Item("COSTO")))
                    objGrillaProd.TipoExistencia = CStr(IIf(IsDBNull(lector.Item("TipoExistencia")) = True, "", lector.Item("TipoExistencia")))
                    objGrillaProd.Linea = CStr(IIf(IsDBNull(lector.Item("Linea")) = True, "", lector.Item("Linea")))
                    objGrillaProd.SubLinea = CStr(IIf(IsDBNull(lector.Item("SubLinea")) = True, "", lector.Item("SubLinea")))
                    objGrillaProd.Tienda = CStr(IIf(IsDBNull(lector.Item("Tienda")) = True, "", lector.Item("Tienda")))
                    objGrillaProd.PVP = CDec(IIf(IsDBNull(lector.Item("PVP")) = True, 0, lector.Item("PVP")))
                    objGrillaProd.UM = CStr(IIf(IsDBNull(lector.Item("UM")) = True, "", lector.Item("UM")))

                    Lista.Add(objGrillaProd)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function



    Public Function BuscarProducto_PaginadoConsulta(ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, _
                                             ByVal Nombre As String, _
                                            ByVal Codigo As String, ByVal Tb As List(Of Entidades.ProductoTipoTablaValor), _
                                            ByVal pageIndex As Integer, ByVal pagesize As Integer) As List(Of Entidades.GrillaProducto_M)
        Nombre = Nombre.Replace("*", "%")
        Dim dt As New DataTable
        dt.Columns.Add("IdTipoTabla", GetType(System.Int64))
        dt.Columns.Add("IdTipoTablaValor", GetType(System.Int64))

        If Tb IsNot Nothing Then
            For Each objprodTTV As Entidades.ProductoTipoTablaValor In Tb
                Dim dr As DataRow = dt.NewRow()
                dr(0) = objprodTTV.IdTipoTabla
                dr(1) = objprodTTV.IdTipoTablaValor
                dt.Rows.Add(dr)
            Next
        End If

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Producto_Listar_Paginado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoExistencia", IdTipoExistencia)
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        'cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        'cmd.Parameters.AddWithValue("@IdEstadoProd", IdEstadoProd)
        cmd.Parameters.AddWithValue("@Nombre", Nombre)
        cmd.Parameters.AddWithValue("@Codigo", Codigo)
        'cmd.Parameters.AddWithValue("@CodigoProveedor", CodigoProveedor)
        'cmd.Parameters.AddWithValue("@Tb", dt)
        cmd.Parameters.AddWithValue("@PageIndex", pageIndex)
        cmd.Parameters.AddWithValue("@PageSize", pagesize)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.GrillaProducto_M)
                Do While lector.Read
                    Dim objGrillaProd As New Entidades.GrillaProducto_M
                    objGrillaProd.IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                    objGrillaProd.NoVisible = CStr(IIf(IsDBNull(lector.Item("NoVisible")) = True, "", lector.Item("NoVisible")))
                    objGrillaProd.Cod_Proveedor = CStr(IIf(IsDBNull(lector.Item("Cod_Proveedor")) = True, "", lector.Item("Cod_Proveedor")))
                    objGrillaProd.prod_Codigo = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")) = True, "", lector.Item("prod_Codigo")))
                    objGrillaProd.prod_Atributos = CStr(IIf(IsDBNull(lector.Item("prod_Atributos")) = True, "", lector.Item("prod_Atributos")))
                    objGrillaProd.NomProducto = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                    objGrillaProd.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    objGrillaProd.NomLinea = CStr(IIf(IsDBNull(lector.Item("lin_Nombre")) = True, "", lector.Item("lin_Nombre")))
                    objGrillaProd.IdSubLinea = CInt(IIf(IsDBNull(lector.Item("IdSubLinea")) = True, 0, lector.Item("IdSubLinea")))
                    objGrillaProd.NomSubLinea = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")) = True, "", lector.Item("sl_Nombre")))
                    objGrillaProd.NomEstado = CStr(IIf(IsDBNull(lector.Item("ep_Nombre")) = True, "", lector.Item("ep_Nombre")))
                    objGrillaProd.PrecioCompra = CDec(IIf(IsDBNull(lector.Item("prod_PrecioCompra")) = True, 0, lector.Item("prod_PrecioCompra")))
                    objGrillaProd.IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                    objGrillaProd.NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_simbolo")) = True, "", lector.Item("mon_simbolo")))
                    Lista.Add(objGrillaProd)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function



    Public Function BuscarProducto_Paginado(ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, _
                                            ByVal IdEstadoProd As Integer, ByVal Nombre As String, _
                                            ByVal Codigo As String, ByVal CodigoProveedor As String, ByVal Tb As List(Of Entidades.ProductoTipoTablaValor), _
                                            ByVal pageIndex As Integer, ByVal pagesize As Integer) As List(Of Entidades.GrillaProducto_M)


        Nombre = Nombre.Replace("*", "%")
        Dim dt As New DataTable
        dt.Columns.Add("IdTipoTabla", GetType(System.Int64))
        dt.Columns.Add("IdTipoTablaValor", GetType(System.Int64))

        If Tb IsNot Nothing Then
            For Each objprodTTV As Entidades.ProductoTipoTablaValor In Tb
                Dim dr As DataRow = dt.NewRow()
                dr(0) = objprodTTV.IdTipoTabla
                dr(1) = objprodTTV.IdTipoTablaValor
                dt.Rows.Add(dr)
            Next
        End If

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Producto_Listar_Paginado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoExistencia", IdTipoExistencia)
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@IdEstadoProd", IdEstadoProd)
        cmd.Parameters.AddWithValue("@Nombre", Nombre)
        cmd.Parameters.AddWithValue("@Codigo", Codigo)
        cmd.Parameters.AddWithValue("@CodigoProveedor", CodigoProveedor)
        cmd.Parameters.AddWithValue("@Tb", dt)
        cmd.Parameters.AddWithValue("@PageIndex", pageIndex)
        cmd.Parameters.AddWithValue("@PageSize", pagesize)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.GrillaProducto_M)
                Do While lector.Read
                    Dim objGrillaProd As New Entidades.GrillaProducto_M
                    objGrillaProd.IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                    objGrillaProd.NoVisible = CStr(IIf(IsDBNull(lector.Item("NoVisible")) = True, "", lector.Item("NoVisible")))
                    objGrillaProd.Cod_Proveedor = CStr(IIf(IsDBNull(lector.Item("Cod_Proveedor")) = True, "", lector.Item("Cod_Proveedor")))
                    objGrillaProd.prod_Codigo = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")) = True, "", lector.Item("prod_Codigo")))
                    objGrillaProd.prod_Atributos = CStr(IIf(IsDBNull(lector.Item("prod_Atributos")) = True, "", lector.Item("prod_Atributos")))
                    objGrillaProd.NomProducto = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                    objGrillaProd.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    objGrillaProd.NomLinea = CStr(IIf(IsDBNull(lector.Item("lin_Nombre")) = True, "", lector.Item("lin_Nombre")))
                    objGrillaProd.IdSubLinea = CInt(IIf(IsDBNull(lector.Item("IdSubLinea")) = True, 0, lector.Item("IdSubLinea")))
                    objGrillaProd.NomSubLinea = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")) = True, "", lector.Item("sl_Nombre")))
                    objGrillaProd.NomEstado = CStr(IIf(IsDBNull(lector.Item("ep_Nombre")) = True, "", lector.Item("ep_Nombre")))
                    objGrillaProd.PrecioCompra = CDec(IIf(IsDBNull(lector.Item("prod_PrecioCompra")) = True, 0, lector.Item("prod_PrecioCompra")))
                    objGrillaProd.IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                    objGrillaProd.NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_simbolo")) = True, "", lector.Item("mon_simbolo")))
                    Lista.Add(objGrillaProd)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function fx_getValorEquivalenteProducto(ByVal IdProducto As Integer, ByVal IdUnidadMedida_Origen As Integer, ByVal IdUnidadMedida_Destino As Integer, ByVal valor As Decimal) As Decimal

        Dim equivalencia As Decimal = 0
        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Try

            cn.Open()

            Dim cmd As New SqlCommand("      select dbo.fx_getValorEquivalenteProducto (" + CStr(IdProducto) + "," + CStr(IdUnidadMedida_Origen) + "," + CStr(IdUnidadMedida_Destino) + "," + CStr(valor) + ")         ", cn)
            cmd.CommandType = CommandType.Text

            equivalencia = CDec(cmd.ExecuteScalar)

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return equivalencia

    End Function
    Public Function Producto_ConsultaEquivalenciaProductoxParams(ByVal IdProducto As Integer, ByVal IdUnidadMedida_Entrada As Integer, ByVal IdUnidadMedida_Salida As Integer, ByVal Cantidad_Ingreso As Decimal, ByVal TablaUM As DataTable, ByVal utilizarRedondeoUp As Integer) As DataTable

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ds As New DataSet

        Try

            Dim cmd As New SqlCommand("_Producto_ConsultaEquivalenciaProductoxParams", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
            cmd.Parameters.AddWithValue("@IdUnidadMedida_Entrada", IdUnidadMedida_Entrada)
            cmd.Parameters.AddWithValue("@IdUnidadMedida_Salida", IdUnidadMedida_Salida)
            cmd.Parameters.AddWithValue("@Cantidad_Ingreso", Cantidad_Ingreso)
            cmd.Parameters.AddWithValue("@TablaUM", TablaUM)
            cmd.Parameters.AddWithValue("@utilizarRedondeoUp", utilizarRedondeoUp)
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "ProductoEQ")

        Catch ex As Exception
            Throw ex
        End Try

        Return ds.Tables("ProductoEQ")

    End Function
   
    Public Function ProductoSelectxIdProducto(ByVal IdProducto As Integer) As Entidades.ProductoView

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim objProducto As Entidades.ProductoView = Nothing
        Try

            Dim p() As SqlParameter = New SqlParameter(0) {}

            p(0) = New SqlParameter("@IdProducto", SqlDbType.Int)
            p(0).Value = IdProducto

            cn.Open()
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_ProductoSelectxIdProducto", p)
            While (reader.Read)

                objProducto = New Entidades.ProductoView
                With objProducto

                    .Id = CInt(IIf(IsDBNull(reader("IdProducto")) = True, 0, reader("IdProducto")))
                    .Descripcion = CStr(IIf(IsDBNull(reader("Descripcion")) = True, "", reader("Descripcion")))
                    .PrecioCompra = CDec(IIf(IsDBNull(reader("PrecioCompra")) = True, 0, reader("PrecioCompra")))
                    .IdMoneda = CInt(IIf(IsDBNull(reader("IdMoneda")) = True, 0, reader("IdMoneda")))
                    .IdSubLInea = CInt(IIf(IsDBNull(reader("IdSubLInea")) = True, 0, reader("IdSubLInea")))
                    .IdEstadoProd = CInt(IIf(IsDBNull(reader("IdEstadoProd")) = True, 0, reader("IdEstadoProd")))
                    .Codigo = CStr(IIf(IsDBNull(reader("CodigoProducto")) = True, "", reader("CodigoProducto")))
                    .SubLinea = CStr(IIf(IsDBNull(reader("SubLinea")) = True, "", reader("SubLinea")))
                    .IdLinea = CInt(IIf(IsDBNull(reader("IdLinea")) = True, 0, reader("IdLinea")))
                    .Linea = CStr(IIf(IsDBNull(reader("Linea")) = True, "", reader("Linea")))
                    .Moneda = CStr(IIf(IsDBNull(reader("Moneda_PC")) = True, "", reader("Moneda_PC")))
                    .UnidadMedida = CStr(IIf(IsDBNull(reader("UnidadMedida")) = True, "", reader("UnidadMedida")))
                    .IdUnidadMedida = CInt(IIf(IsDBNull(reader("IdUnidadMedida")) = True, 0, reader("IdUnidadMedida")))

                End With

            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objProducto

    End Function
    Public Function InsertaProductoTipoMercaderia(idUsuario As Integer, ByVal prod As Entidades.Producto, ByVal cn As SqlConnection, Optional ByVal tr As SqlTransaction = Nothing) As Integer
        'recibe la conexion abierta
        Try

            Dim ArrayParametros() As SqlParameter = New SqlParameter(26) {}
            ArrayParametros(0) = New SqlParameter("@prod_CodigoBarras", SqlDbType.VarChar)
            ArrayParametros(0).Value = IIf(prod.CodigoBarras = Nothing, DBNull.Value, prod.CodigoBarras)
            ArrayParametros(1) = New SqlParameter("@prod_Nombre", SqlDbType.VarChar)
            ArrayParametros(1).Value = IIf(prod.Descripcion = Nothing, DBNull.Value, prod.Descripcion)
            ArrayParametros(2) = New SqlParameter("@prod_StockMin", SqlDbType.Int)
            ArrayParametros(2).Value = IIf(prod.StockMinimo = Nothing, DBNull.Value, prod.StockMinimo)
            ArrayParametros(3) = New SqlParameter("@prod_StockMax", SqlDbType.Int)
            ArrayParametros(3).Value = IIf(prod.StockMaximo = Nothing, DBNull.Value, prod.StockMaximo)
            ArrayParametros(4) = New SqlParameter("@prod_StockReposicion", SqlDbType.Int)
            ArrayParametros(4).Value = IIf(prod.StockReposicion = Nothing, DBNull.Value, prod.StockReposicion)
            ArrayParametros(5) = New SqlParameter("@IdSubLInea", SqlDbType.Int)
            ArrayParametros(5).Value = IIf(prod.IdSubLInea = Nothing, DBNull.Value, prod.IdSubLInea)
            ArrayParametros(6) = New SqlParameter("@IdProveedor", SqlDbType.Int)
            ArrayParametros(6).Value = IIf(prod.IdProveedor = Nothing, DBNull.Value, prod.IdProveedor)
            ArrayParametros(7) = New SqlParameter("@IdColor", SqlDbType.Int)
            ArrayParametros(7).Value = IIf(prod.IdColor = Nothing, DBNull.Value, prod.IdColor)
            ArrayParametros(8) = New SqlParameter("@IdEstadoProd", SqlDbType.Int)
            ArrayParametros(8).Value = IIf(prod.IdEstadoProd = Nothing, DBNull.Value, prod.IdEstadoProd)
            'ArrayParametros(14) = New SqlParameter("@IdFormaProd", SqlDbType.Int)
            'ArrayParametros(14).Value = IIf(prod.IdFormaProducto = Nothing, DBNull.Value, prod.IdFormaProducto)
            ArrayParametros(9) = New SqlParameter("@IdTipoProd", SqlDbType.Int)
            ArrayParametros(9).Value = IIf(prod.IdTipoProducto = Nothing, DBNull.Value, prod.IdTipoProducto)

            ArrayParametros(10) = New SqlParameter("@NoVisible", SqlDbType.VarChar, 30)
            ArrayParametros(10).Value = IIf(prod.NoVisible = Nothing, DBNull.Value, prod.NoVisible)

            ArrayParametros(11) = New SqlParameter("@prod_Codigo", SqlDbType.VarChar, 30)
            ArrayParametros(11).Value = IIf(prod.Codigo = Nothing, DBNull.Value, prod.Codigo)

            ArrayParametros(12) = New SqlParameter("@prod_Atributos", SqlDbType.VarChar, 30)
            ArrayParametros(12).Value = IIf(prod.CodAtributos = Nothing, DBNull.Value, prod.CodAtributos)

            ArrayParametros(13) = New SqlParameter("@Cod_Proveedor", SqlDbType.VarChar, 20)
            ArrayParametros(13).Value = IIf(prod.CodProveedor = Nothing, DBNull.Value, prod.CodProveedor)
            ArrayParametros(14) = New SqlParameter("@IdProd", SqlDbType.Int)
            ArrayParametros(14).Direction = ParameterDirection.Output
            ArrayParametros(14).Value = 0

            ArrayParametros(15) = New SqlParameter("@prod_Kit", SqlDbType.Bit)
            ArrayParametros(15).Value = IIf(prod.Kit = Nothing, DBNull.Value, prod.Kit)

            ArrayParametros(16) = New SqlParameter("@prod_CalculoFlete", SqlDbType.Char)
            ArrayParametros(16).Value = IIf(prod.CalculoFlete = Nothing, DBNull.Value, prod.CalculoFlete)

            ArrayParametros(17) = New SqlParameter("@prod_SinUso", SqlDbType.VarChar)
            ArrayParametros(17).Value = IIf(prod.prod_SinUso = Nothing, DBNull.Value, prod.prod_SinUso)

            ArrayParametros(18) = New SqlParameter("@prod_CodbarraFabricante", SqlDbType.VarChar)
            ArrayParametros(18).Value = IIf(prod.CodBarrasFabricante = Nothing, DBNull.Value, prod.CodBarrasFabricante)

            ArrayParametros(19) = New SqlParameter("@prod_AfectoPercepcion", SqlDbType.Int)
            ArrayParametros(19).Value = IIf(prod.prod_AfectoPercepcion = Nothing, DBNull.Value, prod.prod_AfectoPercepcion)

            ArrayParametros(20) = New SqlParameter("@CodAntiguo", SqlDbType.VarChar)
            ArrayParametros(20).Value = IIf(prod.CodAntiguo = Nothing, DBNull.Value, prod.CodAntiguo)

            ArrayParametros(21) = New SqlParameter("@Longitud", SqlDbType.Int)
            ArrayParametros(21).Value = IIf(prod.prod_Longitud_AutoGen = Nothing, DBNull.Value, prod.prod_Longitud_AutoGen)

            ArrayParametros(22) = New SqlParameter("@flagTG", SqlDbType.Int)
            ArrayParametros(22).Value = IIf(prod.flagTG = Nothing, DBNull.Value, prod.flagTG)

            ArrayParametros(23) = New SqlParameter("@leadTime", SqlDbType.Decimal)
            ArrayParametros(23).Value = IIf(prod.LeadTime = Nothing, DBNull.Value, prod.LeadTime)

            ArrayParametros(24) = New SqlParameter("@VolMinCompra", SqlDbType.Decimal)
            ArrayParametros(24).Value = IIf(prod.VolMinCompra = Nothing, DBNull.Value, prod.VolMinCompra)

            ArrayParametros(25) = New SqlParameter("@ReposicionWeeks", SqlDbType.Decimal)
            ArrayParametros(25).Value = IIf(prod.ReposicionWeeks = Nothing, DBNull.Value, prod.ReposicionWeeks)

            ArrayParametros(26) = New SqlParameter("@idUsuario", SqlDbType.Int)
            ArrayParametros(26).Value = idUsuario


            If tr IsNot Nothing Then
                Return HDAO.InsertaTParameterOutPut(cn, "_ProductoTipoMercaderiaInsert", ArrayParametros, tr)
            Else
                Return HDAO.Insert_ReturnParameter(cn, "_ProductoTipoMercaderiaInsert", ArrayParametros)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function ActualizaProductoTipoMercaderia(idUsuario As Integer, ByVal prod As Entidades.Producto, ByVal cn As SqlConnection, Optional ByVal tr As SqlTransaction = Nothing) As Boolean
        Try
            Dim ArrayParametros() As SqlParameter = New SqlParameter(25) {}
            ArrayParametros(0) = New SqlParameter("@prod_CodigoBarras", SqlDbType.VarChar)
            ArrayParametros(0).Value = IIf(prod.CodigoBarras = Nothing, DBNull.Value, prod.CodigoBarras)
            ArrayParametros(1) = New SqlParameter("@prod_Nombre", SqlDbType.VarChar)
            ArrayParametros(1).Value = IIf(prod.Descripcion = Nothing, DBNull.Value, prod.Descripcion)
            ArrayParametros(2) = New SqlParameter("@prod_StockMin", SqlDbType.Int)
            ArrayParametros(2).Value = IIf(prod.StockMinimo = Nothing, DBNull.Value, prod.StockMinimo)
            ArrayParametros(3) = New SqlParameter("@prod_StockMax", SqlDbType.Int)
            ArrayParametros(3).Value = IIf(prod.StockMaximo = Nothing, DBNull.Value, prod.StockMaximo)
            ArrayParametros(4) = New SqlParameter("@prod_StockReposicion", SqlDbType.Int)
            ArrayParametros(4).Value = IIf(prod.StockReposicion = Nothing, DBNull.Value, prod.StockReposicion)
            ArrayParametros(5) = New SqlParameter("@IdSubLInea", SqlDbType.Int)
            ArrayParametros(5).Value = IIf(prod.IdSubLInea = Nothing, DBNull.Value, prod.IdSubLInea)
            ArrayParametros(6) = New SqlParameter("@IdProveedor", SqlDbType.Int)
            ArrayParametros(6).Value = IIf(prod.IdProveedor = Nothing, DBNull.Value, prod.IdProveedor)
            ArrayParametros(7) = New SqlParameter("@IdColor", SqlDbType.Int)
            ArrayParametros(7).Value = IIf(prod.IdColor = Nothing, DBNull.Value, prod.IdColor)
            ArrayParametros(8) = New SqlParameter("@IdEstadoProd", SqlDbType.Int)
            ArrayParametros(8).Value = IIf(prod.IdEstadoProd = Nothing, DBNull.Value, prod.IdEstadoProd)

            ArrayParametros(9) = New SqlParameter("@IdTipoProd", SqlDbType.Int)
            ArrayParametros(9).Value = IIf(prod.IdTipoProducto = Nothing, DBNull.Value, prod.IdTipoProducto)

            ArrayParametros(10) = New SqlParameter("@NoVisible", SqlDbType.VarChar, 30)
            ArrayParametros(10).Value = IIf(prod.NoVisible = Nothing, DBNull.Value, prod.NoVisible)

            ArrayParametros(11) = New SqlParameter("@prod_Codigo", SqlDbType.VarChar, 30)
            ArrayParametros(11).Value = IIf(prod.Codigo = Nothing, DBNull.Value, prod.Codigo)

            ArrayParametros(12) = New SqlParameter("@prod_Atributos", SqlDbType.VarChar, 30)
            ArrayParametros(12).Value = IIf(prod.CodAtributos = Nothing, DBNull.Value, prod.CodAtributos)

            ArrayParametros(13) = New SqlParameter("@Cod_Proveedor", SqlDbType.VarChar, 20)
            ArrayParametros(13).Value = IIf(prod.CodProveedor = Nothing, DBNull.Value, prod.CodProveedor)
            ArrayParametros(14) = New SqlParameter("@IdProducto", SqlDbType.Int)
            ArrayParametros(14).Value = IIf(prod.Id = Nothing, DBNull.Value, prod.Id)

            ArrayParametros(15) = New SqlParameter("@prod_Kit", SqlDbType.Bit)
            ArrayParametros(15).Value = IIf(prod.Kit = Nothing, DBNull.Value, prod.Kit)

            ArrayParametros(16) = New SqlParameter("@prod_CalculoFlete", SqlDbType.Char)
            ArrayParametros(16).Value = IIf(prod.CalculoFlete = Nothing, DBNull.Value, prod.CalculoFlete)

            ArrayParametros(17) = New SqlParameter("@prod_SinUso", SqlDbType.VarChar)
            ArrayParametros(17).Value = IIf(prod.prod_SinUso = Nothing, DBNull.Value, prod.prod_SinUso)

            ArrayParametros(18) = New SqlParameter("@prod_CodbarraFabricante", SqlDbType.VarChar)
            ArrayParametros(18).Value = IIf(prod.CodBarrasFabricante = Nothing, DBNull.Value, prod.CodBarrasFabricante)

            ArrayParametros(19) = New SqlParameter("@prod_AfectoPercepcion", SqlDbType.Int)
            ArrayParametros(19).Value = IIf(prod.prod_AfectoPercepcion = Nothing, DBNull.Value, prod.prod_AfectoPercepcion)

            ArrayParametros(20) = New SqlParameter("@CodAntiguo", SqlDbType.VarChar)
            ArrayParametros(20).Value = IIf(prod.CodAntiguo = Nothing, DBNull.Value, prod.CodAntiguo)

            ArrayParametros(21) = New SqlParameter("@flagTG", SqlDbType.Int)
            ArrayParametros(21).Value = IIf(prod.flagTG = Nothing, DBNull.Value, prod.flagTG)

            ArrayParametros(22) = New SqlParameter("@leadTime", SqlDbType.Decimal)
            ArrayParametros(22).Value = IIf(prod.LeadTime = Nothing, DBNull.Value, prod.LeadTime)

            ArrayParametros(23) = New SqlParameter("@VolMinCompra", SqlDbType.Decimal)
            ArrayParametros(23).Value = IIf(prod.VolMinCompra = Nothing, DBNull.Value, prod.VolMinCompra)

            ArrayParametros(24) = New SqlParameter("@ReposicionWeeks", SqlDbType.Decimal)
            ArrayParametros(24).Value = IIf(prod.ReposicionWeeks = Nothing, DBNull.Value, prod.ReposicionWeeks)

            ArrayParametros(25) = New SqlParameter("@idUsuario", SqlDbType.Int)
            ArrayParametros(25).Value = idUsuario

            HDAO.UpdateT(cn, "_ProductoTipoMercaderiaUpdate", ArrayParametros, tr)
            Return True
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function
    Public Function InsertaProducto(ByVal producto As Entidades.Producto) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(17) {}
        ArrayParametros(0) = New SqlParameter("@prod_CodigoBarras", SqlDbType.VarChar)
        ArrayParametros(0).Value = producto.CodigoBarras
        ArrayParametros(1) = New SqlParameter("@IdContenido", SqlDbType.Int)
        ArrayParametros(1).Value = producto.IdContenido
        ArrayParametros(2) = New SqlParameter("@prod_Nombre", SqlDbType.VarChar)
        ArrayParametros(2).Value = producto.Descripcion
        ArrayParametros(3) = New SqlParameter("@prod_StockMin", SqlDbType.Int)
        ArrayParametros(3).Value = producto.StockMinimo
        ArrayParametros(4) = New SqlParameter("@prod_StockMax", SqlDbType.Int)
        ArrayParametros(4).Value = producto.StockMaximo
        ArrayParametros(5) = New SqlParameter("@prod_StockActual", SqlDbType.Decimal)
        ArrayParametros(5).Value = producto.StockActual

        ArrayParametros(6) = New SqlParameter("@prod_StockReposicion", SqlDbType.Int)
        ArrayParametros(6).Value = producto.StockReposicion

        ArrayParametros(7) = New SqlParameter("@IdSubLInea", SqlDbType.Int)
        ArrayParametros(7).Value = producto.IdSubLInea
        ArrayParametros(8) = New SqlParameter("@IdProveedor", SqlDbType.Int)
        ArrayParametros(8).Value = producto.IdProveedor
        ArrayParametros(9) = New SqlParameter("@IdPropietario", SqlDbType.Int)
        ArrayParametros(9).Value = producto.IdPropietario
        ArrayParametros(10) = New SqlParameter("@IdMarca", SqlDbType.Int)
        ArrayParametros(10).Value = producto.IdMarca
        ArrayParametros(11) = New SqlParameter("@IdMaterial", SqlDbType.Int)
        ArrayParametros(11).Value = producto.IdMaterial
        ArrayParametros(12) = New SqlParameter("@IdFabricante", SqlDbType.Int)
        ArrayParametros(12).Value = producto.IdFabricante
        'ArrayParametros(12) = New SqlParameter("@IdSabor", SqlDbType.Int)
        'ArrayParametros(12).Value = producto.IdSabor
        ArrayParametros(13) = New SqlParameter("@IdColor", SqlDbType.Int)
        ArrayParametros(13).Value = producto.IdColor
        'ArrayParametros(14) = New SqlParameter("@IdTamanio", SqlDbType.Int)
        'ArrayParametros(14).Value = producto.IdTamanio
        'ArrayParametros(15) = New SqlParameter("@IdTalla", SqlDbType.Int)
        'ArrayParametros(15).Value = producto.IdTalla
        ArrayParametros(14) = New SqlParameter("@IdEstadoProd", SqlDbType.Int)
        ArrayParametros(14).Value = producto.IdEstadoProd
        ArrayParametros(15) = New SqlParameter("@IdPerUbicacion", SqlDbType.Int)
        ArrayParametros(15).Value = producto.IdPerUbicacion
        ArrayParametros(16) = New SqlParameter("@IdMoraEnvase", SqlDbType.Int)
        ArrayParametros(16).Value = producto.IdMoraEnvase
        ArrayParametros(17) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        ArrayParametros(17).Value = producto.IdUsuario
        Return HDAO.Insert(cn, "_ProductoInsert", ArrayParametros)
    End Function
    Public Function ActualizaProducto(ByVal producto As Entidades.Producto) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(19) {}
        ArrayParametros(0) = New SqlParameter("@prod_CodigoBarras", SqlDbType.VarChar)
        ArrayParametros(0).Value = producto.CodigoBarras
        ArrayParametros(1) = New SqlParameter("@IdContenido", SqlDbType.Int)
        ArrayParametros(1).Value = producto.IdContenido
        ArrayParametros(2) = New SqlParameter("@prod_Nombre", SqlDbType.VarChar)
        ArrayParametros(2).Value = producto.Descripcion
        ArrayParametros(3) = New SqlParameter("@prod_FechaAlta", SqlDbType.DateTime)
        ArrayParametros(3).Value = producto.FechaAlta
        ArrayParametros(4) = New SqlParameter("@prod_StockMin", SqlDbType.Int)
        ArrayParametros(4).Value = producto.StockMinimo
        ArrayParametros(5) = New SqlParameter("@prod_StockMax", SqlDbType.Int)
        ArrayParametros(5).Value = producto.StockMaximo
        ArrayParametros(6) = New SqlParameter("@prod_StockActual", SqlDbType.Decimal)
        ArrayParametros(6).Value = producto.StockActual

        ArrayParametros(7) = New SqlParameter("@prod_StockReposicion", SqlDbType.Decimal)
        ArrayParametros(7).Value = producto.StockReposicion

        ArrayParametros(8) = New SqlParameter("@IdSubLInea", SqlDbType.Int)
        ArrayParametros(8).Value = producto.IdSubLInea
        ArrayParametros(9) = New SqlParameter("@IdProveedor", SqlDbType.Int)
        ArrayParametros(9).Value = producto.IdProveedor
        ArrayParametros(10) = New SqlParameter("@IdPropietario", SqlDbType.Int)
        ArrayParametros(10).Value = producto.IdPropietario
        ArrayParametros(11) = New SqlParameter("@IdMarca", SqlDbType.Int)
        ArrayParametros(11).Value = producto.IdMarca
        ArrayParametros(12) = New SqlParameter("@IdMaterial", SqlDbType.Int)
        ArrayParametros(12).Value = producto.IdMaterial
        ArrayParametros(13) = New SqlParameter("@IdFabricante", SqlDbType.Int)
        ArrayParametros(13).Value = producto.IdFabricante
        'ArrayParametros(13) = New SqlParameter("@IdSabor", SqlDbType.Int)
        'ArrayParametros(13).Value = producto.IdSabor
        ArrayParametros(14) = New SqlParameter("@IdColor", SqlDbType.Int)
        ArrayParametros(14).Value = producto.IdColor
        'ArrayParametros(15) = New SqlParameter("@IdTamanio", SqlDbType.Int)
        'ArrayParametros(15).Value = producto.IdTamanio
        'ArrayParametros(16) = New SqlParameter("@IdTalla", SqlDbType.Int)
        'ArrayParametros(16).Value = producto.IdTalla
        ArrayParametros(15) = New SqlParameter("@IdEstadoProd", SqlDbType.Int)
        ArrayParametros(15).Value = producto.IdEstadoProd
        ArrayParametros(16) = New SqlParameter("@IdPerUbicacion", SqlDbType.Int)
        ArrayParametros(16).Value = producto.IdPerUbicacion
        ArrayParametros(17) = New SqlParameter("@IdMoraEnvase", SqlDbType.Int)
        ArrayParametros(17).Value = producto.IdMoraEnvase
        ArrayParametros(18) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        ArrayParametros(18).Value = producto.IdUsuario
        ArrayParametros(19) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(19).Value = producto.Id
        Return HDAO.Update(cn, "_ProductoUpdate", ArrayParametros)
    End Function
    Public Function ActualizaProductoPrecioCompra(idUsuario As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal idProducto As Integer, ByVal precioCompra As Decimal, ByVal IdMoneda As Integer) As Boolean
        'recibo la conexion abierta
        Try
            Dim cmd As New SqlCommand("_ProductoUpdatePrecioCompraxIdProducto", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdProducto", IIf(idProducto = Nothing, DBNull.Value, idProducto))
            cmd.Parameters.AddWithValue("@PrecioCompra", IIf(precioCompra = Nothing, DBNull.Value, precioCompra))
            cmd.Parameters.AddWithValue("@IdMoneda", IIf(IdMoneda = Nothing, DBNull.Value, IdMoneda))
            cmd.Parameters.AddWithValue("@idUsuario", IIf(idUsuario = Nothing, DBNull.Value, idUsuario))
            If cmd.ExecuteNonQuery = 0 Then
                Return False
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.Producto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Producto)
                Do While lector.Read
                    Dim prod As New Entidades.Producto
                    prod.CodigoBarras = CStr(lector.Item("prod_CodigoBarras"))
                    prod.Descripcion = CStr(lector.Item("prod_Nombre"))
                    prod.FechaAlta = CDate(lector.Item("prod_FechaAlta"))
                    prod.Id = CInt(lector.Item("IdProducto"))
                    prod.IdColor = CInt(lector.Item("IdColor"))
                    prod.IdContenido = CInt(lector.Item("IdContenido"))
                    prod.IdEstadoProd = CInt(lector.Item("IdEstadoProd"))
                    prod.IdFabricante = CInt(lector.Item("IdFabricante"))
                    prod.IdMarca = CInt(lector.Item("IdMarca"))
                    prod.IdMaterial = CInt(lector.Item("IdMaterial"))
                    prod.IdMoraEnvase = CInt(lector.Item("IdMoraEnvase"))
                    prod.IdPerUbicacion = CInt(lector.Item("IdPerUbicacion"))
                    prod.IdPropietario = CInt(lector.Item("IdPropietario"))
                    prod.IdProveedor = CInt(lector.Item("IdProveedor"))
                    'prod.IdSabor = CInt(lector.Item("IdSabor"))
                    prod.IdSubLInea = CInt(lector.Item("IdSubLInea"))
                    'prod.IdTalla = CInt(lector.Item("IdTalla"))
                    'prod.IdTamanio = CInt(lector.Item("IdTamanio"))
                    prod.IdUsuario = CInt(lector.Item("IdUsuario"))
                    prod.StockActual = CDec(lector.Item("prod_StockActual"))
                    prod.StockMaximo = CInt(lector.Item("prod_StockMax"))
                    prod.StockMinimo = CInt(lector.Item("prod_StockMin"))
                    Lista.Add(prod)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Producto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Producto)
                Do While lector.Read
                    Dim prod As New Entidades.Producto
                    prod.CodigoBarras = CStr(lector.Item("prod_CodigoBarras"))
                    prod.Descripcion = CStr(lector.Item("prod_Nombre"))
                    prod.FechaAlta = CDate(lector.Item("prod_FechaAlta"))
                    prod.Id = CInt(lector.Item("IdProducto"))
                    prod.IdColor = CInt(lector.Item("IdColor"))
                    prod.IdContenido = CInt(lector.Item("IdContenido"))
                    prod.IdEstadoProd = CInt(lector.Item("IdEstadoProd"))
                    prod.IdFabricante = CInt(lector.Item("IdFabricante"))
                    prod.IdMarca = CInt(lector.Item("IdMarca"))
                    prod.IdMaterial = CInt(lector.Item("IdMaterial"))
                    prod.IdMoraEnvase = CInt(lector.Item("IdMoraEnvase"))
                    prod.IdPerUbicacion = CInt(lector.Item("IdPerUbicacion"))
                    prod.IdPropietario = CInt(lector.Item("IdPropietario"))
                    prod.IdProveedor = CInt(lector.Item("IdProveedor"))
                    'prod.IdSabor = CInt(lector.Item("IdSabor"))
                    prod.IdSubLInea = CInt(lector.Item("IdSubLInea"))
                    'prod.IdTalla = CInt(lector.Item("IdTalla"))
                    'prod.IdTamanio = CInt(lector.Item("IdTamanio"))
                    prod.IdUsuario = CInt(lector.Item("IdUsuario"))
                    prod.StockActual = CDec(lector.Item("prod_StockActual"))
                    prod.StockMaximo = CInt(lector.Item("prod_StockMax"))
                    prod.StockMinimo = CInt(lector.Item("prod_StockMin"))
                    Lista.Add(prod)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Producto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Producto)
                Do While lector.Read
                    Dim prod As New Entidades.Producto
                    prod.CodigoBarras = CStr(lector.Item("prod_CodigoBarras"))
                    prod.Descripcion = CStr(lector.Item("prod_Nombre"))
                    prod.FechaAlta = CDate(lector.Item("prod_FechaAlta"))
                    prod.Id = CInt(lector.Item("IdProducto"))
                    prod.IdColor = CInt(lector.Item("IdColor"))
                    prod.IdContenido = CInt(lector.Item("IdContenido"))
                    prod.IdEstadoProd = CInt(lector.Item("IdEstadoProd"))
                    prod.IdFabricante = CInt(lector.Item("IdFabricante"))
                    prod.IdMarca = CInt(lector.Item("IdMarca"))
                    prod.IdMaterial = CInt(lector.Item("IdMaterial"))
                    prod.IdMoraEnvase = CInt(lector.Item("IdMoraEnvase"))
                    prod.IdPerUbicacion = CInt(lector.Item("IdPerUbicacion"))
                    prod.IdPropietario = CInt(lector.Item("IdPropietario"))
                    prod.IdProveedor = CInt(lector.Item("IdProveedor"))
                    'prod.IdSabor = CInt(lector.Item("IdSabor"))
                    prod.IdSubLInea = CInt(lector.Item("IdSubLInea"))
                    'prod.IdTalla = CInt(lector.Item("IdTalla"))
                    'prod.IdTamanio = CInt(lector.Item("IdTamanio"))
                    prod.IdUsuario = CInt(lector.Item("IdUsuario"))
                    prod.StockActual = CDec(lector.Item("prod_StockActual"))
                    prod.StockMaximo = CInt(lector.Item("prod_StockMax"))
                    prod.StockMinimo = CInt(lector.Item("prod_StockMin"))
                    Lista.Add(prod)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectPrecioCompra(ByVal cn As SqlConnection, ByVal IdProducto As Integer, ByVal tr As SqlTransaction) As Decimal
        Dim cmd As New SqlCommand("_ProductoSelectPrecioCompraxIdProducto", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        Dim lector As SqlDataReader
        lector = cmd.ExecuteReader()
        Dim costoCompra As Decimal = 0
        If lector.Read Then
            costoCompra = CDec(IIf(IsDBNull(lector.Item("prod_PrecioCompra")) = True, 0, lector.Item("prod_PrecioCompra")))
        End If
        lector.Close()
        Return costoCompra
    End Function
    Public Function SelectPrecioCompra(ByVal IdProducto As Integer) As Decimal
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim costoCompra As Decimal = 0
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim cmd As New SqlCommand("_ProductoSelectPrecioCompraxIdProducto", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
            Dim lector As SqlDataReader
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If lector.Read Then
                costoCompra = CDec(IIf(IsDBNull(lector.Item("prod_PrecioCompra")) = True, 0, lector.Item("prod_PrecioCompra")))
            End If
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return costoCompra
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Producto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Producto)
                Do While lector.Read
                    Dim prod As New Entidades.Producto
                    prod.CodigoBarras = CStr(lector.Item("prod_CodigoBarras"))
                    prod.Descripcion = CStr(lector.Item("prod_Nombre"))
                    prod.FechaAlta = CDate(lector.Item("prod_FechaAlta"))
                    prod.Id = CInt(lector.Item("IdProducto"))
                    prod.IdColor = CInt(lector.Item("IdColor"))
                    prod.IdContenido = CInt(lector.Item("IdContenido"))
                    prod.IdEstadoProd = CInt(lector.Item("IdEstadoProd"))
                    prod.IdFabricante = CInt(lector.Item("IdFabricante"))
                    prod.IdMarca = CInt(lector.Item("IdMarca"))
                    prod.IdMaterial = CInt(lector.Item("IdMaterial"))
                    prod.IdMoraEnvase = CInt(lector.Item("IdMoraEnvase"))
                    prod.IdPerUbicacion = CInt(lector.Item("IdPerUbicacion"))
                    prod.IdPropietario = CInt(lector.Item("IdPropietario"))
                    prod.IdProveedor = CInt(lector.Item("IdProveedor"))
                    'prod.IdSabor = CInt(lector.Item("IdSabor"))
                    prod.IdSubLInea = CInt(lector.Item("IdSubLInea"))
                    'prod.IdTalla = CInt(lector.Item("IdTalla"))
                    'prod.IdTamanio = CInt(lector.Item("IdTamanio"))
                    prod.IdUsuario = CInt(lector.Item("IdUsuario"))
                    prod.StockActual = CDec(lector.Item("prod_StockActual"))
                    prod.StockMaximo = CInt(lector.Item("prod_StockMax"))
                    prod.StockMinimo = CInt(lector.Item("prod_StockMin"))
                    Lista.Add(prod)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllxLinea(ByVal idLinea As Integer) As List(Of Entidades.Producto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoSelectAllxLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idLinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Producto)
                Do While lector.Read
                    Dim prod As New Entidades.Producto
                    prod.CodigoBarras = CStr(lector.Item("prod_CodigoBarras"))
                    prod.Descripcion = CStr(lector.Item("prod_Nombre"))
                    prod.FechaAlta = CDate(lector.Item("prod_FechaAlta"))
                    prod.Id = CInt(lector.Item("IdProducto"))
                    prod.IdColor = CInt(lector.Item("IdColor"))
                    prod.IdContenido = CInt(lector.Item("IdContenido"))
                    prod.IdEstadoProd = CInt(lector.Item("IdEstadoProd"))
                    prod.IdFabricante = CInt(lector.Item("IdFabricante"))
                    prod.IdMarca = CInt(lector.Item("IdMarca"))
                    prod.IdMaterial = CInt(lector.Item("IdMaterial"))
                    prod.IdMoraEnvase = CInt(lector.Item("IdMoraEnvase"))
                    prod.IdPerUbicacion = CInt(lector.Item("IdPerUbicacion"))
                    prod.IdPropietario = CInt(lector.Item("IdPropietario"))
                    prod.IdProveedor = CInt(lector.Item("IdProveedor"))
                    'prod.IdSabor = CInt(lector.Item("IdSabor"))
                    prod.IdSubLInea = CInt(lector.Item("IdSubLInea"))
                    'prod.IdTalla = CInt(lector.Item("IdTalla"))
                    'prod.IdTamanio = CInt(lector.Item("IdTamanio"))
                    prod.IdUsuario = CInt(lector.Item("IdUsuario"))
                    prod.StockActual = CDec(lector.Item("prod_StockActual"))
                    prod.StockMaximo = CInt(lector.Item("prod_StockMax"))
                    prod.StockMinimo = CInt(lector.Item("prod_StockMin"))
                    Lista.Add(prod)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectActivoxLinea(ByVal idLinea As Integer) As List(Of Entidades.Producto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoSelectActivoxLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idLinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Producto)
                Do While lector.Read
                    Dim prod As New Entidades.Producto
                    prod.CodigoBarras = CStr(lector.Item("prod_CodigoBarras"))
                    prod.Descripcion = CStr(lector.Item("prod_Nombre"))
                    prod.FechaAlta = CDate(lector.Item("prod_FechaAlta"))
                    prod.Id = CInt(lector.Item("IdProducto"))
                    prod.IdColor = CInt(lector.Item("IdColor"))
                    prod.IdContenido = CInt(lector.Item("IdContenido"))
                    prod.IdEstadoProd = CInt(lector.Item("IdEstadoProd"))
                    prod.IdFabricante = CInt(lector.Item("IdFabricante"))
                    prod.IdMarca = CInt(lector.Item("IdMarca"))
                    prod.IdMaterial = CInt(lector.Item("IdMaterial"))
                    prod.IdMoraEnvase = CInt(lector.Item("IdMoraEnvase"))
                    prod.IdPerUbicacion = CInt(lector.Item("IdPerUbicacion"))
                    prod.IdPropietario = CInt(lector.Item("IdPropietario"))
                    prod.IdProveedor = CInt(lector.Item("IdProveedor"))
                    'prod.IdSabor = CInt(lector.Item("IdSabor"))
                    prod.IdSubLInea = CInt(lector.Item("IdSubLInea"))
                    'prod.IdTalla = CInt(lector.Item("IdTalla"))
                    'prod.IdTamanio = CInt(lector.Item("IdTamanio"))
                    prod.IdUsuario = CInt(lector.Item("IdUsuario"))
                    prod.StockActual = CDec(lector.Item("prod_StockActual"))
                    prod.StockMaximo = CInt(lector.Item("prod_StockMax"))
                    prod.StockMinimo = CInt(lector.Item("prod_StockMin"))
                    Lista.Add(prod)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectInactivoxLinea(ByVal idLinea As Integer) As List(Of Entidades.Producto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoSelectInactivoxLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idLinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Producto)
                Do While lector.Read
                    Dim prod As New Entidades.Producto
                    prod.CodigoBarras = CStr(lector.Item("prod_CodigoBarras"))
                    prod.Descripcion = CStr(lector.Item("prod_Nombre"))
                    prod.FechaAlta = CDate(lector.Item("prod_FechaAlta"))
                    prod.Id = CInt(lector.Item("IdProducto"))
                    prod.IdColor = CInt(lector.Item("IdColor"))
                    prod.IdContenido = CInt(lector.Item("IdContenido"))
                    prod.IdEstadoProd = CInt(lector.Item("IdEstadoProd"))
                    prod.IdFabricante = CInt(lector.Item("IdFabricante"))
                    prod.IdMarca = CInt(lector.Item("IdMarca"))
                    prod.IdMaterial = CInt(lector.Item("IdMaterial"))
                    prod.IdMoraEnvase = CInt(lector.Item("IdMoraEnvase"))
                    prod.IdPerUbicacion = CInt(lector.Item("IdPerUbicacion"))
                    prod.IdPropietario = CInt(lector.Item("IdPropietario"))
                    prod.IdProveedor = CInt(lector.Item("IdProveedor"))
                    'prod.IdSabor = CInt(lector.Item("IdSabor"))
                    prod.IdSubLInea = CInt(lector.Item("IdSubLInea"))
                    'prod.IdTalla = CInt(lector.Item("IdTalla"))
                    'prod.IdTamanio = CInt(lector.Item("IdTamanio"))
                    prod.IdUsuario = CInt(lector.Item("IdUsuario"))
                    prod.StockActual = CDec(lector.Item("prod_StockActual"))
                    prod.StockMaximo = CInt(lector.Item("prod_StockMax"))
                    prod.StockMinimo = CInt(lector.Item("prod_StockMin"))
                    Lista.Add(prod)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllxSubLinea(ByVal idSubLinea As Integer) As List(Of Entidades.Producto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoSelectAllxSubLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSubLinea", idSubLinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Producto)
                Do While lector.Read
                    Dim prod As New Entidades.Producto
                    prod.CodigoBarras = CStr(lector.Item("prod_CodigoBarras"))
                    prod.Descripcion = CStr(lector.Item("prod_Nombre"))
                    prod.FechaAlta = CDate(lector.Item("prod_FechaAlta"))
                    prod.Id = CInt(lector.Item("IdProducto"))
                    prod.IdColor = CInt(lector.Item("IdColor"))
                    prod.IdContenido = CInt(lector.Item("IdContenido"))
                    prod.IdEstadoProd = CInt(lector.Item("IdEstadoProd"))
                    prod.IdFabricante = CInt(lector.Item("IdFabricante"))
                    prod.IdMarca = CInt(lector.Item("IdMarca"))
                    prod.IdMaterial = CInt(lector.Item("IdMaterial"))
                    prod.IdMoraEnvase = CInt(lector.Item("IdMoraEnvase"))
                    prod.IdPerUbicacion = CInt(lector.Item("IdPerUbicacion"))
                    prod.IdPropietario = CInt(lector.Item("IdPropietario"))
                    prod.IdProveedor = CInt(lector.Item("IdProveedor"))
                    'prod.IdSabor = CInt(lector.Item("IdSabor"))
                    prod.IdSubLInea = CInt(lector.Item("IdSubLInea"))
                    'prod.IdTalla = CInt(lector.Item("IdTalla"))
                    'prod.IdTamanio = CInt(lector.Item("IdTamanio"))
                    prod.IdUsuario = CInt(lector.Item("IdUsuario"))
                    prod.StockActual = CDec(lector.Item("prod_StockActual"))
                    prod.StockMaximo = CInt(lector.Item("prod_StockMax"))
                    prod.StockMinimo = CInt(lector.Item("prod_StockMin"))
                    Lista.Add(prod)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectActivoxSubLinea(ByVal idSublinea As Integer) As List(Of Entidades.Producto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoSelectActivoxSubLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSubLinea", idSublinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Producto)
                Do While lector.Read
                    Dim prod As New Entidades.Producto
                    prod.CodigoBarras = CStr(lector.Item("prod_CodigoBarras"))
                    prod.Descripcion = CStr(lector.Item("prod_Nombre"))
                    prod.FechaAlta = CDate(lector.Item("prod_FechaAlta"))
                    prod.Id = CInt(lector.Item("IdProducto"))
                    prod.IdColor = CInt(lector.Item("IdColor"))
                    prod.IdContenido = CInt(lector.Item("IdContenido"))
                    prod.IdEstadoProd = CInt(lector.Item("IdEstadoProd"))
                    prod.IdFabricante = CInt(lector.Item("IdFabricante"))
                    prod.IdMarca = CInt(lector.Item("IdMarca"))
                    prod.IdMaterial = CInt(lector.Item("IdMaterial"))
                    prod.IdMoraEnvase = CInt(lector.Item("IdMoraEnvase"))
                    prod.IdPerUbicacion = CInt(lector.Item("IdPerUbicacion"))
                    prod.IdPropietario = CInt(lector.Item("IdPropietario"))
                    prod.IdProveedor = CInt(lector.Item("IdProveedor"))
                    'prod.IdSabor = CInt(lector.Item("IdSabor"))
                    prod.IdSubLInea = CInt(lector.Item("IdSubLInea"))
                    'prod.IdTalla = CInt(lector.Item("IdTalla"))
                    'prod.IdTamanio = CInt(lector.Item("IdTamanio"))
                    prod.IdUsuario = CInt(lector.Item("IdUsuario"))
                    prod.StockActual = CDec(lector.Item("prod_StockActual"))
                    prod.StockMaximo = CInt(lector.Item("prod_StockMax"))
                    prod.StockMinimo = CInt(lector.Item("prod_StockMin"))
                    Lista.Add(prod)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectInactivoxSubLinea(ByVal idSubLinea As Integer) As List(Of Entidades.Producto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoSelectInactivoxSubLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSubLinea", idSubLinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Producto)
                Do While lector.Read
                    Dim prod As New Entidades.Producto
                    prod.CodigoBarras = CStr(lector.Item("prod_CodigoBarras"))
                    prod.Descripcion = CStr(lector.Item("prod_Nombre"))
                    prod.FechaAlta = CDate(lector.Item("prod_FechaAlta"))
                    prod.Id = CInt(lector.Item("IdProducto"))
                    prod.IdColor = CInt(lector.Item("IdColor"))
                    prod.IdContenido = CInt(lector.Item("IdContenido"))
                    prod.IdEstadoProd = CInt(lector.Item("IdEstadoProd"))
                    prod.IdFabricante = CInt(lector.Item("IdFabricante"))
                    prod.IdMarca = CInt(lector.Item("IdMarca"))
                    prod.IdMaterial = CInt(lector.Item("IdMaterial"))
                    prod.IdMoraEnvase = CInt(lector.Item("IdMoraEnvase"))
                    prod.IdPerUbicacion = CInt(lector.Item("IdPerUbicacion"))
                    prod.IdPropietario = CInt(lector.Item("IdPropietario"))
                    prod.IdProveedor = CInt(lector.Item("IdProveedor"))
                    'prod.IdSabor = CInt(lector.Item("IdSabor"))
                    prod.IdSubLInea = CInt(lector.Item("IdSubLInea"))
                    'prod.IdTalla = CInt(lector.Item("IdTalla"))
                    'prod.IdTamanio = CInt(lector.Item("IdTamanio"))
                    prod.IdUsuario = CInt(lector.Item("IdUsuario"))
                    prod.StockActual = CDec(lector.Item("prod_StockActual"))
                    prod.StockMaximo = CInt(lector.Item("prod_StockMax"))
                    prod.StockMinimo = CInt(lector.Item("prod_StockMin"))
                    Lista.Add(prod)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Producto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Producto)
                Do While lector.Read
                    Dim prod As New Entidades.Producto
                    prod.CodigoBarras = CStr(lector.Item("prod_CodigoBarras"))
                    prod.Descripcion = CStr(lector.Item("prod_Nombre"))
                    prod.FechaAlta = CDate(lector.Item("prod_FechaAlta"))
                    prod.Id = CInt(lector.Item("IdProducto"))
                    prod.IdColor = CInt(lector.Item("IdColor"))
                    prod.IdContenido = CInt(lector.Item("IdContenido"))
                    prod.IdEstadoProd = CInt(lector.Item("IdEstadoProd"))
                    prod.IdFabricante = CInt(lector.Item("IdFabricante"))
                    prod.IdMarca = CInt(lector.Item("IdMarca"))
                    prod.IdMaterial = CInt(lector.Item("IdMaterial"))
                    prod.IdMoraEnvase = CInt(lector.Item("IdMoraEnvase"))
                    prod.IdPerUbicacion = CInt(lector.Item("IdPerUbicacion"))
                    prod.IdPropietario = CInt(lector.Item("IdPropietario"))
                    prod.IdProveedor = CInt(lector.Item("IdProveedor"))
                    'prod.IdSabor = CInt(lector.Item("IdSabor"))
                    prod.IdSubLInea = CInt(lector.Item("IdSubLInea"))
                    'prod.IdTalla = CInt(lector.Item("IdTalla"))
                    'prod.IdTamanio = CInt(lector.Item("IdTamanio"))
                    prod.IdUsuario = CInt(lector.Item("IdUsuario"))
                    prod.StockActual = CDec(lector.Item("prod_StockActual"))
                    prod.StockMaximo = CInt(lector.Item("prod_StockMax"))
                    prod.StockMinimo = CInt(lector.Item("prod_StockMin"))
                    Lista.Add(prod)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Producto)
        nombre = nombre.Replace("*", "%")
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Producto)
                Do While lector.Read
                    Dim prod As New Entidades.Producto
                    prod.CodigoBarras = CStr(lector.Item("prod_CodigoBarras"))
                    prod.Descripcion = CStr(lector.Item("prod_Nombre"))
                    prod.FechaAlta = CDate(lector.Item("prod_FechaAlta"))
                    prod.Id = CInt(lector.Item("IdProducto"))
                    prod.IdColor = CInt(lector.Item("IdColor"))
                    prod.IdContenido = CInt(lector.Item("IdContenido"))
                    prod.IdEstadoProd = CInt(lector.Item("IdEstadoProd"))
                    prod.IdFabricante = CInt(lector.Item("IdFabricante"))
                    prod.IdMarca = CInt(lector.Item("IdMarca"))
                    prod.IdMaterial = CInt(lector.Item("IdMaterial"))
                    prod.IdMoraEnvase = CInt(lector.Item("IdMoraEnvase"))
                    prod.IdPerUbicacion = CInt(lector.Item("IdPerUbicacion"))
                    prod.IdPropietario = CInt(lector.Item("IdPropietario"))
                    prod.IdProveedor = CInt(lector.Item("IdProveedor"))
                    'prod.IdSabor = CInt(lector.Item("IdSabor"))
                    prod.IdSubLInea = CInt(lector.Item("IdSubLInea"))
                    'prod.IdTalla = CInt(lector.Item("IdTalla"))
                    'prod.IdTamanio = CInt(lector.Item("IdTamanio"))
                    prod.IdUsuario = CInt(lector.Item("IdUsuario"))
                    prod.StockActual = CDec(lector.Item("prod_StockActual"))
                    prod.StockMaximo = CInt(lector.Item("prod_StockMax"))
                    prod.StockMinimo = CInt(lector.Item("prod_StockMin"))
                    Lista.Add(prod)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Producto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Producto)
                Do While lector.Read
                    Dim prod As New Entidades.Producto
                    prod.CodigoBarras = CStr(lector.Item("prod_CodigoBarras"))
                    prod.Descripcion = CStr(lector.Item("prod_Nombre"))
                    prod.FechaAlta = CDate(lector.Item("prod_FechaAlta"))
                    prod.Id = CInt(lector.Item("IdProducto"))
                    prod.IdColor = CInt(lector.Item("IdColor"))
                    prod.IdContenido = CInt(lector.Item("IdContenido"))
                    prod.IdEstadoProd = CInt(lector.Item("IdEstadoProd"))
                    prod.IdFabricante = CInt(lector.Item("IdFabricante"))
                    prod.IdMarca = CInt(lector.Item("IdMarca"))
                    prod.IdMaterial = CInt(lector.Item("IdMaterial"))
                    prod.IdMoraEnvase = CInt(lector.Item("IdMoraEnvase"))
                    prod.IdPerUbicacion = CInt(lector.Item("IdPerUbicacion"))
                    prod.IdPropietario = CInt(lector.Item("IdPropietario"))
                    prod.IdProveedor = CInt(lector.Item("IdProveedor"))
                    'prod.IdSabor = CInt(lector.Item("IdSabor"))
                    prod.IdSubLInea = CInt(lector.Item("IdSubLInea"))
                    'prod.IdTalla = CInt(lector.Item("IdTalla"))
                    'prod.IdTamanio = CInt(lector.Item("IdTamanio"))
                    prod.IdUsuario = CInt(lector.Item("IdUsuario"))
                    prod.StockActual = CDec(lector.Item("prod_StockActual"))
                    prod.StockMaximo = CInt(lector.Item("prod_StockMax"))
                    prod.StockMinimo = CInt(lector.Item("prod_StockMin"))
                    Lista.Add(prod)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllxIdProveedor(ByVal idProveedor As Integer) As List(Of Entidades.Producto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoSelectAllxIdProveedor", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProveedor", idProveedor)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Producto)
                Do While lector.Read
                    Dim prod As New Entidades.Producto
                    prod.CodigoBarras = CStr(lector.Item("prod_CodigoBarras"))
                    prod.Descripcion = CStr(lector.Item("prod_Nombre"))
                    prod.FechaAlta = CDate(lector.Item("prod_FechaAlta"))
                    prod.Id = CInt(lector.Item("IdProducto"))
                    prod.IdColor = CInt(lector.Item("IdColor"))
                    prod.IdContenido = CInt(lector.Item("IdContenido"))
                    prod.IdEstadoProd = CInt(lector.Item("IdEstadoProd"))
                    prod.IdFabricante = CInt(lector.Item("IdFabricante"))
                    prod.IdMarca = CInt(lector.Item("IdMarca"))
                    prod.IdMaterial = CInt(lector.Item("IdMaterial"))
                    prod.IdMoraEnvase = CInt(lector.Item("IdMoraEnvase"))
                    prod.IdPerUbicacion = CInt(lector.Item("IdPerUbicacion"))
                    prod.IdPropietario = CInt(lector.Item("IdPropietario"))
                    prod.IdProveedor = CInt(lector.Item("IdProveedor"))
                    'prod.IdSabor = CInt(lector.Item("IdSabor"))
                    prod.IdSubLInea = CInt(lector.Item("IdSubLInea"))
                    'prod.IdTalla = CInt(lector.Item("IdTalla"))
                    'prod.IdTamanio = CInt(lector.Item("IdTamanio"))
                    prod.IdUsuario = CInt(lector.Item("IdUsuario"))
                    prod.StockActual = CDec(lector.Item("prod_StockActual"))
                    prod.StockMaximo = CInt(lector.Item("prod_StockMax"))
                    prod.StockMinimo = CInt(lector.Item("prod_StockMin"))
                    Lista.Add(prod)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectActivoxIdProveedor(ByVal idProveedor As Integer) As List(Of Entidades.Producto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoSelectActivoxIdProveedor", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProveedor", idProveedor)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Producto)
                Do While lector.Read
                    Dim prod As New Entidades.Producto
                    prod.CodigoBarras = CStr(lector.Item("prod_CodigoBarras"))
                    prod.Descripcion = CStr(lector.Item("prod_Nombre"))
                    prod.FechaAlta = CDate(lector.Item("prod_FechaAlta"))
                    prod.Id = CInt(lector.Item("IdProducto"))
                    prod.IdColor = CInt(lector.Item("IdColor"))
                    prod.IdContenido = CInt(lector.Item("IdContenido"))
                    prod.IdEstadoProd = CInt(lector.Item("IdEstadoProd"))
                    prod.IdFabricante = CInt(lector.Item("IdFabricante"))
                    prod.IdMarca = CInt(lector.Item("IdMarca"))
                    prod.IdMaterial = CInt(lector.Item("IdMaterial"))
                    prod.IdMoraEnvase = CInt(lector.Item("IdMoraEnvase"))
                    prod.IdPerUbicacion = CInt(lector.Item("IdPerUbicacion"))
                    prod.IdPropietario = CInt(lector.Item("IdPropietario"))
                    prod.IdProveedor = CInt(lector.Item("IdProveedor"))
                    'prod.IdSabor = CInt(lector.Item("IdSabor"))
                    prod.IdSubLInea = CInt(lector.Item("IdSubLInea"))
                    'prod.IdTalla = CInt(lector.Item("IdTalla"))
                    'prod.IdTamanio = CInt(lector.Item("IdTamanio"))
                    prod.IdUsuario = CInt(lector.Item("IdUsuario"))
                    prod.StockActual = CDec(lector.Item("prod_StockActual"))
                    prod.StockMaximo = CInt(lector.Item("prod_StockMax"))
                    prod.StockMinimo = CInt(lector.Item("prod_StockMin"))
                    Lista.Add(prod)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectInactivoxIdProveedor(ByVal idProveedor As Integer) As List(Of Entidades.Producto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoSelectInactivoxIdProveedor", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProveedor", idProveedor)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Producto)
                Do While lector.Read
                    Dim prod As New Entidades.Producto
                    prod.CodigoBarras = CStr(lector.Item("prod_CodigoBarras"))
                    prod.Descripcion = CStr(lector.Item("prod_Nombre"))
                    prod.FechaAlta = CDate(lector.Item("prod_FechaAlta"))
                    prod.Id = CInt(lector.Item("IdProducto"))
                    prod.IdColor = CInt(lector.Item("IdColor"))
                    prod.IdContenido = CInt(lector.Item("IdContenido"))
                    prod.IdEstadoProd = CInt(lector.Item("IdEstadoProd"))
                    prod.IdFabricante = CInt(lector.Item("IdFabricante"))
                    prod.IdMarca = CInt(lector.Item("IdMarca"))
                    prod.IdMaterial = CInt(lector.Item("IdMaterial"))
                    prod.IdMoraEnvase = CInt(lector.Item("IdMoraEnvase"))
                    prod.IdPerUbicacion = CInt(lector.Item("IdPerUbicacion"))
                    prod.IdPropietario = CInt(lector.Item("IdPropietario"))
                    prod.IdProveedor = CInt(lector.Item("IdProveedor"))
                    'prod.IdSabor = CInt(lector.Item("IdSabor"))
                    prod.IdSubLInea = CInt(lector.Item("IdSubLInea"))
                    'prod.IdTalla = CInt(lector.Item("IdTalla"))
                    'prod.IdTamanio = CInt(lector.Item("IdTamanio"))
                    prod.IdUsuario = CInt(lector.Item("IdUsuario"))
                    prod.StockActual = CDec(lector.Item("prod_StockActual"))
                    prod.StockMaximo = CInt(lector.Item("prod_StockMax"))
                    prod.StockMinimo = CInt(lector.Item("prod_StockMin"))
                    Lista.Add(prod)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectGrillaProd_M(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, _
                                       ByVal IdProveedor As Integer, ByVal IdEstadoProd As Integer, _
                                       ByVal Nombre As String) As List(Of Entidades.GrillaProducto_M)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SelectProductoGrilla_M", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@IdProveedor", IdProveedor)
        cmd.Parameters.AddWithValue("@IdEstadoProd", IdEstadoProd)
        cmd.Parameters.AddWithValue("@Nombre", Nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.GrillaProducto_M)
                Do While lector.Read
                    Dim objGrillaProd As New Entidades.GrillaProducto_M
                    objGrillaProd.IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                    objGrillaProd.NomProducto = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                    objGrillaProd.NomLinea = CStr(IIf(IsDBNull(lector.Item("lin_Nombre")) = True, "", lector.Item("lin_Nombre")))
                    objGrillaProd.NomSubLinea = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")) = True, "", lector.Item("sl_Nombre")))
                    objGrillaProd.NomProveedor = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                    objGrillaProd.NomEstado = CStr(IIf(IsDBNull(lector.Item("ep_Nombre")) = True, "", lector.Item("ep_Nombre")))

                    Lista.Add(objGrillaProd)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    '' NUEVO BUSQUEDA CON PARAMETROS ADICIONALES
    Public Function SelectGrillaProd_M(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdEstadoProd As Integer, ByVal Nombre As String, ByVal Codigo As String, ByVal Tb As List(Of Entidades.ProductoTipoTablaValor)) As List(Of Entidades.GrillaProducto_M)
        Dim dt As New DataTable
        dt.Columns.Add("IdTipoTabla", GetType(System.Int64))
        dt.Columns.Add("IdTipoTablaValor", GetType(System.Int64))

        If Tb IsNot Nothing Then
            For Each objprodTTV As Entidades.ProductoTipoTablaValor In Tb
                Dim dr As DataRow = dt.NewRow()
                dr(0) = objprodTTV.IdTipoTabla
                dr(1) = objprodTTV.IdTipoTablaValor
                dt.Rows.Add(dr)
            Next
        End If

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SelectProductoGrilla_M", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@IdEstadoProd", IdEstadoProd)
        cmd.Parameters.AddWithValue("@Nombre", Nombre)
        cmd.Parameters.AddWithValue("@Codigo", Codigo)
        cmd.Parameters.AddWithValue("@Tb", dt)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.GrillaProducto_M)
                Do While lector.Read
                    Dim objGrillaProd As New Entidades.GrillaProducto_M
                    objGrillaProd.IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                    objGrillaProd.NoVisible = CStr(IIf(IsDBNull(lector.Item("NoVisible")) = True, "", lector.Item("NoVisible")))
                    objGrillaProd.prod_Codigo = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")) = True, "", lector.Item("prod_Codigo")))
                    objGrillaProd.prod_Atributos = CStr(IIf(IsDBNull(lector.Item("prod_Atributos")) = True, "", lector.Item("prod_Atributos")))
                    objGrillaProd.NomProducto = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                    objGrillaProd.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    objGrillaProd.NomLinea = CStr(IIf(IsDBNull(lector.Item("lin_Nombre")) = True, "", lector.Item("lin_Nombre")))
                    objGrillaProd.IdSubLinea = CInt(IIf(IsDBNull(lector.Item("IdSubLinea")) = True, 0, lector.Item("IdSubLinea")))
                    objGrillaProd.NomSubLinea = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")) = True, "", lector.Item("sl_Nombre")))
                    objGrillaProd.NomEstado = CStr(IIf(IsDBNull(lector.Item("ep_Nombre")) = True, "", lector.Item("ep_Nombre")))
                    objGrillaProd.PrecioCompra = CDec(IIf(IsDBNull(lector.Item("prod_PrecioCompra")) = True, 0, lector.Item("prod_PrecioCompra")))
                    objGrillaProd.IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                    objGrillaProd.NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_simbolo")) = True, "", lector.Item("mon_simbolo")))
                    Lista.Add(objGrillaProd)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectGrillaProd_E(ByVal Serie As String, ByVal Nombre As String, ByVal IdEstado As Integer) As List(Of Entidades.GrillaProducto_E)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SelectProductoGrilla_E", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Serie", Serie)
        cmd.Parameters.AddWithValue("@Nombre", Nombre)
        cmd.Parameters.AddWithValue("@IdEstado", IdEstado)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.GrillaProducto_E)
                Do While lector.Read
                    Dim objGrillaProd As New Entidades.GrillaProducto_E
                    objGrillaProd.IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                    objGrillaProd.NomComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                    objGrillaProd.NomEstado = CStr(IIf(IsDBNull(lector.Item("ep_Nombre")) = True, "", lector.Item("ep_Nombre")))
                    objGrillaProd.NomPerNatural = CStr(IIf(IsDBNull(lector.Item("nom_Natural")) = True, "", lector.Item("nom_Natural")))
                    objGrillaProd.NomProducto = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                    objGrillaProd.Serie = CStr(IIf(IsDBNull(lector.Item("sp_Numero")) = True, "", lector.Item("sp_Numero")))
                    Lista.Add(objGrillaProd)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectxSubLinea_Cbo(ByVal idSubLinea As Integer) As List(Of Entidades.Producto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoSelectxSubLinea_Cbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idSubLinea", idSubLinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Producto)
                Do While lector.Read
                    Dim prod As New Entidades.Producto
                    prod.Descripcion = CStr(lector.Item("prod_Nombre"))
                    prod.Id = CInt(lector.Item("IdProducto"))
                    Lista.Add(prod)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectEquivalenciaUMPrincipal(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal IdProducto As Integer, ByVal IdUnidadMedida As Integer, ByVal escalar As Decimal) As Decimal
        Dim cmd As New SqlCommand("_ProductoSelectEquivalenciaUMPrincipal", cn, tr)
        Dim retorno As Decimal = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        cmd.Parameters.AddWithValue("@IdUnidadMedida", IdUnidadMedida)
        cmd.Parameters.AddWithValue("@Escalar", escalar)
        Dim sqlParam As New SqlParameter("@Retorno", SqlDbType.Decimal)
        sqlParam.Precision = 18
        sqlParam.Scale = 6
        sqlParam.Value = 0
        sqlParam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(sqlParam)
        Try
            cmd.ExecuteNonQuery()
            retorno = CDec(IIf(IsDBNull(cmd.Parameters("@Retorno").Value) = True, 0, cmd.Parameters("@Retorno").Value))
            Return retorno
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectUMPrincipal(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal IdProducto As Integer) As String
        Dim cmd As New SqlCommand("_ProductoSelectUMPrincipal", cn, tr)
        Dim retorno As String = ""
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        cmd.Parameters.Add(New SqlParameter("@Retorno", SqlDbType.VarChar, 5))
        cmd.Parameters("@Retorno").Direction = ParameterDirection.Output
        Try
            cmd.ExecuteNonQuery()
            retorno = CStr(cmd.Parameters("@Retorno").Value)
            Return retorno
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function ProductoMercaderiaSelectxIdProducto(ByVal IdProducto As Integer) As Entidades.Producto
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoMercaderiaSelectxIdProducto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim objProd As Entidades.Producto = Nothing
                If lector.Read Then
                    objProd = New Entidades.Producto
                    objProd.Id = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                    objProd.CodigoBarras = CStr(IIf(IsDBNull(lector.Item("prod_CodigoBarras")) = True, "", lector.Item("prod_CodigoBarras")))
                    objProd.Descripcion = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                    objProd.StockMinimo = CInt(IIf(IsDBNull(lector.Item("prod_StockMin")) = True, 0, lector.Item("prod_StockMin")))
                    objProd.StockMaximo = CInt(IIf(IsDBNull(lector.Item("prod_StockMax")) = True, 0, lector.Item("prod_StockMax")))
                    objProd.StockActual = CInt(IIf(IsDBNull(lector.Item("prod_StockActual")) = True, 0, lector.Item("prod_StockActual")))
                    objProd.StockReposicion = CInt(IIf(IsDBNull(lector.Item("prod_StockReposicion")) = True, 0, lector.Item("prod_StockReposicion")))
                    objProd.IdSubLInea = CInt(IIf(IsDBNull(lector.Item("IdSubLInea")) = True, 0, lector.Item("IdSubLInea")))
                    objProd.IdProveedor = CInt(IIf(IsDBNull(lector.Item("IdProveedor")) = True, 0, lector.Item("IdProveedor")))
                    objProd.IdEstadoProd = CInt(IIf(IsDBNull(lector.Item("IdEstadoProd")) = True, 0, lector.Item("IdEstadoProd")))
                    objProd.IdTipoProducto = CInt(IIf(IsDBNull(lector.Item("IdTipoProducto")) = True, 0, lector.Item("IdTipoProducto")))
                    objProd.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    objProd.IdTipoExistencia = CInt(IIf(IsDBNull(lector.Item("IdTipoExistencia")) = True, 0, lector.Item("IdTipoExistencia")))
                    objProd.NoVisible = CStr(IIf(IsDBNull(lector.Item("NoVisible")) = True, "", lector.Item("NoVisible")))
                    objProd.Codigo = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")) = True, "", lector.Item("prod_Codigo")))
                    objProd.CodAtributos = CStr(IIf(IsDBNull(lector.Item("prod_Atributos")) = True, "", lector.Item("prod_Atributos")))
                    objProd.CodProveedor = CStr(IIf(IsDBNull(lector.Item("Cod_Proveedor")) = True, "", lector.Item("Cod_Proveedor")))
                    objProd.Kit = CBool(IIf(IsDBNull(lector.Item("Kit")) = True, Nothing, lector.Item("Kit")))
                    objProd.CalculoFlete = CStr(IIf(IsDBNull(lector.Item("CalculoFlete")) = True, Nothing, lector.Item("CalculoFlete")))
                    objProd.CodBarrasFabricante = CStr(IIf(IsDBNull(lector.Item("Prod_CodBarraFabricante")) = True, "", lector.Item("Prod_CodBarraFabricante")))
                    objProd.CodAntiguo = CStr(IIf(IsDBNull(lector.Item("CodAntiguo")) = True, "", lector.Item("CodAntiguo")))
                    objProd.flagTG = CInt(IIf(IsDBNull(lector.Item("flagTG")) = True, 0, lector.Item("flagTG")))

                    objProd.LeadTime = CDec(IIf(IsDBNull(lector.Item("leadTime")) = True, 0, lector.Item("leadTime")))
                    objProd.VolMinCompra = CDec(IIf(IsDBNull(lector.Item("volmincompra")) = True, 0, lector.Item("volmincompra")))
                    objProd.ReposicionWeeks = CDec(IIf(IsDBNull(lector.Item("ReposicionWeeks")) = True, 0, lector.Item("ReposicionWeeks")))

                End If
                lector.Close()
                Return objProd
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function ProductoCostoCompraSelectxIdSubLinea(ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, _
                                                         ByVal IdSubLinea As Integer, ByVal Prod_Codigo As String, _
                                                         ByVal Prod_Nombre As String, ByVal Tabla As DataTable) As List(Of Entidades.ProductoUMCostoCompraView)
        Prod_Nombre = Prod_Nombre.Replace("*", "%")
        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Dim cmd As New SqlCommand("_ProductoCostoCompraSelectxIdSubLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@IdTipoExistencia", IIf(IdTipoExistencia = Nothing, DBNull.Value, IdTipoExistencia))
        cmd.Parameters.AddWithValue("@IdLinea", IIf(IdLinea = Nothing, DBNull.Value, IdLinea))
        cmd.Parameters.AddWithValue("@IdSubLinea", IIf(IdSubLinea = Nothing, DBNull.Value, IdSubLinea))
        cmd.Parameters.AddWithValue("@prod_Codigo", IIf(Prod_Codigo = Nothing, DBNull.Value, Prod_Codigo))
        cmd.Parameters.AddWithValue("@prod_Nombre", IIf(Prod_Nombre = Nothing, DBNull.Value, Prod_Nombre))
        cmd.Parameters.AddWithValue("@Tabla", Tabla)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ProductoUMCostoCompraView)
                Do While lector.Read
                    Dim obj As New Entidades.ProductoUMCostoCompraView
                    obj.IdProducto = CInt(lector.Item("IdProducto"))
                    obj.NomProducto = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                    obj.NomUMPrincipal = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                    obj.PrecioCompra = CDec(IIf(IsDBNull(lector.Item("prod_PrecioCompra")) = True, 0, lector.Item("prod_PrecioCompra")))
                    obj.IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                    obj.CodigoProducto = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")) = True, "", lector.Item("prod_Codigo")))

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectUMPrincipalxIdSubLinea(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal nomProducto As String) As List(Of Entidades.GrillaProducto_M)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoSelectUMPrincipalxIdSubLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@NomProducto", nomProducto)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.GrillaProducto_M)
                Do While lector.Read
                    Dim obj As New Entidades.GrillaProducto_M
                    obj.FlagUMPrincipal = CBool(lector.Item("pum_UnidadPrincipal"))
                    obj.IdLinea = CInt(lector.Item("IdLinea"))
                    obj.IdProducto = CInt(lector.Item("IdProducto"))
                    obj.IdSubLinea = CInt(lector.Item("IdSubLInea"))
                    obj.IdUnidadMedida = CInt(lector.Item("IdUnidadMedida"))
                    obj.NomLinea = CStr(lector.Item("lin_Nombre"))
                    obj.NomProducto = CStr(lector.Item("prod_Nombre"))
                    obj.NomSubLinea = CStr(lector.Item("sl_Nombre"))
                    obj.NomUMedida = CStr(lector.Item("um_NombreCorto"))
                    obj.PrecioCompra = CDec(IIf(IsDBNull(lector.Item("prod_PrecioCompra")) = True, 0, lector.Item("prod_PrecioCompra")))
                    obj.IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                    obj.NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectOrdenxIdSubLinea(ByVal idsublinea As Integer) As List(Of Entidades.Producto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoSelectOrdenxIdSubLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Producto)
                Do While lector.Read
                    Dim prod As New Entidades.Producto
                    prod.Descripcion = CStr(lector.Item("prod_Nombre"))
                    prod.Id = CInt(lector.Item("IdProducto"))
                    prod.Orden = CInt(IIf(IsDBNull(lector.Item("prod_orden")) = True, 0, lector.Item("prod_orden")))
                    Lista.Add(prod)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Sub UpdateOrdenxIdProducto(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal idproducto As Integer, ByVal orden As Integer)
        'recibo la conexion abierta
        Dim cmd As New SqlCommand("_ProductoUpdateOrdenxIdProducto", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", idproducto)
        cmd.Parameters.AddWithValue("@Orden", orden)
        If cmd.ExecuteNonQuery = 0 Then
            Throw New Exception("Fall� el proceso de Actualizaci�n.")
        End If
    End Sub

    Public Function SelectEquivalenciaEntreUM(ByVal IdProducto As Integer, ByVal IdUM_Origen As Integer, ByVal IdUM_Destino As Integer, ByVal Escalar As Decimal) As Decimal
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoSelectEquivalenciaEntreUM", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            cn.Open()
            cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
            cmd.Parameters.AddWithValue("@IdUM_Origen", IdUM_Origen)
            cmd.Parameters.AddWithValue("@IdUM_Destino", IdUM_Destino)
            cmd.Parameters.AddWithValue("@Escalar", Escalar)
            Dim param As New SqlParameter("@Retorno", SqlDbType.Decimal)
            param.Precision = 18
            param.Scale = 6
            param.Direction = ParameterDirection.Output
            cmd.Parameters.Add(param)
            cmd.ExecuteScalar()

            Return CDec(IIf(IsDBNull(cmd.Parameters("@Retorno").Value) = True, 0, cmd.Parameters("@Retorno").Value))

        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function


    Public Function SelectBusquedaProdxParams_Standar_Paginado(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal nomProducto As String, ByVal codSubLinea As Integer, ByVal pagenumber As Integer, ByVal pagesize As Integer) As List(Of Entidades.GrillaProducto_M)
        nomProducto = nomProducto.Replace("*", "%")
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoSelectActivoxParams_Paginado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@NomProducto", nomProducto)
        cmd.Parameters.AddWithValue("@CodSubLinea", codSubLinea)
        cmd.Parameters.AddWithValue("@PageNumber", pagenumber)
        cmd.Parameters.AddWithValue("@PageSize", pagesize)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.GrillaProducto_M)
                Do While lector.Read
                    Dim obj As New Entidades.GrillaProducto_M
                    obj.FlagUMPrincipal = CBool(lector.Item("pum_UnidadPrincipal"))
                    obj.IdLinea = CInt(lector.Item("IdLinea"))
                    obj.IdProducto = CInt(lector.Item("IdProducto"))
                    obj.IdSubLinea = CInt(lector.Item("IdSubLInea"))
                    obj.IdUnidadMedida = CInt(lector.Item("IdUnidadMedida"))
                    obj.NomLinea = CStr(lector.Item("lin_Nombre"))
                    obj.NomProducto = CStr(lector.Item("prod_Nombre"))
                    obj.NomSubLinea = CStr(lector.Item("sl_Nombre"))
                    obj.NomUMedida = CStr(lector.Item("um_NombreCorto"))
                    obj.PrecioCompra = CDec(IIf(IsDBNull(lector.Item("prod_PrecioCompra")) = True, 0, lector.Item("prod_PrecioCompra")))
                    obj.IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                    obj.NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    '********************** BUSQUEDA DE PRODUCTOS STANDAR
    Public Function Producto_ListarProductosxParams(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As String, ByVal Descripcion As String, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal pagesize As Integer, ByVal pagenumber As Integer, ByVal IdPais As Integer, ByVal IdFabricante As Integer, ByVal IdMarca As Integer, ByVal IdModelo As Integer, ByVal IdFormato As Integer, ByVal IdProveedor As Integer, ByVal IdEstilo As Integer, ByVal IdTransito As Integer, ByVal IdCalidad As Integer) As List(Of Entidades.ProductoView)
        Descripcion = Descripcion.Replace("*", "%")
        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Dim cmd As New SqlCommand("_Producto_ListarProductosxParams", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@CodSubLinea", CodSubLinea)
        cmd.Parameters.AddWithValue("@Descripcion", Descripcion)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@PageNumber", pagenumber)
        cmd.Parameters.AddWithValue("@PageSize", pagesize)

        cmd.Parameters.AddWithValue("@IdPais", IdPais)
        cmd.Parameters.AddWithValue("@IdFabricante", IdFabricante)
        cmd.Parameters.AddWithValue("@IdMarca", IdMarca)
        cmd.Parameters.AddWithValue("@IdModelo", IdModelo)
        cmd.Parameters.AddWithValue("@IdFormato", IdFormato)
        cmd.Parameters.AddWithValue("@IdProveedor", IdProveedor)
        cmd.Parameters.AddWithValue("@IdEstilo", IdEstilo)
        cmd.Parameters.AddWithValue("@IdTransito", IdTransito)
        cmd.Parameters.AddWithValue("@IdCalidad", IdCalidad)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ProductoView)
                Do While lector.Read
                    Dim obj As New Entidades.ProductoView

                    With obj

                        obj.UMPrincipal = CBool(lector.Item("pum_UnidadPrincipal"))
                        obj.IdLinea = CInt(lector.Item("IdLinea"))
                        obj.Id = CInt(lector.Item("IdProducto"))
                        obj.IdSubLInea = CInt(lector.Item("IdSubLInea"))
                        obj.IdUnidadMedida = CInt(lector.Item("IdUnidadMedida"))
                        obj.Linea = CStr(lector.Item("lin_Nombre"))
                        obj.Descripcion = CStr(lector.Item("prod_Nombre"))
                        obj.SubLinea = CStr(lector.Item("sl_Nombre"))
                        obj.UnidadMedida = CStr(lector.Item("um_NombreCorto"))
                        obj.PrecioCompra = CDec(IIf(IsDBNull(lector.Item("prod_PrecioCompra")) = True, 0, lector.Item("prod_PrecioCompra")))
                        obj.IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        obj.Moneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                        obj.StockDisponible = CDec(IIf(IsDBNull(lector.Item("StockDisponible")) = True, 0, lector.Item("StockDisponible")))
                        obj.cadenaUM = CStr(IIf(IsDBNull(lector.Item("cadenaUM")) = True, "", lector.Item("cadenaUM")))
                        obj.StockDisponible_Cadena = CStr(IIf(IsDBNull(lector.Item("StockDisponible_Cadena")) = True, "", lector.Item("StockDisponible_Cadena")))
                        obj.Codigo = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")) = True, "", lector.Item("prod_Codigo")))
                    End With

                    Lista.Add(obj)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function


    Public Function Producto_ListarProductosxParams_V2(ByVal IdLinea As Integer, _
                                                       ByVal IdSubLinea As Integer, _
                                                       ByVal CodSubLinea As String, _
                                                       ByVal Descripcion As String, _
                                                       ByVal IdEmpresa As Integer, _
                                                       ByVal IdAlmacen As Integer, _
                                                       ByVal pagesize As Integer, _
                                                       ByVal pagenumber As Integer, _
                                                       ByVal codigoProducto As String, _
                                                       ByVal IdTipoExistencia As Integer, _
                                                       ByVal tableTablaTipoValor As DataTable) As List(Of Entidades.ProductoView)

        Descripcion = Descripcion.Replace("*", "%")
        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Dim cmd As New SqlCommand("_Producto_ListarProductosxParams_V3", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoExistencia", IdTipoExistencia)
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@CodSubLinea", CodSubLinea)
        cmd.Parameters.AddWithValue("@Descripcion", Descripcion)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@PageNumber", pagenumber)
        cmd.Parameters.AddWithValue("@PageSize", pagesize)

        cmd.Parameters.AddWithValue("@CodigoProducto", codigoProducto)
        cmd.Parameters.AddWithValue("@Tabla", tableTablaTipoValor)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ProductoView)
                Do While lector.Read
                    Dim obj As New Entidades.ProductoView

                    With obj

                        obj.UMPrincipal = CBool(lector.Item("pum_UnidadPrincipal"))
                        obj.IdLinea = CInt(lector.Item("IdLinea"))
                        obj.Id = CInt(lector.Item("IdProducto"))
                        obj.IdSubLInea = CInt(lector.Item("IdSubLInea"))
                        obj.IdUnidadMedida = CInt(lector.Item("IdUnidadMedida"))
                        obj.Linea = CStr(lector.Item("lin_Nombre"))
                        obj.Descripcion = CStr(lector.Item("prod_Nombre"))
                        obj.SubLinea = CStr(lector.Item("sl_Nombre"))
                        obj.UnidadMedida = CStr(lector.Item("um_NombreCorto"))
                        obj.PrecioCompra = CDec(IIf(IsDBNull(lector.Item("prod_PrecioCompra")) = True, 0, lector.Item("prod_PrecioCompra")))
                        obj.IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        obj.Moneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                        obj.StockDisponible = CDec(IIf(IsDBNull(lector.Item("StockDisponible")) = True, 0, lector.Item("StockDisponible")))
                        obj.cadenaUM = CStr(IIf(IsDBNull(lector.Item("cadenaUM")) = True, "", lector.Item("cadenaUM")))
                        obj.StockDisponible_Cadena = CStr(IIf(IsDBNull(lector.Item("StockDisponible_Cadena")) = True, "", lector.Item("StockDisponible_Cadena")))
                        obj.Codigo = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")) = True, "", lector.Item("prod_Codigo")))
                        obj.Kit = CBool(lector.Item("prod_Kit"))
                        obj.idSector = CInt(lector.Item("id_sector"))
                    End With

                    Lista.Add(obj)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function


    Public Function SelectProductoxLineaSubLinea(ByVal idlinea As Integer, ByVal idsublinea As Integer) As List(Of Entidades.Producto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("Sel_ProductoxLineaSubLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Producto)
                Do While lector.Read
                    Dim prod As New Entidades.Producto
                    prod.Descripcion = CStr(lector.Item("prod_Nombre"))
                    prod.Id = CInt(lector.Item("IdProducto"))
                    Lista.Add(prod)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DeleteVehiculoxDocumento(ByVal Vehiculo As Integer) As Integer

        Dim ArrayParametros() As SqlParameter = New SqlParameter(0) {}
        ArrayParametros(0) = New SqlParameter("@IdVehiculo", SqlDbType.Int)
        ArrayParametros(0).Value = Vehiculo

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim cmd As SqlCommand
        Dim codigo As Integer = -1
        Try

            Using cn
                cn.Open()
                tr = cn.BeginTransaction(IsolationLevel.Serializable)

                cmd = New SqlCommand("_VehiculoDeletexDocumento", cn, tr)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(ArrayParametros)

                codigo = CInt(cmd.ExecuteScalar())

                tr.Commit()

                Return codigo

            End Using
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function InsertaVehiculoxEstadoProxSubLinea(ByVal Vehiculo As Entidades.Vehiculo) As Integer

        Dim ArrayParametros() As SqlParameter = New SqlParameter(6) {}
        ArrayParametros(0) = New SqlParameter("@Veh_SubLinea", SqlDbType.Int)
        ArrayParametros(0).Value = Vehiculo.IdSubLinea
        ArrayParametros(1) = New SqlParameter("@Veh_EstadoPro", SqlDbType.Int)
        ArrayParametros(1).Value = Vehiculo.IdEstadoProd
        ArrayParametros(2) = New SqlParameter("@Veh_Placa", SqlDbType.VarChar)
        ArrayParametros(2).Value = Vehiculo.Placa
        ArrayParametros(3) = New SqlParameter("@veh_Modelo", SqlDbType.VarChar)
        ArrayParametros(3).Value = Vehiculo.Modelo
        ArrayParametros(4) = New SqlParameter("@veh_NConstanciaIns", SqlDbType.VarChar)
        ArrayParametros(4).Value = Vehiculo.ConstanciaIns
        ArrayParametros(5) = New SqlParameter("@veh_ConfVeh", SqlDbType.VarChar)
        ArrayParametros(5).Value = Vehiculo.ConfVeh
        ArrayParametros(6) = New SqlParameter("@veh_Tara", SqlDbType.VarChar)
        ArrayParametros(6).Value = Vehiculo.Tara
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim cmd As SqlCommand
        Dim codigo As Integer = -1
        Try

            Using cn
                cn.Open()
                tr = cn.BeginTransaction(IsolationLevel.Serializable)

                cmd = New SqlCommand("_VehivuloInsetxEstadoProdxSubLinea", cn, tr)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(ArrayParametros)

                codigo = CInt(cmd.ExecuteScalar())

                tr.Commit()

                Return codigo

            End Using
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function
    Public Function ActualizaVehiculoxEstadoProxSubLinea(ByVal Vehiculo As Entidades.Vehiculo) As Integer

        Dim ArrayParametros() As SqlParameter = New SqlParameter(7) {}
        ArrayParametros(0) = New SqlParameter("@Veh_SubLinea", SqlDbType.Int)
        ArrayParametros(0).Value = Vehiculo.IdSubLinea
        ArrayParametros(1) = New SqlParameter("@Veh_EstadoPro", SqlDbType.Int)
        ArrayParametros(1).Value = Vehiculo.IdEstadoProd
        ArrayParametros(2) = New SqlParameter("@Veh_Placa", SqlDbType.VarChar)
        ArrayParametros(2).Value = Vehiculo.Placa
        ArrayParametros(3) = New SqlParameter("@veh_Modelo", SqlDbType.VarChar)
        ArrayParametros(3).Value = Vehiculo.Modelo
        ArrayParametros(4) = New SqlParameter("@veh_NConstanciaIns", SqlDbType.VarChar)
        ArrayParametros(4).Value = Vehiculo.ConstanciaIns
        ArrayParametros(5) = New SqlParameter("@veh_ConfVeh", SqlDbType.VarChar)
        ArrayParametros(5).Value = Vehiculo.ConfVeh
        ArrayParametros(6) = New SqlParameter("@veh_Tara", SqlDbType.VarChar)
        ArrayParametros(6).Value = Vehiculo.Tara
        ArrayParametros(7) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(7).Value = Vehiculo.IdProducto
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim cmd As SqlCommand
        Dim codigo As Integer = -1
        Try

            Using cn
                cn.Open()
                tr = cn.BeginTransaction(IsolationLevel.Serializable)

                cmd = New SqlCommand("__VehivuloUpdatexEstadoProdxSubLinea", cn, tr)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(ArrayParametros)

                codigo = CInt(cmd.ExecuteScalar())

                tr.Commit()

                Return codigo

            End Using
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function SelectGrillaVehiculo_Buscar(ByVal Placa As String, ByVal Modelo As String, ByVal Constancia As String, ByVal EstadoPro As String) As List(Of Entidades.Vehiculo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SelectVehiculoGrilla_Buscar", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Placa", Placa)
        cmd.Parameters.AddWithValue("@modelo", Modelo)
        cmd.Parameters.AddWithValue("@Constancia", Constancia)
        cmd.Parameters.AddWithValue("@EstadoProd", EstadoPro)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Vehiculo)
                Do While lector.Read
                    Dim VehiculoSelect As New Entidades.Vehiculo
                    VehiculoSelect.IdTipoExistencia = CInt(IIf(IsDBNull(lector.Item("IdTipoExistencia")) = True, 0, lector.Item("IdTipoExistencia")))
                    VehiculoSelect.Placa = CStr(IIf(IsDBNull(lector.Item("veh_Placa")) = True, "", lector.Item("veh_Placa")))
                    VehiculoSelect.Modelo = CStr(IIf(IsDBNull(lector.Item("veh_Modelo")) = True, "", lector.Item("veh_Modelo")))
                    VehiculoSelect.ConstanciaIns = CStr(IIf(IsDBNull(lector.Item("veh_NConstanciaIns")) = True, "", lector.Item("veh_NConstanciaIns")))
                    VehiculoSelect.ConfVeh = CStr(IIf(IsDBNull(lector.Item("veh_ConfVeh")) = True, "", lector.Item("veh_ConfVeh")))
                    VehiculoSelect.Tara = CDec(IIf(IsDBNull(lector.Item("veh_Tara")) = True, 0, lector.Item("veh_Tara")))
                    VehiculoSelect.nombreTipoExistencia = CStr(IIf(IsDBNull(lector.Item("tex_Nombre")) = True, "", lector.Item("tex_Nombre")))
                    VehiculoSelect.NomLinea = CStr(IIf(IsDBNull(lector.Item("lin_Nombre")) = True, "", lector.Item("lin_Nombre")))
                    VehiculoSelect.NomSubLinea = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")) = True, "", lector.Item("sl_Nombre")))
                    VehiculoSelect.IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                    VehiculoSelect.NomEstadoProd = CStr(IIf(IsDBNull(lector.Item("ep_Nombre")) = True, "", lector.Item("ep_Nombre")))
                    VehiculoSelect.IdEstadoProd = CInt(IIf(IsDBNull(lector.Item("IdEstadoProd")) = True, 0, lector.Item("IdEstadoProd")))
                    VehiculoSelect.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    VehiculoSelect.IdSubLinea = CInt(IIf(IsDBNull(lector.Item("IdSubLInea")) = True, 0, lector.Item("IdSubLInea")))
                    Lista.Add(VehiculoSelect)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ProductoValidadMovAlmacenIdProducto(ByVal IdProducto As Integer) As Entidades.Producto
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoValidadMovAlmacenxIdProducto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim objProd As Entidades.Producto = Nothing
                If lector.Read Then
                    objProd = New Entidades.Producto
                    objProd.Id = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                End If
                lector.Close()
                Return objProd
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ValidarAtributos(ByVal NoVisible As String, ByVal prod_Codigo As String, ByVal prod_Atributos As String) As Entidades.Producto
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoValidarCodigoGenerado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@NoVisible", NoVisible)
        cmd.Parameters.AddWithValue("@prod_Codigo", prod_Codigo)
        cmd.Parameters.AddWithValue("@prod_Atributos", prod_Atributos)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim obj As Entidades.Producto = Nothing
                If lector.Read Then
                    obj = New Entidades.Producto
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                    obj.NoVisible = CStr(IIf(IsDBNull(lector.Item("NoVisible")) = True, "", lector.Item("NoVisible")))
                    obj.Codigo = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")) = True, "", lector.Item("prod_Codigo")))
                    obj.CodAtributos = CStr(IIf(IsDBNull(lector.Item("prod_Atributos")) = True, "", lector.Item("prod_Atributos")))
                End If
                lector.Close()
                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Producto_ListarProductosxParamsReporte(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As String, ByVal Descripcion As String, ByVal IdEmpresa As Integer, ByVal pagesize As Integer, ByVal pagenumber As Integer, ByVal IdPais As Integer, ByVal IdFabricante As Integer, ByVal IdMarca As Integer, ByVal IdModelo As Integer, ByVal IdFormato As Integer, ByVal IdProveedor As Integer, ByVal IdEstilo As Integer, ByVal IdTransito As Integer, ByVal IdCalidad As Integer) As List(Of Entidades.ProductoView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Dim cmd As New SqlCommand("[_Producto_ListarProductosxParamsSinStock]", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@CodSubLinea", CodSubLinea)
        cmd.Parameters.AddWithValue("@Descripcion", Descripcion)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@PageNumber", pagenumber)
        cmd.Parameters.AddWithValue("@PageSize", pagesize)

        cmd.Parameters.AddWithValue("@IdPais", IdPais)
        cmd.Parameters.AddWithValue("@IdFabricante", IdFabricante)
        cmd.Parameters.AddWithValue("@IdMarca", IdMarca)
        cmd.Parameters.AddWithValue("@IdModelo", IdModelo)
        cmd.Parameters.AddWithValue("@IdFormato", IdFormato)
        cmd.Parameters.AddWithValue("@IdProveedor", IdProveedor)
        cmd.Parameters.AddWithValue("@IdEstilo", IdEstilo)
        cmd.Parameters.AddWithValue("@IdTransito", IdTransito)
        cmd.Parameters.AddWithValue("@IdCalidad", IdCalidad)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ProductoView)
                Do While lector.Read
                    Dim obj As New Entidades.ProductoView

                    With obj

                        obj.UMPrincipal = CBool(lector.Item("pum_UnidadPrincipal"))
                        obj.IdLinea = CInt(lector.Item("IdLinea"))
                        obj.Id = CInt(lector.Item("IdProducto"))
                        obj.IdSubLInea = CInt(lector.Item("IdSubLInea"))
                        obj.IdUnidadMedida = CInt(lector.Item("IdUnidadMedida"))
                        obj.Linea = CStr(lector.Item("lin_Nombre"))
                        obj.Descripcion = CStr(lector.Item("prod_Nombre"))
                        obj.SubLinea = CStr(lector.Item("sl_Nombre"))
                        obj.UnidadMedida = CStr(lector.Item("um_NombreCorto"))
                        obj.PrecioCompra = CDec(IIf(IsDBNull(lector.Item("prod_PrecioCompra")) = True, 0, lector.Item("prod_PrecioCompra")))
                        obj.IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        obj.Moneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                        obj.Prod_Codigo = CStr(IIf(IsDBNull(lector.Item("Prod_Codigo")) = True, "", lector.Item("Prod_Codigo")))

                    End With

                    Lista.Add(obj)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function Producto_ListarProductosxParams_V2Reporte(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As String, ByVal Descripcion As String, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal pagesize As Integer, ByVal pagenumber As Integer, ByVal codigoProducto As String, ByVal tableTablaTipoValor As DataTable) As List(Of Entidades.ProductoView)
        Descripcion = Descripcion.Replace("*", "%")

        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Dim cmd As New SqlCommand("[_Producto_ListarProductosxParams_V2Reporte]", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@CodSubLinea", CodSubLinea)
        cmd.Parameters.AddWithValue("@Descripcion", Descripcion)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@PageNumber", pagenumber)
        cmd.Parameters.AddWithValue("@PageSize", pagesize)

        cmd.Parameters.AddWithValue("@CodigoProducto", codigoProducto)
        cmd.Parameters.AddWithValue("@Tabla", tableTablaTipoValor)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ProductoView)
                Do While lector.Read
                    Dim obj As New Entidades.ProductoView

                    With obj

                        obj.UMPrincipal = CBool(lector.Item("pum_UnidadPrincipal"))
                        obj.IdLinea = CInt(lector.Item("IdLinea"))
                        obj.Id = CInt(lector.Item("IdProducto"))
                        obj.IdSubLInea = CInt(lector.Item("IdSubLInea"))
                        obj.IdUnidadMedida = CInt(lector.Item("IdUnidadMedida"))
                        obj.Linea = CStr(lector.Item("lin_Nombre"))
                        obj.Descripcion = CStr(lector.Item("prod_Nombre"))
                        obj.SubLinea = CStr(lector.Item("sl_Nombre"))
                        obj.UnidadMedida = CStr(lector.Item("um_NombreCorto"))
                        obj.PrecioCompra = CDec(IIf(IsDBNull(lector.Item("prod_PrecioCompra")) = True, 0, lector.Item("prod_PrecioCompra")))
                        obj.IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        obj.Moneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                        obj.StockDisponible = CDec(IIf(IsDBNull(lector.Item("StockDisponible")) = True, 0, lector.Item("StockDisponible")))
                        obj.cadenaUM = CStr(IIf(IsDBNull(lector.Item("cadenaUM")) = True, "", lector.Item("cadenaUM")))
                        'obj.StockDisponible_Cadena = CStr(IIf(IsDBNull(lector.Item("StockDisponible_Cadena")) = True, "", lector.Item("StockDisponible_Cadena")))
                        obj.Codigo = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")) = True, "", lector.Item("prod_Codigo")))
                    End With

                    Lista.Add(obj)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function


    Public Function LongitudProducto() As Entidades.Producto
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("Longitud_Producto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim obj As Entidades.Producto = Nothing
                If lector.Read Then
                    obj = New Entidades.Producto
                    obj.IdSubLInea = CInt(IIf(IsDBNull(lector.Item("IdSubLinea")) = True, 0, lector.Item("IdSubLinea")))
                    obj.LongitudProd = CInt(IIf(IsDBNull(lector.Item("LongitudProd")) = True, 0, lector.Item("LongitudProd")))
                End If
                lector.Close()
                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub EliminarxIdProducto(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal idproducto As Integer)
        Dim cmd As New SqlCommand("_Producto_EliminarxIdProducto", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", idproducto)
        If cmd.ExecuteNonQuery = 0 Then
            Throw New Exception("Fall� el proceso de Eliminaci�n.")
        End If
    End Sub

    Public Function productoAfectoSublineaforInsert(ByVal idsublinea As Integer) As Integer
        Dim resul As Integer = 0
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Try
            cn.Open()
            Dim cmd As New SqlCommand("  select dbo._FX_ProductoAfectoPercepcionForInsert(" + CStr(idsublinea) + ")   ", cn)
            cmd.CommandType = CommandType.Text
            resul = CInt(cmd.ExecuteScalar)

        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return resul
    End Function
    Public Function productoAfectoSublineaForUpdate(ByVal idsublinea As Integer, ByVal idproducto As Integer) As Integer
        Dim resp As Integer = 0
        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Try
            cn.Open()
            Dim cmd As New SqlCommand("  select dbo._Fx_productoAfectoPercepcionForUpdate(" + CStr(idsublinea) + "," + CStr(idproducto) + ")   ", cn)
            cmd.CommandType = CommandType.Text

            resp = CInt(cmd.ExecuteScalar)

            cn.Close()
        Catch ex As Exception
            Throw ex
        End Try
        Return resp
    End Function


End Class
