﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.
Imports System.Data.SqlClient

Public Class DAOOrdenCompra
    Dim ObjConexion As New Conexion
    Dim HDAO As New DAO.HelperDAO
    Dim objDaoDCC As DAO.DAODocumento_CondicionC
    Private Util As New DAO.DAOUtil

#Region "Mantenimiento"

    Public Sub Anular(ByVal IdDocumento As Integer, ByVal DeleteDetalleDocumento As Boolean, ByVal DeletePuntoLlegada As Boolean, ByVal DeleteCondicionC As Boolean, ByVal DeleteObservacion As Boolean, ByVal DeleteMontoRegimen As Boolean, ByVal DeleteCuentaPorPagar As Boolean, ByVal Anular As Boolean, ByVal sqlcn As SqlConnection, ByVal sqltr As SqlTransaction)
        Dim cmd As New SqlCommand("_DocumentoOrdenCompra_DeshacerMov", sqlcn, sqltr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@DeleteDetalleDocumento", DeleteDetalleDocumento)
        cmd.Parameters.AddWithValue("@DeletePuntoLlegada", DeletePuntoLlegada)
        cmd.Parameters.AddWithValue("@DeleteCondicionC", DeleteCondicionC)
        cmd.Parameters.AddWithValue("@DeleteObservacion", DeleteObservacion)
        cmd.Parameters.AddWithValue("@DeleteMontoRegimen", DeleteMontoRegimen)
        cmd.Parameters.AddWithValue("@DeleteCuentaPorPagar", DeleteCuentaPorPagar)
        cmd.Parameters.AddWithValue("@Anular", Anular)

        cmd.ExecuteNonQuery()

    End Sub

    Public Function Validar_CosteoImportacion(ByVal IdDocumento As Integer) As Nullable(Of Integer)

        Dim IdDocumentoRef As Object
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoOrdenCompra_Val33", cn)
        cn.Open()
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        Try
            IdDocumentoRef = cmd.ExecuteScalar()
            If IsNumeric(IdDocumentoRef) Then
                Return CInt(IdDocumentoRef)
            End If
        Catch ex As Exception
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Nothing
    End Function

    Public Function OrdenCompra_Select_Tipo(ByVal tipoOrden As Boolean, ByVal aprobado As Boolean, _
                                            ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal pageIndex As Integer, _
                                            ByVal pageSize As Integer) As List(Of Entidades.DocumentoView)
        OrdenCompra_Select_Tipo = New List(Of Entidades.DocumentoView)
        Dim lector As SqlDataReader = Nothing
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_OrdenCompra_Select_Tipo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@TipoOrden", tipoOrden)
        cmd.Parameters.AddWithValue("@Aprobado", aprobado)
        cmd.Parameters.AddWithValue("@IdEmpresa", idempresa)
        cmd.Parameters.AddWithValue("@IdTienda", idtienda)
        cmd.Parameters.AddWithValue("@pageIndex", pageIndex)
        cmd.Parameters.AddWithValue("@pageSize", pageSize)
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.DocumentoView
                With obj
                    .IdDocumento = CInt(lector("IdDocumento"))
                    .Codigo = CStr(lector("doc_Codigo"))
                    .Serie = CStr(lector("doc_Serie"))
                    .FechaEmision = CStr(lector("doc_FechaEmision"))
                    .NomMoneda = CStr(lector("mon_Simbolo"))
                    .TotalAPagar = CDec(lector("doc_TotalAPagar"))
                    .RazonSocial = CStr(lector("jur_Rsocial"))
                    .RUC = CStr(lector("Ruc"))
                    .NomEmpleado = CStr(lector("Contacto"))
                    .doc_Importacion = CObj(lector("doc_Importacion"))
                    .EnviarTablaCuenta = CBool(lector("anex_Aprobar"))
                End With
            Loop
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return OrdenCompra_Select_Tipo
    End Function

    Public Function OrdenCompra_Datos_Contacto(ByVal IdPersona As Integer) As List(Of Entidades.PersonaView)
        OrdenCompra_Datos_Contacto = New List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_OrdenCompra_Datos_Contacto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@idpersona", IdPersona)
        Dim lector As SqlDataReader
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.PersonaView
                With obj
                    .IdPersona = CInt(lector("IdPersona"))
                    .IdTipoAgente = CInt(lector("IdTabla")) ' IDTABLA
                    .Descripcion = CStr(lector("Descripcion")) ' DESCRIPCION
                    .NombreComercial = CStr(lector("Tabla")) ' TABLA
                End With
                OrdenCompra_Datos_Contacto.Add(obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return OrdenCompra_Datos_Contacto
    End Function

    Public Function CuentaProveedor_SelectActivo(ByVal IdProveedor As Integer, ByVal IdPropietario As Integer) As List(Of Entidades.CuentaProveedor)
        CuentaProveedor_SelectActivo = New List(Of Entidades.CuentaProveedor)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CuentaProveedor_SelectActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdProveedor", IdProveedor)
        cmd.Parameters.AddWithValue("@IdPropietario", IdPropietario)
        Dim lector As SqlDataReader
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.CuentaProveedor
                With obj
                    .IdMoneda = CInt(lector("IdMoneda"))
                    .cprov_CargoMax = CDec(lector("cprov_CargoMax"))
                    .cprov_Saldo = CDec(lector("cprov_Saldo"))
                    .IdProveedor = CInt(lector("IdProveedor"))
                    .IdPropietario = CInt(lector("IdPropietario"))
                End With
                CuentaProveedor_SelectActivo.Add(obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return CuentaProveedor_SelectActivo
    End Function

    Public Function SelectOrdenCompra(ByVal idserie As Integer, ByVal codigo As Integer, Optional ByVal idDocumento As Integer = 0) As Entidades.Documento
        Dim obj As New Entidades.Documento
        Dim lector As SqlDataReader = Nothing
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_selectOrdenCompra", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@idserie", idserie)
        cmd.Parameters.AddWithValue("@codigo", codigo)
        cmd.Parameters.AddWithValue("@IdDocumento", IIf(idDocumento = Nothing, DBNull.Value, idDocumento))
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If lector.HasRows = False Then
                Throw New Exception("No existe documento con Ese código")
            End If
            If lector.Read Then
                With obj
                    .Id = CInt(lector("IdDocumento"))
                    .Codigo = CStr(lector("doc_Codigo"))

                    .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")), Nothing, lector("doc_FechaEmision")))
                    .FechaAEntregar = CDate(IIf(IsDBNull(lector("doc_FechaAentregar")), Nothing, lector("doc_FechaAentregar")))
                    .FechaVenc = CDate(IIf(IsDBNull(lector("doc_FechaVenc")), Nothing, lector("doc_FechaVenc")))
                    .FechaCancelacion = CDate(IIf(IsDBNull(lector("doc_FechaCancelacion")), Nothing, lector("doc_FechaCancelacion")))

                    .Descuento = CDec(IIf(IsDBNull(lector("doc_Descuento")), 0, lector("doc_Descuento")))
                    .SubTotal = CDec(IIf(IsDBNull(lector("doc_SubTotal")), 0, lector("doc_SubTotal")))
                    .IGV = CDec(IIf(IsDBNull(lector("doc_Igv")), 0, lector("doc_Igv")))
                    .Total = CDec(IIf(IsDBNull(lector("doc_Total")), 0, lector("doc_Total")))
                    .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")), 0, lector("doc_TotalAPagar")))
                    .TotalLetras = CStr(IIf(IsDBNull(lector("doc_TotalLetras")), "", lector("doc_TotalLetras")))

                    .IdEstadoDoc = CInt(IIf(IsDBNull(lector("IdEstadoDoc")), 0, lector("IdEstadoDoc")))
                    .IdEstadoCancelacion = CInt(IIf(IsDBNull(lector("IdEstadoCan")), 0, lector("IdEstadoCan")))
                    .IdEstadoEntrega = CInt(IIf(IsDBNull(lector("IdEstadoEnt")), 0, lector("IdEstadoEnt")))
                    .IdCondicionPago = CInt(IIf(IsDBNull(lector("IdCondicionPago")), 0, lector("IdCondicionPago")))

                    .IdPersona = CInt(lector("IdPersona"))
                    .IdTipoAgente = CInt(lector("IdTipoAgente"))
                    .IdDestinatario = CInt(IIf(IsDBNull(lector("IdDestinatario")), 0, lector("IdDestinatario")))
                    .IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")), 0, lector("IdMoneda")))
                    .LugarEntrega = CInt(IIf(IsDBNull(lector("LugarEntrega")), 0, lector("LugarEntrega")))
                    .IdEmpresa = CInt(lector("IdEmpresa"))
                    .IdTienda = CInt(lector("IdTienda"))
                    .IdSerie = CInt(lector("IdSerie"))
                    .IdRemitente = CInt(IIf(IsDBNull(lector("IdRemitente")), 0, lector("IdRemitente")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(lector("IdTipoOperacion")), 0, lector("IdTipoOperacion")))
                    .doc_Importacion = lector("doc_Importacion").ToString
                    .IdArea = CInt(IIf(IsDBNull(lector("IdArea")), 0, lector("IdArea")))
                    .anex_PrecImportacion = CInt(IIf(IsDBNull(lector("IdTPImp")), 0, lector("IdTPImp")))
                    .Direccion = CStr(IIf(IsDBNull(lector("IdCentroCosto")), "", lector("IdCentroCosto")))
                    .IdDocRelacionado = CInt(IIf(IsDBNull(lector("IdDocumento2")), 0, lector("IdDocumento2")))
                    .EnviarTablaCuenta = CBool(IIf(IsDBNull(lector("MovIdDocumento")), False, True))
                    'si es diferente a 0 es por q hay una factura 
                    .ValorReferencial = CDec(IIf(IsDBNull(lector("TieneEnlace")), 0, lector("TieneEnlace")))
                    .anex_igv = CBool(IIf(IsDBNull(lector("anex_igv")), False, lector("anex_igv")))
                End With
            End If

        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return obj
    End Function

    Public Function InsertBD_OrdenCompra(ByVal obj As Entidades.Documento, ByVal cn As SqlConnection, _
                            ByVal tr As SqlTransaction) As String
        Dim parametro() As SqlParameter = New SqlParameter(30) {}

        parametro(0) = New SqlParameter("@doc_FechaEmision", SqlDbType.DateTime)
        parametro(0).Value = IIf(obj.FechaEmision = Nothing, DBNull.Value, obj.FechaEmision)

        parametro(1) = New SqlParameter("@doc_FechaAentregar", SqlDbType.DateTime)
        parametro(1).Value = IIf(obj.FechaAEntregar = Nothing, DBNull.Value, obj.FechaAEntregar)

        parametro(2) = New SqlParameter("@doc_FechaVenc", SqlDbType.DateTime)
        parametro(2).Value = IIf(obj.FechaVenc = Nothing, DBNull.Value, obj.FechaVenc)

        parametro(3) = New SqlParameter("@doc_Descuento", SqlDbType.Decimal)
        parametro(3).Value = IIf(obj.Descuento = Nothing, DBNull.Value, obj.Descuento)

        parametro(4) = New SqlParameter("@doc_SubTotal", SqlDbType.Decimal)
        parametro(4).Value = IIf(obj.SubTotal = Nothing, DBNull.Value, obj.SubTotal)

        parametro(5) = New SqlParameter("@doc_Igv", SqlDbType.Decimal)
        parametro(5).Value = IIf(obj.IGV = Nothing, DBNull.Value, obj.IGV)

        parametro(6) = New SqlParameter("@doc_Total", SqlDbType.Decimal)
        parametro(6).Value = IIf(obj.Total = Nothing, DBNull.Value, obj.Total)

        parametro(7) = New SqlParameter("@doc_TotalLetras", SqlDbType.Xml)
        parametro(7).Value = IIf(obj.TotalLetras = Nothing, DBNull.Value, obj.TotalLetras)

        parametro(8) = New SqlParameter("@doc_TotalAPagar", SqlDbType.Decimal)
        parametro(8).Value = IIf(obj.TotalAPagar = Nothing, DBNull.Value, obj.TotalAPagar)

        parametro(9) = New SqlParameter("@IdPersona", SqlDbType.Int) '** Proveedor
        parametro(9).Value = IIf(obj.IdPersona = Nothing, DBNull.Value, obj.IdPersona)

        parametro(10) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        parametro(10).Value = IIf(obj.IdUsuario = Nothing, DBNull.Value, obj.IdUsuario)

        parametro(11) = New SqlParameter("@IdDestinatario", SqlDbType.Int)
        parametro(11).Value = IIf(obj.IdDestinatario = Nothing, DBNull.Value, obj.IdDestinatario)

        parametro(12) = New SqlParameter("@IdEstadoDoc", SqlDbType.Int)
        parametro(12).Value = IIf(obj.IdEstadoDoc = Nothing, DBNull.Value, obj.IdEstadoDoc)

        parametro(13) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        parametro(13).Value = IIf(obj.IdMoneda = Nothing, DBNull.Value, obj.IdMoneda)

        parametro(14) = New SqlParameter("@LugarEntrega", SqlDbType.Int)
        parametro(14).Value = IIf(obj.LugarEntrega = Nothing, DBNull.Value, obj.LugarEntrega)

        parametro(15) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        parametro(15).Value = IIf(obj.IdTipoOperacion = Nothing, DBNull.Value, obj.IdTipoOperacion)

        parametro(16) = New SqlParameter("@IdTienda", SqlDbType.Int)
        parametro(16).Value = IIf(obj.IdTienda = Nothing, DBNull.Value, obj.IdTienda)

        parametro(17) = New SqlParameter("@IdSerie", SqlDbType.Int)
        parametro(17).Value = IIf(obj.IdSerie = Nothing, DBNull.Value, obj.IdSerie)

        parametro(18) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        parametro(18).Value = IIf(obj.IdEmpresa = Nothing, DBNull.Value, obj.IdEmpresa)

        parametro(19) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        parametro(19).Value = IIf(obj.IdTipoDocumento = Nothing, DBNull.Value, obj.IdTipoDocumento)

        parametro(20) = New SqlParameter("@IdEstadoCan", SqlDbType.Int)
        parametro(20).Value = IIf(obj.IdEstadoCancelacion = Nothing, DBNull.Value, obj.IdEstadoCancelacion)

        parametro(21) = New SqlParameter("@IdEstadoEnt", SqlDbType.Int)
        parametro(21).Value = IIf(obj.IdEstadoEntrega = Nothing, DBNull.Value, obj.IdEstadoEntrega)

        parametro(22) = New SqlParameter("@doc_Importacion", SqlDbType.Bit)
        parametro(22).Value = IIf(obj.doc_Importacion.ToString = "", DBNull.Value, CBool(obj.doc_Importacion))

        parametro(23) = New SqlParameter("@IdRemitente", SqlDbType.Int)
        parametro(23).Value = IIf(obj.IdRemitente = Nothing, DBNull.Value, obj.IdRemitente)

        parametro(24) = New SqlParameter("@IdArea", SqlDbType.Int)
        parametro(24).Value = IIf(obj.IdArea = Nothing, DBNull.Value, obj.IdArea)

        parametro(25) = New SqlParameter("@anex_PrecImportacion", SqlDbType.Int)
        parametro(25).Value = IIf(obj.anex_PrecImportacion = Nothing, DBNull.Value, obj.anex_PrecImportacion)

        parametro(26) = New SqlParameter("@doc_FechaCancelacion", SqlDbType.Date)
        parametro(26).Value = IIf(obj.FechaCancelacion = Nothing, DBNull.Value, obj.FechaCancelacion)

        parametro(27) = New SqlParameter("@IdCondicionPago", SqlDbType.Int)
        parametro(27).Value = IIf(obj.IdCondicionPago = Nothing, DBNull.Value, obj.IdCondicionPago)

        parametro(28) = New SqlParameter("@idcentrocosto", SqlDbType.VarChar)
        parametro(28).Value = IIf(obj.Direccion = Nothing, DBNull.Value, obj.Direccion)

        parametro(29) = New SqlParameter("@enviarCuentaPorPagar", SqlDbType.Bit)
        parametro(29).Value = obj.EnviarTablaCuenta

        parametro(30) = New SqlParameter("@anex_igv", SqlDbType.Bit)
        parametro(30).Value = obj.anex_igv

        Dim cmd As New SqlCommand("_InsertOrdenCompra", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametro)
        Try
            Return cmd.ExecuteScalar().ToString
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function InsertarOrdenCompra(ByVal ObjDocumento As Entidades.Documento, _
                                        ByVal listaDetalle As List(Of Entidades.DetalleDocumento), _
                                        ByVal obs As Entidades.Observacion, _
                                        ByVal objMonto As Entidades.MontoRegimen, _
                                        ByVal listaCondicionComercial As List(Of Entidades.CondicionComercial)) _
                                        As String
        Dim cad As String = String.Empty
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        cn.Open()
        Dim tr As SqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)



        Try
            ' ****************** DOCUMENTO
            cad = Me.InsertBD_OrdenCompra(ObjDocumento, cn, tr)
            ObjDocumento.Id = CInt(cad.Split(CChar(","))(0))

            ' ****************** DETALLEDOCUMENTO
            Dim ObjDaoDetalleDocumento As New DAO.DAODetalleDocumento
            Dim ObjDAOAnexo_DetalleDocumento As New DAO.DAOAnexo_DetalleDocumento

            For i As Integer = 0 To listaDetalle.Count - 1
                With listaDetalle.Item(i)
                    .IdDocumento = ObjDocumento.Id
                    Dim iddetalle As Integer = ObjDaoDetalleDocumento.InsertaDetalleDocumento(cn, listaDetalle(i), tr)
                    Dim objAnexo_DetalleDoc As New Entidades.Anexo_DetalleDocumento
                    With objAnexo_DetalleDoc
                        .IdDocumento = listaDetalle(i).IdDocumento
                        .IdKit = listaDetalle(i).IdKit
                        .ComponenteKit = listaDetalle(i).Kit
                        .IdDetalleDocumento = iddetalle
                    End With

                    ObjDAOAnexo_DetalleDocumento._Anexo_DetalleDocumentoInsert(objAnexo_DetalleDoc, cn, tr)

                End With
            Next





            Dim montoRegimen As New DAO.DAOMontoRegimen

            If ObjDocumento.CompPercepcion = True And objMonto.Monto > 0 Then
                objMonto.IdDocumento = ObjDocumento.Id
                montoRegimen.InsertaMontoRegimen(cn, objMonto, tr)
            End If


            
            updatePrecioCompraProducto(ObjDocumento.IdMoneda, listaDetalle, cn, tr)

            Dim objPuntoLLegada As New Entidades.PuntoLlegada
            Dim objDaoPuntoLLegada As New DAO.DAOPuntoLlegada

            With objPuntoLLegada
                .IdDocumento = ObjDocumento.Id
                .IdTienda = ObjDocumento.IdTienda
                .IdAlmacen = ObjDocumento.LugarEntrega
            End With

            objDaoPuntoLLegada.InsertaPuntoLlegadaT(cn, objPuntoLLegada, tr)

            For i As Integer = 0 To listaCondicionComercial.Count - 1

                listaCondicionComercial.Item(i).IdDocumento = ObjDocumento.Id
                listaCondicionComercial.Item(i).IdTipoDocumento = ObjDocumento.IdTipoDocumento

                objDaoDCC = New DAODocumento_CondicionC
                objDaoDCC.Documento_CondicionC_Insert(cn, tr, listaCondicionComercial(i))
            Next




            '******************** OBSERVACIONES
            If (obs IsNot Nothing) Then
                Dim objObservacion As New DAO.DAOObservacion

                obs.IdDocumento = ObjDocumento.Id
                objObservacion.InsertaObservacionT(cn, tr, obs)

            End If



            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return cad
    End Function

    Public Function listarOrdenesCompraxFiltro(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdProveedor As Integer, ByVal FechaIni As String, ByVal FechaFin As String, ByVal IdEstadoCan As Integer, ByVal IdEstadoEnt As Integer, ByVal PageIndex As Integer, ByVal PageSize As Integer) As List(Of Entidades.DocumentoView)

        Dim parametros() As SqlParameter = New SqlParameter(8) {}

        parametros(0) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        parametros(0).Value = IdEmpresa
        parametros(1) = New SqlParameter("@IdTienda", SqlDbType.Int)
        parametros(1).Value = IdTienda
        parametros(2) = New SqlParameter("@IdProveedor", SqlDbType.Int)
        parametros(2).Value = IdProveedor
        parametros(3) = New SqlParameter("@FechaIni", SqlDbType.Char, 10)
        parametros(3).Value = FechaIni
        parametros(4) = New SqlParameter("@FechaFin", SqlDbType.Char, 10)
        parametros(4).Value = FechaFin
        parametros(5) = New SqlParameter("@IdEstadoCan", SqlDbType.Int)
        parametros(5).Value = IdEstadoCan
        parametros(6) = New SqlParameter("@IdEstadoEntrega", SqlDbType.Int)
        parametros(6).Value = IdEstadoEnt
        parametros(7) = New SqlParameter("@pageindex", SqlDbType.Int)
        parametros(7).Value = PageIndex
        parametros(8) = New SqlParameter("@pageSize", SqlDbType.Int)
        parametros(8).Value = PageSize

        Dim Lista As New List(Of Entidades.DocumentoView)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Try
            cn.Open()
            Dim lector As SqlDataReader
            Dim cmd As New SqlCommand("_ListarOrdenesCompraxFiltro", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(parametros)
            cmd.CommandTimeout = 0
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            While lector.Read
                Dim obj As New Entidades.DocumentoView
                With obj
                    .Id = CInt(lector("IdDocumento"))
                    .Codigo = CStr(IIf(IsDBNull(lector("Codigo")), "", lector("Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector("Serie")), "", lector("Serie")))
                    .FechaEmision = CStr(IIf(IsDBNull(lector("FechaEmision")), Nothing, lector("FechaEmision")))
                    .FechaEntrega = CDate(IIf(IsDBNull(lector("FechaEntrega")), Nothing, lector("FechaEntrega")))
                    .NomMoneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")), "", lector("mon_Simbolo")))
                    .Total = CDec(IIf(IsDBNull(lector("total")), 0, lector("total")))
                    .NomMonedaDestino = CStr(IIf(IsDBNull(lector("mon_SimboloDestino")), "", lector("mon_SimboloDestino")))
                    .TotalAPagar = CDec(IIf(IsDBNull(lector("totalDestino")), 0, lector("totalDestino")))
                    .DescripcionPersona = CStr(IIf(IsDBNull(lector("Proveedor")), "", lector("Proveedor")))
                    .RUC = CStr(IIf(IsDBNull(lector("Ruc")), "", lector("Ruc")))
                    .NroDocumento = .Serie + " - " + .Codigo
                    .NomContactoProveedor = CStr(IIf(IsDBNull(lector("ProveedorC")), "", lector("ProveedorC")))
                    .NomEmpleado = CStr(IIf(IsDBNull(lector("Usuario")), "", lector("Usuario")))
                    .NomContactoPropietario = CStr(IIf(IsDBNull(lector("UsuarioC")), "", lector("UsuarioC")))
                    .NomEstadoDocumento = CStr(IIf(IsDBNull(lector("edoc_Nombre")), "", lector("edoc_Nombre")))
                    .NomEstadoCancelacion = CStr(IIf(IsDBNull(lector("ecan_Nombre")), "", lector("ecan_Nombre")))
                    .NomEstadoEntregado = CStr(IIf(IsDBNull(lector("eent_Nombre")), "", lector("eent_Nombre")))
                    .NomCondicionPago = CStr(IIf(IsDBNull(lector("cp_Nombre")), "", lector("cp_Nombre")))
                End With
                Lista.Add(obj)
            End While
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

        Return Lista

    End Function

    Public Function ListarPrecioXidMoneda(ByVal idmoneda As Integer, ByVal idproducto As Integer) As Decimal
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ListarPrecioXidMoneda", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idmoneda", idmoneda)
        cmd.Parameters.AddWithValue("@idproducto", idproducto)
        Dim lector As SqlDataReader
        Dim PrecioSD As Decimal
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If lector.Read Then
                PrecioSD = CDec(IIf(IsDBNull(lector("PrecioSD")), 0.0, lector("PrecioSD")))
            End If
            lector.Close()
            Return PrecioSD
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function listarContactoxIdPersona(ByVal idPersona As Integer, ByVal addIdUsuario As Integer) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_listarContactoxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idpersona", idPersona)
        cmd.Parameters.AddWithValue("@addIdUsuario", IIf(addIdUsuario = Nothing, DBNull.Value, addIdUsuario))
        Dim lector As SqlDataReader
        Dim lista As New List(Of Entidades.PersonaView)
        Try
            cn.Open()
            Using cn
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                While lector.Read
                    Dim obj As New Entidades.PersonaView
                    With obj
                        .IdPersona = CInt(lector("IdPersona"))
                        .Nombres = CStr(IIf(IsDBNull(lector("Nombre")), "---", lector("Nombre")))
                    End With
                    lista.Add(obj)
                End While
                lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function listarPersonaJuridicaxRolxEmpresa(ByVal NomPersona As String, ByVal Ruc As String, ByVal Tipo As Integer, ByVal IdRol As Integer, ByVal Condicion As Integer, ByVal Estado As Integer, ByVal PageIndex As Integer, ByVal PageSize As Integer, Optional ByVal Dni As String = "") As List(Of Entidades.PersonaView)

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_listarPersonaJuridicaxRolxEmpresa", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Dni", Dni)
        cmd.Parameters.AddWithValue("@Ruc", Ruc)
        cmd.Parameters.AddWithValue("@razonApe", NomPersona)
        cmd.Parameters.AddWithValue("@tipo", Tipo)
        cmd.Parameters.AddWithValue("@pageindex", PageIndex)
        cmd.Parameters.AddWithValue("@pageSize", PageSize)
        cmd.Parameters.AddWithValue("@idRol", IdRol)
        cmd.Parameters.AddWithValue("@estado", Estado)
        cmd.Parameters.AddWithValue("@condicion", Condicion)

        Dim lector As SqlDataReader
        Try
            cn.Open()
            Using cn
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                While lector.Read
                    Dim obj As New Entidades.PersonaView
                    With obj
                        .IdPersona = CInt(lector("IdPersona"))
                        .RazonSocial = CStr(IIf(IsDBNull(lector("jur_Rsocial")), "---", lector("jur_Rsocial")))
                        .Direccion = CStr(IIf(IsDBNull(lector("dir_Direccion")), "---", lector("dir_Direccion")))
                        .Ruc = CStr(IIf(IsDBNull(lector("RUC")), "---", lector("RUC")))
                        .Telefeono = CStr(IIf(IsDBNull(lector("Telefono")), "---", lector("Telefono")))
                        .Correo = CStr(IIf(IsDBNull(lector("Correo")), "---", lector("Correo")))
                        .IdTipoAgente = CInt(lector("IdTipoAgente"))
                    End With
                    Lista.Add(obj)
                End While
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function listarProductosProveedorOrdenCompra(ByVal idempresa As Integer, _
          ByVal idproveedor As Integer, ByVal IdAgenteProveedor As Integer, ByVal descripcion As String, ByVal idlinea As Integer, _
          ByVal idsublinea As Integer, ByVal codsublinea As Integer, ByVal idmoneda As Integer, _
          ByVal pageindex As Integer, ByVal pageSise As Integer, ByVal vertodo As Boolean, _
          ByVal IdArea As Integer, ByVal tabla As DataTable, ByVal codigoProducto As String, ByVal codigoProveedor As String, _
          ByVal IncluyeIgv As Boolean) As List(Of Entidades.Catalogo)

        descripcion = descripcion.Replace("*", "%")

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_listarProductoProveedorOrdenCompra", cn)
        cmd.CommandTimeout = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idempresa", idempresa)
        cmd.Parameters.AddWithValue("@idproveeedor", idproveedor)
        cmd.Parameters.AddWithValue("@IdAgenteProveedor", IdAgenteProveedor)
        cmd.Parameters.AddWithValue("@descripcion", descripcion)
        cmd.Parameters.AddWithValue("@linea", idlinea)
        cmd.Parameters.AddWithValue("@sublinea", idsublinea)
        cmd.Parameters.AddWithValue("@codsublinea", codsublinea)
        cmd.Parameters.AddWithValue("@idmoneda", idmoneda)
        cmd.Parameters.AddWithValue("@pageindex", pageindex)
        cmd.Parameters.AddWithValue("@pageSize", pageSise)
        cmd.Parameters.AddWithValue("@vertodo", vertodo)
        cmd.Parameters.AddWithValue("@idarea", IdArea)
        cmd.Parameters.AddWithValue("@Tabla", tabla)
        cmd.Parameters.AddWithValue("@CodigoProducto", codigoProducto)
        cmd.Parameters.AddWithValue("@CodigoProveedor", codigoProveedor)
        cmd.Parameters.AddWithValue("@incluyeigv", IncluyeIgv)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Catalogo)
                Do While lector.Read
                    Dim ObjCatalogo As New Entidades.Catalogo
                    With ObjCatalogo
                        .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")), "", lector.Item("prod_Codigo")))
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")), 0, lector.Item("IdProducto")))
                        .NomLinea = CStr(IIf(IsDBNull(lector.Item("lin_Nombre")), "", lector.Item("lin_Nombre")))
                        .NomSubLinea = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")), "", lector.Item("sl_Nombre")))
                        .Descripcion = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")), "", lector.Item("prod_Nombre")))
                        .NoVisible = CStr(IIf(IsDBNull(lector.Item("NoVisible")), "", lector.Item("NoVisible")))
                        .Cod_Proveedor = CStr(IIf(IsDBNull(lector.Item("Cod_Proveedor")), "", lector.Item("Cod_Proveedor")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")), 0, lector.Item("IdUnidadMedida")))
                        .NomTipoPV = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")), "", lector.Item("um_NombreCorto")))
                        .StockDisponibleN = CDec(IIf(IsDBNull(lector.Item("StockDisponible")), 0, lector.Item("StockDisponible")))
                        .SimbMoneda = CStr(IIf(IsDBNull(lector("SimbMoneda")), "---", lector("SimbMoneda")))
                        .Kit = CBool(IIf(IsDBNull(lector.Item("prod_Kit")) = True, 0, lector.Item("prod_Kit")))
                        .PrecioSD = CDec(IIf(IsDBNull(lector("PrecioSD")), 0, lector("PrecioSD")))
                        .Percepcion = CDec(IIf(IsDBNull(lector.Item("Percepcion")), 0, lector.Item("Percepcion")))
                        .codBarraFabricante = CStr(IIf(IsDBNull(lector.Item("Prod_CodBarraFabricante")), "", lector.Item("Prod_CodBarraFabricante")))
                    End With
                    Lista.Add(ObjCatalogo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function listarProductosProveedorOrdenCompraCodBarraFab(ByVal idempresa As Integer, _
           ByVal idproveedor As Integer, ByVal IdAgenteProveedor As Integer _
           , ByVal idmoneda As Integer, _
           ByVal IdArea As Integer, _
           ByVal IncluyeIgv As Boolean, ByVal CodbarraFab As String) As List(Of Entidades.Catalogo)

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_listarProductoProveedorOrdenCompraCodBarraFab", cn)
        cmd.CommandTimeout = 0
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@idempresa", idempresa)
        cmd.Parameters.AddWithValue("@idproveeedor", idproveedor)
        cmd.Parameters.AddWithValue("@IdAgenteProveedor", IdAgenteProveedor)
        cmd.Parameters.AddWithValue("@idmoneda", idmoneda)
        cmd.Parameters.AddWithValue("@idarea", IdArea)
        cmd.Parameters.AddWithValue("@incluyeigv", IncluyeIgv)
        cmd.Parameters.AddWithValue("@CodBarraFabricante", CodbarraFab)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Catalogo)
                Do While lector.Read
                    Dim ObjCatalogo As New Entidades.Catalogo
                    With ObjCatalogo
                        .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")), "", lector.Item("prod_Codigo")))
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")), 0, lector.Item("IdProducto")))
                        .NomLinea = CStr(IIf(IsDBNull(lector.Item("lin_Nombre")), "", lector.Item("lin_Nombre")))
                        .NomSubLinea = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")), "", lector.Item("sl_Nombre")))
                        .Descripcion = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")), "", lector.Item("prod_Nombre")))
                        .NoVisible = CStr(IIf(IsDBNull(lector.Item("NoVisible")), "", lector.Item("NoVisible")))
                        .Cod_Proveedor = CStr(IIf(IsDBNull(lector.Item("Cod_Proveedor")), "", lector.Item("Cod_Proveedor")))

                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")), 0, lector.Item("IdUnidadMedida")))
                        .NomTipoPV = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")), "", lector.Item("um_NombreCorto")))
                        .SimbMoneda = CStr(IIf(IsDBNull(lector("SimbMoneda")), "---", lector("SimbMoneda")))
                        .PrecioSD = CDec(IIf(IsDBNull(lector("PrecioSD")), 0, lector("PrecioSD")))
                        .StockAReal = CDec(IIf(IsDBNull(lector.Item("Stock")), 0, lector.Item("Stock")))
                        .Percepcion = CDec(IIf(IsDBNull(lector.Item("Percepcion")), 0, lector.Item("Percepcion")))
                        .codBarraFabricante = CStr(IIf(IsDBNull(lector.Item("Prod_CodBarraFabricante")), "", lector.Item("Prod_CodBarraFabricante")))
                    End With
                    Lista.Add(ObjCatalogo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function listarDireccionAlmacen() As List(Of Entidades.Almacen)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ListarDireccionAlmacen", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                cmd.CommandTimeout = 0
                Dim Lista As New List(Of Entidades.Almacen)
                While lector.Read
                    Dim almacen As New Entidades.Almacen
                    With almacen
                        .IdAlmacen = CInt(lector.Item("IdAlmacen"))
                        .Nombre = CStr(lector.Item("alm_Nombre"))
                        .Direccion = CStr(lector.Item("alm_Direccion"))
                    End With
                    Lista.Add(almacen)
                End While
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub ActualizarDocCondiconComercial(ByVal cn As SqlConnection, ByVal t As SqlTransaction, ByVal iddocumento As Integer)
        Dim cmd As New SqlCommand("_actualizarDocCondiconComercial", cn, t)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            cmd.Parameters.AddWithValue("@iddocumento", iddocumento)
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function listarProductoPedidoSucursal(ByVal idtienda As Integer, ByVal idlinea As Integer, ByVal idsublinea As Integer, _
                                                 ByVal descripcion As String, ByVal codsublinea As Integer, ByVal pageIndex As Integer, ByVal pageSize As Integer) As List(Of Entidades.Catalogo)
        descripcion = descripcion.Replace("*", "%")

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_listarProductoPedidoSucursal", cn)
        cmd.CommandTimeout = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idtienda", idtienda)
        cmd.Parameters.AddWithValue("@linea", idlinea)
        cmd.Parameters.AddWithValue("@sublinea", idsublinea)
        cmd.Parameters.AddWithValue("@descripcion", descripcion)
        cmd.Parameters.AddWithValue("@codsublinea", codsublinea)
        cmd.Parameters.AddWithValue("@pageindex", pageIndex)
        cmd.Parameters.AddWithValue("@pageSize", pageSize)
        Dim lector As SqlDataReader

        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Catalogo)
                Do While lector.Read
                    Dim objCatalogo As New Entidades.Catalogo
                    With objCatalogo
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")), 0, lector.Item("IdProducto")))
                        .NomLinea = CStr(IIf(IsDBNull(lector.Item("lin_Nombre")), "", lector.Item("lin_Nombre")))
                        .NomSubLinea = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")), "", lector.Item("sl_Nombre")))
                        .Descripcion = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")), "", lector.Item("prod_Nombre")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")), 0, lector.Item("IdUnidadMedida")))
                        .NomTipoPV = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")), "", lector.Item("um_NombreCorto")))
                        .StockAReal = CDec(IIf(IsDBNull(lector.Item("Stock")), 0.0, lector.Item("Stock")))
                    End With
                    Lista.Add(objCatalogo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function listarPreciosCompra(ByVal idproducto As Integer) As List(Of Entidades.DetalleDocumento)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_listarPreciosCompra", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idproducto", idproducto)
        cmd.CommandTimeout = 0
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleDocumento)
                Do While lector.Read
                    Dim objDetallle As New Entidades.DetalleDocumento
                    objDetallle.CantidadxAtenderText = CStr(lector.Item("doc_FechaRegistro"))
                    objDetallle.Cantidad = CDec(lector.Item("dc_Cantidad"))
                    objDetallle.Moneda = CStr(lector.Item("mon_Simbolo"))
                    objDetallle.PrecioCD = CDec(IIf(IsDBNull(lector.Item("dc_PrecioCD")), 0.0, lector.Item("dc_PrecioCD")))
                    objDetallle.Descuento = CDec(IIf(IsDBNull(lector.Item("dc_Descuento")), 0.0, lector.Item("dc_Descuento")))
                    objDetallle.dc_Descuento1 = CDec(IIf(IsDBNull(lector.Item("dc_Descuento1")), 0.0, lector.Item("dc_Descuento1")))
                    objDetallle.dc_Descuento2 = CDec(IIf(IsDBNull(lector.Item("dc_Descuento2")), 0.0, lector.Item("dc_Descuento2")))
                    objDetallle.dc_Descuento3 = CDec(IIf(IsDBNull(lector.Item("dc_Descuento3")), 0.0, lector.Item("dc_Descuento3")))

                    Lista.Add(objDetallle)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function DetalleDocumentoViewSelectxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumentoView)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DetalleDocumentoViewSelectxIdDocumentoOC", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.CommandTimeout = 0
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleDocumentoView)
                Do While lector.Read
                    Dim obj As New Entidades.DetalleDocumentoView
                    With obj
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .IdDetalleDocumento = CInt(IIf(IsDBNull(lector.Item("IdDetalleDocumento")) = True, 0, lector.Item("IdDetalleDocumento")))
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("prod_Codigo")), "", lector.Item("prod_Codigo")))
                        .Cantidad = CDec(IIf(IsDBNull(lector.Item("dc_Cantidad")) = True, 0, lector.Item("dc_Cantidad")))
                        .PrecioSD = CDec(IIf(IsDBNull(lector.Item("dc_PrecioSD")) = True, 0, lector.Item("dc_PrecioSD")))
                        .PrecioCD = CDec(IIf(IsDBNull(lector.Item("dc_PrecioCD")) = True, 0, lector.Item("dc_PrecioCD")))
                        .Importe = CDec(IIf(IsDBNull(lector.Item("dc_Importe")) = True, 0, lector.Item("dc_Importe")))
                        .Descripcion = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                        .IdUMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .NomUMPrincipal = CStr(IIf(IsDBNull(lector.Item("dc_UMedida")) = True, "", lector.Item("dc_UMedida")))
                        ' uso Detraccion  solo para mostrar el precio de compra de la tabla producto
                        .Detraccion = CDec(IIf(IsDBNull(lector.Item("PrecioCompra")) = True, 0, lector.Item("PrecioCompra")))
                        .Descuento = CDec(IIf(IsDBNull(lector.Item("dc_Descuento")) = True, 0, lector.Item("dc_Descuento")))
                        .StockDisponible = CDec(IIf(IsDBNull(lector.Item("StockDisponible")) = True, 0, lector.Item("StockDisponible")))
                        .Percepcion = CDec(IIf(IsDBNull(lector.Item("dc_TasaPercepcion")) = True, 0, lector.Item("dc_TasaPercepcion")))
                        Try : .Pdscto = (.Descuento / .PrecioSD) * 100 : Catch ex As Exception : End Try
                        .dc_Descuento1 = CDec(IIf(IsDBNull(lector("dc_descuento1")), 0, lector("dc_descuento1")))
                        .dc_Descuento2 = CDec(IIf(IsDBNull(lector("dc_descuento2")), 0, lector("dc_descuento2")))
                        .dc_Descuento3 = CDec(IIf(IsDBNull(lector("dc_descuento3")), 0, lector("dc_descuento3")))
                        .Kit = CBool(IIf(IsDBNull(lector.Item("prod_Kit")) = True, 0, lector.Item("prod_Kit")))
                        .dc_Costo_Imp = CDec(IIf(IsDBNull(lector("dc_Costo_Imp")), 0, lector("dc_Costo_Imp")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub DeleteDetallexID(ByVal iddocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Dim cmd As New SqlCommand("_deleteDetallexID", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@iddocumento", iddocumento)
        Try
            Using cmd
                If cmd.ExecuteNonQuery = 0 Then
                    Throw New Exception("Fracaso la Operaccion")
                End If
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function UpdateDocumento(ByVal objDocumento As Entidades.Documento, ByVal listaDetalle As List(Of Entidades.DetalleDocumento), _
                                    ByVal objObs As Entidades.Observacion, ByVal objMonto As Entidades.MontoRegimen, _
                                    ByVal listaCondicionComercial As List(Of Entidades.CondicionComercial)) As Boolean

        Dim ObjDaoDetalleDocumento As New DAO.DAODetalleDocumento

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        cn.Open()
        Dim tr As SqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)
        Try


            Anular(objDocumento.Id, True, True, True, True, True, True, False, cn, tr)

            Me.ActualizarOrdenCompra(cn, objDocumento, tr)

            Dim ObjDAOAnexo_DetalleDocumento As New DAOAnexo_DetalleDocumento

            For i As Integer = 0 To listaDetalle.Count - 1
                With listaDetalle.Item(i)
                    .IdDocumento = objDocumento.Id
                    Dim iddetalle As Integer = ObjDaoDetalleDocumento.InsertaDetalleDocumento(cn, listaDetalle(i), tr)

                    Dim objAnexo_DetalleDoc As New Entidades.Anexo_DetalleDocumento
                    With objAnexo_DetalleDoc
                        .IdDocumento = listaDetalle(i).IdDocumento
                        .IdKit = listaDetalle(i).IdKit
                        .ComponenteKit = listaDetalle(i).Kit
                        .IdDetalleDocumento = iddetalle
                    End With

                    ObjDAOAnexo_DetalleDocumento._Anexo_DetalleDocumentoInsert(objAnexo_DetalleDoc, cn, tr)



                End With
            Next

            updatePrecioCompraProducto(objDocumento.IdMoneda, listaDetalle, cn, tr)

            If objDocumento.CompPercepcion = True And objMonto.Monto > 0 Then
                Dim objregimen As New DAO.DAOMontoRegimen
                objMonto.IdDocumento = objDocumento.Id
                objregimen.InsertaMontoRegimen(cn, objMonto, tr)
            End If

            Dim objPuntoLLegada As New Entidades.PuntoLlegada
            Dim objDaoPuntoLLegada As New DAO.DAOPuntoLlegada

            With objPuntoLLegada
                .IdDocumento = objDocumento.Id
                .IdTienda = objDocumento.IdTienda
                .IdAlmacen = objDocumento.LugarEntrega
            End With

            objDaoPuntoLLegada.InsertaPuntoLlegadaT(cn, objPuntoLLegada, tr)

            For i As Integer = 0 To listaCondicionComercial.Count - 1

                listaCondicionComercial.Item(i).IdDocumento = objDocumento.Id
                listaCondicionComercial.Item(i).IdTipoDocumento = objDocumento.IdTipoDocumento

                objDaoDCC = New DAODocumento_CondicionC
                objDaoDCC.Documento_CondicionC_Insert(cn, tr, listaCondicionComercial(i))

            Next

            '******************** OBSERVACIONES
            If (objObs IsNot Nothing) Then
                Dim objObservacion As New DAO.DAOObservacion

                objObs.IdDocumento = objDocumento.Id
                objObservacion.InsertaObservacionT(cn, tr, objObs)

            End If

            tr.Commit()
            Return True

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Sub ActualizarOrdenCompra(ByVal cn As SqlConnection, ByVal obj As Entidades.Documento, ByVal tr As SqlTransaction)

        Dim parametro() As SqlParameter = New SqlParameter(25) {}

        parametro(0) = New SqlParameter("@doc_FechaEmision", SqlDbType.DateTime)
        parametro(0).Value = IIf(obj.FechaEmision = Nothing, DBNull.Value, obj.FechaEmision)

        parametro(1) = New SqlParameter("@doc_FechaAentregar", SqlDbType.DateTime)
        parametro(1).Value = IIf(obj.FechaAEntregar = Nothing, DBNull.Value, obj.FechaAEntregar)

        parametro(2) = New SqlParameter("@doc_FechaVenc", SqlDbType.DateTime)
        parametro(2).Value = IIf(obj.FechaVenc = Nothing, DBNull.Value, obj.FechaVenc)

        parametro(3) = New SqlParameter("@doc_Descuento", SqlDbType.Decimal)
        parametro(3).Value = IIf(obj.Descuento = Nothing, DBNull.Value, obj.Descuento)

        parametro(4) = New SqlParameter("@doc_SubTotal", SqlDbType.Decimal)
        parametro(4).Value = IIf(obj.SubTotal = Nothing, DBNull.Value, obj.SubTotal)

        parametro(5) = New SqlParameter("@doc_Igv", SqlDbType.Decimal)
        parametro(5).Value = IIf(obj.IGV = Nothing, DBNull.Value, obj.IGV)

        parametro(6) = New SqlParameter("@doc_Total", SqlDbType.Decimal)
        parametro(6).Value = IIf(obj.Total = Nothing, DBNull.Value, obj.Total)

        parametro(7) = New SqlParameter("@doc_TotalLetras", SqlDbType.Xml)
        parametro(7).Value = IIf(obj.TotalLetras = Nothing, DBNull.Value, obj.TotalLetras)

        parametro(8) = New SqlParameter("@doc_TotalAPagar", SqlDbType.Decimal)
        parametro(8).Value = IIf(obj.TotalAPagar = Nothing, DBNull.Value, obj.TotalAPagar)

        parametro(9) = New SqlParameter("@IdPersona", SqlDbType.Int) '** Proveedor
        parametro(9).Value = IIf(obj.IdPersona = Nothing, DBNull.Value, obj.IdPersona)

        parametro(10) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        parametro(10).Value = IIf(obj.IdUsuario = Nothing, DBNull.Value, obj.IdUsuario)

        parametro(11) = New SqlParameter("@IdDestinatario", SqlDbType.Int)
        parametro(11).Value = IIf(obj.IdDestinatario = Nothing, DBNull.Value, obj.IdDestinatario)

        parametro(12) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        parametro(12).Value = IIf(obj.IdMoneda = Nothing, DBNull.Value, obj.IdMoneda)

        parametro(13) = New SqlParameter("@LugarEntrega", SqlDbType.Int)
        parametro(13).Value = IIf(obj.LugarEntrega = Nothing, DBNull.Value, obj.LugarEntrega)

        parametro(14) = New SqlParameter("@IdTienda", SqlDbType.Int)
        parametro(14).Value = IIf(obj.IdTienda = Nothing, DBNull.Value, obj.IdTienda)

        parametro(15) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        parametro(15).Value = IIf(obj.IdEmpresa = Nothing, DBNull.Value, obj.IdEmpresa)

        parametro(16) = New SqlParameter("@doc_Importacion", SqlDbType.Bit)
        parametro(16).Value = IIf(obj.doc_Importacion.ToString = "", DBNull.Value, CBool(obj.doc_Importacion))

        parametro(17) = New SqlParameter("@IdRemitente", SqlDbType.Int)
        parametro(17).Value = IIf(obj.IdRemitente = Nothing, DBNull.Value, obj.IdRemitente)

        parametro(18) = New SqlParameter("@IdArea", SqlDbType.Int)
        parametro(18).Value = IIf(obj.IdArea = Nothing, DBNull.Value, obj.IdArea)

        parametro(19) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        parametro(19).Value = IIf(obj.Id = Nothing, DBNull.Value, obj.Id)

        parametro(20) = New SqlParameter("@anex_PrecImportacion", SqlDbType.Int)
        parametro(20).Value = IIf(obj.anex_PrecImportacion = Nothing, DBNull.Value, obj.anex_PrecImportacion)

        parametro(21) = New SqlParameter("@doc_FechaCancelacion", SqlDbType.Date)
        parametro(21).Value = IIf(obj.FechaCancelacion = Nothing, DBNull.Value, obj.FechaCancelacion)

        parametro(22) = New SqlParameter("@IdCondicionPago", SqlDbType.Int)
        parametro(22).Value = IIf(obj.IdCondicionPago = Nothing, DBNull.Value, obj.IdCondicionPago)

        parametro(23) = New SqlParameter("@idcentrocosto", SqlDbType.VarChar)
        parametro(23).Value = IIf(obj.Direccion = Nothing, DBNull.Value, obj.Direccion)

        parametro(24) = New SqlParameter("@enviarCuentaPorPagar", SqlDbType.Bit)
        parametro(24).Value = IIf(obj.EnviarTablaCuenta = Nothing, DBNull.Value, obj.EnviarTablaCuenta)

        parametro(25) = New SqlParameter("@anex_igv", SqlDbType.Bit)
        parametro(25).Value = obj.anex_igv

        Dim cmd As New SqlCommand("_ActualizarOrdenCompra", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametro)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub updatePrecioCompraProducto(ByVal idmoneda As Integer, ByVal lista As List(Of Entidades.DetalleDocumento), ByVal cnx As SqlConnection, ByVal trx As SqlTransaction)
        Dim cmdx As SqlCommand
        For Each obj As Entidades.DetalleDocumento In lista
            cmdx = New SqlCommand("_updatePrecioCompraProducto", cnx, trx)
            cmdx.CommandType = CommandType.StoredProcedure
            cmdx.Parameters.AddWithValue("@idproducto", obj.IdProducto)
            cmdx.Parameters.AddWithValue("@precio", obj.CostoMovIngreso)
            cmdx.Parameters.AddWithValue("@idmoneda", idmoneda)

            cmdx.ExecuteNonQuery()
        Next
    End Sub

#End Region

#Region "Reportes"
    Public Function ComprasxProveedor(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idpersona As Integer, ByVal fechaInicio As String, ByVal fechafin As String, ByVal tipo As Integer, ByVal linea As Integer, ByVal sublinea As Integer) As DataSet

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim daD As SqlDataAdapter
        Dim ds As New DataSet
        Try
            If (tipo = 1) Then
                Dim cmdD As New SqlCommand("[_CR_detalladoComxProveyVtasxUsuario]", cn)
                cmdD.CommandType = CommandType.StoredProcedure
                cmdD.CommandTimeout = 0
                cmdD.Parameters.AddWithValue("@Idempresa", idempresa)
                cmdD.Parameters.AddWithValue("@idTienda", idtienda)
                cmdD.Parameters.AddWithValue("@idpersona", idpersona)
                cmdD.Parameters.AddWithValue("@idusuario", 0)
                cmdD.Parameters.AddWithValue("@fechainicio", fechaInicio)
                cmdD.Parameters.AddWithValue("@fechafin", fechafin)
                cmdD.Parameters.AddWithValue("@idlinea", linea)
                cmdD.Parameters.AddWithValue("@idsublinea", sublinea)

                'solo orden de Compras
                cmdD.Parameters.AddWithValue("@idtipodocumento", 16)

                daD = New SqlDataAdapter(cmdD)
                daD.Fill(ds, "DT_DetalleVtasCpras")
            Else
                Dim cmdD As New SqlCommand("[_CR_ResumenComxProveedor]", cn)
                cmdD.CommandType = CommandType.StoredProcedure
                cmdD.Parameters.AddWithValue("@Idempresa", idempresa)
                cmdD.Parameters.AddWithValue("@idTienda", idtienda)
                cmdD.Parameters.AddWithValue("@idpersona", idpersona)
                cmdD.Parameters.AddWithValue("@fechainicio", fechaInicio)
                cmdD.Parameters.AddWithValue("@fechafin", fechafin)
                cmdD.Parameters.AddWithValue("@idtipodocumento", 16)
                cmdD.Parameters.AddWithValue("@idlinea", linea)
                cmdD.Parameters.AddWithValue("@idsublinea", sublinea)


                daD = New SqlDataAdapter(cmdD)
                daD.Fill(ds, "DT_ResumidoVentasxVendedor")

            End If

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function

    Public Function ventasxVendedor(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idpersona As Integer, ByVal fechaInicio As String, ByVal fechafin As String, ByVal tipo As Integer, ByVal linea As Integer, ByVal sublinea As Integer) As DataSet
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim daD As SqlDataAdapter
        Dim ds As New DataSet
        Try
            If (tipo = 1) Then
                Dim cmdD As New SqlCommand("[_CR_detalladoComxProveyVtasxUsuario]", cn)
                cmdD.CommandType = CommandType.StoredProcedure
                cmdD.Parameters.AddWithValue("@Idempresa", idempresa)
                cmdD.Parameters.AddWithValue("@idTienda", idtienda)
                cmdD.Parameters.AddWithValue("@idpersona", 0)
                cmdD.Parameters.AddWithValue("@idusuario", idpersona)
                cmdD.Parameters.AddWithValue("@fechainicio", fechaInicio)
                cmdD.Parameters.AddWithValue("@fechafin", fechafin)
                cmdD.Parameters.AddWithValue("@idlinea", linea)
                cmdD.Parameters.AddWithValue("@idsublinea", sublinea)
                'para cond fac y bol
                cmdD.Parameters.AddWithValue("@idtipodocumento", 0)

                daD = New SqlDataAdapter(cmdD)
                daD.Fill(ds, "DT_DetalleVtasCpras")
            Else
                Dim cmdD As New SqlCommand("[_CR_ResumenVtasxUsu]", cn)
                cmdD.CommandType = CommandType.StoredProcedure
                cmdD.Parameters.AddWithValue("@Idempresa", idempresa)
                cmdD.Parameters.AddWithValue("@idTienda", idtienda)
                cmdD.Parameters.AddWithValue("@idusuario", idpersona)
                cmdD.Parameters.AddWithValue("@fechainicio", fechaInicio)
                cmdD.Parameters.AddWithValue("@fechafin", fechafin)
                cmdD.Parameters.AddWithValue("@idtipodocumento", 888)
                cmdD.Parameters.AddWithValue("@idlinea", linea)
                cmdD.Parameters.AddWithValue("@idsublinea", sublinea)


                daD = New SqlDataAdapter(cmdD)
                daD.Fill(ds, "DT_ResumidoVentasxVendedor")

            End If

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds

    End Function

    Public Function RankingProvedores(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idCondicionpago As Integer, ByVal idmoneda As Integer, ByVal idTipopersona As Integer, ByVal fechaInicio As String, ByVal fechafin As String, ByVal nprimeros As Integer, ByVal idlinea As Integer, ByVal idsublinea As Integer) As DataSet
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim daD As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmdD As New SqlCommand("[_CR_RankingProvedores]", cn)
            cmdD.CommandType = CommandType.StoredProcedure
            cmdD.Parameters.AddWithValue("@Idempresa", idempresa)
            cmdD.Parameters.AddWithValue("@idtienda", idtienda)
            cmdD.Parameters.AddWithValue("@idCondicionpago", idCondicionpago)
            cmdD.Parameters.AddWithValue("@idmoneda", idmoneda)
            cmdD.Parameters.AddWithValue("@idTipopersona", idTipopersona)
            cmdD.Parameters.AddWithValue("@fechainicio", fechaInicio)
            cmdD.Parameters.AddWithValue("@fechafin", fechafin)
            cmdD.Parameters.AddWithValue("@filas", nprimeros)
            cmdD.Parameters.AddWithValue("@idlinea", idlinea)
            cmdD.Parameters.AddWithValue("@idsublinea", idsublinea)

            daD = New SqlDataAdapter(cmdD)
            daD.Fill(ds, "DT_RankingProveedores")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function

    Public Function getDataSetOrdenCompra(ByVal iddocumento As Integer) As DataSet
        Dim da_C As SqlDataAdapter
        Dim da_D As SqlDataAdapter
        Dim da_CC As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_SelectDocCompraCabecera", ObjConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)
            da_C = New SqlDataAdapter(cmd)
            da_C.Fill(ds, "CR_dtCabecera")

            cmd.CommandText = "_CR_OrdenCompraDet"
            da_D = New SqlDataAdapter(cmd)
            da_C.Fill(ds, "CR_dtDetalle")

            cmd.CommandText = "_CRCondicionComercialxIdDocumento"
            da_CC = New SqlDataAdapter(cmd)
            da_CC.Fill(ds, "CR_CondicionComercial")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function

    Public Function getDataSetComparativo(ByVal year As Integer, _
                             ByVal columna As String, ByVal idproveedor As Integer, _
                             ByVal idmoneda As Integer, ByVal idpropietario As Integer) As DataSet
        Dim parametro() As SqlParameter = New SqlParameter(4) {}
        parametro(0) = New SqlParameter("@year", SqlDbType.Int)
        parametro(0).Value = IIf(year = Nothing, DBNull.Value, year)
        parametro(1) = New SqlParameter("@variable", SqlDbType.VarChar)
        parametro(1).Value = IIf(columna = Nothing, DBNull.Value, columna)
        parametro(2) = New SqlParameter("@idproveedor", SqlDbType.Int)
        parametro(2).Value = IIf(idproveedor = Nothing, DBNull.Value, idproveedor)
        parametro(3) = New SqlParameter("@idMoneda", SqlDbType.Int)
        parametro(3).Value = IIf(idmoneda = Nothing, DBNull.Value, idmoneda)
        parametro(4) = New SqlParameter("@idpropietario", SqlDbType.Int)
        parametro(4).Value = IIf(idpropietario = Nothing, DBNull.Value, idpropietario)
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        Try
            Dim cmd As New SqlCommand("_CR_ComparativoCompras", ObjConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(parametro)
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_comparativo")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function

    Public Function getReporteVentasStock(ByVal idEmpresa As Integer, ByVal idtienda As Integer, ByVal idlinea As Integer, ByVal idSublinea As Integer, _
                                          ByVal fechainicio As String, ByVal fechafin As String, ByVal yeari As Integer, ByVal semanai As Integer, _
                                          ByVal yearf As Integer, ByVal semanaf As Integer, ByVal filtrarsemana As Integer, _
                                          ByVal ltProd As List(Of Entidades.ProductoView), _
                                          ByVal ltTAlmacen As List(Of Entidades.TipoAlmacen), ByVal stockxtienda As Short) As DataSet

        Dim dt As New DataTable
        dt.Columns.Add("idproducto", GetType(System.Int64))

        If ltProd IsNot Nothing Then
            For Each objprod As Entidades.ProductoView In ltProd
                Dim dr As DataRow = dt.NewRow()
                dr(0) = objprod.Id
                dt.Rows.Add(dr)
            Next
        End If


        Dim tb As New DataTable
        tb.Columns.Add("idproducto", GetType(System.Int64))

        For Each objtAlmacen As Entidades.TipoAlmacen In ltTAlmacen
            Dim dr As DataRow = tb.NewRow()
            dr(0) = objtAlmacen.IdTipoAlmacen
            tb.Rows.Add(dr)
        Next

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        Try
            Dim cmdD As New SqlCommand("[_CR_ComprasStock_Analisis]", cn)
            cmdD.CommandType = CommandType.StoredProcedure
            cmdD.Parameters.AddWithValue("@idEmpresa", idEmpresa)
            cmdD.Parameters.AddWithValue("@idtienda", idtienda)
            cmdD.Parameters.AddWithValue("@fechainicio", fechainicio)
            cmdD.Parameters.AddWithValue("@fechafin", fechafin)
            cmdD.Parameters.AddWithValue("@yeari", yeari)
            cmdD.Parameters.AddWithValue("@semanai", semanai)
            cmdD.Parameters.AddWithValue("@yearf", yearf)
            cmdD.Parameters.AddWithValue("@semanaf", semanaf)
            cmdD.Parameters.AddWithValue("@filtrarsemana", filtrarsemana)
            cmdD.Parameters.AddWithValue("@StockTransitoxtienda", stockxtienda)
            cmdD.Parameters.AddWithValue("@ParProducto", dt)
            cmdD.Parameters.AddWithValue("@tbTipoAlmacen", tb)
            cmdD.Parameters.AddWithValue("@idlinea", idlinea)
            cmdD.Parameters.AddWithValue("@idSublinea", idSublinea)

            da = New SqlDataAdapter(cmdD)
            da.Fill(ds, "_DT_VentasStock")

        Catch ex As Exception
            ds = Nothing
        End Try

        Return ds

    End Function

#End Region

End Class
