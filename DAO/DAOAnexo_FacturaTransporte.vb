﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'**********************   VIERNES 25 06 2010
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOAnexo_FacturaTransporte

    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private objDaoMantenedor As New DAO.DAOMantenedor
    Private tr As SqlTransaction
    Private cmd As SqlCommand
    Private reader As SqlDataReader

    Public Function registrar(ByVal objAnexo_FacturaTransporte As Entidades.Anexo_FacturaTransporte, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer

        Dim p() As SqlParameter = New SqlParameter(5) {}
        p(0) = objDaoMantenedor.getParam(objAnexo_FacturaTransporte.IdDocumento, "@IdDocumento", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(objAnexo_FacturaTransporte.IdMoneda, "@IdMoneda", SqlDbType.Int)
        p(2) = objDaoMantenedor.getParam(objAnexo_FacturaTransporte.IdValorReferencial, "@IdValorReferencial", SqlDbType.Int)
        p(3) = objDaoMantenedor.getParam(objAnexo_FacturaTransporte.ValorReferencial_Unit, "@aft_ValorReferencial_Unit", SqlDbType.Decimal)
        p(4) = objDaoMantenedor.getParam(objAnexo_FacturaTransporte.Cantidad, "@aft_Cantidad", SqlDbType.Decimal)
        p(5) = objDaoMantenedor.getParam(objAnexo_FacturaTransporte.ValorReferencia_Total, "@aft_ValorReferencia_Total", SqlDbType.Decimal)

        Return CInt(SqlHelper.ExecuteScalar(tr, CommandType.StoredProcedure, "_Anexo_FacturaTransporte_Registrar", p))

    End Function

    Public Function SelectxIdDocumento(ByVal IdDocumento As Integer) As Entidades.Anexo_FacturaTransporte

        Dim objAnexo_FacturaTransporte As Entidades.Anexo_FacturaTransporte = Nothing

        Try

            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = objDaoMantenedor.getParam(IdDocumento, "@IdDocumento", SqlDbType.Int)

            cn.Open()
            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_Anexo_FacturaTransporte_SelectxIdDocumento", p)
            If (reader.Read) Then

                objAnexo_FacturaTransporte = New Entidades.Anexo_FacturaTransporte
                With objAnexo_FacturaTransporte

                    .IdDocumento = objDaoMantenedor.UCInt(reader("IdDocumento"))
                    .IdMoneda = objDaoMantenedor.UCInt(reader("IdMoneda"))
                    .IdValorReferencial = objDaoMantenedor.UCInt(reader("IdValorReferencial"))
                    .ValorReferencial_Unit = objDaoMantenedor.UCDec(reader("aft_ValorReferencial_Unit"))
                    .Cantidad = objDaoMantenedor.UCDec(reader("aft_Cantidad"))
                    .ValorReferencia_Total = objDaoMantenedor.UCDec(reader("aft_ValorReferencia_Total"))

                End With

            End If
            reader.Close()


        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objAnexo_FacturaTransporte

    End Function

End Class
