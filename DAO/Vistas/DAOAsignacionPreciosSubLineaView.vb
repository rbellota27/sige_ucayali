﻿Imports System.Data.SqlClient
Public Class DAOAsignacionPreciosSubLineaView
    Private objConexion As New Conexion
    Public Function SelectxIdSubLinea(ByVal IdSubLinea As Integer) As List(Of Entidades.AsignacionPreciosSubLineaView)
        'FUNCION UTILIZADA PARA EL REGISTRO DE UN NUEVO PRECIO DE VENTA POR SUBLINEA
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AsignacionPreciosSubLineaSelectxIdSubLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.AsignacionPreciosSubLineaView)
                Do While lector.Read
                    Dim obj As New Entidades.AsignacionPreciosSubLineaView
                    obj.IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                    obj.nomProducto = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                    obj.IdUMPrincipal = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                    obj.nomUMPrincipal = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                    obj.PrecioCompra = CDec(IIf(IsDBNull(lector.Item("prod_PrecioCompra")) = True, 0, lector.Item("prod_PrecioCompra")))
                    obj.escalarValor = CDec(IIf(IsDBNull(lector.Item("prod_PrecioCompra")) = True, 0, lector.Item("prod_PrecioCompra")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
