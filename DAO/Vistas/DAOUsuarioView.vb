﻿Imports System.Data.SqlClient
Public Class DAOUsuarioView
    Private objConexion As New DAO.Conexion
    Public Function SelectxIdPersona(ByVal IdPersona As Integer) As Entidades.UsuarioView
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim objUsuario As Entidades.UsuarioView = Nothing
        Try
            cn.Open()
            Dim cmd As New SqlCommand("_UsuarioViewSelectxIdPersona", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If lector.Read Then
                objUsuario = New Entidades.UsuarioView
                With objUsuario
                    .DNI = CStr(IIf(IsDBNull(lector.Item("DNI")) = True, "", lector.Item("DNI")))
                    .Estado = CStr(IIf(IsDBNull(lector.Item("us_Estado")) = True, "", lector.Item("us_Estado")))
                    .fechaAlta = CDate(IIf(IsDBNull(lector.Item("us_FechaAlta")) = True, Nothing, lector.Item("us_FechaAlta")))
                    .fechaBaja = CDate(IIf(IsDBNull(lector.Item("us_FechaBaja")) = True, Nothing, lector.Item("us_FechaBaja")))
                    .IdMotivoBaja = CInt(IIf(IsDBNull(lector.Item("IdMotivoBaja")) = True, 0, lector.Item("IdMotivoBaja")))
                    .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    .Login = CStr(IIf(IsDBNull(lector.Item("us_Login")) = True, "", lector.Item("us_Login")))
                    .Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                    .Clave = CStr(IIf(IsDBNull(lector.Item("us_Clave")) = True, "", lector.Item("us_Clave")))
                    .FlagAtencionPago = CInt(IIf(IsDBNull(lector.Item("flagAtencionPago")) = True, 0, lector.Item("flagAtencionPago")))
                End With
            End If            
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return objUsuario
    End Function


    Public Function SelectxEstadoxNombre_IdPerfil(ByVal nombre As String, ByVal estado As String, ByVal IdPerfil As Integer) As List(Of Entidades.UsuarioView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim lista As New List(Of Entidades.UsuarioView)
        Try
            cn.Open()
            Dim cmd As New SqlCommand("_UsuarioViewSelectxEstadoxNombre_IdPerfil", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Nombre", nombre)
            cmd.Parameters.AddWithValue("@Estado", estado)
            cmd.Parameters.AddWithValue("@IdPerfil", IdPerfil)


            Dim lector As SqlDataReader = cmd.ExecuteReader
            While lector.Read
                Dim obj As New Entidades.UsuarioView
                obj.DNI = CStr(IIf(IsDBNull(lector.Item("DNI")) = True, "", lector.Item("DNI")))
                obj.Estado = CStr(IIf(IsDBNull(lector.Item("us_Estado")) = True, "", lector.Item("us_Estado")))
                obj.fechaAlta = CDate(IIf(IsDBNull(lector.Item("us_FechaAlta")) = True, Nothing, lector.Item("us_FechaAlta")))
                obj.fechaBaja = CDate(IIf(IsDBNull(lector.Item("us_FechaBaja")) = True, Nothing, lector.Item("us_FechaBaja")))
                obj.IdMotivoBaja = CInt(IIf(IsDBNull(lector.Item("IdMotivoBaja")) = True, 0, lector.Item("IdMotivoBaja")))
                obj.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                obj.Login = CStr(IIf(IsDBNull(lector.Item("us_Login")) = True, "", lector.Item("us_Login")))
                obj.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                lista.Add(obj)
            End While
            lector.Close()
            cn.Close()
        Catch ex As Exception
        Finally

        End Try
        Return lista
    End Function



    Public Function SelectxEstadoxNombre(ByVal nombre As String, ByVal estado As String) As List(Of Entidades.UsuarioView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim lista As New List(Of Entidades.UsuarioView)
        Try
            cn.Open()
            Dim cmd As New SqlCommand("_UsuarioViewSelectxEstadoxNombre", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Nombre", nombre)
            cmd.Parameters.AddWithValue("@Estado", estado)

            Dim lector As SqlDataReader = cmd.ExecuteReader
            While lector.Read
                Dim obj As New Entidades.UsuarioView
                obj.DNI = CStr(IIf(IsDBNull(lector.Item("DNI")) = True, "", lector.Item("DNI")))
                obj.Estado = CStr(IIf(IsDBNull(lector.Item("us_Estado")) = True, "", lector.Item("us_Estado")))
                obj.fechaAlta = CDate(IIf(IsDBNull(lector.Item("us_FechaAlta")) = True, Nothing, lector.Item("us_FechaAlta")))
                obj.fechaBaja = CDate(IIf(IsDBNull(lector.Item("us_FechaBaja")) = True, Nothing, lector.Item("us_FechaBaja")))
                obj.IdMotivoBaja = CInt(IIf(IsDBNull(lector.Item("IdMotivoBaja")) = True, 0, lector.Item("IdMotivoBaja")))
                obj.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                obj.Login = CStr(IIf(IsDBNull(lector.Item("us_Login")) = True, "", lector.Item("us_Login")))
                obj.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                lista.Add(obj)
            End While
            lector.Close()
            cn.Close()
        Catch ex As Exception
        Finally

        End Try
        Return lista
    End Function

    Public Function SelectxPersonaView(ByVal nombre As String) As List(Of Entidades.UsuarioView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim lista As New List(Of Entidades.UsuarioView)
        Try
            cn.Open()
            Dim cmd As New SqlCommand("_PersonaView", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Nombre", nombre)

            Dim lector As SqlDataReader = cmd.ExecuteReader
            While lector.Read
                Dim obj As New Entidades.UsuarioView
                obj.DNI = CStr(IIf(IsDBNull(lector.Item("DNI")) = True, "", lector.Item("DNI")))
                obj.Estado = CStr(IIf(IsDBNull(lector.Item("per_estado")) = True, "", lector.Item("per_estado")))
                obj.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))                
                obj.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                lista.Add(obj)
            End While
            lector.Close()
            cn.Close()
        Catch ex As Exception
        Finally

        End Try
        Return lista
    End Function
    Public Function getDocumentosUsuario(ByVal IdEmpresa As Integer, ByVal Idtienda As Integer, ByVal fechainicio As String, ByVal fechafin As String) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_DocumentXUsuario", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
            cmd.Parameters.AddWithValue("@IdTienda", Idtienda)
            cmd.Parameters.AddWithValue("@fechainicio", fechainicio)
            cmd.Parameters.AddWithValue("@fechafin", fechafin)

            '*********** cargo la data
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_InvNOValorizado")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function
    Public Function FunVerTodasTiendas(ByVal idusuario As Integer, ByVal idpermiso As Integer) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim Cant As Integer
        Try
            cn.Open()
            Dim cmd As New SqlCommand("_CR_PermisoTodaTienda", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idusuario", idusuario)
            cmd.Parameters.AddWithValue("@idpermiso", idpermiso)
            Cant = CInt(cmd.ExecuteScalar())
            cn.Close()
        Catch ex As Exception
        Finally

        End Try
        Return Cant
    End Function
End Class
