﻿'08_febrero_2010
Imports System.Data.SqlClient
Public Class DAOCxCREsumen
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectGrillaCxCResumen(ByVal idEmpresa As Integer, ByVal idTienda As Integer) As List(Of Entidades.CxCResumenView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CR_CuentasxCobrarResumen", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idEmpresa", idEmpresa)
        cmd.Parameters.AddWithValue("@idtienda", idTienda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CxCResumenView)
                Do While lector.Read
                    Dim CxCD As New Entidades.CxCResumenView
                    CxCD.IdMoneda = CInt(IIf(IsDBNull(lector.Item("Idmoneda")) = True, 0, lector.Item("Idmoneda")))
                    CxCD.mon_simbolo = CStr(lector.Item("mon_Simbolo"))
                    CxCD.Subtotal = CDec(IIf(IsDBNull(lector.Item("monto")) = True, 0, lector.Item("monto")))
                    CxCD.tc_CompraC = CDec(IIf(IsDBNull(lector.Item("tipocambio")) = True, 0, lector.Item("tipocambio")))
                    CxCD.mon_base = CStr(IIf(IsDBNull(lector.Item("monedaBase")) = True, "", lector.Item("monedaBase")))
                    CxCD.TotalResumen = CDec(IIf(IsDBNull(lector.Item("TotalSoles")) = True, 0, lector.Item("TotalSoles")))
                    CxCD.TC_Cambio = CDec(IIf(IsDBNull(lector.Item("Tc_CambioDol")) = True, 0, lector.Item("Tc_CambioDol")))

                    Lista.Add(CxCD)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectGrillaCxCResumenCliente(ByVal idEmpresa As Integer, ByVal idtienda As Integer, ByVal idPersona As Integer) As List(Of Entidades.CxCResumenView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("[_CR_CuentasxCobrarResumenClienteM]", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idEmpresa", idEmpresa)
        cmd.Parameters.AddWithValue("@idPersona", idPersona)
        cmd.Parameters.AddWithValue("@idtienda", idtienda)
        'cmd.Parameters.AddWithValue("@nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CxCResumenView)
                Do While lector.Read
                    Dim CxCD As New Entidades.CxCResumenView
                    CxCD.idpersona = CInt(lector.Item("IdPersona"))
                    CxCD.nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                    CxCD.RucDni = CDbl(lector.Item("RucDni"))
                    CxCD.SalDol = CDec(IIf(IsDBNull(lector.Item("SalDol")) = True, 0, lector.Item("SalDol")))
                    CxCD.SalSol = (CInt(IIf(IsDBNull(lector.Item("SalSol")) = True, 0, lector.Item("SalSol"))))

                    Lista.Add(CxCD)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectGrillaCxCResumenDocumento(ByVal Idempresa As Integer, ByVal idtienda As Integer, ByVal idmoneda As Integer, ByVal idpersona As Integer) As List(Of Entidades.CxCResumenView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CR_CuentasxCobrarResumenDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Idempresa", Idempresa)
        cmd.Parameters.AddWithValue("@idtienda", idtienda)
        cmd.Parameters.AddWithValue("@idPersona", idpersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CxCResumenView)
                Do While lector.Read
                    Dim CxCD As New Entidades.CxCResumenView
                    CxCD.idDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                    CxCD.tdoc_NombreCorto = CStr(IIf(IsDBNull(lector.Item("Documento")) = True, "", lector.Item("Documento")))
                    CxCD.doc_FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                    CxCD.doc_FechaVencimiento = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                    CxCD.nroDias = CInt(IIf(IsDBNull(lector.Item("diasVencidos")) = True, 0, lector.Item("diasVencidos")))
                    CxCD.mon_simbolo = CStr(lector.Item("moneda"))
                    CxCD.doc_TotalPagar = CDec(IIf(IsDBNull(lector.Item("mcu_Monto")) = True, Nothing, lector.Item("mcu_Monto")))
                    CxCD.abono = CDec(IIf(IsDBNull(lector.Item("TotalAbono")) = True, 0, lector.Item("TotalAbono")))
                    CxCD.Saldo = CDec(IIf(IsDBNull(lector.Item("mcu_Saldo")) = True, 0, lector.Item("mcu_Saldo")))
                    CxCD.Numero = CStr(IIf(IsDBNull(lector.Item("numero")) = True, "", lector.Item("numero")))
                    Lista.Add(CxCD)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetDetalleAbonos(ByVal iddocumento As Integer) As List(Of Entidades.CxCResumenView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CR_GetDetalleAbonoxDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Iddocumento", iddocumento)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CxCResumenView)
                Do While lector.Read
                    Dim CxCD As New Entidades.CxCResumenView
                    CxCD.tdoc_NombreCorto = CStr(IIf(IsDBNull(lector.Item("Documento")) = True, "", lector.Item("Documento")))
                    CxCD.doc_FechaEmision = CDate(IIf(IsDBNull(lector.Item("mcu_Fecha")) = True, Nothing, lector.Item("mcu_Fecha")))
                    CxCD.mon_simbolo = CStr(IIf(IsDBNull(lector.Item("Moneda")) = True, "", lector.Item("Moneda")))
                    CxCD.doc_TotalPagar = CDec(IIf(IsDBNull(lector.Item("mcu_Monto")) = True, Nothing, lector.Item("mcu_Monto")))
                    CxCD.abono = CDec(IIf(IsDBNull(lector.Item("Abonos")) = True, 0, lector.Item("Abonos")))
                    CxCD.Saldo = CDec(IIf(IsDBNull(lector.Item("mcu_Saldo")) = True, 0, lector.Item("mcu_Saldo")))
                    CxCD.Numero = CStr(IIf(IsDBNull(lector.Item("numero")) = True, "", lector.Item("numero")))
                    CxCD.CuentaTipo = CStr(IIf(IsDBNull(lector.Item("CuentaTipo")) = True, "", lector.Item("CuentaTipo")))

                    Lista.Add(CxCD)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function getInventarioValorizado(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("[_CR_InventarioValorizado]", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
            cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
            cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
            cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)

            '*********** cargo la data
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_InvetarioValorizado")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function
    Public Function getGuiaRecepcionOrdendespacho(ByVal idempresa As Integer, ByVal idalmacen As Integer, ByVal idtipodocumento As Integer, ByVal fechainicio As String, ByVal fechafin As String) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("[_CR_DetalleGuiaRecepcionODespacho]", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdEmpresa", idempresa)
            cmd.Parameters.AddWithValue("@IdAlmacen", idalmacen)
            cmd.Parameters.AddWithValue("@idtipoDocumento", idtipodocumento)
            cmd.Parameters.AddWithValue("@Fechainicio", fechainicio)
            cmd.Parameters.AddWithValue("@fechafin", fechafin)

            '*********** cargo la data
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DetalleGuiaRecepcionDespacho")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function
    Public Function getRankingProductos(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal nprimeros As Integer, ByVal fechainicio As String, ByVal fechafin As String, ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal IdVendedor As Integer) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("[_CR_RankingProductosXEmpresaxTienda]", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdEmpresa", idempresa)
            cmd.Parameters.AddWithValue("@IdTienda", idtienda)
            cmd.Parameters.AddWithValue("@filas", nprimeros)
            cmd.Parameters.AddWithValue("@Fechainicio", fechainicio)
            cmd.Parameters.AddWithValue("@fechafin", fechafin)
            cmd.Parameters.AddWithValue("@idlinea", idlinea)
            cmd.Parameters.AddWithValue("@idsublinea", idsublinea)
            cmd.Parameters.AddWithValue("@IdVendedor", IdVendedor)

            '*********** cargo la data
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_RankingProductos")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds

    End Function
    Public Function getKardexValorizado(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdProducto As Integer, ByVal Signo As String, ByVal Cantidad As Decimal) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("[_CR_StockValorizado]", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
            cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
            cmd.Parameters.AddWithValue("@IdTipoExistencia", IdTipoExistencia)
            cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
            cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
            cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
            cmd.Parameters.AddWithValue("@Signo", Signo)
            cmd.Parameters.AddWithValue("@Cantidad", Cantidad)

            '*********** cargo la data
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "_CR_KardexValorizado")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function

    Public Function getUtilidadxProducto(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal fechainicio As String, ByVal fechafin As String, ByVal idlinea As Integer, ByVal idsublinea As Integer) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("[_CR_UtilidadVentasCompras]", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdEmpresa", idempresa)
            cmd.Parameters.AddWithValue("@IdTienda", idtienda)
            cmd.Parameters.AddWithValue("@Fechai", fechainicio)
            cmd.Parameters.AddWithValue("@fechaf", fechafin)
            cmd.Parameters.AddWithValue("@idlinea", idlinea)
            cmd.Parameters.AddWithValue("@idsublinea", idsublinea)

            '*********** cargo la data
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_UtilidadxProducto")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds

    End Function

End Class
