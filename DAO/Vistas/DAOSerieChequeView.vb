﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOSerieChequeView
    Inherits DAOSerieCheque

    Public Function SelectxParams(ByVal x As Entidades.SerieChequeView) As List(Of Entidades.SerieChequeView)
        Cn = objConexion.ConexionSIGE()
        Dim Prm() As SqlParameter
        Prm = GetVectorParametros(CType(x, Entidades.SerieCheque), modo_query.Show)
        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()

                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "_SerieChequeViewSelectxParams", Prm)

                Dim Lista As New List(Of Entidades.SerieChequeView)
                With lector
                    Do While .Read

                        Dim obj As New Entidades.SerieChequeView

                        obj.Id = UCInt(.Item("IdSerieCheque"))
                        obj.IdBanco = UCInt(.Item("IdBanco"))
                        obj.IdCuentaBancaria = UCInt(.Item("IdCuentaBancaria"))
                        obj.Serie = UCStr(.Item("sc_Serie"))
                        obj.NroInicio = UCInt(.Item("sc_NroInicio"))
                        obj.NroFin = UCInt(.Item("sc_NroFin"))
                        obj.Longitud = UCInt(.Item("sc_Longitud"))
                        obj.Estado = UCBool(.Item("sc_Estado"))
                        obj.Descripcion = UCStr(.Item("sc_Descripcion"))

                        obj.SaldoContable = UCDec(.Item("SaldoContable"))
                        obj.SaldoDisponible = UCDec(.Item("SaldoDisponible"))
                        obj.IdMoneda = UCInt(.Item("IdMoneda"))
                        obj.mon_Simbolo = UCStr(.Item("mon_Simbolo"))

                        obj.Banco = UCStr(.Item("Banco"))
                        obj.CuentaBancaria = UCStr(.Item("CuentaBancaria"))

                        Lista.Add(obj)

                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAll() As List(Of Entidades.SerieChequeView)

        Dim x As New Entidades.SerieChequeView

        Return SelectxParams(x)

    End Function
    Public Function SelectAllActivo() As List(Of Entidades.SerieChequeView)

        Dim x As New Entidades.SerieChequeView
        x.Estado = True
        Return SelectxParams(x)

    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.SerieChequeView)

        Dim x As New Entidades.SerieChequeView
        x.Estado = False
        Return SelectxParams(x)

    End Function
    Public Function SelectxId(ByVal ID As Integer) As Entidades.SerieChequeView

        Dim x As New Entidades.SerieChequeView
        x.Id = ID

        Return SelectxParams(x).Item(0)

    End Function

    Public Function SelectCboxIdBancoxIdCuentaBancaria(ByVal IdBanco As Integer, ByVal IdCuentaBancaria As Integer) As List(Of Entidades.SerieChequeView)

        If IdBanco <> 0 And IdCuentaBancaria <> 0 Then
            Dim x As New Entidades.SerieChequeView
            x.IdBanco = IdBanco
            x.IdCuentaBancaria = IdCuentaBancaria
            Return SelectxParams(x)
        Else
            Return Nothing
        End If
    End Function

    Public Function SelectCboxIdCuentaBancaria(ByVal IdCuentaBancaria As Integer) As List(Of Entidades.SerieChequeView)

        If IdCuentaBancaria <> 0 Then
            Dim x As New Entidades.SerieChequeView
            x.IdCuentaBancaria = IdCuentaBancaria
            Return SelectxParams(x)
        Else
            Return Nothing
        End If
    End Function

End Class
