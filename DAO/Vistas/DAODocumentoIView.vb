﻿Imports System.Data.SqlClient
Public Class DAODocumentoIView
    Dim objConexion As New Conexion
    Public Function SelectxIdPersona(ByVal IdPersona As Integer) As List(Of Entidades.DocumentoI)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoISelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DocumentoI)
                Do While lector.Read
                    Dim objDocumentoIView As New Entidades.DocumentoI
                    objDocumentoIView.IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumentoI")) = True, DBNull.Value, lector.Item("IdDocumentoI")))
                    objDocumentoIView.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, DBNull.Value, lector.Item("IdPersona")))
                    objDocumentoIView.IdTipoDocumentoI = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumentoI")) = True, DBNull.Value, lector.Item("IdTipoDocumentoI")))
                    objDocumentoIView.Numero = CStr(IIf(IsDBNull(lector.Item("doc_Numero")) = True, Nothing, lector.Item("doc_Numero")))
                    Lista.Add(objDocumentoIView)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
