﻿Imports System.Data.SqlClient
Public Class DAOConsultaDeudaCxC_View
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectGrillaCxCDeuda() As List(Of Entidades.ConsultaDeudaCxC_View)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CuentasxCobrarSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ConsultaDeudaCxC_View)
                Do While lector.Read
                    Dim CxCD As New Entidades.ConsultaDeudaCxC_View
                    CxCD.IdPersona = CInt(lector.Item("IdPersona"))
                    'CxCD.DocumentoI = CStr(IIf(IsDBNull(lector.Item("doc_Numero")) = True, "", lector.Item("doc_Numero")))
                    CxCD.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                    CxCD.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    CxCD.SimboloM = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                    CxCD.Saldo = CDec(IIf(IsDBNull(lector.Item("Saldo")) = True, 0, lector.Item("Saldo")))
                    'CxCD.getNombreParaMostrar = lector.Item("Nombre")
                    Lista.Add(CxCD)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
