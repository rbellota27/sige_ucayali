﻿Imports System.Data.SqlClient
Public Class DAONaturalView
    Private objConexion As New Conexion
    Public Function _NaturalViewSelectAll() As List(Of Entidades.NaturalView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_NaturalViewSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.NaturalView)
                Do While lector.Read
                    Dim obj As New Entidades.NaturalView
                    obj.DNI = CStr(IIf(IsDBNull(lector.Item("DNI")) = True, "", lector.Item("DNI")))
                    obj.FechaNac = CDate(IIf(IsDBNull(lector.Item("nat_FechaNac")) = True, Nothing, lector.Item("nat_FechaNac")))
                    obj.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                    obj.NombreCargo = CStr(IIf(IsDBNull(lector.Item("car_Nombre")) = True, "", lector.Item("car_Nombre")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function _NaturalViewSelectAllxNombre(ByVal nombre As String) As List(Of Entidades.NaturalView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_NaturalViewSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.NaturalView)
                Do While lector.Read
                    Dim obj As New Entidades.NaturalView
                    obj.DNI = CStr(IIf(IsDBNull(lector.Item("DNI")) = True, "", lector.Item("DNI")))
                    obj.FechaNac = CDate(IIf(IsDBNull(lector.Item("nat_FechaNac")) = True, Nothing, lector.Item("nat_FechaNac")))
                    obj.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                    obj.NombreCargo = CStr(IIf(IsDBNull(lector.Item("car_Nombre")) = True, "", lector.Item("car_Nombre")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As Entidades.NaturalView
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_NaturalViewSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim obj As New Entidades.NaturalView
                If lector.Read Then
                    obj.DNI = CStr(IIf(IsDBNull(lector.Item("DNI")) = True, "", lector.Item("DNI")))
                    obj.FechaNac = CDate(IIf(IsDBNull(lector.Item("nat_FechaNac")) = True, Nothing, lector.Item("nat_FechaNac")))
                    obj.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                    obj.NombreCargo = CStr(IIf(IsDBNull(lector.Item("car_Nombre")) = True, "", lector.Item("car_Nombre")))
                Else
                    Throw New Exception
                End If
                lector.Close()
                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxDNI(ByVal dni As String) As Entidades.NaturalView
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_NaturalViewSelectxDNI", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@dni", dni)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim obj As New Entidades.NaturalView
                If lector.Read Then
                    obj.DNI = CStr(IIf(IsDBNull(lector.Item("DNI")) = True, "", lector.Item("DNI")))
                    obj.FechaNac = CDate(IIf(IsDBNull(lector.Item("nat_FechaNac")) = True, Nothing, lector.Item("nat_FechaNac")))
                    obj.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                    obj.NombreCargo = CStr(IIf(IsDBNull(lector.Item("car_Nombre")) = True, "", lector.Item("car_Nombre")))
                Else
                    Throw New Exception
                End If
                lector.Close()
                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
