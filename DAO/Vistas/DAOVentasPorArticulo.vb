﻿''22_02_2010
Imports System.Data.SqlClient
Public Class DAOVentasPorArticulo
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function CRVtasProductoProm(ByVal idEmpresa As Integer, ByVal idtienda As Integer, ByVal idDocumento As Integer, ByVal ListaProducto As List(Of Entidades.ProductoView), ByVal fechainicio As String, ByVal fechafin As String, ByVal yeari As Integer, ByVal semanai As Integer, ByVal yearf As Integer, ByVal semanaf As Integer, ByVal filtrarsemana As Integer, ByVal idpersona As Integer) As DataSet
        '14
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try


            Dim dt As New DataTable
            dt.Columns.Add("idproducto", GetType(System.Int64))

            If ListaProducto IsNot Nothing Then
                For Each objprod As Entidades.ProductoView In ListaProducto
                    Dim dr As DataRow = dt.NewRow()
                    dr(0) = objprod.Id
                    dt.Rows.Add(dr)
                Next
            End If


            Dim cmdD As New SqlCommand("_CR_VentasProductosPromocion", cn)
            cmdD.CommandType = CommandType.StoredProcedure
            cmdD.Parameters.AddWithValue("@idEmpresa", idEmpresa)
            cmdD.Parameters.AddWithValue("@idtienda", idtienda)
            cmdD.Parameters.AddWithValue("@idDocumento", idDocumento)
            cmdD.Parameters.AddWithValue("@idcliente", idpersona)
            cmdD.Parameters.AddWithValue("@tbproducto", dt)
            cmdD.Parameters.AddWithValue("@fechainicio", fechainicio)
            cmdD.Parameters.AddWithValue("@fechafin", fechafin)

            cmdD.Parameters.AddWithValue("@yeari", yeari)
            cmdD.Parameters.AddWithValue("@semanai", semanai)
            cmdD.Parameters.AddWithValue("@yearf", yearf)
            cmdD.Parameters.AddWithValue("@semanaf", semanaf)
            cmdD.Parameters.AddWithValue("@filtrarsemana", filtrarsemana)

            da = New SqlDataAdapter(cmdD)
            da.Fill(ds, "_DT_VtasProductoProm")
        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function

    Public Function CRVentasPorArticulos(ByVal idEmpresa As Integer, ByVal idtienda As Integer, ByVal idlinea As Integer, ByVal idSublinea As Integer, ByVal idProducto As Integer, ByVal fechainicio As String, ByVal fechafin As String, ByVal yeari As Integer, ByVal semanai As Integer, ByVal yearf As Integer, ByVal semanaf As Integer, ByVal filtrarsemana As Integer) As DataSet
        '14
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmdD As New SqlCommand("_CR_VentasPorArticulosDetallado", cn)
            cmdD.CommandType = CommandType.StoredProcedure
            cmdD.Parameters.AddWithValue("@idEmpresa", idEmpresa)
            cmdD.Parameters.AddWithValue("@idtienda", idtienda)
            cmdD.Parameters.AddWithValue("@idlinea", idlinea)
            cmdD.Parameters.AddWithValue("@idSublinea", idSublinea)
            cmdD.Parameters.AddWithValue("@idProducto", idProducto)
            cmdD.Parameters.AddWithValue("@fechainicio", fechainicio)
            cmdD.Parameters.AddWithValue("@fechafin", fechafin)
            cmdD.Parameters.AddWithValue("@yeari", yeari)
            cmdD.Parameters.AddWithValue("@semanai", semanai)
            cmdD.Parameters.AddWithValue("@yearf", yearf)
            cmdD.Parameters.AddWithValue("@semanaf", semanaf)
            cmdD.Parameters.AddWithValue("@filtrarsemana", filtrarsemana)



            da = New SqlDataAdapter(cmdD)
            da.Fill(ds, "_DT_VentasPorArticulos")
        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function
    Public Function CRVentasPorArticulosResumido(ByVal idEmpresa As Integer, ByVal idtienda As Integer, ByVal idlinea As Integer, ByVal idSublinea As Integer, ByVal idProducto As Integer, ByVal fechainicio As String, ByVal fechafin As String, ByVal yeari As Integer, ByVal semanai As Integer, ByVal yearf As Integer, ByVal semanaf As Integer, ByVal filtrarsemana As Integer) As DataSet
        '15
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmdD As New SqlCommand("_CR_VentasPorArticulosResumido", cn)
            cmdD.CommandType = CommandType.StoredProcedure
            cmdD.Parameters.AddWithValue("@idEmpresa", idEmpresa)
            cmdD.Parameters.AddWithValue("@idtienda", idtienda)
            cmdD.Parameters.AddWithValue("@idlinea", idlinea)
            cmdD.Parameters.AddWithValue("@idSublinea", idSublinea)
            cmdD.Parameters.AddWithValue("@idProducto", idProducto)
            cmdD.Parameters.AddWithValue("@fechainicio", fechainicio)
            cmdD.Parameters.AddWithValue("@fechafin", fechafin)
            cmdD.Parameters.AddWithValue("@yeari", yeari)
            cmdD.Parameters.AddWithValue("@semanai", semanai)
            cmdD.Parameters.AddWithValue("@yearf", yearf)
            cmdD.Parameters.AddWithValue("@semanaf", semanaf)
            cmdD.Parameters.AddWithValue("@filtrarsemana", filtrarsemana)

            da = New SqlDataAdapter(cmdD)
            da.Fill(ds, "_DT_VentasPorArticulosResumido")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function

    Public Function CRVentasPorArticulosDetallladoValorizado(ByVal idEmpresa As Integer, ByVal idtienda As Integer, ByVal IdTipoExistencia As Integer, ByVal idlinea As Integer, ByVal idSublinea As Integer, ByVal idProducto As Integer, ByVal fechainicio As String, ByVal fechafin As String, ByVal yeari As Integer, ByVal semanai As Integer, ByVal yearf As Integer, ByVal semanaf As Integer, ByVal filtrarsemana As Integer) As DataSet
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmdD As New SqlCommand("cr_VentasXArticulosDetalladoValorizado", cn)
            cmdD.CommandType = CommandType.StoredProcedure
            cmdD.Parameters.AddWithValue("@idEmpresa", idEmpresa)
            cmdD.Parameters.AddWithValue("@idtienda", idtienda)
            cmdD.Parameters.AddWithValue("@IdTipoExistencia", IdTipoExistencia)
            cmdD.Parameters.AddWithValue("@idlinea", idlinea)
            cmdD.Parameters.AddWithValue("@idSublinea", idSublinea)
            cmdD.Parameters.AddWithValue("@idProducto", idProducto)
            cmdD.Parameters.AddWithValue("@fechainicio", fechainicio)
            cmdD.Parameters.AddWithValue("@fechafin", fechafin)
            cmdD.Parameters.AddWithValue("@yeari", yeari)
            cmdD.Parameters.AddWithValue("@semanai", semanai)
            cmdD.Parameters.AddWithValue("@yearf", yearf)
            cmdD.Parameters.AddWithValue("@semanaf", semanaf)
            cmdD.Parameters.AddWithValue("@filtrarsemana", filtrarsemana)


            da = New SqlDataAdapter(cmdD)
            da.Fill(ds, "_Dt_VentasXArticulosDetalladoValorizado")
        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function
    Public Function CRVentasPorArticulosResumidoValorizado(ByVal idEmpresa As Integer, ByVal idtienda As Integer, ByVal idlinea As Integer, ByVal idSublinea As Integer, ByVal idProducto As Integer, ByVal fechainicio As String, ByVal fechafin As String, ByVal yeari As Integer, ByVal semanai As Integer, ByVal yearf As Integer, ByVal semanaf As Integer, ByVal filtrarsemana As Integer) As DataSet
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmdD As New SqlCommand("cr_VentasXArticulosResumidoValorizado", cn)
            cmdD.CommandType = CommandType.StoredProcedure
            cmdD.Parameters.AddWithValue("@idEmpresa", idEmpresa)
            cmdD.Parameters.AddWithValue("@idtienda", idtienda)
            cmdD.Parameters.AddWithValue("@idlinea", idlinea)
            cmdD.Parameters.AddWithValue("@idSublinea", idSublinea)
            cmdD.Parameters.AddWithValue("@idProducto", idProducto)
            cmdD.Parameters.AddWithValue("@fechainicio", fechainicio)
            cmdD.Parameters.AddWithValue("@fechafin", fechafin)
            cmdD.Parameters.AddWithValue("@yeari", yeari)
            cmdD.Parameters.AddWithValue("@semanai", semanai)
            cmdD.Parameters.AddWithValue("@yearf", yearf)
            cmdD.Parameters.AddWithValue("@semanaf", semanaf)
            cmdD.Parameters.AddWithValue("@filtrarsemana", filtrarsemana)

            da = New SqlDataAdapter(cmdD)
            da.Fill(ds, "_DT_VentasXArticulosResumidoValorizado")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function
    Public Function ReporteListaPrecios(ByVal idtienda As Integer, ByVal idlinea As Integer, ByVal idSublinea As Integer, ByVal codSublinea As Integer, ByVal idproducto As Integer, ByVal descripcion As String, ByVal idtipoMedida As Integer, ByVal idmoneda As Integer, ByVal tabla As DataTable) As DataSet
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim procedure As String = getNameProcedure(idtipoMedida)
            Dim cmdD As New SqlCommand(procedure, cn)
            cmdD.CommandType = CommandType.StoredProcedure
            cmdD.CommandTimeout = 0


            cmdD.Parameters.AddWithValue("@tienda", idtienda)
            cmdD.Parameters.AddWithValue("@linea", idlinea)
            cmdD.Parameters.AddWithValue("@sublinea", idSublinea)
            cmdD.Parameters.AddWithValue("@codsublinea", codSublinea)
            cmdD.Parameters.AddWithValue("@idProducto", idproducto)
            cmdD.Parameters.AddWithValue("@descripcion", descripcion)
            cmdD.Parameters.AddWithValue("@idmoneda", idmoneda)
            cmdD.Parameters.AddWithValue("@Atributo", Tabla)

            da = New SqlDataAdapter(cmdD)
            da.Fill(ds, "_DT_ListaPrecios")
        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function
    Public Function getNameProcedure(ByVal idtipoprecio As Integer) As String
        Dim nomprocedure As String = String.Empty
        Select Case idtipoprecio
            Case 0
                nomprocedure = "[_CR_listarProductosPreciosPrincipalRetazo]" 'sp principal-Retazo
            Case 1
                nomprocedure = "[_CR_listarProductosPrecios]" 'sp principal
            Case 2
                nomprocedure = "[_CR_listarProductosPreciosOtrasUnidades]" 'sp Retazo
        End Select
        Return nomprocedure
    End Function

End Class
