﻿
'**************************   '08_febrero_2010


'modifi 08_febrero_2010
Imports System.Data.SqlClient
Public Class DAODocumentoView
    Private objConexion As New Conexion

    Public Function DocumentoBuscarDocxSeriexCodigo(ByVal serie As Integer, ByVal codigo As Integer, ByVal IdTipoOperacion As Integer) As List(Of Entidades.DocumentoView)
        '*********** busca por la serie y el codigo 
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoBuscarDocxSeriexCodigo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Serie", serie)
        cmd.Parameters.AddWithValue("@Codigo", codigo)
        cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DocumentoView)
                Do While lector.Read
                    Dim obj As New Entidades.DocumentoView
                    With obj
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .FechaRegistro = CDate(IIf(IsDBNull(lector.Item("doc_FechaRegistro")) = True, Nothing, lector.Item("doc_FechaRegistro")))
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .NomPropietario = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .NomTienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, "", lector.Item("doc_FechaEmision")))
                        .Numero = .Serie + " - " + .Codigo
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function DocumentoDetalle(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idmoneda As Integer, ByVal idtipodoc As Integer, ByVal idestadoDoc As Integer, ByVal fechainicio As String, ByVal fechafin As String, ByVal persona As Integer, ByVal serie As Integer, ByVal codigo As Integer, ByVal rango As Integer, ByVal resumen As Integer, ByVal FiltrarSemana As Integer, ByVal YearIni As Integer, ByVal SemanaIni As Integer, ByVal YearFin As Integer, ByVal SemanaFin As Integer) As DataSet
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim daD As SqlDataAdapter
        Dim ds As New DataSet
        Try
            'Dim cmdD As New SqlCommand("_CR_DocumentoCabeceraDetalle", cn)
            Dim cmdD As New SqlCommand(Procedimiento(idtipodoc, 1, resumen), cn)

            cmdD.CommandType = CommandType.StoredProcedure
            cmdD.Parameters.AddWithValue("@Idempresa", idempresa)
            cmdD.Parameters.AddWithValue("@idtienda", idtienda)
            cmdD.Parameters.AddWithValue("@idmoneda", idmoneda)
            cmdD.Parameters.AddWithValue("@idtipoDoc", idtipodoc)
            cmdD.Parameters.AddWithValue("@idEstadoDoc", idestadoDoc)

            cmdD.Parameters.AddWithValue("@fechainicio", fechainicio)
            cmdD.Parameters.AddWithValue("@fechafin", fechafin)
            cmdD.Parameters.AddWithValue("@idpersona", persona)
            cmdD.Parameters.AddWithValue("@serie", serie)
            cmdD.Parameters.AddWithValue("@codigo", codigo)
            cmdD.Parameters.AddWithValue("@FiltrarFecha", rango)


            cmdD.Parameters.AddWithValue("@FiltrarSemana", FiltrarSemana)
            cmdD.Parameters.AddWithValue("@IdYearIni", YearIni)
            cmdD.Parameters.AddWithValue("@IdSemanaIni", SemanaIni)
            cmdD.Parameters.AddWithValue("@IdYearFin", YearFin)
            cmdD.Parameters.AddWithValue("@IdSemanaFin", SemanaFin)

            daD = New SqlDataAdapter(cmdD)
            'daD.Fill(ds, "DT_DocumentoDetalle")
            daD.Fill(ds, Procedimiento(idtipodoc, 2, resumen))

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function
    Public Function DocumentoDetallexMes(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idmoneda As Integer, ByVal idtipodoc As Integer, ByVal idestadoDoc As Integer, ByVal fechainicio As String, ByVal fechafin As String, ByVal persona As Integer, ByVal serie As Integer, ByVal codigo As Integer, ByVal rango As Integer, ByVal resumen As Integer, ByVal FiltrarSemana As Integer, ByVal YearIni As Integer, ByVal SemanaIni As Integer, ByVal YearFin As Integer, ByVal SemanaFin As Integer) As DataSet
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim daD As SqlDataAdapter
        Dim ds As New DataSet
        Try
            'Dim cmdD As New SqlCommand("_CR_DocumentoCabeceraDetalle", cn)
            Dim cmdD As New SqlCommand(ProcedimientoxMes(idtipodoc, 1, resumen), cn)

            cmdD.CommandType = CommandType.StoredProcedure
            cmdD.Parameters.AddWithValue("@Idempresa", idempresa)
            cmdD.Parameters.AddWithValue("@idtienda", idtienda)
            cmdD.Parameters.AddWithValue("@idmoneda", idmoneda)
            cmdD.Parameters.AddWithValue("@idtipoDoc", idtipodoc)
            cmdD.Parameters.AddWithValue("@idEstadoDoc", idestadoDoc)

            cmdD.Parameters.AddWithValue("@fechainicio", fechainicio)
            cmdD.Parameters.AddWithValue("@fechafin", fechafin)
            cmdD.Parameters.AddWithValue("@idpersona", persona)
            cmdD.Parameters.AddWithValue("@serie", serie)
            cmdD.Parameters.AddWithValue("@codigo", codigo)
            cmdD.Parameters.AddWithValue("@FiltrarFecha", rango)


            cmdD.Parameters.AddWithValue("@FiltrarSemana", FiltrarSemana)
            cmdD.Parameters.AddWithValue("@IdYearIni", YearIni)
            cmdD.Parameters.AddWithValue("@IdSemanaIni", SemanaIni)
            cmdD.Parameters.AddWithValue("@IdYearFin", YearFin)
            cmdD.Parameters.AddWithValue("@IdSemanaFin", SemanaFin)

            daD = New SqlDataAdapter(cmdD)
            'daD.Fill(ds, "DT_DocumentoDetalle")
            daD.Fill(ds, ProcedimientoxMes(idtipodoc, 2, resumen))

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function
    Private Function ProcedimientoxMes(ByVal idtipodoc As Integer, ByVal tipo As Integer, ByVal resumen As Integer) As String
        Dim proc As String = ""
        Dim tabla As String = ""
        Dim cadena As String = ""



        proc = "_CR_DocumentoCabeceraDetallexMes"
        tabla = "DT_DocumentoDetalle"


        If tipo = 1 Then
            cadena = proc
        Else
            cadena = tabla
        End If


        Return cadena

    End Function
    Private Function Procedimiento(ByVal idtipodoc As Integer, ByVal tipo As Integer, ByVal resumen As Integer) As String
        Dim proc As String = ""
        Dim tabla As String = ""
        Dim cadena As String = ""


        If resumen = 0 Then  ''detallados
            If (idtipodoc = 1 Or idtipodoc = 3 Or idtipodoc = 0) Then 'fac y boletas

                proc = "_CR_DocumentoCabeceraDetalle"
                tabla = "DT_DocumentoDetalle"
            Else
                proc = "_CR_DetalleDocumentoOtros"
                tabla = "DT_DocumentoDetalleOtros"

            End If

        Else '' Resumido
            proc = "[_CR_ResumenDocumento]"
            tabla = "DT_ResumenDocumento"
        End If


        If tipo = 1 Then
            cadena = proc
        Else
            cadena = tabla
        End If


        Return cadena

    End Function
    Public Function SelectDocumUsuario(ByVal idEmpresa As Integer, ByVal idTienda As Integer, ByVal FechaInicio As String, ByVal fechafin As String, ByVal idtipodocumento As Integer, ByVal idTipo As Integer, ByVal ascdes As Int16) As DataSet
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            cn.Open()
            Dim cmd As New SqlCommand("[_CR_DocumentXUsuario]", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idempresa", idEmpresa)
            cmd.Parameters.AddWithValue("@idtienda ", idTienda)
            cmd.Parameters.AddWithValue("@fechaInicio", FechaInicio)
            cmd.Parameters.AddWithValue("@fechaFin", fechafin)
            cmd.Parameters.AddWithValue("@idtipodocumento", idtipodocumento)
            cmd.Parameters.AddWithValue("@idorden", idTipo)
            cmd.Parameters.AddWithValue("@AscDes", ascdes)
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocumentoUsuario")

        Catch ex As Exception
            Throw ex
            ds = Nothing
        Finally

        End Try
        Return ds

    End Function
    Public Function SelectDocumMontDiferentes(ByVal idEmpresa As Integer, ByVal idTienda As Integer, ByVal FechaInicio As String, ByVal fechafin As String) As DataSet
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            cn.Open()
            Dim cmd As New SqlCommand("[_CR_DocumentoMontoDifer]", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idempresa", idEmpresa)
            cmd.Parameters.AddWithValue("@idtienda ", idTienda)
            cmd.Parameters.AddWithValue("@fechaInicio", FechaInicio)
            cmd.Parameters.AddWithValue("@fechaFin", fechafin)

            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocumentMontDiferentes")

        Catch ex As Exception
            Throw ex
            ds = Nothing
        Finally

        End Try
        Return ds

    End Function

    Public Function SelectDocumentMediosPagos(ByVal idEmpresa As Integer, ByVal idTienda As Integer, ByVal FechaInicio As String, ByVal fechafin As String) As DataSet
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            cn.Open()
            Dim cmd As New SqlCommand("[_CR_documentosMedioPago]", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idempresa", idEmpresa)
            cmd.Parameters.AddWithValue("@idtienda ", idTienda)
            cmd.Parameters.AddWithValue("@fechaInicio", FechaInicio)
            cmd.Parameters.AddWithValue("@fechaFin", fechafin)

            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocMediosPago")

        Catch ex As Exception
            Throw ex
            ds = Nothing
        Finally

        End Try
        Return ds

    End Function

    Public Function SelectCantidadFilasDocumento(ByVal idEmpresa As Integer, ByVal idTipoDocumento As Integer) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim filas As Integer
        Try
            cn.Open()
            Dim cmd As New SqlCommand("_CantidadFilasxDocumento", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idempresa", idEmpresa)
            cmd.Parameters.AddWithValue("@idtipodocumento", idTipoDocumento)

            filas = CInt(cmd.ExecuteScalar())

        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return filas
    End Function



    Public Function SelectCantidadFilasDocumentoProducto(ByVal IdsProductos As String) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim filas As Integer
        Try
            cn.Open()
            Dim cmd As New SqlCommand("_CantidadFilasxDocumentoDetalle", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdsProductos", IdsProductos)


            filas = CInt(cmd.ExecuteScalar())

        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return filas
    End Function


End Class
