﻿Imports System.Data.SqlClient
Public Class DAOPropietario
    Dim HDAO As New HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectCbo() As List(Of Entidades.Propietario)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PropietarioSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Propietario)
                Do While lector.Read
                    Dim Propietario As New Entidades.Propietario
                    Propietario.Id = CInt(lector.Item("IdPersona"))
                    Propietario.NombreComercial = CStr(lector.Item("per_NComercial"))
                    Lista.Add(Propietario)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
