﻿


'*************   MARTES 27 ABRIL 2010 HORA 02_27 PM




Imports System.Data.SqlClient
Public Class DAOPersonaView
    Inherits DAO.DAOMantenedor

    Private Shadows objConexion As New Conexion


    Public Function SelectActivoxParam_PaginadoUsuario(ByVal texto As String, ByVal opcion As Integer, ByVal PageSize As Integer, ByVal PageNumber As Integer) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("[_PersonaSelectActivoxParam_PaginadoUsuario]", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Texto", texto)
        cmd.Parameters.AddWithValue("@Opcion", opcion)
        cmd.Parameters.AddWithValue("@PageNumber", PageNumber)
        cmd.Parameters.AddWithValue("@PageSize", PageSize)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim obj As New Entidades.PersonaView
                    With obj
                        .IdPersona = CInt(lector.Item("IdPersona"))
                        .Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                        .NombreComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                        .Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                        .Login = CStr(IIf(IsDBNull(lector.Item("us_login")) = True, "", lector.Item("us_login")))
                        .Ubigeo = CStr(IIf(IsDBNull(lector.Item("dir_Ubigeo")) = True, "", lector.Item("dir_Ubigeo")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function SelectActivoxParam_Paginado(ByVal texto As String, ByVal opcion As Integer, ByVal PageSize As Integer, ByVal PageNumber As Integer) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PersonaSelectActivoxParam_Paginado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Texto", texto)
        cmd.Parameters.AddWithValue("@Opcion", opcion)
        cmd.Parameters.AddWithValue("@PageNumber", PageNumber)
        cmd.Parameters.AddWithValue("@PageSize", PageSize)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim obj As New Entidades.PersonaView
                    With obj
                        .IdPersona = CInt(lector.Item("IdPersona"))
                        .Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                        .NombreComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                        .Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                        .Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .Dni = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                        .Ubigeo = CStr(IIf(IsDBNull(lector.Item("dir_Ubigeo")) = True, "", lector.Item("dir_Ubigeo")))
                        .Propietario = CBool(IIf(IsDBNull(lector.Item("Propietario")) = True, 0, lector.Item("Propietario")))
                        .PorcentPercepcion = CDec(IIf(IsDBNull(lector.Item("PorcentPercepcion")) = True, 0, lector.Item("PorcentPercepcion")))
                        .PorcentRetencion = CDec(IIf(IsDBNull(lector.Item("PorcentRetencion")) = True, 0, lector.Item("PorcentRetencion")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function SelectActivoxParam(ByVal texto As String, ByVal opcion As Integer) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PersonaSelectActivoxParam", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Texto", texto)
        cmd.Parameters.AddWithValue("@Opcion", opcion)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim obj As New Entidades.PersonaView
                    With obj
                        .IdPersona = CInt(lector.Item("IdPersona"))
                        .Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                        .NombreComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                        .Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                        .Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .Dni = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                        .Ubigeo = CStr(IIf(IsDBNull(lector.Item("dir_Ubigeo")) = True, "", lector.Item("dir_Ubigeo")))
                        .Propietario = CBool(IIf(IsDBNull(lector.Item("Propietario")) = True, 0, lector.Item("Propietario")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PersonaSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim obj As New Entidades.PersonaView
                    With obj
                        .IdPersona = CInt(lector.Item("IdPersona"))
                        .Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                        .NombreComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                        .Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                        .Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .Dni = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                        .Ubigeo = CStr(IIf(IsDBNull(lector.Item("dir_Ubigeo")) = True, "", lector.Item("dir_Ubigeo")))
                        .Propietario = CBool(IIf(IsDBNull(lector.Item("Propietario")) = True, 0, lector.Item("Propietario")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    '
    Public Function SelectxIdPersonaMaestros(ByVal idpersona As Integer) As Entidades.PersonaView
        Dim objPersonaView As Entidades.PersonaView = Nothing
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Try
            Dim cmd As New SqlCommand("_PersonaViewSelectxIdPersonaMaestros", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdPersona", idpersona)
            cn.Open()
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If lector.Read Then
                objPersonaView = New Entidades.PersonaView
                With objPersonaView
                    .IdPersona = CInt(lector.Item("IdPersona"))
                    .NombreComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                    .Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                    .RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    .Dni = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                    .Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                    .Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                    .Ubigeo = CStr(IIf(IsDBNull(lector.Item("dir_Ubigeo")) = True, "000000", lector.Item("dir_Ubigeo")))
                    .Propietario = CBool(IIf(IsDBNull(lector.Item("Propietario")) = True, 0, lector.Item("Propietario")))
                    .Descripcion = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                    .NroLicencia = CStr(IIf(IsDBNull(lector.Item("ch_NLicencia")) = True, "", lector.Item("ch_NLicencia")))
                    .TipoPV = CStr(IIf(IsDBNull(lector.Item("TipoPV")) = True, "", lector.Item("TipoPV")))
                    .IdTipoPV = UCInt(lector("IdTipoPV"))
                    .Telefeono = UCStr(lector("Telefono"))
                    .Correo = UCStr(lector("Correo"))
                    .ApPaterno = CStr(IIf(IsDBNull(lector.Item("ApePaterno")) = True, "", lector.Item("ApePaterno")))
                    .ApMaterno = CStr(IIf(IsDBNull(lector.Item("ApeMaterno")) = True, "", lector.Item("ApeMaterno")))
                    .Nombres = CStr(IIf(IsDBNull(lector.Item("NombreMaestro")) = True, "", lector.Item("NombreMaestro")))
                    .rol = CStr(IIf(IsDBNull(lector.Item("Rol")) = True, "", lector.Item("Rol")))

                End With
            End If
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return objPersonaView
    End Function



    Public Function SelectxIdPersona(ByVal idpersona As Integer) As Entidades.PersonaView
        Dim objPersonaView As Entidades.PersonaView = Nothing
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Try
            Dim cmd As New SqlCommand("_PersonaViewSelectxIdPersona", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdPersona", idpersona)
            cn.Open()
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If lector.Read Then
                objPersonaView = New Entidades.PersonaView
                With objPersonaView
                    .IdPersona = CInt(lector.Item("IdPersona"))
                    .NombreComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                    .Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                    .RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    .Dni = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                    .Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                    .Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                    .Ubigeo = CStr(IIf(IsDBNull(lector.Item("dir_Ubigeo")) = True, "000000", lector.Item("dir_Ubigeo")))
                    .Propietario = CBool(IIf(IsDBNull(lector.Item("Propietario")) = True, 0, lector.Item("Propietario")))
                    .Descripcion = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                    .NroLicencia = CStr(IIf(IsDBNull(lector.Item("ch_NLicencia")) = True, "", lector.Item("ch_NLicencia")))
                    .TipoPV = CStr(IIf(IsDBNull(lector.Item("TipoPV")) = True, "", lector.Item("TipoPV")))
                    .IdTipoPV = UCInt(lector("IdTipoPV"))
                    .Telefeono = UCStr(lector("Telefono"))
                    .Correo = UCStr(lector("Correo"))
                    '.Rol = CStr(IIf(IsDBNull(lector.Item("Rol")) = True, "", lector.Item("Rol")))

                End With
            End If
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return objPersonaView
    End Function
    Public Function SelectActivoxPersonaRucDni(ByVal texto As String, ByVal Ruc As String, ByVal dni As String, ByVal opcion As Integer, ByVal PageSize As Integer, ByVal PageNumber As Integer) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_GetxPersonaNaturalJuridica ", cn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@Dni", dni)
        cmd.Parameters.AddWithValue("@Ruc", Ruc)
        cmd.Parameters.AddWithValue("@razonApe", texto)
        cmd.Parameters.AddWithValue("@tipo", opcion)
        cmd.Parameters.AddWithValue("@pageindex", PageNumber)
        cmd.Parameters.AddWithValue("@pageSize", PageSize)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim obj As New Entidades.PersonaView
                    With obj
                        .IdPersona = CInt(lector.Item("IdPersona"))
                        .Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                        .Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                        .Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .Dni = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))

                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxPersonaRucDniDetDoc(ByVal texto As String, ByVal opcion As Integer, ByVal PageSize As Integer, ByVal PageNumber As Integer) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_GetxPersonaNaturalJuridicaDetDoc ", cn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@Dni", texto)
        cmd.Parameters.AddWithValue("@Ruc", texto)
        cmd.Parameters.AddWithValue("@razonApe", texto)
        cmd.Parameters.AddWithValue("@tipo", opcion)
        cmd.Parameters.AddWithValue("@pageindex", PageNumber)
        cmd.Parameters.AddWithValue("@pageSize", PageSize)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim obj As New Entidades.PersonaView
                    With obj
                        .IdPersona = CInt(lector.Item("IdPersona"))
                        .Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                        .Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                        .Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .Dni = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
