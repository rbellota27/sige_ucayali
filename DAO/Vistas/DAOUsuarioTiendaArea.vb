﻿Imports System.Data.SqlClient
Public Class DAOUsuarioTiendaArea
    Private objConexion As New Conexion
    Private objHelperDAO As New HelperDAO
    Public Function SelectActivoDistinctTiendaxIdPersonaxIdPerfil(ByVal IdPersona As Integer, ByVal IdPerfil As Integer) As List(Of Entidades.UsuarioTiendaArea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UsuarioTiendaAreaSelectActivoDistinctTiendaxIdPersonaxIdPerfil", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdPerfil", IdPerfil)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.UsuarioTiendaArea)
                Do While lector.Read
                    Dim obj As New Entidades.UsuarioTiendaArea
                    obj.IdTienda = CInt(lector.Item("IdTienda"))
                    obj.NomTienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Sub Insert(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal objUTA As Entidades.UsuarioTiendaArea)
        Dim parametros() As SqlParameter = New SqlParameter(4) {}
        parametros(0) = New SqlParameter("@ust_Estado", SqlDbType.Bit)
        parametros(0).Value = objUTA.Estado
        parametros(1) = New SqlParameter("@IdArea", SqlDbType.Int)
        parametros(1).Value = objUTA.IdArea
        parametros(2) = New SqlParameter("@IdTienda", SqlDbType.Int)
        parametros(2).Value = objUTA.IdTienda
        parametros(3) = New SqlParameter("@IdPerfil", SqlDbType.Int)
        parametros(3).Value = objUTA.IdPerfil
        parametros(4) = New SqlParameter("@IdPersona", SqlDbType.Int)
        parametros(4).Value = objUTA.IdPersona
        objHelperDAO.InsertaT(cn, "_UsuarioTiendaAreaInsert", parametros, tr)
    End Sub
    Public Sub Update(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal objUTA As Entidades.UsuarioTiendaArea)
        Dim parametros() As SqlParameter = New SqlParameter(4) {}
        parametros(0) = New SqlParameter("@ust_Estado", SqlDbType.Bit)
        parametros(0).Value = objUTA.Estado
        parametros(1) = New SqlParameter("@IdArea", SqlDbType.Int)
        parametros(1).Value = objUTA.IdArea
        parametros(2) = New SqlParameter("@IdTienda", SqlDbType.Int)
        parametros(2).Value = objUTA.IdTienda
        parametros(3) = New SqlParameter("@IdPerfil", SqlDbType.Int)
        parametros(3).Value = objUTA.IdPerfil
        parametros(4) = New SqlParameter("@IdPersona", SqlDbType.Int)
        parametros(4).Value = objUTA.IdPersona
        objHelperDAO.UpdateT(cn, "_UsuarioTiendaAreaUpdate", parametros, tr)
    End Sub
    Public Sub DeletexIdPersona(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal IdPersona As Integer)
        Dim cmd As New SqlCommand("_UsuarioTiendaAreaDeletexIdPersona", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub
    Public Function SelectAllxIdPersona(ByVal idpersona As Integer) As List(Of Entidades.UsuarioTiendaArea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UsuarioTiendaAreaSelectAllxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", idpersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.UsuarioTiendaArea)
                Do While lector.Read
                    Dim obj As New Entidades.UsuarioTiendaArea(CInt(lector.Item("IdTienda")), CInt(lector.Item("IdArea")), CInt(lector.Item("IdPerfil")), CInt(lector.Item("IdPersona")), CStr(lector.Item("tie_Nombre")), CStr(lector.Item("ar_NombreCorto")), CStr(lector.Item("perf_Nombre")), CBool(lector.Item("ust_Estado")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
