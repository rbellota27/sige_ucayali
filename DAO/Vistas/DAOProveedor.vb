﻿Imports System.Data.SqlClient
Public Class DAOProveedor
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

    Public Function ListarProveedorOrdenCompra(ByVal idproveedor As Integer) As Entidades.PersonaView
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_listarProveedorOrdenCompra", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idproveedor", idproveedor)
        Dim lector As SqlDataReader
        Try
            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
            Dim objProveedor As New Entidades.PersonaView
            If lector.Read Then
                With objProveedor
                    .IdPersona = CInt(lector("IdPersona"))
                    .RazonSocial = CStr(IIf(IsDBNull(lector("jur_Rsocial")), "---", lector("jur_Rsocial")))
                    .Direccion = CStr(IIf(IsDBNull(lector("dir_Direccion")), "---", lector("dir_Direccion")))
                    .Ruc = CStr(IIf(IsDBNull(lector("RUC")), "---", lector("RUC")))
                    .Telefeono = CStr(IIf(IsDBNull(lector("Telefono")), "---", lector("Telefono")))
                    .Correo = CStr(IIf(IsDBNull(lector("Correo")), "---", lector("Correo")))
                End With
            End If
            lector.Close()
            Return objProveedor
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectCbo() As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProveedorSelectActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim prov As New Entidades.PersonaView
                    prov.IdPersona = CInt(IIf(IsDBNull(lector.Item("idpersona")) = True, 0, lector.Item("idpersona")))
                    prov.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    prov.Ruc = CStr(IIf(IsDBNull(lector.Item("RUC")) = True, "", lector.Item("RUC")))
                    Lista.Add(prov)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCboImportado() As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProveedorSelectActivoImportado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim prov As New Entidades.PersonaView
                    prov.IdPersona = CInt(IIf(IsDBNull(lector.Item("idpersona")) = True, 0, lector.Item("idpersona")))
                    prov.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    prov.Ruc = CStr(IIf(IsDBNull(lector.Item("RUC")) = True, "", lector.Item("RUC")))
                    Lista.Add(prov)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectGrillaProveedor() As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProveedorSelectGrilla", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim clie As New Entidades.PersonaView
                    clie.IdPersona = CInt(lector.Item("IdPersona"))
                    clie.Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                    clie.NombreComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                    clie.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    clie.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                    clie.Telefeono = CStr(IIf(IsDBNull(lector.Item("tel_Numero")) = True, "", lector.Item("tel_Numero")))
                    Lista.Add(clie)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectGrillaProveedorxEstado(ByVal estado As String) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProveedorSelectxEstado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Estado", estado)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim clie As New Entidades.PersonaView
                    clie.IdPersona = CInt(lector.Item("IdPersona"))
                    clie.Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                    clie.NombreComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                    clie.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    clie.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                    clie.Telefeono = CStr(IIf(IsDBNull(lector.Item("tel_Numero")) = True, "", lector.Item("tel_Numero")))
                    Lista.Add(clie)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectGrillaProveedorxNombre(ByVal nombre As String) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProveedorSelectxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim Prov As New Entidades.PersonaView
                    Prov.IdPersona = CInt(lector.Item("IdPersona"))
                    Prov.Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                    Prov.NombreComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                    Prov.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    Prov.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                    Prov.Telefeono = CStr(IIf(IsDBNull(lector.Item("tel_Numero")) = True, "", lector.Item("tel_Numero")))
                    Lista.Add(Prov)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectGrillaProveedorxRazonSocial(ByVal nombre As String) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProveedorSelectxRazonSocial", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@jur_Rsocial", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim Prov As New Entidades.PersonaView
                    Prov.IdPersona = CInt(lector.Item("IdPersona"))
                    Prov.Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                    Prov.NombreComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                    Prov.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    Prov.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                    Prov.Telefeono = CStr(IIf(IsDBNull(lector.Item("tel_Numero")) = True, "", lector.Item("tel_Numero")))
                    Lista.Add(Prov)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectGrillaProveedorxDNI(ByVal numero As String) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProveedorSelectxDni", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Dni", numero)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim Prov As New Entidades.PersonaView
                    Prov.IdPersona = CInt(lector.Item("IdPersona"))
                    Prov.Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                    Prov.NombreComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                    Prov.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    Prov.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                    Prov.Telefeono = CStr(IIf(IsDBNull(lector.Item("tel_Numero")) = True, "", lector.Item("tel_Numero")))
                    Lista.Add(Prov)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectGrillaProveedorxRUC(ByVal numero As String) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProveedorSelectxRucLike", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Ruc", numero)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim Prov As New Entidades.PersonaView
                    Prov.IdPersona = CInt(lector.Item("IdPersona"))
                    Prov.Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                    Prov.NombreComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                    Prov.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    Prov.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                    Prov.Telefeono = CStr(IIf(IsDBNull(lector.Item("tel_Numero")) = True, "", lector.Item("tel_Numero")))
                    Lista.Add(Prov)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectGrillaProveedorxIdPersona(ByVal codigo As String) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProveedorSelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", codigo)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim Prov As New Entidades.PersonaView
                    Prov.IdPersona = CInt(lector.Item("IdPersona"))
                    Prov.Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                    Prov.NombreComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                    Prov.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    Prov.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                    Prov.Telefeono = CStr(IIf(IsDBNull(lector.Item("tel_Numero")) = True, "", lector.Item("tel_Numero")))
                    Lista.Add(Prov)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
