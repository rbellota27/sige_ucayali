﻿'**************** MARTES 16 FEB 2010 HORA 02_37 PM


''''''''''''''''''''''MIRAYA ANAMARIA, EDGAR 02/02/2010 8:19 pm
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOTarjetaView
    Inherits DAOTarjeta

    Public Function SelectCboxTipoTarjetaCaja(ByVal IdTarjeta As Integer, ByVal IdCaja As Integer) As List(Of Entidades.Tarjeta)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("[_TarjetaSelectCboxTipoTarjetaCaja]", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoTarjeta", IdTarjeta)
        cmd.Parameters.AddWithValue("@IdCaja", IdCaja)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tarjeta)
                Do While lector.Read
                    Dim obj As New Entidades.Tarjeta
                    With obj
                        .Id = CInt(IIf(IsDBNull(lector("IdTarjeta")) = True, 0, lector("IdTarjeta")))
                        .Nombre = CStr(IIf(IsDBNull(lector("t_Nombre")) = True, "", lector("t_Nombre")))
                    End With

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectCboxIdPost(ByVal IdPost As Integer) As List(Of Entidades.Tarjeta)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TarjetaSelectCboxIdPost", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPost", IdPost)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tarjeta)
                Do While lector.Read
                    Dim obj As New Entidades.Tarjeta
                    With obj
                        .Id = CInt(IIf(IsDBNull(lector("IdTarjeta")) = True, 0, lector("IdTarjeta")))
                        .Nombre = CStr(IIf(IsDBNull(lector("t_Nombre")) = True, "", lector("t_Nombre")))
                    End With

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxParams(ByVal x As Entidades.TarjetaView) As List(Of Entidades.TarjetaView)
        cn = objConexion.ConexionSIGE()
        Dim Prm() As SqlParameter = New SqlParameter(3) {}

        Prm(0) = New SqlParameter("@IdTarjeta", SqlDbType.Int)
        Prm(0).Value = IIf(IsNothing(x.Id), DBNull.Value, x.Id)
        Prm(1) = New SqlParameter("@t_Nombre", SqlDbType.VarChar)
        Prm(1).Value = x.Nombre
        Prm(2) = New SqlParameter("@t_Estado", SqlDbType.Bit)
        Prm(2).Value = x.Estado
        Prm(3) = New SqlParameter("@IdTipoTarjeta", SqlDbType.Int)
        Prm(3).Value = x.IdTipoTarjeta
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()

                lector = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_TarjetaViewSelectxParams", Prm)

                Dim Lista As New List(Of Entidades.TarjetaView)
                With lector
                    Do While .Read

                        Dim obj As New Entidades.TarjetaView

                        obj.Id = CInt(.Item("IdTarjeta"))
                        obj.Nombre = CStr(.Item("t_Nombre"))
                        obj.Estado = CBool(.Item("t_Estado"))
                        obj.IdTipoTarjeta = CInt(.Item("IdTipoTarjeta"))
                        obj.TipoTarjeta = CStr(.Item("ttarjeta_nombre"))

                        Lista.Add(obj)

                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Overloads Function SelectAll() As List(Of Entidades.TarjetaView)

        Dim x As New Entidades.TarjetaView
        Return SelectxParams(x)

    End Function

    Public Overloads Function SelectAllActivo() As List(Of Entidades.TarjetaView)

        Dim x As New Entidades.TarjetaView
        x.Estado = True
        Return SelectxParams(x)

    End Function

    Public Overloads Function SelectAllInactivo() As List(Of Entidades.TarjetaView)

        Dim x As New Entidades.TarjetaView
        x.Estado = False
        Return SelectxParams(x)

    End Function

    Public Overloads Function SelectxIdTipoTarjeta(ByVal Id As Integer) As List(Of Entidades.TarjetaView)

        Dim x As New Entidades.TarjetaView
        x.Estado = True
        x.IdTipoTarjeta = Id
        Return SelectxParams(x)

    End Function


End Class
