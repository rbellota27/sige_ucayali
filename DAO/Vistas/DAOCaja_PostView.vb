﻿'MIERCOLES 17 DE FEBRERO 12 17 PM   Se agrego datos de Banco y CuentaBancaria
'MIERCOLES 17 DE FEBRERO 11 39 AM   Se elimino los valores IdTipoTarjeta y TipoTarjeta, por cambio en la bd
''''''''''''''''''''''MIRAYA ANAMARIA, EDGAR 29/01/2010 6:50 pm
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOCaja_PostView
    Inherits DAOCaja_Post

    Public Function SelectxParams(ByVal x As Entidades.Caja_PostView) As List(Of Entidades.Caja_PostView)
        Cn = objConexion.ConexionSIGE()
        Dim Prm() As SqlParameter = New SqlParameter(2) {}

        Prm(0) = New SqlParameter("@IdCaja", SqlDbType.Int)
        Prm(0).Value = x.IdCaja
        Prm(1) = New SqlParameter("@IdPost", SqlDbType.Int)
        Prm(1).Value = x.IdPost
        Prm(2) = New SqlParameter("@cpo_Estado", SqlDbType.Bit)
        Prm(2).Value = x.Estado

        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()

                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "_Caja_PostViewSelectxParams", Prm)

                Dim Lista As New List(Of Entidades.Caja_PostView)
                With lector
                    Do While .Read

                        Dim obj As New Entidades.Caja_PostView

                        obj.IdCaja = CInt(.Item("IdCaja"))
                        obj.IdPost = CInt(.Item("IdPost"))
                        obj.Estado = CBool(.Item("cpo_Estado"))

                        obj.CajaNombre = CStr(.Item("caja_Nombre"))
                        obj.CajaNumero = CStr(.Item("caja_Numero"))
                        obj.PostDescripcion = CStr(.Item("po_Descripcion"))
                        obj.PostIdentificador = CStr(.Item("po_Identificador"))

                        obj.IdTienda = CInt(.Item("IdTienda"))
                        obj.Tienda = CStr(.Item("Tienda"))

                        obj.IdBanco = CInt(.Item("IdBanco"))
                        obj.Banco = CStr(.Item("Banco"))
                        obj.IdCuentaBancaria = CInt(.Item("IdCuentaBancaria"))
                        obj.CuentaBancaria = CStr(.Item("CuentaBancaria"))

                        Lista.Add(obj)

                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAll() As List(Of Entidades.Caja_PostView)

        Dim x As New Entidades.Caja_PostView
        Return SelectxParams(x)

    End Function

    Public Function SelectAllActivo() As List(Of Entidades.Caja_PostView)

        Dim x As New Entidades.Caja_PostView
        x.Estado = True
        Return SelectxParams(x)

    End Function

    Public Function SelectAllInactivo() As List(Of Entidades.Caja_PostView)

        Dim x As New Entidades.Caja_PostView
        x.Estado = False
        Return SelectxParams(x)

    End Function

End Class
