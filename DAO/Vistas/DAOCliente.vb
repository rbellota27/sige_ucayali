﻿'*********************   LUNES 24052010
'***************** '08_febrero_2010
Imports System.Data.SqlClient
Imports Microsoft.Office.Interop
Public Class DAOCliente
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function ReporteDetalleDAOT(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idpersona As Integer, ByVal fechaInicio As String, ByVal fechafin As String, ByVal tipo As Integer, ByVal uit As Decimal, ByVal doc As Integer) As DataSet

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim daD As SqlDataAdapter
        Dim ds As New DataSet
        Try
            If (tipo = 1) Then
                Dim cmdD As New SqlCommand("[_CR_detalladoDAOT]", cn)
                cmdD.CommandType = CommandType.StoredProcedure
                cmdD.Parameters.AddWithValue("@Idempresa", idempresa)
                cmdD.Parameters.AddWithValue("@idTienda", idtienda)
                cmdD.Parameters.AddWithValue("@idpersona", idpersona)
                cmdD.Parameters.AddWithValue("@fechainicio", fechaInicio)
                cmdD.Parameters.AddWithValue("@fechafin", fechafin)
                cmdD.Parameters.AddWithValue("@uit", uit)
                cmdD.Parameters.AddWithValue("@idtipodocumento", doc)
                daD = New SqlDataAdapter(cmdD)
                daD.Fill(ds, "DT_DetalleDAOT")
            Else
                Dim cmdD As New SqlCommand("[_CR_ResumenDAOT]", cn)
                cmdD.CommandType = CommandType.StoredProcedure
                cmdD.Parameters.AddWithValue("@Idempresa", idempresa)
                cmdD.Parameters.AddWithValue("@idTienda", idtienda)
                cmdD.Parameters.AddWithValue("@idpersona", idpersona)
                cmdD.Parameters.AddWithValue("@fechainicio", fechaInicio)
                cmdD.Parameters.AddWithValue("@fechafin", fechafin)
                cmdD.Parameters.AddWithValue("@uit", uit)
                cmdD.Parameters.AddWithValue("@idtipodocumento", doc)


                daD = New SqlDataAdapter(cmdD)
                daD.Fill(ds, "DT_ResumidoDAOT")

            End If

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function

    Public Function PersonaUpdate_Ventas(ByVal objPersonaView As Entidades.PersonaView) As Boolean

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim hecho As Boolean = False
        Dim tr As SqlTransaction = Nothing
        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            Dim cmd As New SqlCommand("_PersonaUpdate_Ventas_v2", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure

            Dim p() As SqlParameter = New SqlParameter(13) {}

            p(0) = New SqlParameter("@IdPersona", SqlDbType.Int)
            p(0).Value = IIf(objPersonaView.IdPersona = Nothing, DBNull.Value, objPersonaView.IdPersona)

            p(1) = New SqlParameter("@RasonSocial", SqlDbType.VarChar)
            p(1).Value = IIf(objPersonaView.RazonSocial = Nothing, DBNull.Value, objPersonaView.RazonSocial)

            p(2) = New SqlParameter("@ApPaterno", SqlDbType.VarChar)
            p(2).Value = IIf(objPersonaView.ApPaterno = Nothing, DBNull.Value, objPersonaView.ApPaterno)

            p(3) = New SqlParameter("@ApMaterno", SqlDbType.VarChar)
            p(3).Value = IIf(objPersonaView.ApMaterno = Nothing, DBNull.Value, objPersonaView.ApMaterno)

            p(4) = New SqlParameter("@Nombres", SqlDbType.VarChar)
            p(4).Value = IIf(objPersonaView.Nombres = Nothing, DBNull.Value, objPersonaView.Nombres)

            p(5) = New SqlParameter("@RUC", SqlDbType.VarChar)
            p(5).Value = IIf(objPersonaView.Ruc = Nothing, DBNull.Value, objPersonaView.Ruc)

            p(6) = New SqlParameter("@Direccion", SqlDbType.VarChar)
            p(6).Value = IIf(objPersonaView.Direccion = Nothing, DBNull.Value, objPersonaView.Direccion)

            p(7) = New SqlParameter("@DNI", SqlDbType.VarChar)
            p(7).Value = IIf(objPersonaView.Dni = Nothing, DBNull.Value, objPersonaView.Dni)

            p(8) = New SqlParameter("@IdTipoAgente", SqlDbType.Int)
            p(8).Value = IIf(objPersonaView.IdTipoAgente = Nothing, DBNull.Value, objPersonaView.IdTipoAgente)

            p(9) = New SqlParameter("@TipoPersona", SqlDbType.Int)
            p(9).Value = IIf(objPersonaView.TipoPersona = Nothing, DBNull.Value, objPersonaView.TipoPersona)

            p(10) = New SqlParameter("@Ubigeo", SqlDbType.VarChar)
            p(10).Value = IIf(objPersonaView.Ubigeo = Nothing, DBNull.Value, objPersonaView.Ubigeo)

            p(11) = New SqlParameter("@telefono", SqlDbType.NVarChar)
            p(11).Value = IIf(objPersonaView.Telefeono = Nothing, DBNull.Value, objPersonaView.Telefeono)

            p(12) = New SqlParameter("@telefonomovil", SqlDbType.NVarChar)
            p(12).Value = IIf(objPersonaView.TelefeonoMovil = Nothing, DBNull.Value, objPersonaView.TelefeonoMovil)

            p(13) = New SqlParameter("@email", SqlDbType.NVarChar)
            p(13).Value = IIf(objPersonaView.Correo = Nothing, DBNull.Value, objPersonaView.Correo)


            cmd.Parameters.AddRange(p)

            cmd.ExecuteNonQuery()

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return hecho
    End Function

    Public Function PersonaUpdate_MO(ByVal objPersonaView As Entidades.PersonaView) As Boolean

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim hecho As Boolean = False
        Dim tr As SqlTransaction = Nothing
        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)

            Dim cmd As New SqlCommand("_PersonaUpdate_MO", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure

            Dim p() As SqlParameter = New SqlParameter(11) {}

            p(0) = New SqlParameter("@IdPersona", SqlDbType.Int)
            p(0).Value = IIf(objPersonaView.IdPersona = Nothing, DBNull.Value, objPersonaView.IdPersona)

            p(1) = New SqlParameter("@RasonSocial", SqlDbType.VarChar)
            p(1).Value = IIf(objPersonaView.RazonSocial = Nothing, DBNull.Value, objPersonaView.RazonSocial)

            p(2) = New SqlParameter("@ApPaterno", SqlDbType.VarChar)
            p(2).Value = IIf(objPersonaView.ApPaterno = Nothing, DBNull.Value, objPersonaView.ApPaterno)

            p(3) = New SqlParameter("@ApMaterno", SqlDbType.VarChar)
            p(3).Value = IIf(objPersonaView.ApMaterno = Nothing, DBNull.Value, objPersonaView.ApMaterno)

            p(4) = New SqlParameter("@Nombres", SqlDbType.VarChar)
            p(4).Value = IIf(objPersonaView.Nombres = Nothing, DBNull.Value, objPersonaView.Nombres)

            p(5) = New SqlParameter("@RUC", SqlDbType.VarChar)
            p(5).Value = IIf(objPersonaView.Ruc = Nothing, DBNull.Value, objPersonaView.Ruc)

            p(6) = New SqlParameter("@Direccion", SqlDbType.VarChar)
            p(6).Value = IIf(objPersonaView.Direccion = Nothing, DBNull.Value, objPersonaView.Direccion)

            p(7) = New SqlParameter("@DNI", SqlDbType.VarChar)
            p(7).Value = IIf(objPersonaView.Dni = Nothing, DBNull.Value, objPersonaView.Dni)

            p(8) = New SqlParameter("@IdTipoAgente", SqlDbType.Int)
            p(8).Value = IIf(objPersonaView.IdTipoAgente = Nothing, DBNull.Value, objPersonaView.IdTipoAgente)

            p(9) = New SqlParameter("@TipoPersona", SqlDbType.Int)
            p(9).Value = 1

            p(10) = New SqlParameter("@Ubigeo", SqlDbType.VarChar)
            p(10).Value = IIf(objPersonaView.Ubigeo = Nothing, DBNull.Value, objPersonaView.Ubigeo)

            p(11) = New SqlParameter("@correo", SqlDbType.VarChar)
            p(11).Value = IIf(objPersonaView.EMail = Nothing, DBNull.Value, objPersonaView.EMail)

            cmd.Parameters.AddRange(p)

            cmd.ExecuteNonQuery()

            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return hecho
    End Function

    Public Function SelectxId_Ventas(ByVal IdPersona As Integer, ByVal idempresa As Integer) As Entidades.PersonaView
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ClienteSelectxIPersona_Ventas", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdEmpresa", idempresa)

        Dim objCliente As Entidades.PersonaView = Nothing

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                If lector.Read Then
                    objCliente = New Entidades.PersonaView
                    With objCliente
                        .IdPersona = CInt(lector.Item("IdPersona"))
                        .NombreComercial = CStr(IIf(IsDBNull(lector.Item("Per_NComercial")) = True, "", lector.Item("Per_NComercial")))
                        .RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                        .Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                        .Dni = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                        .Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                        .Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                        .Ubigeo = CStr(IIf(IsDBNull(lector.Item("dir_Ubigeo")) = True, "", lector.Item("dir_Ubigeo")))
                        .Referencia = CStr(IIf(IsDBNull(lector.Item("dir_Ubigeo")) = True, "", lector.Item("dir_Ubigeo")))
                        .Telefeono = CStr(IIf(IsDBNull(lector.Item("tel_Numero")) = True, "", lector.Item("tel_Numero")))
                        .TelefeonoMovil = CStr(IIf(IsDBNull(lector.Item("tel_NumeroMovil")) = True, "", lector.Item("tel_NumeroMovil")))
                        .Estado = CStr(lector.Item("per_estado"))
                        .IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                        .ApPaterno = CStr(IIf(IsDBNull(lector.Item("nat_Apepat")) = True, "", lector.Item("nat_Apepat")))
                        .ApMaterno = CStr(IIf(IsDBNull(lector.Item("nat_Apemat")) = True, "", lector.Item("nat_Apemat")))
                        .Nombres = CStr(IIf(IsDBNull(lector.Item("nat_Nombres")) = True, "", lector.Item("nat_Nombres")))
                        .IdTipoAgente = CInt(IIf(IsDBNull(lector.Item("IdAgente")) = True, 0, lector.Item("IdAgente")))
                        .PorcentTipoAgente = CDec(IIf(IsDBNull(lector.Item("PorcentTipoAgente")) = True, 0, lector.Item("PorcentTipoAgente")))
                        .TipoPersona = CInt(IIf(IsDBNull(lector.Item("TipoPersona")) = True, 0, lector.Item("TipoPersona")))
                        .SujetoARetencion = CBool(IIf(IsDBNull(lector.Item("ag_SujetoRetencion")) = True, 0, lector.Item("ag_SujetoRetencion")))
                        .SujetoAPercepcion = CBool(IIf(IsDBNull(lector.Item("ag_SujetoAPercepcion")) = True, 0, lector.Item("ag_SujetoAPercepcion")))
                        .IdRol = CInt(IIf(IsDBNull(lector.Item("RolCliente")) = True, 0, lector.Item("RolCliente")))
                        .Correo = CStr(IIf(IsDBNull(lector.Item("corr_Nombre")) = True, 0, lector.Item("corr_Nombre")))


                    End With
                Else
                    Throw New Exception("No se halló el Cliente.")
                End If
                Return objCliente
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxId(ByVal IdPersona As Integer) As Entidades.PersonaView
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ClienteSelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                lector.Read()

                Dim clie As New Entidades.PersonaView
                clie.IdPersona = CInt(lector.Item("IdPersona"))
                clie.NombreComercial = CStr(IIf(IsDBNull(lector.Item("Per_NComercial")) = True, "", lector.Item("Per_NComercial")))
                clie.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                clie.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                clie.Dni = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                clie.Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                clie.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                clie.Ubigeo = CStr(IIf(IsDBNull(lector.Item("dir_Ubigeo")) = True, "", lector.Item("dir_Ubigeo")))
                clie.Referencia = CStr(IIf(IsDBNull(lector.Item("dir_Ubigeo")) = True, "", lector.Item("dir_Ubigeo")))
                clie.Telefeono = CStr(IIf(IsDBNull(lector.Item("tel_Numero")) = True, "", lector.Item("tel_Numero")))
                clie.Estado = CStr(lector.Item("per_estado"))
                clie.IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))

                clie.ApPaterno = CStr(IIf(IsDBNull(lector.Item("nat_Apepat")) = True, "", lector.Item("nat_Apepat")))
                clie.ApMaterno = CStr(IIf(IsDBNull(lector.Item("nat_Apemat")) = True, "", lector.Item("nat_Apemat")))
                clie.Nombres = CStr(IIf(IsDBNull(lector.Item("nat_Nombres")) = True, "", lector.Item("nat_Nombres")))

                Return clie
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxDni(ByVal Dni As String) As Entidades.PersonaView
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ClienteSelectxDni", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Dni", Dni)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                lector.Read()
                If lector.HasRows = False Then
                    Throw New Exception("No Existe CLiente con ese Dni")
                End If
                Dim clie As New Entidades.PersonaView
                clie.IdPersona = CInt(lector.Item("IdPersona"))
                clie.NombreComercial = CStr(IIf(IsDBNull(lector.Item("Per_NComercial")) = True, "", lector.Item("Per_NComercial")))
                clie.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                clie.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                clie.Dni = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                clie.Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                clie.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))

                clie.Ubigeo = CStr(IIf(IsDBNull(lector.Item("dir_Ubigeo")) = True, "", lector.Item("dir_Ubigeo")))
                clie.Referencia = CStr(IIf(IsDBNull(lector.Item("dir_Ubigeo")) = True, "", lector.Item("dir_Ubigeo")))
                clie.Telefeono = CStr(IIf(IsDBNull(lector.Item("tel_Numero")) = True, "", lector.Item("tel_Numero")))
                clie.Estado = CStr(lector.Item("per_estado"))
                clie.IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                Return clie
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxRuc(ByVal Ruc As String) As Entidades.PersonaView
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ClienteSelectxRuc", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Ruc", Ruc)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                lector.Read()
                If lector.HasRows = False Then
                    Throw New Exception("No Existe CLiente con ese Ruc")
                End If
                Dim clie As New Entidades.PersonaView
                clie.IdPersona = CInt(lector.Item("IdPersona"))
                clie.NombreComercial = CStr(IIf(IsDBNull(lector.Item("Per_NComercial")) = True, "", lector.Item("Per_NComercial")))
                clie.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                clie.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                clie.Dni = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                clie.Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                clie.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                clie.Ubigeo = CStr(IIf(IsDBNull(lector.Item("dir_Ubigeo")) = True, "", lector.Item("dir_Ubigeo")))
                clie.Referencia = CStr(IIf(IsDBNull(lector.Item("dir_Ubigeo")) = True, "", lector.Item("dir_Ubigeo")))
                clie.Telefeono = CStr(IIf(IsDBNull(lector.Item("tel_Numero")) = True, "", lector.Item("tel_Numero")))
                clie.Estado = CStr(lector.Item("per_estado"))
                clie.IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                Return clie
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxNombre(ByVal Nombre As String) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ClienteSelectxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", Nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim clie As New Entidades.PersonaView
                    clie.IdPersona = CInt(lector.Item("IdPersona"))
                    clie.NombreComercial = CStr(IIf(IsDBNull(lector.Item("Per_NComercial")) = True, "", lector.Item("Per_NComercial")))
                    clie.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    clie.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                    clie.Dni = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                    clie.Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                    clie.Estado = CStr(lector.Item("per_estado"))
                    clie.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                    clie.IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                    Lista.Add(clie)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxNombreComercial(ByVal NombreComercial As String) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ClienteSelectxNombreComercial", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@per_NComercial", NombreComercial)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim clie As New Entidades.PersonaView
                    clie.IdPersona = CInt(lector.Item("IdPersona"))
                    clie.NombreComercial = CStr(IIf(IsDBNull(lector.Item("Per_NComercial")) = True, "", lector.Item("Per_NComercial")))
                    clie.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    clie.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                    clie.Dni = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                    clie.Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                    clie.Estado = CStr(lector.Item("per_estado"))
                    clie.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                    clie.IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                    Lista.Add(clie)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxRazonSocial(ByVal RazonSocial As String) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ClienteSelectxRazonSocial", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@jur_Rsocial", RazonSocial)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim clie As New Entidades.PersonaView
                    clie.IdPersona = CInt(lector.Item("IdPersona"))
                    clie.NombreComercial = CStr(IIf(IsDBNull(lector.Item("Per_NComercial")) = True, "", lector.Item("Per_NComercial")))
                    clie.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    clie.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                    clie.Dni = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                    clie.Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                    clie.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                    clie.Estado = CStr(lector.Item("per_estado"))
                    clie.IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                    Lista.Add(clie)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ClienteSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim clie As New Entidades.PersonaView
                    clie.IdPersona = CInt(lector.Item("IdPersona"))
                    clie.NombreComercial = CStr(IIf(IsDBNull(lector.Item("Per_NComercial")) = True, "", lector.Item("Per_NComercial")))
                    clie.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    clie.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                    clie.Dni = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                    clie.Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                    clie.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                    clie.Estado = CStr(lector.Item("per_estado"))
                    clie.IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                    Lista.Add(clie)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectGrillaCliente() As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ClienteSelectGrilla", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim clie As New Entidades.PersonaView
                    clie.IdPersona = CInt(lector.Item("IdPersona"))
                    clie.Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                    clie.NombreComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                    clie.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    clie.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                    clie.Telefeono = CStr(IIf(IsDBNull(lector.Item("tel_Numero")) = True, "", lector.Item("tel_Numero")))
                    clie.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                    Lista.Add(clie)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectGrillaClientexNombre(ByVal nombre As String, ByVal BuscaPor As Integer) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        'Dim cmd As New SqlCommand("_ClienteSelectxNombre", cn) 'Antes 3 columnas, ahora una combinada (NombreComercial/RazonSocial/Nombre)
        Dim cmd As New SqlCommand("_ClienteBuscarxParam", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Texto", nombre)
        cmd.Parameters.AddWithValue("@Opcion", BuscaPor)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim clie As New Entidades.PersonaView
                    clie.IdPersona = CInt(lector.Item("IdPersona"))
                    clie.Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                    clie.NombreComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                    clie.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    clie.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                    clie.Telefeono = CStr(IIf(IsDBNull(lector.Item("tel_Numero")) = True, "", lector.Item("tel_Numero")))
                    clie.Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                    Lista.Add(clie)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectGrillaClientexRazonSocial(ByVal nombre As String) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ClienteSelectxRazonSocial", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@jur_Rsocial", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim clie As New Entidades.PersonaView
                    clie.IdPersona = CInt(lector.Item("IdPersona"))
                    clie.Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                    clie.NombreComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                    clie.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    clie.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                    clie.Telefeono = CStr(IIf(IsDBNull(lector.Item("tel_Numero")) = True, "", lector.Item("tel_Numero")))
                    Lista.Add(clie)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectGrillaClientexDNI(ByVal numero As String) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ClienteSelectxDni", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Dni", numero)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim clie As New Entidades.PersonaView
                    clie.IdPersona = CInt(lector.Item("IdPersona"))
                    clie.Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                    clie.NombreComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                    clie.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    clie.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                    clie.Telefeono = CStr(IIf(IsDBNull(lector.Item("tel_Numero")) = True, "", lector.Item("tel_Numero")))
                    Lista.Add(clie)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectGrillaClientexRUC(ByVal numero As String) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ClienteSelectxRucLike", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Ruc", numero)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim clie As New Entidades.PersonaView
                    clie.IdPersona = CInt(lector.Item("IdPersona"))
                    clie.Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                    clie.NombreComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                    clie.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    clie.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                    clie.Telefeono = CStr(IIf(IsDBNull(lector.Item("tel_Numero")) = True, "", lector.Item("tel_Numero")))
                    Lista.Add(clie)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectGrillaClientexIdPersona(ByVal codigo As String) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ClienteSelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", codigo)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim clie As New Entidades.PersonaView
                    clie.IdPersona = CInt(lector.Item("IdPersona"))
                    clie.Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                    clie.NombreComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                    clie.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    clie.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                    clie.Telefeono = CStr(IIf(IsDBNull(lector.Item("tel_Numero")) = True, "", lector.Item("tel_Numero")))
                    Lista.Add(clie)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectGrillaClientexEstado(ByVal estado As String) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ClienteSelectxEstado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Estado", estado)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim clie As New Entidades.PersonaView
                    clie.IdPersona = CInt(lector.Item("IdPersona"))
                    clie.Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                    clie.NombreComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                    clie.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                    clie.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                    clie.Telefeono = CStr(IIf(IsDBNull(lector.Item("tel_Numero")) = True, "", lector.Item("tel_Numero")))
                    Lista.Add(clie)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function DataSetCxCResumenCliente(ByVal idEmpresa As Integer, ByVal idtienda As Integer, ByVal idmoneda As Integer) As DataSet
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim daC As SqlDataAdapter

        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_CuentasxCobrarResumenCliente", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idEmpresa", idEmpresa)
            cmd.Parameters.AddWithValue("@idtienda", idtienda)
            cmd.Parameters.AddWithValue("@idmoneda", idmoneda)

            '*********** cargo la data
            daC = New SqlDataAdapter(cmd)
            daC.Fill(ds, "ResumenCxCCliente")
        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function
    Public Function DataSetCxCResumenDocumento(ByVal idEmpresa As Integer, ByVal idtienda As Integer, ByVal idmoneda As Integer, ByVal idpersona As Integer) As DataSet
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim daD As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmdD As New SqlCommand("_CR_CuentasxCobrarResumenDocumento", cn)
            cmdD.CommandType = CommandType.StoredProcedure
            cmdD.Parameters.AddWithValue("@Idempresa", idEmpresa)
            cmdD.Parameters.AddWithValue("@idtienda", idtienda)
            cmdD.Parameters.AddWithValue("@idmoneda", idmoneda)
            cmdD.Parameters.AddWithValue("@idPersona", idpersona)
            daD = New SqlDataAdapter(cmdD)
            daD.Fill(ds, "ResumenCxCDocumento")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function
    Public Function RankingClientes(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal idCondicionpago As Integer, ByVal idmoneda As Integer, ByVal idTipopersona As Integer, ByVal fechaInicio As String, ByVal fechafin As String, ByVal nprimeros As Integer, ByVal idlinea As Integer, ByVal idsublinea As Integer) As DataSet
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim daD As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmdD As New SqlCommand("_CR_RankingClientes", cn)
            cmdD.CommandType = CommandType.StoredProcedure
            cmdD.Parameters.AddWithValue("@Idempresa", idempresa)
            cmdD.Parameters.AddWithValue("@idtienda", idtienda)
            cmdD.Parameters.AddWithValue("@idCondicionpago", idCondicionpago)
            cmdD.Parameters.AddWithValue("@idmoneda", idmoneda)
            cmdD.Parameters.AddWithValue("@idTipopersona", idTipopersona)
            cmdD.Parameters.AddWithValue("@fechainicio", fechaInicio)
            cmdD.Parameters.AddWithValue("@fechafin", fechafin)
            cmdD.Parameters.AddWithValue("@filas", nprimeros)
            cmdD.Parameters.AddWithValue("@idlinea", idlinea)
            cmdD.Parameters.AddWithValue("@idsublinea", idsublinea)

            daD = New SqlDataAdapter(cmdD)
            daD.Fill(ds, "DT_RankingClientes")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function
    Public Function DetalleGuiasRemision(ByVal idempresa As Integer, ByVal idAlmacenOrigen As Integer, ByVal idAlmacenDestino As Integer, ByVal fechaInicio As String, ByVal fechafin As String, ByVal vmercaTransito As Integer, ByVal IdTipoOperacion As Integer) As DataSet
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim daD As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim sp As String = GetProcedureGuiaRemision(vmercaTransito)
            Dim cmdD As New SqlCommand(sp, cn)
            cmdD.CommandType = CommandType.StoredProcedure
            cmdD.Parameters.AddWithValue("@idempresa", idempresa)
            cmdD.Parameters.AddWithValue("@idOrigen", idAlmacenOrigen)
            cmdD.Parameters.AddWithValue("@idDestino", idAlmacenDestino)

            cmdD.Parameters.AddWithValue("@fechainicio", fechaInicio)
            cmdD.Parameters.AddWithValue("@fechafin", fechafin)
            cmdD.Parameters.AddWithValue("@idtipooperacion", IdTipoOperacion)
            daD = New SqlDataAdapter(cmdD)
            daD.Fill(ds, "_DT_DetallesGuiasRemision")
        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function
    Private Function GetProcedureGuiaRemision(ByVal mercaTransito As Integer) As String
        Dim procedure As String

        If (mercaTransito = 0) Then 'todos, recepcionados y no
            procedure = "_CR_DetalleGuiaRemision"
        Else
            procedure = "_CR_DetalleGuiaRemisionTransito" 'no recepcionados(en transito)
        End If
        Return procedure
    End Function

    Public Function ReporteIngresosxTienda(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal fechaInicio As String, ByVal fechafin As String) As DataSet
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim daD As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmdD As New SqlCommand("_CR_IngresosXTienda", cn)
            cmdD.CommandType = CommandType.StoredProcedure
            cmdD.Parameters.AddWithValue("@idempresa", idempresa)
            cmdD.Parameters.AddWithValue("@idTienda", idempresa)
            cmdD.Parameters.AddWithValue("@fechainicio", fechaInicio)
            cmdD.Parameters.AddWithValue("@fechafin", fechafin)
            daD = New SqlDataAdapter(cmdD)
            daD.Fill(ds, "DT_IngresoxTienda")
        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function


    Public Function DetalleCxC(ByVal idpersona As Integer, ByVal vopcion As Integer, ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal FechaIni As String, ByVal FechaFin As String) As DataSet
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim daD As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmdD As New SqlCommand(getProcedCXC(vopcion), cn)
            cmdD.CommandType = CommandType.StoredProcedure
            cmdD.Parameters.AddWithValue("@idpersona", idpersona)
            cmdD.Parameters.AddWithValue("@idEmpresa", idempresa)
            cmdD.Parameters.AddWithValue("@idTienda", idtienda)
            cmdD.Parameters.AddWithValue("@FechaIni", FechaIni)
            cmdD.Parameters.AddWithValue("@FechaFin", FechaFin)

            daD = New SqlDataAdapter(cmdD)
            daD.Fill(ds, "DT_DetalleCxC")
        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function
    Public Function DetalleCxCDocCliente(ByVal idpersona As Integer, ByVal vopcion As Integer, ByVal idempresa As Integer, ByVal idtienda As Integer) As DataSet
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim daD As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmdD As New SqlCommand("_CR_CxCDocumentoXCliente", cn)
            cmdD.CommandType = CommandType.StoredProcedure
            cmdD.Parameters.AddWithValue("@idpersona", idpersona)
            cmdD.Parameters.AddWithValue("@idEmpresa", idempresa)
            cmdD.Parameters.AddWithValue("@idTienda", idtienda)
            cmdD.Parameters.AddWithValue("@xCancelar", vopcion)

            daD = New SqlDataAdapter(cmdD)
            daD.Fill(ds, "Dt_cxcDocxCliente")
        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function

    Private Function getProcedCXC(ByVal idopcion As Integer) As String
        Dim StoreProcedure As String = String.Empty

        Select Case idopcion
            Case 1
                StoreProcedure = "[_CR_CxCDocumento]"
            Case 2
                StoreProcedure = "[_CR_CxCDocumentoALL]"
            Case 3
                StoreProcedure = "[_CR_CxCDocumentoALLCancel]"
        End Select
        Return StoreProcedure
    End Function


    Public Function VentasXTienda(ByVal idempresa As Integer, ByVal idLinea As Integer, ByVal idSublinea As Integer, ByVal idProducto As Integer, ByVal fechaInicio As String, ByVal fechafin As String) As DataSet
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim daD As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmdD As New SqlCommand("[_CR_ResumenVentasxtiendas]", cn)
            cmdD.CommandType = CommandType.StoredProcedure
            cmdD.Parameters.AddWithValue("@Idempresa", idempresa)
            cmdD.Parameters.AddWithValue("@idLinea", idLinea)
            cmdD.Parameters.AddWithValue("@idSublinea", idSublinea)
            cmdD.Parameters.AddWithValue("@idProducto", idProducto)
            cmdD.Parameters.AddWithValue("@fechainicio", fechaInicio)
            cmdD.Parameters.AddWithValue("@fechafin", fechafin)
            daD = New SqlDataAdapter(cmdD)
            daD.Fill(ds, "DT_ResumenVentasxTienda")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function

    Public Function VentasXTiendaRSet(ByVal idempresa As Integer, ByVal idLinea As Integer, ByVal idSublinea As Integer, ByVal idProducto As Integer, ByVal fechaInicio As String, ByVal fechafin As String) As DataSet
        Dim daR As SqlDataAdapter
        Dim ds As New DataSet
        'Dim rcset As New ADODB.Recordset
        Dim objutil As New DAO.DAOUtil
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE

        Try
            Dim cmdD As New SqlCommand("[_CR_ResumenVentasxtiendasPvD]", cn)
            cmdD.CommandType = CommandType.StoredProcedure
            cmdD.Parameters.AddWithValue("@Idempresa", idempresa)
            cmdD.Parameters.AddWithValue("@idLinea", idLinea)
            cmdD.Parameters.AddWithValue("@idSublinea", idSublinea)
            cmdD.Parameters.AddWithValue("@idProducto", idProducto)
            cmdD.Parameters.AddWithValue("@fechainicio", fechaInicio)
            cmdD.Parameters.AddWithValue("@fechafin", fechafin)

            daR = New SqlDataAdapter(cmdD)

            daR.Fill(ds, "DT_Tienda")
            'rcset = objutil.ConvertToRecordset(ds.Tables("DT_Tienda"))

            'getOExcel(rcset, "Ventas de Articulo por Tienda")

        Catch ex As Exception
            Throw ex
            'rcset = Nothing
        Finally

        End Try
        Return ds
    End Function


    'metodo para testear
    Public Sub gettable(ByVal dt As DataTable)
        Try
            Dim tienda As String = ""

            For Each row As DataRow In dt.Rows
                tienda = row("Prod_Nombre").ToString
            Next

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
