﻿
'*************************   MIERCOLES 02 06 2010

Imports System.Data.SqlClient
Public Class DAOCatalogo

    Private ObjConexion As New DAO.Conexion
    Private objDaoMantenedor As New DAO.DAOMantenedor
    Public Function SelectxIdProductoDetalladoDescripcion(ByVal IdProducto As Integer) As Entidades.Catalogo
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim obj As New Entidades.Catalogo
        Try
            Dim cmd As New SqlCommand("SelectxIdProductoDetalladoDescripcion", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
            cmd.CommandTimeout = 0
            cn.Open()
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If (lector.HasRows = True) Then
                lector.Read()
                With obj
                    .NomLinea = CStr(IIf(IsDBNull(lector("Linea")), "", lector("Linea")))
                    .NomSubLinea = CStr(IIf(IsDBNull(lector("SubLinea")), "", lector("SubLinea")))
                    .Pais = CStr(IIf(IsDBNull(lector("OR1")), "", lector("OR1")))
                    .Formato = CStr(IIf(IsDBNull(lector("CO1")), "", lector("CO1")))
                    .Atributo1 = CStr(IIf(IsDBNull(lector("AT1")), "", lector("AT1")))
                    .IdProducto = CInt(IIf(IsDBNull(lector("IdProducto")), 0, lector("IdProducto")))
                End With
            Else
                obj = Nothing
            End If
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return obj

    End Function

    Public Function SelectProductoxIdProducto(ByVal IdProducto As Integer, ByVal IdTienda As Integer, ByVal IdTipoPV As Integer) As Entidades.Catalogo
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim obj As New Entidades.Catalogo
        Try
            Dim cmd As New SqlCommand("SelectProductoxIdProducto", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
            cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
            cmd.Parameters.AddWithValue("@IdTipoPV", IdTipoPV)
            cmd.CommandTimeout = 0
            cn.Open()
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If (lector.HasRows = True) Then
                lector.Read()
                With obj
                    .Descripcion = CStr(IIf(IsDBNull(lector("prod_Nombre")), "", lector("prod_Nombre")))
                    .IdProducto = CInt(IIf(IsDBNull(lector("IdProducto")), 0, lector("IdProducto")))
                    .CodigoProducto = CStr(IIf(IsDBNull(lector("prod_codigo")), "", lector("prod_codigo")))
                    .PrecioLista = CDec(IIf(IsDBNull(lector("Precio")), 0, lector("Precio")))
                    .Tienda = CStr(IIf(IsDBNull(lector("Tienda")), "", lector("Tienda")))
                    .Equivalencia = CStr(IIf(IsDBNull(lector("EquivalenciaCaja")), "", lector("EquivalenciaCaja")))
                    .NomTipoPV = CStr(IIf(IsDBNull(lector("NombrePV")), "", lector("NombrePV")))
                    .cadenaUM = CStr(IIf(IsDBNull(lector("UM")), "", lector("UM")))
                    .IsKit = CInt(IIf(IsDBNull(lector("prod_Kit")), 0, lector("prod_Kit")))
                End With
            Else
                obj = Nothing
            End If
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return obj

    End Function
    Public Function SelectProductoxCodigoxDescripcion(ByVal IdTienda As Integer, ByVal IdTipoPV As Integer, ByVal CodigoProducto As String, ByVal NombreProducto As String) As List(Of Entidades.Catalogo)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim lista As New List(Of Entidades.Catalogo)
        Try
            Dim cmd As New SqlCommand("SelectProductoxCodigoxDescripcion", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
            cmd.Parameters.AddWithValue("@IdTipoPV", IdTipoPV)
            cmd.Parameters.AddWithValue("@ProdNombre", NombreProducto)
            cmd.Parameters.AddWithValue("@CodigoProducto", CodigoProducto)
            cmd.CommandTimeout = 0
            cn.Open()
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If (lector.HasRows = True) Then
                While (lector.Read)
                    Dim obj As New Entidades.Catalogo
                    With obj
                        .Descripcion = CStr(IIf(IsDBNull(lector("prod_Nombre")), "", lector("prod_Nombre")))
                        .IdProducto = CInt(IIf(IsDBNull(lector("IdProducto")), 0, lector("IdProducto")))
                        .CodigoProducto = CStr(IIf(IsDBNull(lector("prod_codigo")), "", lector("prod_codigo")))
                        .PrecioLista = CDec(IIf(IsDBNull(lector("Precio")), 0, lector("Precio")))
                        .Tienda = CStr(IIf(IsDBNull(lector("Tienda")), "", lector("Tienda")))
                        .NomTipoPV = CStr(IIf(IsDBNull(lector("NombrePV")), "", lector("NombrePV")))
                        .IdTienda = IdTienda
                        .IdTipoPV = IdTipoPV
                    End With
                    lista.Add(obj)
                End While
            Else
                lista = Nothing
            End If
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return lista

    End Function


    Public Function Producto_Consultar_Stock_Precio_Venta(ByVal IdProducto As Integer, ByVal IdTienda As Integer, ByVal IdTipoPV As Integer, ByVal IdMonedaDestino As Integer, ByVal Fecha As Date, ByVal IdEmpresa As Integer, ByVal IdTipoAlmancen As Integer) As List(Of Entidades.Catalogo)

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim lista As New List(Of Entidades.Catalogo)
        Try

            Dim cmd As New SqlCommand("_Producto_Consultar_Stock_Precio_Venta", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
            cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
            cmd.Parameters.AddWithValue("@IdTipoPV", IdTipoPV)
            cmd.Parameters.AddWithValue("@IdMonedaDestino", IdMonedaDestino)
            cmd.Parameters.AddWithValue("@Fecha", Fecha)
            cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
            cmd.Parameters.AddWithValue("@IdTipoAlmancen", IdTipoAlmancen)
            cn.Open()
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            While (lector.Read)
                Dim obj As New Entidades.Catalogo
                With obj
                    .cadenaUM = CStr(IIf(IsDBNull(lector("UM_Principal")), "", lector("UM_Principal")))
                    .PrecioLista = CDec(IIf(IsDBNull(lector("PrecioLista")), 0, lector("PrecioLista")))
                    .SimbMoneda = CStr(IIf(IsDBNull(lector("MonedaDestino")), "", lector("MonedaDestino")))
                    .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")), 0, lector("IdTienda")))
                    .Tienda = CStr(IIf(IsDBNull(lector("tie_Nombre")), "", lector("tie_Nombre")))
                    .IdAlmacen = CInt(IIf(IsDBNull(lector("IdAlmacenPrincipal")), 0, lector("IdAlmacenPrincipal")))
                    .Almacen = CStr(IIf(IsDBNull(lector("AlmacenPrincipal")), "", lector("AlmacenPrincipal")))
                    .IdUnidadMedida = CInt(IIf(IsDBNull(lector("IdUnidadMedidaPrincipal")), 0, lector("IdUnidadMedidaPrincipal")))
                    .StockAReal = CDec(IIf(IsDBNull(lector("StockReal")), 0, lector("StockReal")))
                    .StockComprometido = CDec(IIf(IsDBNull(lector("StockComprometido")), 0, lector("StockComprometido")))
                    .StockDisponibleN = CDec(IIf(IsDBNull(lector("StockDisponible")), 0, lector("StockDisponible")))
                    .StockEnTransito = CDec(IIf(IsDBNull(lector("StockEnTransito")), 0, lector("StockEnTransito")))
                End With
                lista.Add(obj)
            End While
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return lista

    End Function




    Public Function Producto_Consultar_Stock_Precio_Venta2(ByVal IdProducto As Integer, ByVal IdTienda As Integer, ByVal IdTipoPV As Integer, ByVal IdMonedaDestino As Integer, ByVal Fecha As Date, ByVal IdEmpresa As Integer, ByVal IdTipoAlmancen As Integer) As List(Of Entidades.Catalogo)

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE2
        Dim lista As New List(Of Entidades.Catalogo)
        Try

            Dim cmd As New SqlCommand("_Producto_Consultar_Stock_Precio_Venta", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
            cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
            cmd.Parameters.AddWithValue("@IdTipoPV", IdTipoPV)
            cmd.Parameters.AddWithValue("@IdMonedaDestino", IdMonedaDestino)
            cmd.Parameters.AddWithValue("@Fecha", Fecha)
            cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
            cmd.Parameters.AddWithValue("@IdTipoAlmancen", IdTipoAlmancen)
            cn.Open()
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            While (lector.Read)
                Dim obj As New Entidades.Catalogo
                With obj
                    .cadenaUM = CStr(IIf(IsDBNull(lector("UM_Principal")), "", lector("UM_Principal")))
                    .PrecioLista = CDec(IIf(IsDBNull(lector("PrecioLista")), 0, lector("PrecioLista")))
                    .SimbMoneda = CStr(IIf(IsDBNull(lector("MonedaDestino")), "", lector("MonedaDestino")))
                    .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")), 0, lector("IdTienda")))
                    .Tienda = CStr(IIf(IsDBNull(lector("tie_Nombre")), "", lector("tie_Nombre")))
                    .IdAlmacen = CInt(IIf(IsDBNull(lector("IdAlmacenPrincipal")), 0, lector("IdAlmacenPrincipal")))
                    .Almacen = CStr(IIf(IsDBNull(lector("AlmacenPrincipal")), "", lector("AlmacenPrincipal")))
                    .IdUnidadMedida = CInt(IIf(IsDBNull(lector("IdUnidadMedidaPrincipal")), 0, lector("IdUnidadMedidaPrincipal")))
                    .StockAReal = CDec(IIf(IsDBNull(lector("StockReal")), 0, lector("StockReal")))
                    .StockComprometido = CDec(IIf(IsDBNull(lector("StockComprometido")), 0, lector("StockComprometido")))
                    .StockDisponibleN = CDec(IIf(IsDBNull(lector("StockDisponible")), 0, lector("StockDisponible")))
                    .StockEnTransito = CDec(IIf(IsDBNull(lector("StockEnTransito")), 0, lector("StockEnTransito")))
                End With
                lista.Add(obj)
            End While
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return lista

    End Function

    Public Function CatalogoProductoSelectxParams(ByVal IdProducto As Integer, ByVal IdTienda As Integer, ByVal IdTipoPV As Integer, ByVal IdMonedaDestino As Integer, ByVal IdAlmacen As Integer, Optional ByVal addListaUM_Venta As Boolean = False) As Entidades.Catalogo

        Dim objCatalogo As Entidades.Catalogo = Nothing
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoTipoPVCatalogoxIds_Ventas", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdTipoPV", IdTipoPV)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdMonedaDestino", IdMonedaDestino)
        cn.Open()
        Dim lector As SqlDataReader = cmd.ExecuteReader()
        If lector.Read Then
            objCatalogo = New Entidades.Catalogo
            With objCatalogo
                .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")) = True, 0, lector("IdTienda")))
                .IdProducto = CInt(IIf(IsDBNull(lector("IdProducto")) = True, 0, lector("IdProducto")))
                .IdUnidadMedida = CInt(IIf(IsDBNull(lector("IdUnidadMedida")) = True, 0, lector("IdUnidadMedida")))
                .IdTipoPV = CInt(IIf(IsDBNull(lector("IdTipoPV")) = True, 0, lector("IdTipoPV")))
                .Descripcion = CStr(IIf(IsDBNull(lector("prod_Nombre")) = True, "", lector("prod_Nombre")))
                .PrecioSD = CDec(IIf(IsDBNull(lector("ValorDestino")) = True, 0, lector("ValorDestino")))
                .StockAReal = CDec(IIf(IsDBNull(lector("StockReal")) = True, 0, lector("StockReal")))
                .StockComprometido = CDec(IIf(IsDBNull(lector("StockComprometido")) = True, 0, lector("StockComprometido")))
                .IdUnidadMedida = CInt(IIf(IsDBNull(lector("IdUnidadMedida")) = True, 0, lector("IdUnidadMedida")))
                .Percepcion = CDec(IIf(IsDBNull(lector("Percepcion")) = True, 0, lector("Percepcion")))
                .Detraccion = CDec(IIf(IsDBNull(lector("Detraccion")) = True, 0, lector("Detraccion")))
            End With
        End If
        lector.Close()
        If ((addListaUM_Venta) And (objCatalogo IsNot Nothing)) Then
            objCatalogo.ListaUM_Venta = (New DAO.DAOProductoTipoPV).SelectUM_Venta(IdProducto, IdTienda, IdTipoPV)
        End If
        If cn.State = ConnectionState.Open Then cn.Close()
        Return objCatalogo
    End Function

    Public Function CatalogoProducto_PV_Paginado(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As Integer, ByVal Descripcion As String, ByVal IdTienda As Integer, ByVal IdMonedaDestino As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal idtipopv As Integer, ByVal pagesize As Integer, ByVal pagenumber As Integer) As List(Of Entidades.Catalogo)
        Descripcion = Descripcion.Replace("*", "%")

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE

        Dim cmd As New SqlCommand("_CatalogoProducto_PV_Paginado", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@CodSubLinea", CodSubLinea)
        cmd.Parameters.AddWithValue("@Descripcion", Descripcion)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdMoneda", IdMonedaDestino)
        cmd.Parameters.AddWithValue("@IdTipoPV", idtipopv)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)

        cmd.Parameters.AddWithValue("@PageNumber", pagenumber)
        cmd.Parameters.AddWithValue("@PageSize", pagesize)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Catalogo)
                Do While lector.Read
                    Dim objCatalogo As New Entidades.Catalogo

                    With objCatalogo
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .Descripcion = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .NomLinea = CStr(IIf(IsDBNull(lector.Item("NomLinea")) = True, "", lector.Item("NomLinea")))
                        .NomSubLinea = CStr(IIf(IsDBNull(lector.Item("NomSubLinea")) = True, "", lector.Item("NomSubLinea")))
                        .SimbMoneda = CStr(IIf(IsDBNull(lector.Item("MonedaDestino")) = True, "", lector.Item("MonedaDestino")))
                        .PrecioSD = CDec(IIf(IsDBNull(lector.Item("ValorFinal")) = True, 0, lector.Item("ValorFinal")))
                        .Percepcion = CDec(IIf(IsDBNull(lector.Item("Percepcion")) = True, 0, lector.Item("Percepcion")))
                        .Detraccion = CDec(IIf(IsDBNull(lector.Item("Detraccion")) = True, 0, lector.Item("Detraccion")))
                        .StockAReal = CDec(IIf(IsDBNull(lector.Item("StockReal")) = True, 0, lector.Item("StockReal")))
                        .StockComprometido = CDec(IIf(IsDBNull(lector.Item("StockComprometido")) = True, 0, lector.Item("StockComprometido")))
                        .IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .NomTipoPV = CStr(IIf(IsDBNull(lector.Item("NomTipoPV")) = True, "", lector.Item("NomTipoPV")))
                        .CodigoSubLinea = CStr(IIf(IsDBNull(lector.Item("sl_Codigo")) = True, 0, lector.Item("sl_Codigo")))
                    End With

                    Lista.Add(objCatalogo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function


    Public Function CatalogoProducto_PV_Paginado_Cotizacion(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As String, ByVal Descripcion As String, ByVal IdTienda As Integer, ByVal IdMonedaDestino As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal idtipopv As Integer, ByVal pagesize As Integer, ByVal pagenumber As Integer, ByVal IdUsuario As Integer, ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer, ByVal IdCliente As Integer, ByVal IdPais As Integer, ByVal IdFabricante As Integer, ByVal IdMarca As Integer, ByVal IdModelo As Integer, ByVal IdFormato As Integer, ByVal IdProveedor As Integer, ByVal IdEstilo As Integer, ByVal IdTransito As Integer, ByVal IdCalidad As Integer) As List(Of Entidades.Catalogo)
        Descripcion = Descripcion.Replace("*", "%")

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE

        Dim cmd As New SqlCommand("CatalogoProducto_PV_Paginado_Cotizacion", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@CodSubLinea", CodSubLinea)
        cmd.Parameters.AddWithValue("@Descripcion", Descripcion)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdMoneda", IdMonedaDestino)
        cmd.Parameters.AddWithValue("@IdTipoPV", idtipopv)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)

        cmd.Parameters.AddWithValue("@PageNumber", pagenumber)
        cmd.Parameters.AddWithValue("@PageSize", pagesize)

        cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario)
        cmd.Parameters.AddWithValue("@IdCondicionPago", IdCondicionPago)
        cmd.Parameters.AddWithValue("@IdMedioPago", IdMedioPago)
        cmd.Parameters.AddWithValue("@IdCliente", IdCliente)

        cmd.Parameters.AddWithValue("@IdPais", IdPais)
        cmd.Parameters.AddWithValue("@IdFabricante", IdFabricante)
        cmd.Parameters.AddWithValue("@IdMarca", IdMarca)
        cmd.Parameters.AddWithValue("@IdModelo", IdModelo)
        cmd.Parameters.AddWithValue("@IdFormato", IdFormato)
        cmd.Parameters.AddWithValue("@IdProveedor", IdProveedor)
        cmd.Parameters.AddWithValue("@IdEstilo", IdEstilo)
        cmd.Parameters.AddWithValue("@IdTransito", IdTransito)
        cmd.Parameters.AddWithValue("@IdCalidad", IdCalidad)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Catalogo)
                Do While lector.Read
                    Dim objCatalogo As New Entidades.Catalogo

                    With objCatalogo
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .Descripcion = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .NomLinea = CStr(IIf(IsDBNull(lector.Item("NomLinea")) = True, "", lector.Item("NomLinea")))
                        .NomSubLinea = CStr(IIf(IsDBNull(lector.Item("NomSubLinea")) = True, "", lector.Item("NomSubLinea")))
                        .SimbMoneda = CStr(IIf(IsDBNull(lector.Item("MonedaDestino")) = True, "", lector.Item("MonedaDestino")))
                        .PrecioSD = CDec(IIf(IsDBNull(lector.Item("ValorFinal")) = True, 0, lector.Item("ValorFinal")))
                        .PrecioLista = CDec(IIf(IsDBNull(lector.Item("ValorFinal")) = True, 0, lector.Item("ValorFinal")))
                        .Percepcion = CDec(IIf(IsDBNull(lector.Item("Percepcion")) = True, 0, lector.Item("Percepcion")))
                        .Detraccion = CDec(IIf(IsDBNull(lector.Item("Detraccion")) = True, 0, lector.Item("Detraccion")))
                        .IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .NomTipoPV = CStr(IIf(IsDBNull(lector.Item("NomTipoPV")) = True, "", lector.Item("NomTipoPV")))
                        .CodigoSubLinea = CStr(IIf(IsDBNull(lector.Item("CodigoSubLinea")) = True, 0, lector.Item("CodigoSubLinea")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacenRef")) = True, 0, lector.Item("IdAlmacenRef")))
                        .Almacen = CStr(IIf(IsDBNull(lector.Item("Almacen")) = True, "", lector.Item("Almacen")))
                        .PorcentDctoMaximo = CDec(IIf(IsDBNull(lector.Item("PorcentDctoMaximo")) = True, 0, lector.Item("PorcentDctoMaximo")))
                        .StockDisponibleN = CDec(IIf(IsDBNull(lector.Item("StockDisponible")) = True, 0, lector.Item("StockDisponible")))

                        .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("CodigoProducto")) = True, "", lector.Item("CodigoProducto")))

                    End With

                    Lista.Add(objCatalogo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function CatalogoProducto_PV_Paginado_Cotizacion_V2(ByVal IdExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As String, ByVal Descripcion As String, ByVal IdTienda As Integer, ByVal IdMonedaDestino As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal idtipopv As Integer, ByVal pagesize As Integer, ByVal pagenumber As Integer, ByVal IdUsuario As Integer, ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer, ByVal IdCliente As Integer, ByVal tableTipoTabla As DataTable, ByVal codigoProducto As String, ByVal filtroProductoCampania As Boolean) As List(Of Entidades.Catalogo)
        Descripcion = Descripcion.Replace("*", "%")
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("CatalogoProducto_PV_Paginado_Cotizacion_V2", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoExistencia", IdExistencia)
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@CodSubLinea", CodSubLinea)
        cmd.Parameters.AddWithValue("@Descripcion", Descripcion)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdMoneda", IdMonedaDestino)
        cmd.Parameters.AddWithValue("@IdTipoPV", idtipopv)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)

        cmd.Parameters.AddWithValue("@PageNumber", pagenumber)
        cmd.Parameters.AddWithValue("@PageSize", pagesize)

        cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario)
        cmd.Parameters.AddWithValue("@IdCondicionPago", IdCondicionPago)
        cmd.Parameters.AddWithValue("@IdMedioPago", IdMedioPago)
        cmd.Parameters.AddWithValue("@IdCliente", IdCliente)

        cmd.Parameters.AddWithValue("@Tabla", tableTipoTabla)
        cmd.Parameters.AddWithValue("@CodigoProducto", codigoProducto)
        cmd.Parameters.AddWithValue("@filtroProductoCampania", filtroProductoCampania)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Catalogo)
                Do While lector.Read
                    Dim objCatalogo As New Entidades.Catalogo

                    With objCatalogo
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .Descripcion = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .NomLinea = CStr(IIf(IsDBNull(lector.Item("NomLinea")) = True, "", lector.Item("NomLinea")))
                        .NomSubLinea = CStr(IIf(IsDBNull(lector.Item("NomSubLinea")) = True, "", lector.Item("NomSubLinea")))
                        .SimbMoneda = CStr(IIf(IsDBNull(lector.Item("MonedaDestino")) = True, "", lector.Item("MonedaDestino")))
                        .PrecioSD = CDec(IIf(IsDBNull(lector.Item("ValorFinal")) = True, 0, lector.Item("ValorFinal")))
                        .PrecioLista = CDec(IIf(IsDBNull(lector.Item("ValorFinal")) = True, 0, lector.Item("ValorFinal")))
                        .Percepcion = CDec(IIf(IsDBNull(lector.Item("Percepcion")) = True, 0, lector.Item("Percepcion")))
                        .Detraccion = CDec(IIf(IsDBNull(lector.Item("Detraccion")) = True, 0, lector.Item("Detraccion")))
                        .IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .NomTipoPV = CStr(IIf(IsDBNull(lector.Item("NomTipoPV")) = True, "", lector.Item("NomTipoPV")))
                        .CodigoSubLinea = CStr(IIf(IsDBNull(lector.Item("CodigoSubLinea")) = True, 0, lector.Item("CodigoSubLinea")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacenRef")) = True, 0, lector.Item("IdAlmacenRef")))
                        .Almacen = CStr(IIf(IsDBNull(lector.Item("Almacen")) = True, "", lector.Item("Almacen")))

                        .PorcentDctoMaximo = CDec(IIf(IsDBNull(lector.Item("PorcentDctoMaximo")) = True, 0, lector.Item("PorcentDctoMaximo")))
                        .PrecioBaseDcto = CStr(IIf(IsDBNull(lector.Item("PrecioBaseDcto")) = True, "", lector.Item("PrecioBaseDcto")))

                        .StockDisponibleN = CDec(IIf(IsDBNull(lector.Item("StockDisponible")) = True, 0, lector.Item("StockDisponible")))
                        .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("CodigoProducto")) = True, "", lector.Item("CodigoProducto")))
                        .pvComercial = CDec(IIf(IsDBNull(lector.Item("ppv_Valor")) = True, 0, lector.Item("ppv_Valor")))
                        .Kit = CBool(IIf(IsDBNull(lector.Item("prod_Kit")) = True, 0, lector.Item("prod_Kit")))
                        .ExisteCampania_Producto = objDaoMantenedor.UCBool(lector("ExisteCampania_Producto"))

                    End With

                    Lista.Add(objCatalogo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function
    'busqueda desde la capa de producto pero parametro con codigo de barras
    Public Function CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarras(ByVal IdLinea As Integer, _
                                               ByVal IdSubLinea As Integer, _
                                               ByVal CodSubLinea As String, _
                                               ByVal Descripcion As String, _
                                               ByVal IdTienda As Integer, _
                                               ByVal IdMonedaDestino As Integer, _
                                               ByVal IdEmpresa As Integer, _
                                               ByVal IdAlmacen As Integer, _
                                               ByVal idtipopv As Integer, _
                                               ByVal pagesize As Integer, _
                                               ByVal pagenumber As Integer, _
                                               ByVal IdUsuario As Integer, _
                                               ByVal IdCondicionPago As Integer, _
                                               ByVal IdMedioPago As Integer, _
                                               ByVal IdCliente As Integer, _
                                               ByVal tableTipoTabla As DataTable, _
                                               ByVal codigoProducto As String, _
                                               ByVal filtroProductoCampania As Boolean, _
                                               ByVal codbarras As String, _
                                               ByVal IdTipoExistencia As Integer, _
                                               ByVal codigoProveedor As String) As List(Of Entidades.Catalogo)



        Descripcion = Descripcion.Replace("*", "%")

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarrasCliente_V2", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoExistencia", IdTipoExistencia)
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@CodSubLinea", CodSubLinea)
        cmd.Parameters.AddWithValue("@Descripcion", Descripcion)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdMoneda", IdMonedaDestino)
        cmd.Parameters.AddWithValue("@IdTipoPV", idtipopv)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)

        cmd.Parameters.AddWithValue("@PageNumber", pagenumber)
        cmd.Parameters.AddWithValue("@PageSize", pagesize)

        cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario)
        cmd.Parameters.AddWithValue("@IdCondicionPago", IdCondicionPago)
        cmd.Parameters.AddWithValue("@IdMedioPago", IdMedioPago)
        cmd.Parameters.AddWithValue("@IdCliente", IdCliente)

        cmd.Parameters.AddWithValue("@Tabla", tableTipoTabla)
        cmd.Parameters.AddWithValue("@CodigoProducto", codigoProducto)
        cmd.Parameters.AddWithValue("@filtroProductoCampania", filtroProductoCampania)
        cmd.Parameters.AddWithValue("@codbarras", codbarras)
        cmd.Parameters.AddWithValue("@CodigoProveedor", codigoProveedor)


        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.IsolationLevel.ReadUncommitted)
                Dim Lista As New List(Of Entidades.Catalogo)
                Do While lector.Read
                    Dim objCatalogo As New Entidades.Catalogo

                    With objCatalogo
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .Descripcion = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .NomLinea = CStr(IIf(IsDBNull(lector.Item("NomLinea")) = True, "", lector.Item("NomLinea")))
                        .NomSubLinea = CStr(IIf(IsDBNull(lector.Item("NomSubLinea")) = True, "", lector.Item("NomSubLinea")))
                        .SimbMoneda = CStr(IIf(IsDBNull(lector.Item("MonedaDestino")) = True, "", lector.Item("MonedaDestino")))
                        .PrecioSD = CDec(IIf(IsDBNull(lector.Item("ValorFinal")) = True, 0, lector.Item("ValorFinal")))
                        .PrecioLista = CDec(IIf(IsDBNull(lector.Item("ValorFinal")) = True, 0, lector.Item("ValorFinal")))
                        .Percepcion = CDec(IIf(IsDBNull(lector.Item("Percepcion")) = True, 0, lector.Item("Percepcion")))
                        .Detraccion = CDec(IIf(IsDBNull(lector.Item("Detraccion")) = True, 0, lector.Item("Detraccion")))
                        .IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .NomTipoPV = CStr(IIf(IsDBNull(lector.Item("NomTipoPV")) = True, "", lector.Item("NomTipoPV")))
                        .CodigoSubLinea = CStr(IIf(IsDBNull(lector.Item("CodigoSubLinea")) = True, 0, lector.Item("CodigoSubLinea")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacenRef")) = True, 0, lector.Item("IdAlmacenRef")))
                        .Almacen = CStr(IIf(IsDBNull(lector.Item("Almacen")) = True, "", lector.Item("Almacen")))

                        .PorcentDctoMaximo = CDec(IIf(IsDBNull(lector.Item("PorcentDctoMaximo")) = True, 0, lector.Item("PorcentDctoMaximo")))
                        .PrecioBaseDcto = CStr(IIf(IsDBNull(lector.Item("PrecioBaseDcto")) = True, "", lector.Item("PrecioBaseDcto")))

                        .StockDisponibleN = CDec(IIf(IsDBNull(lector.Item("StockDisponible")) = True, 0, lector.Item("StockDisponible")))
                        .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("CodigoProducto")) = True, "", lector.Item("CodigoProducto")))
                        .pvComercial = CDec(IIf(IsDBNull(lector.Item("ppv_Valor")) = True, 0, lector.Item("ppv_Valor")))
                        .Kit = CBool(IIf(IsDBNull(lector.Item("prod_Kit")) = True, 0, lector.Item("prod_Kit")))
                        .ExisteCampania_Producto = objDaoMantenedor.UCBool(lector("ExisteCampania_Producto"))
                        .Prod_CodigoBarras = CStr(IIf(IsDBNull(lector.Item("prod_CodigoBarras")) = True, "", lector.Item("prod_CodigoBarras")))
                        .NoVisible = CStr(IIf(IsDBNull(lector.Item("NoVisible")) = True, "", lector.Item("NoVisible")))
                    End With

                    Lista.Add(objCatalogo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarras2(ByVal IdLinea As Integer, _
                                               ByVal IdSubLinea As Integer, _
                                               ByVal CodSubLinea As String, _
                                               ByVal Descripcion As String, _
                                               ByVal IdTienda As Integer, _
                                               ByVal IdMonedaDestino As Integer, _
                                               ByVal IdEmpresa As Integer, _
                                               ByVal IdAlmacen As Integer, _
                                               ByVal idtipopv As Integer, _
                                               ByVal pagesize As Integer, _
                                               ByVal pagenumber As Integer, _
                                               ByVal IdUsuario As Integer, _
                                               ByVal IdCondicionPago As Integer, _
                                               ByVal IdMedioPago As Integer, _
                                               ByVal IdCliente As Integer, _
                                               ByVal tableTipoTabla As DataTable, _
                                               ByVal codigoProducto As String, _
                                               ByVal filtroProductoCampania As Boolean, _
                                               ByVal codbarras As String, _
                                               ByVal IdTipoExistencia As Integer, _
                                               ByVal codigoProveedor As String) As List(Of Entidades.Catalogo)



        Descripcion = Descripcion.Replace("*", "%")

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE2
        Dim cmd As New SqlCommand("CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarrasCliente_V2", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoExistencia", IdTipoExistencia)
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@CodSubLinea", CodSubLinea)
        cmd.Parameters.AddWithValue("@Descripcion", Descripcion)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdMoneda", IdMonedaDestino)
        cmd.Parameters.AddWithValue("@IdTipoPV", idtipopv)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)

        cmd.Parameters.AddWithValue("@PageNumber", pagenumber)
        cmd.Parameters.AddWithValue("@PageSize", pagesize)

        cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario)
        cmd.Parameters.AddWithValue("@IdCondicionPago", IdCondicionPago)
        cmd.Parameters.AddWithValue("@IdMedioPago", IdMedioPago)
        cmd.Parameters.AddWithValue("@IdCliente", IdCliente)

        cmd.Parameters.AddWithValue("@Tabla", tableTipoTabla)
        cmd.Parameters.AddWithValue("@CodigoProducto", codigoProducto)
        cmd.Parameters.AddWithValue("@filtroProductoCampania", filtroProductoCampania)
        cmd.Parameters.AddWithValue("@codbarras", codbarras)
        cmd.Parameters.AddWithValue("@CodigoProveedor", codigoProveedor)


        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.IsolationLevel.ReadUncommitted)
                Dim Lista As New List(Of Entidades.Catalogo)
                Do While lector.Read
                    Dim objCatalogo As New Entidades.Catalogo

                    With objCatalogo
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .Descripcion = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .NomLinea = CStr(IIf(IsDBNull(lector.Item("NomLinea")) = True, "", lector.Item("NomLinea")))
                        .NomSubLinea = CStr(IIf(IsDBNull(lector.Item("NomSubLinea")) = True, "", lector.Item("NomSubLinea")))
                        .SimbMoneda = CStr(IIf(IsDBNull(lector.Item("MonedaDestino")) = True, "", lector.Item("MonedaDestino")))
                        .PrecioSD = CDec(IIf(IsDBNull(lector.Item("ValorFinal")) = True, 0, lector.Item("ValorFinal")))
                        .PrecioLista = CDec(IIf(IsDBNull(lector.Item("ValorFinal")) = True, 0, lector.Item("ValorFinal")))
                        .Percepcion = CDec(IIf(IsDBNull(lector.Item("Percepcion")) = True, 0, lector.Item("Percepcion")))
                        .Detraccion = CDec(IIf(IsDBNull(lector.Item("Detraccion")) = True, 0, lector.Item("Detraccion")))
                        .IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .NomTipoPV = CStr(IIf(IsDBNull(lector.Item("NomTipoPV")) = True, "", lector.Item("NomTipoPV")))
                        .CodigoSubLinea = CStr(IIf(IsDBNull(lector.Item("CodigoSubLinea")) = True, 0, lector.Item("CodigoSubLinea")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacenRef")) = True, 0, lector.Item("IdAlmacenRef")))
                        .Almacen = CStr(IIf(IsDBNull(lector.Item("Almacen")) = True, "", lector.Item("Almacen")))

                        .PorcentDctoMaximo = CDec(IIf(IsDBNull(lector.Item("PorcentDctoMaximo")) = True, 0, lector.Item("PorcentDctoMaximo")))
                        .PrecioBaseDcto = CStr(IIf(IsDBNull(lector.Item("PrecioBaseDcto")) = True, "", lector.Item("PrecioBaseDcto")))

                        .StockDisponibleN = CDec(IIf(IsDBNull(lector.Item("StockDisponible")) = True, 0, lector.Item("StockDisponible")))
                        .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("CodigoProducto")) = True, "", lector.Item("CodigoProducto")))
                        .pvComercial = CDec(IIf(IsDBNull(lector.Item("ppv_Valor")) = True, 0, lector.Item("ppv_Valor")))
                        .Kit = CBool(IIf(IsDBNull(lector.Item("prod_Kit")) = True, 0, lector.Item("prod_Kit")))
                        .ExisteCampania_Producto = objDaoMantenedor.UCBool(lector("ExisteCampania_Producto"))
                        .Prod_CodigoBarras = CStr(IIf(IsDBNull(lector.Item("prod_CodigoBarras")) = True, "", lector.Item("prod_CodigoBarras")))
                        .NoVisible = CStr(IIf(IsDBNull(lector.Item("NoVisible")) = True, "", lector.Item("NoVisible")))
                    End With

                    Lista.Add(objCatalogo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function
    Public Function CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarrasAvanzado(ByVal IdLinea As Integer, _
                                                 ByVal IdSubLinea As Integer, _
                                                 ByVal CodSubLinea As String, _
                                                 ByVal Descripcion As String, _
                                                 ByVal IdTienda As Integer, _
                                                 ByVal IdMonedaDestino As Integer, _
                                                 ByVal IdEmpresa As Integer, _
                                                 ByVal IdAlmacen As Integer, _
                                                 ByVal idtipopv As Integer, _
                                                 ByVal pagesize As Integer, _
                                                 ByVal pagenumber As Integer, _
                                                 ByVal IdUsuario As Integer, _
                                                 ByVal IdCondicionPago As Integer, _
                                                 ByVal IdMedioPago As Integer, _
                                                 ByVal IdCliente As Integer, _
                                                 ByVal tableTipoTabla As DataTable, _
                                                 ByVal codigoProducto As String, _
                                                 ByVal filtroProductoCampania As Boolean, _
                                                 ByVal codbarras As String, _
                                                 ByVal IdTipoExistencia As Integer, _
                                                 ByVal codigoProveedor As String, _
                                                 ByVal IdTipoOperacion As Integer) As List(Of Entidades.Catalogo)



        Descripcion = Descripcion.Replace("*", "%")

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarrasAvanzado", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoExistencia", IdTipoExistencia)
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@CodSubLinea", CodSubLinea)
        cmd.Parameters.AddWithValue("@Descripcion", Descripcion)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdMoneda", IdMonedaDestino)
        cmd.Parameters.AddWithValue("@IdTipoPV", idtipopv)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@PageNumber", pagenumber)
        cmd.Parameters.AddWithValue("@PageSize", pagesize)
        cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario)
        cmd.Parameters.AddWithValue("@IdCondicionPago", IdCondicionPago)
        cmd.Parameters.AddWithValue("@IdMedioPago", IdMedioPago)
        cmd.Parameters.AddWithValue("@IdCliente", IdCliente)
        cmd.Parameters.AddWithValue("@Tabla", tableTipoTabla)
        cmd.Parameters.AddWithValue("@CodigoProducto", codigoProducto)
        cmd.Parameters.AddWithValue("@filtroProductoCampania", filtroProductoCampania)
        cmd.Parameters.AddWithValue("@codbarras", codbarras)
        cmd.Parameters.AddWithValue("@CodigoProveedor", codigoProveedor)
        cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion)




        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Catalogo)
                Do While lector.Read
                    Dim objCatalogo As New Entidades.Catalogo

                    With objCatalogo
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .Descripcion = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .NomLinea = CStr(IIf(IsDBNull(lector.Item("NomLinea")) = True, "", lector.Item("NomLinea")))
                        .NomSubLinea = CStr(IIf(IsDBNull(lector.Item("NomSubLinea")) = True, "", lector.Item("NomSubLinea")))
                        .SimbMoneda = CStr(IIf(IsDBNull(lector.Item("MonedaDestino")) = True, "", lector.Item("MonedaDestino")))
                        .PrecioSD = CDec(IIf(IsDBNull(lector.Item("ValorFinal")) = True, 0, lector.Item("ValorFinal")))
                        .PrecioLista = CDec(IIf(IsDBNull(lector.Item("ValorFinal")) = True, 0, lector.Item("ValorFinal")))
                        .Percepcion = CDec(IIf(IsDBNull(lector.Item("Percepcion")) = True, 0, lector.Item("Percepcion")))
                        .Detraccion = CDec(IIf(IsDBNull(lector.Item("Detraccion")) = True, 0, lector.Item("Detraccion")))
                        .IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .NomTipoPV = CStr(IIf(IsDBNull(lector.Item("NomTipoPV")) = True, "", lector.Item("NomTipoPV")))
                        .CodigoSubLinea = CStr(IIf(IsDBNull(lector.Item("CodigoSubLinea")) = True, 0, lector.Item("CodigoSubLinea")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacenRef")) = True, 0, lector.Item("IdAlmacenRef")))
                        .Almacen = CStr(IIf(IsDBNull(lector.Item("Almacen")) = True, "", lector.Item("Almacen")))

                        .PorcentDctoMaximo = CDec(IIf(IsDBNull(lector.Item("PorcentDctoMaximo")) = True, 0, lector.Item("PorcentDctoMaximo")))
                        .PrecioBaseDcto = CStr(IIf(IsDBNull(lector.Item("PrecioBaseDcto")) = True, "", lector.Item("PrecioBaseDcto")))

                        .StockDisponibleN = CDec(IIf(IsDBNull(lector.Item("StockDisponible")) = True, 0, lector.Item("StockDisponible")))
                        .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("CodigoProducto")) = True, "", lector.Item("CodigoProducto")))
                        .pvComercial = CDec(IIf(IsDBNull(lector.Item("ppv_Valor")) = True, 0, lector.Item("ppv_Valor")))
                        .Kit = CBool(IIf(IsDBNull(lector.Item("prod_Kit")) = True, 0, lector.Item("prod_Kit")))
                        .ExisteCampania_Producto = objDaoMantenedor.UCBool(lector("ExisteCampania_Producto"))
                        .Prod_CodigoBarras = CStr(IIf(IsDBNull(lector.Item("prod_CodigoBarras")) = True, "", lector.Item("prod_CodigoBarras")))
                        .NoVisible = CStr(IIf(IsDBNull(lector.Item("NoVisible")) = True, "", lector.Item("NoVisible")))
                    End With

                    Lista.Add(objCatalogo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function GetDescuentoxIdProducto(ByVal IdUsuario As Integer, ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer, ByVal IdTipoPV As Integer, ByVal IdTipoOperacion As Integer, ByVal IdProducto As Integer) As Entidades.Catalogo

        Dim obj As Entidades.Catalogo = New Entidades.Catalogo
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("GetDescuentoxIdProducto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario)
        cmd.Parameters.AddWithValue("@IdCondicionPago", IdCondicionPago)
        cmd.Parameters.AddWithValue("@IdMedioPago", IdMedioPago)
        cmd.Parameters.AddWithValue("@IdTipoPV", IdTipoPV)
        cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion)
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)

        Try
            cn.Open()
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If (lector.HasRows = True) Then
                lector.Read()
                With obj
                    .PrecioBaseDcto = CStr(IIf(IsDBNull(lector("PrecioBaseDcto")), "", lector("PrecioBaseDcto")))
                    .PorcentDctoMaximo = CDec(IIf(IsDBNull(lector("PorcentDctoMaximo")), "", lector("PorcentDctoMaximo")))
                End With
            Else
                obj = Nothing
            End If
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return obj
    End Function
    Public Function CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarrasAvanzadoV2(ByVal IdLinea As Integer, _
                                                ByVal IdSubLinea As Integer, _
                                                ByVal CodSubLinea As String, _
                                                ByVal Descripcion As String, _
                                                ByVal IdTienda As Integer, _
                                                ByVal IdMonedaDestino As Integer, _
                                                ByVal IdEmpresa As Integer, _
                                                ByVal IdAlmacen As Integer, _
                                                ByVal idtipopv As Integer, _
                                                ByVal pagesize As Integer, _
                                                ByVal pagenumber As Integer, _
                                                ByVal IdUsuario As Integer, _
                                                ByVal IdCondicionPago As Integer, _
                                                ByVal IdMedioPago As Integer, _
                                                ByVal IdCliente As Integer, _
                                                ByVal tableTipoTabla As DataTable, _
                                                ByVal codigoProducto As String, _
                                                ByVal filtroProductoCampania As Boolean, _
                                                ByVal codbarras As String, _
                                                ByVal IdTipoExistencia As Integer, _
                                                ByVal codigoProveedor As String, _
                                                ByVal IdTipoOperacion As Integer) As List(Of Entidades.Catalogo)



        Descripcion = Descripcion.Replace("*", "%")

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarrasAvanzadoV2", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoExistencia", IdTipoExistencia)
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@CodSubLinea", CodSubLinea)
        cmd.Parameters.AddWithValue("@Descripcion", Descripcion)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdMoneda", IdMonedaDestino)
        cmd.Parameters.AddWithValue("@IdTipoPV", idtipopv)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@PageNumber", pagenumber)
        cmd.Parameters.AddWithValue("@PageSize", pagesize)
        cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario)
        cmd.Parameters.AddWithValue("@IdCondicionPago", IdCondicionPago)
        cmd.Parameters.AddWithValue("@IdMedioPago", IdMedioPago)
        cmd.Parameters.AddWithValue("@IdCliente", IdCliente)
        cmd.Parameters.AddWithValue("@Tabla", tableTipoTabla)
        cmd.Parameters.AddWithValue("@CodigoProducto", codigoProducto)
        cmd.Parameters.AddWithValue("@filtroProductoCampania", filtroProductoCampania)
        cmd.Parameters.AddWithValue("@codbarras", codbarras)
        cmd.Parameters.AddWithValue("@CodigoProveedor", codigoProveedor)
        cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion)




        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Catalogo)
                Do While lector.Read
                    Dim objCatalogo As New Entidades.Catalogo

                    With objCatalogo
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .Descripcion = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        '.NomLinea = CStr(IIf(IsDBNull(lector.Item("NomLinea")) = True, "", lector.Item("NomLinea")))
                        '.NomSubLinea = CStr(IIf(IsDBNull(lector.Item("NomSubLinea")) = True, "", lector.Item("NomSubLinea")))
                        .SimbMoneda = CStr(IIf(IsDBNull(lector.Item("MonedaDestino")) = True, "", lector.Item("MonedaDestino")))
                        .PrecioSD = CDec(IIf(IsDBNull(lector.Item("ValorFinal")) = True, 0, lector.Item("ValorFinal")))
                        .PrecioLista = CDec(IIf(IsDBNull(lector.Item("ValorFinal")) = True, 0, lector.Item("ValorFinal")))
                        .Percepcion = CDec(IIf(IsDBNull(lector.Item("Percepcion")) = True, 0, lector.Item("Percepcion")))
                        .Detraccion = CDec(IIf(IsDBNull(lector.Item("Detraccion")) = True, 0, lector.Item("Detraccion")))
                        .IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .NomTipoPV = CStr(IIf(IsDBNull(lector.Item("NomTipoPV")) = True, "", lector.Item("NomTipoPV")))
                        .CodigoSubLinea = CStr(IIf(IsDBNull(lector.Item("CodigoSubLinea")) = True, 0, lector.Item("CodigoSubLinea")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacenRef")) = True, 0, lector.Item("IdAlmacenRef")))
                        .Almacen = CStr(IIf(IsDBNull(lector.Item("Almacen")) = True, "", lector.Item("Almacen")))
                        .PorcentDctoMaximo = CDec(IIf(IsDBNull(lector.Item("PorcentDctoMaximo")) = True, 0, lector.Item("PorcentDctoMaximo")))
                        .PrecioBaseDcto = CStr(IIf(IsDBNull(lector.Item("PrecioBaseDcto")) = True, "", lector.Item("PrecioBaseDcto")))
                        .StockDisponibleN = CDec(IIf(IsDBNull(lector.Item("StockDisponible")) = True, 0, lector.Item("StockDisponible")))
                        .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("CodigoProducto")) = True, "", lector.Item("CodigoProducto")))
                        .pvComercial = CDec(IIf(IsDBNull(lector.Item("ppv_Valor")) = True, 0, lector.Item("ppv_Valor")))
                        .Kit = CBool(IIf(IsDBNull(lector.Item("prod_Kit")) = True, 0, lector.Item("prod_Kit")))
                        .ExisteCampania_Producto = objDaoMantenedor.UCBool(lector("ExisteCampania_Producto"))
                        .Prod_CodigoBarras = CStr(IIf(IsDBNull(lector.Item("prod_CodigoBarras")) = True, "", lector.Item("prod_CodigoBarras")))
                        .NoVisible = CStr(IIf(IsDBNull(lector.Item("NoVisible")) = True, "", lector.Item("NoVisible")))
                    End With

                    Lista.Add(objCatalogo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function


    'para busqueda directa
    Public Function CatalogoProducto_PV_Cotizacion_V2CodBarras(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As String, ByVal Descripcion As String, ByVal IdTienda As Integer, ByVal IdMonedaDestino As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal idtipopv As Integer, ByVal pagesize As Integer, ByVal pagenumber As Integer, ByVal IdUsuario As Integer, ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer, ByVal IdCliente As Integer, ByVal tableTipoTabla As DataTable, ByVal codigoProducto As String, ByVal filtroProductoCampania As Boolean, ByVal codbarras As String) As List(Of Entidades.Catalogo)
        Descripcion = Descripcion.Replace("*", "%")

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("[CatalogoProducto_PV_Paginado_Cotizacion_V2ParamCobBarras]", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@CodSubLinea", CodSubLinea)
        cmd.Parameters.AddWithValue("@Descripcion", Descripcion)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdMoneda", IdMonedaDestino)
        cmd.Parameters.AddWithValue("@IdTipoPV", idtipopv)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)

        cmd.Parameters.AddWithValue("@PageNumber", pagenumber)
        cmd.Parameters.AddWithValue("@PageSize", pagesize)

        cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario)
        cmd.Parameters.AddWithValue("@IdCondicionPago", IdCondicionPago)
        cmd.Parameters.AddWithValue("@IdMedioPago", IdMedioPago)
        cmd.Parameters.AddWithValue("@IdCliente", IdCliente)

        cmd.Parameters.AddWithValue("@Tabla", tableTipoTabla)
        cmd.Parameters.AddWithValue("@CodigoProducto", codigoProducto)
        cmd.Parameters.AddWithValue("@filtroProductoCampania", filtroProductoCampania)
        cmd.Parameters.AddWithValue("@codBarras", codbarras)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Catalogo)
                Do While lector.Read
                    Dim objCatalogo As New Entidades.Catalogo

                    With objCatalogo
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .Descripcion = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .NomLinea = CStr(IIf(IsDBNull(lector.Item("NomLinea")) = True, "", lector.Item("NomLinea")))
                        .NomSubLinea = CStr(IIf(IsDBNull(lector.Item("NomSubLinea")) = True, "", lector.Item("NomSubLinea")))
                        .SimbMoneda = CStr(IIf(IsDBNull(lector.Item("MonedaDestino")) = True, "", lector.Item("MonedaDestino")))
                        .PrecioSD = CDec(IIf(IsDBNull(lector.Item("ValorFinal")) = True, 0, lector.Item("ValorFinal")))
                        .PrecioLista = CDec(IIf(IsDBNull(lector.Item("ValorFinal")) = True, 0, lector.Item("ValorFinal")))
                        .Percepcion = CDec(IIf(IsDBNull(lector.Item("Percepcion")) = True, 0, lector.Item("Percepcion")))
                        .Detraccion = CDec(IIf(IsDBNull(lector.Item("Detraccion")) = True, 0, lector.Item("Detraccion")))
                        .IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .NomTipoPV = CStr(IIf(IsDBNull(lector.Item("NomTipoPV")) = True, "", lector.Item("NomTipoPV")))
                        .CodigoSubLinea = CStr(IIf(IsDBNull(lector.Item("CodigoSubLinea")) = True, 0, lector.Item("CodigoSubLinea")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacenRef")) = True, 0, lector.Item("IdAlmacenRef")))
                        .Almacen = CStr(IIf(IsDBNull(lector.Item("Almacen")) = True, "", lector.Item("Almacen")))

                        .PorcentDctoMaximo = CDec(IIf(IsDBNull(lector.Item("PorcentDctoMaximo")) = True, 0, lector.Item("PorcentDctoMaximo")))
                        .PrecioBaseDcto = CStr(IIf(IsDBNull(lector.Item("PrecioBaseDcto")) = True, "", lector.Item("PrecioBaseDcto")))

                        .StockDisponibleN = CDec(IIf(IsDBNull(lector.Item("StockDisponible")) = True, 0, lector.Item("StockDisponible")))
                        .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("CodigoProducto")) = True, "", lector.Item("CodigoProducto")))
                        .pvComercial = CDec(IIf(IsDBNull(lector.Item("ppv_Valor")) = True, 0, lector.Item("ppv_Valor")))
                        .Kit = CBool(IIf(IsDBNull(lector.Item("prod_Kit")) = True, 0, lector.Item("prod_Kit")))
                        .ExisteCampania_Producto = objDaoMantenedor.UCBool(lector("ExisteCampania_Producto"))
                        .Prod_CodigoBarras = CStr(IIf(IsDBNull(lector.Item("prod_CodigoBarras")) = True, "", lector.Item("prod_CodigoBarras")))

                    End With

                    Lista.Add(objCatalogo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function




    Public Function CatalogoProducto_PV(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As Integer, ByVal Descripcion As String, ByVal IdTienda As Integer, ByVal IdMonedaDestino As Integer, ByVal IdAlmacen As Integer, ByVal idtipopv As Integer) As List(Of Entidades.Catalogo)

        Descripcion = Descripcion.Replace("*", "%")

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CatalogoProducto_PV", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@CodSubLinea", CodSubLinea)
        cmd.Parameters.AddWithValue("@Descripcion", Descripcion)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdMoneda", IdMonedaDestino)
        cmd.Parameters.AddWithValue("@IdTipoPV", idtipopv)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Catalogo)
                Do While lector.Read
                    Dim objCatalogo As New Entidades.Catalogo

                    With objCatalogo
                        .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                        .Descripcion = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                        .NomLinea = CStr(IIf(IsDBNull(lector.Item("NomLinea")) = True, "", lector.Item("NomLinea")))
                        .NomSubLinea = CStr(IIf(IsDBNull(lector.Item("NomSubLinea")) = True, "", lector.Item("NomSubLinea")))
                        .SimbMoneda = CStr(IIf(IsDBNull(lector.Item("MonedaDestino")) = True, "", lector.Item("MonedaDestino")))
                        .PrecioSD = CDec(IIf(IsDBNull(lector.Item("ValorFinal")) = True, 0, lector.Item("ValorFinal")))
                        .Percepcion = CDec(IIf(IsDBNull(lector.Item("Percepcion")) = True, 0, lector.Item("Percepcion")))
                        .Detraccion = CDec(IIf(IsDBNull(lector.Item("Detraccion")) = True, 0, lector.Item("Detraccion")))
                        .StockAReal = CDec(IIf(IsDBNull(lector.Item("StockReal")) = True, 0, lector.Item("StockReal")))
                        .StockComprometido = CDec(IIf(IsDBNull(lector.Item("StockComprometido")) = True, 0, lector.Item("StockComprometido")))

                        .IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))

                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))


                        .NomTipoPV = CStr(IIf(IsDBNull(lector.Item("NomTipoPV")) = True, "", lector.Item("NomTipoPV")))

                    End With

                    Lista.Add(objCatalogo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function
    Public Function SelectxIdSublineaxIds(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, _
    ByVal IdAlmacen As Integer, ByVal IdSublinea As Integer) As List(Of Entidades.Catalogo)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CatalogoSelectxIds", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdSublinea", IdSublinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Catalogo)
                Do While lector.Read
                    Dim Catalogo As New Entidades.Catalogo
                    Catalogo.IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                    Catalogo.Descripcion = CStr(lector.Item("prod_Nombre"))
                    Dim DAOProductoUM As New DAOProductoUMView
                    Catalogo.ListaProdUM = DAOProductoUM.SelectCboxIdProducto(Catalogo.IdProducto)
                    Catalogo.IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                    Catalogo.PrecioSD = CDec(IIf(IsDBNull(lector.Item("PrecioSD")) = True, 0, lector.Item("PrecioSD")))
                    Catalogo.StockAReal = CDec(IIf(IsDBNull(lector.Item("StockReal")) = True, 0, lector.Item("StockReal")))
                    Catalogo.StockComprometido = CDec(IIf(IsDBNull(lector.Item("StockComprometido")) = True, 0, lector.Item("StockComprometido")))
                    Catalogo.AfectoPercepcion = CBool(IIf(IsDBNull(lector.Item("sl_AfectoPercepcion")) = True, 0, lector.Item("sl_AfectoPercepcion")))
                    Catalogo.Percepcion = CDec(IIf(IsDBNull(lector.Item("Percepcion")) = True, 0, lector.Item("Percepcion")))
                    Catalogo.AfectoDetraccion = CBool(IIf(IsDBNull(lector.Item("sl_AfectoDetraccion")) = True, 0, lector.Item("sl_AfectoDetraccion")))
                    Catalogo.Detraccion = CDec(IIf(IsDBNull(lector.Item("Detraccion")) = True, 0, lector.Item("Detraccion")))
                    Lista.Add(Catalogo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxIdSublineaxIdsxNombre(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, _
                                                      ByVal IdAlmacen As Integer, ByVal IdSublinea As Integer, _
                                                      ByVal NombreProducto As String) As List(Of Entidades.Catalogo)
        NombreProducto = NombreProducto.Replace("*", "%")
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CatalogoSelectxIdsxNombreProd", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdSublinea", IdSublinea)
        cmd.Parameters.AddWithValue("@prod_Nombre", NombreProducto)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Catalogo)
                Do While lector.Read
                    Dim Catalogo As New Entidades.Catalogo
                    Catalogo.IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                    Catalogo.Descripcion = CStr(lector.Item("prod_Nombre"))
                    Dim DAOProductoUM As New DAOProductoUMView
                    Catalogo.ListaProdUM = DAOProductoUM.SelectCboxIdProducto(Catalogo.IdProducto)
                    Catalogo.IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                    Catalogo.PrecioSD = CDec(IIf(IsDBNull(lector.Item("PrecioSD")) = True, 0, lector.Item("PrecioSD")))
                    Catalogo.StockAReal = CDec(IIf(IsDBNull(lector.Item("StockReal")) = True, 0, lector.Item("StockReal")))
                    Catalogo.StockComprometido = CDec(IIf(IsDBNull(lector.Item("StockComprometido")) = True, 0, lector.Item("StockComprometido")))
                    Catalogo.AfectoPercepcion = CBool(IIf(IsDBNull(lector.Item("sl_AfectoPercepcion")) = True, 0, lector.Item("sl_AfectoPercepcion")))
                    Catalogo.Percepcion = CDec(IIf(IsDBNull(lector.Item("Percepcion")) = True, 0, lector.Item("Percepcion")))
                    Catalogo.AfectoDetraccion = CBool(IIf(IsDBNull(lector.Item("sl_AfectoDetraccion")) = True, 0, lector.Item("sl_AfectoDetraccion")))
                    Catalogo.Detraccion = CDec(IIf(IsDBNull(lector.Item("Detraccion")) = True, 0, lector.Item("Detraccion")))
                    Lista.Add(Catalogo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function RptCatalogo(ByVal IdTienda As Integer, ByVal IdSublinea As Integer) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = ObjConexion.ConexionSIGE
        Try
            Dim cmd As New SqlCommand("_CatalogoRPTxIdSubLineaxIdTienda", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdSublinea", IdSublinea)
            cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "Catalogo")
        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function
    Public Function RptCatalogoProductoPVxParametros(ByVal IdTienda As Integer, ByVal idtipopv As Integer, ByVal Idlinea As Integer, ByVal idsublinea As Integer, ByVal umprincipal As Integer, ByVal umretazo As Integer) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = ObjConexion.ConexionSIGE
        Try
            Dim cmd As New SqlCommand("_CR_CatalogoProductosxTiendaxTipoPVxLineaxSubLineaxUM", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
            cmd.Parameters.AddWithValue("@IdTipoPV", idtipopv)
            cmd.Parameters.AddWithValue("@IdLinea", Idlinea)
            cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
            cmd.Parameters.AddWithValue("@UMPincipal", umprincipal)
            cmd.Parameters.AddWithValue("@UMRetazo", umretazo)
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "CR_CatalogoProductosxTiendaxTipoPVxLineaxSubLineaxUM")
        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function

    Public Function SelectAll() As List(Of Entidades.Catalogo)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CatalogoSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Catalogo)
                Do While lector.Read
                    Dim Catalogo As New Entidades.Catalogo
                    Catalogo.IdProducto = CInt(lector.Item("IdProducto"))
                    'Catalogo.UM = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                    Catalogo.Descripcion = CStr(lector.Item("prod_Nombre"))
                    Catalogo.PrecioSD = CDec(IIf(IsDBNull(lector.Item("ppv_Valor")) = True, 0, lector.Item("ppv_Valor")))
                    'Catalogo.TipoPrecio = CStr(IIf(IsDBNull(lector.Item("pv_Nombre")) = True, "", lector.Item("pv_Nombre")))
                    'Catalogo.StockActual = CDec(IIf(IsDBNull(lector.Item("prod_StockActual")) = True, 0, lector.Item("prod_StockActual")))
                    Lista.Add(Catalogo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
