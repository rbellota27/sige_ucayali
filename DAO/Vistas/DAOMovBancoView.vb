﻿
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOMovBancoView
    Inherits DAOMovBanco

    Public Function SelectMovBancosxFechas(ByVal FechaInicial As Date, ByVal Fechafinal As Date, ByVal NroOperacion As String, ByVal IdBanco As Integer, ByVal IdCuentaBancaria As Integer) As List(Of Entidades.MovBancoView)
        ''Declaraciones
        Dim lista As New List(Of Entidades.MovBancoView)

        ''Procedimiento
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SelectMovBancosxFechas", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@FechaInicial", FechaInicial)
        cmd.Parameters.AddWithValue("@FechaFinal", Fechafinal)
        cmd.Parameters.AddWithValue("@NroOperacion", IIf(NroOperacion = Nothing, DBNull.Value, NroOperacion))
        cmd.Parameters.AddWithValue("@IdBanco", IIf(IdBanco = Nothing, 0, IdBanco))
        cmd.Parameters.AddWithValue("@IdCuentaBancaria", IIf(IdCuentaBancaria = Nothing, 0, IdCuentaBancaria))

        Try
            Using cn
                cn.Open()
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                If (lector.HasRows = True) Then
                    With lector
                        Do While .Read
                            Dim objDocumento As New Entidades.MovBancoView
                            objDocumento.IdMovBanco = CInt(IIf(IsDBNull(lector.Item("IdMovBanco")) = True, 0, lector.Item("IdMovBanco")))
                            objDocumento.DescFechaRegistro = CDate(IIf(IsDBNull(lector.Item("mban_FechaRegistro")) = True, Nothing, lector.Item("mban_FechaRegistro")))
                            objDocumento.DescFechaMov = CDate(IIf(IsDBNull(lector.Item("mban_FechaMov")) = True, Nothing, lector.Item("mban_FechaMov")))
                            objDocumento.TipoConceptoBancoResumen = CStr(IIf(IsDBNull(lector.Item("TipoConceptoBanco")) = True, "", lector.Item("TipoConceptoBanco")))
                            objDocumento.ConceptoMovBanco = CStr(IIf(IsDBNull(lector.Item("conceptoMovBanco")) = True, "", lector.Item("conceptoMovBanco")))
                            objDocumento.DescMonto = CStr(IIf(IsDBNull(lector.Item("mban_Monto")) = True, "", lector.Item("mban_Monto")))
                            objDocumento.TipoTarjeta = CStr(IIf(IsDBNull(lector.Item("TipoTarjeta")) = True, "", lector.Item("TipoTarjeta")))
                            objDocumento.Tarjeta = CStr(IIf(IsDBNull(lector.Item("Tarjeta")) = True, "", lector.Item("Tarjeta")))
                            objDocumento.DescFechaAprobacion = CDate(IIf(IsDBNull(lector.Item("mban_FechaAprobacion")) = True, Nothing, lector.Item("mban_FechaAprobacion")))
                            objDocumento.Tienda = CStr(IIf(IsDBNull(lector.Item("Tienda")) = True, "", lector.Item("Tienda")))
                            objDocumento.Usuario = Nothing
                            objDocumento.EstadoMov = True
                            objDocumento.SaldoDisponible = CDec(IIf(IsDBNull(lector.Item("cb_SaldoDisponible")) = True, 0, lector.Item("cb_SaldoDisponible")))
                            objDocumento.SaldoContable = CDec(IIf(IsDBNull(lector.Item("cb_SaldoContable")) = True, 0, lector.Item("cb_SaldoContable")))
                            objDocumento.NroOperacion = CStr(IIf(IsDBNull(lector.Item("mban_NroOperacion")) = True, "", lector.Item("mban_NroOperacion")))
                            objDocumento.Referencia1x = CStr(IIf(IsDBNull(lector.Item("referencia1")) = True, "", lector.Item("referencia1")))
                            objDocumento.Referencia2x = CStr(IIf(IsDBNull(lector.Item("referencia2")) = True, "", lector.Item("referencia2")))
                            lista.Add(objDocumento)
                        Loop
                    End With
                Else
                    lista = Nothing
                End If
                Return lista
                cn.Close()
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectMovBancosxId(ByVal IdMovBanco As Integer) As Entidades.MovBancoView
        Dim lista As New List(Of Entidades.MovBancoView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SelectMovBancosxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdMovBanco", IdMovBanco)

        Try

            Using cn
                cn.Open()

                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

                lector.Read()
                Dim objDocumento As New Entidades.MovBancoView
                With objDocumento
                    .IdMovBanco = CInt(IIf(IsDBNull(lector.Item("IdMovBanco")) = True, 0, lector.Item("IdMovBanco")))
                    .DescFechaRegistro = CDate(IIf(IsDBNull(lector.Item("mban_FechaRegistro")) = True, Nothing, lector.Item("mban_FechaRegistro")))
                    .DescFechaMov = CDate(IIf(IsDBNull(lector.Item("mban_FechaMov")) = True, Nothing, lector.Item("mban_FechaMov")))
                    .TipoConceptoBancoResumen = CStr(IIf(IsDBNull(lector.Item("TipoConceptoBanco")) = True, "", lector.Item("TipoConceptoBanco")))
                    .ConceptoMovBanco = CStr(IIf(IsDBNull(lector.Item("conceptoMovBanco")) = True, "", lector.Item("conceptoMovBanco")))
                    .DescMonto = CStr(IIf(IsDBNull(lector.Item("mban_Monto")) = True, "", lector.Item("mban_Monto")))
                    .TipoTarjeta = CStr(IIf(IsDBNull(lector.Item("TipoTarjeta")) = True, "", lector.Item("TipoTarjeta")))
                    .Tarjeta = CStr(IIf(IsDBNull(lector.Item("Tarjeta")) = True, "", lector.Item("Tarjeta")))
                    .DescFechaAprobacion = CDate(IIf(IsDBNull(lector.Item("mban_FechaAprobacion")) = True, Nothing, lector.Item("mban_FechaAprobacion")))
                    .Tienda = CStr(IIf(IsDBNull(lector.Item("Tienda")) = True, "", lector.Item("Tienda")))
                    .Usuario = Nothing
                    .EstadoMov = True
                    .SaldoDisponible = CDec(IIf(IsDBNull(lector.Item("cb_SaldoDisponible")) = True, 0, lector.Item("cb_SaldoDisponible")))
                    .SaldoContable = CDec(IIf(IsDBNull(lector.Item("cb_SaldoContable")) = True, 0, lector.Item("cb_SaldoContable")))
                End With
                Return objDocumento
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function SelectxIdBancoxIdCuentaBancaria(ByVal IdBanco As Integer, ByVal IdCuentaBancaria As Integer) As Entidades.MovBancoView
        Dim lista As New List(Of Entidades.MovBancoView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SelectxIdCuentaBancoxIdBanco", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdBanco", IdBanco)
        cmd.Parameters.AddWithValue("@IdCuentaBanco", IdCuentaBancaria)
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                lector.Read()

                Dim objDocumento As New Entidades.MovBancoView
                With objDocumento

                    .idestructurabanco = CInt(IIf(IsDBNull(lector.Item("idestructurabanco")) = True, 0, lector.Item("idestructurabanco")))
                    .nom_Hoja = CStr(IIf(IsDBNull(lector.Item("nom_Hoja")) = True, "", lector.Item("nom_Hoja")))
                End With
                Return objDocumento
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectUltimoIdentificadorExport() As Integer


        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SelectUltimoIdentificadorExport", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@IdentificadorExport", SqlDbType.Int)
        cmd.Parameters("@IdentificadorExport").Direction = ParameterDirection.Output
        cn.Open()

        cmd.ExecuteScalar()

        Dim IdentificadorExport As Integer = CInt(IIf(IsDBNull(cmd.Parameters("@IdentificadorExport").Value) = True, 0, cmd.Parameters("@IdentificadorExport").Value))

        If cn.State = ConnectionState.Open Then cn.Close()

        Return IdentificadorExport

    End Function

    Public Function SelectExcelttoGridView(ByVal namearchivo As String, ByVal nom_hoja As String, ByVal IdEstructuraBanco As Integer) As List(Of Entidades.MovBancoView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ImportaExcelToSql", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@namearchivo", namearchivo)
        cmd.Parameters.AddWithValue("@IdEstructuraBanco", IdEstructuraBanco)
        Try
            Using cn
                cn.Open()
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MovBancoView)
                If (lector.HasRows = True) Then
                    With lector
                        Do While .Read
                            Dim obj As New Entidades.MovBancoView
                            obj.Fecha = CDate(IIf(IsDBNull(.Item("Fecha")) = True, Nothing, .Item("Fecha")))
                            'obj.Fecha_valuta = CDate(.Item("Fecha_valuta"))
                            obj.Descripción_operación = CStr(IIf(IsDBNull(.Item("Descripción_operación")) = True, Nothing, .Item("Descripción_operación")))
                            obj.Monto = CDec(IIf(IsDBNull(.Item("Monto").ToString.Trim) = True, 0, .Item("Monto").ToString.Trim))
                            'obj.Saldo = CDec(.Item("Saldo"))
                            'obj.Sucursal_agencia = CStr(.Item("Sucursal_agencia"))
                            obj.Operación_Número = CStr(IIf(IsDBNull(.Item("Operación_Número")) = True, Nothing, .Item("Operación_Número")))
                            'obj.Operación_Hora = CStr(.Item("Operación_Hora"))
                            'obj.Usuario = CStr(.Item("Usuario"))
                            'obj.UTC = CStr(.Item("UTC"))
                            obj.Referencia1 = CStr(IIf(IsDBNull(.Item("Referencia1")) = True, Nothing, .Item("Referencia1")))
                            obj.Referencia2 = CStr(IIf(IsDBNull(.Item("Referencia2")) = True, Nothing, .Item("Referencia2")))
                            Lista.Add(obj)
                        Loop
                    End With
                Else
                    Lista = Nothing
                End If
                cn.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.MovBancoView)
        Cn = objConexion.ConexionSIGE()
        Dim Prm() As SqlParameter = New SqlParameter(0) {}

        Prm(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        Prm(0).Value = IdDocumento

        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()

                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "[_MovBancoViewSelectxIdDocumento]", Prm)

                Dim Lista As New List(Of Entidades.MovBancoView)
                With lector
                    Do While .Read

                        Dim obj As New Entidades.MovBancoView

                        obj.IdMovBanco = CInt(.Item("IdMovBanco"))
                        obj.IdBanco = CInt(.Item("IdBanco"))
                        obj.IdCuentaBancaria = CInt(.Item("IdCuentaBancaria"))

                        obj.NroOperacion = CStr(.Item("mban_NroOperacion"))
                        obj.Observacion = CStr(.Item("mban_Observacion"))
                        obj.Factor = CInt(IIf(IsDBNull(.Item("mban_Factor")) = True, Nothing, .Item("mban_Factor")))
                        'obj.Factor = CInt(IIf(IsDBNull(.Item("mban_Factor")) = True, 0, .Item("mban_Factor")))
                        obj.Monto = CDec(.Item("mban_Monto"))

                        obj.FechaRegistro = CDate(IIf(IsDBNull(.Item("mban_FechaRegistro")) = True, Nothing, .Item("mban_FechaRegistro")))
                        obj.FechaMov = CDate(IIf(IsDBNull(.Item("mban_FechaMov")) = True, Nothing, .Item("mban_FechaMov")))
                        obj.FechaAprobacion = CDate(IIf(IsDBNull(.Item("mban_FechaAprobacion")) = True, Nothing, .Item("mban_FechaAprobacion")))

                        obj.EstadoAprobacion = CBool(.Item("mban_EstadoAprobacion"))
                        obj.EstadoMov = CBool(.Item("mban_EstadoMov"))

                        obj.IdUsuario = CInt(.Item("IdUsuario"))
                        obj.IdSupervisor = CInt(.Item("IdSupervisor"))
                        obj.IdMovBanco = CInt(.Item("IdMovBanco"))
                        obj.IdTarjeta = CInt(.Item("IdTarjeta"))
                        obj.IdPersonaRef = CInt(.Item("IdPersonaRef"))
                        obj.IdCaja = CInt(.Item("IdCaja"))
                        obj.IdTienda = CInt(.Item("IdTienda"))

                        obj.IdTipoTarjeta = CInt(.Item("IdTipoTarjeta"))
                        obj.IdConceptoMovBanco = CInt(.Item("IdConceptoMovBanco"))
                        obj.IdTipoConceptoBanco = CInt(.Item("IdTipoConceptoBanco"))
                        obj.IdPost = CInt(.Item("IdPost"))

                        obj.Banco = CStr(.Item("Banco"))
                        obj.CuentaBancaria = CStr(.Item("CuentaBancaria"))
                        obj.Usuario = CStr(.Item("Usuario"))
                        obj.Supervisor = CStr(.Item("Supervisor"))
                        obj.ConceptoMovBanco = CStr(.Item("ConceptoMovBanco"))
                        obj.TipoConceptoBanco = CStr(.Item("TipoConceptoBanco"))
                        obj.Tarjeta = CStr(.Item("Tarjeta"))
                        obj.TipoTarjeta = CStr(.Item("TipoTarjeta"))
                        obj.PersonaReferencia = CStr(.Item("PersonaReferencia"))
                        obj.Caja = CStr(.Item("Caja"))
                        obj.Tienda = CStr(.Item("Tienda"))
                        obj.Post = CStr(.Item("Post"))
                        obj.SaldoDisponible = CDec(.Item("cb_SaldoDisponible"))
                        obj.SaldoContable = CDec(.Item("cb_SaldoContable"))
                        obj.IdCheque = CInt(.Item("IdCheque"))
                        obj.IdDocumentoRef = CInt(.Item("IdDocumentoRef"))
                        obj.IdDocumentoRef2 = CInt(.Item("IdDocumentoRef2"))
                        Lista.Add(obj)

                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxParams(ByVal x As Entidades.MovBancoView) As List(Of Entidades.MovBancoView)
        Cn = objConexion.ConexionSIGE()
        Dim Prm() As SqlParameter
        Prm = GetVectorParametros(CType(x, Entidades.MovBanco), modo_query.Show)
        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()

                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "_MovBancoViewSelectxParams", Prm)

                Dim Lista As New List(Of Entidades.MovBancoView)
                With lector
                    Do While .Read

                        Dim obj As New Entidades.MovBancoView

                        obj.IdMovBanco = CInt(IIf(IsDBNull(.Item("IdMovBanco")) = True, 0, .Item("IdMovBanco")))
                        obj.IdBanco = CInt(IIf(IsDBNull(.Item("IdBanco")) = True, 0, .Item("IdBanco")))
                        obj.Banco = CStr(IIf(IsDBNull(.Item("Banco")) = True, Nothing, .Item("Banco")))

                        obj.IdCuentaBancaria = CInt(IIf(IsDBNull(.Item("IdCuentaBancaria")) = True, 0, .Item("IdCuentaBancaria")))

                        obj.NroOperacion = CStr(IIf(IsDBNull(.Item("mban_NroOperacion")) = True, Nothing, .Item("mban_NroOperacion")))
                        obj.Observacion = CStr(IIf(IsDBNull(.Item("mban_Observacion")) = True, Nothing, .Item("mban_Observacion")))
                        obj.Factor = CInt(IIf(IsDBNull(.Item("mban_Factor")) = True, 0, .Item("mban_Factor")))
                        'obj.Factor = CInt(IIf(IsDBNull(.Item("mban_Factor")) = True, 0, .Item("mban_Factor")))
                        obj.Monto = CDec(IIf(IsDBNull(.Item("mban_Monto")) = True, 0, .Item("mban_Monto")))

                        obj.FechaRegistro = CDate(IIf(IsDBNull(.Item("mban_FechaRegistro")) = True, Nothing, .Item("mban_FechaRegistro")))
                        obj.FechaMov = CDate(IIf(IsDBNull(.Item("mban_FechaMov")) = True, Nothing, .Item("mban_FechaMov")))
                        obj.FechaAprobacion = CDate(IIf(IsDBNull(.Item("mban_FechaAprobacion")) = True, Nothing, .Item("mban_FechaAprobacion")))

                        obj.EstadoAprobacion = CBool(IIf(IsDBNull(.Item("mban_EstadoAprobacion")) = True, Nothing, .Item("mban_EstadoAprobacion")))
                        obj.EstadoMov = CBool(IIf(IsDBNull(.Item("mban_EstadoMov")) = True, Nothing, .Item("mban_EstadoMov")))

                        obj.IdUsuario = CInt(IIf(IsDBNull(.Item("IdUsuario")) = True, 0, .Item("IdUsuario")))
                        obj.IdSupervisor = CInt(IIf(IsDBNull(.Item("IdSupervisor")) = True, 0, .Item("IdSupervisor")))
                        obj.IdMovBanco = CInt(IIf(IsDBNull(.Item("IdMovBanco")) = True, 0, .Item("IdMovBanco")))
                        obj.IdTarjeta = CInt(IIf(IsDBNull(.Item("IdTarjeta")) = True, 0, .Item("IdTarjeta")))
                        obj.IdPersonaRef = CInt(IIf(IsDBNull(.Item("IdPersonaRef")) = True, 0, .Item("IdPersonaRef")))
                        obj.IdCaja = CInt(IIf(IsDBNull(.Item("IdCaja")) = True, 0, .Item("IdCaja")))
                        obj.IdTienda = CInt(IIf(IsDBNull(.Item("IdTienda")) = True, 0, .Item("IdTienda")))

                        obj.IdTipoTarjeta = CInt(IIf(IsDBNull(.Item("IdTipoTarjeta")) = True, 0, .Item("IdTipoTarjeta")))
                        obj.IdConceptoMovBanco = CInt(IIf(IsDBNull(.Item("IdConceptoMovBanco")) = True, 0, .Item("IdConceptoMovBanco")))
                        obj.IdTipoConceptoBanco = CInt(IIf(IsDBNull(.Item("IdTipoConceptoBanco")) = True, 0, .Item("IdTipoConceptoBanco")))
                        obj.IdPost = CInt(IIf(IsDBNull(.Item("IdPost")) = True, 0, .Item("IdPost")))


                        obj.CuentaBancaria = CStr(IIf(IsDBNull(.Item("CuentaBancaria")) = True, Nothing, .Item("CuentaBancaria")))
                        obj.Usuario = CStr(IIf(IsDBNull(.Item("Usuario")) = True, Nothing, .Item("Usuario")))
                        obj.Supervisor = CStr(IIf(IsDBNull(.Item("Supervisor")) = True, Nothing, .Item("Supervisor")))
                        obj.ConceptoMovBanco = CStr(IIf(IsDBNull(.Item("ConceptoMovBanco")) = True, Nothing, .Item("ConceptoMovBanco")))
                        obj.TipoConceptoBanco = CStr(IIf(IsDBNull(.Item("TipoConceptoBanco")) = True, Nothing, .Item("TipoConceptoBanco")))
                        obj.Tarjeta = CStr(IIf(IsDBNull(.Item("Tarjeta")) = True, Nothing, .Item("Tarjeta")))
                        obj.TipoTarjeta = CStr(IIf(IsDBNull(.Item("TipoTarjeta")) = True, Nothing, .Item("TipoTarjeta")))
                        obj.PersonaReferencia = CStr(IIf(IsDBNull(.Item("PersonaReferencia")) = True, Nothing, .Item("PersonaReferencia")))
                        obj.Caja = CStr(IIf(IsDBNull(.Item("Caja")) = True, Nothing, .Item("Caja")))
                        obj.Tienda = CStr(IIf(IsDBNull(.Item("Tienda")) = True, Nothing, .Item("Tienda")))
                        obj.Post = CStr(IIf(IsDBNull(.Item("Post")) = True, Nothing, .Item("Post")))
                        obj.SaldoDisponible = CDec(IIf(IsDBNull(.Item("cb_SaldoDisponible")) = True, 0, .Item("cb_SaldoDisponible")))
                        obj.SaldoContable = CDec(IIf(IsDBNull(.Item("cb_SaldoContable")) = True, 0, .Item("cb_SaldoContable")))
                        obj.Referencia1x = CStr(IIf(IsDBNull(.Item("referencia1")) = True, "", .Item("referencia1")))
                        obj.Referencia2x = CStr(IIf(IsDBNull(.Item("referencia2")) = True, "", .Item("referencia2")))
                        Lista.Add(obj)

                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAll() As List(Of Entidades.MovBancoView)

        Dim x As New Entidades.MovBancoView

        Return SelectxParams(x)

    End Function

    Public Function SelectAllActivo() As List(Of Entidades.MovBancoView)

        Dim x As New Entidades.MovBancoView
        x.EstadoMov = True
        Return SelectxParams(x)

    End Function

    Public Function SelectAllInactivo() As List(Of Entidades.MovBancoView)

        Dim x As New Entidades.MovBancoView
        x.EstadoMov = False
        Return SelectxParams(x)

    End Function

    Public Function SelectxId(ByVal Id As Integer) As Entidades.MovBancoView

        Dim x As New Entidades.MovBancoView
        x.IdMovBanco = Id
        Return SelectxParams(x).Item(0)

    End Function

End Class
