'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAORelacionDocumento
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion


    Public Function Documento_Referencia(ByVal IdDocumento As Integer) As List(Of Entidades.RelacionDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DOCUMENTO_REFERENCIA", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.RelacionDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.RelacionDocumento
                    obj.IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                    obj.IdDocumentoRef = CInt(IIf(IsDBNull(lector.Item("IDREF")) = True, 0, lector.Item("IDREF")))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                    obj.IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex

        End Try
    End Function

    Public Function SelectxIdDocumentoxIdTipoDocumento(ByVal IdDocumento As Integer, ByVal IdTipoDocumento As Integer) As List(Of Entidades.Documento)
        Dim obj As Entidades.Documento = Nothing
        Dim lista As New List(Of Entidades.Documento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand
        Dim reader As SqlDataReader = Nothing
        cmd.CommandText = "select * from [dbo].[fn_DocumentoRefxIdTipoDocumento](" + CStr(IdDocumento) + "," + CStr(IdTipoDocumento) + ")"
        cmd.Connection = cn
        cmd.CommandType = CommandType.Text
        cmd.CommandTimeout = 0

        Try
            cn.Open()
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            Do While reader.Read
                obj = New Entidades.Documento
                obj.Id = CInt(reader("IdDocumento"))
                obj.Codigo = CStr(IIf(IsDBNull(reader("Numero")), "", reader("Numero")))
                obj.NomTipoDocumento = CStr(IIf(IsDBNull(reader("TipoDocumento")), "", reader("TipoDocumento")))
                obj.IdTipoDocumento = CInt(IIf(IsDBNull(reader("IdTipoDocumento")), 0, reader("IdTipoDocumento")))
                lista.Add(obj)
            Loop
            reader.Close()

            Return lista
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Nothing

    End Function

    Public Function RelacionDocumento_SelectDocumentoRefxIdDocumento2(ByVal IdDocumento2 As Integer) As List(Of Entidades.Documento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RelacionDocumento_SelectDocumentoRefxIdDocumento2", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento2)
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.Documento)

                While (lector.Read)

                    Dim Documento As New Entidades.Documento
                    With Documento
                        .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumentoRef")) = True, 0, lector.Item("IdDocumentoRef")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                        .FechaEntrega = CDate(IIf(IsDBNull(lector.Item("doc_FechaEntrega")) = True, Nothing, lector.Item("doc_FechaEntrega")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                        .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdDestinatario = CInt(IIf(IsDBNull(lector.Item("IdDestinatario")) = True, 0, lector.Item("IdDestinatario")))
                        .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                        .IdEstadoEntrega = CInt(IIf(IsDBNull(lector.Item("IdEstadoEnt")) = True, 0, lector.Item("IdEstadoEnt")))
                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                        .NomAlmacen = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                        .Tienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))
                        .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("edoc_Nombre")) = True, "", lector.Item("edoc_Nombre")))
                        .Empresa = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                        .NroDocumento = .Serie + " - " + .Codigo
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, 0, lector.Item("mon_Simbolo")))
                        .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, 0, lector.Item("doc_Total")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, 0, lector.Item("doc_TotalAPagar")))

                    End With

                    lista.Add(Documento)

                End While

                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UpdateRelacionDocumento(ByVal obj As Entidades.RelacionDocumento, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Dim cmd As New SqlCommand("_updateRelacionDocumento", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idDoc1", obj.IdDocumento1)
        cmd.Parameters.AddWithValue("@idDoc2", obj.IdDocumento2)
        cmd.ExecuteNonQuery()
    End Sub

    Public Sub InsertRelacionChequeProgramacionPago(ByVal relaciondocumento As Entidades.RelacionDocumento, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Dim parameters() As SqlParameter = New SqlParameter(1) {}
        parameters(0) = New SqlParameter("@ProgramacionPago_CXP", SqlDbType.Int)
        parameters(0).Value = relaciondocumento.IdDocumento1
        parameters(1) = New SqlParameter("@IdDocumentoRef", SqlDbType.Int)
        parameters(1).Value = relaciondocumento.IdDocumento2

        HDAO.UpdateT(cn, "InsertRelacionChequeProgramacionPago", parameters, tr)
    End Sub

    Public Sub InsertaRelacionDocRegimen(ByVal relaciondocumento As Entidades.RelacionDocumento, ByVal cn As SqlConnection, ByVal tr As SqlTransaction, _
                                         ByVal idRegimen As Integer, ByVal porcent As Decimal, ByVal idRequerimientoCadena As String)
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "SP_INSERTA_MONTO_REGIMEN_V2"
                .Connection = cn
                .Transaction = tr
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@IdDocumento1", SqlDbType.Int)).Value = relaciondocumento.IdDocumento1
                .Parameters.Add(New SqlParameter("@IdDocumento2", SqlDbType.Int)).Value = relaciondocumento.IdDocumento2
                .Parameters.Add(New SqlParameter("@tot_amortizado", SqlDbType.Decimal)).Value = relaciondocumento.montoAmortizado
                .Parameters.Add(New SqlParameter("@tot_deuda", SqlDbType.Decimal)).Value = relaciondocumento.montoDeuda
                .Parameters.Add(New SqlParameter("@MontoOtroTributo", SqlDbType.Decimal)).Value = relaciondocumento.MontoOtroTributo
                .Parameters.Add(New SqlParameter("@idRegimen", SqlDbType.Int)).Value = idRegimen
                .Parameters.Add(New SqlParameter("@porcen", SqlDbType.Decimal)).Value = porcent
                .Parameters.Add(New SqlParameter("@montoRegimen", SqlDbType.Decimal)).Value = relaciondocumento.montoAgente
                .Parameters.Add(New SqlParameter("@tipoRegimen", SqlDbType.Int)).Value = relaciondocumento.tipoRegimen
                .Parameters.Add(New SqlParameter("@idCadenaRequerimientos", SqlDbType.NVarChar)).Value = idRequerimientoCadena

                .ExecuteNonQuery()
            End With

        End Using
    End Sub
    Public Sub InsertaRelacionDocRegimenParcial(ByVal relaciondocumento As Entidades.RelacionDocumento, ByVal cn As SqlConnection, ByVal tr As SqlTransaction, _
                                           ByVal idRegimen As Integer, ByVal porcent As Decimal, ByVal idRequerimientoCadena As String)
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "SP_INSERTA_MONTO_REGIMEN_V2_PARCIAL"
                .Connection = cn
                .Transaction = tr
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@IdDocumento1", SqlDbType.Int)).Value = relaciondocumento.IdDocumento1
                .Parameters.Add(New SqlParameter("@IdDocumento2", SqlDbType.Int)).Value = relaciondocumento.IdDocumento2
                .Parameters.Add(New SqlParameter("@tot_amortizado", SqlDbType.Decimal)).Value = relaciondocumento.montoAmortizado
                .Parameters.Add(New SqlParameter("@montoRegimen", SqlDbType.Decimal)).Value = relaciondocumento.montoAgente
                .Parameters.Add(New SqlParameter("@tot_deuda", SqlDbType.Decimal)).Value = relaciondocumento.montoDeuda
                .Parameters.Add(New SqlParameter("@MontoOtroTributo", SqlDbType.Decimal)).Value = relaciondocumento.MontoOtroTributo
                .Parameters.Add(New SqlParameter("@idRegimen", SqlDbType.Int)).Value = idRegimen
                .Parameters.Add(New SqlParameter("@porcen", SqlDbType.Decimal)).Value = porcent
                .Parameters.Add(New SqlParameter("@tipoRegimen", SqlDbType.Int)).Value = relaciondocumento.tipoRegimen
                .Parameters.Add(New SqlParameter("@idCadenaRequerimientos", SqlDbType.NVarChar)).Value = idRequerimientoCadena

                .ExecuteNonQuery()
            End With

        End Using
    End Sub

    Public Sub InsertaRelacionDocRegimenParcial_LQ(ByVal relaciondocumento As Entidades.RelacionDocumento, ByVal cn As SqlConnection, ByVal tr As SqlTransaction, _
                                         ByVal idRegimen As Integer, ByVal porcent As Decimal, ByVal idRequerimientoCadena As String)
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "SP_INSERTA_MONTO_REGIMEN_V2_PARCIAL_LQ"
                .Connection = cn
                .Transaction = tr
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@IdDocumento1", SqlDbType.Int)).Value = relaciondocumento.IdDocumento1
                .Parameters.Add(New SqlParameter("@IdDocumento2", SqlDbType.Int)).Value = relaciondocumento.IdDocumento2
                .Parameters.Add(New SqlParameter("@tot_amortizado", SqlDbType.Decimal)).Value = relaciondocumento.montoAmortizado
                .Parameters.Add(New SqlParameter("@montoRegimen", SqlDbType.Decimal)).Value = relaciondocumento.montoAgente
                .Parameters.Add(New SqlParameter("@tot_deuda", SqlDbType.Decimal)).Value = relaciondocumento.montoDeuda
                .Parameters.Add(New SqlParameter("@MontoOtroTributo", SqlDbType.Decimal)).Value = relaciondocumento.MontoOtroTributo
                .Parameters.Add(New SqlParameter("@idRegimen", SqlDbType.Int)).Value = idRegimen
                .Parameters.Add(New SqlParameter("@porcen", SqlDbType.Decimal)).Value = porcent
                .Parameters.Add(New SqlParameter("@tipoRegimen", SqlDbType.Int)).Value = relaciondocumento.tipoRegimen
                .Parameters.Add(New SqlParameter("@idCadenaRequerimientos", SqlDbType.NVarChar)).Value = idRequerimientoCadena

                .ExecuteNonQuery()
            End With

        End Using
    End Sub
    Public Sub InsertaRelacionAplicacionContraDeudas(ByVal listaDocumentosAplicacionesContraDeudas As Entidades.be_documentoAplicacionDeudasPendientes, ByVal cn As SqlConnection, _
                                                     ByVal tr As SqlTransaction)
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "SP_INSERTA_DOC_CONTRA_DEUDAS_PEND"
                .Connection = cn
                .Transaction = tr
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@idProvision", SqlDbType.Int)).Value = listaDocumentosAplicacionesContraDeudas.idProvision
                .Parameters.Add(New SqlParameter("@idFacturaAplicacion", SqlDbType.Int)).Value = listaDocumentosAplicacionesContraDeudas.idFacturaAplicacion
                .Parameters.Add(New SqlParameter("@montoAplicadoContraDeudaPendiente", SqlDbType.Int)).Value = listaDocumentosAplicacionesContraDeudas.montoAplicadoContraDeudaPendiente

                .ExecuteNonQuery()
            End With

        End Using
    End Sub

    Public Sub InsertaRelacionDocumento( ByVal relaciondocumento As Entidades.RelacionDocumento, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "_RelacionDocumentoInsert"
                .Connection = cn
                .Transaction = tr
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@IdDocumento1", SqlDbType.Int)).Value = relaciondocumento.IdDocumento1
                .Parameters.Add(New SqlParameter("@IdDocumento2", SqlDbType.Int)).Value = relaciondocumento.IdDocumento2
                .ExecuteNonQuery()
            End With

        End Using

        'Dim parameters() As SqlParameter = New SqlParameter(1) {}
        'parameters(0) = New SqlParameter("@IdDocumento1", SqlDbType.Int)
        'parameters(0).Value = relaciondocumento.IdDocumento1
        'parameters(1) = New SqlParameter("@IdDocumento2", SqlDbType.Int)
        'parameters(1).Value = relaciondocumento.IdDocumento2
        'HDAO.InsertaT(cn, "_RelacionDocumentoInsert", parameters, tr)
    End Sub

    Public Sub ActualizarMontos(ByVal relaciondocumento As Entidades.RelacionDocumento, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "_ActualizaMontosLetraRenegociada"
                .Connection = cn
                .Transaction = tr
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@IdDocumento1", SqlDbType.Int)).Value = relaciondocumento.IdDocumento1
                .Parameters.Add(New SqlParameter("@IdDocumento2", SqlDbType.Int)).Value = relaciondocumento.IdDocumento2

                .ExecuteNonQuery()
            End With

        End Using

        'Dim parameters() As SqlParameter = New SqlParameter(1) {}
        'parameters(0) = New SqlParameter("@IdDocumento1", SqlDbType.Int)
        'parameters(0).Value = relaciondocumento.IdDocumento1
        'parameters(1) = New SqlParameter("@IdDocumento2", SqlDbType.Int)
        'parameters(1).Value = relaciondocumento.IdDocumento2
        'HDAO.InsertaT(cn, "_RelacionDocumentoInsert", parameters, tr)
    End Sub

    Public Function SelectAllIdDoc2xIdDoc1(ByVal Cn As SqlConnection, ByVal T As SqlTransaction, ByVal IdDocumento1 As Integer) As List(Of Entidades.RelacionDocumento)
        ' Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RelacionDocumentoSelectAllIdDoc2xIdDoc1", Cn, T)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento1", IdDocumento1)
        Dim lector As SqlDataReader
        Try
            'Using Cn
            ' Cn.Open()
            lector = cmd.ExecuteReader '(Data.CommandBehavior.CloseConnection)
            Dim Lista As New List(Of Entidades.RelacionDocumento)
            If lector.HasRows = True Then ' Si lee algun registro
                Do While lector.Read
                    Dim obj As New Entidades.RelacionDocumento
                    obj.IdDocumento2 = CInt(lector.Item("IdDocumento2"))
                    Lista.Add(obj)
                Loop
            End If
            lector.Close()
            Return Lista
            'End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GrabaRelacionDocumentoT(ByVal cn As SqlConnection, ByVal vIdDocumento As Integer, ByVal objRelacDoc As Entidades.RelacionDocumento, Optional ByVal T As SqlTransaction = Nothing) As Boolean
        Try
            Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
            ArrayParametros(0) = New SqlParameter("@IdDocumento1", SqlDbType.Int)
            ArrayParametros(0).Value = vIdDocumento

            ArrayParametros(1) = New SqlParameter("@IdDocumento2", SqlDbType.Bit)
            ArrayParametros(1).Value = objRelacDoc.IdDocumento2

            HDAO.UpdateT(cn, "InsUpd_RelacionDocumento", ArrayParametros, T)
            Return True
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

    Public Function RelacionDocumentoSelectxIdDocumento2xIdTipoDocumento(ByVal IdDocumento2 As Integer, ByVal IdTipoDocumento As Integer) As List(Of Entidades.RelacionDocumento)

        Dim lista As New List(Of Entidades.RelacionDocumento)

        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Try

            cn.Open()

            Dim cmd As New SqlCommand("_RelacionDocumentoSelectxIdDocumento2xIdTipoDocumento", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento2", IdDocumento2)
            cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)

            Dim lector As SqlDataReader = cmd.ExecuteReader
            While (lector.Read)

                Dim obj As New Entidades.RelacionDocumento
                obj.IdDocumento1 = CInt(lector("IdDocumento1"))
                obj.IdDocumento2 = CInt(lector("IdDocumento2"))

                lista.Add(obj)

            End While
            lector.Read()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista

    End Function


End Class
