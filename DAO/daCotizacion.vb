﻿Imports System.Data
Imports System.Data.SqlClient
Imports Entidades

Public Class daCotizacion
    Private objDaoMantenedor As New DAO.DAOMantenedor

    Public Function obtenerListas(ByVal IdUsuario As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer,
       ByVal IdTipoDocumento As Integer, ByVal IdTipoOperacion As Integer, ByVal IdCondicionPago As Integer,
       ByVal CodDep As String, ByVal CodProv As String, ByVal IdTipoExistencia As Integer,
       ByVal IdMangitud As Integer, ByVal Permisos As String, ByVal Parametros As String, ByVal con As SqlConnection) As beCotizacionListas

        Dim obeCotizacionListas As beCotizacionListas

        Dim cmd As New SqlCommand("uspHte_CotizacionListas", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@IdUsuario", SqlDbType.Int)
        cmd.Parameters.Add(par1)
        par1.Direction = ParameterDirection.Input
        par1.Value = IdUsuario

        Dim par2 As New SqlParameter("@IdEmpresa", SqlDbType.Int)
        cmd.Parameters.Add(par2)
        par2.Direction = ParameterDirection.Input
        par2.Value = IdEmpresa

        Dim par3 As New SqlParameter("@IdTienda", SqlDbType.Int)
        cmd.Parameters.Add(par3)
        par3.Direction = ParameterDirection.Input
        par3.Value = IdTienda

        Dim par4 As New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        cmd.Parameters.Add(par4)
        par4.Direction = ParameterDirection.Input
        par4.Value = IdTipoDocumento

        Dim par5 As New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        cmd.Parameters.Add(par5)
        par5.Direction = ParameterDirection.Input
        par5.Value = IdTipoOperacion

        Dim par6 As New SqlParameter("@IdCondicionPago", SqlDbType.Int)
        cmd.Parameters.Add(par6)
        par6.Direction = ParameterDirection.Input
        par6.Value = IdCondicionPago

        Dim par7 As New SqlParameter("@ub_CodDpto", SqlDbType.Char, 2)
        cmd.Parameters.Add(par7)
        par7.Direction = ParameterDirection.Input
        par7.Value = CodDep

        Dim par8 As New SqlParameter("@ub_CodProv", SqlDbType.Char, 2)
        cmd.Parameters.Add(par8)
        par8.Direction = ParameterDirection.Input
        par8.Value = CodProv

        Dim par9 As New SqlParameter("@IdTipoExistencia", SqlDbType.Int)
        cmd.Parameters.Add(par9)
        par9.Direction = ParameterDirection.Input
        par9.Value = IdTipoExistencia

        'Dim par10 As New SqlParameter("@IdLinea", SqlDbType.Int)
        'par10.Direction = ParameterDirection.Input
        'par10.Value = IdLinea

        'Dim par11 As New SqlParameter("@IdSubLinea", SqlDbType.Int)
        'par11.Direction = ParameterDirection.Input
        'par11.Value = IdSubLinea

        Dim par12 As New SqlParameter("@IdMagnitud", SqlDbType.Int)
        cmd.Parameters.Add(par12)
        par12.Direction = ParameterDirection.Input
        par12.Value = IdMangitud

        Dim par13 As New SqlParameter("@Permisos", SqlDbType.VarChar, 58)
        cmd.Parameters.Add(par13)
        par13.Direction = ParameterDirection.Input
        par13.Value = Permisos

        Dim par14 As New SqlParameter("@Parametros", SqlDbType.VarChar, Integer.MaxValue)
        cmd.Parameters.Add(par14)
        par14.Direction = ParameterDirection.Input
        par14.Value = Parametros

        Dim drd As SqlDataReader = cmd.ExecuteReader()

        If (drd IsNot Nothing) Then
            obeCotizacionListas = New beCotizacionListas
            If (drd.HasRows) Then
                Dim valor As String = ""
                If (drd.Read) Then
                    valor = drd.GetString(0)
                End If
                obeCotizacionListas.valorParametrosGenerales = valor
            End If
            If (drd.NextResult) Then
                Dim lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    lista.Add(obj)
                End While
                obeCotizacionListas.listaEmpresa = lista
            End If
            If (drd.NextResult) Then
                Dim lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    lista.Add(obj)
                End While
                obeCotizacionListas.listaTienda = lista
            End If
            If (drd.NextResult) Then
                Dim lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    lista.Add(obj)
                End While
                obeCotizacionListas.listaSerie = lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero2)
                Dim obj As beCampoEntero2
                While drd.Read
                    obj = New beCampoEntero2
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    obj.campo3 = drd.GetString(2)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaTipoDocumento = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaMoneda = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaEstadoDocumento = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaTipoOperacion = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaMotivoTraslado = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaCondicionPago = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaMedioPago = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaTipoPrecio = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero2)
                Dim obj As beCampoEntero2
                While drd.Read
                    obj = New beCampoEntero2
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    obj.campo3 = drd.GetString(2)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaRolCliente = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaTipoAgente = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCboDepartamento)
                Dim obj As beCboDepartamento
                While drd.Read
                    obj = New beCboDepartamento
                    obj.CodDpto = drd.GetString(0)
                    obj.Nombre = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaDepartamento = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCboProvincia)
                Dim obj As beCboProvincia
                While drd.Read
                    obj = New beCboProvincia
                    obj.CodProv = drd.GetString(0)
                    obj.Nombre = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaProvincia = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCboDistrito)
                Dim obj As beCboDistrito
                While drd.Read
                    obj = New beCboDistrito
                    obj.CodDist = drd.GetString(0)
                    obj.Nombre = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaDistrito = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaAlmacen = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaTipoExistencia = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaLinea = Lista
            End If
            'If (drd.NextResult) Then
            '    Dim Lista As New List(Of beCampoEntero)
            '    Dim obj As beCampoEntero
            '    While drd.Read
            '        obj = New beCampoEntero
            '        obj.campo1 = drd.GetInt32(0)
            '        obj.campo2 = drd.GetString(1)
            '        Lista.Add(obj)
            '    End While
            '    obeCotizacionListas.listaSubLinea = Lista
            'End If
            'If (drd.NextResult) Then
            '    Dim Lista As New List(Of beCampoEntero)
            '    Dim obj As beCampoEntero
            '    While drd.Read
            '        obj = New beCampoEntero
            '        obj.campo1 = drd.GetInt32(0)
            '        obj.campo2 = drd.GetString(1)
            '        Lista.Add(obj)
            '    End While
            '    obeCotizacionListas.listaTipoTabla = Lista
            'End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaPersona = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaTipoAlmacen = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCampoEntero)
                Dim obj As beCampoEntero
                While drd.Read
                    obj = New beCampoEntero
                    obj.campo1 = drd.GetInt32(0)
                    obj.campo2 = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaMagnitud = Lista
            End If
            If (drd.NextResult) Then
                If (drd.HasRows) Then
                    drd.Read()
                    obeCotizacionListas.TipoDiseno = drd.GetInt32(0)
                End If
            End If
            If (drd.NextResult) Then
                If (drd.HasRows) Then
                    drd.Read()
                    obeCotizacionListas.IdMedioPago = drd.GetInt32(0)
                End If
            End If
            If (drd.NextResult) Then
                If (drd.HasRows) Then
                    drd.Read()
                    obeCotizacionListas.TasaIGV = drd.GetDecimal(0)
                End If
            End If
            If (drd.NextResult) Then
                If (drd.HasRows) Then
                    drd.Read()
                    obeCotizacionListas.FechaActual = drd.GetDateTime(0)
                End If
            End If
            If (drd.NextResult) Then
                If (drd.HasRows) Then
                    drd.Read()
                    obeCotizacionListas.TipoPrecio = drd.GetInt32(0)
                End If
            End If
            If (drd.NextResult) Then
                If (drd.HasRows) Then
                    Dim Lista As New List(Of Decimal)
                    While drd.Read
                        Lista.Add(drd.GetDecimal(0))
                    End While
                    obeCotizacionListas.listaParametros = Lista
                End If
            End If
            If (drd.NextResult) Then
                If (drd.HasRows) Then
                    drd.Read()
                    Dim obj As New beCampoCadena
                    obj.campo1 = drd.GetString(0)
                    obj.campo2 = drd.GetString(1)
                    obeCotizacionListas.TiendaUbigeo = obj
                End If
            End If
            If (drd.NextResult) Then
                If (drd.HasRows) Then
                    drd.Read()
                    obeCotizacionListas.NroDocumento = drd.GetString(0)
                End If
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCboDepartamento)
                Dim obj As beCboDepartamento
                While drd.Read
                    obj = New beCboDepartamento
                    obj.CodDpto = drd.GetString(0)
                    obj.Nombre = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaDepartamentoPartida = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCboProvincia)
                Dim obj As beCboProvincia
                While drd.Read
                    obj = New beCboProvincia
                    obj.CodProv = drd.GetString(0)
                    obj.Nombre = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaProvinciaPartida = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of beCboDistrito)
                Dim obj As beCboDistrito
                While drd.Read
                    obj = New beCboDistrito
                    obj.CodDist = drd.GetString(0)
                    obj.Nombre = drd.GetString(1)
                    Lista.Add(obj)
                End While
                obeCotizacionListas.listaDistritoPartida = Lista
            End If
            If (drd.NextResult) Then
                Dim Lista As New List(Of Integer)
                While drd.Read
                    Lista.Add(drd.GetInt32(0))
                End While
                obeCotizacionListas.listaPermisos = Lista
            End If
            drd.Close()
        End If
        Return obeCotizacionListas
    End Function

    Public Function obtenerProductos(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal CodSubLinea As String, ByVal Descripcion As String,
       ByVal IdTienda As Integer, ByVal IdMonedaDestino As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer,
       ByVal idtipopv As Integer, ByVal pagesize As Integer, ByVal pagenumber As Integer, ByVal IdUsuario As Integer,
       ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer, ByVal IdCliente As Integer, ByVal tableTipoTabla As DataTable,
       ByVal codigoProducto As String, ByVal filtroProductoCampania As Boolean, ByVal codbarras As String, ByVal IdTipoExistencia As Integer,
       ByVal codigoProveedor As String, ByVal IdTipoOperacion As Integer, ByVal con As SqlConnection) As List(Of Entidades.Catalogo)
        Descripcion = Descripcion.Replace("*", "%")

        Dim cmd As New SqlCommand("CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarrasAvanzado_HTE", con)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@IdTipoExistencia", IdTipoExistencia)
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@CodSubLinea", CodSubLinea)
        cmd.Parameters.AddWithValue("@Descripcion", Descripcion)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdMoneda", IdMonedaDestino)
        cmd.Parameters.AddWithValue("@IdTipoPV", idtipopv)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@PageNumber", pagenumber)
        cmd.Parameters.AddWithValue("@PageSize", pagesize)
        cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario)
        cmd.Parameters.AddWithValue("@IdCondicionPago", IdCondicionPago)
        cmd.Parameters.AddWithValue("@IdMedioPago", IdMedioPago)
        cmd.Parameters.AddWithValue("@IdCliente", IdCliente)
        cmd.Parameters.AddWithValue("@Tabla", tableTipoTabla)
        cmd.Parameters.AddWithValue("@CodigoProducto", codigoProducto)
        cmd.Parameters.AddWithValue("@filtroProductoCampania", filtroProductoCampania)
        cmd.Parameters.AddWithValue("@codbarras", codbarras)
        cmd.Parameters.AddWithValue("@CodigoProveedor", codigoProveedor)
        cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion)

        Dim lector As SqlDataReader
        lector = cmd.ExecuteReader()
        Dim Lista As New List(Of Entidades.Catalogo)
        Do While lector.Read
            Dim objCatalogo As New Entidades.Catalogo
            With objCatalogo
                .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                .Descripcion = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                .IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                .idunidadMedidaPrincipal = CInt(IIf(IsDBNull(lector.Item("idUnidadMedidaPrincipal")) = True, 0, lector.Item("idUnidadMedidaPrincipal")))
                .NomLinea = CStr(IIf(IsDBNull(lector.Item("NomLinea")) = True, "", lector.Item("NomLinea")))
                .NomSubLinea = CStr(IIf(IsDBNull(lector.Item("NomSubLinea")) = True, "", lector.Item("NomSubLinea")))
                .SimbMoneda = CStr(IIf(IsDBNull(lector.Item("MonedaDestino")) = True, "", lector.Item("MonedaDestino")))
                .PrecioSD = CDec(IIf(IsDBNull(lector.Item("ValorFinal")) = True, 0, lector.Item("ValorFinal")))
                .PrecioLista = CDec(IIf(IsDBNull(lector.Item("ValorFinal")) = True, 0, lector.Item("ValorFinal")))
                .Percepcion = CDec(IIf(IsDBNull(lector.Item("Percepcion")) = True, 0, lector.Item("Percepcion")))
                .Detraccion = CDec(IIf(IsDBNull(lector.Item("Detraccion")) = True, 0, lector.Item("Detraccion")))
                .IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                .NomTipoPV = CStr(IIf(IsDBNull(lector.Item("NomTipoPV")) = True, "", lector.Item("NomTipoPV")))
                .CodigoSubLinea = CStr(IIf(IsDBNull(lector.Item("CodigoSubLinea")) = True, 0, lector.Item("CodigoSubLinea")))
                .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacenRef")) = True, 0, lector.Item("IdAlmacenRef")))
                .Almacen = CStr(IIf(IsDBNull(lector.Item("Almacen")) = True, "", lector.Item("Almacen")))
                .PorcentDctoMaximo = CDec(IIf(IsDBNull(lector.Item("PorcentDctoMaximo")) = True, 0, lector.Item("PorcentDctoMaximo")))
                .PrecioBaseDcto = CStr(IIf(IsDBNull(lector.Item("PrecioBaseDcto")) = True, "", lector.Item("PrecioBaseDcto")))
                .StockDisponibleN = CDec(IIf(IsDBNull(lector.Item("StockDisponible")) = True, 0, lector.Item("StockDisponible")))
                .CodigoProducto = CStr(IIf(IsDBNull(lector.Item("CodigoProducto")) = True, "", lector.Item("CodigoProducto")))
                .pvComercial = CDec(IIf(IsDBNull(lector.Item("ppv_Valor")) = True, 0, lector.Item("ppv_Valor")))
                .Kit = CBool(IIf(IsDBNull(lector.Item("prod_Kit")) = True, 0, lector.Item("prod_Kit")))
                .ExisteCampania_Producto = IIf(lector.GetInt32(lector.GetOrdinal("ExisteCampania_Producto")) = 1, True, False)
                .Prod_CodigoBarras = CStr(IIf(IsDBNull(lector.Item("prod_CodigoBarras")) = True, "", lector.Item("prod_CodigoBarras")))
                .NoVisible = CStr(IIf(IsDBNull(lector.Item("NoVisible")) = True, "", lector.Item("NoVisible")))
                .UMProducto = CStr(lector.Item("UMProducto"))
                .cadenaComboUnidadMedida = CStr(IIf(IsDBNull(lector.Item("cadenaUnidadMedida")) = True, "", lector.Item("cadenaUnidadMedida")))
            End With
            Lista.Add(objCatalogo)
        Loop
        Return Lista

    End Function

    Public Function adicionar(ByVal con As SqlConnection, ByVal Documento As Entidades.Documento, ByVal AnexoDocumento As Entidades.Anexo_Documento, ByVal lista_DetalleDocumento As String, ByVal lista_DetalleConcepto As String,
      ByVal objObservacion As Entidades.Observacion, ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal objMontoRegimenPercepcion As Entidades.MontoRegimen,
      ByVal lista_RelacionDocumento As String, ByVal trx As SqlTransaction) As Integer
        Dim id As Integer = 0

        Dim cmd As New SqlCommand("uspHte_RegistrarCotizacion", con, trx)
        cmd.CommandType = CommandType.StoredProcedure

        'cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

        'Insert Cabecera
        cmd.Parameters.AddWithValue("@doc_Codigo", IIf(Documento.Codigo = Nothing, DBNull.Value, Documento.Codigo))
        cmd.Parameters.AddWithValue("@doc_Serie", IIf(Documento.Serie = Nothing, DBNull.Value, Documento.Serie))
        cmd.Parameters.AddWithValue("@doc_FechaEmision", IIf(Documento.FechaEmision = Nothing, DBNull.Value, Documento.FechaEmision))
        cmd.Parameters.AddWithValue("@doc_FechaIniTraslado", IIf(Documento.FechaIniTraslado = Nothing, DBNull.Value, Documento.FechaIniTraslado))
        cmd.Parameters.AddWithValue("@doc_FechaAentregar", IIf(Documento.FechaAEntregar = Nothing, DBNull.Value, Documento.FechaAEntregar))
        cmd.Parameters.AddWithValue("@doc_FechaEntrega", IIf(Documento.FechaEntrega = Nothing, DBNull.Value, Documento.FechaEntrega))
        cmd.Parameters.AddWithValue("@doc_FechaVenc", IIf(Documento.FechaVenc = Nothing, DBNull.Value, Documento.FechaVenc))
        cmd.Parameters.AddWithValue("@doc_ImporteTotal", IIf(Documento.ImporteTotal = Nothing, DBNull.Value, Documento.ImporteTotal))
        cmd.Parameters.AddWithValue("@doc_Descuento", IIf(Documento.Descuento = Nothing, DBNull.Value, Documento.Descuento))
        cmd.Parameters.AddWithValue("@doc_SubTotal", IIf(Documento.SubTotal = Nothing, DBNull.Value, Documento.SubTotal))
        cmd.Parameters.AddWithValue("@doc_Igv", IIf(Documento.IGV = Nothing, DBNull.Value, Documento.IGV))
        cmd.Parameters.AddWithValue("@doc_Total", IIf(Documento.Total = Nothing, DBNull.Value, Documento.Total))
        cmd.Parameters.AddWithValue("@doc_TotalLetras", IIf(Documento.TotalLetras = Nothing, DBNull.Value, Documento.TotalLetras))
        cmd.Parameters.AddWithValue("@doc_TotalAPagar", IIf(Documento.TotalAPagar = Nothing, DBNull.Value, Documento.TotalAPagar))
        cmd.Parameters.AddWithValue("@doc_ValorReferencial", IIf(Documento.ValorReferencial = Nothing, DBNull.Value, Documento.ValorReferencial))
        cmd.Parameters.AddWithValue("@doc_Utilidad", IIf(Documento.Utilidad = Nothing, DBNull.Value, Documento.Utilidad))
        cmd.Parameters.AddWithValue("@IdPersona", IIf(Documento.IdPersona = Nothing, DBNull.Value, Documento.IdPersona))
        cmd.Parameters.AddWithValue("@IdUsuario", IIf(Documento.IdUsuario = Nothing, DBNull.Value, Documento.IdUsuario))
        cmd.Parameters.AddWithValue("@IdTransportista", IIf(Documento.IdTransportista = Nothing, DBNull.Value, Documento.IdTransportista))
        cmd.Parameters.AddWithValue("@IdRemitente", IIf(Documento.IdRemitente = Nothing, DBNull.Value, Documento.IdRemitente))
        cmd.Parameters.AddWithValue("@IdDestinatario", IIf(Documento.IdDestinatario = Nothing, DBNull.Value, Documento.IdDestinatario))
        cmd.Parameters.AddWithValue("@IdEstadoDoc", IIf(Documento.IdEstadoDoc = Nothing, DBNull.Value, Documento.IdEstadoDoc))
        cmd.Parameters.AddWithValue("@IdCondicionPago", IIf(Documento.IdCondicionPago = Nothing, DBNull.Value, Documento.IdCondicionPago))
        cmd.Parameters.AddWithValue("@IdMoneda", IIf(Documento.IdMoneda = Nothing, DBNull.Value, Documento.IdMoneda))
        cmd.Parameters.AddWithValue("@LugarEntrega", IIf(Documento.LugarEntrega = Nothing, DBNull.Value, Documento.LugarEntrega))
        cmd.Parameters.AddWithValue("@IdTipoOperacion", IIf(Documento.IdTipoOperacion = Nothing, DBNull.Value, Documento.IdTipoOperacion))
        cmd.Parameters.AddWithValue("@IdTienda", IIf(Documento.IdTienda = Nothing, DBNull.Value, Documento.IdTienda))
        cmd.Parameters.AddWithValue("@IdSerie", IIf(Documento.IdSerie = Nothing, DBNull.Value, Documento.IdSerie))
        cmd.Parameters.AddWithValue("@doc_ExportadoConta", IIf(Documento.ExportadoConta = Nothing, DBNull.Value, Documento.ExportadoConta))
        cmd.Parameters.AddWithValue("@doc_NroVoucherConta", IIf(Documento.NroVoucherConta = Nothing, DBNull.Value, Documento.NroVoucherConta))
        cmd.Parameters.AddWithValue("@IdEmpresa", IIf(Documento.IdEmpresa = Nothing, DBNull.Value, Documento.IdEmpresa))
        cmd.Parameters.AddWithValue("@IdChofer", IIf(Documento.IdChofer = Nothing, DBNull.Value, Documento.IdChofer))
        cmd.Parameters.AddWithValue("@IdMotivoT", IIf(Documento.IdMotivoT = Nothing, DBNull.Value, Documento.IdMotivoT))
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IIf(Documento.IdTipoDocumento = Nothing, DBNull.Value, Documento.IdTipoDocumento))
        cmd.Parameters.AddWithValue("@IdVehiculo", IIf(Documento.IdVehiculo = Nothing, DBNull.Value, Documento.IdVehiculo))
        'cmd.Parameters.AddWithValue("@IdDocumento", output) 
        cmd.Parameters.AddWithValue("@IdEstadoCan", IIf(Documento.IdEstadoCancelacion = Nothing, DBNull.Value, Documento.IdEstadoCancelacion))
        cmd.Parameters.AddWithValue("@IdEstadoEnt", IIf(Documento.IdEstadoEntrega = Nothing, DBNull.Value, Documento.IdEstadoEntrega))
        cmd.Parameters.AddWithValue("@IdAlmacen", IIf(Documento.IdAlmacen = Nothing, DBNull.Value, Documento.IdAlmacen))
        cmd.Parameters.AddWithValue("@IdTipoPV", IIf(Documento.IdTipoPV = Nothing, DBNull.Value, Documento.IdTipoPV))
        cmd.Parameters.AddWithValue("@IdCaja", IIf(Documento.IdCaja = Nothing, DBNull.Value, Documento.IdCaja))
        cmd.Parameters.AddWithValue("@IdMedioPagoCredito", IIf(Documento.IdMedioPagoCredito = Nothing, DBNull.Value, Documento.IdMedioPagoCredito))
        cmd.Parameters.AddWithValue("@doc_CompPercepcion", IIf(Documento.CompPercepcion = Nothing, DBNull.Value, Documento.CompPercepcion))
        cmd.Parameters.AddWithValue("@doc_FechaCancelacion", IIf(Documento.FechaCancelacion = Nothing, DBNull.Value, Documento.FechaCancelacion))
        cmd.Parameters.AddWithValue("@IdUsuarioComision", IIf(Documento.IdUsuarioComision = Nothing, DBNull.Value, Documento.IdUsuarioComision))

        ''Insert Anexo
        Dim prm() As SqlParameter
        prm = GetVectorParametros(AnexoDocumento)
        cmd.Parameters.AddRange(prm)

        cmd.Parameters.AddWithValue("@ListaDetalleDocumento", lista_DetalleDocumento)
        cmd.Parameters.AddWithValue("@ListaDetalleConcepto", lista_DetalleConcepto)

        'Observación
        If (objObservacion IsNot Nothing) Then
            cmd.Parameters.AddWithValue("@ob_Observacion", IIf(objObservacion.Observacion = Nothing, DBNull.Value, objObservacion.Observacion))
        Else
            cmd.Parameters.AddWithValue("@ob_Observacion", DBNull.Value)
        End If
        'Punto de Partida
        If (objPuntoPartida IsNot Nothing) Then
            cmd.Parameters.AddWithValue("@pp_IdAlmacen", IIf(objPuntoPartida.IdAlmacen = Nothing, DBNull.Value, objPuntoPartida.IdAlmacen))
            cmd.Parameters.AddWithValue("@pp_Ubigeo", IIf(objPuntoPartida.Ubigeo = Nothing, DBNull.Value, objPuntoPartida.Ubigeo))
            cmd.Parameters.AddWithValue("@pp_Direccion", IIf(objPuntoPartida.Direccion = Nothing, DBNull.Value, objPuntoPartida.Direccion))
            'cmd.Parameters.AddWithValue("@IdDocumento", puntopartida.IdDocumento)
            cmd.Parameters.AddWithValue("@pp_IdTienda", IIf(objPuntoPartida.IdTienda = Nothing, DBNull.Value, objPuntoPartida.IdTienda))
        Else
            cmd.Parameters.AddWithValue("@pp_IdAlmacen", DBNull.Value)
            cmd.Parameters.AddWithValue("@pp_Ubigeo", DBNull.Value)
            cmd.Parameters.AddWithValue("@pp_Direccion", DBNull.Value)
            'cmd.Parameters.AddWithValue("@IdDocumento", puntopartida.IdDocumento)
            cmd.Parameters.AddWithValue("@pp_IdTienda", DBNull.Value)
        End If
        'Punto de Partida
        If (objPuntoLlegada IsNot Nothing) Then
            cmd.Parameters.AddWithValue("@pll_IdAlmacen", IIf(objPuntoLlegada.IdAlmacen = Nothing, DBNull.Value, objPuntoLlegada.IdAlmacen))
            cmd.Parameters.AddWithValue("@pll_Ubigeo", IIf(objPuntoLlegada.pll_Ubigeo = Nothing, DBNull.Value, objPuntoLlegada.pll_Ubigeo))
            cmd.Parameters.AddWithValue("@pll_Direccion", IIf(objPuntoLlegada.pll_Direccion = Nothing, DBNull.Value, objPuntoLlegada.pll_Direccion))
            'cmd.Parameters.AddWithValue("@IdDocumento", puntollegada.IdDocumento)
            cmd.Parameters.AddWithValue("@pll_IdTienda", IIf(objPuntoLlegada.IdTienda = Nothing, DBNull.Value, objPuntoLlegada.IdTienda))
        Else
            cmd.Parameters.AddWithValue("@pll_IdAlmacen", DBNull.Value)
            cmd.Parameters.AddWithValue("@pll_Ubigeo", DBNull.Value)
            cmd.Parameters.AddWithValue("@pll_Direccion", DBNull.Value)
            'cmd.Parameters.AddWithValue("@IdDocumento", puntollegada.IdDocumento)
            cmd.Parameters.AddWithValue("@pll_IdTienda", DBNull.Value)
        End If

        'Regimen Percepción
        If (objMontoRegimenPercepcion IsNot Nothing) Then
            'cmd.Parameters.AddWithValue("@IdDocumento", objMontoRegimenPercepcion.IdDocumento)
            cmd.Parameters.AddWithValue("@mr_Monto", IIf(objMontoRegimenPercepcion.Monto = Nothing, DBNull.Value, objMontoRegimenPercepcion.Monto))
            cmd.Parameters.AddWithValue("@ro_Id", IIf(objMontoRegimenPercepcion.ro_Id = Nothing, DBNull.Value, objMontoRegimenPercepcion.ro_Id))
        Else
            cmd.Parameters.AddWithValue("@mr_Monto", DBNull.Value)
            cmd.Parameters.AddWithValue("@ro_Id", DBNull.Value)
        End If

        'Lista de Relación Documento
        cmd.Parameters.AddWithValue("@ListaRelacionDocumento", lista_RelacionDocumento)

        Dim drd As SqlDataReader = cmd.ExecuteReader()
        If (drd IsNot Nothing) Then
            If (drd.HasRows) Then
                drd.Read()
                id = drd.GetInt32(0)
            End If
            drd.Close()
        End If
        Return id
    End Function

    Public Function actualizar(ByVal con As SqlConnection, ByVal Documento As Entidades.Documento, ByVal AnexoDocumento As Entidades.Anexo_Documento, ByVal lista_DetalleDocumento As String, ByVal lista_DetalleConcepto As String,
      ByVal objObservacion As Entidades.Observacion, ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal objMontoRegimenPercepcion As Entidades.MontoRegimen,
      ByVal lista_RelacionDocumento As String, ByVal trx As SqlTransaction) As Integer
        Dim id As Integer = 0

        Dim cmd As New SqlCommand("uspHte_ActualizarCotizacion", con, trx)
        cmd.CommandType = CommandType.StoredProcedure

        'Insert Cabecera
        cmd.Parameters.AddWithValue("@IdDocumento", Documento.Id)
        cmd.Parameters.AddWithValue("@doc_Codigo", IIf(Documento.Codigo = Nothing, DBNull.Value, Documento.Codigo))
        cmd.Parameters.AddWithValue("@doc_Serie", IIf(Documento.Serie = Nothing, DBNull.Value, Documento.Serie))
        cmd.Parameters.AddWithValue("@doc_FechaEmision", IIf(Documento.FechaEmision = Nothing, DBNull.Value, Documento.FechaEmision))
        cmd.Parameters.AddWithValue("@doc_FechaIniTraslado", IIf(Documento.FechaIniTraslado = Nothing, DBNull.Value, Documento.FechaIniTraslado))
        cmd.Parameters.AddWithValue("@doc_FechaAentregar", IIf(Documento.FechaAEntregar = Nothing, DBNull.Value, Documento.FechaAEntregar))
        cmd.Parameters.AddWithValue("@doc_FechaEntrega", IIf(Documento.FechaEntrega = Nothing, DBNull.Value, Documento.FechaEntrega))
        cmd.Parameters.AddWithValue("@doc_FechaVenc", IIf(Documento.FechaVenc = Nothing, DBNull.Value, Documento.FechaVenc))
        cmd.Parameters.AddWithValue("@doc_ImporteTotal", IIf(Documento.ImporteTotal = Nothing, DBNull.Value, Documento.ImporteTotal))
        cmd.Parameters.AddWithValue("@doc_Descuento", IIf(Documento.Descuento = Nothing, DBNull.Value, Documento.Descuento))
        cmd.Parameters.AddWithValue("@doc_SubTotal", IIf(Documento.SubTotal = Nothing, DBNull.Value, Documento.SubTotal))
        cmd.Parameters.AddWithValue("@doc_Igv", IIf(Documento.IGV = Nothing, DBNull.Value, Documento.IGV))
        cmd.Parameters.AddWithValue("@doc_Total", IIf(Documento.Total = Nothing, DBNull.Value, Documento.Total))
        cmd.Parameters.AddWithValue("@doc_TotalLetras", IIf(Documento.TotalLetras = Nothing, DBNull.Value, Documento.TotalLetras))
        cmd.Parameters.AddWithValue("@doc_TotalAPagar", IIf(Documento.TotalAPagar = Nothing, DBNull.Value, Documento.TotalAPagar))
        cmd.Parameters.AddWithValue("@doc_ValorReferencial", IIf(Documento.ValorReferencial = Nothing, DBNull.Value, Documento.ValorReferencial))
        cmd.Parameters.AddWithValue("@doc_Utilidad", IIf(Documento.Utilidad = Nothing, DBNull.Value, Documento.Utilidad))
        cmd.Parameters.AddWithValue("@IdPersona", IIf(Documento.IdPersona = Nothing, DBNull.Value, Documento.IdPersona))
        cmd.Parameters.AddWithValue("@IdUsuario", IIf(Documento.IdUsuario = Nothing, DBNull.Value, Documento.IdUsuario))
        cmd.Parameters.AddWithValue("@IdTransportista", IIf(Documento.IdTransportista = Nothing, DBNull.Value, Documento.IdTransportista))
        cmd.Parameters.AddWithValue("@IdRemitente", IIf(Documento.IdRemitente = Nothing, DBNull.Value, Documento.IdRemitente))
        cmd.Parameters.AddWithValue("@IdDestinatario", IIf(Documento.IdDestinatario = Nothing, DBNull.Value, Documento.IdDestinatario))
        cmd.Parameters.AddWithValue("@IdEstadoDoc", IIf(Documento.IdEstadoDoc = Nothing, DBNull.Value, Documento.IdEstadoDoc))
        cmd.Parameters.AddWithValue("@IdCondicionPago", IIf(Documento.IdCondicionPago = Nothing, DBNull.Value, Documento.IdCondicionPago))
        cmd.Parameters.AddWithValue("@IdMoneda", IIf(Documento.IdMoneda = Nothing, DBNull.Value, Documento.IdMoneda))
        cmd.Parameters.AddWithValue("@LugarEntrega", IIf(Documento.LugarEntrega = Nothing, DBNull.Value, Documento.LugarEntrega))
        cmd.Parameters.AddWithValue("@IdTipoOperacion", IIf(Documento.IdTipoOperacion = Nothing, DBNull.Value, Documento.IdTipoOperacion))
        cmd.Parameters.AddWithValue("@IdTienda", IIf(Documento.IdTienda = Nothing, DBNull.Value, Documento.IdTienda))
        cmd.Parameters.AddWithValue("@IdSerie", IIf(Documento.IdSerie = Nothing, DBNull.Value, Documento.IdSerie))
        cmd.Parameters.AddWithValue("@doc_ExportadoConta", IIf(Documento.ExportadoConta = Nothing, DBNull.Value, Documento.ExportadoConta))
        cmd.Parameters.AddWithValue("@doc_NroVoucherConta", IIf(Documento.NroVoucherConta = Nothing, DBNull.Value, Documento.NroVoucherConta))
        cmd.Parameters.AddWithValue("@IdEmpresa", IIf(Documento.IdEmpresa = Nothing, DBNull.Value, Documento.IdEmpresa))
        cmd.Parameters.AddWithValue("@IdChofer", IIf(Documento.IdChofer = Nothing, DBNull.Value, Documento.IdChofer))
        cmd.Parameters.AddWithValue("@IdMotivoT", IIf(Documento.IdMotivoT = Nothing, DBNull.Value, Documento.IdMotivoT))
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IIf(Documento.IdTipoDocumento = Nothing, DBNull.Value, Documento.IdTipoDocumento))
        cmd.Parameters.AddWithValue("@IdVehiculo", IIf(Documento.IdVehiculo = Nothing, DBNull.Value, Documento.IdVehiculo))
        'cmd.Parameters.AddWithValue("@IdDocumento", output) 
        cmd.Parameters.AddWithValue("@IdEstadoCan", IIf(Documento.IdEstadoCancelacion = Nothing, DBNull.Value, Documento.IdEstadoCancelacion))
        cmd.Parameters.AddWithValue("@IdEstadoEnt", IIf(Documento.IdEstadoEntrega = Nothing, DBNull.Value, Documento.IdEstadoEntrega))
        cmd.Parameters.AddWithValue("@IdAlmacen", IIf(Documento.IdAlmacen = Nothing, DBNull.Value, Documento.IdAlmacen))
        cmd.Parameters.AddWithValue("@IdTipoPV", IIf(Documento.IdTipoPV = Nothing, DBNull.Value, Documento.IdTipoPV))
        cmd.Parameters.AddWithValue("@IdCaja", IIf(Documento.IdCaja = Nothing, DBNull.Value, Documento.IdCaja))
        cmd.Parameters.AddWithValue("@IdMedioPagoCredito", IIf(Documento.IdMedioPagoCredito = Nothing, DBNull.Value, Documento.IdMedioPagoCredito))
        cmd.Parameters.AddWithValue("@doc_CompPercepcion", IIf(Documento.CompPercepcion = Nothing, DBNull.Value, Documento.CompPercepcion))
        cmd.Parameters.AddWithValue("@doc_FechaCancelacion", IIf(Documento.FechaCancelacion = Nothing, DBNull.Value, Documento.FechaCancelacion))
        cmd.Parameters.AddWithValue("@IdUsuarioComision", IIf(Documento.IdUsuarioComision = Nothing, DBNull.Value, Documento.IdUsuarioComision))

        ''Insert Anexo
        Dim prm() As SqlParameter
        prm = GetVectorParametros(AnexoDocumento)
        cmd.Parameters.AddRange(prm)

        cmd.Parameters.AddWithValue("@ListaDetalleDocumento", lista_DetalleDocumento)
        cmd.Parameters.AddWithValue("@ListaDetalleConcepto", lista_DetalleConcepto)

        'Observación
        If (objObservacion IsNot Nothing) Then
            cmd.Parameters.AddWithValue("@ob_Observacion", IIf(objObservacion.Observacion = Nothing, DBNull.Value, objObservacion.Observacion))
        Else
            cmd.Parameters.AddWithValue("@ob_Observacion", DBNull.Value)
        End If
        'Punto de Partida
        If (objPuntoPartida IsNot Nothing) Then
            cmd.Parameters.AddWithValue("@pp_IdAlmacen", IIf(objPuntoPartida.IdAlmacen = Nothing, DBNull.Value, objPuntoPartida.IdAlmacen))
            cmd.Parameters.AddWithValue("@pp_Ubigeo", IIf(objPuntoPartida.Ubigeo = Nothing, DBNull.Value, objPuntoPartida.Ubigeo))
            cmd.Parameters.AddWithValue("@pp_Direccion", IIf(objPuntoPartida.Direccion = Nothing, DBNull.Value, objPuntoPartida.Direccion))
            'cmd.Parameters.AddWithValue("@IdDocumento", puntopartida.IdDocumento)
            cmd.Parameters.AddWithValue("@pp_IdTienda", IIf(objPuntoPartida.IdTienda = Nothing, DBNull.Value, objPuntoPartida.IdTienda))
        Else
            cmd.Parameters.AddWithValue("@pp_IdAlmacen", DBNull.Value)
            cmd.Parameters.AddWithValue("@pp_Ubigeo", DBNull.Value)
            cmd.Parameters.AddWithValue("@pp_Direccion", DBNull.Value)
            'cmd.Parameters.AddWithValue("@IdDocumento", puntopartida.IdDocumento)
            cmd.Parameters.AddWithValue("@pp_IdTienda", DBNull.Value)
        End If
        'Punto de Partida
        If (objPuntoLlegada IsNot Nothing) Then
            cmd.Parameters.AddWithValue("@pll_IdAlmacen", IIf(objPuntoLlegada.IdAlmacen = Nothing, DBNull.Value, objPuntoLlegada.IdAlmacen))
            cmd.Parameters.AddWithValue("@pll_Ubigeo", IIf(objPuntoLlegada.pll_Ubigeo = Nothing, DBNull.Value, objPuntoLlegada.pll_Ubigeo))
            cmd.Parameters.AddWithValue("@pll_Direccion", IIf(objPuntoLlegada.pll_Direccion = Nothing, DBNull.Value, objPuntoLlegada.pll_Direccion))
            'cmd.Parameters.AddWithValue("@IdDocumento", puntollegada.IdDocumento)
            cmd.Parameters.AddWithValue("@pll_IdTienda", IIf(objPuntoLlegada.IdTienda = Nothing, DBNull.Value, objPuntoLlegada.IdTienda))
        Else
            cmd.Parameters.AddWithValue("@pll_IdAlmacen", DBNull.Value)
            cmd.Parameters.AddWithValue("@pll_Ubigeo", DBNull.Value)
            cmd.Parameters.AddWithValue("@pll_Direccion", DBNull.Value)
            'cmd.Parameters.AddWithValue("@IdDocumento", puntollegada.IdDocumento)
            cmd.Parameters.AddWithValue("@pll_IdTienda", DBNull.Value)
        End If

        'Regimen Percepción
        If (objMontoRegimenPercepcion IsNot Nothing) Then
            'cmd.Parameters.AddWithValue("@IdDocumento", objMontoRegimenPercepcion.IdDocumento)
            cmd.Parameters.AddWithValue("@mr_Monto", IIf(objMontoRegimenPercepcion.Monto = Nothing, DBNull.Value, objMontoRegimenPercepcion.Monto))
            cmd.Parameters.AddWithValue("@ro_Id", IIf(objMontoRegimenPercepcion.ro_Id = Nothing, DBNull.Value, objMontoRegimenPercepcion.ro_Id))
        Else
            cmd.Parameters.AddWithValue("@mr_Monto", DBNull.Value)
            cmd.Parameters.AddWithValue("@ro_Id", DBNull.Value)
        End If

        'Lista de Relación Documento
        cmd.Parameters.AddWithValue("@ListaRelacionDocumento", lista_RelacionDocumento)

        Dim drd As SqlDataReader = cmd.ExecuteReader()
        If (drd IsNot Nothing) Then
            If (drd.HasRows) Then
                drd.Read()
                id = drd.GetInt32(0)
            End If
            drd.Close()
        End If
        Return id
    End Function

    Public Function buscarCotizacion(ByVal IdSerie As Integer, ByVal nroDocumento As Integer, ByVal IdDocumento As Integer, ByVal IdTipoDocumento As Integer, ByVal Consignacion As Boolean, ByVal IdMagnitudPeso As Integer, ByVal PesoTotal As Integer, ByVal IdTipoDocumentoMP As Integer, ByVal con As SqlConnection) As beCotizacionBuscar
        Dim obeCotizacionBuscar As beCotizacionBuscar = Nothing

        Dim cmd As New SqlCommand("uspHte_BuscarCotizacion", con)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@IdSerie", IdSerie)
        cmd.Parameters.AddWithValue("@NroDocumento", nroDocumento)
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        'Lista de Productos
        cmd.Parameters.AddWithValue("@Consignacion", Consignacion)
        'Lista de Conceptos
        cmd.Parameters.AddWithValue("@IdTipoDocumentoDC", IdTipoDocumento)
        'Punto de Partida
        'Punto de Llegada
        'Observación
        'Relación Documento
        'Peso
        'TipoOperacion
        cmd.Parameters.AddWithValue("@IdMagnitudPeso", IdMagnitudPeso)
        cmd.Parameters.AddWithValue("@PesoTotal", PesoTotal)
        'MedioPago
        cmd.Parameters.AddWithValue("@IdTipoDocumentoMP", IdTipoDocumentoMP)

        Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

        If (lector IsNot Nothing) Then

            If (lector.Read) Then
                obeCotizacionBuscar = New beCotizacionBuscar
                With obeCotizacionBuscar

                    .getObjTipoAgente.Tasa = CDec(IIf(IsDBNull(lector("TasaAgente")) = True, 0, lector("TasaAgente")))
                    .Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                    .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                    .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))
                    .FechaAEntregar = CDate(IIf(IsDBNull(lector("doc_FechaAentregar")) = True, Nothing, lector("doc_FechaAentregar")))
                    .FechaVenc = CDate(IIf(IsDBNull(lector("doc_FechaVenc")) = True, Nothing, lector("doc_FechaVenc")))
                    .ImporteTotal = CDec(IIf(IsDBNull(lector("doc_ImporteTotal")) = True, 0, lector("doc_ImporteTotal")))
                    .Descuento = CDec(IIf(IsDBNull(lector("doc_Descuento")) = True, 0, lector("doc_Descuento")))
                    .SubTotal = CDec(IIf(IsDBNull(lector("doc_SubTotal")) = True, 0, lector("doc_SubTotal")))
                    .IGV = CDec(IIf(IsDBNull(lector("doc_Igv")) = True, 0, lector("doc_Igv")))
                    .getObjImpuesto.Tasa = CDec(IIf(IsDBNull(lector("tasaIGV")) = True, 0, lector("tasaIGV")))
                    .Total = CDec(IIf(IsDBNull(lector("doc_Total")) = True, 0, lector("doc_Total")))
                    .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")) = True, 0, lector("doc_TotalAPagar")))
                    .Percepcion = CDec(IIf(IsDBNull(lector("Percepcion")) = True, 0, lector("Percepcion")))
                    .CompPercepcion = objDaoMantenedor.UCBool(lector("doc_CompPercepcion"))

                    .IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")) = True, 0, lector("IdPersona")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(lector("IdEstadoDoc")) = True, 0, lector("IdEstadoDoc")))
                    .IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")) = True, 0, lector("IdMoneda")))
                    .IdCondicionPago = CInt(IIf(IsDBNull(lector("IdCondicionPago")) = True, 0, lector("IdCondicionPago")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(lector("IdTipoOperacion")) = True, 0, lector("IdTipoOperacion")))
                    .IdMotivoT = CInt(IIf(IsDBNull(lector("IdMotivoT")) = True, 0, lector("IdMotivoT")))
                    .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")) = True, 0, lector("IdTienda")))
                    .IdCaja = CInt(IIf(IsDBNull(lector("IdCaja")) = True, 0, lector("IdCaja")))
                    .IdSerie = CInt(IIf(IsDBNull(lector("IdSerie")) = True, 0, lector("IdSerie")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector("IdEmpresa")) = True, 0, lector("IdEmpresa")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector("IdTipoDocumento")) = True, 0, lector("IdTipoDocumento")))
                    .IdAlmacen = CInt(IIf(IsDBNull(lector("IdAlmacen")) = True, 0, lector("IdAlmacen")))
                    .IdTipoPV = CInt(IIf(IsDBNull(lector("IdTipoPV")) = True, 0, lector("IdTipoPV")))
                    .getObjAnexoDocumento.IdMedioPago = CInt(IIf(IsDBNull(lector("IdMedioPago")) = True, 0, lector("IdMedioPago")))
                    .getObjAnexoDocumento.NroDiasVigencia = CDec(IIf(IsDBNull(lector("anex_NroDiasVigencia")) = True, 0, lector("anex_NroDiasVigencia")))
                    .getObjAnexoDocumento.TotalConcepto = CDec(IIf(IsDBNull(lector("anex_TotalConcepto")) = True, 0, lector("anex_TotalConcepto")))
                    .getObjAnexoDocumento.anex_DescuentoGlobal = CDec(IIf(IsDBNull(lector("anex_DescuentoGlobal")) = True, 0, lector("anex_DescuentoGlobal")))

                    .IdArea = CInt(IIf(IsDBNull(lector("IdArea")) = True, 0, lector("IdArea")))
                    .getObjPersonaView.ApPaterno = CStr(IIf(IsDBNull(lector("nat_Apepat")) = True, "", lector("nat_Apepat")))
                    .getObjPersonaView.ApMaterno = CStr(IIf(IsDBNull(lector("nat_Apemat")) = True, "", lector("nat_Apemat")))
                    .getObjPersonaView.Nombres = CStr(IIf(IsDBNull(lector("nat_Nombres")) = True, "", lector("nat_Nombres")))

                    .getObjPersonaView.TipoPersona = CInt(IIf(IsDBNull(lector("TipoPersona")) = True, "0", lector("TipoPersona")))
                    .getObjPersonaView.RazonSocial = CStr(IIf(IsDBNull(lector("jur_Rsocial")) = True, "", lector("jur_Rsocial")))
                    .getObjPersonaView.Dni = CStr(IIf(IsDBNull(lector("Dni")) = True, "", lector("Dni")))
                    .getObjPersonaView.Ruc = CStr(IIf(IsDBNull(lector("Ruc")) = True, "", lector("Ruc")))
                    .getObjPersonaView.Direccion = CStr(IIf(IsDBNull(lector("DireccionFiscal")) = True, "", lector("DireccionFiscal")))
                    .getObjTipoAgente.IdAgente = CInt(IIf(IsDBNull(lector("IdAgente")) = True, 0, lector("IdAgente")))
                    .getObjPersonaView.SujetoARetencion = objDaoMantenedor.UCBool(lector("SujetoARetencion"))
                    .getObjPersonaView.IdRol = CInt(IIf(IsDBNull(lector("IdRol")) = True, 0, lector("IdRol")))

                    .getObjMaestroObra.IdPersona = CInt(IIf(IsDBNull(lector("IdMaestroObra")) = True, 0, lector("IdMaestroObra")))
                    .getObjMaestroObra.Descripcion = CStr(IIf(IsDBNull(lector("MaestroObra_Descripcion")) = True, "", lector("MaestroObra_Descripcion")))
                    .getObjMaestroObra.Dni = CStr(IIf(IsDBNull(lector("MaestroObra_Dni")) = True, "", lector("MaestroObra_Dni")))
                    .getObjMaestroObra.Ruc = CStr(IIf(IsDBNull(lector("MaestroObra_Ruc")) = True, "", lector("MaestroObra_Ruc")))

                    .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))
                    .NomTipoOperacion = CStr(IIf(IsDBNull(lector("top_Nombre")) = True, "", lector("top_Nombre")))
                    .DescripcionPersona = CStr(IIf(IsDBNull(lector("DescripcionPersona")) = True, "", lector("DescripcionPersona")))
                    .NomCondicionPago = CStr(IIf(IsDBNull(lector("cp_Nombre")) = True, "", lector("cp_Nombre")))
                    .MedioPago = CStr(IIf(IsDBNull(lector("mp_Nombre")) = True, "", lector("mp_Nombre")))

                    .IdDocumentoRef = CInt(IIf(IsDBNull(lector("IdDocumentoRef")) = True, 0, lector("IdDocumentoRef")))
                    .IdUsuarioComision = CInt(IIf(IsDBNull(lector("IdUsuarioComision")) = True, 0, lector("IdUsuarioComision")))
                    .IdMedioPagoCredito = CInt(IIf(IsDBNull(lector("IdMedioPagoCredito")) = True, 0, lector("IdMedioPagoCredito")))

                    .Retencion = objDaoMantenedor.UCDec(lector("Retencion"))
                    .Detraccion = objDaoMantenedor.UCDec(lector("Detraccion"))

                End With
                If (lector.NextResult) Then
                    While (lector.Read)
                        Dim objDetalleDocumento As New Entidades.DetalleDocumento
                        With objDetalleDocumento
                            .DsctoOriginal = CDec(IIf(IsDBNull(lector("DsctoOriginal")) = True, 0, lector("DsctoOriginal")))
                            .IdDetalleDocumento = CInt(IIf(IsDBNull(lector("IdDetalleDocumento")) = True, 0, lector("IdDetalleDocumento")))
                            .IdDetalleAfecto = CInt(IIf(IsDBNull(lector("IdDetalleAfecto")) = True, 0, lector("IdDetalleAfecto")))
                            .IdDocumento = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                            .IdProducto = CInt(IIf(IsDBNull(lector("IdProducto")) = True, 0, lector("IdProducto")))
                            .IdUnidadMedida = CInt(IIf(IsDBNull(lector("IdUnidadMedida")) = True, 0, lector("IdUnidadMedida")))
                            .Cantidad = CDec(IIf(IsDBNull(lector("dc_Cantidad")) = True, 0, lector("dc_Cantidad")))
                            .PrecioLista = CDec(IIf(IsDBNull(lector("dc_PrecioLista")) = True, 0, lector("dc_PrecioLista")))
                            .PrecioSD = CDec(IIf(IsDBNull(lector("dc_PrecioSD")) = True, 0, lector("dc_PrecioSD")))
                            .Descuento = CDec(IIf(IsDBNull(lector("dc_Descuento")) = True, 0, lector("dc_Descuento")))
                            .PorcentDcto = CDec(IIf(IsDBNull(lector("PorcentDcto")) = True, 0, lector("PorcentDcto")))
                            .PrecioCD = CDec(IIf(IsDBNull(lector("dc_PrecioCD")) = True, 0, lector("dc_PrecioCD")))
                            .Importe = CDec(IIf(IsDBNull(lector("dc_Importe")) = True, 0, lector("dc_Importe")))
                            .TasaPercepcion = CDec(IIf(IsDBNull(lector("dc_TasaPercepcion")) = True, 0, lector("dc_TasaPercepcion")))
                            .DetalleGlosa = CStr(IIf(IsDBNull(lector("dc_DetalleGlosa")) = True, "", lector("dc_DetalleGlosa")))
                            .IdUsuarioSupervisor = CInt(IIf(IsDBNull(lector("IdUsuarioSupervisor")) = True, 0, lector("IdUsuarioSupervisor")))
                            .IdTipoPV = CInt(IIf(IsDBNull(lector("IdTipoPV")) = True, 0, lector("IdTipoPV")))
                            .NomProducto = CStr(IIf(IsDBNull(lector("prod_Nombre")) = True, "", lector("prod_Nombre")))
                            .CodigoProducto = CStr(IIf(IsDBNull(lector("prod_Codigo")) = True, "", lector("prod_Codigo")))
                            .PorcentDctoMaximo = CDec(IIf(IsDBNull(lector("PorcentDctoMax")) = True, 0, lector("PorcentDctoMax")))
                            .PrecioBaseDcto = CStr(IIf(IsDBNull(lector("PrecioBaseDcto")) = True, "", lector("PrecioBaseDcto")))
                            .VolumenVentaMin = CDec(IIf(IsDBNull(lector("VolumenVentaMin")) = True, 0, lector("VolumenVentaMin")))
                            .pvComercial = CDec(IIf(IsDBNull(lector("PrecioComercial")) = True, 0, lector("PrecioComercial")))
                            .ComponenteKit = CBool(IIf(IsDBNull(lector("add_ComponenteKit")) = True, False, lector("add_ComponenteKit")))
                            .IdKit = CInt(IIf(IsDBNull(lector("IdKit")) = True, 0, lector("IdKit")))
                            .Kit = CBool(IIf(IsDBNull(lector("prod_Kit")) = True, False, lector("prod_Kit")))
                            .IdCampania = objDaoMantenedor.UCInt(lector("IdCampania"))
                            .ExisteCampania_Producto = objDaoMantenedor.UCBool(lector("ExisteCampania_Producto"))
                            .IdProductoAux = objDaoMantenedor.UCInt(lector("IdProductoRef"))
                            .UMProducto = lector.GetString(lector.GetOrdinal("UMProducto"))
                            .UMDescCorta = lector.GetString(lector.GetOrdinal("um_NombreCorto"))
                            .Peso = lector.GetDecimal(lector.GetOrdinal("dc_Peso"))
                            If (.UMProducto <> "") Then
                                Dim data As String() = .UMProducto.Split(";")
                                Dim campos As String()
                                Dim nRegistros As Integer = data.Length
                                Dim obj As Entidades.UnidadMedida
                                .ListaUM_Venta = New List(Of Entidades.UnidadMedida)
                                For i = 0 To nRegistros - 1 Step 1
                                    campos = data(i).Split("|")
                                    obj = New Entidades.UnidadMedida
                                    obj.Id = campos(0)
                                    obj.DescripcionCorto = campos(1)
                                    .ListaUM_Venta.Add(obj)
                                Next
                            End If
                        End With
                        obeCotizacionBuscar.lista_Productos.Add(objDetalleDocumento)
                    End While
                    If (lector.NextResult) Then
                        Dim cboConceptos As String = ""
                        While (lector.Read)
                            Dim objDetalleConcepto As New Entidades.DetalleConcepto
                            With objDetalleConcepto
                                .IdDocumento = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                                .IdDetalleConcepto = CInt(IIf(IsDBNull(lector("IdDetalleConcepto")) = True, 0, lector("IdDetalleConcepto")))
                                .Concepto = CStr(IIf(IsDBNull(lector("dr_Concepto")) = True, "", lector("dr_Concepto")))
                                .IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")) = True, 0, lector("IdMoneda")))
                                .Monto = CDec(IIf(IsDBNull(lector("dr_Monto")) = True, 0, lector("dr_Monto")))
                                .IdConcepto = CInt(IIf(IsDBNull(lector("IdConcepto")) = True, 0, lector("IdConcepto")))
                                .Moneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")) = True, "", lector("mon_Simbolo")))
                                .IdDocumentoRef = CInt(IIf(IsDBNull(lector("IdDocumentoRef")) = True, 0, lector("IdDocumentoRef")))
                                .ConceptoAdelanto = CBool(IIf(IsDBNull(lector("con_ConceptoAdelanto")) = True, 0, lector("con_ConceptoAdelanto")))
                                .PorcentDetraccion = objDaoMantenedor.UCDec(lector("con_PorcentDetraccion"))
                                .MontoMinDetraccion = objDaoMantenedor.UCDec(lector("con_MontoMinDetraccion"))
                                .NoAfectoIGV = objDaoMantenedor.UCBool(lector("con_NoAfectoIGV"))
                                cboConceptos = lector.GetString(lector.GetOrdinal("ListaConcepto"))
                                If (cboConceptos <> "") Then
                                    Dim data As String() = cboConceptos.Split(";")
                                    Dim campos As String()
                                    Dim nRegistros As Integer = data.Length
                                    Dim obj As Entidades.Concepto
                                    .ListaConcepto = New List(Of Entidades.Concepto)
                                    For i = 0 To nRegistros - 1 Step 1
                                        campos = data(i).Split("|")
                                        obj = New Entidades.Concepto
                                        obj.Id = campos(0)
                                        obj.Nombre = campos(1)
                                        .ListaConcepto.Add(obj)
                                    Next
                                End If
                            End With
                            obeCotizacionBuscar.lista_DetalleConcepto.Add(objDetalleConcepto)
                        End While
                        If (lector.NextResult) Then
                            If (lector.HasRows) Then
                                lector.Read()
                                Dim objPuntoPartida As Entidades.PuntoPartida = Nothing
                                objPuntoPartida = New Entidades.PuntoPartida
                                With objPuntoPartida
                                    .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                                    .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                                    .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                                    .Direccion = CStr(IIf(IsDBNull(lector.Item("pp_Direccion")) = True, "", lector.Item("pp_Direccion")))
                                    .Ubigeo = CStr(IIf(IsDBNull(lector.Item("pp_Ubigeo")) = True, "000000", lector.Item("pp_Ubigeo")))
                                    .IdTipoAlmacen = CInt(IIf(IsDBNull(lector.Item("IdTipoAlmacen")) = True, 0, lector.Item("IdTipoAlmacen")))
                                End With
                                obeCotizacionBuscar.PuntoPartida = objPuntoPartida
                            Else
                                obeCotizacionBuscar.PuntoPartida = Nothing
                            End If
                            If (lector.NextResult) Then
                                If (lector.HasRows) Then
                                    lector.Read()
                                    Dim objPuntoLlegada As Entidades.PuntoLlegada = Nothing
                                    objPuntoLlegada = New Entidades.PuntoLlegada
                                    With objPuntoLlegada
                                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                                        .pll_Direccion = CStr(IIf(IsDBNull(lector.Item("pll_Direccion")) = True, "", lector.Item("pll_Direccion")))
                                        .pll_Ubigeo = CStr(IIf(IsDBNull(lector.Item("pll_Ubigeo")) = True, "000000", lector.Item("pll_Ubigeo")))
                                        .IdTipoAlmacen = CInt(IIf(IsDBNull(lector.Item("IdTipoAlmacen")) = True, 0, lector.Item("IdTipoAlmacen")))
                                    End With
                                    obeCotizacionBuscar.PuntoLlegada = objPuntoLlegada
                                Else
                                    obeCotizacionBuscar.PuntoLlegada = Nothing
                                End If
                                If (lector.NextResult) Then
                                    If lector.HasRows Then
                                        lector.Read()
                                        Dim objObservacion As Entidades.Observacion = Nothing
                                        objObservacion = New Entidades.Observacion
                                        With objObservacion
                                            .Id = CInt(lector("IdObservacion"))
                                            .IdDocumento = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                                            .Observacion = CStr(IIf(IsDBNull(lector("ob_Observacion")) = True, "", lector("ob_Observacion")))
                                        End With
                                        obeCotizacionBuscar.Observacion = objObservacion
                                    Else
                                        obeCotizacionBuscar.Observacion = Nothing
                                    End If
                                    If (lector.NextResult) Then
                                        Dim listaRD As New List(Of Entidades.Documento)
                                        While (lector.Read)
                                            Dim Documento As New Entidades.Documento
                                            With Documento
                                                .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumentoRef")) = True, 0, lector.Item("IdDocumentoRef")))
                                                .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                                                .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                                                .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                                                .FechaEntrega = CDate(IIf(IsDBNull(lector.Item("doc_FechaEntrega")) = True, Nothing, lector.Item("doc_FechaEntrega")))
                                                .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                                                .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                                                .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                                                .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                                                .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                                                .IdDestinatario = CInt(IIf(IsDBNull(lector.Item("IdDestinatario")) = True, 0, lector.Item("IdDestinatario")))
                                                .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                                                .IdEstadoEntrega = CInt(IIf(IsDBNull(lector.Item("IdEstadoEnt")) = True, 0, lector.Item("IdEstadoEnt")))
                                                .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                                                .NomAlmacen = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                                                .Tienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))
                                                .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("edoc_Nombre")) = True, "", lector.Item("edoc_Nombre")))
                                                .Empresa = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                                                .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                                                .NroDocumento = .Serie + " - " + .Codigo
                                                .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                                                .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, 0, lector.Item("mon_Simbolo")))
                                                .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, 0, lector.Item("doc_Total")))
                                                .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, 0, lector.Item("doc_TotalAPagar")))
                                            End With
                                            listaRD.Add(Documento)
                                        End While
                                        obeCotizacionBuscar.lista_RelacionDocumento = listaRD
                                        If (lector.NextResult) Then
                                            If (lector.HasRows) Then
                                                lector.Read()
                                                Dim Tasa As Decimal
                                                Tasa = CDec(IIf(IsDBNull(lector.Item("imp_tasa")) = True, 0, lector.Item("imp_tasa")))
                                                Tasa = Decimal.Round(Tasa, 2)
                                                obeCotizacionBuscar.IGV = Tasa
                                            End If
                                            If (lector.NextResult) Then
                                                Dim ListaMT As New List(Of Entidades.beCampoEntero)
                                                Dim MotivoT As Entidades.beCampoEntero
                                                Do While lector.Read
                                                    MotivoT = New Entidades.beCampoEntero
                                                    MotivoT.campo1 = CInt(lector.Item("IdMotivoT"))
                                                    MotivoT.campo2 = CStr(IIf(IsDBNull(lector.Item("mt_Nombre")) = True, "", lector.Item("mt_Nombre")))
                                                    ListaMT.Add(MotivoT)
                                                Loop
                                                obeCotizacionBuscar.lista_MotivoTraslado = ListaMT
                                                If (lector.NextResult) Then
                                                    Dim listaMP As New List(Of Entidades.beCampoEntero)
                                                    While (lector.Read)
                                                        Dim obj As New Entidades.beCampoEntero
                                                        With obj
                                                            .campo1 = objDaoMantenedor.UCInt(lector("IdMedioPago"))
                                                            .campo2 = objDaoMantenedor.UCStr(lector("mp_Nombre"))
                                                        End With
                                                        listaMP.Add(obj)
                                                    End While
                                                    obeCotizacionBuscar.lista_MedioPago = listaMP
                                                    If (lector.NextResult) Then
                                                        Dim ListaCP As New List(Of Entidades.CuentaPersona)
                                                        With lector
                                                            Do While .Read
                                                                Dim obj As New Entidades.CuentaPersona
                                                                obj.IdCuentaPersona = CInt(IIf(IsDBNull(.Item("IdCuentaPersona")) = True, 0, .Item("IdCuentaPersona")))
                                                                obj.IdMoneda = CInt(IIf(IsDBNull(.Item("IdMoneda")) = True, 0, .Item("IdMoneda")))
                                                                obj.MonedaSimbolo = CStr(IIf(IsDBNull(.Item("mon_Simbolo")) = True, "", .Item("mon_Simbolo")))
                                                                obj.CargoMaximo = CDec(IIf(IsDBNull(.Item("cp_CargoMaximo")) = True, 0, .Item("cp_CargoMaximo")))
                                                                obj.Saldo = CDec(IIf(IsDBNull(.Item("Saldo")) = True, 0, .Item("Saldo")))
                                                                obj.CuentaFormal = CBool(IIf(IsDBNull(.Item("cp_CuentaFormal")) = True, 0, .Item("cp_CuentaFormal")))
                                                                obj.IdTienda = CInt(IIf(IsDBNull(.Item("IdTienda")) = True, 0, .Item("IdTienda")))
                                                                obj.IdEmpresa = CInt(IIf(IsDBNull(.Item("IdEmpresa")) = True, 0, .Item("IdEmpresa")))
                                                                obj.IdSupervisor = CInt(IIf(IsDBNull(.Item("IdSupervisor")) = True, 0, .Item("IdSupervisor")))
                                                                obj.FechaAlta = CStr(IIf(IsDBNull(.Item("cp_FechaAlta")) = True, Nothing, .Item("cp_FechaAlta")))
                                                                ListaCP.Add(obj)
                                                            Loop
                                                            obeCotizacionBuscar.lista_CuentaPersona = ListaCP
                                                        End With
                                                        If (lector.NextResult) Then
                                                            Dim listaProvinciaPP As New List(Of Entidades.beCboProvincia)
                                                            While (lector.Read)
                                                                Dim obj As New Entidades.beCboProvincia
                                                                With obj
                                                                    .CodProv = lector.GetString(lector.GetOrdinal("ub_CodProv"))
                                                                    .Nombre = lector.GetString(lector.GetOrdinal("ub_Nombre"))
                                                                End With
                                                                listaProvinciaPP.Add(obj)
                                                            End While
                                                            obeCotizacionBuscar.lista_ProvinciaPP = listaProvinciaPP
                                                            If (lector.NextResult) Then
                                                                Dim listaDistritoPP As New List(Of Entidades.beCboDistrito)
                                                                While (lector.Read)
                                                                    Dim obj As New Entidades.beCboDistrito
                                                                    With obj
                                                                        .CodDist = lector.GetString(lector.GetOrdinal("ub_CodDist"))
                                                                        .Nombre = lector.GetString(lector.GetOrdinal("ub_Nombre"))
                                                                    End With
                                                                    listaDistritoPP.Add(obj)
                                                                End While
                                                                obeCotizacionBuscar.lista_DistritoPP = listaDistritoPP
                                                                If (lector.NextResult) Then
                                                                    Dim listaProvinciaPL As New List(Of Entidades.beCboProvincia)
                                                                    While (lector.Read)
                                                                        Dim obj As New Entidades.beCboProvincia
                                                                        With obj
                                                                            .CodProv = lector.GetString(lector.GetOrdinal("ub_CodProv"))
                                                                            .Nombre = lector.GetString(lector.GetOrdinal("ub_Nombre"))
                                                                        End With
                                                                        listaProvinciaPL.Add(obj)
                                                                    End While
                                                                    obeCotizacionBuscar.lista_ProvinciaPL = listaProvinciaPL
                                                                    If (lector.NextResult) Then
                                                                        Dim listaDistritoPL As New List(Of Entidades.beCboDistrito)
                                                                        While (lector.Read)
                                                                            Dim obj As New Entidades.beCboDistrito
                                                                            With obj
                                                                                .CodDist = lector.GetString(lector.GetOrdinal("ub_CodDist"))
                                                                                .Nombre = lector.GetString(lector.GetOrdinal("ub_Nombre"))
                                                                            End With
                                                                            listaDistritoPL.Add(obj)
                                                                        End While
                                                                        obeCotizacionBuscar.lista_DistritoPL = listaDistritoPL
                                                                    End If
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Else

            If (Not (IdSerie = Nothing And nroDocumento = Nothing And IdDocumento = Nothing)) Then
                Throw New Exception("No se hallaron registros.")
            End If

        End If

        Return obeCotizacionBuscar
    End Function

    Public Function nuevaCotizacion(ByVal IdTienda As Integer, ByVal IdSerie As Integer, ByVal con As SqlConnection) As beCotizacionNuevo
        Dim obeCotizacionNuevo As New beCotizacionNuevo

        Dim cmd As New SqlCommand("uspHte_NuevaCotizacion", con)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdSerie", IdSerie)

        Dim drd As SqlDataReader = cmd.ExecuteReader()

        If (drd IsNot Nothing) Then
            If (drd.HasRows) Then
                drd.Read()
                obeCotizacionNuevo.FechaActual = drd.GetDateTime(0)
            End If
            If (drd.NextResult) Then
                If (drd.HasRows) Then
                    Dim lParametros As New List(Of Decimal)
                    While drd.Read()
                        lParametros.Add(drd.GetDecimal(0))
                    End While
                    obeCotizacionNuevo.listaParametros = lParametros
                End If
                If (drd.NextResult) Then
                    If (drd.HasRows) Then
                        Dim oTiendaUbigeo As New beCampoCadena
                        drd.Read()
                        'oTiendaUbigeo.campo1 = CInt(drd.GetString(0))
                        oTiendaUbigeo.campo1 = drd.GetString(0)
                        oTiendaUbigeo.campo2 = drd.GetString(1)
                        obeCotizacionNuevo.TiendaUbigeo = oTiendaUbigeo
                    End If
                    If (drd.NextResult) Then
                        If (drd.HasRows) Then
                            drd.Read()
                            obeCotizacionNuevo.NroDocumento = drd.GetString(0)
                        End If
                        If (drd.NextResult) Then
                            If (drd.HasRows) Then
                                Dim lDepartamentoPartida As New List(Of beCboDepartamento)
                                Dim obj As beCboDepartamento
                                While drd.Read
                                    obj = New beCboDepartamento
                                    obj.CodDpto = drd.GetString(0)
                                    obj.Nombre = drd.GetString(1)
                                    lDepartamentoPartida.Add(obj)
                                End While
                                obeCotizacionNuevo.listaDepartamentoPartida = lDepartamentoPartida
                            End If
                            If (drd.NextResult) Then
                                If (drd.HasRows) Then
                                    Dim lProvinciaPartida As New List(Of beCboProvincia)
                                    Dim obj As beCboProvincia
                                    While drd.Read
                                        obj = New beCboProvincia
                                        obj.CodProv = drd.GetString(0)
                                        obj.Nombre = drd.GetString(1)
                                        lProvinciaPartida.Add(obj)
                                    End While
                                    obeCotizacionNuevo.listaProvinciaPartida = lProvinciaPartida
                                End If
                                If (drd.NextResult) Then
                                    If (drd.HasRows) Then
                                        Dim lDistritoPartida As New List(Of beCboDistrito)
                                        Dim obj As beCboDistrito
                                        While drd.Read
                                            obj = New beCboDistrito
                                            obj.CodDist = drd.GetString(0)
                                            obj.Nombre = drd.GetString(1)
                                            lDistritoPartida.Add(obj)
                                        End While
                                        obeCotizacionNuevo.listaDistritoPartida = lDistritoPartida
                                    End If
                                    If (drd.NextResult) Then
                                        If (drd.HasRows) Then
                                            drd.Read()
                                            obeCotizacionNuevo.IGV = drd.GetDecimal(0)
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
            drd.Close()
        End If
        Return obeCotizacionNuevo
    End Function


    Public Function cargarDocumentoReferencia(ByVal IdDocumentoReferencia As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Fecha As Date, ByVal IdTipoPV As Integer, ByVal IdMoneda As Integer, ByVal IdVendedor As Integer, ByVal IdTipoDocumento As Integer, ByVal CondicionPago As Integer, ByVal con As SqlConnection) As beCotizacionReferencia
        Dim obeCotizacionReferencia As beCotizacionReferencia = Nothing
        Dim cmd As New SqlCommand("uspHte_BuscarCotizacionReferencia", con)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumentoReferencia)
        'cmd.Parameters.AddWithValue("@IdHdfPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        Dim p() As SqlParameter = New SqlParameter(5) {}
        p(0) = objDaoMantenedor.getParam(Fecha, "@Fecha", SqlDbType.Date)
        p(1) = objDaoMantenedor.getParam(IdTipoPV, "@IdTipoPVDD", SqlDbType.Int)
        p(2) = objDaoMantenedor.getParam(IdMoneda, "@IdMoneda", SqlDbType.Int)
        p(3) = objDaoMantenedor.getParam(IdVendedor, "@IdVendedor", SqlDbType.Int)
        p(4) = objDaoMantenedor.getParam(IdTipoDocumento, "@IdTipoDocumentoMP", SqlDbType.Int)
        p(5) = objDaoMantenedor.getParam(CondicionPago, "@IdCondicionPagoMP", SqlDbType.Int)
        cmd.Parameters.AddRange(p)

        cmd.CommandTimeout = 0

        Dim lector As SqlDataReader = cmd.ExecuteReader()

        If (lector IsNot Nothing) Then
            obeCotizacionReferencia = New beCotizacionReferencia
            If (lector.HasRows) Then
                lector.Read()
                Dim Documento As New Entidades.Documento
                With Documento
                    .Id = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                    .FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                    .FechaIniTraslado = CDate(IIf(IsDBNull(lector.Item("doc_FechaIniTraslado")) = True, Nothing, lector.Item("doc_FechaIniTraslado")))
                    .FechaRegistro = CDate(IIf(IsDBNull(lector.Item("doc_FechaRegistro")) = True, Nothing, lector.Item("doc_FechaRegistro")))
                    .FechaAEntregar = CDate(IIf(IsDBNull(lector.Item("doc_FechaAentregar")) = True, Nothing, lector.Item("doc_FechaAentregar")))
                    .FechaEntrega = CDate(IIf(IsDBNull(lector.Item("doc_FechaEntrega")) = True, Nothing, lector.Item("doc_FechaEntrega")))
                    .FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                    .Descuento = CDec(IIf(IsDBNull(lector.Item("doc_Descuento")) = True, Nothing, lector.Item("doc_Descuento")))
                    .SubTotal = CDec(IIf(IsDBNull(lector.Item("doc_SubTotal")) = True, Nothing, lector.Item("doc_SubTotal")))
                    .IGV = CDec(IIf(IsDBNull(lector.Item("doc_IGV")) = True, Nothing, lector.Item("doc_IGV")))
                    .Total = CDec(IIf(IsDBNull(lector.Item("doc_Total")) = True, Nothing, lector.Item("doc_Total")))
                    '.TotalLetras = CStr(IIf(IsDBNull(lector.Item("doc_TotalLetras")) = True, Nothing, lector.Item("doc_TotalLetras")))
                    .TotalAPagar = CDec(IIf(IsDBNull(lector.Item("doc_TotalAPagar")) = True, Nothing, lector.Item("doc_TotalAPagar")))
                    .ValorReferencial = CDec(IIf(IsDBNull(lector.Item("doc_ValorReferencial")) = True, Nothing, lector.Item("doc_ValorReferencial")))
                    .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    .IdUsuario = CInt(IIf(IsDBNull(lector.Item("IdUsuario")) = True, 0, lector.Item("IdUsuario")))
                    .IdTransportista = CInt(IIf(IsDBNull(lector.Item("IdTransportista")) = True, 0, lector.Item("IdTransportista")))
                    .IdRemitente = CInt(IIf(IsDBNull(lector.Item("IdRemitente")) = True, 0, lector.Item("IdRemitente")))
                    .IdDestinatario = CInt(IIf(IsDBNull(lector.Item("IdDestinatario")) = True, 0, lector.Item("IdDestinatario")))
                    .IdEstadoDoc = CInt(IIf(IsDBNull(lector.Item("IdEstadoDoc")) = True, 0, lector.Item("IdEstadoDoc")))
                    .IdCondicionPago = CInt(IIf(IsDBNull(lector.Item("IdCondicionPago")) = True, 0, lector.Item("IdCondicionPago")))
                    .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                    .LugarEntrega = CInt(IIf(IsDBNull(lector.Item("LugarEntrega")) = True, 0, lector.Item("LugarEntrega")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                    .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                    .IdChofer = CInt(IIf(IsDBNull(lector.Item("IdChofer")) = True, 0, lector.Item("IdChofer")))
                    .IdMotivoT = CInt(IIf(IsDBNull(lector.Item("IdMotivoT")) = True, 0, lector.Item("IdMotivoT")))
                    .IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                    .IdVehiculo = CInt(IIf(IsDBNull(lector.Item("IdVehiculo")) = True, 0, lector.Item("IdVehiculo")))
                    .IdEstadoCancelacion = CInt(IIf(IsDBNull(lector.Item("IdEstadoCan")) = True, 0, lector.Item("IdEstadoCan")))
                    .IdEstadoEntrega = CInt(IIf(IsDBNull(lector.Item("IdEstadoEnt")) = True, 0, lector.Item("IdEstadoEnt")))
                    .FechaCancelacion = CDate(IIf(IsDBNull(lector.Item("doc_FechaCancelacion")) = True, Nothing, lector.Item("doc_FechaCancelacion")))
                    .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    .NomAlmacen = CStr(IIf(IsDBNull(lector.Item("NomAlmacen")) = True, "", lector.Item("NomAlmacen")))
                    .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                    .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                    .Percepcion = CDec(IIf(IsDBNull(lector.Item("Percepcion")) = True, 0, lector.Item("Percepcion")))
                    .Detraccion = CDec(IIf(IsDBNull(lector.Item("Detraccion")) = True, 0, lector.Item("Detraccion")))
                    .Retencion = CDec(IIf(IsDBNull(lector.Item("Retencion")) = True, 0, lector.Item("Retencion")))
                    .PoseeOrdenDespacho = CBool(IIf(IsDBNull(lector.Item("PoseeOrdenDespacho")) = True, 0, lector.Item("PoseeOrdenDespacho")))
                    .PoseeAmortizaciones = CBool(IIf(IsDBNull(lector.Item("PoseeAmortizaciones")) = True, 0, lector.Item("PoseeAmortizaciones")))
                    .PoseeGRecepcion = CBool(IIf(IsDBNull(lector.Item("PoseeGRecepcion")) = True, 0, lector.Item("PoseeGRecepcion")))
                    .NomMoneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                    .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                    .Empresa = CStr(IIf(IsDBNull(lector.Item("Empresa")) = True, "", lector.Item("Empresa")))
                    .Tienda = CStr(IIf(IsDBNull(lector.Item("Tienda")) = True, "", lector.Item("Tienda")))
                    .NomEstadoDocumento = CStr(IIf(IsDBNull(lector.Item("EstadoDocumento")) = True, "", lector.Item("EstadoDocumento")))
                    .NroDocumento = .Serie + " - " + .Codigo
                    .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("DescripcionPersona")) = True, "", lector.Item("DescripcionPersona")))
                    .IdUsuarioComision = CInt(IIf(IsDBNull(lector.Item("IdUsuarioComision")) = True, 0, lector.Item("IdUsuarioComision")))
                End With
                obeCotizacionReferencia.Documento = Documento
                If (lector.NextResult) Then
                    Dim objObservacion As Entidades.Observacion = Nothing
                    If lector.HasRows Then
                        lector.Read()
                        objObservacion = New Entidades.Observacion
                        With objObservacion
                            .Id = CInt(lector("IdObservacion"))
                            .IdDocumento = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                            .Observacion = CStr(IIf(IsDBNull(lector("ob_Observacion")) = True, "", lector("ob_Observacion")))
                        End With
                        obeCotizacionReferencia.Observacion = objObservacion
                    End If
                    If (lector.NextResult) Then
                        Dim objPuntoPartida As Entidades.PuntoPartida = Nothing
                        If lector.HasRows Then
                            lector.Read()
                            objPuntoPartida = New Entidades.PuntoPartida
                            With objPuntoPartida
                                .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                                .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                                .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                                .Direccion = CStr(IIf(IsDBNull(lector.Item("pp_Direccion")) = True, "", lector.Item("pp_Direccion")))
                                .Ubigeo = CStr(IIf(IsDBNull(lector.Item("pp_Ubigeo")) = True, "000000", lector.Item("pp_Ubigeo")))
                                .IdTipoAlmacen = CInt(IIf(IsDBNull(lector.Item("IdTipoAlmacen")) = True, 0, lector.Item("IdTipoAlmacen")))
                            End With
                            obeCotizacionReferencia.PuntoPartida = objPuntoPartida
                        End If
                        If (lector.NextResult) Then
                            Dim objPuntoLlegada As Entidades.PuntoLlegada = Nothing
                            If lector.HasRows Then
                                lector.Read()
                                objPuntoLlegada = New Entidades.PuntoLlegada
                                With objPuntoLlegada
                                    .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                                    .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                                    .pll_Direccion = CStr(IIf(IsDBNull(lector.Item("pll_Direccion")) = True, "", lector.Item("pll_Direccion")))
                                    .pll_Ubigeo = CStr(IIf(IsDBNull(lector.Item("pll_Ubigeo")) = True, "000000", lector.Item("pll_Ubigeo")))
                                    .IdTipoAlmacen = CInt(IIf(IsDBNull(lector.Item("IdTipoAlmacen")) = True, 0, lector.Item("IdTipoAlmacen")))
                                End With
                                obeCotizacionReferencia.PuntoLlegada = objPuntoLlegada
                            End If
                            If (lector.NextResult) Then
                                If lector.HasRows Then
                                    lector.Read()
                                    Dim objCliente = New Entidades.PersonaView
                                    With objCliente
                                        .IdPersona = CInt(lector.Item("IdPersona"))
                                        .NombreComercial = CStr(IIf(IsDBNull(lector.Item("Per_NComercial")) = True, "", lector.Item("Per_NComercial")))
                                        .RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                                        .Nombre = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                                        .Dni = CStr(IIf(IsDBNull(lector.Item("Dni")) = True, "", lector.Item("Dni")))
                                        .Ruc = CStr(IIf(IsDBNull(lector.Item("Ruc")) = True, "", lector.Item("Ruc")))
                                        .Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, "", lector.Item("dir_Direccion")))
                                        .Ubigeo = CStr(IIf(IsDBNull(lector.Item("dir_Ubigeo")) = True, "", lector.Item("dir_Ubigeo")))
                                        .Referencia = CStr(IIf(IsDBNull(lector.Item("dir_Ubigeo")) = True, "", lector.Item("dir_Ubigeo")))
                                        .Telefeono = CStr(IIf(IsDBNull(lector.Item("tel_Numero")) = True, "", lector.Item("tel_Numero")))
                                        .Estado = CStr(lector.Item("per_estado"))
                                        .IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                                        .ApPaterno = CStr(IIf(IsDBNull(lector.Item("nat_Apepat")) = True, "", lector.Item("nat_Apepat")))
                                        .ApMaterno = CStr(IIf(IsDBNull(lector.Item("nat_Apemat")) = True, "", lector.Item("nat_Apemat")))
                                        .Nombres = CStr(IIf(IsDBNull(lector.Item("nat_Nombres")) = True, "", lector.Item("nat_Nombres")))
                                        .IdTipoAgente = CInt(IIf(IsDBNull(lector.Item("IdAgente")) = True, 0, lector.Item("IdAgente")))
                                        .PorcentTipoAgente = CDec(IIf(IsDBNull(lector.Item("PorcentTipoAgente")) = True, 0, lector.Item("PorcentTipoAgente")))
                                        .TipoPersona = CInt(IIf(IsDBNull(lector.Item("TipoPersona")) = True, 0, lector.Item("TipoPersona")))
                                        .SujetoARetencion = CBool(IIf(IsDBNull(lector.Item("ag_SujetoRetencion")) = True, 0, lector.Item("ag_SujetoRetencion")))
                                        .SujetoAPercepcion = CBool(IIf(IsDBNull(lector.Item("ag_SujetoAPercepcion")) = True, 0, lector.Item("ag_SujetoAPercepcion")))
                                        .IdRol = CInt(IIf(IsDBNull(lector.Item("RolCliente")) = True, 0, lector.Item("RolCliente")))
                                    End With
                                    obeCotizacionReferencia.Persona = objCliente
                                End If
                                If (lector.NextResult) Then
                                    If (lector.HasRows) Then
                                        Dim lCuentaPersona As New List(Of Entidades.CuentaPersona)
                                        With lector
                                            Do While .Read
                                                Dim obj As New Entidades.CuentaPersona
                                                obj.IdCuentaPersona = CInt(IIf(IsDBNull(.Item("IdCuentaPersona")) = True, 0, .Item("IdCuentaPersona")))
                                                obj.IdMoneda = CInt(IIf(IsDBNull(.Item("IdMoneda")) = True, 0, .Item("IdMoneda")))
                                                obj.MonedaSimbolo = CStr(IIf(IsDBNull(.Item("mon_Simbolo")) = True, "", .Item("mon_Simbolo")))
                                                obj.CargoMaximo = CDec(IIf(IsDBNull(.Item("cp_CargoMaximo")) = True, 0, .Item("cp_CargoMaximo")))
                                                obj.Saldo = CDec(IIf(IsDBNull(.Item("Saldo")) = True, 0, .Item("Saldo")))
                                                obj.CuentaFormal = CBool(IIf(IsDBNull(.Item("cp_CuentaFormal")) = True, 0, .Item("cp_CuentaFormal")))
                                                obj.IdTienda = CInt(IIf(IsDBNull(.Item("IdTienda")) = True, 0, .Item("IdTienda")))
                                                obj.IdEmpresa = CInt(IIf(IsDBNull(.Item("IdEmpresa")) = True, 0, .Item("IdEmpresa")))
                                                obj.IdSupervisor = CInt(IIf(IsDBNull(.Item("IdSupervisor")) = True, 0, .Item("IdSupervisor")))
                                                obj.FechaAlta = CStr(IIf(IsDBNull(.Item("cp_FechaAlta")) = True, Nothing, .Item("cp_FechaAlta")))
                                                lCuentaPersona.Add(obj)
                                            Loop
                                        End With
                                        obeCotizacionReferencia.Lista_CuentaPersona = lCuentaPersona
                                    End If
                                    If (lector.NextResult) Then
                                        If (lector.HasRows) Then
                                            Dim listaProvinciaPP As New List(Of Entidades.beCboProvincia)
                                            While (lector.Read)
                                                Dim obj As New Entidades.beCboProvincia
                                                With obj
                                                    .CodProv = lector.GetString(lector.GetOrdinal("ub_CodProv"))
                                                    .Nombre = lector.GetString(lector.GetOrdinal("ub_Nombre"))
                                                End With
                                                listaProvinciaPP.Add(obj)
                                            End While
                                            obeCotizacionReferencia.ListaProvinciaPP = listaProvinciaPP
                                        End If
                                        If (lector.NextResult) Then
                                            If (lector.HasRows) Then
                                                Dim listaDistritoPP As New List(Of Entidades.beCboDistrito)
                                                While (lector.Read)
                                                    Dim obj As New Entidades.beCboDistrito
                                                    With obj
                                                        .CodDist = lector.GetString(lector.GetOrdinal("ub_CodDist"))
                                                        .Nombre = lector.GetString(lector.GetOrdinal("ub_Nombre"))
                                                    End With
                                                    listaDistritoPP.Add(obj)
                                                End While
                                                obeCotizacionReferencia.ListaDistritoPP = listaDistritoPP
                                            End If
                                            If (lector.NextResult) Then
                                                If (lector.HasRows) Then
                                                    Dim listaProvinciaPL As New List(Of Entidades.beCboProvincia)
                                                    While (lector.Read)
                                                        Dim obj As New Entidades.beCboProvincia
                                                        With obj
                                                            .CodProv = lector.GetString(lector.GetOrdinal("ub_CodProv"))
                                                            .Nombre = lector.GetString(lector.GetOrdinal("ub_Nombre"))
                                                        End With
                                                        listaProvinciaPL.Add(obj)
                                                    End While
                                                    obeCotizacionReferencia.ListaProvinciaPL = listaProvinciaPL
                                                End If
                                                If (lector.NextResult) Then
                                                    If (lector.HasRows) Then
                                                        Dim listaDistritoPL As New List(Of Entidades.beCboDistrito)
                                                        While (lector.Read)
                                                            Dim obj As New Entidades.beCboDistrito
                                                            With obj
                                                                .CodDist = lector.GetString(lector.GetOrdinal("ub_CodDist"))
                                                                .Nombre = lector.GetString(lector.GetOrdinal("ub_Nombre"))
                                                            End With
                                                            listaDistritoPL.Add(obj)
                                                        End While
                                                        obeCotizacionReferencia.ListaDistritoPL = listaDistritoPL
                                                    End If
                                                    If (lector.NextResult) Then ''''''''''''''''''''''''Otro Ubigeo
                                                        If (lector.HasRows) Then
                                                            Dim listaProvinciaPPD As New List(Of Entidades.beCboProvincia)
                                                            While (lector.Read)
                                                                Dim obj As New Entidades.beCboProvincia
                                                                With obj
                                                                    .CodProv = lector.GetString(lector.GetOrdinal("ub_CodProv"))
                                                                    .Nombre = lector.GetString(lector.GetOrdinal("ub_Nombre"))
                                                                End With
                                                                listaProvinciaPPD.Add(obj)
                                                            End While
                                                            obeCotizacionReferencia.ListaProvinciaPPD = listaProvinciaPPD
                                                        End If
                                                        If (lector.NextResult) Then
                                                            If (lector.HasRows) Then
                                                                Dim listaDistritoPPD As New List(Of Entidades.beCboDistrito)
                                                                While (lector.Read)
                                                                    Dim obj As New Entidades.beCboDistrito
                                                                    With obj
                                                                        .CodDist = lector.GetString(lector.GetOrdinal("ub_CodDist"))
                                                                        .Nombre = lector.GetString(lector.GetOrdinal("ub_Nombre"))
                                                                    End With
                                                                    listaDistritoPPD.Add(obj)
                                                                End While
                                                                obeCotizacionReferencia.ListaDistritoPPD = listaDistritoPPD
                                                            End If
                                                            If (lector.NextResult) Then
                                                                If (lector.HasRows) Then
                                                                    Dim listaProvinciaPLD As New List(Of Entidades.beCboProvincia)
                                                                    While (lector.Read)
                                                                        Dim obj As New Entidades.beCboProvincia
                                                                        With obj
                                                                            .CodProv = lector.GetString(lector.GetOrdinal("ub_CodProv"))
                                                                            .Nombre = lector.GetString(lector.GetOrdinal("ub_Nombre"))
                                                                        End With
                                                                        listaProvinciaPLD.Add(obj)
                                                                    End While
                                                                    obeCotizacionReferencia.ListaProvinciaPLD = listaProvinciaPLD
                                                                End If
                                                                If (lector.NextResult) Then
                                                                    If (lector.HasRows) Then
                                                                        Dim listaDistritoPLD As New List(Of Entidades.beCboDistrito)
                                                                        While (lector.Read)
                                                                            Dim obj As New Entidades.beCboDistrito
                                                                            With obj
                                                                                .CodDist = lector.GetString(lector.GetOrdinal("ub_CodDist"))
                                                                                .Nombre = lector.GetString(lector.GetOrdinal("ub_Nombre"))
                                                                            End With
                                                                            listaDistritoPLD.Add(obj)
                                                                        End While
                                                                        obeCotizacionReferencia.ListaDistritoPLD = listaDistritoPLD
                                                                    End If
                                                                    If (lector.NextResult) Then
                                                                        If (lector.HasRows) Then
                                                                            Dim lDetalleDocumento As New List(Of Entidades.DetalleDocumento)
                                                                            While (lector.Read)
                                                                                Dim objDetalleDocumento As New Entidades.DetalleDocumento
                                                                                With objDetalleDocumento
                                                                                    .IdProducto = objDaoMantenedor.UCInt(lector("IdProducto"))
                                                                                    .IdUnidadMedida = objDaoMantenedor.UCInt(lector("IdUnidadMedida"))
                                                                                    .Cantidad = objDaoMantenedor.UCDec(lector("Cantidad"))
                                                                                    .PrecioLista = objDaoMantenedor.UCDec(lector("PrecioLista"))
                                                                                    .PrecioSD = objDaoMantenedor.UCDec(lector("PrecioLista"))
                                                                                    .Descuento = objDaoMantenedor.UCDec(0)
                                                                                    .PorcentDcto = objDaoMantenedor.UCDec(0)
                                                                                    .PrecioCD = objDaoMantenedor.UCDec(lector("PrecioLista"))
                                                                                    .Importe = .PrecioCD * .Cantidad
                                                                                    .TasaPercepcion = objDaoMantenedor.UCDec(lector("PorcentPercepcion"))
                                                                                    .DetalleGlosa = ""
                                                                                    .IdUsuarioSupervisor = Nothing
                                                                                    .IdTipoPV = objDaoMantenedor.UCInt(lector("IdTipoPv"))
                                                                                    .NomProducto = objDaoMantenedor.UCStr(lector("Producto"))
                                                                                    .CodigoProducto = objDaoMantenedor.UCStr(lector("CodigoProducto"))
                                                                                    .PorcentDctoMaximo = objDaoMantenedor.UCDec(lector("PorcentDctoMax"))
                                                                                    .PrecioBaseDcto = objDaoMantenedor.UCStr(lector("PrecioBaseDcto"))
                                                                                    .VolumenVentaMin = objDaoMantenedor.UCDec(lector("VolumenMinVenta"))
                                                                                    .pvComercial = objDaoMantenedor.UCDec(lector("PrecioComercial"))
                                                                                    .ExisteCampania_Producto = objDaoMantenedor.UCBool(lector("ExisteCampaniaProducto"))
                                                                                    .IdCampania = objDaoMantenedor.UCInt(lector("IdCampania"))
                                                                                    .IdProductoAux = objDaoMantenedor.UCInt(lector("IdProductoRef"))
                                                                                    .UMProducto = lector.GetString(lector.GetOrdinal("UMProducto"))
                                                                                    If (.UMProducto <> "") Then
                                                                                        Dim data As String() = .UMProducto.Split(";")
                                                                                        Dim campos As String()
                                                                                        Dim nRegistros As Integer = data.Length
                                                                                        Dim obj As Entidades.UnidadMedida
                                                                                        .ListaUM_Venta = New List(Of Entidades.UnidadMedida)
                                                                                        For i = 0 To nRegistros - 1 Step 1
                                                                                            campos = data(i).Split("|")
                                                                                            obj = New Entidades.UnidadMedida
                                                                                            obj.Id = campos(0)
                                                                                            obj.DescripcionCorto = campos(1)
                                                                                            .ListaUM_Venta.Add(obj)
                                                                                        Next
                                                                                    End If
                                                                                End With
                                                                                lDetalleDocumento.Add(objDetalleDocumento)
                                                                            End While
                                                                            obeCotizacionReferencia.ListaDetalleDocumento = lDetalleDocumento
                                                                        End If
                                                                        If (lector.NextResult) Then
                                                                            If (lector.HasRows) Then
                                                                                Dim lMedioPago As New List(Of Entidades.beCampoEntero)
                                                                                While (lector.Read)
                                                                                    Dim obeCampoEntero As New Entidades.beCampoEntero
                                                                                    With obeCampoEntero
                                                                                        .campo1 = objDaoMantenedor.UCInt(lector("IdMedioPago"))
                                                                                        .campo2 = objDaoMantenedor.UCStr(lector("mp_Nombre"))
                                                                                    End With
                                                                                    lMedioPago.Add(obeCampoEntero)
                                                                                End While
                                                                                obeCotizacionReferencia.ListaMedioPago = lMedioPago
                                                                            End If
                                                                        End If
                                                                    End If
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
            lector.Close()
        End If
        Return obeCotizacionReferencia
    End Function

    Public Function GetVectorParametros(ByVal x As Entidades.Anexo_Documento) As SqlParameter()

        Dim Prm() As SqlParameter
        Prm = New SqlParameter(26) {}
        'Prm(0) = getParam(x.IdDocumento, "@IdDocumento", SqlDbType.Int)
        Prm(0) = New SqlParameter("@doc_Importacion", SqlDbType.Bit)
        Prm(0).Value = IIf(x.doc_Importacion = Nothing, DBNull.Value, x.doc_Importacion)
        'Prm(1) = getParam(x.doc_Importacion, "@doc_Importacion", SqlDbType.Bit)
        Prm(1) = getParam(x.IdMedioPago, "@IdMedioPago", SqlDbType.Int)
        Prm(2) = getParam(x.IdUsuarioSupervisor, "@IdUsuarioSupervisor", SqlDbType.Int)
        Prm(3) = getParam(x.PrecImportacion, "@anex_PrecImportacion", SqlDbType.Char)
        Prm(4) = getParam(x.NroContenedores, "@anex_NroContenedores", SqlDbType.VarChar)
        Prm(5) = getParam(x.NroDiasVigencia, "@anex_NroDiasVigencia", SqlDbType.Decimal)
        Prm(6) = getParam(x.TotalConcepto, "@anex_TotalConcepto", SqlDbType.Decimal)
        Prm(7) = getParam(x.IdArea, "@IdArea", SqlDbType.Int)
        Prm(8) = getParam(x.IdAlias, "@IdAlias", SqlDbType.Int)
        Prm(9) = getParam(x.IdMaestroObra, "@IdMaestroObra", SqlDbType.Int)
        Prm(10) = getParam(x.IdTPImp, "@IdTPImp", SqlDbType.Int)
        Prm(11) = getParam(x.IdSerieCheque, "@IdSerieCheque", SqlDbType.Int)
        Prm(12) = getParam(x.Aprobar, "@Aprobar", SqlDbType.Bit)
        Prm(13) = getParam(x.IdYear, "@IdYear", SqlDbType.Int)
        Prm(14) = getParam(x.IdSemana, "@IdSemana", SqlDbType.Int)
        Prm(15) = getParam(x.NroOperacion, "@anex_NroOperacion", SqlDbType.VarChar)
        Prm(16) = getParam(x.FechaAprobacion, "@anex_FechaAprobacion", SqlDbType.DateTime)

        Prm(17) = getParam(x.MontoAfectoIgv, "@anex_MontoAfectoIgv", SqlDbType.Decimal)
        Prm(18) = getParam(x.MontoNoAfectoIgv, "@anex_MontoNoAfectoIgv", SqlDbType.Decimal)

        Prm(19) = getParam(x.Liquidado, "@anex_Liquidado", SqlDbType.Bit)
        Prm(20) = getParam(x.MontoxSustentar, "@anex_MontoxSustentar", SqlDbType.Decimal)
        Prm(21) = getParam(x.Codigo, "@anex_Codigo", SqlDbType.VarChar)
        Prm(22) = getParam(x.Serie, "@anex_Serie", SqlDbType.VarChar)
        Prm(23) = getParam(x.MontoISC, "@montoISC", SqlDbType.Decimal)
        Prm(24) = getParam(x.MontoOtroTributo, "@montoOtroTributo", SqlDbType.Decimal)
        Prm(25) = getParam(x.anex_DescuentoGlobal, "@anex_DescuentoGlobal", SqlDbType.Decimal)
        Prm(26) = getParam(x.EstadoRequerimiento, "@est_reque", SqlDbType.Int)


        Return Prm
    End Function

    Public Function getParam(ByVal a As Object, ByVal parameterName As String, ByVal BDTipo As SqlDbType) As SqlParameter
        Dim prmtro As New SqlParameter

        prmtro.SqlDbType = BDTipo
        prmtro.ParameterName = parameterName

        'Se ha utilizado las conversiones del sistema, 
        'por cuestion de rendimiento, se supone que 'a' 
        'es un valor ya validado, o sea es string ó int ó etc.
        Select Case BDTipo
            Case SqlDbType.VarChar
                prmtro.Value = getParamValueStr(CStr(a))
            Case SqlDbType.Bit
                prmtro.Value = CBool(a)
            Case SqlDbType.Int
                prmtro.Value = getParamValueInt(CInt(a))
            Case SqlDbType.Decimal
                prmtro.Value = getParamValueDec(CDec(a))
            Case SqlDbType.Date
                prmtro.Value = getParamValueDate(CDate(a))
            Case SqlDbType.DateTime
                prmtro.Value = getParamValueDate(CDate(a))
            Case SqlDbType.Char
                prmtro.Value = getParamValueChar(CChar(a))
        End Select

        Return prmtro
    End Function

    Public Const DEFAULT_Integer As Integer = 0
    Public Const DEFAULT_Double As Double = 0
    Public Const DEFAULT_Decimal As Decimal = 0D
    Public Const DEFAULT_Boolean As Boolean = False
    Public Const DEFAULT_String As String = ""
    Public Const DEFAULT_Date As Date = Nothing


    'UCInt User CInt
    Public Function UCInt(ByVal obj As Object) As Integer
        Return CInt(IIf(IsDBNull(obj), DEFAULT_Integer, obj))
    End Function
    Public Function UCDbl(ByVal obj As Object) As Double
        Return CDbl(IIf(IsDBNull(obj), DEFAULT_Double, obj))
    End Function
    Public Function UCDec(ByVal obj As Object) As Decimal
        Return CDec(IIf(IsDBNull(obj), DEFAULT_Decimal, obj))
    End Function

    Public Function UCBool(ByVal obj As Object) As Boolean
        Return CBool(IIf(IsDBNull(obj), DEFAULT_Boolean, obj))
    End Function

    Public Function UCStr(ByVal obj As Object) As String
        Return CStr(IIf(IsDBNull(obj), DEFAULT_String, obj))
    End Function

    Public Function UCDate(ByVal obj As Object) As Date
        Return CDate(IIf(IsDBNull(obj), DEFAULT_Date, obj))
    End Function

    Public Function getParamValueInt(ByVal a As Integer) As Object
        'Return IIf(IsNothing(a) Or a = 0, DBNull.Value, a)
        Return IIf(a = DEFAULT_Integer Or a = Nothing, DBNull.Value, a)
    End Function

    Public Function getParamValueDec(ByVal a As Decimal) As Object
        Return IIf(a = DEFAULT_Decimal Or a = Nothing, DBNull.Value, a)
    End Function

    Public Function getParamValueStr(ByVal a As String) As Object
        Return IIf(String.IsNullOrEmpty(a), DBNull.Value, a)
        'Return IIf(a = Nothing Or a = DEFAULT_String, DBNull.Value, a)
    End Function
    Public Function getParamValueDate(ByVal a As Date) As Object
        Return IIf(a = Nothing, DBNull.Value, a)
    End Function
    Public Function getParamValueChar(ByVal a As Char) As Object
        Return IIf(a = Nothing, DBNull.Value, a)
    End Function

End Class
