'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOSerieProducto
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaSerieProducto(ByVal serieproducto As Entidades.SerieProducto) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(0).Value = serieproducto.IdProducto
        ArrayParametros(1) = New SqlParameter("@sp_Numero", SqlDbType.VarChar)
        ArrayParametros(1).Value = serieproducto.Numero
        Return HDAO.Insert(cn, "_SerieProductoInsert", ArrayParametros)
    End Function
    Public Function ActualizaSerieProducto(ByVal serieproducto As Entidades.SerieProducto) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(0).Value = serieproducto.IdProducto
        ArrayParametros(1) = New SqlParameter("@sp_Numero", SqlDbType.VarChar)
        ArrayParametros(1).Value = serieproducto.Numero
        Return HDAO.Update(cn, "_SerieProductoUpdate", ArrayParametros)
    End Function
End Class
