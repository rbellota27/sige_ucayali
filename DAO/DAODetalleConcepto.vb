﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'**************************   MARTES 11 MAYO 2010 HORA 10_45 AM



Imports System.Data.SqlClient
Public Class DAODetalleConcepto
    Private objHDAO As New DAO.HelperDAO
    Private cn As SqlConnection
    Private tr As SqlTransaction
    Private cmd As SqlCommand

    Private objDaoMantenedor As New DAO.DAOMantenedor

    Public Function DetalleConceptoInsert(ByVal objDetalleConcepto As Entidades.DetalleConcepto, ByVal cn As SqlConnection, ByVal tr As SqlTransaction, Optional ByVal valConceptoTienda As Boolean = False) As Integer

        Dim p() As SqlParameter = New SqlParameter(12) {}
        p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        p(0).Value = IIf(objDetalleConcepto.IdDocumento = Nothing, DBNull.Value, objDetalleConcepto.IdDocumento)
        p(1) = New SqlParameter("@IdFlete", SqlDbType.Int)
        p(1).Value = IIf(objDetalleConcepto.IdFlete = Nothing, DBNull.Value, objDetalleConcepto.IdFlete)
        p(2) = New SqlParameter("@IdTipoDocumentoRef", SqlDbType.Int)
        p(2).Value = IIf(objDetalleConcepto.IdTipoDocumentoRef = Nothing, DBNull.Value, objDetalleConcepto.IdTipoDocumentoRef)
        p(3) = New SqlParameter("@dg_NroDocumento", SqlDbType.VarChar)
        p(3).Value = IIf(objDetalleConcepto.NroDocumento = Nothing, DBNull.Value, objDetalleConcepto.NroDocumento)
        p(4) = New SqlParameter("@dr_Concepto", SqlDbType.VarChar)
        p(4).Value = IIf(objDetalleConcepto.Concepto = Nothing, DBNull.Value, objDetalleConcepto.Concepto)
        p(5) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        p(5).Value = IIf(objDetalleConcepto.IdMoneda = Nothing, DBNull.Value, objDetalleConcepto.IdMoneda)
        p(6) = New SqlParameter("@dr_Monto", SqlDbType.Decimal)
        p(6).Value = IIf(objDetalleConcepto.Monto = Nothing, DBNull.Value, objDetalleConcepto.Monto)
        p(7) = New SqlParameter("@IdConcepto", SqlDbType.Int)
        p(7).Value = IIf(objDetalleConcepto.IdConcepto = Nothing, DBNull.Value, objDetalleConcepto.IdConcepto)
        p(8) = New SqlParameter("@IdDetalleConcepto", SqlDbType.Int)
        p(8).Direction = ParameterDirection.Output

        p(9) = New SqlParameter("@valConceptoTienda", SqlDbType.Bit)
        p(9).Value = valConceptoTienda

        p(10) = New SqlParameter("@IdDocumentoRef", SqlDbType.Int)
        p(10).Value = IIf(objDetalleConcepto.IdDocumentoRef = Nothing, DBNull.Value, objDetalleConcepto.IdDocumentoRef)

        p(11) = New SqlParameter("@dr_Monto2", SqlDbType.Decimal)
        p(11).Value = IIf(objDetalleConcepto.Monto2 = Nothing, DBNull.Value, objDetalleConcepto.Monto2)

        p(12) = objDaoMantenedor.getParam(objDetalleConcepto.IdMotivoGasto, "@IdMotivoGasto", SqlDbType.Int)


        Return objHDAO.InsertaTParameterOutPut(cn, "_DetalleConceptoInsert", p, tr)

    End Function
    Public Function DetalleConceptoInsertSolPago(ByVal objDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal obj As Entidades.Documento, ByVal cn As SqlConnection, ByVal T As SqlTransaction) As Integer

        Dim Detalleconcepto As Entidades.DetalleConcepto
        For Each Detalleconcepto In objDetalleConcepto
            Dim ArrayParametros() As SqlParameter = New SqlParameter(9) {}
            ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            ArrayParametros(0).Value = IIf(obj.IdDocumento = Nothing, DBNull.Value, obj.IdDocumento)

            ArrayParametros(1) = New SqlParameter("@IdDetalleConcepto", SqlDbType.Int)
            ArrayParametros(1).Value = IIf(Detalleconcepto.IdDetalleConcepto = Nothing, DBNull.Value, Detalleconcepto.IdDetalleConcepto)

            ArrayParametros(2) = New SqlParameter("@IdTipoDocumentoRef", SqlDbType.Int)
            ArrayParametros(2).Value = IIf(Detalleconcepto.IdTipoDocumentoRef = Nothing, DBNull.Value, Detalleconcepto.IdTipoDocumentoRef)

            ArrayParametros(3) = New SqlParameter("@dg_NroDocumento", SqlDbType.Int)
            ArrayParametros(3).Value = IIf(Detalleconcepto.NroDocumento = Nothing, DBNull.Value, Detalleconcepto.NroDocumento)

            ArrayParametros(4) = New SqlParameter("@IdMoneda", SqlDbType.Int)
            ArrayParametros(4).Value = IIf(Detalleconcepto.IdMoneda = Nothing, DBNull.Value, Detalleconcepto.IdMoneda)

            ArrayParametros(5) = New SqlParameter("@Monto", SqlDbType.Int)
            ArrayParametros(5).Value = IIf(Detalleconcepto.Monto = Nothing, DBNull.Value, Detalleconcepto.Monto)

            ArrayParametros(6) = New SqlParameter("@IdConcepto", SqlDbType.Int)
            ArrayParametros(6).Value = IIf(Detalleconcepto.IdConcepto = Nothing, DBNull.Value, Detalleconcepto.IdConcepto)

            ArrayParametros(7) = New SqlParameter("@IdProveedor", SqlDbType.Int)
            ArrayParametros(7).Value = IIf(Detalleconcepto.IdProveedor = Nothing, DBNull.Value, Detalleconcepto.IdProveedor)

            ArrayParametros(8) = New SqlParameter("@dr_Concepto", SqlDbType.VarChar)
            ArrayParametros(8).Value = IIf(Detalleconcepto.Concepto = Nothing, DBNull.Value, Detalleconcepto.Concepto)

            ArrayParametros(9) = New SqlParameter("@IdDocumentoRef", SqlDbType.Int)
            ArrayParametros(9).Value = IIf(Detalleconcepto.IdDocumentoRef = Nothing, DBNull.Value, Detalleconcepto.IdDocumentoRef)

            objHDAO.InsertaT(cn, "InsUpd_DetalleConcepto", ArrayParametros, T)
        Next



    End Function

    Public Function GrabarDetalleConceptoT(ByVal cn As SqlConnection, ByVal vIdDocumento As Integer, ByVal ldetalleconcepto As List(Of Entidades.DetalleConcepto), ByVal T As SqlTransaction, Optional ByVal valConceptoAnticipo As Boolean = False) As Boolean
        Dim Detalleconcepto As Entidades.DetalleConcepto
        For Each Detalleconcepto In ldetalleconcepto
            Dim ArrayParametros() As SqlParameter = New SqlParameter(10) {}
            ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            ArrayParametros(0).Value = IIf(vIdDocumento = Nothing, DBNull.Value, vIdDocumento)

            ArrayParametros(1) = New SqlParameter("@IdDetalleConcepto", SqlDbType.Int)
            ArrayParametros(1).Value = IIf(Detalleconcepto.IdDetalleConcepto = Nothing, DBNull.Value, Detalleconcepto.IdDetalleConcepto)

            ArrayParametros(2) = New SqlParameter("@IdTipoDocumentoRef", SqlDbType.Int)
            ArrayParametros(2).Value = IIf(Detalleconcepto.IdTipoDocumentoRef = Nothing, DBNull.Value, Detalleconcepto.IdTipoDocumentoRef)

            ArrayParametros(3) = New SqlParameter("@dg_NroDocumento", SqlDbType.Int)
            ArrayParametros(3).Value = IIf(Detalleconcepto.NroDocumento = Nothing, DBNull.Value, Detalleconcepto.NroDocumento)

            ArrayParametros(4) = New SqlParameter("@IdMoneda", SqlDbType.Int)
            ArrayParametros(4).Value = IIf(Detalleconcepto.IdMoneda = Nothing, DBNull.Value, Detalleconcepto.IdMoneda)

            ArrayParametros(5) = New SqlParameter("@Monto", SqlDbType.Int)
            ArrayParametros(5).Value = IIf(Detalleconcepto.Monto = Nothing, DBNull.Value, Detalleconcepto.Monto)

            ArrayParametros(6) = New SqlParameter("@IdConcepto", SqlDbType.Int)
            ArrayParametros(6).Value = IIf(Detalleconcepto.IdConcepto = Nothing, DBNull.Value, Detalleconcepto.IdConcepto)

            ArrayParametros(7) = New SqlParameter("@IdProveedor", SqlDbType.Int)
            ArrayParametros(7).Value = IIf(Detalleconcepto.IdProveedor = Nothing, DBNull.Value, Detalleconcepto.IdProveedor)

            ArrayParametros(8) = New SqlParameter("@dr_Concepto", SqlDbType.VarChar)
            ArrayParametros(8).Value = IIf(Detalleconcepto.Concepto = Nothing, DBNull.Value, Detalleconcepto.Concepto)

            ArrayParametros(9) = New SqlParameter("@IdDocumentoRef", SqlDbType.Int)
            ArrayParametros(9).Value = IIf(Detalleconcepto.IdDocumentoRef = Nothing, DBNull.Value, Detalleconcepto.IdDocumentoRef)

            ArrayParametros(10) = New SqlParameter("@valConceptoAnticipo", SqlDbType.Bit)
            ArrayParametros(10).Value = valConceptoAnticipo

            objHDAO.InsertaT(cn, "InsUpd_DetalleConcepto", ArrayParametros, T)
        Next
    End Function

    Public Function SelectxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleConcepto)

        Dim lista As New List(Of Entidades.DetalleConcepto)

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            cmd = New SqlCommand("_DetalleConceptoSelectxIdDocumento", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            cn.Open()
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            While (lector.Read)

                Dim objDetalleConcepto As New Entidades.DetalleConcepto
                With objDetalleConcepto

                    .IdDocumento = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                    .IdDetalleConcepto = CInt(IIf(IsDBNull(lector("IdDetalleConcepto")) = True, 0, lector("IdDetalleConcepto")))
                    .Concepto = CStr(IIf(IsDBNull(lector("dr_Concepto")) = True, "", lector("dr_Concepto")))
                    .IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")) = True, 0, lector("IdMoneda")))
                    .Monto = CDec(IIf(IsDBNull(lector("dr_Monto")) = True, 0, lector("dr_Monto")))
                    .IdConcepto = CInt(IIf(IsDBNull(lector("IdConcepto")) = True, 0, lector("IdConcepto")))
                    .Descripcion = CStr(IIf(IsDBNull(lector("DescripcionCompleto")) = True, "", lector("DescripcionCompleto")))
                    .Moneda = CStr(IIf(IsDBNull(lector("Moneda")) = True, "", lector("Moneda")))
                    .IdMotivoGasto = objDaoMantenedor.UCInt(lector("IdMotGasto"))
                    .IdDocumentoRef = objDaoMantenedor.UCInt(lector("IdDocumentoRef"))

                End With

                lista.Add(objDetalleConcepto)

            End While

            lector.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista


    End Function




End Class
