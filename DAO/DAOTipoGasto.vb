﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'********************  JUEVES 11 FEB 2010 HORA 10_00 AM

'*************** LUNES 18 ENERO 2010 HORA 11_51 AM

'********************************************************
'Autor      : Chang Carnero, Edgar
'Módulo     : Concepto (CboTipoGasto)
'Sistema    : Sanicenter
'Empresa    : Digrafic SRL
'Modificado : 13-Enero-2010
'Hora       : 01:00:00 pm
'********************************************************
Imports System.Data.SqlClient
Public Class DAOTipoGasto
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectAllActivo() As List(Of Entidades.TipoGasto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoGastoSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoGasto)
                Do While lector.Read
                    Dim objTipoGasto As New Entidades.TipoGasto
                    'objCalidad.Abv = CStr(IIf(IsDBNull(lector.Item("Cal_Abv")) = True, "", lector.Item("Cal_Abv")))
                    objTipoGasto.TG_Nombre = CStr(IIf(IsDBNull(lector.Item("TG_Nombre")) = True, "", lector.Item("TG_Nombre")))
                    objTipoGasto.Estado = CBool(IIf(IsDBNull(lector.Item("TG_Estado")) = True, "", lector.Item("TG_Estado")))
                    objTipoGasto.IdTipoGasto = CInt(IIf(IsDBNull(lector.Item("IdTipoGasto")) = True, 0, lector.Item("IdTipoGasto")))
                    Lista.Add(objTipoGasto)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.TipoGasto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoGastoSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoGasto)
                Do While lector.Read
                    Dim objTipoGasto As New Entidades.TipoGasto
                    objTipoGasto.TG_Nombre = CStr(IIf(IsDBNull(lector.Item("tg_Nombre")) = True, "", lector.Item("tg_Nombre")))
                    objTipoGasto.Estado = CBool(IIf(IsDBNull(lector.Item("Tg_Estado")) = True, "", lector.Item("Tg_Estado")))
                    objTipoGasto.IdTipoGasto = CInt(IIf(IsDBNull(lector.Item("idtipogasto")) = True, 0, lector.Item("idtipogasto")))
                    Lista.Add(objTipoGasto)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.TipoGasto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoGastoSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoGasto)
                Do While lector.Read
                    Dim objTipoGasto As New Entidades.TipoGasto
                    objTipoGasto.TG_Nombre = CStr(IIf(IsDBNull(lector.Item("tg_Nombre")) = True, "", lector.Item("tg_Nombre")))
                    objTipoGasto.Estado = CBool(IIf(IsDBNull(lector.Item("tg_Estado")) = True, "", lector.Item("tg_Estado")))
                    objTipoGasto.IdTipoGasto = CInt(IIf(IsDBNull(lector.Item("IdTipoGasto")) = True, 0, lector.Item("IdTipoGasto")))
                    Lista.Add(objTipoGasto)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function InsertaTipoGasto(ByVal TipoGasto As Entidades.TipoGasto) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@TG_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = TipoGasto.TG_Nombre
        ArrayParametros(1) = New SqlParameter("@TG_Estado", SqlDbType.Bit)
        ArrayParametros(1).Value = TipoGasto.Estado
        Return HDAO.Insert(cn, "_TipoGastoInsert", ArrayParametros)
    End Function
    Public Function ActualizaTipoGasto(ByVal TipoGasto As Entidades.TipoGasto) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@tg_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = TipoGasto.TG_Nombre
        ArrayParametros(1) = New SqlParameter("@tg_Estado", SqlDbType.Bit)
        ArrayParametros(1).Value = TipoGasto.Estado
        ArrayParametros(2) = New SqlParameter("@IdTipoGasto", SqlDbType.Int)
        ArrayParametros(2).Value = TipoGasto.IdTipoGasto
        Return HDAO.Update(cn, "_TipoGastoUpdate", ArrayParametros)
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.TipoGasto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoGastoSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoGasto)
                Do While lector.Read
                    Dim objTipoGasto As New Entidades.TipoGasto
                    objTipoGasto.TG_Nombre = CStr(IIf(IsDBNull(lector.Item("TG_Nombre")) = True, "", lector.Item("TG_Nombre")))
                    objTipoGasto.Estado = CBool(IIf(IsDBNull(lector.Item("TG_Estado")) = True, "", lector.Item("TG_Estado")))
                    objTipoGasto.IdTipoGasto = CInt(IIf(IsDBNull(lector.Item("IdTipoGasto")) = True, 0, lector.Item("IdTipoGasto")))
                    Lista.Add(objTipoGasto)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoGasto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoGastoSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoGasto)
                Do While lector.Read
                    Dim objTipoGasto As New Entidades.TipoGasto
                    objTipoGasto.TG_Nombre = CStr(IIf(IsDBNull(lector.Item("tg_Nombre")) = True, "", lector.Item("tg_Nombre")))
                    objTipoGasto.Estado = CBool(IIf(IsDBNull(lector.Item("tg_Estado")) = True, "", lector.Item("tg_Estado")))
                    objTipoGasto.IdTipoGasto = CInt(IIf(IsDBNull(lector.Item("IdTipoGasto")) = True, 0, lector.Item("IdTipoGasto")))
                    Lista.Add(objTipoGasto)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoGasto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoGastoSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoGasto)
                Do While lector.Read
                    Dim objTipoGasto As New Entidades.TipoGasto
                    objTipoGasto.TG_Nombre = CStr(IIf(IsDBNull(lector.Item("tg_Nombre")) = True, "", lector.Item("tg_Nombre")))
                    objTipoGasto.Estado = CBool(IIf(IsDBNull(lector.Item("tg_Estado")) = True, "", lector.Item("tg_Estado")))
                    objTipoGasto.IdTipoGasto = CInt(IIf(IsDBNull(lector.Item("IdTipoGasto")) = True, 0, lector.Item("IdTipoGasto")))
                    Lista.Add(objTipoGasto)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.TipoGasto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoGastoSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoGasto)
                Do While lector.Read
                    Dim objTipoGasto As New Entidades.TipoGasto
                    objTipoGasto.TG_Nombre = CStr(IIf(IsDBNull(lector.Item("tg_Nombre")) = True, "", lector.Item("tg_Nombre")))
                    objTipoGasto.Estado = CBool(IIf(IsDBNull(lector.Item("tg_Estado")) = True, "", lector.Item("tg_Estado")))
                    objTipoGasto.IdTipoGasto = CInt(IIf(IsDBNull(lector.Item("IdTipoGasto")) = True, 0, lector.Item("IdTipoGasto")))
                    Lista.Add(objTipoGasto)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCbo() As List(Of Entidades.TipoGasto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoGastoSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.TipoGasto)
                Do While lector.Read
                    Dim TipoGasto As New Entidades.TipoGasto
                    TipoGasto.IdTipoGasto = CInt(lector.Item("IdTipoGasto"))
                    TipoGasto.TG_Nombre = CStr(lector.Item("tg_Nombre"))
                    lista.Add(TipoGasto)
                Loop
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function
End Class


