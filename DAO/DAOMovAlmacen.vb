'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.





'*******************      VIERNES   19 MARZO 2010 HORA 10_22 AM









Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOMovAlmacen
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion


    Public Sub MovAlmacenUpdate_Cantidad_CostoxIdDetalleDocumento(ByVal Cantidad As Decimal, ByVal Costo As Decimal, ByVal IdDetalleDocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(2) {}
        p(0) = New SqlParameter("@Cantidad", SqlDbType.Decimal)
        p(0).Value = Cantidad

        p(1) = New SqlParameter("@Costo", SqlDbType.Decimal)
        p(1).Value = Costo

        p(2) = New SqlParameter("@IdDetalleDocumento", SqlDbType.Int)
        p(2).Value = IdDetalleDocumento


        Dim cmd As New SqlCommand("_MovAlmacenUpdate_Cantidad_CostoxIdDetalleDocumento", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0

        cmd.Parameters.AddRange(p)

        cmd.ExecuteNonQuery()

    End Sub

    Public Sub InsertaMovAlmacen(ByVal cn As SqlConnection, ByVal movalmacen As Entidades.MovAlmacen, ByVal T As SqlTransaction)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(14) {}
        ArrayParametros(0) = New SqlParameter("@IdDetalleDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = movalmacen.IdDetalleDocumento
        ArrayParametros(1) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(1).Value = movalmacen.IdDocumento
        ArrayParametros(2) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(2).Value = movalmacen.IdProducto
        ArrayParametros(3) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
        ArrayParametros(3).Value = movalmacen.IdUnidadMedida
        ArrayParametros(4) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(4).Value = movalmacen.IdEmpresa
        ArrayParametros(5) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(5).Value = IIf(movalmacen.IdTienda = Nothing, DBNull.Value, movalmacen.IdTienda)
        ArrayParametros(6) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        ArrayParametros(6).Value = movalmacen.IdAlmacen
        ArrayParametros(7) = New SqlParameter("@ma_UMPrincipal", SqlDbType.VarChar)
        ArrayParametros(7).Value = movalmacen.UMPrincipal
        ArrayParametros(8) = New SqlParameter("@ma_IdUMPrincipal", SqlDbType.VarChar)
        ArrayParametros(8).Value = movalmacen.IdUMPrincipal
        ArrayParametros(9) = New SqlParameter("@ma_CantidadMov", SqlDbType.Decimal)
        ArrayParametros(9).Value = movalmacen.CantidadMov
        ArrayParametros(10) = New SqlParameter("@ma_CantxAtender", SqlDbType.Decimal)
        ArrayParametros(10).Value = IIf(movalmacen.CantxAtender = Nothing, DBNull.Value, movalmacen.CantxAtender)
        ArrayParametros(11) = New SqlParameter("@ma_Comprometido", SqlDbType.Bit)
        ArrayParametros(11).Value = movalmacen.Comprometido
        ArrayParametros(12) = New SqlParameter("@ma_Factor", SqlDbType.Int)
        ArrayParametros(12).Value = movalmacen.Factor
        ArrayParametros(13) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        ArrayParametros(13).Value = IIf(movalmacen.IdTipoOperacion = Nothing, DBNull.Value, movalmacen.IdTipoOperacion)
        ArrayParametros(14) = New SqlParameter("@IdMetodoV", SqlDbType.Int)
        ArrayParametros(14).Value = IIf(movalmacen.IdMetodoV = Nothing, DBNull.Value, movalmacen.IdMetodoV)
        HDAO.InsertaT(cn, "_MovAlmacenInsert", ArrayParametros, T)
    End Sub

    Public Sub ActualizaMovAlmacen(ByVal cn As SqlConnection, ByVal movalmacen As Entidades.MovAlmacen, ByVal T As SqlTransaction)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(14) {}
        ArrayParametros(0) = New SqlParameter("@IdDetalleDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = movalmacen.IdDetalleDocumento
        ArrayParametros(1) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(1).Value = movalmacen.IdDocumento
        ArrayParametros(2) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(2).Value = movalmacen.IdProducto
        ArrayParametros(3) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
        ArrayParametros(3).Value = movalmacen.IdUnidadMedida
        ArrayParametros(4) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(4).Value = movalmacen.IdEmpresa
        ArrayParametros(5) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(5).Value = IIf(movalmacen.IdTienda = Nothing, DBNull.Value, movalmacen.IdTienda)
        ArrayParametros(6) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        ArrayParametros(6).Value = movalmacen.IdAlmacen
        ArrayParametros(7) = New SqlParameter("@ma_UMPrincipal", SqlDbType.VarChar)
        ArrayParametros(7).Value = movalmacen.UMPrincipal
        ArrayParametros(8) = New SqlParameter("@ma_IdUMPrincipal", SqlDbType.VarChar)
        ArrayParametros(8).Value = movalmacen.IdUMPrincipal
        ArrayParametros(9) = New SqlParameter("@ma_CantidadMov", SqlDbType.Decimal)
        ArrayParametros(9).Value = movalmacen.CantidadMov
        ArrayParametros(10) = New SqlParameter("@ma_CantxAtender", SqlDbType.Decimal)
        ArrayParametros(10).Value = IIf(movalmacen.CantxAtender = Nothing, DBNull.Value, movalmacen.CantxAtender)
        ArrayParametros(11) = New SqlParameter("@ma_Comprometido", SqlDbType.Bit)
        ArrayParametros(11).Value = movalmacen.Comprometido
        ArrayParametros(12) = New SqlParameter("@ma_Factor", SqlDbType.Int)
        ArrayParametros(12).Value = movalmacen.Factor
        ArrayParametros(13) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        ArrayParametros(13).Value = movalmacen.IdTipoOperacion
        ArrayParametros(14) = New SqlParameter("@IdMetodoV", SqlDbType.Int)
        ArrayParametros(14).Value = IIf(movalmacen.IdMetodoV = Nothing, DBNull.Value, movalmacen.IdMetodoV)
        Dim cmd As New SqlCommand("_MovAlmacenUpdate", cn, T)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        If cmd.ExecuteNonQuery = 0 Then
            Throw New Exception
        End If
    End Sub


    Public Sub InsertaLMovAlmacen(ByVal cn As SqlConnection, ByVal LMovalmacen As List(Of Entidades.MovAlmacen), ByVal T As SqlTransaction)
        Dim MovAlmacen As New Entidades.MovAlmacen
        For Each MovAlmacen In LMovalmacen

            Dim ArrayParametros() As SqlParameter = New SqlParameter(14) {}
            ArrayParametros(0) = New SqlParameter("@IdDetalleDocumento", SqlDbType.Int)
            ArrayParametros(0).Value = MovAlmacen.IdDetalleDocumento
            ArrayParametros(1) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            ArrayParametros(1).Value = MovAlmacen.IdDocumento
            ArrayParametros(2) = New SqlParameter("@IdProducto", SqlDbType.Int)
            ArrayParametros(2).Value = MovAlmacen.IdProducto
            ArrayParametros(3) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
            ArrayParametros(3).Value = MovAlmacen.IdUnidadMedida
            ArrayParametros(4) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
            ArrayParametros(4).Value = MovAlmacen.IdEmpresa
            ArrayParametros(5) = New SqlParameter("@IdTienda", SqlDbType.Int)
            ArrayParametros(5).Value = MovAlmacen.IdTienda
            ArrayParametros(6) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
            ArrayParametros(6).Value = MovAlmacen.IdAlmacen
            ArrayParametros(7) = New SqlParameter("@ma_UMPrincipal", SqlDbType.VarChar)
            ArrayParametros(7).Value = MovAlmacen.UMPrincipal
            ArrayParametros(8) = New SqlParameter("@ma_IdUMPrincipal", SqlDbType.VarChar)
            ArrayParametros(8).Value = MovAlmacen.IdUMPrincipal
            ArrayParametros(9) = New SqlParameter("@ma_CantidadMov", SqlDbType.Decimal)
            ArrayParametros(9).Value = MovAlmacen.CantidadMov
            ArrayParametros(10) = New SqlParameter("@ma_CantxAtender", SqlDbType.Decimal)
            ArrayParametros(10).Value = IIf(MovAlmacen.CantxAtender = Nothing, DBNull.Value, MovAlmacen.CantxAtender)
            ArrayParametros(11) = New SqlParameter("@ma_Comprometido", SqlDbType.Bit)
            ArrayParametros(11).Value = MovAlmacen.Comprometido
            ArrayParametros(12) = New SqlParameter("@ma_Factor", SqlDbType.Int)
            ArrayParametros(12).Value = MovAlmacen.Factor
            ArrayParametros(13) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
            ArrayParametros(13).Value = MovAlmacen.IdTipoOperacion
            ArrayParametros(14) = New SqlParameter("@IdMetodoV", SqlDbType.Int)
            ArrayParametros(14).Value = IIf(MovAlmacen.IdMetodoV = Nothing, DBNull.Value, MovAlmacen.IdMetodoV)

            HDAO.InsertaT(cn, "_MovAlmacenInsert", ArrayParametros, T)
        Next
    End Sub

    Public Function SelectxEmpresaxAlmacenxProducto(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal idempresa As Integer, ByVal idalmacen As Integer, ByVal idproducto As Integer) As List(Of Entidades.MovAlmacen)
        Dim cmd As New SqlCommand("_MovAlmacenSelectxEmpresaxAlmacenxProducto", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", idempresa)
        cmd.Parameters.AddWithValue("@IdAlmacen", idalmacen)
        cmd.Parameters.AddWithValue("@IdProducto", idproducto)
        Dim lector As SqlDataReader
        Try
            lector = cmd.ExecuteReader()
            Dim Lista As New List(Of Entidades.MovAlmacen)
            Do While lector.Read
                Dim obj As New Entidades.MovAlmacen
                With obj
                    .CantidadMov = CDec(IIf(IsDBNull(lector.Item("ma_CantidadMov")) = True, 0, lector.Item("ma_CantidadMov")))
                    .CantxAtender = CDec(IIf(IsDBNull(lector.Item("ma_CantxAtender")) = True, 0, lector.Item("ma_CantxAtender")))
                    .Comprometido = CBool(IIf(IsDBNull(lector.Item("ma_Comprometido")) = True, False, lector.Item("ma_Comprometido")))
                    .Factor = CInt(IIf(IsDBNull(lector.Item("ma_Factor")) = True, 0, lector.Item("ma_Factor")))
                    .Fecha = CDate(IIf(IsDBNull(lector.Item("ma_Fecha")) = True, Nothing, lector.Item("ma_Fecha")))
                    .IdDetalleDocumento = CInt(IIf(IsDBNull(lector.Item("IdMovAlmacen")) = True, 0, lector.Item("IdMovAlmacen")))
                    .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                    .IdMetodoV = CInt(IIf(IsDBNull(lector.Item("IdMetodoV")) = True, 0, lector.Item("IdMetodoV")))
                    .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                    .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                    .UMPrincipal = CStr(IIf(IsDBNull(lector.Item("ma_UMPrincipal")) = True, "", lector.Item("ma_UMPrincipal")))
                End With
                Lista.Add(obj)
            Loop
            lector.Close()
            Return Lista
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectxEmpresaxAlmacenxProductoxMovAlmacenBase(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal idempresa As Integer, ByVal idalmacen As Integer, ByVal idproducto As Integer, ByVal idmovalmacenbase As Integer) As List(Of Entidades.MovAlmacen)
        Dim cmd As New SqlCommand("_MovAlmacenListadoSelectxEmpresaxAlmacenxProductoxMovAlmacenBase", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdAlmacen", idalmacen)
        cmd.Parameters.AddWithValue("@IdProducto", idproducto)
        cmd.Parameters.AddWithValue("@IdEmpresa", idempresa)
        cmd.Parameters.AddWithValue("@IdMovAlmacenBase", idmovalmacenbase)
        Dim lector As SqlDataReader
        Try
            lector = cmd.ExecuteReader()
            Dim Lista As New List(Of Entidades.MovAlmacen)
            Do While lector.Read
                Dim obj As New Entidades.MovAlmacen
                With obj
                    .CantidadMov = CDec(IIf(IsDBNull(lector.Item("ma_CantidadMov")) = True, 0, lector.Item("ma_CantidadMov")))
                    .CantxAtender = CDec(IIf(IsDBNull(lector.Item("ma_CantxAtender")) = True, 0, lector.Item("ma_CantxAtender")))
                    .Comprometido = CBool(IIf(IsDBNull(lector.Item("ma_Comprometido")) = True, False, lector.Item("ma_Comprometido")))
                    .Factor = CInt(IIf(IsDBNull(lector.Item("ma_Factor")) = True, 0, lector.Item("ma_Factor")))
                    .Fecha = CDate(IIf(IsDBNull(lector.Item("ma_Fecha")) = True, Nothing, lector.Item("ma_Fecha")))
                    .IdDetalleDocumento = CInt(IIf(IsDBNull(lector.Item("IdMovAlmacen")) = True, 0, lector.Item("IdMovAlmacen")))
                    .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                    .IdMetodoV = CInt(IIf(IsDBNull(lector.Item("IdMetodoV")) = True, 0, lector.Item("IdMetodoV")))
                    .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                    .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                    .UMPrincipal = CStr(IIf(IsDBNull(lector.Item("ma_UMPrincipal")) = True, "", lector.Item("ma_UMPrincipal")))
                End With
                Lista.Add(obj)
            Loop
            lector.Close()
            Return Lista
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function DeletexIdDocumento_1(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal IdDocumento As Integer) As Boolean
        Dim cmd As New SqlCommand("_MovAlmacenDeletexIdDocumento", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.ExecuteNonQuery()
        Return True
    End Function
    Public Sub DeletexIdDocumento(ByVal cn As SqlConnection, ByVal T As SqlTransaction, ByVal IdDocumento As Integer)
        'Dim cmd As New SqlCommand("_MovAlmacenDeletexIdDocumento", cn, tr)
        'cmd.CommandType = CommandType.StoredProcedure
        'P.Parameters.AddWithValue("@IdDocumento", iddocumento)
        Dim P As SqlParameter = New SqlParameter("@IdDocumento", SqlDbType.Int)
        P.Value = IdDocumento
        HDAO.DeletexParam1T(cn, "_MovAlmacenDeletexIdDocumento", P, T)
        'Try
        '    cmd.ExecuteNonQuery()
        '    Return True
        'Catch ex As Exception
        '    Throw ex
        'End Try
    End Sub
    Public Function SelectMinIdMovAlmacenxIdDocumento(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal IdDocumento As Integer) As Integer
        Dim cmd As New SqlCommand("_MovAlmacenSelectMinIdMovAlmacenxIdDocumento", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim idMovAlmacenBase As Integer = 0
        Dim lector As SqlDataReader
        Try
            lector = cmd.ExecuteReader()
            If lector.Read Then
                idMovAlmacenBase = CInt(IIf(IsDBNull(lector.Item("IdMovAlmacen")) = True, 0, lector.Item("IdMovAlmacen")))
            End If
            lector.Close()
            Return idMovAlmacenBase
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function UpdateCantxAtenderxIdDetalleDocumento(ByVal IdDetalleDocumento As Integer, ByVal cantxatender As Decimal, ByVal comprometido As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Dim cmd As New SqlCommand("_MovAlmacenUpdateCantxAtenderxIdDetalleDocumento", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDetalleDocumento", IdDetalleDocumento)
        cmd.Parameters.AddWithValue("@CantxAtender", cantxatender)
        cmd.Parameters.AddWithValue("@Comprometido", comprometido)
        If cmd.ExecuteNonQuery = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    Public Function SelectxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.MovAlmacen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MovAlmacenSelectxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Dim Lista As New List(Of Entidades.MovAlmacen)
            Do While lector.Read
                Dim obj As New Entidades.MovAlmacen
                With obj
                    .CantidadMov = CDec(IIf(IsDBNull(lector.Item("ma_CantidadMov")) = True, 0, lector.Item("ma_CantidadMov")))
                    .CantxAtender = CDec(IIf(IsDBNull(lector.Item("ma_CantxAtender")) = True, 0, lector.Item("ma_CantxAtender")))
                    .Comprometido = CBool(IIf(IsDBNull(lector.Item("ma_Comprometido")) = True, False, lector.Item("ma_Comprometido")))
                    .Factor = CInt(IIf(IsDBNull(lector.Item("ma_Factor")) = True, 0, lector.Item("ma_Factor")))
                    .Fecha = CDate(IIf(IsDBNull(lector.Item("ma_Fecha")) = True, Nothing, lector.Item("ma_Fecha")))
                    .IdDetalleDocumento = CInt(IIf(IsDBNull(lector.Item("IdMovAlmacen")) = True, 0, lector.Item("IdMovAlmacen")))
                    .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                    .IdMetodoV = CInt(IIf(IsDBNull(lector.Item("IdMetodoV")) = True, 0, lector.Item("IdMetodoV")))
                    .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                    .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                    .IdTipoOperacion = CInt(IIf(IsDBNull(lector.Item("IdTipoOperacion")) = True, 0, lector.Item("IdTipoOperacion")))
                    .UMPrincipal = CStr(IIf(IsDBNull(lector.Item("ma_UMPrincipal")) = True, "", lector.Item("ma_UMPrincipal")))
                    .NomProducto = CStr(IIf(IsDBNull(lector.Item("prod_Nombre")) = True, "", lector.Item("prod_Nombre")))
                End With
                Lista.Add(obj)
            Loop
            lector.Close()
            Return Lista
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function
    Public Function SelectIdDocumentoInvInicial(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdTipoOperacion As Integer) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MovAlmacenSelectIdDocumentoInvInicial", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion)
        Dim lector As SqlDataReader
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Dim IdDocumento As Integer = 0
            If lector.Read Then
                IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
            Else
                IdDocumento = -1
            End If
            lector.Close()
            Return IdDocumento
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function RptMovAlmacen(ByVal idempresa As Integer, ByVal idorigen As Integer, ByVal iddestino As String, ByVal idlinea As String, ByVal idsublinea As String, ByVal fechaini As String, ByVal fechafin As String) As DataSet
        Dim da, da2 As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_MovAlmacenConsolidado", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdEmpresa", idempresa)
            cmd.Parameters.AddWithValue("@IdAlmacenOrigen", idorigen)
            cmd.Parameters.AddWithValue("@IdAlmacenDestino", iddestino)
            cmd.Parameters.AddWithValue("@IdLinea", idlinea)
            cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
            cmd.Parameters.AddWithValue("@FechaIni", fechaini)
            cmd.Parameters.AddWithValue("@FechaFin", fechafin)
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DtMovAlmacen")

            Dim cmd2 As New SqlCommand("List_ImagenReporte", objConexion.ConexionSIGE)
            cmd2.CommandType = CommandType.StoredProcedure
            da2 = New SqlDataAdapter(cmd2)
            da2.Fill(ds, "DtRptImagen")
        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function
    Public Function RptKardexMovAlmacen(ByVal idempresa As Integer, ByVal idalmacen As Integer, ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal idproducto As Integer, ByVal fechaini As String, ByVal fechafin As String) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_KardexAlmacen", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdEmpresa", idempresa)
            cmd.Parameters.AddWithValue("@IdAlmacen", idalmacen)
            cmd.Parameters.AddWithValue("@IdLinea", idlinea)
            cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
            cmd.Parameters.AddWithValue("@IdProducto", idproducto)
            cmd.Parameters.AddWithValue("@Fecha1", fechaini)
            cmd.Parameters.AddWithValue("@Fecha2", fechafin)
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DTKardexMovAlmacen")
        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function

    Public Function RptMovAlmacenxDoc(ByVal idempresa As Integer, ByVal idorigen As Integer, ByVal iddestino As String, ByVal idlinea As String, ByVal idsublinea As String, ByVal fechaini As String, ByVal fechafin As String, ByVal idtipdoc As Integer) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_MovAlmacenConsolidadoxDoc", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdEmpresa", idempresa)
            cmd.Parameters.AddWithValue("@IdAlmacenOrigen", idorigen)
            cmd.Parameters.AddWithValue("@IdAlmacenDestino", iddestino)
            cmd.Parameters.AddWithValue("@IdLinea", idlinea)
            cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
            cmd.Parameters.AddWithValue("@FechaIni", fechaini)
            cmd.Parameters.AddWithValue("@FechaFin", fechafin)
            cmd.Parameters.AddWithValue("@IdTipDoc", idtipdoc)
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DtMovAlmacendoc")
        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds


    End Function

    Public Function kardexporMetVal(ByVal idempresa As Integer, ByVal idalmacen As Integer, ByVal idproducto As Integer, ByVal fechainicio As String, ByVal fechafin As String) As DataSet

        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("[_CR_KardexProductoXMetVal]", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            cmd.Parameters.AddWithValue("@IdEmpresa", idempresa)
            cmd.Parameters.AddWithValue("@IdAlmacen", idalmacen)
            cmd.Parameters.AddWithValue("@IdProducto", idproducto)
            cmd.Parameters.AddWithValue("@Fechainicio", fechainicio)
            cmd.Parameters.AddWithValue("@Fechafin", fechafin)
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_kardexporMetVal")
        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds

    End Function


    Public Function CR_KardexProductoXMetVal(ByVal idempresa As Integer, ByVal idalmacen As Integer, ByVal idproducto As Integer, ByVal fechainicio As String, ByVal fechafin As String) As List(Of Entidades.Kardex)

        Dim lista As List(Of Entidades.Kardex)
        Dim obj As Entidades.Kardex
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim reader As SqlDataReader = Nothing
        Dim cmd As SqlCommand = Nothing
        Try
            cn.Open()

            cmd = New SqlCommand("[_CR_KardexProductoXMetVal]", cn)
            cmd.CommandTimeout = 0
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdEmpresa", idempresa)
            cmd.Parameters.AddWithValue("@IdAlmacen", idalmacen)
            cmd.Parameters.AddWithValue("@IdProducto", idproducto)
            cmd.Parameters.AddWithValue("@Fechainicio", fechainicio)
            cmd.Parameters.AddWithValue("@Fechafin", fechafin)

            lista = New List(Of Entidades.Kardex)
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            Do While reader.Read

                obj = New Entidades.Kardex
                With obj

                    .idregistro = CType(IIf(IsDBNull(reader("idregistro")), Nothing, reader("idregistro")), Integer?)
                    .IdDocumento = CType(IIf(IsDBNull(reader("IdDocumento")), Nothing, reader("IdDocumento")), Integer?)
                    .Documento = CStr(IIf(IsDBNull(reader("Documento")), "", reader("Documento")))
                    .Nro = CStr(IIf(IsDBNull(reader("Nro")), "", reader("Nro")))
                    .IdDetalleDocumento = CType(IIf(IsDBNull(reader("IdDetalleDocumento")), Nothing, reader("IdDetalleDocumento")), Integer?)
                    .doc_FechaEmision = CType(IIf(IsDBNull(reader("doc_FechaEmision")), Nothing, reader("doc_FechaEmision")), Date?)
                    .EntSal = CStr(IIf(IsDBNull(reader("EntSal")), "", reader("EntSal")))
                    .dma_Cantidad = CType(IIf(IsDBNull(reader("dma_Cantidad")), Nothing, reader("dma_Cantidad")), Decimal?)
                    .dma_Costo = CType(IIf(IsDBNull(reader("dma_Costo")), Nothing, reader("dma_Costo")), Decimal?)
                    .dma_Total = CType(IIf(IsDBNull(reader("dma_Total")), Nothing, reader("dma_Total")), Decimal?)
                    .sk_CantidadSaldo = CType(IIf(IsDBNull(reader("sk_CantidadSaldo")), Nothing, reader("sk_CantidadSaldo")), Decimal?)
                    .sk_CostoSaldo = CType(IIf(IsDBNull(reader("sk_CostoSaldo")), Nothing, reader("sk_CostoSaldo")), Decimal?)
                    .sk_TotalSaldo = CType(IIf(IsDBNull(reader("sk_TotalSaldo")), Nothing, reader("sk_TotalSaldo")), Decimal?)
                    .DocRef = CStr(IIf(IsDBNull(reader("DocRef")), "", reader("DocRef")))
                    .DocRefNumero = CStr(IIf(IsDBNull(reader("DocRefNumero")), "", reader("DocRefNumero")))

                End With

                lista.Add(obj)
                obj = Nothing
            Loop

            reader.Close()

            Return lista

        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

        Return Nothing

    End Function

    Public Function CR_KardexProductoXMetVal(ByVal idempresa As Integer, ByVal idalmacen As Integer, ByVal idproducto As Integer, ByVal fechainicio As String, ByVal fechafin As String, ByVal Igv As Decimal) As List(Of Entidades.Kardex)

        Dim lista As List(Of Entidades.Kardex)
        Dim obj As Entidades.Kardex
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim reader As SqlDataReader = Nothing
        Dim cmd As SqlCommand = Nothing
        Try
            cn.Open()

            cmd = New SqlCommand("[_CR_KardexProductoXMetVal]", cn)
            cmd.CommandTimeout = 0
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdEmpresa", idempresa)
            cmd.Parameters.AddWithValue("@IdAlmacen", idalmacen)
            cmd.Parameters.AddWithValue("@IdProducto", idproducto)
            cmd.Parameters.AddWithValue("@Fechainicio", fechainicio)
            cmd.Parameters.AddWithValue("@Fechafin", fechafin)

            lista = New List(Of Entidades.Kardex)
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            Do While reader.Read

                obj = New Entidades.Kardex
                With obj

                    .idregistro = CType(IIf(IsDBNull(reader("idregistro")), Nothing, reader("idregistro")), Integer?)
                    .IdDocumento = CType(IIf(IsDBNull(reader("IdDocumento")), Nothing, reader("IdDocumento")), Integer?)
                    .Documento = CStr(IIf(IsDBNull(reader("Documento")), "", reader("Documento")))
                    .Nro = CStr(IIf(IsDBNull(reader("Nro")), "", reader("Nro")))
                    .IdDetalleDocumento = CType(IIf(IsDBNull(reader("IdDetalleDocumento")), Nothing, reader("IdDetalleDocumento")), Integer?)
                    .doc_FechaEmision = CType(IIf(IsDBNull(reader("doc_FechaEmision")), Nothing, reader("doc_FechaEmision")), Date?)
                    .EntSal = CStr(IIf(IsDBNull(reader("EntSal")), "", reader("EntSal")))
                    .dma_Cantidad = CType(IIf(IsDBNull(reader("dma_Cantidad")), Nothing, reader("dma_Cantidad")), Decimal?)

                    .dma_Costo = CType(IIf(IsDBNull(reader("dma_Costo")), Nothing, reader("dma_Costo")), Decimal?)
                    .dma_Total = CType(IIf(IsDBNull(reader("dma_Total")), Nothing, reader("dma_Total")), Decimal?)

                    If Igv > 0 Then
                        .dma_Costo = .dma_Costo * (1 + Igv)
                        .dma_Total = .dma_Cantidad * .dma_Costo
                    End If

                    .sk_CantidadSaldo = CType(IIf(IsDBNull(reader("sk_CantidadSaldo")), Nothing, reader("sk_CantidadSaldo")), Decimal?)

                    .sk_CostoSaldo = CType(IIf(IsDBNull(reader("sk_CostoSaldo")), Nothing, reader("sk_CostoSaldo")), Decimal?)
                    .sk_TotalSaldo = CType(IIf(IsDBNull(reader("sk_TotalSaldo")), Nothing, reader("sk_TotalSaldo")), Decimal?)

                    If Igv > 0 Then
                        .sk_CostoSaldo = .sk_CostoSaldo * (1 + Igv)
                        .sk_TotalSaldo = .sk_CantidadSaldo * .sk_CostoSaldo
                    End If

                    .DocRef = CStr(IIf(IsDBNull(reader("DocRef")), "", reader("DocRef")))
                    .DocRefNumero = CStr(IIf(IsDBNull(reader("DocRefNumero")), "", reader("DocRefNumero")))

                End With

                lista.Add(obj)
                obj = Nothing
            Loop

            reader.Close()

            Return lista

        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

        Return Nothing

    End Function

End Class

