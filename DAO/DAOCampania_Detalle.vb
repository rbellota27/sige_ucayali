﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'******************   LUNES 31 MAYO 2010 HORA 03_23 PM

Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOCampania_Detalle

    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private cn2 As SqlConnection = (New DAO.Conexion).ConexionSIGE2
    Private reader As SqlDataReader
    Private objDaoMantenedor As New DAO.DAOMantenedor
    Private sqlCmd As SqlCommand
    Dim objConexion As New DAO.Conexion

    Public Function Campania_Detalle_Select(ByVal IdCampania As Integer, _
                                            ByVal IdTipoExistencia As Integer, _
                                            ByVal IdLinea As Integer, _
                                            ByVal IdSubLinea As Integer, _
                                            ByVal prod_codigo As String, _
                                            ByVal prod_nombre As String, _
                                            ByVal PageNumber As Integer, _
                                            ByVal PageSize As Integer, _
                                            ByVal tabla As DataTable) As List(Of Entidades.Campania_Detalle)


        Dim listaCampania_Detalle As New List(Of Entidades.Campania_Detalle)

        Try
            sqlCmd = New SqlCommand
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.CommandText = "_Campania_Detalle_Select"
            sqlCmd.Connection = cn

            sqlCmd.Parameters.AddWithValue("@IdCampania", IdCampania)
            sqlCmd.Parameters.AddWithValue("@IdTipoExistencia", IdTipoExistencia)
            sqlCmd.Parameters.AddWithValue("@IdLinea", IdLinea)
            sqlCmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
            sqlCmd.Parameters.AddWithValue("@prod_Codigo", prod_codigo)
            sqlCmd.Parameters.AddWithValue("@prod_Nombre", prod_nombre)
            sqlCmd.Parameters.AddWithValue("@PageNumber", PageNumber)
            sqlCmd.Parameters.AddWithValue("@PageSize", PageSize)
            sqlCmd.Parameters.AddWithValue("@Tabla", tabla)



            cn.Open()

            reader = sqlCmd.ExecuteReader(CommandBehavior.CloseConnection)

            While (reader.Read)

                Dim objCampania_Detalle As New Entidades.Campania_Detalle
                With objCampania_Detalle

                    .IdCampania = objDaoMantenedor.UCInt(reader("IdCampania"))
                    .IdCampaniaDetalle = objDaoMantenedor.UCInt(reader("IdCampaniaDetalle"))
                    .IdProducto = objDaoMantenedor.UCInt(reader("IdProducto"))
                    .IdUnidadMedida = objDaoMantenedor.UCInt(reader("IdUnidadMedida"))
                    .IdMoneda = objDaoMantenedor.UCInt(reader("IdMoneda"))
                    .Precio = objDaoMantenedor.UCDec(reader("cdet_Precio"))
                    .CantidadMin = objDaoMantenedor.UCDec(reader("cdet_CantidadMin"))
                    .Moneda_Campania = objDaoMantenedor.UCStr(reader("Moneda"))
                    .UnidadMedida = objDaoMantenedor.UCStr(reader("UnidadMedida"))
                    .CodigoProducto = objDaoMantenedor.UCStr(reader("prod_Codigo"))
                    .Producto = objDaoMantenedor.UCStr(reader("prod_Nombre"))
                    .Cadena_IdUnidadMedida_Prod = objDaoMantenedor.UCStr(reader("Cadena_IdUnidadMedida_Prod"))
                    .Cadena_UnidadMedida_Prod = objDaoMantenedor.UCStr(reader("Cadena_UnidadMedida_Prod"))

                End With
                listaCampania_Detalle.Add(objCampania_Detalle)

            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return listaCampania_Detalle

    End Function


    Public Function Campania_Detalle_Vigente_SelectxParams_DT(ByVal IdCampania As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdProducto As Integer, ByVal IdUnidadMedida As Integer, ByVal Fecha As Date) As DataTable

        Dim ds As New DataSet

        Try

            Dim p() As SqlParameter = New SqlParameter(5) {}
            p(0) = objDaoMantenedor.getParam(IdCampania, "@IdCampania", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(IdProducto, "@IdProducto", SqlDbType.Int)
            p(4) = objDaoMantenedor.getParam(Fecha, "@Fecha", SqlDbType.Date)
            p(5) = objDaoMantenedor.getParam(IdUnidadMedida, "@IdUnidadMedida", SqlDbType.Int)

            Dim cmd As New SqlCommand("_Campania_Detalle_Vigente_SelectxParams", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Campania_Producto")

        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return ds.Tables("DT_Campania_Producto")

    End Function

    Public Function Campania_Detalle_Vigente_SelectxParams_DT2(ByVal IdCampania As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdProducto As Integer, ByVal IdUnidadMedida As Integer, ByVal Fecha As Date) As DataTable

        Dim ds As New DataSet

        Try

            Dim p() As SqlParameter = New SqlParameter(5) {}
            p(0) = objDaoMantenedor.getParam(IdCampania, "@IdCampania", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(IdProducto, "@IdProducto", SqlDbType.Int)
            p(4) = objDaoMantenedor.getParam(Fecha, "@Fecha", SqlDbType.Date)
            p(5) = objDaoMantenedor.getParam(IdUnidadMedida, "@IdUnidadMedida", SqlDbType.Int)

            Dim cmd As New SqlCommand("_Campania_Detalle_Vigente_SelectxParams", cn2)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Campania_Producto")

        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return ds.Tables("DT_Campania_Producto")

    End Function

    Public Function Campania_Detalle_Vigente(ByVal IdCampania As Integer, ByVal IdProducto As Integer, ByVal IdUnidadMedida As Integer) As Entidades.Campania_Detalle

        Dim objCampania_Detalle As Entidades.Campania_Detalle = Nothing

        Try

            Dim p() As SqlParameter = New SqlParameter(5) {}
            p(0) = objDaoMantenedor.getParam(IdCampania, "@IdCampania", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(Nothing, "@IdEmpresa", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(Nothing, "@IdTienda", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(IdProducto, "@IdProducto", SqlDbType.Int)
            p(4) = objDaoMantenedor.getParam(Nothing, "@Fecha", SqlDbType.Date)
            p(5) = objDaoMantenedor.getParam(IdUnidadMedida, "@IdUnidadMedida", SqlDbType.Int)

            cn.Open()
            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_Campania_Detalle_Vigente_SelectxParams", p)

            If (reader.Read) Then
                objCampania_Detalle = New Entidades.Campania_Detalle
                With objCampania_Detalle
                    .IdCampaniaDetalle = objDaoMantenedor.UCInt(reader("IdCampaniaDetalle"))
                    .IdCampania = objDaoMantenedor.UCInt(reader("IdCampania"))
                    .IdProducto = objDaoMantenedor.UCInt(reader("IdProducto"))
                    .IdUnidadMedida = objDaoMantenedor.UCInt(reader("IdUnidadMedida"))
                    .IdMoneda = objDaoMantenedor.UCInt(reader("IdMoneda"))
                    .Precio = objDaoMantenedor.UCDec(reader("PrecioCampania"))
                    .CantidadMin = objDaoMantenedor.UCInt(reader("CantidadMin"))
                    .Moneda_Campania = objDaoMantenedor.UCStr(reader("Moneda"))
                    .UnidadMedida = objDaoMantenedor.UCStr(reader("UnidadMedida"))
                    '******************.idemp= objDaoMantenedor.UCInt(reader("IdEmpresa"))
                    '****************.idt= objDaoMantenedor.UCInt(reader("IdTienda"))
                    '*******************.cam= objDaoMantenedor.UCInt(reader("Campania"))
                    '**************.IdCampania = objDaoMantenedor.UCInt(reader("FechaInicio"))
                    '****************.IdCampania = objDaoMantenedor.UCInt(reader("FechaFin"))
                    '***************.IdCampania = objDaoMantenedor.UCInt(reader("Estado"))
                    .Producto = objDaoMantenedor.UCStr(reader("Producto"))
                    .CodigoProducto = objDaoMantenedor.UCStr(reader("CodigoProducto"))
                End With
            End If

            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objCampania_Detalle

    End Function

    Public Function Campania_Detalle_SelectxIdCampania(ByVal IdCampania As Integer) As List(Of Entidades.Campania_Detalle)

        Dim listaCampania_Detalle As New List(Of Entidades.Campania_Detalle)

        Try

            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = objDaoMantenedor.getParam(IdCampania, "@IdCampania", SqlDbType.Int)

            cn.Open()
            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_Campania_Detalle_SelectxIdCampania", p)

            While (reader.Read)

                Dim objCampania_Detalle As New Entidades.Campania_Detalle
                With objCampania_Detalle

                    .IdCampania = objDaoMantenedor.UCInt(reader("IdCampania"))
                    .IdProducto = objDaoMantenedor.UCInt(reader("IdProducto"))
                    .IdUnidadMedida = objDaoMantenedor.UCInt(reader("IdUnidadMedida"))
                    .IdMoneda = objDaoMantenedor.UCInt(reader("IdMoneda"))
                    .Precio = objDaoMantenedor.UCDec(reader("PrecioCampania"))
                    .CantidadMin = objDaoMantenedor.UCDec(reader("CantidadMinima_Campania"))
                    .Moneda_Campania = objDaoMantenedor.UCStr(reader("Moneda_Campania"))
                    .UnidadMedida = objDaoMantenedor.UCStr(reader("UnidadMedida"))
                    .CodigoProducto = objDaoMantenedor.UCStr(reader("CodigoProducto"))
                    .Producto = objDaoMantenedor.UCStr(reader("Producto"))

                    .PrecioActual = objDaoMantenedor.UCDec(reader("PrecioActual"))
                    .Moneda_PrecioActual = objDaoMantenedor.UCStr(reader("Moneda_PrecioActual"))
                    .Cadena_IdUnidadMedida_Prod = objDaoMantenedor.UCStr(reader("Cadena_IdUnidadMedida_Prod"))
                    .Cadena_UnidadMedida_Prod = objDaoMantenedor.UCStr(reader("Cadena_UnidadMedida_Prod"))

                End With
                listaCampania_Detalle.Add(objCampania_Detalle)

            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return listaCampania_Detalle

    End Function

    Public Function Campania_SelectProducto_AddDetallexParams(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal Tabla_IdProducto As DataTable, ByVal IdTienda As Integer, ByVal IdTipoPV As Integer) As List(Of Entidades.Campania_Detalle)

        Dim listaCampania_Detalle As New List(Of Entidades.Campania_Detalle)

        Try

            Dim p() As SqlParameter = New SqlParameter(4) {}
            p(0) = objDaoMantenedor.getParam(IdLinea, "@IdLinea", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdSubLinea, "@IdSubLinea", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(IdTipoPV, "@IdTipoPV", SqlDbType.Int)
            p(4) = New SqlParameter("@Tabla_IdProducto", Tabla_IdProducto)

            cn.Open()

            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_Campania_SelectProducto_AddDetallexParams", p)

            While (reader.Read)

                Dim objCampania_Detalle As New Entidades.Campania_Detalle

                With objCampania_Detalle

                    .IdProducto = objDaoMantenedor.UCInt(reader("IdProducto"))
                    .CodigoProducto = objDaoMantenedor.UCStr(reader("CodigoProducto"))
                    .Producto = objDaoMantenedor.UCStr(reader("Producto"))
                    .PrecioActual = objDaoMantenedor.UCDec(reader("PrecioActual"))
                    .Moneda_PrecioActual = objDaoMantenedor.UCStr(reader("Moneda_PrecioActual"))
                    .Cadena_IdUnidadMedida_Prod = objDaoMantenedor.UCStr(reader("Cadena_IdUnidadMedida_Prod"))
                    .Cadena_UnidadMedida_Prod = objDaoMantenedor.UCStr(reader("Cadena_UnidadMedida_Prod"))

                End With

                listaCampania_Detalle.Add(objCampania_Detalle)

            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return listaCampania_Detalle

    End Function

    Public Sub Campania_Detalle_Registrar(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal objCampania_Detalle As Entidades.Campania_Detalle)

        Dim p() As SqlParameter = New SqlParameter(5) {}

        p(0) = objDaoMantenedor.getParam(objCampania_Detalle.IdCampania, "@IdCampania", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(objCampania_Detalle.IdProducto, "@IdProducto", SqlDbType.Int)
        p(2) = objDaoMantenedor.getParam(objCampania_Detalle.IdUnidadMedida, "@IdUnidadMedida", SqlDbType.Int)
        p(3) = objDaoMantenedor.getParam(objCampania_Detalle.IdMoneda, "@IdMoneda", SqlDbType.Int)
        p(4) = objDaoMantenedor.getParam(objCampania_Detalle.Precio, "@cdet_Precio", SqlDbType.Decimal)
        p(5) = objDaoMantenedor.getParam(objCampania_Detalle.CantidadMin, "@cdet_CantidadMin", SqlDbType.Decimal)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_Campania_Detalle_Registrar", p)

    End Sub

    Public Sub Campania_Detalle_DeletexParams(ByVal IdCampania As Integer, ByVal IdProducto As Integer, ByVal IdUnidadMedida As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)


        Dim p() As SqlParameter = New SqlParameter(2) {}

        p(0) = objDaoMantenedor.getParam(IdCampania, "@IdCampania", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(IdProducto, "@IdProducto", SqlDbType.Int)
        p(2) = objDaoMantenedor.getParam(IdUnidadMedida, "@IdUnidadMedida", SqlDbType.Int)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_Campania_Detalle_DeletexParams", p)

    End Sub

End Class
