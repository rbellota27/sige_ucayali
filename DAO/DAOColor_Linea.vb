﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOColor_Linea
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectAllActivoxIdLineaIdTipoExistencia(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.Color_Linea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ColorLineaSelectAllActivoxIdLineaIdTipoExistencia", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idtipoexistencia)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Color_Linea)
                Do While lector.Read
                    Dim objColorLinea As New Entidades.Color_Linea
                    objColorLinea.Id = CInt((lector.Item("IdColor")))
                    objColorLinea.Nombre = CStr(lector.Item("NomColor"))
                    objColorLinea.NombreCorto = CStr(lector.Item("NomCorto"))
                    objColorLinea.IdLinea = CInt(lector.Item("IdLinea"))
                    objColorLinea.IdTipoExistencia = CInt(lector.Item("IdTipoExistencia"))
                    objColorLinea.CLEstado = CBool(lector.Item("coll_Estado"))
                    Lista.Add(objColorLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function GrabaColorLineaT(ByVal IdLinea As Integer, ByVal cn As SqlConnection, ByVal listaColorLinea As List(Of Entidades.Color_Linea), ByVal T As SqlTransaction) As Boolean
        Dim cmd As SqlCommand
        Try
            For i As Integer = 0 To listaColorLinea.Count - 1
                If T IsNot Nothing Then
                    cmd = New SqlCommand("InsUpd_ColorLinea", cn, T)
                Else
                    cmd = New SqlCommand("InsUpd_ColorLinea", cn)
                End If
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
                cmd.Parameters.AddWithValue("@IdColor", listaColorLinea.Item(i).Id)
                cmd.Parameters.AddWithValue("@IdTipoExistencia", listaColorLinea.Item(i).IdTipoExistencia)
                cmd.Parameters.AddWithValue("@coll_Estado", listaColorLinea.Item(i).CLEstado)
                cmd.Parameters.AddWithValue("@coll_Orden", listaColorLinea.Item(i).Orden)
                Dim cont As Integer = cmd.ExecuteNonQuery
                If cmd.ExecuteNonQuery = 0 Then
                    Return False
                End If
            Next
            Return True
        Catch ex As Exception
            Return False
        Finally

        End Try
    End Function
    Public Function SelectAllActivoxIdLineaIdTipoExistenciaV2(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.Color_Linea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ColorLineaSelectAllActivoxIdLineaIdTipoExistenciaV2", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idtipoexistencia)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Color_Linea)
                Do While lector.Read
                    Dim objColorLinea As New Entidades.Color_Linea
                    objColorLinea.IdColorS = CStr((lector.Item("IdColor")))
                    objColorLinea.Nombre = CStr(lector.Item("NomColor"))
                    objColorLinea.NombreCorto = CStr(lector.Item("NomCorto"))
                    objColorLinea.IdLinea = CInt(lector.Item("IdLinea"))
                    objColorLinea.IdTipoExistencia = CInt(lector.Item("IdTipoExistencia"))
                    objColorLinea.CLEstado = CBool(lector.Item("coll_Estado"))
                    Lista.Add(objColorLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
