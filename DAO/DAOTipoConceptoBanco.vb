﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOTipoConceptoBanco

    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

    Public Function InsertaTipoConceptoBanco(ByVal TipoConceptoBanco As Entidades.TipoConceptoBanco) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        'Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@tcba_Descripcion", SqlDbType.VarChar)
        ArrayParametros(0).Value = TipoConceptoBanco.Descripcion
        ArrayParametros(1) = New SqlParameter("@tcba_Factor", SqlDbType.Int)
        ArrayParametros(1).Value = TipoConceptoBanco.Factor
        ArrayParametros(2) = New SqlParameter("@tcba_Estado", SqlDbType.Bit)
        ArrayParametros(2).Value = TipoConceptoBanco.Estado
        'ArrayParametros(3) = New SqlParameter("@IdTipoConceptoBanco", SqlDbType.Int)
        'ArrayParametros(3).Direction = ParameterDirection.Output

        Return HDAO.Insert(cn, "_TipoConceptoBancoInsert", ArrayParametros)

    End Function
    Public Function ActualizaTipoConceptoBanco(ByVal TipoConceptoBanco As Entidades.TipoConceptoBanco) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@IdTipoConceptoBanco", SqlDbType.Int)
        ArrayParametros(0).Value = TipoConceptoBanco.Id
        ArrayParametros(1) = New SqlParameter("@tcba_Descripcion", SqlDbType.VarChar)
        ArrayParametros(1).Value = TipoConceptoBanco.Descripcion
        ArrayParametros(2) = New SqlParameter("@tcba_Factor", SqlDbType.Int)
        ArrayParametros(2).Value = TipoConceptoBanco.Factor
        ArrayParametros(3) = New SqlParameter("@tcba_Estado", SqlDbType.Bit)
        ArrayParametros(3).Value = TipoConceptoBanco.Estado

        Return HDAO.Update(cn, "_TipoConceptoBancoUpdate", ArrayParametros)

    End Function
    Public Function SelectAll() As List(Of Entidades.TipoConceptoBanco)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoConceptoBancoSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoConceptoBanco)
                Do While lector.Read
                    Dim TipoConceptoBanco As New Entidades.TipoConceptoBanco
                    TipoConceptoBanco.Id = CInt(lector.Item("IdTipoConceptoBanco"))
                    TipoConceptoBanco.Descripcion = CStr(lector.Item("tcba_Descripcion"))
                    TipoConceptoBanco.Factor = CInt(lector.Item("tcba_Factor"))
                    TipoConceptoBanco.Estado = CBool(lector.Item("tcba_Estado"))
                    Lista.Add(TipoConceptoBanco)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.TipoConceptoBanco)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoConceptoBancoSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoConceptoBanco)
                Do While lector.Read
                    Dim TipoConceptoBanco As New Entidades.TipoConceptoBanco
                    TipoConceptoBanco.Id = CInt(lector.Item("IdTipoConceptoBanco"))
                    TipoConceptoBanco.Descripcion = CStr(lector.Item("tcba_Descripcion"))
                    TipoConceptoBanco.Factor = CInt(lector.Item("tcba_Factor"))
                    TipoConceptoBanco.Estado = CBool(lector.Item("tcba_Estado"))
                    Lista.Add(TipoConceptoBanco)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.TipoConceptoBanco)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoConceptoBancoSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoConceptoBanco)
                Do While lector.Read
                    Dim TipoConceptoBanco As New Entidades.TipoConceptoBanco
                    TipoConceptoBanco.Id = CInt(lector.Item("IdTipoConceptoBanco"))
                    TipoConceptoBanco.Descripcion = CStr(lector.Item("tcba_Descripcion"))
                    TipoConceptoBanco.Factor = CInt(lector.Item("tcba_Factor"))
                    TipoConceptoBanco.Estado = CBool(lector.Item("tcba_Estado"))
                    Lista.Add(TipoConceptoBanco)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal idTipoConceptoBanco As Integer) As List(Of Entidades.TipoConceptoBanco)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoConceptoBancoSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoConceptoBanco", idTipoConceptoBanco)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoConceptoBanco)
                Do While lector.Read
                    Dim TipoConceptoBanco As New Entidades.TipoConceptoBanco
                    TipoConceptoBanco.Id = CInt(lector.Item("IdTipoConceptoBanco"))
                    TipoConceptoBanco.Descripcion = CStr(lector.Item("tcba_Descripcion"))
                    TipoConceptoBanco.Factor = CInt(lector.Item("tcba_factor"))
                    TipoConceptoBanco.Estado = CBool(lector.Item("tcba_Estado"))
                    Lista.Add(TipoConceptoBanco)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.TipoConceptoBanco)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoConceptoBancoSelectAllXNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoConceptoBanco)
                Do While lector.Read
                    Dim TipoConceptoBanco As New Entidades.TipoConceptoBanco
                    TipoConceptoBanco.Id = CInt(lector.Item("IdTipoConceptoBanco"))
                    TipoConceptoBanco.Descripcion = CStr(lector.Item("tcba_Descripcion"))
                    TipoConceptoBanco.Factor = CInt(lector.Item("tcba_factor"))
                    TipoConceptoBanco.Estado = CBool(lector.Item("tcba_Estado"))
                    Lista.Add(TipoConceptoBanco)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoConceptoBanco)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoConceptoBancoSelectXNombreXActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoConceptoBanco)
                Do While lector.Read
                    Dim TipoConceptoBanco As New Entidades.TipoConceptoBanco
                    TipoConceptoBanco.Id = CInt(lector.Item("IdTipoConceptoBanco"))
                    TipoConceptoBanco.Descripcion = CStr(lector.Item("tcba_Descripcion"))
                    TipoConceptoBanco.Factor = CInt(lector.Item("tcba_factor"))
                    TipoConceptoBanco.Estado = CBool(lector.Item("tcba_Estado"))
                    Lista.Add(TipoConceptoBanco)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoConceptoBanco)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoConceptoBancoSelectXNombreXInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoConceptoBanco)
                Do While lector.Read
                    Dim TipoConceptoBanco As New Entidades.TipoConceptoBanco
                    TipoConceptoBanco.Id = CInt(lector.Item("IdTipoConceptoBanco"))
                    TipoConceptoBanco.Descripcion = CStr(lector.Item("tcba_Descripcion"))
                    TipoConceptoBanco.Factor = CInt(lector.Item("tcba_factor"))
                    TipoConceptoBanco.Estado = CBool(lector.Item("tcba_Estado"))
                    Lista.Add(TipoConceptoBanco)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxFactor(ByVal Factor As Integer) As List(Of Entidades.TipoConceptoBanco)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoConceptoBancoSelectxFactor", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@tcba_Factor", Factor)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoConceptoBanco)
                Do While lector.Read
                    Dim TipoConceptoBanco As New Entidades.TipoConceptoBanco
                    TipoConceptoBanco.Id = CInt(lector.Item("IdTipoConceptoBanco"))
                    TipoConceptoBanco.Descripcion = CStr(lector.Item("tcba_Descripcion"))
                    TipoConceptoBanco.Factor = CInt(lector.Item("tcba_Factor"))
                    TipoConceptoBanco.Estado = CBool(lector.Item("tcba_Estado"))
                    Lista.Add(TipoConceptoBanco)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCbo() As List(Of Entidades.TipoConceptoBanco)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoConceptoBancoSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoConceptoBanco)
                Do While lector.Read
                    Dim TipoConceptoBanco As New Entidades.TipoConceptoBanco
                    TipoConceptoBanco.Id = CInt(lector.Item("ID"))
                    TipoConceptoBanco.Descripcion = CStr(lector.Item("Nombre"))
                    Lista.Add(TipoConceptoBanco)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

End Class

