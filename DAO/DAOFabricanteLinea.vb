﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOFabricanteLinea
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectAllActivoxIdLineaIdTipoExistencia(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal idpais As String) As List(Of Entidades.FabricanteLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_FabricanteLineaSelectAllActivoxIdLineaIdTipoExistencia", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idtipoexistencia)
        cmd.Parameters.AddWithValue("@IdPais", idpais)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.FabricanteLinea)
                Do While lector.Read
                    Dim objFabLinea As New Entidades.FabricanteLinea
                    objFabLinea.IdFabricante = CInt(lector.Item("IdFabricante"))
                    objFabLinea.NomFabricante = CStr(IIf(IsDBNull(lector.Item("NomFabricante")) = True, "", lector.Item("NomFabricante")))
                    objFabLinea.IdLinea = CInt(lector.Item("IdLinea"))
                    objFabLinea.IdTipoExistencia = CInt(IIf(IsDBNull(lector.Item("IdTipoExistencia")) = True, "", lector.Item("IdTipoExistencia")))
                    objFabLinea.Estado = CBool(IIf(IsDBNull(lector.Item("fabl_Estado")) = True, "", lector.Item("fabl_Estado")))
                    Lista.Add(objFabLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAllActivoxIdLineaIdTipoExistenciav2(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal idpais As String) As List(Of Entidades.FabricanteLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_FabricanteLineaSelectAllActivoxIdLineaIdTipoExistenciav2", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idtipoexistencia)
        cmd.Parameters.AddWithValue("@IdPais", idpais)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.FabricanteLinea)
                Do While lector.Read
                    Dim objFabLinea As New Entidades.FabricanteLinea
                    objFabLinea.IdFabricanteOrden = CStr(lector.Item("IdFabricante"))
                    objFabLinea.NomFabricante = CStr(IIf(IsDBNull(lector.Item("NomFabricante")) = True, "", lector.Item("NomFabricante")))
                    Lista.Add(objFabLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function GrabaFabricanteLineaT(ByVal IdLinea As Integer, ByVal cn As SqlConnection, ByVal lfabricantelinea As List(Of Entidades.FabricanteLinea), ByVal T As SqlTransaction) As Boolean
        Dim cmd As SqlCommand
        Try
            For i As Integer = 0 To lfabricantelinea.Count - 1
                If T IsNot Nothing Then
                    cmd = New SqlCommand("InsUpd_FabricanteLinea", cn, T)
                Else
                    cmd = New SqlCommand("InsUpd_FabricanteLinea", cn)
                End If
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
                cmd.Parameters.AddWithValue("@IdFabricante", lfabricantelinea.Item(i).IdFabricante)
                cmd.Parameters.AddWithValue("@IdTipoExistencia", lfabricantelinea.Item(i).IdTipoExistencia)
                cmd.Parameters.AddWithValue("@fablEstado", lfabricantelinea.Item(i).Estado)
                cmd.Parameters.AddWithValue("@fablOrden", lfabricantelinea.Item(i).Orden)
                Dim cont As Integer = cmd.ExecuteNonQuery
                If cmd.ExecuteNonQuery = 0 Then
                    Return False
                End If
            Next
            Return True
        Catch ex As Exception
            Return False
        Finally

        End Try
    End Function

    Public Function SelectAllActivoxIdLineaIdTipoExistencia(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.FabricanteLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_FabricanteLineaSelectAllActivoxIdLineaIdTipoExistenciaV3", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idtipoexistencia)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.FabricanteLinea)
                Do While lector.Read
                    Dim objFabLinea As New Entidades.FabricanteLinea
                    objFabLinea.IdFabricante = CInt(lector.Item("IdFabricante"))
                    objFabLinea.NomFabricante = CStr(IIf(IsDBNull(lector.Item("NomFabricante")) = True, "", lector.Item("NomFabricante")))
                    objFabLinea.IdLinea = CInt(lector.Item("IdLinea"))
                    objFabLinea.IdTipoExistencia = CInt(IIf(IsDBNull(lector.Item("IdTipoExistencia")) = True, "", lector.Item("IdTipoExistencia")))
                    objFabLinea.Estado = CBool(IIf(IsDBNull(lector.Item("fabl_Estado")) = True, "", lector.Item("fabl_Estado")))
                    Lista.Add(objFabLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
