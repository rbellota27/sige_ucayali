'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAODocumentoI
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

#Region "Otros"



    Public Sub InsertaDocumentoI(ByVal cn As SqlConnection, ByVal LDoi As List(Of Entidades.DocumentoI), ByVal T As SqlTransaction)
        Dim DocumentoI As New Entidades.DocumentoI
        For Each DocumentoI In LDoi
            Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
            'ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            'ArrayParametros(0).Value = DocumentoI.IdDocumento
            ArrayParametros(0) = New SqlParameter("@doc_Numero", SqlDbType.VarChar)
            ArrayParametros(0).Value = DocumentoI.Numero
            ArrayParametros(1) = New SqlParameter("@IdTipoDocumentoI", SqlDbType.Int)
            ArrayParametros(1).Value = DocumentoI.IdTipoDocumentoI
            ArrayParametros(2) = New SqlParameter("@IdPersona", SqlDbType.Int)
            ArrayParametros(2).Value = DocumentoI.IdPersona
            HDAO.InsertaT(cn, "_DocumentoIInsert", ArrayParametros, T)

        Next
    End Sub

    Public Function ActualizaDocumentoI(ByVal documentoi As Entidades.DocumentoI) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = documentoi.IdDocumento
        ArrayParametros(1) = New SqlParameter("@doc_Numero", SqlDbType.VarChar)
        ArrayParametros(1).Value = documentoi.Numero
        ArrayParametros(2) = New SqlParameter("@IdTipoDocumentoI", SqlDbType.Int)
        ArrayParametros(2).Value = documentoi.IdTipoDocumentoI
        ArrayParametros(3) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(3).Value = documentoi.IdPersona
        Return HDAO.Update(cn, "_DocumentoIUpdate", ArrayParametros)
    End Function

    Public Function SelectxIdPersona(ByVal IdPersona As Integer) As Entidades.DocumentoI
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoISelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Using cn
            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
            Dim objDocumentoI As New Entidades.DocumentoI
            lector.Read()
            If lector.HasRows = False Then
                objDocumentoI.Numero = ""
                Return objDocumentoI
            End If
            objDocumentoI = New Entidades.DocumentoI
            objDocumentoI.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
            objDocumentoI.IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumentoI")) = True, 0, lector.Item("IdDocumentoI")))
            objDocumentoI.Numero = CStr(IIf(IsDBNull(lector.Item("doc_Numero")) = True, "", lector.Item("doc_Numero")))
            objDocumentoI.IdTipoDocumentoI = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumentoI")) = True, 0, lector.Item("IdTipoDocumentoI")))
            lector.Close()
            Return objDocumentoI
        End Using
    End Function


    Public Sub ActualizaDocumentoIT(ByVal cn As SqlConnection, ByVal Ldoi As List(Of Entidades.DocumentoI), ByVal T As SqlTransaction)
        Dim DocumentoI As New Entidades.DocumentoI
        For Each DocumentoI In Ldoi
            Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
            ArrayParametros(0) = New SqlParameter("@IdPersona", SqlDbType.Int)
            ArrayParametros(0).Value = IIf(DocumentoI.IdPersona = Nothing, DBNull.Value, DocumentoI.IdPersona)
            ArrayParametros(1) = New SqlParameter("@IdDocumentoI", SqlDbType.Int)
            ArrayParametros(1).Value = IIf(DocumentoI.IdDocumento = Nothing, DBNull.Value, DocumentoI.IdDocumento)
            ArrayParametros(2) = New SqlParameter("@IdTipoDocumentoI", SqlDbType.Int)
            ArrayParametros(2).Value = IIf(DocumentoI.IdTipoDocumentoI = Nothing, DBNull.Value, DocumentoI.IdTipoDocumentoI)
            ArrayParametros(3) = New SqlParameter("@doc_Numero", SqlDbType.VarChar)
            ArrayParametros(3).Value = IIf(DocumentoI.Numero = Nothing, DBNull.Value, DocumentoI.Numero)

            HDAO.UpdateT(cn, "_DocumentoIUpdate", ArrayParametros, T)
        Next
    End Sub
    Public Function DeletexIdPersonaT(ByVal cn As SqlConnection, ByVal IdPersona As Integer, ByVal T As SqlTransaction) As Boolean
        Dim cmd As New SqlCommand("_DocumentoIDeletexIdPersona", cn, T)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Try
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
#End Region

#Region "mantenimineto Persona"

    Public Function listarTipoDocumento() As List(Of Entidades.TipoDocumentoI)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                Dim cmd As New SqlCommand("_listarTipoDocumento", cn)
                cmd.CommandType = CommandType.StoredProcedure
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDocumentoI)
                Do While lector.Read
                    Dim obj As New Entidades.TipoDocumentoI
                    obj.Id = CInt(lector("id"))
                    obj.Abv = CStr(IIf(IsDBNull(lector("descripcion")), "", lector("descripcion")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function Selectx_IdPersona(ByVal IdPersona As Integer) As List(Of Entidades.DocumentoI)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoISelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Try
            Dim lector As SqlDataReader
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DocumentoI)

                Do While lector.Read
                    Dim objDocumentoI As New Entidades.DocumentoI
                    objDocumentoI = New Entidades.DocumentoI
                    objDocumentoI.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    objDocumentoI.IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumentoI")) = True, 0, lector.Item("IdDocumentoI")))
                    objDocumentoI.Numero = CStr(IIf(IsDBNull(lector.Item("doc_Numero")) = True, "", lector.Item("doc_Numero")))
                    objDocumentoI.IdTipoDocumentoI = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumentoI")) = True, 0, lector.Item("IdTipoDocumentoI")))
                    Lista.Add(objDocumentoI)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

#End Region

End Class
