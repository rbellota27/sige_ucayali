'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'*************************   MIERCOLES 02 06 2010









Imports System.Data.SqlClient

Public Class DAOProductoUM
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

    
    Public Function SelectxIdProductoxIdUM(ByVal idproducto As Integer, ByVal idunidadmedida As Integer) As Entidades.ProductoUM
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoUMSelectxIdProductoxIdUM", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", idproducto)
        cmd.Parameters.AddWithValue("@IdUnidadMedida", idunidadmedida)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim objPUM As Entidades.ProductoUM = Nothing

                If lector.Read Then

                    objPUM = New Entidades.ProductoUM

                    With objPUM

                        .IdProducto = CInt(IIf(IsDBNull(lector("IdProducto")) = True, 0, lector("IdProducto")))
                        .IdUnidadMedida = CInt(IIf(IsDBNull(lector("IdUnidadMedida")) = True, 0, lector("IdUnidadMedida")))
                        .UnidadPrincipal = CBool(IIf(IsDBNull(lector("pum_UnidadPrincipal")) = True, False, lector("pum_UnidadPrincipal")))
                        .Equivalencia = CDec(IIf(IsDBNull(lector("pum_Equivalencia")) = True, 0, lector("pum_Equivalencia")))
                        .Retazo = CBool(IIf(IsDBNull(lector("pum_Retazo")) = True, False, lector("pum_Retazo")))
                        .Estado = CStr(IIf(IsDBNull(lector("pum_Estado")) = True, "0", lector("pum_Estado")))
                        .PorcentRetazo = CDec(IIf(IsDBNull(lector("pum_PorcentRetazo")) = True, 0, lector("pum_PorcentRetazo")))
                        .NomUnidadMedida = CStr(IIf(IsDBNull(lector("um_NombreCorto")) = True, "", lector("um_NombreCorto")))


                    End With

                End If
                lector.Close()
                Return objPUM
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function InsertaListaProductoUM(idusuario As Integer, ByVal IdProducto As Integer, ByVal lista As List(Of Entidades.ProductoUMView), ByVal cn As SqlConnection, Optional ByVal tr As SqlTransaction = Nothing) As Boolean
        'recibo la conexion abierta
        Dim cmd As SqlCommand
        Try
            For i As Integer = 0 To lista.Count - 1
                If tr IsNot Nothing Then
                    cmd = New SqlCommand("_ProductoUMInsert", cn, tr)
                Else
                    cmd = New SqlCommand("_ProductoUMInsert", cn)
                End If
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
                cmd.Parameters.AddWithValue("@IdUnidadMedida", lista.Item(i).IdUnidadMedida)
                cmd.Parameters.AddWithValue("@pum_UnidadPrincipal", lista.Item(i).UnidadPrincipal)
                cmd.Parameters.AddWithValue("@pum_Equivalencia", lista.Item(i).Equivalencia)
                cmd.Parameters.AddWithValue("@pum_Retazo", lista.Item(i).Retazo)
                cmd.Parameters.AddWithValue("@pum_Estado", lista.Item(i).Estado)
                cmd.Parameters.AddWithValue("@pum_codbarraxUm", lista.Item(i).CodBarraXUm)
                cmd.Parameters.AddWithValue("@idUsuario", idusuario)

                Dim cont As Integer = cmd.ExecuteNonQuery

                If cmd.ExecuteNonQuery = 0 Then
                    Return False
                End If
            Next
            Return True
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function
    Public Function DeletexIdProducto(ByVal IdProducto As Integer, ByVal cn As SqlConnection, Optional ByVal tr As SqlTransaction = Nothing) As Boolean
        'recibo la conexion abierta
        Dim cmd As SqlCommand
        Try
            If tr IsNot Nothing Then
                cmd = New SqlCommand("_ProductoUMDeletexIdProducto", cn, tr)
            Else
                cmd = New SqlCommand("_ProductoUMDeletexIdProducto", cn)
            End If
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Function InsertaProductoUM(ByVal productoum As Entidades.ProductoUM) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(0).Value = productoum.IdProducto
        ArrayParametros(1) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
        ArrayParametros(1).Value = productoum.IdUnidadMedida
        ArrayParametros(2) = New SqlParameter("@pum_UnidadPrincipal", SqlDbType.Bit)
        ArrayParametros(2).Value = productoum.UnidadPrincipal
        ArrayParametros(3) = New SqlParameter("@pum_Equivalencia", SqlDbType.Decimal)
        ArrayParametros(3).Value = productoum.Equivalencia
        ArrayParametros(4) = New SqlParameter("@pum_Retazo", SqlDbType.Bit)
        ArrayParametros(4).Value = productoum.Retazo
        Return HDAO.Insert(cn, "_ProductoUMInsert", ArrayParametros)
    End Function
    Public Function ActualizaProductoUM(ByVal productoum As Entidades.ProductoUM) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(0).Value = productoum.IdProducto
        ArrayParametros(1) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
        ArrayParametros(1).Value = productoum.IdUnidadMedida
        ArrayParametros(2) = New SqlParameter("@pum_UnidadPrincipal", SqlDbType.Bit)
        ArrayParametros(2).Value = productoum.UnidadPrincipal
        ArrayParametros(3) = New SqlParameter("@pum_Equivalencia", SqlDbType.Decimal)
        ArrayParametros(3).Value = productoum.Equivalencia
        ArrayParametros(4) = New SqlParameter("@pum_Retazo", SqlDbType.Bit)
        ArrayParametros(4).Value = productoum.Retazo
        Return HDAO.Update(cn, "_ProductoUMUpdate", ArrayParametros)
    End Function
    Public Function SelectxIdProducto(ByVal idProducto As Integer) As List(Of Entidades.ProductoUM)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoUMSelectxIdProducto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", idProducto)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ProductoUM)
                Do While lector.Read
                    Dim objProdUM As New Entidades.ProductoUM
                    objProdUM.Equivalencia = CDec(lector.Item("pum_Equivalencia"))
                    objProdUM.IdProducto = CInt(lector.Item("IdProducto"))
                    objProdUM.IdUnidadMedida = CInt(lector.Item("IdUnidadMedida"))
                    objProdUM.Retazo = CBool(lector.Item("pum_Retazo"))
                    objProdUM.UnidadPrincipal = CBool(lector.Item("pum_UnidadPrincipal"))
                    Lista.Add(objProdUM)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectIdUMPrincipalxIdProducto(ByVal IdProducto As Integer) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoUMSelectIdUMPrincipalxIdProducto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                lector.Read()
                Dim IdUnidadMedida As Integer = CInt(lector.Item("IdUnidadMedida"))
                Return IdUnidadMedida
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectEquivalenciaxIdProductoxIdUnidadMedida(ByVal idProducto As Integer, ByVal IdUnidadMedida As Integer) As Decimal
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoUMSelectEquivalenciaxIdProductoxIdUM", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", idProducto)
        cmd.Parameters.AddWithValue("@IdUnidadMedida", IdUnidadMedida)
        Dim lector As SqlDataReader
        Dim equivalencia As Decimal = 0
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                If lector.Read Then
                    equivalencia = CDec(IIf(IsDBNull(lector.Item("pum_Equivalencia")) = True, 0, lector.Item("pum_Equivalencia")))
                End If
                lector.Close()
                Return equivalencia
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.ProductoUM)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoUMSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ProductoUM)
                Do While lector.Read
                    Dim objProdUM As New Entidades.ProductoUM
                    objProdUM.Equivalencia = CDec(lector.Item("pum_Equivalencia"))
                    objProdUM.IdProducto = CInt(lector.Item("IdProducto"))
                    objProdUM.IdUnidadMedida = CInt(lector.Item("IdUnidadMedida"))
                    objProdUM.Retazo = CBool(lector.Item("pum_Retazo"))
                    objProdUM.UnidadPrincipal = CBool(lector.Item("pum_UnidadPrincipal"))
                    Lista.Add(objProdUM)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function ProductoUMUpdatePorcentRetazo(ByVal idproducto As Integer, ByVal idunidadmedida As Integer, ByVal porcentRetazo As Decimal, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Dim hecho As Boolean = False
        Try
            'recibo la conexion abierta
            Dim cmd As New SqlCommand("_ProductoUMUpdatePorcentAdicionalxIdProductoxIdUnidadMedida", cn, tr)

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdProducto", idproducto)
            cmd.Parameters.AddWithValue("@IdUnidadMedida", idunidadmedida)
            cmd.Parameters.AddWithValue("@PorcentRetazo", porcentRetazo)
            If cmd.ExecuteNonQuery = 0 Then
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Sub DeletexIdProductoxIdUnidadMedida(ByVal IdProducto As Integer, ByVal IdUnidadMedida As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Dim cmd As New SqlCommand("_ProductoUMDeletexIdProductoxIdUnidadMedida", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdUnidadMedida", IdUnidadMedida)
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        If cmd.ExecuteNonQuery() = 0 Then
            Throw New Exception
        End If
    End Sub

   

End Class
