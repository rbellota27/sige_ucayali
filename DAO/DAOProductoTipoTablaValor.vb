﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOProductoTipoTablaValor
    Dim objConexion As New DAO.Conexion
    Dim HDAO As New DAO.HelperDAO
    Public Function GrabaProductoTipoTablaValorT(ByVal IdProducto As Integer, ByVal cn As SqlConnection, ByVal lista As List(Of Entidades.ProductoTipoTablaValor), ByVal T As SqlTransaction) As Boolean
        Dim cmd As SqlCommand
        Try
            For i As Integer = 0 To lista.Count - 1
                If T IsNot Nothing Then
                    cmd = New SqlCommand("_ProductoTipoTablaValorInsert", cn, T)
                Else
                    cmd = New SqlCommand("_ProductoTipoTablaValorInsert", cn)
                End If
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
                cmd.Parameters.AddWithValue("@IdTipoTabla", lista.Item(i).IdTipoTabla)
                cmd.Parameters.AddWithValue("@IdTipoTablaValor", lista.Item(i).IdTipoTablaValor)
                cmd.ExecuteNonQuery()
            Next
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Function DeleteProductoTipoTablaValor(ByVal IdProducto As Integer, ByVal cn As SqlConnection, Optional ByVal tr As SqlTransaction = Nothing) As Boolean
        Dim cmd As SqlCommand
        Try
            If tr IsNot Nothing Then
                cmd = New SqlCommand("_ProductoTipoTablaValorDelete", cn, tr)
            Else
                cmd = New SqlCommand("_ProductoTipoTablaValorDelete", cn)
            End If
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class
