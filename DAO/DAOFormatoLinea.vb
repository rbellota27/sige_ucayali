﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOFormatoLinea
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectAllActivoxIdLineaIdTipoExistencia(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.FormatoLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_FormatoLineaSelectAllActivoxIdLineaIdTipoExistencia", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idtipoexistencia)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.FormatoLinea)
                Do While lector.Read
                    Dim objFormatoLinea As New Entidades.FormatoLinea
                    objFormatoLinea.IdFormato = CInt(IIf(IsDBNull(lector.Item("IdFormato")) = True, 0, lector.Item("IdFormato")))
                    objFormatoLinea.NomFormato = CStr(IIf(IsDBNull(lector.Item("NomFormato")) = True, 0, lector.Item("NomFormato")))
                    objFormatoLinea.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    objFormatoLinea.IdTipoExistencia = CInt(IIf(IsDBNull(lector.Item("IdTipoExistencia")) = True, 0, lector.Item("IdTipoExistencia")))
                    objFormatoLinea.Estado = CBool(IIf(IsDBNull(lector.Item("fl_Estado")) = True, "", lector.Item("fl_Estado")))
                    Lista.Add(objFormatoLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAllActivoxIdLineaIdTipoExistenciav2(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.FormatoLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_FormatoLineaSelectAllActivoxIdLineaIdTipoExistenciav2", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idtipoexistencia)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.FormatoLinea)
                Do While lector.Read
                    Dim objFormatoLinea As New Entidades.FormatoLinea
                    objFormatoLinea.IdFormatoOrden = CStr(IIf(IsDBNull(lector.Item("IdFormato")) = True, "", lector.Item("IdFormato")))
                    objFormatoLinea.NomFormato = CStr(IIf(IsDBNull(lector.Item("NomFormato")) = True, "", lector.Item("NomFormato")))
                    objFormatoLinea.Abv = CStr(IIf(IsDBNull(lector.Item("NomCorto")) = True, "", lector.Item("NomCorto")))
                    Lista.Add(objFormatoLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function GrabaFormatoLineaT(ByVal IdLinea As Integer, ByVal cn As SqlConnection, ByVal listaFormatoLinea As List(Of Entidades.FormatoLinea), ByVal T As SqlTransaction) As Boolean
        Dim cmd As SqlCommand
        Try
            For i As Integer = 0 To listaFormatoLinea.Count - 1
                If T IsNot Nothing Then
                    cmd = New SqlCommand("InsUpd_FormatoLinea", cn, T)
                Else
                    cmd = New SqlCommand("InsUpd_FormatoLinea", cn)
                End If
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
                cmd.Parameters.AddWithValue("@IdFormato", listaFormatoLinea.Item(i).IdFormato)
                cmd.Parameters.AddWithValue("@IdTipoExistencia", listaFormatoLinea.Item(i).IdTipoExistencia)
                cmd.Parameters.AddWithValue("@flEstado", listaFormatoLinea.Item(i).Estado)
                cmd.Parameters.AddWithValue("@flOrden", listaFormatoLinea.Item(i).Orden)
                Dim cont As Integer = cmd.ExecuteNonQuery
                If cmd.ExecuteNonQuery = 0 Then
                    Return False
                End If
            Next
            Return True
        Catch ex As Exception
            Return False
        Finally

        End Try
    End Function
End Class
