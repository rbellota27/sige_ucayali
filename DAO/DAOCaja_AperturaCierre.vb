﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOCaja_AperturaCierre

    Dim objConexion As New DAO.Conexion
    Dim list_Caja_AperturaCierre As List(Of Entidades.Caja_AperturaCierre)
    Dim obj_Caja_AperturaCierre As Entidades.Caja_AperturaCierre


    Public Function Caja_AperturaCierre_Transaction(ByVal objCaja_AperturaCierre As Entidades.Caja_AperturaCierre, ByVal sqlCN As SqlConnection, ByVal sqlTR As SqlTransaction) As Integer
        Dim IdCaja_AperturaCierre As Integer
        Dim Parameter() As SqlParameter = New SqlParameter(9) {}

        Parameter(0) = New SqlParameter("@Id", SqlDbType.Int)
        Parameter(0).Direction = ParameterDirection.Output

        Parameter(1) = New SqlParameter("@IdCaja", SqlDbType.Int)
        Parameter(1).Value = IIf(objCaja_AperturaCierre.IdCaja = Nothing, DBNull.Value, objCaja_AperturaCierre.IdCaja)

        Parameter(2) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        Parameter(2).Value = IIf(objCaja_AperturaCierre.IdUsuario = Nothing, DBNull.Value, objCaja_AperturaCierre.IdUsuario)

        Parameter(3) = New SqlParameter("@cac_FechaApertura", SqlDbType.DateTime)
        Parameter(3).Value = IIf(objCaja_AperturaCierre.cac_FechaApertura Is Nothing, DBNull.Value, objCaja_AperturaCierre.cac_FechaApertura)

        Parameter(4) = New SqlParameter("@cac_ControlCaja", SqlDbType.Char)
        Parameter(4).Value = objCaja_AperturaCierre.cac_ControlCaja

        Parameter(5) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        Parameter(5).Value = IIf(objCaja_AperturaCierre.IdMoneda = Nothing, DBNull.Value, objCaja_AperturaCierre.IdMoneda)

        Parameter(6) = New SqlParameter("@cac_MontoApertura", SqlDbType.Decimal)
        Parameter(6).Value = IIf(objCaja_AperturaCierre.cac_MontoApertura = Nothing, DBNull.Value, objCaja_AperturaCierre.cac_MontoApertura)

        Parameter(7) = New SqlParameter("@cac_MontoCierre", SqlDbType.Decimal)
        Parameter(7).Value = IIf(objCaja_AperturaCierre.cac_MontoCierre = Nothing, DBNull.Value, objCaja_AperturaCierre.cac_MontoCierre)

        Parameter(8) = New SqlParameter("@cac_FechaCierre", SqlDbType.DateTime)
        Parameter(8).Value = IIf(objCaja_AperturaCierre.cac_FechaCierre Is Nothing, DBNull.Value, objCaja_AperturaCierre.cac_FechaCierre)

        Parameter(9) = New SqlParameter("@IdCaja_AperturaCierre", SqlDbType.Int)
        Parameter(9).Value = IIf(objCaja_AperturaCierre.IdCaja_AperturaCierre = Nothing, DBNull.Value, objCaja_AperturaCierre.IdCaja_AperturaCierre)


        Dim sqlCMD As New SqlCommand("_Caja_AperturaCierre_Insert", sqlCN, sqlTR)
        sqlCMD.CommandType = CommandType.StoredProcedure
        sqlCMD.Parameters.AddRange(Parameter)

        Try
            sqlCMD.ExecuteNonQuery()
            IdCaja_AperturaCierre = CInt(sqlCMD.Parameters("@Id").Value)
        Catch ex As Exception
            Throw ex
        Finally            
        End Try

        Return IdCaja_AperturaCierre

    End Function


    Public Function Caja_AperturaCierre_Select(ByVal Id As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdCaja As Integer, ByVal IdUsuario As Integer, Optional ByVal cac_FechaApertura As String = Nothing, Optional ByVal cac_FechaCierre As String = Nothing) As List(Of Entidades.Caja_AperturaCierre)
        list_Caja_AperturaCierre = New List(Of Entidades.Caja_AperturaCierre)
        Dim cn As SqlConnection = (New Conexion).ConexionSIGE
        Dim reader As SqlDataReader = Nothing
        Dim cmd As New SqlCommand("_Caja_AperturaCierre_Select", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", IIf(Id = Nothing, DBNull.Value, Id))
        cmd.Parameters.AddWithValue("@IdCaja", IIf(IdCaja = Nothing, DBNull.Value, IdCaja))
        cmd.Parameters.AddWithValue("@IdEmpresa", IIf(IdEmpresa = Nothing, DBNull.Value, IdEmpresa))
        cmd.Parameters.AddWithValue("@IdTienda", IIf(IdTienda = Nothing, DBNull.Value, IdTienda))
        cmd.Parameters.AddWithValue("@IdUsuario", IIf(IdUsuario = Nothing, DBNull.Value, IdUsuario))
        cmd.Parameters.AddWithValue("@cac_FechaApertura", IIf(cac_FechaApertura = Nothing, DBNull.Value, cac_FechaApertura))
        cmd.Parameters.AddWithValue("@cac_FechaCierre", IIf(cac_FechaCierre = Nothing, DBNull.Value, cac_FechaCierre))

        Try
            cn.Open()
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While reader.Read
                obj_Caja_AperturaCierre = New Entidades.Caja_AperturaCierre
                With obj_Caja_AperturaCierre
                    .IdCaja_AperturaCierre = CInt(IIf(IsDBNull(reader("IdCaja_AperturaCierre")), 0, reader("IdCaja_AperturaCierre")))
                    .IdCaja = CInt(IIf(IsDBNull(reader("IdCaja")), 0, reader("IdCaja")))
                    .caja_Nombre = CStr(IIf(IsDBNull(reader("caja_Nombre")), "", reader("caja_Nombre")))
                    .IdUsuario = CInt(IIf(IsDBNull(reader("IdUsuario")), 0, reader("IdUsuario")))
                    .NomPersona = CStr(IIf(IsDBNull(reader("NomPersona")), "", reader("NomPersona")))
                    .cac_FechaApertura = CDate(IIf(IsDBNull(reader("cac_FechaApertura")), Nothing, reader("cac_FechaApertura")))
                    If IsDate(reader("cac_FechaCierre")) Then .cac_FechaCierre = CDate(reader("cac_FechaCierre"))
                    .IdMoneda = CInt(IIf(IsDBNull(reader("IdMoneda")), 0, reader("IdMoneda")))
                    .mon_Simbolo = CStr(IIf(IsDBNull(reader("mon_Simbolo")), "", reader("mon_Simbolo")))
                    .cac_MontoApertura = CDec(IIf(IsDBNull(reader("cac_MontoApertura")), Decimal.Zero, reader("cac_MontoApertura")))
                    .cac_MontoCierre = CDec(IIf(IsDBNull(reader("cac_MontoCierre")), Decimal.Zero, reader("cac_MontoCierre")))
                    .cac_ControlCaja = CStr(IIf(IsDBNull(reader("cac_ControlCaja")), "", reader("cac_ControlCaja")))
                    .ControlCaja = CStr(IIf(IsDBNull(reader("ControlCaja")), "", reader("ControlCaja")))
                    .TotalCajaEfectivo = CDec(IIf(IsDBNull(reader("TotalCajaEfectivo")), Decimal.Zero, reader("TotalCajaEfectivo")))
                End With
                list_Caja_AperturaCierre.Add(obj_Caja_AperturaCierre)
            Loop
            reader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return list_Caja_AperturaCierre
    End Function

    Public Function Caja_AperturaCierre_SelectEstado(ByVal IdCaja As Integer, ByVal cac_FechaApertura As String) As String
        Dim estado As String = String.Empty
        Dim cn As SqlConnection = (New Conexion).ConexionSIGE
        Dim reader As SqlDataReader = Nothing
        Dim cmd As New SqlCommand("_Caja_AperturaCierre_Estado", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdCaja", IIf(IdCaja = Nothing, DBNull.Value, IdCaja))
        cmd.Parameters.AddWithValue("@cac_FechaApertura", IIf(cac_FechaApertura = Nothing, DBNull.Value, cac_FechaApertura))

        Try
            cn.Open()
            estado = cmd.ExecuteScalar().ToString
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return estado
    End Function

End Class
