﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Imports System.Data

Public Class DAOSerieCheque
    Inherits DAOMantenedor

    Public Function InsertT_GetID(ByVal x As Entidades.SerieCheque, _
 Optional ByRef Cnx As SqlConnection = Nothing, _
 Optional ByRef Trx As SqlTransaction = Nothing) As Integer

        Return QueryT(GetVectorParametros(x, modo_query.Insert), modo_query.Insert, "_SerieChequeInsert", Cnx, Trx)
    End Function
    Public Function InsertT(ByVal x As Entidades.SerieCheque, _
    Optional ByRef Cnx As SqlConnection = Nothing, _
    Optional ByRef Trx As SqlTransaction = Nothing) As Boolean
        If InsertT_GetID(x, Cnx, Trx) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function UpdateT(ByVal x As Entidades.SerieCheque, _
    Optional ByRef Cnx As SqlConnection = Nothing, _
    Optional ByRef Trx As SqlTransaction = Nothing) As Boolean

        Return CBool(QueryT(GetVectorParametros(x, modo_query.Update), modo_query.Update, "_SerieChequeUpdate", Cnx, Trx))

    End Function

    Public Function GetVectorParametros(ByVal x As Entidades.SerieCheque, ByVal op As modo_query) As SqlParameter()

        Dim Prm() As SqlParameter

        Select Case op
            Case modo_query.Show, modo_query.Update, modo_query.Insert

                If op = modo_query.Update Or op = modo_query.Show Then
                    Prm = New SqlParameter(8) {}
                    'Id de la Tabla
                    Prm(8) = New SqlParameter("@IdSerieCheque", SqlDbType.Int)
                    Prm(8).Value = getParamValueInt(x.Id)
                Else
                    Prm = New SqlParameter(7) {}
                End If

                Prm(0) = New SqlParameter("@IdBanco", SqlDbType.Int)
                Prm(0).Value = getParamValueInt(x.IdBanco)

                Prm(1) = New SqlParameter("@IdCuentaBancaria", SqlDbType.Int)
                Prm(1).Value = getParamValueInt(x.IdCuentaBancaria)

                Prm(2) = New SqlParameter("@sc_Serie", SqlDbType.VarChar)
                Prm(2).Value = getParamValueStr(x.Serie)

                Prm(3) = New SqlParameter("@sc_NroInicio", SqlDbType.Int)
                Prm(3).Value = getParamValueInt(x.NroInicio)

                Prm(4) = New SqlParameter("@sc_NroFin", SqlDbType.Int)
                Prm(4).Value = getParamValueInt(x.NroFin)

                Prm(5) = New SqlParameter("@sc_Longitud", SqlDbType.Int)
                Prm(5).Value = getParamValueInt(x.Longitud)

                Prm(6) = New SqlParameter("@sc_Estado", SqlDbType.Bit)
                Prm(6).Value = x.EstadoNullable

                Prm(7) = New SqlParameter("@sc_Descripcion", SqlDbType.VarChar)
                Prm(7).Value = getParamValueStr(x.Descripcion)

                'Case modo_query.Delete
        End Select

        Return Prm
    End Function
End Class
