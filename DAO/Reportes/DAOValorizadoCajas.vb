﻿Imports System.Data.SqlClient
Public Class DAOValorizadoCajas
    Dim conexion As New Conexion


    Public Function DAO_CargarComboSecuencia(ByVal filtro As String, ByVal tabla As String) As DataTable
        Try
            Dim dt As New DataTable
            Dim con As New SqlConnection
            con = conexion.ConexionSIGE
            Dim cmd As New SqlCommand("SP_DROPDOWNLIST_FILTRO", con)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@FITLRO", filtro)
            cmd.Parameters.AddWithValue("@TABLA", tabla)
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            Return dt
        Catch ex As Exception
            Throw ex
        End Try      
    End Function

    Public Function DAO_CargarComboSecuenciaTonos(ByVal filtro As String, ByVal tabla As String, ByVal Cantidad As Decimal) As DataTable
        Try
            Dim dt As New DataTable
            Dim con As New SqlConnection
            con = conexion.ConexionSIGE
            Dim cmd As New SqlCommand("SP_DROPDOWNLIST_FILTRO_TONOS", con)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@FITLRO", filtro)
            cmd.Parameters.AddWithValue("@TABLA", tabla)
            cmd.Parameters.AddWithValue("@Cantidad", Cantidad)
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            Return dt
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DAO_ObtenerDatosGrilla(ByVal idTienda As Integer, ByVal fechaInicio As Date, ByVal fechaFinal As Date, ByVal topNumero As Integer, _
                                           ByVal opcion As Integer, ByVal opcFiltro As Integer, ByVal existencia As Integer, ByVal linea As Integer, _
                                           ByVal sublinea As Integer, ByVal pais As Integer, ByVal idFabrica As String, ByVal idTipoCliente As String) As DataTable
        Try
            Dim dt As New DataTable
            Dim con As New SqlConnection
            con = conexion.ConexionSIGE
            Dim cmd As New SqlCommand("SanicERP_SelectTop_Ventas_valorizado_conVtaCant_Lineas_Cajas", con)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            cmd.Parameters.AddWithValue("@idtienda", idTienda)
            cmd.Parameters.AddWithValue("@fechainicio", fechaInicio)
            cmd.Parameters.AddWithValue("@fechafinal", fechaFinal)
            cmd.Parameters.AddWithValue("@topnumero", topNumero)
            cmd.Parameters.AddWithValue("@option", opcion)
            cmd.Parameters.AddWithValue("@opcfiltro", opcFiltro)
            cmd.Parameters.AddWithValue("@Existencia", existencia)
            cmd.Parameters.AddWithValue("@Linea", linea)
            cmd.Parameters.AddWithValue("@SubLinea", sublinea)
            cmd.Parameters.AddWithValue("@Pais", pais)
            cmd.Parameters.AddWithValue("@idFabrica", idFabrica)
            cmd.Parameters.AddWithValue("@idtipocliente", idTipoCliente)
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            Return dt
        Catch ex As Exception
            Throw ex
        End Try

    End Function
End Class
