﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'**************** '29Marzo 2010 01_00
Imports System.Data.SqlClient
Public Class DAOComparativoVentasAnual
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

    Public Function ComparativoVentasAnual(ByVal year As Integer, ByVal variable As String, ByVal IdPersona As Integer, ByVal idMoneda As Integer, ByVal idpropietario As Integer) As DataSet
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim da As SqlDataAdapter 'abre y cierra la conexion
        Dim ds As New DataSet
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}

        Try
            Dim cmd As New SqlCommand("_CR_ComparativoVentas", cn) 'representa intruccion transact sql o proc almacendo

            ArrayParametros(0) = New SqlParameter("@year", SqlDbType.Int)
            ArrayParametros(0).Value = IIf(year = Nothing, DBNull.Value, year)
            ArrayParametros(1) = New SqlParameter("@variable", SqlDbType.VarChar)
            ArrayParametros(1).Value = IIf(variable = Nothing, DBNull.Value, variable)
            ArrayParametros(2) = New SqlParameter("@IdPersona", SqlDbType.Int)
            ArrayParametros(2).Value = IIf(IdPersona = Nothing, DBNull.Value, IdPersona)
            ArrayParametros(3) = New SqlParameter("@idMoneda", SqlDbType.Int)
            ArrayParametros(3).Value = IIf(idMoneda = Nothing, DBNull.Value, idMoneda)
            ArrayParametros(4) = New SqlParameter("@idpropietario", SqlDbType.Int)
            ArrayParametros(4).Value = IIf(idpropietario = Nothing, DBNull.Value, idpropietario)
            'cmd.Parameters.AddWithValue("@variable", variable)
            'cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
            'cmd.Parameters.AddWithValue("@idMoneda", idMoneda)
            'cmd.Parameters.AddWithValue("@idpropietario", idpropietario)
            'Cargo la data
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(ArrayParametros)
            da = New SqlDataAdapter(cmd)  'lee los datos de la base datos
            da.Fill(ds, "DT_ComparativoVentas") 'creo un alias y guardo los datos del dataadpater en el data set

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function

    Public Function reporteComisiones(ByVal fechainicio As String, ByVal fechafin As String, ByVal empresa As Integer, ByVal tienda As Integer, ByVal perfil As Integer, ByVal rol As Integer, ByVal usuario As Integer) As DataSet

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim da As SqlDataAdapter 'abre y cierra la conexion
        Dim ds As New DataSet
        Dim ArrayParametros() As SqlParameter = New SqlParameter(6) {}
        Try
            Dim cmd As New SqlCommand("CR_Comisiones", cn) 'representa intruccion transact sql o proc almacendo

            ArrayParametros(0) = New SqlParameter("@fechaInicio", SqlDbType.VarChar)
            ArrayParametros(0).Value = IIf(fechainicio = Nothing, DBNull.Value, fechainicio)
            ArrayParametros(1) = New SqlParameter("@fechaFin", SqlDbType.VarChar)
            ArrayParametros(1).Value = IIf(fechafin = Nothing, DBNull.Value, fechafin)
            ArrayParametros(2) = New SqlParameter("@idempresa", SqlDbType.Int)
            ArrayParametros(2).Value = IIf(empresa = Nothing, 0, empresa)
            ArrayParametros(3) = New SqlParameter("@idtienda", SqlDbType.Int)
            ArrayParametros(3).Value = IIf(tienda = Nothing, 0, tienda)
            ArrayParametros(4) = New SqlParameter("@idperfil", SqlDbType.Int)
            ArrayParametros(4).Value = IIf(perfil = Nothing, 0, perfil)
            ArrayParametros(5) = New SqlParameter("@idrol", SqlDbType.Int)
            ArrayParametros(5).Value = IIf(rol = Nothing, 0, rol)
            ArrayParametros(6) = New SqlParameter("@idusuario", SqlDbType.Int)
            ArrayParametros(6).Value = IIf(usuario = Nothing, 0, usuario)

            'Cargo la data
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(ArrayParametros)
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Comisiones")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try

        Return ds

    End Function
End Class
