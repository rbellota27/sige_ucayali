﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAORegistroVentas
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function RegistroVentasxParams(ByVal idempresa As Integer, ByVal idtienda As Integer, ByVal fechai As Date, ByVal fechaf As Date, ByVal conPercep As Integer, ByVal monBase As Integer) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Dim procedure As String = NombreProcedure(conPercep, monBase)
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Try
            Dim cmd As New SqlCommand(procedure, cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdTienda", idtienda)
            cmd.Parameters.AddWithValue("@IdEmpresa", idempresa)
            cmd.Parameters.AddWithValue("@FechaI", fechai)
            cmd.Parameters.AddWithValue("@FechaF", fechaf)
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "_CR_RegistroVentas")
        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function
    Private Function NombreProcedure(ByVal percepcion As Integer, ByVal monbase As Integer) As String
        Dim procedure As String = ""
        If monbase = 0 Then

            If percepcion = 0 Then
                procedure = "_CR_RegistroVentas" 'todos
            ElseIf percepcion = 1 Then
                procedure = "[_CR_RegistroVentas2]" 'perccepcion
            Else
                procedure = "[_CR_RegistroVentas]" 'todos
            End If

        ElseIf monbase = 1 Then
            If percepcion = 0 Then
                procedure = "[_CR_RegistroVentaMonBas]" 'todos
            ElseIf percepcion = 1 Then
                procedure = "[_CR_RegistroVentas2MonBas]" 'perccepcion
            Else
                procedure = "[_CR_RegistroVentaMonBas]" 'todos
            End If
        Else
            If percepcion = 0 Then
                procedure = "[_CR_RegistroVentaMonBas]" 'todos
            ElseIf percepcion = 1 Then
                procedure = "[_CR_RegistroVentas2MonBas]" 'perccepcion
            Else
                procedure = "[_CR_RegistroVentaMonBas]" 'todos
            End If
        End If
        Return procedure
    End Function
End Class
