﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'******************   LUNES 31 MAYO 2010 HORA 03_23 PM

Imports System.Data.SqlClient
Public Class DAOAnexo_DetalleDocumento
    Inherits DAO.DAOMantenedor


    Dim cmd As SqlCommand


    Public Sub _Anexo_DetalleDocumentoInsert(ByVal objAnexo_DetalleDocumento As Entidades.Anexo_DetalleDocumento, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(7) {}

        p(0) = MyBase.getParam(objAnexo_DetalleDocumento.IdDocumento, "@IdDocumento", SqlDbType.Int)
        p(1) = MyBase.getParam(objAnexo_DetalleDocumento.IdDetalleDocumento, "@IdDetalleDocumento", SqlDbType.Int)
        p(2) = MyBase.getParam(objAnexo_DetalleDocumento.IdTipoPV, "@IdTipoPV", SqlDbType.Int)
        p(3) = MyBase.getParam(objAnexo_DetalleDocumento.CantidadAprobada, "@add_CantidadAprobada", SqlDbType.Decimal)
        p(4) = MyBase.getParam(objAnexo_DetalleDocumento.ComponenteKit, "@add_ComponenteKit", SqlDbType.Bit)
        p(5) = MyBase.getParam(objAnexo_DetalleDocumento.IdKit, "@IdKit", SqlDbType.Int)
        p(6) = MyBase.getParam(objAnexo_DetalleDocumento.IdCampania, "@IdCampania", SqlDbType.Int)
        p(7) = MyBase.getParam(objAnexo_DetalleDocumento.IdProductoRef, "@IdProductoRef", SqlDbType.Int)

        cmd = New SqlCommand("_Anexo_DetalleDocumentoInsert", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(p)

        cmd.ExecuteNonQuery()

    End Sub



End Class
