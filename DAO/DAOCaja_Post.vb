﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

''''''''''''''''''''''MIRAYA ANAMARIA, EDGAR 29/01/2010 6:50 pm
'DAOCaja_PostView hereda de esta clase, verifiquela antes de agregar o cambiar algo.
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOCaja_Post

    Protected objConexion As New Conexion
    Protected Cn As SqlConnection ' = objConexion.ConexionSIGE()
    Protected Tr As SqlTransaction

    Public Function InsertT(ByVal x As Entidades.Caja_Post) As Boolean
        Cn = objConexion.ConexionSIGE()
        Dim Prm() As SqlParameter = New SqlParameter(2) {}

        Prm(0) = New SqlParameter("@IdCaja", SqlDbType.Int)
        Prm(0).Value = x.IdCaja
        Prm(1) = New SqlParameter("@IdPost", SqlDbType.Int)
        Prm(1).Value = x.IdPost
        Prm(2) = New SqlParameter("@cpo_Estado", SqlDbType.Bit)
        Prm(2).Value = x.Estado

        Cn.Open()
        Tr = Cn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(Tr, CommandType.StoredProcedure, "_Caja_PostInsert", Prm)
            Tr.Commit()
            Return True
        Catch e As Exception
            Tr.Rollback()
            Throw (e)
            Return False
        Finally
            Cn.Close()
            Cn.Dispose()
        End Try

    End Function
    Public Function UpdateT(ByVal x As Entidades.Caja_Post) As Boolean
        Cn = objConexion.ConexionSIGE()
        Dim Prm() As SqlParameter = New SqlParameter(2) {}

        Prm(0) = New SqlParameter("@IdCaja", SqlDbType.Int)
        Prm(0).Value = x.IdCaja
        Prm(1) = New SqlParameter("@IdPost", SqlDbType.Int)
        Prm(1).Value = x.IdPost
        Prm(2) = New SqlParameter("@cpo_Estado", SqlDbType.Bit)
        Prm(2).Value = x.Estado

        Cn.Open()
        Tr = Cn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(Tr, CommandType.StoredProcedure, "_Caja_PostUpdate", Prm)
            Tr.Commit()
            Return True
        Catch e As Exception
            Tr.Rollback()
            Throw (e)
            Return False
        Finally
            Cn.Close()
            Cn.Dispose()
        End Try
    End Function

End Class
