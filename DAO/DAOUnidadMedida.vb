'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOUnidadMedida
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaUnidadMedida(ByVal unidadmedida As Entidades.UnidadMedida) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@um_CodigoSunat", SqlDbType.Char)
        ArrayParametros(0).Value = unidadmedida.CodigoSunat
        ArrayParametros(1) = New SqlParameter("@um_NombreLargo", SqlDbType.VarChar)
        ArrayParametros(1).Value = unidadmedida.Descripcion
        ArrayParametros(2) = New SqlParameter("@um_NombreCorto", SqlDbType.VarChar)
        ArrayParametros(2).Value = unidadmedida.DescripcionCorto
        ArrayParametros(3) = New SqlParameter("@um_Estado", SqlDbType.Char)
        ArrayParametros(3).Value = unidadmedida.Estado
        Return HDAO.Insert(cn, "_UnidadMedidaInsert", ArrayParametros)
    End Function
    Public Function ActualizaUnidadMedida(ByVal unidadmedida As Entidades.UnidadMedida) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@um_CodigoSunat", SqlDbType.Char)
        ArrayParametros(0).Value = unidadmedida.CodigoSunat
        ArrayParametros(1) = New SqlParameter("@um_NombreLargo", SqlDbType.VarChar)
        ArrayParametros(1).Value = unidadmedida.Descripcion
        ArrayParametros(2) = New SqlParameter("@um_NombreCorto", SqlDbType.VarChar)
        ArrayParametros(2).Value = unidadmedida.DescripcionCorto
        ArrayParametros(3) = New SqlParameter("@um_Estado", SqlDbType.Char)
        ArrayParametros(3).Value = unidadmedida.Estado
        ArrayParametros(4) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
        ArrayParametros(4).Value = unidadmedida.Id
        Return HDAO.Update(cn, "_UnidadMedidaUpdate", ArrayParametros)
    End Function
    Public Function SelectAll() As List(Of Entidades.UnidadMedida)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UnidadMedidaSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.UnidadMedida)
                Do While lector.Read
                    Dim objUM As New Entidades.UnidadMedida
                    objUM.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("um_CodigoSunat")) = True, "", lector.Item("um_CodigoSunat")))
                    objUM.Descripcion = CStr(IIf(IsDBNull(lector.Item("um_NombreLargo")) = True, "", lector.Item("um_NombreLargo")))
                    objUM.DescripcionCorto = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                    objUM.Estado = CStr(IIf(IsDBNull(lector.Item("um_Estado")) = True, "", lector.Item("um_Estado")))
                    objUM.Id = CInt(lector.Item("IdUnidadMedida"))
                    Lista.Add(objUM)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.UnidadMedida)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UnidadMedidaSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.UnidadMedida)
                Do While lector.Read
                    Dim objUM As New Entidades.UnidadMedida
                    objUM.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("um_CodigoSunat")) = True, "", lector.Item("um_CodigoSunat")))
                    objUM.Descripcion = CStr(IIf(IsDBNull(lector.Item("um_NombreLargo")) = True, "", lector.Item("um_NombreLargo")))
                    objUM.DescripcionCorto = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                    objUM.Estado = CStr(IIf(IsDBNull(lector.Item("um_Estado")) = True, "", lector.Item("um_Estado")))
                    objUM.Id = CInt(lector.Item("IdUnidadMedida"))
                    Lista.Add(objUM)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.UnidadMedida)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UnidadMedidaSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.UnidadMedida)
                Do While lector.Read
                    Dim objUM As New Entidades.UnidadMedida
                    objUM.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("um_CodigoSunat")) = True, "", lector.Item("um_CodigoSunat")))
                    objUM.Descripcion = CStr(IIf(IsDBNull(lector.Item("um_NombreLargo")) = True, "", lector.Item("um_NombreLargo")))
                    objUM.DescripcionCorto = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                    objUM.Estado = CStr(IIf(IsDBNull(lector.Item("um_Estado")) = True, "", lector.Item("um_Estado")))
                    objUM.Id = CInt(lector.Item("IdUnidadMedida"))
                    Lista.Add(objUM)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCbo() As List(Of Entidades.UnidadMedida)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UnidadMedidaSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.UnidadMedida)
                Do While lector.Read
                    Dim objUM As New Entidades.UnidadMedida
                    objUM.DescripcionCorto = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                    objUM.Id = CInt(lector.Item("IdUnidadMedida"))
                    Lista.Add(objUM)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.UnidadMedida)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UnidadMedidaSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.UnidadMedida)
                Do While lector.Read
                    Dim objUM As New Entidades.UnidadMedida
                    objUM.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("um_CodigoSunat")) = True, "", lector.Item("um_CodigoSunat")))
                    objUM.Descripcion = CStr(lector.Item("um_NombreLargo"))
                    objUM.DescripcionCorto = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                    objUM.Estado = CStr(lector.Item("um_Estado"))
                    objUM.Id = CInt(lector.Item("IdUnidadMedida"))
                    Lista.Add(objUM)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.UnidadMedida)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UnidadMedidaSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.UnidadMedida)
                Do While lector.Read
                    Dim objUM As New Entidades.UnidadMedida
                    objUM.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("um_CodigoSunat")) = True, "", lector.Item("um_CodigoSunat")))
                    objUM.Descripcion = CStr(lector.Item("um_NombreLargo"))
                    objUM.DescripcionCorto = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                    objUM.Estado = CStr(lector.Item("um_Estado"))
                    objUM.Id = CInt(lector.Item("IdUnidadMedida"))
                    Lista.Add(objUM)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.UnidadMedida)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UnidadMedidaSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.UnidadMedida)
                Do While lector.Read
                    Dim objUM As New Entidades.UnidadMedida
                    objUM.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("um_CodigoSunat")) = True, "", lector.Item("um_CodigoSunat")))
                    objUM.Descripcion = CStr(lector.Item("um_NombreLargo"))
                    objUM.DescripcionCorto = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                    objUM.Estado = CStr(lector.Item("um_Estado"))
                    objUM.Id = CInt(lector.Item("IdUnidadMedida"))
                    Lista.Add(objUM)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.UnidadMedida)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UnidadMedidaSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.UnidadMedida)
                Do While lector.Read
                    Dim objUM As New Entidades.UnidadMedida
                    objUM.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("um_CodigoSunat")) = True, "", lector.Item("um_CodigoSunat")))
                    objUM.Descripcion = CStr(lector.Item("um_NombreLargo"))
                    objUM.DescripcionCorto = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                    objUM.Estado = CStr(lector.Item("um_Estado"))
                    objUM.Id = CInt(lector.Item("IdUnidadMedida"))
                    Lista.Add(objUM)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxCodSunat(ByVal cod As String) As List(Of Entidades.UnidadMedida)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UnidadMedidaSelectxCodSunat", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Cod", cod)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.UnidadMedida)
                Do While lector.Read
                    Dim objUM As New Entidades.UnidadMedida
                    objUM.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("um_CodigoSunat")) = True, "", lector.Item("um_CodigoSunat")))
                    objUM.Descripcion = CStr(lector.Item("um_NombreLargo"))
                    objUM.DescripcionCorto = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                    objUM.Estado = CStr(lector.Item("um_Estado"))
                    objUM.Id = CInt(lector.Item("IdUnidadMedida"))
                    Lista.Add(objUM)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectNombreCortoxIdUnidadMedida(ByVal IdUnidadMedida As Integer) As String
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UnidadMedidaSelectNombreCortoxIdUnidadMedida", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdUnidadMedida", IdUnidadMedida)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                lector.Read()
                Dim UM As String = CStr(lector.Item("um_NombreCorto"))
                Return UM
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
