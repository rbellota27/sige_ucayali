﻿Imports Entidades
Imports System.Data.SqlClient
Public Class DAO_UsuarioView
    Public Function SelectxEstadoxNombreUsuario(ByVal cn As SqlConnection, ByVal Nombre As String, ByVal Estado As Integer) As List(Of Entidades.be_UsuarioView)

        Dim lista As List(Of Entidades.be_UsuarioView) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "_UsuarioViewSelectxEstadoxNombre_Responsable"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@Nombre", SqlDbType.NVarChar)).Value = Nombre
                .Parameters.Add(New SqlParameter("@Estado", SqlDbType.Int)).Value = Estado
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_UsuarioView)
                Dim ind1 As Integer = rdr.GetOrdinal("IdPersona")
                Dim ind2 As Integer = rdr.GetOrdinal("Nombre")

                Dim objeto As Entidades.be_UsuarioView = Nothing

                While rdr.Read()

                    objeto = New Entidades.be_UsuarioView
                    With objeto

                        .IdUsuario = rdr.GetInt32(ind1)
                        .ar_NombreLargo = rdr.GetString(ind2)

                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

End Class
