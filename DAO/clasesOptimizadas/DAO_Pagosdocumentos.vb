﻿Imports Entidades
Imports System.Data.SqlClient

Public Class DAO_Pagosdocumentos

    Public Function selectDocumentos(ByVal cn As SqlConnection, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdArea As Integer, ByVal Opcion_Aprobacion As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal monto As Decimal) As List(Of Entidades.be_PagosDocumentos)

        Dim lista As List(Of Entidades.be_PagosDocumentos) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "_DocumentoProgramacionPago_Aprobacion"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@IdEmpresa", SqlDbType.Int)).Value = IdEmpresa
                .Parameters.Add(New SqlParameter("@IdTienda", SqlDbType.Int)).Value = IdTienda
                .Parameters.Add(New SqlParameter("@IdArea", SqlDbType.Int)).Value = IdArea
                .Parameters.Add(New SqlParameter("@Opcion_Aprobacion", SqlDbType.Int)).Value = Opcion_Aprobacion
                .Parameters.Add(New SqlParameter("@FechaInicio", SqlDbType.Date)).Value = FechaInicio
                .Parameters.Add(New SqlParameter("@FechaFin", SqlDbType.Date)).Value = FechaFin
                .Parameters.Add(New SqlParameter("@monto", SqlDbType.Decimal)).Value = monto

            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_PagosDocumentos)

                Dim ind2 As String = rdr.GetOrdinal("Aprobado")
                Dim ind3 As String = rdr.GetOrdinal("FechaAprobacion")
                Dim ind4 As String = rdr.GetOrdinal("Area")
                Dim ind5 As String = rdr.GetOrdinal("Moneda")
                Dim ind6 As String = rdr.GetOrdinal("NumeroProgramacion")
                Dim ind7 As String = rdr.GetOrdinal("DescripcionPersona")
                Dim ind8 As String = rdr.GetOrdinal("FechaEmision")
                Dim ind9 As String = rdr.GetOrdinal("FechaPago")
                Dim ind10 As String = rdr.GetOrdinal("NroDocumento")
                Dim ind11 As String = rdr.GetOrdinal("TipoDocumento")
                Dim ind12 As Decimal = rdr.GetOrdinal("TotalAPagar")
                Dim ind13 As Integer = rdr.GetOrdinal("MontoDetraccion")
                Dim ind14 As Integer = rdr.GetOrdinal("MontoRetencion")
                Dim ind15 As Integer = rdr.GetOrdinal("Pago")
                Dim ind16 As Integer = rdr.GetOrdinal("NombreBanco")
                Dim ind17 As Integer = rdr.GetOrdinal("idProgramacion")
                Dim ind18 As Integer = rdr.GetOrdinal("IdDocumento")
                Dim ind19 As Integer = rdr.GetOrdinal("Id")
                Dim ind20 As Integer = rdr.GetOrdinal("IdArea")
                Dim ind21 As Integer = rdr.GetOrdinal("IdPersona")
                Dim ind22 As Integer = rdr.GetOrdinal("Alerta")
                'Dim ind23 As Integer = rdr.GetOrdinal("Deuda")

                Dim objeto As Entidades.be_PagosDocumentos = Nothing
                While rdr.Read()

                    objeto = New Entidades.be_PagosDocumentos
                    With objeto

                        .Aprobado = rdr.GetInt32(ind2)
                        .FechaAprobacion = rdr.GetDateTime(ind3)
                        .Area = rdr.GetString(ind4)
                        .Moneda = rdr.GetString(ind5)
                        .NumeroProgramacion = rdr.GetString(ind6)
                        .DescripcionPersona = rdr.GetString(ind7)
                        .FechaEmision = rdr.GetDateTime(ind8)
                        .FechaPago = rdr.GetDateTime(ind9)
                        .NroDocumento = rdr.GetString(ind10)
                        .TipoDocumento = rdr.GetString(ind11)
                        .TotalAPagar = rdr.GetDecimal(ind12)
                        .MontoDetraccion = rdr.GetDecimal(ind13)
                        .MontoRetencion = rdr.GetDecimal(ind14)
                        .Pago = rdr.GetDecimal(ind15)
                        .NombreBanco = rdr.GetString(ind16)
                        .idProgramacion = rdr.GetInt32(ind17)
                        .IdDocumento = rdr.GetInt32(ind18)
                        .Id = rdr.GetInt32(ind19)
                        .IdArea = rdr.GetInt32(ind20)
                        .IdPersona = rdr.GetInt32(ind21)
                        .Alerta = rdr.GetInt32(ind22)
                        '.Deuda = rdr.GetDecimal(ind23)

                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function
    Public Function selectDocumentosPagados(ByVal cn As SqlConnection, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdArea As Integer, ByVal Opcion_Aprobacion As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal monto As Decimal) As List(Of Entidades.be_PagosDocumentos)

        Dim lista As List(Of Entidades.be_PagosDocumentos) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "_DocumentoProgramacionPago_Aprobacion_Cancelados"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@IdEmpresa", SqlDbType.Int)).Value = IdEmpresa
                .Parameters.Add(New SqlParameter("@IdTienda", SqlDbType.Int)).Value = IdTienda
                .Parameters.Add(New SqlParameter("@IdArea", SqlDbType.Int)).Value = IdArea
                .Parameters.Add(New SqlParameter("@Opcion_Aprobacion", SqlDbType.Int)).Value = Opcion_Aprobacion
                .Parameters.Add(New SqlParameter("@FechaInicio", SqlDbType.Date)).Value = FechaInicio
                .Parameters.Add(New SqlParameter("@FechaFin", SqlDbType.Date)).Value = FechaFin
                .Parameters.Add(New SqlParameter("@monto", SqlDbType.Decimal)).Value = monto

            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_PagosDocumentos)
                Dim ind2 As String = rdr.GetOrdinal("Aprobado")
                Dim ind3 As String = rdr.GetOrdinal("FechaAprobacion")
                Dim ind4 As String = rdr.GetOrdinal("Area")
                Dim ind5 As String = rdr.GetOrdinal("Moneda")
                Dim ind6 As String = rdr.GetOrdinal("NumeroProgramacion")
                Dim ind7 As String = rdr.GetOrdinal("DescripcionPersona")
                Dim ind8 As String = rdr.GetOrdinal("FechaEmision")
                Dim ind9 As String = rdr.GetOrdinal("FechaPago")
                Dim ind10 As String = rdr.GetOrdinal("NroDocumento")
                Dim ind11 As String = rdr.GetOrdinal("TipoDocumento")
                Dim ind12 As Decimal = rdr.GetOrdinal("TotalAPagar")
                Dim ind13 As Integer = rdr.GetOrdinal("MontoDetraccion")
                Dim ind14 As Integer = rdr.GetOrdinal("MontoRetencion")
                Dim ind15 As Integer = rdr.GetOrdinal("Pago")
                Dim ind16 As Integer = rdr.GetOrdinal("NombreBanco")
                Dim ind17 As Integer = rdr.GetOrdinal("idProgramacion")
                Dim ind18 As Integer = rdr.GetOrdinal("IdDocumento")
                Dim ind19 As Integer = rdr.GetOrdinal("Id")
                Dim ind20 As Integer = rdr.GetOrdinal("IdArea")
                Dim ind21 As Integer = rdr.GetOrdinal("IdPersona")
                Dim ind22 As Integer = rdr.GetOrdinal("Alerta")
                Dim objeto As Entidades.be_PagosDocumentos = Nothing

                While rdr.Read()

                    objeto = New Entidades.be_PagosDocumentos
                    With objeto

                        '.est_reque = rdr.GetInt32(ind1)
                        .Aprobado = rdr.GetInt32(ind2)
                        .FechaAprobacion = rdr.GetDateTime(ind3)
                        .Area = rdr.GetString(ind4)
                        .Moneda = rdr.GetString(ind5)
                        .NumeroProgramacion = rdr.GetString(ind6)
                        .DescripcionPersona = rdr.GetString(ind7)
                        .FechaEmision = rdr.GetDateTime(ind8)
                        .FechaPago = rdr.GetDateTime(ind9)
                        .NroDocumento = rdr.GetString(ind10)
                        .TipoDocumento = rdr.GetString(ind11)
                        .TotalAPagar = rdr.GetDecimal(ind12)
                        .MontoDetraccion = rdr.GetDecimal(ind13)
                        .MontoRetencion = rdr.GetDecimal(ind14)
                        .Pago = rdr.GetDecimal(ind15)
                        .NombreBanco = rdr.GetString(ind16)
                        .idProgramacion = rdr.GetInt32(ind17)
                        .IdDocumento = rdr.GetInt32(ind18)
                        .Id = rdr.GetInt32(ind19)
                        .IdArea = rdr.GetInt32(ind20)
                        .IdPersona = rdr.GetInt32(ind21)
                        .Alerta = rdr.GetInt32(ind22)


                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

    Public Function selectDocumentosPagadosaExportar(ByVal cn As SqlConnection, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdArea As Integer, ByVal Opcion_Aprobacion As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal monto As Decimal) As List(Of Entidades.be_PagosDocumentosExport)

        Dim lista As List(Of Entidades.be_PagosDocumentosExport) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "_DocumentoProgramacionPago_Aprobacion_Cancelados_Excel"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@IdEmpresa", SqlDbType.Int)).Value = IdEmpresa
                .Parameters.Add(New SqlParameter("@IdTienda", SqlDbType.Int)).Value = IdTienda
                .Parameters.Add(New SqlParameter("@IdArea", SqlDbType.Int)).Value = IdArea
                .Parameters.Add(New SqlParameter("@Opcion_Aprobacion", SqlDbType.Int)).Value = Opcion_Aprobacion
                .Parameters.Add(New SqlParameter("@FechaInicio", SqlDbType.Date)).Value = FechaInicio
                .Parameters.Add(New SqlParameter("@FechaFin", SqlDbType.Date)).Value = FechaFin
                .Parameters.Add(New SqlParameter("@monto", SqlDbType.Decimal)).Value = monto

            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_PagosDocumentosExport)

                Dim ind4 As String = rdr.GetOrdinal("Area")
                Dim ind5 As String = rdr.GetOrdinal("Moneda")
                Dim ind6 As String = rdr.GetOrdinal("NumeroProgramacion")
                Dim ind7 As String = rdr.GetOrdinal("DescripcionPersona")
                Dim ind8 As String = rdr.GetOrdinal("FechaEmision")
                Dim ind9 As String = rdr.GetOrdinal("FechaPago")
                Dim ind10 As String = rdr.GetOrdinal("NroDocumento")
                Dim ind11 As String = rdr.GetOrdinal("TipoDocumento")
                Dim ind12 As Decimal = rdr.GetOrdinal("TotalAPagar")
                Dim ind13 As Integer = rdr.GetOrdinal("MontoDetraccion")
                Dim ind14 As Integer = rdr.GetOrdinal("MontoRetencion")
                Dim ind15 As Integer = rdr.GetOrdinal("Pago")
                Dim ind16 As Integer = rdr.GetOrdinal("NombreBanco")
                Dim ind17 As Integer = rdr.GetOrdinal("NroOperacion")

                Dim objeto As Entidades.be_PagosDocumentosExport = Nothing

                While rdr.Read()

                    objeto = New Entidades.be_PagosDocumentosExport
                    With objeto

                        '.est_reque = rdr.GetInt32(ind1)

                        .Area = rdr.GetString(ind4)
                        .Moneda = rdr.GetString(ind5)
                        .NumeroProgramacion = rdr.GetString(ind6)
                        .DescripcionPersona = rdr.GetString(ind7)
                        .FechaEmision = rdr.GetDateTime(ind8)
                        .FechaPago = rdr.GetDateTime(ind9)
                        .NroDocumento = rdr.GetString(ind10)
                        .TipoDocumento = rdr.GetString(ind11)
                        .TotalAPagar = rdr.GetDecimal(ind12)
                        .MontoDetraccion = rdr.GetDecimal(ind13)
                        .MontoRetencion = rdr.GetDecimal(ind14)
                        .Pago = rdr.GetDecimal(ind15)
                        .NombreBanco = rdr.GetString(ind16)
                        .NroOperacion = rdr.GetString(ind17)


                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

    Public Function selectSumarTotales(ByVal cn As SqlConnection, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdArea As Integer, ByVal Opcion_Aprobacion As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal monto As Decimal) As List(Of Entidades.be_PagosDocumentos)

        Dim lista As List(Of Entidades.be_PagosDocumentos) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "_SelectSumDocumentoProgramacionPago_Aprobacion"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@IdEmpresa", SqlDbType.Int)).Value = IdEmpresa
                .Parameters.Add(New SqlParameter("@IdTienda", SqlDbType.Int)).Value = IdTienda
                .Parameters.Add(New SqlParameter("@IdArea", SqlDbType.Int)).Value = IdArea
                .Parameters.Add(New SqlParameter("@Opcion_Aprobacion", SqlDbType.Int)).Value = Opcion_Aprobacion
                .Parameters.Add(New SqlParameter("@FechaInicio", SqlDbType.Date)).Value = FechaInicio
                .Parameters.Add(New SqlParameter("@FechaFin", SqlDbType.Date)).Value = FechaFin
                .Parameters.Add(New SqlParameter("@monto", SqlDbType.Decimal)).Value = monto

            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_PagosDocumentos)
                'Dim ind1 As Integer = rdr.GetOrdinal("est_reque")
           
                Dim ind12 As Decimal = rdr.GetOrdinal("Totales")
     
                Dim objeto As Entidades.be_PagosDocumentos = Nothing

                While rdr.Read()

                    objeto = New Entidades.be_PagosDocumentos
                    With objeto

                     
                        .TotalAPagar = rdr.GetDecimal(ind12)
                       
                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function




    Public Function UpdateDocumentoReprogramados(ByVal cn As SqlConnection, ByVal Fecha As String, ByVal IdDocumento As Integer) As List(Of Entidades.be_PagosDocumentos)

        Dim lista As List(Of Entidades.be_PagosDocumentos) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "SP_ACTUALIZAR_FECHA_PROGRAMACION"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@Fecha", SqlDbType.NVarChar)).Value = Fecha
                .Parameters.Add(New SqlParameter("@IdDocumento", SqlDbType.Int)).Value = IdDocumento
            End With
            Dim rdr As SqlDataReader = cmd.BeginExecuteNonQuery
        End Using
        Return lista

    End Function

    Public Function UpdateDocumento(ByVal cn As SqlConnection, ByVal IdDocumento As Integer, ByVal idrequerimiento As Integer) As List(Of Entidades.be_PagosDocumentos)

        Dim lista As List(Of Entidades.be_PagosDocumentos) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "_DocumentoProgramacionPago_Aprobacion_Update"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@IdDocumento", SqlDbType.Int)).Value = IdDocumento
                .Parameters.Add(New SqlParameter("@idrequerimiento", SqlDbType.Int)).Value = idrequerimiento


            End With
            Dim rdr As SqlDataReader = cmd.BeginExecuteNonQuery

        End Using
        Return lista
    End Function


    Public Function UpdateDocumentoRetencion(ByVal cn As SqlConnection, ByVal IdDocumento As Integer, ByVal idrequerimiento As Integer) As List(Of Entidades.be_PagosDocumentos)

        Dim lista As List(Of Entidades.be_PagosDocumentos) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "_DocumentoProgramacionPago_Retencion"

                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@IdDocumento", SqlDbType.Int)).Value = IdDocumento
                .Parameters.Add(New SqlParameter("@idrequerimiento", SqlDbType.Int)).Value = idrequerimiento


            End With
            Dim rdr As SqlDataReader = cmd.BeginExecuteNonQuery

        End Using
        Return lista
    End Function

End Class
