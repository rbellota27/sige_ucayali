﻿Imports System.Data.SqlClient
Imports Entidades
Public Class DAO_Reportes
    Public Function rpt_despacho_clienteFinal(ByVal cn As SqlConnection, ByVal flag As Boolean, ByVal fechaInicio As Date, _
                                              ByVal fechaFinal As Date) As DataTable
        Dim dt As New DataTable
        Using cmd As New SqlCommand("SP_REPORTE_PROGRAMACION_DESPACHOS", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@flag", SqlDbType.Bit)).Value = flag
                .Parameters.Add(New SqlParameter("@fechaInicio", SqlDbType.Date)).Value = fechaInicio
                .Parameters.Add(New SqlParameter("@fechaFinal", SqlDbType.Date)).Value = fechaFinal

            End With
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
        End Using
        Return dt
    End Function

    Public Function rpt_despacho_Sanicenter_subContratado(ByVal cn As SqlConnection, ByVal flag As Boolean, ByVal fechaInicio As Date, _
                                              ByVal fechaFinal As Date) As DataTable
        Dim dt As New DataTable
        Using cmd As New SqlCommand("SP_REPORTE_PROGRAMACION_DESPACHOS_SUBCONTRATADOS", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@flag", SqlDbType.Bit)).Value = flag
                .Parameters.Add(New SqlParameter("@fechaInicio", SqlDbType.Date)).Value = fechaInicio
                .Parameters.Add(New SqlParameter("@fechaFinal", SqlDbType.Date)).Value = fechaFinal

            End With
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
        End Using
        Return dt
    End Function

    Public Function rptGenerico(ByVal cn As SqlConnection, ByVal IdUsuario As Integer) As List(Of be_reportes)
        Dim dt As New DataTable
        Dim lista As List(Of be_reportes) = Nothing
        Using cmd As New SqlCommand("SP_LISTAR_REPORTE_PERFIL", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@IdUsuario", SqlDbType.Int)).Value = IdUsuario

            End With
            'Dim da As New SqlDataAdapter(cmd)
            'da.Fill(dt)
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)

            If rdr IsNot Nothing Then

                lista = New List(Of be_reportes)
                Dim ord1 As Integer = rdr.GetOrdinal("idtblReporte")
                Dim ord2 As Integer = rdr.GetOrdinal("nombreReporte")
                Dim ord3 As Integer = rdr.GetOrdinal("descripcionReporte")
                Dim ord4 As Integer = rdr.GetOrdinal("version")
                Dim ord5 As Integer = rdr.GetOrdinal("area")

                Dim objeto As Entidades.be_reportes = Nothing
                While rdr.Read()
                    objeto = New be_reportes
                    With objeto
                        .idtblReporte = rdr.GetInt32(ord1)
                        .nombreReporte = rdr.GetString(ord2)
                        .descripcionReporte = rdr.GetString(ord3)
                        .version = rdr.GetString(ord4)
                        .area = rdr.GetString(ord5)
                    End With
                    lista.Add(objeto)
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

    Public Function rptFiltros(ByVal cn As SqlConnection, ByVal idReporte As Integer, IdUSuario As Integer) As DataTable
        Dim dt As New DataTable
        Using cmd As New SqlCommand("SP_FILTROS_REPORTE", cn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add(New SqlParameter("@idReporte", SqlDbType.Int)).Value = idReporte
            cmd.Parameters.Add(New SqlParameter("@IdUSuario", SqlDbType.Int)).Value = IdUSuario
            Dim da As New SqlDataAdapter(cmd)
            Try
                da.Fill(dt)
            Catch ex As Exception
                Throw ex
            End Try
        End Using
        Return dt
    End Function

    Public Function ejecutaScripts(ByVal cn As SqlConnection, ByVal script As String) As DataTable
        Dim dt As New DataTable
        Using cmd As New SqlCommand(script, cn)
            cmd.CommandType = CommandType.Text
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
        End Using
        Return dt
    End Function

    Public Function ejecutaProceduresReportes(ByVal cn As SqlConnection, ByVal idReporte As Integer, ByVal parametros As String) As DataTable
        Dim dt As New DataTable
        Using cmd As New SqlCommand("SP_EJECUTA_PROCEDURES", cn)

            cmd.CommandTimeout = 150
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add(New SqlParameter("@idReporte", SqlDbType.Int)).Value = idReporte
            cmd.Parameters.Add(New SqlParameter("@parametros", SqlDbType.NVarChar)).Value = parametros

            Dim da As New SqlDataAdapter(cmd)
            Try
                da.Fill(dt)
            Catch ex As Exception
                Throw ex
            End Try
        End Using
        Return dt
    End Function

    'Public Function rpt_despacho_clienteFinal(ByVal cn As SqlConnection, ByVal flag As Boolean, ByVal fechaInicio As Date, _
    '                                          ByVal fechaFinal As Date) As List(Of be_rpt_despacho)
    '    Dim lista As List(Of be_rpt_despacho) = Nothing
    '    Using cmd As New SqlCommand("SP_REPORTE_PROGRAMACION_DESPACHOS", cn)
    '        With cmd
    '            .CommandType = CommandType.StoredProcedure

    '            .Parameters.Add(New SqlParameter("@flag", SqlDbType.Bit)).Value = flag
    '            .Parameters.Add(New SqlParameter("@fechaInicio", SqlDbType.Date)).Value = fechaInicio
    '            .Parameters.Add(New SqlParameter("@fechaFinal", SqlDbType.Date)).Value = fechaFinal

    '        End With
    '        Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
    '        If rdr IsNot Nothing Then
    '            lista = New List(Of Entidades.be_rpt_despacho)
    '            Dim ord1 As Integer = rdr.GetOrdinal("turno")
    '            Dim ord2 As Integer = rdr.GetOrdinal("fecInicioDespacho")
    '            Dim ord3 As Integer = rdr.GetOrdinal("veh_placa")
    '            Dim ord4 As Integer = rdr.GetOrdinal("nroVuelta")
    '            Dim ord5 As Integer = rdr.GetOrdinal("capacidadTotal")
    '            Dim ord6 As Integer = rdr.GetOrdinal("pesoUsado")
    '            Dim ord7 As Integer = rdr.GetOrdinal("cantDocAsociado")
    '            Dim ord8 As Integer = rdr.GetOrdinal("conductor")
    '            Dim ord9 As Integer = rdr.GetOrdinal("codigo")
    '            Dim ord10 As Integer = rdr.GetOrdinal("pesoItemsDocumento")
    '            Dim ord11 As Integer = rdr.GetOrdinal("clienteFinal")
    '            Dim ord12 As Integer = rdr.GetOrdinal("distrito")
    '            Dim ord13 As Integer = rdr.GetOrdinal("tie_Nombre")
    '            Dim objeto As be_rpt_despacho = Nothing
    '            While rdr.Read()
    '                objeto = New be_rpt_despacho
    '                With objeto
    '                    .turno = rdr.GetString(ord1)
    '                    .fecInicioDespacho = rdr.GetDateTime(ord2)
    '                    .vehPlaca = rdr.GetString(ord3)
    '                    .nroVuelta = rdr.GetInt32(ord4)
    '                    .capacidadTotal = rdr.GetDecimal(ord5)
    '                    .pesoUsado = rdr.GetDecimal(ord6)
    '                    .nroDocumento = rdr.GetInt32(ord7)
    '                    .conductor = rdr.GetString(ord8)
    '                    .codigo = rdr.GetString(ord9)
    '                    .pesoDocumento = rdr.GetDecimal(ord10)
    '                    .clienteFinal = rdr.GetString(ord11)
    '                    .distrito = rdr.GetString(ord12)
    '                    .tienda = rdr.GetString(ord13)
    '                End With
    '                lista.Add(objeto)
    '            End While
    '            rdr.Close()
    '        End If
    '    End Using
    '    Return lista
    'End Function
End Class
