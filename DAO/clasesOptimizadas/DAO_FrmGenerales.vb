﻿Imports System.Data.SqlClient
Imports Entidades
Public Class DAO_FrmGenerales

    Public Function validarEditar(ByVal cn As SqlConnection)

        Using cmd As New SqlCommand("")

        End Using

    End Function
    Public Function cls_frmCancelarProvisiones(ByVal cn As SqlConnection, ByVal idUsuario As Integer) As be_frmCargaPrincipal
        Dim objetoGenerico As be_frmCargaPrincipal = Nothing
        Using cmd As New SqlCommand("SP_FORM_APROBAR_COTI", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@idUSuario", SqlDbType.Int)).Value = idUsuario
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader()
            If rdr IsNot Nothing Then
                objetoGenerico = New be_frmCargaPrincipal
                If rdr.HasRows Then
                    Dim lista As New List(Of be_ComboGenerico)
                    Dim objeto As be_ComboGenerico = Nothing
                    While rdr.Read()
                        objeto = New be_ComboGenerico
                        objeto.campo1 = rdr.GetInt32(0)
                        objeto.campo2 = rdr.GetString(1)
                        lista.Add(objeto)
                    End While
                    objetoGenerico.listaEmpresa = lista
                End If
                If rdr.NextResult() Then
                    Dim lista As New List(Of be_ComboGenerico)
                    Dim objeto As be_ComboGenerico = Nothing
                    While rdr.Read()
                        objeto = New be_ComboGenerico
                        objeto.campo1 = rdr.GetInt32(0)
                        objeto.campo2 = rdr.GetString(1)
                        lista.Add(objeto)
                    End While
                    objetoGenerico.listaTienda = lista
                End If
                If rdr.NextResult() Then
                    Dim idTienda As Integer = 0
                    If rdr.HasRows Then
                        While rdr.Read()
                            idTienda = rdr.GetInt32(0)
                        End While
                        objetoGenerico.idTiendaPrincipal = idTienda
                    End If
                End If
                If rdr.NextResult() Then
                    Dim lista As New List(Of be_ComboGenerico)
                    Dim objeto As be_ComboGenerico = Nothing
                    While rdr.Read()
                        objeto = New be_ComboGenerico
                        objeto.campo1 = rdr.GetInt32(0)
                        objeto.campo2 = rdr.GetString(1)
                        lista.Add(objeto)
                    End While
                    objetoGenerico.listaArea = lista
                End If
                rdr.Close()
            End If
        End Using
        Return objetoGenerico
    End Function

    Public Function reportePicking(ByVal cn As SqlConnection, ByVal idSerie As Integer, ByVal codigo As String, ByVal idTipodocumento As Integer) As DataTable
        Dim dt As DataTable = Nothing
        Using cmd As New SqlCommand("SP_REPORTE_PICKING", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@idSerie", SqlDbType.Int)).Value = idSerie
                .Parameters.Add(New SqlParameter("@codigo", SqlDbType.NVarChar)).Value = codigo
                .Parameters.Add(New SqlParameter("@idTipoDocumento", SqlDbType.Int)).Value = idTipodocumento
            End With            
            Dim da As New SqlDataAdapter(cmd)
            If da IsNot Nothing Then
                dt = New DataTable
                da.Fill(dt)
                dt.TableName = "dt_reportePicking"
            End If
        End Using
        Return dt
    End Function
End Class
