﻿Imports Entidades
Imports System.Data.SqlClient
Public Class DAO_Bancos

    Public Function listarBancos(ByVal cn As SqlConnection, ByVal IdEstado As Integer) As List(Of Entidades.be_Bancos)

        Dim lista As List(Of Entidades.be_Bancos) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "_BancoSelectAllEstado"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@IdEstado", SqlDbType.Int)).Value = IdEstado

            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_Bancos)
                Dim ind1 As Integer = rdr.GetOrdinal("IdBanco")
                Dim ind2 As Integer = rdr.GetOrdinal("ban_Nombre")


                Dim objeto As Entidades.be_Bancos = Nothing

                While rdr.Read()

                    objeto = New Entidades.be_Bancos
                    With objeto

                        .IdBanco = rdr.GetInt32(ind1)
                        .ban_Nombre = rdr.GetString(ind2)
                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

End Class
