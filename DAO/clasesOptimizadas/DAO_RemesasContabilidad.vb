﻿Imports System.Data.SqlClient
Public Class DAO_RemesasContabilidad
    Inherits Conexion



    Public Function listarRemesasContabilidad(ByVal cn As SqlConnection, ByVal Empresa As String, ByVal Moneda As Integer, ByVal banco As Integer, ByVal fechaInicio As String, ByVal FechaFinal As String, ByVal FechaAsiento As String) As List(Of Entidades.be_RemesasContabilidad)

        Dim lista As List(Of Entidades.be_RemesasContabilidad) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "SP_Remesas_Contabilidad"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@Empresa", SqlDbType.NVarChar)).Value = Empresa
                .Parameters.Add(New SqlParameter("@moneda", SqlDbType.Int)).Value = Moneda
                .Parameters.Add(New SqlParameter("@banco", SqlDbType.Int)).Value = banco
                .Parameters.Add(New SqlParameter("@fechaIncial", SqlDbType.Date)).Value = fechaInicio
                .Parameters.Add(New SqlParameter("@fechaFinal", SqlDbType.Date)).Value = FechaFinal
                .Parameters.Add(New SqlParameter("@FechaAsiento", SqlDbType.Date)).Value = FechaAsiento

            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_RemesasContabilidad)



                Dim ind1 As Integer = rdr.GetOrdinal("Marcar")
                Dim ind2 As Integer = rdr.GetOrdinal("IdMovBanco")
                Dim ind3 As String = rdr.GetOrdinal("NombreBanco")
                Dim ind4 As String = rdr.GetOrdinal("CuentaBancaria")
                Dim ind5 As String = rdr.GetOrdinal("cuentaContable")
                Dim ind6 As String = rdr.GetOrdinal("CuentaCaja")
                Dim ind7 As String = rdr.GetOrdinal("Glosa")
                Dim ind8 As String = rdr.GetOrdinal("mban_FechaMov")
                Dim ind9 As Decimal = rdr.GetOrdinal("mban_Monto")
                Dim ind10 As String = rdr.GetOrdinal("FechEmisReciboEgreso")
                Dim ind11 As String = rdr.GetOrdinal("NroVoucherContable")
                Dim ind12 As String = rdr.GetOrdinal("FechaAsiento")

                Dim objeto As Entidades.be_RemesasContabilidad = Nothing

                While rdr.Read()

                    objeto = New Entidades.be_RemesasContabilidad
                    With objeto

                        .Marcar = rdr.GetInt32(ind1)
                        .IdMovBanco = rdr.GetInt32(ind2)
                        .NombreBanco = rdr.GetString(ind3)
                        .CuentaBancaria = rdr.GetString(ind4)
                        .cuentaContable = rdr.GetString(ind5)
                        .CuentaCaja = rdr.GetString(ind6)
                        .Glosa = rdr.GetString(ind7)
                        .mban_FechaMov = rdr.GetString(ind8)
                        .mban_Monto = rdr.GetDecimal(ind9)
                        .FechaEmisionReciboEgreso = rdr.GetString(ind10)
                        .NroVoucherContable = rdr.GetString(ind11)
                        .FechaAsiento = rdr.GetString(ind12)

                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

    Public Function UpdateDocumentoEgreso(ByVal cn As SqlConnection, ByVal IdMovBanco As Integer, ByVal FechaAsiento As String) As List(Of Entidades.be_RemesasContabilidad)

        Dim lista As List(Of Entidades.be_RemesasContabilidad) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "SP_Marcar_Remesas_Contabilidad"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@IdMovBanco", SqlDbType.Int)).Value = IdMovBanco
                .Parameters.Add(New SqlParameter("@FechaAsiento", SqlDbType.NVarChar)).Value = FechaAsiento

            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_RemesasContabilidad)



                Dim ind1 As Integer = rdr.GetOrdinal("Marcar")
                Dim ind2 As Integer = rdr.GetOrdinal("IdMovBanco")
                Dim ind3 As String = rdr.GetOrdinal("NombreBanco")
                Dim ind4 As String = rdr.GetOrdinal("CuentaBancaria")
                Dim ind5 As String = rdr.GetOrdinal("cuentaContable")
                Dim ind6 As String = rdr.GetOrdinal("CuentaCaja")
                Dim ind7 As String = rdr.GetOrdinal("Glosa")
                Dim ind8 As String = rdr.GetOrdinal("mban_FechaMov")
                Dim ind9 As Decimal = rdr.GetOrdinal("mban_Monto")
                Dim ind10 As String = rdr.GetOrdinal("FechaEmisionReciboEgreso")
                Dim ind11 As String = rdr.GetOrdinal("NroVoucherContable")
                Dim ind12 As String = rdr.GetOrdinal("FechaAsiento")

                Dim objeto As Entidades.be_RemesasContabilidad = Nothing

                While rdr.Read()

                    objeto = New Entidades.be_RemesasContabilidad
                    With objeto

                        .Marcar = rdr.GetInt32(ind1)
                        .IdMovBanco = rdr.GetInt32(ind2)
                        .NombreBanco = rdr.GetString(ind3)
                        .CuentaBancaria = rdr.GetString(ind4)
                        .cuentaContable = rdr.GetString(ind5)
                        .CuentaCaja = rdr.GetString(ind6)
                        .Glosa = rdr.GetString(ind7)
                        .mban_FechaMov = rdr.GetString(ind8)
                        .mban_Monto = rdr.GetDecimal(ind9)
                        .FechaEmisionReciboEgreso = rdr.GetString(ind10)
                        .NroVoucherContable = rdr.GetString(ind11)
                        .FechaAsiento = rdr.GetString(ind12)
                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function



End Class
