﻿Imports System.Data.SqlClient
Public Class DAO_Exportacion

    Public Function listarDetraccionesAprobadas(ByVal cn As SqlConnection, ByVal fecInicio As String, ByVal fecFin As String, _
                                                  ByVal flagFiltro As String) As List(Of Entidades.be_Exportacion)

        Dim lista As List(Of Entidades.be_Exportacion) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "SP_LISTARDETRACCIONES_VALIDAS"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@FechaInicio", SqlDbType.Date)).Value = fecInicio
                .Parameters.Add(New SqlParameter("@FechaFin", SqlDbType.Date)).Value = fecFin
                .Parameters.Add(New SqlParameter("@Anio", SqlDbType.Int)).Value = flagFiltro
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then

                lista = New List(Of Entidades.be_Exportacion)
                Dim ind23 As Integer = rdr.GetOrdinal("cadena")
                Dim objeto As Entidades.be_Exportacion = Nothing
                While rdr.Read()

                    objeto = New Entidades.be_Exportacion
                    With objeto
                        .cadena = rdr.GetString(ind23)
                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

    Public Function GeneraLoteDetracciones(ByVal cn As SqlConnection, ByVal fecInicio As String, ByVal fecFin As String, _
                                                    ByVal flagFiltro As String) As List(Of Entidades.be_Exportacion)

        Dim lista As List(Of Entidades.be_Exportacion) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                '.CommandText = "SP_LISTARDETRACCIONES_VALIDAS"
                .CommandText = "SP_GenerarLoteDetracciones"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@FechaInicio", SqlDbType.Date)).Value = fecInicio
                .Parameters.Add(New SqlParameter("@FechaFin", SqlDbType.Date)).Value = fecFin
                .Parameters.Add(New SqlParameter("@Anio", SqlDbType.Int)).Value = flagFiltro
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then

                lista = New List(Of Entidades.be_Exportacion)
                Dim ind23 As Integer = rdr.GetOrdinal("cadena")
                Dim objeto As Entidades.be_Exportacion = Nothing
                While rdr.Read()

                    objeto = New Entidades.be_Exportacion
                    With objeto
                        .cadena = rdr.GetString(ind23)
                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

    Public Function CierreLoteDetracciones(ByVal cn As SqlConnection, ByVal fecInicio As String, ByVal fecFin As String, _
                                                        ByVal lote As String) As List(Of Entidades.be_Exportacion)

        Dim lista As List(Of Entidades.be_Exportacion) = Nothing
        Using cmd As New SqlCommand()
            With cmd

                .CommandText = "SP_CierreLoteDetracciones"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@FechaInicio", SqlDbType.Date)).Value = fecInicio
                .Parameters.Add(New SqlParameter("@FechaFin", SqlDbType.Date)).Value = fecFin
                .Parameters.Add(New SqlParameter("@lote", SqlDbType.Int)).Value = lote

            End With

            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then

                lista = New List(Of Entidades.be_Exportacion)
                Dim ind23 As Integer = rdr.GetOrdinal("cadena")
                Dim objeto As Entidades.be_Exportacion = Nothing
                While rdr.Read()

                    objeto = New Entidades.be_Exportacion
                    With objeto
                        .cadena = rdr.GetString(ind23)
                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

    Public Function listarDetraccionesxLote(ByVal cn As SqlConnection, ByVal flagFiltro As String) As List(Of Entidades.be_Exportacion)

        Dim lista As List(Of Entidades.be_Exportacion) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "SP_ReportePagoDetraciones_X_Lote"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@lote", SqlDbType.Int)).Value = flagFiltro

            End With

            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)

            If rdr IsNot Nothing Then

                lista = New List(Of Entidades.be_Exportacion)
                Dim ind23 As Integer = rdr.GetOrdinal("cadena")



                Dim objeto As Entidades.be_Exportacion = Nothing

                While rdr.Read()

                    objeto = New Entidades.be_Exportacion
                    With objeto
                        .cadena = rdr.GetString(ind23)
                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function


End Class
