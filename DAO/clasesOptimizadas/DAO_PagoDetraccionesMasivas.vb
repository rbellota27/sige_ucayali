﻿Imports System.Data.SqlClient
Public Class DAO_PagoDetraccionesMasivas
    Inherits Conexion

    ' (ByVal cn As SqlConnection) As List(Of Entidades.Documento)
    Public Function listarDetraccionesMasivas(ByVal cn As SqlConnection, ByVal fecInicio As String, ByVal fecFin As String, _
                                                  ByVal flagFiltro As String) As List(Of Entidades.be_detraccionMasiva)

        Dim lista As List(Of Entidades.be_detraccionMasiva) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "SP_LISTARDETRACCIONES_X_PAGAR"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@FechaInicio", SqlDbType.Date)).Value = fecInicio
                .Parameters.Add(New SqlParameter("@FechaFin", SqlDbType.Date)).Value = fecFin
                .Parameters.Add(New SqlParameter("@Anio", SqlDbType.Int)).Value = flagFiltro
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then

                lista = New List(Of Entidades.be_detraccionMasiva)
                Dim ind1 As Integer = rdr.GetOrdinal("IdDocumento")
                Dim ind2 As Integer = rdr.GetOrdinal("doc_FechaEmision")
                Dim ind3 As Integer = rdr.GetOrdinal("doc_NroVoucherConta")
                Dim ind4 As Integer = rdr.GetOrdinal("Ruc")
                Dim ind5 As Integer = rdr.GetOrdinal("NombreProveedor")
                Dim ind6 As Integer = rdr.GetOrdinal("Serie")
                Dim ind7 As Integer = rdr.GetOrdinal("codigo")
                Dim ind8 As Integer = rdr.GetOrdinal("Moneda")
                Dim ind9 As Integer = rdr.GetOrdinal("montoRegimen")
                Dim ind10 As Integer = rdr.GetOrdinal("TC")
                Dim ind11 As Integer = rdr.GetOrdinal("MontoEnSoles")
                Dim ind12 As Integer = rdr.GetOrdinal("can_nroOperacion")
                Dim ind13 As Integer = rdr.GetOrdinal("can_montoCancelado")
                Dim ind14 As Integer = rdr.GetOrdinal("NumeroCuenta")
                Dim ind15 As Integer = rdr.GetOrdinal("reg_Nombre")
                Dim ind16 As Integer = rdr.GetOrdinal("reg_Id")
                Dim ind17 As Integer = rdr.GetOrdinal("can_fechaCancelacion")
                Dim ind18 As Integer = rdr.GetOrdinal("ban_Abv")
                Dim ind19 As Integer = rdr.GetOrdinal("IdPersona")
                Dim ind20 As Integer = rdr.GetOrdinal("IdCuenta")
                Dim ind21 As Integer = rdr.GetOrdinal("IdTipoOperacion")
                Dim ind22 As Integer = rdr.GetOrdinal("IdConcepto")

               
                Dim objeto As Entidades.be_detraccionMasiva = Nothing

                While rdr.Read()

                    objeto = New Entidades.be_detraccionMasiva
                    With objeto

                        .IdDocumento = rdr.GetInt32(ind1)
                        .doc_FechaEmision = rdr.GetDateTime(ind2)
                        .doc_NroVoucherConta = rdr.GetString(ind3)
                        .Ruc = rdr.GetString(ind4)
                        .NombreProveedor = rdr.GetString(ind5)
                        .Serie = rdr.GetString(ind6)
                        .codigo = rdr.GetString(ind7)
                        .Moneda = rdr.GetString(ind8)
                        .montoRegimen = rdr.GetDecimal(ind9)
                        .TC = rdr.GetDecimal(ind10)
                        .MontoEnSoles = rdr.GetDecimal(ind11)
                        .can_nroOperacion = rdr.GetString(ind12)
                        .can_montoCancelado = rdr.GetDecimal(ind13)
                        .NumeroCuenta = rdr.GetString(ind14)
                        .reg_Nombre = rdr.GetString(ind15)
                        .reg_Id = rdr.GetInt32(ind16)
                        .can_fechaCancelacion = rdr.GetString(ind17)
                        .ban_Abv = rdr.GetString(ind18)
                        .IdPersona = rdr.GetInt32(ind19)
                        .IdCuenta = rdr.GetInt32(ind20)
                        .IdTipoOperacion = rdr.GetInt32(ind21)
                        .IdConcepto = rdr.GetInt32(ind22)

                        lista.Add(objeto)

                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

    Public Function listaDetraccionesporLote(ByVal cn As SqlConnection, ByVal lote As String, ByVal anio As String) As List(Of Entidades.be_detraccionMasiva)
        Dim lista As List(Of Entidades.be_detraccionMasiva) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "SP_LISTARDETRACCIONES_X_LOTES"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@lote", SqlDbType.NVarChar)).Value = lote
                .Parameters.Add(New SqlParameter("@anio", SqlDbType.NVarChar)).Value = anio
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then

                lista = New List(Of Entidades.be_detraccionMasiva)
                Dim ind1 As Integer = rdr.GetOrdinal("IdDocumento")
                Dim ind2 As Integer = rdr.GetOrdinal("doc_FechaEmision")
                Dim ind3 As Integer = rdr.GetOrdinal("doc_NroVoucherConta")
                Dim ind4 As Integer = rdr.GetOrdinal("Ruc")
                Dim ind5 As Integer = rdr.GetOrdinal("NombreProveedor")
                Dim ind6 As Integer = rdr.GetOrdinal("Serie")
                Dim ind7 As Integer = rdr.GetOrdinal("codigo")
                Dim ind8 As Integer = rdr.GetOrdinal("Moneda")
                Dim ind9 As Integer = rdr.GetOrdinal("montoRegimen")
                Dim ind10 As Integer = rdr.GetOrdinal("TC")
                Dim ind11 As Integer = rdr.GetOrdinal("MontoEnSoles")
                Dim ind12 As Integer = rdr.GetOrdinal("can_nroOperacion")
                Dim ind13 As Integer = rdr.GetOrdinal("can_montoCancelado")
                Dim ind14 As Integer = rdr.GetOrdinal("NumeroCuenta")
                Dim ind15 As Integer = rdr.GetOrdinal("reg_Nombre")
                Dim ind16 As Integer = rdr.GetOrdinal("reg_Id")
                Dim ind17 As Integer = rdr.GetOrdinal("can_fechaCancelacion")
                Dim ind18 As Integer = rdr.GetOrdinal("ban_Abv")
                Dim ind19 As Integer = rdr.GetOrdinal("IdPersona")
                Dim ind20 As Integer = rdr.GetOrdinal("IdCuenta")
                Dim ind21 As Integer = rdr.GetOrdinal("IdTipoOperacion")
                Dim ind22 As Integer = rdr.GetOrdinal("IdConcepto")


                Dim objeto As Entidades.be_detraccionMasiva = Nothing

                While rdr.Read()

                    objeto = New Entidades.be_detraccionMasiva
                    With objeto

                        .IdDocumento = rdr.GetInt32(ind1)
                        .doc_FechaEmision = rdr.GetDateTime(ind2)
                        .doc_NroVoucherConta = rdr.GetString(ind3)
                        .Ruc = rdr.GetString(ind4)
                        .NombreProveedor = rdr.GetString(ind5)
                        .Serie = rdr.GetString(ind6)
                        .codigo = rdr.GetString(ind7)
                        .Moneda = rdr.GetString(ind8)
                        .montoRegimen = rdr.GetDecimal(ind9)
                        .TC = rdr.GetDecimal(ind10)
                        .MontoEnSoles = rdr.GetDecimal(ind11)
                        .can_nroOperacion = rdr.GetString(ind12)
                        .can_montoCancelado = rdr.GetDecimal(ind13)
                        .NumeroCuenta = rdr.GetString(ind14)

                        .reg_Nombre = rdr.GetString(ind15)
                        .reg_Id = rdr.GetInt32(ind16)
                        .can_fechaCancelacion = rdr.GetString(ind17)
                        .ban_Abv = rdr.GetString(ind18)
                        .IdPersona = rdr.GetInt32(ind19)
                        .IdCuenta = rdr.GetInt32(ind20)
                        .IdTipoOperacion = rdr.GetInt32(ind21)
                        .IdConcepto = rdr.GetInt32(ind22)

                        lista.Add(objeto)

                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function
    Public Function listarDetraccionesMasivasEstados(ByVal cn As SqlConnection, ByVal iddocumento As String, ByVal estado As String) As List(Of Entidades.be_detraccionMasiva)

        Dim lista As List(Of Entidades.be_detraccionMasiva) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "SP_LISTARDETRACCIONES_X_PAGAR_ESTADOS"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@iddocumento", SqlDbType.NVarChar)).Value = iddocumento
                .Parameters.Add(New SqlParameter("@estado", SqlDbType.NVarChar)).Value = estado
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then

                lista = New List(Of Entidades.be_detraccionMasiva)
                Dim ind1 As Integer = rdr.GetOrdinal("IdDocumento")
                Dim ind2 As Integer = rdr.GetOrdinal("doc_FechaEmision")
                Dim ind3 As Integer = rdr.GetOrdinal("doc_NroVoucherConta")
                Dim ind4 As Integer = rdr.GetOrdinal("Ruc")
                Dim ind5 As Integer = rdr.GetOrdinal("NombreProveedor")
                Dim ind6 As Integer = rdr.GetOrdinal("Serie")
                Dim ind7 As Integer = rdr.GetOrdinal("codigo")
                Dim ind8 As Integer = rdr.GetOrdinal("Moneda")
                Dim ind9 As Integer = rdr.GetOrdinal("montoRegimen")
                Dim ind10 As Integer = rdr.GetOrdinal("TC")
                Dim ind11 As Integer = rdr.GetOrdinal("MontoEnSoles")
                Dim ind12 As Integer = rdr.GetOrdinal("can_nroOperacion")
                Dim ind13 As Integer = rdr.GetOrdinal("can_montoCancelado")
                Dim ind14 As Integer = rdr.GetOrdinal("NumeroCuenta")
                Dim ind15 As Integer = rdr.GetOrdinal("reg_Nombre")
                Dim ind16 As Integer = rdr.GetOrdinal("reg_Id")
                Dim ind17 As Integer = rdr.GetOrdinal("can_fechaCancelacion")
                Dim ind18 As Integer = rdr.GetOrdinal("ban_Abv")
                Dim ind19 As Integer = rdr.GetOrdinal("IdPersona")
                Dim ind20 As Integer = rdr.GetOrdinal("IdCuenta")
                Dim ind21 As Integer = rdr.GetOrdinal("IdTipoOperacion")
                Dim ind22 As Integer = rdr.GetOrdinal("IdConcepto")


                Dim objeto As Entidades.be_detraccionMasiva = Nothing

                While rdr.Read()

                    objeto = New Entidades.be_detraccionMasiva
                    With objeto

                        .IdDocumento = rdr.GetInt32(ind1)
                        .doc_FechaEmision = rdr.GetDateTime(ind2)
                        .doc_NroVoucherConta = rdr.GetString(ind3)
                        .Ruc = rdr.GetString(ind4)
                        .NombreProveedor = rdr.GetString(ind5)
                        .Serie = rdr.GetString(ind6)
                        .codigo = rdr.GetString(ind7)
                        .Moneda = rdr.GetString(ind8)
                        .montoRegimen = rdr.GetDecimal(ind9)
                        .TC = rdr.GetDecimal(ind10)
                        .MontoEnSoles = rdr.GetDecimal(ind11)
                        .can_nroOperacion = rdr.GetString(ind12)
                        .can_montoCancelado = rdr.GetDecimal(ind13)
                        .NumeroCuenta = rdr.GetString(ind14)
                        .reg_Nombre = rdr.GetString(ind15)
                        .reg_Id = rdr.GetInt32(ind16)
                        .can_fechaCancelacion = rdr.GetString(ind17)
                        .ban_Abv = rdr.GetString(ind18)
                        .IdPersona = rdr.GetInt32(ind19)
                        .IdCuenta = rdr.GetInt32(ind20)
                        .IdTipoOperacion = rdr.GetInt32(ind21)
                        .IdConcepto = rdr.GetInt32(ind22)

                        lista.Add(objeto)

                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function


    Public Function listarDetraccionesMasivasMontosEstados(ByVal cn As SqlConnection, ByVal iddocumento As String, ByVal MontoN As Integer, ByVal estado As String) As List(Of Entidades.be_detraccionMasiva)

        Dim lista As List(Of Entidades.be_detraccionMasiva) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "SP_LISTARDETRACCIONES_X_PAGAR_ESTADOS_MONTOS"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@iddocumento", SqlDbType.NVarChar)).Value = iddocumento
                .Parameters.Add(New SqlParameter("@estado", SqlDbType.NVarChar)).Value = estado
                .Parameters.Add(New SqlParameter("@MontoN", SqlDbType.Int)).Value = MontoN
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then

                lista = New List(Of Entidades.be_detraccionMasiva)
                Dim ind1 As Integer = rdr.GetOrdinal("IdDocumento")
                Dim ind2 As Integer = rdr.GetOrdinal("doc_FechaEmision")
                Dim ind3 As Integer = rdr.GetOrdinal("doc_NroVoucherConta")
                Dim ind4 As Integer = rdr.GetOrdinal("Ruc")
                Dim ind5 As Integer = rdr.GetOrdinal("NombreProveedor")
                Dim ind6 As Integer = rdr.GetOrdinal("Serie")
                Dim ind7 As Integer = rdr.GetOrdinal("codigo")
                Dim ind8 As Integer = rdr.GetOrdinal("Moneda")
                Dim ind9 As Integer = rdr.GetOrdinal("montoRegimen")
                Dim ind10 As Integer = rdr.GetOrdinal("TC")
                Dim ind11 As Integer = rdr.GetOrdinal("MontoEnSoles")
                Dim ind12 As Integer = rdr.GetOrdinal("can_nroOperacion")
                Dim ind13 As Integer = rdr.GetOrdinal("can_montoCancelado")
                Dim ind14 As Integer = rdr.GetOrdinal("NumeroCuenta")
                Dim ind15 As Integer = rdr.GetOrdinal("reg_Nombre")
                Dim ind16 As Integer = rdr.GetOrdinal("reg_Id")
                Dim ind17 As Integer = rdr.GetOrdinal("can_fechaCancelacion")
                Dim ind18 As Integer = rdr.GetOrdinal("ban_Abv")
                Dim ind19 As Integer = rdr.GetOrdinal("IdPersona")
                Dim ind20 As Integer = rdr.GetOrdinal("IdCuenta")
                Dim ind21 As Integer = rdr.GetOrdinal("IdTipoOperacion")
                Dim ind22 As Integer = rdr.GetOrdinal("IdConcepto")


                Dim objeto As Entidades.be_detraccionMasiva = Nothing

                While rdr.Read()

                    objeto = New Entidades.be_detraccionMasiva
                    With objeto

                        .IdDocumento = rdr.GetInt32(ind1)
                        .doc_FechaEmision = rdr.GetDateTime(ind2)
                        .doc_NroVoucherConta = rdr.GetString(ind3)
                        .Ruc = rdr.GetString(ind4)
                        .NombreProveedor = rdr.GetString(ind5)
                        .Serie = rdr.GetString(ind6)
                        .codigo = rdr.GetString(ind7)
                        .Moneda = rdr.GetString(ind8)
                        .montoRegimen = rdr.GetDecimal(ind9)
                        .TC = rdr.GetDecimal(ind10)
                        .MontoEnSoles = rdr.GetDecimal(ind11)
                        .can_nroOperacion = rdr.GetString(ind12)
                        .can_montoCancelado = rdr.GetDecimal(ind13)
                        .NumeroCuenta = rdr.GetString(ind14)
                        .reg_Nombre = rdr.GetString(ind15)
                        .reg_Id = rdr.GetInt32(ind16)
                        .can_fechaCancelacion = rdr.GetString(ind17)
                        .ban_Abv = rdr.GetString(ind18)
                        .IdPersona = rdr.GetInt32(ind19)
                        .IdCuenta = rdr.GetInt32(ind20)
                        .IdTipoOperacion = rdr.GetInt32(ind21)
                        .IdConcepto = rdr.GetInt32(ind22)

                        lista.Add(objeto)

                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function


    
End Class




