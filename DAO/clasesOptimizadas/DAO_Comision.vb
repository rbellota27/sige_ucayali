﻿Imports Entidades
Imports System.Data.SqlClient
Public Class DAO_Comision
    Public Function comisionxSucursal(ByVal cn As SqlConnection, ByVal fecInicio As String, ByVal fecFin As String)
        'los datos estáticos fueron definidos de esa forma por despinoza
        Dim lista As List(Of be_comision) = Nothing
        Using cmd As New SqlCommand("SanicERP_ReporteComsionesporsucural", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@idusuario", SqlDbType.Int)).Value = 0
                .Parameters.Add(New SqlParameter("@IdEmpresa", SqlDbType.Int)).Value = 1
                .Parameters.Add(New SqlParameter("@IdTienda", SqlDbType.Int)).Value = 0
                .Parameters.Add(New SqlParameter("@FechaInicio", SqlDbType.Date)).Value = CDate(fecInicio)
                .Parameters.Add(New SqlParameter("@FechaFin", SqlDbType.Date)).Value = CDate(fecFin)
                .Parameters.Add(New SqlParameter("@idtipodocumento", SqlDbType.Int)).Value = 898
                .Parameters.Add(New SqlParameter("@idlinea", SqlDbType.Int)).Value = 0
                .Parameters.Add(New SqlParameter("@idsublinea", SqlDbType.Int)).Value = 0
                .Parameters.Add(New SqlParameter("@IdPersonaU", SqlDbType.Int)).Value = 0
                .Parameters.Add(New SqlParameter("@ReportID", SqlDbType.Int)).Value = 44
                .Parameters.Add(New SqlParameter("@nombrereport", SqlDbType.NVarChar)).Value = "ReporteVentasTiendas.rpt"
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of be_comision)
                Dim ordinal1 As Integer = rdr.GetOrdinal("Persona")
                Dim ordinal2 As Integer = rdr.GetOrdinal("Tienda")
                Dim ordinal3 As Integer = rdr.GetOrdinal("Pago1Porc")
                Dim ordinal4 As Integer = rdr.GetOrdinal("Pago05Porc")
                Dim ordinal5 As Integer = rdr.GetOrdinal("TotalPagoComisVol")
                Dim ordinal6 As Integer = rdr.GetOrdinal("TotalComisPremium")
                Dim ordinal7 As Integer = rdr.GetOrdinal("Adicional")
                Dim ordinal8 As Integer = rdr.GetOrdinal("TotalComision")

                Dim objetoComision As be_comision = Nothing
                While rdr.Read()
                    objetoComision = New be_comision
                    With objetoComision
                        .nomComisionista = rdr.GetString(ordinal1)
                        .nomtienda = rdr.GetString(ordinal2)
                        .primerPorcPago = rdr.GetDecimal(ordinal3)
                        .segundoPorcPago = rdr.GetDecimal(ordinal4)
                        .subTotalPagoComision = rdr.GetDecimal(ordinal5)
                        .totalComisionPremium = rdr.GetDecimal(ordinal6)
                        .Pagoadicional = rdr.GetDecimal(ordinal7)
                        .pagoTotalComision = rdr.GetDecimal(ordinal8)
                    End With
                    lista.Add(objetoComision)
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function
End Class
