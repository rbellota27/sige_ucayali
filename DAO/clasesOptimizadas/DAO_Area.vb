﻿Imports Entidades
Imports System.Data.SqlClient
Public Class DAO_Area
    Public Function selectArea(ByVal cn As SqlConnection, ByVal IdEmpresa As Integer) As List(Of Entidades.be_Area)

        Dim lista As List(Of Entidades.be_Area) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "_AreaSelectCombo"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@IdEmpresa", SqlDbType.Int)).Value = IdEmpresa

            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_Area)
                Dim ind1 As Integer = rdr.GetOrdinal("IdArea")
                Dim ind2 As Integer = rdr.GetOrdinal("ar_NombreLargo")

                Dim objeto As Entidades.be_Area = Nothing

                While rdr.Read()

                    objeto = New Entidades.be_Area
                    With objeto

                        .IdArea = rdr.GetInt32(ind1)
                        .ar_NombreLargo = rdr.GetString(ind2)

                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

End Class



