﻿Imports Entidades
Imports System.Data.SqlClient
Public Class DAO_LetrasxCobrar



    Public Function listarLetrasxExportar(ByVal cn As SqlConnection, ByVal IdTienda As Integer, ByVal IdEstado As Integer, ByVal fechaInicio As String, ByVal FechaFinal As String) As List(Of Entidades.ExportaLetras)

        Dim lista As List(Of Entidades.ExportaLetras) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "SP_SELECT_LETRASXCOBRAR_EXPORTAR"
                .Connection = cn
                .CommandTimeout = 0

                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@IdTienda", SqlDbType.Int)).Value = IdTienda
                .Parameters.Add(New SqlParameter("@IdEstado", SqlDbType.Int)).Value = IdEstado
                .Parameters.Add(New SqlParameter("@fechaInicio", SqlDbType.NVarChar)).Value = fechaInicio
                .Parameters.Add(New SqlParameter("@FechaFinal", SqlDbType.NVarChar)).Value = FechaFinal



            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then



                lista = New List(Of Entidades.ExportaLetras)
                Dim ind23 As Integer = rdr.GetOrdinal("EstadoProtesto")
                Dim ind24 As Integer = rdr.GetOrdinal("FechaEnProtesto")

                Dim ind1 As Integer = rdr.GetOrdinal("EstadoLetraCambio")
                Dim ind2 As Integer = rdr.GetOrdinal("FechaCancelacion")
                Dim ind3 As Integer = rdr.GetOrdinal("doc_FechaEmision")
                'Dim ind4 As Integer = rdr.GetOrdinal("doc_Serie")
                Dim ind5 As Integer = rdr.GetOrdinal("doc_Codigo")
                Dim ind6 As Integer = rdr.GetOrdinal("doc_Total")

                Dim ind7 As Integer = rdr.GetOrdinal("Saldo")
                Dim ind8 As Integer = rdr.GetOrdinal("MontoCancelado")

                'Dim ind9 As Integer = rdr.GetOrdinal("RUCODNI")
                Dim ind10 As Integer = rdr.GetOrdinal("RazonSocial")
                Dim ind11 As Integer = rdr.GetOrdinal("doc_FechaVenc")
                Dim ind12 As Integer = rdr.GetOrdinal("FacturasRelacionadas")
                Dim ind13 As Integer = rdr.GetOrdinal("FechaEntregaVendedor")
                Dim ind14 As Integer = rdr.GetOrdinal("Nombre") 'VENDEDOR
                Dim ind15 As Integer = rdr.GetOrdinal("FechaRecepcionLetraFirmada")
                Dim ind16 As Integer = rdr.GetOrdinal("FechaEnvioBanco")
                Dim ind17 As Integer = rdr.GetOrdinal("NroUnico")
                Dim ind18 As Integer = rdr.GetOrdinal("CondicionLetra")
                'Dim ind19 As Integer = rdr.GetOrdinal("Observacion")
                Dim ind20 As Integer = rdr.GetOrdinal("RecibosIngreso")
                Dim ind21 As Integer = rdr.GetOrdinal("Banco")
                Dim ind22 As Integer = rdr.GetOrdinal("IdEstadoLetra")



                Dim objeto As Entidades.ExportaLetras = Nothing
                While rdr.Read()
                    objeto = New Entidades.ExportaLetras
                    With objeto
                        .EstadoProtesto = rdr.GetString(ind23)
                        .FechaEnProtesto = rdr.GetString(ind24)
                        .EstadoLetraCambio = rdr.GetString(ind1)
                        .FechaCancelacion = rdr.GetDateTime(ind2)
                        .doc_FechaEmision = rdr.GetString(ind3)
                        '.doc_Serie = rdr.GetString(ind4)
                        .doc_Codigo = rdr.GetString(ind5)
                        .doc_Total = rdr.GetDecimal(ind6)
                        .Saldo = rdr.GetDecimal(ind7)
                        .MontoCancelado = rdr.GetDecimal(ind8)
                        '.RUCODNI = rdr.GetString(ind9)
                        .RazonSocial = rdr.GetString(ind10)
                        .doc_FechaVenc = rdr.GetString(ind11)
                        .FacturasRelacionadas = rdr.GetString(ind12)
                        .FechaEntregaVendedor = rdr.GetDateTime(ind13)
                        .Nombre = rdr.GetString(ind14)
                        .FechaRecepcionLetraFirmada = rdr.GetDateTime(ind15)
                        .FechaEnvioBanco = rdr.GetDateTime(ind16)
                        .NroUnico = rdr.GetString(ind17)
                        .CondicionLetra = rdr.GetString(ind18)
                        '.Observacion = rdr.GetString(ind19)
                        .RecibosIngreso = rdr.GetString(ind20)
                        .Banco = rdr.GetString(ind21)
                        .IdEstadoLetra = rdr.GetInt32(ind22)

                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function


    Public Function listarLetrasxCobrar(ByVal cn As SqlConnection, ByVal IdTienda As Integer, ByVal IdEstado As Integer, ByVal fechaInicio As String, ByVal FechaFinal As String) As List(Of Entidades.be_LetrasxCobrar)

        Dim lista As List(Of Entidades.be_LetrasxCobrar) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "SP_SELECT_LETRASXCOBRAR"
                .Connection = cn
                .CommandTimeout = 0

                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@IdTienda", SqlDbType.Int)).Value = IdTienda
                .Parameters.Add(New SqlParameter("@IdEstado", SqlDbType.Int)).Value = IdEstado
                .Parameters.Add(New SqlParameter("@fechaInicio", SqlDbType.NVarChar)).Value = fechaInicio
                .Parameters.Add(New SqlParameter("@FechaFinal", SqlDbType.NVarChar)).Value = FechaFinal



            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then

                lista = New List(Of Entidades.be_LetrasxCobrar)
                Dim ind1 As Integer = rdr.GetOrdinal("IdDocumento")
                Dim ind2 As Integer = rdr.GetOrdinal("doc_Serie")
                Dim ind3 As Integer = rdr.GetOrdinal("doc_Codigo")
                Dim ind4 As Integer = rdr.GetOrdinal("doc_FechaEmision")
                Dim ind5 As Integer = rdr.GetOrdinal("doc_FechaVenc")
                Dim ind6 As Integer = rdr.GetOrdinal("RUCODNI")
                Dim ind7 As Integer = rdr.GetOrdinal("RazonSocial")
                Dim ind8 As Integer = rdr.GetOrdinal("FacturasRelacionadas")
                Dim ind9 As Integer = rdr.GetOrdinal("NroUnico")
                Dim ind10 As Integer = rdr.GetOrdinal("doc_Total")
                Dim ind12 As Integer = rdr.GetOrdinal("MontoCancelado")
                Dim ind13 As Integer = rdr.GetOrdinal("Saldo")
                Dim ind11 As Integer = rdr.GetOrdinal("Estado")
                Dim ind14 As Integer = rdr.GetOrdinal("FechaEntregaVendedor")
                Dim ind15 As Integer = rdr.GetOrdinal("FechaRecepcionLetraFirmada")
                Dim ind16 As Integer = rdr.GetOrdinal("FechaEnvioBanco")
                Dim ind17 As Integer = rdr.GetOrdinal("FechaPendientePago")
                Dim ind18 As Integer = rdr.GetOrdinal("FechaRenovacion")
                Dim ind19 As Integer = rdr.GetOrdinal("FechaEnProtesto")
                Dim ind20 As Integer = rdr.GetOrdinal("FechaAnulacion")
                Dim ind21 As Integer = rdr.GetOrdinal("FechaProgramada")
                Dim ind22 As Integer = rdr.GetOrdinal("FechaCancelacion")
                Dim ind23 As Integer = rdr.GetOrdinal("IdEstadoLetra")
                Dim ind24 As Integer = rdr.GetOrdinal("idpersona")
                Dim ind25 As Integer = rdr.GetOrdinal("RecibosIngreso")
                Dim ind26 As Integer = rdr.GetOrdinal("IdVendedorR")
                Dim ind27 As Integer = rdr.GetOrdinal("IdBanco")
                Dim ind28 As Integer = rdr.GetOrdinal("EstadoLetra")
                Dim ind29 As Integer = rdr.GetOrdinal("Nombre")
                Dim ind30 As Integer = rdr.GetOrdinal("CondicionLetra")
                Dim ind31 As Integer = rdr.GetOrdinal("Observacion")
                Dim ind32 As Integer = rdr.GetOrdinal("EstadoLetraCambio")



                Dim objeto As Entidades.be_LetrasxCobrar = Nothing
                While rdr.Read()
                    objeto = New Entidades.be_LetrasxCobrar
                    With objeto

                        .IdDocumento = rdr.GetInt32(ind1)
                        .doc_Serie = rdr.GetString(ind2)
                        .doc_Codigo = rdr.GetString(ind3)
                        .doc_FechaEmision = rdr.GetString(ind4)
                        .doc_FechaVenc = rdr.GetString(ind5)
                        .RUCODNI = rdr.GetString(ind6)
                        .RazonSocial = rdr.GetString(ind7)
                        .FacturasRelacionadas = rdr.GetString(ind8)
                        .NroUnico = rdr.GetString(ind9)
                        .doc_Total = rdr.GetDecimal(ind10)
                        .MontoCancelado = rdr.GetDecimal(ind12)
                        .Saldo = rdr.GetDecimal(ind13)
                        .Estado = rdr.GetString(ind11)
                        .FechaEntregaVendedor = rdr.GetDateTime(ind14)
                        .FechaRecepcionLetraFirmada = rdr.GetDateTime(ind15)
                        .FechaEnvioBanco = rdr.GetDateTime(ind16)
                        .FechaPendientePago = rdr.GetDateTime(ind17)
                        .FechaRenovacion = rdr.GetDateTime(ind18)
                        .FechaEnProtesto = rdr.GetDateTime(ind19)
                        .FechaAnulacion = rdr.GetDateTime(ind20)
                        .FechaProgramada = rdr.GetDateTime(ind21)
                        .FechaCancelacion = rdr.GetDateTime(ind22)
                        .IdEstadoLetra = rdr.GetInt32(ind23)
                        .idpersona = rdr.GetInt32(ind24)
                        .RecibosIngreso = rdr.GetString(ind25)
                        .IdVendedorR = rdr.GetInt32(ind26)
                        .IdBanco = rdr.GetInt32(ind27)
                        .EstadoLetra = rdr.GetInt32(ind28)
                        .Nombre = rdr.GetString(ind29)
                        .CondicionLetra = rdr.GetInt32(ind30)
                        .Observacion = rdr.GetString(ind31)
                        .EstadoLetraCambio = rdr.GetString(ind32)

                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

    Public Function InsertUpdateLetras(ByVal cn As SqlConnection, ByVal IdDocumento As Integer, IdVendedor As Integer, FechaEntregaVend As String, FechaEntregaLFirm As String, FechaEnvioBanco As String, FechaCancelacion As String, FechaProtesto As String, NroUnico As String, IdBanco As Integer, FechaPRogramado As String, FechaRenovacion As String, IdUsuario As Integer, CondicionLetra As Integer)

        Using cmd As New SqlCommand("Insert_UpdateLetrasdeCambio_V1 ", cn)
            With cmd

                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@IdDocumento", SqlDbType.Int)).Value = IdDocumento
                .Parameters.Add(New SqlParameter("@IdVendedor", SqlDbType.Int)).Value = IdVendedor
                .Parameters.Add(New SqlParameter("@FechaEntregaVend", SqlDbType.NVarChar)).Value = FechaEntregaVend
                .Parameters.Add(New SqlParameter("@FechaEntregaLFirm", SqlDbType.NVarChar)).Value = FechaEntregaLFirm
                .Parameters.Add(New SqlParameter("@FechaEnvioBanco", SqlDbType.NVarChar)).Value = FechaEnvioBanco
                '.Parameters.Add(New SqlParameter("@FechaCancelacion", SqlDbType.NVarChar)).Value = FechaCancelacion
                .Parameters.Add(New SqlParameter("@FechaProtesto", SqlDbType.NVarChar)).Value = FechaProtesto
                .Parameters.Add(New SqlParameter("@NroUnico", SqlDbType.NVarChar)).Value = NroUnico
                .Parameters.Add(New SqlParameter("@IdBanco", SqlDbType.Int)).Value = IdBanco
                '.Parameters.Add(New SqlParameter("@FechaPRogramado", SqlDbType.NVarChar)).Value = FechaPRogramado
                .Parameters.Add(New SqlParameter("@FechaRenovacion", SqlDbType.NVarChar)).Value = FechaRenovacion
                .Parameters.Add(New SqlParameter("@IdUsuario", SqlDbType.Int)).Value = IdUsuario
                .Parameters.Add(New SqlParameter("@CondicionLetra", SqlDbType.Int)).Value = CondicionLetra

            End With
            Try
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                Throw ex
            End Try
        End Using


    End Function


End Class
