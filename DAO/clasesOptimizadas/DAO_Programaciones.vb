﻿Imports System.Data.SqlClient
Imports Entidades
Public Class DAO_Programaciones


    Public Function form_listarPagosProgramados(cn As SqlConnection, IdMovCtaPP As Integer, ByVal fechaPagoProgramado As Date) As be_frmCargaPrincipal
        Dim objetoFrmPrincipal As be_frmCargaPrincipal = Nothing

        Using cmd As New SqlCommand("SP_FORM_PAGOS_PROGRAMADOS", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@IdMovCtaPP", SqlDbType.Int)).Value = IdMovCtaPP
                .Parameters.Add(New SqlParameter("@fechaPagoProgrmado", SqlDbType.Date)).Value = fechaPagoProgramado
            End With

            Dim rdr As SqlDataReader = cmd.ExecuteReader()
            If rdr IsNot Nothing Then
                objetoFrmPrincipal = New be_frmCargaPrincipal
                If rdr.HasRows Then
                    Dim objeto As be_ComboGenerico = Nothing
                    Dim lista As New List(Of be_ComboGenerico)

                    Dim ordinal1 As Integer = rdr.GetOrdinal("idBanco")
                    Dim ordinal2 As Integer = rdr.GetOrdinal("ban_nombre")
                    While rdr.Read
                        objeto = New be_ComboGenerico
                        With objeto
                            .campo1 = rdr.GetInt32(ordinal1)
                            .campo2 = rdr.GetString(ordinal2)
                        End With
                        lista.Add(objeto)
                    End While
                    objetoFrmPrincipal.listaBancos = lista
                End If

                If rdr.NextResult Then
                    Dim lista As New List(Of be_cancelacionDocumentos)
                    Dim objeto As be_cancelacionDocumentos = Nothing

                    Dim ordinal1 As Integer = rdr.GetOrdinal("IdMovCtaPP")
                    Dim ordinal2 As Integer = rdr.GetOrdinal("IdProgramacionPago")
                    Dim ordinal3 As Integer = rdr.GetOrdinal("pp_FechaPagoProg")
                    Dim ordinal4 As Integer = rdr.GetOrdinal("pp_MontoPagoProg")
                    Dim ordinal5 As Integer = rdr.GetOrdinal("proveedor")
                    Dim ordinal6 As Integer = rdr.GetOrdinal("IdBanco")
                    Dim ordinal7 As Integer = rdr.GetOrdinal("IdMedioPago")
                    Dim ordinal8 As Integer = rdr.GetOrdinal("IdCuentaBancaria")
                    Dim ordinal9 As Integer = rdr.GetOrdinal("pp_Observacion")
                    Dim ordinal10 As Integer = rdr.GetOrdinal("mp_Nombre")
                    Dim ordinal11 As Integer = rdr.GetOrdinal("ban_Nombre")
                    Dim ordinal12 As Integer = rdr.GetOrdinal("cb_numero")
                    Dim ordinal13 As Integer = rdr.GetOrdinal("IdMoneda")
                    Dim ordinal14 As Integer = rdr.GetOrdinal("mon_Simbolo")
                    Dim ordinal15 As Integer = rdr.GetOrdinal("tipocambio")
                    Dim ordinal16 As Integer = rdr.GetOrdinal("nomMoneda")
                    Dim ordinal17 As Integer = rdr.GetOrdinal("NroDocumentoRef")
                    Dim ordinal18 As Integer = rdr.GetOrdinal("IdDocumentoRef")
                    Dim ordinal19 As Integer = rdr.GetOrdinal("pp_esDetraccion")
                    Dim ordinal20 As Integer = rdr.GetOrdinal("pp_esRetencion")
                    Dim ordinal23 As Integer = rdr.GetOrdinal("idTipoOperacion")
                    Dim ordinal24 As Integer = rdr.GetOrdinal("idPersona")
                    Dim ordinal25 As Integer = rdr.GetOrdinal("pla_cCuentaContable")
                    Dim ordinal26 As Integer = rdr.GetOrdinal("MN")
                    Dim ordinal27 As Integer = rdr.GetOrdinal("ME")
                    Dim ordinal28 As Integer = rdr.GetOrdinal("cadenaDocumentoReferencia")
                    Dim ordinal29 As Integer = rdr.GetOrdinal("flagCancelacion")
                    Dim ordinal30 As Integer = rdr.GetOrdinal("cadenaIdRequerimiento")
                    Dim ordinal31 As Integer = rdr.GetOrdinal("DocRequerimiento")
                    While rdr.Read()
                        objeto = New be_cancelacionDocumentos
                        With objeto
                            .idMovCtaPP = rdr.GetInt32(ordinal1)
                            .IdProgramacionPago = rdr.GetInt32(ordinal2)
                            .pp_FechaPagoProg = rdr.GetDateTime(ordinal3)
                            .pp_MontoPagoProg = rdr.GetDecimal(ordinal4)
                            .proveedor = rdr.GetString(ordinal5)
                            .IdBanco = rdr.GetInt32(ordinal6)
                            .IdMedioPago = rdr.GetInt32(ordinal7)
                            .IdCuentaBancaria = rdr.GetInt32(ordinal8)
                            .pp_Observacion = rdr.GetString(ordinal9)
                            .mp_Nombre = rdr.GetString(ordinal10)
                            .ban_Nombre = rdr.GetString(ordinal11)
                            .cb_numero = rdr.GetString(ordinal12)
                            .IdMoneda = rdr.GetInt32(ordinal13)
                            .mon_Simbolo = rdr.GetString(ordinal14)
                            .tipocambio = rdr.GetDecimal(ordinal15)
                            .nomMoneda = rdr.GetString(ordinal16)
                            .NroDocumentoRef = rdr.GetString(ordinal17)
                            .IdDocumentoRef = rdr.GetInt32(ordinal18)
                            .pp_esDetraccion = rdr.GetBoolean(ordinal19)
                            .pp_esRetencion = rdr.GetBoolean(ordinal20)
                            .idtipoOperacion = rdr.GetInt32(ordinal23)
                            .idProveedor = rdr.GetInt32(ordinal24)
                            .pla_cCuentaContable = rdr.GetString(ordinal25)
                            .monedaNacional = rdr.GetDecimal(ordinal26)
                            .monedaExtranjera = rdr.GetDecimal(ordinal27)
                            .cadenaDocumentoReferencia = rdr.GetString(ordinal28)
                            .flagCancelacion = rdr.GetBoolean(ordinal29)
                            .idRequerimientosCadena = rdr.GetString(ordinal30)
                            .nroRequerimientoCadena = rdr.GetString(ordinal31)
                        End With
                        lista.Add(objeto)
                    End While
                    objetoFrmPrincipal.listaCancelacionesProgramadas = lista
                End If
            End If
        End Using
        Return objetoFrmPrincipal
    End Function

    Public Function filtrarListaProgramaciones(ByVal cn As SqlConnection, ByVal fecInicio As Date, ByVal fecFin As Date, _
                                               ByVal idBanco As Integer, ByVal serie As Integer, ByVal codigo As Integer, _
                                               ByVal flag As Integer) As List(Of be_cancelacionDocumentos)
        Dim lista As List(Of be_cancelacionDocumentos) = Nothing
        Using cmd As New SqlCommand("SP_CANCELACION_DOCUMENTOS", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@flag", SqlDbType.Int)).Value = flag
                .Parameters.Add(New SqlParameter("@fechaInicio", SqlDbType.Date)).Value = fecInicio
                .Parameters.Add(New SqlParameter("@fechaFin", SqlDbType.Date)).Value = fecFin
                .Parameters.Add(New SqlParameter("@idBanco", SqlDbType.Int)).Value = idBanco
                .Parameters.Add(New SqlParameter("@Serie", SqlDbType.Int)).Value = serie
                .Parameters.Add(New SqlParameter("@Codigo", SqlDbType.Int)).Value = codigo
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of be_cancelacionDocumentos)
                Dim objeto As be_cancelacionDocumentos = Nothing

                Dim ordinal1 As Integer = rdr.GetOrdinal("IdMovCtaPP")
                Dim ordinal2 As Integer = rdr.GetOrdinal("IdProgramacionPago")
                Dim ordinal3 As Integer = rdr.GetOrdinal("pp_FechaPagoProg")
                Dim ordinal4 As Integer = rdr.GetOrdinal("pp_MontoPagoProg")
                Dim ordinal5 As Integer = rdr.GetOrdinal("proveedor")
                Dim ordinal6 As Integer = rdr.GetOrdinal("IdBanco")
                Dim ordinal7 As Integer = rdr.GetOrdinal("IdMedioPago")
                Dim ordinal8 As Integer = rdr.GetOrdinal("IdCuentaBancaria")
                Dim ordinal9 As Integer = rdr.GetOrdinal("pp_Observacion")
                Dim ordinal10 As Integer = rdr.GetOrdinal("mp_Nombre")
                Dim ordinal11 As Integer = rdr.GetOrdinal("ban_Nombre")
                Dim ordinal12 As Integer = rdr.GetOrdinal("cb_numero")
                Dim ordinal13 As Integer = rdr.GetOrdinal("IdMoneda")
                Dim ordinal14 As Integer = rdr.GetOrdinal("mon_Simbolo")
                Dim ordinal15 As Integer = rdr.GetOrdinal("tipocambio")
                Dim ordinal16 As Integer = rdr.GetOrdinal("nomMoneda")
                Dim ordinal17 As Integer = rdr.GetOrdinal("NroDocumentoRef")
                Dim ordinal18 As Integer = rdr.GetOrdinal("IdDocumentoRef")
                Dim ordinal19 As Integer = rdr.GetOrdinal("pp_esDetraccion")
                Dim ordinal20 As Integer = rdr.GetOrdinal("pp_esRetencion")
                Dim ordinal23 As Integer = rdr.GetOrdinal("idTipoOperacion")
                Dim ordinal24 As Integer = rdr.GetOrdinal("idPersona")
                Dim ordinal25 As Integer = rdr.GetOrdinal("pla_cCuentaContable")
                Dim ordinal26 As Integer = rdr.GetOrdinal("MN")
                Dim ordinal27 As Integer = rdr.GetOrdinal("ME")
                Dim ordinal28 As Integer = rdr.GetOrdinal("cadenaDocumentoReferencia")
                Dim ordinal29 As Integer = rdr.GetOrdinal("flagCancelacion")
                Dim ordinal30 As Integer = rdr.GetOrdinal("cadenaIdRequerimiento")
                Dim ordinal31 As Integer = rdr.GetOrdinal("DocRequerimiento")

                While rdr.Read()
                    objeto = New be_cancelacionDocumentos
                    With objeto
                        .idMovCtaPP = rdr.GetInt32(ordinal1)
                        .IdProgramacionPago = rdr.GetInt32(ordinal2)
                        .pp_FechaPagoProg = rdr.GetDateTime(ordinal3)
                        .pp_MontoPagoProg = rdr.GetDecimal(ordinal4)
                        .proveedor = rdr.GetString(ordinal5)
                        .IdBanco = rdr.GetInt32(ordinal6)
                        .IdMedioPago = rdr.GetInt32(ordinal7)
                        .IdCuentaBancaria = rdr.GetInt32(ordinal8)
                        .pp_Observacion = rdr.GetString(ordinal9)
                        .mp_Nombre = rdr.GetString(ordinal10)
                        .ban_Nombre = rdr.GetString(ordinal11)
                        .cb_numero = rdr.GetString(ordinal12)
                        .IdMoneda = rdr.GetInt32(ordinal13)
                        .mon_Simbolo = rdr.GetString(ordinal14)
                        .tipocambio = rdr.GetDecimal(ordinal15)
                        .nomMoneda = rdr.GetString(ordinal16)
                        .NroDocumentoRef = rdr.GetString(ordinal17)
                        .IdDocumentoRef = rdr.GetInt32(ordinal18)
                        .pp_esDetraccion = rdr.GetBoolean(ordinal19)
                        .pp_esRetencion = rdr.GetBoolean(ordinal20)
                        .idtipoOperacion = rdr.GetInt32(ordinal23)
                        .idProveedor = rdr.GetInt32(ordinal24)
                        .pla_cCuentaContable = rdr.GetString(ordinal25)
                        .monedaNacional = rdr.GetDecimal(ordinal26)
                        .monedaExtranjera = rdr.GetDecimal(ordinal27)
                        .cadenaDocumentoReferencia = rdr.GetString(ordinal28)
                        .flagCancelacion = rdr.GetBoolean(ordinal29)
                        .idRequerimientosCadena = rdr.GetString(ordinal30)
                        .nroRequerimientoCadena = rdr.GetString(ordinal31)
                    End With
                    lista.Add(objeto)
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

    Public Function listarPagosxCancelar(ByVal cn As SqlConnection, ByVal idCadenaRequerimiento As String, ByVal esDetraccion As Boolean, ByVal idProgramacion As Integer) As Entidades.be_cancelacionDocumentos
        Dim objetoDeudasPendientes As New Entidades.be_cancelacionDocumentos
        Using cmd As New SqlCommand("SP_LISTAR_PAGO_CANCELACION_PENDIENTE", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@CadenaidRequerimiento", SqlDbType.NVarChar)).Value = idCadenaRequerimiento
                .Parameters.Add(New SqlParameter("@flag_detraccion", SqlDbType.Bit)).Value = esDetraccion
                .Parameters.Add(New SqlParameter("@idProgramacion", SqlDbType.Int)).Value = idProgramacion
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader()
            If rdr IsNot Nothing Then
                Dim listaCancelacion As New List(Of Entidades.Documento_MovCuentaPorPagar)
                Dim objetoCancelacion As Entidades.Documento_MovCuentaPorPagar = Nothing

                Dim ordinal1 As Integer = rdr.GetOrdinal("IdDocumento")
                Dim ordinal2 As Integer = rdr.GetOrdinal("idTipoDocumento")
                Dim ordinal3 As Integer = rdr.GetOrdinal("codigo")
                Dim ordinal4 As Integer = rdr.GetOrdinal("doc_Fechaemision")
                Dim ordinal5 As Integer = rdr.GetOrdinal("doc_FechaVenc")
                Dim ordinal6 As Integer = rdr.GetOrdinal("NroDiasMora")
                Dim ordinal7 As Integer = rdr.GetOrdinal("doc_TotalAPagar")
                Dim ordinal8 As Integer = rdr.GetOrdinal("tdoc_NombreLargo")
                Dim ordinal9 As Integer = rdr.GetOrdinal("mon_Simbolo")
                Dim ordinal10 As Integer = rdr.GetOrdinal("Total")
                Dim ordinal11 As Integer = rdr.GetOrdinal("Retencion")
                Dim ordinal12 As Integer = rdr.GetOrdinal("Detraccion")
                'Dim ordinal12 As Integer = rdr.GetOrdinal("montoProgramado")                

                While rdr.Read()
                    objetoCancelacion = New Documento_MovCuentaPorPagar
                    With objetoCancelacion
                        .IdDocumento = rdr.GetInt32(ordinal1)
                        .IdTipoDocumento = rdr.GetInt32(ordinal2)
                        .NomTipoDocumento = rdr.GetString(ordinal8)
                        .Codigo = rdr.GetString(ordinal3)
                        .FechaEmision = rdr.GetDateTime(ordinal4)
                        .FechaVenc = rdr.GetDateTime(ordinal5)
                        .NroDiasMora = rdr.GetInt32(ordinal6)
                        .MontoTotal = rdr.GetDecimal(ordinal7)
                        .NomMoneda = rdr.GetString(ordinal9)
                        .Total = rdr.GetDecimal(ordinal10)
                        .Retencion = rdr.GetDecimal(ordinal11)
                        .Detraccion = rdr.GetDecimal(ordinal12)
                        '.montoProgramado = rdr.GetDecimal(ordinal12)
                    End With
                    listaCancelacion.Add(objetoCancelacion)
                End While
                objetoDeudasPendientes.listaCuentasxPagar = listaCancelacion
                If rdr.NextResult() Then
                    Dim listaAplicaciones As New List(Of Entidades.be_Requerimiento_x_pagar)
                    Dim objetoAplicaciones As Entidades.be_Requerimiento_x_pagar = Nothing
                    While rdr.Read()
                        objetoAplicaciones = New Entidades.be_Requerimiento_x_pagar
                        With objetoAplicaciones
                            .nom_simbolo = rdr.GetString(0)
                            .idDocumento = rdr.GetInt32(1)
                            .tipoDocumento = rdr.GetString(2)
                            .doc_codigo = rdr.GetString(3)
                            .doc_TotalAPagar = rdr.GetDecimal(4)
                        End With
                        listaAplicaciones.Add(objetoAplicaciones)
                    End While
                    objetoDeudasPendientes.listaFacturasAplicadas = listaAplicaciones
                End If
                rdr.Close()
            End If
        End Using
        Return objetoDeudasPendientes
    End Function

    Public Function listarMediodePagos(ByVal cn As SqlConnection, ByVal idDocumentodePago As Integer) As List(Of DocumentoCheque)
        Dim lista As List(Of DocumentoCheque) = Nothing
        Using cmd As New SqlCommand("SP_SELECT_MEDIOPAGO_CANCELACIONES", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@idDocumentoMedioPago", SqlDbType.Int)).Value = idDocumentodePago
            End With

            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.DocumentoCheque)
                Dim ordinal1 As Integer = rdr.GetOrdinal("IdDocumento")
                Dim ordinal2 As Integer = rdr.GetOrdinal("ChequeNumero")
                Dim ordinal3 As Integer = rdr.GetOrdinal("IdBeneficiario")
                Dim ordinal4 As Integer = rdr.GetOrdinal("Beneficiario")
                Dim ordinal5 As Integer = rdr.GetOrdinal("Monto")
                Dim ordinal6 As Integer = rdr.GetOrdinal("IdMoneda")
                Dim ordinal7 As Integer = rdr.GetOrdinal("doc_Serie")
                Dim ordinal8 As Integer = rdr.GetOrdinal("FechaACobrar")
                Dim ordinal9 As Integer = rdr.GetOrdinal("fechaemision")
                Dim ordinal10 As Integer = rdr.GetOrdinal("FechaRegistro")
                Dim ordinal11 As Integer = rdr.GetOrdinal("IdSerieCheque")
                Dim ordinal12 As Integer = rdr.GetOrdinal("IdEstadoDoc")
                Dim ordinal13 As Integer = rdr.GetOrdinal("IdEstadoEnt")
                Dim ordinal14 As Integer = rdr.GetOrdinal("IdEstadoCan")
                Dim ordinal15 As Integer = rdr.GetOrdinal("NomEstadoDocumento")
                Dim ordinal16 As Integer = rdr.GetOrdinal("NomEstadoEntregado")
                Dim ordinal17 As Integer = rdr.GetOrdinal("NomEstadoCancelacion")
                Dim ordinal18 As Integer = rdr.GetOrdinal("doc_TotalLetras")
                Dim ordinal19 As Integer = rdr.GetOrdinal("idbanco")
                Dim ordinal20 As Integer = rdr.GetOrdinal("idCuentaBancaria")

                Dim objetoCheque As Entidades.DocumentoCheque = Nothing

                While rdr.Read()
                    objetoCheque = New Entidades.DocumentoCheque

                    With objetoCheque
                        .Id = rdr.GetInt32(ordinal1)
                        .ChequeNumero = rdr.GetString(ordinal2)
                        .IdBeneficiario = rdr.GetInt32(ordinal3)
                        .Beneficiario = rdr.GetString(ordinal4)
                        .Monto = rdr.GetDecimal(ordinal5)
                        .IdMoneda = rdr.GetInt32(ordinal6)
                        .Serie = rdr.GetString(ordinal17)
                        .FechaACobrar = rdr.GetDateTime(ordinal8)
                        .FechaEmision = rdr.GetDateTime(ordinal9)
                        .FechaRegistro = rdr.GetDateTime(ordinal10)
                        .IdSerieCheque = rdr.GetInt32(ordinal11)
                        .IdEstadoDoc = rdr.GetInt32(ordinal12)
                        .IdEstadoEntrega = rdr.GetInt32(ordinal13)
                        .IdEstadoCancelacion = rdr.GetInt32(ordinal14)
                        .NomEstadoDocumento = rdr.GetString(ordinal15)
                        .NomEstadoEntregado = rdr.GetString(ordinal16)
                        .NomEstadoCancelacion = rdr.GetString(ordinal17)
                        .TotalLetras = rdr.GetString(ordinal18)
                        .SerieCheque.IdBanco = rdr.GetInt32(ordinal19)
                        .SerieCheque.IdCuentaBancaria = rdr.GetInt32(ordinal20)
                    End With
                    lista.Add(objetoCheque)
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

    Public Function llenarComboCuentaBancariaDetraccionPorProveedor(ByVal cn As SqlConnection, ByVal idProveedor As Integer) As List(Of be_ComboGenerico)
        Dim lista As List(Of be_ComboGenerico) = Nothing
        Using cmd As New SqlCommand("select * from fun_CuentaDetraccion(@idPersona)", cn)
            With cmd
                .CommandType = CommandType.Text

                .Parameters.Add(New SqlParameter("@idPersona", SqlDbType.Int)).Value = idProveedor
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of be_ComboGenerico)
                Dim objeto As be_ComboGenerico = Nothing
                While rdr.Read()
                    objeto = New be_ComboGenerico
                    objeto.campo1 = rdr.GetInt32(0)
                    objeto.campo2 = rdr.GetString(1)
                    lista.Add(objeto)
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

    Public Function registraDetraccionComodocExterno(ByVal cn As SqlConnection, ByVal objeto As Entidades.Documento, ByVal tr As SqlTransaction) As List(Of Entidades.Documento)
        Dim lista As List(Of Documento) = Nothing
        Using cmd As New SqlCommand("SP_REGISTRA_DETRACCION", cn, tr)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@nroConstancia", SqlDbType.NVarChar, 25)).Value = objeto.NroDocumento
                .Parameters.Add(New SqlParameter("@fechaPago", SqlDbType.DateTime)).Value = objeto.FechaEmision
                .Parameters.Add(New SqlParameter("@montoDeposito", SqlDbType.Decimal)).Value = objeto.Total
                .Parameters.Add(New SqlParameter("@idUsuario", SqlDbType.Int)).Value = objeto.IdUsuario
                .Parameters.Add(New SqlParameter("@idPersona", SqlDbType.Int)).Value = objeto.IdPersona
                .Parameters.Add(New SqlParameter("@idtienda", SqlDbType.Int)).Value = objeto.IdTienda
                .Parameters.Add(New SqlParameter("@idRequerimientoCadena", SqlDbType.NVarChar)).Value = objeto.strIdRequerimientoCadena
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleRow)
            If rdr IsNot Nothing Then
                lista = New List(Of Documento)
                Dim objetoDocumento As Documento = Nothing
                Dim ordinal1 As Integer = rdr.GetOrdinal("tdoc_NombreLargo")
                Dim ordinal2 As Integer = rdr.GetOrdinal("NroComprobante")
                Dim ordinal3 As Integer = rdr.GetOrdinal("anex_MontoNoAfectoIgv")
                Dim ordinal4 As Integer = rdr.GetOrdinal("idDocumento")
                While rdr.Read()
                    objetoDocumento = New Documento
                    objetoDocumento.NomTipoDocumento = rdr.GetString(ordinal1)
                    objetoDocumento.NroDocumento = rdr.GetString(ordinal2)
                    objetoDocumento.TotalAPagar = rdr.GetDecimal(ordinal3)
                    objetoDocumento.IdDocumento = rdr.GetInt32(ordinal4)
                    lista.Add(objetoDocumento)
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

    Public Sub crearBancoDetraccionxProveedor(ByVal cn As SqlConnection, idBanco As Integer, idPersona As Integer, cb_numero As String, cuentaContable As String,
                                              cb_cuentaInterbancaria As String, cb_swiftbancario As String, idMoneda As Integer)
        Using cmd As New SqlCommand("SP_CREAR_CUENTA_DETRACCION", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@idbanco", SqlDbType.Int)).Value = idBanco
                .Parameters.Add(New SqlParameter("@idPersona", SqlDbType.Int)).Value = idPersona
                .Parameters.Add(New SqlParameter("@cb_numero", SqlDbType.NVarChar)).Value = cb_numero
                .Parameters.Add(New SqlParameter("@cuentaContable", SqlDbType.Char)).Value = cuentaContable
                .Parameters.Add(New SqlParameter("@cb_CuentaInterbancaria", SqlDbType.NVarChar)).Value = cb_cuentaInterbancaria
                .Parameters.Add(New SqlParameter("@cb_SwiftBancario", SqlDbType.NVarChar)).Value = cb_swiftbancario
                .Parameters.Add(New SqlParameter("@idmoneda", SqlDbType.Int)).Value = idMoneda
            End With
            Try
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Sub

    Public Function buscarTipoCambioCompra(ByVal cn As SqlConnection, ByVal fechaPago As Date) As Decimal
        Dim tc As Decimal = 0
        Using cmd As New SqlCommand("select dbo.fx_getTipoCambioComVentOFXFecha('V',2,@FechaRegistro)", cn)
            With cmd
                .CommandType = CommandType.Text

                .Parameters.Add(New SqlParameter("@FechaRegistro", SqlDbType.Date)).Value = fechaPago
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleRow)
            If rdr IsNot Nothing Then
                While rdr.Read()
                    tc = rdr.GetDecimal(0)
                End While
                rdr.Close()
            End If
        End Using
        Return tc
    End Function

    Public Function BuscaDetraccionxProgramacion(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal idRequerimiento As String) As List(Of Documento)
        Dim lista As List(Of Documento) = Nothing
        Using cmd As New SqlCommand("SP_BUSCA_DETRACCION_X_PROGRAMACION", cn, tr)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@idRequerimiento", SqlDbType.NVarChar)).Value = idRequerimiento
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleRow)
            If rdr IsNot Nothing Then
                lista = New List(Of Documento)
                Dim objetoDocumento As Documento = Nothing
                Dim ordinal1 As Integer = rdr.GetOrdinal("tdoc_NombreLargo")
                Dim ordinal2 As Integer = rdr.GetOrdinal("NroComprobante")
                Dim ordinal3 As Integer = rdr.GetOrdinal("anex_MontoNoAfectoIgv")
                Dim ordinal4 As Integer = rdr.GetOrdinal("iddocumento")
                While rdr.Read()
                    objetoDocumento = New Documento
                    objetoDocumento.NomTipoDocumento = rdr.GetString(ordinal1)
                    objetoDocumento.NroDocumento = rdr.GetString(ordinal2)
                    objetoDocumento.TotalAPagar = rdr.GetDecimal(ordinal3)
                    objetoDocumento.IdDocumento = rdr.GetInt32(ordinal4)
                    lista.Add(objetoDocumento)
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

    Public Sub DeleteRegistroDetraccion(ByVal cn As SqlConnection, idDocumento As Integer)
        Using cmd As New SqlCommand("SP_DELETE_DETRACCION", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@idDocumento", SqlDbType.Int)).Value = idDocumento
            End With
            Try
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Sub

    Public Function buscarRelacionConDocumentoRetencion(ByVal cn As SqlConnection, ByVal cadenaDocumentosRef As String, ByVal tc As Decimal) As List(Of be_cancelacionDocumentos)
        Dim lista As List(Of be_cancelacionDocumentos) = Nothing
        Using cmd As New SqlCommand("SP_DOC_REF_RETENCION", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@cadenaDocumentosRef", SqlDbType.NVarChar)).Value = cadenaDocumentosRef
                .Parameters.Add(New SqlParameter("@tc", SqlDbType.NVarChar)).Value = tc
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of be_cancelacionDocumentos)
                Dim ordinal1 As Integer = rdr.GetOrdinal("idtienda")
                Dim ordinal2 As Integer = rdr.GetOrdinal("idTipoDocumento")
                Dim ordinal3 As Integer = rdr.GetOrdinal("idMoneda")
                Dim ordinal4 As Integer = rdr.GetOrdinal("Tienda")
                Dim ordinal5 As Integer = rdr.GetOrdinal("tipoDocumento")
                Dim ordinal6 As Integer = rdr.GetOrdinal("nroDocumento")
                Dim ordinal7 As Integer = rdr.GetOrdinal("doc_fechaEmision")
                Dim ordinal8 As Integer = rdr.GetOrdinal("idDocumento")

                Dim ordinal9 As Integer = rdr.GetOrdinal("montoME")
                Dim ordinal10 As Integer = rdr.GetOrdinal("montoMN")
                Dim ordinal11 As Integer = rdr.GetOrdinal("monedaN")
                Dim ordinal12 As Integer = rdr.GetOrdinal("monedaE")
                Dim ordinal13 As Integer = rdr.GetOrdinal("montoRetenido")
                Dim ordinal14 As Integer = rdr.GetOrdinal("monedaOriginal")
                Dim objDocumento As be_cancelacionDocumentos = Nothing

                While rdr.Read()
                    objDocumento = New be_cancelacionDocumentos
                    With objDocumento
                        .IdTienda = rdr.GetInt32(ordinal1)
                        .IdTipoDocumento = rdr.GetInt32(ordinal2)
                        .IdMoneda = rdr.GetInt32(ordinal3)
                        .Tienda = rdr.GetString(ordinal4)
                        .NomTipoDocumento = rdr.GetString(ordinal5)
                        .Codigo = rdr.GetString(ordinal6)
                        .FechaEmision = rdr.GetDateTime(ordinal7)
                        .IdDocumento = rdr.GetInt32(ordinal8)
                        .monedaExtranjera = rdr.GetDecimal(ordinal9)
                        .monedaNacional = rdr.GetDecimal(ordinal10)
                        .monedasimboloN = rdr.GetString(ordinal11)
                        .monedasimboloE = rdr.GetString(ordinal12)
                        .montoRetenido = rdr.GetDecimal(ordinal13)
                        .monedaOriginal = rdr.GetString(ordinal14)
                    End With
                    lista.Add(objDocumento)
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

    Public Function ListarConceptoAdelantos(ByVal cn As SqlConnection, ByVal flag As String) As List(Of Concepto)
        Dim str As String = "select * from dbo.func_Anticipos()"
        Dim lista As List(Of Entidades.Concepto) = Nothing
        Using cmd As New SqlCommand(str, cn)
            cmd.CommandType = CommandType.Text

            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.Concepto)
                Dim objeto As Concepto = Nothing
                While rdr.Read()
                    objeto = New Concepto
                    objeto.Id = rdr.GetInt32(0)
                    objeto.Nombre = rdr.GetString(1)
                    lista.Add(objeto)
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

    Public Function ListarcuentasPorRendir(ByVal cn As SqlConnection, ByVal flag As String) As List(Of Concepto)
        Dim str As String = "select * from dbo.func_cuentas_por_rendir(@flag)"
        Dim lista As List(Of Entidades.Concepto) = Nothing
        Using cmd As New SqlCommand(str, cn)
            cmd.CommandType = CommandType.Text
            cmd.Parameters.Add(New SqlParameter("@flag", SqlDbType.NVarChar)).Value = flag

            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.Concepto)
                Dim objeto As Concepto = Nothing
                While rdr.Read()
                    objeto = New Concepto
                    objeto.Id = rdr.GetInt32(0)
                    objeto.Nombre = rdr.GetString(1)
                    lista.Add(objeto)
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

    Public Function ListarPagoProveedor(ByVal cn As SqlConnection, ByVal flag As String) As List(Of Concepto)
        Dim str As String = "select * from dbo.func_PagoProveedor(@flag)"
        Dim lista As List(Of Entidades.Concepto) = Nothing
        Using cmd As New SqlCommand(str, cn)
            cmd.CommandType = CommandType.Text
            cmd.Parameters.Add(New SqlParameter("@flag", SqlDbType.NVarChar)).Value = flag

            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.Concepto)
                Dim objeto As Concepto = Nothing
                While rdr.Read()
                    objeto = New Concepto
                    objeto.Id = rdr.GetInt32(0)
                    objeto.Nombre = rdr.GetString(1)
                    lista.Add(objeto)
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

    Public Sub CancelarProgramacion(ByVal cn As SqlConnection, ByVal idProgramacion As Integer, ByVal nroOperacion As String, ByVal montoDeposito As Decimal, _
                                    ByVal idbanco As Integer, ByVal fechaPago As Date, ByVal idPersona As Integer)

        Using cmd As New SqlCommand("SP_CANCELACION_PROGRAMACION", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@idProgramacion", SqlDbType.Int)).Value = idProgramacion
                .Parameters.Add(New SqlParameter("@can_NroOperacion", SqlDbType.NVarChar)).Value = nroOperacion
                .Parameters.Add(New SqlParameter("@can_montoDeposito", SqlDbType.Decimal)).Value = montoDeposito
                .Parameters.Add(New SqlParameter("@can_idBanco", SqlDbType.Int)).Value = idbanco
                .Parameters.Add(New SqlParameter("@can_fechaPago", SqlDbType.NVarChar)).Value = fechaPago
                .Parameters.Add(New SqlParameter("@idPersonaXRendir", SqlDbType.Int)).Value = idPersona
            End With
            Try
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Sub

    Public Function CancelarProgramacionMasivos(ByVal cn As SqlConnection, ByVal fechainicio As Date, ByVal fechafin As Date, ByVal idProgramacion As Integer, ByVal nroOperacion As String, ByVal montoDeposito As Decimal, _
                                ByVal idbanco As Integer, ByVal fechaPago As Date, ByVal idPersona As Integer, ByVal idUsuario As Integer) As String
        Dim nroVoucher As String = String.Empty
    
        Using cmd As New SqlCommand("SP_CANCELACION_PROGRAMACION_MASIVOS", cn)

            With cmd
                cmd.CommandTimeout = 200
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@FechaInicio", SqlDbType.Date)).Value = fechainicio
                .Parameters.Add(New SqlParameter("@FechaFin", SqlDbType.Date)).Value = fechafin
                .Parameters.Add(New SqlParameter("@iddocumento", SqlDbType.Int)).Value = idProgramacion
                .Parameters.Add(New SqlParameter("@can_NroOperacion", SqlDbType.NVarChar)).Value = nroOperacion
                .Parameters.Add(New SqlParameter("@can_montoDeposito", SqlDbType.Decimal)).Value = montoDeposito
                .Parameters.Add(New SqlParameter("@can_idBanco", SqlDbType.Int)).Value = idbanco
                .Parameters.Add(New SqlParameter("@can_fechaPago", SqlDbType.NVarChar)).Value = fechaPago
                .Parameters.Add(New SqlParameter("@idPersonaXRendir", SqlDbType.Int)).Value = idPersona
                .Parameters.Add(New SqlParameter("@idUsuario", SqlDbType.Int)).Value = idUsuario
            End With
            Try
                Dim rdr As SqlDataReader = cmd.ExecuteReader()
                If rdr IsNot Nothing Then
                    Dim ordinal As Integer = rdr.GetOrdinal("doc_NroVoucherConta")
                    While rdr.Read()
                        nroVoucher = rdr.GetString(ordinal)
                    End While
                    rdr.Close()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Using
                Return nroVoucher
    End Function

    Public Function CancelarDetraccionesLotes(ByVal cn As SqlConnection, ByVal Lote As Integer, ByVal Periodo As Integer, ByVal idbanco As Integer, ByVal nroOperacion As String, ByVal montoDeposito As Decimal, _
                              ByVal FechaCancelacion As Date, ByVal idUsuario As Integer) As String
        Dim nroVoucher As String = String.Empty

        Using cmd As New SqlCommand("SP_CANCELACION_PROGRAMACION_DETRACCIONES_MASIVOS", cn)
            cmd.CommandTimeout = 200
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@Lote", SqlDbType.Int)).Value = Lote
                .Parameters.Add(New SqlParameter("@Periodo", SqlDbType.Int)).Value = Periodo
                .Parameters.Add(New SqlParameter("@can_idBanco", SqlDbType.Int)).Value = idbanco
                .Parameters.Add(New SqlParameter("@can_NroOperacion", SqlDbType.NVarChar)).Value = nroOperacion
                .Parameters.Add(New SqlParameter("@can_montoDeposito", SqlDbType.Decimal)).Value = montoDeposito
                .Parameters.Add(New SqlParameter("@can_fechaPago", SqlDbType.NVarChar)).Value = FechaCancelacion
                .Parameters.Add(New SqlParameter("@idUsuario", SqlDbType.Int)).Value = idUsuario
            End With
            Try
                Dim rdr As SqlDataReader = cmd.ExecuteReader()
                If rdr IsNot Nothing Then
                    Dim ordinal As Integer = rdr.GetOrdinal("doc_NroVoucherConta")
                    While rdr.Read()
                        nroVoucher = rdr.GetString(ordinal)
                    End While
                    rdr.Close()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Using
        Return nroVoucher
    End Function

    'Public Function crearAsientoContableProgramacion(ByVal cn As SqlConnection, ByVal idDocumento As Integer, ByVal idProgramacion As Integer, ByVal idpersona As Integer) As String
    '    Dim nroVoucher As String = String.Empty
    '    Using cmd As New SqlCommand("SP_ASIENTO_CONTABLE_FINAL", cn)
    '        With cmd
    '            .CommandType = CommandType.StoredProcedure

    '            .Parameters.Add(New SqlParameter("@idpersona", SqlDbType.Int)).Value = idpersona
    '            .Parameters.Add(New SqlParameter("@idDocumentoxCancelar", SqlDbType.Int)).Value = idProgramacion
    '            .Parameters.Add(New SqlParameter("@idDocumento", SqlDbType.Int)).Value = idDocumento
    '        End With
    '        Try
    '            Dim rdr As SqlDataReader = cmd.ExecuteReader()
    '            If rdr IsNot Nothing Then
    '                Dim ordinal As Integer = rdr.GetOrdinal("doc_NroVoucherConta")
    '                While rdr.Read()
    '                    nroVoucher = rdr.GetString(ordinal)
    '                End While
    '                rdr.Close()
    '            End If
    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End Using
    '    Return nroVoucher
    'End Function

    Public Sub CancelarProgramacionDetraccion(ByVal cn As SqlConnection, ByVal idDocumento As Integer, ByVal nroOperacion As String, ByVal montoDeposito As Decimal, _
                                ByVal idbanco As Integer, ByVal fechaPago As Date)

        Using cmd As New SqlCommand("SP_CANCELACION_DETRA_PROGRAMACION", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@idDocumentoRef", SqlDbType.Int)).Value = idDocumento
                .Parameters.Add(New SqlParameter("@can_NroOperacion", SqlDbType.NVarChar)).Value = nroOperacion
                .Parameters.Add(New SqlParameter("@can_montoDeposito", SqlDbType.Decimal)).Value = montoDeposito
                .Parameters.Add(New SqlParameter("@can_idBanco", SqlDbType.Int)).Value = idbanco
                .Parameters.Add(New SqlParameter("@can_fechaPago", SqlDbType.NVarChar)).Value = fechaPago
            End With
            Try
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Sub

    Public Function crearAsientoContableProgramacion(ByVal cn As SqlConnection, ByVal idDocumento As Integer, ByVal idProgramacion As Integer, ByVal idpersona As Integer) As String
        Dim nroVoucher As String = String.Empty
        Using cmd As New SqlCommand("SP_ASIENTO_CONTABLE_FINAL", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@idpersona", SqlDbType.Int)).Value = idpersona
                .Parameters.Add(New SqlParameter("@idDocumentoxCancelar", SqlDbType.Int)).Value = idProgramacion
                .Parameters.Add(New SqlParameter("@idDocumento", SqlDbType.Int)).Value = idDocumento
            End With
            Try
                Dim rdr As SqlDataReader = cmd.ExecuteReader()
                If rdr IsNot Nothing Then
                    Dim ordinal As Integer = rdr.GetOrdinal("doc_NroVoucherConta")
                    While rdr.Read()
                        nroVoucher = rdr.GetString(ordinal)
                    End While
                    rdr.Close()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Using
        Return nroVoucher
    End Function

    Public Function listarTrabajadores(ByVal cn As SqlConnection) As DataTable
        Dim dt As New DataTable
        Using cmd As New SqlCommand("SP_LISTARTRABAJADORES", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure
            End With
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)

            Return dt
        End Using
    End Function

    Public Function listarDeudasPendientes(ByVal cn As SqlConnection, ByVal fechaInicio As String, ByVal fechaFinal As String, ByVal estadoPRovision As Integer _
                                           , ByVal proveedor As String, ByVal nroDocumento As String) As List(Of Entidades.Programacion)
        Dim listaProgramacion As New List(Of Entidades.Programacion)
        Using cmd As New SqlCommand("SP_REPORTE_PROGRAM_DEUDAXPAGAR", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@fechaEmisionDocumentoInicio", SqlDbType.NVarChar)).Value = fechaInicio
                .Parameters.Add(New SqlParameter("@fechaEmisionDocumentoFinal", SqlDbType.NVarChar)).Value = fechaFinal
                .Parameters.Add(New SqlParameter("@pendientePago", SqlDbType.Int)).Value = estadoPRovision
                .Parameters.Add(New SqlParameter("@proveedor", SqlDbType.NVarChar)).Value = proveedor
                .Parameters.Add(New SqlParameter("@nroDocumento", SqlDbType.NVarChar)).Value = nroDocumento
            End With

            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                Dim objetoProgramacion As Entidades.Programacion = Nothing
                While rdr.Read()
                    objetoProgramacion = New Entidades.Programacion
                    Dim or1 As Integer = rdr.GetOrdinal("IdDocumento")
                    Dim or2 As Integer = rdr.GetOrdinal("doc_FechaEmision")
                    Dim or3 As Integer = rdr.GetOrdinal("codigo")
                    Dim or4 As Integer = rdr.GetOrdinal("Proveedor")
                    Dim or5 As Integer = rdr.GetOrdinal("doc_TotalFactura")
                    Dim or6 As Integer = rdr.GetOrdinal("nroDiasPlazo")
                    Dim or7 As Integer = rdr.GetOrdinal("doc_FechaVenc")
                    Dim or8 As Integer = rdr.GetOrdinal("MedioPago")
                    Dim or9 As Integer = rdr.GetOrdinal("fechaCancelacion")
                    Dim or10 As Integer = rdr.GetOrdinal("Banco")
                    Dim or11 As Integer = rdr.GetOrdinal("nroOperacion")
                    Dim or12 As Integer = rdr.GetOrdinal("MontoCancelado")
                    Dim or13 As Integer = rdr.GetOrdinal("nroOperacionDET")
                    Dim or14 As Integer = rdr.GetOrdinal("montoCanceladoDET")
                    Dim or15 As Integer = rdr.GetOrdinal("Retención")
                    Dim or16 As Integer = rdr.GetOrdinal("NroRequerimiento")
                    Dim or17 As Integer = rdr.GetOrdinal("estado")


                    With objetoProgramacion
                        .idDocumento = rdr.GetInt32(or1)
                        .docFechaEmision = rdr.GetDateTime(or2)
                        .codigo = rdr.GetString(or3)
                        .proveedor = rdr.GetString(or4)
                        .totalFactura = rdr.GetDecimal(or5)
                        .nroDiasPlazo = rdr.GetInt32(or6)
                        .doc_fechaVEncimiento = rdr.GetString(or7)
                        .medioPago = rdr.GetString(or8)
                        .doc_fechaCancelacion = rdr.GetString(or9)
                        .banco = rdr.GetString(or10)
                        .nroOperacion = rdr.GetString(or11)
                        .montoCancelado = rdr.GetDecimal(or12)
                        .nroOperacionDET = rdr.GetString(or13)
                        .montoCanceladoDET = rdr.GetDecimal(or14)
                        .retencion = rdr.GetDecimal(or15)
                        .nroRequerimiento = rdr.GetString(or16)
                        .estado = rdr.GetString(or17)

                        listaProgramacion.Add(objetoProgramacion)
                    End With

                End While
                rdr.Close()
            End If
        End Using
        Return listaProgramacion
    End Function

    Public Function validaSiGuiaFueProgramada(ByVal cn As SqlConnection, ByVal idDocumento As Integer)
        Dim retornar As Boolean = False
        Using cmd As New SqlCommand("SP_VERIFICA_DOCUMENTO_PROGRAMACION", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@idDocumento", SqlDbType.Int)).Value = idDocumento
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleRow)
            If rdr IsNot Nothing Then
                While rdr.Read()
                    retornar = rdr.GetBoolean(0)
                End While
                rdr.Close()
            End If
        End Using
        Return retornar
    End Function

    Public Function CreacionAsientosContable(ByVal cn As SqlConnection, ByVal periodocanc As Integer, ByVal Banco As Integer, ByVal Moneda As Integer, ByVal FechaAsiento As String, ByVal idUsuario As Integer) As String
        Dim nroVoucher As String = String.Empty
        Using cmd As New SqlCommand("SP_ASIENTOS_REMESAS", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@PeriodoCanc", SqlDbType.NVarChar)).Value = periodocanc
                .Parameters.Add(New SqlParameter("@can_idBanco", SqlDbType.Int)).Value = Banco
                .Parameters.Add(New SqlParameter("@Moneda", SqlDbType.Int)).Value = Moneda
                .Parameters.Add(New SqlParameter("@can_fechaPago", SqlDbType.NVarChar)).Value = FechaAsiento
                .Parameters.Add(New SqlParameter("@idUsuario", SqlDbType.Int)).Value = idUsuario
            End With
            Try
                Dim rdr As SqlDataReader = cmd.ExecuteReader()
                If rdr IsNot Nothing Then
                    Dim ordinal As Integer = rdr.GetOrdinal("doc_NroVoucherConta")
                    While rdr.Read()
                        nroVoucher = rdr.GetString(ordinal)
                    End While
                    rdr.Close()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Using
        Return nroVoucher

    End Function

   
End Class
