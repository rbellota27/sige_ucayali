﻿Imports Entidades
Imports System.Data.SqlClient
Public Class DAO_Montos


    Public Function selectSumarTotales(ByVal cn As SqlConnection, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdArea As Integer, ByVal Opcion_Aprobacion As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal monto As Decimal) As List(Of Entidades.be_Montos)

        Dim lista As List(Of Entidades.be_Montos) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "_SelectSumDocumentoProgramacionPago_Aprobacion"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@IdEmpresa", SqlDbType.Int)).Value = IdEmpresa
                .Parameters.Add(New SqlParameter("@IdTienda", SqlDbType.Int)).Value = IdTienda
                .Parameters.Add(New SqlParameter("@IdArea", SqlDbType.Int)).Value = IdArea
                .Parameters.Add(New SqlParameter("@Opcion_Aprobacion", SqlDbType.Int)).Value = Opcion_Aprobacion
                .Parameters.Add(New SqlParameter("@FechaInicio", SqlDbType.Date)).Value = FechaInicio
                .Parameters.Add(New SqlParameter("@FechaFin", SqlDbType.Date)).Value = FechaFin
                .Parameters.Add(New SqlParameter("@monto", SqlDbType.Decimal)).Value = monto

            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_Montos)

                Dim ind12 As Decimal = rdr.GetOrdinal("Totales")

                Dim objeto As Entidades.be_Montos = Nothing

                While rdr.Read()

                    objeto = New Entidades.be_Montos
                    With objeto

                        .TotalAPagar = rdr.GetDecimal(ind12)

                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function





End Class
