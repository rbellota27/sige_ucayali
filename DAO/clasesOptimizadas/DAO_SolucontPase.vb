﻿Imports Entidades
Imports System.Data.SqlClient
Public Class DAO_SolucontPase
    Public Overridable Function listaDocumentos_provisionados(ByVal cn As SqlConnection, ByVal fechaInicio As String, ByVal fechaFin As String, _
                                                  ByVal idEmpresa As String, ByVal anio As String, ByVal moneda As String) As List(Of Entidades.be_provision)
        Dim lista As List(Of Entidades.be_provision) = Nothing
        Using cmd As New SqlCommand("SP_BUSCAR_PROVISIONES", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                .Parameters.Add(New SqlParameter("@Emp_cCodigo", SqlDbType.Char)).Value = idEmpresa
                .Parameters.Add(New SqlParameter("@Pan_cAnio", SqlDbType.Char)).Value = anio
                .Parameters.Add(New SqlParameter("@desde", SqlDbType.NVarChar)).Value = fechaInicio
                .Parameters.Add(New SqlParameter("@hasta", SqlDbType.NVarChar)).Value = fechaFin
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            Try
                If rdr IsNot Nothing Then
                    lista = New List(Of be_provision)
                    Dim ord1 As Integer = rdr.GetOrdinal("ruc")
                    Dim ord2 As Integer = rdr.GetOrdinal("razonsocial")
                    Dim ord3 As Integer = rdr.GetOrdinal("numero")
                    Dim ord4 As Integer = rdr.GetOrdinal("fecha")
                    Dim ord5 As Integer = rdr.GetOrdinal("tc")
                    Dim ord6 As Integer = rdr.GetOrdinal("concepto")
                    Dim ord7 As Integer = rdr.GetOrdinal("detalle")
                    Dim ord8 As Integer = rdr.GetOrdinal("per_cPeriodo")
                    Dim ord9 As Integer = rdr.GetOrdinal("ase_nVoucher")
                    Dim ord10 As Integer = rdr.GetOrdinal("detraccion")
                    Dim ord11 As Integer = rdr.GetOrdinal("idDocumento")
                    Dim objeto As Entidades.be_provision = Nothing
                    While rdr.Read()
                        objeto = New Entidades.be_provision

                        With objeto
                            .ruc = rdr.GetString(ord1)
                            .razonSocial = rdr.GetString(ord2)
                            .numero = rdr.GetString(ord3)
                            .fecha = rdr.GetDateTime(ord4)
                            .tc = rdr.GetDecimal(ord5)
                            .concepto = rdr.GetString(ord6)
                            .detalle = rdr.GetString(ord7)
                            .periodo = rdr.GetString(ord8)
                            .voucher = rdr.GetString(ord9)
                            .detraccion = rdr.GetDecimal(ord10)
                            .idDocumento = rdr.GetInt32(ord11)
                        End With
                        lista.Add(objeto)
                    End While
                End If
            Catch ex As Exception
                Throw ex
            Finally
                cn.Close()
            End Try

        End Using
        Return lista
    End Function

    Public Overridable Function listaDocumentosPasadosCorrectamente(ByVal cn As SqlConnection, ByVal fechaInicio As String, ByVal fechaFin As String, _
                                                                    ByVal anio As String) As List(Of Entidades.be_provision)

        Dim lista As List(Of Entidades.be_provision) = Nothing
        Using cmd As New SqlCommand("SP_PASE_PROVI_AL_SIGE", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                .Parameters.Add(New SqlParameter("@desde", SqlDbType.NVarChar)).Value = fechaInicio
                .Parameters.Add(New SqlParameter("@hasta", SqlDbType.NVarChar)).Value = fechaFin
                .Parameters.Add(New SqlParameter("@Pan_cAnio", SqlDbType.Char)).Value = anio
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            Try
                If rdr IsNot Nothing Then
                    lista = New List(Of be_provision)
                    Dim ord1 As Integer = rdr.GetOrdinal("ruc")
                    Dim ord2 As Integer = rdr.GetOrdinal("razonsocial")
                    Dim ord3 As Integer = rdr.GetOrdinal("numero")
                    Dim ord4 As Integer = rdr.GetOrdinal("fecha")
                    Dim ord5 As Integer = rdr.GetOrdinal("tc")
                    Dim ord6 As Integer = rdr.GetOrdinal("concepto")
                    Dim ord7 As Integer = rdr.GetOrdinal("detalle")
                    Dim ord8 As Integer = rdr.GetOrdinal("per_cPeriodo")
                    Dim ord9 As Integer = rdr.GetOrdinal("ase_nVoucher")
                    Dim ord10 As Integer = rdr.GetOrdinal("detraccion")
                    Dim ord11 As Integer = rdr.GetOrdinal("idDocumento")
                    Dim objeto As Entidades.be_provision = Nothing
                    While rdr.Read()
                        objeto = New Entidades.be_provision
                        With objeto
                            .ruc = rdr.GetString(ord1)
                            .razonSocial = rdr.GetString(ord2)
                            .numero = rdr.GetString(ord3)
                            .fecha = rdr.GetDateTime(ord4)
                            .tc = rdr.GetDecimal(ord5)
                            .concepto = rdr.GetString(ord6)
                            .detalle = rdr.GetString(ord7)
                            .periodo = rdr.GetString(ord8)
                            .voucher = rdr.GetString(ord9)
                            .detraccion = rdr.GetDecimal(ord10)
                            .IdDocumento = rdr.GetOrdinal(ord11)
                        End With
                        lista.Add(objeto)
                    End While
                End If
            Catch ex As Exception
                Throw ex
            Finally
                cn.Close()
            End Try

        End Using
        Return lista
    End Function
End Class