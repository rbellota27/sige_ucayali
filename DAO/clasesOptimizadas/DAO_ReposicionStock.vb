﻿Imports System.Data.SqlClient
Imports Entidades
Public Class DAO_ReposicionStock

    Public Function ListarObjetosGenerales(cn As SqlConnection, idUsuario As Integer) As be_reponerStockListarObjetos
        Dim objListaObjetosReponerStock As be_reponerStockListarObjetos = Nothing

        Dim lista As New List(Of be_ComboGenerico)
        Dim obj As be_ComboGenerico

        Using cmd As New SqlCommand("SP_REPONERSTOCK_LISTAS", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@idUsuario", SqlDbType.Int)).Value = idUsuario
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader()
            If rdr IsNot Nothing Then
                objListaObjetosReponerStock = New be_reponerStockListarObjetos
                lista = New List(Of be_ComboGenerico)

                While rdr.Read
                    obj = New be_ComboGenerico
                    obj.campo1 = rdr.GetInt32(0)
                    obj.campo2 = rdr.GetString(1)
                    lista.Add(obj)
                End While
                objListaObjetosReponerStock.listaEmpresa = lista

                If rdr.NextResult Then
                    lista = New List(Of be_ComboGenerico)
                    While rdr.Read()
                        obj = New be_ComboGenerico
                        obj.campo1 = rdr.GetInt32(0)
                        obj.campo2 = rdr.GetString(1)
                        lista.Add(obj)
                    End While
                    objListaObjetosReponerStock.listaTienda = lista
                End If

                If rdr.NextResult Then
                    lista = New List(Of be_ComboGenerico)
                    While rdr.Read()
                        obj = New be_ComboGenerico
                        obj.campo1 = rdr.GetInt32(0)
                        obj.campo2 = rdr.GetString(1)
                        lista.Add(obj)
                    End While
                    objListaObjetosReponerStock.listaAlmacen = lista
                End If

                If rdr.NextResult Then
                    Dim valor As String
                    If rdr.HasRows Then
                        If rdr.Read() Then
                            valor = rdr.GetInt32(0)
                        End If
                        objListaObjetosReponerStock.idTiendaPrincipal = valor
                    End If
                End If
                rdr.Close()
            End If
        End Using
        Return objListaObjetosReponerStock
    End Function
    Public Function lista_reposicionStock_datatable(cn As SqlConnection, ByVal fechainicial As Date, ByVal fechafinal As Date, ByVal idTienda As Integer, _
                                          ByVal idEmpresa As Integer, ByVal idalmacencomparativo As String) As DataTable
        Dim dt As New DataTable
        Using cmd As New SqlCommand("_CR_ReposicionStockSucursales_Prueba1", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                .Parameters.Add(New SqlParameter("@fechainicial", SqlDbType.Date)).Value = fechainicial
                .Parameters.Add(New SqlParameter("@fechafinal", SqlDbType.Date)).Value = fechafinal
                .Parameters.Add(New SqlParameter("@idtienda", SqlDbType.Int)).Value = idTienda
                .Parameters.Add(New SqlParameter("@idempresa", SqlDbType.Int)).Value = idEmpresa
                .Parameters.Add(New SqlParameter("@idalmacencomparativo", SqlDbType.NVarChar)).Value = idalmacencomparativo
            End With
            Dim da As New SqlDataAdapter(cmd)
            Try
                da.Fill(dt)
            Catch ex As Exception
                Throw ex
            End Try
        End Using
        Return dt
    End Function


    Public Function listar_reposicionStock_listaReportetop(cn As SqlConnection, ByVal fechainicial As Date, ByVal fechafinal As Date, ByVal idTienda As Integer, _
                                          ByVal idEmpresa As Integer, ByVal idalmacencomparativo As String) As List(Of Entidades.be_listaReponerStock8)
        Dim lista As List(Of Entidades.be_listaReponerStock8) = Nothing
        'Using cmd As New SqlCommand("_CR_ReposicionStockSucursales_Prueba1", cn)
        Using cmd As New SqlCommand("_CR_ReposicionStockSucursales_Lima_TopSemanas2", cn)

            With cmd
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                .Parameters.Add(New SqlParameter("@fechainicial", SqlDbType.Date)).Value = fechainicial
                .Parameters.Add(New SqlParameter("@fechafinal", SqlDbType.Date)).Value = fechafinal
                .Parameters.Add(New SqlParameter("@idtienda", SqlDbType.Int)).Value = idTienda
                .Parameters.Add(New SqlParameter("@idempresa", SqlDbType.Int)).Value = idEmpresa
                .Parameters.Add(New SqlParameter("@idalmacencomparativo", SqlDbType.NVarChar)).Value = idalmacencomparativo
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_listaReponerStock8)
                
                Dim objeto As Entidades.be_listaReponerStock8 = Nothing
                While rdr.Read()
                    objeto = New Entidades.be_listaReponerStock8

                    With objeto
                        'solo para este caso especial se tomará el indice y se hará de forma estática debido a que el reporte no cambiará
                        .codigo = rdr.GetString(0)
                        .descripcion = rdr.GetString(1)
                        .s1 = rdr.GetDecimal(2)
                        .s2 = rdr.GetDecimal(3)
                        .s3 = rdr.GetDecimal(4)
                        .s4 = rdr.GetDecimal(5)
                        .s5 = rdr.GetDecimal(6)
                        .s6 = rdr.GetDecimal(7)
                        .s7 = rdr.GetDecimal(8)
                        .s8 = rdr.GetDecimal(9)
                        .vtaTotalSemana = rdr.GetDecimal(10)
                        .promVenta = rdr.GetDecimal(11)
                        .almacenTienda = rdr.GetDecimal(12)
                        .almacenCentral = rdr.GetDecimal(13)
                        .cobertura = rdr.GetDecimal(14)
                        .max = IIf(IsDBNull(rdr.GetDecimal(14)), 0, rdr.GetDecimal(15))
                        .min = IIf(IsDBNull(rdr.GetDecimal(15)), 0, rdr.GetDecimal(16))
                        '.reposicion = rdr.GetString(23)
                        '.multiplo = rdr.GetDecimal(24)
                        '.idProducto = rdr.GetInt32(25)
                        '.idUnidadMedida = rdr.GetInt32(26)
                        .linea = rdr.GetString(17)
                        .sublinea = rdr.GetString(18)
                        .pais = rdr.GetString(19)

                        ' codsige desc 1,2,3,4,5,6,7,8,vt,pvta,alm,almccentral,cober,max,min,linea,sublinea,pais
                    End With
                    lista.Add(objeto)
                End While
                rdr.Close()
            End If
            'Dim da As New SqlDataAdapter(cmd)
            'da.Fill(dt)
        End Using
        Return lista
        'Return dt
    End Function

    Public Function lista_reposicionStock_lista(cn As SqlConnection, ByVal fechainicial As Date, ByVal fechafinal As Date, ByVal idTienda As Integer, _
                                          ByVal idEmpresa As Integer, ByVal idalmacencomparativo As String) As List(Of Entidades.be_listaReponerStock)
        Dim lista As List(Of Entidades.be_listaReponerStock) = Nothing
        Using cmd As New SqlCommand("_CR_ReposicionStockSucursales_Prueba1", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                .Parameters.Add(New SqlParameter("@fechainicial", SqlDbType.Date)).Value = fechainicial
                .Parameters.Add(New SqlParameter("@fechafinal", SqlDbType.Date)).Value = fechafinal
                .Parameters.Add(New SqlParameter("@idtienda", SqlDbType.Int)).Value = idTienda
                .Parameters.Add(New SqlParameter("@idempresa", SqlDbType.Int)).Value = idEmpresa
                .Parameters.Add(New SqlParameter("@idalmacencomparativo", SqlDbType.NVarChar)).Value = idalmacencomparativo
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_listaReponerStock)
               
                Dim objeto As Entidades.be_listaReponerStock = Nothing
                While rdr.Read()
                    objeto = New Entidades.be_listaReponerStock

                    With objeto
                        'solo para este caso especial se tomará el indice y se hará de forma estática debido a que el reporte no cambiará
                        .codigo = rdr.GetString(0)
                        .descripcion = rdr.GetString(1)
                        .s1 = rdr.GetDecimal(2)
                        .s2 = rdr.GetDecimal(3)
                        .s3 = rdr.GetDecimal(4)
                        .s4 = rdr.GetDecimal(5)
                        .s5 = rdr.GetDecimal(6)
                        .s6 = rdr.GetDecimal(7)
                        .s7 = rdr.GetDecimal(8)
                        .s8 = rdr.GetDecimal(9)
                        .s9 = rdr.GetDecimal(10)
                        .s10 = rdr.GetDecimal(11)
                        .s11 = rdr.GetDecimal(12)
                        .s12 = rdr.GetDecimal(13)
                        .s13 = rdr.GetDecimal(14)
                        .s14 = rdr.GetDecimal(15)
                        .s15 = rdr.GetDecimal(16)
                        .promVenta = rdr.GetDecimal(17)
                        .almacenTienda = rdr.GetDecimal(18)
                        .almacenCentral = rdr.GetDecimal(19)
                        .cobertura = rdr.GetDecimal(20)
                        .max = IIf(IsDBNull(rdr.GetDecimal(20)), 0, rdr.GetDecimal(21))
                        .min = IIf(IsDBNull(rdr.GetDecimal(21)), 0, rdr.GetDecimal(22))
                        .reposicion = rdr.GetString(23)
                        .multiplo = rdr.GetDecimal(24)
                        .idProducto = rdr.GetInt32(25)
                        .idUnidadMedida = rdr.GetInt32(26)
                        .linea = rdr.GetString(27)
                        .sublinea = rdr.GetString(28)
                        .pais = rdr.GetString(29)                        
                    End With
                    lista.Add(objeto)
                End While
                rdr.Close()
            End If
            'Dim da As New SqlDataAdapter(cmd)
            'da.Fill(dt)
        End Using
        Return lista
        'Return dt
    End Function

    Public Function CrearCabeceraCotizacion(ByVal tr As SqlTransaction, cn As SqlConnection, ByVal idUsuario As Integer, iddocumento As Integer) As Integer
        Dim retornarId As Integer = 0
        Dim nomParameterOutPut As String = ""
        Using cmd As New SqlCommand("SP_REPONERSTOCK_COTI_CABECERA", cn, tr)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@IdDocumento", SqlDbType.Int)).Direction = ParameterDirection.Output
                .Parameters.Add(New SqlParameter("@idUsuario", SqlDbType.Int)).Value = idUsuario
            End With
            Try
                cmd.ExecuteNonQuery()
                retornarId = CInt(cmd.Parameters("@IdDocumento").Value)
            Catch ex As Exception
                Throw ex
            End Try
        End Using
        Return retornarId
    End Function

    Public Function CrearDetalleCotizacion(ByVal tr As SqlTransaction, ByVal cn As SqlConnection, ByVal idUsuario As Integer, _
                                           ByVal lista As List(Of Entidades.be_reposicionStock), idDocumento As Integer)
        Dim idDetalleDocumento As Integer = 0
        For i As Integer = 0 To lista.Count - 1
            Using cmd As New SqlCommand("SP_REPONERSTOCK_COTI_DETALLE", cn, tr)
                With cmd
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add(New SqlParameter("@IdDetalleDocumento", SqlDbType.Int)).Direction = ParameterDirection.Output
                    .Parameters.Add(New SqlParameter("@IdDocumento", SqlDbType.Int)).Value = idDocumento
                    .Parameters.Add(New SqlParameter("@idProducto", SqlDbType.Int)).Value = lista(i).idProducto
                    .Parameters.Add(New SqlParameter("@cantidadProducto", SqlDbType.Decimal)).Value = lista(i).cantidadProducto
                    .Parameters.Add(New SqlParameter("@idUnidadMedida", SqlDbType.Int)).Value = lista(i).idUnidadMedida
                    .Parameters.Add(New SqlParameter("@idUsuario", SqlDbType.Int)).Value = idUsuario
                End With
                Try
                    cmd.ExecuteNonQuery()

                    idDetalleDocumento = CInt(cmd.Parameters("@IdDetalleDocumento").Value)
                Catch ex As Exception
                    Throw ex
                End Try
            End Using
        Next
        Return idDetalleDocumento
    End Function
End Class
