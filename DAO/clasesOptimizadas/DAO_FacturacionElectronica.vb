﻿Imports System.Data.SqlClient
Imports Entidades
Public Class DAO_FacturacionElectronica

    Public Shared Function estructuraFacturacion(ByVal cn As SqlConnection, ByVal idtipoDocumento As Integer) As List(Of be_facturacionElectronica)
        Dim lista As List(Of be_facturacionElectronica) = Nothing

        Using cmd As New SqlCommand("SP_ESTRUCTURA_FACTURACION_V2", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@idTipoDocumento", SqlDbType.Int)).Value = idtipoDocumento

                Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
                If rdr IsNot Nothing Then
                    lista = New List(Of be_facturacionElectronica)
                    Dim ordinal As Integer = rdr.GetOrdinal("id")
                    Dim ordina2 As Integer = rdr.GetOrdinal("codigo")
                    Dim ordina3 As Integer = rdr.GetOrdinal("Concepto_digiflow")
                    Dim ordina4 As Integer = rdr.GetOrdinal("campo_digiflow")
                    Dim objeto As be_facturacionElectronica = Nothing
                    While rdr.Read()
                        objeto = New be_facturacionElectronica
                        With objeto
                            .id = rdr.GetInt32(ordinal)
                            .codigo = rdr.GetString(ordina2)
                            .conceptoDigiflow = rdr.GetString(ordina3)
                            .campo = rdr.GetString(ordina4)
                        End With
                        lista.Add(objeto)
                    End While
                    rdr.Close()
                End If
            End With
        End Using
        Return lista
    End Function
    ' 
    Public Function fun_GENERAR_PRINT_RETENCION_ELECTRONICA(cn As SqlConnection, IdDocumento As Integer) As Integer
        Using cmd As New SqlCommand("SP_RETENCIONES_V1", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@IdDocumento", SqlDbType.Int)).Value = IdDocumento

            End With
            cmd.ExecuteNonQuery()
        End Using

        Return 0
    End Function


    Public Function fun_GENERAR_PRINT_FAC_ELECTRONICA(cn As SqlConnection, idSerie As Integer, codigo As String) As Integer
        Using cmd As New SqlCommand("SP_GENERAR_PRINT_FAC_ELECTRONICA", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@idSerie", SqlDbType.Int)).Value = idSerie
                .Parameters.Add(New SqlParameter("@CODIGO", SqlDbType.NVarChar)).Value = codigo
            End With
            cmd.ExecuteNonQuery()
        End Using

        Return 0
    End Function


End Class
