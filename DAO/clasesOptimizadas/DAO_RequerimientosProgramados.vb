﻿Imports System.Data.SqlClient
Public Class DAO_RequerimientosProgramados
    Inherits Conexion
    Public Function listarprogramaciones(ByVal cn As SqlConnection, ByVal fecInicio As String, ByVal fecFin As String) As List(Of Entidades.be_RequerimientoProgramados)

        Dim lista As List(Of Entidades.be_RequerimientoProgramados) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "_MovCuentaPorPagar_SelectxParamsReport_Masivos"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@FechaInicio", SqlDbType.Date)).Value = fecInicio
                .Parameters.Add(New SqlParameter("@FechaFin", SqlDbType.Date)).Value = fecFin
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_RequerimientoProgramados)
                Dim ind1 As Integer = rdr.GetOrdinal("Marcar")
                Dim ind2 As Integer = rdr.GetOrdinal("NroDocumento")
                Dim ind3 As Integer = rdr.GetOrdinal("Proveedor")
                Dim ind4 As Integer = rdr.GetOrdinal("FechaPago")
                Dim ind5 As Integer = rdr.GetOrdinal("Simbolo")
                Dim ind6 As Integer = rdr.GetOrdinal("mcp_Monto")
                Dim ind7 As Integer = rdr.GetOrdinal("mcp_Saldo")
                Dim ind8 As Integer = rdr.GetOrdinal("doc_TotalApagar")
                Dim ind9 As Integer = rdr.GetOrdinal("doc_FechaVenc")
                Dim ind10 As Integer = rdr.GetOrdinal("IdMoneda")
                Dim ind11 As Integer = rdr.GetOrdinal("Tienda")
                Dim ind12 As Integer = rdr.GetOrdinal("IdTienda")
                Dim ind13 As Integer = rdr.GetOrdinal("IdTipoDocumento")
                Dim ind14 As Integer = rdr.GetOrdinal("iddocumento")

                Dim objeto As Entidades.be_RequerimientoProgramados = Nothing
                While rdr.Read()
                    objeto = New Entidades.be_RequerimientoProgramados
                    With objeto
                        .Marcar = rdr.GetInt32(ind1)
                        .NroDocumento = rdr.GetString(ind2)
                        .Proveedor = rdr.GetString(ind3)
                        .FechaPago = rdr.GetDateTime(ind4)
                        .Simbolo = rdr.GetString(ind5)
                        .mcp_Monto = rdr.GetDecimal(ind6)
                        .mcp_Saldo = rdr.GetDecimal(ind7)
                        .doc_TotalApagar = rdr.GetDecimal(ind8)
                        .doc_FechaVenc = rdr.GetDateTime(ind9)
                        .IdMoneda = rdr.GetInt32(ind10)
                        .Tienda = rdr.GetString(ind11)
                        .IdTienda = rdr.GetInt32(ind12)
                        .IdTipoDocumento = rdr.GetInt32(ind13)
                        .iddocumento = rdr.GetInt32(ind14)
                        lista.Add(objeto)

                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function


    Public Function listarprogramacionesEstado(ByVal cn As SqlConnection, ByVal fecInicio As String, ByVal fecFin As String, ByVal iddocumento As Integer, ByVal IdUsuario As Integer) As List(Of Entidades.be_RequerimientoProgramados)

        Dim lista As List(Of Entidades.be_RequerimientoProgramados) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "_MovCuentaPorPagar_SelectxParamsReport_Masivos"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@FechaInicio", SqlDbType.Date)).Value = fecInicio
                .Parameters.Add(New SqlParameter("@FechaFin", SqlDbType.Date)).Value = fecFin
                .Parameters.Add(New SqlParameter("@iddocumento", SqlDbType.Int)).Value = iddocumento
                .Parameters.Add(New SqlParameter("@IdUsuario", SqlDbType.Int)).Value = IdUsuario
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_RequerimientoProgramados)
                Dim ind1 As Integer = rdr.GetOrdinal("Marcar")
                Dim ind2 As Integer = rdr.GetOrdinal("NroDocumento")
                Dim ind3 As Integer = rdr.GetOrdinal("Proveedor")
                Dim ind4 As Integer = rdr.GetOrdinal("FechaPago")
                Dim ind5 As Integer = rdr.GetOrdinal("Simbolo")
                Dim ind6 As Integer = rdr.GetOrdinal("mcp_Monto")
                Dim ind7 As Integer = rdr.GetOrdinal("mcp_Saldo")
                Dim ind8 As Integer = rdr.GetOrdinal("doc_TotalApagar")
                Dim ind9 As Integer = rdr.GetOrdinal("doc_FechaVenc")
                Dim ind10 As Integer = rdr.GetOrdinal("IdMoneda")
                Dim ind11 As Integer = rdr.GetOrdinal("Tienda")
                Dim ind12 As Integer = rdr.GetOrdinal("IdTienda")
                Dim ind13 As Integer = rdr.GetOrdinal("IdTipoDocumento")
                Dim ind14 As Integer = rdr.GetOrdinal("iddocumento")

                Dim objeto As Entidades.be_RequerimientoProgramados = Nothing
                While rdr.Read()
                    objeto = New Entidades.be_RequerimientoProgramados
                    With objeto
                        .Marcar = rdr.GetInt32(ind1)
                        .NroDocumento = rdr.GetString(ind2)
                        .Proveedor = rdr.GetString(ind3)
                        .FechaPago = rdr.GetDateTime(ind4)
                        .Simbolo = rdr.GetString(ind5)
                        .mcp_Monto = rdr.GetDecimal(ind6)
                        .mcp_Saldo = rdr.GetDecimal(ind7)
                        .doc_TotalApagar = rdr.GetDecimal(ind8)
                        .doc_FechaVenc = rdr.GetDateTime(ind9)
                        .IdMoneda = rdr.GetInt32(ind10)
                        .Tienda = rdr.GetString(ind11)
                        .IdTienda = rdr.GetInt32(ind12)
                        .IdTipoDocumento = rdr.GetInt32(ind13)
                        .iddocumento = rdr.GetInt32(ind14)
                        lista.Add(objeto)

                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function




    Public Function listarprogramacionesEstadoReprogramado(ByVal cn As SqlConnection, ByVal fecInicio As String, ByVal fecFin As String, ByVal FechaCambio As String, ByVal iddocumento As Integer) As List(Of Entidades.be_RequerimientoProgramados)

        Dim lista As List(Of Entidades.be_RequerimientoProgramados) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "_MovCuentaPorPagar_SelectxParamsReport_Masivos_FechaReprogramada"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@FechaInicio", SqlDbType.Date)).Value = fecInicio
                .Parameters.Add(New SqlParameter("@FechaFin", SqlDbType.Date)).Value = fecFin
                .Parameters.Add(New SqlParameter("@FechaCambio", SqlDbType.Date)).Value = FechaCambio

                .Parameters.Add(New SqlParameter("@iddocumento", SqlDbType.Int)).Value = iddocumento
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_RequerimientoProgramados)
                Dim ind1 As Integer = rdr.GetOrdinal("Marcar")
                Dim ind2 As Integer = rdr.GetOrdinal("NroDocumento")
                Dim ind3 As Integer = rdr.GetOrdinal("Proveedor")
                Dim ind4 As Integer = rdr.GetOrdinal("FechaPago")
                Dim ind5 As Integer = rdr.GetOrdinal("Simbolo")
                Dim ind6 As Integer = rdr.GetOrdinal("mcp_Monto")
                Dim ind7 As Integer = rdr.GetOrdinal("mcp_Saldo")
                Dim ind8 As Integer = rdr.GetOrdinal("doc_TotalApagar")
                Dim ind9 As Integer = rdr.GetOrdinal("doc_FechaVenc")
                Dim ind10 As Integer = rdr.GetOrdinal("IdMoneda")
                Dim ind11 As Integer = rdr.GetOrdinal("Tienda")
                Dim ind12 As Integer = rdr.GetOrdinal("IdTienda")
                Dim ind13 As Integer = rdr.GetOrdinal("IdTipoDocumento")
                Dim ind14 As Integer = rdr.GetOrdinal("iddocumento")

                Dim objeto As Entidades.be_RequerimientoProgramados = Nothing
                While rdr.Read()
                    objeto = New Entidades.be_RequerimientoProgramados
                    With objeto
                        .Marcar = rdr.GetInt32(ind1)
                        .NroDocumento = rdr.GetString(ind2)
                        .Proveedor = rdr.GetString(ind3)
                        .FechaPago = rdr.GetDateTime(ind4)
                        .Simbolo = rdr.GetString(ind5)
                        .mcp_Monto = rdr.GetDecimal(ind6)
                        .mcp_Saldo = rdr.GetDecimal(ind7)
                        .doc_TotalApagar = rdr.GetDecimal(ind8)
                        .doc_FechaVenc = rdr.GetDateTime(ind9)
                        .IdMoneda = rdr.GetInt32(ind10)
                        .Tienda = rdr.GetString(ind11)
                        .IdTienda = rdr.GetInt32(ind12)
                        .IdTipoDocumento = rdr.GetInt32(ind13)
                        .iddocumento = rdr.GetInt32(ind14)
                        lista.Add(objeto)

                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function


    Public Function listarmonto(ByVal cn As SqlConnection, ByVal fecInicio As String, ByVal fecFin As String, ByVal iddocumento As Integer, ByVal usuario As Integer) As List(Of Entidades.be_MontoProgramado)

        Dim lista As List(Of Entidades.be_MontoProgramado) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "_MovCuentaPorPagar_SelectxParamsReport_MasivosMontos"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@FechaInicio", SqlDbType.Date)).Value = fecInicio
                .Parameters.Add(New SqlParameter("@FechaFin", SqlDbType.Date)).Value = fecFin
                .Parameters.Add(New SqlParameter("@iddocumento", SqlDbType.Int)).Value = iddocumento
                .Parameters.Add(New SqlParameter("@IdUsuario", SqlDbType.Int)).Value = usuario
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_MontoProgramado)
                Dim ind1 As Decimal = rdr.GetOrdinal("montoProgramado")


                Dim objeto As Entidades.be_MontoProgramado = Nothing
                While rdr.Read()
                    objeto = New Entidades.be_MontoProgramado
                    With objeto
                        .montoProgramado = rdr.GetDecimal(ind1)

                        lista.Add(objeto)

                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function


    Public Function AnularCheque(ByVal cn As SqlConnection, ByVal IdDocumento As Integer) As List(Of Entidades.be_RequerimientoProgramados)

        Dim lista As List(Of Entidades.be_RequerimientoProgramados) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "_AnularChequeEmitido"

                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@IdDocumento", SqlDbType.Int)).Value = IdDocumento


            End With
            Dim rdr As SqlDataReader = cmd.BeginExecuteNonQuery

        End Using
        Return lista
    End Function
End Class
