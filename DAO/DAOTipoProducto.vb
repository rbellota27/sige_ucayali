﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class DAOTipoProducto
    Inherits DAOMantenedor
    Private HDAO As New DAO.HelperDAO
    Private reader As SqlDataReader


    Public Function SelectCbo() As List(Of Entidades.TipoProducto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoProductoSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoProducto)
                Do While lector.Read
                    Dim objTP As New Entidades.TipoProducto
                    objTP.Id = CInt(lector.Item("IdTipoProducto"))
                    objTP.Nombre = CStr(lector.Item("tp_Nombre"))
                    Lista.Add(objTP)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function selectTipoProductoAll(ByVal nombre As String) As List(Of Entidades.TipoProducto)
        Dim lista As New List(Of Entidades.TipoProducto)
        Try
            Cn = (New DAO.Conexion).ConexionSIGE
            Cn.Open()
            Dim arrayParametros() As SqlParameter = New SqlParameter(0) {}
            arrayParametros(0) = New SqlParameter("@Nombre", SqlDbType.VarChar)
            arrayParametros(0).Value = nombre
            reader = SqlHelper.ExecuteReader(MyBase.Cn, CommandType.StoredProcedure, "_tipoProductoSelectall", arrayParametros)
            While (reader.Read)
                Dim obj As New Entidades.TipoProducto
                With obj
                    .Id = UCInt(reader("IdTipoProducto"))
                    .Nombre = UCStr(reader("tp_Nombre"))
                    .Abv = UCStr(reader("tp_Abv"))
                    .Estado = UCStr(reader("tp_Estado"))
                End With
                lista.Add(obj)
            End While
            reader.Close()
        Catch ex As Exception
            Throw ex

        Finally
            If (MyBase.Cn.State = ConnectionState.Open) Then MyBase.Cn.Close()
        End Try
        Return lista
    End Function
    Public Function InsertaTipoProducto(ByVal tipoProducto As Entidades.TipoProducto, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer
        'Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@tp_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = tipoProducto.Nombre
        ArrayParametros(1) = New SqlParameter("@tp_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = tipoProducto.Abv
        ArrayParametros(2) = New SqlParameter("@tp_Estado", SqlDbType.VarChar)
        ArrayParametros(2).Value = tipoProducto.Estado
        'Return HDAO.Insert(cn, "_TipoDocumentoInsert", ArrayParametros)
        Dim cmd As New SqlCommand("_TipoproductoInsert", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Return CInt(cmd.ExecuteScalar)
    End Function
    Public Function UpdateTipoproducto(ByVal tipoProducto As Entidades.TipoProducto, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Dim arrayParametros() As SqlParameter = New SqlParameter(3) {}
        arrayParametros(0) = New SqlParameter("@IdTipoProducto", SqlDbType.Int)
        arrayParametros(0).Value = tipoProducto.Id
        arrayParametros(1) = New SqlParameter("@tp_Nombre", SqlDbType.VarChar)
        arrayParametros(1).Value = tipoProducto.Nombre
        arrayParametros(2) = New SqlParameter("@tp_Abv", SqlDbType.VarChar)
        arrayParametros(2).Value = tipoProducto.Abv
        arrayParametros(3) = New SqlParameter("@tp_Estado", SqlDbType.Char)
        arrayParametros(3).Value = tipoProducto.Estado

        Dim cmd As New SqlCommand("_TipoproductoUpdate", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(arrayParametros)
        If cmd.ExecuteNonQuery = 0 Then
            Return False
        End If
        Return True
    End Function



End Class
