﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'************  VIERNES 04 06 2010

Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAOComision_Det
    Private objDaoMantenedor As New DAO.DAOMantenedor
    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private reader As SqlDataReader
    Dim objConexion As New DAO.Conexion

    Public Function Registrar(ByVal objComisionDet As Entidades.Comision_Det, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer

        Dim p() As SqlParameter = New SqlParameter(16) {}
        p(0) = objDaoMantenedor.getParam(objComisionDet.IdComisionCab, "@IdComisionCab", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(objComisionDet.IdProducto, "@IdProducto", SqlDbType.Int)
        p(2) = objDaoMantenedor.getParam(objComisionDet.IdUnidadMedida, "@IdUnidadMedida", SqlDbType.Int)
        p(3) = objDaoMantenedor.getParam(objComisionDet.FechaInicio, "@comd_FechaInicio", SqlDbType.Date)
        p(4) = objDaoMantenedor.getParam(objComisionDet.FechaFin, "@comd_FechaFin", SqlDbType.Date)
        p(5) = objDaoMantenedor.getParam(objComisionDet.ValorComision, "@comd_ValorComision", SqlDbType.Decimal)
        p(6) = objDaoMantenedor.getParam(objComisionDet.TipoCalculoCom, "@comd_TipoCalculoCom", SqlDbType.VarChar)
        p(7) = objDaoMantenedor.getParam(objComisionDet.IdMoneda, "@IdMoneda", SqlDbType.Int)
        p(8) = objDaoMantenedor.getParam(objComisionDet.PrecioBaseComision, "@comd_PrecioBaseComision", SqlDbType.VarChar)
        p(9) = objDaoMantenedor.getParam(objComisionDet.IncluyeIgv, "@comd_IncluyeIgv", SqlDbType.Bit)
        p(10) = objDaoMantenedor.getParam(objComisionDet.Dcto_Inicio, "@comd_Dcto_Inicio", SqlDbType.Decimal)
        p(11) = objDaoMantenedor.getParam(objComisionDet.Dcto_Fin, "@comd_Dcto_Fin", SqlDbType.Decimal)
        p(12) = objDaoMantenedor.getParam(objComisionDet.PrecioBaseDcto, "@comd_PrecioBaseDcto", SqlDbType.VarChar)
        p(13) = objDaoMantenedor.getParam(objComisionDet.IdEmpresa, "@IdEmpresa", SqlDbType.Int)
        p(14) = objDaoMantenedor.getParam(objComisionDet.IdTienda, "@IdTienda", SqlDbType.Int)
        p(15) = objDaoMantenedor.getParam(objComisionDet.Estado, "@comd_Estado", SqlDbType.Bit)
        p(16) = objDaoMantenedor.getParam(objComisionDet.IdComisionDet, "@IdComisionDet", SqlDbType.Int)

        Return CInt(SqlHelper.ExecuteScalar(tr, CommandType.StoredProcedure, "_Comision_Det_Registrar", p))

    End Function

    Public Function Comision_SelectProducto_AddDetallexParams(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal Tabla_IdProducto As DataTable, ByVal IdTienda As Integer, ByVal IdComisionCab As Integer, ByVal FiltrarComsion As Boolean, ByVal Descripcion As String, ByVal CodigoProducto As String, ByVal PageIndex As Integer, ByVal PageSize As Integer) As List(Of Entidades.Comision_Det)

        Dim listaComision_Det As New List(Of Entidades.Comision_Det)

        Try

            Dim p() As SqlParameter = New SqlParameter(9) {}
            p(0) = objDaoMantenedor.getParam(IdLinea, "@IdLinea", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdSubLinea, "@IdSubLinea", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(IdComisionCab, "@IdComisionCab", SqlDbType.Int)
            p(4) = New SqlParameter("@Tabla_IdProducto", Tabla_IdProducto)
            p(5) = objDaoMantenedor.getParam(FiltrarComsion, "@FiltrarComsion", SqlDbType.Int)
            p(6) = objDaoMantenedor.getParam(PageIndex, "@PageIndex", SqlDbType.Int)
            p(7) = objDaoMantenedor.getParam(PageSize, "@PageSize", SqlDbType.Int)
            p(8) = objDaoMantenedor.getParam(Descripcion, "@Descripcion", SqlDbType.VarChar)
            p(9) = objDaoMantenedor.getParam(CodigoProducto, "@CodigoProducto", SqlDbType.VarChar)


            cn.Open()

            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_Comision_Det_SELECT_productos", p)

            While (reader.Read)

                Dim objComision_Det As New Entidades.Comision_Det

                With objComision_Det

                    .IdProducto = objDaoMantenedor.UCInt(reader("IdProducto"))
                    .IdComisionDet = objDaoMantenedor.UCInt(reader("IdComisionDet"))
                    .CodigoProducto = objDaoMantenedor.UCStr(reader("CodigoProducto"))
                    .Producto = objDaoMantenedor.UCStr(reader("Producto"))
                    .Cadena_IdUnidadMedida_Prod = objDaoMantenedor.UCStr(reader("Cadena_IdUnidadMedida_Prod"))
                    .Cadena_UnidadMedida_Prod = objDaoMantenedor.UCStr(reader("Cadena_UnidadMedida_Prod"))
                    .IdUnidadMedida = objDaoMantenedor.UCInt(reader("IdUnidadMedida"))
                    .IdMoneda = objDaoMantenedor.UCInt(reader("IdMoneda"))
                    .FechaInicio = objDaoMantenedor.UCDate(reader("comd_FechaInicio"))
                    .FechaFin = objDaoMantenedor.UCDate(reader("comd_FechaFin"))
                    .ValorComision = objDaoMantenedor.UCDec(reader("comd_ValorComision"))
                    .PrecioBaseComision = objDaoMantenedor.UCStr(reader("comd_PrecioBaseComision"))
                    .PrecioBaseDcto = objDaoMantenedor.UCStr(reader("comd_PrecioBaseDcto"))
                    .TipoCalculoCom = objDaoMantenedor.UCStr(reader("comd_TipoCalculoCom"))
                    .IncluyeIgv = objDaoMantenedor.UCBool(reader("comd_IncluyeIgv"))
                    .Dcto_Inicio = objDaoMantenedor.UCDec(reader("comd_Dcto_Inicio"))
                    .Dcto_Fin = objDaoMantenedor.UCDec(reader("comd_Dcto_Fin"))
                    .Estado = objDaoMantenedor.UCBool(reader("comd_Estado"))
                End With

                listaComision_Det.Add(objComision_Det)

            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return listaComision_Det

    End Function



    Public Function CR_ComisionxVendedor(ByVal IdComisionCab As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdVendedor As Integer, ByVal FechaIni As String, ByVal FechaFin As String, ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdProducto As Integer, ByVal Tabla As DataTable) As DataSet

        Dim DS As New DataSet
        Dim DA As SqlDataAdapter

        Dim cmd As New SqlCommand("_CR_ComisionxVendedor", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0

        cmd.Parameters.AddWithValue("@IdComisionCab", IdComisionCab)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdVendedor", IdVendedor)
        cmd.Parameters.AddWithValue("@FechaIni", FechaIni)
        cmd.Parameters.AddWithValue("@FechaFin", FechaFin)

        cmd.Parameters.AddWithValue("@IdTipoExistencia", IdTipoExistencia)
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)

        cmd.Parameters.AddWithValue("@Atributo", Tabla)

        Try

            DA = New SqlDataAdapter(cmd)
            DA.Fill(DS, "DetComision")

        Catch ex As Exception
            DS = Nothing
        Finally

        End Try

        Return DS
    End Function


    Public Function CR_DetalleComision(ByVal IdComisionCab As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdVendedor As Integer, ByVal FechaIni As String, ByVal FechaFin As String) As DataSet

        Dim DS As New DataSet
        Dim DA As SqlDataAdapter

        Dim cmd As New SqlCommand("_CR_DetalleComision", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0

        cmd.Parameters.AddWithValue("@IdComisionCab", IdComisionCab)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdVendedor", IdVendedor)
        cmd.Parameters.AddWithValue("@FechaIni", FechaIni)
        cmd.Parameters.AddWithValue("@FechaFin", FechaFin)


        Try

            DA = New SqlDataAdapter(cmd)
            DA.Fill(DS, "DetComision")

        Catch ex As Exception
            DS = Nothing
        Finally

        End Try

        Return DS
    End Function

End Class
