﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'*********************   MARTES 15062010

Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOCheque_DocumentoRef

    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private tr As SqlTransaction
    Private objDaoMantenedor As New DAO.DAOMantenedor

    Public Sub Registrar(ByVal objCheque_DocumentoRef As Entidades.Cheque_DocumentoRef, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(1) {}
        p(0) = objDaoMantenedor.getParam(objCheque_DocumentoRef.IdCheque, "@IdCheque", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(objCheque_DocumentoRef.IdDocumentoRef, "@IdDocumentoRef", SqlDbType.Int)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_Cheque_DocumentoRef_Registrar", p)

    End Sub


End Class
