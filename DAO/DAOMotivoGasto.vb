﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'********************************************************
'Autor      : Chigne Bazan, Dany
'Módulo     : MotivoGasto
'Sistema    : Sanicenter
'Empresa    : Digrafic SRL
'Modificado : 17-Febrero-2010
'Hora       : 06:00:00 pm
'********************************************************
Imports System.Data.SqlClient
Public Class DAOMotivoGasto
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaMotivoGasto(ByVal MotivoGasto As Entidades.MotivoGasto) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}

        ArrayParametros(0) = New SqlParameter("@Nombre_MG", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(MotivoGasto.Nombre_MG = Nothing, DBNull.Value, MotivoGasto.Nombre_MG)
        ArrayParametros(1) = New SqlParameter("@NombreCorto_MG", SqlDbType.VarChar)
        ArrayParametros(1).Value = IIf(MotivoGasto.NombreCorto_MG = Nothing, DBNull.Value, MotivoGasto.NombreCorto_MG)
        ArrayParametros(2) = New SqlParameter("@Estado_MG", SqlDbType.VarChar)
        ArrayParametros(2).Value = IIf(MotivoGasto.Estado_MG = Nothing, DBNull.Value, MotivoGasto.Estado_MG)
        ArrayParametros(3) = New SqlParameter("@IdConcepto", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(MotivoGasto.IdConcepto = Nothing, DBNull.Value, MotivoGasto.IdConcepto)


        Return HDAO.Insert(cn, "_MotivoGastoInsert", ArrayParametros)

    End Function
    Public Function ActualizaMotivoGasto(ByVal MotivoGasto As Entidades.MotivoGasto) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@Nombre_MG", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(MotivoGasto.Nombre_MG = Nothing, DBNull.Value, MotivoGasto.Nombre_MG)
        ArrayParametros(1) = New SqlParameter("@NombreCorto_MG", SqlDbType.VarChar)
        ArrayParametros(1).Value = IIf(MotivoGasto.NombreCorto_MG = Nothing, DBNull.Value, MotivoGasto.NombreCorto_MG)
        ArrayParametros(2) = New SqlParameter("@Estado_MG", SqlDbType.VarChar)
        ArrayParametros(2).Value = IIf(MotivoGasto.Estado_MG = Nothing, DBNull.Value, MotivoGasto.Estado_MG)
        ArrayParametros(3) = New SqlParameter("@IdConcepto", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(MotivoGasto.IdConcepto = Nothing, DBNull.Value, MotivoGasto.IdConcepto)
        ArrayParametros(4) = New SqlParameter("@IdMotGasto", SqlDbType.Int)
        ArrayParametros(4).Value = IIf(MotivoGasto.Id = Nothing, DBNull.Value, MotivoGasto.Id)

        Return HDAO.Update(cn, "_Motivo_GastoUpdate", ArrayParametros)
    End Function

    Public Function SelectxConceptoxMotivoGasto(ByVal IdConcepto As Integer) As List(Of Entidades.MotivoGasto)
        Dim Lista As New List(Of Entidades.MotivoGasto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoGastoSelectCboConcepto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdConcepto", IdConcepto)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)

                Do While lector.Read
                    Dim obj As New Entidades.MotivoGasto

                    obj.Id = CInt(IIf(IsDBNull(lector("IdMotGasto")) = True, 0, lector("IdMotGasto")))
                    obj.Nombre_MG = CStr(IIf(IsDBNull(lector("Nombre_MG")) = True, 0, lector("Nombre_MG")))

                    Lista.Add(obj) 'llena la lista
                Loop
                lector.Close() 'cierra el lector

            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close() 'cierra la conexion
        End Try
        Return Lista 'Retorna lista
    End Function

    Public Function SelectAllMotivoGasto() As List(Of Entidades.MotivoGasto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoGastoSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim Lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                Lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.MotivoGasto)
                Do While Lector.Read
                    Dim obj As New Entidades.MotivoGasto
                    obj.Nombre_MG = CStr(IIf(IsDBNull(Lector.Item("Nombre_MG")) = True, "", Lector.Item("Nombre_MG")))
                    obj.NombreCorto_MG = CStr(IIf(IsDBNull(Lector.Item("NombreCorto_MG")) = True, "", Lector.Item("NombreCorto_MG")))
                    obj.Estado_MG = CStr(IIf(IsDBNull(Lector.Item("Estado_MG")) = True, "", Lector.Item("Estado_MG")))
                    obj.Id = CInt(IIf(IsDBNull(Lector.Item("IdMotGasto")) = True, "", Lector.Item("IdMotGasto")))
                    obj.IdConcepto = CInt(IIf(IsDBNull(Lector.Item("IdConcepto")) = True, "", Lector.Item("IdConcepto")))
                    obj.con_Nombre = CStr(IIf(IsDBNull(Lector.Item("con_Nombre")) = True, "", Lector.Item("con_Nombre")))
                    lista.Add(obj)
                Loop
                Lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivoMotivoGasto() As List(Of Entidades.MotivoGasto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoGastoSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim Lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                Lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.MotivoGasto)
                Do While Lector.Read
                    Dim obj As New Entidades.MotivoGasto
                    obj.Nombre_MG = CStr(IIf(IsDBNull(Lector.Item("Nombre_MG")) = True, "", Lector.Item("Nombre_MG")))
                    obj.NombreCorto_MG = CStr(IIf(IsDBNull(Lector.Item("NombreCorto_MG")) = True, "", Lector.Item("NombreCorto_MG")))
                    obj.Estado_MG = CStr(IIf(IsDBNull(Lector.Item("Estado_MG")) = True, "", Lector.Item("Estado_MG")))
                    obj.Id = CInt(IIf(IsDBNull(Lector.Item("IdMotGasto")) = True, "", Lector.Item("IdMotGasto")))
                    obj.IdConcepto = CInt(IIf(IsDBNull(Lector.Item("IdConcepto")) = True, "", Lector.Item("IdConcepto")))
                    obj.con_Nombre = CStr(IIf(IsDBNull(Lector.Item("con_Nombre")) = True, "", Lector.Item("con_Nombre")))
                    lista.Add(obj)
                Loop
                Lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivoMotivoGasto() As List(Of Entidades.MotivoGasto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoGastoSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim Lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                Lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.MotivoGasto)
                Do While Lector.Read
                    Dim obj As New Entidades.MotivoGasto
                    obj.Nombre_MG = CStr(IIf(IsDBNull(Lector.Item("Nombre_MG")) = True, "", Lector.Item("Nombre_MG")))
                    obj.NombreCorto_MG = CStr(IIf(IsDBNull(Lector.Item("NombreCorto_MG")) = True, "", Lector.Item("NombreCorto_MG")))
                    obj.Estado_MG = CStr(IIf(IsDBNull(Lector.Item("Estado_MG")) = True, "", Lector.Item("Estado_MG")))
                    obj.Id = CInt(IIf(IsDBNull(Lector.Item("IdMotGasto")) = True, "", Lector.Item("IdMotGasto")))
                    obj.IdConcepto = CInt(IIf(IsDBNull(Lector.Item("IdConcepto")) = True, "", Lector.Item("IdConcepto")))
                    obj.con_Nombre = CStr(IIf(IsDBNull(Lector.Item("con_Nombre")) = True, "", Lector.Item("con_Nombre")))
                    lista.Add(obj)
                Loop
                Lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.MotivoGasto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoGastoSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre_MG", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MotivoGasto)
                Do While lector.Read
                    Dim obj As New Entidades.MotivoGasto
                    obj.Nombre_MG = CStr(IIf(IsDBNull(lector.Item("Nombre_MG")) = True, "", lector.Item("Nombre_MG")))
                    obj.NombreCorto_MG = CStr(IIf(IsDBNull(lector.Item("NombreCorto_MG")) = True, "", lector.Item("NombreCorto_MG")))
                    obj.Estado_MG = CStr(IIf(IsDBNull(lector.Item("Estado_MG")) = True, "", lector.Item("Estado_MG")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdMotGasto")) = True, "", lector.Item("IdMotGasto")))
                    obj.IdConcepto = CInt(IIf(IsDBNull(lector.Item("IdConcepto")) = True, "", lector.Item("IdConcepto")))
                    obj.con_Nombre = CStr(IIf(IsDBNull(lector.Item("con_Nombre")) = True, "", lector.Item("con_Nombre")))
                    Lista.Add(obj)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombreActivo(ByVal nombre As String) As List(Of Entidades.MotivoGasto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoGastoSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre_MG", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MotivoGasto)
                Do While lector.Read
                    Dim obj As New Entidades.MotivoGasto
                    obj.Nombre_MG = CStr(IIf(IsDBNull(lector.Item("Nombre_MG")) = True, "", lector.Item("Nombre_MG")))
                    obj.NombreCorto_MG = CStr(IIf(IsDBNull(lector.Item("NombreCorto_MG")) = True, "", lector.Item("NombreCorto_MG")))
                    obj.Estado_MG = CStr(IIf(IsDBNull(lector.Item("Estado_MG")) = True, "", lector.Item("Estado_MG")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdMotGasto")) = True, "", lector.Item("IdMotGasto")))
                    obj.IdConcepto = CInt(IIf(IsDBNull(lector.Item("IdConcepto")) = True, "", lector.Item("IdConcepto")))
                    obj.con_Nombre = CStr(IIf(IsDBNull(lector.Item("con_Nombre")) = True, "", lector.Item("con_Nombre")))
                    Lista.Add(obj)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombreInactivo(ByVal nombre As String) As List(Of Entidades.MotivoGasto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoGastoSelectAllxNombreInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre_MG", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MotivoGasto)
                Do While lector.Read
                    Dim obj As New Entidades.MotivoGasto
                    obj.Nombre_MG = CStr(IIf(IsDBNull(lector.Item("Nombre_MG")) = True, "", lector.Item("Nombre_MG")))
                    obj.NombreCorto_MG = CStr(IIf(IsDBNull(lector.Item("NombreCorto_MG")) = True, "", lector.Item("NombreCorto_MG")))
                    obj.Estado_MG = CStr(IIf(IsDBNull(lector.Item("Estado_MG")) = True, "", lector.Item("Estado_MG")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdMotGasto")) = True, "", lector.Item("IdMotGasto")))
                    obj.IdConcepto = CInt(IIf(IsDBNull(lector.Item("IdConcepto")) = True, "", lector.Item("IdConcepto")))
                    obj.con_Nombre = CStr(IIf(IsDBNull(lector.Item("con_Nombre")) = True, "", lector.Item("con_Nombre")))
                    Lista.Add(obj)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxId(ByVal Codigo As Integer) As List(Of Entidades.MotivoGasto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoGastoSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdMotGasto", Codigo)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MotivoGasto)
                Do While lector.Read
                    Dim obj As New Entidades.MotivoGasto
                    obj.Nombre_MG = CStr(IIf(IsDBNull(lector.Item("Nombre_MG")) = True, "", lector.Item("Nombre_MG")))
                    obj.NombreCorto_MG = CStr(IIf(IsDBNull(lector.Item("NombreCorto_MG")) = True, "", lector.Item("NombreCorto_MG")))
                    obj.Estado_MG = CStr(IIf(IsDBNull(lector.Item("Estado_MG")) = True, "", lector.Item("Estado_MG")))
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdMotGasto")) = True, "", lector.Item("IdMotGasto")))
                    obj.IdConcepto = CInt(IIf(IsDBNull(lector.Item("IdConcepto")) = True, "", lector.Item("IdConcepto")))
                    obj.con_Nombre = CStr(IIf(IsDBNull(lector.Item("con_Nombre")) = True, "", lector.Item("con_Nombre")))
                    Lista.Add(obj)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
