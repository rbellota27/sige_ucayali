﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



'******************   LUNES 31 MAYO 2010 HORA 03_23 PM

Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOCampania

    Private objDaoMantenedor As New DAO.DAOMantenedor
    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private cn2 As SqlConnection = (New DAO.Conexion).ConexionSIGE2
    Private cmd As SqlCommand
    Private reader As SqlDataReader
    Dim objConexion As New DAO.Conexion
    Public Function Campania_SelectxParams(ByVal IdCampania As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Descripcion As String, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal Estado As Boolean) As DataTable

        Dim ds As New DataSet

        Try

            Dim p() As SqlParameter = New SqlParameter(6) {}
            p(0) = objDaoMantenedor.getParam(IdCampania, "@IdCampania", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(Descripcion, "@Descripcion", SqlDbType.VarChar)
            p(4) = objDaoMantenedor.getParam(FechaInicio, "@FechaInicio", SqlDbType.Date)
            p(5) = objDaoMantenedor.getParam(FechaFin, "@FechaFin", SqlDbType.Date)
            p(6) = objDaoMantenedor.getParam(Estado, "@Estado", SqlDbType.Bit)

            cmd = New SqlCommand("_Campania_SelectxParams", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Campania")

        Catch ex As Exception

            Throw ex
        Finally

        End Try

        Return ds.Tables("DT_Campania")

    End Function

    Public Function Campania_SelectxParams2(ByVal IdCampania As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal Descripcion As String, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal Estado As Boolean) As DataTable

        Dim ds As New DataSet

        Try

            Dim p() As SqlParameter = New SqlParameter(6) {}
            p(0) = objDaoMantenedor.getParam(IdCampania, "@IdCampania", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(Descripcion, "@Descripcion", SqlDbType.VarChar)
            p(4) = objDaoMantenedor.getParam(FechaInicio, "@FechaInicio", SqlDbType.Date)
            p(5) = objDaoMantenedor.getParam(FechaFin, "@FechaFin", SqlDbType.Date)
            p(6) = objDaoMantenedor.getParam(Estado, "@Estado", SqlDbType.Bit)

            cmd = New SqlCommand("_Campania_SelectxParams", cn2)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Campania")

        Catch ex As Exception

            Throw ex
        Finally

        End Try

        Return ds.Tables("DT_Campania")

    End Function

    Public Function Campania_SelectxIdCampania(ByVal IdCampania As Integer) As Entidades.Campania

        Dim objCampania As Entidades.Campania = Nothing

        Try

            Dim p() As SqlParameter = New SqlParameter(6) {}
            p(0) = objDaoMantenedor.getParam(IdCampania, "@IdCampania", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(Nothing, "@IdEmpresa", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(Nothing, "@IdTienda", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(Nothing, "@Descripcion", SqlDbType.VarChar)
            p(4) = objDaoMantenedor.getParam(Nothing, "@FechaInicio", SqlDbType.Date)
            p(5) = objDaoMantenedor.getParam(Nothing, "@FechaFin", SqlDbType.Date)
            p(6) = objDaoMantenedor.getParam(True, "@Estado", SqlDbType.Bit)

            cn.Open()
            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_Campania_SelectxParams", p)

            If (reader.Read) Then

                objCampania = New Entidades.Campania

                With objCampania

                    .IdCampania = objDaoMantenedor.UCInt(reader("IdCampania"))
                    .IdEmpresa = objDaoMantenedor.UCInt(reader("IdEmpresa"))
                    .IdTienda = objDaoMantenedor.UCInt(reader("IdTienda"))
                    .Descripcion = objDaoMantenedor.UCStr(reader("cam_Descripcion"))
                    .FechaInicio = objDaoMantenedor.UCDate(reader("cam_FechaInicio"))
                    .FechaFin = objDaoMantenedor.UCDate(reader("cam_FechaFin"))
                    .Estado = objDaoMantenedor.UCBool(reader("cam_Estado"))
                    .IdUsuarioInsert = objDaoMantenedor.UCInt(reader("IdUsuarioInsert"))
                    .FechaInsert = objDaoMantenedor.UCDate(reader("cam_FechaInsert"))
                    .IdUsuarioUpdate = objDaoMantenedor.UCInt(reader("IdUsuarioUpdate"))
                    .FechaUpdate = objDaoMantenedor.UCDate(reader("cam_FechaUpdate"))
                    .Empresa = objDaoMantenedor.UCStr(reader("per_NComercial"))
                    .Tienda = objDaoMantenedor.UCStr(reader("tie_Nombre"))

                End With

            End If
            reader.Close()
            

        Catch ex As Exception

            Throw ex

        Finally

            If (cn.State = ConnectionState.Open) Then cn.Close()

        End Try

        Return objCampania

    End Function
    Public Function Campania_Registrar(ByVal objCampania As Entidades.Campania, ByVal IdUsuario As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer

        Dim p() As SqlParameter = New SqlParameter(7) {}
        p(0) = objDaoMantenedor.getParam(objCampania.IdCampania, "@IdCampania", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(objCampania.IdEmpresa, "@IdEmpresa", SqlDbType.Int)
        p(2) = objDaoMantenedor.getParam(objCampania.IdTienda, "@IdTienda", SqlDbType.Int)
        p(3) = objDaoMantenedor.getParam(objCampania.Descripcion, "@cam_Descripcion", SqlDbType.VarChar)
        p(4) = objDaoMantenedor.getParam(objCampania.FechaInicio, "@cam_FechaInicio", SqlDbType.Date)
        p(5) = objDaoMantenedor.getParam(objCampania.FechaFin, "@cam_FechaFin", SqlDbType.Date)
        p(6) = objDaoMantenedor.getParam(objCampania.Estado, "@cam_Estado", SqlDbType.Bit)
        p(7) = objDaoMantenedor.getParam(IdUsuario, "@IdUsuario", SqlDbType.Int)

        Return CInt(SqlHelper.ExecuteScalar(tr, CommandType.StoredProcedure, "_Campania_Registrar", p))

    End Function
End Class
