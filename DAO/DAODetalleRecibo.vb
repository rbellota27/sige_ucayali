'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Nagamine Molina, Jorge
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 20-Octubre-2009
'Hora    : 01:00 pm
'*************************************************

Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data


Public Class DAODetalleRecibo
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Sub InsertaDetalleRecibo(ByVal detallerecibo As Entidades.DetalleRecibo, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(6) {}

        p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        p(0).Value = detallerecibo.IdDocumento

        p(1) = New SqlParameter("@dr_Concepto", SqlDbType.VarChar)
        p(1).Value = IIf(detallerecibo.Concepto = Nothing, DBNull.Value, detallerecibo.Concepto)

        p(2) = New SqlParameter("@dr_Monto", SqlDbType.Decimal)
        p(2).Value = IIf(detallerecibo.Monto = Nothing, DBNull.Value, detallerecibo.Monto)

        p(3) = New SqlParameter("@IdConcepto", SqlDbType.Int)
        p(3).Value = IIf(detallerecibo.IdConcepto = Nothing, DBNull.Value, detallerecibo.IdConcepto)

        p(4) = New SqlParameter("@IdDocumentoAfecto", SqlDbType.Int)
        p(4).Value = IIf(detallerecibo.IdDocumentoAfecto = Nothing, DBNull.Value, detallerecibo.IdDocumentoAfecto)

        p(5) = New SqlParameter("@IdMovCuentaAfecto", SqlDbType.Int)
        p(5).Value = IIf(detallerecibo.IdMovCuentaAfecto = Nothing, DBNull.Value, detallerecibo.IdMovCuentaAfecto)

        p(6) = New SqlParameter("@TipoRecibo", SqlDbType.Char)
        p(6).Value = detallerecibo.TipoRecibo


        HDAO.InsertaT(cn, "_DetalleReciboInsert", p, tr)

    End Sub
    Public Function ActualizaDetalleRecibo(ByVal detallerecibo As Entidades.DetalleRecibo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@IdRecibo", SqlDbType.Int)
        ArrayParametros(0).Value = detallerecibo.IdRecibo
        ArrayParametros(1) = New SqlParameter("@dr_Concepto", SqlDbType.VarChar)
        ArrayParametros(1).Value = detallerecibo.Concepto
        ArrayParametros(2) = New SqlParameter("@dr_Monto", SqlDbType.Decimal)
        ArrayParametros(2).Value = detallerecibo.Monto
        ArrayParametros(3) = New SqlParameter("@IdConcepto", SqlDbType.Int)
        ArrayParametros(3).Value = detallerecibo.IdConcepto
        Return HDAO.Update(cn, "_DetalleReciboUpdate", ArrayParametros)
    End Function
    '********************************************************
    'M�todo fnInsTblDetalleRecibo, para obtener IdDetalleRecibo 
    'Autor   : Hans Fernando, Quispe Espinoza
    'M�dulo  : Caja-Documento
    'Sistema : Indusfer
    'Empresa : Digrafic SRL
    'Fecha   : 8-Set-2009
    'Parametros : BEDetalleRecibo
    'Retorna : IdDetalleRecibo
    '********************************************************
    Public Function fnInsTblDetalleRecibo(ByVal poBETblDetalleRecibo As Entidades.DetalleRecibo) As Integer

        Dim resul As Integer
        'conexion
        Dim loTx As SqlTransaction
        Dim loCn As New SqlConnection()
        loCn.ConnectionString = objConexion.ConexionSIGE.ConnectionString.ToString()

        'parametros
        Dim par() As SqlParameter = New SqlParameter(4) {}
        par(0) = New SqlParameter("@IdConcepto", System.Data.SqlDbType.Int)
        par(0).Value = poBETblDetalleRecibo.IdConcepto
        par(1) = New SqlParameter("@dr_Monto", System.Data.SqlDbType.Decimal)
        par(1).Value = poBETblDetalleRecibo.Monto
        par(2) = New SqlParameter("@dr_Concepto", System.Data.SqlDbType.VarChar, 50)
        par(2).Value = poBETblDetalleRecibo.Concepto
        par(3) = New SqlParameter("@IdDocumento", System.Data.SqlDbType.VarChar, 50)
        par(3).Value = poBETblDetalleRecibo.IdRecibo
        par(4) = New SqlParameter("@IdRecibo", System.Data.SqlDbType.Int)
        par(4).Direction = ParameterDirection.Output
        loCn.Open()
        loTx = loCn.BeginTransaction()
        Try
            resul = Convert.ToInt32(Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteScalar(loTx, CommandType.StoredProcedure, "fnInsTblDetalleRecibo", par))
            loTx.Commit()
        Catch e As Exception
            loTx.Rollback()
            Throw (e)
        Finally
            loCn.Close()
            loCn.Dispose()
        End Try
        Return (resul)
    End Function
    '********************************************************
    'M�todo Din, para obtener DetalleRecibo por cualquier campo
    'Autor   : Hans Fernando, Quispe Espinoza
    'M�dulo  : Caja-Documento
    'Sistema : Indusfer
    'Empresa : Digrafic SRL
    'Fecha   : 24-Set-2009
    'Parametros : psWhere, psOrder
    'Retorna : Lista de tipo DetalleRecibo
    '********************************************************
    Public Function fnSelTblDetalleReciboDin(ByVal psWhere As String, ByVal psOrder As String) As List(Of Entidades.DetalleRecibo)
        Dim loList As New List(Of Entidades.DetalleRecibo)()
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Dim par As SqlParameter() = New SqlParameter(1) {}
        par(0) = New SqlParameter("@WhereCondition", System.Data.SqlDbType.VarChar, 500)
        par(0).Value = psWhere
        par(1) = New SqlParameter("@OrderByExpression", System.Data.SqlDbType.VarChar, 250)
        par(1).Value = psOrder
        Try
            Dim loDr As SqlDataReader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "usp_SelDetalleReciboDin", par)
            While (loDr.Read())
                Dim loDetalleRecibo As New Entidades.DetalleRecibo
                loDetalleRecibo.IdRecibo = Convert.ToInt32(CDec(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdDocumento"))) = True, Nothing, loDr.GetValue(loDr.GetOrdinal("IdDocumento")))))
                loDetalleRecibo.IdConcepto = Convert.ToInt32(CDec(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdDetalleRecibo"))) = True, Nothing, loDr.GetValue(loDr.GetOrdinal("IdDetalleRecibo")))))
                loDetalleRecibo.Concepto = Convert.ToString(CStr(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("dr_Concepto"))) = True, "", loDr.GetValue(loDr.GetOrdinal("dr_Conceptop")))))
                loDetalleRecibo.Monto = Convert.ToDecimal(CDec(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("dr_Monto"))) = True, Nothing, loDr.GetValue(loDr.GetOrdinal("dr_Monto")))))
                loList.Add(loDetalleRecibo)
            End While
            loDr.Close()
        Catch e As Exception
            Throw e
        Finally

        End Try
        Return loList
    End Function

    Public Function DocumentoReciboIngresoSelectDet(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleReciboView_1)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoReciboIngresoSelectDet", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleReciboView_1)
                Do While lector.Read
                    Dim obj As New Entidades.DetalleReciboView_1
                    With obj
                        .Concepto = CStr(IIf(IsDBNull(lector("DescConcepto")) = True, "", lector("DescConcepto")))
                        .IdDocumento = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                        .IdDetalleRecibo = CInt(IIf(IsDBNull(lector("IdDetalleRecibo")) = True, 0, lector("IdDetalleRecibo")))
                        .Monto = CDec(IIf(IsDBNull(lector("dr_Monto")) = True, 0, lector("dr_Monto")))
                        .IdConcepto = CInt(IIf(IsDBNull(lector("IdConcepto")) = True, 0, lector("IdConcepto")))
                        .Moneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")) = True, "", lector("mon_Simbolo")))

                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function DocumentoReciboEgresoSelectDet(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleReciboView_1)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DocumentoReciboEgresoSelectDet", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleReciboView_1)
                Do While lector.Read



                    Dim obj As New Entidades.DetalleReciboView_1
                    With obj
                        .Concepto = CStr(IIf(IsDBNull(lector("DescConcepto")) = True, "", lector("DescConcepto")))
                        .IdDocumento = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                        .IdDetalleRecibo = CInt(IIf(IsDBNull(lector("IdDetalleRecibo")) = True, 0, lector("IdDetalleRecibo")))
                        .Monto = CDec(IIf(IsDBNull(lector("dr_Monto")) = True, 0, lector("dr_Monto")))
                        .IdConcepto = CInt(IIf(IsDBNull(lector("IdConcepto")) = True, 0, lector("IdConcepto")))
                        .Moneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")) = True, "", lector("mon_Simbolo")))

                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function DetalleReciboSelectxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleRecibo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DetalleReciboSelectxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DetalleRecibo)
                Do While lector.Read
                    Dim obj As New Entidades.DetalleRecibo
                    With obj

                        .IdDocumento = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                        .IdDetalleRecibo = CInt(IIf(IsDBNull(lector("IdDetalleRecibo")) = True, 0, lector("IdDetalleRecibo")))
                        .Concepto = CStr(IIf(IsDBNull(lector("dr_Concepto")) = True, "", lector("dr_Concepto")))
                        .Monto = CDec(IIf(IsDBNull(lector("dr_Monto")) = True, 0, lector("dr_Monto")))
                        .IdConcepto = CInt(IIf(IsDBNull(lector("IdConcepto")) = True, 0, lector("IdConcepto")))


                    End With

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

End Class
