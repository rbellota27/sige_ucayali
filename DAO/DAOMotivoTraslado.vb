'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Chang Carnero, Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 03-Noviembre-2009
'Hora    : 05:30 pm
'*************************************************
Imports System.Data.SqlClient

Public Class DAOMotivoTraslado
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    'Public Function InsertaMotivoTraslado(ByVal motivotraslado As Entidades.MotivoTraslado) As Boolean
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
    '    ArrayParametros(0) = New SqlParameter("@mt_Nombre", SqlDbType.VarChar)
    '    ArrayParametros(0).Value = motivotraslado.Nombre
    '    ArrayParametros(1) = New SqlParameter("@mt_Estado", SqlDbType.Char)
    '    ArrayParametros(1).Value = motivotraslado.Estado
    '    Return HDAO.Insert(cn, "_MotivoTrasladoInsert", ArrayParametros)
    'End Function
    Public Function InsertaMotivoTraslado(ByVal MotivoTras As Entidades.MotivoTraslado, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer
        'Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@mt_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = MotivoTras.Nombre
        ArrayParametros(1) = New SqlParameter("@mt_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = MotivoTras.Estado
        ArrayParametros(2) = New SqlParameter("@mt_Comprometedor", SqlDbType.Bit)
        ArrayParametros(2).Value = MotivoTras.DocumentoReferente
        ArrayParametros(3) = New SqlParameter("@mt_Id", SqlDbType.Int)
        ArrayParametros(3).Direction = ParameterDirection.Output
        Dim cmd As New SqlCommand("_MotivoTrasladoInsert", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        cmd.ExecuteNonQuery()
        Return CInt(cmd.Parameters("@mt_Id").Value)
    End Function
    'Public Function ActualizaMotivoTraslado(ByVal motivotraslado As Entidades.MotivoTraslado) As Boolean
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
    '    ArrayParametros(0) = New SqlParameter("@mt_Nombre", SqlDbType.VarChar)
    '    ArrayParametros(0).Value = motivotraslado.Nombre
    '    ArrayParametros(1) = New SqlParameter("@mt_Estado", SqlDbType.Char)
    '    ArrayParametros(1).Value = motivotraslado.Estado
    '    ArrayParametros(2) = New SqlParameter("@IdMotivoT", SqlDbType.Int)
    '    ArrayParametros(2).Value = motivotraslado.Id
    '    Return HDAO.Update(cn, "_MotivoTrasladoUpdate", ArrayParametros)
    'End Function
    Public Function ActualizaMotivoTraslado(ByVal motivotraslado As Entidades.MotivoTraslado, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        'Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@mt_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = motivotraslado.Nombre
        ArrayParametros(1) = New SqlParameter("@mt_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = motivotraslado.Estado
        ArrayParametros(2) = New SqlParameter("@mt_Comprometedor", SqlDbType.Bit)
        ArrayParametros(2).Value = motivotraslado.DocumentoReferente
        ArrayParametros(3) = New SqlParameter("@IdMotivoT", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(motivotraslado.Id = Nothing, DBNull.Value, motivotraslado.Id)
        Dim cmd As New SqlCommand("_MotivoTrasladoUpdate", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        If (cmd.ExecuteNonQuery = 0) Then
            Return False
        End If
        Return True
    End Function
    Public Function SelectAll() As List(Of Entidades.MotivoTraslado)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoTrasladoSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MotivoTraslado)
                Do While lector.Read
                    Dim obj As New Entidades.MotivoTraslado
                    obj.Estado = CStr(lector.Item("mt_Estado"))
                    obj.Nombre = CStr(lector.Item("mt_Nombre"))
                    obj.DocumentoReferente = CBool(IIf(IsDBNull(lector.Item("mt_Comprometedor")), False, lector.Item("mt_Comprometedor")))
                    obj.Id = CInt(lector.Item("IdMotivoT"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.MotivoTraslado)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoTrasladoSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MotivoTraslado)
                Do While lector.Read
                    Dim obj As New Entidades.MotivoTraslado
                    obj.Estado = CStr(lector.Item("mt_Estado"))
                    obj.Nombre = CStr(lector.Item("mt_Nombre"))
                    obj.DocumentoReferente = CBool(IIf(IsDBNull(lector.Item("mt_Comprometedor")), False, lector.Item("mt_Comprometedor")))
                    obj.Id = CInt(lector.Item("IdMotivoT"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.MotivoTraslado)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoTrasladoSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MotivoTraslado)
                Do While lector.Read
                    Dim obj As New Entidades.MotivoTraslado
                    obj.Estado = CStr(lector.Item("mt_Estado"))
                    obj.Nombre = CStr(lector.Item("mt_Nombre"))
                    obj.DocumentoReferente = CBool(IIf(IsDBNull(lector.Item("mt_Comprometedor")), False, lector.Item("mt_Comprometedor")))
                    obj.Id = CInt(lector.Item("IdMotivoT"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.MotivoTraslado)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoTrasladoSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MotivoTraslado)
                Do While lector.Read
                    Dim obj As New Entidades.MotivoTraslado
                    obj.Estado = CStr(lector.Item("mt_Estado"))
                    obj.Nombre = CStr(lector.Item("mt_Nombre"))
                    obj.DocumentoReferente = CBool(IIf(IsDBNull(lector.Item("mt_Comprometedor")), False, lector.Item("mt_Comprometedor")))
                    obj.Id = CInt(lector.Item("IdMotivoT"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.MotivoTraslado)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoTrasladoSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MotivoTraslado)
                Do While lector.Read
                    Dim obj As New Entidades.MotivoTraslado
                    obj.Estado = CStr(lector.Item("mt_Estado"))
                    obj.Nombre = CStr(lector.Item("mt_Nombre"))
                    obj.DocumentoReferente = CBool(IIf(IsDBNull(lector.Item("mt_Comprometedor")), False, lector.Item("mt_Comprometedor")))
                    obj.Id = CInt(lector.Item("IdMotivoT"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.MotivoTraslado)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoTrasladoSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MotivoTraslado)
                Do While lector.Read
                    Dim obj As New Entidades.MotivoTraslado
                    obj.Estado = CStr(lector.Item("mt_Estado"))
                    obj.Nombre = CStr(lector.Item("mt_Nombre"))
                    obj.DocumentoReferente = CBool(IIf(IsDBNull(lector.Item("mt_Comprometedor")), False, lector.Item("mt_Comprometedor")))
                    obj.Id = CInt(lector.Item("IdMotivoT"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.MotivoTraslado)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoTrasladoSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MotivoTraslado)
                Do While lector.Read
                    Dim obj As New Entidades.MotivoTraslado
                    obj.Estado = CStr(lector.Item("mt_Estado"))
                    obj.Nombre = CStr(lector.Item("mt_Nombre"))
                    obj.DocumentoReferente = CBool(IIf(IsDBNull(lector.Item("mt_Comprometedor")), False, lector.Item("mt_Comprometedor")))
                    obj.Id = CInt(lector.Item("IdMotivoT"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    'Para EDITAR la TipoOperacion mediante el IdMotivoT
    'Public Function _TipoDocumento_TipoOperacionSelectxIdMotivoT(ByVal IdTraslado As Integer) As List(Of Entidades.MotivoT_TipoOperacion)
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim cmd As New SqlCommand("_MotivoT_TipoOperacionSelectxIdMotivoT", cn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.AddWithValue("@idTraslado", IdTraslado)
    '    Dim lector As SqlDataReader
    '    Try
    '        Using cn
    '            cn.Open()
    '            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
    '            Dim lista As New List(Of Entidades.MotivoT_TipoOperacion)
    '            Do While lector.Read
    '                Dim obj As New Entidades.MotivoT_TipoOperacion
    '                obj.IdMotivoT = CInt(lector.Item("IdMotivoT"))
    '                obj.idTipoOperac = CInt(lector.Item("idTipoOperac"))
    '                obj.NombreTipoOperacion = CStr(lector.Item("NombreTipoOperacion"))
    '                lista.Add(obj)
    '            Loop
    '            lector.Close()
    '            Return lista
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    Public Function SelectComprometedorxIdMotivoT(ByVal IdMotivoT As Integer) As Boolean
        Dim comprometedor As Boolean = False
        Try
            Dim cn As SqlConnection = objConexion.ConexionSIGE
            Dim cmd As New SqlCommand("_MotivoTrasladoSelectComprometedorxIdMotivoT", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdMotivoT", IdMotivoT)
            cn.Open()
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If lector.Read Then
                comprometedor = CBool(IIf(IsDBNull(lector.Item("mt_Comprometedor")) = True, False, lector.Item("mt_Comprometedor")))
            End If
            lector.Close()
        Catch ex As Exception
            Throw ex

        End Try
        Return comprometedor
    End Function



End Class