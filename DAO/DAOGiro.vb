'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOGiro
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaGiro(ByVal giro As Entidades.Giro) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@giro_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = giro.Descripcion
        ArrayParametros(1) = New SqlParameter("@giro_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = giro.Estado
        Return HDAO.Insert(cn, "_GiroInsert", ArrayParametros)
    End Function
    Public Function ActualizaGiro(ByVal giro As Entidades.Giro) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@giro_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = giro.Descripcion
        ArrayParametros(1) = New SqlParameter("@giro_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = giro.Estado
        ArrayParametros(2) = New SqlParameter("@IdGiro", SqlDbType.Int)
        ArrayParametros(2).Value = giro.Id
        Return HDAO.Update(cn, "_GiroUpdate", ArrayParametros)
    End Function
    Public Function SelectAll() As List(Of Entidades.Giro)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_GiroSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Giro)
                Do While lector.Read
                    Dim obj As New Entidades.Giro
                    obj.Id = CInt(lector.Item("IdGiro"))
                    obj.Descripcion = CStr(lector.Item("giro_Nombre"))
                    obj.Estado = CStr(lector.Item("giro_Estado"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Giro)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_GiroSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Giro)
                Do While lector.Read
                    Dim obj As New Entidades.Giro
                    obj.Id = CInt(lector.Item("IdGiro"))
                    obj.Descripcion = CStr(lector.Item("giro_Nombre"))
                    obj.Estado = CStr(lector.Item("giro_Estado"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Giro)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_GiroSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Giro)
                Do While lector.Read
                    Dim obj As New Entidades.Giro
                    obj.Id = CInt(lector.Item("IdGiro"))
                    obj.Descripcion = CStr(lector.Item("giro_Nombre"))
                    obj.Estado = CStr(lector.Item("giro_Estado"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Giro)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_GiroSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Giro)
                Do While lector.Read
                    Dim obj As New Entidades.Giro
                    obj.Id = CInt(lector.Item("IdGiro"))
                    obj.Descripcion = CStr(lector.Item("giro_Nombre"))
                    obj.Estado = CStr(lector.Item("giro_Estado"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Giro)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_GiroSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Giro)
                Do While lector.Read
                    Dim obj As New Entidades.Giro
                    obj.Id = CInt(lector.Item("IdGiro"))
                    obj.Descripcion = CStr(lector.Item("giro_Nombre"))
                    obj.Estado = CStr(lector.Item("giro_Estado"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Giro)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_GiroSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Giro)
                Do While lector.Read
                    Dim obj As New Entidades.Giro
                    obj.Id = CInt(lector.Item("IdGiro"))
                    obj.Descripcion = CStr(lector.Item("giro_Nombre"))
                    obj.Estado = CStr(lector.Item("giro_Estado"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Giro)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_GiroSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Giro)
                Do While lector.Read
                    Dim obj As New Entidades.Giro
                    obj.Id = CInt(lector.Item("IdGiro"))
                    obj.Descripcion = CStr(lector.Item("giro_Nombre"))
                    obj.Estado = CStr(lector.Item("giro_Estado"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCbo() As List(Of Entidades.Giro)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_GiroSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Giro)
                Do While lector.Read
                    Dim Giro As New Entidades.Giro
                    Giro.Id = CInt(lector.Item("IdGiro"))
                    Giro.Descripcion = CStr(lector.Item("giro_Nombre"))
                    Lista.Add(Giro)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
