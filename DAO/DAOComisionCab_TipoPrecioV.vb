﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'********************   JUEVES 10 JUNIO 2010 HORA 12_46 PM

Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAOComisionCab_TipoPrecioV

    Private objDaoMantenedor As New DAO.DAOMantenedor
    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private reader As SqlDataReader

    Public Sub DeletexIdComisionCabxIdTipoPv(ByVal IdComisionCab As Integer, ByVal IdTipoPv As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim P() As SqlParameter = New SqlParameter(1) {}
        P(0) = objDaoMantenedor.getParam(IdComisionCab, "@IdComisionCab", SqlDbType.Int)
        P(1) = objDaoMantenedor.getParam(IdTipoPv, "@IdTipoPv", SqlDbType.Int)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_ComisionCab_TipoPv_DeletexIdComisionCabxIdTipoPv", P)

    End Sub

    Public Function Registrar(ByVal objComisionCab_TipoPV As Entidades.ComisionCab_TipoPrecioV, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer

        Dim p() As SqlParameter = New SqlParameter(2) {}
        p(0) = objDaoMantenedor.getParam(objComisionCab_TipoPV.IdComisionCab, "@IdComisionCab", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(objComisionCab_TipoPV.IdTipoPV, "@IdTipoPV", SqlDbType.Int)
        p(2) = objDaoMantenedor.getParam(objComisionCab_TipoPV.Estado, "@com_Estado", SqlDbType.Bit)

        Return CInt(SqlHelper.ExecuteScalar(tr, CommandType.StoredProcedure, "_ComisionCab_TipoPrecioV_Registrar", p))

    End Function

    Public Function ComisionCab_TipoPrecioV_SelectxParams(ByVal IdComisionCab As Integer) As List(Of Entidades.ComisionCab_TipoPrecioV)

        Dim listaComisionCab_TipoPv As New List(Of Entidades.ComisionCab_TipoPrecioV)

        Try

            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = objDaoMantenedor.getParam(IdComisionCab, "@IdComisionCab", SqlDbType.Int)

            cn.Open()
            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_ComisionCab_TipoPrecioV_SelectxParams", p)
            While (reader.Read)

                Dim objComisionCab_TipoPv As New Entidades.ComisionCab_TipoPrecioV

                With objComisionCab_TipoPv

                    .IdComisionCab = objDaoMantenedor.UCInt(reader("IdComisionCab"))
                    .IdTipoPV = objDaoMantenedor.UCInt(reader("IdTipoPV"))
                    .Estado = objDaoMantenedor.UCBool(reader("Estado"))
                    .TipoPrecioV = objDaoMantenedor.UCStr(reader("TipoPrecioV"))

                End With

                listaComisionCab_TipoPv.Add(objComisionCab_TipoPv)

            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return listaComisionCab_TipoPv

    End Function

End Class
