'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.


'**************** LUNES 17 05 2010 HORA 06 00 PM

Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAOMedioPago
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Private objDaoMantenedor As New DAO.DAOMantenedor
    Private cn As SqlConnection
    Private reader As SqlDataReader





    Public Function MedioPago_MovBanco(ByVal IdTipoConceptoBanco As Integer) As List(Of Entidades.MedioPago)

        Dim lista As List(Of Entidades.MedioPago)
        Dim obj As Entidades.MedioPago

        Dim cmd As SqlCommand
        cn = objConexion.ConexionSIGE

        Try
            cn.Open()
            cmd = New SqlCommand("_MedioPago_MovBanco", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdTipoConceptoBanco", IdTipoConceptoBanco)

            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            lista = New List(Of Entidades.MedioPago)
            While reader.Read
                obj = New Entidades.MedioPago
                With obj
                    .Id = CInt(reader("IdMedioPago"))
                    .Nombre = CStr(reader("mp_Nombre"))
                End With
                lista.Add(obj)
                obj = Nothing
            End While

            reader.Close()

            Return lista

        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

        Return Nothing

    End Function









    ''*************************22 junio 2010
#Region "Mantenimiento"
    Public Function InsertarMedioPago_ListaTipoDocumento(ByVal obj As Entidades.MedioPago, _
   ByVal ListaTipoDoc_MedioP As List(Of Entidades.TipoDocumento_MedioPago)) As Boolean
        Dim IdMedioPago As Integer = 0
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(8) {}
        ArrayParametros(0) = New SqlParameter("@mp_CodigoSunat", SqlDbType.Char)
        ArrayParametros(0).Value = IIf(obj.CodigoSunat = Nothing, DBNull.Value, obj.CodigoSunat)
        ArrayParametros(1) = New SqlParameter("@mp_Nombre", SqlDbType.VarChar)
        ArrayParametros(1).Value = IIf(obj.Nombre = Nothing, DBNull.Value, obj.Nombre)
        ArrayParametros(2) = New SqlParameter("@mp_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = IIf(obj.Estado = Nothing, DBNull.Value, obj.Estado)
        ArrayParametros(3) = New SqlParameter("@IdMedioPagoInterfaz", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(obj.IdMedioPagoInterfaz = Nothing, DBNull.Value, obj.IdMedioPagoInterfaz)
        ArrayParametros(4) = New SqlParameter("@mp_Principal", SqlDbType.Bit)
        ArrayParametros(4).Value = IIf(obj.Principal = Nothing, False, obj.Principal)
        ArrayParametros(5) = New SqlParameter("@mp_CuentaContable", SqlDbType.VarChar)
        ArrayParametros(5).Value = IIf(obj.mp_CuentaContable = Nothing, DBNull.Value, obj.mp_CuentaContable)
        ArrayParametros(6) = New SqlParameter("@IdConceptoMovBanco", SqlDbType.Int)
        ArrayParametros(6).Value = IIf(obj.IdConceptoMovBanco = Nothing, DBNull.Value, obj.IdConceptoMovBanco)

        ArrayParametros(7) = New SqlParameter("@Abrev", SqlDbType.VarChar)
        ArrayParametros(7).Value = IIf(obj.Abrev = Nothing, DBNull.Value, obj.Abrev)
        ArrayParametros(8) = New SqlParameter("@mp_autoMovbanco", SqlDbType.Bit)
        ArrayParametros(8).Value = obj.mp_autoMovbanco


        Dim tr As SqlTransaction = Nothing
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim cmd As New SqlCommand("_MedioPagoInsert", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(ArrayParametros)
            IdMedioPago = CInt(cmd.ExecuteScalar())
            If ListaTipoDoc_MedioP.Count > 0 Then
                Me.InsertaListaTipoDoc_MedioP(cn, IdMedioPago, ListaTipoDoc_MedioP, tr)
            End If
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return True
    End Function
    Public Function ActualizarMedioPago_ListaTipoDoc(ByVal obj As Entidades.MedioPago, _
                    ByVal ListaTipoDoc_MedioP As List(Of Entidades.TipoDocumento_MedioPago)) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(9) {}
        ArrayParametros(0) = New SqlParameter("@mp_CodigoSunat", SqlDbType.Char)
        ArrayParametros(0).Value = IIf(obj.CodigoSunat = Nothing, DBNull.Value, obj.CodigoSunat)
        ArrayParametros(1) = New SqlParameter("@mp_Nombre", SqlDbType.VarChar)
        ArrayParametros(1).Value = IIf(obj.Nombre = Nothing, DBNull.Value, obj.Nombre)
        ArrayParametros(2) = New SqlParameter("@mp_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = IIf(obj.Estado = Nothing, DBNull.Value, obj.Estado)
        ArrayParametros(3) = New SqlParameter("@IdMedioPagoInterfaz", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(obj.IdMedioPagoInterfaz = Nothing, DBNull.Value, obj.IdMedioPagoInterfaz)
        ArrayParametros(4) = New SqlParameter("@mp_Principal", SqlDbType.Bit)
        ArrayParametros(4).Value = IIf(obj.Principal = Nothing, False, obj.Principal)
        ArrayParametros(5) = New SqlParameter("@mp_CuentaContable", SqlDbType.VarChar)
        ArrayParametros(5).Value = IIf(obj.mp_CuentaContable = Nothing, DBNull.Value, obj.mp_CuentaContable)
        ArrayParametros(6) = New SqlParameter("@IdMedioPago", SqlDbType.Int)
        ArrayParametros(6).Value = IIf(obj.Id = Nothing, DBNull.Value, obj.Id)
        ArrayParametros(7) = New SqlParameter("@IdConceptoMovBanco", SqlDbType.Int)
        ArrayParametros(7).Value = IIf(obj.IdConceptoMovBanco = Nothing, DBNull.Value, obj.IdConceptoMovBanco)
        ArrayParametros(8) = New SqlParameter("@Abrev", SqlDbType.VarChar)
        ArrayParametros(8).Value = IIf(obj.Abrev = Nothing, DBNull.Value, obj.Abrev)
        ArrayParametros(9) = New SqlParameter("@mp_autoMovbanco", SqlDbType.Bit)
        ArrayParametros(9).Value = obj.mp_autoMovbanco


        Dim tr As SqlTransaction = Nothing
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim cmd As New SqlCommand("_MedioPagoUpdate", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(ArrayParametros)
            cmd.ExecuteNonQuery()
            Me.InsertaListaTipoDoc_MedioP(cn, obj.Id, ListaTipoDoc_MedioP, tr)
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return True
    End Function
    Public Function InsertaListaTipoDoc_MedioP(ByVal cn As SqlConnection, ByVal IdMedioPAgo As Integer, _
                      ByVal lista_TipoDoc_MedioP As List(Of Entidades.TipoDocumento_MedioPago), _
                      ByVal Tr As SqlTransaction) As Boolean
        'borrar=0--> borra todo en inserta; borrar=1--> inserta; borrar=2-->borra todo
        Dim Borrar As Integer = 0

        If lista_TipoDoc_MedioP.Count = 0 Then
            Borrar = 2
            Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
            ArrayParametros(0) = New SqlParameter("@IdMedioPago", SqlDbType.Int)
            ArrayParametros(0).Value = IdMedioPAgo
            ArrayParametros(1) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
            ArrayParametros(1).Value = DBNull.Value
            ArrayParametros(2) = New SqlParameter("@tdmp_Estado", SqlDbType.Int)
            ArrayParametros(2).Value = DBNull.Value
            ArrayParametros(3) = New SqlParameter("@borrar", SqlDbType.Int)
            ArrayParametros(3).Value = Borrar
            Dim cmd As New SqlCommand("_InsertarTipoDocumento_MedioPago", cn, Tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(ArrayParametros)
            cmd.ExecuteNonQuery()
        End If
        For Each obj As Entidades.TipoDocumento_MedioPago In lista_TipoDoc_MedioP
            Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
            ArrayParametros(0) = New SqlParameter("@IdMedioPago", SqlDbType.Int)
            ArrayParametros(0).Value = IdMedioPAgo
            ArrayParametros(1) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
            ArrayParametros(1).Value = IIf(obj.IdTipoDocumento = Nothing, DBNull.Value, obj.IdTipoDocumento)
            ArrayParametros(2) = New SqlParameter("@tdmp_Estado", SqlDbType.Bit)
            ArrayParametros(2).Value = IIf(obj.Estado = Nothing, False, obj.Estado)
            ArrayParametros(3) = New SqlParameter("@borrar", SqlDbType.Int)
            ArrayParametros(3).Value = IIf(Borrar = 0, DBNull.Value, Borrar)


            Dim cmd As New SqlCommand("_InsertarTipoDocumento_MedioPago", cn, Tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(ArrayParametros)
            Borrar = CInt(cmd.ExecuteScalar)
        Next
        Return True
    End Function
    Public Function SelectCboMpInterfaz() As List(Of Entidades.MedioPago)
        Dim Lista As New List(Of Entidades.MedioPago)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ListarMediopagoInterfaz", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.MedioPago
                With obj
                    .Id = CInt(lector("IdMedioPagoInterfaz"))
                    .mpi_Descripcion = CStr(lector("mpi_Descripcion"))
                End With
                Lista.Add(obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function
    Public Function ListarTipoDoc_MedioP(ByVal idMedioPago As Integer) As List(Of Entidades.TipoDocumento_MedioPago)
        Dim ListaTipoDoc As New List(Of Entidades.TipoDocumento_MedioPago)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ListarTipoDoc_MedioP", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idMedioPago", idMedioPago)
        Dim lector As SqlDataReader
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.TipoDocumento_MedioPago
                With obj
                    .IdMedioPago = CInt(lector("IdMedioPago"))
                    .IdTipoDocumento = CInt(lector("IdTipoDocumento"))
                    .Estado = CBool(IIf(IsDBNull(lector("tdmp_Estado")), False, lector("tdmp_Estado")))
                End With
                ListaTipoDoc.Add(obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return ListaTipoDoc
    End Function

#End Region


    Public Function SelectxIdTipoDocumentoxIdCondicionPago(ByVal IdTipoDocumento As Integer, ByVal IdCondicionPago As Integer) As List(Of Entidades.MedioPago)

        Dim lista As New List(Of Entidades.MedioPago)

        Try

            Dim p() As SqlParameter = New SqlParameter(1) {}
            p(0) = objDaoMantenedor.getParam(IdTipoDocumento, "@IdTipoDocumento", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdCondicionPago, "@IdCondicionPago", SqlDbType.Int)

            cn = (objConexion).ConexionSIGE
            cn.Open()

            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_MedioPago_SelectxIdTipoDocumentoxIdCondicionPago", p)

            While (reader.Read)

                Dim obj As New Entidades.MedioPago

                With obj

                    .Id = objDaoMantenedor.UCInt(reader("IdMedioPago"))
                    .CodigoSunat = objDaoMantenedor.UCStr(reader("mp_CodigoSunat"))
                    .Nombre = objDaoMantenedor.UCStr(reader("mp_Nombre"))
                    .mp_CuentaContable = objDaoMantenedor.UCStr(reader("mp_CuentaContable"))
                    .Estado = objDaoMantenedor.UCStr(reader("mp_Estado"))
                    .Principal = objDaoMantenedor.UCBool(reader("mp_Principal"))
                    .IdMedioPagoInterfaz = objDaoMantenedor.UCInt(reader("IdMedioPagoInterfaz"))
                    .IdConceptoMovBanco = objDaoMantenedor.UCInt(reader("IdConceptoMovBanco"))

                End With

                lista.Add(obj)

            End While

            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (Me.cn.State = ConnectionState.Open) Then Me.cn.Close()
        End Try

        Return lista

    End Function

    Public Function SelectIdMedioPagoPrincipal() As Integer

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MedioPagoSelectIdMedioPagoPrincipal", cn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim IdMedioPagoPrincipal As Integer = 0

        Try
            Using cn
                cn.Open()

                IdMedioPagoPrincipal = CInt(cmd.ExecuteScalar)

            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return IdMedioPagoPrincipal
    End Function
    Public Function SelectCbo() As List(Of Entidades.MedioPago)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MedioPagoSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MedioPago)
                Do While lector.Read
                    Dim MedioPago As New Entidades.MedioPago
                    MedioPago.Id = CInt(lector.Item("IdMedioPago"))
                    MedioPago.Nombre = CStr(lector.Item("mp_Nombre"))
                    Lista.Add(MedioPago)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.MedioPago)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MedioPagoSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MedioPago)
                Do While lector.Read
                    Dim MedioPago As New Entidades.MedioPago
                    MedioPago.Id = CInt(lector.Item("IdMedioPago"))
                    MedioPago.Nombre = CStr(lector.Item("mp_Nombre"))
                    MedioPago.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("mp_CodigoSunat")) = True, "", lector.Item("mp_CodigoSunat")))
                    MedioPago.Estado = CStr(IIf(IsDBNull(lector.Item("mp_Estado")) = True, "", lector.Item("mp_Estado")))
                    MedioPago.mp_CuentaContable = CStr(IIf(IsDBNull(lector.Item("mp_CuentaContable")) = True, "", lector.Item("mp_CuentaContable")))
                    MedioPago.Principal = CBool(IIf(IsDBNull(lector.Item("mp_Principal")) = True, "0", lector.Item("mp_Principal")))
                    MedioPago.IdMedioPagoInterfaz = CInt(IIf(IsDBNull(lector("IdMedioPagoInterfaz")) = True, 0, lector("IdMedioPagoInterfaz")))
                    MedioPago.IdConceptoMovBanco = CInt(IIf(IsDBNull(lector.Item("IdConceptoMovBanco")), 0, lector.Item("IdConceptoMovBanco")))
                    MedioPago.IdTipoConceptoBanco = CInt(IIf(IsDBNull(lector.Item("IdTipoConceptoBanco")), 0, lector.Item("IdTipoConceptoBanco")))
                    MedioPago.Abrev = CStr(IIf(IsDBNull(lector.Item("mp_Abv")), "", lector.Item("mp_Abv")))

                    Lista.Add(MedioPago)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.MedioPago)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MedioPagoSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MedioPago)
                Do While lector.Read
                    Dim MedioPago As New Entidades.MedioPago
                    MedioPago.Id = CInt(lector.Item("IdMedioPago"))
                    MedioPago.Nombre = CStr(lector.Item("mp_Nombre"))
                    MedioPago.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("mp_CodigoSunat")) = True, "", lector.Item("mp_CodigoSunat")))
                    MedioPago.Estado = CStr(IIf(IsDBNull(lector.Item("mp_Estado")) = True, "", lector.Item("mp_Estado")))
                    MedioPago.mp_CuentaContable = CStr(IIf(IsDBNull(lector.Item("mp_CuentaContable")) = True, "", lector.Item("mp_CuentaContable")))
                    MedioPago.Principal = CBool(IIf(IsDBNull(lector.Item("mp_Principal")) = True, "0", lector.Item("mp_Principal")))
                    MedioPago.IdMedioPagoInterfaz = CInt(IIf(IsDBNull(lector("IdMedioPagoInterfaz")) = True, 0, lector("IdMedioPagoInterfaz")))
                    MedioPago.IdConceptoMovBanco = CInt(IIf(IsDBNull(lector.Item("IdConceptoMovBanco")), 0, lector.Item("IdConceptoMovBanco")))
                    MedioPago.IdTipoConceptoBanco = CInt(IIf(IsDBNull(lector.Item("IdTipoConceptoBanco")), 0, lector.Item("IdTipoConceptoBanco")))
                    MedioPago.Abrev = CStr(IIf(IsDBNull(lector.Item("mp_Abv")), "", lector.Item("mp_Abv")))
                    MedioPago.mp_autoMovbanco = CBool(IIf(IsDBNull(lector.Item("mp_autoMovbanco")) = True, "0", lector.Item("mp_autoMovbanco")))
                    Lista.Add(MedioPago)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.MedioPago)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MedioPagoSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MedioPago)
                Do While lector.Read
                    Dim MedioPago As New Entidades.MedioPago
                    MedioPago.Id = CInt(lector.Item("IdMedioPago"))
                    MedioPago.Nombre = CStr(lector.Item("mp_Nombre"))
                    MedioPago.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("mp_CodigoSunat")) = True, "", lector.Item("mp_CodigoSunat")))
                    MedioPago.Estado = CStr(IIf(IsDBNull(lector.Item("mp_Estado")) = True, "", lector.Item("mp_Estado")))
                    MedioPago.mp_CuentaContable = CStr(IIf(IsDBNull(lector.Item("mp_CuentaContable")) = True, "", lector.Item("mp_CuentaContable")))
                    MedioPago.Principal = CBool(IIf(IsDBNull(lector.Item("mp_Principal")) = True, "0", lector.Item("mp_Principal")))
                    MedioPago.IdMedioPagoInterfaz = CInt(IIf(IsDBNull(lector("IdMedioPagoInterfaz")) = True, 0, lector("IdMedioPagoInterfaz")))
                    MedioPago.IdConceptoMovBanco = CInt(IIf(IsDBNull(lector.Item("IdConceptoMovBanco")), 0, lector.Item("IdConceptoMovBanco")))
                    MedioPago.IdTipoConceptoBanco = CInt(IIf(IsDBNull(lector.Item("IdTipoConceptoBanco")), 0, lector.Item("IdTipoConceptoBanco")))
                    MedioPago.Abrev = CStr(IIf(IsDBNull(lector.Item("mp_Abv")), "", lector.Item("mp_Abv")))

                    Lista.Add(MedioPago)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.MedioPago)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MedioPagoSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MedioPago)
                Do While lector.Read
                    Dim MedioPago As New Entidades.MedioPago
                    MedioPago.Id = CInt(lector.Item("IdMedioPago"))
                    MedioPago.Nombre = CStr(lector.Item("mp_Nombre"))
                    MedioPago.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("mp_CodigoSunat")) = True, "", lector.Item("mp_CodigoSunat")))
                    MedioPago.Estado = CStr(IIf(IsDBNull(lector.Item("mp_Estado")) = True, "", lector.Item("mp_Estado")))
                    MedioPago.mp_CuentaContable = CStr(IIf(IsDBNull(lector.Item("mp_CuentaContable")) = True, "", lector.Item("mp_CuentaContable")))
                    MedioPago.Principal = CBool(IIf(IsDBNull(lector.Item("mp_Principal")) = True, "0", lector.Item("mp_Principal")))
                    MedioPago.IdMedioPagoInterfaz = CInt(IIf(IsDBNull(lector("IdMedioPagoInterfaz")) = True, 0, lector("IdMedioPagoInterfaz")))
                    MedioPago.IdConceptoMovBanco = CInt(IIf(IsDBNull(lector.Item("IdConceptoMovBanco")), 0, lector.Item("IdConceptoMovBanco")))
                    MedioPago.IdTipoConceptoBanco = CInt(IIf(IsDBNull(lector.Item("IdTipoConceptoBanco")), 0, lector.Item("IdTipoConceptoBanco")))
                    MedioPago.Abrev = CStr(IIf(IsDBNull(lector.Item("mp_Abv")), "", lector.Item("mp_Abv")))
                    MedioPago.mp_autoMovbanco = CBool(IIf(IsDBNull(lector.Item("mp_autoMovbanco")) = True, "0", lector.Item("mp_autoMovbanco")))

                    Lista.Add(MedioPago)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.MedioPago)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MedioPagoSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MedioPago)
                Do While lector.Read
                    Dim MedioPago As New Entidades.MedioPago
                    MedioPago.Id = CInt(lector.Item("IdMedioPago"))
                    MedioPago.Nombre = CStr(lector.Item("mp_Nombre"))
                    MedioPago.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("mp_CodigoSunat")) = True, "", lector.Item("mp_CodigoSunat")))
                    MedioPago.Estado = CStr(IIf(IsDBNull(lector.Item("mp_Estado")) = True, "", lector.Item("mp_Estado")))
                    MedioPago.mp_CuentaContable = CStr(IIf(IsDBNull(lector.Item("mp_CuentaContable")) = True, "", lector.Item("mp_CuentaContable")))
                    MedioPago.Principal = CBool(IIf(IsDBNull(lector.Item("mp_Principal")) = True, "0", lector.Item("mp_Principal")))
                    MedioPago.IdMedioPagoInterfaz = CInt(IIf(IsDBNull(lector("IdMedioPagoInterfaz")) = True, 0, lector("IdMedioPagoInterfaz")))
                    MedioPago.IdConceptoMovBanco = CInt(IIf(IsDBNull(lector.Item("IdConceptoMovBanco")), 0, lector.Item("IdConceptoMovBanco")))
                    MedioPago.IdTipoConceptoBanco = CInt(IIf(IsDBNull(lector.Item("IdTipoConceptoBanco")), 0, lector.Item("IdTipoConceptoBanco")))
                    MedioPago.Abrev = CStr(IIf(IsDBNull(lector.Item("mp_Abv")), "", lector.Item("mp_Abv")))
                    MedioPago.mp_autoMovbanco = CBool(IIf(IsDBNull(lector.Item("mp_autoMovbanco")) = True, "0", lector.Item("mp_autoMovbanco")))

                    Lista.Add(MedioPago)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.MedioPago)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MedioPagoSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MedioPago)
                Do While lector.Read
                    Dim MedioPago As New Entidades.MedioPago
                    MedioPago.Id = CInt(lector.Item("IdMedioPago"))
                    MedioPago.Nombre = CStr(lector.Item("mp_Nombre"))
                    MedioPago.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("mp_CodigoSunat")) = True, "", lector.Item("mp_CodigoSunat")))
                    MedioPago.Estado = CStr(IIf(IsDBNull(lector.Item("mp_Estado")) = True, "", lector.Item("mp_Estado")))
                    MedioPago.mp_CuentaContable = CStr(IIf(IsDBNull(lector.Item("mp_CuentaContable")) = True, "", lector.Item("mp_CuentaContable")))
                    MedioPago.Principal = CBool(IIf(IsDBNull(lector.Item("mp_Principal")) = True, "0", lector.Item("mp_Principal")))
                    MedioPago.IdMedioPagoInterfaz = CInt(IIf(IsDBNull(lector("IdMedioPagoInterfaz")) = True, 0, lector("IdMedioPagoInterfaz")))
                    MedioPago.IdConceptoMovBanco = CInt(IIf(IsDBNull(lector.Item("IdConceptoMovBanco")), 0, lector.Item("IdConceptoMovBanco")))
                    MedioPago.IdTipoConceptoBanco = CInt(IIf(IsDBNull(lector.Item("IdTipoConceptoBanco")), 0, lector.Item("IdTipoConceptoBanco")))
                    MedioPago.Abrev = CStr(IIf(IsDBNull(lector.Item("mp_Abv")), "", lector.Item("mp_Abv")))
                    MedioPago.mp_autoMovbanco = CBool(IIf(IsDBNull(lector.Item("mp_autoMovbanco")) = True, "0", lector.Item("mp_autoMovbanco")))

                    Lista.Add(MedioPago)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.MedioPago)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MedioPagoSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MedioPago)
                Do While lector.Read
                    Dim MedioPago As New Entidades.MedioPago
                    MedioPago.Id = CInt(lector.Item("IdMedioPago"))
                    MedioPago.Nombre = CStr(lector.Item("mp_Nombre"))
                    MedioPago.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("mp_CodigoSunat")) = True, "", lector.Item("mp_CodigoSunat")))
                    MedioPago.Estado = CStr(IIf(IsDBNull(lector.Item("mp_Estado")) = True, "", lector.Item("mp_Estado")))
                    MedioPago.mp_CuentaContable = CStr(IIf(IsDBNull(lector.Item("mp_CuentaContable")) = True, "", lector.Item("mp_CuentaContable")))
                    MedioPago.Principal = CBool(IIf(IsDBNull(lector.Item("mp_Principal")) = True, "0", lector.Item("mp_Principal")))
                    MedioPago.IdMedioPagoInterfaz = CInt(IIf(IsDBNull(lector("IdMedioPagoInterfaz")) = True, 0, lector("IdMedioPagoInterfaz")))
                    MedioPago.IdConceptoMovBanco = CInt(IIf(IsDBNull(lector.Item("IdConceptoMovBanco")), 0, lector.Item("IdConceptoMovBanco")))
                    MedioPago.IdTipoConceptoBanco = CInt(IIf(IsDBNull(lector.Item("IdTipoConceptoBanco")), 0, lector.Item("IdTipoConceptoBanco")))
                    MedioPago.Abrev = CStr(IIf(IsDBNull(lector.Item("mp_Abv")), "", lector.Item("mp_Abv")))
                    MedioPago.mp_autoMovbanco = CBool(IIf(IsDBNull(lector.Item("mp_autoMovbanco")) = True, "0", lector.Item("mp_autoMovbanco")))

                    Lista.Add(MedioPago)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxCodSunat(ByVal codSunat As String) As List(Of Entidades.MedioPago)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MedioPagoSelectxCodSunat", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Cod", codSunat)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MedioPago)
                Do While lector.Read
                    Dim MedioPago As New Entidades.MedioPago
                    MedioPago.Id = CInt(lector.Item("IdMedioPago"))
                    MedioPago.Nombre = CStr(lector.Item("mp_Nombre"))
                    MedioPago.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("mp_CodigoSunat")) = True, "", lector.Item("mp_CodigoSunat")))
                    MedioPago.Estado = CStr(IIf(IsDBNull(lector.Item("mp_Estado")) = True, "", lector.Item("mp_Estado")))
                    MedioPago.mp_CuentaContable = CStr(IIf(IsDBNull(lector.Item("mp_CuentaContable")) = True, "", lector.Item("mp_CuentaContable")))
                    MedioPago.Principal = CBool(IIf(IsDBNull(lector.Item("mp_Principal")) = True, "0", lector.Item("mp_Principal")))
                    MedioPago.IdMedioPagoInterfaz = CInt(IIf(IsDBNull(lector("IdMedioPagoInterfaz")) = True, 0, lector("IdMedioPagoInterfaz")))
                    MedioPago.IdConceptoMovBanco = CInt(IIf(IsDBNull(lector.Item("IdConceptoMovBanco")), 0, lector.Item("IdConceptoMovBanco")))
                    MedioPago.IdTipoConceptoBanco = CInt(IIf(IsDBNull(lector.Item("IdTipoConceptoBanco")), 0, lector.Item("IdTipoConceptoBanco")))
                    MedioPago.Abrev = CStr(IIf(IsDBNull(lector.Item("mp_Abv")), "", lector.Item("mp_Abv")))
                    MedioPago.mp_autoMovbanco = CBool(IIf(IsDBNull(lector.Item("mp_autoMovbanco")) = True, "0", lector.Item("mp_autoMovbanco")))

                    Lista.Add(MedioPago)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    'Public Function InsertaMedioPago_CP(ByVal CondicionPago As Entidades.CondicionPago, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer
    '    'Inserta solo Medio Pago
    '    Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
    '    ArrayParametros(0) = New SqlParameter("@cp_Nombre", SqlDbType.VarChar)
    '    ArrayParametros(0).Value = CondicionPago.Nombre
    '    ArrayParametros(1) = New SqlParameter("@cp_Estado", SqlDbType.Char)
    '    ArrayParametros(1).Value = CondicionPago.Estado
    '    ArrayParametros(2) = New SqlParameter("@cp_IdCondicionPago", SqlDbType.Int)
    '    ArrayParametros(2).Direction = ParameterDirection.Output
    '    Dim cmd As New SqlCommand("_CondicionPagoInsertxMedioP", cn, tr)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.AddRange(ArrayParametros)
    '    cmd.ExecuteNonQuery()
    '    Return CInt(cmd.Parameters("@cp_IdCondicionPago").Value)
    'End Function


    'Public Function ActualizaMedioPago_condicionP(ByVal CondicionPago As Entidades.CondicionPago, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
    '    Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
    '    ArrayParametros(0) = New SqlParameter("@cp_Nombre", SqlDbType.VarChar)
    '    ArrayParametros(0).Value = CondicionPago.Nombre
    '    ArrayParametros(1) = New SqlParameter("@cp_Estado", SqlDbType.Char)
    '    ArrayParametros(1).Value = CondicionPago.Estado
    '    ArrayParametros(2) = New SqlParameter("@IdCondicionPago", SqlDbType.Int)
    '    ArrayParametros(2).Value = IIf(CondicionPago.Id = Nothing, DBNull.Value, CondicionPago.Id)
    '    Dim cmd As New SqlCommand("_CondicionPagoUpdate", cn, tr)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.AddRange(ArrayParametros)
    '    If (cmd.ExecuteNonQuery = 0) Then
    '        Return False
    '    End If
    '    Return True
    'End Function

End Class
