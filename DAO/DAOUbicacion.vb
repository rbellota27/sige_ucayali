'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOUbicacion
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaUbicacion(ByVal ubicacion As Entidades.Ubicacion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@ubi_Anaquel", SqlDbType.Int)
        ArrayParametros(0).Value = ubicacion.Anaquel
        ArrayParametros(1) = New SqlParameter("@ubi_Fila", SqlDbType.Int)
        ArrayParametros(1).Value = ubicacion.Fila
        ArrayParametros(2) = New SqlParameter("@ubi_Columna", SqlDbType.Int)
        ArrayParametros(2).Value = ubicacion.Columna
        ArrayParametros(3) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(3).Value = ubicacion.Id
        Return HDAO.Insert(cn, "_UbicacionInsert", ArrayParametros)
    End Function
    Public Function ActualizaUbicacion(ByVal ubicacion As Entidades.Ubicacion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@ubi_Anaquel", SqlDbType.Int)
        ArrayParametros(0).Value = ubicacion.Anaquel
        ArrayParametros(1) = New SqlParameter("@ubi_Fila", SqlDbType.Int)
        ArrayParametros(1).Value = ubicacion.Fila
        ArrayParametros(2) = New SqlParameter("@ubi_Columna", SqlDbType.Int)
        ArrayParametros(2).Value = ubicacion.Columna
        ArrayParametros(3) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(3).Value = ubicacion.IdProducto
        ArrayParametros(4) = New SqlParameter("@IdUbicacion", SqlDbType.Int)
        ArrayParametros(4).Value = ubicacion.Id
        Return HDAO.Update(cn, "_UbicacionUpdate", ArrayParametros)
    End Function
    Public Function SelectxIdProducto(ByVal IdProducto As Integer) As List(Of Entidades.Ubicacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UbicacionSelectxIdProducto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Ubicacion)
                Do While lector.Read
                    Dim objUb As New Entidades.Ubicacion
                    objUb.Anaquel = CInt(lector.Item("ubi_Anaquel"))
                    objUb.Columna = CInt(lector.Item("ubi_Columna"))
                    objUb.Fila = CInt(lector.Item("ubi_Fila"))
                    objUb.Id = CInt(lector.Item("IdUbicacion"))
                    objUb.IdProducto = CInt(lector.Item("IdProducto"))
                    Lista.Add(objUb)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxIdUbicacion(ByVal IdUbicacion As Integer) As List(Of Entidades.Ubicacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UbicacionSelectxIdUbicacion", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdUbicacion", IdUbicacion)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Ubicacion)
                Do While lector.Read
                    Dim objUb As New Entidades.Ubicacion
                    objUb.Anaquel = CInt(lector.Item("ubi_Anaquel"))
                    objUb.Columna = CInt(lector.Item("ubi_Columna"))
                    objUb.Fila = CInt(lector.Item("ubi_Fila"))
                    objUb.Id = CInt(lector.Item("IdUbicacion"))
                    objUb.IdProducto = CInt(lector.Item("IdProducto"))
                    Lista.Add(objUb)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.Ubicacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UbicacionSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Ubicacion)
                Do While lector.Read
                    Dim objUb As New Entidades.Ubicacion
                    objUb.Anaquel = CInt(lector.Item("ubi_Anaquel"))
                    objUb.Columna = CInt(lector.Item("ubi_Columna"))
                    objUb.Fila = CInt(lector.Item("ubi_Fila"))
                    objUb.Id = CInt(lector.Item("IdUbicacion"))
                    objUb.IdProducto = CInt(lector.Item("IdProducto"))
                    Lista.Add(objUb)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
