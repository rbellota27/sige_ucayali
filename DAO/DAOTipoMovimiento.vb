'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOTipoMovimiento
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaTipoMovimiento(ByVal tipomovimiento As Entidades.TipoMovimiento) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@tm_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = tipomovimiento.Nombre
        ArrayParametros(1) = New SqlParameter("@tm_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = tipomovimiento.Estado
        Return HDAO.Insert(cn, "_TipoMovimientoInsert", ArrayParametros)
    End Function
    Public Function ActualizaTipoMovimiento(ByVal tipomovimiento As Entidades.TipoMovimiento) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@tm_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = tipomovimiento.Nombre
        ArrayParametros(1) = New SqlParameter("@tm_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = tipomovimiento.Estado
        ArrayParametros(2) = New SqlParameter("@IdTipoMovimiento", SqlDbType.Int)
        ArrayParametros(2).Value = tipomovimiento.Id
        Return HDAO.Update(cn, "_TipoMovimientoUpdate", ArrayParametros)
    End Function
    Public Function SelectAll() As List(Of Entidades.TipoMovimiento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoMovimientoSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoMovimiento)
                Do While lector.Read
                    Dim obj As New Entidades.TipoMovimiento
                    obj.Estado = CStr(lector.Item("tm_Estado"))
                    obj.Nombre = CStr(lector.Item("tm_Nombre"))
                    obj.Id = CInt(lector.Item("IdTipoMovimiento"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.TipoMovimiento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoMovimientoSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoMovimiento)
                Do While lector.Read
                    Dim obj As New Entidades.TipoMovimiento
                    obj.Estado = CStr(lector.Item("tm_Estado"))
                    obj.Nombre = CStr(lector.Item("tm_Nombre"))
                    obj.Id = CInt(lector.Item("IdTipoMovimiento"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.TipoMovimiento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoMovimientoSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoMovimiento)
                Do While lector.Read
                    Dim obj As New Entidades.TipoMovimiento
                    obj.Estado = CStr(lector.Item("tm_Estado"))
                    obj.Nombre = CStr(lector.Item("tm_Nombre"))
                    obj.Id = CInt(lector.Item("IdTipoMovimiento"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.TipoMovimiento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoMovimientoSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoMovimiento)
                Do While lector.Read
                    Dim obj As New Entidades.TipoMovimiento
                    obj.Estado = CStr(lector.Item("tm_Estado"))
                    obj.Nombre = CStr(lector.Item("tm_Nombre"))
                    obj.Id = CInt(lector.Item("IdTipoMovimiento"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoMovimiento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoMovimientoSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoMovimiento)
                Do While lector.Read
                    Dim obj As New Entidades.TipoMovimiento
                    obj.Estado = CStr(lector.Item("tm_Estado"))
                    obj.Nombre = CStr(lector.Item("tm_Nombre"))
                    obj.Id = CInt(lector.Item("IdTipoMovimiento"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoMovimiento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoMovimientoSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoMovimiento)
                Do While lector.Read
                    Dim obj As New Entidades.TipoMovimiento
                    obj.Estado = CStr(lector.Item("tm_Estado"))
                    obj.Nombre = CStr(lector.Item("tm_Nombre"))
                    obj.Id = CInt(lector.Item("IdTipoMovimiento"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.TipoMovimiento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoMovimientoSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoMovimiento)
                Do While lector.Read
                    Dim obj As New Entidades.TipoMovimiento
                    obj.Estado = CStr(lector.Item("tm_Estado"))
                    obj.Nombre = CStr(lector.Item("tm_Nombre"))
                    obj.Id = CInt(lector.Item("IdTipoMovimiento"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
