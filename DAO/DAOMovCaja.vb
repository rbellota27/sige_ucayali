'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOMovCaja
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Sub InsertaMovCaja(ByVal Cn As SqlConnection, ByVal movcaja As Entidades.MovCaja, ByVal T As SqlTransaction)
        'Dim cn As SqlConnection = objConexion.ConexionSIGE

        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}

        ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = movcaja.IdDocumento
        ArrayParametros(1) = New SqlParameter("@IdCaja", SqlDbType.Int)
        ArrayParametros(1).Value = movcaja.IdCaja
        ArrayParametros(2) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(2).Value = movcaja.IdTienda
        ArrayParametros(3) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(3).Value = movcaja.IdEmpresa
        ArrayParametros(4) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(4).Value = movcaja.IdPersona
        ArrayParametros(5) = New SqlParameter("@IdTipoMovimiento", SqlDbType.Int)
        ArrayParametros(5).Value = movcaja.IdTipoMovimiento

        HDAO.InsertaT(Cn, "_MovCajaInsert", ArrayParametros, T)

    End Sub
    Public Sub ActualizaMovCaja(ByVal movcaja As Entidades.MovCaja)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}

        ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = movcaja.IdDocumento
        ArrayParametros(1) = New SqlParameter("@IdCaja", SqlDbType.Int)
        ArrayParametros(1).Value = movcaja.IdCaja
        ArrayParametros(2) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(2).Value = movcaja.IdTienda
        ArrayParametros(3) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(3).Value = movcaja.IdEmpresa
        ArrayParametros(4) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(4).Value = movcaja.IdPersona
        ArrayParametros(5) = New SqlParameter("@IdTipoMovimiento", SqlDbType.Int)
        ArrayParametros(5).Value = movcaja.IdTipoMovimiento

        HDAO.Update(cn, "_MovCajaUpdate", ArrayParametros)
    End Sub


    Public Function IngresosDiariosxParams(ByVal IdTienda As Integer, ByVal IdEmpresa As Integer) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_MovCajaIngresosDiariosxParams", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
            cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DSIngresoCajaDiaria")
        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function
    Public Sub DeletexIdDocumento(ByVal cn As SqlConnection, ByVal T As SqlTransaction, ByVal IdDocumento As Integer)

        Dim cmd As New SqlCommand("_MovCajaDeletexIdDocumento", cn, T)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.ExecuteNonQuery()

    End Sub
    '********************************************************
    'M�todo fnInsTblDetalleRecibo, para obtener IdDetalleRecibo 
    'Autor   : Hans Fernando, Quispe Espinoza
    'M�dulo  : Caja-Documento
    'Sistema : Indusfer
    'Empresa : Digrafic SRL
    'Fecha   : 8-Set-2009
    'Parametros : BEDetalleRecibo
    'Retorna : IdDetalleRecibo
    '********************************************************
    Public Function fnInsTblMovCaja(ByVal poBETblMovCaja As Entidades.MovCaja) As Integer

        Dim resul As Integer
        'conexion
        Dim loTx As SqlTransaction
        Dim loCn As New SqlConnection()
        loCn.ConnectionString = objConexion.ConexionSIGE.ConnectionString.ToString()

        'parametros
        Dim par() As SqlParameter = New SqlParameter(5) {}
        par(0) = New SqlParameter("@IdCaja", System.Data.SqlDbType.Int)
        par(0).Value = poBETblMovCaja.IdCaja
        par(1) = New SqlParameter("@IdDocumento", System.Data.SqlDbType.Int)
        par(1).Value = poBETblMovCaja.IdDocumento
        par(2) = New SqlParameter("@IdEmpresa", System.Data.SqlDbType.Int)
        par(2).Value = poBETblMovCaja.IdEmpresa
        par(3) = New SqlParameter("@IdTipoMovimiento", System.Data.SqlDbType.Int)
        par(3).Value = poBETblMovCaja.IdTipoMovimiento
        par(4) = New SqlParameter("@IdTienda", System.Data.SqlDbType.Int)
        par(4).Value = poBETblMovCaja.IdTienda
        par(5) = New SqlParameter("@IdPersona", System.Data.SqlDbType.Int)
        par(5).Value = poBETblMovCaja.IdPersona
        loCn.Open()
        loTx = loCn.BeginTransaction()
        Try
            resul = Convert.ToInt32(Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(loTx, CommandType.StoredProcedure, "_MovCajaInsert", par))
            loTx.Commit()
        Catch e As Exception
            loTx.Rollback()
            Throw (e)
        Finally
            loCn.Close()
            loCn.Dispose()
        End Try
        Return (resul)
    End Function
End Class
