﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAODocumentosMercantiles
    Dim objConexion As New Conexion

    Public Function pathFacturaElectronica(ByVal direccion As String, ByVal idSerie As Integer, ByVal doc_Codigo As String, ByVal idTipoDocumento As Integer, _
                                           ByVal cn As SqlConnection) As String
        Dim path As String = ""
        Using cmd As New SqlCommand("select dbo.fnPathFacturaElectronica(@idSerie, @doc_Codigo, @idTipoDocumento,  @direccion)", cn)
            With cmd
                .CommandType = CommandType.Text

                .Parameters.Add(New SqlParameter("@idSerie", SqlDbType.Int)).Value = idSerie
                .Parameters.Add(New SqlParameter("@doc_Codigo", SqlDbType.NVarChar)).Value = doc_Codigo
                .Parameters.Add(New SqlParameter("@idTipoDocumento", SqlDbType.Int)).Value = idTipoDocumento
                .Parameters.Add(New SqlParameter("@direccion", SqlDbType.NVarChar)).Value = direccion
            End With

            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleRow)
            If rdr IsNot Nothing Then
                While rdr.Read()
                    path = rdr.GetString(0)
                End While
                rdr.Close()
            End If
            Return path
        End Using
    End Function
    Public Function RptCosteoProyectadoImportacion(ByVal IdDocumento As Integer) As DataSet
        Dim da1, da2, da3 As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Try

            Dim cmd As New SqlCommand()
            cmd.Connection = cn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            cmd.CommandText = "_CR_COSTEOIMPORTACION"
            da1 = New SqlDataAdapter(cmd)
            da1.Fill(ds, "Documento")


            cmd.CommandText = "_CR_COSTEOIMPORTACION_DET"
            da2 = New SqlDataAdapter(cmd)
            da2.Fill(ds, "Detalle")

            cmd.CommandText = "_DetalleGastoSelectxId"
            da3 = New SqlDataAdapter(cmd)
            da3.Fill(ds, "Gasto")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function
    Public Function RptCosteoImportacion(ByVal IdDocumento As Integer) As DataSet
        Dim da1, da2, da3 As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Try

            Dim cmd As New SqlCommand()
            cmd.Connection = cn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            cmd.CommandText = "_CR_COSTEOIMPORTACION"
            da1 = New SqlDataAdapter(cmd)
            da1.Fill(ds, "Documento")


            cmd.CommandText = "_CR_COSTEOIMPORTACION_DET"
            da2 = New SqlDataAdapter(cmd)
            da2.Fill(ds, "Detalle")

            cmd.CommandText = "_DetalleGastoSelectxId"
            da3 = New SqlDataAdapter(cmd)
            da3.Fill(ds, "Gasto")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function

    Public Function RptDocReciboI(ByVal IdDocumento As Integer) As DataSet
        Dim da, da2 As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Try
            Dim cmd As New SqlCommand("_CR_DocMercReciboIE", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            '*********** lleno cabecera
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocCaBecera")

            Dim cmd2 As New SqlCommand("List_ImagenReporte", cn)
            cmd2.CommandType = CommandType.StoredProcedure
            da2 = New SqlDataAdapter(cmd2)
            da2.Fill(ds, "DtRptImagen")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function
    Public Function RptDocReciboE(ByVal IdDocumento As Integer) As DataSet
        Dim da, da2 As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Try
            Dim cmd As New SqlCommand("_CR_DocMercReciboIE", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            '*********** lleno cabecera
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocCaBecera")

            Dim cmd2 As New SqlCommand("List_ImagenReporte", cn)
            cmd2.CommandType = CommandType.StoredProcedure
            da2 = New SqlDataAdapter(cmd2)
            da2.Fill(ds, "DtRptImagen")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function

    Public Function RptDocNotaCredito(ByVal IdDocumento As Integer) As DataSet
        Dim da, da2 As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Try
            Dim cmd As New SqlCommand("_CR_DocNotaCreditoCab", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            '*********** lleno cabecera
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocCaBecera")

            'detalle
            cmd.CommandText = "_CR_DocNotaCreditoDet"
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocDetalle")

            Dim cmd2 As New SqlCommand("List_ImagenReporte", cn)
            cmd2.CommandType = CommandType.StoredProcedure
            da2 = New SqlDataAdapter(cmd2)
            da2.Fill(ds, "DtRptImagen")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds

    End Function
    Public Function getDataSetOrdenCompraNomComercial(ByVal iddocumento As Integer) As DataSet
        Dim da_C As SqlDataAdapter
        Dim da_D, da2 As SqlDataAdapter
        Dim da_CC As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Try
            Dim cmd As New SqlCommand("_CR_SelectDocCompraCabeceraNomComercial", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)
            da_C = New SqlDataAdapter(cmd)
            da_C.Fill(ds, "CR_dtCabecera")

            cmd.CommandText = "_CR_OrdenCompraDet"
            da_D = New SqlDataAdapter(cmd)
            da_C.Fill(ds, "CR_dtDetalle")

            cmd.CommandText = "_cr_CondicionComercial"
            da_CC = New SqlDataAdapter(cmd)
            da_CC.Fill(ds, "CR_CondicionC")


            Dim cmd2 As New SqlCommand("List_ImagenReporte", cn)
            cmd2.CommandType = CommandType.StoredProcedure
            da2 = New SqlDataAdapter(cmd2)
            da2.Fill(ds, "DtRptImagen")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function

    Public Function getDataSetOrdenCompra(ByVal iddocumento As Integer) As DataSet
        Dim da_C As SqlDataAdapter
        Dim da_D, da2 As SqlDataAdapter
        Dim da_CC As SqlDataAdapter
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Dim ds As New DataSet
        Try
            Dim cmd As New SqlCommand("_CR_SelectDocCompraCabecera", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)
            da_C = New SqlDataAdapter(cmd)
            da_C.Fill(ds, "CR_dtCabecera")

            cmd.CommandText = "_CR_OrdenCompraDet"
            da_D = New SqlDataAdapter(cmd)
            da_C.Fill(ds, "CR_dtDetalle")

            cmd.CommandText = "_cr_CondicionComercial"
            da_CC = New SqlDataAdapter(cmd)
            da_CC.Fill(ds, "CR_CondicionC")


            Dim cmd2 As New SqlCommand("List_ImagenReporte", cn)
            cmd2.CommandType = CommandType.StoredProcedure
            da2 = New SqlDataAdapter(cmd2)
            da2.Fill(ds, "DtRptImagen")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function

    Public Function rptDocFactBol(ByVal iddocumento As Integer, ByVal tipoimpresion As Integer) As DataSet

        Dim da, da2 As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Try
            Dim cmd As New SqlCommand("_CR_DocFactBolCab", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)

            '*********** lleno cabecera
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocCaBecera")

            'detalle
            If tipoimpresion = 3 Then
                cmd.CommandText = "_CR_DocFactBolDetAgrup"
                da = New SqlDataAdapter(cmd)
                da.Fill(ds, "DT_DocDetalle")
            Else
                cmd.CommandText = "_CR_DocFactBolDet"
                da = New SqlDataAdapter(cmd)
                da.Fill(ds, "DT_DocDetalle")
            End If


            Dim cmd2 As New SqlCommand("List_ImagenReporte", cn)
            cmd2.CommandType = CommandType.StoredProcedure
            da2 = New SqlDataAdapter(cmd2)
            da2.Fill(ds, "DtRptImagen")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds

    End Function

    Public Function rptDocNotaDebito(ByVal iddocumento As Integer) As DataSet

        Dim da, da2 As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Try
            Dim cmd As New SqlCommand("_CR_DocNotaDebitoCab", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@iddocumento", iddocumento)

            '*********** lleno cabecera
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocCaBecera1")

            'detalle
            cmd.CommandText = "_CR_DocNotaDebitoDet"
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocDetalle2")

            Dim cmd2 As New SqlCommand("List_ImagenReporte", cn)
            cmd2.CommandType = CommandType.StoredProcedure
            da2 = New SqlDataAdapter(cmd2)
            da2.Fill(ds, "DtRptImagen")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds

    End Function

    Public Function rptDocPedidoCliente(ByVal iddocumento As Integer) As DataSet

        Dim da, da2 As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Try
            Dim cmd As New SqlCommand("_CR_DocPedidoClienteCabT", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)

            '*********** lleno cabecera
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocCaBecera")

            'detalle
            cmd.CommandText = "_CR_DocPedidoClienteDet"
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocDetalle")

            Dim cmd2 As New SqlCommand("List_ImagenReporte", cn)
            cmd2.CommandType = CommandType.StoredProcedure
            da2 = New SqlDataAdapter(cmd2)
            da2.Fill(ds, "DtRptImagen")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds

    End Function
    Public Function rptDocControlInterno(ByVal iddocumento As Integer) As DataSet
        ''MODIFICACION DEL SP , DE MODIFICO EL REMITENTE Y DESTINATARIO.
        Dim da, da2 As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Try
            Dim cmd As New SqlCommand("_CR_DocControlInternoCab", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)

            '*********** lleno cabecera
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocCaBecera")


            'detalle
            cmd.CommandText = "_CR_DocControlInternoDet"
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocDetalle")

            Dim cmd2 As New SqlCommand("List_ImagenReporte", cn)
            cmd2.CommandType = CommandType.StoredProcedure
            da2 = New SqlDataAdapter(cmd2)
            da2.Fill(ds, "DtRptImagen")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds

    End Function

    Public Function rptGuiasxTonos(ByVal iddocumento As Integer) As DataSet

        Dim da, da2 As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Try
            Dim cmd As New SqlCommand("_CR_DocGuiaRemisionRemitenteCab", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)

            '*********** lleno cabecera
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocCaBecera")

            'detalle
            cmd.CommandText = "_CR_DocGuiaRemisionRemitenteDet_v2"
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocDetalle")

            Dim cmd2 As New SqlCommand("List_ImagenReporte", cn)
            cmd2.CommandType = CommandType.StoredProcedure
            da2 = New SqlDataAdapter(cmd2)
            da2.Fill(ds, "DtRptImagen")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds

    End Function

    Public Function rptDocGuias(ByVal iddocumento As Integer) As DataSet

        Dim da, da2 As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Try
            Dim cmd As New SqlCommand("_CR_DocGuiaRemisionRemitenteCab", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)

            '*********** lleno cabecera
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocCaBecera")

            'detalle
            cmd.CommandText = "_CR_DocGuiaRemisionRemitenteDet"
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DocDetalle")

            Dim cmd2 As New SqlCommand("List_ImagenReporte", cn)
            cmd2.CommandType = CommandType.StoredProcedure
            da2 = New SqlDataAdapter(cmd2)
            da2.Fill(ds, "DtRptImagen")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds

    End Function
    Public Function getReporteDocCotizacion(ByVal IdDocumento As Integer) As DataSet
        Dim da, da2 As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE

        Try
            Dim cmd As New SqlCommand("_CR_CotizacionCab", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

            '*********** lleno cabecera
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_CabCotizacion")

            '*********** lleno detalle
            cmd.CommandText = "_CR_CotizacionDet"
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_DetCotizacion")

            Dim cmd2 As New SqlCommand("List_ImagenReporte", cn)
            cmd2.CommandType = CommandType.StoredProcedure
            da2 = New SqlDataAdapter(cmd2)
            da2.Fill(ds, "DtRptImagen")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds
    End Function

    Public Function ImpDocsEjm(ByVal IdTipoDocs As Integer) As Integer
        Dim iddocumento As Integer = 0
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Try
            cn.Open()
            Dim cmd As New SqlCommand("EjmDocsImpresion", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idtipodocumento", IdTipoDocs)
            iddocumento = CInt(cmd.ExecuteScalar())
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return iddocumento
    End Function
    Public Function crpImprimir(ByVal idtipodocumento As Integer) As Int16
        Dim iddiseno As Int16 = 0
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Try
            cn.Open()
            Dim cmd As New SqlCommand("getTipoImpresion", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idtipoDocumento", idtipodocumento)
            iddiseno = CShort(cmd.ExecuteScalar())
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return iddiseno
    End Function


End Class
