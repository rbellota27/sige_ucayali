'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOSabor
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaSabor(ByVal sabor As Entidades.Sabor) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@sa_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = sabor.Descripcion
        ArrayParametros(1) = New SqlParameter("@sa_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = sabor.Abv
        ArrayParametros(2) = New SqlParameter("@sa_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = sabor.Estado
        Return HDAO.Insert(cn, "_SaborInsert", ArrayParametros)
    End Function
    Public Function ActualizaSabor(ByVal sabor As Entidades.Sabor) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@sa_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = sabor.Descripcion
        ArrayParametros(1) = New SqlParameter("@sa_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = sabor.Abv
        ArrayParametros(2) = New SqlParameter("@sa_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = sabor.Estado
        ArrayParametros(3) = New SqlParameter("@IdSabor", SqlDbType.Int)
        ArrayParametros(3).Value = sabor.Id
        Return HDAO.Update(cn, "_SaborUpdate", ArrayParametros)
    End Function
    Public Function SaborSelectCbo() As List(Of Entidades.Sabor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SaborSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Sabor)
                Do While lector.Read
                    Dim objSabor As New Entidades.Sabor
                    objSabor.Id = CInt(lector.Item("IdSabor"))
                    objSabor.Descripcion = CStr(lector.Item("sa_Nombre"))
                    Lista.Add(objSabor)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.Sabor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SaborSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Sabor)
                Do While lector.Read
                    Dim obj As New Entidades.Sabor
                    obj.Id = CInt(lector.Item("IdSabor"))
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("sa_Abv")) = True, "", lector.Item("sa_Abv")))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("sa_Nombre")) = True, "", lector.Item("sa_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("sa_Estado")) = True, "", lector.Item("sa_Estado")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Sabor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SaborSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Sabor)
                Do While lector.Read
                    Dim obj As New Entidades.Sabor
                    obj.Id = CInt(lector.Item("IdSabor"))
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("sa_Abv")) = True, "", lector.Item("sa_Abv")))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("sa_Nombre")) = True, "", lector.Item("sa_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("sa_Estado")) = True, "", lector.Item("sa_Estado")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Sabor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SaborSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Sabor)
                Do While lector.Read
                    Dim obj As New Entidades.Sabor
                    obj.Id = CInt(lector.Item("IdSabor"))
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("sa_Abv")) = True, "", lector.Item("sa_Abv")))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("sa_Nombre")) = True, "", lector.Item("sa_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("sa_Estado")) = True, "", lector.Item("sa_Estado")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Sabor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SaborSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Sabor)
                Do While lector.Read
                    Dim obj As New Entidades.Sabor
                    obj.Id = CInt(lector.Item("IdSabor"))
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("sa_Abv")) = True, "", lector.Item("sa_Abv")))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("sa_Nombre")) = True, "", lector.Item("sa_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("sa_Estado")) = True, "", lector.Item("sa_Estado")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Sabor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SaborSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Sabor)
                Do While lector.Read
                    Dim obj As New Entidades.Sabor
                    obj.Id = CInt(lector.Item("IdSabor"))
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("sa_Abv")) = True, "", lector.Item("sa_Abv")))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("sa_Nombre")) = True, "", lector.Item("sa_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("sa_Estado")) = True, "", lector.Item("sa_Estado")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Sabor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SaborSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Sabor)
                Do While lector.Read
                    Dim obj As New Entidades.Sabor
                    obj.Id = CInt(lector.Item("IdSabor"))
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("sa_Abv")) = True, "", lector.Item("sa_Abv")))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("sa_Nombre")) = True, "", lector.Item("sa_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("sa_Estado")) = True, "", lector.Item("sa_Estado")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Sabor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SaborSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Sabor)
                Do While lector.Read
                    Dim obj As New Entidades.Sabor
                    obj.Id = CInt(lector.Item("IdSabor"))
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("sa_Abv")) = True, "", lector.Item("sa_Abv")))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("sa_Nombre")) = True, "", lector.Item("sa_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("sa_Estado")) = True, "", lector.Item("sa_Estado")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
