'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOAreaEmpresa
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Sub InsertaAreaEmpresaT(ByVal cn As SqlConnection, ByVal lareaEmpresa As List(Of Entidades.AreaEmpresa), ByVal T As SqlTransaction)
        Dim AreaEmpresa As Entidades.AreaEmpresa
        For Each AreaEmpresa In lareaEmpresa
            Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
            ArrayParametros(0) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
            ArrayParametros(0).Value = AreaEmpresa.IdEmpresa
            ArrayParametros(1) = New SqlParameter("@IdArea", SqlDbType.Int)
            ArrayParametros(1).Value = AreaEmpresa.Id
            HDAO.InsertaT(cn, "_AreaEmpresaInsert", ArrayParametros, T)
        Next
    End Sub
    Public Function DeletexIdEmpresa(ByVal cn As SqlConnection, ByVal IdEmpresa As Integer, ByVal T As SqlTransaction) As Boolean
        Dim cmd As New SqlCommand("_AreaEmpresaDeleteAllByIdEmpresa", cn, T)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        Try
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Sub GrabaAreaEmpresaT(ByVal cn As SqlConnection, ByVal lareaEmpresa As List(Of Entidades.AreaEmpresa), ByVal T As SqlTransaction)
        Dim AreaEmpresa As Entidades.AreaEmpresa
        For Each AreaEmpresa In lareaEmpresa
            Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
            ArrayParametros(0) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
            ArrayParametros(0).Value = AreaEmpresa.IdEmpresa
            ArrayParametros(1) = New SqlParameter("@IdArea", SqlDbType.Int)
            ArrayParametros(1).Value = AreaEmpresa.Id
            ArrayParametros(2) = New SqlParameter("@Estado", SqlDbType.Bit)
            ArrayParametros(2).Value = AreaEmpresa.EstadoAE
            HDAO.InsertaT(cn, "IndUpd_AreaEmpresa", ArrayParametros, T)
        Next
    End Sub
End Class
