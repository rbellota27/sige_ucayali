﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'''''''    26 julio 2010 Hora 12:08 am



'**************************   MARTES 11 MAYO 2010 HORA 10_45 AM








'**************** VIERNES 18 Marzo 
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class DAOCuentaProveedor
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion


#Region "fx anteriores"
    Public Function InsertaCuentaProveedor(ByVal CuentaProveedor As Entidades.CuentaProveedor) As Integer

        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}

        ArrayParametros(0) = New SqlParameter("@IdProveedor", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(CuentaProveedor.IdProveedor = Nothing, DBNull.Value, CuentaProveedor.IdProveedor)
        ArrayParametros(1) = New SqlParameter("@IdPropietario", SqlDbType.Int)
        ArrayParametros(1).Value = IIf(CuentaProveedor.IdPropietario = Nothing, DBNull.Value, CuentaProveedor.IdPropietario)
        ArrayParametros(2) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        ArrayParametros(2).Value = IIf(CuentaProveedor.IdMoneda = Nothing, DBNull.Value, CuentaProveedor.IdMoneda)
        ArrayParametros(3) = New SqlParameter("@cprov_CargoMax", SqlDbType.Decimal)
        ArrayParametros(3).Value = IIf(CuentaProveedor.cprov_CargoMax = Nothing, DBNull.Value, CuentaProveedor.cprov_CargoMax)
        ArrayParametros(4) = New SqlParameter("@cprov_Saldo", SqlDbType.Decimal)
        ArrayParametros(4).Value = IIf(CuentaProveedor.cprov_Saldo = Nothing, DBNull.Value, CuentaProveedor.cprov_Saldo)
        ArrayParametros(5) = New SqlParameter("@cpro_Estado", SqlDbType.Char)
        ArrayParametros(5).Value = IIf(CuentaProveedor.Estado = Nothing, True, CuentaProveedor.Estado)


        'parametros(5) = New SqlParameter("@cet_Estado", SqlDbType.Bit)
        'parametros(5).Value = IIf(obj.cet_Estado = Nothing, False, obj.cet_Estado)

        'ArrayParametros(6) = New SqlParameter("@IdCuentaProv", SqlDbType.Int)
        'ArrayParametros(6).Direction = ParameterDirection.Output
        Dim tr As SqlTransaction = Nothing
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim IdCuentaProveedor As Integer
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim cmd As New SqlCommand("_CuentaProveedorInsert", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(ArrayParametros)

            IdCuentaProveedor = CInt(cmd.ExecuteScalar) 'Recupera el codigo de tabla
            tr.Commit()
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally

        End Try
        Return IdCuentaProveedor

    End Function
    Public Function ActualizarCuentaProveedor(ByVal cuentaproveedor As Entidades.CuentaProveedor) As Boolean
        Dim ArrayParametros() As SqlParameter = New SqlParameter(6) {}

        ArrayParametros(0) = New SqlParameter("@IdProveedor", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(cuentaproveedor.IdProveedor = Nothing, DBNull.Value, cuentaproveedor.IdProveedor)
        ArrayParametros(1) = New SqlParameter("@IdPropietario", SqlDbType.Int)
        ArrayParametros(1).Value = IIf(cuentaproveedor.IdPropietario = Nothing, DBNull.Value, cuentaproveedor.IdPropietario)
        ArrayParametros(2) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        ArrayParametros(2).Value = IIf(cuentaproveedor.IdMoneda = Nothing, DBNull.Value, cuentaproveedor.IdMoneda)
        ArrayParametros(3) = New SqlParameter("@cprov_CargoMax", SqlDbType.Decimal)
        ArrayParametros(3).Value = IIf(cuentaproveedor.cprov_CargoMax = Nothing, DBNull.Value, cuentaproveedor.cprov_CargoMax)
        ArrayParametros(4) = New SqlParameter("@cprov_Saldo", SqlDbType.Decimal)
        ArrayParametros(4).Value = IIf(cuentaproveedor.cprov_Saldo = Nothing, DBNull.Value, cuentaproveedor.cprov_Saldo)
        ArrayParametros(5) = New SqlParameter("@cpro_Estado", SqlDbType.Char)
        ArrayParametros(5).Value = IIf(cuentaproveedor.cpro_Estado = Nothing, DBNull.Value, cuentaproveedor.cpro_Estado)
        ArrayParametros(6) = New SqlParameter("@IdCuentaProv", SqlDbType.Int)
        ArrayParametros(6).Value = IIf(cuentaproveedor.IdCuentaProv = Nothing, DBNull.Value, cuentaproveedor.IdCuentaProv)

        Dim tr As SqlTransaction = Nothing
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        'Dim IdCuentaProveedor As Boolean
        Try

            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim cmd As New SqlCommand("_CuentaProveedorUpdate", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(ArrayParametros)
            cmd.ExecuteNonQuery() 'Recupera el codigo de tabla
            'cmd.ExecuteReader()

            'If cmd.ExecuteNonQuery = 0 Then
            '    Return False
            'End If
            'Return True

            tr.Commit()



        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally

        End Try
        Return True

    End Function
    Public Function DeletexIdProveedorCuentaProveedor(ByVal IdProveedor As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Dim cmd As New SqlCommand("_CuentaProveedorDeletexIdProveedor", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProveedor", IdProveedor)
        If cmd.ExecuteNonQuery = 0 Then
            Throw New Exception
        End If

    End Function
    Public Function SelectxIdProveedorCuentaProveedor(ByVal IdProveedor As Integer) As List(Of Entidades.CuentaProveedor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CuentaProveedorSelectxIdProvedor", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProveedor", IdProveedor)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.CuentaProveedor)
                Do While lector.Read
                    Dim obj As New Entidades.CuentaProveedor
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdCuentaProv")) = True, 0, lector.Item("IdCuentaProv")))
                    obj.IdProveedor = CInt(IIf(IsDBNull(lector.Item("IdProveedor")) = True, 0, lector.Item("IdProveedor")))
                    obj.IdPropietario = CInt(IIf(IsDBNull(lector.Item("IdPropietario")) = True, 0, lector.Item("IdPropietario")))
                    obj.IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                    obj.Mon_Simbolo = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                    obj.cprov_CargoMax = CDec(IIf(IsDBNull(lector.Item("cprov_CargoMax")) = True, 0, lector.Item("cprov_CargoMax")))
                    'obj.cprov_FechaAlta = CDate(IIf(IsDBNull(lector.Item("cprov_FechaAlta")) = True, Nothing, lector.Item("cprov_FechaAlta")))
                    obj.cprov_Saldo = CDec(IIf(IsDBNull(lector.Item("cprov_Saldo")) = True, 0, lector.Item("cprov_Saldo")))
                    obj.cpro_Estado = CStr(IIf(IsDBNull(lector.Item("cpro_Estado")) = True, "", lector.Item("cpro_Estado")))
                    lista.Add(obj)
                Loop
                cn.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxIdProv_IdPropCuentaProveedor(ByVal IdProveedor As Integer, ByVal IdPropietario As Integer) As List(Of Entidades.CuentaProveedor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CuentaProveedorSelectxIdProv_IdProp", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProveedor", IdProveedor)
        cmd.Parameters.AddWithValue("@IdPropietario", IdPropietario)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.CuentaProveedor)
                Do While lector.Read
                    Dim obj As New Entidades.CuentaProveedor
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdCuentaProv")) = True, 0, lector.Item("IdCuentaProv")))
                    obj.IdProveedor = CInt(IIf(IsDBNull(lector.Item("IdProveedor")) = True, 0, lector.Item("IdProveedor")))
                    obj.IdPropietario = CInt(IIf(IsDBNull(lector.Item("IdPropietario")) = True, 0, lector.Item("IdPropietario")))
                    obj.IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                    obj.Mon_Simbolo = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                    obj.cprov_CargoMax = CDec(IIf(IsDBNull(lector.Item("cprov_CargoMax")) = True, 0, lector.Item("cprov_CargoMax")))
                    'obj.cprov_FechaAlta = CDate(IIf(IsDBNull(lector.Item("cprov_FechaAlta")) = True, Nothing, lector.Item("cprov_FechaAlta")))
                    obj.cprov_Saldo = CDec(IIf(IsDBNull(lector.Item("cprov_Saldo")) = True, 0, lector.Item("cprov_Saldo")))  '*******  DISPONIBLE
                    obj.cpro_Estado = CStr(IIf(IsDBNull(lector.Item("cpro_Estado")) = True, "", lector.Item("cpro_Estado")))
                    obj.DeudaPendiente = CDec(IIf(IsDBNull(lector.Item("DeudaPendiente")) = True, 0, lector.Item("DeudaPendiente")))
                    lista.Add(obj)
                Loop
                cn.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllCuentaProveedor() As List(Of Entidades.CuentaProveedor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CuentaProveedorSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.CuentaProveedor)
                Do While lector.Read
                    Dim obj As New Entidades.CuentaProveedor
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdCuentaProv")) = True, 0, lector.Item("IdCuentaProv")))
                    obj.IdProveedor = CInt(IIf(IsDBNull(lector.Item("IdProveedor")) = True, 0, lector.Item("IdProveedor")))
                    obj.IdPropietario = CInt(IIf(IsDBNull(lector.Item("IdPropietario")) = True, 0, lector.Item("IdPropietario")))
                    obj.IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                    obj.Mon_Simbolo = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                    obj.cprov_CargoMax = CDec(IIf(IsDBNull(lector.Item("cprov_CargoMax")) = True, 0, lector.Item("cprov_CargoMax")))
                    obj.cprov_Saldo = CDec(IIf(IsDBNull(lector.Item("cprov_Saldo")) = True, 0, lector.Item("cprov_Saldo")))
                    obj.cprov_FechaAlta = CStr(IIf(IsDBNull(lector.Item("cprov_FechaAlta")) = True, Nothing, lector.Item("cprov_FechaAlta")))
                    obj.cpro_Estado = CStr(IIf(IsDBNull(lector.Item("cpro_Estado")) = True, "", lector.Item("cpro_Estado")))
                    lista.Add(obj)
                Loop
                cn.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoCuentaProveedor() As List(Of Entidades.CuentaProveedor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CuentaProveedorSelectActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.CuentaProveedor)
                Do While lector.Read
                    Dim obj As New Entidades.CuentaProveedor
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdCuentaProv")) = True, 0, lector.Item("IdCuentaProv")))
                    obj.IdProveedor = CInt(IIf(IsDBNull(lector.Item("IdProveedor")) = True, 0, lector.Item("IdProveedor")))
                    obj.IdPropietario = CInt(IIf(IsDBNull(lector.Item("IdPropietario")) = True, 0, lector.Item("IdPropietario")))
                    obj.IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                    obj.Mon_Simbolo = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                    obj.cprov_CargoMax = CDec(IIf(IsDBNull(lector.Item("cprov_CargoMax")) = True, 0, lector.Item("cprov_CargoMax")))
                    obj.cprov_Saldo = CDec(IIf(IsDBNull(lector.Item("cprov_Saldo")) = True, 0, lector.Item("cprov_Saldo")))
                    obj.cprov_FechaAlta = CStr(IIf(IsDBNull(lector.Item("cprov_FechaAlta")) = True, Nothing, lector.Item("cprov_FechaAlta")))
                    obj.cpro_Estado = CStr(IIf(IsDBNull(lector.Item("cpro_Estado")) = True, "", lector.Item("cpro_Estado")))
                    lista.Add(obj)
                Loop
                cn.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoCuentaProveedor() As List(Of Entidades.CuentaProveedor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CuentaProveedorSelectInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.CuentaProveedor)
                Do While lector.Read
                    Dim obj As New Entidades.CuentaProveedor
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdCuentaProv")) = True, 0, lector.Item("IdCuentaProv")))
                    obj.IdProveedor = CInt(IIf(IsDBNull(lector.Item("IdProveedor")) = True, 0, lector.Item("IdProveedor")))
                    obj.IdPropietario = CInt(IIf(IsDBNull(lector.Item("IdPropietario")) = True, 0, lector.Item("IdPropietario")))
                    obj.IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                    obj.Mon_Simbolo = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                    obj.cprov_CargoMax = CDec(IIf(IsDBNull(lector.Item("cprov_CargoMax")) = True, 0, lector.Item("cprov_CargoMax")))
                    obj.cprov_Saldo = CDec(IIf(IsDBNull(lector.Item("cprov_Saldo")) = True, 0, lector.Item("cprov_Saldo")))
                    obj.cprov_FechaAlta = CStr(IIf(IsDBNull(lector.Item("cprov_FechaAlta")) = True, Nothing, lector.Item("cprov_FechaAlta")))
                    obj.cpro_Estado = CStr(IIf(IsDBNull(lector.Item("cpro_Estado")) = True, "", lector.Item("cpro_Estado")))
                    lista.Add(obj)
                Loop
                cn.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectGrillaCxPResumenDocumento(ByVal idProveedor As Integer, ByVal idmoneda As Integer) As List(Of Entidades.CxPResumenView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("[_CR_CuentasxPagarResumenDocumentoxMoneda]", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idmoneda", idmoneda)
        'cmd.Parameters.AddWithValue("@idProveedor", idProveedor)
        cmd.Parameters.AddWithValue("@idProveedor", idProveedor)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CxPResumenView)
                Do While lector.Read
                    Dim CxCD As New Entidades.CxPResumenView
                    CxCD.idDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                    CxCD.tdoc_NombreCorto = CStr(IIf(IsDBNull(lector.Item("Documento")) = True, "", lector.Item("Documento")))
                    CxCD.doc_FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                    CxCD.doc_FechaVencimiento = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                    CxCD.nroDias = CInt(IIf(IsDBNull(lector.Item("diasVencidos")) = True, 0, lector.Item("diasVencidos")))
                    CxCD.mon_simbolo = CStr(lector.Item("moneda"))
                    CxCD.doc_TotalPagar = CDec(IIf(IsDBNull(lector.Item("mcp_Monto")) = True, Nothing, lector.Item("mcp_Monto")))
                    CxCD.abono = CDec(IIf(IsDBNull(lector.Item("TotalAbono")) = True, 0, lector.Item("TotalAbono")))
                    CxCD.Saldo = CDec(IIf(IsDBNull(lector.Item("mcp_Saldo")) = True, 0, lector.Item("mcp_Saldo")))
                    CxCD.Numero = CStr(IIf(IsDBNull(lector.Item("numero")) = True, "", lector.Item("numero")))
                    Lista.Add(CxCD)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

#End Region

#Region "FX_Actuales"
    'Public Function InsertaCuentaPersona(ByVal cuentapersona As Entidades.CuentaPersona) As Boolean
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
    '    ArrayParametros(0) = New SqlParameter("@cp_FechaAlta", SqlDbType.DateTime)
    '    ArrayParametros(0).Value = cuentapersona.FechaAlta
    '    ArrayParametros(1) = New SqlParameter("@cp_CargoMaximo", SqlDbType.Decimal)
    '    ArrayParametros(1).Value = cuentapersona.CargoMaximo
    '    ArrayParametros(2) = New SqlParameter("@IdPersona", SqlDbType.Int)
    '    ArrayParametros(2).Value = cuentapersona.IdPersona
    '    ArrayParametros(3) = New SqlParameter("@IdMoneda", SqlDbType.Int)
    '    ArrayParametros(3).Value = cuentapersona.IdMoneda
    '    ArrayParametros(4) = New SqlParameter("@cp_Estado", SqlDbType.Char)
    '    ArrayParametros(4).Value = cuentapersona.Estado
    '    Return HDAO.Insert(cn, "_CuentaPersonaInsert", ArrayParametros)
    'End Function
    'Public Function ActualizaCuentaPersona(ByVal cuentapersona As Entidades.CuentaPersona) As Boolean
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
    '    ArrayParametros(0) = New SqlParameter("@cp_FechaAlta", SqlDbType.DateTime)
    '    ArrayParametros(0).Value = cuentapersona.FechaAlta
    '    ArrayParametros(1) = New SqlParameter("@cp_CargoMaximo", SqlDbType.Decimal)
    '    ArrayParametros(1).Value = cuentapersona.CargoMaximo
    '    'ArrayParametros(2) = New SqlParameter("@cp_Saldo", SqlDbType.Decimal)
    '    'ArrayParametros(2).Value = cuentapersona.Saldo
    '    ArrayParametros(2) = New SqlParameter("@IdPersona", SqlDbType.Int)
    '    ArrayParametros(2).Value = cuentapersona.IdPersona
    '    ArrayParametros(3) = New SqlParameter("@IdMoneda", SqlDbType.Int)
    '    ArrayParametros(3).Value = cuentapersona.IdMoneda
    '    ArrayParametros(4) = New SqlParameter("@cp_Estado", SqlDbType.Char)
    '    ArrayParametros(4).Value = cuentapersona.Estado
    '    Return HDAO.Update(cn, "_CuentaPersonaUpdate", ArrayParametros)
    'End Function
    Public Function SelectGrillaCxCResumenDocumento(ByVal idpersona As Integer, ByVal idmoneda As Integer) As List(Of Entidades.CxCResumenView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("[_CR_CuentasxCobrarResumenDocumentoxMoneda]", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idmoneda", idmoneda)
        cmd.Parameters.AddWithValue("@idPersona", idpersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CxCResumenView)
                Do While lector.Read
                    Dim CxCD As New Entidades.CxCResumenView
                    CxCD.idDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                    CxCD.tdoc_NombreCorto = CStr(IIf(IsDBNull(lector.Item("Documento")) = True, "", lector.Item("Documento")))
                    CxCD.doc_FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                    CxCD.doc_FechaVencimiento = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                    CxCD.nroDias = CInt(IIf(IsDBNull(lector.Item("diasVencidos")) = True, 0, lector.Item("diasVencidos")))
                    CxCD.mon_simbolo = CStr(lector.Item("moneda"))
                    CxCD.doc_TotalPagar = CDec(IIf(IsDBNull(lector.Item("mcu_Monto")) = True, Nothing, lector.Item("mcu_Monto")))
                    CxCD.abono = CDec(IIf(IsDBNull(lector.Item("TotalAbono")) = True, 0, lector.Item("TotalAbono")))
                    CxCD.Saldo = CDec(IIf(IsDBNull(lector.Item("mcu_Saldo")) = True, 0, lector.Item("mcu_Saldo")))
                    CxCD.Numero = CStr(IIf(IsDBNull(lector.Item("numero")) = True, "", lector.Item("numero")))
                    Lista.Add(CxCD)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function DocAsociadosCtaProvxPag(ByVal idCtaProv As Integer) As List(Of Entidades.CxCResumenView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("[_CR_CuentasxPagarCtaIdproveedor]", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idCuentaProv", idCtaProv)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CxCResumenView)
                Do While lector.Read
                    Dim CxCD As New Entidades.CxCResumenView
                    CxCD.idDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                    CxCD.tdoc_NombreCorto = CStr(IIf(IsDBNull(lector.Item("Documento")) = True, "", lector.Item("Documento")))
                    CxCD.doc_FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                    CxCD.doc_FechaVencimiento = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                    CxCD.nroDias = CInt(IIf(IsDBNull(lector.Item("diasVencidos")) = True, 0, lector.Item("diasVencidos")))
                    CxCD.mon_simbolo = CStr(lector.Item("moneda"))
                    CxCD.doc_TotalPagar = CDec(IIf(IsDBNull(lector.Item("mcp_Monto")) = True, Nothing, lector.Item("mcp_Monto")))
                    CxCD.abono = CDec(IIf(IsDBNull(lector.Item("TotalAbono")) = True, 0, lector.Item("TotalAbono")))
                    CxCD.Saldo = CDec(IIf(IsDBNull(lector.Item("mcp_Saldo")) = True, 0, lector.Item("mcp_Saldo")))
                    CxCD.Numero = CStr(IIf(IsDBNull(lector.Item("numero")) = True, "", lector.Item("numero")))
                    Lista.Add(CxCD)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAllConsultaxIdProveedor(ByVal IdProveedor As Integer) As List(Of Entidades.CuentaProveedor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CuentaProveedorSelectAllConsultaxIdProveedor", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProveedor", IdProveedor)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CuentaProveedor)
                With lector
                    Do While .Read
                        Dim obj As New Entidades.CuentaProveedor
                        obj.IdCuentaProv = CInt(IIf(IsDBNull(.Item("IdCuentaProv")) = True, 0, .Item("IdCuentaProv")))
                        obj.IdMoneda = CInt(IIf(IsDBNull(.Item("IdMoneda")) = True, 0, .Item("IdMoneda")))
                        obj.MonedaSimbolo = CStr(IIf(IsDBNull(.Item("mon_Simbolo")) = True, "", .Item("mon_Simbolo")))
                        obj.cprov_CargoMax = CDec(IIf(IsDBNull(.Item("cprov_CargoMax")) = True, 0, .Item("cprov_CargoMax")))
                        obj.cprov_Saldo = CDec(IIf(IsDBNull(.Item("cprov_Saldo")) = True, 0, .Item("cprov_Saldo")))
                        obj.IdPropietario = CInt(IIf(IsDBNull(.Item("IdPropietario")) = True, 0, .Item("IdPropietario")))
                        obj.IdProveedor = CInt(IIf(IsDBNull(.Item("IdProveedor")) = True, 0, .Item("IdProveedor")))
                        obj.cprov_FechaAlta = CStr(IIf(IsDBNull(.Item("cprov_FechaAlta")) = True, Nothing, .Item("cprov_FechaAlta")))
                        obj.ObservProv = CStr(IIf(IsDBNull(.Item("cprov_Obs")) = True, Nothing, .Item("cprov_Obs")))
                        obj.cpro_Estado = CStr(IIf(IsDBNull(.Item("cpro_estado")) = True, "0", .Item("cpro_estado")))

                        Lista.Add(obj)
                    Loop
                    .Close()
                End With
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllConsultaxHistCtaProv(ByVal IdCtaProveedor As Integer) As List(Of Entidades.CuentaProveedor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CuentaProveedorSelectAllConsultaxIdCtaCuentaProveedorHis", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdCtaProveedor", IdCtaProveedor)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CuentaProveedor)
                With lector
                    Do While .Read
                        Dim obj As New Entidades.CuentaProveedor
                        obj.IdCuentaProv = CInt(IIf(IsDBNull(.Item("IdCuentaProv")) = True, 0, .Item("IdCuentaProv")))
                        obj.IdMoneda = CInt(IIf(IsDBNull(.Item("IdMoneda")) = True, 0, .Item("IdMoneda")))
                        obj.MonedaSimbolo = CStr(IIf(IsDBNull(.Item("mon_Simbolo")) = True, "", .Item("mon_Simbolo")))
                        obj.cprov_CargoMax = CDec(IIf(IsDBNull(.Item("cprov_CargoMax")) = True, 0, .Item("cprov_CargoMax")))
                        obj.cprov_Saldo = CDec(IIf(IsDBNull(.Item("cprov_Saldo")) = True, 0, .Item("cprov_Saldo")))
                        obj.IdPropietario = CInt(IIf(IsDBNull(.Item("IdPropietario")) = True, 0, .Item("IdPropietario")))
                        obj.IdProveedor = CInt(IIf(IsDBNull(.Item("IdProveedor")) = True, 0, .Item("IdProveedor")))
                        obj.cprov_FechaAlta = CStr(IIf(IsDBNull(.Item("cprov_FechaAlta")) = True, Nothing, .Item("cprov_FechaAlta")))
                        obj.ObservProv = CStr(IIf(IsDBNull(.Item("cprov_Obs")) = True, Nothing, .Item("cprov_Obs")))
                        obj.cpro_Estado = CStr(IIf(IsDBNull(.Item("cpro_estado")) = True, "0", .Item("cpro_estado")))

                        Lista.Add(obj)
                    Loop
                    .Close()
                End With
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function SelectxIdProveedor(ByVal IdProveedor As Integer) As List(Of Entidades.CuentaProveedor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CuentaPersonaSelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idproveedor", IdProveedor)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CuentaProveedor)
                With lector
                    Do While .Read
                        Dim obj As New Entidades.CuentaProveedor
                        obj.IdCuentaProv = CInt(IIf(IsDBNull(.Item("IdCuentaProv")) = True, 0, .Item("IdCuentaProv")))
                        obj.IdMoneda = CInt(IIf(IsDBNull(.Item("IdMoneda")) = True, 0, .Item("IdMoneda")))
                        obj.MonedaSimbolo = CStr(IIf(IsDBNull(.Item("mon_Simbolo")) = True, "", .Item("mon_Simbolo")))
                        obj.cprov_CargoMax = CDec(IIf(IsDBNull(.Item("cprov_CargoMax")) = True, 0, .Item("cprov_CargoMax")))
                        obj.cprov_Saldo = CDec(IIf(IsDBNull(.Item("cprov_Saldo")) = True, 0, .Item("cprov_Saldo")))
                        obj.IdPropietario = CInt(IIf(IsDBNull(.Item("IdPropietario")) = True, 0, .Item("IdPropietario")))
                        obj.IdProveedor = CInt(IIf(IsDBNull(.Item("IdProveedor")) = True, 0, .Item("IdProveedor")))
                        obj.cprov_FechaAlta = CStr(IIf(IsDBNull(.Item("cprov_FechaAlta")) = True, Nothing, .Item("cprov_FechaAlta")))

                        Lista.Add(obj)
                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Sub InsertaCuentaProveedorT(ByVal obj As Entidades.CuentaProveedor, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim par() As SqlParameter = New SqlParameter(5) {}

        par(0) = New SqlParameter("@IdMoneda", System.Data.SqlDbType.Int)
        par(0).Value = IIf(obj.IdMoneda = 0, DBNull.Value, obj.IdMoneda)
        par(1) = New SqlParameter("@cp_Estado", System.Data.SqlDbType.NVarChar, 2)
        par(1).Value = obj.Estado
        par(2) = New SqlParameter("@cp_CargoMaximo", System.Data.SqlDbType.Decimal)
        par(2).Value = obj.cprov_CargoMax

        par(3) = New SqlParameter("@IdProveedor", System.Data.SqlDbType.Int)
        par(3).Value = IIf(obj.IdProveedor = 0, DBNull.Value, obj.IdProveedor)
        par(4) = New SqlParameter("@idpropietario", System.Data.SqlDbType.Int)
        par(4).Value = obj.IdPropietario
        par(5) = New SqlParameter("@cp_Obs", System.Data.SqlDbType.VarChar)
        par(5).Value = obj.ObservProv


        HDAO.InsertaT(cn, "[_CuentaProveedorInserta]", par, tr)

    End Sub

    Public Sub UpdateCuentaProveedorT(ByVal obj As Entidades.CuentaProveedor, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim par() As SqlParameter = New SqlParameter(7) {}

        par(0) = New SqlParameter("@IdMoneda", System.Data.SqlDbType.Int)
        par(0).Value = IIf(obj.IdMoneda = 0, DBNull.Value, obj.IdMoneda)
        par(1) = New SqlParameter("@cp_Estado", System.Data.SqlDbType.NVarChar, 2)
        par(1).Value = obj.Estado
        par(2) = New SqlParameter("@cp_CargoMaximo", System.Data.SqlDbType.Decimal)
        par(2).Value = obj.cprov_CargoMax

        par(3) = New SqlParameter("@IdProveedor", System.Data.SqlDbType.Int)
        par(3).Value = IIf(obj.IdProveedor = 0, DBNull.Value, obj.IdProveedor)

        par(4) = New SqlParameter("@idpropietario", System.Data.SqlDbType.Int)
        par(4).Value = obj.IdPropietario
        par(5) = New SqlParameter("@cp_Obs", System.Data.SqlDbType.VarChar)
        par(5).Value = obj.ObservProv
        par(6) = New SqlParameter("@idCuentaProv", System.Data.SqlDbType.Int)
        par(6).Value = IIf(obj.IdCuentaProv = 0, DBNull.Value, obj.IdCuentaProv)
        par(7) = New SqlParameter("@cprov_Saldo", System.Data.SqlDbType.Decimal)
        par(7).Value = obj.cprov_Saldo

        HDAO.UpdateT(cn, "_CuentaProveedorUpdate2", par, tr)

    End Sub

    Public Sub DeletexIdCtaProveedor(ByVal idctaProveedor As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Dim cmd As New SqlCommand("[_CuentaProveedorDeletexIdCtaProveedor]", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idctaProveedor", idctaProveedor)
        If cmd.ExecuteNonQuery = 0 Then
            Throw New Exception
        End If
    End Sub



    Public Sub DeletexCuentaProveedorxIdProveedor(ByVal idproveedor As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Dim cmd As New SqlCommand("_CuentaProveedorDeletexIdProveedor", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProveedor", idproveedor)
        If cmd.ExecuteNonQuery = 0 Then
            Throw New Exception
        End If

    End Sub
    Public Sub DeletexIdCuentaProveedor(ByVal idcuentaProveedor As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Dim cmd As New SqlCommand("_CuentaProveedorDeletexIdCuentaProveedor", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdCuentaProveedor", idcuentaProveedor)
        If cmd.ExecuteNonQuery = 0 Then
            Throw New Exception
        End If
    End Sub
    '********************************************************
    'Método Insertar, para Insertar Saldo Pendiente
    'Autor   : Hans Fernando, Quispe Espinoza
    'Módulo  : Caja-Documento
    'Sistema : Indusfer
    'Empresa : Digrafic SRL
    'Fecha   : 5-Set-2009
    'Parametros : poBEMovCuenta
    'Retorna : IdMovCuenta
    '********************************************************
    Public Function fnInsTblCuentaPersona(ByVal obj As Entidades.CuentaPersona) As Integer

        Dim resul As Integer
        'conexion
        Dim loTx As SqlTransaction
        Dim loCn As New SqlConnection()
        loCn.ConnectionString = objConexion.ConexionSIGE.ConnectionString.ToString()

        'parametros
        Dim par() As SqlParameter = New SqlParameter(7) {}

        par(0) = New SqlParameter("@IdMoneda", System.Data.SqlDbType.Int)
        par(0).Value = obj.IdMoneda
        par(1) = New SqlParameter("@cp_Estado", System.Data.SqlDbType.NVarChar, 2)
        par(1).Value = obj.Estado
        par(2) = New SqlParameter("@cp_CargoMaximo", System.Data.SqlDbType.Decimal)
        par(2).Value = obj.CargoMaximo
        par(3) = New SqlParameter("@IdTienda", System.Data.SqlDbType.Int)
        par(3).Value = obj.IdTienda
        par(4) = New SqlParameter("@IdEmpresa", System.Data.SqlDbType.Int)
        par(4).Value = obj.IdEmpresa
        par(5) = New SqlParameter("@cp_CuentaFormal", System.Data.SqlDbType.Bit)
        par(5).Value = obj.CuentaFormal
        par(6) = New SqlParameter("@IdSupervisor", System.Data.SqlDbType.Int)
        par(6).Value = obj.IdSupervisor
        par(7) = New SqlParameter("@IdPersona", System.Data.SqlDbType.Int)
        par(7).Value = obj.IdPersona

        loCn.Open()
        loTx = loCn.BeginTransaction()
        Try
            resul = Convert.ToInt32(SqlHelper.ExecuteScalar(loTx, CommandType.StoredProcedure, "_CuentaPersonaInsert", par))
            loTx.Commit()
        Catch e As Exception
            loTx.Rollback()
            Throw (e)
        Finally
            loCn.Close()
            loCn.Dispose()
        End Try
        Return (resul)
    End Function
    '********************************************************
    'Método Insertar, para Insertar Saldo Pendiente
    'Autor   : Hans Fernando, Quispe Espinoza
    'Módulo  : Caja-Documento
    'Sistema : Indusfer
    'Empresa : Digrafic SRL
    'Fecha   : 5-Set-2009
    'Parametros : poBEMovCuenta
    'Retorna : IdMovCuenta
    '********************************************************
    Public Function fnUdpTblCuentaPersona(ByVal obj As Entidades.CuentaPersona) As Integer

        Dim resul As Integer
        'conexion
        Dim loTx As SqlTransaction
        Dim loCn As New SqlConnection()
        loCn.ConnectionString = objConexion.ConexionSIGE.ConnectionString.ToString()

        'parametros
        Dim par() As SqlParameter = New SqlParameter(9) {}

        par(0) = New SqlParameter("@IdMoneda", System.Data.SqlDbType.Int)
        par(0).Value = obj.IdMoneda
        par(1) = New SqlParameter("@cp_Estado", System.Data.SqlDbType.NVarChar, 2)
        par(1).Value = obj.Estado
        par(2) = New SqlParameter("@cp_CargoMaximo", System.Data.SqlDbType.Decimal)
        par(2).Value = obj.CargoMaximo
        par(3) = New SqlParameter("@IdTienda", System.Data.SqlDbType.Int)
        par(3).Value = obj.IdTienda
        par(4) = New SqlParameter("@IdEmpresa", System.Data.SqlDbType.Int)
        par(4).Value = obj.IdEmpresa
        par(5) = New SqlParameter("@cp_CuentaFormal", System.Data.SqlDbType.Bit)
        par(5).Value = obj.CuentaFormal
        par(6) = New SqlParameter("@IdSupervisor", System.Data.SqlDbType.Int)
        par(6).Value = obj.IdSupervisor
        par(7) = New SqlParameter("@IdPersona", System.Data.SqlDbType.Int)
        par(7).Value = obj.IdPersona
        par(8) = New SqlParameter("@cp_Saldo", System.Data.SqlDbType.Decimal)
        par(8).Value = obj.Saldo
        par(9) = New SqlParameter("@IdCuentaPersona", System.Data.SqlDbType.Int)
        par(9).Value = obj.IdCuentaPersona

        loCn.Open()
        loTx = loCn.BeginTransaction()
        Try
            resul = Convert.ToInt32(SqlHelper.ExecuteScalar(loTx, CommandType.StoredProcedure, "_CuentaPersonaUpdate", par))
            loTx.Commit()
        Catch e As Exception
            loTx.Rollback()
            Throw (e)
        Finally
            loCn.Close()
            loCn.Dispose()
        End Try
        Return (resul)
    End Function

    Public Function SelectxParams(ByVal IdPersona As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer) As List(Of Entidades.CuentaPersona)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CuentaPersonaSelectxParams", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CuentaPersona)
                With lector
                    Do While .Read
                        Dim obj As New Entidades.CuentaPersona
                        obj.IdCuentaPersona = CInt(IIf(IsDBNull(.Item("IdCuentaPersona")) = True, 0, .Item("IdCuentaPersona")))
                        obj.IdMoneda = CInt(IIf(IsDBNull(.Item("IdMoneda")) = True, 0, .Item("IdMoneda")))
                        obj.IdPersona = CInt(IIf(IsDBNull(.Item("IdPersona")) = True, 0, .Item("IdPersona")))
                        obj.MonedaSimbolo = CStr(IIf(IsDBNull(.Item("mon_Simbolo")) = True, "", .Item("mon_Simbolo")))
                        obj.CargoMaximo = CDec(IIf(IsDBNull(.Item("cp_CargoMaximo")) = True, 0, .Item("cp_CargoMaximo")))
                        obj.Saldo = CDec(IIf(IsDBNull(.Item("cp_Saldo")) = True, 0, .Item("cp_Saldo")))

                        obj.IdEmpresa = CInt(IIf(IsDBNull(.Item("IdEmpresa")) = True, 0, .Item("IdEmpresa")))
                        obj.IdTienda = CInt(IIf(IsDBNull(.Item("IdTienda")) = True, 0, .Item("IdTienda")))

                        obj.CuentaFormal = CBool(IIf(IsDBNull(.Item("cp_CuentaFormal")) = True, 0, .Item("cp_CuentaFormal")))
                        obj.IdSupervisor = CInt(IIf(IsDBNull(.Item("IdSupervisor")) = True, 0, .Item("IdSupervisor")))
                        obj.FechaAlta = CStr(IIf(IsDBNull(.Item("cp_FechaAlta")) = True, Nothing, .Item("cp_FechaAlta")))

                        Lista.Add(obj)
                    Loop
                    .Close()
                End With
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

#End Region



End Class
