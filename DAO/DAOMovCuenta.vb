'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.






'*********************   MIERCOLES 10 MARZO 2010 HORA 02_19 PM













'********************************************************
'Autor   : Caro Freyre, verly
'M�dulo  : Caja-Documento
'Sistema : Indusfer
'Empresa : Digrafic SRL
'Fecha   : 01-Oct-2009
'********************************************************
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOMovCuenta
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

    Public Sub UpdateSaldoxIdMovCuenta(ByVal IdMovCuenta As Integer, ByVal SaldoNew As Decimal, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(1) {}

        p(0) = New SqlParameter("@IdMovCuenta", SqlDbType.Int)
        p(0).Value = IdMovCuenta

        p(1) = New SqlParameter("@SaldoNew", SqlDbType.Decimal)
        p(1).Value = SaldoNew

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_MovCuentaUpdateSaldoxIdMovCuenta", p)

    End Sub







    Public Sub InsertaMovCuenta(ByVal cn As SqlConnection, ByVal movcuenta As Entidades.MovCuenta, ByVal T As SqlTransaction)
        'Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(8) {}
        ArrayParametros(0) = New SqlParameter("@IdCuentaPersona", SqlDbType.Int)
        ArrayParametros(0).Value = movcuenta.IdCuentaPersona
        ArrayParametros(1) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(1).Value = movcuenta.IdPersona
        ArrayParametros(2) = New SqlParameter("@mcu_Monto", SqlDbType.Decimal)
        ArrayParametros(2).Value = movcuenta.Monto
        ArrayParametros(3) = New SqlParameter("@mcu_Factor", SqlDbType.Int)
        ArrayParametros(3).Value = movcuenta.Factor
        ArrayParametros(4) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(4).Value = movcuenta.IdDocumento
        ArrayParametros(5) = New SqlParameter("@IdDetalleRecibo", SqlDbType.Int)
        ArrayParametros(5).Value = IIf(movcuenta.IdDetalleRecibo = Nothing, DBNull.Value, movcuenta.IdDetalleRecibo)
        ArrayParametros(6) = New SqlParameter("@IdMovCuentaTipo", SqlDbType.Int)
        ArrayParametros(6).Value = movcuenta.IdMovCuentaTipo
        ArrayParametros(7) = New SqlParameter("@mcu_Saldo", SqlDbType.Decimal)
        ArrayParametros(7).Value = movcuenta.Saldo
        ArrayParametros(8) = New SqlParameter("@IdCargoCuenta", SqlDbType.Int)
        ArrayParametros(8).Value = IIf(movcuenta.IdCargoCuenta = Nothing, DBNull.Value, movcuenta.IdCargoCuenta)

        HDAO.InsertaT(cn, "_MovCuentaInsert", ArrayParametros, T)
    End Sub

    Public Function SelectDeudaxIdPersona(ByVal idPersona As Integer) As List(Of Entidades.MovCuenta)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MovCuentaSelectDeudaxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", idPersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MovCuenta)
                Do While lector.Read
                    Dim obj As New Entidades.MovCuenta
                    With obj
                        .IdMovCuenta = CInt(lector.Item("IdMovCuenta"))
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .Saldo = CDec(IIf(IsDBNull(lector.Item("mcu_Saldo")) = True, 0, lector.Item("mcu_Saldo")))
                        .IdMovCuentaTipo = CInt(IIf(IsDBNull(lector.Item("IdMovCuentaTipo")) = True, 0, lector.Item("IdMovCuentaTipo")))
                        .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                        .IdCuentaPersona = CInt(IIf(IsDBNull(lector.Item("IdCuentaPersona")) = True, 0, lector.Item("IdCuentaPersona")))
                        .Monto = CDec(IIf(IsDBNull(lector.Item("mcu_Monto")) = True, 0, lector.Item("mcu_Monto")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("doc_Codigo")) = True, "", lector.Item("doc_Codigo")))
                        .Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                        .IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                        .MonedaSimbolo = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                        .Fecha = CDate(IIf(IsDBNull(lector.Item("mcu_Fecha")) = True, Nothing, lector.Item("mcu_Fecha")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    '********************************************************
    'M�todo Din, para obtener Cuenta por cualquier campo
    'Autor   : Hans Fernando, Quispe Espinoza
    'M�dulo  : Caja-Documento
    'Sistema : Indusfer
    'Empresa : Digrafic SRL
    'Fecha   : 3-Set-2009
    'Parametros : psWhere, psOrder
    'Retorna : Lista de tipo MovCuenta
    '********************************************************
    Public Function fnSelTblMovCuentaDin(ByVal psWhere As String, ByVal psOrder As String) As List(Of Entidades.MovCuenta)
        Dim loList As New List(Of Entidades.MovCuenta)()
        Dim par As SqlParameter() = New SqlParameter(1) {}
        par(0) = New SqlParameter("@WhereCondition", System.Data.SqlDbType.VarChar, 500)
        par(0).Value = psWhere
        par(1) = New SqlParameter("@OrderByExpression", System.Data.SqlDbType.VarChar, 250)
        par(1).Value = psOrder
        Try
            Dim loDr As SqlDataReader = SqlHelper.ExecuteReader(objConexion.ConexionSIGE, CommandType.StoredProcedure, "usp_SelMovCuentaDin", par)
            While (loDr.Read())
                Dim loMovCuenta As New Entidades.MovCuenta
                If loDr.GetValue(loDr.GetOrdinal("mcu_Factor")).Equals(DBNull.Value) Then
                    loMovCuenta.Factor = 0
                Else
                    loMovCuenta.Factor = DirectCast(loDr.GetValue(loDr.GetOrdinal("mcu_Factor")), [Int32])
                End If
                If loDr.GetValue(loDr.GetOrdinal("mcu_Fecha")).Equals(DBNull.Value) Then
                    loMovCuenta.Fecha = Convert.ToDateTime(DateTime.Now)
                Else
                    loMovCuenta.Fecha = DirectCast(loDr.GetValue(loDr.GetOrdinal("mcu_Fecha")), [DateTime])
                End If

                If loDr.GetValue(loDr.GetOrdinal("IdCargoCuenta")).Equals(DBNull.Value) Then
                    loMovCuenta.IdCargoCuenta = 0
                Else
                    loMovCuenta.IdCargoCuenta = DirectCast(loDr.GetValue(loDr.GetOrdinal("IdCargoCuenta")), [Int32])
                End If
                If loDr.GetValue(loDr.GetOrdinal("IdCuentaPersona")).Equals(DBNull.Value) Then
                    loMovCuenta.IdCuentaPersona = 0
                Else
                    loMovCuenta.IdCuentaPersona = DirectCast(loDr.GetValue(loDr.GetOrdinal("IdCuentaPersona")), [Int32])
                End If


                If loDr.GetValue(loDr.GetOrdinal("IdDetalleRecibo")).Equals(DBNull.Value) Then
                    loMovCuenta.IdDetalleRecibo = 0
                Else
                    loMovCuenta.IdDetalleRecibo = DirectCast(loDr.GetValue(loDr.GetOrdinal("IdDetalleRecibo")), [Int32])
                End If
                loMovCuenta.IdDocumento = DirectCast(loDr.GetValue(loDr.GetOrdinal("IdDocumento")), [Int32])
                loMovCuenta.IdMovCuenta = DirectCast(loDr.GetValue(loDr.GetOrdinal("IdMovCuenta")), [Int32])
                loMovCuenta.IdMovCuentaTipo = DirectCast(loDr.GetValue(loDr.GetOrdinal("IdMovCuentaTipo")), [Int32])
                loMovCuenta.IdPersona = DirectCast(loDr.GetValue(loDr.GetOrdinal("IdPersona")), [Int32])
                loMovCuenta.Monto = DirectCast(loDr.GetValue(loDr.GetOrdinal("mcu_Monto")), [Decimal])
                loMovCuenta.Saldo = DirectCast(loDr.GetValue(loDr.GetOrdinal("mcu_Saldo")), [Decimal])
                loList.Add(loMovCuenta)
            End While
            loDr.Close()
        Catch e As Exception
            Throw e
        Finally
        End Try
        Return loList
    End Function
    '********************************************************
    'M�todo Insertar, para Insertar Saldo Pendiente
    'Autor   : Hans Fernando, Quispe Espinoza
    'M�dulo  : Caja-Documento
    'Sistema : Indusfer
    'Empresa : Digrafic SRL
    'Fecha   : 5-Set-2009
    'Parametros : poBEMovCuenta
    'Retorna : IdMovCuenta
    '********************************************************
    Public Function fnInsTblMovCuenta(ByVal poBEMovCuenta As Entidades.MovCuenta) As Integer

        Dim resul As Integer
        'conexion
        Dim loTx As SqlTransaction
        Dim loCn As New SqlConnection()
        loCn.ConnectionString = objConexion.ConexionSIGE.ConnectionString.ToString()

        'parametros
        Dim par() As SqlParameter = New SqlParameter(9) {}
        par(0) = New SqlParameter("@IdCuentaPersona", System.Data.SqlDbType.Int)
        par(0).Value = poBEMovCuenta.IdCuentaPersona
        par(1) = New SqlParameter("@IdPersona", System.Data.SqlDbType.Int)
        par(1).Value = poBEMovCuenta.IdPersona
        par(2) = New SqlParameter("@mcu_Monto", System.Data.SqlDbType.Decimal)
        par(2).Value = poBEMovCuenta.Monto
        par(3) = New SqlParameter("@mcu_Factor", System.Data.SqlDbType.Int)
        par(3).Value = poBEMovCuenta.Factor
        par(4) = New SqlParameter("@IdDocumento", System.Data.SqlDbType.Int)
        par(4).Value = poBEMovCuenta.IdDocumento
        par(5) = New SqlParameter("@IdDetalleRecibo", System.Data.SqlDbType.Int)
        par(5).Value = poBEMovCuenta.IdDetalleRecibo
        par(6) = New SqlParameter("@IdMovCuentaTipo", System.Data.SqlDbType.Int)
        par(6).Value = poBEMovCuenta.IdMovCuentaTipo
        par(7) = New SqlParameter("@mcu_Saldo", System.Data.SqlDbType.Decimal)
        par(7).Value = poBEMovCuenta.Saldo
        par(8) = New SqlParameter("@IdCargoCuenta", System.Data.SqlDbType.Int)
        par(8).Value = IIf(poBEMovCuenta.IdCargoCuenta = Nothing, DBNull.Value, poBEMovCuenta.IdCargoCuenta)
        par(9) = New SqlParameter("@IdMovCuenta", System.Data.SqlDbType.Int)
        par(9).Direction = ParameterDirection.Output
        loCn.Open()
        loTx = loCn.BeginTransaction()
        Try
            resul = Convert.ToInt32(SqlHelper.ExecuteScalar(loTx, CommandType.StoredProcedure, "usp_InsertarMovCuenta", par))
            loTx.Commit()
        Catch e As Exception
            loTx.Rollback()
            Throw (e)
        Finally
            loCn.Close()
            loCn.Dispose()
        End Try
        Return (resul)
    End Function
    '********************************************************
    'M�todo Update, para actualizar Saldo Pendiente
    'Autor   : Hans Fernando, Quispe Espinoza
    'M�dulo  : Caja-Documento
    'Sistema : Indusfer
    'Empresa : Digrafic SRL
    'Fecha   : 5-Set-2009
    'Parametros : poBEMovCuenta
    'Retorna : NULL
    '********************************************************
    Public Function fnUdpTblMovCuenta(ByVal poBEMovCuenta As Entidades.MovCuenta) As Integer

        Dim resul As Integer
        'conexion
        Dim loTx As SqlTransaction
        Dim loCn As New SqlConnection()
        loCn.ConnectionString = objConexion.ConexionSIGE.ConnectionString.ToString()

        'parametros
        Dim par() As SqlParameter = New SqlParameter(1) {}
        par(0) = New SqlParameter("@Amortizacion", System.Data.SqlDbType.Decimal)
        par(0).Value = poBEMovCuenta.Saldo
        par(1) = New SqlParameter("@IdMovCuenta", System.Data.SqlDbType.Int)
        par(1).Value = poBEMovCuenta.IdMovCuenta
        loCn.Open()
        loTx = loCn.BeginTransaction()
        Try
            resul = Convert.ToInt32(SqlHelper.ExecuteNonQuery(loTx, CommandType.StoredProcedure, "usp_ActualizarCancelarFactura", par))
            loTx.Commit()
        Catch e As Exception
            loTx.Rollback()
            Throw (e)
        Finally
            loCn.Close()
            loCn.Dispose()
        End Try
        Return (resul)
    End Function

    Public Sub DeleteMovCuenta_PagoProgramadoxIdDocumento(ByVal IdDocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As New SqlCommand("_MovCuenta_PagoProgramado_DeletexIdDocumento", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

        cmd.ExecuteNonQuery()


    End Sub


    Public Function SelectDeudaxIdPersona(ByVal IdPersona As Integer, ByVal IdMonedaDestino As Integer) As List(Of Entidades.Documento_MovCuenta)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PersonaSelectDeudaxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdMonedaDestino", IdMonedaDestino)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Documento_MovCuenta)
                Do While lector.Read
                    Dim obj As New Entidades.Documento_MovCuenta
                    With obj

                        .IdMovCuenta = CInt(IIf(IsDBNull(lector("IdMovCuenta")) = True, 0, lector("IdMovCuenta")))
                        .Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                        .MontoTotal = CDec(IIf(IsDBNull(lector("MontoTotal")) = True, 0, lector("MontoTotal")))
                        .Saldo = CDec(IIf(IsDBNull(lector("Saldo")) = True, 0, lector("Saldo")))
                        .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                        .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))
                        .FechaVenc = CDate(IIf(IsDBNull(lector("doc_FechaVenc")) = True, Nothing, lector("doc_FechaVenc")))
                        .NroDiasMora = CInt(IIf(IsDBNull(lector("NroDiasMora")) = True, 0, lector("NroDiasMora")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector("MonedaDestino")) = True, "", lector("MonedaDestino")))

                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function SelectDeudaxIdPersona_NoLetras(ByVal IdPersona As Integer, ByVal IdMonedaDestino As Integer) As List(Of Entidades.Documento_MovCuenta)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PersonaSelectDeudaxIdPersona_NoLetras", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdMonedaDestino", IdMonedaDestino)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Documento_MovCuenta)
                Do While lector.Read
                    Dim obj As New Entidades.Documento_MovCuenta
                    With obj

                        .IdMovCuenta = CInt(IIf(IsDBNull(lector("IdMovCuenta")) = True, 0, lector("IdMovCuenta")))
                        .Id = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                        .MontoTotal = CDec(IIf(IsDBNull(lector("MontoTotal")) = True, 0, lector("MontoTotal")))
                        .Saldo = CDec(IIf(IsDBNull(lector("Saldo")) = True, 0, lector("Saldo")))
                        .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                        .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))
                        .FechaVenc = CDate(IIf(IsDBNull(lector("doc_FechaVenc")) = True, Nothing, lector("doc_FechaVenc")))
                        .NroDiasMora = CInt(IIf(IsDBNull(lector("NroDiasMora")) = True, 0, lector("NroDiasMora")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector("MonedaDestino")) = True, "", lector("MonedaDestino")))

                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function SelectAbonosxIdMovCuenta(ByVal IdMovCuenta As Integer) As List(Of Entidades.Documento_MovCuenta)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MovCuentaSelectAbonosxIdMovCuenta", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdMovCuenta", IdMovCuenta)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Documento_MovCuenta)
                Do While lector.Read
                    Dim obj As New Entidades.Documento_MovCuenta
                    With obj

                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))
                        .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                        .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                        .FechaEmision = CDate(IIf(IsDBNull(lector("doc_FechaEmision")) = True, Nothing, lector("doc_FechaEmision")))
                        .NomMoneda = CStr(IIf(IsDBNull(lector("mon_Simbolo")) = True, "", lector("mon_Simbolo")))
                        .TotalAPagar = CDec(IIf(IsDBNull(lector("doc_TotalAPagar")) = True, 0, lector("doc_TotalAPagar")))

                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function





End Class
