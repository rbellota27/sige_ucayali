'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOEmpresaAreaCargo
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaEmpresaAreaCargo(ByVal empresaareacargo As Entidades.EmpresaAreaCargo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(0).Value = empresaareacargo.IdEmpresa
        ArrayParametros(1) = New SqlParameter("@IdArea", SqlDbType.Int)
        ArrayParametros(1).Value = empresaareacargo.IdArea
        ArrayParametros(2) = New SqlParameter("@IdCargo", SqlDbType.Int)
        ArrayParametros(2).Value = empresaareacargo.IdCargo
        Return HDAO.Insert(cn, "_EmpresaAreaCargoInsert", ArrayParametros)
    End Function
End Class
