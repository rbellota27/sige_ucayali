'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.


'**********************   VIERNES 25 06 2010

Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAOPuntoPartida
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Private objDaoMantenedor As New DAO.DAOMantenedor

    Public Sub Registrar(ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(3) {}
        p(0) = objDaoMantenedor.getParam(objPuntoPartida.IdAlmacen, "@IdAlmacen", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(objPuntoPartida.Ubigeo, "@pp_Ubigeo", SqlDbType.VarChar)
        p(2) = objDaoMantenedor.getParam(objPuntoPartida.Direccion, "@pp_Direccion", SqlDbType.VarChar)
        p(3) = objDaoMantenedor.getParam(objPuntoPartida.IdDocumento, "@IdDocumento", SqlDbType.Int)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_PuntoPartida_Registrar", p)

    End Sub

    Public Sub UpdatePuntoPartida(ByVal puntopartida As Entidades.PuntoPartida, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(puntopartida.IdAlmacen = Nothing, DBNull.Value, puntopartida.IdAlmacen)
        ArrayParametros(1) = New SqlParameter("@pp_Ubigeo", SqlDbType.Char)
        ArrayParametros(1).Value = IIf(puntopartida.Ubigeo = Nothing, DBNull.Value, puntopartida.Ubigeo)
        ArrayParametros(2) = New SqlParameter("@pp_Direccion", SqlDbType.VarChar)
        ArrayParametros(2).Value = IIf(puntopartida.Direccion = Nothing, DBNull.Value, puntopartida.Direccion)
        ArrayParametros(3) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(3).Value = puntopartida.IdDocumento
        ArrayParametros(4) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(4).Value = IIf(puntopartida.IdTienda = Nothing, DBNull.Value, puntopartida.IdTienda)

        Dim cmd As New SqlCommand("_updatePuntoPartida", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        cmd.ExecuteNonQuery()
    End Sub

    Public Sub InsertaPuntoPartida(ByVal puntopartida As Entidades.PuntoPartida, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(puntopartida.IdAlmacen = Nothing, DBNull.Value, puntopartida.IdAlmacen)
        ArrayParametros(1) = New SqlParameter("@pp_Ubigeo", SqlDbType.Char)
        ArrayParametros(1).Value = IIf(puntopartida.Ubigeo = Nothing, DBNull.Value, puntopartida.Ubigeo)
        ArrayParametros(2) = New SqlParameter("@pp_Direccion", SqlDbType.VarChar)
        ArrayParametros(2).Value = IIf(puntopartida.Direccion = Nothing, DBNull.Value, puntopartida.Direccion)
        ArrayParametros(3) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(3).Value = puntopartida.IdDocumento
        ArrayParametros(4) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(4).Value = IIf(puntopartida.IdTienda = Nothing, DBNull.Value, puntopartida.IdTienda)

        HDAO.InsertaT(cn, "_PuntoPartidaInsert", ArrayParametros, tr)
    End Sub
    Public Function ActualizaPuntoPartida(ByVal puntopartida As Entidades.PuntoPartida) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        ArrayParametros(0).Value = puntopartida.IdAlmacen
        ArrayParametros(1) = New SqlParameter("@pp_Ubigeo", SqlDbType.Char)
        ArrayParametros(1).Value = puntopartida.Ubigeo
        ArrayParametros(2) = New SqlParameter("@pp_Direccion", SqlDbType.VarChar)
        ArrayParametros(2).Value = puntopartida.Direccion
        ArrayParametros(3) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(3).Value = puntopartida.IdDocumento
        Return HDAO.Update(cn, "_PuntoPartidaUpdate", ArrayParametros)
    End Function



    Public Function SelectxIdDocumento(ByVal iddocumento As Integer) As Entidades.PuntoPartida
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PuntoPartidaSelectxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim objPuntoPartida As Entidades.PuntoPartida = Nothing
                If lector.Read Then

                    objPuntoPartida = New Entidades.PuntoPartida

                    With objPuntoPartida

                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .Direccion = CStr(IIf(IsDBNull(lector.Item("pp_Direccion")) = True, "", lector.Item("pp_Direccion")))
                        .Ubigeo = CStr(IIf(IsDBNull(lector.Item("pp_Ubigeo")) = True, "000000", lector.Item("pp_Ubigeo")))

                        .IdTipoAlmacen = CInt(IIf(IsDBNull(lector.Item("IdTipoAlmacen")) = True, 0, lector.Item("IdTipoAlmacen")))

                    End With


                End If
                lector.Close()
                Return objPuntoPartida
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub DeletexIdDocumento(ByVal iddocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As New SqlCommand("_PuntoPartidaDeletexIdDocumento", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)
        cmd.ExecuteNonQuery()

    End Sub

    Public Function SelectxIdDocumentoDireccionP(ByVal iddocumento As Integer) As Entidades.PuntoPartida
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PuntoPartidaSelectxIdDocumento_DireccionAlmacen", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim objPuntoPartida As Entidades.PuntoPartida = Nothing
                If lector.Read Then

                    objPuntoPartida = New Entidades.PuntoPartida

                    With objPuntoPartida

                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                        .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                        .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                        .Direccion = CStr(IIf(IsDBNull(lector.Item("pp_Direccion")) = True, "", lector.Item("pp_Direccion")))
                        .Ubigeo = CStr(IIf(IsDBNull(lector.Item("pp_Ubigeo")) = True, "000000", lector.Item("pp_Ubigeo")))

                        .IdTipoAlmacen = CInt(IIf(IsDBNull(lector.Item("IdTipoAlmacen")) = True, 0, lector.Item("IdTipoAlmacen")))

                    End With


                End If
                lector.Close()
                Return objPuntoPartida
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
