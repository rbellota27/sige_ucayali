﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOModelo
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Private cn As SqlConnection
    Private cmd As SqlCommand
    Private lector As SqlDataReader

    Public Function InsertaModelo(ByVal Modelo As Entidades.Modelo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@m_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(Modelo.m_Nombre = Nothing, DBNull.Value, Modelo.m_Nombre)
        ArrayParametros(1) = New SqlParameter("@m_Estado", SqlDbType.Bit)
        ArrayParametros(1).Value = IIf(Modelo.Estado = Nothing, DBNull.Value, Modelo.Estado)
        ArrayParametros(2) = New SqlParameter("@m_Codigo", SqlDbType.Char)
        ArrayParametros(2).Value = IIf(Modelo.m_Codigo = Nothing, DBNull.Value, Modelo.m_Codigo)
        
        Dim cmd As New SqlCommand("_ModeloInsert", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return True
    End Function
    Public Function ActualizaModelo(ByVal Modelo As Entidades.Modelo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@m_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(Modelo.m_Nombre = Nothing, DBNull.Value, Modelo.m_Nombre)
        ArrayParametros(1) = New SqlParameter("@m_Estado", SqlDbType.Bit)
        ArrayParametros(1).Value = Modelo.Estado 'IIf(Modelo.Estado = Nothing, DBNull.Value, Modelo.Estado)

        ArrayParametros(2) = New SqlParameter("@m_Codigo", SqlDbType.Char)
        ArrayParametros(2).Value = IIf(Modelo.m_Codigo = Nothing, DBNull.Value, Modelo.m_Codigo)
        ArrayParametros(3) = New SqlParameter("@IdModelo", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(Modelo.Id = Nothing, DBNull.Value, Modelo.Id)
        Return HDAO.Update(cn, "_ModeloUpdate", ArrayParametros)
    End Function
    Public Function SelectAllModelo() As List(Of Entidades.Modelo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ModeloSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Modelo)
                Do While lector.Read
                    Dim obj As New Entidades.Modelo
                    obj.IdModelo = CStr(IIf(IsDBNull(lector.Item("IdModelo")) = True, "", lector.Item("IdModelo")))
                    obj.m_Nombre = CStr(IIf(IsDBNull(lector.Item("m_Nombre")) = True, "", lector.Item("m_Nombre")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("m_Estado")) = True, "", lector.Item("m_Estado")))
                    obj.m_Codigo = CStr(IIf(IsDBNull(lector.Item("m_Codigo")) = True, "", lector.Item("m_Codigo")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivoModelo() As List(Of Entidades.Modelo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ModeloSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim Lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                Lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.Modelo)
                Do While Lector.Read
                    Dim obj As New Entidades.Modelo
                    obj.IdModelo = CStr(IIf(IsDBNull(Lector.Item("IdModelo")) = True, "", Lector.Item("IdModelo")))
                    obj.m_Nombre = CStr(IIf(IsDBNull(Lector.Item("m_Nombre")) = True, "", Lector.Item("m_Nombre")))
                    obj.Estado = CBool(IIf(IsDBNull(Lector.Item("m_Estado")) = True, "", Lector.Item("m_Estado")))
                    obj.m_Codigo = CStr(IIf(IsDBNull(Lector.Item("m_Codigo")) = True, "", Lector.Item("m_Codigo")))

                    lista.Add(obj)
                Loop
                Lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivoModelo() As List(Of Entidades.Modelo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ModeloSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim Lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                Lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.Modelo)
                Do While Lector.Read
                    Dim obj As New Entidades.Modelo
                    obj.IdModelo = CStr(IIf(IsDBNull(Lector.Item("IdModelo")) = True, "", Lector.Item("IdModelo")))
                    obj.m_Nombre = CStr(IIf(IsDBNull(Lector.Item("m_Nombre")) = True, "", Lector.Item("m_Nombre")))
                    obj.Estado = CBool(IIf(IsDBNull(Lector.Item("m_Estado")) = True, "", Lector.Item("m_Estado")))
                    obj.m_Codigo = CStr(IIf(IsDBNull(Lector.Item("m_Codigo")) = True, "", Lector.Item("m_Codigo")))
                    lista.Add(obj)
                Loop
                Lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Modelo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ModeloSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@m_Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Modelo)
                Do While lector.Read
                    Dim obj As New Entidades.Modelo
                    obj.IdModelo = CStr(IIf(IsDBNull(lector.Item("IdModelo")) = True, "", lector.Item("IdModelo")))
                    obj.m_Nombre = CStr(IIf(IsDBNull(lector.Item("m_Nombre")) = True, "", lector.Item("m_Nombre")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("m_Estado")) = True, "", lector.Item("m_Estado")))
                    obj.m_Codigo = CStr(IIf(IsDBNull(lector.Item("m_Codigo")) = True, "", lector.Item("m_Codigo")))
                    Lista.Add(obj)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombreActivo(ByVal nombre As String) As List(Of Entidades.Modelo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ModeloSelectAllxNombreActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@m_Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Modelo)
                Do While lector.Read
                    Dim obj As New Entidades.Modelo
                    obj.IdModelo = CStr(IIf(IsDBNull(lector.Item("IdModelo")) = True, "", lector.Item("IdModelo")))
                    obj.m_Nombre = CStr(IIf(IsDBNull(lector.Item("m_Nombre")) = True, "", lector.Item("m_Nombre")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("m_Estado")) = True, "", lector.Item("m_Estado")))
                    obj.m_Codigo = CStr(IIf(IsDBNull(lector.Item("m_Codigo")) = True, "", lector.Item("m_Codigo")))
                    Lista.Add(obj)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombreInactivo(ByVal nombre As String) As List(Of Entidades.Modelo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ModeloSelectAllxNombreInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@m_Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Modelo)
                Do While lector.Read
                    Dim obj As New Entidades.Modelo
                    obj.IdModelo = CStr(IIf(IsDBNull(lector.Item("IdModelo")) = True, "", lector.Item("IdModelo")))
                    obj.m_Nombre = CStr(IIf(IsDBNull(lector.Item("m_Nombre")) = True, "", lector.Item("m_Nombre")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("m_Estado")) = True, "", lector.Item("m_Estado")))
                    obj.m_Codigo = CStr(IIf(IsDBNull(lector.Item("m_Codigo")) = True, "", lector.Item("m_Codigo")))
                    Lista.Add(obj)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal Codigo As Integer) As List(Of Entidades.Modelo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ModeloSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdModelo", Codigo)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Modelo)
                Do While lector.Read
                    Dim obj As New Entidades.Modelo
                    obj.IdModelo = CStr(IIf(IsDBNull(lector.Item("IdModelo")) = True, "", lector.Item("IdModelo")))
                    obj.m_Nombre = CStr(IIf(IsDBNull(lector.Item("m_Nombre")) = True, "", lector.Item("m_Nombre")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("m_Estado")) = True, "", lector.Item("m_Estado")))
                    obj.m_Codigo = CStr(IIf(IsDBNull(lector.Item("m_Codigo")) = True, "", lector.Item("m_Codigo")))
                    Lista.Add(obj)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

End Class
