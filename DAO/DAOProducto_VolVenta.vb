﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




'***********************    MIERCOLES 04 ABRIL 2010 HORA 05_39 PM



'***********************    MIERCOLES 04 ABRIL 2010 HORA 04_54 PM



Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOProducto_VolVenta
    Inherits DAOMantenedor

    'Dim HDAO As New DAO.HelperDAO
    'Dim objConexion As New Conexion

    Public Function SelectxLineaxSubLineaxTipoPVxTienda(ByVal IdLinea As Integer, _
    ByVal IdSubLinea As Integer, ByVal IdTipoPV As Integer, _
    ByVal IdTienda As Integer, ByVal IdTipoExistencia As Integer, ByVal tabla As DataTable) As List(Of Entidades.Producto_VolVenta)

        Cn = objConexion.ConexionSIGE()

        Dim cmd As New SqlCommand("_ProductoVolVentaViewSelectxLineaxSubLineaxTipoPVxTienda", Cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdLinea", IIf(IdLinea = Nothing, DBNull.Value, IdLinea))
        cmd.Parameters.AddWithValue("@IdSublinea", IIf(IdSubLinea = Nothing, DBNull.Value, IdSubLinea))
        cmd.Parameters.AddWithValue("@IdTipoPV", IIf(IdTipoPV = Nothing, DBNull.Value, IdTipoPV))
        cmd.Parameters.AddWithValue("@IdTienda", IIf(IdTienda = Nothing, DBNull.Value, IdTienda))
        cmd.Parameters.AddWithValue("@IdTipoExistencia", IIf(IdTipoExistencia = Nothing, DBNull.Value, IdTipoExistencia))
        cmd.Parameters.AddWithValue("@tabla", tabla)


        Dim lector As SqlDataReader


        Try

            Cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Dim Lista As New List(Of Entidades.Producto_VolVenta)
            Do While lector.Read
                Dim obj As New Entidades.Producto_VolVenta

                obj.IdLinea = UCInt(lector("IdLinea"))
                obj.IdSubLinea = UCInt(lector("IdSubLinea"))
                obj.IdTipoPV = UCInt(lector("IdTipoPV"))
                obj.IdTienda = UCInt(lector("IdTienda"))
                obj.IdProducto = UCInt(lector("IdProducto"))

                obj.prod_Codigo = UCStr(lector("prod_Codigo"))
                obj.prod_Nombre = UCStr(lector("prod_Nombre"))
                obj.um_NombreLargo = UCStr(lector("um_NombreLargo"))
                obj.vven_CantidadMin_UMPr = UCDec(lector("vven_CantidadMin_UMPr"))
                'obj.vven_FechaRegistro = UCDate(lector("vven_FechaRegistro"))

                Lista.Add(obj)
            Loop
            lector.Close()
            Return Lista
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectxIdProductoxIdTipoPVxIdTienda(ByVal IdProducto As Integer, _
    ByVal IdTipoPV As Integer, ByVal IdTienda As Integer) As List(Of Entidades.Producto_VolVenta)

        cn = objConexion.ConexionSIGE()
        Dim Prm() As SqlParameter = New SqlParameter(2) {}

        Prm(0) = getParam(IdProducto, "@IdProducto", SqlDbType.Int)
        Prm(1) = getParam(IdTipoPV, "@IdTipoPV", SqlDbType.Int)
        Prm(2) = getParam(IdTienda, "@IdTienda", SqlDbType.Int)

        Dim lector As SqlDataReader

        Try
            Using Cn
                Cn.Open()
                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "_VolumenVentaSelectxIdProductoxIdTipoPVxIdTienda", Prm)

                Dim Lista As New List(Of Entidades.Producto_VolVenta)
                Do While lector.Read
                    Dim obj As New Entidades.Producto_VolVenta

                    obj.IdProducto = UCInt(lector("IdProducto"))
                    obj.IdTipoPV = UCInt(lector("IdTipoPV"))
                    obj.IdTienda = UCInt(lector("IdTienda"))

                    obj.prod_Nombre = UCStr(lector("prod_Nombre"))
                    obj.um_NombreCorto = UCStr(lector("UnidadMedida"))
                    obj.vven_CantidadMin_UMPr = UCDec(lector("vven_CantidadMin_UMPr"))
                    obj.TipoPV = UCStr(lector("pv_Nombre"))
                    obj.TipoPrecioV_Ref = UCStr(lector("TipoPrecioV_Ref"))
                    'obj.vven_FechaRegistro = UCDate(lector("vven_FechaRegistro"))

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
