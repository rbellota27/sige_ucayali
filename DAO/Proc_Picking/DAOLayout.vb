﻿Imports System.Data.SqlClient
Public Class DAOLayout
    Inherits Conexion


    Public Function ListarLayout(ByVal cn As SqlConnection, ByVal IdAlmacen As Integer, ByVal IdZona As Integer) As List(Of Entidades.beLayout)

        Dim lista As List(Of Entidades.beLayout) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "_ListarUbicacionesLayout_PorZonas"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@IdAlmacen", SqlDbType.Int)).Value = IdAlmacen
                .Parameters.Add(New SqlParameter("@IdZona", SqlDbType.Int)).Value = IdZona


            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.beLayout)

                Dim ind1 As Integer = rdr.GetOrdinal("IdUbicacion")
                Dim ind2 As Integer = rdr.GetOrdinal("IdEstanteria")
                Dim ind3 As Integer = rdr.GetOrdinal("IdZona")
                Dim ind4 As Integer = rdr.GetOrdinal("NroColumna")
                Dim ind5 As Integer = rdr.GetOrdinal("NroFila")
                Dim ind6 As Integer = rdr.GetOrdinal("FondoD")
                Dim ind7 As Integer = rdr.GetOrdinal("AnchoL")
                Dim ind8 As Integer = rdr.GetOrdinal("AlturaH")
                Dim ind9 As Integer = rdr.GetOrdinal("PesoMax")
                Dim ind10 As Integer = rdr.GetOrdinal("CodEstanteria")
                Dim ind11 As Integer = rdr.GetOrdinal("IdTipoPaleta")
                Dim ind12 As Integer = rdr.GetOrdinal("crossdocking")
                Dim ind13 As Integer = rdr.GetOrdinal("picking")
                Dim ind14 As Integer = rdr.GetOrdinal("Piso")
                Dim ind15 As Integer = rdr.GetOrdinal("OrdenRecorrido")
                Dim ind16 As Integer = rdr.GetOrdinal("MedAncho")
                Dim ind17 As Integer = rdr.GetOrdinal("MedAltura")

                Dim objeto As Entidades.beLayout = Nothing

                While rdr.Read()
                    objeto = New Entidades.beLayout
                    With objeto

                        .IdUbicacion = rdr.GetInt32(ind1)
                        .IdEstanteria = rdr.GetInt32(ind2)
                        .IdZona = rdr.GetInt32(ind3)
                        .NroColumna = rdr.GetInt32(ind4)
                        .NroFila = rdr.GetInt32(ind5)
                        .FondoD = rdr.GetDecimal(ind6)
                        .AnchoL = rdr.GetDecimal(ind7)
                        .AlturaH = rdr.GetDecimal(ind8)
                        .PesoMax = rdr.GetDecimal(ind9)
                        .CodEstanteria = rdr.GetString(ind10)
                        .IdTipoPaleta = rdr.GetInt32(ind11)
                        .crossdocking = rdr.GetBoolean(ind12)
                        .picking = rdr.GetBoolean(ind13)
                        .Piso = rdr.GetInt32(ind14)
                        .OrdenRecorrido = rdr.GetInt32(ind15)
                        .MedAncho = rdr.GetInt32(ind16)
                        .MedAltura = rdr.GetInt32(ind17)

                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

End Class
