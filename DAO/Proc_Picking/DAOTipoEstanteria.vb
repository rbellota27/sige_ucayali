﻿
Imports System.Data.SqlClient
Public Class DAOTipoEstanteria
    Inherits Conexion

    Public Function ListarTipoEstanteria(ByVal cn As SqlConnection) As List(Of Entidades.beTipoEstanteria)

        Dim lista As List(Of Entidades.beTipoEstanteria) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "Usp_ListarTipoEstanteria"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure


            End With

            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.beTipoEstanteria)

                Dim ind1 As Integer = rdr.GetOrdinal("IdTipoEstanteria")
                Dim ind2 As Integer = rdr.GetOrdinal("NombreTipoEstanteria")
                Dim ind3 As Integer = rdr.GetOrdinal("CodEstanteria")
                Dim ind4 As Integer = rdr.GetOrdinal("Estado")
                Dim ind5 As Integer = rdr.GetOrdinal("ParametroUbic")
                Dim ind6 As Integer = rdr.GetOrdinal("Observacion")
                Dim ind7 As Integer = rdr.GetOrdinal("Imagen")


                Dim objeto As Entidades.beTipoEstanteria = Nothing

                While rdr.Read()

                    objeto = New Entidades.beTipoEstanteria
                    With objeto

                        .IdTipoEstanteria = rdr.GetInt32(ind1)
                        .NombreTipoEstanteria = rdr.GetString(ind2)
                        .CodEstanteria = rdr.GetString(ind3)
                        .Estado = rdr.GetInt32(ind4)
                        .ParametroUbic = rdr.GetInt32(ind5)
                        .Observacion = rdr.GetString(ind6)
                        .Imagen = rdr.GetString(ind7)

                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

End Class
