﻿Imports System.Data.SqlClient
Public Class DAOListarPersonal

    Public Function listarPersonal(ByVal cn As SqlConnection) As List(Of Entidades.bePersonal)

        Dim lista As List(Of Entidades.bePersonal) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "Sp_Select_PersonalSanicenter"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

            End With
            'Dim rdr As SqlDataReader = cmd.BeginExecuteReader(CommandBehavior.SingleResult)
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.bePersonal)



                Dim ind1 As Integer = rdr.GetOrdinal("Ideint")
                Dim ind2 As Integer = rdr.GetOrdinal("nomane")
                Dim ind3 As Integer = rdr.GetOrdinal("ideane1")

                Dim objeto As Entidades.bePersonal = Nothing

                While rdr.Read()
                    objeto = New Entidades.bePersonal
                    With objeto

                        .Ideint = rdr.GetDecimal(ind1)
                        .nomane = rdr.GetString(ind2)
                        .ideane1 = rdr.GetString(ind3)
                        lista.Add(objeto)

                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function


    Public Function listaPersMilla(ByVal cn As SqlConnection) As List(Of Entidades.bePersonalMilla)

        Dim lista As List(Of Entidades.bePersonalMilla) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "Sp_Select_PersonalMilla"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

            End With

            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.bePersonalMilla)

                Dim ind1 As Integer = rdr.GetOrdinal("IdEmpleado")
                Dim ind2 As Integer = rdr.GetOrdinal("NombreEmpleado")
                Dim ind3 As Integer = rdr.GetOrdinal("Codigo")

                Dim objeto As Entidades.bePersonalMilla = Nothing

                While rdr.Read()
                    objeto = New Entidades.bePersonalMilla
                    With objeto

                        .IdEmpleado = rdr.GetInt32(ind1)
                        .NombreEmpleado = rdr.GetString(ind2)
                        .Codigo = rdr.GetString(ind3)
                        lista.Add(objeto)

                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function


    Public Function GuardarPersonal(ByVal cn As SqlConnection, ByVal IdPersona As Integer, ByVal nombre As String) As List(Of Entidades.bePersonalMilla)
  
        Dim lista As List(Of Entidades.bePersonalMilla) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "USP_InsertUpdateEmpleadoMilla"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
            End With

            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.bePersonalMilla)

                Dim ind1 As Integer = rdr.GetOrdinal("IdEmpleado")
                Dim ind2 As Integer = rdr.GetOrdinal("NombreEmpleado")
                Dim ind3 As Integer = rdr.GetOrdinal("Codigo")

                Dim objeto As Entidades.bePersonalMilla = Nothing

                While rdr.Read()

                    objeto = New Entidades.bePersonalMilla
                    With objeto

                        .IdEmpleado = rdr.GetInt32(ind1)
                        .NombreEmpleado = rdr.GetString(ind2)
                        .Codigo = rdr.GetString(ind3)
                        lista.Add(objeto)

                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista

    End Function



End Class
