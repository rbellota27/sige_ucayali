﻿Imports System.Data.SqlClient



Public Class DAOListarEstanteria
    Inherits Conexion

    Public Function listarEstanterias(ByVal cn As SqlConnection) As List(Of Entidades.be_Estanteria)

        Dim lista As List(Of Entidades.be_Estanteria) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "USP_SelectEstanterias"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_Estanteria)

                Dim ind1 As Integer = rdr.GetOrdinal("IdEstanteria")
                Dim ind2 As Integer = rdr.GetOrdinal("NombreEstanteria")

                Dim objeto As Entidades.be_Estanteria = Nothing

                While rdr.Read()
                    objeto = New Entidades.be_Estanteria
                    With objeto

                        .IdEstanteria = rdr.GetInt32(ind1)
                        .NombreEstanteria = rdr.GetString(ind2)
                        lista.Add(objeto)

                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function



End Class


