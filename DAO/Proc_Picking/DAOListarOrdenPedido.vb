﻿Imports System.Data.SqlClient
Public Class DAOListarOrdenPedido
Inherits Conexion

    Public Function ListarOrdenPedido(ByVal cn As SqlConnection, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal fecInicio As Date, ByVal fecFin As Date) As List(Of Entidades.OrdenPedidoPicking)

        Dim lista As List(Of Entidades.OrdenPedidoPicking) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "_DocumentoSelectDocxDespachar_picking_abc"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@IdEmpresa", SqlDbType.Int)).Value = IdEmpresa
                .Parameters.Add(New SqlParameter("@IdAlmacen", SqlDbType.Int)).Value = IdAlmacen
                .Parameters.Add(New SqlParameter("@FechaInicio", SqlDbType.Date)).Value = fecInicio
                .Parameters.Add(New SqlParameter("@FechaFin", SqlDbType.Date)).Value = fecFin


            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.OrdenPedidoPicking)

                Dim ind1 As Integer = rdr.GetOrdinal("Marcar")
                Dim ind2 As Integer = rdr.GetOrdinal("iddocumento")
                Dim ind3 As Integer = rdr.GetOrdinal("NroOPV")
                Dim ind4 As Integer = rdr.GetOrdinal("doc_FechaEmision")
                Dim ind5 As Integer = rdr.GetOrdinal("tie_Nombre")
                Dim ind6 As Integer = rdr.GetOrdinal("FechaHoraRegistro")
                Dim ind7 As Integer = rdr.GetOrdinal("CadTipoDocumentoRef")
                Dim ind8 As Integer = rdr.GetOrdinal("TipoOperacion")



                Dim objeto As Entidades.OrdenPedidoPicking = Nothing

                While rdr.Read()

                    objeto = New Entidades.OrdenPedidoPicking
                    With objeto

                        .Marcar = rdr.GetBoolean(ind1)
                        .iddocumento = rdr.GetInt32(ind2)
                        .NroOPV = rdr.GetString(ind3)
                        .doc_FechaEmision = rdr.GetString(ind4)
                        .tie_Nombre = rdr.GetString(ind5)
                        .FechaHoraRegistro = rdr.GetDateTime(ind6)
                        .CadTipoDocumentoRef = rdr.GetString(ind7)
                        .TipoOperacion = rdr.GetString(ind8)

                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

End Class
