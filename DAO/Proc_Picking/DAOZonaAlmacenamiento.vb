﻿Imports System.Data.SqlClient
Public Class DAOZonaAlmacenamiento
    Inherits Conexion

    Public Function listarComboZonaAlmacenamiento(ByVal cn As SqlConnection, ByVal IdTienda As Integer) As List(Of Entidades.beZonaAlmacenamiento)

        Dim lista As List(Of Entidades.beZonaAlmacenamiento) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "listarComboZonaAlmacenamiento"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@IdTienda", SqlDbType.Int)).Value = IdTienda

            End With

            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.beZonaAlmacenamiento)

                Dim ind1 As Integer = rdr.GetOrdinal("IdZonaAlmacenamiento")
                Dim ind2 As Integer = rdr.GetOrdinal("NombreZonaAlmc")
        
                Dim objeto As Entidades.beZonaAlmacenamiento = Nothing

                While rdr.Read()
                    objeto = New Entidades.beZonaAlmacenamiento
                    With objeto

                        .IdZonaAlmacenamiento = rdr.GetInt32(ind1)
                        .NombreZonaAlmc = rdr.GetString(ind2)

                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

    Public Function ListarZonaAlmacenamiento(ByVal cn As SqlConnection, ByVal IdTienda As Integer) As List(Of Entidades.beZonaAlmacenamiento)

        Dim lista As List(Of Entidades.beZonaAlmacenamiento) = Nothing
        Using cmd As New SqlCommand()
            With cmd
                .CommandText = "ListarZonaAlmacenamiento"
                .Connection = cn
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure


                .Parameters.Add(New SqlParameter("@IdTienda", SqlDbType.Int)).Value = IdTienda

            End With

            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.beZonaAlmacenamiento)

                Dim ind1 As Integer = rdr.GetOrdinal("IdZonaAlmacenamiento")
                Dim ind2 As Integer = rdr.GetOrdinal("NombreZonaAlmc")
                Dim ind3 As Integer = rdr.GetOrdinal("Tienda")
                Dim ind4 As Integer = rdr.GetOrdinal("IdTienda")



                Dim objeto As Entidades.beZonaAlmacenamiento = Nothing

                While rdr.Read()

                    objeto = New Entidades.beZonaAlmacenamiento
                    With objeto

                        .IdZonaAlmacenamiento = rdr.GetInt32(ind1)
                        .NombreZonaAlmc = rdr.GetString(ind2)
                        .Tienda = rdr.GetString(ind3)
                        .IdTienda = rdr.GetInt32(ind4)

                        lista.Add(objeto)
                    End With
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

End Class
