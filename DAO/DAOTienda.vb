'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'*****************  VIERNES 22 ENERO 2010 HORA 03_54 PM

'*************************************************
'Autor   : Chang Carnero,Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 02-Noviembre-2009
'Hora    : 1:00 pm
'*************************************************

'*************************************************
'Autor   : Nagamine Molina, Jorge
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 02-Noviembre-2009
'Hora    : 12:13 pm
'*************************************************



Imports System.Data.SqlClient

Public Class DAOTienda
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

    Public Function UpdateObjetivoxTienda(ByVal obj As Entidades.Tienda) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@num_objetivo", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(obj.ObjetivoxTienda = Nothing, DBNull.Value, obj.ObjetivoxTienda)
        ArrayParametros(1) = New SqlParameter("@mes", SqlDbType.VarChar)
        ArrayParametros(1).Value = IIf(obj.Mes = Nothing, DBNull.Value, obj.Mes)
        ArrayParametros(2) = New SqlParameter("@anio", SqlDbType.VarChar)
        ArrayParametros(2).Value = IIf(obj.Anio = Nothing, DBNull.Value, obj.Anio)
        ArrayParametros(3) = New SqlParameter("@idobjetivo", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(obj.IdObjetivo = Nothing, DBNull.Value, obj.IdObjetivo)
        ArrayParametros(4) = New SqlParameter("@idTienda", SqlDbType.Int)
        ArrayParametros(4).Value = IIf(obj.IdObjetivo = Nothing, DBNull.Value, obj.Id)

        Return HDAO.UpdateTi(cn, "_TiendaUpdateObjetivo", ArrayParametros)
    End Function
    Public Function SelectxIdTiendaObjetivo(ByVal obj As Entidades.Tienda) As Entidades.Tienda

        Dim Etienda As Entidades.Tienda = New Entidades.Tienda
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Try

            Dim cmd As New SqlCommand("SelectxIdTiendaObjetivo", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@anio", obj.Anio)
            cmd.Parameters.AddWithValue("@mes", obj.Mes)
            cmd.Parameters.AddWithValue("@idtienda", obj.Id)
            cn.Open()
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            If (lector.Read) Then

                With Etienda
                    .Nombre = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")), "", lector.Item("tie_Nombre")))
                    .ObjetivoxTienda = CInt(IIf(IsDBNull(lector.Item("num_objetivo")), 0, lector.Item("num_objetivo")))
                    .Mes = CStr(IIf(IsDBNull(lector.Item("mes")), "", lector.Item("mes")))
                    .Anio = CStr(IIf(IsDBNull(lector.Item("anio")), "", lector.Item("anio")))
                    .IdObjetivo = CInt(IIf(IsDBNull(lector.Item("idobjetivo")), 0, lector.Item("idobjetivo")))
                    .Id = CInt(IIf(IsDBNull(lector.Item("idTienda")), 0, lector.Item("idTienda")))
                End With
            Else
                If (Not (obj.Id = Nothing And obj.Mes = Nothing And obj.Anio = Nothing)) Then
                    obj.Id = 0
                End If

            End If
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return Etienda
    End Function

    Public Function SelectAllObjetivoxTienda(ByVal obj As Entidades.Tienda) As List(Of Entidades.Tienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SelectAllObjetivoxTienda", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@anio", obj.Anio)
        cmd.Parameters.AddWithValue("@mes", obj.Mes)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tienda)
                Do While lector.Read
                    Dim objTienda As New Entidades.Tienda
                    objTienda.Nombre = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")), "", lector.Item("tie_Nombre")))
                    objTienda.ObjetivoxTienda = CInt(IIf(IsDBNull(lector.Item("num_objetivo")), 0, lector.Item("num_objetivo")))
                    objTienda.Mes = CStr(IIf(IsDBNull(lector.Item("mes")), "", lector.Item("mes")))
                    objTienda.Anio = CStr(IIf(IsDBNull(lector.Item("anio")), "", lector.Item("anio")))
                    objTienda.ResultadoEscala = CStr(IIf(IsDBNull(lector.Item("nombre_escala")), "", lector.Item("nombre_escala")))

                    Lista.Add(objTienda)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function InsertObjxTienda(ByVal obj As Entidades.Tienda) As Boolean

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@idTienda", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(obj.Id = Nothing, DBNull.Value, obj.Id)
        ArrayParametros(1) = New SqlParameter("@num_objetivo", SqlDbType.Int)
        ArrayParametros(1).Value = IIf(obj.ObjetivoxTienda = Nothing, DBNull.Value, obj.ObjetivoxTienda)
        ArrayParametros(2) = New SqlParameter("@mes", SqlDbType.VarChar)
        ArrayParametros(2).Value = IIf(obj.Mes = Nothing, DBNull.Value, obj.Mes)
        ArrayParametros(3) = New SqlParameter("@anio", SqlDbType.VarChar)
        ArrayParametros(3).Value = IIf(obj.Anio = Nothing, DBNull.Value, obj.Anio)
        'ArrayParametros(4) = New SqlParameter("@anio", SqlDbType.VarChar)
        'ArrayParametros(4).Value = IIf(obj.Anio = Nothing, DBNull.Value, obj.Anio)
        Return HDAO.Insertation(cn, "InsertObjxTienda", ArrayParametros)
    End Function



    Public Function SelectCboxEmpresa(ByVal idEmpresa As Integer) As List(Of Entidades.Tienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TiendaSelectCboxEmpresa", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", idEmpresa)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tienda)
                Do While lector.Read
                    Dim objTienda As New Entidades.Tienda
                    objTienda.Id = CInt(IIf(IsDBNull(lector.Item("IdTienda")), 0, lector.Item("IdTienda")))
                    objTienda.Nombre = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")), "", lector.Item("tie_Nombre")))
                    Lista.Add(objTienda)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCbo() As List(Of Entidades.Tienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TiendaSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tienda)
                Do While lector.Read
                    Dim objTienda As New Entidades.Tienda
                    objTienda.Id = CInt(IIf(IsDBNull(lector.Item("IdTienda")), 0, lector.Item("IdTienda")))
                    objTienda.Nombre = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")), "", lector.Item("tie_Nombre")))
                    Lista.Add(objTienda)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.Tienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TiendaSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tienda)
                Do While lector.Read
                    Dim tienda As New Entidades.Tienda
                    tienda.Id = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                    tienda.Nombre = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")), "", lector.Item("tie_Nombre")))
                    tienda.Estado = CStr(IIf(IsDBNull(lector.Item("tie_Estado")), "", lector.Item("tie_Estado")))
                    Lista.Add(tienda)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Tienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TiendaSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tienda)
                Do While lector.Read
                    Dim tienda As New Entidades.Tienda
                    tienda.Id = CInt(IIf(IsDBNull(lector.Item("IdTienda")), 0, lector.Item("IdTienda")))
                    tienda.Nombre = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")), "", lector.Item("tie_Nombre")))
                    tienda.Estado = CStr(IIf(IsDBNull(lector.Item("tie_Estado")), "", lector.Item("tie_Estado")))
                    Lista.Add(tienda)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Tienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TiendaSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tienda)
                Do While lector.Read
                    Dim tienda As New Entidades.Tienda
                    tienda.Id = CInt(IIf(IsDBNull(lector.Item("IdTienda")), 0, lector.Item("IdTienda")))
                    tienda.Nombre = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")), "", lector.Item("tie_Nombre")))
                    tienda.Estado = CStr(IIf(IsDBNull(lector.Item("tie_Estado")), "", lector.Item("tie_Estado")))
                    Lista.Add(tienda)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal IdTienda As Integer) As List(Of Entidades.Tienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TiendaSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tienda)
                Do While lector.Read
                    Dim obj As New Entidades.Tienda
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdTienda")), 0, lector.Item("IdTienda")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")), "", lector.Item("tie_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("tie_Estado")), "", lector.Item("tie_Estado")))
                    obj.Ubigeo = CStr(IIf(IsDBNull(lector.Item("tie_Ubigeo")), "000000", lector.Item("tie_Ubigeo")))
                    obj.Direccion = CStr(IIf(IsDBNull(lector.Item("tie_Direccion")), "", lector.Item("tie_Direccion")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Tienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TiendaSelectAllXNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tienda)
                Do While lector.Read
                    Dim tienda As New Entidades.Tienda
                    tienda.Id = CInt(IIf(IsDBNull(lector.Item("IdTienda")), 0, lector.Item("IdTienda")))
                    tienda.Nombre = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")), "", lector.Item("tie_Nombre")))
                    tienda.Estado = CStr(IIf(IsDBNull(lector.Item("tie_Estado")), "", lector.Item("tie_Estado")))
                    Lista.Add(tienda)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Tienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TiendaSelectXNombreXActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tienda)
                Do While lector.Read
                    Dim tienda As New Entidades.Tienda
                    tienda.Id = CInt(IIf(IsDBNull(lector.Item("IdTienda")), 0, lector.Item("IdTienda")))
                    tienda.Nombre = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")), "", lector.Item("tie_Nombre")))
                    tienda.Estado = CStr(IIf(IsDBNull(lector.Item("tie_Estado")), "", lector.Item("tie_Estado")))
                    Lista.Add(tienda)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Tienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TiendaSelectXNombreXInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tienda)
                Do While lector.Read
                    Dim tienda As New Entidades.Tienda
                    tienda.Id = CInt(IIf(IsDBNull(lector.Item("IdTienda")), 0, lector.Item("IdTienda")))
                    tienda.Nombre = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")), "", lector.Item("tie_Nombre")))
                    tienda.Estado = CStr(IIf(IsDBNull(lector.Item("tie_Estado")), "", lector.Item("tie_Estado")))
                    Lista.Add(tienda)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function InsertaConfiguracionTiendaT(ByVal IdTienda As Integer, ByVal lTiendaArea As List(Of Entidades.TiendaArea), ByVal lTiendaAlmacen As List(Of Entidades.TiendaAlmacen)) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        cn.Open()
        Dim T As SqlTransaction = cn.BeginTransaction(Data.IsolationLevel.Serializable)
        Try
            '***Agregar Tienda Area
            Dim daoTiendaArea As New DAO.DAOTiendaArea
            'daoTiendaArea.DeletexIdTienda(cn, IdTienda, T)
            'daoTiendaArea.InsertaTiendaAreaT(cn, lTiendaArea, T)
            daoTiendaArea.GrabaTiendaAreaT(cn, lTiendaArea, T)
            '***Agregar Tienda Almacen
            Dim daoTiendaAlmacen As New DAO.DAOTiendaAlmacen
            'daoTiendaAlmacen.DeletexIdTienda(cn, IdTienda, T)
            'daoTiendaAlmacen.InsertaTiendaAlmacenT(cn, lTiendaAlmacen, T)
            daoTiendaAlmacen.GrabaTiendaAlmacenT(cn, lTiendaAlmacen, T)
            '***Agregar Tienda Caja
            'Dim daoTiendaCaja As New DAO.DAOCaja
            ''daoTiendaCaja.DeletexIdTienda(cn, IdTienda, T)
            ''daoTiendaCaja.InsertaCajaT(cn, lTiendaCaja, T)

            'daoTiendaCaja.GrabaCajaT(cn, lTiendaCaja, T)
            T.Commit()
            Return True
        Catch ex As Exception
            T.Rollback()
            Return False
        Finally

        End Try
    End Function



    Public Function SelectCboxIdEmpresaxIdUsuario_Caja(ByVal idEmpresa As Integer, ByVal IdUsuario As Integer) As List(Of Entidades.Tienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TiendaSelectCboxIdEmpresaxIdUsuario_Caja", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", idEmpresa)
        cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tienda)
                Do While lector.Read
                    Dim objTienda As New Entidades.Tienda
                    objTienda.Id = CInt(IIf(IsDBNull(lector.Item("IdTienda")), 0, lector.Item("IdTienda")))
                    objTienda.Nombre = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")), "", lector.Item("tie_Nombre")))
                    Lista.Add(objTienda)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function InsertaTienda(ByVal tienda As Entidades.Tienda) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(10) {}
        ArrayParametros(0) = New SqlParameter("@tie_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(tienda.Nombre = Nothing, DBNull.Value, tienda.Nombre)
        ArrayParametros(1) = New SqlParameter("@tie_Ubigeo", SqlDbType.Char)
        ArrayParametros(1).Value = IIf(tienda.Ubigeo = Nothing, DBNull.Value, tienda.Ubigeo)
        ArrayParametros(2) = New SqlParameter("@tie_Direccion", SqlDbType.VarChar)
        ArrayParametros(2).Value = IIf(tienda.Direccion = Nothing, DBNull.Value, tienda.Direccion)
        ArrayParametros(3) = New SqlParameter("@tie_Referencia", SqlDbType.VarChar)
        ArrayParametros(3).Value = IIf(tienda.Referencia = Nothing, DBNull.Value, tienda.Referencia)
        ArrayParametros(4) = New SqlParameter("@tie_Telefono", SqlDbType.VarChar)
        ArrayParametros(4).Value = IIf(tienda.Telefono = Nothing, DBNull.Value, tienda.Telefono)
        ArrayParametros(5) = New SqlParameter("@tie_Fax", SqlDbType.VarChar)
        ArrayParametros(5).Value = IIf(tienda.Fax = Nothing, DBNull.Value, tienda.Fax)
        ArrayParametros(6) = New SqlParameter("@tie_Estado", SqlDbType.Char)
        ArrayParametros(6).Value = IIf(tienda.Estado = Nothing, DBNull.Value, tienda.Estado)
        ArrayParametros(7) = New SqlParameter("@IdTipoTienda", SqlDbType.Int)
        ArrayParametros(7).Value = IIf(tienda.IdTipoTienda = Nothing, DBNull.Value, tienda.IdTipoTienda)
        ArrayParametros(8) = New SqlParameter("@tie_EMail", SqlDbType.Int)
        ArrayParametros(8).Value = IIf(tienda.Email = Nothing, DBNull.Value, tienda.Email)
        ArrayParametros(9) = New SqlParameter("@tie_CtaDebeVenta", SqlDbType.VarChar)
        ArrayParametros(9).Value = IIf(tienda.CtaDebeVenta = Nothing, DBNull.Value, tienda.CtaDebeVenta)
        ArrayParametros(10) = New SqlParameter("@tie_CtaHaberVenta", SqlDbType.VarChar)
        ArrayParametros(10).Value = IIf(tienda.CtaHaberVenta = Nothing, DBNull.Value, tienda.CtaHaberVenta)
        Return HDAO.Insert(cn, "_TiendaInsert", ArrayParametros)
    End Function


    Public Function ActualizaTienda(ByVal tienda As Entidades.Tienda) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(11) {}
        ArrayParametros(0) = New SqlParameter("@tie_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(tienda.Nombre = Nothing, DBNull.Value, tienda.Nombre)
        ArrayParametros(1) = New SqlParameter("@tie_Ubigeo", SqlDbType.Char)
        ArrayParametros(1).Value = IIf(tienda.Ubigeo = Nothing, DBNull.Value, tienda.Ubigeo)
        ArrayParametros(2) = New SqlParameter("@tie_Direccion", SqlDbType.VarChar)
        ArrayParametros(2).Value = IIf(tienda.Direccion = Nothing, DBNull.Value, tienda.Direccion)
        ArrayParametros(3) = New SqlParameter("@tie_Referencia", SqlDbType.VarChar)
        ArrayParametros(3).Value = IIf(tienda.Referencia = Nothing, DBNull.Value, tienda.Referencia)
        ArrayParametros(4) = New SqlParameter("@tie_Telefono", SqlDbType.VarChar)
        ArrayParametros(4).Value = IIf(tienda.Telefono = Nothing, DBNull.Value, tienda.Telefono)
        ArrayParametros(5) = New SqlParameter("@tie_Fax", SqlDbType.VarChar)
        ArrayParametros(5).Value = IIf(tienda.Fax = Nothing, DBNull.Value, tienda.Fax)
        ArrayParametros(6) = New SqlParameter("@tie_Estado", SqlDbType.Char)
        ArrayParametros(6).Value = IIf(tienda.Estado = Nothing, DBNull.Value, tienda.Estado)
        ArrayParametros(7) = New SqlParameter("@IdTipoTienda", SqlDbType.Int)
        ArrayParametros(7).Value = IIf(tienda.IdTipoTienda = Nothing, DBNull.Value, tienda.IdTipoTienda)
        ArrayParametros(8) = New SqlParameter("@tie_EMail", SqlDbType.Int)
        ArrayParametros(8).Value = IIf(tienda.Email = Nothing, DBNull.Value, tienda.Email)
        ArrayParametros(9) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(9).Value = IIf(tienda.Id = Nothing, DBNull.Value, tienda.Id)
        ArrayParametros(10) = New SqlParameter("@tie_CtaDebeVenta", SqlDbType.Int)
        ArrayParametros(10).Value = IIf(tienda.CtaDebeVenta = Nothing, DBNull.Value, tienda.CtaDebeVenta)
        ArrayParametros(11) = New SqlParameter("@tie_CtaHaberVenta", SqlDbType.Int)
        ArrayParametros(11).Value = IIf(tienda.CtaHaberVenta = Nothing, DBNull.Value, tienda.CtaHaberVenta)

        Return HDAO.Update(cn, "_TiendaUpdate", ArrayParametros)
    End Function



    Public Function BuscaAllxNombre(ByVal nombre As String) As List(Of Entidades.Tienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TiendaBuscaXNombreXActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tienda)
                Do While lector.Read
                    Dim tienda As New Entidades.Tienda
                    tienda.Id = CInt(IIf(IsDBNull(lector.Item("IdTienda")), 0, lector.Item("IdTienda")))
                    tienda.Nombre = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")), "", lector.Item("tie_Nombre")))
                    tienda.Ubigeo = CStr(IIf(IsDBNull(lector.Item("tie_Ubigeo")), "", lector.Item("tie_Ubigeo")))
                    tienda.Dpto = CStr(IIf(IsDBNull(lector.Item("Departamento")), "", lector.Item("Departamento")))
                    tienda.Prov = CStr(IIf(IsDBNull(lector.Item("Provincia")), "", lector.Item("Provincia")))
                    tienda.Dist = CStr(IIf(IsDBNull(lector.Item("Distrito")), "", lector.Item("Distrito")))
                    tienda.Direccion = CStr(IIf(IsDBNull(lector.Item("tie_Direccion")), "", lector.Item("tie_Direccion")))
                    tienda.Referencia = CStr(IIf(IsDBNull(lector.Item("tie_Referencia")), "", lector.Item("tie_Referencia")))
                    tienda.Telefono = CStr(IIf(IsDBNull(lector.Item("tie_Telefono")), "", lector.Item("tie_Telefono")))
                    tienda.Fax = CStr(IIf(IsDBNull(lector.Item("tie_Fax")), "", lector.Item("tie_Fax")))
                    tienda.IdTipoTienda = CInt(IIf(IsDBNull(lector.Item("IdTipoTienda")), 0, lector.Item("IdTipoTienda")))
                    tienda.TipoTienda = CStr(IIf(IsDBNull(lector.Item("TipoTienda")), "", lector.Item("TipoTienda")))
                    tienda.Estado = CStr(IIf(IsDBNull(lector.Item("tie_Estado")), "", lector.Item("tie_Estado")))
                    tienda.Email = CStr(IIf(IsDBNull(lector.Item("tie_EMail")), "", lector.Item("tie_EMail")))
                    Lista.Add(tienda)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function




    Public Function BuscaAllxNombreActivoInactivo(ByVal nombre As String) As List(Of Entidades.Tienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TiendaBuscaXNombreXActivoInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tienda)
                Do While lector.Read
                    Dim tienda As New Entidades.Tienda
                    tienda.Id = CInt(IIf(IsDBNull(lector.Item("IdTienda")), 0, lector.Item("IdTienda")))
                    tienda.Nombre = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")), "", lector.Item("tie_Nombre")))
                    tienda.Ubigeo = CStr(IIf(IsDBNull(lector.Item("tie_Ubigeo")), "", lector.Item("tie_Ubigeo")))
                    tienda.Dpto = CStr(IIf(IsDBNull(lector.Item("Departamento")), "", lector.Item("Departamento")))
                    tienda.Prov = CStr(IIf(IsDBNull(lector.Item("Provincia")), "", lector.Item("Provincia")))
                    tienda.Dist = CStr(IIf(IsDBNull(lector.Item("Distrito")), "", lector.Item("Distrito")))
                    tienda.Direccion = CStr(IIf(IsDBNull(lector.Item("tie_Direccion")), "", lector.Item("tie_Direccion")))
                    tienda.Referencia = CStr(IIf(IsDBNull(lector.Item("tie_Referencia")), "", lector.Item("tie_Referencia")))
                    tienda.Telefono = CStr(IIf(IsDBNull(lector.Item("tie_Telefono")), "", lector.Item("tie_Telefono")))
                    tienda.Fax = CStr(IIf(IsDBNull(lector.Item("tie_Fax")), "", lector.Item("tie_Fax")))
                    tienda.IdTipoTienda = CInt(IIf(IsDBNull(lector.Item("IdTipoTienda")), 0, lector.Item("IdTipoTienda")))
                    tienda.TipoTienda = CStr(IIf(IsDBNull(lector.Item("TipoTienda")), "", lector.Item("TipoTienda")))
                    tienda.Estado = CStr(IIf(IsDBNull(lector.Item("tie_Estado")), "", lector.Item("tie_Estado")))
                    tienda.Email = CStr(IIf(IsDBNull(lector.Item("tie_EMail")), "", lector.Item("tie_EMail")))
                    tienda.CtaDebeVenta = CStr(IIf(IsDBNull(lector.Item("tie_CtaDebeVenta")), "", lector.Item("tie_CtaDebeVenta")))
                    tienda.CtaHaberVenta = CStr(IIf(IsDBNull(lector.Item("tie_CtaHaberVenta")), "", lector.Item("tie_CtaHaberVenta")))

                    Lista.Add(tienda)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function






End Class
