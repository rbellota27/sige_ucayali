'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOTipoAgente
    Inherits DAOMantenedor

    Dim HDAO As New DAO.HelperDAO
    'Dim objConexion As New Conexion
    Public Function InsertaTipoAgente(ByVal obj As Entidades.TipoAgente) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Dim Prm() As SqlParameter = New SqlParameter(3) {}

        Prm(0) = getParam(obj.Nombre, "@ag_Nombre", SqlDbType.VarChar)
        Prm(1) = getParam(obj.Estado, "@ag_Estado", SqlDbType.Char)
        Prm(2) = getParam(obj.Tasa, "@ag_tasa", SqlDbType.Decimal)
        Prm(3) = getParam(obj.SujetoAPercepcion, "@ag_SujetoAPercepcion", SqlDbType.Bit)

        Return HDAO.Insert(cn, "_TipoAgenteInsert", Prm)
    End Function
    Public Function ActualizaTipoAgente(ByVal obj As Entidades.TipoAgente) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim Prm() As SqlParameter = New SqlParameter(4) {}

        Prm(0) = getParam(obj.Nombre, "@ag_Nombre", SqlDbType.VarChar)
        Prm(1) = getParam(obj.Estado, "@ag_Estado", SqlDbType.Char)
        Prm(2) = getParam(obj.Tasa, "@ag_tasa", SqlDbType.Decimal)
        Prm(3) = getParam(obj.SujetoAPercepcion, "@ag_SujetoAPercepcion", SqlDbType.Bit)
        Prm(4) = getParam(obj.IdAgente, "@IdAgente", SqlDbType.Int)

        Return HDAO.Update(cn, "_TipoAgenteUpdate", Prm)
    End Function
    Public Function SelectAll() As List(Of Entidades.TipoAgente)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoAgenteSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            Using cn
                cn.Open()
                Return getListTipoAgente(cmd.ExecuteReader(Data.CommandBehavior.CloseConnection))
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.TipoAgente)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoAgenteSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            Using cn
                cn.Open()
                Return getListTipoAgente(cmd.ExecuteReader(Data.CommandBehavior.CloseConnection))
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.TipoAgente)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoAgenteSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure

        Try
            Using cn
                cn.Open()
                Return getListTipoAgente(cmd.ExecuteReader(Data.CommandBehavior.CloseConnection))
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.TipoAgente)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoAgenteSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Try
            Using cn
                cn.Open()
                Return getListTipoAgente(cmd.ExecuteReader(Data.CommandBehavior.CloseConnection))
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoAgente)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoAgenteSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Try
            Using cn
                cn.Open()
                Return getListTipoAgente(cmd.ExecuteReader(Data.CommandBehavior.CloseConnection))
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoAgente)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoAgenteSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)

        Try
            Using cn
                cn.Open()

                Return getListTipoAgente(cmd.ExecuteReader(Data.CommandBehavior.CloseConnection))

            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.TipoAgente)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoAgenteSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Try
            Using cn
                cn.Open()
                Return getListTipoAgente(cmd.ExecuteReader(Data.CommandBehavior.CloseConnection))
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectCbo() As List(Of Entidades.TipoAgente)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoAgenteSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoAgente)
                Do While lector.Read
                    Dim obj As New Entidades.TipoAgente
                    obj.Nombre = CStr(lector.Item("ag_Nombre"))
                    obj.IdAgente = CInt(lector.Item("IdAgente"))
                    obj.SujetoAPercepcion = CBool(lector.Item("ag_SujetoAPercepcion"))
                    obj.SujetoADetraccion = CBool(lector.Item("ag_SujetoADetraccion"))
                    obj.SujetoARetencion = CBool(lector.Item("ag_SujetoRetencion"))
                    obj.Tasa = CDec(lector.Item("ag_tasa"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function getTipoAgente(ByVal lector As SqlDataReader) As Entidades.TipoAgente

        Dim Obj As New Entidades.TipoAgente
        With Obj

            .IdAgente = UCInt(lector("IdAgente"))
            .Nombre = UCStr(lector("ag_Nombre"))
            .Tasa = UCDec(lector("ag_tasa"))
            .SujetoAPercepcion = UCBool(lector("ag_SujetoAPercepcion"))
            .Estado = UCStr(lector("ag_Estado"))

        End With
        Return Obj
    End Function

    Public Function getListTipoAgente(ByRef lector As SqlDataReader) As List(Of Entidades.TipoAgente)
        Dim L As New List(Of Entidades.TipoAgente)
        Do While lector.Read
            L.Add(getTipoAgente(lector))
        Loop
        lector.Close()
        Return L
    End Function
End Class
