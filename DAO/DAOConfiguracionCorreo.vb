﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOConfiguracionCorreo

    Dim sql_cn As SqlConnection = (New Conexion).ConexionSIGE
    Dim sql_tr As SqlTransaction = Nothing
    Dim sql_reader As SqlDataReader = Nothing
    Dim sql_cmd As SqlCommand = Nothing
    Dim obj_ConfiguracionCorreo As Entidades.ConfiguracionCorreo
    Dim list_ConfiguracionCorreo As New List(Of Entidades.ConfiguracionCorreo)
    Dim Transaction As Integer = 0

    Public Function ConfiguracionCorreo_select(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal estado As Integer) As List(Of Entidades.ConfiguracionCorreo)
        sql_cmd = New SqlCommand("_ConfiguracionCorreo_select", sql_cn)
        sql_cmd.CommandType = CommandType.StoredProcedure
        sql_cmd.Parameters.AddWithValue("@IdEmpresa", IIf(IdEmpresa = Nothing, DBNull.Value, IdEmpresa))
        sql_cmd.Parameters.AddWithValue("@IdTienda", IIf(IdTienda = Nothing, DBNull.Value, IdTienda))
        sql_cmd.Parameters.AddWithValue("@Estado", IIf(estado = 2, DBNull.Value, estado))

        Try
            sql_cn.Open()
            sql_reader = sql_cmd.ExecuteReader(CommandBehavior.CloseConnection)

            Do While sql_reader.Read

                obj_ConfiguracionCorreo = New Entidades.ConfiguracionCorreo
                With obj_ConfiguracionCorreo
                    .IdConfiguracionCorreo = CInt(sql_reader("IdConfiguracionCorreo"))
                    .IdEmpresa = CInt(sql_reader("IdEmpresa"))
                    .NomEmpresa = CStr(IIf(IsDBNull(sql_reader("Empresa")), "", sql_reader("Empresa")))
                    .NomTienda = CStr(IIf(IsDBNull(sql_reader("Tienda")), "", sql_reader("Tienda")))
                    .IdTienda = CInt(sql_reader("IdTienda"))
                    .servidorCorreo = CStr(IIf(IsDBNull(sql_reader("servidorCorreo")), "", sql_reader("servidorCorreo")))
                    .PuertoServidorCorreo = CInt(IIf(IsDBNull(sql_reader("PuertoServidorCorreo")), 0, sql_reader("PuertoServidorCorreo")))
                    .HostInteligente = CStr(IIf(IsDBNull(sql_reader("HostInteligente")), "", sql_reader("HostInteligente")))
                    .UsarCredencial = CBool(IIf(IsDBNull(sql_reader("UsarCredencial")), False, sql_reader("UsarCredencial")))
                    .CuentaCorreo = CStr(IIf(IsDBNull(sql_reader("CuentaCorreo")), "", sql_reader("CuentaCorreo")))
                    .Clave = CStr(IIf(IsDBNull(sql_reader("Clave")), "", sql_reader("Clave")))
                    .Estado = CBool(IIf(IsDBNull(sql_reader("Estado")), False, sql_reader("Estado")))
                End With
                list_ConfiguracionCorreo.Add(obj_ConfiguracionCorreo)

            Loop

            sql_reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If sql_cn.State = ConnectionState.Open Then sql_cn.Close()
        End Try

        Return list_ConfiguracionCorreo

    End Function

    Public Function _ConfiguracionCorreoTransaction(ByVal obj As Entidades.ConfiguracionCorreo, ByVal getList As Boolean) As List(Of Entidades.ConfiguracionCorreo)
        Try
            sql_cn.Open()
            sql_tr = sql_cn.BeginTransaction(IsolationLevel.Serializable)
            sql_cmd = New SqlCommand("_ConfiguracionCorreo", sql_cn, sql_tr)
            sql_cmd.CommandType = CommandType.StoredProcedure

            sql_cmd.Parameters.AddWithValue("@IdConfiguracionCorreo", IIf(obj.IdConfiguracionCorreo = Nothing, DBNull.Value, obj.IdConfiguracionCorreo))
            sql_cmd.Parameters.AddWithValue("@IdEmpresa", IIf(obj.IdEmpresa = Nothing, DBNull.Value, obj.IdEmpresa))
            sql_cmd.Parameters.AddWithValue("@IdTienda", IIf(obj.IdTienda = Nothing, DBNull.Value, obj.IdTienda))
            sql_cmd.Parameters.AddWithValue("@servidorCorreo", IIf(obj.servidorCorreo = Nothing, DBNull.Value, obj.servidorCorreo))
            sql_cmd.Parameters.AddWithValue("@PuertoServidorCorreo", IIf(obj.PuertoServidorCorreo = Nothing, DBNull.Value, obj.PuertoServidorCorreo))
            sql_cmd.Parameters.AddWithValue("@HostInteligente", IIf(obj.HostInteligente = Nothing, DBNull.Value, obj.HostInteligente))
            sql_cmd.Parameters.AddWithValue("@UsarCredencial", IIf(obj.UsarCredencial = Nothing, DBNull.Value, obj.UsarCredencial))
            sql_cmd.Parameters.AddWithValue("@CuentaCorreo", IIf(obj.CuentaCorreo = Nothing, DBNull.Value, obj.CuentaCorreo))
            sql_cmd.Parameters.AddWithValue("@Clave", IIf(obj.Clave = Nothing, DBNull.Value, obj.Clave))
            sql_cmd.Parameters.AddWithValue("@Estado", obj.Estado)

            Transaction = sql_cmd.ExecuteNonQuery()

            sql_tr.Commit()
        Catch ex As Exception
            sql_tr.Rollback()
            Throw ex
        Finally
            If sql_cn.State = ConnectionState.Open Then sql_cn.Close()
        End Try

        If (getList) Then
            list_ConfiguracionCorreo = ConfiguracionCorreo_select(obj.IdEmpresa, Nothing, obj.Ver_Estado)
        End If

        Return list_ConfiguracionCorreo

    End Function



End Class
