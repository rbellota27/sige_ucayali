﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Nagamine Molina, Jorge
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 10-Noviembre-2009
'Hora    : 03:40 pm
'*************************************************




'*************************************************
'Autor   : Chang Carnero, Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 10-Noviembre-2009
'Hora    : 01:00 pm
'*************************************************
Imports System.Data.SqlClient
Public Class DAOPerfil_Permiso
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Private ListaPermiso As List(Of Entidades.Perfil)

    Public Sub InsertaPerfil_Permiso(ByVal objPerfil_Permiso As Entidades.Perfil_Permiso, ByVal IdPerfil As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)


        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@IdPerfil", SqlDbType.Int)
        ArrayParametros(0).Value = objPerfil_Permiso.IdPerfil
        ArrayParametros(1) = New SqlParameter("@IdPermiso", SqlDbType.Int)
        ArrayParametros(1).Value = objPerfil_Permiso.IdPermiso
        HDAO.InsertaT(cn, "_Perfil_PermisoInsert", ArrayParametros, tr)

    End Sub
    Public Sub DeletexIdPerfilxIdArea(ByVal IdPerfil As Integer, ByVal IdArea As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Dim cmd As New SqlCommand("_Perfil_PermisoDeletexIdPerfilxIdArea", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idPerfil", IdPerfil)
        cmd.Parameters.AddWithValue("@IdArea", IdArea)
        cmd.ExecuteNonQuery()
    End Sub
    'Public Function DeletexIdMotivoT(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal idTraslado As Integer) As Boolean
    '    Try

    '        Dim cmd As New SqlCommand("_Perfil_PermisoBorrar", cn, tr)
    '        cmd.CommandType = CommandType.StoredProcedure
    '        cmd.Parameters.AddWithValue("@idTraslado", idTraslado)
    '        cmd.Parameters.AddWithValue("@IdOficina", IdOficina)
    '        cmd.ExecuteNonQuery()

    '        Return True
    '    Catch ex As Exception
    '        Return False
    '    End Try

    'End Function
End Class
