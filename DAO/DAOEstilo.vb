﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOEstilo
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaEstilo(ByVal estilo As Entidades.Estilo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@est_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = estilo.Descripcion
        ArrayParametros(1) = New SqlParameter("@est_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = estilo.Abv
        ArrayParametros(2) = New SqlParameter("@est_Codigo", SqlDbType.Char, 2)
        ArrayParametros(2).Value = estilo.Est_Codigo
        ArrayParametros(3) = New SqlParameter("@est_Estado", SqlDbType.Bit)
        ArrayParametros(3).Value = estilo.EstadoBit
        Dim cmd As New SqlCommand("_EstiloInsert", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return True
    End Function
    Public Function ActualizaEstilo(ByVal estilo As Entidades.Estilo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@est_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = estilo.Descripcion
        ArrayParametros(1) = New SqlParameter("@est_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = estilo.Abv

        ArrayParametros(2) = New SqlParameter("@est_Codigo", SqlDbType.Char, 2)
        ArrayParametros(2).Value = estilo.Est_Codigo

        ArrayParametros(3) = New SqlParameter("@est_Estado", SqlDbType.Bit)
        ArrayParametros(3).Value = estilo.EstadoBit
        ArrayParametros(4) = New SqlParameter("@IdEstilo", SqlDbType.Int)
        ArrayParametros(4).Value = estilo.Id
        Return HDAO.Update(cn, "_EstiloUpdate", ArrayParametros)
    End Function
    Public Function SelectAll() As List(Of Entidades.Estilo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstiloSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Estilo)
                Do While lector.Read
                    Dim objEstilo As New Entidades.Estilo
                    objEstilo.Abv = CStr(IIf(IsDBNull(lector.Item("est_Abv")) = True, "", lector.Item("est_Abv")))
                    objEstilo.Descripcion = CStr(IIf(IsDBNull(lector.Item("est_Nombre")) = True, "", lector.Item("est_Nombre")))
                    objEstilo.Estado = CStr(IIf(IsDBNull(lector.Item("est_Estado")) = True, "", lector.Item("est_Estado")))
                    objEstilo.Id = CInt(IIf(IsDBNull(lector.Item("IdEstilo")) = True, 0, lector.Item("IdEstilo")))
                    objEstilo.Est_Codigo = CStr(iif(IsDBNull(lector.Item("est_Codigo")) = True, "", lector.Item("est_Codigo")))
                    Lista.Add(objEstilo)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Estilo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstiloSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Estilo)
                Do While lector.Read
                    Dim objEstilo As New Entidades.Estilo
                    objEstilo.Abv = CStr(IIf(IsDBNull(lector.Item("est_Abv")) = True, "", lector.Item("est_Abv")))
                    objEstilo.Descripcion = CStr(IIf(IsDBNull(lector.Item("est_Nombre")) = True, "", lector.Item("est_Nombre")))
                    objEstilo.Estado = CStr(IIf(IsDBNull(lector.Item("est_Estado")) = True, "", lector.Item("est_Estado")))
                    objEstilo.Id = CInt(IIf(IsDBNull(lector.Item("IdEstilo")) = True, 0, lector.Item("IdEstilo")))
                    objEstilo.Est_Codigo = CStr(IIf(IsDBNull(lector.Item("est_Codigo")) = True, "", lector.Item("est_Codigo")))
                    Lista.Add(objEstilo)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Estilo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstiloSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Estilo)
                Do While lector.Read
                    Dim objEstilo As New Entidades.Estilo
                    objEstilo.Abv = CStr(IIf(IsDBNull(lector.Item("est_Abv")) = True, "", lector.Item("est_Abv")))
                    objEstilo.Descripcion = CStr(IIf(IsDBNull(lector.Item("est_Nombre")) = True, "", lector.Item("est_Nombre")))
                    objEstilo.Estado = CStr(IIf(IsDBNull(lector.Item("est_Estado")) = True, "", lector.Item("est_Estado")))
                    objEstilo.Id = CInt(IIf(IsDBNull(lector.Item("IdEstilo")) = True, 0, lector.Item("IdEstilo")))
                    objEstilo.Est_Codigo = CStr(IIf(IsDBNull(lector.Item("est_Codigo")) = True, "", lector.Item("est_Codigo")))
                    Lista.Add(objEstilo)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Estilo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstiloSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Estilo)
                Do While lector.Read
                    Dim objEstilo As New Entidades.Estilo
                    objEstilo.Abv = CStr(IIf(IsDBNull(lector.Item("est_Abv")) = True, "", lector.Item("est_Abv")))
                    objEstilo.Descripcion = CStr(IIf(IsDBNull(lector.Item("est_Nombre")) = True, "", lector.Item("est_Nombre")))
                    objEstilo.Estado = CStr(IIf(IsDBNull(lector.Item("est_Estado")) = True, "", lector.Item("est_Estado")))
                    objEstilo.Id = CInt(IIf(IsDBNull(lector.Item("IdEstilo")) = True, 0, lector.Item("IdEstilo")))
                    Lista.Add(objEstilo)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Estilo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstiloSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Estilo)
                Do While lector.Read
                    Dim objEstilo As New Entidades.Estilo
                    objEstilo.Abv = CStr(IIf(IsDBNull(lector.Item("est_Abv")) = True, "", lector.Item("est_Abv")))
                    objEstilo.Descripcion = CStr(IIf(IsDBNull(lector.Item("est_Nombre")) = True, "", lector.Item("est_Nombre")))
                    objEstilo.Estado = CStr(IIf(IsDBNull(lector.Item("est_Estado")) = True, "", lector.Item("est_Estado")))
                    objEstilo.Id = CInt(IIf(IsDBNull(lector.Item("IdEstilo")) = True, 0, lector.Item("IdEstilo")))
                    objEstilo.Est_Codigo = CStr(IIf(IsDBNull(lector.Item("est_Codigo")) = True, "", lector.Item("est_Codigo")))
                    Lista.Add(objEstilo)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Estilo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstiloSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Estilo)
                Do While lector.Read
                    Dim objEstilo As New Entidades.Estilo
                    objEstilo.Abv = CStr(IIf(IsDBNull(lector.Item("est_Abv")) = True, "", lector.Item("est_Abv")))
                    objEstilo.Descripcion = CStr(IIf(IsDBNull(lector.Item("est_Nombre")) = True, "", lector.Item("est_Nombre")))
                    objEstilo.Estado = CStr(IIf(IsDBNull(lector.Item("est_Estado")) = True, "", lector.Item("est_Estado")))
                    objEstilo.Id = CInt(IIf(IsDBNull(lector.Item("IdEstilo")) = True, 0, lector.Item("IdEstilo")))
                    objEstilo.Est_Codigo = CStr(IIf(IsDBNull(lector.Item("est_Codigo")) = True, "", lector.Item("est_Codigo")))
                    Lista.Add(objEstilo)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Estilo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstiloSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Estilo)
                Do While lector.Read
                    Dim objEstilo As New Entidades.Estilo
                    objEstilo.Abv = CStr(IIf(IsDBNull(lector.Item("est_Abv")) = True, "", lector.Item("est_Abv")))
                    objEstilo.Descripcion = CStr(IIf(IsDBNull(lector.Item("est_Nombre")) = True, "", lector.Item("est_Nombre")))
                    objEstilo.Estado = CStr(IIf(IsDBNull(lector.Item("est_Estado")) = True, "", lector.Item("est_Estado")))
                    objEstilo.Id = CInt(IIf(IsDBNull(lector.Item("IdEstilo")) = True, 0, lector.Item("IdEstilo")))
                    objEstilo.Est_Codigo = CStr(IIf(IsDBNull(lector.Item("est_Codigo")) = True, "", lector.Item("est_Codigo")))
                    Lista.Add(objEstilo)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

End Class
