'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAORecibo
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaRecibo(ByVal recibo As Entidades.Recibo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(10) {}
        ArrayParametros(0) = New SqlParameter("@re_Codigo", SqlDbType.VarChar)
        ArrayParametros(0).Value = recibo.Codigo
        ArrayParametros(1) = New SqlParameter("@re_Serie", SqlDbType.VarChar)
        ArrayParametros(1).Value = recibo.Serie
        ArrayParametros(2) = New SqlParameter("@re_Fecha", SqlDbType.DateTime)
        ArrayParametros(2).Value = recibo.Fecha
        ArrayParametros(3) = New SqlParameter("@re_Total", SqlDbType.Decimal)
        ArrayParametros(3).Value = recibo.Total
        ArrayParametros(4) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        ArrayParametros(4).Value = recibo.IdMoneda
        ArrayParametros(5) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(5).Value = recibo.IdTienda
        ArrayParametros(6) = New SqlParameter("@IdSerie", SqlDbType.Int)
        ArrayParametros(6).Value = recibo.IdSerie
        ArrayParametros(7) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(7).Value = recibo.IdEmpresa
        ArrayParametros(8) = New SqlParameter("@IdEstadoDoc", SqlDbType.Int)
        ArrayParametros(8).Value = recibo.IdEstadoDoc
        ArrayParametros(9) = New SqlParameter("@IdTipoMovimiento", SqlDbType.Int)
        ArrayParametros(9).Value = recibo.IdTipoMovimiento
        ArrayParametros(10) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        ArrayParametros(10).Value = recibo.IdUsuario
        Return HDAO.Insert(cn, "_ReciboInsert", ArrayParametros)
    End Function
    Public Function ActualizaRecibo(ByVal recibo As Entidades.Recibo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(11) {}
        ArrayParametros(0) = New SqlParameter("@re_Codigo", SqlDbType.VarChar)
        ArrayParametros(0).Value = recibo.Codigo
        ArrayParametros(1) = New SqlParameter("@re_Serie", SqlDbType.VarChar)
        ArrayParametros(1).Value = recibo.Serie
        ArrayParametros(2) = New SqlParameter("@re_Fecha", SqlDbType.DateTime)
        ArrayParametros(2).Value = recibo.Fecha
        ArrayParametros(3) = New SqlParameter("@re_Total", SqlDbType.Decimal)
        ArrayParametros(3).Value = recibo.Total
        ArrayParametros(4) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        ArrayParametros(4).Value = recibo.IdMoneda
        ArrayParametros(5) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(5).Value = recibo.IdTienda
        ArrayParametros(6) = New SqlParameter("@IdSerie", SqlDbType.Int)
        ArrayParametros(6).Value = recibo.IdSerie
        ArrayParametros(7) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(7).Value = recibo.IdEmpresa
        ArrayParametros(8) = New SqlParameter("@IdEstadoDoc", SqlDbType.Int)
        ArrayParametros(8).Value = recibo.IdEstadoDoc
        ArrayParametros(9) = New SqlParameter("@IdTipoMovimiento", SqlDbType.Int)
        ArrayParametros(9).Value = recibo.IdTipoMovimiento
        ArrayParametros(10) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        ArrayParametros(10).Value = recibo.IdUsuario
        ArrayParametros(11) = New SqlParameter("@IdRecibo", SqlDbType.Int)
        ArrayParametros(11).Value = recibo.Id
        Return HDAO.Update(cn, "_ReciboUpdate", ArrayParametros)
    End Function
End Class
