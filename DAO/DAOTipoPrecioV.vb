'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'*****************  VIERNES 22 ENERO 2010 HORA 03_54 PM

Imports System.Data.SqlClient

Public Class DAOTipoPrecioV
    Inherits DAOMantenedor

    Dim HDAO As New DAO.HelperDAO
    'Dim objConexion As New Conexion

    Public Function SelectIdTipoPrecioVDefault() As Integer

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim IdTipoPV As Integer = 0
        Try

            Dim cmd As New SqlCommand("select dbo.fx_getIdTipoPrecioPVDefault()", cn)
            cmd.CommandType = CommandType.Text
            cn.Open()
            IdTipoPV = CInt(cmd.ExecuteScalar)
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try
        Return IdTipoPV

    End Function

    Public Function InsertaTipoPrecioV(ByVal obj As Entidades.TipoPrecioV) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim Prm() As SqlParameter = New SqlParameter(5) {}

        Prm(0) = getParam(obj.Nombre, "@pv_Nombre", SqlDbType.VarChar)
        Prm(1) = getParam(obj.Abv, "@pv_Abv", SqlDbType.VarChar)
        Prm(2) = getParam(obj.Estado, "@pv_Estado", SqlDbType.Char)
        Prm(3) = getParam(obj.TipoPvDefault, "@pv_Default", SqlDbType.Bit)
        Prm(4) = getParam(obj.IdTipoPvPredecesor, "@IdTipoPVRef", SqlDbType.Int)
        Prm(5) = getParam(obj.AfectoCargoTarjeta, "@pv_AfectoCargoTarjeta", SqlDbType.Bit)

        Return HDAO.Insert(cn, "_TipoPrecioVInsert", Prm)
    End Function

    Public Function ActualizaTipoPrecioV(ByVal obj As Entidades.TipoPrecioV) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Dim Prm() As SqlParameter = New SqlParameter(6) {}

        Prm(0) = getParam(obj.Nombre, "@pv_Nombre", SqlDbType.VarChar)
        Prm(1) = getParam(obj.Abv, "@pv_Abv", SqlDbType.VarChar)
        Prm(2) = getParam(obj.Estado, "@pv_Estado", SqlDbType.Char)
        Prm(3) = getParam(obj.TipoPvDefault, "@pv_Default", SqlDbType.Bit)
        Prm(4) = getParam(obj.IdTipoPvPredecesor, "@IdTipoPVRef", SqlDbType.Int)
        Prm(5) = getParam(obj.AfectoCargoTarjeta, "@pv_AfectoCargoTarjeta", SqlDbType.Bit)
        Prm(6) = getParam(obj.IdTipoPv, "@IdTipoPv", SqlDbType.Int)
      
        Return HDAO.Update(cn, "_TipoPrecioVUpdate", Prm)
    End Function

    Public Function getTipoPrecioV(ByVal lector As SqlDataReader) As Entidades.TipoPrecioV

        Dim Obj As New Entidades.TipoPrecioV
        With Obj

            .IdTipoPv = UCInt(lector("IdTipoPv"))
            .Nombre = UCStr(lector("pv_Nombre"))
            .Abv = UCStr(lector("pv_Abv"))
            .Estado = UCStr(lector("pv_Estado"))

            .TipoPvDefault = UCBool(lector("pv_Default"))
            .IdTipoPvPredecesor = UCInt(lector("IdTipoPVRef"))
            .NombrePredecesor = UCStr(lector("pv_NombreRef"))
            .AfectoCargoTarjeta = UCBool(lector("pv_AfectoCargoTarjeta"))

        End With
        Return Obj
    End Function

    Public Function getListTipoPrecioV(ByRef lector As SqlDataReader) As List(Of Entidades.TipoPrecioV)
        Dim L As New List(Of Entidades.TipoPrecioV)
        Do While lector.Read
            L.Add(getTipoPrecioV(lector))
        Loop
        lector.Close()
        Return L
    End Function

    Public Function SelectAll() As List(Of Entidades.TipoPrecioV)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoPrecioVSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure

        Try
            Using cn
                cn.Open()
                Return getListTipoPrecioV(cmd.ExecuteReader(Data.CommandBehavior.CloseConnection))
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.TipoPrecioV)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoPrecioVSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure

        Try
            Using cn
                cn.Open()
                Return getListTipoPrecioV(cmd.ExecuteReader(Data.CommandBehavior.CloseConnection))
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.TipoPrecioV)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoPrecioVSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure

        Try
            Using cn
                cn.Open()
                Return getListTipoPrecioV(cmd.ExecuteReader(Data.CommandBehavior.CloseConnection))
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.TipoPrecioV)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoPrecioVSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)

        Try
            Using cn
                cn.Open()
                Return getListTipoPrecioV(cmd.ExecuteReader(Data.CommandBehavior.CloseConnection))
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoPrecioV)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoPrecioVSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)

        Try
            Using cn
                cn.Open()
                Return getListTipoPrecioV(cmd.ExecuteReader(Data.CommandBehavior.CloseConnection))
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoPrecioV)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoPrecioVSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)

        Try
            Using cn
                cn.Open()
                Return getListTipoPrecioV(cmd.ExecuteReader(Data.CommandBehavior.CloseConnection))
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.TipoPrecioV)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoPrecioVSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)

        Try
            Using cn
                cn.Open()
                Return getListTipoPrecioV(cmd.ExecuteReader(Data.CommandBehavior.CloseConnection))
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCbo() As List(Of Entidades.TipoPrecioV)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoPrecioVSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoPrecioV)
                Do While lector.Read
                    Dim TipoPrecioV As New Entidades.TipoPrecioV
                    TipoPrecioV.IdTipoPv = CInt(lector.Item("IdTipoPV"))
                    TipoPrecioV.Nombre = CStr(lector.Item("pv_Nombre"))
                    TipoPrecioV.Estado = CStr(lector.Item("pv_Estado"))
                    Lista.Add(TipoPrecioV)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectCbo2() As List(Of Entidades.TipoPrecioV)
        Dim cn As SqlConnection = objConexion.ConexionSIGE2
        Dim cmd As New SqlCommand("_TipoPrecioVSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoPrecioV)
                Do While lector.Read
                    Dim TipoPrecioV As New Entidades.TipoPrecioV
                    TipoPrecioV.IdTipoPv = CInt(lector.Item("IdTipoPV"))
                    TipoPrecioV.Nombre = CStr(lector.Item("pv_Nombre"))
                    TipoPrecioV.Estado = CStr(lector.Item("pv_Estado"))
                    Lista.Add(TipoPrecioV)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCbo1() As List(Of Entidades.TipoPrecioV)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoPrecioVSelectCbo1", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoPrecioV)
                Do While lector.Read
                    Dim TipoPrecioV As New Entidades.TipoPrecioV
                    TipoPrecioV.IdTipoPv = CInt(lector.Item("IdTipoPV"))
                    TipoPrecioV.Nombre = CStr(lector.Item("pv_Nombre"))
                    TipoPrecioV.Estado = CStr(lector.Item("pv_Estado"))
                    Lista.Add(TipoPrecioV)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
