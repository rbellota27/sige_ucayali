'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOPersonaTipoAgente
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

    Public Function PersonaTipoAgente_Select(ByVal IdPersona As Integer) As List(Of Entidades.PersonaTipoAgente)
        Dim Lista_PersonaTipoAgente As New List(Of Entidades.PersonaTipoAgente)
        Dim obj_PersonaTipoAgente As Entidades.PersonaTipoAgente = Nothing
        Dim reader As SqlDataReader = Nothing
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PersonaTipoAgente_Select", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Try
            cn.Open()
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While reader.Read
                obj_PersonaTipoAgente = New Entidades.PersonaTipoAgente
                With obj_PersonaTipoAgente
                    .IdAgente = CInt(reader("IdAgente"))
                    .Descripcion = CStr(IIf(IsDBNull(reader("ag_Nombre")), "", reader("ag_Nombre")))
                    .Tasa = CDec(IIf(IsDBNull(reader("ag_tasa")), Decimal.Zero, reader("ag_tasa")))
                    .SujetoAPercepcion = CBool(IIf(IsDBNull(reader("ag_SujetoAPercepcion")), False, reader("ag_SujetoAPercepcion")))
                    .SujetoARetencion = CBool(IIf(IsDBNull(reader("ag_SujetoRetencion")), False, reader("ag_SujetoRetencion")))
                End With
                Lista_PersonaTipoAgente.Add(obj_PersonaTipoAgente)
            Loop
            reader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista_PersonaTipoAgente
    End Function

    Public Sub InsertaPersonaTipoAgente(ByVal cn As SqlConnection, ByVal personatipoagente As Entidades.PersonaTipoAgente, ByVal T As SqlTransaction)
        'Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(0).Value = personatipoagente.IdPersona
        ArrayParametros(1) = New SqlParameter("@IdAgente", SqlDbType.Int)
        ArrayParametros(1).Value = personatipoagente.IdAgente
        HDAO.InsertaT(cn, "_PersonaTipoAgenteInsert", ArrayParametros, T)
    End Sub
    Public Function SelectxId(ByVal IdPersona As Integer) As Entidades.PersonaTipoAgente
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PersonaTipoAgenteSelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Using cn
            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
            Dim objPersonaTipoAgente As New Entidades.PersonaTipoAgente
            If lector.Read Then
                objPersonaTipoAgente = New Entidades.PersonaTipoAgente
                objPersonaTipoAgente.IdAgente = CInt(IIf(IsDBNull(lector.Item("IdAgente")) = True, 0, lector.Item("IdAgente")))
                objPersonaTipoAgente.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
            End If
            lector.Close()
            Return objPersonaTipoAgente
        End Using
    End Function
    Public Sub ActualizaPersonaTipoAgenteT(ByVal cn As SqlConnection, ByVal agente As Entidades.PersonaTipoAgente, ByVal T As SqlTransaction)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(agente.IdPersona = Nothing, DBNull.Value, agente.IdPersona)
        ArrayParametros(1) = New SqlParameter("@IdAgente", SqlDbType.Int)
        ArrayParametros(1).Value = IIf(agente.IdAgente = Nothing, DBNull.Value, agente.IdAgente)

        HDAO.UpdateT(cn, "_PersonaTipoAgenteUpdate", ArrayParametros, T)
    End Sub
    Public Function SelectIdAgenteTasaxIdPersona(ByVal IdPersona As Integer) As Entidades.TipoAgente
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PersonaTipoAgenteSelectIdAgenteTasaxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                lector.Read()
                Dim obj As New Entidades.TipoAgente
                If lector.HasRows = False Then
                    obj.IdAgente = 0
                    obj.Tasa = CDec(0)
                    Return obj
                End If
                obj.IdAgente = CInt(IIf(IsDBNull(lector.Item("IdAgente")) = True, 0, lector.Item("IdAgente")))
                obj.Tasa = CDec(IIf(IsDBNull(lector.Item("ag_tasa")) = True, 0, lector.Item("ag_tasa")))
                Return obj
            End Using

        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
