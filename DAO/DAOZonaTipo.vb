'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOZonaTipo
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaZonaTipo(ByVal zonatipo As Entidades.ZonaTipo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@zt_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = zonatipo.Nombre
        ArrayParametros(1) = New SqlParameter("@zt_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = zonatipo.Abv
        ArrayParametros(2) = New SqlParameter("@zt_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = zonatipo.Estado
        Return HDAO.Insert(cn, "_ZonaTipoInsert", ArrayParametros)
    End Function
    Public Function ActualizaZonaTipo(ByVal zonatipo As Entidades.ZonaTipo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@zt_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = zonatipo.Nombre
        ArrayParametros(1) = New SqlParameter("@zt_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = zonatipo.Abv
        ArrayParametros(2) = New SqlParameter("@zt_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = zonatipo.Estado
        ArrayParametros(3) = New SqlParameter("@IdZonaTipo", SqlDbType.Int)
        ArrayParametros(3).Value = zonatipo.Id
        Return HDAO.Update(cn, "_ZonaTipoUpdate", ArrayParametros)
    End Function

    Public Function SelectCbo() As List(Of Entidades.ZonaTipo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ZonaTipoSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ZonaTipo)
                Do While lector.Read
                    Dim ZonaTipo As New Entidades.ZonaTipo
                    ZonaTipo.Id = CInt(lector.Item("IdZonaTipo"))
                    ZonaTipo.Nombre = CStr(lector.Item("zt_Nombre"))
                    ZonaTipo.Abv = CStr(lector.Item("zt_Abv"))
                    Lista.Add(ZonaTipo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.ZonaTipo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ZonaTipoSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ZonaTipo)
                Do While lector.Read
                    Dim ZonaTipo As New Entidades.ZonaTipo
                    ZonaTipo.Id = CInt(lector.Item("IdZonaTipo"))
                    ZonaTipo.Nombre = CStr(lector.Item("zt_Nombre"))
                    ZonaTipo.Abv = CStr(IIf(IsDBNull(lector.Item("zt_Abv")) = True, "", lector.Item("zt_Abv")))
                    ZonaTipo.Estado = CStr(IIf(IsDBNull(lector.Item("zt_Estado")) = True, "", lector.Item("zt_Estado")))
                    Lista.Add(ZonaTipo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.ZonaTipo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ZonaTipoSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ZonaTipo)
                Do While lector.Read
                    Dim ZonaTipo As New Entidades.ZonaTipo
                    ZonaTipo.Id = CInt(lector.Item("IdZonaTipo"))
                    ZonaTipo.Nombre = CStr(lector.Item("zt_Nombre"))
                    ZonaTipo.Abv = CStr(IIf(IsDBNull(lector.Item("zt_Abv")) = True, "", lector.Item("zt_Abv")))
                    ZonaTipo.Estado = CStr(IIf(IsDBNull(lector.Item("zt_Estado")) = True, "", lector.Item("zt_Estado")))
                    Lista.Add(ZonaTipo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.ZonaTipo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ZonaTipoSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ZonaTipo)
                Do While lector.Read
                    Dim ZonaTipo As New Entidades.ZonaTipo
                    ZonaTipo.Id = CInt(lector.Item("IdZonaTipo"))
                    ZonaTipo.Nombre = CStr(lector.Item("zt_Nombre"))
                    ZonaTipo.Abv = CStr(IIf(IsDBNull(lector.Item("zt_Abv")) = True, "", lector.Item("zt_Abv")))
                    ZonaTipo.Estado = CStr(IIf(IsDBNull(lector.Item("zt_Estado")) = True, "", lector.Item("zt_Estado")))
                    Lista.Add(ZonaTipo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.ZonaTipo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ZonaTipoSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ZonaTipo)
                Do While lector.Read
                    Dim ZonaTipo As New Entidades.ZonaTipo
                    ZonaTipo.Id = CInt(lector.Item("IdZonaTipo"))
                    ZonaTipo.Nombre = CStr(lector.Item("zt_Nombre"))
                    ZonaTipo.Abv = CStr(IIf(IsDBNull(lector.Item("zt_Abv")) = True, "", lector.Item("zt_Abv")))
                    ZonaTipo.Estado = CStr(IIf(IsDBNull(lector.Item("zt_Estado")) = True, "", lector.Item("zt_Estado")))
                    Lista.Add(ZonaTipo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.ZonaTipo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ZonaTipoSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ZonaTipo)
                Do While lector.Read
                    Dim ZonaTipo As New Entidades.ZonaTipo
                    ZonaTipo.Id = CInt(lector.Item("IdZonaTipo"))
                    ZonaTipo.Nombre = CStr(lector.Item("zt_Nombre"))
                    ZonaTipo.Abv = CStr(IIf(IsDBNull(lector.Item("zt_Abv")) = True, "", lector.Item("zt_Abv")))
                    ZonaTipo.Estado = CStr(IIf(IsDBNull(lector.Item("zt_Estado")) = True, "", lector.Item("zt_Estado")))
                    Lista.Add(ZonaTipo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.ZonaTipo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ZonaTipoSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ZonaTipo)
                Do While lector.Read
                    Dim ZonaTipo As New Entidades.ZonaTipo
                    ZonaTipo.Id = CInt(lector.Item("IdZonaTipo"))
                    ZonaTipo.Nombre = CStr(lector.Item("zt_Nombre"))
                    ZonaTipo.Abv = CStr(IIf(IsDBNull(lector.Item("zt_Abv")) = True, "", lector.Item("zt_Abv")))
                    ZonaTipo.Estado = CStr(IIf(IsDBNull(lector.Item("zt_Estado")) = True, "", lector.Item("zt_Estado")))
                    Lista.Add(ZonaTipo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.ZonaTipo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ZonaTipoSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ZonaTipo)
                Do While lector.Read
                    Dim ZonaTipo As New Entidades.ZonaTipo
                    ZonaTipo.Id = CInt(lector.Item("IdZonaTipo"))
                    ZonaTipo.Nombre = CStr(lector.Item("zt_Nombre"))
                    ZonaTipo.Abv = CStr(IIf(IsDBNull(lector.Item("zt_Abv")) = True, "", lector.Item("zt_Abv")))
                    ZonaTipo.Estado = CStr(IIf(IsDBNull(lector.Item("zt_Estado")) = True, "", lector.Item("zt_Estado")))
                    Lista.Add(ZonaTipo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

End Class
