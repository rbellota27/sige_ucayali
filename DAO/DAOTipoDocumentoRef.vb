﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOTipoDocumentoRef
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Dim cn As SqlConnection = objConexion.ConexionSIGE

    Public Function InsertaTipoDocumentoRef(ByVal TipoDocumentoRef As Entidades.TipoDocumentoRef) As Boolean

        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@IdTipoDocumento", SqlDbType.VarChar)
        ArrayParametros(0).Value = TipoDocumentoRef.IdTipoDoc
        ArrayParametros(1) = New SqlParameter("@IdTipoDocumentoRef", SqlDbType.VarChar)
        ArrayParametros(1).Value = TipoDocumentoRef.IdTipoDocRef

        Return HDAO.Insert(cn, "_TipoDocumentoRefInsert", ArrayParametros)

    End Function

    Public Function DeleteTipoDocumentoRef(ByVal TipoDocumentoRef As Entidades.TipoDocumentoRef) As Boolean

        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@IdTipoDocumento", SqlDbType.VarChar)
        ArrayParametros(0).Value = TipoDocumentoRef.IdTipoDoc
        ArrayParametros(1) = New SqlParameter("@IdTipoDocumentoRef", SqlDbType.VarChar)
        ArrayParametros(1).Value = TipoDocumentoRef.IdTipoDocRef

        Return HDAO.DeletexParams(cn, "_TipoDocumentoRefDelete", ArrayParametros)

    End Function
    Public Function SelectxIdTipoDoc(ByVal IdTipoDoc As Integer) As List(Of Entidades.TipoDocumentoRef)

        Dim cmd As New SqlCommand("_TipoDocumentoRefSelectxIdDoc", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDoc)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoDocumentoRef)
                Do While lector.Read
                    Dim TipoDocumentoRef As New Entidades.TipoDocumentoRef
                    TipoDocumentoRef.IdTipoDoc = CInt(lector.Item("IdTipoDocumento"))
                    TipoDocumentoRef.IdTipoDocRef = CInt(lector.Item("IdTipoDocumentoRef"))
                    Lista.Add(TipoDocumentoRef)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxIdtipoDocRef(ByVal idTipoDocumento As Integer) As List(Of Entidades.TipoDocumento)
        Dim Lista As New List(Of Entidades.TipoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SelectxIdtipoDocRef", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoDocumento", idTipoDocumento)
        Dim lector As SqlDataReader
        Try
            cn.Open()
            lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While lector.Read
                Dim obj As New Entidades.TipoDocumento
                With obj
                    .Id = CInt(lector("IdTipoDocumentoRef"))
                    .Descripcion = CStr(lector("tdoc_NombreLargo"))
                    .DescripcionCorto = CStr(lector("tdoc_NombreCorto"))
                End With
                Lista.Add(obj)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function
        
End Class
