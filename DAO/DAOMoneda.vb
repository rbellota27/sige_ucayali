'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.


'********************   JUEVES 10 JUNIO 2010 HORA 12_46 PM

Imports System.Data.SqlClient
Public Class DAOMoneda
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

    Private objDaoMantenedor As New DAO.DAOMantenedor

    Public Function InsertaMoneda(ByVal moneda As Entidades.Moneda) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@mon_CodigoSunat", SqlDbType.Char)
        ArrayParametros(0).Value = moneda.CodigoSunat
        ArrayParametros(1) = New SqlParameter("@mon_Nombre", SqlDbType.VarChar)
        ArrayParametros(1).Value = moneda.Descripcion
        ArrayParametros(2) = New SqlParameter("@mon_Simbolo", SqlDbType.Char)
        ArrayParametros(2).Value = moneda.Simbolo
        ArrayParametros(3) = New SqlParameter("@mon_Base", SqlDbType.Bit)
        ArrayParametros(3).Value = moneda.Base
        ArrayParametros(4) = New SqlParameter("@mon_Estado", SqlDbType.Char)
        ArrayParametros(4).Value = moneda.Estado
        Return HDAO.Insert(cn, "_MonedaInsert", ArrayParametros)
    End Function
    Public Function ActualizaMoneda(ByVal moneda As Entidades.Moneda) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
        ArrayParametros(0) = New SqlParameter("@mon_CodigoSunat", SqlDbType.Char)
        ArrayParametros(0).Value = moneda.CodigoSunat
        ArrayParametros(1) = New SqlParameter("@mon_Nombre", SqlDbType.VarChar)
        ArrayParametros(1).Value = moneda.Descripcion
        ArrayParametros(2) = New SqlParameter("@mon_Simbolo", SqlDbType.Char)
        ArrayParametros(2).Value = moneda.Simbolo
        ArrayParametros(3) = New SqlParameter("@mon_Base", SqlDbType.Bit)
        ArrayParametros(3).Value = moneda.Base
        ArrayParametros(4) = New SqlParameter("@mon_Estado", SqlDbType.Char)
        ArrayParametros(4).Value = moneda.Estado
        ArrayParametros(5) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        ArrayParametros(5).Value = moneda.Id
        Return HDAO.Update(cn, "_MonedaUpdate", ArrayParametros)
    End Function
    Public Function SelectCboNoBase() As List(Of Entidades.Moneda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MonedaSelectCboNoBase", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Moneda)
                Do While lector.Read
                    Dim Moneda As New Entidades.Moneda
                    Moneda.Id = CInt(lector.Item("IdMoneda"))
                    Moneda.Simbolo = CStr(lector.Item("mon_Simbolo"))
                    Lista.Add(Moneda)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCbo() As List(Of Entidades.Moneda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MonedaSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Moneda)
                Do While lector.Read
                    Dim Moneda As New Entidades.Moneda
                    Moneda.Id = CInt(lector.Item("IdMoneda"))
                    Moneda.Simbolo = CStr(lector.Item("mon_Simbolo"))
                    Lista.Add(Moneda)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectCboMonedaBase() As List(Of Entidades.Moneda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MonedaSelectCboMonedaBase", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Moneda)
                Do While lector.Read
                    Dim Moneda As New Entidades.Moneda
                    Moneda.Id = CInt(lector.Item("IdMoneda"))
                    Moneda.Simbolo = CStr(lector.Item("mon_Simbolo"))
                    Lista.Add(Moneda)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAll() As List(Of Entidades.Moneda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MonedaSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Moneda)
                Do While lector.Read
                    Dim Moneda As New Entidades.Moneda
                    Moneda.Id = CInt(lector.Item("IdMoneda"))
                    Moneda.Simbolo = CStr(lector.Item("mon_Simbolo"))
                    Moneda.Base = CBool(lector.Item("mon_Base"))
                    Moneda.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("mon_CodigoSunat")) = True, "", lector.Item("mon_CodigoSunat")))
                    Moneda.Descripcion = CStr(IIf(IsDBNull(lector.Item("mon_Nombre")) = True, "", lector.Item("mon_Nombre")))
                    Moneda.Estado = CStr(IIf(IsDBNull(lector.Item("mon_Estado")) = True, "", lector.Item("mon_Estado")))
                    Lista.Add(Moneda)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Moneda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MonedaSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Moneda)
                Do While lector.Read
                    Dim Moneda As New Entidades.Moneda
                    Moneda.Id = objDaoMantenedor.UCInt(lector.Item("IdMoneda"))
                    Moneda.Simbolo = objDaoMantenedor.UCStr(lector.Item("mon_Simbolo"))
                    Moneda.Base = objDaoMantenedor.UCBool(lector.Item("mon_Base"))
                    Moneda.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("mon_CodigoSunat")) = True, "", lector.Item("mon_CodigoSunat")))
                    Moneda.Descripcion = CStr(IIf(IsDBNull(lector.Item("mon_Nombre")) = True, "", lector.Item("mon_Nombre")))
                    Moneda.Estado = CStr(IIf(IsDBNull(lector.Item("mon_Estado")) = True, "", lector.Item("mon_Estado")))
                    Lista.Add(Moneda)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Moneda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MonedaSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Moneda)
                Do While lector.Read
                    Dim Moneda As New Entidades.Moneda
                    Moneda.Id = CInt(lector.Item("IdMoneda"))
                    Moneda.Simbolo = CStr(lector.Item("mon_Simbolo"))
                    Moneda.Base = CBool(lector.Item("mon_Base"))
                    Moneda.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("mon_CodigoSunat")) = True, "", lector.Item("mon_CodigoSunat")))
                    Moneda.Descripcion = CStr(IIf(IsDBNull(lector.Item("mon_Nombre")) = True, "", lector.Item("mon_Nombre")))
                    Moneda.Estado = CStr(IIf(IsDBNull(lector.Item("mon_Estado")) = True, "", lector.Item("mon_Estado")))
                    Lista.Add(Moneda)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Moneda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MonedaSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Moneda)
                Do While lector.Read
                    Dim Moneda As New Entidades.Moneda
                    Moneda.Id = CInt(lector.Item("IdMoneda"))
                    Moneda.Simbolo = CStr(lector.Item("mon_Simbolo"))
                    Moneda.Base = CBool(lector.Item("mon_Base"))
                    Moneda.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("mon_CodigoSunat")) = True, "", lector.Item("mon_CodigoSunat")))
                    Moneda.Descripcion = CStr(IIf(IsDBNull(lector.Item("mon_Nombre")) = True, "", lector.Item("mon_Nombre")))
                    Moneda.Estado = CStr(IIf(IsDBNull(lector.Item("mon_Estado")) = True, "", lector.Item("mon_Estado")))
                    Lista.Add(Moneda)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Moneda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MonedaSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Moneda)
                Do While lector.Read
                    Dim Moneda As New Entidades.Moneda
                    Moneda.Id = CInt(lector.Item("IdMoneda"))
                    Moneda.Simbolo = CStr(lector.Item("mon_Simbolo"))
                    Moneda.Base = CBool(lector.Item("mon_Base"))
                    Moneda.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("mon_CodigoSunat")) = True, "", lector.Item("mon_CodigoSunat")))
                    Moneda.Descripcion = CStr(IIf(IsDBNull(lector.Item("mon_Nombre")) = True, "", lector.Item("mon_Nombre")))
                    Moneda.Estado = CStr(IIf(IsDBNull(lector.Item("mon_Estado")) = True, "", lector.Item("mon_Estado")))
                    Lista.Add(Moneda)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Moneda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MonedaSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Moneda)
                Do While lector.Read
                    Dim Moneda As New Entidades.Moneda
                    Moneda.Id = CInt(lector.Item("IdMoneda"))
                    Moneda.Simbolo = CStr(lector.Item("mon_Simbolo"))
                    Moneda.Base = CBool(lector.Item("mon_Base"))
                    Moneda.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("mon_CodigoSunat")) = True, "", lector.Item("mon_CodigoSunat")))
                    Moneda.Descripcion = CStr(IIf(IsDBNull(lector.Item("mon_Nombre")) = True, "", lector.Item("mon_Nombre")))
                    Moneda.Estado = CStr(IIf(IsDBNull(lector.Item("mon_Estado")) = True, "", lector.Item("mon_Estado")))
                    Lista.Add(Moneda)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Moneda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MonedaSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Moneda)
                Do While lector.Read
                    Dim Moneda As New Entidades.Moneda
                    Moneda.Id = CInt(lector.Item("IdMoneda"))
                    Moneda.Simbolo = CStr(lector.Item("mon_Simbolo"))
                    Moneda.Base = CBool(lector.Item("mon_Base"))
                    Moneda.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("mon_CodigoSunat")) = True, "", lector.Item("mon_CodigoSunat")))
                    Moneda.Descripcion = CStr(IIf(IsDBNull(lector.Item("mon_Nombre")) = True, "", lector.Item("mon_Nombre")))
                    Moneda.Estado = CStr(IIf(IsDBNull(lector.Item("mon_Estado")) = True, "", lector.Item("mon_Estado")))
                    Lista.Add(Moneda)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxCodSunat(ByVal codSunat As String) As List(Of Entidades.Moneda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MonedaSelectxCodSunat", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Cod", codSunat)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Moneda)
                Do While lector.Read
                    Dim Moneda As New Entidades.Moneda
                    Moneda.Id = CInt(lector.Item("IdMoneda"))
                    Moneda.Simbolo = CStr(lector.Item("mon_Simbolo"))
                    Moneda.Base = CBool(lector.Item("mon_Base"))
                    Moneda.CodigoSunat = CStr(IIf(IsDBNull(lector.Item("mon_CodigoSunat")) = True, "", lector.Item("mon_CodigoSunat")))
                    Moneda.Descripcion = CStr(IIf(IsDBNull(lector.Item("mon_Nombre")) = True, "", lector.Item("mon_Nombre")))
                    Moneda.Estado = CStr(IIf(IsDBNull(lector.Item("mon_Estado")) = True, "", lector.Item("mon_Estado")))
                    Lista.Add(Moneda)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCountBase() As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MonedaSelectCountBase", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Dim cont As Integer = 0
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                If lector.Read Then
                    cont = CInt(IIf(IsDBNull(lector.Item("Cuenta")) = True, 0, lector.Item("Cuenta")))
                End If
                'lector.Close()
                Return cont
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
   
End Class
