﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




'**************************   MARTES 11 MAYO 2010 HORA 10_45 AM









Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAODetalleConcepto_Anexo

    Private objDaoMantenedor As New DAO.DAOMantenedor
    Private cn As SqlConnection
    Private tr As SqlTransaction
    Private reader As SqlDataReader

    Public Function DetalleConcepto_Anexo_Insert(ByVal objDetalleConcepto_Anexo As Entidades.DetalleConcepto_Anexo, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer

        Dim p() As SqlParameter = New SqlParameter(2) {}

        p(0) = objDaoMantenedor.getParam(objDetalleConcepto_Anexo.IdDetalleConcepto, "@IdDetalleConcepto", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(objDetalleConcepto_Anexo.IdDocumento, "@IdDocumento", SqlDbType.Int)
        p(2) = objDaoMantenedor.getParam(objDetalleConcepto_Anexo.IdDocumentoRef, "@IdDocumentoRef", SqlDbType.Int)

        Return CInt(SqlHelper.ExecuteScalar(tr, CommandType.StoredProcedure, "_DetalleConcepto_Anexo_Insert", p))

    End Function

    Public Sub DetalleDocumento_Anexo_DeletexIdDetalleConcepto(ByVal IdDetalleConcepto As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(0) {}

        p(0) = objDaoMantenedor.getParam(IdDetalleConcepto, "@IdDetalleConcepto", SqlDbType.Int)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_DetalleDocumento_Anexo_DeletexIdDetalleConcepto", p)

    End Sub

    Public Function DetalleDocumento_Anexo_SelectDocumentoxIdDocumentoxIdDetalleConcepto(ByVal IdDocumento As Integer, ByVal IdDetalleConcepto As Integer) As List(Of Entidades.Documento)

        Dim lista As New List(Of Entidades.Documento)

        Try

            Dim p() As SqlParameter = New SqlParameter(1) {}
            p(0) = objDaoMantenedor.getParam(IdDocumento, "@IdDocumento", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdDetalleConcepto, "@IdDetalleConcepto", SqlDbType.Int)

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()

            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_DetalleDocumento_Anexo_SelectDocumentoxIdDocumentoxIdDetalleConcepto", p)
            While (reader.Read)

                Dim obj As New Entidades.Documento
                With obj

                    .Id = objDaoMantenedor.UCInt(reader("IdDocumento"))
                    .Serie = objDaoMantenedor.UCStr(reader("doc_Serie"))
                    .Codigo = objDaoMantenedor.UCStr(reader("doc_Codigo"))
                    .FechaEmision = objDaoMantenedor.UCDate(reader("doc_FechaEmision"))
                    .TotalAPagar = objDaoMantenedor.UCDec(reader("doc_TotalAPagar"))
                    .IdPersona = objDaoMantenedor.UCInt(reader("IdPersona"))
                    .NomMoneda = objDaoMantenedor.UCStr(reader("mon_Simbolo"))
                    .DescripcionPersona = objDaoMantenedor.UCStr(reader("DescripcionPersona"))
                    .NomTipoDocumento = objDaoMantenedor.UCStr(reader("tdoc_NombreCorto"))
                    .NomEstadoDocumento = objDaoMantenedor.UCStr(reader("edoc_Nombre"))
                    .NroDocumento = .Serie + " - " + .Codigo
                End With
                lista.Add(obj)

            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista

    End Function

End Class
