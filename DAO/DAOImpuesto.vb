'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOImpuesto

    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Dim obj As Entidades.Impuesto

    Public Function impuesto_select(ByVal IdImpuesto As Integer, ByVal IdEstado As String, ByVal Nombre As String) As List(Of Entidades.Impuesto)
        Dim Lista As New List(Of Entidades.Impuesto)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim reader As SqlDataReader = Nothing
        Dim cmd As New SqlCommand("_impuesto_select", cn)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdImpuesto", IIf(IdImpuesto = Nothing, DBNull.Value, IdImpuesto))
        cmd.Parameters.AddWithValue("@imp_Estado", IIf(IdEstado = Nothing, DBNull.Value, IdEstado))
        cmd.Parameters.AddWithValue("@imp_Nombre", IIf(Nombre = Nothing, DBNull.Value, Nombre))

        Try
            cn.Open()
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While reader.Read
                obj = New Entidades.Impuesto
                With obj
                    .Id = CInt(reader("IdImpuesto"))
                    .Descripcion = CStr(IIf(IsDBNull(reader("imp_Nombre")), "", reader("imp_Nombre")))
                    .Abv = CStr(IIf(IsDBNull(reader("imp_Abv")), "", reader("imp_Abv")))
                    .Tasa = CDec(IIf(IsDBNull(reader("imp_tasa")), 0, reader("imp_tasa")))
                    .CuentaContable = CStr(IIf(IsDBNull(reader("imp_CuentaContable")), "", reader("imp_CuentaContable")))
                    .Estado = CStr(IIf(IsDBNull(reader("imp_Estado")), "0", reader("imp_Estado")))
                End With
                Lista.Add(obj)
            Loop
            reader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Open()
        End Try
        Return Lista
    End Function

    Public Function InsertaImpuesto(ByVal impuesto As Entidades.Impuesto) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@imp_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = impuesto.Descripcion
        ArrayParametros(1) = New SqlParameter("@imp_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = impuesto.Abv
        ArrayParametros(2) = New SqlParameter("@imp_tasa", SqlDbType.Decimal)
        ArrayParametros(2).Value = impuesto.Tasa
        ArrayParametros(3) = New SqlParameter("@imp_CuentaContable", SqlDbType.VarChar)
        ArrayParametros(3).Value = impuesto.CuentaContable
        ArrayParametros(4) = New SqlParameter("@imp_Estado", SqlDbType.Char)
        ArrayParametros(4).Value = impuesto.Estado
        Return HDAO.Insert(cn, "_ImpuestoInsert", ArrayParametros)
    End Function
    Public Function ActualizaImpuesto(ByVal impuesto As Entidades.Impuesto) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
        ArrayParametros(0) = New SqlParameter("@imp_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = impuesto.Descripcion
        ArrayParametros(1) = New SqlParameter("@imp_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = impuesto.Abv
        ArrayParametros(2) = New SqlParameter("@imp_tasa", SqlDbType.Decimal)
        ArrayParametros(2).Value = impuesto.Tasa
        ArrayParametros(3) = New SqlParameter("@imp_CuentaContable", SqlDbType.VarChar)
        ArrayParametros(3).Value = impuesto.CuentaContable
        ArrayParametros(4) = New SqlParameter("@imp_Estado", SqlDbType.Char)
        ArrayParametros(4).Value = impuesto.Estado
        ArrayParametros(5) = New SqlParameter("@IdImpuesto", SqlDbType.Int)
        ArrayParametros(5).Value = impuesto.Id
        Return HDAO.Update(cn, "_ImpuestoUpdate", ArrayParametros)
    End Function
    Public Function SelectTasaIGV() As Decimal
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ImpuestoSelectTasaIGV", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                lector.Read()
                Dim Tasa As Decimal
                Tasa = CDec(IIf(IsDBNull(lector.Item("imp_tasa")) = True, 0, lector.Item("imp_tasa")))
                Tasa = Decimal.Round(Tasa, 2)
                Return Tasa
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
