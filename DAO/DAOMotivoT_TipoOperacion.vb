﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.





'*********************   JUEVES 11 MARZO 2010 HORA 06_08 PM










Imports System.Data.SqlClient

Public Class DAOMotivoT_TipoOperacion
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Sub InsertaMotivoT_TipoOperacion(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal lista As List(Of Entidades.MotivoT_TipoOperacion), ByVal IdMotivoT As Integer)
        Dim fila As New Entidades.MotivoT_TipoOperacion
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@IdTipoTraslado ", SqlDbType.Int)
        ArrayParametros(0).Value = IdMotivoT
        For Each fila In lista
            ArrayParametros(1) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
            ArrayParametros(1).Value = fila.idTipoOperac
            HDAO.InsertaT(cn, "[_motivoT_TipoOperacionInsert]", ArrayParametros, tr)
        Next
    End Sub
    Public Function ActualizaMotivoTraslado_TipoOperacion(ByVal UpdateTipoOperacion As List(Of Entidades.MotivoT_TipoOperacion)) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim TipoOperacion As New Entidades.MotivoT_TipoOperacion
        For Each TipoOperacion In UpdateTipoOperacion
            Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
            ArrayParametros(0) = New SqlParameter("@IdMotivoT", SqlDbType.Int)
            ArrayParametros(0).Value = TipoOperacion.IdMotivoT
            ArrayParametros(1) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
            ArrayParametros(1).Value = TipoOperacion.idTipoOperac
            Return HDAO.Update(cn, "_MotivoT_TipoOperacionUpdate", ArrayParametros)
        Next
    End Function
    Public Function DeletexIdMotivoT(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal idTraslado As Integer) As Boolean
        Try

            Dim cmd As New SqlCommand("_MotivoTraslado_TipoOperacionBorrar", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idTraslado", idTraslado)
            cmd.ExecuteNonQuery()

            Return True
        Catch ex As Exception
            Return False
        Finally

        End Try

    End Function
    'Para EDITAR la TipoOperacion mediante el IdMotivoT
    Public Function _MotivoT_TipoOperacionSelectxIdTraslado(ByVal IdTraslado As Integer) As List(Of Entidades.MotivoT_TipoOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoT_TipoOperacionSelectxIdMotivoT", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idTraslado", IdTraslado)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.MotivoT_TipoOperacion)
                Do While lector.Read
                    Dim obj As New Entidades.MotivoT_TipoOperacion
                    obj.IdMotivoT = CInt(lector.Item("IdMotivoT"))
                    obj.idTipoOperac = CInt(lector.Item("IdTipoOperacion"))
                    obj.NombreTipoOperacion = CStr(lector.Item("top_Nombre"))
                    lista.Add(obj)
                Loop
                lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCboTipoOperacion() As List(Of Entidades.MotivoT_TipoOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("TipoDocumento_TipoOperacionSelectCboTipoOreacion", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MotivoT_TipoOperacion)
                Do While lector.Read
                    Dim MotivoT As New Entidades.MotivoT_TipoOperacion
                    MotivoT.IdMotivoT = CInt(lector.Item("IdMotivoT"))
                    MotivoT.NombreTipoOperacion = CStr(lector.Item("NombreTipoOperacion"))
                    Lista.Add(MotivoT)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectCboxIdTipoOperacion(ByVal IdTipoOperacion As Integer) As List(Of Entidades.MotivoTraslado)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MotivoTrasladoSelectCboxIdTipoOperacion", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoOperacion", IdTipoOperacion)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MotivoTraslado)
                Do While lector.Read
                    Dim MotivoT As New Entidades.MotivoTraslado
                    MotivoT.Id = CInt(lector.Item("IdMotivoT"))
                    MotivoT.Nombre = CStr(IIf(IsDBNull(lector.Item("mt_Nombre")) = True, "", lector.Item("mt_Nombre")))
                    Lista.Add(MotivoT)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

