'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOEmpresaTienda
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaEmpresaTiendaT(ByVal cn As SqlConnection, ByVal lempresatienda As List(Of Entidades.EmpresaTienda), ByVal T As SqlTransaction) As Boolean
        Dim EmpresaTiendalista As Entidades.EmpresaTienda
        For Each EmpresaTiendalista In lempresatienda
            Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
            ArrayParametros(0) = New SqlParameter("@IdTienda", SqlDbType.Int)
            ArrayParametros(0).Value = EmpresaTiendalista.IdTienda
            ArrayParametros(1) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
            ArrayParametros(1).Value = EmpresaTiendalista.IdEmpresa
            ArrayParametros(2) = New SqlParameter("@et_CuentaContable", SqlDbType.VarChar)
            ArrayParametros(2).Value = EmpresaTiendalista.CuentaContable
            ArrayParametros(3) = New SqlParameter("@et_CentroCosto", SqlDbType.VarChar)
            ArrayParametros(3).Value = EmpresaTiendalista.CentroCosto
            HDAO.InsertaT(cn, "_EmpresaTiendaInsert", ArrayParametros, T)
        Next
    End Function
    Public Function ActualizaEmpresaTienda(ByVal empresatienda As Entidades.EmpresaTienda) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(0).Value = empresatienda.IdTienda
        ArrayParametros(1) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(1).Value = empresatienda.IdEmpresa
        ArrayParametros(2) = New SqlParameter("@et_CuentaContable", SqlDbType.VarChar)
        ArrayParametros(2).Value = empresatienda.CuentaContable
        ArrayParametros(3) = New SqlParameter("@et_CentroCosto", SqlDbType.VarChar)
        ArrayParametros(3).Value = empresatienda.CentroCosto
        Return HDAO.Update(cn, "_EmpresaTiendaUpdate", ArrayParametros)
    End Function
    Public Function DeletexIdEmpresa(ByVal cn As SqlConnection, ByVal IdEmpresa As Integer, ByVal T As SqlTransaction) As Boolean
        Dim cmd As New SqlCommand("_EmpresaTiendaDeleteAllByIdEmpresa", cn, T)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        Try
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function GrabaEmpresaTiendaT(ByVal cn As SqlConnection, ByVal lempresatienda As List(Of Entidades.EmpresaTienda), ByVal T As SqlTransaction) As Boolean
        Dim EmpresaTiendalista As Entidades.EmpresaTienda
        For Each EmpresaTiendalista In lempresatienda
            Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
            ArrayParametros(0) = New SqlParameter("@IdTienda", SqlDbType.Int)
            ArrayParametros(0).Value = EmpresaTiendalista.IdTienda
            ArrayParametros(1) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
            ArrayParametros(1).Value = EmpresaTiendalista.IdEmpresa
            ArrayParametros(2) = New SqlParameter("@et_CuentaContable", SqlDbType.VarChar)
            ArrayParametros(2).Value = EmpresaTiendalista.CuentaContable
            ArrayParametros(3) = New SqlParameter("@et_CentroCosto", SqlDbType.VarChar)
            ArrayParametros(3).Value = EmpresaTiendalista.CentroCosto
            ArrayParametros(4) = New SqlParameter("@Estado", SqlDbType.Bit)
            ArrayParametros(4).Value = EmpresaTiendalista.Estado
            HDAO.InsertaT(cn, "InsUpd_EmpresaTienda", ArrayParametros, T)
        Next
    End Function

    Public Function SelectGrillaxEmpresaTienda(ByVal idEmpresa As Integer) As List(Of Entidades.EmpresaTienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SelectEmpresaTienda", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", idEmpresa)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.EmpresaTienda)
                Do While lector.Read
                    Dim objTienda As New Entidades.EmpresaTienda
                    objTienda.Id = CInt(lector.Item("IdTienda"))
                    objTienda.Nombre = CStr(lector.Item("Tienda"))
                    objTienda.IdEmpresa = CInt(lector.Item("IdEmpresa"))
                    objTienda.CuentaContable = CStr(lector.Item("et_CuentaContable"))
                    objTienda.CentroCosto = CStr(lector.Item("et_CentroCosto"))
                    objTienda.Estado = CBool(lector.Item("et_Estado"))
                    Lista.Add(objTienda)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

End Class
