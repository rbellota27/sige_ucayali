﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'**************** MIERCOLES 14 JULIO


Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAOBotella
    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private cmd As SqlCommand
    Private reader As SqlDataReader
    Private objDaoMantenedor As New DAO.DAOMantenedor
    Dim objConexion As New DAO.Conexion

    Public Function Select_CboContenido() As DataTable

        Dim ds As New DataSet
        Try

            cmd = New SqlCommand("_Botella_Select_CboContenido", cn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Contenido")

        Catch ex As Exception

            Throw ex

        End Try

        Return ds.Tables("DT_Contenido")

    End Function

    Public Function SelectBotellaDisponiblexParams(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdContenido As Integer) As DataTable

        Dim ds As New DataSet

        Try

            Dim p() As SqlParameter = New SqlParameter(2) {}
            p(0) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdContenido, "@IdContenido", SqlDbType.Int)

            cmd = New SqlCommand("_Botella_SelectBotellaDisponiblexParams", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Botella")

        Catch ex As Exception

            Throw ex
        Finally

        End Try

        Return ds.Tables("DT_Botella")

    End Function

    Public Function SelectPendientesDevolucionCliente(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdCliente As Integer, ByVal NroBotella As String) As DataTable

        Dim ds As New DataSet

        Try

            Dim p() As SqlParameter = New SqlParameter(3) {}
            p(0) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdCliente, "@IdCliente", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(NroBotella, "@NroBotella", SqlDbType.VarChar)

            cmd = New SqlCommand("_Botella_SelectPendientesDevolucionCliente", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Botella")

        Catch ex As Exception

            Throw ex
        Finally

        End Try

        Return ds.Tables("DT_Botella")

    End Function


    Public Function registrar(ByVal objBotella As Entidades.Botella, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer

        Dim p() As SqlParameter = New SqlParameter(10) {}
        p(0) = objDaoMantenedor.getParam(objBotella.IdBotella, "@IdBotella", SqlDbType.BigInt)
        p(1) = objDaoMantenedor.getParam(objBotella.NroBotella, "@bot_NroBotella", SqlDbType.VarChar)
        p(2) = objDaoMantenedor.getParam(objBotella.Capacidad, "@bot_Capacidad", SqlDbType.Decimal)
        p(3) = objDaoMantenedor.getParam(objBotella.IdUnidadMedida_Capacidad, "@IdUnidadMedida_Capacidad", SqlDbType.Int)
        p(4) = objDaoMantenedor.getParam(objBotella.IdContenido, "@IdContenido", SqlDbType.Int)
        p(5) = objDaoMantenedor.getParam(objBotella.IdPropietario, "@IdPropietario", SqlDbType.Int)
        p(6) = objDaoMantenedor.getParam(objBotella.IdUbicacion, "@IdUbicacion", SqlDbType.Int)
        p(7) = objDaoMantenedor.getParam(objBotella.Estado, "@bot_Estado", SqlDbType.Bit)
        p(8) = objDaoMantenedor.getParam(objBotella.IdUsuarioInsert, "@IdUsuarioInsert", SqlDbType.Int)
        p(9) = objDaoMantenedor.getParam(objBotella.IdUsuarioUpdate, "@IdUsuarioUpdate", SqlDbType.Int)
        p(10) = objDaoMantenedor.getParam(objBotella.IdTienda, "@IdTienda", SqlDbType.Int)

        Return CInt(SqlHelper.ExecuteScalar(tr, CommandType.StoredProcedure, "_Botella_Registrar", p))

    End Function

End Class


