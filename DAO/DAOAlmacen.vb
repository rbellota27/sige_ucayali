'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.






'************************   JUEVES 15 ABRIL 01_09 PM






Imports System.Data.SqlClient
Public Class DAOAlmacen
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion



    Public Function AlmacenSelectActivoCentroDistribucionxIdEmpresa(ByVal IdEmpresa As Integer) As List(Of Entidades.Almacen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Dim cmd As New SqlCommand("_AlmacenSelectActivoCentroDistribucionxIdEmpresa", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Almacen)
                Do While lector.Read
                    Dim almacen As New Entidades.Almacen
                    almacen.IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    almacen.Nombre = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                    Lista.Add(almacen)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAllActivoxIdTipoAlmacenxIdEmpresa(ByVal IdTipoAlmacen As Integer, ByVal IdEmpresa As Integer) As List(Of Entidades.Almacen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Dim cmd As New SqlCommand("_AlmacenSelectAllActivoxIdTipoAlmacenxIdEmpresa", cn)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTipoAlmacen", IdTipoAlmacen)
        cmd.CommandType = CommandType.StoredProcedure

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Almacen)
                Do While lector.Read
                    Dim almacen As New Entidades.Almacen
                    almacen.IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    almacen.Nombre = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                    almacen.Ubigeo = CStr(IIf(IsDBNull(lector.Item("alm_Ubigeo")) = True, "", lector.Item("alm_Ubigeo")))
                    almacen.Direccion = CStr(IIf(IsDBNull(lector.Item("alm_Direccion")) = True, "", lector.Item("alm_Direccion")))
                    almacen.Referencia = CStr(IIf(IsDBNull(lector.Item("alm_Referencia")) = True, "", lector.Item("alm_Referencia")))
                    almacen.Area = CInt(IIf(IsDBNull(lector.Item("alm_Area")) = True, 0, lector.Item("alm_Area")))
                    almacen.IdAlmacenRef = CInt(IIf(IsDBNull(lector.Item("IdAlmacenRef")) = True, 0, lector.Item("IdAlmacenRef")))

                    Lista.Add(almacen)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function SelectCboxIdTipoAlmacenxIdTienda(ByVal IdTipoAlmacen As Integer, ByVal IdTienda As Integer) As List(Of Entidades.Almacen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AlmacenSelectCboxIdTipoAlmacenxIdTienda", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoAlmacen", IdTipoAlmacen)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Almacen)
                Do While lector.Read
                    Dim objAlmacen As New Entidades.Almacen
                    objAlmacen.IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    objAlmacen.Nombre = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                    Lista.Add(objAlmacen)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAlmacenTAPrincipal() As List(Of Entidades.Almacen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CR_SelectAlmacenCboRef", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Almacen)
                Do While lector.Read
                    Dim objAlmacen As New Entidades.Almacen
                    objAlmacen.IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    objAlmacen.Nombre = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                    Lista.Add(objAlmacen)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAlmacenPrincipalxIdTienda(ByVal idTienda As Integer) As List(Of Entidades.Almacen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AlmacenPrincipalxIdTienda", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTienda", idTienda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Almacen)
                Do While lector.Read
                    Dim objAlmacen As New Entidades.Almacen
                    objAlmacen.IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    objAlmacen.Nombre = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                    Lista.Add(objAlmacen)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function InsertaAlmacen(ByVal almacen As Entidades.Almacen) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Try
            Dim ArrayParametros() As SqlParameter = New SqlParameter(8) {}
            ArrayParametros(0) = New SqlParameter("@alm_Nombre", SqlDbType.VarChar)
            ArrayParametros(0).Value = almacen.Nombre
            ArrayParametros(1) = New SqlParameter("@alm_Ubigeo", SqlDbType.Char)
            ArrayParametros(1).Value = almacen.Ubigeo
            ArrayParametros(2) = New SqlParameter("@alm_Direccion", SqlDbType.VarChar)
            ArrayParametros(2).Value = almacen.Direccion
            ArrayParametros(3) = New SqlParameter("@alm_Referencia", SqlDbType.VarChar)
            ArrayParametros(3).Value = almacen.Referencia
            ArrayParametros(4) = New SqlParameter("@alm_Area", SqlDbType.Int)
            ArrayParametros(4).Value = almacen.Area
            ArrayParametros(5) = New SqlParameter("@alm_Estado", SqlDbType.Char)
            ArrayParametros(5).Value = almacen.Estado

            ArrayParametros(6) = New SqlParameter("@idAlmacenref", SqlDbType.Int)
            ArrayParametros(6).Value = almacen.IdAlmacenRef

            ArrayParametros(7) = New SqlParameter("@idtipoAlmacen", SqlDbType.Int)
            ArrayParametros(7).Value = almacen.IdtipoAlmacen

            ArrayParametros(8) = New SqlParameter("@CtroDistribuicion", SqlDbType.VarChar)
            ArrayParametros(8).Value = almacen.CtroDistribuicion


            Return HDAO.Insert(cn, "_AlmacenInsert", ArrayParametros)
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function
    Public Function ActualizarAlmacen(ByVal almacen As Entidades.Almacen) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Try
            Dim ArrayParametros() As SqlParameter = New SqlParameter(9) {}
            ArrayParametros(0) = New SqlParameter("@alm_Nombre", SqlDbType.VarChar)
            ArrayParametros(0).Value = almacen.Nombre
            ArrayParametros(1) = New SqlParameter("@alm_Ubigeo", SqlDbType.Char)
            ArrayParametros(1).Value = almacen.Ubigeo
            ArrayParametros(2) = New SqlParameter("@alm_Direccion", SqlDbType.VarChar)
            ArrayParametros(2).Value = almacen.Direccion
            ArrayParametros(3) = New SqlParameter("@alm_Referencia", SqlDbType.VarChar)
            ArrayParametros(3).Value = almacen.Referencia
            ArrayParametros(4) = New SqlParameter("@alm_Area", SqlDbType.Int)
            ArrayParametros(4).Value = almacen.Area
            ArrayParametros(5) = New SqlParameter("@alm_Estado", SqlDbType.Char)
            ArrayParametros(5).Value = almacen.Estado
            ArrayParametros(6) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
            ArrayParametros(6).Value = almacen.IdAlmacen

            ArrayParametros(7) = New SqlParameter("@idAlmacenref", SqlDbType.Int)
            ArrayParametros(7).Value = almacen.IdAlmacenRef

            ArrayParametros(8) = New SqlParameter("@idtipoAlmacen", SqlDbType.Int)
            ArrayParametros(8).Value = almacen.IdtipoAlmacen

            ArrayParametros(9) = New SqlParameter("@CtroDistribuicion", SqlDbType.VarChar)
            ArrayParametros(9).Value = almacen.CtroDistribuicion


            Return HDAO.Update(cn, "_AlmacenUpdate", ArrayParametros)
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCboxIdTienda(ByVal IdTienda As Integer) As List(Of Entidades.Almacen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AlmacenSelectCboxIdTienda", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Almacen)
                Do While lector.Read
                    Dim objAlmacen As New Entidades.Almacen
                    objAlmacen.IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    objAlmacen.Nombre = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                    objAlmacen.Seleccionado = CBool(IIf(IsDBNull(lector.Item("tal_Estado")) = True, 1, lector.Item("tal_Estado")))
                    objAlmacen.TalPrincipal = CBool(IIf(IsDBNull(lector.Item("tal_Principal")) = True, 1, lector.Item("tal_Principal")))
                    Lista.Add(objAlmacen)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectCboxIdTienda2(ByVal IdTienda As Integer) As List(Of Entidades.Almacen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE2
        Dim cmd As New SqlCommand("_AlmacenSelectCboxIdTienda", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Almacen)
                Do While lector.Read
                    Dim objAlmacen As New Entidades.Almacen
                    objAlmacen.IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    objAlmacen.Nombre = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                    objAlmacen.Seleccionado = CBool(IIf(IsDBNull(lector.Item("tal_Estado")) = True, 1, lector.Item("tal_Estado")))
                    objAlmacen.TalPrincipal = CBool(IIf(IsDBNull(lector.Item("tal_Principal")) = True, 1, lector.Item("tal_Principal")))
                    Lista.Add(objAlmacen)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCboxIdEmpresa(ByVal IdEmpresa As Integer) As List(Of Entidades.Almacen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AlmacenSelectCboxIdEmpresa", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Almacen)
                Do While lector.Read
                    Dim objAlmacen As New Entidades.Almacen
                    objAlmacen.IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    objAlmacen.Nombre = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                    objAlmacen.EstadoAI = CBool(IIf(IsDBNull(lector.Item("ea_Estado")) = True, 0, lector.Item("ea_Estado")))
                    Lista.Add(objAlmacen)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxId(ByVal IdAlmacen As Integer) As Entidades.Almacen
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AlmacenSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                'Dim Lista As New List(Of Entidades.Almacen)
                Dim obj As New Entidades.Almacen
                'Do While lector.Read
                If lector.Read Then


                    With obj

                        .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                        .Nombre = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                        .Ubigeo = CStr(IIf(IsDBNull(lector.Item("alm_Ubigeo")) = True, "", lector.Item("alm_Ubigeo")))
                        .Direccion = CStr(IIf(IsDBNull(lector.Item("alm_Direccion")) = True, "", lector.Item("alm_Direccion")))
                        .Referencia = CStr(IIf(IsDBNull(lector.Item("alm_Referencia")) = True, "", lector.Item("alm_Referencia")))
                        .Area = CInt(IIf(IsDBNull(lector.Item("alm_Area")) = True, 0, lector.Item("alm_Area")))
                        .Estado = CStr(IIf(IsDBNull(lector.Item("alm_Estado")) = True, "", lector.Item("alm_Estado")))

                    End With

                End If
                'Lista.Add(obj)
                'Loop
                lector.Close()
                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectGrilla() As List(Of Entidades.Almacen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AlmacenSelectGrilla", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Almacen)
                Do While lector.Read
                    Dim almacen As New Entidades.Almacen
                    almacen.IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    almacen.Nombre = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                    almacen.Ubigeo = CStr(IIf(IsDBNull(lector.Item("alm_Ubigeo")) = True, "", lector.Item("alm_Ubigeo")))
                    almacen.Direccion = CStr(IIf(IsDBNull(lector.Item("alm_Direccion")) = True, "", lector.Item("alm_Direccion")))
                    almacen.Referencia = CStr(IIf(IsDBNull(lector.Item("alm_Referencia")) = True, "", lector.Item("alm_Referencia")))
                    almacen.Area = CInt(IIf(IsDBNull(lector.Item("alm_Area")) = True, 0, lector.Item("alm_Area")))
                    almacen.Estado = CStr(IIf(IsDBNull(lector.Item("alm_Estado")) = True, "", lector.Item("alm_Estado")))
                    Lista.Add(almacen)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo(ByVal estado As Boolean) As List(Of Entidades.Almacen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As SqlCommand
        If estado = True Then
            cmd = New SqlCommand("_AlmacenSelectAllActivo", cn)
        Else
            cmd = New SqlCommand("[_AlmacenSelectAllInactivo]", cn)
        End If
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Almacen)
                Do While lector.Read
                    Dim almacen As New Entidades.Almacen
                    almacen.IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    almacen.Nombre = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                    almacen.Ubigeo = CStr(IIf(IsDBNull(lector.Item("alm_Ubigeo")) = True, "", lector.Item("alm_Ubigeo")))
                    almacen.Direccion = CStr(IIf(IsDBNull(lector.Item("alm_Direccion")) = True, "", lector.Item("alm_Direccion")))
                    almacen.Referencia = CStr(IIf(IsDBNull(lector.Item("alm_Referencia")) = True, "", lector.Item("alm_Referencia")))
                    almacen.Area = CInt(IIf(IsDBNull(lector.Item("alm_Area")) = True, 0, lector.Item("alm_Area")))
                    almacen.Estado = CStr(IIf(IsDBNull(lector.Item("alm_Estado")) = True, "", lector.Item("alm_Estado")))
                    Lista.Add(almacen)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAll() As List(Of Entidades.Almacen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AlmacenSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Almacen)
                Do While lector.Read
                    Dim almacen As New Entidades.Almacen
                    almacen.IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    almacen.Nombre = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                    almacen.Ubigeo = CStr(IIf(IsDBNull(lector.Item("alm_Ubigeo")) = True, "", lector.Item("alm_Ubigeo")))

                    almacen.Direccion = CStr(IIf(IsDBNull(lector.Item("alm_Direccion")) = True, "", lector.Item("alm_Direccion")))
                    almacen.Referencia = CStr(IIf(IsDBNull(lector.Item("alm_Referencia")) = True, "", lector.Item("alm_Referencia")))
                    almacen.Area = CInt(IIf(IsDBNull(lector.Item("alm_Area")) = True, 0, lector.Item("alm_Area")))

                    almacen.Estado = CStr(IIf(IsDBNull(lector.Item("alm_Estado")) = True, "", lector.Item("alm_Estado")))
                    Lista.Add(almacen)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Almacen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AlmacenSelectAllXNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Almacen)
                Do While lector.Read
                    Dim almacen As New Entidades.Almacen

                    almacen.IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    almacen.Nombre = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                    almacen.Ubigeo = CStr(IIf(IsDBNull(lector.Item("alm_Ubigeo")) = True, "", lector.Item("alm_Ubigeo")))


                    almacen.Direccion = CStr(IIf(IsDBNull(lector.Item("alm_Direccion")) = True, "", lector.Item("alm_Direccion")))
                    almacen.Referencia = CStr(IIf(IsDBNull(lector.Item("alm_Referencia")) = True, "", lector.Item("alm_Referencia")))
                    almacen.Area = CInt(IIf(IsDBNull(lector.Item("alm_Area")) = True, 0, lector.Item("alm_Area")))
                    almacen.Estado = CStr(IIf(IsDBNull(lector.Item("alm_Estado")) = True, "", lector.Item("alm_Estado")))

                    Lista.Add(almacen)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Almacen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AlmacenSelectXNombreXActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Almacen)
                Do While lector.Read
                    Dim almacen As New Entidades.Almacen


                    almacen.IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    almacen.Nombre = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))

                    almacen.Ubigeo = CStr(IIf(IsDBNull(lector.Item("alm_Ubigeo")) = True, "", lector.Item("alm_Ubigeo")))
                    almacen.Direccion = CStr(IIf(IsDBNull(lector.Item("alm_Direccion")) = True, "", lector.Item("alm_Direccion")))
                    almacen.Referencia = CStr(IIf(IsDBNull(lector.Item("alm_Referencia")) = True, "", lector.Item("alm_Referencia")))

                    almacen.Area = CInt(IIf(IsDBNull(lector.Item("alm_Area")) = True, 0, lector.Item("alm_Area")))
                    almacen.Estado = CStr(IIf(IsDBNull(lector.Item("alm_Estado")) = True, "", lector.Item("alm_Estado")))

                    Lista.Add(almacen)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Almacen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AlmacenSelectXNombreXInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Almacen)
                Do While lector.Read
                    Dim almacen As New Entidades.Almacen
                    almacen.IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    almacen.Nombre = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                    almacen.Ubigeo = CStr(IIf(IsDBNull(lector.Item("alm_Ubigeo")) = True, "", lector.Item("alm_Ubigeo")))

                    almacen.Direccion = CStr(IIf(IsDBNull(lector.Item("alm_Direccion")) = True, "", lector.Item("alm_Direccion")))
                    almacen.Referencia = CStr(IIf(IsDBNull(lector.Item("alm_Referencia")) = True, "", lector.Item("alm_Referencia")))
                    almacen.Area = CInt(IIf(IsDBNull(lector.Item("alm_Area")) = True, 0, lector.Item("alm_Area")))


                    almacen.Estado = CStr(IIf(IsDBNull(lector.Item("alm_Estado")) = True, "", lector.Item("alm_Estado")))
                    Lista.Add(almacen)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectCbo() As List(Of Entidades.Almacen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AlmacenSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Almacen)
                Do While lector.Read
                    Dim objAlmacen As New Entidades.Almacen
                    objAlmacen.IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    objAlmacen.Nombre = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                    Lista.Add(objAlmacen)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectGrillaActivoxNombre(ByVal nombre As String) As List(Of Entidades.Almacen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AlmacenSelectActivosXNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Almacen)
                Do While lector.Read
                    Dim almacen As New Entidades.Almacen
                    almacen.IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    almacen.Nombre = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                    almacen.Ubigeo = CStr(IIf(IsDBNull(lector.Item("alm_Ubigeo")) = True, "", lector.Item("alm_Ubigeo")))
                    almacen.Dpto = CStr(IIf(IsDBNull(lector.Item("Departamento")) = True, "", lector.Item("Departamento")))
                    almacen.Prov = CStr(IIf(IsDBNull(lector.Item("Provincia")) = True, "", lector.Item("Provincia")))
                    almacen.Dist = CStr(IIf(IsDBNull(lector.Item("Distrito")) = True, "", lector.Item("Distrito")))
                    almacen.Direccion = CStr(IIf(IsDBNull(lector.Item("alm_Direccion")) = True, "", lector.Item("alm_Direccion")))
                    almacen.Referencia = CStr(IIf(IsDBNull(lector.Item("alm_Referencia")) = True, "", lector.Item("alm_Referencia")))
                    almacen.Area = CInt(IIf(IsDBNull(lector.Item("alm_Area")) = True, 0, lector.Item("alm_Area")))
                    almacen.NomArea = CStr(IIf(IsDBNull(lector.Item("Area")) = True, "", lector.Item("Area")))
                    almacen.Estado = CStr(IIf(IsDBNull(lector.Item("alm_Estado")) = True, "", lector.Item("alm_Estado")))
                    Lista.Add(almacen)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectGrillaActivoInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Almacen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AlmacenSelectXNombreXActivoInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Almacen)
                Do While lector.Read
                    Dim almacen As New Entidades.Almacen
                    almacen.IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    almacen.Nombre = CStr(IIf(IsDBNull(lector.Item("alm_Nombre")) = True, "", lector.Item("alm_Nombre")))
                    almacen.Ubigeo = CStr(IIf(IsDBNull(lector.Item("alm_Ubigeo")) = True, "", lector.Item("alm_Ubigeo")))
                    almacen.Dpto = CStr(IIf(IsDBNull(lector.Item("Departamento")) = True, "", lector.Item("Departamento")))
                    almacen.Prov = CStr(IIf(IsDBNull(lector.Item("Provincia")) = True, "", lector.Item("Provincia")))
                    almacen.Dist = CStr(IIf(IsDBNull(lector.Item("Distrito")) = True, "", lector.Item("Distrito")))
                    almacen.Direccion = CStr(IIf(IsDBNull(lector.Item("alm_Direccion")) = True, "", lector.Item("alm_Direccion")))
                    almacen.Referencia = CStr(IIf(IsDBNull(lector.Item("alm_Referencia")) = True, "", lector.Item("alm_Referencia")))
                    almacen.Area = CInt(IIf(IsDBNull(lector.Item("alm_Area")) = True, 0, lector.Item("alm_Area")))
                    almacen.NomArea = CStr(IIf(IsDBNull(lector.Item("Area")) = True, "", lector.Item("Area")))
                    almacen.Estado = CStr(IIf(IsDBNull(lector.Item("alm_Estado")) = True, "", lector.Item("alm_Estado")))
                    almacen.IdAlmacenRef = CInt(IIf(IsDBNull(lector.Item("IdAlmacenRef")) = True, 0, lector.Item("IdAlmacenRef")))
                    almacen.IdtipoAlmacen = CInt(IIf(IsDBNull(lector.Item("IdTipoAlmacen")) = True, 0, lector.Item("IdTipoAlmacen")))
                    almacen.Alm_NombreRef = CStr(IIf(IsDBNull(lector.Item("AlmaceReferencia")) = True, "", lector.Item("AlmaceReferencia")))
                    almacen.Tipoalm_Nombre = CStr(IIf(IsDBNull(lector.Item("Tipoalm_Nombre")) = True, "", lector.Item("Tipoalm_Nombre")))
                    almacen.Tipoalm_Principal = CInt(IIf(IsDBNull(lector.Item("Tipoalm_Principal")) = True, 0, lector.Item("Tipoalm_Principal")))
                    almacen.CtroDistribuicion = CStr(IIf(IsDBNull(lector.Item("alm_CentroDistribucion")) = True, "", lector.Item("alm_CentroDistribucion")))
                    Lista.Add(almacen)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex

        Finally

        End Try
    End Function

#Region " +++++++++++++++++++++ Reporte ++++++"

    Public Function getDataSet_ComercializacionPorMes(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdAlmacen As Integer, ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdProducto As Integer, ByVal FechaIni As String, ByVal FechaFin As String, ByVal TipoAlmacen As DataTable, ByVal IdDocumento As Integer, ByVal IdTiendaPrecio As Integer, ByVal AtributoProducto As DataTable) As DataSet

        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ComercializacionMensual", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTiendaOrigen", IdTienda)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)

        cmd.Parameters.AddWithValue("@IdTipoExistencia", IdTipoExistencia)
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)

        cmd.Parameters.AddWithValue("@FechaIni", FechaIni)
        cmd.Parameters.AddWithValue("@FechaFin", FechaFin)

        cmd.Parameters.AddWithValue("@TipoAlmacen", TipoAlmacen)
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@IdTiendaPrecio", IdTiendaPrecio)
        cmd.Parameters.AddWithValue("@tbTipoTablaValor", AtributoProducto)

        Try

            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "Producto")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try
        Return ds

    End Function

    Public Function getDataSet_ComercializacionPorSemana(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdAlmacen As Integer, ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdProducto As Integer, ByVal YearIni As Integer, ByVal SemanaIni As Integer, ByVal YearFin As Integer, ByVal SemanaFin As Integer, ByVal TipoAlmacen As DataTable, ByVal IdDocumento As Integer, ByVal IdTiendaPrecio As Integer, ByVal AtributoProducto As DataTable) As DataSet

        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ComercializacionPorSemana", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTiendaOrigen", IdTienda)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)

        cmd.Parameters.AddWithValue("@IdTipoExistencia", IdTipoExistencia)
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)

        cmd.Parameters.AddWithValue("@IdYearIni", YearIni)
        cmd.Parameters.AddWithValue("@IdSemanaIni", SemanaIni)
        cmd.Parameters.AddWithValue("@IdYearFin", YearFin)
        cmd.Parameters.AddWithValue("@IdSemanaFin", SemanaFin)

        cmd.Parameters.AddWithValue("@TipoAlmacen", TipoAlmacen)
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@IdTiendaPrecio", IdTiendaPrecio)
        cmd.Parameters.AddWithValue("@tbTipoTablaValor", AtributoProducto)

        Try

            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "Producto")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try

        Return ds

    End Function

    Public Function getDataSet_CR_Comercializacion_Foshan02(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal FechaIni As String, ByVal FechaFin As String, ByVal YearIni As Integer, ByVal SemanaIni As Integer, ByVal YearFin As Integer, ByVal SemanaFin As Integer, ByVal FiltrarSemana As Integer, ByVal AtributoProducto As DataTable, ByVal TipoAlmacen As DataTable) As DataSet

        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CR_Comercializacion_Foshan02", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)

        cmd.Parameters.AddWithValue("@IdTipoExistencia", IdTipoExistencia)
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)

        cmd.Parameters.AddWithValue("@FechaIni", FechaIni)
        cmd.Parameters.AddWithValue("@FechaFin", FechaFin)

        cmd.Parameters.AddWithValue("@YearIni", YearIni)
        cmd.Parameters.AddWithValue("@SemanaIni", SemanaIni)
        cmd.Parameters.AddWithValue("@YearFin", YearFin)
        cmd.Parameters.AddWithValue("@SemanaFin", SemanaFin)


        cmd.Parameters.AddWithValue("@FiltrarSemana", FiltrarSemana)
        cmd.Parameters.AddWithValue("@tbTipoTablaValor", AtributoProducto)
        cmd.Parameters.AddWithValue("@tbTipoAlmacen", TipoAlmacen)


        Try

            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "Producto")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try

        Return ds

    End Function

    Public Function getDataSet_ProductoPorEntregar(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdDocumento As Integer, ByVal IdPersona As Integer) As DataSet

        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("[dbo].[_ProductoPorEntregar]", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0

        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)

        Try

            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "Producto")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try

        Return ds

    End Function

    Public Function getDataSet_AlmacenSaldoxFecha(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdProducto As Integer, ByVal FechaFin As String, ByVal FiltrarSaldo As Integer, ByVal Saldo As Decimal, ByVal Tabla As DataTable) As DataSet

        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AlmacenSaldoxFecha", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)

        cmd.Parameters.AddWithValue("@IdTipoExistencia", IdTipoExistencia)
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)


        cmd.Parameters.AddWithValue("@FechaFin", FechaFin)
        cmd.Parameters.AddWithValue("@FiltrarSaldo", FiltrarSaldo)
        cmd.Parameters.AddWithValue("@Saldo", Saldo)

        cmd.Parameters.AddWithValue("@Tabla", Tabla)


        Try

            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "Producto")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try

        Return ds

    End Function

    Public Function getDataSet_ProductoEnTransito(ByVal IdAlmacen As Integer, ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdProducto As Integer) As DataSet

        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoEnTransito", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)

        cmd.Parameters.AddWithValue("@IdTipoExistencia", IdTipoExistencia)
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)

        Try

            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "Producto")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try

        Return ds

    End Function

    Public Function getDataSet_ProductoComprometido(ByVal IdAlmacen As Integer, ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdProducto As Integer) As DataSet

        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoComprometido", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)

        cmd.Parameters.AddWithValue("@IdTipoExistencia", IdTipoExistencia)
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)

        Try

            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "Producto")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try

        Return ds

    End Function


#End Region

End Class
