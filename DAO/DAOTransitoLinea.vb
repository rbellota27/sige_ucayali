﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOTransitoLinea
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectAllActivoxIdLineaIdTipoExistencia(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.TransitoLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TransitoLineaSelectAllActivoxIdLineaIdTipoExistencia", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idtipoexistencia)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TransitoLinea)
                Do While lector.Read
                    Dim objTransitoLinea As New Entidades.TransitoLinea
                    objTransitoLinea.IdTransito = CInt((lector.Item("IdTransito")))
                    objTransitoLinea.NomTransito = CStr(lector.Item("NomTransito"))
                    objTransitoLinea.Abv = CStr(lector.Item("NomCorto"))
                    objTransitoLinea.IdLinea = CInt(lector.Item("IdLinea"))
                    objTransitoLinea.IdTipoExistencia = CInt(lector.Item("IdTipoExistencia"))
                    objTransitoLinea.Estado = CBool(lector.Item("tl_Estado"))
                    Lista.Add(objTransitoLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAllActivoxIdLineaIdTipoExistenciav2(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.TransitoLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TransitoLineaSelectAllActivoxIdLineaIdTipoExistenciav2", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idtipoexistencia)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TransitoLinea)
                Do While lector.Read
                    Dim objTransitoLinea As New Entidades.TransitoLinea
                    objTransitoLinea.IdTransitoOrden = CStr((lector.Item("IdTransito")))
                    objTransitoLinea.NomTransito = CStr(lector.Item("NomTransito"))
                    objTransitoLinea.Abv = CStr(lector.Item("NomCorto"))
                    Lista.Add(objTransitoLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function GrabaTransitoLineaT(ByVal IdLinea As Integer, ByVal cn As SqlConnection, ByVal listaTransitoLinea As List(Of Entidades.TransitoLinea), ByVal T As SqlTransaction) As Boolean
        Dim cmd As SqlCommand
        Try
            For i As Integer = 0 To listaTransitoLinea.Count - 1
                If T IsNot Nothing Then
                    cmd = New SqlCommand("InsUpd_TransitoLinea", cn, T)
                Else
                    cmd = New SqlCommand("InsUpd_TransitoLinea", cn)
                End If
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
                cmd.Parameters.AddWithValue("@IdTransito", listaTransitoLinea.Item(i).IdTransito)
                cmd.Parameters.AddWithValue("@IdTipoExistencia", listaTransitoLinea.Item(i).IdTipoExistencia)
                cmd.Parameters.AddWithValue("@tlEstado", listaTransitoLinea.Item(i).Estado)
                cmd.Parameters.AddWithValue("@tlOrden", listaTransitoLinea.Item(i).Orden)
                Dim cont As Integer = cmd.ExecuteNonQuery
                If cmd.ExecuteNonQuery = 0 Then
                    Return False
                End If
            Next
            Return True
        Catch ex As Exception
            Return False
        Finally

        End Try
    End Function
End Class
