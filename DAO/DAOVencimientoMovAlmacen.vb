'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOVencimientoMovAlmacen
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaVencimientoMovAlmacen(ByVal vencimientomovalmacen As Entidades.VencimientoMovAlmacen) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
        ArrayParametros(0) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(0).Value = vencimientomovalmacen.IdTienda
        ArrayParametros(1) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        ArrayParametros(1).Value = vencimientomovalmacen.IdAlmacen
        ArrayParametros(2) = New SqlParameter("@IdMovAlmacen", SqlDbType.Int)
        ArrayParametros(2).Value = vencimientomovalmacen.IdMovAlmacen
        ArrayParametros(3) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(3).Value = vencimientomovalmacen.IdEmpresa
        ArrayParametros(4) = New SqlParameter("@vma_FechaVencimiento", SqlDbType.DateTime)
        ArrayParametros(4).Value = vencimientomovalmacen.FechaVencimiento
        ArrayParametros(5) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(5).Value = vencimientomovalmacen.IdProducto
        Return HDAO.Insert(cn, "_VencimientoMovAlmacenInsert", ArrayParametros)
    End Function
    Public Function ActualizaVencimientoMovAlmacen(ByVal vencimientomovalmacen As Entidades.VencimientoMovAlmacen) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
        ArrayParametros(0) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(0).Value = vencimientomovalmacen.IdTienda
        ArrayParametros(1) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        ArrayParametros(1).Value = vencimientomovalmacen.IdAlmacen
        ArrayParametros(2) = New SqlParameter("@IdMovAlmacen", SqlDbType.Int)
        ArrayParametros(2).Value = vencimientomovalmacen.IdMovAlmacen
        ArrayParametros(3) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(3).Value = vencimientomovalmacen.IdEmpresa
        ArrayParametros(4) = New SqlParameter("@vma_FechaVencimiento", SqlDbType.DateTime)
        ArrayParametros(4).Value = vencimientomovalmacen.FechaVencimiento
        ArrayParametros(5) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(5).Value = vencimientomovalmacen.IdProducto
        Return HDAO.Update(cn, "_VencimientoMovAlmacenUpdate", ArrayParametros)
    End Function
End Class
