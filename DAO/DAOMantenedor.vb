﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.





'**************************   MARTES 11 MAYO 2010 HORA 10_45 AM








Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOMantenedor
    Protected objConexion As New Conexion
    Protected Cn As SqlConnection '= objConexion.ConexionSIGE()
    Protected Tr As SqlTransaction
    Protected Prm() As SqlParameter

    Enum modo_query
        Show = 0 'No se puede poner select
        Insert = 1
        Update = 2
        Delete = 3
    End Enum

    Public Function QueryT(ByVal Prmx() As SqlParameter, ByVal modo As modo_query, ByVal sp As String, _
 Optional ByRef Cnx As SqlConnection = Nothing, _
 Optional ByRef Trx As SqlTransaction = Nothing) As Integer
        'sp: storedProcedure ' o procedimiento almacenado
        Dim resul As Integer

        Prm = Prmx

        If Cnx Is Nothing Then
            Cn = objConexion.ConexionSIGE()
            Cn.Open()
        Else
            Cn = Cnx
        End If

        If Trx Is Nothing Then
            Tr = Cn.BeginTransaction()
        Else
            Tr = Trx
        End If

        Try
            Select Case modo
                Case modo_query.Insert
                    resul = Convert.ToInt32(SqlHelper.ExecuteScalar(Tr, CommandType.StoredProcedure, sp, Prm))
                Case modo_query.Update, modo_query.Delete
                    Try
                        SqlHelper.ExecuteNonQuery(Tr, CommandType.StoredProcedure, sp, Prm)
                        resul = 1
                    Catch ex As Exception
                        resul = 0
                        Throw (ex)
                    End Try
            End Select

            If Trx Is Nothing Then
                Tr.Commit()
            End If

        Catch e As Exception
            If Trx Is Nothing Then
                Tr.Rollback()
            End If
            Throw (e)
        Finally
            If Cnx Is Nothing Then
                Cn.Close()
                Cn.Dispose()
            End If
        End Try
        Return (resul)
    End Function

    Public Const DEFAULT_Integer As Integer = 0
    Public Const DEFAULT_Double As Double = 0
    Public Const DEFAULT_Decimal As Decimal = 0D
    Public Const DEFAULT_Boolean As Boolean = False
    Public Const DEFAULT_String As String = ""
    Public Const DEFAULT_Date As Date = Nothing


    'UCInt User CInt
    Public Function UCInt(ByVal obj As Object) As Integer
        Return CInt(IIf(IsDBNull(obj), DEFAULT_Integer, obj))
    End Function
    Public Function UCDbl(ByVal obj As Object) As Double
        Return CDbl(IIf(IsDBNull(obj), DEFAULT_Double, obj))
    End Function
    Public Function UCDec(ByVal obj As Object) As Decimal
        Return CDec(IIf(IsDBNull(obj), DEFAULT_Decimal, obj))
    End Function

    Public Function UCBool(ByVal obj As Object) As Boolean
        Return CBool(IIf(IsDBNull(obj), DEFAULT_Boolean, obj))
    End Function

    Public Function UCStr(ByVal obj As Object) As String
        Return CStr(IIf(IsDBNull(obj), DEFAULT_String, obj))
    End Function

    Public Function UCDate(ByVal obj As Object) As Date
        Return CDate(IIf(IsDBNull(obj), DEFAULT_Date, obj))
    End Function

    Public Function getParamValueInt(ByVal a As Integer) As Object
        'Return IIf(IsNothing(a) Or a = 0, DBNull.Value, a)
        Return IIf(a = DEFAULT_Integer Or a = Nothing, DBNull.Value, a)
    End Function

    Public Function getParamValueDec(ByVal a As Decimal) As Object
        Return IIf(a = DEFAULT_Decimal Or a = Nothing, DBNull.Value, a)
    End Function

    Public Function getParamValueStr(ByVal a As String) As Object
        Return IIf(String.IsNullOrEmpty(a), DBNull.Value, a)
        'Return IIf(a = Nothing Or a = DEFAULT_String, DBNull.Value, a)
    End Function
    Public Function getParamValueDate(ByVal a As Date) As Object
        Return IIf(a = Nothing, DBNull.Value, a)
    End Function
    Public Function getParamValueChar(ByVal a As Char) As Object
        Return IIf(a = Nothing, DBNull.Value, a)
    End Function

    Public Function getParam(ByVal a As Object, ByVal parameterName As String, ByVal BDTipo As SqlDbType) As SqlParameter
        Dim prmtro As New SqlParameter

        prmtro.SqlDbType = BDTipo
        prmtro.ParameterName = parameterName

        'Se ha utilizado las conversiones del sistema, 
        'por cuestion de rendimiento, se supone que 'a' 
        'es un valor ya validado, o sea es string ó int ó etc.
        Select Case BDTipo
            Case SqlDbType.VarChar
                prmtro.Value = getParamValueStr(CStr(a))
            Case SqlDbType.Bit
                prmtro.Value = CBool(a)
            Case SqlDbType.Int
                prmtro.Value = getParamValueInt(CInt(a))
            Case SqlDbType.Decimal
                prmtro.Value = getParamValueDec(CDec(a))
            Case SqlDbType.Date
                prmtro.Value = getParamValueDate(CDate(a))
            Case SqlDbType.DateTime
                prmtro.Value = getParamValueDate(CDate(a))
            Case SqlDbType.Char
                prmtro.Value = getParamValueChar(CChar(a))
        End Select

        Return prmtro
    End Function

End Class

