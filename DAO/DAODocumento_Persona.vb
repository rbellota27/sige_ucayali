﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.







'*********************   MIERCOLES  24/03/2010 HORA 05_59 PM












Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAODocumento_Persona

    Private cn As SqlConnection
    Private tr As SqlTransaction
    Private dr As SqlDataReader
    Dim objConexion As New DAO.Conexion
    Public Sub Documento_PersonaInsert(ByVal objDocumento_Persona As Entidades.Documento_Persona, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As SqlCommand
        Dim p() As SqlParameter = New SqlParameter(1) {}

        p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        p(0).Value = objDocumento_Persona.IdDocumento

        p(1) = New SqlParameter("@IdPersona", SqlDbType.Int)
        p(1).Value = objDocumento_Persona.IdPersona

        cmd = New SqlCommand
        cmd.Connection = cn
        cmd.CommandText = "_Documento_PersonaInsert"
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Transaction = tr
        cmd.CommandTimeout = 0
        cmd.Parameters.AddRange(p)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try


    End Sub

    Public Function Documento_PersonaSelectxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.PersonaView)

        Dim lista As New List(Of Entidades.PersonaView)

        Try

            Dim p() As SqlParameter = New SqlParameter(0) {}

            p(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
            p(0).Value = IdDocumento

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()

            dr = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_Documento_PersonaSelectxIdDocumento", p)

            While (dr.Read)

                Dim obj As New Entidades.PersonaView

                With obj

                    .IdPersona = CInt(IIf(IsDBNull(dr("IdPersona")) = True, 0, dr("IdPersona")))
                    .Descripcion = CStr(IIf(IsDBNull(dr("Descripcion")) = True, "", dr("Descripcion")))
                    .Ruc = CStr(IIf(IsDBNull(dr("Ruc")) = True, "", dr("Ruc")))
                    .Dni = CStr(IIf(IsDBNull(dr("Dni")) = True, "", dr("Dni")))

                End With

                lista.Add(obj)

            End While
            dr.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista

    End Function

End Class
