'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOJuridica
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Sub InsertaJuridica(ByVal cn As SqlConnection, ByVal juridica As Entidades.Juridica, ByVal T As SqlTransaction)
        ' Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@jur_Rsocial", SqlDbType.VarChar)
        ArrayParametros(0).Value = juridica.RazonSocial
        ArrayParametros(1) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(1).Value = juridica.IdPersona
        ArrayParametros(2) = New SqlParameter("@jur_FechaAniversario", SqlDbType.DateTime)
        ArrayParametros(2).Value = IIf(juridica.FechaAniversario = Nothing, DBNull.Value, juridica.FechaAniversario)
        HDAO.InsertaT(cn, "_JuridicaInsert", ArrayParametros, T)
    End Sub

    Public Sub InsertaObjJuridica(ByVal cn As SqlConnection, ByVal juridica As Entidades.Juridica, ByVal T As SqlTransaction)
        ' Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@jur_Rsocial", SqlDbType.VarChar)
        ArrayParametros(0).Value = juridica.RazonSocial
        ArrayParametros(1) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(1).Value = juridica.IdPersona
        ArrayParametros(2) = New SqlParameter("@jur_FechaAniversario", SqlDbType.DateTime)
        ArrayParametros(2).Value = IIf(juridica.FechaAniversario = Nothing, DBNull.Value, juridica.FechaAniversario)
        ArrayParametros(3) = New SqlParameter("@idagente", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(juridica.IdAgente = Nothing, DBNull.Value, juridica.IdAgente)
        ArrayParametros(4) = New SqlParameter("@ruc", SqlDbType.Int)
        ArrayParametros(4).Value = IIf(juridica.RUC = Nothing, DBNull.Value, juridica.RUC)
        HDAO.InsertaT(cn, "_InsertarPersonaJuridica", ArrayParametros, T)
    End Sub


    Public Function ActualizaJuridica(ByVal cn As SqlConnection, ByVal juridica As Entidades.Juridica, ByVal tr As SqlTransaction) As Boolean
        Try
            Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
            ArrayParametros(0) = New SqlParameter("@jur_Rsocial", SqlDbType.VarChar)
            ArrayParametros(0).Value = IIf(juridica.RazonSocial = Nothing, DBNull.Value, juridica.RazonSocial)
            ArrayParametros(1) = New SqlParameter("@IdPersona", SqlDbType.Int)
            ArrayParametros(1).Value = IIf(juridica.IdPersona = Nothing, DBNull.Value, juridica.IdPersona)
            ArrayParametros(2) = New SqlParameter("@jur_FechaAniversario", SqlDbType.DateTime)
            ArrayParametros(2).Value = IIf(juridica.FechaAniversario = Nothing, DBNull.Value, juridica.FechaAniversario)
            ArrayParametros(3) = New SqlParameter("@idagente", SqlDbType.Int)
            ArrayParametros(3).Value = IIf(juridica.IdAgente = Nothing, DBNull.Value, juridica.IdAgente)
            ArrayParametros(4) = New SqlParameter("@ruc", SqlDbType.VarChar)
            ArrayParametros(4).Value = IIf(juridica.RUC = Nothing, DBNull.Value, juridica.RUC)
            HDAO.UpdateT(cn, "_JuridicaUpdate", ArrayParametros, tr)
            Return True
        Catch ex As Exception

        End Try
    End Function
    Public Function SelectxId(ByVal IdPersona As Integer) As Entidades.Juridica
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_JuridicaSelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Using cn
            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
            Dim objJuridica As New Entidades.Juridica
            If lector.Read Then
                objJuridica = New Entidades.Juridica
                objJuridica.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                objJuridica.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                objJuridica.FechaAniversario = CDate(IIf(IsDBNull(lector.Item("jur_FechaAniversario")) = True, Nothing, lector.Item("jur_FechaAniversario")))
                objJuridica.RUC = CStr(IIf(IsDBNull(lector.Item("RUC")) = True, "", lector.Item("RUC")))
            End If
            lector.Close()
            Return objJuridica
        End Using
    End Function

    Public Function Selectx_Id(ByVal IdPersona As Integer) As Entidades.Juridica
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_listarJuridicaSelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Using cn
            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
            Dim objJuridica As New Entidades.Juridica
            If lector.Read Then
                objJuridica = New Entidades.Juridica
                objJuridica.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                objJuridica.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                objJuridica.FechaAniversario = CDate(IIf(IsDBNull(lector.Item("jur_FechaAniversario")) = True, Nothing, lector.Item("jur_FechaAniversario")))
                objJuridica.IdAgente = CInt(IIf(IsDBNull(lector.Item("Idagente")) = True, "", lector.Item("Idagente")))
                objJuridica.RUC = CStr(IIf(IsDBNull(lector.Item("RUC")) = True, "", lector.Item("RUC")))
            End If
            lector.Close()
            Return objJuridica
        End Using
    End Function

    Public Function SelectActivoxRazonSocial(ByVal razonSocial As String) As List(Of Entidades.Juridica)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_JuridicaSelectActivoxRazonSocial", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@RazonSocial", razonSocial)
        Dim lector As SqlDataReader
        Using cn
            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
            Dim lista As New List(Of Entidades.Juridica)
            While lector.Read
                Dim objJuridica As New Entidades.Juridica
                objJuridica.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                objJuridica.RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))
                objJuridica.RUC = CStr(IIf(IsDBNull(lector.Item("RUC")) = True, "", lector.Item("RUC")))
                lista.add(objJuridica)
            End While
            lector.Close()
            Return lista
        End Using
    End Function
   
End Class
