'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOColor
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaColor(ByVal color As Entidades.Color) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@col_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = color.Nombre
        ArrayParametros(1) = New SqlParameter("@col_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = color.NombreCorto

        ArrayParametros(2) = New SqlParameter("@col_Codigo", SqlDbType.Char, 2)
        ArrayParametros(2).Value = color.Col_Codigo

        ArrayParametros(3) = New SqlParameter("@col_Estado", SqlDbType.Char)
        ArrayParametros(3).Value = color.Estado
        Dim cmd As New SqlCommand("_ColorInsert", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
        Return True
    End Function
    Public Function ActualizaColor(ByVal color As Entidades.Color) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@col_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = color.Nombre
        ArrayParametros(1) = New SqlParameter("@col_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = color.NombreCorto

        ArrayParametros(2) = New SqlParameter("@col_Codigo", SqlDbType.Char, 2)
        ArrayParametros(2).Value = color.Col_Codigo

        ArrayParametros(3) = New SqlParameter("@col_Estado", SqlDbType.Char)
        ArrayParametros(3).Value = color.Estado
        ArrayParametros(4) = New SqlParameter("@IdColor", SqlDbType.Int)
        ArrayParametros(4).Value = color.Id
        Return HDAO.Update(cn, "_ColorUpdate", ArrayParametros)
    End Function
    Public Function SelectAllActivo_Cbo() As List(Of Entidades.Color)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ColorSelectAllActivo_Cbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Color)
                Do While lector.Read
                    Dim objColor As New Entidades.Color
                    objColor.Id = CInt(lector.Item("IdColor"))
                    objColor.Nombre = CStr(lector.Item("col_Nombre"))
                    Lista.Add(objColor)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.Color)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ColorSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Color)
                Do While lector.Read
                    Dim obj As New Entidades.Color
                    obj.Estado = CStr(lector.Item("col_Estado"))
                    obj.Id = CInt(lector.Item("IdColor"))
                    obj.Nombre = CStr(lector.Item("col_Nombre"))
                    obj.NombreCorto = CStr(IIf(IsDBNull(lector.Item("col_Abv")) = True, "", lector.Item("col_Abv")))
                    obj.Col_Codigo = CStr(IIf(IsDBNull(lector.Item("col_Codigo")) = True, "", lector.Item("col_Codigo")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Color)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ColorSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Color)
                Do While lector.Read
                    Dim obj As New Entidades.Color
                    obj.Estado = CStr(lector.Item("col_Estado"))
                    obj.Id = CInt(lector.Item("IdColor"))
                    obj.Nombre = CStr(lector.Item("col_Nombre"))
                    obj.NombreCorto = CStr(IIf(IsDBNull(lector.Item("col_Abv")) = True, "", lector.Item("col_Abv")))
                    obj.Col_Codigo = CStr(IIf(IsDBNull(lector.Item("col_Codigo")) = True, "", lector.Item("col_Codigo")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Color)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ColorSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Color)
                Do While lector.Read
                    Dim obj As New Entidades.Color
                    obj.Estado = CStr(lector.Item("col_Estado"))
                    obj.Id = CInt(lector.Item("IdColor"))
                    obj.Nombre = CStr(lector.Item("col_Nombre"))
                    obj.NombreCorto = CStr(IIf(IsDBNull(lector.Item("col_Abv")) = True, "", lector.Item("col_Abv")))
                    obj.Col_Codigo = CStr(IIf(IsDBNull(lector.Item("col_Codigo")) = True, "", lector.Item("col_Codigo")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Color)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ColorSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Color)
                Do While lector.Read
                    Dim obj As New Entidades.Color
                    obj.Estado = CStr(lector.Item("col_Estado"))
                    obj.Id = CInt(lector.Item("IdColor"))
                    obj.Nombre = CStr(lector.Item("col_Nombre"))
                    obj.NombreCorto = CStr(IIf(IsDBNull(lector.Item("col_Abv")) = True, "", lector.Item("col_Abv")))
                    obj.Col_Codigo = CStr(IIf(IsDBNull(lector.Item("col_Codigo")) = True, "", lector.Item("col_Codigo")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Color)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ColorSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Color)
                Do While lector.Read
                    Dim obj As New Entidades.Color
                    obj.Estado = CStr(lector.Item("col_Estado"))
                    obj.Id = CInt(lector.Item("IdColor"))
                    obj.Nombre = CStr(lector.Item("col_Nombre"))
                    obj.NombreCorto = CStr(IIf(IsDBNull(lector.Item("col_Abv")) = True, "", lector.Item("col_Abv")))
                    obj.Col_Codigo = CStr(IIf(IsDBNull(lector.Item("col_Codigo")) = True, "", lector.Item("col_Codigo")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Color)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ColorSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Color)
                Do While lector.Read
                    Dim obj As New Entidades.Color
                    obj.Estado = CStr(lector.Item("col_Estado"))
                    obj.Id = CInt(lector.Item("IdColor"))
                    obj.Nombre = CStr(lector.Item("col_Nombre"))
                    obj.NombreCorto = CStr(IIf(IsDBNull(lector.Item("col_Abv")) = True, "", lector.Item("col_Abv")))
                    obj.Col_Codigo = CStr(IIf(IsDBNull(lector.Item("col_Codigo")) = True, "", lector.Item("col_Codigo")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Color)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ColorSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Color)
                Do While lector.Read
                    Dim obj As New Entidades.Color
                    obj.Estado = CStr(lector.Item("col_Estado"))
                    obj.Id = CInt(lector.Item("IdColor"))
                    obj.Nombre = CStr(lector.Item("col_Nombre"))
                    obj.NombreCorto = CStr(IIf(IsDBNull(lector.Item("col_Abv")) = True, "", lector.Item("col_Abv")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
