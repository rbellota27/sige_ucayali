'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.


'************************************************* 21 Junio 2010
Imports System.Data.SqlClient

Public Class DAOBanco
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Dim _reader As SqlDataReader = Nothing
    Dim _cn As SqlConnection = Nothing
    Dim _cmd As SqlCommand = Nothing


    Public Function Banco_CuentaBancaria(ByVal IdPersona As Integer) As List(Of Entidades.Banco)

        Dim lista As List(Of Entidades.Banco)
        Dim obj As Entidades.Banco

        _cn = objConexion.ConexionSIGE

        _cmd = New SqlCommand("_Banco_CuentaBancaria", _cn)
        _cmd.CommandType = CommandType.StoredProcedure
        _cmd.Parameters.AddWithValue("@IdPersona", IdPersona)

        Try
            _cn.Open()
            _reader = _cmd.ExecuteReader(CommandBehavior.CloseConnection)

            lista = New List(Of Entidades.Banco)
            While _reader.Read
                obj = New Entidades.Banco

                With obj
                    .Id = CInt(_reader("IdBanco"))
                    .Nombre = CStr(_reader("ban_Nombre"))
                    .Abrev = CStr(IIf(IsDBNull(_reader("ban_Abv")), "", _reader("ban_Abv")))

                End With

                lista.Add(obj)
                obj = Nothing

            End While

            _reader.Close()

            Return lista

        Catch ex As Exception
            Throw ex
        Finally
            If _cn.State = ConnectionState.Open Then _cn.Close()
        End Try

        Return Nothing

    End Function





    Public Function InsertaBanco(ByVal Banco As Entidades.Banco, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer

        Try
            Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
            ArrayParametros(0) = New SqlParameter("@ban_CodigoSunat", SqlDbType.Char)
            ArrayParametros(0).Value = IIf(IsDBNull(Banco.CodigoSunat) = True, "0", Banco.CodigoSunat)
            ArrayParametros(1) = New SqlParameter("ban_Nombre", SqlDbType.VarChar)
            ArrayParametros(1).Value = IIf(IsDBNull(Banco.Nombre) = True, "", Banco.Nombre)

            ArrayParametros(2) = New SqlParameter("@ban_Estado", SqlDbType.Char)
            ArrayParametros(2).Value = IIf(IsDBNull(Banco.Estado) = True, "1", Banco.Estado)
            ArrayParametros(3) = New SqlParameter("@ban_Abv", SqlDbType.Char)
            ArrayParametros(3).Value = IIf(IsDBNull(Banco.Abrev) = True, "", Banco.Abrev)

            ArrayParametros(4) = New SqlParameter("@IdBanco", SqlDbType.Int)
            ArrayParametros(4).Direction = ParameterDirection.Output

            Dim cmd As New SqlCommand("_BancoInsert", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(ArrayParametros)
            cmd.ExecuteNonQuery()
            Return CInt(cmd.Parameters("@IdBanco").Value)
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function
    Public Function ActualizaBanco(ByVal banco As Entidades.Banco, ByVal cn As SqlConnection, Optional ByVal tr As SqlTransaction = Nothing) As Boolean
        Try

            'Dim cmd As New SqlCommand(" _BancoOficina", cn, tr)
            Dim cmd As New SqlCommand("_BancoUpdate", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdBanco", banco.Id)
            cmd.Parameters.AddWithValue("@ban_CodigoSunat", banco.CodigoSunat)
            cmd.Parameters.AddWithValue("@ban_Nombre", banco.Nombre)
            cmd.Parameters.AddWithValue("@ban_Estado", banco.Estado)
            cmd.Parameters.AddWithValue("@ban_Abv", banco.Abrev)

            If cmd.ExecuteNonQuery = 0 Then
                Return False
            End If
            Return True
        Catch ex As Exception
            Throw ex
            Return False
        Finally

        End Try
        'Dim cn As SqlConnection = objConexion.ConexionSIGE
        'Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        'ArrayParametros(0) = New SqlParameter("@ban_CodigoSunat", SqlDbType.Char)
        'ArrayParametros(0).Value = banco.CodigoSunat
        'ArrayParametros(1) = New SqlParameter("@ban_Nombre", SqlDbType.VarChar)
        'ArrayParametros(1).Value = banco.Nombre
        'ArrayParametros(2) = New SqlParameter("@ban_Estado", SqlDbType.Char)
        'ArrayParametros(2).Value = banco.Estado
        'ArrayParametros(3) = New SqlParameter("@IdBanco", SqlDbType.Int)
        'ArrayParametros(3).Value = banco.Id
        'Return HDAO.Update(cn, "_BancoUpdate", ArrayParametros)
    End Function
    'Public Function ActualizaBanco(ByVal banco As Entidades.Banco, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
    '    'Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
    '    ArrayParametros(0) = New SqlParameter("@ban_CodigoSunat", SqlDbType.Char)
    '    ArrayParametros(0).Value = banco.CodigoSunat
    '    ArrayParametros(1) = New SqlParameter("@ban_Nombre", SqlDbType.VarChar)
    '    ArrayParametros(1).Value = banco.Nombre
    '    ArrayParametros(2) = New SqlParameter("@ban_Estado", SqlDbType.Char)
    '    ArrayParametros(2).Value = banco.Estado
    '    ArrayParametros(3) = New SqlParameter("@IdBanco", SqlDbType.Int)
    '    ArrayParametros(3).Value = IIf(banco.Id = Nothing, DBNull.Value, banco.Id)
    '    'Return HDAO.Update(cn, "_TipoDocumentoUpdate", ArrayParametros)
    '    Dim cmd As New SqlCommand("_BancoUpdate", cn, tr)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.AddRange(ArrayParametros)
    '    If (cmd.ExecuteNonQuery = 0) Then
    '        Return False
    '    End If
    '    Return True
    'End Function

    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Banco)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_BancoSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Banco)
                Do While lector.Read
                    Dim banco As New Entidades.Banco
                    banco.Id = CInt(lector.Item("IdBanco"))
                    banco.CodigoSunat = CStr(lector.Item("ban_CodigoSunat"))
                    banco.Nombre = CStr(lector.Item("ban_Nombre"))
                    banco.Estado = CStr(lector.Item("ban_Estado"))
                    banco.Abrev = CStr(lector.Item("ban_Abv"))
                    Lista.Add(banco)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.Banco)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_BancoSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Banco)
                Do While lector.Read
                    Dim banco As New Entidades.Banco
                    banco.Id = CInt(lector.Item("IdBanco"))
                    banco.CodigoSunat = CStr(lector.Item("ban_CodigoSunat"))
                    banco.Nombre = CStr(lector.Item("ban_Nombre"))
                    banco.Estado = CStr(lector.Item("ban_Estado"))
                    banco.Abrev = CStr(lector.Item("ban_Abv"))
                    Lista.Add(banco)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Banco)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_BancoSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Banco)
                Do While lector.Read
                    Dim banco As New Entidades.Banco
                    banco.Id = CInt(lector.Item("IdBanco"))
                    banco.CodigoSunat = CStr(lector.Item("ban_CodigoSunat"))
                    banco.Nombre = CStr(lector.Item("ban_Nombre"))
                    banco.Estado = CStr(lector.Item("ban_Estado"))
                    banco.Abrev = CStr(IIf(IsDBNull(lector.Item("ban_Abv")) = True, "", lector.Item("ban_Abv")))

                    Lista.Add(banco)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Banco)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_BancoSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Banco)
                Do While lector.Read
                    Dim banco As New Entidades.Banco
                    banco.Id = CInt(lector.Item("IdBanco"))
                    banco.CodigoSunat = CStr(lector.Item("ban_CodigoSunat"))
                    banco.Nombre = CStr(lector.Item("ban_Nombre"))
                    banco.Estado = CStr(lector.Item("ban_Estado"))
                    banco.Abrev = CStr(IIf(IsDBNull(lector.Item("ban_Abv")) = True, "", lector.Item("ban_Abv")))


                    Lista.Add(banco)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Banco)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_BancoSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Banco)
                Do While lector.Read
                    Dim banco As New Entidades.Banco
                    banco.Id = CInt(lector.Item("IdBanco"))
                    banco.CodigoSunat = CStr(lector.Item("ban_CodigoSunat"))
                    banco.Nombre = CStr(lector.Item("ban_Nombre"))
                    banco.Estado = CStr(lector.Item("ban_Estado"))
                    banco.Abrev = CStr(IIf(IsDBNull(lector.Item("ban_Abv")) = True, "", lector.Item("ban_Abv")))

                    Lista.Add(banco)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Banco)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_BancoSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Banco)
                Do While lector.Read
                    Dim banco As New Entidades.Banco
                    banco.Id = CInt(lector.Item("IdBanco"))
                    banco.CodigoSunat = CStr(lector.Item("ban_CodigoSunat"))
                    banco.Nombre = CStr(lector.Item("ban_Nombre"))
                    banco.Estado = CStr(lector.Item("ban_Estado"))
                    banco.Abrev = CStr(IIf(IsDBNull(lector.Item("ban_Abv")) = True, "", lector.Item("ban_Abv")))

                    Lista.Add(banco)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxCodSunat(ByVal codSunat As String) As List(Of Entidades.Banco)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_BancoSelectxCodSunat", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@CodSunat", codSunat)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Banco)
                Do While lector.Read
                    Dim banco As New Entidades.Banco
                    banco.Id = CInt(lector.Item("IdBanco"))
                    banco.CodigoSunat = CStr(lector.Item("ban_CodigoSunat"))
                    banco.Nombre = CStr(lector.Item("ban_Nombre"))
                    banco.Estado = CStr(lector.Item("ban_Estado"))
                    banco.Abrev = CStr(IIf(IsDBNull(lector.Item("ban_Abv")) = True, "", lector.Item("ban_Abv")))

                    Lista.Add(banco)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal idbanco As Integer) As List(Of Entidades.Banco)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_BancoSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdBanco", idbanco)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Banco)
                Do While lector.Read
                    Dim banco As New Entidades.Banco
                    banco.Id = CInt(lector.Item("IdBanco"))
                    banco.CodigoSunat = CStr(lector.Item("ban_CodigoSunat"))
                    banco.Nombre = CStr(lector.Item("ban_Nombre"))
                    banco.Estado = CStr(lector.Item("ban_Estado"))
                    banco.Abrev = CStr(IIf(IsDBNull(lector.Item("ban_Abv")) = True, "", lector.Item("ban_Abv")))


                    Lista.Add(banco)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCbo() As List(Of Entidades.Banco)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_BancoSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Banco)
                Do While lector.Read
                    Dim Banco As New Entidades.Banco
                    Banco.Id = CInt(lector.Item("IdBanco"))
                    Banco.Nombre = CStr(lector.Item("ban_Nombre"))
                    Lista.Add(Banco)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function selectCuentaDetraccion() As List(Of Entidades.Banco)
        Dim cn As SqlConnection = (New Conexion).ConexionSIGE
        Dim lista As List(Of Entidades.Banco) = Nothing
        Using cmd As New SqlCommand("SP_DROPDOWNLIST_FILTRO")
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Connection = cn

                .Parameters.Add(New SqlParameter("@FITLRO", SqlDbType.VarChar)).Value = 0
                .Parameters.Add(New SqlParameter("@TABLA", SqlDbType.VarChar)).Value = "BANCO_DETRACCION"
            End With
            cn.Open()
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleRow)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.Banco)

                Dim idBanco As Integer = rdr.GetOrdinal("IdBanco")
                Dim ban_Nombre As Integer = rdr.GetOrdinal("ban_Nombre")
                Dim objetoBanco As Entidades.Banco = Nothing
                While rdr.Read()
                    objetoBanco = New Entidades.Banco
                    With objetoBanco
                        .Id = rdr.GetInt32(idBanco)
                        .Nombre = rdr.GetString(ban_Nombre)
                    End With
                    lista.Add(objetoBanco)
                End While
                rdr.Close()
            End If
            Return lista
        End Using
    End Function
End Class
