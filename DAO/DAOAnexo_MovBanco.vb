﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'**************** VIERNES 19 FEB 2010 HORA 02_58 PM



Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOAnexo_MovBanco
    Inherits DAOMantenedor
    'Protected objConexion As New Conexion
    'Private cn As SqlConnection
    'Private tr As SqlTransaction
    Private cmd As SqlCommand

    Public Function Anexo_MovBancoInsert(ByVal objAnexoMovBanco As Entidades.Anexo_MovBanco, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer

        Dim resul As Integer

        Dim param() As SqlParameter = New SqlParameter(4) {}
        param(0) = New SqlParameter("@IdMovBanco", SqlDbType.Int)
        param(0).Value = IIf(objAnexoMovBanco.IdMovBanco = Nothing, DBNull.Value, objAnexoMovBanco.IdMovBanco)
        param(1) = New SqlParameter("@IdBanco", SqlDbType.Int)
        param(1).Value = IIf(objAnexoMovBanco.IdBanco = Nothing, DBNull.Value, objAnexoMovBanco.IdBanco)
        param(2) = New SqlParameter("@IdCuentaBancaria", SqlDbType.Int)
        param(2).Value = IIf(objAnexoMovBanco.IdCuentaBancaria = Nothing, DBNull.Value, objAnexoMovBanco.IdCuentaBancaria)
        param(3) = New SqlParameter("@IdDocumentoRef", SqlDbType.Int)
        param(3).Value = IIf(objAnexoMovBanco.IdDocumentoRef = Nothing, DBNull.Value, objAnexoMovBanco.IdDocumentoRef)
        param(4) = New SqlParameter("@IdDocumentoRef2", SqlDbType.Int)
        param(4).Value = IIf(objAnexoMovBanco.IdDocumentoRef2 = Nothing, DBNull.Value, objAnexoMovBanco.IdDocumentoRef2)

        resul = Convert.ToInt32(SqlHelper.ExecuteScalar(tr, CommandType.StoredProcedure, "_Anexo_MovBancoInsert", param))

        Return (resul)

    End Function
    Public Function SelectxIdMovBanco(ByVal IdMovBanco As Integer) As List(Of Entidades.Anexo_MovBanco)
        Cn = objConexion.ConexionSIGE()

        Dim param() As SqlParameter = New SqlParameter(0) {}
        param(0) = New SqlParameter("@IdMovBanco", SqlDbType.Int)
        param(0).Value = IdMovBanco

        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()

                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "_Anexo_MovBancoViewSelectxIdMovBanco", param)

                Dim Lista As New List(Of Entidades.Anexo_MovBanco)
                With lector
                    Do While .Read

                        Dim obj As New Entidades.Anexo_MovBanco

                        obj.IdAnexo_MovBanco = CInt(.Item("IdAnexo_MovBanco"))
                        obj.IdMovBanco = CInt(.Item("IdMovBanco"))
                        obj.IdBanco = CInt(.Item("IdBanco"))
                        obj.IdCuentaBancaria = CInt(.Item("IdCuentaBancaria"))
                        obj.IdDocumentoRef = CInt(IIf(IsDBNull(.Item("IdDocumentoRef")), 0, .Item("IdDocumentoRef")))
                        obj.NomTipoDocumento = CStr(IIf(IsDBNull(.Item("TipoDocumento")), "", .Item("TipoDocumento")))
                        obj.NroDocumento = CStr(IIf(IsDBNull(.Item("doc_serie")), "", .Item("doc_serie"))) + " - " + CStr(IIf(IsDBNull(.Item("doc_codigo")), "", .Item("doc_codigo")))

                        Lista.Add(obj)

                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.Anexo_MovBanco)
        Cn = objConexion.ConexionSIGE()

        Dim param() As SqlParameter = New SqlParameter(0) {}
        param(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        param(0).Value = IdDocumento

        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()

                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "_Anexo_MovBancoViewSelectxIdDocumento", param)

                Dim Lista As New List(Of Entidades.Anexo_MovBanco)
                With lector
                    Do While .Read

                        Dim obj As New Entidades.Anexo_MovBanco

                        obj.IdAnexo_MovBanco = CInt(.Item("IdAnexo_MovBanco"))
                        obj.IdMovBanco = CInt(.Item("IdMovBanco"))
                        obj.IdBanco = CInt(.Item("IdBanco"))
                        obj.IdCuentaBancaria = CInt(.Item("IdCuentaBancaria"))
                        obj.IdDocumentoRef = CInt(IIf(IsDBNull(.Item("IdDocumentoRef")), 0, .Item("IdDocumentoRef")))
                        obj.NomTipoDocumento = CStr(IIf(IsDBNull(.Item("TipoDocumento")), "", .Item("TipoDocumento")))
                        obj.NroDocumento = CStr(IIf(IsDBNull(.Item("doc_serie")), "", .Item("doc_serie"))) + " - " + CStr(IIf(IsDBNull(.Item("doc_codigo")), "", .Item("doc_codigo")))

                        Lista.Add(obj)

                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function DeletexIdMovBanco(ByVal IdMovBanco As Integer, _
    Optional ByRef Cnx As SqlConnection = Nothing, _
    Optional ByRef Trx As SqlTransaction = Nothing) As Boolean

        Dim param() As SqlParameter = New SqlParameter(0) {}
        param(0) = New SqlParameter("@IdMovBanco", SqlDbType.Int)
        param(0).Value = IIf(IdMovBanco = Nothing, DBNull.Value, IdMovBanco)

        Return CBool(QueryT(param, modo_query.Delete, "_Anexo_MovBancoDeletexIdMovBanco", Cnx, Trx))
        
    End Function
    
End Class
