'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOCondTrabajo
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaCondTrabajo(ByVal condtrabajo As Entidades.CondTrabajo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@ct_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = condtrabajo.Nombre
        ArrayParametros(1) = New SqlParameter("@ct_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = condtrabajo.Estado
        Return HDAO.Insert(cn, "_CondTrabajoInsert", ArrayParametros)
    End Function
    Public Function ActualizaCondTrabajo(ByVal condtrabajo As Entidades.CondTrabajo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@ct_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = condtrabajo.Nombre
        ArrayParametros(1) = New SqlParameter("@ct_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = condtrabajo.Estado
        ArrayParametros(2) = New SqlParameter("@IdCondTrabajo", SqlDbType.Int)
        ArrayParametros(2).Value = condtrabajo.Id
        Return HDAO.Update(cn, "_CondTrabajoUpdate", ArrayParametros)
    End Function
    Public Function SelectAll() As List(Of Entidades.CondTrabajo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondTrabajoSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CondTrabajo)
                Do While lector.Read
                    Dim obj As New Entidades.CondTrabajo
                    obj.Estado = CStr(lector.Item("ct_Estado"))
                    obj.Nombre = CStr(lector.Item("ct_Nombre"))
                    obj.Id = CInt(lector.Item("IdCondTrabajo"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.CondTrabajo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondTrabajoSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CondTrabajo)
                Do While lector.Read
                    Dim obj As New Entidades.CondTrabajo
                    obj.Estado = CStr(lector.Item("ct_Estado"))
                    obj.Nombre = CStr(lector.Item("ct_Nombre"))
                    obj.Id = CInt(lector.Item("IdCondTrabajo"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.CondTrabajo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondTrabajoSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CondTrabajo)
                Do While lector.Read
                    Dim obj As New Entidades.CondTrabajo
                    obj.Estado = CStr(lector.Item("ct_Estado"))
                    obj.Nombre = CStr(lector.Item("ct_Nombre"))
                    obj.Id = CInt(lector.Item("IdCondTrabajo"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.CondTrabajo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondTrabajoSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CondTrabajo)
                Do While lector.Read
                    Dim obj As New Entidades.CondTrabajo
                    obj.Estado = CStr(lector.Item("ct_Estado"))
                    obj.Nombre = CStr(lector.Item("ct_Nombre"))
                    obj.Id = CInt(lector.Item("IdCondTrabajo"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.CondTrabajo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondTrabajoSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CondTrabajo)
                Do While lector.Read
                    Dim obj As New Entidades.CondTrabajo
                    obj.Estado = CStr(lector.Item("ct_Estado"))
                    obj.Nombre = CStr(lector.Item("ct_Nombre"))
                    obj.Id = CInt(lector.Item("IdCondTrabajo"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.CondTrabajo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondTrabajoSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CondTrabajo)
                Do While lector.Read
                    Dim obj As New Entidades.CondTrabajo
                    obj.Estado = CStr(lector.Item("ct_Estado"))
                    obj.Nombre = CStr(lector.Item("ct_Nombre"))
                    obj.Id = CInt(lector.Item("IdCondTrabajo"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.CondTrabajo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondTrabajoSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CondTrabajo)
                Do While lector.Read
                    Dim obj As New Entidades.CondTrabajo
                    obj.Estado = CStr(lector.Item("ct_Estado"))
                    obj.Nombre = CStr(lector.Item("ct_Nombre"))
                    obj.Id = CInt(lector.Item("IdCondTrabajo"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCbo() As List(Of Entidades.CondTrabajo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondTrabajoSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CondTrabajo)
                Do While lector.Read
                    Dim obj As New Entidades.CondTrabajo
                    obj.Nombre = CStr(lector.Item("ct_Nombre"))
                    obj.Id = CInt(lector.Item("IdCondTrabajo"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
