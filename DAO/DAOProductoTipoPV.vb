'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'*****************  VIERNES 22 ENERO 2010 HORA 03_54 PM

Imports System.Data.SqlClient

Public Class DAOProductoTipoPV
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion




    Public Sub ReplicarPrecioPV_IdProducto(ByVal IdTienda_Origen As Integer, ByVal IdTienda_Destino As Integer, ByVal IdProducto As Integer, ByVal IdTipoPV_Origen As Integer, ByVal IdTipoPV_Destino As Integer, ByVal PorcentPV As Decimal, ByVal addCostoFletexPeso As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As New SqlCommand("[dbo].[_ReplicarPrecioPV_IdProducto]", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdTienda_Origen", IdTienda_Origen)
        cmd.Parameters.AddWithValue("@IdTienda_Destino", IdTienda_Destino)
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        cmd.Parameters.AddWithValue("@IdTipoPV_Origen", IdTipoPV_Origen)
        cmd.Parameters.AddWithValue("@IdTipoPV_Destino", IdTipoPV_Destino)
        cmd.Parameters.AddWithValue("@PorcentPV", PorcentPV)
        cmd.Parameters.AddWithValue("@addCostoFletexPeso", addCostoFletexPeso)

        cmd.ExecuteNonQuery()

    End Sub

    Public Function ProductoPrecioV(ByVal IdTiendaOrigen As Integer, ByVal IdTiendaDestino As Integer, ByVal IdTipoPV_Origen As Integer, ByVal IdTipoPV_Destino As Integer, ByVal IdProducto As Integer, ByVal PorcentPV As Decimal, ByVal addCostoFletexPeso As Boolean) As List(Of Entidades.ProductoTipoPV)

        Dim Lista As List(Of Entidades.ProductoTipoPV)
        Dim obj As Entidades.ProductoTipoPV

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim reader As SqlDataReader = Nothing

        Dim cmd As New SqlCommand("_ProductoPrecioV", cn)
        cmd.CommandTimeout = 0
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        cmd.Parameters.AddWithValue("@IdTienda_Origen", IdTiendaOrigen)
        cmd.Parameters.AddWithValue("@IdTienda_Destino", IdTiendaDestino)
        cmd.Parameters.AddWithValue("@IdTipoPV_Origen", IdTipoPV_Origen)
        cmd.Parameters.AddWithValue("@IdTipoPV_Destino  ", IdTipoPV_Destino)

        cmd.Parameters.AddWithValue("@PorcentPV", PorcentPV)
        cmd.Parameters.AddWithValue("@addCostoFletexPeso", addCostoFletexPeso)

        Try
            cn.Open()

            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If IsNothing(Lista) Then Lista = New List(Of Entidades.ProductoTipoPV)

            Do While reader.Read
                obj = New Entidades.ProductoTipoPV
                With obj
                    .IdProducto = CInt(reader("IdProducto"))
                    .IdUnidadMedida = CInt(reader("IdUnidadMedida"))
                    .NomUMedida = CStr(reader("UnidadMedida"))
                    .Equivalencia = CDec(IIf(IsDBNull(reader("Equivalencia")), 0, reader("Equivalencia")))
                    .IdTipoPv = CInt(reader("IdTipoPV"))
                    .TipoPV = CStr(reader("pv_Nombre"))
                    .Valor = CDec(IIf(IsDBNull(reader("ActualPVOrigen")), 0, reader("ActualPVOrigen")))
                    .IdMoneda = CInt(reader("IdMoneda"))
                    .NomMoneda = CStr(reader("mon_Simbolo"))
                    .CostoFlete = CDec(IIf(IsDBNull(reader("CostoFlete")), 0, reader("CostoFlete")))
                    .UnidadPrincipal = CBool(IIf(IsDBNull(reader("Principal")), False, reader("Principal")))
                    .IdTienda = CInt(reader("IdTienda"))
                    .IdTiendaDestino = CInt(reader("IdTiendaDestino"))
                    .Tienda = CStr(reader("Tienda"))
                    .TiendaDestino = CStr(reader("TiendaDestino"))
                    .ActualPVDestino = CDec(IIf(IsDBNull(reader("ActualPVDestino")), 0, reader("ActualPVDestino")))
                    .NuevoPVDestino = CDec(IIf(IsDBNull(reader("NuevoPVDestino")), 0, reader("NuevoPVDestino")))
                End With

                Lista.Add(obj)
                obj = Nothing

            Loop

            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

        Return Lista

    End Function

    Public Sub ProductoTipoPV_ReplicarPrecioPV(ByVal IdTienda_Origen As Integer, ByVal IdTienda_Destino As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal IdTipoPV_Origen As Integer, ByVal IdTipoPV_Destino As Integer, ByVal PorcentPV As Decimal, ByVal DT_Atributos As DataTable, ByVal addCostoFletexPeso As Boolean, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As New SqlCommand("_ProductoTipoPV_ReplicarPrecioPV", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdTienda_Origen", IdTienda_Origen)
        cmd.Parameters.AddWithValue("@IdTienda_Destino", IdTienda_Destino)
        cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)
        cmd.Parameters.AddWithValue("@IdTipoPV_Origen", IdTipoPV_Origen)
        cmd.Parameters.AddWithValue("@IdTipoPV_Destino", IdTipoPV_Destino)
        cmd.Parameters.AddWithValue("@PorcentPV", PorcentPV)
        cmd.Parameters.AddWithValue("@Tabla", DT_Atributos)
        cmd.Parameters.AddWithValue("@addCostoFletexPeso", addCostoFletexPeso)

        cmd.ExecuteNonQuery()

    End Sub



    Public Function SelectValor_Equivalencia_VentaxParams(ByVal IdAlmacen As Integer, ByVal IdMoneda As Integer, ByVal IdUnidadMedida As Integer, ByVal IdProducto As Integer, ByVal idtienda As Integer, ByVal idtipopv As Integer, Optional ByVal addPorcentRetazoAlways As Boolean = False) As Entidades.Catalogo

        Dim objCatalogo As Entidades.Catalogo = Nothing

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoTipoPVSelectValor_Equivalencia_VentaxParams", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdUnidadMedida", IdUnidadMedida)
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        cmd.Parameters.AddWithValue("@IdTipoPV", idtipopv)
        cmd.Parameters.AddWithValue("@IdTienda", idtienda)
        cmd.Parameters.AddWithValue("@IdMoneda", IdMoneda)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)

        cmd.Parameters.AddWithValue("@addPorcentRetazoAlways", addPorcentRetazoAlways)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)

                If lector.Read Then

                    objCatalogo = New Entidades.Catalogo
                    objCatalogo.PrecioSD = CDec(IIf(IsDBNull(lector.Item("ValorFinal")) = True, 0, lector.Item("ValorFinal")))
                    objCatalogo.StockAReal = CDec(IIf(IsDBNull(lector.Item("StockReal_Eq")) = True, 0, lector.Item("StockReal_Eq")))
                    objCatalogo.StockComprometido = CDec(IIf(IsDBNull(lector.Item("StockComprometido_Eq")) = True, 0, lector.Item("StockComprometido_Eq")))

                End If

                lector.Close()
                Return objCatalogo
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function SelectUM_Venta(ByVal IdProducto As Integer, ByVal idtienda As Integer, ByVal idtipopv As Integer, ByVal cn As SqlConnection) As List(Of Entidades.UnidadMedida)

        Dim cmd As New SqlCommand("_ProductoTipoPVSelectUMxParams", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        cmd.Parameters.AddWithValue("@IdTienda", idtienda)
        cmd.Parameters.AddWithValue("@IdTipoPV", idtipopv)

        Dim lector As SqlDataReader
        Try

            lector = cmd.ExecuteReader()
            Dim Lista As New List(Of Entidades.UnidadMedida)
            Do While lector.Read
                Dim objUM As New Entidades.UnidadMedida
                objUM.Id = CInt(lector.Item("IdUnidadMedida"))
                objUM.DescripcionCorto = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                Lista.Add(objUM)
            Loop
            lector.Close()
            Return Lista

        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function SelectUM_Venta(ByVal IdProducto As Integer, ByVal idtienda As Integer, ByVal idtipopv As Integer) As List(Of Entidades.UnidadMedida)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoTipoPVSelectUMxParams", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        cmd.Parameters.AddWithValue("@IdTienda", idtienda)
        cmd.Parameters.AddWithValue("@IdTipoPV", idtipopv)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.UnidadMedida)
                Do While lector.Read
                    Dim objUM As New Entidades.UnidadMedida
                    objUM.Id = CInt(lector.Item("IdUnidadMedida"))
                    objUM.DescripcionCorto = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                    Lista.Add(objUM)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function



    Public Function DeletexIdTiendaxIdProdxIdUMxIdTipoPV(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal objProductoTipoPV As Entidades.ProductoTipoPV) As Boolean
        'recibo la conexion abierta
        Dim hecho As Boolean = False
        Try
            Dim cmd As New SqlCommand("_ProductoTipoPVDeletexIdTiendaxIdProdxIdUMxIdTipoPV", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdTienda", objProductoTipoPV.IdTienda)
            cmd.Parameters.AddWithValue("@IdProducto", objProductoTipoPV.IdProducto)
            cmd.Parameters.AddWithValue("@IdUnidadMedida", objProductoTipoPV.IdUnidadMedida)
            cmd.Parameters.AddWithValue("@IdTipoPV", objProductoTipoPV.IdTipoPv)
            If cmd.ExecuteNonQuery = 0 Then
                hecho = False
            Else
                hecho = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function DeletexIdTiendaxIdProdxIdTipoPV(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal idproducto As Integer, ByVal idtienda As Integer, ByVal idtipopv As Integer) As Boolean
        'recibo la conexion abierta
        Dim hecho As Boolean = False
        Try
            Dim cmd As New SqlCommand("_ProductoTipoPVDeletexIdTiendaxIdProdxIdTipoPV", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdTienda", idtienda)
            cmd.Parameters.AddWithValue("@IdProducto", idproducto)
            cmd.Parameters.AddWithValue("@IdTipoPV", idtipopv)
            If cmd.ExecuteNonQuery = 0 Then
                hecho = False
            Else
                hecho = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function InsertaProductoTipoPVT(ByVal lista As List(Of Entidades.ProductoTipoPV), ByVal CambiarxEquivalencia As Boolean) As Boolean

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            For i As Integer = 0 To lista.Count - 1

                '********** Nuevo Procedimiento
                Me.DeletexIdTiendaxIdProdxIdUMxIdTipoPV(cn, tr, lista.Item(i))
                Me.InsertaProductoTipoPV(cn, tr, lista.Item(i), CambiarxEquivalencia)

            Next
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function

    Public Function InsertaProductoTipoPVTxProducto(ByVal lista As List(Of Entidades.ProductoTipoPV), ByVal idproducto As Integer, ByVal idtienda As Integer, ByVal idtipopv As Integer) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim hecho As Boolean = False
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Me.DeletexIdTiendaxIdProdxIdTipoPV(cn, tr, idproducto, idtienda, idtipopv)

            For i As Integer = 0 To lista.Count - 1
                Me.InsertaProductoTipoPV(cn, tr, lista(i))
            Next
            tr.Commit()
            hecho = True
        Catch ex As Exception
            tr.Rollback()
            hecho = False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return hecho
    End Function

    Public Sub InsertaProductoTipoPV(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal productotipopv As Entidades.ProductoTipoPV, Optional ByVal CambiarxEquivalencia As Boolean = False)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(11) {}
        ArrayParametros(0) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(productotipopv.IdProducto = Nothing, DBNull.Value, productotipopv.IdProducto)
        ArrayParametros(1) = New SqlParameter("@IdTipoPv", SqlDbType.Int)
        ArrayParametros(1).Value = IIf(productotipopv.IdTipoPv = Nothing, DBNull.Value, productotipopv.IdTipoPv)
        ArrayParametros(2) = New SqlParameter("@ppv_PUtilFijo", SqlDbType.Decimal)
        ArrayParametros(2).Value = IIf(productotipopv.PUtilFijo = Nothing, DBNull.Value, productotipopv.PUtilFijo)
        ArrayParametros(3) = New SqlParameter("@ppv_PUtilVariable", SqlDbType.Decimal)
        ArrayParametros(3).Value = IIf(productotipopv.PUtilVariable = Nothing, DBNull.Value, productotipopv.PUtilVariable)
        ArrayParametros(4) = New SqlParameter("@ppv_Valor", SqlDbType.Decimal)
        ArrayParametros(4).Value = IIf(productotipopv.Valor = Nothing, DBNull.Value, productotipopv.Valor)
        ArrayParametros(5) = New SqlParameter("@ppv_Utilidad", SqlDbType.Decimal)
        ArrayParametros(5).Value = IIf(productotipopv.Utilidad = Nothing, DBNull.Value, productotipopv.Utilidad)
        ArrayParametros(6) = New SqlParameter("@ppv_Estado", SqlDbType.Char)
        ArrayParametros(6).Value = IIf(productotipopv.Estado = Nothing, DBNull.Value, productotipopv.Estado)
        ArrayParametros(7) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        ArrayParametros(7).Value = IIf(productotipopv.IdUsuario = Nothing, DBNull.Value, productotipopv.IdUsuario)
        ArrayParametros(8) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(8).Value = IIf(productotipopv.IdTienda = Nothing, DBNull.Value, productotipopv.IdTienda)
        ArrayParametros(9) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
        ArrayParametros(9).Value = IIf(productotipopv.IdUnidadMedida = Nothing, DBNull.Value, productotipopv.IdUnidadMedida)
        ArrayParametros(10) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        ArrayParametros(10).Value = IIf(productotipopv.IdMoneda = Nothing, DBNull.Value, productotipopv.IdMoneda)
        ArrayParametros(11) = New SqlParameter("@CambiarxEquivalencia", SqlDbType.Bit)
        ArrayParametros(11).Value = CambiarxEquivalencia

        Dim cmd As New SqlCommand("_ProductoTipoPVInsert", cn, tr)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        If cmd.ExecuteNonQuery = 0 Then
            Throw New Exception
        End If
    End Sub
    Public Sub ActualizaProductoTipoPV(ByVal productotipopv As Entidades.ProductoTipoPV, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(10) {}
        ArrayParametros(0) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(productotipopv.IdProducto = Nothing, DBNull.Value, productotipopv.IdProducto)
        ArrayParametros(1) = New SqlParameter("@IdTipoPv", SqlDbType.Int)
        ArrayParametros(1).Value = IIf(productotipopv.IdTipoPv = Nothing, DBNull.Value, productotipopv.IdTipoPv)
        ArrayParametros(2) = New SqlParameter("@ppv_PUtilFijo", SqlDbType.Decimal)
        ArrayParametros(2).Value = IIf(productotipopv.PUtilFijo = Nothing, DBNull.Value, productotipopv.PUtilFijo)
        ArrayParametros(3) = New SqlParameter("@ppv_PUtilVariable", SqlDbType.Decimal)
        ArrayParametros(3).Value = IIf(productotipopv.PUtilVariable = Nothing, DBNull.Value, productotipopv.PUtilVariable)
        ArrayParametros(4) = New SqlParameter("@ppv_Valor", SqlDbType.Decimal)
        ArrayParametros(4).Value = IIf(productotipopv.Valor = Nothing, DBNull.Value, productotipopv.Valor)
        ArrayParametros(5) = New SqlParameter("@ppv_Utilidad", SqlDbType.Decimal)
        ArrayParametros(5).Value = IIf(productotipopv.Utilidad = Nothing, DBNull.Value, productotipopv.Utilidad)
        ArrayParametros(6) = New SqlParameter("@ppv_Estado", SqlDbType.Char)
        ArrayParametros(6).Value = IIf(productotipopv.Estado = Nothing, DBNull.Value, productotipopv.Estado)
        ArrayParametros(7) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        ArrayParametros(7).Value = IIf(productotipopv.IdUsuario = Nothing, DBNull.Value, productotipopv.IdUsuario)
        ArrayParametros(8) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(8).Value = IIf(productotipopv.IdTienda = Nothing, DBNull.Value, productotipopv.IdTienda)
        ArrayParametros(9) = New SqlParameter("@IdUnidadMedida", SqlDbType.Int)
        ArrayParametros(9).Value = IIf(productotipopv.IdUnidadMedida = Nothing, DBNull.Value, productotipopv.IdUnidadMedida)
        ArrayParametros(10) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        ArrayParametros(10).Value = IIf(productotipopv.IdMoneda = Nothing, DBNull.Value, productotipopv.IdMoneda)

        Dim cmd As New SqlCommand("_ProductoTipoPVUpdate", cn, tr)

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        If cmd.ExecuteNonQuery = 0 Then
            Throw New Exception
        End If
    End Sub
    Public Function SelectAll() As List(Of Entidades.ProductoTipoPV)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoTipoPVSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ProductoTipoPV)
                Do While lector.Read
                    Dim ProductoTipoPV As New Entidades.ProductoTipoPV
                    ProductoTipoPV.IdProducto = CInt(lector.Item("IdProducto"))
                    ProductoTipoPV.IdTipoPv = CInt(lector.Item("IdTipoPv"))
                    ProductoTipoPV.PUtilFijo = CDec(lector.Item("ppv_PUtilFijo"))
                    ProductoTipoPV.PUtilVariable = CDec(lector.Item("ppv_PUtilVariable"))
                    ProductoTipoPV.Valor = CDec(lector.Item("ppv_Valor"))
                    ProductoTipoPV.Utilidad = CDec(lector.Item("ppv_Utilidad"))
                    ProductoTipoPV.Estado = CStr(lector.Item("ppv_Estado"))
                    ProductoTipoPV.IdUsuario = CInt(lector.Item("IdUsuario"))
                    Lista.Add(ProductoTipoPV)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectPrecioPublicoxParams(ByVal IdTienda As Integer, ByVal IdProducto As Integer, _
                                               ByVal IdUnidadMedida As Integer) As Decimal

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoTipoPVSelectPrecioPublicoxParams", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        cmd.Parameters.AddWithValue("@IdUnidadMedida", IdUnidadMedida)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                lector.Read()
                If lector.HasRows = False Then
                    Return CDec(0)
                End If
                Dim Precio As Decimal = CDec(IIf(IsDBNull(lector.Item("PrecioSD")) = True, 0, lector.Item("PrecioSD")))
                Return Precio
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectPrecioxParams(ByVal IdTienda As Integer, ByVal IdProducto As Integer, _
                                           ByVal IdUnidadMedida As Integer, ByVal IdTipoPV As Integer) As Decimal
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoTipoPVSelectPrecioxParams", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        cmd.Parameters.AddWithValue("@IdUnidadMedida", IdUnidadMedida)
        cmd.Parameters.AddWithValue("@IdTipoPv", IdTipoPV)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                lector.Read()
                If lector.HasRows = False Then
                    Return CDec(0)
                End If
                Dim Precio As Decimal = CDec(IIf(IsDBNull(lector.Item("PrecioSD")) = True, 0, lector.Item("PrecioSD")))
                Return Precio
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectPpublicoUMPrincipalxParams(ByVal IdTienda As Integer, ByVal IdProducto As Integer) As Decimal
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoTipoPVSelectPpublicoUMPrincipalxParams", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)


        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                lector.Read()
                If lector.HasRows = False Then
                    Return CDec(0)
                End If
                Dim Precio As Decimal = CDec(IIf(IsDBNull(lector.Item("ppv_Valor")) = True, 0, lector.Item("ppv_Valor")))
                Return Precio
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectxIdProductoxIdTiendaxIdTipoPV(ByVal idproducto As Integer, ByVal idtienda As Integer, ByVal idtipopv As Integer) As List(Of Entidades.ProductoTipoPV)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoTipoPVSelectxIdProductoxIdTiendaxIdTipoPV", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", idproducto)
        cmd.Parameters.AddWithValue("@IdTienda", idtienda)
        cmd.Parameters.AddWithValue("@IdTipoPV", idtipopv)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ProductoTipoPV)
                Do While lector.Read
                    Dim obj As New Entidades.ProductoTipoPV

                    obj.IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                    obj.IdTipoPv = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                    obj.IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                    obj.IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                    obj.PUtilFijo = CDec(IIf(IsDBNull(lector.Item("ppv_PUtilFijo")) = True, 0, lector.Item("ppv_PUtilFijo")))
                    obj.PUtilVariable = CDec(IIf(IsDBNull(lector.Item("ppv_PUtilVariable")) = True, 0, lector.Item("ppv_PUtilVariable")))
                    obj.Utilidad = CDec(IIf(IsDBNull(lector.Item("ppv_Utilidad")) = True, 0, lector.Item("ppv_Utilidad")))
                    obj.Valor = CDec(IIf(IsDBNull(lector.Item("ppv_Valor")) = True, 0, lector.Item("ppv_Valor")))
                    obj.IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                    obj.NomUMedida = CStr(IIf(IsDBNull(lector.Item("um_NombreCorto")) = True, "", lector.Item("um_NombreCorto")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("ppv_Estado")) = True, "1", lector.Item("ppv_Estado")))
                    obj.Equivalencia = CDec(IIf(IsDBNull(lector.Item("pum_Equivalencia")) = True, 0, lector.Item("pum_Equivalencia")))

                    obj.NomMonedaPC = CStr(IIf(IsDBNull(lector.Item("NomMonedaPC")) = True, "", lector.Item("NomMonedaPC")))

                    obj.UnidadPrincipal = CBool(IIf(IsDBNull(lector.Item("pum_UnidadPrincipal")) = True, False, lector.Item("pum_UnidadPrincipal")))
                    obj.Retazo = CBool(IIf(IsDBNull(lector.Item("pum_Retazo")) = True, False, lector.Item("pum_Retazo")))
                    obj.PorcentRetazo = CDec(IIf(IsDBNull(lector.Item("pum_PorcentRetazo")) = True, 0, lector.Item("pum_PorcentRetazo")))
                    obj.TipoPV = CStr(IIf(IsDBNull(lector.Item("TipoPrecio")) = True, "", lector.Item("TipoPrecio")))

                    obj.PV_Dolares = obj.Valor
                    obj.PV_Soles = obj.Valor

                    obj.IdTiendaPrincipal = CInt(IIf(IsDBNull(lector.Item("IdTiendaPrincipal")) = True, 0, lector.Item("IdTiendaPrincipal")))
                    obj.IdMonedaTiendaPrincipal = CInt(IIf(IsDBNull(lector.Item("IdMonedaTiendaPrincipal")) = True, 0, lector.Item("IdMonedaTiendaPrincipal")))
                    obj.PrecioTiendaPrincipal = CDec(IIf(IsDBNull(lector.Item("PrecioTiendaPrincipal")) = True, 0, lector.Item("PrecioTiendaPrincipal")))

                    Lista.Add(obj)


                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectUmPrincipalxIdProducto(ByVal idproducto As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As List(Of Entidades.ProductoTipoPV)
        Dim cmd As New SqlCommand("_ProductoTipoPVSelectUmPrincipalxIdProducto", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@IdProducto", idproducto)
        Dim lector As SqlDataReader
        Try
            lector = cmd.ExecuteReader()
            Dim Lista As New List(Of Entidades.ProductoTipoPV)
            Do While lector.Read
                Dim obj As New Entidades.ProductoTipoPV
                obj.IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                obj.IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                obj.IdTipoPv = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                obj.IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                obj.Valor = CDec(IIf(IsDBNull(lector.Item("ppv_Valor")) = True, 0, lector.Item("ppv_Valor")))
                obj.IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                obj.PrecioCompraxUMPrincipal = CDec(IIf(IsDBNull(lector.Item("prod_PrecioCompra")) = True, 0, lector.Item("prod_PrecioCompra")))
                obj.Equivalencia = CDec(IIf(IsDBNull(lector.Item("pum_Equivalencia")) = True, 0, lector.Item("pum_Equivalencia")))
                obj.Estado = CStr(IIf(IsDBNull(lector.Item("ppv_Estado")) = True, "1", lector.Item("ppv_Estado")))
                Lista.Add(obj)
            Loop
            lector.Close()
            Return Lista
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectxIdProducto(ByVal idproducto As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As List(Of Entidades.ProductoTipoPV)
        Dim cmd As New SqlCommand("_ProductoTipoPVSelectxIdProducto", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", idproducto)
        Dim lector As SqlDataReader
        Try
            lector = cmd.ExecuteReader()
            Dim Lista As New List(Of Entidades.ProductoTipoPV)
            Do While lector.Read
                Dim obj As New Entidades.ProductoTipoPV
                obj.IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                obj.IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                obj.IdTipoPv = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                obj.IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                obj.PUtilFijo = CDec(IIf(IsDBNull(lector.Item("ppv_PUtilFijo")) = True, 0, lector.Item("ppv_PUtilFijo")))
                obj.PUtilVariable = CDec(IIf(IsDBNull(lector.Item("ppv_PUtilVariable")) = True, 0, lector.Item("ppv_PUtilVariable")))
                obj.Valor = CDec(IIf(IsDBNull(lector.Item("ppv_Valor")) = True, 0, lector.Item("ppv_Valor")))
                obj.Utilidad = CDec(IIf(IsDBNull(lector.Item("ppv_Utilidad")) = True, 0, lector.Item("ppv_Utilidad")))
                obj.Equivalencia = CDec(IIf(IsDBNull(lector.Item("pum_Equivalencia")) = True, 0, lector.Item("pum_Equivalencia")))
                obj.PrecioCompraxUMPrincipal = CDec(IIf(IsDBNull(lector.Item("prod_PrecioCompra")) = True, 0, lector.Item("prod_PrecioCompra")))
                obj.Estado = CStr(IIf(IsDBNull(lector.Item("ppv_estado")) = True, "1", lector.Item("ppv_estado")))
                obj.UnidadPrincipal = CBool(IIf(IsDBNull(lector.Item("pum_UnidadPrincipal")) = True, False, lector.Item("pum_UnidadPrincipal")))
                obj.Retazo = CBool(IIf(IsDBNull(lector.Item("pum_Retazo")) = True, False, lector.Item("pum_Retazo")))
                obj.PorcentRetazo = CDec(IIf(IsDBNull(lector.Item("pum_PorcentRetazo")) = True, 0, lector.Item("pum_PorcentRetazo")))
                Lista.Add(obj)
            Loop
            lector.Close()
            Return Lista
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectxIdProductoxIdTiendaxIdTipoPV(ByVal idproducto As Integer, ByVal idtienda As Integer, ByVal idtipopv As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As List(Of Entidades.ProductoTipoPV)
        Dim cmd As New SqlCommand("_ProductoTipoPVSelectxIdProductoxIdTiendaxIdTipoPV_1", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", idproducto)
        cmd.Parameters.AddWithValue("@IdTienda", idtienda)
        cmd.Parameters.AddWithValue("@IdTipoPV", idtipopv)
        Dim lector As SqlDataReader
        Try
            lector = cmd.ExecuteReader()
            Dim Lista As New List(Of Entidades.ProductoTipoPV)
            Do While lector.Read
                Dim obj As New Entidades.ProductoTipoPV
                obj.IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                obj.IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                obj.IdTipoPv = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                obj.IdUnidadMedida = CInt(IIf(IsDBNull(lector.Item("IdUnidadMedida")) = True, 0, lector.Item("IdUnidadMedida")))
                obj.PUtilFijo = CDec(IIf(IsDBNull(lector.Item("ppv_PUtilFijo")) = True, 0, lector.Item("ppv_PUtilFijo")))
                obj.PUtilVariable = CDec(IIf(IsDBNull(lector.Item("ppv_PUtilVariable")) = True, 0, lector.Item("ppv_PUtilVariable")))
                obj.Valor = CDec(IIf(IsDBNull(lector.Item("ppv_Valor")) = True, 0, lector.Item("ppv_Valor")))
                obj.Utilidad = CDec(IIf(IsDBNull(lector.Item("ppv_Utilidad")) = True, 0, lector.Item("ppv_Utilidad")))
                obj.Equivalencia = CDec(IIf(IsDBNull(lector.Item("pum_Equivalencia")) = True, 0, lector.Item("pum_Equivalencia")))
                obj.PrecioCompraxUMPrincipal = CDec(IIf(IsDBNull(lector.Item("prod_PrecioCompra")) = True, 0, lector.Item("prod_PrecioCompra")))

                obj.IdMoneda = CInt(IIf(IsDBNull(lector.Item("IdMoneda")) = True, 0, lector.Item("IdMoneda")))
                obj.Estado = CStr(IIf(IsDBNull(lector.Item("ppv_Estado")) = True, "1", lector.Item("ppv_Estado")))

                obj.UnidadPrincipal = CBool(IIf(IsDBNull(lector.Item("pum_UnidadPrincipal")) = True, False, lector.Item("pum_UnidadPrincipal")))
                obj.Retazo = CBool(IIf(IsDBNull(lector.Item("pum_Retazo")) = True, False, lector.Item("pum_Retazo")))
                obj.PorcentRetazo = CDec(IIf(IsDBNull(lector.Item("pum_PorcentRetazo")) = True, 0, lector.Item("pum_PorcentRetazo")))
                Lista.Add(obj)
            Loop
            lector.Close()
            Return Lista
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectValorxIdTiendaxIdProdxIdUMxIdTipoPV(ByVal idproducto As Integer, ByVal idtienda As Integer, ByVal idtipopv As Integer, ByVal idunidadmedida As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Decimal
        Dim cmd As New SqlCommand("_ProductoTipoPVSelectValorxIdTiendaxIdProdxIdUMxIdTipoPV", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", idproducto)
        cmd.Parameters.AddWithValue("@IdTienda", idtienda)
        cmd.Parameters.AddWithValue("@IdTipoPV", idtipopv)
        cmd.Parameters.AddWithValue("@IdUnidadMedida", idunidadmedida)
        Dim lector As SqlDataReader
        Dim valor As Decimal = 0
        Try
            lector = cmd.ExecuteReader()
            If lector.Read Then
                valor = CDec(IIf(IsDBNull(lector.Item("ppv_Valor")) = True, 0, lector.Item("ppv_Valor")))
            End If
            lector.Close()
            Return valor
        Catch ex As Exception
            Throw ex
        End Try
    End Function






    Public Function SelectValorCalculadoxParams(ByVal idproducto As Integer, ByVal idtienda As Integer, ByVal idtipopv As Integer, ByVal idunidadmedida As Integer, ByVal idmoneda As Integer) As Decimal
        '***************************** Funcion utilizado para hallar el valor de venta adicionado el porcentaje retazo
        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Dim cmd As New SqlCommand("_ProductoTipoPVSelectValorCalculadoxParams", cn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@IdProducto", idproducto)
        cmd.Parameters.AddWithValue("@IdTienda", idtienda)
        cmd.Parameters.AddWithValue("@IdTipoPV", idtipopv)
        cmd.Parameters.AddWithValue("@IdUnidadMedida", idunidadmedida)
        cmd.Parameters.AddWithValue("@IdMoneda", idmoneda)

        Dim param As New SqlParameter("@Retorno", SqlDbType.Decimal)
        param.Direction = ParameterDirection.Output
        param.Scale = 6
        param.Precision = 18

        cmd.Parameters.Add(param)

        Try

            cn.Open()
            cmd.ExecuteScalar()

            Return CDec(IIf(IsDBNull(cmd.Parameters("@Retorno").Value) = True, 0, cmd.Parameters("@Retorno").Value))

        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function




    Public Function SelectValor_Equivalencia_VentaxParams_Cotizacion(ByVal IdUnidadMedida As Integer, ByVal IdProducto As Integer, ByVal IdTipoPV As Integer, ByVal IdTienda As Integer, ByVal IdMoneda As Integer, ByVal IdAlmacen As Integer, ByVal IdEmpresa As Integer, ByVal Fecha As Date, ByVal cantidad As Decimal, ByVal IdUMold As Integer) As Entidades.Catalogo

        Dim objCatalogo As Entidades.Catalogo = Nothing

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoTipoPVSelectValor_Equivalencia_VentaxParams_Cotizacion", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdUnidadMedida", IdUnidadMedida)
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        cmd.Parameters.AddWithValue("@IdTipoPV", IdTipoPV)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdMoneda", IdMoneda)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@Fecha", Fecha)
        cmd.Parameters.AddWithValue("@cantidad", cantidad)
        cmd.Parameters.AddWithValue("@IdUnidaMedidaOld", IdUMold)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)

                If lector.Read Then

                    objCatalogo = New Entidades.Catalogo
                    objCatalogo.PrecioSD = CDec(IIf(IsDBNull(lector.Item("PrecioLista")) = True, 0, lector.Item("PrecioLista")))
                    objCatalogo.PrecioLista = CDec(IIf(IsDBNull(lector.Item("PrecioLista")) = True, 0, lector.Item("PrecioLista")))
                    objCatalogo.StockDisponibleN = CDec(IIf(IsDBNull(lector.Item("StockDisponible_Eq")) = True, 0, lector.Item("StockDisponible_Eq")))
                    objCatalogo.Cantidad = CDec(IIf(IsDBNull(lector.Item("CantidadNew")) = True, 0, lector.Item("CantidadNew")))
                End If

                lector.Close()
                Return objCatalogo
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function SelectValor_Equivalencia_VentaxParams_Cotizacion2(ByVal IdUnidadMedida As Integer, ByVal IdProducto As Integer, ByVal IdTipoPV As Integer, ByVal IdTienda As Integer, ByVal IdMoneda As Integer, ByVal IdAlmacen As Integer, ByVal IdEmpresa As Integer, ByVal Fecha As Date, ByVal cantidad As Decimal, ByVal IdUMold As Integer) As Entidades.Catalogo

        Dim objCatalogo As Entidades.Catalogo = Nothing

        Dim cn As SqlConnection = objConexion.ConexionSIGE2
        Dim cmd As New SqlCommand("_ProductoTipoPVSelectValor_Equivalencia_VentaxParams_Cotizacion", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdUnidadMedida", IdUnidadMedida)
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        cmd.Parameters.AddWithValue("@IdTipoPV", IdTipoPV)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdMoneda", IdMoneda)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@Fecha", Fecha)
        cmd.Parameters.AddWithValue("@cantidad", cantidad)
        cmd.Parameters.AddWithValue("@IdUnidaMedidaOld", IdUMold)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)

                If lector.Read Then

                    objCatalogo = New Entidades.Catalogo
                    objCatalogo.PrecioSD = CDec(IIf(IsDBNull(lector.Item("PrecioLista")) = True, 0, lector.Item("PrecioLista")))
                    objCatalogo.PrecioLista = CDec(IIf(IsDBNull(lector.Item("PrecioLista")) = True, 0, lector.Item("PrecioLista")))
                    objCatalogo.StockDisponibleN = CDec(IIf(IsDBNull(lector.Item("StockDisponible_Eq")) = True, 0, lector.Item("StockDisponible_Eq")))
                    objCatalogo.Cantidad = CDec(IIf(IsDBNull(lector.Item("CantidadNew")) = True, 0, lector.Item("CantidadNew")))
                End If

                lector.Close()
                Return objCatalogo
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function



End Class