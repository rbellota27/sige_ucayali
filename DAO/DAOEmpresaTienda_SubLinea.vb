﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'******************** MARTES 19 MAYO 2010 HORA 11_35 AM

Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAOEmpresaTienda_SubLinea
    Dim objconexion As New DAO.Conexion
    Private cn As SqlConnection
    Private reader As SqlDataReader
    Private objDaoMantenedor As New DAO.DAOMantenedor

    Public Function DeletexParams(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdSubLinea As Integer) As Boolean

        Dim hecho As Boolean = False

        Try

            Dim p() As SqlParameter = New SqlParameter(2) {}
            p(0) = objDaoMantenedor.getParam(IdSubLinea, "@IdSubLinea", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)


            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            SqlHelper.ExecuteNonQuery(cn, CommandType.StoredProcedure, "_EmpresaTienda_SubLinea_DeletexParams", p)

            hecho = True

        Catch ex As Exception
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function

    Public Function SelectxParams_DT(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer) As DataTable

        Dim ds As New DataSet

        Try

            cn = (New DAO.Conexion).ConexionSIGE
            Dim cmd As New SqlCommand("_EmpresaTienda_SubLinea_SelectxParams", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
            cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
            cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
            cmd.Parameters.AddWithValue("@IdSubLinea", IdSubLinea)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_EmpresaTienda_SubLinea")

        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return ds.Tables("DT_EmpresaTienda_SubLinea")

    End Function

    Public Function SelectxIdEmpresaxIdTiendaxIdSubLinea(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdSubLinea As Integer) As Entidades.EmpresaTienda_SubLinea

        Dim objEmpresaTiendaS As Entidades.EmpresaTienda_SubLinea = Nothing

        Try

            Dim p() As SqlParameter = New SqlParameter(3) {}
            p(0) = objDaoMantenedor.getParam(IdSubLinea, "@IdSubLinea", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(0, "@IdLinea", SqlDbType.Int)

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()
            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_EmpresaTienda_SubLinea_SelectxParams", p)

            If (reader.Read) Then

                objEmpresaTiendaS = New Entidades.EmpresaTienda_SubLinea
                With objEmpresaTiendaS

                    .IdEmpresa = objDaoMantenedor.UCInt(reader("IdEmpresa"))
                    .IdTienda = objDaoMantenedor.UCInt(reader("IdTienda"))
                    .IdSubLinea = objDaoMantenedor.UCInt(reader("IdSubLinea"))
                    .CtaDebeCompra = objDaoMantenedor.UCStr(reader("ets_CtaDebeCompra"))
                    .CtaCostoCompra = objDaoMantenedor.UCStr(reader("ets_CtaCostoCompra"))
                    .CtaDebeVenta = objDaoMantenedor.UCStr(reader("ets_CtaDebeVenta"))
                    .CtaHaberVenta = objDaoMantenedor.UCStr(reader("ets_CtaHaberVenta"))
                    .CtaCostoVenta = objDaoMantenedor.UCStr(reader("ets_CtaCostoVenta"))
                    .CtaHaberCompra = objDaoMantenedor.UCStr(reader("ets_CtaHaberCompra"))
                    .IdLinea = objDaoMantenedor.UCInt(reader("IdLinea"))

                End With

            End If
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objEmpresaTiendaS

    End Function

    Public Sub EmpresaTienda_SubLinea_Registrar(ByVal objETS As Entidades.EmpresaTienda_SubLinea, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(8) {}
        p(0) = objDaoMantenedor.getParam(objETS.IdEmpresa, "@IdEmpresa", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(objETS.IdTienda, "@IdTienda", SqlDbType.Int)
        p(2) = objDaoMantenedor.getParam(objETS.IdSubLinea, "@IdSubLinea", SqlDbType.Int)
        p(3) = objDaoMantenedor.getParam(objETS.CtaDebeCompra, "@ets_CtaDebeCompra", SqlDbType.VarChar)
        p(4) = objDaoMantenedor.getParam(objETS.CtaHaberCompra, "@ets_CtaHaberCompra", SqlDbType.VarChar)
        p(5) = objDaoMantenedor.getParam(objETS.CtaCostoCompra, "@ets_CtaCostoCompra", SqlDbType.VarChar)
        p(6) = objDaoMantenedor.getParam(objETS.CtaDebeVenta, "@ets_CtaDebeVenta", SqlDbType.VarChar)
        p(7) = objDaoMantenedor.getParam(objETS.CtaHaberVenta, "@ets_CtaHaberVenta", SqlDbType.VarChar)
        p(8) = objDaoMantenedor.getParam(objETS.CtaCostoVenta, "@ets_CtaCostoVenta", SqlDbType.VarChar)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_EmpresaTienda_SubLinea_Registrar", p)

    End Sub

End Class
