'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.



'*********************   VIERNES 14 MAYO 2010 HORA 04_38 PM


Imports System.Data.SqlClient
Public Class DAOCaja
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

    Private objDaoMantenedor As New DAO.DAOMantenedor

    Public Function SelectResumenxParams(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdCaja As Integer, ByVal IdMedioPago As Integer) As DataTable

        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE
        Try


            Dim p() As SqlParameter = New SqlParameter(3) {}
            p(0) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdMedioPago, "@IdMedioPago", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(IdCaja, "@IdCaja", SqlDbType.Int)

            Dim cmd As New SqlCommand("_Caja_SelectResumenxParams", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Caja_Resumen")

        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return ds.Tables("DT_Caja_Resumen")

    End Function


    Public Sub InsertaCajaT(ByVal cn As SqlConnection, ByVal lcaja As List(Of Entidades.Caja), ByVal T As SqlTransaction)
        Dim TiendaCaja As Entidades.Caja
        For Each TiendaCaja In lcaja
            Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
            ArrayParametros(0) = New SqlParameter("@caja_Nombre", SqlDbType.VarChar)
            ArrayParametros(0).Value = TiendaCaja.Nombre
            ArrayParametros(1) = New SqlParameter("@caja_Estado", SqlDbType.Char)
            ArrayParametros(1).Value = TiendaCaja.Estado
            ArrayParametros(2) = New SqlParameter("@IdTienda", SqlDbType.Int)
            ArrayParametros(2).Value = TiendaCaja.IdTienda
            ArrayParametros(3) = New SqlParameter("@caja_numero", SqlDbType.VarChar)
            ArrayParametros(3).Value = TiendaCaja.CajaNumero
            HDAO.InsertaT(cn, "_CajaInsert", ArrayParametros, T)
        Next
    End Sub

    Public Sub GrabaCajaT(ByVal cn As SqlConnection, ByVal lcaja As List(Of Entidades.Caja), ByVal T As SqlTransaction)
        Dim TiendaCaja As Entidades.Caja
        For Each TiendaCaja In lcaja
            Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
            ArrayParametros(0) = New SqlParameter("@IdCaja", SqlDbType.Int)
            ArrayParametros(0).Value = TiendaCaja.IdCaja
            ArrayParametros(1) = New SqlParameter("@caja_Nombre", SqlDbType.VarChar)
            ArrayParametros(1).Value = TiendaCaja.Nombre
            ArrayParametros(2) = New SqlParameter("@caja_Estado", SqlDbType.Char, 1)
            ArrayParametros(2).Value = TiendaCaja.Estado
            ArrayParametros(3) = New SqlParameter("@IdTienda", SqlDbType.Int)
            ArrayParametros(3).Value = TiendaCaja.IdTienda
            ArrayParametros(4) = New SqlParameter("@caja_numero", SqlDbType.VarChar)
            ArrayParametros(4).Value = TiendaCaja.CajaNumero
            ArrayParametros(5) = New SqlParameter("@caja_chica", SqlDbType.Bit)
            ArrayParametros(5).Value = TiendaCaja.Caja_Chica


            HDAO.InsertaT(cn, "InsUpd_Caja", ArrayParametros, T)
        Next
    End Sub


    Public Function DeletexIdTienda(ByVal cn As SqlConnection, ByVal IdTienda As Integer, ByVal T As SqlTransaction) As Boolean
        Dim cmd As New SqlCommand("_TiendaCajaDeleteAllByIdTienda", cn, T)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        Try
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function ActualizaCaja(ByVal caja As Entidades.Caja) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@caja_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = caja.Nombre
        ArrayParametros(1) = New SqlParameter("@caja_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = caja.Estado
        ArrayParametros(2) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(2).Value = caja.IdTienda
        ArrayParametros(3) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(3).Value = caja.IdEmpresa
        ArrayParametros(4) = New SqlParameter("@IdCaja", SqlDbType.Int)
        ArrayParametros(4).Value = caja.IdCaja
        Return HDAO.Update(cn, "_CajaUpdate", ArrayParametros)
    End Function
    Public Function SelectCboxIdTienda(ByVal idtienda As Integer) As List(Of Entidades.Caja)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CajaSelectCboxIdTienda", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTienda", idtienda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Caja)
                Do While lector.Read
                    Dim obj As New Entidades.Caja
                    With obj
                        .IdCaja = CInt(IIf(IsDBNull(lector.Item("IdCaja")) = True, 0, lector.Item("IdCaja")))
                        .Nombre = CStr(IIf(IsDBNull(lector.Item("caja_Nombre")) = True, "", lector.Item("caja_Nombre")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectIdCajaxIdPersona(ByVal IdPersona As Integer) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CajaSelectIdCajaxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                lector.Read()
                If lector.HasRows = False Then
                    Return CInt(0)
                End If
                Dim IdCaja As Integer = CInt(IIf(IsDBNull(lector.Item("IdCaja")) = True, 0, lector.Item("IdCaja")))
                Return IdCaja
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.Caja)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CajaSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Caja)
                Do While lector.Read
                    Dim caja As New Entidades.Caja
                    caja.IdCaja = CInt(IIf(IsDBNull(lector.Item("IdCaja")) = True, 0, lector.Item("IdCaja")))
                    caja.IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                    caja.Nombre = CStr(IIf(IsDBNull(lector.Item("caja_Nombre")) = True, "", lector.Item("caja_Nombre")))
                    caja.Estado = CStr(lector.Item("caja_Estado"))
                    Lista.Add(caja)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectGrillaxIdTienda(ByVal idtienda As Integer) As List(Of Entidades.Caja)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CajaSelectGrillaxIdTienda", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTienda", idtienda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Caja)
                Do While lector.Read
                    Dim obj As New Entidades.Caja
                    With obj
                        .IdCaja = CInt(IIf(IsDBNull(lector.Item("IdCaja")) = True, 0, lector.Item("IdCaja")))
                        .Nombre = CStr(IIf(IsDBNull(lector.Item("caja_Nombre")) = True, "", lector.Item("caja_Nombre")))
                        .CajaNumero = CStr(IIf(IsDBNull(lector.Item("caja_Numero")) = True, "", lector.Item("caja_Numero")))
                        .EstadoTC = CBool(IIf(IsDBNull(lector.Item("caja_Estado")) = True, 1, lector.Item("caja_Estado")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCajaxId(ByVal IdCaja As Integer) As Entidades.Caja
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CajaSelectId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdCaja", IdCaja)
        Dim lector As SqlDataReader
        Using cn
            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
            Dim objCaja As New Entidades.Caja
            If lector.Read Then
                objCaja = New Entidades.Caja
                objCaja.IdCaja = CInt(IIf(IsDBNull(lector.Item("IdCaja")) = True, 0, lector.Item("IdCaja")))
                objCaja.IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                objCaja.Nombre = CStr(IIf(IsDBNull(lector.Item("caja_Nombre")) = True, "", lector.Item("caja_Nombre")))
                objCaja.Estado = CStr(lector.Item("caja_Estado"))
                objCaja.CajaNumero = CStr(IIf(IsDBNull(lector.Item("caja_Numero")) = True, "", lector.Item("caja_Numero")))
            End If
            lector.Close()
            Return objCaja
        End Using
    End Function

    Public Function SelectAllActivoInactivo() As List(Of Entidades.Caja)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CajaSelectAllActivoInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Caja)
                Do While lector.Read
                    Dim caja As New Entidades.Caja
                    caja.IdCaja = CInt(IIf(IsDBNull(lector.Item("IdCaja")) = True, 0, lector.Item("IdCaja")))
                    caja.IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                    caja.Nombre = CStr(IIf(IsDBNull(lector.Item("caja_Nombre")) = True, "", lector.Item("caja_Nombre")))
                    caja.CajaNumero = CStr(IIf(IsDBNull(lector.Item("caja_Numero")) = True, "", lector.Item("caja_Numero")))
                    caja.NomTienda = CStr(IIf(IsDBNull(lector.Item("NomTienda")) = True, "", lector.Item("NomTienda")))
                    caja.Estado = CStr(lector.Item("caja_Estado"))
                    caja.Caja_Chica = CBool(lector.Item("Caja_Chica"))
                    Lista.Add(caja)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function


    Public Function getDataSet_LiquidacionCaja(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdCaja As Integer, ByVal FechaIni As String, ByVal FechaFin As String) As DataSet


        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Dim cn As New SqlConnection
        cn = objConexion.ConexionSIGE

        Dim cmd As New SqlCommand("_CR_LiquidacionCaja", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdCaja", IdCaja)

        cmd.Parameters.AddWithValue("@FechaIni", FechaIni)
        cmd.Parameters.AddWithValue("@FechaFin", FechaFin)


        Try

            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "LiquidacionCaja")

        Catch ex As Exception
            ds = Nothing
        Finally

        End Try

        Return ds

    End Function

End Class
