﻿Imports System.Data.SqlClient
Public Class dao_test
    Public Function dao_test(cn As SqlConnection)
        Dim lista As List(Of Entidades.be_usuario) = Nothing
        Using cmd As New SqlCommand("SP_TEST", cn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_usuario)
                Dim ordinal1 As Integer = rdr.GetOrdinal("idPersona")
                Dim ordinal2 As Integer = rdr.GetOrdinal("nombre")
                Dim ordinal3 As Integer = rdr.GetOrdinal("dni")
                Dim ordinal4 As Integer = rdr.GetOrdinal("ruc")
                Dim ordinal5 As Integer = rdr.GetOrdinal("dir_direccion")
                Dim objeto As Entidades.be_usuario
                While rdr.Read()
                    objeto = New Entidades.be_usuario
                    With objeto
                        .IdPersona = rdr.GetInt32(ordinal1)
                        .Nombre = rdr.GetString(ordinal2)
                        .Dni = rdr.GetString(ordinal3)
                        .Ruc = rdr.GetString(ordinal4)
                        .Direccion = rdr.GetString(ordinal5)
                    End With
                    lista.Add(objeto)
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function
End Class
