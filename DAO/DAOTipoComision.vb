﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************** LUNES 18 ENERO 2010 HORA 11_51 AM

Imports System.Data.SqlClient
Public Class DAOTipoComision
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectAllActivo() As List(Of Entidades.TipoComision)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoComisionSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoComision)
                Do While lector.Read
                    Dim objTipoComision As New Entidades.TipoComision
                    objTipoComision.Nombre = CStr(IIf(IsDBNull(lector.Item("tcom_Nombre")) = True, "", lector.Item("tcom_Nombre")))
                    objTipoComision.Estado = CBool(IIf(IsDBNull(lector.Item("tcom_Estado")) = True, "", lector.Item("tcom_Estado")))
                    objTipoComision.IdTipoComision = CInt(IIf(IsDBNull(lector.Item("IdTipoComision")) = True, 0, lector.Item("IdTipoComision")))
                    Lista.Add(objTipoComision)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.TipoComision)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoComisionSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoComision)
                Do While lector.Read
                    Dim objTipoComision As New Entidades.TipoComision
                    objTipoComision.Nombre = CStr(IIf(IsDBNull(lector.Item("tcom_Nombre")) = True, "", lector.Item("tcom_Nombre")))
                    objTipoComision.Estado = CBool(IIf(IsDBNull(lector.Item("tcom_Estado")) = True, "", lector.Item("tcom_Estado")))
                    objTipoComision.IdTipoComision = CInt(IIf(IsDBNull(lector.Item("IdTipoComision")) = True, 0, lector.Item("IdTipoComision")))
                    Lista.Add(objTipoComision)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.TipoComision)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoComisionSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoComision)
                Do While lector.Read
                    Dim objTipoComision As New Entidades.TipoComision
                    objTipoComision.Nombre = CStr(IIf(IsDBNull(lector.Item("tcom_Nombre")) = True, "", lector.Item("tcom_Nombre")))
                    objTipoComision.Estado = CBool(IIf(IsDBNull(lector.Item("tcom_Estado")) = True, "", lector.Item("tcom_Estado")))
                    objTipoComision.IdTipoComision = CInt(IIf(IsDBNull(lector.Item("IdTipoComision")) = True, 0, lector.Item("IdTipoComision")))
                    Lista.Add(objTipoComision)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function InsertaTipoComision(ByVal TipoComision As Entidades.TipoComision) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@tcom_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = TipoComision.Nombre
        ArrayParametros(1) = New SqlParameter("@tcom_Estado", SqlDbType.Bit)
        ArrayParametros(1).Value = TipoComision.Estado
        Return HDAO.Insert(cn, "_TipoComisionInsert", ArrayParametros)
    End Function
    Public Function ActualizaTipoComision(ByVal TipoComision As Entidades.TipoComision) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@tcom_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = TipoComision.Nombre
        ArrayParametros(1) = New SqlParameter("@tcom_Estado", SqlDbType.Bit)
        ArrayParametros(1).Value = TipoComision.Estado
        ArrayParametros(2) = New SqlParameter("@IdTipoComision", SqlDbType.Int)
        ArrayParametros(2).Value = TipoComision.IdTipoComision
        Return HDAO.Update(cn, "_TipoComisionUpdate", ArrayParametros)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.TipoComision)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoComisionSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoComision)
                Do While lector.Read
                    Dim objTipoComision As New Entidades.TipoComision
                    objTipoComision.Nombre = CStr(IIf(IsDBNull(lector.Item("tcom_Nombre")) = True, "", lector.Item("tcom_Nombre")))
                    objTipoComision.Estado = CBool(IIf(IsDBNull(lector.Item("tcom_Estado")) = True, "", lector.Item("tcom_Estado")))
                    objTipoComision.IdTipoComision = CInt(IIf(IsDBNull(lector.Item("IdTipoComision")) = True, 0, lector.Item("IdTipoComision")))
                    Lista.Add(objTipoComision)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.TipoComision)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoComisionSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoComision)
                Do While lector.Read
                    Dim objTipoComision As New Entidades.TipoComision
                    objTipoComision.Nombre = CStr(IIf(IsDBNull(lector.Item("tcom_Nombre")) = True, "", lector.Item("tcom_Nombre")))
                    objTipoComision.Estado = CBool(IIf(IsDBNull(lector.Item("tcom_Estado")) = True, "", lector.Item("tcom_Estado")))
                    objTipoComision.IdTipoComision = CInt(IIf(IsDBNull(lector.Item("IdTipoComision")) = True, 0, lector.Item("IdTipoComision")))
                    Lista.Add(objTipoComision)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoComision)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoComisionSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoComision)
                Do While lector.Read
                    Dim objTipoComision As New Entidades.TipoComision
                    objTipoComision.Nombre = CStr(IIf(IsDBNull(lector.Item("tcom_Nombre")) = True, "", lector.Item("tcom_Nombre")))
                    objTipoComision.Estado = CBool(IIf(IsDBNull(lector.Item("tcom_Estado")) = True, "", lector.Item("tcom_Estado")))
                    objTipoComision.IdTipoComision = CInt(IIf(IsDBNull(lector.Item("IdTipoComision")) = True, 0, lector.Item("IdTipoComision")))
                    Lista.Add(objTipoComision)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.TipoComision)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoComisionSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.TipoComision)
                Do While lector.Read
                    Dim objTipoComision As New Entidades.TipoComision
                    objTipoComision.Nombre = CStr(IIf(IsDBNull(lector.Item("tcom_Nombre")) = True, "", lector.Item("tcom_Nombre")))
                    objTipoComision.Estado = CBool(IIf(IsDBNull(lector.Item("tcom_Estado")) = True, "", lector.Item("tcom_Estado")))
                    objTipoComision.IdTipoComision = CInt(IIf(IsDBNull(lector.Item("IdTipoComision")) = True, 0, lector.Item("IdTipoComision")))
                    Lista.Add(objTipoComision)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class