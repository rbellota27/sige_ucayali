﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'29-01-2010 2:06 pm Se agregó como parametro la lista lRolEmpresa a la funcion 
'InsertaConfiguracionEmpresa

Imports System.Data.SqlClient
Public Class DAOEmpresa
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaConfiguracionEmpresa( _
    ByVal IdEmpresa As Integer, ByVal lEmpresaTienda As List(Of Entidades.EmpresaTienda), _
    ByVal lEmpresaAlmacen As List(Of Entidades.EmpresaAlmacen), _
    ByVal lAreaEmpresa As List(Of Entidades.AreaEmpresa), _
    ByVal lRolEmpresa As List(Of Entidades.Rol_Empresa)) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        cn.Open()
        Dim T As SqlTransaction = cn.BeginTransaction(Data.IsolationLevel.Serializable)
        Try
            '***Agregar Empresa Tienda
            Dim daoEmpresaTienda As New DAO.DAOEmpresaTienda
            'daoEmpresaTienda.DeletexIdEmpresa(cn, IdEmpresa, T)
            'daoEmpresaTienda.InsertaEmpresaTiendaT(cn, lEmpresaTienda, T)
            daoEmpresaTienda.GrabaEmpresaTiendaT(cn, lEmpresaTienda, T)
            '***Agregar Empresa Almacen
            Dim daoEmpresaAlmacen As New DAO.DAOEmpresaAlmacen
            'daoEmpresaAlmacen.DeletexIdEmpresa(cn, IdEmpresa, T)
            'daoEmpresaAlmacen.InsertaEmpresaAlmacenT(cn, lEmpresaAlmacen, T)
            daoEmpresaAlmacen.GrabaEmpresaAlmacenT(cn, lEmpresaAlmacen, T)
            '***Agregar Empresa Area
            Dim daoAreaEmpresa As New DAO.DAOAreaEmpresa
            'daoAreaEmpresa.DeletexIdEmpresa(cn, IdEmpresa, T)
            'daoAreaEmpresa.InsertaAreaEmpresaT(cn, lAreaEmpresa, T)
            daoAreaEmpresa.GrabaAreaEmpresaT(cn, lAreaEmpresa, T)

            Dim daoRolEmpresa As New DAO.DAORol_Empresa
            daoRolEmpresa.GrabaRol_EmpresaT(cn, lRolEmpresa, T)

            T.Commit()
            Return True
        Catch ex As Exception
            T.Rollback()
            Return False
        End Try
    End Function
End Class
