'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'29-01-2010 01:56 pm GrabaRol_EmpresaT
'29-01-2010 11:22 am Se agreg� Actualiza, SelectxIdEmpresa y se agrego estado al Inserta
Imports System.Data.SqlClient

Public Class DAORol_Empresa
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaRol_Empresa(ByVal rol_empresa As Entidades.Rol_Empresa) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(0).Value = rol_empresa.IdEmpresa
        ArrayParametros(1) = New SqlParameter("@IdRol", SqlDbType.Int)
        ArrayParametros(1).Value = rol_empresa.IdRol
        ArrayParametros(2) = New SqlParameter("@re_estado", SqlDbType.Bit)
        ArrayParametros(2).Value = rol_empresa.Estado

        Return HDAO.Insert(cn, "_Rol_EmpresaInsert", ArrayParametros)
    End Function

    Public Function ActualizaRol_Empresa(ByVal rol_empresa As Entidades.Rol_Empresa) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(0).Value = rol_empresa.IdEmpresa
        ArrayParametros(1) = New SqlParameter("@IdRol", SqlDbType.Int)
        ArrayParametros(1).Value = rol_empresa.IdRol
        ArrayParametros(2) = New SqlParameter("@re_estado", SqlDbType.Bit)
        ArrayParametros(2).Value = rol_empresa.Estado

        Return HDAO.Insert(cn, "_Rol_EmpresaUpdate", ArrayParametros)
    End Function

    Public Function SelectxIdEmpresa(ByVal IdEmpresa As Integer) As List(Of Entidades.Rol_Empresa)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Rol_EmpresaSelectxIdEmpresa", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Rol_Empresa)
                Do While lector.Read
                    Dim obj As New Entidades.Rol_Empresa
                    obj.IdEmpresa = IdEmpresa
                    obj.IdRol = CInt(lector.Item("IdRol"))
                    obj.Rol = CStr(lector.Item("rol_Nombre"))
                    obj.Estado = CBool(lector.Item("re_estado"))

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub GrabaRol_EmpresaT(ByVal cn As SqlConnection, ByVal lRolEmpresa As List(Of Entidades.Rol_Empresa), ByVal T As SqlTransaction)
        Dim RolEmpresa As Entidades.Rol_Empresa
        For Each RolEmpresa In lRolEmpresa
            Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
            ArrayParametros(0) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
            ArrayParametros(0).Value = RolEmpresa.IdEmpresa
            ArrayParametros(1) = New SqlParameter("@IdRol", SqlDbType.Int)
            ArrayParametros(1).Value = RolEmpresa.IdRol
            ArrayParametros(2) = New SqlParameter("@Estado", SqlDbType.Bit)
            ArrayParametros(2).Value = RolEmpresa.Estado
            HDAO.InsertaT(cn, "_Rol_EmpresaInsert_or_Update", ArrayParametros, T)
        Next
    End Sub

End Class
