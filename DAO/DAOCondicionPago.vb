'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'********************************************************
'Autor   : Chang Carnero, Edgar
'M�dulo  : Concepto
'Sistema : Indusfer
'Empresa : Digrafic SRL
'Fecha   : 30-Oct-2009
'********************************************************
Imports System.Data.SqlClient

Public Class DAOCondicionPago
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaCondicionPago(ByVal condicionpago As Entidades.CondicionPago) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@cp_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = condicionpago.Nombre
        ArrayParametros(1) = New SqlParameter("@cp_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = condicionpago.Estado
        Return HDAO.Insert(cn, "_CondicionPagoInsert", ArrayParametros)
    End Function
    Public Function ActualizaCondicionPago(ByVal condicionpago As Entidades.CondicionPago) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@cp_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = condicionpago.Nombre
        ArrayParametros(1) = New SqlParameter("@cp_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = condicionpago.Estado
        ArrayParametros(2) = New SqlParameter("@IdCondicionPago", SqlDbType.Int)
        ArrayParametros(2).Value = condicionpago.Id
        Return HDAO.Update(cn, "_CondicionPagoUpdate", ArrayParametros)
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.CondicionPago)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondicionPagoSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CondicionPago)
                Do While lector.Read
                    Dim CondicionPago As New Entidades.CondicionPago
                    CondicionPago.Id = CInt(lector.Item("IdCondicionPago"))
                    CondicionPago.Nombre = CStr(lector.Item("cp_Nombre"))
                    CondicionPago.Estado = CStr(lector.Item("cp_Estado"))
                    Lista.Add(CondicionPago)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.CondicionPago)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondicionPagoSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CondicionPago)
                Do While lector.Read
                    Dim CondicionPago As New Entidades.CondicionPago
                    CondicionPago.Id = CInt(lector.Item("IdCondicionPago"))
                    CondicionPago.Nombre = CStr(lector.Item("cp_Nombre"))
                    CondicionPago.Estado = CStr(lector.Item("cp_Estado"))
                    Lista.Add(CondicionPago)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.CondicionPago)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondicionPagoSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CondicionPago)
                Do While lector.Read
                    Dim CondicionPago As New Entidades.CondicionPago
                    CondicionPago.Id = CInt(lector.Item("IdCondicionPago"))
                    CondicionPago.Nombre = CStr(lector.Item("cp_Nombre"))
                    CondicionPago.Estado = CStr(lector.Item("cp_Estado"))
                    Lista.Add(CondicionPago)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.CondicionPago)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondicionPagoSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CondicionPago)
                Do While lector.Read
                    Dim CondicionPago As New Entidades.CondicionPago
                    CondicionPago.Id = CInt(lector.Item("IdCondicionPago"))
                    CondicionPago.Nombre = CStr(lector.Item("cp_Nombre"))
                    CondicionPago.Estado = CStr(lector.Item("cp_Estado"))
                    Lista.Add(CondicionPago)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.CondicionPago)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondicionPagoSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CondicionPago)
                Do While lector.Read
                    Dim CondicionPago As New Entidades.CondicionPago
                    CondicionPago.Id = CInt(lector.Item("IdCondicionPago"))
                    CondicionPago.Nombre = CStr(lector.Item("cp_Nombre"))
                    CondicionPago.Estado = CStr(lector.Item("cp_Estado"))
                    Lista.Add(CondicionPago)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.CondicionPago)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondicionPagoSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CondicionPago)
                Do While lector.Read
                    Dim CondicionPago As New Entidades.CondicionPago
                    CondicionPago.Id = CInt(lector.Item("IdCondicionPago"))
                    CondicionPago.Nombre = CStr(lector.Item("cp_Nombre"))
                    CondicionPago.Estado = CStr(lector.Item("cp_Estado"))
                    Lista.Add(CondicionPago)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.CondicionPago)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondicionPagoSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CondicionPago)
                Do While lector.Read
                    Dim CondicionPago As New Entidades.CondicionPago
                    CondicionPago.Id = CInt(lector.Item("IdCondicionPago"))
                    CondicionPago.Nombre = CStr(lector.Item("cp_Nombre"))
                    CondicionPago.Estado = CStr(lector.Item("cp_Estado"))
                    Lista.Add(CondicionPago)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function InsertaMedioPago_CP(ByVal CondicionPago As Entidades.CondicionPago, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer
        'Inserta solo Medio Pago
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@cp_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = CondicionPago.Nombre
        ArrayParametros(1) = New SqlParameter("@cp_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = CondicionPago.Estado
        ArrayParametros(2) = New SqlParameter("@cp_IdCondicionPago", SqlDbType.Int)
        ArrayParametros(2).Direction = ParameterDirection.Output
        Dim cmd As New SqlCommand("_CondicionPagoInsertxMedioP", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        cmd.ExecuteNonQuery()
        Return CInt(cmd.Parameters("@cp_IdCondicionPago").Value)
    End Function
    Public Function ActualizaMedioPago_condicionP(ByVal CondicionPago As Entidades.CondicionPago, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@cp_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = CondicionPago.Nombre
        ArrayParametros(1) = New SqlParameter("@cp_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = CondicionPago.Estado
        ArrayParametros(2) = New SqlParameter("@IdCondicionPago", SqlDbType.Int)
        ArrayParametros(2).Value = IIf(CondicionPago.Id = Nothing, DBNull.Value, CondicionPago.Id)
        Dim cmd As New SqlCommand("_CondicionPagoUpdate", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        If (cmd.ExecuteNonQuery = 0) Then
            Return False
        End If
        Return True
    End Function
End Class
