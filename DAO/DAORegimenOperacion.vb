'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAORegimenOperacion
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaRegimenOperacion(ByVal RegimenOperacion As Entidades.RegimenOperacion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@ro_Id", SqlDbType.VarChar)
        ArrayParametros(0).Value = RegimenOperacion.IdRegimen
        ArrayParametros(1) = New SqlParameter("@ro_CuentaContable", SqlDbType.VarChar)
        ArrayParametros(1).Value = regimenoperacion.CuentaContable
        ArrayParametros(2) = New SqlParameter("@ro_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = regimenoperacion.Estado
        ArrayParametros(3) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        ArrayParametros(3).Value = regimenoperacion.IdTipoOperacion
        Return HDAO.Insert(cn, "_RegimenOperacionInsert", ArrayParametros)
    End Function
    Public Function ActualizaRegimenOperacion(ByVal RegimenOperacion As Entidades.RegimenOperacion) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@ro_Id", SqlDbType.VarChar)
        ArrayParametros(0).Value = RegimenOperacion.IdRegimen
        ArrayParametros(1) = New SqlParameter("@ro_CuentaContable", SqlDbType.VarChar)
        ArrayParametros(1).Value = regimenoperacion.CuentaContable
        ArrayParametros(2) = New SqlParameter("@ro_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = regimenoperacion.Estado
        ArrayParametros(3) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        ArrayParametros(3).Value = RegimenOperacion.IdTipoOperacion
        Return HDAO.Update(cn, "_RegimenOperacionUpdate", ArrayParametros)
    End Function
    Public Function SelectAll() As List(Of Entidades.RegimenOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RegimenOperacionSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.RegimenOperacion)
                Do While lector.Read
                    Dim RegimenOperacion As New Entidades.RegimenOperacion
                    RegimenOperacion.IdRegimen = CInt(lector.Item("ro_id"))
                    RegimenOperacion.CuentaContable = CStr(IIf(IsDBNull(lector.Item("ro_cuentacontable")) = True, "", lector.Item("ro_cuentacontable")))
                    RegimenOperacion.Estado = CStr(lector.Item("ro_Estado"))
                    RegimenOperacion.IdTipoOperacion = CInt(lector.Item("idTipoOperacion"))

                    Lista.Add(RegimenOperacion)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.RegimenOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RegimenOperacionSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.RegimenOperacion)
                Do While lector.Read
                    Dim RegimenOperacion As New Entidades.RegimenOperacion
                    RegimenOperacion.IdRegimen = CInt(lector.Item("ro_id"))
                    RegimenOperacion.CuentaContable = CStr(IIf(IsDBNull(lector.Item("ro_cuentacontable")) = True, "", lector.Item("ro_cuentacontable")))
                    RegimenOperacion.Estado = CStr(lector.Item("ro_Estado"))
                    RegimenOperacion.IdTipoOperacion = CInt(lector.Item("idTipoOperacion"))

                    Lista.Add(RegimenOperacion)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.RegimenOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RegimenOperacionSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.RegimenOperacion)
                Do While lector.Read
                    Dim RegimenOperacion As New Entidades.RegimenOperacion
                    RegimenOperacion.IdRegimen = CInt(lector.Item("ro_id"))
                    RegimenOperacion.CuentaContable = CStr(IIf(IsDBNull(lector.Item("ro_cuentacontable")) = True, "", lector.Item("ro_cuentacontable")))
                    RegimenOperacion.Estado = CStr(lector.Item("ro_Estado"))
                    RegimenOperacion.IdTipoOperacion = CInt(lector.Item("idTipoOperacion"))

                    Lista.Add(RegimenOperacion)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.RegimenOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RegimenOperacionSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.RegimenOperacion)
                Do While lector.Read
                    Dim RegimenOperacion As New Entidades.RegimenOperacion
                    RegimenOperacion.IdRegimen = CInt(lector.Item("ro_id"))
                    RegimenOperacion.CuentaContable = CStr(IIf(IsDBNull(lector.Item("ro_cuentacontable")) = True, "", lector.Item("ro_cuentacontable")))
                    RegimenOperacion.Estado = CStr(lector.Item("ro_Estado"))
                    RegimenOperacion.IdTipoOperacion = CInt(lector.Item("idTipoOperacion"))

                    Lista.Add(RegimenOperacion)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.RegimenOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RegimenOperacionSelectAllActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.RegimenOperacion)
                Do While lector.Read
                    Dim RegimenOperacion As New Entidades.RegimenOperacion
                    RegimenOperacion.IdRegimen = CInt(lector.Item("ro_id"))
                    RegimenOperacion.CuentaContable = CStr(IIf(IsDBNull(lector.Item("ro_cuentacontable")) = True, "", lector.Item("ro_cuentacontable")))
                    RegimenOperacion.Estado = CStr(lector.Item("ro_Estado"))
                    RegimenOperacion.IdTipoOperacion = CInt(lector.Item("idTipoOperacion"))

                    Lista.Add(RegimenOperacion)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.RegimenOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RegimenOperacionSelectAllInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.RegimenOperacion)
                Do While lector.Read
                    Dim RegimenOperacion As New Entidades.RegimenOperacion
                    RegimenOperacion.IdRegimen = CInt(lector.Item("ro_id"))
                    RegimenOperacion.CuentaContable = CStr(IIf(IsDBNull(lector.Item("ro_cuentacontable")) = True, "", lector.Item("ro_cuentacontable")))
                    RegimenOperacion.Estado = CStr(lector.Item("ro_Estado"))
                    RegimenOperacion.IdTipoOperacion = CInt(lector.Item("idTipoOperacion"))

                    Lista.Add(RegimenOperacion)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectxIdRegimen(ByVal ro_id As Integer) As List(Of Entidades.RegimenOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RegimenOperacionSelectAllActivoxIdRegimen", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@ro_id", ro_id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.RegimenOperacion)
                Do While lector.Read
                    Dim RegimenOperacion As New Entidades.RegimenOperacion
                    RegimenOperacion.IdRegimen = CInt(lector.Item("ro_id"))
                    RegimenOperacion.CuentaContable = CStr(IIf(IsDBNull(lector.Item("ro_cuentacontable")) = True, "", lector.Item("ro_cuentacontable")))
                    RegimenOperacion.Estado = CStr(lector.Item("ro_Estado"))
                    RegimenOperacion.IdTipoOperacion = CInt(lector.Item("idTipoOperacion"))

                    Lista.Add(RegimenOperacion)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectxIdTipoOperacion(ByVal idTipoOperacion As Integer) As List(Of Entidades.RegimenOperacion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RegimenOperacionSelectAllActivoxIdTipoOperacion", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idTipoOperacion", idTipoOperacion)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.RegimenOperacion)
                Do While lector.Read
                    Dim RegimenOperacion As New Entidades.RegimenOperacion
                    RegimenOperacion.IdRegimen = CInt(lector.Item("ro_id"))
                    RegimenOperacion.CuentaContable = CStr(IIf(IsDBNull(lector.Item("ro_cuentacontable")) = True, "", lector.Item("ro_cuentacontable")))
                    RegimenOperacion.Estado = CStr(lector.Item("ro_Estado"))
                    RegimenOperacion.IdTipoOperacion = CInt(lector.Item("idTipoOperacion"))

                    Lista.Add(RegimenOperacion)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
