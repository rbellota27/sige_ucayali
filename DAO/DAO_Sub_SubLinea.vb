﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'********************************************************
'Autor      : Chigne Bazan, Dany
'Módulo     : MotivoGasto
'Sistema    : Sanicenter
'Empresa    : Digrafic SRL
'Modificado : 23-Febrero-2010
'Hora       : 8:15:00 pm
'********************************************************
Imports System.Data.SqlClient
Public Class DAO_Sub_SubLinea
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaSub_SubLinea(ByVal Sub_SubLinea As Entidades.Sub_SubLinea) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}

        ArrayParametros(0) = New SqlParameter("@Sub_SubLNombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(Sub_SubLinea.Sub_SubLNombre = Nothing, DBNull.Value, Sub_SubLinea.Sub_SubLNombre)
        ArrayParametros(1) = New SqlParameter("@Sub_SubL_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = IIf(Sub_SubLinea.Sub_SubL_Abv = Nothing, DBNull.Value, Sub_SubLinea.Sub_SubL_Abv)
        ArrayParametros(2) = New SqlParameter("@Sub_SubL_Estado", SqlDbType.Bit)
        ArrayParametros(2).Value = IIf(Sub_SubLinea.Sub_SubL_Estado = Nothing, DBNull.Value, Sub_SubLinea.Sub_SubL_Estado)
        ArrayParametros(3) = New SqlParameter("@IdSubLinea", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(Sub_SubLinea.IdSubLinea = Nothing, DBNull.Value, Sub_SubLinea.IdSubLinea)
        ArrayParametros(4) = New SqlParameter("@Sub_SubLCodigo", SqlDbType.Char)
        ArrayParametros(4).Value = IIf(Sub_SubLinea.Sub_SubLCodigo = Nothing, DBNull.Value, Sub_SubLinea.Sub_SubLCodigo)
        Return HDAO.Insert(cn, "_Sub_SubLineaInsert", ArrayParametros)

    End Function
    Public Function ActualizaSub_SubLinea(ByVal Sub_SubLinea As Entidades.Sub_SubLinea) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}

        ArrayParametros(0) = New SqlParameter("@IdSub_SubLinea", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(Sub_SubLinea.IdSub_SubLinea = Nothing, DBNull.Value, Sub_SubLinea.IdSub_SubLinea)
        ArrayParametros(1) = New SqlParameter("@Sub_SubLNombre", SqlDbType.VarChar)
        ArrayParametros(1).Value = IIf(Sub_SubLinea.Sub_SubLNombre = Nothing, DBNull.Value, Sub_SubLinea.Sub_SubLNombre)
        ArrayParametros(2) = New SqlParameter("@Sub_SubL_Abv", SqlDbType.VarChar)
        ArrayParametros(2).Value = IIf(Sub_SubLinea.Sub_SubL_Abv = Nothing, DBNull.Value, Sub_SubLinea.Sub_SubL_Abv)
        ArrayParametros(3) = New SqlParameter("@Sub_SubL_Estado", SqlDbType.Bit)
        ArrayParametros(3).Value = IIf(Sub_SubLinea.Sub_SubL_Estado = Nothing, DBNull.Value, Sub_SubLinea.Sub_SubL_Estado)
        ArrayParametros(4) = New SqlParameter("@IdSubLinea", SqlDbType.Int)
        ArrayParametros(4).Value = IIf(Sub_SubLinea.IdSubLinea = Nothing, DBNull.Value, Sub_SubLinea.IdSubLinea)
        ArrayParametros(5) = New SqlParameter("@Sub_SubLCodigo", SqlDbType.Char)
        ArrayParametros(5).Value = IIf(Sub_SubLinea.Sub_SubLCodigo = Nothing, DBNull.Value, Sub_SubLinea.Sub_SubLCodigo)
        Return HDAO.Update(cn, "_Sub_SubLineaUpdate", ArrayParametros)

    End Function

    'Busca todas las sub_sub_linea
    Public Function SelectAllSub_SubLinea() As List(Of Entidades.Sub_SubLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Sub_SubLineaSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim Lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                Lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)                
                Dim lista As New List(Of Entidades.Sub_SubLinea)
                Do While Lector.Read
                    Dim obj As New Entidades.Sub_SubLinea
                    obj.Sub_SubLNombre = CStr(IIf(IsDBNull(Lector.Item("Sub_SubLNombre")) = True, "", Lector.Item("Sub_SubLNombre")))
                    obj.Sub_SubL_Abv = CStr(IIf(IsDBNull(Lector.Item("Sub_SubL_Abv")) = True, "", Lector.Item("Sub_SubL_Abv")))
                    obj.Sub_SubL_Estado = CBool(IIf(IsDBNull(Lector.Item("Sub_SubL_Estado")) = True, "", Lector.Item("Sub_SubL_Estado")))
                    obj.Sub_SubLCodigo = CStr(IIf(IsDBNull(Lector.Item("Sub_SubL_Estado")) = True, "", Lector.Item("Sub_SubL_Estado")))
                    obj.IdSub_SubLinea = CInt(IIf(IsDBNull(Lector.Item("IdSub_SubLinea")) = True, "", Lector.Item("IdSub_SubLinea")))
                    obj.IdSubLinea = CInt(IIf(IsDBNull(Lector.Item("IdSubLinea")) = True, "", Lector.Item("IdSubLinea")))
                    obj.sl_Nombre = CStr(IIf(IsDBNull(Lector.Item("sl_Nombre")) = True, "", Lector.Item("sl_Nombre")))
                    lista.Add(obj)

                Loop
                Lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    'Busca las sub_sub_linea con estado Activo
    Public Function SelectActivoSub_SubLinea() As List(Of Entidades.Sub_SubLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Sub_SubLineaSelectActivos", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim Lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                Lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.Sub_SubLinea)
                Do While Lector.Read
                    Dim obj As New Entidades.Sub_SubLinea
                    obj.Sub_SubLNombre = CStr(IIf(IsDBNull(Lector.Item("Sub_SubLNombre")) = True, "", Lector.Item("Sub_SubLNombre")))
                    obj.Sub_SubL_Abv = CStr(IIf(IsDBNull(Lector.Item("Sub_SubL_Abv")) = True, "", Lector.Item("Sub_SubL_Abv")))
                    obj.Sub_SubL_Estado = CBool(IIf(IsDBNull(Lector.Item("Sub_SubL_Estado")) = True, "", Lector.Item("Sub_SubL_Estado")))
                    obj.Sub_SubLCodigo = CStr(IIf(IsDBNull(Lector.Item("Sub_SubL_Estado")) = True, "", Lector.Item("Sub_SubL_Estado")))
                    obj.IdSub_SubLinea = CInt(IIf(IsDBNull(Lector.Item("IdSub_SubLinea")) = True, "", Lector.Item("IdSub_SubLinea")))
                    obj.IdSubLinea = CInt(IIf(IsDBNull(Lector.Item("IdSubLinea")) = True, "", Lector.Item("IdSubLinea")))
                    obj.sl_Nombre = CStr(IIf(IsDBNull(Lector.Item("sl_Nombre")) = True, "", Lector.Item("sl_Nombre")))
                    lista.Add(obj)

                Loop
                Lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    'Busca las sub_sub_linea con estado Inactivo
    Public Function SelectInactivoSub_SubLinea() As List(Of Entidades.Sub_SubLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Sub_SubLineaSelectInactivos", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim Lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                Lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.Sub_SubLinea)
                Do While Lector.Read
                    Dim obj As New Entidades.Sub_SubLinea
                    obj.Sub_SubLNombre = CStr(IIf(IsDBNull(Lector.Item("Sub_SubLNombre")) = True, "", Lector.Item("Sub_SubLNombre")))
                    obj.Sub_SubL_Abv = CStr(IIf(IsDBNull(Lector.Item("Sub_SubL_Abv")) = True, "", Lector.Item("Sub_SubL_Abv")))
                    obj.Sub_SubL_Estado = CBool(IIf(IsDBNull(Lector.Item("Sub_SubL_Estado")) = True, "", Lector.Item("Sub_SubL_Estado")))
                    obj.Sub_SubLCodigo = CStr(IIf(IsDBNull(Lector.Item("Sub_SubL_Estado")) = True, "", Lector.Item("Sub_SubL_Estado")))
                    obj.IdSub_SubLinea = CInt(IIf(IsDBNull(Lector.Item("IdSub_SubLinea")) = True, "", Lector.Item("IdSub_SubLinea")))
                    obj.IdSubLinea = CInt(IIf(IsDBNull(Lector.Item("IdSubLinea")) = True, "", Lector.Item("IdSubLinea")))
                    obj.sl_Nombre = CStr(IIf(IsDBNull(Lector.Item("sl_Nombre")) = True, "", Lector.Item("sl_Nombre")))
                    lista.Add(obj)

                Loop
                Lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    'Busca todas las sub_sub_linea por nombre ingresado
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Sub_SubLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Sub_SubLineaSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Sub_SubLNombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Sub_SubLinea)
                Do While lector.Read
                    Dim obj As New Entidades.Sub_SubLinea
                    obj.Sub_SubLNombre = CStr(IIf(IsDBNull(lector.Item("Sub_SubLNombre")) = True, "", lector.Item("Sub_SubLNombre")))
                    obj.Sub_SubL_Abv = CStr(IIf(IsDBNull(lector.Item("Sub_SubL_Abv")) = True, "", lector.Item("Sub_SubL_Abv")))
                    obj.Sub_SubL_Estado = CBool(IIf(IsDBNull(lector.Item("Sub_SubL_Estado")) = True, "", lector.Item("Sub_SubL_Estado")))
                    obj.Sub_SubLCodigo = CStr(IIf(IsDBNull(lector.Item("Sub_SubL_Estado")) = True, "", lector.Item("Sub_SubL_Estado")))
                    obj.IdSub_SubLinea = CInt(IIf(IsDBNull(lector.Item("IdSub_SubLinea")) = True, "", lector.Item("IdSub_SubLinea")))
                    obj.IdSubLinea = CInt(IIf(IsDBNull(lector.Item("IdSubLinea")) = True, "", lector.Item("IdSubLinea")))
                    obj.sl_Nombre = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")) = True, "", lector.Item("sl_Nombre")))
                    Lista.Add(obj)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    'Busca todas las sub_sub_linea por nombre Activos
    Public Function SelectNombreActivo(ByVal nombre As String) As List(Of Entidades.Sub_SubLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Sub_SubLineaSelectAllxNombreActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Sub_SubLNombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Sub_SubLinea)
                Do While lector.Read
                    Dim obj As New Entidades.Sub_SubLinea
                    obj.Sub_SubLNombre = CStr(IIf(IsDBNull(lector.Item("Sub_SubLNombre")) = True, "", lector.Item("Sub_SubLNombre")))
                    obj.Sub_SubL_Abv = CStr(IIf(IsDBNull(lector.Item("Sub_SubL_Abv")) = True, "", lector.Item("Sub_SubL_Abv")))
                    obj.Sub_SubL_Estado = CBool(IIf(IsDBNull(lector.Item("Sub_SubL_Estado")) = True, "", lector.Item("Sub_SubL_Estado")))
                    obj.Sub_SubLCodigo = CStr(IIf(IsDBNull(lector.Item("Sub_SubL_Estado")) = True, "", lector.Item("Sub_SubL_Estado")))
                    obj.IdSub_SubLinea = CInt(IIf(IsDBNull(lector.Item("IdSub_SubLinea")) = True, "", lector.Item("IdSub_SubLinea")))
                    obj.IdSubLinea = CInt(IIf(IsDBNull(lector.Item("IdSubLinea")) = True, "", lector.Item("IdSubLinea")))
                    obj.sl_Nombre = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")) = True, "", lector.Item("sl_Nombre")))
                    Lista.Add(obj)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    'Busca todas las sub_sub_linea por nombre Inactivo
    Public Function SelectNombreInactivo(ByVal nombre As String) As List(Of Entidades.Sub_SubLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Sub_SubLineaSelectAllxNombreInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Sub_SubLNombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Sub_SubLinea)
                Do While lector.Read
                    Dim obj As New Entidades.Sub_SubLinea
                    obj.Sub_SubLNombre = CStr(IIf(IsDBNull(lector.Item("Sub_SubLNombre")) = True, "", lector.Item("Sub_SubLNombre")))
                    obj.Sub_SubL_Abv = CStr(IIf(IsDBNull(lector.Item("Sub_SubL_Abv")) = True, "", lector.Item("Sub_SubL_Abv")))
                    obj.Sub_SubL_Estado = CBool(IIf(IsDBNull(lector.Item("Sub_SubL_Estado")) = True, "", lector.Item("Sub_SubL_Estado")))
                    obj.Sub_SubLCodigo = CStr(IIf(IsDBNull(lector.Item("Sub_SubL_Estado")) = True, "", lector.Item("Sub_SubL_Estado")))
                    obj.IdSub_SubLinea = CInt(IIf(IsDBNull(lector.Item("IdSub_SubLinea")) = True, "", lector.Item("IdSub_SubLinea")))
                    obj.IdSubLinea = CInt(IIf(IsDBNull(lector.Item("IdSubLinea")) = True, "", lector.Item("IdSubLinea")))
                    obj.sl_Nombre = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")) = True, "", lector.Item("sl_Nombre")))
                    Lista.Add(obj)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    'Busca todas las sub_sub_linea por Codigo
    Public Function SelectxId(ByVal Codigo As Integer) As List(Of Entidades.Sub_SubLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Sub_SubLineaSelectAllxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSub_SubLinea", Codigo)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Sub_SubLinea)
                Do While lector.Read
                    Dim obj As New Entidades.Sub_SubLinea
                    obj.Sub_SubLNombre = CStr(IIf(IsDBNull(lector.Item("Sub_SubLNombre")) = True, "", lector.Item("Sub_SubLNombre")))
                    obj.Sub_SubL_Abv = CStr(IIf(IsDBNull(lector.Item("Sub_SubL_Abv")) = True, "", lector.Item("Sub_SubL_Abv")))
                    obj.Sub_SubL_Estado = CBool(IIf(IsDBNull(lector.Item("Sub_SubL_Estado")) = True, "", lector.Item("Sub_SubL_Estado")))
                    obj.Sub_SubLCodigo = CStr(IIf(IsDBNull(lector.Item("Sub_SubL_Estado")) = True, "", lector.Item("Sub_SubL_Estado")))
                    obj.IdSub_SubLinea = CInt(IIf(IsDBNull(lector.Item("IdSub_SubLinea")) = True, "", lector.Item("IdSub_SubLinea")))
                    obj.IdSubLinea = CInt(IIf(IsDBNull(lector.Item("IdSubLinea")) = True, "", lector.Item("IdSubLinea")))
                    obj.sl_Nombre = CStr(IIf(IsDBNull(lector.Item("sl_Nombre")) = True, "", lector.Item("sl_Nombre")))
                    Lista.Add(obj)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    'Busca todas las sub_sub_linea por SubLinea
    Public Function SelectSub_SubLineaxSubLinea(ByVal IdSubLInea As Integer) As List(Of Entidades.Sub_SubLinea)
        Dim Lista As New List(Of Entidades.Sub_SubLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Sub_SubLineaSelectCboSubLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSubLInea", IdSubLInea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)

                Do While lector.Read
                    Dim obj As New Entidades.Sub_SubLinea

                    obj.IdSub_SubLinea = CInt(IIf(IsDBNull(lector.Item("IdSub_SubLinea")) = True, 0, lector.Item("IdSub_SubLinea")))
                    obj.Sub_SubLNombre = CStr(IIf(IsDBNull(lector.Item("Sub_SubLNombre")) = True, 0, lector.Item("Sub_SubLNombre")))
                    obj.Sub_SubL_Abv = CStr(IIf(IsDBNull(lector.Item("Sub_SubL_Abv")) = True, "", lector.Item("Sub_SubL_Abv")))
                    Lista.Add(obj) 'llena la lista
                Loop
                lector.Close() 'cierra el lector

            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close() 'cierra la conexion
        End Try
        Return Lista 'Retorna lista
    End Function

    Public Function SelectAllActivoxIdLineav2(ByVal idsublinea As Integer) As List(Of Entidades.Sub_SubLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_Sub_SubLineaSelectAllActivoxIdSubLineav2", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Sub_SubLinea)
                Do While lector.Read
                    Dim obj As New Entidades.Sub_SubLinea
                    obj.Id = CStr(lector.Item("IdSub_SubLinea"))
                    obj.Sub_SubLNombre = CStr(lector.Item("Sub_SubLNombre"))
                    obj.Sub_SubL_Abv = CStr(lector.Item("Sub_SubL_Abv"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
