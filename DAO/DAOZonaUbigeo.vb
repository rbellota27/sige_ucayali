﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'********************  JUEVES 11 FEB 2010 HORA 10_00 AM









'***************   LUNES 18 ENERO 2010 HORA 12_11 pm

Imports System.Data.SqlClient
Public Class DAOZonaUbigeo
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectxEstadoxNombre(ByVal estado As String) As List(Of Entidades.ZonaUbigeo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim lista As New List(Of Entidades.ZonaUbigeo)
        Try
            cn.Open()
            Dim cmd As New SqlCommand("_ZonaUbigeoSelectAllActivoxEstado", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Estado", estado)
            Dim lector As SqlDataReader = cmd.ExecuteReader
            While lector.Read
                Dim obj As New Entidades.ZonaUbigeo
                obj.IdZona = CStr(IIf(IsDBNull(lector.Item("IdZona")) = True, "", lector.Item("IdZona")))
                obj.Nombre = CStr(IIf(IsDBNull(lector.Item("zo_Nombre")) = True, "", lector.Item("zo_Nombre")))
                obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("zo_Descripcion")) = True, "", lector.Item("zo_Descripcion")))
                obj.Moneda = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                obj.MontoMin = CStr(IIf(IsDBNull(lector.Item("zo_MontoMin")) = True, "", lector.Item("zo_MontoMin")))
                obj.Estado = CBool(IIf(IsDBNull(lector.Item("zo_Estado")) = True, 0, lector.Item("zo_Estado")))
                lista.Add(obj)
            End While
            lector.Close()
            cn.Close()
        Catch ex As Exception
        Finally

        End Try
        Return lista
    End Function

    Public Function InsertaZonaUbigeo(ByVal zonaubigeo As Entidades.ZonaUbigeo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
            ArrayParametros(0) = New SqlParameter("@zo_Nombre", SqlDbType.VarChar)
            ArrayParametros(0).Value = IIf(zonaubigeo.Nombre = Nothing, DBNull.Value, zonaubigeo.Nombre)
            ArrayParametros(1) = New SqlParameter("@zo_Descripcion", SqlDbType.VarChar)
            ArrayParametros(1).Value = IIf(zonaubigeo.Descripcion = Nothing, DBNull.Value, zonaubigeo.Descripcion)
            ArrayParametros(2) = New SqlParameter("@zo_Estado", SqlDbType.Char)
            ArrayParametros(2).Value = IIf(zonaubigeo.Estado = Nothing, DBNull.Value, zonaubigeo.Estado)
            ArrayParametros(3) = New SqlParameter("@IdMoneda", SqlDbType.Int)
            ArrayParametros(3).Value = IIf(zonaubigeo.Moneda = Nothing, DBNull.Value, zonaubigeo.Moneda)
            ArrayParametros(4) = New SqlParameter("@zo_MontoMin", SqlDbType.Decimal)
            ArrayParametros(4).Value = IIf(zonaubigeo.MontoMin = Nothing, DBNull.Value, zonaubigeo.MontoMin)
            HDAO.InsertaT(cn, "_ZonaUbigeoInsert", ArrayParametros, tr)

            tr.Commit()
            Return True
        Catch ex As Exception
            tr.Rollback()
            Return False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function EliminaZonaUbigeo(ByVal zonaubigeo As Entidades.ZonaUbigeo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim cmd As New SqlCommand("_ZonaUbigeoDelete", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            Dim ArrayParametros() As SqlParameter = New SqlParameter(0) {}
            ArrayParametros(0) = New SqlParameter("@IdZona", SqlDbType.Int)
            ArrayParametros(0).Value = IIf(zonaubigeo.IdZona = Nothing, DBNull.Value, zonaubigeo.IdZona)
            cmd.Parameters.AddRange(ArrayParametros)

            If cmd.ExecuteNonQuery = 0 Then
                Throw New Exception
            End If

            tr.Commit()
            Return True
        Catch ex As Exception
            tr.Rollback()
            Return False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function ActualizaZonaUbigeo(ByVal zonaubigeo As Entidades.ZonaUbigeo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim objDaoProcedencia As New DAO.DAOProcedencia

        '************************* obtengo la lista original

        Dim PEstado As Byte = 0
        'Select usuario.Estado
        '   Case True
        'PEstado = "1"
        '   Case False
        'PEstado = "0"
        'End Select
        If (zonaubigeo.Estado = True) Then
            PEstado = 1
        End If
        If (zonaubigeo.Estado = False) Then
            PEstado = 0
        End If

        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim cmd As New SqlCommand("_ZonaUbigeoUpdate", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
            ArrayParametros(0) = New SqlParameter("@IdZona", SqlDbType.Int)
            ArrayParametros(0).Value = IIf(zonaubigeo.IdZona = Nothing, DBNull.Value, zonaubigeo.IdZona)
            ArrayParametros(1) = New SqlParameter("@zo_Nombre", SqlDbType.VarChar)
            ArrayParametros(1).Value = IIf(zonaubigeo.Nombre = Nothing, DBNull.Value, zonaubigeo.Nombre)
            ArrayParametros(2) = New SqlParameter("@zo_Descripcion", SqlDbType.VarChar)
            ArrayParametros(2).Value = IIf(zonaubigeo.Descripcion = Nothing, DBNull.Value, zonaubigeo.Descripcion)
            ArrayParametros(3) = New SqlParameter("@zo_Estado", SqlDbType.Char)
            ArrayParametros(3).Value = PEstado
            ArrayParametros(4) = New SqlParameter("@IdMoneda", SqlDbType.Int)
            ArrayParametros(4).Value = IIf(zonaubigeo.Moneda = Nothing, DBNull.Value, zonaubigeo.Moneda)
            ArrayParametros(5) = New SqlParameter("@zo_MontoMin", SqlDbType.Decimal)
            ArrayParametros(5).Value = IIf(zonaubigeo.MontoMin = Nothing, DBNull.Value, zonaubigeo.MontoMin)
            cmd.Parameters.AddRange(ArrayParametros)
            If cmd.ExecuteNonQuery = 0 Then
                Throw New Exception
            End If

            '********************* registro los nuevos productos
            Dim objUtil As New DAO.DAOUtil
            tr.Commit()
            Return True
        Catch ex As Exception
            tr.Rollback()
            Return False
        Finally

        End Try
    End Function

    Public Function SelectxIdZona(ByVal IdZona As Integer) As Entidades.ZonaUbigeo
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim objZonaUbigeo As Entidades.ZonaUbigeo = Nothing
        Try
            cn.Open()
            Dim cmd As New SqlCommand("_ZonaUbigeoSelectAllActivoxIdZona", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdZona", IdZona)
            Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            If lector.Read Then
                objZonaUbigeo = New Entidades.ZonaUbigeo
                With objZonaUbigeo
                    .IdZona = CStr(IIf(IsDBNull(lector.Item("IdZona")) = True, "", lector.Item("IdZona")))
                    .Estado = CBool(IIf(IsDBNull(lector.Item("zo_Estado")) = True, "", lector.Item("zo_Estado")))
                    .Nombre = CStr(IIf(IsDBNull(lector.Item("zo_Nombre")) = True, Nothing, lector.Item("zo_Nombre")))
                    .Descripcion = CStr(IIf(IsDBNull(lector.Item("zo_Descripcion")) = True, "", lector.Item("zo_Descripcion")))
                    .Moneda = CStr(IIf(IsDBNull(lector.Item("IdMoneda")) = True, Nothing, lector.Item("IdMoneda")))
                    .MontoMin = CStr(IIf(IsDBNull(lector.Item("zo_MontoMin")) = True, Nothing, lector.Item("zo_MontoMin")))
                End With
            End If

            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return objZonaUbigeo
    End Function
    Public Function SelectCbo() As List(Of Entidades.ZonaUbigeo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ZonaUbigeoSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ZonaUbigeo)
                Do While lector.Read
                    Dim ZonaUbigeo As New Entidades.ZonaUbigeo
                    ZonaUbigeo.IdZona = CStr(lector.Item("IdZona"))
                    ZonaUbigeo.Nombre = CStr(lector.Item("zo_Nombre"))
                    Lista.Add(ZonaUbigeo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
