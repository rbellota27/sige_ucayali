﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




'*********************   LUNES 24052010








Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOMovCuentaPorPagar
    Inherits DAOMantenedor

    Public Function Registrar(ByVal obj As Entidades.MovCuentaPorPagar, ByVal sqlcn As SqlConnection, ByVal sqltr As SqlTransaction) As Integer
        Try
            Dim parametro() As SqlParameter = GetVectorParametros(obj, modo_query.Insert)

            Dim cmd As New SqlCommand("_MovCuentaPorPagar_Insert", sqlcn, sqltr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(parametro)            
            Return CInt(cmd.ExecuteScalar().ToString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function CR_MovCuenta_CXP_Lista(ByVal IdEmpresa As Integer, ByVal IdPersona As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal OpcionDeudas As Integer) As List(Of Entidades.MovCuentaPorPagar)

        Dim lista As New List(Of Entidades.MovCuentaPorPagar)
        Try
            Dim p() As SqlParameter = New SqlParameter(4) {}

            p(0) = MyBase.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(1) = MyBase.getParam(IdPersona, "@IdPersona", SqlDbType.Int)
            p(2) = MyBase.getParam(FechaInicio, "@FechaInicio", SqlDbType.Date)
            p(3) = MyBase.getParam(FechaFin, "@FechaFin", SqlDbType.Date)
            p(4) = MyBase.getParam(OpcionDeudas, "@OpcionDeudas", SqlDbType.Int)

            MyBase.Cn = (New DAO.Conexion).ConexionSIGE
            MyBase.Cn.Open()

            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(MyBase.Cn, CommandType.StoredProcedure, "_CR_MovCuenta_CXP", p)
            While (reader.Read)
                Dim obj As New Entidades.MovCuentaPorPagar
                With obj

                    .TipoDoc_NroDocumento = MyBase.UCStr(reader("TipoDoc_NroDocumento"))
                    .doc_FechaEmision = MyBase.UCDate(reader("doc_FechaEmision"))
                    .doc_FechaVenc = MyBase.UCDate(reader("doc_FechaVenc"))
                    .Moneda_Monto = MyBase.UCStr(reader("Moneda_Monto"))
                    .Moneda_Saldo = MyBase.UCStr(reader("Moneda_Saldo"))
                    .Ruc = MyBase.UCStr(reader("Ruc"))
                    .Empresa = MyBase.UCStr(reader("Empresa"))
                    .DescripcionPersona = MyBase.UCStr(reader("DescripcionPersona"))
                    .ChequesAsociados = MyBase.UCStr(reader("ChequesAsociados"))
                    .PagoProgramado = MyBase.UCStr(reader("PagoProgramado"))
                    .NroDocumentoExterno = MyBase.UCStr(reader("NroDocumentoExterno"))

                End With
                lista.Add(obj)
            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (MyBase.Cn.State = ConnectionState.Open) Then MyBase.Cn.Close()
        End Try

        Return lista


    End Function

    Public Function CR_MovCuenta_CXP_DT(ByVal IdEmpresa As Integer, ByVal IdPersona As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal OpcionDeudas As Integer) As DataTable

        Dim ds As New DataSet

        Try

            Dim p() As SqlParameter = New SqlParameter(4) {}

            p(0) = MyBase.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(1) = MyBase.getParam(IdPersona, "@IdPersona", SqlDbType.Int)
            p(2) = MyBase.getParam(FechaInicio, "@FechaInicio", SqlDbType.Date)
            p(3) = MyBase.getParam(FechaFin, "@FechaFin", SqlDbType.Date)
            p(4) = MyBase.getParam(OpcionDeudas, "@OpcionDeudas", SqlDbType.Int)


            Dim cmd As New SqlCommand("_CR_MovCuenta_CXP", (New DAO.Conexion).ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)

            da.Fill(ds, "DT_MovCuenta_CXP")

        Catch ex As Exception

            Throw ex

        End Try

        Return ds.Tables("DT_MovCuenta_CXP")

    End Function

    Public Function ReporteCancelacionBancos(ByVal FechaInicio As Date, ByVal FechaFin As Date) As List(Of Entidades.Documento)

        Dim lista As New List(Of Entidades.Documento)

        Try
            Dim p() As SqlParameter = New SqlParameter(1) {}
            p(0) = MyBase.getParam(FechaInicio, "@fechaInicio", SqlDbType.Date)
            p(1) = MyBase.getParam(FechaFin, "@fechafin", SqlDbType.Date)
            MyBase.Cn = (New DAO.Conexion).ConexionSIGE
            MyBase.Cn.Open()

            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(MyBase.Cn, CommandType.StoredProcedure, "SelectReportDocCancelacionBancos", p)
            While (reader.Read)
                Dim obj As New Entidades.Documento
                With obj
                    .DescripcionPersona = MyBase.UCStr(reader("DescripcionPersona"))
                    .NroDocumento = MyBase.UCStr(reader("NroDocumento"))
                    .NomTipoDocumento = MyBase.UCStr(reader("TipoDocumento"))
                    .NomTipoOperacion = MyBase.UCStr(reader("TipoOperacion"))
                    .Tienda = MyBase.UCStr(reader("Tienda"))
                    .FechaEmision = MyBase.UCDate(reader("doc_FechaEmision"))
                    .Total = MyBase.UCDec(reader("doc_Total"))
                    .NomMoneda = MyBase.UCStr(reader("Moneda"))
                End With
                lista.Add(obj)
            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (MyBase.Cn.State = ConnectionState.Open) Then MyBase.Cn.Close()
        End Try

        Return lista

    End Function



    Public Function ReporteProgramaciones(ByVal FechaInicio As Date, ByVal FechaFin As Date) As List(Of Entidades.ProgramacionReporte)

        Dim lista As New List(Of Entidades.ProgramacionReporte)

        Try
            Dim p() As SqlParameter = New SqlParameter(1) {}
            p(0) = MyBase.getParam(FechaInicio, "@FechaVctoInicio", SqlDbType.Date)
            p(1) = MyBase.getParam(FechaFin, "@FechaVctoFin", SqlDbType.Date)
            MyBase.Cn = (New DAO.Conexion).ConexionSIGE
            MyBase.Cn.Open()

            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(MyBase.Cn, CommandType.StoredProcedure, "_SelectReporteProgramaciondePago", p)
            While (reader.Read)
                Dim obj As New Entidades.ProgramacionReporte

                With obj
                  
                    .Proveedor = MyBase.UCStr(reader("Proveedor"))
                    .pp_Observacion = MyBase.UCStr(reader("pp_Observacion"))
                    .pp_MontoPagoProg = MyBase.UCDec(reader("pp_MontoPagoProg"))
                    .pp_FechaPagoProg = MyBase.UCDate(reader("pp_FechaPagoProg"))
                    .mp_Nombre = MyBase.UCStr(reader("mp_Nombre"))
                    .monedapago = MyBase.UCStr(reader("monedapago"))
                    .mon_Simbolo = MyBase.UCStr(reader("mon_Simbolo"))
                    .mcp_Saldo = MyBase.UCDec(reader("mcp_Saldo"))
                    .mcp_Monto = MyBase.UCDec(reader("mcp_Monto"))
                    .mcp_FechaCanc = MyBase.UCDate(reader("mcp_FechaCanc"))
                    .mcp_Fecha = MyBase.UCDate(reader("mcp_Fecha"))
                    .IdDocumento = MyBase.UCInt(reader("IdDocumento"))
                    .edoc_Nombre = MyBase.UCStr(reader("edoc_Nombre"))
                    .doc_TotalAPagar = MyBase.UCDec(reader("doc_TotalAPagar"))
                    .doc_Serie = MyBase.UCStr(reader("doc_Serie"))
                    .doc_FechaVenc = MyBase.UCDate(reader("doc_FechaVenc"))
                    .doc_FechaEmision = MyBase.UCDate(reader("doc_FechaEmision"))
                    .doc_Codigo = MyBase.UCStr(reader("doc_Codigo"))
                    .cb_numero = MyBase.UCStr(reader("cb_numero"))
                    .ban_Nombre = MyBase.UCStr(reader("ban_Nombre"))
                    .NumeroDocumento = .doc_Serie + "-" + .doc_Codigo
                    .TipoDocumento = MyBase.UCStr(reader("TipoDocumento"))
                End With

                lista.Add(obj)

            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (MyBase.Cn.State = ConnectionState.Open) Then MyBase.Cn.Close()
        End Try

        Return lista

    End Function

    Public Function SelectProgramacionPagoRQxFechas(ByVal FechaVctoInicio As Date, ByVal FechaVctoFin As Date) As DataTable
        Dim ds As New DataSet
        Try
            Dim p() As SqlParameter = New SqlParameter(1) {}

            p(0) = MyBase.getParam(FechaVctoInicio, "@FechaVctoInicio", SqlDbType.Date)
            p(1) = MyBase.getParam(FechaVctoFin, "@FechaVctoFin", SqlDbType.Date)

            Dim cmd As New SqlCommand("SelectProgramacionPagoRQxFechas", (New DAO.Conexion).ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            cmd.Parameters.AddRange(p)
            Dim da As New SqlDataAdapter(cmd)

            da.Fill(ds, "DT_ProgramacionPagoRQ")

        Catch ex As Exception
            Throw ex
        End Try

        Return ds.Tables("DT_ProgramacionPagoRQ")
    End Function

    Public Function _MovCuentaPorPagarRQCheckes(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdMoneda As Integer, ByVal IdPersona As Integer, ByVal FechaVctoInicio As Date, ByVal FechaVctoFin As Date, ByVal serie As Integer, ByVal codigo As Integer, ByVal Opcion_Programacion As Integer, ByVal Opcion_Deuda As Integer, ByVal PageIndex As Integer, ByVal PageSize As Integer, ByVal IdTipoDocumento As Integer) As DataTable
        Dim ds As New DataSet
        Try
            Dim p() As SqlParameter = New SqlParameter(12) {}

            p(0) = MyBase.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(1) = MyBase.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(2) = MyBase.getParam(IdMoneda, "@IdMoneda", SqlDbType.Int)
            p(3) = MyBase.getParam(IdPersona, "@IdPersona", SqlDbType.Int)
            p(4) = MyBase.getParam(FechaVctoInicio, "@FechaVctoInicio", SqlDbType.Date)
            p(5) = MyBase.getParam(FechaVctoFin, "@FechaVctoFin", SqlDbType.Date)
            p(6) = MyBase.getParam(Opcion_Programacion, "@Opcion_Programacion", SqlDbType.Int)
            p(7) = MyBase.getParam(Opcion_Deuda, "@Opcion_Deuda", SqlDbType.Int)
            p(8) = MyBase.getParam(PageIndex, "@PageIndex", SqlDbType.Int)
            p(9) = MyBase.getParam(PageSize, "@PageSize", SqlDbType.Int)
            p(10) = MyBase.getParam(IdTipoDocumento, "@IdTipoDocumento", SqlDbType.Int)
            p(11) = MyBase.getParam(serie, "@serie", SqlDbType.Int)
            p(12) = MyBase.getParam(codigo, "@codigo", SqlDbType.Int)

            Dim cmd As New SqlCommand("_MovCuentaPorPagarRQCheckes", (New DAO.Conexion).ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)
            Dim da As New SqlDataAdapter(cmd)

            da.Fill(ds, "DT_MovCuenta_CXP")

        Catch ex As Exception
            Throw ex
        End Try

        Return ds.Tables("DT_MovCuenta_CXP")
    End Function
    Public Function SelectMovCuentaporPagarxIddocumento(ByVal IdDocumento As Integer) As Entidades.MovCuentaPorPagar

        Dim objDocumento As New Entidades.MovCuentaPorPagar
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SelectMovCuentaporPagarxIddocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)

        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                If (lector.HasRows = True) Then
                    lector.Read()
                    With objDocumento
                        .Id = CInt(IIf(IsDBNull(lector.Item("IdMovCtaPP")) = True, 0, lector.Item("IdMovCtaPP")))
                        .DescripcionPersona = CStr(IIf(IsDBNull(lector.Item("Proveedor")) = True, "", lector.Item("Proveedor")))
                        .MonedaSimbolo = CStr(IIf(IsDBNull(lector.Item("mon_Simbolo")) = True, "", lector.Item("mon_Simbolo")))
                        .Monto = CDec(IIf(IsDBNull(lector.Item("mcp_Monto")) = True, 0, lector.Item("mcp_Monto")))
                        .Saldo = CDec(IIf(IsDBNull(lector.Item("mcp_Saldo")) = True, 0, lector.Item("mcp_Saldo")))
                        .NroDocumento = CStr(IIf(IsDBNull(lector.Item("NroDocumento")) = True, "", lector.Item("NroDocumento")))
                        .doc_FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, "", lector.Item("doc_FechaEmision")))
                        .doc_FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, "", lector.Item("doc_FechaVenc")))
                        .IdProveedor = CInt(IIf(IsDBNull(lector.Item("IdProveedor")) = True, 0, lector.Item("IdProveedor")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector.Item("TipoDocumento")) = True, "", lector.Item("TipoDocumento")))
                    End With
                End If

            End Using
            Return objDocumento
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function MovCuentaPorPagar_SelectxParams_DataTableReport(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdMoneda As Integer _
                                                                    , ByVal IdPersona As Integer, ByVal FechaVctoInicio As Date, ByVal FechaVctoFin As Date _
                                                                    , ByVal serie As Integer, ByVal codigo As Integer, ByVal Opcion_Programacion As Integer _
                                                                    , ByVal Opcion_Deuda As Integer, ByVal PageIndex As Integer, ByVal PageSize As Integer _
                                                                    , ByVal IdTipoDocumento As Integer, ByVal cn As SqlConnection) As Entidades.be_Requerimiento_x_pagar
        Dim objeto As Entidades.be_Requerimiento_x_pagar = Nothing
        Using cmd As New SqlCommand("_MovCuentaPorPagar_SelectxParamsReport", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add(New SqlParameter("@IdEmpresa", SqlDbType.Int, 2)).Value = IdEmpresa
                .Parameters.Add(New SqlParameter("@IdTienda", SqlDbType.Int, 40)).Value = IdTienda
                .Parameters.Add(New SqlParameter("@IdMoneda", SqlDbType.Int, 40)).Value = IdMoneda
                .Parameters.Add(New SqlParameter("@IdPersona", SqlDbType.Int, 40)).Value = IdPersona
                .Parameters.Add(New SqlParameter("@FechaVctoInicio", SqlDbType.Date)).Value = IIf(FechaVctoInicio = "12:00:00 AM", DBNull.Value, FechaVctoInicio)
                .Parameters.Add(New SqlParameter("@FechaVctoFin", SqlDbType.Date)).Value = IIf(FechaVctoFin = "12:00:00 AM", DBNull.Value, FechaVctoFin)
                .Parameters.Add(New SqlParameter("@Opcion_Programacion", SqlDbType.Int, 40)).Value = Opcion_Programacion
                .Parameters.Add(New SqlParameter("@Opcion_Deuda", SqlDbType.Int, 2)).Value = Opcion_Deuda
                .Parameters.Add(New SqlParameter("@PageIndex", SqlDbType.Int, 10)).Value = PageIndex
                .Parameters.Add(New SqlParameter("@PageSize", SqlDbType.Int, 10)).Value = PageSize
                .Parameters.Add(New SqlParameter("@IdTipoDocumento", SqlDbType.Int, 40)).Value = IdTipoDocumento
                .Parameters.Add(New SqlParameter("@serie", SqlDbType.Int, 10)).Value = serie
                .Parameters.Add(New SqlParameter("@codigo", SqlDbType.Int, 40)).Value = codigo
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader()
            If rdr IsNot Nothing Then
                objeto = New Entidades.be_Requerimiento_x_pagar
                If rdr.HasRows Then
                    Dim listaReq As New List(Of Entidades.be_Requerimiento_x_pagar)
                    Dim objetoRequerimientos As Entidades.be_Requerimiento_x_pagar = Nothing

                    Dim idMovCtaPP As Integer = rdr.GetOrdinal("idMovCtaPP")
                    Dim _idCuentaProv As Integer = rdr.GetOrdinal("idCuentaProv")
                    Dim _idProveedor As Integer = rdr.GetOrdinal("idProveedor")
                    Dim _idProveedor2 As Integer = rdr.GetOrdinal("idProveedor2")
                    Dim _proveedor As Integer = rdr.GetOrdinal("proveedor")
                    Dim _mcp_Fecha As Integer = rdr.GetOrdinal("mcp_Fecha")
                    Dim _mcp_FechaCanc As Integer = rdr.GetOrdinal("mcp_FechaCanc")
                    Dim _mcp_Monto As Integer = rdr.GetOrdinal("mcp_Monto")
                    Dim _mcp_saldo As Integer = rdr.GetOrdinal("mcp_saldo")
                    Dim _idDocumento As Integer = rdr.GetOrdinal("idDocumento")
                    Dim _idDetalleConcepto As Integer = rdr.GetOrdinal("idDetalleConcepto")
                    Dim _doc_serie As Integer = rdr.GetOrdinal("doc_serie")
                    Dim _doc_codigo As Integer = rdr.GetOrdinal("doc_codigo")
                    Dim _doc_FechaEmision As Integer = rdr.GetOrdinal("doc_FechaEmision")
                    Dim _doc_FechaVenc As Integer = rdr.GetOrdinal("doc_FechaVenc")
                    Dim _doc_TotalAPagar As Integer = rdr.GetOrdinal("doc_TotalAPagar")
                    Dim _idMoneda As Integer = rdr.GetOrdinal("idMoneda")
                    Dim _idEmpresa As Integer = rdr.GetOrdinal("idEmpresa")
                    Dim _tienda As Integer = rdr.GetOrdinal("tienda")
                    Dim _idTienda As Integer = rdr.GetOrdinal("idTienda")
                    Dim _idTipoDocumento As Integer = rdr.GetOrdinal("idTipoDocumento")
                    Dim _tipoDocumento As Integer = rdr.GetOrdinal("tipoDocumento")
                    Dim _edoc_Nombre As Integer = rdr.GetOrdinal("edoc_Nombre")
                    Dim _PagoProgramado As Integer = rdr.GetOrdinal("PagoProgramado")
                    Dim _nroDocumento As Integer = rdr.GetOrdinal("nroDocumento")
                    Dim _nom_simbolo As Integer = rdr.GetOrdinal("mon_Simbolo")
                    Dim _NroItem As Integer = rdr.GetOrdinal("NroItem")
                    Dim _idUsuario As Integer = rdr.GetOrdinal("idUsuario")
                    Dim _Ruc As Integer = rdr.GetOrdinal("ruc")

                    While rdr.Read()
                        objetoRequerimientos = New Entidades.be_Requerimiento_x_pagar
                        With objetoRequerimientos
                            .idMovCtaPP = rdr.GetInt32(idMovCtaPP)
                            .idCuentaProv = rdr.GetInt32(_idCuentaProv)
                            .idProveedor = rdr.GetInt32(_idProveedor)
                            .idProveedor2 = rdr.GetInt32(_idProveedor2)
                            .proveedor = rdr.GetString(_proveedor)
                            .mcp_Fecha = rdr.GetDateTime(_mcp_Fecha)
                            .mcp_FechaCanc = rdr.GetString(_mcp_FechaCanc)
                            .mcp_Monto = rdr.GetDecimal(_mcp_Monto)
                            .mcp_saldo = rdr.GetDecimal(_mcp_saldo)
                            .idDocumento = rdr.GetInt32(_idDocumento)
                            .idDetalleConcepto = rdr.GetInt32(_idDetalleConcepto)
                            .doc_serie = rdr.GetString(_doc_serie)
                            .doc_codigo = rdr.GetString(_doc_codigo)
                            .doc_FechaEmision = rdr.GetDateTime(_doc_FechaEmision)
                            .doc_FechaVenc = rdr.GetDateTime(_doc_FechaVenc)
                            .doc_TotalAPagar = rdr.GetDecimal(_doc_TotalAPagar)
                            .idMoneda = rdr.GetInt32(_idMoneda)
                            .idEmpresa = rdr.GetInt32(_idEmpresa)
                            .tienda = rdr.GetString(_tienda)
                            .idTienda = rdr.GetInt32(_idTienda)
                            .idTipoDocumento = rdr.GetInt32(_idTipoDocumento)
                            .tipoDocumento = rdr.GetString(_tipoDocumento)
                            .edoc_Nombre = rdr.GetString(_edoc_Nombre)
                            .PagoProgramado = rdr.GetInt32(_PagoProgramado)
                            .nroDocumento = rdr.GetString(_nroDocumento)
                            .nom_simbolo = rdr.GetString(_nom_simbolo)
                            .NroItem = rdr.GetInt32(_NroItem)
                            .idUsuario = rdr.GetInt32(_idUsuario)
                            .ruc = rdr.GetString(_Ruc)
                        End With
                        listaReq.Add(objetoRequerimientos)
                    End While
                    objeto.listaRequerimientos = listaReq
                End If
                If rdr.NextResult Then
                    Dim objetoAgente As Entidades.TipoAgente = Nothing
                    Dim listaAgente As New List(Of Entidades.TipoAgente)
                    While rdr.Read()
                        objetoAgente = New Entidades.TipoAgente
                        With objetoAgente
                            .IdAgente = rdr.GetInt32(0)
                            .Nombre = rdr.GetString(1)
                            .Tasa = rdr.GetDecimal(2)
                            .tipoAgente = rdr.GetInt32(3)
                        End With
                        listaAgente.Add(objetoAgente)
                    End While
                    objeto.listaAgentes = listaAgente
                End If
                rdr.Close()
            End If
            Return objeto
        End Using
    End Function

    Public Function MovCuentaPorPagar_SelectxParams_DataTable(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdMoneda As Integer, ByVal IdPersona As Integer, ByVal FechaVctoInicio As Date, ByVal FechaVctoFin As Date, ByVal Opcion_Programacion As Integer, ByVal Opcion_Deuda As Integer, ByVal PageIndex As Integer, ByVal PageSize As Integer, ByVal IdTipoDocumento As Integer, ByVal Solicitud As Integer) As DataTable

        Dim ds As New DataSet

        Try

            Dim p() As SqlParameter = New SqlParameter(11) {}

            p(0) = MyBase.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(1) = MyBase.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(2) = MyBase.getParam(IdMoneda, "@IdMoneda", SqlDbType.Int)
            p(3) = MyBase.getParam(IdPersona, "@IdPersona", SqlDbType.Int)
            p(4) = MyBase.getParam(FechaVctoInicio, "@FechaVctoInicio", SqlDbType.Date)
            p(5) = MyBase.getParam(FechaVctoFin, "@FechaVctoFin", SqlDbType.Date)
            p(6) = MyBase.getParam(Opcion_Programacion, "@Opcion_Programacion", SqlDbType.Int)
            p(7) = MyBase.getParam(Opcion_Deuda, "@Opcion_Deuda", SqlDbType.Int)
            p(8) = MyBase.getParam(PageIndex, "@PageIndex", SqlDbType.Int)
            p(9) = MyBase.getParam(PageSize, "@PageSize", SqlDbType.Int)
            p(10) = MyBase.getParam(IdTipoDocumento, "@IdTipoDocumento", SqlDbType.Int)
            p(11) = MyBase.getParam(Solicitud, "@Solicitud", SqlDbType.Int)


            Dim cmd As New SqlCommand("_MovCuentaPorPagar_SelectxParams", (New DAO.Conexion).ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)

            da.Fill(ds, "DT_MovCuenta_CXP")

        Catch ex As Exception

            Throw ex

        End Try

        Return ds.Tables("DT_MovCuenta_CXP")

    End Function
    Public Function MovCuentaPorPagar_SelectxParams_DataTable2(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdMoneda As Integer, ByVal IdPersona As Integer, ByVal FechaVctoInicio As Date, ByVal FechaVctoFin As Date, ByVal Opcion_Programacion As Integer, ByVal Opcion_Deuda As Integer, ByVal PageIndex As Integer, ByVal PageSize As Integer, ByVal IdTipoDocumento As Integer, ByVal Solicitud As Integer) As DataTable

        Dim ds As New DataSet

        Try

            Dim p() As SqlParameter = New SqlParameter(10) {}

            p(0) = MyBase.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(1) = MyBase.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(2) = MyBase.getParam(IdMoneda, "@IdMoneda", SqlDbType.Int)
            p(3) = MyBase.getParam(IdPersona, "@IdPersona", SqlDbType.Int)
            p(4) = MyBase.getParam(FechaVctoInicio, "@FechaVctoInicio", SqlDbType.Date)
            p(5) = MyBase.getParam(FechaVctoFin, "@FechaVctoFin", SqlDbType.Date)
            p(6) = MyBase.getParam(Opcion_Programacion, "@Opcion_Programacion", SqlDbType.Int)
            p(7) = MyBase.getParam(Opcion_Deuda, "@Opcion_Deuda", SqlDbType.Int)
            p(8) = MyBase.getParam(PageIndex, "@PageIndex", SqlDbType.Int)
            p(9) = MyBase.getParam(PageSize, "@PageSize", SqlDbType.Int)
            p(10) = MyBase.getParam(IdTipoDocumento, "@IdTipoDocumento", SqlDbType.Int)



            Dim cmd As New SqlCommand("_MovCuentaPorPagar_SelectxParams", (New DAO.Conexion).ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)

            da.Fill(ds, "DT_MovCuenta_CXP")

        Catch ex As Exception

            Throw ex

        End Try

        Return ds.Tables("DT_MovCuenta_CXP")

    End Function
    Public Sub UpdateSaldoxIdMovCuenta(ByVal IdMovCuentaCxP As Integer, ByVal saldo As Decimal, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(1) {}
        p(0) = MyBase.getParam(IdMovCuentaCxP, "@IdMovCuentaCxP", SqlDbType.Int)
        p(1) = MyBase.getParam(saldo, "@Saldo", SqlDbType.Decimal)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_MovCuentaxPagar_UpdateSaldoxIdMovCuenta", p)


    End Sub

    Public Function SelectDatosCancelacion(ByVal IdDocumento As String) As List(Of Entidades.MovCuentaPorPagar)

        Dim lista As New List(Of Entidades.MovCuentaPorPagar)

        Try
            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = MyBase.getParam(IdDocumento, "@IdDocumento", SqlDbType.VarChar)

            MyBase.Cn = (New DAO.Conexion).ConexionSIGE
            MyBase.Cn.Open()

            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(MyBase.Cn, CommandType.StoredProcedure, "SelectDatosCancelacionxIddocumento", p)

            While (reader.Read)

                Dim obj As New Entidades.MovCuentaPorPagar

                With obj

                    .IdDocumento = MyBase.UCInt(reader("IdDocumento"))
                    .banco = MyBase.UCStr(reader("banco"))
                    .nrocuenta = MyBase.UCStr(reader("nrocuenta"))
                    .dcan_NumeroCheque = MyBase.UCStr(reader("dcan_NumeroCheque"))
                    .moneda = MyBase.UCStr(reader("moneda"))
                    .dcan_monto = MyBase.UCDec(reader("dcan_monto"))
                End With
                lista.Add(obj)

            End While
            reader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (MyBase.Cn.State = ConnectionState.Open) Then MyBase.Cn.Close()
        End Try

        Return lista

    End Function

    Public Function SelectxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.MovCuentaPorPagar)

        Dim lista As New List(Of Entidades.MovCuentaPorPagar)

        Try
            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = MyBase.getParam(IdDocumento, "@IdDocumento", SqlDbType.Int)

            MyBase.Cn = (New DAO.Conexion).ConexionSIGE
            MyBase.Cn.Open()

            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(MyBase.Cn, CommandType.StoredProcedure, "_MovCuentaxPagar_SelectxIdDocumento", p)

            While (reader.Read)

                Dim obj As New Entidades.MovCuentaPorPagar

                With obj

                    .Id = MyBase.UCInt(reader("IdMovCtaPP"))
                    .IdCuentaProv = MyBase.UCInt(reader("IdCuentaProv"))
                    .IdProveedor = MyBase.UCInt(reader("IdProveedor"))
                    .Fecha = MyBase.UCDate(reader("mcp_Fecha"))
                    .FechaCancelacion = MyBase.UCDate(reader("mcp_FechaCanc"))
                    .Monto = MyBase.UCDec(reader("mcp_Monto"))
                    .Saldo = MyBase.UCDec(reader("mcp_Saldo"))
                    .Factor = MyBase.UCInt(reader("mcp_Factor"))
                    .IdDocumento = MyBase.UCInt(reader("IdDocumento"))
                    .IdDetalleConcepto = MyBase.UCInt(reader("IdDetalleConcepto"))
                    .IdMovCuentaTipo = MyBase.UCInt(reader("IdMovCuentaTipo"))
                    .IdCargoCuentaP = MyBase.UCInt(reader("IdCargoCuentaP"))
                    .FlagSustento = MyBase.UCInt(reader("flagsustento"))
                End With

                lista.Add(obj)

            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (MyBase.Cn.State = ConnectionState.Open) Then MyBase.Cn.Close()
        End Try

        Return lista

    End Function

    Public Function SelectDeudaxIdProveedor(ByVal IdProveedor As Integer) As List(Of Entidades.MovCuentaPorPagar)

        Cn = objConexion.ConexionSIGE()
        Dim Prm As SqlParameter = getParam(IdProveedor, "@IdProveedor", SqlDbType.Int)

        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()

                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "_MovCuentaPorPagarSelectDeudaxIdProveedor", Prm)

                Dim Lista As New List(Of Entidades.MovCuentaPorPagar)
                With lector
                    Do While .Read

                        Dim obj As New Entidades.MovCuentaPorPagar

                        obj.Id = UCInt(.Item("IdMovCtaPP"))
                        obj.IdCuentaProv = UCInt(.Item("IdCuentaProv"))
                        obj.IdProveedor = UCInt(.Item("IdProveedor"))
                        obj.IdMovCuentaTipo = UCInt(.Item("IdMovCuentaTipo"))
                        obj.Monto = UCDec(.Item("mcp_Monto"))
                        obj.Saldo = UCDec(.Item("mcp_Saldo"))
                        obj.Fecha = UCDate(.Item("mcp_Fecha"))
                        obj.FechaCancelacion = UCDate(.Item("mcp_FechaCanc"))
                        obj.Factor = UCInt(.Item("mcp_Factor"))
                        obj.IdDocumento = UCInt(.Item("IdDocumento"))
                        obj.Codigo = UCStr(.Item("doc_Codigo"))
                        obj.Serie = UCStr(.Item("doc_Serie"))
                        obj.NomTipoDocumento = UCStr(.Item("TipoDocumentoNomCorto"))
                        obj.IdMoneda = UCInt(.Item("IdMoneda"))
                        obj.MonedaSimbolo = UCStr(.Item("mon_Simbolo"))
                        Lista.Add(obj)

                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try


    End Function

    Public Function SelectDeudaxIdProveedorxIdMoneda(ByVal IdProveedor As Integer, ByVal IdMonedaDestino As Integer) As List(Of Entidades.Documento_MovCuentaPorPagar)

        Cn = objConexion.ConexionSIGE()
        Dim Prm() As SqlParameter = New SqlParameter(1) {}
        Prm(0) = getParam(IdProveedor, "@IdProveedor", SqlDbType.Int)
        Prm(1) = getParam(IdMonedaDestino, "@IdMonedaDestino", SqlDbType.Int)

        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()

                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "_MovCuentaPorPagarSelectDeudaxIdProveedorxIdMoneda", Prm)

                Dim Lista As New List(Of Entidades.Documento_MovCuentaPorPagar)
                With lector
                    Do While .Read

                        Dim obj As New Entidades.Documento_MovCuentaPorPagar

                        obj.IdMovCtaPP = UCInt(.Item("IdMovCtaPP"))
                        obj.Id = UCInt(.Item("IdDocumento"))
                        obj.MontoTotal = UCDec(.Item("MontoTotal"))
                        obj.Saldo = UCDec(.Item("Saldo"))
                        obj.Serie = UCStr(.Item("doc_Serie"))
                        obj.Codigo = UCStr(.Item("doc_Codigo"))
                        obj.NomTipoDocumento = UCStr(.Item("tdoc_NombreCorto"))
                        obj.FechaEmision = UCDate(.Item("doc_FechaEmision"))
                        obj.FechaVenc = UCDate(.Item("doc_FechaVenc"))
                        obj.NroDiasMora = UCInt(.Item("NroDiasMora"))
                        obj.NomMoneda = UCStr(.Item("MonedaDestino"))

                        Lista.Add(obj)

                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try


    End Function

    Public Function SelectDocumentosCancelacionBancosxSustentar( _
    ByVal IdProveedor As Integer, ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal IdTienda As Integer) As List(Of Entidades.Documento_MovCuentaPorPagar)

        Cn = objConexion.ConexionSIGE()
        Dim Prm() As SqlParameter = New SqlParameter(3) {}
        Prm(0) = getParam(IdProveedor, "@IdProveedor", SqlDbType.Int)
        Prm(1) = getParam(FechaInicio, "@FechaInicio", SqlDbType.Date)
        Prm(2) = getParam(FechaFin, "@FechaFin", SqlDbType.Date)
        Prm(3) = getParam(IdTienda, "@Tienda", SqlDbType.Int)
        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()

                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "_SelectCancelacionBancosxSustentar", Prm)

                Dim Lista As New List(Of Entidades.Documento_MovCuentaPorPagar)
                With lector
                    Do While .Read

                        Dim obj As New Entidades.Documento_MovCuentaPorPagar

                        obj.IdMovCtaPP = UCInt(.Item("IdMovCtaPP"))
                        obj.Id = UCInt(.Item("IdDocumento"))
                        obj.MontoTotal = UCDec(.Item("MontoTotal"))
                        obj.Saldo = UCDec(.Item("Saldo"))
                        obj.Serie = UCStr(.Item("doc_Serie"))
                        obj.Codigo = UCStr(.Item("doc_Codigo"))
                        obj.NomTipoDocumento = UCStr(.Item("tdoc_NombreCorto"))
                        obj.FechaEmision = UCDate(.Item("doc_FechaEmision"))
                        obj.FechaVenc = UCDate(.Item("doc_FechaVenc"))
                        obj.NroDiasMora = UCInt(.Item("NroDiasMora"))
                        obj.NomMoneda = UCStr(.Item("Moneda"))
                        obj.Sustento = UCInt(.Item("flagSustento"))
                        obj.Tienda = UCStr(.Item("Tienda"))
                        obj.DescripcionPersona = UCStr(.Item("DescripcionPersona"))
                        obj.NroDocumento = obj.Serie + "-" + obj.Codigo
                        Lista.Add(obj)
                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try


    End Function
    Public Function SelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancelTwo( _
   ByVal IdProveedor As Integer, ByVal IdMonedaDestino As Integer, _
   ByVal IdTipoDocCancel As Integer, ByVal fecha As Date) As List(Of Entidades.Documento_MovCuentaPorPagar)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MovCuentaPorPagarSelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancel2", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        cmd.Parameters.AddWithValue("@IdProveedor", IdProveedor)
        cmd.Parameters.AddWithValue("@IdMonedaDestino", IdMonedaDestino)
        cmd.Parameters.AddWithValue("@IdTipoDocCancel", IdTipoDocCancel)
        cmd.Parameters.AddWithValue("@Fechafin", fecha)
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Documento_MovCuentaPorPagar)
                With lector
                    Do While .Read
                        Dim obj As New Entidades.Documento_MovCuentaPorPagar
                        obj.IdMovCtaPP = UCInt(.Item("IdMovCtaPP"))
                        obj.Id = UCInt(.Item("IdDocumento"))
                        obj.MontoTotal = UCDec(.Item("MontoTotal"))
                        obj.Saldo = UCDec(.Item("Saldo"))
                        obj.Serie = UCStr(.Item("doc_Serie"))
                        obj.Codigo = UCStr(.Item("doc_Codigo"))
                        obj.NomTipoDocumento = UCStr(.Item("tdoc_NombreCorto"))
                        obj.FechaEmision = UCDate(.Item("doc_FechaEmision"))
                        obj.FechaVenc = UCDate(.Item("doc_FechaVenc"))
                        obj.NroDiasMora = UCInt(.Item("NroDiasMora"))
                        obj.NomMoneda = UCStr(.Item("MonedaDestino"))
                        obj.Sustento = UCInt(.Item("flagSustento"))
                        Lista.Add(obj)

                    Loop
                    .Close()
                End With
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try


    End Function
    Public Function SelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancelRC( _
    ByVal IdProveedor As Integer, ByVal IdMonedaDestino As Integer, _
    ByVal IdTipoDocCancel As Integer, ByVal idTienda As Integer) As List(Of Entidades.Documento_MovCuentaPorPagar)

        Cn = objConexion.ConexionSIGE()
        Dim Prm() As SqlParameter = New SqlParameter(3) {}
        Prm(0) = getParam(IdProveedor, "@IdProveedor", SqlDbType.Int)
        Prm(1) = getParam(IdMonedaDestino, "@IdMonedaDestino", SqlDbType.Int)
        Prm(2) = getParam(IdTipoDocCancel, "@IdTipoDocCancel", SqlDbType.Int)
        Prm(3) = getParam(idTienda, "@idTienda", SqlDbType.Int)

        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()

                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "_MovCuentaPorPagarSelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancelRC_v2", Prm)

                Dim Lista As New List(Of Entidades.Documento_MovCuentaPorPagar)
                With lector
                    Do While .Read

                        Dim obj As New Entidades.Documento_MovCuentaPorPagar
                        obj.IdMovCtaPP = UCInt(.Item("IdMovCtaPP"))
                        obj.Id = UCInt(.Item("IdDocumento"))
                        obj.MontoTotal = UCDec(.Item("MontoTotal"))
                        obj.Saldo = UCDec(.Item("Saldo"))
                        obj.Serie = UCStr(.Item("doc_Serie"))
                        obj.Codigo = UCStr(.Item("doc_Codigo"))
                        obj.NomTipoDocumento = UCStr(.Item("tdoc_NombreCorto"))
                        obj.FechaEmision = UCDate(.Item("doc_FechaEmision"))
                        obj.FechaVenc = UCDate(.Item("doc_FechaVenc"))
                        obj.NroDiasMora = UCInt(.Item("NroDiasMora"))
                        obj.NomMoneda = UCStr(.Item("MonedaDestino"))
                        obj.Sustento = UCInt(.Item("flagSustento"))
                        Lista.Add(obj)
                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try


    End Function

    Public Function SelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancel( _
    ByVal IdProveedor As Integer, ByVal IdMonedaDestino As Integer, _
    ByVal IdTipoDocCancel As Integer) As List(Of Entidades.Documento_MovCuentaPorPagar)

        Cn = objConexion.ConexionSIGE()
        Dim Prm() As SqlParameter = New SqlParameter(2) {}
        Prm(0) = getParam(IdProveedor, "@IdProveedor", SqlDbType.Int)
        Prm(1) = getParam(IdMonedaDestino, "@IdMonedaDestino", SqlDbType.Int)
        Prm(2) = getParam(IdTipoDocCancel, "@IdTipoDocCancel", SqlDbType.Int)

        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()

                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "_MovCuentaPorPagarSelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancel", Prm)

                Dim Lista As New List(Of Entidades.Documento_MovCuentaPorPagar)
                With lector
                    Do While .Read

                        Dim obj As New Entidades.Documento_MovCuentaPorPagar

                        obj.IdMovCtaPP = UCInt(.Item("IdMovCtaPP"))
                        obj.Id = UCInt(.Item("IdDocumento"))
                        obj.MontoTotal = UCDec(.Item("MontoTotal"))
                        obj.Saldo = UCDec(.Item("Saldo"))
                        obj.Serie = UCStr(.Item("doc_Serie"))
                        obj.Codigo = UCStr(.Item("doc_Codigo"))
                        obj.NomTipoDocumento = UCStr(.Item("tdoc_NombreCorto"))
                        obj.FechaEmision = UCDate(.Item("doc_FechaEmision"))
                        obj.FechaVenc = UCDate(.Item("doc_FechaVenc"))
                        obj.NroDiasMora = UCInt(.Item("NroDiasMora"))
                        obj.NomMoneda = UCStr(.Item("MonedaDestino"))
                        obj.Sustento = UCInt(.Item("flagSustento"))
                        Lista.Add(obj)

                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try


    End Function

    Public Function SelectAbonosxIdMovCtaPP(ByVal IdMovCtaPP As Integer) As List(Of Entidades.Documento_MovCuentaPorPagar)
        Cn = objConexion.ConexionSIGE()

        Dim Prm As SqlParameter = getParam(IdMovCtaPP, "@IdMovCtaPP", SqlDbType.Int)

        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()

                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "_MovCuentaPorPagarSelectAbonosxIdMovCtaPP", Prm)

                Dim Lista As New List(Of Entidades.Documento_MovCuentaPorPagar)
                With lector
                    Do While .Read

                        Dim obj As New Entidades.Documento_MovCuentaPorPagar

                        obj.NomTipoDocumento = UCStr(.Item("TipoDocumentoNomCorto"))
                        obj.Serie = UCStr(.Item("doc_Serie"))
                        obj.Codigo = UCStr(.Item("doc_Codigo"))
                        obj.FechaEmision = UCDate(.Item("doc_FechaEmision"))
                        obj.NomMoneda = UCStr(.Item("mon_Simbolo"))
                        obj.TotalAPagar = UCDec(.Item("doc_TotalAPagar"))

                        Lista.Add(obj)

                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function InsertT_GetID(ByVal x As Entidades.MovCuentaPorPagar, _
Optional ByRef Cnx As SqlConnection = Nothing, _
Optional ByRef Trx As SqlTransaction = Nothing) As Integer

        Return QueryT(GetVectorParametros(x, modo_query.Insert), modo_query.Insert, "_MovCuentaPorPagar_Insert", Cnx, Trx)
    End Function
    Public Function InsertT(ByVal x As Entidades.MovCuentaPorPagar, _
    Optional ByRef Cnx As SqlConnection = Nothing, _
    Optional ByRef Trx As SqlTransaction = Nothing) As Boolean
        If InsertT_GetID(x, Cnx, Trx) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function UpdateT(ByVal x As Entidades.MovCuentaPorPagar, _
    Optional ByRef Cnx As SqlConnection = Nothing, _
    Optional ByRef Trx As SqlTransaction = Nothing) As Boolean
        Return CBool(QueryT(GetVectorParametros(x, modo_query.Update), modo_query.Update, "_MovCuentaPorPagar_Update", Cnx, Trx))
    End Function

    Public Function GetVectorParametros(ByVal x As Entidades.MovCuentaPorPagar, ByVal op As modo_query) As SqlParameter()

        Dim Prm() As SqlParameter

        Select Case op
            Case modo_query.Show, modo_query.Update, modo_query.Insert

                If op <> modo_query.Insert Then
                    Prm = New SqlParameter(10) {}
                    'Id de la Tabla
                    Prm(10) = getParam(x.Id, "@IdMovCtaPP", SqlDbType.Int)
                Else
                    Prm = New SqlParameter(9) {}
                End If

                Prm(0) = getParam(x.IdCuentaProv, "@IdCuentaProv", SqlDbType.Int)
                Prm(1) = getParam(x.IdProveedor, "@IdProveedor", SqlDbType.Int)
                Prm(2) = getParam(x.FechaCancelacion, "@mcp_FechaCanc", SqlDbType.Date)
                Prm(3) = getParam(x.Monto, "@mcp_monto", SqlDbType.Decimal)
                Prm(4) = getParam(x.Saldo, "@mcp_Saldo", SqlDbType.Decimal)
                Prm(5) = getParam(x.Factor, "@mcp_Factor", SqlDbType.Int)
                Prm(6) = getParam(x.IdDocumento, "@IdDocumento", SqlDbType.Int)
                Prm(7) = getParam(x.IdDetalleConcepto, "@IdDetalleConcepto", SqlDbType.Int)
                Prm(8) = getParam(x.IdMovCuentaTipo, "@IdMovCuentaTipo", SqlDbType.Int)
                Prm(9) = getParam(x.IdCargoCuentaP, "@IdCargoCuentaP", SqlDbType.Int)

                'Case modo_query.Delete
            Case Else
                Prm = Nothing
        End Select

        Return Prm
    End Function
End Class
