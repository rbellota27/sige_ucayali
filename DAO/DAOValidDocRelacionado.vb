﻿
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOValidDocRelacionado
    Private objConexion As New DAO.Conexion

    Public Function SelectDocRefValidNotaCredito(ByVal IdCliente As Integer, ByVal FechaI As Date, ByVal FechaF As Date, _
                                                 ByVal IdTienda As Integer, ByVal serie As Integer, ByVal codigo As Integer, _
                                                 ByVal opcion As Integer, ByVal ValDocRelacionado As Boolean, _
                                                 ByVal idTipoDocumento As Integer) As List(Of Entidades.DocumentoValid)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("[ValidaDoc_Despacho_NotaCredito_v2]", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdCliente", IdCliente)
        cmd.Parameters.AddWithValue("@FechaI", FechaI)
        cmd.Parameters.AddWithValue("@FechaF", FechaF)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@Serie", serie)
        cmd.Parameters.AddWithValue("@Codigo", codigo)
        cmd.Parameters.AddWithValue("@Opcion", opcion)
        cmd.Parameters.AddWithValue("@ValDocRelacionado", ValDocRelacionado)
        cmd.Parameters.AddWithValue("@idTipoDocumento", idTipoDocumento)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.DocumentoValid)
                Do While lector.Read
                    Dim obj As New Entidades.DocumentoValid
                    With obj
                        .IdDocumento = CInt(IIf(IsDBNull(lector("IdDocumento")) = True, 0, lector("IdDocumento")))
                        .NomTipoDocumento = CStr(IIf(IsDBNull(lector("tdoc_NombreCorto")) = True, "", lector("tdoc_NombreCorto")))
                        .Serie = CStr(IIf(IsDBNull(lector("doc_Serie")) = True, "", lector("doc_Serie")))
                        .Codigo = CStr(IIf(IsDBNull(lector("doc_Codigo")) = True, "", lector("doc_Codigo")))
                        .FechaEmision = CStr(IIf(IsDBNull(lector("doc_FechaEmision")) = True, "", Format(lector("doc_FechaEmision"), "dd/MM/yyyy")))
                        .Numero = .Serie + " - " + .Codigo
                        .strTipoDocumentoRef = CStr(IIf(IsDBNull(lector("CadTipoDocumentoRef")) = True, "", lector("CadTipoDocumentoRef")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

End Class
