'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOSerie
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaSerie(ByVal serie As Entidades.Serie) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(8) {}
        ArrayParametros(0) = New SqlParameter("@ser_Numero", SqlDbType.VarChar)
        ArrayParametros(0).Value = serie.Numero
        ArrayParametros(1) = New SqlParameter("@ser_Inicio", SqlDbType.Int)
        ArrayParametros(1).Value = serie.Inicio
        ArrayParametros(2) = New SqlParameter("@ser_Cantidad", SqlDbType.Int)
        ArrayParametros(2).Value = serie.Cantidad
        ArrayParametros(3) = New SqlParameter("@ser_FechaAlta", SqlDbType.DateTime)
        ArrayParametros(3).Value = DBNull.Value
        ArrayParametros(4) = New SqlParameter("@ser_Estado", SqlDbType.Char)
        ArrayParametros(4).Value = serie.Estado
        ArrayParametros(5) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(5).Value = serie.IdTienda
        ArrayParametros(6) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(6).Value = serie.IdEmpresa
        ArrayParametros(7) = New SqlParameter("@ser_Longitud", SqlDbType.Int)
        ArrayParametros(7).Value = serie.Longitud
        ArrayParametros(8) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        ArrayParametros(8).Value = serie.IdTipoDocumento
        Return HDAO.Insert(cn, "_SerieInsert", ArrayParametros)
    End Function
    Public Function ActualizaSerie(ByVal serie As Entidades.Serie) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(9) {}
        ArrayParametros(0) = New SqlParameter("@ser_Numero", SqlDbType.VarChar)
        ArrayParametros(0).Value = serie.Numero
        ArrayParametros(1) = New SqlParameter("@ser_Inicio", SqlDbType.Int)
        ArrayParametros(1).Value = serie.Inicio
        ArrayParametros(2) = New SqlParameter("@ser_Cantidad", SqlDbType.Int)
        ArrayParametros(2).Value = serie.Cantidad
        ArrayParametros(3) = New SqlParameter("@ser_FechaAlta", SqlDbType.DateTime)
        ArrayParametros(3).Value = DBNull.Value
        ArrayParametros(4) = New SqlParameter("@ser_Estado", SqlDbType.Char)
        ArrayParametros(4).Value = serie.Estado
        ArrayParametros(5) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(5).Value = serie.IdTienda
        ArrayParametros(6) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(6).Value = serie.IdEmpresa
        ArrayParametros(7) = New SqlParameter("@ser_Longitud", SqlDbType.Int)
        ArrayParametros(7).Value = serie.Longitud
        ArrayParametros(8) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
        ArrayParametros(8).Value = serie.IdTipoDocumento
        ArrayParametros(9) = New SqlParameter("@IdSerie", SqlDbType.Int)
        ArrayParametros(9).Value = serie.IdSerie
        Return HDAO.Update(cn, "_SerieUpdate", ArrayParametros)
    End Function
    'Public Function LLenarCboSeriexIdUsuarioValidacion(ByVal IdUsuario As Integer, ByVal IdTienda As Integer) As Entidades.Serie
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim cmd As New SqlCommand("LLenarCboSeriexIdUsuarioValidacion", cn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario)
    '    cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
    '    Dim serie As New Entidades.Serie
    '    Dim lector As SqlDataReader
    '    Try
    '        Using cn
    '            cn.Open()
    '            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
    '            If lector.HasRows = False Then

    '                serie.IdSerie = 0
    '                serie.Numero = "0"
    '            Else
    '                lector.Read()
    '                serie.IdSerie = CInt(IIf(IsDBNull(lector.Item("IdSerie")) = True, "0", lector.Item("IdSerie")))
    '                serie.Numero = CStr(IIf(IsDBNull(lector.Item("ser_Numero")) = True, "0", lector.Item("ser_Numero")))

    '            End If
    '            Return serie
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    Public Function LLenarCboSeriexIdUsuarioValidacion(ByVal IdUsuario As Integer, ByVal IdTienda As Integer) As List(Of Entidades.Serie)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("LLenarCboSeriexIdUsuarioValidacion", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        Dim Lista As New List(Of Entidades.Serie)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                If lector.HasRows = False Then
                    Dim serie As New Entidades.Serie
                    Lista = Nothing
                Else
                    Do While lector.Read
                        Dim serie As New Entidades.Serie
                        serie.IdSerie = CInt(IIf(IsDBNull(lector.Item("IdSerie")) = True, "0", lector.Item("IdSerie")))
                        serie.Numero = CStr(IIf(IsDBNull(lector.Item("ser_Numero")) = True, "0", lector.Item("ser_Numero")))
                        Lista.Add(serie)
                    Loop
                End If

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectCboxIdsEmpTienTDocNotinAsignacion(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdTipoDocumento As Integer) As List(Of Entidades.Serie)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("SelectCboxIdsEmpTienTDocNotinAsignacion", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                'If lector.HasRows = False Then
                '    Throw New Exception("No Existe Serie asignada para este Documento")
                'End If
                Dim Lista As New List(Of Entidades.Serie)
                Do While lector.Read
                    Dim serie As New Entidades.Serie
                    serie.IdSerie = CInt(IIf(IsDBNull(lector.Item("IdSerie")) = True, "0", lector.Item("IdSerie")))
                    serie.Numero = CStr(IIf(IsDBNull(lector.Item("ser_Numero")) = True, "0", lector.Item("ser_Numero")))
                    'serie.Inicio = CInt(IIf(IsDBNull(lector.Item("ser_Inicio")) = True, "0", lector.Item("ser_Inicio")))
                    'serie.Cantidad = CInt(IIf(IsDBNull(lector.Item("ser_Cantidad")) = True, "0", lector.Item("ser_Cantidad")))
                    'serie.Longitud = CInt(IIf(IsDBNull(lector.Item("ser_Longitud")) = True, "0", lector.Item("ser_Longitud")))
                    Lista.Add(serie)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectCboxIdsEmpTienTDoc(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdTipoDocumento As Integer) As List(Of Entidades.Serie)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SerieSelectxIdsEmpTienTDoc", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdTipoDocumento", IdTipoDocumento)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                'If lector.HasRows = False Then
                '    Throw New Exception("No Existe Serie asignada para este Documento")
                'End If
                Dim Lista As New List(Of Entidades.Serie)
                Do While lector.Read
                    Dim serie As New Entidades.Serie
                    serie.IdSerie = CInt(IIf(IsDBNull(lector.Item("IdSerie")) = True, "0", lector.Item("IdSerie")))
                    serie.Numero = CStr(IIf(IsDBNull(lector.Item("ser_Numero")) = True, "0", lector.Item("ser_Numero")))
                    'serie.Inicio = CInt(IIf(IsDBNull(lector.Item("ser_Inicio")) = True, "0", lector.Item("ser_Inicio")))
                    'serie.Cantidad = CInt(IIf(IsDBNull(lector.Item("ser_Cantidad")) = True, "0", lector.Item("ser_Cantidad")))
                    'serie.Longitud = CInt(IIf(IsDBNull(lector.Item("ser_Longitud")) = True, "0", lector.Item("ser_Longitud")))
                    Lista.Add(serie)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectxIdSerie(ByVal IdSerie As Integer) As Entidades.Serie
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SerieSelectxIdSerie", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSerie", IdSerie)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                lector.Read()
                Dim obj As New Entidades.Serie
                obj.Cantidad = CInt(IIf(IsDBNull(lector.Item("ser_Cantidad")) = True, 0, lector.Item("ser_Cantidad")))
                obj.Estado = CStr(IIf(IsDBNull(lector.Item("ser_Estado")) = True, "", lector.Item("ser_Estado")))
                obj.FechaAlta = CDate(IIf(IsDBNull(lector.Item("ser_FechaAlta")) = True, Nothing, lector.Item("ser_FechaAlta")))
                obj.IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                obj.IdSerie = CInt(IIf(IsDBNull(lector.Item("IdSerie")) = True, 0, lector.Item("IdSerie")))
                obj.IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                obj.IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                obj.Inicio = CInt(IIf(IsDBNull(lector.Item("ser_Inicio")) = True, 0, lector.Item("ser_Inicio")))
                obj.Longitud = CInt(IIf(IsDBNull(lector.Item("ser_Longitud")) = True, 0, lector.Item("ser_Longitud")))
                obj.Numero = CStr(IIf(IsDBNull(lector.Item("ser_Numero")) = True, "", lector.Item("ser_Numero")))
                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectxEmpresaxTiendaxTipoDocxEstado(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdTipoDoc As Integer, ByVal estado As String) As List(Of Entidades.Serie)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SerieSelectxEmpresaxTiendaxTipoDocxEstado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        cmd.Parameters.AddWithValue("@IdTipoDoc", IdTipoDoc)
        cmd.Parameters.AddWithValue("@Estado", estado)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Serie)
                Do While lector.Read
                    Dim obj As New Entidades.Serie
                    obj.Cantidad = CInt(IIf(IsDBNull(lector.Item("ser_Cantidad")) = True, 0, lector.Item("ser_Cantidad")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("ser_Estado")) = True, "", lector.Item("ser_Estado")))
                    obj.FechaAlta = CDate(IIf(IsDBNull(lector.Item("ser_FechaAlta")) = True, Nothing, lector.Item("ser_FechaAlta")))
                    'obj.IdEmpresa = CInt(IIf(IsDBNull(lector.Item("")) = True, 0, lector.Item("")))
                    obj.IdSerie = CInt(IIf(IsDBNull(lector.Item("IdSerie")) = True, 0, lector.Item("IdSerie")))
                    'obj.IdTienda = CInt(IIf(IsDBNull(lector.Item("")) = True, 0, lector.Item("")))
                    'obj.IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("")) = True, 0, lector.Item("")))
                    obj.Inicio = CInt(IIf(IsDBNull(lector.Item("ser_Inicio")) = True, 0, lector.Item("ser_Inicio")))
                    obj.Longitud = CInt(IIf(IsDBNull(lector.Item("ser_Longitud")) = True, 0, lector.Item("ser_Longitud")))
                    obj.NomEmpresa = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                    obj.NomTienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))
                    obj.NomTipoDoc = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                    obj.Numero = CStr(IIf(IsDBNull(lector.Item("ser_Numero")) = True, "", lector.Item("ser_Numero")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
