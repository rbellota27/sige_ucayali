﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOSubLinea_TipoTablaValor
    Dim Helper As New DAO.HelperDAO
    Dim objConexion As New DAO.Conexion

    Public Function ConfigTablaValor_Paginado(ByVal idlinea As Integer, ByVal idsublinea As Integer, _
                                              ByVal estado As Integer, ByVal idtipoexistencia As Integer, _
                                              ByVal PageIndex As Integer, ByVal PageSize As Integer) As List(Of Entidades.SubLinea_TipoTablaValor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ConfigTablaValor_Paginado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        cmd.Parameters.AddWithValue("@Estado", estado)
        cmd.Parameters.AddWithValue("@idtipoexistencia", idtipoexistencia)
        cmd.Parameters.AddWithValue("@PageIndex", PageIndex)
        cmd.Parameters.AddWithValue("@PageSize", PageSize)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea_TipoTablaValor)
                Do While lector.Read
                    Dim obj As New Entidades.SubLinea_TipoTablaValor
                    obj.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    obj.NomLinea = CStr(IIf(IsDBNull(lector.Item("Linea")) = True, "", lector.Item("Linea")))
                    obj.IdSubLinea = CInt(IIf(IsDBNull(lector.Item("IdSubLinea")) = True, 0, lector.Item("IdSubLinea")))
                    obj.NomSubLinea = CStr(IIf(IsDBNull(lector.Item("SubLinea")) = True, "", lector.Item("SubLinea")))
                    obj.Nombre = LTrim(RTrim(CStr(IIf(IsDBNull(lector.Item("TipoTablaValor")) = True, "", lector.Item("TipoTablaValor")))))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("Estado")) = True, 0, lector.Item("Estado")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function GrabaSubLineaTipoTablaValor(ByVal lista As List(Of Entidades.SubLinea_TipoTablaValor), ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Try
            For i As Integer = 0 To lista.Count - 1
                Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
                ArrayParametros(0) = New SqlParameter("@IdSubLinea", SqlDbType.Int)
                ArrayParametros(0).Value = IIf(IsDBNull(lista.Item(i).IdSubLinea) = True, 0, lista.Item(i).IdSubLinea)
                ArrayParametros(1) = New SqlParameter("@IdTipoTabla", SqlDbType.Int)
                ArrayParametros(1).Value = IIf(IsDBNull(lista.Item(i).IdTipoTabla) = True, 0, lista.Item(i).IdTipoTabla)
                ArrayParametros(2) = New SqlParameter("@IdTipoTablaValor", SqlDbType.Int)
                ArrayParametros(2).Value = IIf(IsDBNull(lista.Item(i).IdTipoTablaValor) = True, 0, lista.Item(i).IdTipoTablaValor)
                ArrayParametros(3) = New SqlParameter("@slttv_Estado", SqlDbType.Int)
                ArrayParametros(3).Value = IIf(IsDBNull(lista.Item(i).Estado) = True, 0, lista.Item(i).Estado)

                Dim cmd As New SqlCommand("InsUpd_SubLinea_TipoTablaValor", cn, tr)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(ArrayParametros)
                cmd.ExecuteNonQuery()
            Next
            Return True
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function AnularSubLineaTipoTablaValor(ByVal idsublinea As Integer, ByVal idtipotabla As Integer, ByVal IdTipoTablaValor As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@IdSubLinea", SqlDbType.Int)
        ArrayParametros(0).Value = idsublinea
        ArrayParametros(1) = New SqlParameter("@IdTipoTabla", SqlDbType.Int)
        ArrayParametros(1).Value = idtipotabla
        ArrayParametros(2) = New SqlParameter("@IdTipoTablaValor", SqlDbType.Int)
        ArrayParametros(2).Value = IdTipoTablaValor

        Dim cmd As New SqlCommand("_SubLineaTipoTablaValorAnular", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return True
    End Function
    Public Function SelectAllxIdLineaxIdSubLineaxEstado(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal estado As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.SubLinea_TipoTablaValor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SubLinea_TipoTablaValorSelectAllxIdLineaxIdSubLineaxEstado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        cmd.Parameters.AddWithValue("@Estado", estado)
        cmd.Parameters.AddWithValue("@idtipoexistencia", idtipoexistencia)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea_TipoTablaValor)
                Do While lector.Read
                    Dim obj As New Entidades.SubLinea_TipoTablaValor
                    obj.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    obj.NomLinea = CStr(IIf(IsDBNull(lector.Item("Linea")) = True, "", lector.Item("Linea")))
                    obj.IdSubLinea = CInt(IIf(IsDBNull(lector.Item("IdSubLinea")) = True, 0, lector.Item("IdSubLinea")))
                    obj.NomSubLinea = CStr(IIf(IsDBNull(lector.Item("SubLinea")) = True, "", lector.Item("SubLinea")))
                    'obj.IdTipoTabla = CInt(IIf(IsDBNull(lector.Item("IdTipoTabla")) = True, 0, lector.Item("IdTipoTabla")))
                    'obj.NomTipoTabla = CStr(IIf(IsDBNull(lector.Item("TipoTabla")) = True, "", lector.Item("TipoTabla")))
                    obj.Nombre = LTrim(RTrim(CStr(IIf(IsDBNull(lector.Item("TipoTablaValor")) = True, "", lector.Item("TipoTablaValor")))))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("Estado")) = True, 0, lector.Item("Estado")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxIdLineaxIdSubLinea(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal IdtipoTabla As Integer, ByVal PageIndex As Integer, ByVal PageSize As Integer, ByVal Descripcion As String) As List(Of Entidades.SubLinea_TipoTablaValor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SubLinea_TipoTablaValorSelectIdLineaxIdSubLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        cmd.Parameters.AddWithValue("@IdTipoTabla", IdtipoTabla)
        cmd.Parameters.AddWithValue("@PageIndex", PageIndex)
        cmd.Parameters.AddWithValue("@PageSize", PageSize)
        cmd.Parameters.AddWithValue("@descripcion", Descripcion)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea_TipoTablaValor)
                Do While lector.Read
                    Dim obj As New Entidades.SubLinea_TipoTablaValor
                    obj.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    obj.NomLinea = CStr(IIf(IsDBNull(lector.Item("Linea")) = True, "", lector.Item("Linea")))
                    obj.IdSubLinea = CInt(IIf(IsDBNull(lector.Item("IdSubLinea")) = True, 0, lector.Item("IdSubLinea")))
                    obj.NomSubLinea = CStr(IIf(IsDBNull(lector.Item("SubLinea")) = True, "", lector.Item("SubLinea")))
                    obj.IdTipoTabla = CInt(IIf(IsDBNull(lector.Item("IdTipoTabla")) = True, 0, lector.Item("IdTipoTabla")))
                    obj.NomTipoTabla = CStr(IIf(IsDBNull(lector.Item("TipoTabla")) = True, "", lector.Item("TipoTabla")))
                    obj.TipoUso = CStr(IIf(IsDBNull(lector.Item("TipoUso")) = True, "", lector.Item("TipoUso")))
                    obj.IdTipoTablaValor = CInt(IIf(IsDBNull(lector.Item("IdTipoTablaValor")) = True, 0, lector.Item("IdTipoTablaValor")))
                    obj.Codigo = CStr(IIf(IsDBNull(lector.Item("Codigo")) = True, "", lector.Item("Codigo")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("TipoTablaValor")) = True, "", lector.Item("TipoTablaValor")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("Estado")) = True, 0, lector.Item("Estado")))
                    obj.Indicador = CBool(IIf(IsDBNull(lector.Item("Indicador")) = True, 0, lector.Item("Indicador")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxIdLineaxIdSubLineaxIdTipoTabla(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal idtipotabla As Integer, Optional ByVal textTabla As String = "") As List(Of Entidades.SubLinea_TipoTablaValor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SubLinea_TipoTablaValorSelectIdLineaxIdSubLineaxIdTipoTabla", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        cmd.Parameters.AddWithValue("@IdTipoTabla", idtipotabla)
        cmd.Parameters.AddWithValue("@texttabla", textTabla)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea_TipoTablaValor)
                Do While lector.Read
                    Dim obj As New Entidades.SubLinea_TipoTablaValor
                    obj.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    obj.IdSubLinea = CInt(IIf(IsDBNull(lector.Item("IdSubLinea")) = True, 0, lector.Item("IdSubLinea")))
                    obj.IdTipoTabla = CInt(IIf(IsDBNull(lector.Item("IdTipoTabla")) = True, 0, lector.Item("IdTipoTabla")))
                    obj.NomTipoTabla = CStr(IIf(IsDBNull(lector.Item("NomTipoTabla")) = True, "", lector.Item("NomTipoTabla")))
                    obj.NroOrden = CInt(IIf(IsDBNull(lector.Item("NroOrden")) = True, 0, lector.Item("NroOrden")))
                    obj.ParteNombre = CBool(IIf(IsDBNull(lector.Item("ParteNombre")) = True, 0, lector.Item("ParteNombre")))
                    obj.IdTipoTablaValor = CInt(IIf(IsDBNull(lector.Item("IdTipoTablaValor")) = True, 0, lector.Item("IdTipoTablaValor")))
                    obj.Codigo = CStr(IIf(IsDBNull(lector.Item("Codigo")) = True, "", lector.Item("Codigo")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("TipoTablaValor")) = True, "", lector.Item("TipoTablaValor")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("Estado")) = True, 0, lector.Item("Estado")))
                    obj.TipoUso = CStr(IIf(IsDBNull(lector.Item("sltt_TipoUso")) = True, "", lector.Item("sltt_TipoUso")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxIdLineaxIdSubLineaxIdTipoTabla2(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal idtipotabla As Integer, Optional ByVal textTabla As String = "") As List(Of Entidades.SubLinea_TipoTablaValor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE2
        Dim cmd As New SqlCommand("_SubLinea_TipoTablaValorSelectIdLineaxIdSubLineaxIdTipoTabla", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        cmd.Parameters.AddWithValue("@IdTipoTabla", idtipotabla)
        cmd.Parameters.AddWithValue("@texttabla", textTabla)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea_TipoTablaValor)
                Do While lector.Read
                    Dim obj As New Entidades.SubLinea_TipoTablaValor
                    obj.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    obj.IdSubLinea = CInt(IIf(IsDBNull(lector.Item("IdSubLinea")) = True, 0, lector.Item("IdSubLinea")))
                    obj.IdTipoTabla = CInt(IIf(IsDBNull(lector.Item("IdTipoTabla")) = True, 0, lector.Item("IdTipoTabla")))
                    obj.NomTipoTabla = CStr(IIf(IsDBNull(lector.Item("NomTipoTabla")) = True, "", lector.Item("NomTipoTabla")))
                    obj.NroOrden = CInt(IIf(IsDBNull(lector.Item("NroOrden")) = True, 0, lector.Item("NroOrden")))
                    obj.ParteNombre = CBool(IIf(IsDBNull(lector.Item("ParteNombre")) = True, 0, lector.Item("ParteNombre")))
                    obj.IdTipoTablaValor = CInt(IIf(IsDBNull(lector.Item("IdTipoTablaValor")) = True, 0, lector.Item("IdTipoTablaValor")))
                    obj.Codigo = CStr(IIf(IsDBNull(lector.Item("Codigo")) = True, "", lector.Item("Codigo")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("TipoTablaValor")) = True, "", lector.Item("TipoTablaValor")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("Estado")) = True, 0, lector.Item("Estado")))
                    obj.TipoUso = CStr(IIf(IsDBNull(lector.Item("sltt_TipoUso")) = True, "", lector.Item("sltt_TipoUso")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxIdLineaxIdSubLineaxIdTipoTabla02(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal idtipotabla As Integer, ByVal idproducto As Integer) As List(Of Entidades.SubLinea_TipoTablaValor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SubLinea_TipoTablaValorSelectIdLineaxIdSubLineaxIdTipoTablaxChk_Sub02", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        cmd.Parameters.AddWithValue("@IdTipoTabla", idtipotabla)
        cmd.Parameters.AddWithValue("@IdProducto", idproducto)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea_TipoTablaValor)
                Do While lector.Read
                    Dim obj As New Entidades.SubLinea_TipoTablaValor
                    obj.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    obj.IdSubLinea = CInt(IIf(IsDBNull(lector.Item("IdSubLinea")) = True, 0, lector.Item("IdSubLinea")))
                    obj.IdTipoTabla = CInt(IIf(IsDBNull(lector.Item("IdTipoTabla")) = True, 0, lector.Item("IdTipoTabla")))
                    obj.NomTipoTabla = CStr(IIf(IsDBNull(lector.Item("NomTipoTabla")) = True, "", lector.Item("NomTipoTabla")))
                    obj.NroOrden = CInt(IIf(IsDBNull(lector.Item("NroOrden")) = True, 0, lector.Item("NroOrden")))
                    obj.ParteNombre = CBool(IIf(IsDBNull(lector.Item("ParteNombre")) = True, 0, lector.Item("ParteNombre")))
                    obj.IdTipoTablaValor = CInt(IIf(IsDBNull(lector.Item("IdTipoTablaValor")) = True, 0, lector.Item("IdTipoTablaValor")))
                    obj.Codigo = CStr(IIf(IsDBNull(lector.Item("Codigo")) = True, "", lector.Item("Codigo")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("TipoTablaValor")) = True, "", lector.Item("TipoTablaValor")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("Estado")) = True, 0, lector.Item("Estado")))
                    obj.TipoUso = CStr(IIf(IsDBNull(lector.Item("sltt_TipoUso")) = True, "", lector.Item("sltt_TipoUso")))
                    obj.Indicador = CBool(IIf(IsDBNull(lector.Item("Indicador")) = True, 0, lector.Item("Indicador")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxIdLineaxIdSubLineaxIdTipoTabla(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal idtipotabla As Integer, ByVal idproducto As Integer) As List(Of Entidades.SubLinea_TipoTablaValor)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SubLinea_TipoTablaValorSelectIdLineaxIdSubLineaxIdTipoTablaxChk", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        cmd.Parameters.AddWithValue("@IdTipoTabla", idtipotabla)
        cmd.Parameters.AddWithValue("@IdProducto", idproducto)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea_TipoTablaValor)
                Do While lector.Read
                    Dim obj As New Entidades.SubLinea_TipoTablaValor
                    obj.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    obj.IdSubLinea = CInt(IIf(IsDBNull(lector.Item("IdSubLinea")) = True, 0, lector.Item("IdSubLinea")))
                    obj.IdTipoTabla = CInt(IIf(IsDBNull(lector.Item("IdTipoTabla")) = True, 0, lector.Item("IdTipoTabla")))
                    obj.NomTipoTabla = CStr(IIf(IsDBNull(lector.Item("NomTipoTabla")) = True, "", lector.Item("NomTipoTabla")))
                    obj.NroOrden = CInt(IIf(IsDBNull(lector.Item("NroOrden")) = True, 0, lector.Item("NroOrden")))
                    obj.ParteNombre = CBool(IIf(IsDBNull(lector.Item("ParteNombre")) = True, 0, lector.Item("ParteNombre")))
                    obj.IdTipoTablaValor = CInt(IIf(IsDBNull(lector.Item("IdTipoTablaValor")) = True, 0, lector.Item("IdTipoTablaValor")))
                    obj.Codigo = CStr(IIf(IsDBNull(lector.Item("Codigo")) = True, "", lector.Item("Codigo")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("TipoTablaValor")) = True, "", lector.Item("TipoTablaValor")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("Estado")) = True, 0, lector.Item("Estado")))
                    obj.TipoUso = CStr(IIf(IsDBNull(lector.Item("sltt_TipoUso")) = True, "", lector.Item("sltt_TipoUso")))
                    obj.Indicador = CBool(IIf(IsDBNull(lector.Item("Indicador")) = True, 0, lector.Item("Indicador")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
