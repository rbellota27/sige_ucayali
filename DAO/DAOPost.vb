﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'***************** MIERCOLES 24 MAR 2010 HORA 5_31 PM


'***************** MIERCOLES 17 FEB 2010 HORA 11_03 AM
'***************** MIERCOLES 17 FEB 2010 HORA 10_53 AM

''''''''''''''''''''''MIRAYA ANAMARIA, EDGAR 01/02/2010 9:55 am
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOPost

    Inherits DAOMantenedor

    'Protected objConexion As New Conexion
    'Protected Cn As SqlConnection '= objConexion.ConexionSIGE()
    'Protected Tr As SqlTransaction


    Public Function SelectxIdPost(ByVal IdPost As Integer) As Entidades.Post

        Dim objPost As Entidades.Post = Nothing
        Try
            Cn = objConexion.ConexionSIGE
            Dim param() As SqlParameter = New SqlParameter(0) {}
            param(0) = New SqlParameter("@IdPost", SqlDbType.Int)
            param(0).Value = IdPost

            Cn.Open()
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "_PostViewSelectxIdPost", param)
            If (reader.Read) Then
                objPost = New Entidades.Post
                With objPost

                    .Id = CInt(IIf(IsDBNull(reader("IdPost")) = True, 0, reader("IdPost")))
                    .Descripcion = CStr(IIf(IsDBNull(reader("po_Descripcion")) = True, 0, reader("po_Descripcion")))
                    .Identificador = CStr(IIf(IsDBNull(reader("po_Identificador")) = True, 0, reader("po_Identificador")))
                    .Estado = CBool(IIf(IsDBNull(reader("po_Estado")) = True, 0, reader("po_Estado")))
                    .IdBanco = CInt(IIf(IsDBNull(reader("IdBanco")) = True, 0, reader("IdBanco")))
                    .IdCuentaBancaria = CInt(IIf(IsDBNull(reader("IdCuentaBancaria")) = True, 0, reader("IdCuentaBancaria")))

                End With
            End If
            reader.Close()
            Cn.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (Cn.State = ConnectionState.Open) Then Cn.Close()
        End Try

        Return objPost

    End Function

    Public Function InsertT_AddListPos_TipoTarjeta(ByVal x As Entidades.Post, _
    ByVal L As List(Of Entidades.Pos_TipoTarjeta), _
    Optional ByRef Cnx As SqlConnection = Nothing, _
    Optional ByRef Trx As SqlTransaction = Nothing) As Integer
        Dim resul As Integer = 0
        Try
            If Cnx Is Nothing Then
                Cn = objConexion.ConexionSIGE()
                Cn.Open()
            Else
                Cn = Cnx
            End If

            If Trx Is Nothing Then
                Tr = Cn.BeginTransaction()
            Else
                Tr = Trx
            End If
            'Esta es la parte importante, lo demas es relleno
            'ini
            x.Id = InsertT_GetID(x, Cn, Tr)
            Dim DAOPos_TipoTarjeta As New DAOPos_TipoTarjeta
            DAOPos_TipoTarjeta.InsertTListxIdPos(L, x.Id, Cn, Tr)
            'fin

            If Trx Is Nothing Then
                Tr.Commit()
            End If
            resul = x.Id
        Catch e As Exception
            If Trx Is Nothing Then
                Tr.Rollback()
            End If
            resul = 0
            Throw (e)
        Finally
            If Cnx Is Nothing Then
                Cn.Close()
                Cn.Dispose()
            End If
        End Try
        Return resul
    End Function

    Public Function UpdateT_AddListPos_TipoTarjeta(ByVal x As Entidades.Post, _
ByVal L As List(Of Entidades.Pos_TipoTarjeta), _
Optional ByRef Cnx As SqlConnection = Nothing, _
Optional ByRef Trx As SqlTransaction = Nothing) As Integer
        Dim resul As Integer = 1
        Try
            If Cnx Is Nothing Then
                Cn = objConexion.ConexionSIGE()
                Cn.Open()
            Else
                Cn = Cnx
            End If

            If Trx Is Nothing Then
                Tr = Cn.BeginTransaction()
            Else
                Tr = Trx
            End If
            'Esta es la parte importante, lo demas es relleno
            'ini
            UpdateT(x, Cn, Tr)
            Dim DAOPos_TipoTarjeta As New DAOPos_TipoTarjeta
            DAOPos_TipoTarjeta.DeleteTxIdPos(x.Id, Cn, Tr)
            DAOPos_TipoTarjeta.InsertTListxIdPos(L, x.Id, Cn, Tr)
            'fin

            If Trx Is Nothing Then
                Tr.Commit()
            End If

        Catch e As Exception
            If Trx Is Nothing Then
                Tr.Rollback()
            End If
            resul = 0
            Throw (e)
        Finally
            If Cnx Is Nothing Then
                Cn.Close()
                Cn.Dispose()
            End If
        End Try
        Return resul
    End Function


    Public Function InsertT_GetID(ByVal x As Entidades.Post, _
 Optional ByRef Cnx As SqlConnection = Nothing, _
 Optional ByRef Trx As SqlTransaction = Nothing) As Integer

        Return QueryT(GetVectorParametros(x, modo_query.Insert), modo_query.Insert, "_PostInsertGetID", Cnx, Trx)
    End Function
    Public Function InsertT(ByVal x As Entidades.Post, _
    Optional ByRef Cnx As SqlConnection = Nothing, _
    Optional ByRef Trx As SqlTransaction = Nothing) As Boolean
        If InsertT_GetID(x, Cnx, Trx) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function UpdateT(ByVal x As Entidades.Post, _
    Optional ByRef Cnx As SqlConnection = Nothing, _
    Optional ByRef Trx As SqlTransaction = Nothing) As Boolean

        Return CBool(QueryT(GetVectorParametros(x, modo_query.Update), modo_query.Update, "_PostUpdate", Cnx, Trx))

    End Function

    Public Function GetVectorParametros(ByVal x As Entidades.Post, ByVal op As modo_query) As SqlParameter()

        Dim Prm() As SqlParameter

        Select Case op
            Case modo_query.Show, modo_query.Update, modo_query.Insert

                If op <> modo_query.Insert Then
                    Prm = New SqlParameter(5) {}
                    'Id de la Tabla
                    Prm(5) = getParam(x.Id, "@IdPost", SqlDbType.Int)
                Else
                    Prm = New SqlParameter(4) {}
                End If

                Prm(0) = getParam(x.Descripcion, "@po_Descripcion", SqlDbType.VarChar)
                Prm(1) = getParam(x.Identificador, "@po_Identificador", SqlDbType.VarChar)
                Prm(2) = getParam(x.Estado, "@po_Estado", SqlDbType.Bit)
                Prm(3) = getParam(x.IdBanco, "@IdBanco", SqlDbType.Int)
                Prm(4) = getParam(x.IdCuentaBancaria, "@IdCuentaBancaria", SqlDbType.Int)
             
                'Case modo_query.Delete
        End Select

        Return Prm
    End Function
End Class
