﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOMovBanco_Cheque


    Public Function MovBanco_Cheque_Transaction(ByVal objMovBanco_Cheque As Entidades.MovBanco_Cheque, ByVal sqlCN As SqlConnection, ByVal sqlTR As SqlTransaction) As Integer
        Dim IdMovBanco As Integer
        Dim Parameter() As SqlParameter = New SqlParameter(4) {}

        Parameter(0) = New SqlParameter("@IdMovBanco", SqlDbType.int)
        Parameter(0).Value = IIf(objMovBanco_Cheque.IdMovBanco = Nothing, DBNull.Value, objMovBanco_Cheque.IdMovBanco)

        Parameter(1) = New SqlParameter("@IdBanco", SqlDbType.int)
        Parameter(1).Value = IIf(objMovBanco_Cheque.IdBanco = Nothing, DBNull.Value, objMovBanco_Cheque.IdBanco)

        Parameter(2) = New SqlParameter("@IdCuentaBancaria", SqlDbType.int)
        Parameter(2).Value = IIf(objMovBanco_Cheque.IdCuentaBancaria = Nothing, DBNull.Value, objMovBanco_Cheque.IdCuentaBancaria)

        Parameter(3) = New SqlParameter("@IdCheque", SqlDbType.int)
        Parameter(3).Value = IIf(objMovBanco_Cheque.IdCheque = Nothing, DBNull.Value, objMovBanco_Cheque.IdCheque)

        Parameter(4) = New SqlParameter("@Id", SqlDbType.Int)
        Parameter(4).Direction = ParameterDirection.Output

        Dim sqlCMD As New SqlCommand("_MovBanco_Cheque_Registrar", sqlCN, sqlTR)
        sqlCMD.CommandType = CommandType.StoredProcedure
        sqlCMD.Parameters.AddRange(Parameter)
        Try
            sqlCMD.ExecuteNonQuery()
            IdMovBanco = CInt(sqlCMD.Parameters("@Id").Value)

        Catch ex As Exception
            Throw ex
        End Try
        Return IdMovBanco
    End Function


    Dim list_MovBanco_Cheque As List(Of Entidades.MovBanco_Cheque)
    Dim obj_MovBanco_Cheque As Entidades.MovBanco_Cheque

    Public Function MovBanco_Cheque_Select(ByVal Id As Integer) As List(Of Entidades.MovBanco_Cheque)
        list_MovBanco_Cheque = New List(Of Entidades.MovBanco_Cheque)
        Dim cn As SqlConnection = (New Conexion).ConexionSIGE
        Dim reader As SqlDataReader = Nothing
        Dim cmd As New SqlCommand("_MovBanco_Cheque_Select", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", IIf(Id = Nothing, DBNull.Value, Id))
        Try
            cn.Open()
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While reader.Read
                obj_MovBanco_Cheque = New Entidades.MovBanco_Cheque
                With obj_MovBanco_Cheque
                    .IdMovBanco = CInt(IIf(IsDBNull(reader("IdMovBanco")), 0, reader("IdMovBanco")))
                    .IdBanco = CInt(IIf(IsDBNull(reader("IdBanco")), 0, reader("IdBanco")))
                    .IdCuentaBancaria = CInt(IIf(IsDBNull(reader("IdCuentaBancaria")), 0, reader("IdCuentaBancaria")))
                    .IdCheque = CInt(IIf(IsDBNull(reader("IdCheque")), 0, reader("IdCheque")))
                    .DB_NAME = CStr(IIf(IsDBNull(reader("DB_NAME")), "", reader("DB_NAME")))
                End With
                list_MovBanco_Cheque.Add(obj_MovBanco_Cheque)
            Loop
            reader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Open()
        End Try
        Return list_MovBanco_Cheque
    End Function


End Class
