'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'********************  JUEVES 11 FEB 2010 HORA 10_00 AM








Imports System.Data.SqlClient
Public Class HelperDAO
    Dim objConexion As New DAO.Conexion
    Public Function Insertation(ByVal Cn As SqlConnection, ByVal Sp As String, ByVal Ap() As SqlParameter) As Boolean
        Dim cmd As New SqlCommand(Sp, Cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim i As Integer = 0
        Do While i < Ap.Length
            cmd.Parameters.AddWithValue(Ap(i).ParameterName, Ap(i).Value)
            i = i + 1
        Loop
        Using Cn
            Try
                Cn.Open()
                If (cmd.ExecuteNonQuery) = 0 Then
                    Throw New Exception("Fracas� la Operaci�n Insert")
                End If
                Cn.Close()
                Return True
            Catch ex As Exception
                Return False
            Finally

            End Try
        End Using
    End Function
    Public Function Insert(ByVal Cn As SqlConnection, ByVal Sp As String, ByVal Ap() As SqlParameter) As Boolean
        Dim cmd As New SqlCommand(Sp, Cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim i As Integer = 0
        Do While i < Ap.Length
            cmd.Parameters.AddWithValue(Ap(i).ParameterName, Ap(i).Value)
            i = i + 1
        Loop
        Using Cn
            Try
                Cn.Open()
                If (cmd.ExecuteNonQuery) = 0 Then
                    Throw New Exception("Fracas� la Operaci�n Insert")
                End If
                Cn.Close()
                Return True
            Catch ex As Exception
                Throw New Exception(ex.Message)
                Return False
            Finally

            End Try
        End Using
    End Function

    Public Sub Inserta(ByVal Cn As SqlConnection, ByVal Sp As String, ByVal Ap() As SqlParameter)
        Dim cmd As New SqlCommand(Sp, Cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim i As Integer = 0
        Do While i < Ap.Length
            If Ap(i).Value IsNot DBNull.Value Then
                cmd.Parameters.AddWithValue(Ap(i).ParameterName, Ap(i).Value)
            End If
            i = i + 1
        Loop
        Using cmd
            Try
                If (cmd.ExecuteNonQuery) = 0 Then
                    Throw New Exception("Fracas� la Operaci�n Insert")
                End If
                'Return True
            Catch ex As Exception
                Throw ex
                ' Return False
            Finally

            End Try
        End Using
    End Sub
    Public Sub InsertaT(ByVal Cn As SqlConnection, ByVal Sp As String, ByVal Ap() As SqlParameter, ByVal T As SqlTransaction)
        Dim cmd As New SqlCommand(Sp, Cn, T)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        Dim i As Integer = 0
        Do While i < Ap.Length
            If Ap(i).Value IsNot DBNull.Value Then
                cmd.Parameters.AddWithValue(Ap(i).ParameterName, Ap(i).Value)
            End If
            i = i + 1
        Loop
        Using cmd
            Try
                If (cmd.ExecuteNonQuery) = 0 Then
                    Throw New Exception("Fracas� la Operaci�n Insert")
                End If
                'Return True
            Catch ex As Exception
                Throw ex
                ' Return False
            Finally

            End Try
        End Using
    End Sub
    Public Sub UpdateT(ByVal Cn As SqlConnection, ByVal Sp As String, ByVal Ap() As SqlParameter, ByVal T As SqlTransaction)
        Dim cmd As New SqlCommand(Sp, Cn, T)
        cmd.CommandType = CommandType.StoredProcedure
        Dim i As Integer = 0
        Do While i < Ap.Length
            If Ap(i).Value IsNot DBNull.Value Then
                cmd.Parameters.AddWithValue(Ap(i).ParameterName, Ap(i).Value)
            End If
            i = i + 1
        Loop
        Using cmd
            Try
                If (cmd.ExecuteNonQuery) = 0 Then
                    Throw New Exception("Fracas� la Operaci�n Update")
                End If
            Catch ex As Exception
                Throw ex
            Finally

            End Try
        End Using
    End Sub
    Public Function Insert_ReturnParameter(ByVal Cn As SqlConnection, ByVal Sp As String, ByVal Ap() As SqlParameter, Optional ByVal tr As SqlTransaction = Nothing) As Integer
        'debo recibir la conexi�n abierta
        Dim cmd As SqlCommand
        Dim nomParameterOutPut As String = ""
        If tr IsNot Nothing Then
            cmd = New SqlCommand(Sp, Cn, tr)
        Else
            cmd = New SqlCommand(Sp, Cn)
        End If
        cmd.CommandType = CommandType.StoredProcedure
        Dim i As Integer = 0
        Do While i < Ap.Length
            cmd.Parameters.AddWithValue(Ap(i).ParameterName, Ap(i).Value)
            If Ap(i).Direction = ParameterDirection.Output Then
                cmd.Parameters(i).Direction = ParameterDirection.Output
                nomParameterOutPut = Ap(i).ParameterName
            End If
            i = i + 1
        Loop
        Try
            If (cmd.ExecuteNonQuery) = 0 Then
                Throw New Exception("Fracas� la Operaci�n Insert")
            End If
            Return CInt(cmd.Parameters(nomParameterOutPut).Value)
        Catch ex As Exception
            Return -1
        Finally

        End Try
    End Function
    Public Function InsertaTParameterOutPut(ByVal Cn As SqlConnection, ByVal Sp As String, ByVal Ap() As SqlParameter, ByVal T As SqlTransaction) As Integer
        'debo recibir la conexi�n abierta

        Dim nomParameterOutPut As String = ""
        Dim cmd As New SqlCommand(Sp, Cn, T)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        Dim i As Integer = 0
        Do While i < Ap.Length

            If Ap(i).Direction = ParameterDirection.Output Then
                cmd.Parameters.AddWithValue(Ap(i).ParameterName, 0).Direction = ParameterDirection.Output
                nomParameterOutPut = Ap(i).ParameterName
            Else
                cmd.Parameters.AddWithValue(Ap(i).ParameterName, Ap(i).Value)
            End If

            i = i + 1
        Loop
        Try
            'If (cmd.ExecuteNonQuery) = 0 Then
            '    Throw New Exception("Fracas� la Operaci�n Insert")
            'End If

            cmd.ExecuteNonQuery()
            Return CInt(cmd.Parameters(nomParameterOutPut).Value)

        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function ValidarxUnParametro(ByVal Cn As SqlConnection, ByVal Sp As String, ByVal Ap() As SqlParameter) As Integer

        Dim cmd As New SqlCommand(Sp, Cn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim i As Integer = 0
        Do While i < Ap.Length
            cmd.Parameters.AddWithValue(Ap(i).ParameterName, Ap(i).Value)
            i = i + 1
        Loop
        cmd.Parameters.Add(New SqlParameter("@retorno", SqlDbType.Int, 9, CStr(ParameterDirection.Output)))
        cmd.Parameters(3).Direction = ParameterDirection.Output

        Using Cn
            Cn.Open()
            cmd.ExecuteNonQuery()
            Cn.Close()
            Return CInt(cmd.Parameters(3).Value)
        End Using
    End Function
    Public Function UpdateTi(ByVal Cn As SqlConnection, ByVal Sp As String, ByVal Ap() As SqlParameter) As Boolean
        Using Cn
            Cn.Open()
            Dim T As SqlTransaction = Cn.BeginTransaction(Data.IsolationLevel.Serializable)
            Dim cmd As New SqlCommand(Sp, Cn, T)
            cmd.CommandType = CommandType.StoredProcedure
            Dim i As Integer = 0
            Do While i < Ap.Length
                cmd.Parameters.AddWithValue(Ap(i).ParameterName, Ap(i).Value)
                i = i + 1
            Loop
            Try
                If (cmd.ExecuteNonQuery) = 0 Then
                    Throw New Exception("Fracas� la Operaci�n Update")
                End If
                T.Commit()
                Cn.Close()
                Return True
            Catch ex As Exception
                T.Rollback()
                Return False
            Finally

            End Try
        End Using
    End Function

    Public Function Update(ByVal Cn As SqlConnection, ByVal Sp As String, ByVal Ap() As SqlParameter) As Boolean
        Using Cn
            Cn.Open()
            Dim T As SqlTransaction = Cn.BeginTransaction(Data.IsolationLevel.Serializable)
            Dim cmd As New SqlCommand(Sp, Cn, T)
            cmd.CommandType = CommandType.StoredProcedure
            Dim i As Integer = 0
            Do While i < Ap.Length
                cmd.Parameters.AddWithValue(Ap(i).ParameterName, Ap(i).Value)
                i = i + 1
            Loop
            Try
                If (cmd.ExecuteNonQuery) = 0 Then
                    Throw New Exception("Fracas� la Operaci�n Update")
                End If
                T.Commit()
                Cn.Close()
                Return True
            Catch ex As Exception
                T.Rollback()
                Throw New Exception(ex.Message)
                Return False
            Finally

            End Try
        End Using
    End Function

    Public Function DeletexParam1T(ByVal Cn As SqlConnection, ByVal Sp As String, ByVal P As SqlParameter, ByVal T As SqlTransaction) As Boolean
        Dim cmd As New SqlCommand(Sp, Cn, T)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue(P.ParameterName, P.Value)
        Using cmd
            Try
                If (cmd.ExecuteNonQuery) = 0 Then
                    Throw New Exception("Fracas� la Operaci�n Delete")
                End If
            Catch ex As Exception
                Throw ex
            Finally

            End Try
        End Using
    End Function

    Public Function DeletexParams(ByVal Cn As SqlConnection, ByVal Sp As String, ByVal Ap() As SqlParameter) As Boolean
        Dim cmd As New SqlCommand(Sp, Cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim i As Integer = 0
        'Do While i <= Ap.Length    Editado 'No puede ser i = Length, si el length es 2 el indice i nunca debe llegar a 2
        'Revisar las otras funciones de esta clase
        Do While i < Ap.Length
            cmd.Parameters.AddWithValue(Ap(i).ParameterName, Ap(i).Value)
            i = i + 1
        Loop
        Using Cn
            Try
                Cn.Open()
                If (cmd.ExecuteNonQuery) = 0 Then
                    Throw New Exception("Fracas� la Operaci�n Delete")
                End If
                Cn.Close()
                Return True
            Catch ex As Exception
                Throw New Exception(ex.Message)
                Return False

            End Try
        End Using
    End Function

    'Public Function ListarAll(ByVal Cn As SqlConnection, ByVal Sp As String) As SqlDataReader
    '    Dim cmd As New SqlCommand(Sp, Cn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    Dim lector As SqlDataReader
    '    Try
    '        Using Cn
    '            Cn.Open()
    '            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
    '            Return lector
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    'Public Function ListarxParam1(ByVal Cn As SqlConnection, ByVal Sp As String, ByVal P As SqlParameter) As SqlDataReader
    '    Dim cmd As New SqlCommand(Sp, Cn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.AddWithValue(P.ParameterName, P.Value)
    '    Dim lector As SqlDataReader
    '    Try
    '        Using Cn
    '            Cn.Open()
    '            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
    '            Return lector
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    'Public Function ListarxParams(ByVal Cn As SqlConnection, ByVal Sp As String, ByVal Ap() As SqlParameter) As SqlDataReader
    '    Dim cmd As New SqlCommand(Sp, Cn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    Dim i As Integer = 0
    '    Do While i <= Ap.Length
    '        cmd.Parameters.AddWithValue(Ap(i).ParameterName, Ap(i).Value)
    '        i = +1
    '    Loop
    '    Dim lector As SqlDataReader
    '    Try
    '        Using Cn
    '            Cn.Open()
    '            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
    '            Return lector
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
End Class
