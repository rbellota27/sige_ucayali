'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOSaldoKardex
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Sub InsertaSaldoKardex(ByVal cn As SqlConnection, ByVal saldokardex As Entidades.SaldoKardex, ByVal T As SqlTransaction)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(8) {}
        ArrayParametros(0) = New SqlParameter("@sk_CantidadSaldo", SqlDbType.Decimal)
        ArrayParametros(0).Value = IIf(saldokardex.CantidadSaldo = Nothing, DBNull.Value, saldokardex.CantidadSaldo)
        ArrayParametros(1) = New SqlParameter("@sk_CostoSaldo", SqlDbType.Decimal)
        ArrayParametros(1).Value = IIf(saldokardex.CostoSaldo = Nothing, DBNull.Value, saldokardex.CostoSaldo)
        ArrayParametros(2) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(2).Value = IIf(saldokardex.IdTienda = Nothing, DBNull.Value, saldokardex.IdTienda)
        ArrayParametros(3) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(saldokardex.IdAlmacen = Nothing, DBNull.Value, saldokardex.IdAlmacen)
        ArrayParametros(4) = New SqlParameter("@IdMovAlmacen", SqlDbType.Int)
        ArrayParametros(4).Value = IIf(saldokardex.IdMovAlmacen = Nothing, DBNull.Value, saldokardex.IdMovAlmacen)
        ArrayParametros(5) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(5).Value = IIf(saldokardex.IdEmpresa = Nothing, DBNull.Value, saldokardex.IdEmpresa)
        ArrayParametros(6) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(6).Value = IIf(saldokardex.IdProducto = Nothing, DBNull.Value, saldokardex.IdProducto)
        ArrayParametros(7) = New SqlParameter("@sk_TotalSaldo", SqlDbType.Decimal)
        ArrayParametros(7).Value = IIf(saldokardex.TotalSaldo = Nothing, DBNull.Value, saldokardex.TotalSaldo)
        ArrayParametros(8) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(8).Value = saldokardex.IdDocumento
        HDAO.InsertaT(cn, "_SaldoKardexInsert", ArrayParametros, T)
    End Sub
    Public Function ActualizaSaldoKardex(ByVal saldokardex As Entidades.SaldoKardex) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(8) {}
        ArrayParametros(0) = New SqlParameter("@sk_CantidadSaldo", SqlDbType.Decimal)
        ArrayParametros(0).Value = saldokardex.CantidadSaldo
        ArrayParametros(1) = New SqlParameter("@sk_CostoSaldo", SqlDbType.Decimal)
        ArrayParametros(1).Value = saldokardex.CostoSaldo
        ArrayParametros(2) = New SqlParameter("@IdTienda", SqlDbType.Int)
        ArrayParametros(2).Value = saldokardex.IdTienda
        ArrayParametros(3) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
        ArrayParametros(3).Value = saldokardex.IdAlmacen
        ArrayParametros(4) = New SqlParameter("@IdMovAlmacen", SqlDbType.Int)
        ArrayParametros(4).Value = saldokardex.IdMovAlmacen
        ArrayParametros(5) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        ArrayParametros(5).Value = saldokardex.IdEmpresa
        ArrayParametros(6) = New SqlParameter("@IdProducto", SqlDbType.Int)
        ArrayParametros(6).Value = saldokardex.IdProducto
        ArrayParametros(7) = New SqlParameter("@sk_TotalSaldo", SqlDbType.Decimal)
        ArrayParametros(7).Value = saldokardex.TotalSaldo
        ArrayParametros(8) = New SqlParameter("@IdSaldoKardex", SqlDbType.Int)
        ArrayParametros(8).Value = saldokardex.IdSaldoKardex
        Return HDAO.Update(cn, "_SaldoKardexUpdate", ArrayParametros)
    End Function
    Public Function SelectxIdProducto(ByVal cn As SqlConnection, ByVal IdProducto As Integer, ByVal T As SqlTransaction) As List(Of Entidades.SaldoKardex)
        Dim cmd As New SqlCommand("_SaldoKardexSelectxIdProducto", cn, T)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        Dim lector As SqlDataReader
        Try
            lector = cmd.ExecuteReader()
            Dim Lista As New List(Of Entidades.SaldoKardex)
            Do While lector.Read
                Dim obj As New Entidades.SaldoKardex
                With obj
                    .CantidadSaldo = CDec(IIf(IsDBNull(lector.Item("sk_CantidadSaldo")) = True, 0, lector.Item("sk_CantidadSaldo")))
                    .CostoSaldo = CDec(IIf(IsDBNull(lector.Item("sk_CostoSaldo")) = True, 0, lector.Item("sk_CostoSaldo")))
                    .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                    .IdMovAlmacen = CInt(IIf(IsDBNull(lector.Item("IdMovAlmacen")) = True, 0, lector.Item("IdMovAlmacen")))
                    .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                    .IdSaldoKardex = CInt(IIf(IsDBNull(lector.Item("IdSaldoKardex")) = True, 0, lector.Item("IdSaldoKardex")))
                    .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                    .TotalSaldo = CDec(IIf(IsDBNull(lector.Item("sk_TotalSaldo")) = True, 0, lector.Item("sk_TotalSaldo")))
                End With
                Lista.Add(obj)
            Loop
            lector.Close()
            Return Lista
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectxEmpresaxAlmacenxProducto(ByVal cn As SqlConnection, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdProducto As Integer, ByVal T As SqlTransaction) As Entidades.SaldoKardex
        Dim cmd As New SqlCommand("_SaldoKardexSelectxEmpresaxAlmacenxProducto", cn, T)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        Dim lector As SqlDataReader
        Try
            lector = cmd.ExecuteReader()
            Dim obj As New Entidades.SaldoKardex
            If lector.Read Then
                With obj
                    .CantidadSaldo = CDec(IIf(IsDBNull(lector.Item("sk_CantidadSaldo")) = True, 0, lector.Item("sk_CantidadSaldo")))
                    .CostoSaldo = CDec(IIf(IsDBNull(lector.Item("sk_CostoSaldo")) = True, 0, lector.Item("sk_CostoSaldo")))
                    .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                    .IdMovAlmacen = CInt(IIf(IsDBNull(lector.Item("IdMovAlmacen")) = True, 0, lector.Item("IdMovAlmacen")))
                    .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                    .IdSaldoKardex = CInt(IIf(IsDBNull(lector.Item("IdSaldoKardex")) = True, 0, lector.Item("IdSaldoKardex")))
                    .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                    .TotalSaldo = CDec(IIf(IsDBNull(lector.Item("sk_TotalSaldo")) = True, 0, lector.Item("sk_TotalSaldo")))
                End With
            Else
                obj = Nothing
            End If
            lector.Close()
            Return obj
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function DeletexIdDocumento(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal iddocumento As Integer) As Boolean
        Dim cmd As New SqlCommand("_SaldoKardexDeletexIdDocumento", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", iddocumento)
        Try
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function UpdateCantidadCostoxIdMovAlmacen(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal idmovalmacen As Integer, ByVal cantidad As Decimal, ByVal costo As Decimal) As Boolean
        Dim cmd As New SqlCommand("_SaldoKardexUpdateCantidadCostoxIdMovAlmacen", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Cantidad", cantidad)
        cmd.Parameters.AddWithValue("@Costo", costo)
        cmd.Parameters.AddWithValue("@IdMovAlmacen", idmovalmacen)
        Try
            If cmd.ExecuteNonQuery() = 0 Then
                Throw New Exception
            End If
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectUltimoSaldoKardexxIdMovAlmacen(ByVal IdMovAlmacenBase As Integer, ByVal IdProducto As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Entidades.SaldoKardex
        Dim cmd As New SqlCommand("_SaldoKardexSelectUltimoSaldoKardexxIdMovAlmacen", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProducto", IdProducto)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdAlmacen", IdAlmacen)
        cmd.Parameters.AddWithValue("@IdMovAlmacenBase", IdMovAlmacenBase)
        Dim lector As SqlDataReader
        Try
            lector = cmd.ExecuteReader()
            Dim obj As New Entidades.SaldoKardex
            If lector.Read Then
                With obj
                    .CantidadSaldo = CDec(IIf(IsDBNull(lector.Item("sk_CantidadSaldo")) = True, 0, lector.Item("sk_CantidadSaldo")))
                    .CostoSaldo = CDec(IIf(IsDBNull(lector.Item("sk_CostoSaldo")) = True, 0, lector.Item("sk_CostoSaldo")))
                    .IdAlmacen = CInt(IIf(IsDBNull(lector.Item("IdAlmacen")) = True, 0, lector.Item("IdAlmacen")))
                    .IdEmpresa = CInt(IIf(IsDBNull(lector.Item("IdEmpresa")) = True, 0, lector.Item("IdEmpresa")))
                    .IdMovAlmacen = CInt(IIf(IsDBNull(lector.Item("IdMovAlmacen")) = True, 0, lector.Item("IdMovAlmacen")))
                    .IdProducto = CInt(IIf(IsDBNull(lector.Item("IdProducto")) = True, 0, lector.Item("IdProducto")))
                    .IdSaldoKardex = CInt(IIf(IsDBNull(lector.Item("IdSaldoKardex")) = True, 0, lector.Item("IdSaldoKardex")))
                    .IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                    .TotalSaldo = CDec(IIf(IsDBNull(lector.Item("sk_TotalSaldo")) = True, 0, lector.Item("sk_TotalSaldo")))
                    .IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                End With
            End If
            lector.Close()
            Return obj
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
