'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAORegimen
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaRegimen(ByVal regimen As Entidades.Regimen) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@reg_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = regimen.Nombre
        ArrayParametros(1) = New SqlParameter("@reg_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = regimen.Abv
        ArrayParametros(2) = New SqlParameter("@reg_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = regimen.Estado
        Return HDAO.Insert(cn, "_RegimenInsert", ArrayParametros)
    End Function
    Public Function ActualizaRegimen(ByVal regimen As Entidades.Regimen) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@reg_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = regimen.Nombre
        ArrayParametros(1) = New SqlParameter("@reg_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = regimen.Abv
        ArrayParametros(2) = New SqlParameter("@reg_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = regimen.Estado
        ArrayParametros(3) = New SqlParameter("@reg_Id", SqlDbType.Int)
        ArrayParametros(3).Value = regimen.Id
        Return HDAO.Update(cn, "_RegimenUpdate", ArrayParametros)
    End Function
    Public Function SelectAll() As List(Of Entidades.Regimen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RegimenSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Regimen)
                Do While lector.Read
                    Dim obj As New Entidades.Regimen
                    obj.Id = CInt(lector.Item("reg_Id"))
                    obj.Nombre = CStr(lector.Item("reg_Nombre"))
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("reg_Abv")) = True, "", lector.Item("reg_Abv")))
                    obj.Estado = CStr(lector.Item("reg_Estado"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Regimen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RegimenSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Regimen)
                Do While lector.Read
                    Dim obj As New Entidades.Regimen
                    obj.Id = CInt(lector.Item("reg_Id"))
                    obj.Nombre = CStr(lector.Item("reg_Nombre"))
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("reg_Abv")) = True, "", lector.Item("reg_Abv")))
                    obj.Estado = CStr(lector.Item("reg_Estado"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectxIdSubLinea(ByVal idsublinea As Integer) As List(Of Entidades.Regimen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProductoRegimenSelectxIdSubLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Regimen)
                Do While lector.Read
                    Dim obj As New Entidades.Regimen
                    obj.Id = CInt(lector.Item("reg_Id"))
                    obj.Nombre = CStr(lector.Item("reg_Nombre"))
                    obj.Tasa = CDec(IIf(IsDBNull(lector.Item("pr_Tasa")) = True, 0, lector.Item("pr_Tasa")))
                    'obj.Abv = CStr(IIf(IsDBNull(lector.Item("reg_Abv")) = True, "", lector.Item("reg_Abv")))
                    'obj.Estado = CStr(lector.Item("reg_Estado"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectCbo() As List(Of Entidades.Regimen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RegimenSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Regimen)
                Do While lector.Read
                    Dim obj As New Entidades.Regimen
                    obj.Id = CInt(lector.Item("reg_Id"))
                    obj.Nombre = CStr(lector.Item("reg_Nombre"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Regimen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RegimenSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Regimen)
                Do While lector.Read
                    Dim obj As New Entidades.Regimen
                    obj.Id = CInt(lector.Item("reg_Id"))
                    obj.Nombre = CStr(lector.Item("reg_Nombre"))
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("reg_Abv")) = True, "", lector.Item("reg_Abv")))
                    obj.Estado = CStr(lector.Item("reg_Estado"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Regimen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RegimenSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Regimen)
                Do While lector.Read
                    Dim obj As New Entidades.Regimen
                    obj.Id = CInt(lector.Item("reg_Id"))
                    obj.Nombre = CStr(lector.Item("reg_Nombre"))
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("reg_Abv")) = True, "", lector.Item("reg_Abv")))
                    obj.Estado = CStr(lector.Item("reg_Estado"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Regimen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RegimenSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Regimen)
                Do While lector.Read
                    Dim obj As New Entidades.Regimen
                    obj.Id = CInt(lector.Item("reg_Id"))
                    obj.Nombre = CStr(lector.Item("reg_Nombre"))
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("reg_Abv")) = True, "", lector.Item("reg_Abv")))
                    obj.Estado = CStr(lector.Item("reg_Estado"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Regimen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RegimenSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Regimen)
                Do While lector.Read
                    Dim obj As New Entidades.Regimen
                    obj.Id = CInt(lector.Item("reg_Id"))
                    obj.Nombre = CStr(lector.Item("reg_Nombre"))
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("reg_Abv")) = True, "", lector.Item("reg_Abv")))
                    obj.Estado = CStr(lector.Item("reg_Estado"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Regimen)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_RegimenSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Regimen)
                Do While lector.Read
                    Dim obj As New Entidades.Regimen
                    obj.Id = CInt(lector.Item("reg_Id"))
                    obj.Nombre = CStr(lector.Item("reg_Nombre"))
                    obj.Abv = CStr(IIf(IsDBNull(lector.Item("reg_Abv")) = True, "", lector.Item("reg_Abv")))
                    obj.Estado = CStr(lector.Item("reg_Estado"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
