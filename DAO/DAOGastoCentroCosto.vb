﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'**************************   MARTES 11 MAYO 2010 HORA 04_05 PM


Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAOGastoCentroCosto

    Private objDaoMantenedor As New DAO.DAOMantenedor
    Private cn As SqlConnection
    Private tr As SqlTransaction
    Private reader As SqlDataReader
    Dim objConexion As New Conexion

    Public Sub Insert(ByVal objGastoCentroCosto As Entidades.GastoCentroCosto, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Try
            Dim p() As SqlParameter = New SqlParameter(2) {}
            p(0) = objDaoMantenedor.getParam(objGastoCentroCosto.IdDocumento, "@IdDocumento", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(objGastoCentroCosto.IdCentroCosto, "@IdCentroCosto", SqlDbType.VarChar)
            p(2) = objDaoMantenedor.getParam(objGastoCentroCosto.Monto, "@gcc_monto", SqlDbType.Decimal)

            SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_GastoPorCentroCosto_Insert", p)
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub
    Public Function EliminarxIdDocumento(ByVal IdDocumento As Integer) As Boolean

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim hecho As Boolean = False
        Try
            Dim cmd As New SqlCommand("_GastoporCentroCosto_DeletexIdDocumento", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            cn.Open()

            Using cmd
                cmd.ExecuteNonQuery()
                Return hecho = True
            End Using
        Catch ex As Exception
            Throw ex
            Return hecho = False
        Finally

        End Try

        Return hecho

    End Function

    Public Sub DeletexIdDocumento(ByVal IdDocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(0) {}
        p(0) = objDaoMantenedor.getParam(IdDocumento, "@IdDocumento", SqlDbType.Int)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_GastoporCentroCosto_DeletexIdDocumento", p)

    End Sub

    Public Function SelectxIdDocumento(ByVal IdDocumento As Integer) As List(Of Entidades.GastoCentroCosto)

        Dim lista As New List(Of Entidades.GastoCentroCosto)

        Try

            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = objDaoMantenedor.getParam(IdDocumento, "@IdDocumento", SqlDbType.Int)

            cn = (New DAO.Conexion).ConexionSIGE
            cn.Open()

            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_GastoPorCentroCosto_Select_IdDocumento", p)
            While (reader.Read)

                Dim obj As New Entidades.GastoCentroCosto
                With obj

                    .IdCentroCosto = objDaoMantenedor.UCStr(reader("IdCentroCosto"))
                    .UnidadNegocio = objDaoMantenedor.UCStr(reader("strCod_UniNeg"))
                    .DeptoFuncional = objDaoMantenedor.UCStr(reader("strCod_DepFunc"))
                    .SubArea1 = objDaoMantenedor.UCStr(reader("strCod_SubCodigo2"))
                    .SubArea2 = objDaoMantenedor.UCStr(reader("strCod_SubCodigo3"))
                    .SubArea3 = objDaoMantenedor.UCStr(reader("strCod_SubCodigo4"))
                    .Monto = objDaoMantenedor.UCDec(reader("gcc_monto"))
                    .Moneda = objDaoMantenedor.UCStr(reader("mon_Simbolo"))
                    .IdDocumento = objDaoMantenedor.UCInt(reader("IdDocumento"))


                End With
                lista.Add(obj)

            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return lista


    End Function

End Class
