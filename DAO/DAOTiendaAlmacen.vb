﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOTiendaAlmacen
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Sub InsertaTiendaAlmacenT(ByVal cn As SqlConnection, ByVal ltiendalmacen As List(Of Entidades.TiendaAlmacen), ByVal T As SqlTransaction)
        Dim TiendaAlmacen As Entidades.TiendaAlmacen
        For Each TiendaAlmacen In ltiendalmacen
            Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
            ArrayParametros(0) = New SqlParameter("@IdTienda", SqlDbType.Int)
            ArrayParametros(0).Value = TiendaAlmacen.IdTienda
            ArrayParametros(1) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
            ArrayParametros(1).Value = TiendaAlmacen.IdAlmacen
            ArrayParametros(2) = New SqlParameter("@tal_Principal", SqlDbType.Bit)
            ArrayParametros(2).Value = TiendaAlmacen.TalPrincipal
            HDAO.InsertaT(cn, "_TiendaAlmacenInsert", ArrayParametros, T)
        Next
    End Sub
    Public Function DeletexIdTienda(ByVal cn As SqlConnection, ByVal IdTienda As Integer, ByVal T As SqlTransaction) As Boolean
        Dim cmd As New SqlCommand("_TiendaAlmacenDeletexIdTienda", cn, T)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        Try
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Sub GrabaTiendaAlmacenT(ByVal cn As SqlConnection, ByVal ltiendalmacen As List(Of Entidades.TiendaAlmacen), ByVal T As SqlTransaction)
        Dim TiendaAlmacen As Entidades.TiendaAlmacen
        For Each TiendaAlmacen In ltiendalmacen
            Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
            ArrayParametros(0) = New SqlParameter("@IdTienda", SqlDbType.Int)
            ArrayParametros(0).Value = TiendaAlmacen.IdTienda
            ArrayParametros(1) = New SqlParameter("@IdAlmacen", SqlDbType.Int)
            ArrayParametros(1).Value = TiendaAlmacen.IdAlmacen
            ArrayParametros(2) = New SqlParameter("@tal_Principal", SqlDbType.Bit)
            ArrayParametros(2).Value = TiendaAlmacen.TalPrincipal
            ArrayParametros(3) = New SqlParameter("@Estado", SqlDbType.Bit)
            ArrayParametros(3).Value = TiendaAlmacen.Seleccionado
            HDAO.InsertaT(cn, "InsUpd_TiendaAlmacen", ArrayParametros, T)
        Next
    End Sub
End Class
