'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOViaTipo
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaViaTipo(ByVal viatipo As Entidades.ViaTipo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@vt_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = viatipo.Nombre
        ArrayParametros(1) = New SqlParameter("@vt_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = viatipo.Abv
        ArrayParametros(2) = New SqlParameter("@vt_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = viatipo.Estado
        Return HDAO.Insert(cn, "_ViaTipoInsert", ArrayParametros)
    End Function
    Public Function ActualizaViaTipo(ByVal viatipo As Entidades.ViaTipo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@vt_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = viatipo.Nombre
        ArrayParametros(1) = New SqlParameter("@vt_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = viatipo.Abv
        ArrayParametros(2) = New SqlParameter("@vt_Estado", SqlDbType.Char)
        ArrayParametros(2).Value = viatipo.Estado
        ArrayParametros(3) = New SqlParameter("@IdViaTipo", SqlDbType.Int)
        ArrayParametros(3).Value = viatipo.Id
        Return HDAO.Update(cn, "_ViaTipoUpdate", ArrayParametros)
    End Function
    Public Function SelectCbo() As List(Of Entidades.ViaTipo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ViaTipoSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ViaTipo)
                Do While lector.Read
                    Dim ViaTipo As New Entidades.ViaTipo
                    ViaTipo.Id = CInt(lector.Item("IdViaTipo"))
                    ViaTipo.Abv = CStr(IIf(IsDBNull(lector.Item("vt_Abv")) = True, "", lector.Item("vt_Abv")))
                    Lista.Add(ViaTipo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.ViaTipo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ViaTipoSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ViaTipo)
                Do While lector.Read
                    Dim ViaTipo As New Entidades.ViaTipo
                    ViaTipo.Id = CInt(lector.Item("IdViaTipo"))
                    ViaTipo.Abv = CStr(IIf(IsDBNull(lector.Item("vt_Abv")) = True, "", lector.Item("vt_Abv")))
                    ViaTipo.Estado = CStr(IIf(IsDBNull(lector.Item("vt_Estado")) = True, "", lector.Item("vt_Estado")))
                    ViaTipo.Nombre = CStr(IIf(IsDBNull(lector.Item("vt_Nombre")) = True, "", lector.Item("vt_Nombre")))
                    Lista.Add(ViaTipo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.ViaTipo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ViaTipoSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ViaTipo)
                Do While lector.Read
                    Dim ViaTipo As New Entidades.ViaTipo
                    ViaTipo.Id = CInt(lector.Item("IdViaTipo"))
                    ViaTipo.Abv = CStr(IIf(IsDBNull(lector.Item("vt_Abv")) = True, "", lector.Item("vt_Abv")))
                    ViaTipo.Estado = CStr(IIf(IsDBNull(lector.Item("vt_Estado")) = True, "", lector.Item("vt_Estado")))
                    ViaTipo.Nombre = CStr(IIf(IsDBNull(lector.Item("vt_Nombre")) = True, "", lector.Item("vt_Nombre")))
                    Lista.Add(ViaTipo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.ViaTipo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ViaTipoSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ViaTipo)
                Do While lector.Read
                    Dim ViaTipo As New Entidades.ViaTipo
                    ViaTipo.Id = CInt(lector.Item("IdViaTipo"))
                    ViaTipo.Abv = CStr(IIf(IsDBNull(lector.Item("vt_Abv")) = True, "", lector.Item("vt_Abv")))
                    ViaTipo.Estado = CStr(IIf(IsDBNull(lector.Item("vt_Estado")) = True, "", lector.Item("vt_Estado")))
                    ViaTipo.Nombre = CStr(IIf(IsDBNull(lector.Item("vt_Nombre")) = True, "", lector.Item("vt_Nombre")))
                    Lista.Add(ViaTipo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.ViaTipo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ViaTipoSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ViaTipo)
                Do While lector.Read
                    Dim ViaTipo As New Entidades.ViaTipo
                    ViaTipo.Id = CInt(lector.Item("IdViaTipo"))
                    ViaTipo.Abv = CStr(IIf(IsDBNull(lector.Item("vt_Abv")) = True, "", lector.Item("vt_Abv")))
                    ViaTipo.Estado = CStr(IIf(IsDBNull(lector.Item("vt_Estado")) = True, "", lector.Item("vt_Estado")))
                    ViaTipo.Nombre = CStr(IIf(IsDBNull(lector.Item("vt_Nombre")) = True, "", lector.Item("vt_Nombre")))
                    Lista.Add(ViaTipo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.ViaTipo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ViaTipoSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ViaTipo)
                Do While lector.Read
                    Dim ViaTipo As New Entidades.ViaTipo
                    ViaTipo.Id = CInt(lector.Item("IdViaTipo"))
                    ViaTipo.Abv = CStr(IIf(IsDBNull(lector.Item("vt_Abv")) = True, "", lector.Item("vt_Abv")))
                    ViaTipo.Estado = CStr(IIf(IsDBNull(lector.Item("vt_Estado")) = True, "", lector.Item("vt_Estado")))
                    ViaTipo.Nombre = CStr(IIf(IsDBNull(lector.Item("vt_Nombre")) = True, "", lector.Item("vt_Nombre")))
                    Lista.Add(ViaTipo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.ViaTipo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ViaTipoSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ViaTipo)
                Do While lector.Read
                    Dim ViaTipo As New Entidades.ViaTipo
                    ViaTipo.Id = CInt(lector.Item("IdViaTipo"))
                    ViaTipo.Abv = CStr(IIf(IsDBNull(lector.Item("vt_Abv")) = True, "", lector.Item("vt_Abv")))
                    ViaTipo.Estado = CStr(IIf(IsDBNull(lector.Item("vt_Estado")) = True, "", lector.Item("vt_Estado")))
                    ViaTipo.Nombre = CStr(IIf(IsDBNull(lector.Item("vt_Nombre")) = True, "", lector.Item("vt_Nombre")))
                    Lista.Add(ViaTipo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.ViaTipo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ViaTipoSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ViaTipo)
                Do While lector.Read
                    Dim ViaTipo As New Entidades.ViaTipo
                    ViaTipo.Id = CInt(lector.Item("IdViaTipo"))
                    ViaTipo.Abv = CStr(IIf(IsDBNull(lector.Item("vt_Abv")) = True, "", lector.Item("vt_Abv")))
                    ViaTipo.Estado = CStr(IIf(IsDBNull(lector.Item("vt_Estado")) = True, "", lector.Item("vt_Estado")))
                    ViaTipo.Nombre = CStr(IIf(IsDBNull(lector.Item("vt_Nombre")) = True, "", lector.Item("vt_Nombre")))
                    Lista.Add(ViaTipo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class

