﻿Imports System.Data.SqlTypes
Imports System.Data.SqlClient
Imports System.Configuration
Public Class DAODespachos
    Dim con As New Conexion
    Public Function DAO_FiltrarGuias(ByVal filtro As String, ByVal flag As String, ByVal condicion As String, _
                                             ByVal tienda As Integer, ByVal serie As Integer, ByVal documento As Integer, ByVal fechaEmision As Date, _
                                             ByVal check As Boolean, ByVal cn As SqlConnection) As List(Of Entidades.be_guiaRemision)
        Dim lista As List(Of Entidades.be_guiaRemision) = Nothing
        Try
            Using cmd As New SqlCommand("SP_CRONOGRAMA_DESPACHOS", cn)
                With cmd
                    .CommandType = CommandType.StoredProcedure

                    .Parameters.AddWithValue("@ACCION", 0)
                    .Parameters.AddWithValue("@FLAG", flag)
                    .Parameters.AddWithValue("@CONDICION", condicion)
                    .Parameters.AddWithValue("@SERIE", serie)
                    .Parameters.AddWithValue("@DOCUMENTO", documento)
                    .Parameters.AddWithValue("@TIENDA", tienda)
                    .Parameters.AddWithValue("@FILTRO", filtro)
                    .Parameters.AddWithValue("@FECHAEMISION", fechaEmision)
                    .Parameters.AddWithValue("@CHECKFILTRO", check)
                End With
                Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
                If rdr IsNot Nothing Then
                    lista = New List(Of Entidades.be_guiaRemision)
                    Dim mt_Nombre As Integer = rdr.GetOrdinal("mt_Nombre")
                    Dim IdTienda As Integer = rdr.GetOrdinal("IdTienda")
                    Dim IdSerie As Integer = rdr.GetOrdinal("IdSerie")
                    Dim doc_Codigo As Integer = rdr.GetOrdinal("doc_Codigo")
                    Dim IdDocumento As Integer = rdr.GetOrdinal("IdDocumento")
                    Dim numero As Integer = rdr.GetOrdinal("numero")
                    Dim peso As Integer = rdr.GetOrdinal("peso")
                    Dim cliente As Integer = rdr.GetOrdinal("cliente")
                    Dim distrito As Integer = rdr.GetOrdinal("distrito")
                    Dim tie_Nombre As Integer = rdr.GetOrdinal("tie_Nombre")

                    Dim objetoGuia As Entidades.be_guiaRemision

                    While rdr.Read()
                        objetoGuia = New Entidades.be_guiaRemision
                        With objetoGuia
                            .mt_Nombre = rdr.GetString(mt_Nombre)
                            .idTienda = rdr.GetInt32(IdTienda)
                            .idSerie = rdr.GetInt32(IdSerie)
                            .doc_Codigo = rdr.GetString(doc_Codigo)
                            .idDocumento = rdr.GetInt32(IdDocumento)
                            .numero = rdr.GetString(numero)
                            .peso = rdr.GetDecimal(peso)
                            .cliente = rdr.GetString(cliente)
                            .distrito = rdr.GetString(distrito)
                            .tie_nombre = rdr.GetString(tie_Nombre)
                            lista.Add(objetoGuia)
                        End With
                    End While
                    rdr.Close()
                End If
                Return lista
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Function

    Public Function DAO_eventoLoad(ByVal cn As SqlConnection) As List(Of Object)
        Dim listaObjetos As List(Of Object) = Nothing
        Dim listaAyudantes As List(Of Entidades.Persona)
        Dim listaChofer As List(Of Entidades.Persona)
        Dim listaPlaca As List(Of Entidades.Vehiculo)

        Using cmd As New SqlCommand("SP_CRONOGRAMA_EVENTO_LOAD", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure
            End With
            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.Default)
            If rdr.HasRows Then
                listaObjetos = New List(Of Object)
                listaAyudantes = New List(Of Entidades.Persona)
                listaChofer = New List(Of Entidades.Persona)
                listaPlaca = New List(Of Entidades.Vehiculo)

                Dim objetoAyudantes As Entidades.Persona
                Dim objetoChofer As Entidades.Persona
                Dim objetoPlaca As Entidades.Vehiculo

                Dim idAyudante As Integer = rdr.GetOrdinal("IdPersona")
                Dim ayudante As Integer = rdr.GetOrdinal("Nombre")
                While rdr.Read()
                    objetoAyudantes = New Entidades.Persona
                    With objetoAyudantes
                        .Id = rdr.GetInt32(idAyudante)
                        .NComercial = rdr.GetString(ayudante)
                        listaAyudantes.Add(objetoAyudantes)
                    End With
                End While
                rdr.NextResult()

                Dim idChofer As Integer = rdr.GetOrdinal("IdPersona")
                Dim chofer As Integer = rdr.GetOrdinal("Nombre")
                While rdr.Read()
                    objetoChofer = New Entidades.Persona
                    With objetoChofer
                        .Id = rdr.GetInt32(idAyudante)
                        .NComercial = rdr.GetString(ayudante)
                        listaChofer.Add(objetoChofer)
                    End With
                End While
                rdr.NextResult()

                Dim placa As Integer = rdr.GetOrdinal("veh_Placa")
                While rdr.Read()
                    objetoPlaca = New Entidades.Vehiculo
                    With objetoPlaca
                        .Placa = rdr.GetString(placa)
                        listaPlaca.Add(objetoPlaca)
                    End With
                End While
                rdr.Close()
                listaObjetos.Add(listaAyudantes)
                listaObjetos.Add(listaChofer)
                listaObjetos.Add(listaPlaca)
            End If
            Return listaObjetos
        End Using
    End Function

    Public Function DAO_ListarDatosGenerales(ByVal filtro As String, ByVal flag As String, ByVal condicion As String, _
                                             ByVal tienda As Integer, ByVal serie As Integer, ByVal documento As Integer, ByVal fechaEmision As Date, _
                                             ByVal check As Boolean, ByVal cn As SqlConnection) As List(Of Entidades.be_cronograma)
        Dim lista As List(Of Entidades.be_cronograma) = Nothing
        Try
            Using cmd As New SqlCommand("SP_CRONOGRAMA_DESPACHOS", cn)
                With cmd
                    .CommandType = CommandType.StoredProcedure

                    .Parameters.AddWithValue("@ACCION", 0)
                    .Parameters.AddWithValue("@FLAG", flag)
                    .Parameters.AddWithValue("@CONDICION", condicion)
                    .Parameters.AddWithValue("@SERIE", serie)
                    .Parameters.AddWithValue("@DOCUMENTO", documento)
                    .Parameters.AddWithValue("@TIENDA", tienda)
                    .Parameters.AddWithValue("@FILTRO", filtro)
                    .Parameters.AddWithValue("@FECHAEMISION", fechaEmision)
                    .Parameters.AddWithValue("@CHECKFILTRO", check)
                End With
                Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
                If rdr IsNot Nothing Then
                    lista = New List(Of Entidades.be_cronograma)
                    Dim agencia As Integer = rdr.GetOrdinal("agencia")
                    Dim IdVehiculo As Integer = rdr.GetOrdinal("IdVehiculo")
                    Dim idCronogramaDespacho As Integer = rdr.GetOrdinal("idCronogramaDespacho")
                    Dim fecInicioDespacho As Integer = rdr.GetOrdinal("fecInicioDespacho")
                    Dim fecFinDespacho As Integer = rdr.GetOrdinal("fecFinDespacho")
                    Dim Id_estadodespacho As Integer = rdr.GetOrdinal("Id_estadodespacho")
                    Dim idDocumentoRelacion As Integer = rdr.GetOrdinal("idDocumentoRelacion")
                    Dim IdConductor As Integer = rdr.GetOrdinal("IdConductor")
                    Dim IdAyudante As Integer = rdr.GetOrdinal("IdAyudante")
                    Dim IdControl As Integer = rdr.GetOrdinal("IdControl")
                    Dim turno As Integer = rdr.GetOrdinal("turno")
                    Dim idAgencia As Integer = rdr.GetOrdinal("idAgencia")
                    Dim veh_Placa As Integer = rdr.GetOrdinal("veh_Placa")
                    Dim veh_Modelo As Integer = rdr.GetOrdinal("veh_Modelo")
                    Dim capmaxima As Integer = rdr.GetOrdinal("capmaxima")
                    Dim capacidadAlcanzada As Integer = rdr.GetOrdinal("capacidadAlcanzada")
                    Dim cantDocAsociado As Integer = rdr.GetOrdinal("cantDocAsociado")
                    Dim Conductor As Integer = rdr.GetOrdinal("Conductor")
                    Dim Ayudante As Integer = rdr.GetOrdinal("Ayudante")
                    Dim Control As Integer = rdr.GetOrdinal("Control")
                    Dim nroVuelta As Integer = rdr.GetOrdinal("nroVuelta")
                    Dim objetoCronograma As Entidades.be_cronograma

                    While rdr.Read()
                        objetoCronograma = New Entidades.be_cronograma
                        With objetoCronograma
                            .nomAgencia = rdr.GetString(agencia)
                            .idVehiculo = rdr.GetInt32(IdVehiculo)
                            .idCronograma = rdr.GetInt32(idCronogramaDespacho)
                            .fecInicio = rdr.GetDateTime(fecInicioDespacho)
                            .fecFin = rdr.GetString(fecFinDespacho)
                            .idEstadoDespacho = rdr.GetInt32(Id_estadodespacho)
                            .idDocumentoRelacion = rdr.GetString(idDocumentoRelacion)
                            .idConductor = rdr.GetInt32(IdConductor)
                            .idAyudante = rdr.GetInt32(IdAyudante)
                            .idControl = rdr.GetInt32(IdControl)
                            .turno = rdr.GetString(turno)
                            .idAgencia = rdr.GetInt32(idAgencia)
                            .vehplaca = rdr.GetString(veh_Placa)
                            .vehModelo = rdr.GetString(veh_Modelo)
                            .capacidadMaxima = rdr.GetDecimal(capmaxima)
                            .capacidadAlcanzada = rdr.GetDecimal(capacidadAlcanzada)
                            .cantDocAsociado = rdr.GetInt32(cantDocAsociado)
                            .conductor = rdr.GetString(Conductor)
                            .ayudante = rdr.GetString(Ayudante)
                            .control = rdr.GetString(Control)
                            .nroVuelta = rdr.GetInt32(nroVuelta)
                        End With
                        lista.Add(objetoCronograma)
                    End While
                    rdr.Close()
                End If
                Return lista
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Function

    Public Function ListarVehiculos(ByVal identificador As Integer, ByVal filtro As String) As DataTable
        Dim dt As New DataTable
        Dim cs As New SqlConnection
        cs = con.ConexionSIGE()
        Dim cmd As New SqlCommand("DESPACHOS")
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = cs

        With cmd
            .Parameters.AddWithValue("@identificador", identificador)
            .Parameters.AddWithValue("@filtro", filtro)
        End With

        Dim da As New SqlDataAdapter(cmd)
        da.Fill(dt)
        Return dt
    End Function

    Public Function DAO_ListarPersonasVehiculo() As DataTable
        Dim dt As New DataTable
        Using con As New SqlConnection(ConfigurationManager.ConnectionStrings("Conexion").ConnectionString)
            Dim da As New SqlDataAdapter("SP_VEHICULO_PERSONA", con)
            da.SelectCommand.CommandType = CommandType.StoredProcedure
            da.Fill(dt)
        End Using
        Return dt
    End Function

    Public Function DAO_UPDATE_FEC_DESPACHOS(ByVal idDocumento As String, ByVal fecha As DateTime)
        Using cd As New SqlConnection(con.cadenaConexion)
            Dim cmd As New SqlCommand("SP_UPDATE_DOC_DESPACHOS", cd)
            cmd.CommandType = CommandType.StoredProcedure

            With cmd
                .Parameters.Add("@IDDOCUMENTO", idDocumento)
                .Parameters.Add("@FECHAFIN", fecha)
            End With
            con.AbrirConexion(cd)
            cmd.ExecuteNonQuery()
            con.CerraConexion(cd)
        End Using
    End Function

    Public Function DAO_UPDATE_PESO_DESPACHOS(ByVal idDocumento As String, ByVal peso As Decimal)
        Using cd As New SqlConnection(con.cadenaConexion)
            Dim cmd As New SqlCommand("SP_CRONOGRAMA_PESO", cd)
            cmd.CommandType = CommandType.StoredProcedure

            With cmd
                .Parameters.Add("@ID_DOCUMENTO", idDocumento)
                .Parameters.Add("@PESO", peso)
            End With
            con.AbrirConexion(cd)
            cmd.ExecuteNonQuery()
            con.CerraConexion(cd)
        End Using
    End Function

    Public Function DAO_DataSetTipados(ByVal dt As DataTable, ByVal flag As String, ByVal condicion As String, ByVal tienda As Integer, ByVal serie As Integer, _
                                       ByVal documento As Integer, ByVal fechaEmision As Date, ByVal check As Boolean)
        Using cs As New SqlConnection(con.cadenaConexion)
            Dim cmd As New SqlCommand("SP_CRONOGRAMA_DESPACHOS", cs)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd
                .Parameters.Add(New SqlParameter("@ACCION", DbType.Int32)).Value = 0
                .Parameters.AddWithValue("@FLAG", flag)
                .Parameters.AddWithValue("@CONDICION", condicion)
                .Parameters.AddWithValue("@SERIE", serie)
                .Parameters.AddWithValue("@DOCUMENTO", documento)
                .Parameters.AddWithValue("@TIENDA", tienda)
                .Parameters.AddWithValue("@FILTRO", "")
                .Parameters.AddWithValue("@FECHAEMISION", fechaEmision)
                .Parameters.AddWithValue("@CHECKFILTRO", check)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
            End With
            Return dt
        End Using
    End Function

    Public Function DAO_ListarDatosGenerales(ByVal filtro As String, ByVal dt As DataTable, ByVal flag As String, ByVal condicion As String, _
                                             ByVal tienda As Integer, ByVal serie As Integer, ByVal documento As Integer, ByVal fechaEmision As Date, ByVal check As Boolean)
        Dim cs As New SqlConnection
        Try

            cs = con.ConexionSIGE()
            Dim cmd As New SqlCommand("SP_CRONOGRAMA_DESPACHOS", cs)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd
                .Parameters.AddWithValue("@ACCION", 0)
                .Parameters.AddWithValue("@FLAG", flag)
                .Parameters.AddWithValue("@CONDICION", condicion)
                .Parameters.AddWithValue("@SERIE", serie)
                .Parameters.AddWithValue("@DOCUMENTO", documento)
                .Parameters.AddWithValue("@TIENDA", tienda)
                .Parameters.AddWithValue("@FILTRO", filtro)
                .Parameters.AddWithValue("@FECHAEMISION", fechaEmision)
                .Parameters.AddWithValue("@CHECKFILTRO", check)
            End With
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            Return dt
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.CerraConexion(cs)
        End Try

    End Function

    Public Function DAO_UpdateProgramacion(ByVal idCronograma As String, ByVal id_anulacion As Integer, ByVal comentario As String)
        Dim cs As New SqlConnection
        Try
            cs = con.ConexionSIGE()
            Dim cmd As New SqlCommand("SP_CRONOGRAMA_ANULAR", cs)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd
                .Parameters.AddWithValue("@IDCRONOGRAMA", idCronograma)
                .Parameters.AddWithValue("@ID_TBL_ANULACION", id_anulacion)
                .Parameters.AddWithValue("@comentario_anulacion", comentario)
            End With
            con.AbrirConexion(cs)
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.CerraConexion(cs)
        End Try
    End Function

    Public Function DAO_InsertarGuias(ByVal peso As Decimal, ByVal fechaInicioDespacho As DateTime, ByVal capacidadAlcanzada As Decimal, ByVal cantidadDoc As Integer, ByVal idEstadoDespacho As Integer, _
                                      ByVal idDocumentoRelacion As String, ByVal entidades As Entidades.Vehiculo, ByVal flag As String, ByVal idCronogramaDespacho As Integer, _
                                      ByVal idDocumentoDeselecion As String, ByVal nroVuelta As Integer)
        Dim cs As New SqlConnection
        cs = con.ConexionSIGE()
        Dim cmd As New SqlCommand("SP_CRONOGRAMA_DESPACHOS_INSERT_V2", cs)
        cmd.CommandType = CommandType.StoredProcedure
        With cmd
            .Parameters.AddWithValue("@PESO", peso)
            .Parameters.AddWithValue("@FECHAINICIODESPACHO", fechaInicioDespacho)
            .Parameters.AddWithValue("@IDCRONOGRAMADESPACHO", idCronogramaDespacho)
            .Parameters.AddWithValue("@FLAG", flag)
            .Parameters.AddWithValue("@TURNO", entidades.Turno)
            .Parameters.AddWithValue("@IDVEHICULO", IIf(entidades.IdVehiculo = 0, 0, entidades.IdVehiculo))
            .Parameters.AddWithValue("@IDCONDUCTOR", entidades.IdConductor)
            .Parameters.AddWithValue("@IDAYUDANTE", entidades.IdAyudante)
            .Parameters.AddWithValue("@IDCONTROL", entidades.IdControl)
            .Parameters.AddWithValue("@CAPACIDADALCANZADA", capacidadAlcanzada)
            .Parameters.AddWithValue("@CANTIDADDOCUMENTOS", cantidadDoc)
            .Parameters.AddWithValue("@IDDOCUMENTORELACION", idDocumentoRelacion)
            .Parameters.AddWithValue("@ID_ESTADO_DESPACHO", idEstadoDespacho)
            .Parameters.AddWithValue("@IDDOCUMENTO_DESELECCION", idDocumentoDeselecion)
            .Parameters.AddWithValue("@IDAGENCIA", IIf(entidades.idPersona = 0, DBNull.Value, entidades.idPersona))
            .Parameters.AddWithValue("@CAPACIDADTOTAL", entidades.veh_Capacidad)
            .Parameters.AddWithValue("@nroVuelta", nroVuelta)
        End With
        cs.Open()
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        cs.Close()
    End Function

    Public Function DAO_Reporte(ByVal fecInicio As Date, ByVal fecFin As Date, ByVal turno As String, ByVal placa As String, idtienda As String)
        Dim dt As New DataTable
        Try
            Dim cs As New SqlConnection
            cs = con.ConexionSIGE()
            Dim cmd As New SqlCommand("SP_CRONOGRAMA_REPORTE", cs)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd
                .Parameters.AddWithValue("@FEC_INICIO", fecInicio)
                .Parameters.AddWithValue("@FEC_FIN", fecFin)
                .Parameters.AddWithValue("@TURNO", turno)
                .Parameters.AddWithValue("@PLACA", placa)
                .Parameters.AddWithValue("@idTienda", idtienda)
            End With
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            Return dt
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function

    Public Function DAO_Reporte_Distribucion(ByVal idDocumento As Integer)
        Dim dt As New DataTable
        Try
            Dim cs As New SqlConnection
            cs = con.ConexionSIGE()
            Dim cmd As New SqlCommand("SP_DISTRIBUCION_REPORTE", cs)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd
                .Parameters.AddWithValue("@IDDOCUMENTO", idDocumento)
            End With
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            Return dt
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function

    Public Function DAO_Controladores(ByVal idDocumentos As String, ByVal flag As String, ByVal idusuario As String, ByVal flagFiltro As Boolean, ByVal fechaFiltro As Date, _
                                      ByVal Trasnsportista As Integer)
        Dim dt As New DataTable
        Try
            Dim cs As New SqlConnection
            cs = con.ConexionSIGE()
            Dim cmd As New SqlCommand("SP_CRONOGRAMA_CONTROLADORES", cs)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd
                .Parameters.AddWithValue("@iddocumento", IIf(String.IsNullOrEmpty(idDocumentos), DBNull.Value, idDocumentos))
                .Parameters.AddWithValue("@flag", flag)
                .Parameters.AddWithValue("@SECTOR", idusuario)
                .Parameters.AddWithValue("@FLAG_FITLRO_TRANSPORTE", flagFiltro)
                .Parameters.AddWithValue("@FECHA_FILTRO", fechaFiltro)
                .Parameters.AddWithValue("@TRANSPORTISTA", Trasnsportista)
            End With
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            Return dt
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function

    Public Function DAO_ListaPicking(ByVal paramidSector As Integer, ByVal flagTransporte As Boolean, ByVal fecFiltro As Date, _
                                     ByVal idTransportista As Integer, ByVal cn As SqlConnection, ByVal turno As String) As List(Of Entidades.be_picking)
        Dim lista As List(Of Entidades.be_picking) = Nothing
        Dim cs As New SqlConnection
        cs = con.ConexionSIGE
        Using cmd As New SqlCommand("SP_PICKING_V2", cs)
            With cmd
                .CommandType = CommandType.StoredProcedure
                .Parameters.AddWithValue("@SECTOR", paramidSector)
                .Parameters.AddWithValue("@FLAG_FITLRO_TRANSPORTE", flagTransporte)
                .Parameters.AddWithValue("@FECHA_FILTRO", fecFiltro)
                .Parameters.AddWithValue("@TRANSPORTISTA", idTransportista)
                .Parameters.AddWithValue("@TURNO", turno)
            End With

            Dim rdr As SqlDataReader
            '
            'Abro la conexión
            '
            cs.Open()
            rdr = cmd.ExecuteReader(CommandBehavior.SingleResult)
            '
            'Capturo la posición de las columnas
            '
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_picking)
                Dim idDocumento As Integer = rdr.GetOrdinal("IdDocumento")
                Dim cliente As Integer = rdr.GetOrdinal("cliente")
                Dim tie_nombre As Integer = rdr.GetOrdinal("tie_nombre")
                Dim idDetalleDocumento As Integer = rdr.GetOrdinal("idDetalleDocumento")
                Dim nroDocumento As Integer = rdr.GetOrdinal("numero")
                Dim idProducto As Integer = rdr.GetOrdinal("idProducto")
                Dim fecInicioDespacho As Integer = rdr.GetOrdinal("fecInicioDespacho")
                Dim veh_Placa As Integer = rdr.GetOrdinal("veh_Placa")
                Dim id_despacho_controladores As Integer = rdr.GetOrdinal("id_despacho_controladores")
                Dim prod_codigo As Integer = rdr.GetOrdinal("prod_codigo")
                Dim prod_Nombre As Integer = rdr.GetOrdinal("prod_Nombre")
                Dim lin_Nombre As Integer = rdr.GetOrdinal("lin_Nombre")
                Dim sl_Nombre As Integer = rdr.GetOrdinal("sl_Nombre")
                Dim unidadMedidaPrincipal As Integer = rdr.GetOrdinal("unidadMedidaPrincipal")
                Dim dc_Cantidad As Integer = rdr.GetOrdinal("dc_Cantidad")
                Dim unidadMedidaSalida As Integer = rdr.GetOrdinal("unidadMedidaSalida")
                Dim nom_sector As Integer = rdr.GetOrdinal("nom_sector")
                Dim idSector As Integer = rdr.GetOrdinal("idSector")
                Dim nombreTono As Integer = rdr.GetOrdinal("tono")
                Dim cantidadTono As Integer = rdr.GetOrdinal("cantidadTono")
                Dim nroVuelta As Integer = rdr.GetOrdinal("nroVuelta")

                Dim objetoPicking As New Entidades.be_picking
                While rdr.Read()
                    objetoPicking = New Entidades.be_picking()
                    With objetoPicking
                        .idDocumento = rdr.GetInt32(idDocumento)
                        .cliente = rdr.GetString(cliente)
                        .tie_nombre = rdr.GetString(tie_nombre)
                        .idDetalleDocumento = rdr.GetInt32(idDetalleDocumento)
                        .nroDocumento = rdr.GetString(nroDocumento)
                        .idProducto = rdr.GetInt32(idProducto)
                        .fecInicioDespacho = rdr.GetDateTime(fecInicioDespacho)
                        .veh_placa = rdr.GetString(veh_Placa)
                        .id_despacho_controladores = rdr.GetInt32(id_despacho_controladores)
                        .prod_codigo = rdr.GetString(prod_codigo)
                        .prod_Nombre = rdr.GetString(prod_Nombre)
                        .lin_Nombre = rdr.GetString(lin_Nombre)
                        .sl_Nombre = rdr.GetString(sl_Nombre)
                        .unidadMedidaPrincipal = rdr.GetInt32(unidadMedidaPrincipal)
                        .dc_Cantidad = rdr.GetString(dc_Cantidad)
                        .unidadMedidaSalida = rdr.GetInt32(unidadMedidaSalida)
                        .nom_sector = rdr.GetString(nom_sector)
                        .idSector = rdr.GetInt32(idSector)
                        .nombreTono = rdr.GetString(nombreTono)
                        .cantidadTono = rdr.GetString(cantidadTono)
                        .nroVuelta = rdr.GetInt32(nroVuelta)
                    End With
                    lista.Add(objetoPicking)
                End While
                rdr.Close()
            End If
            Return lista
        End Using

    End Function

    Public Function DAO_Controladores_UPDATE(ByVal id_despacho_controlador As Integer, ByVal idAlmacen As Integer, ByVal idProducto As Integer, ByVal idSector As Integer, _
                                             ByVal cantidadDespachada As Decimal, ByVal idUnidadEntrada As Integer, ByVal idUnidadSalida As Integer, ByVal idTono As Integer)
        Dim consultaExitosa As Boolean = False
        Dim cs As New SqlConnection
        cs = con.ConexionSIGE()
        Try

            Dim cmd As New SqlCommand("SP_CRONOGRAMA_CONTROLADORES_UPDATE", cs)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd
                .Parameters.AddWithValue("@IDDESPACHO_CONTROLADOR", id_despacho_controlador)
                .Parameters.AddWithValue("@IDALMACEN", idAlmacen)
                .Parameters.AddWithValue("@IDPRODUCTO", idProducto)
                .Parameters.AddWithValue("@IDSECTOR", idSector)
                .Parameters.AddWithValue("@IDTONO", idTono)
                .Parameters.AddWithValue("@CANTIDAD_DESPACHADA", cantidadDespachada)
                .Parameters.AddWithValue("@IDUNIDADMEDIDA_INICIO", idUnidadEntrada)
                .Parameters.AddWithValue("@IDUNIDAD_MEDIDA_SALIDA", idUnidadSalida)
            End With
            cs.Open()
            consultaExitosa = cmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            cs.Close()
        End Try
        Return consultaExitosa
    End Function

    Public Sub DAO_BuscarDocumentoGeneral(ByVal cn As SqlConnection, ByVal nroSerie As Integer, ByVal nroDocumento As Integer, idTienda As String)

        Using cmd As New SqlCommand("SP_CRONOGRAMA_BUSCAR_DOC", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("@nroSerie", SqlDbType.Int)).Value = nroSerie
                .Parameters.Add(New SqlParameter("@nroDocumento", SqlDbType.Int)).Value = nroDocumento
                .Parameters.Add(New SqlParameter("@idTienda", SqlDbType.VarChar)).Value = idTienda
            End With
            cmd.ExecuteNonQuery()
        End Using
    End Sub
End Class
