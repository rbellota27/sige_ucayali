﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Imports System.Data
Imports ADODB
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOUtil
    Private objConexion As New Conexion
    Private objDaoMantenedor As New DAO.DAOMantenedor
    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE

    Public Function fn_ProductoMedida(ByVal IdProducto As Integer, ByVal IdUnidadMedida As Integer, ByVal IdMagnitud As Integer, ByVal IdUmMagnitudDestino As Integer, ByVal Valor As Decimal) As Decimal
        Dim valorRetorno As Decimal = 0
        Try

            Dim commandText As String = " select MagnitudValor from dbo.fn_ProductoMedida(" + CStr(IdProducto) + "," + CStr(IdUnidadMedida) + "," + CStr(IdMagnitud) + "," + CStr(IdUmMagnitudDestino) + "," + CStr(Valor) + ") "
            cn.Open()

            valorRetorno = CDec(SqlHelper.ExecuteScalar(cn, CommandType.Text, commandText))

        Catch ex As Exception

        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

        Return valorRetorno

    End Function

    Public Function valDocComprometerStock(ByVal IdDocumento As Integer) As Integer


        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim Registros As Integer = 0

        Try

            cn.Open()
            Registros = CInt(SqlHelper.ExecuteScalar(cn, CommandType.Text, " select dbo.valDocComprometerStock(" + CStr(IdDocumento) + ") "))


        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return Registros

    End Function
    Public Function fx_StockDisponible(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdProducto As Integer) As Decimal
        Dim valorRetorno As Decimal = 0

        Try
         
            Dim commandText As String = " select dbo.fx_StockDisponible(" + CStr(IdEmpresa) + "," + CStr(IdAlmacen) + "," + CStr(IdProducto) + ") "
            cn.Open()

            valorRetorno = CDec(SqlHelper.ExecuteScalar(cn, CommandType.Text, commandText))


        Catch ex As Exception

        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

        Return valorRetorno

    End Function
    Public Function fx_StockComprometidoxIdEmpresaxIdAlmacenxIdProducto(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdProducto As Integer) As Decimal
        Dim valorRetorno As Decimal = 0

        Try
            
            Dim commandText As String = " select [dbo].[fx_StockComprometidoxIdEmpresaxIdAlmacenxIdProducto](" + CStr(IdEmpresa) + "," + CStr(IdAlmacen) + "," + CStr(IdProducto) + ") "
            cn.Open()

            valorRetorno = CDec(SqlHelper.ExecuteScalar(cn, CommandType.Text, commandText))

        Catch ex As Exception

        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

        Return valorRetorno

    End Function
    Public Function Fn_getStockTransito(ByVal IdAlmacen As Integer, ByVal IdProducto As Integer) As Decimal
        Dim valorRetorno As Decimal = 0

        Try


            Dim commandText As String = " select [dbo].[Fn_getStockTransito](" + CStr(IdAlmacen) + "," + CStr(IdProducto) + ") "
            cn.Open()

            valorRetorno = CDec(SqlHelper.ExecuteScalar(cn, CommandType.Text, commandText))


        Catch ex As Exception

        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

        Return valorRetorno

    End Function

    Public Function fx_getCantidadxTono(ByVal idProducto As Integer, ByVal idTono As Integer) As Decimal
        Dim valorRetorno As Decimal = 0
        Try

            Dim p() As SqlParameter = New SqlParameter(1) {}
            p(0) = objDaoMantenedor.getParam(idProducto, "@IdProducto", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(idTono, "@IdUM_Origen", SqlDbType.Int)

            Dim sql As String = "   select dbo._fx_cantidadxTonoxProducto(" + CStr(idProducto) + "," + CStr(idTono) + " ) "

            cn.Open()

            valorRetorno = CDec(SqlHelper.ExecuteScalar(cn, CommandType.Text, sql))

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return valorRetorno
    End Function

    Public Function fx_getValorEquivalenteProducto(ByVal IdProducto As Integer, ByVal IdUM_Origen As Integer, ByVal IdUM_Destino As Integer, ByVal Cantidad As Decimal) As Decimal

        Dim valorRetorno As Decimal = 0


        Try

            Dim p() As SqlParameter = New SqlParameter(3) {}
            p(0) = objDaoMantenedor.getParam(IdProducto, "@IdProducto", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdUM_Origen, "@IdUM_Origen", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdUM_Destino, "@IdUM_Destino", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(Cantidad, "@Cantidad", SqlDbType.Decimal)


            Dim sql As String = "   select dbo.fx_getValorEquivalenteProducto(" + CStr(IdProducto) + "," + CStr(IdUM_Origen) + "," + CStr(IdUM_Destino) + "," + CStr(Cantidad) + " ) "

            cn.Open()

            valorRetorno = CDec(SqlHelper.ExecuteScalar(cn, CommandType.Text, sql))

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return valorRetorno

    End Function


    Public Function fx_getMagnitudProducto(ByVal IdProducto As Integer, ByVal IdUMProducto As Integer, ByVal Cantidad As Decimal, ByVal IdMagnitud As Integer, ByVal IdUMMagnitud_Destino As Integer) As Decimal

        Dim valorRetorno As Decimal = 0

        Try

            Dim p() As SqlParameter = New SqlParameter(4) {}
            p(0) = objDaoMantenedor.getParam(IdProducto, "@IdProducto", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdUMProducto, "@IdUMProducto", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(Cantidad, "@Cantidad", SqlDbType.Decimal)
            p(3) = objDaoMantenedor.getParam(IdMagnitud, "@IdMagnitud", SqlDbType.Int)
            p(4) = objDaoMantenedor.getParam(IdUMMagnitud_Destino, "@IdUMMagnitud_Destino", SqlDbType.Int)

            Dim sql As String = "   select dbo.fx_getMagnitudProducto(" + CStr(IdProducto) + "," + CStr(IdUMProducto) + "," + CStr(Cantidad) + "," + CStr(IdMagnitud) + "," + CStr(IdUMMagnitud_Destino) + " ) "

            cn.Open()

            valorRetorno = CDec(SqlHelper.ExecuteScalar(cn, CommandType.Text, sql))

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return valorRetorno

    End Function

    Public Function fx_getCostoProducto(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdProducto As Integer, ByVal fechaFin As Date) As Decimal


        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim costoProducto As Decimal = 0

        Try

            cn.Open()

            costoProducto = CDec(SqlHelper.ExecuteScalar(cn, CommandType.Text, " select dbo.fx_getCostoProducto(" + CStr(IdEmpresa) + "," + CStr(IdAlmacen) + "," + CStr(IdProducto) + ",'" + Format(fechaFin, "dd/MM/yyyy") + "')  "))

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return costoProducto

    End Function
    Public Function fx_getParamVerInterfaceMaestroObra(ByVal idparam As Integer) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim valueParam As Integer = 0
        Try
            cn.Open()
            valueParam = CInt(SqlHelper.ExecuteScalar(cn, CommandType.Text, " select dbo._fx_getVerSeccionMaestroObra (" + CStr(idparam) + ")"))
            cn.Close()

        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return valueParam
    End Function
    Public Function fx_getValorParametroGeneralxIdparam(ByVal idparam As String) As String
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim valueParam As String = ""
        Try
            cn.Open()
            valueParam = CStr(SqlHelper.ExecuteScalar(cn, CommandType.Text, " select dbo.getValorParametroGeneralxCadIdParams (" + "'" + CStr(idparam) + "'" + ")"))
            cn.Close()

        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return valueParam
    End Function

    Public Function fx_StockRealxIdEmpresaxIdAlmacenxIdProductoxFechaFin(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdProducto As Integer, ByVal fechaFin As Date) As Decimal


        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim stockFisico As Decimal = 0

        Try

            cn.Open()

            stockFisico = CDec(SqlHelper.ExecuteScalar(cn, CommandType.Text, " select dbo.fx_StockRealxIdEmpresaxIdAlmacenxIdProductoxFechaFin(" + CStr(IdEmpresa) + "," + CStr(IdAlmacen) + "," + CStr(IdProducto) + ",'" + Format(fechaFin, "dd/MM/yyyy") + "')  "))


        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return stockFisico

    End Function

    Public Function fx_StockReal(ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdProducto As Integer) As Decimal


        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim stockFisico As Decimal = 0

        Try

            cn.Open()

            stockFisico = CDec(SqlHelper.ExecuteScalar(cn, CommandType.Text, " select dbo.fx_StockReal(" + CStr(IdEmpresa) + "," + CStr(IdAlmacen) + "," + CStr(IdProducto) + ") "))


        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return stockFisico

    End Function



    Public Function fx_getCantidadMovAlmacenxIdDetalleDocumento(ByVal IdDetalleDocumento As Integer) As Decimal


        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cantidad As Decimal = 0

        Try

            cn.Open()

            cantidad = CDec(SqlHelper.ExecuteScalar(cn, CommandType.Text, "  select dbo.fx_getCantidadMovAlmacenxIdDetalleDocumento(" + CStr(IdDetalleDocumento) + ") "))


        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return cantidad

    End Function


    Public Function CalcularValorxTipoCambio(ByVal valor As Decimal, ByVal IdMonedaOrigen As Integer, ByVal IdMonedaDestino As Integer, ByVal codigo As String, ByVal fecha As Date) As Decimal

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim valorEq As Decimal = 0
        Try

            cn.Open()
            Dim cmd As New SqlCommand("   select dbo._fx_CalcularValorxTipoCambio(" + CStr(valor) + "," + CStr(IdMonedaOrigen) + "," + CStr(IdMonedaDestino) + ",'" + codigo + "','" + Format(fecha, "dd/MM/yyyy") + "')  ", cn)
            cmd.CommandType = CommandType.Text

            valorEq = CDec(cmd.ExecuteScalar)

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return valorEq
    End Function



    Public Function SelectValorxIdMonedaOxIdMonedaD(ByVal IdMonedaOrigen As Integer, ByVal IdMonedaDestino As Integer, ByVal ValorAConvertir As Decimal, ByVal cn As SqlConnection, ByVal CodigoManejo As String) As Decimal
        Try
            Dim cmd As New SqlCommand("_selectValorxIdMonedaOxIdMonedaD", cn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@IdMonedaOrigen", IdMonedaOrigen)
            cmd.Parameters.AddWithValue("@IdMonedaDestino", IdMonedaDestino)
            cmd.Parameters.AddWithValue("@Valor", ValorAConvertir)
            cmd.Parameters.AddWithValue("@Codigo", CodigoManejo)

            Dim param As New SqlParameter("@Retorno", SqlDbType.Decimal)
            param.Direction = ParameterDirection.Output
            param.Scale = 6
            param.Precision = 18

            cmd.Parameters.Add(param)

            cmd.ExecuteScalar()

            Return CDec(cmd.Parameters("@Retorno").Value)
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function ValidarExistenciaxTablax2CamposForUpdate(ByVal nomTabla As String, ByVal nomCampo As String, ByVal valor As String, ByVal idCampo As String, ByVal idValor As Integer) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("[_ValidarExistenciaxTablax2CampoforUpdate]", cn)
        'Dim cont As Integer = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@NOMBRETABLA", nomTabla)

        cmd.Parameters.AddWithValue("@NOMBRECAMPO", nomCampo)
        cmd.Parameters.AddWithValue("@VALORCAMPO", valor)

        cmd.Parameters.AddWithValue("@idCAMPO", idCampo)
        cmd.Parameters.AddWithValue("@idVALORCAMPO", idValor)

        cmd.Parameters.Add(New SqlParameter("@RETORNO", SqlDbType.Int))
        cmd.Parameters("@RETORNO").Direction = ParameterDirection.Output
        'cmd.Parameters("@RETORNO").Direction = ParameterDirection.Output
        'Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                cmd.ExecuteNonQuery()
                Return CInt(cmd.Parameters("@RETORNO").Value)
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function ValidarExistenciaxTablax1Campo(ByVal nomTabla As String, ByVal nomCampo As String, ByVal valor As String) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ValidarExistenciaxTablax1Campo", cn)
        'Dim cont As Integer = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@NOMBRECAMPO", nomCampo)
        cmd.Parameters.AddWithValue("@NOMBRETABLA", nomTabla)
        cmd.Parameters.AddWithValue("@VALORCAMPO", valor)
        cmd.Parameters.Add(New SqlParameter("@RETORNO", SqlDbType.Int))
        cmd.Parameters("@RETORNO").Direction = ParameterDirection.Output
        'cmd.Parameters("@RETORNO").Direction = ParameterDirection.Output
        'Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                cmd.ExecuteNonQuery()
                Return CInt(cmd.Parameters("@RETORNO").Value)
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function ValidarExistenciaxTablax4Campos(ByVal nomTabla As String, ByVal nomCampo1 As String, ByVal valor1 As String, ByVal nomCampo2 As String, ByVal valor2 As String, ByVal nomCampo3 As String, ByVal valor3 As String, ByVal nomCampo4 As String, ByVal valor4 As String) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ValidarxCuatroParametros", cn)

        'Dim cont As Integer = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@NOMBRECAMPO1", nomCampo1)
        cmd.Parameters.AddWithValue("@VALORCAMPO1", valor1)
        cmd.Parameters.AddWithValue("@NOMBRECAMPO2", nomCampo2)
        cmd.Parameters.AddWithValue("@VALORCAMPO2", valor2)
        cmd.Parameters.AddWithValue("@NOMBRECAMPO3", nomCampo3)
        cmd.Parameters.AddWithValue("@VALORCAMPO3", valor3)
        cmd.Parameters.AddWithValue("@NOMBRECAMPO4", nomCampo4)
        cmd.Parameters.AddWithValue("@VALORCAMPO4", valor4)
        cmd.Parameters.AddWithValue("@NOMBRETABLA", nomTabla)
        cmd.Parameters.Add(New SqlParameter("@RETORNO", SqlDbType.Int))
        cmd.Parameters("@RETORNO").Direction = ParameterDirection.Output
        Try
            Using cn
                cn.Open()
                cmd.ExecuteNonQuery()
                Return CInt(cmd.Parameters("@RETORNO").Value)
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function ValidarExistenciaxTablax4Campos(ByVal nomTabla As String, ByVal nomCampo1 As String, ByVal valor1 As String, ByVal nomCampo2 As String, ByVal valor2 As String, ByVal nomCampo3 As String, ByVal valor3 As String, ByVal nomCampo4 As String, ByVal valor4 As String, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer
        Dim cmd As New SqlCommand("_ValidarxCuatroParametros", cn, tr)
        'Dim cont As Integer = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@NOMBRECAMPO1", nomCampo1)
        cmd.Parameters.AddWithValue("@VALORCAMPO1", valor1)
        cmd.Parameters.AddWithValue("@NOMBRECAMPO2", nomCampo2)
        cmd.Parameters.AddWithValue("@VALORCAMPO2", valor2)
        cmd.Parameters.AddWithValue("@NOMBRECAMPO3", nomCampo3)
        cmd.Parameters.AddWithValue("@VALORCAMPO3", valor3)
        cmd.Parameters.AddWithValue("@NOMBRECAMPO4", nomCampo4)
        cmd.Parameters.AddWithValue("@VALORCAMPO4", valor4)
        cmd.Parameters.AddWithValue("@NOMBRETABLA", nomTabla)
        cmd.Parameters.Add(New SqlParameter("@RETORNO", SqlDbType.Int))
        cmd.Parameters("@RETORNO").Direction = ParameterDirection.Output
        Try
            cmd.ExecuteNonQuery()
            Return CInt(cmd.Parameters("@RETORNO").Value)
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function ValidarExistenciaxTablax3Campos(ByVal nomTabla As String, ByVal nomCampo1 As String, ByVal valor1 As String, ByVal nomCampo2 As String, ByVal valor2 As String, ByVal nomCampo3 As String, ByVal valor3 As String) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ValidarxTresParametros", cn)
        'Dim cont As Integer = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@NOMBRECAMPO1", nomCampo1)
        cmd.Parameters.AddWithValue("@VALORCAMPO1", valor1)
        cmd.Parameters.AddWithValue("@NOMBRECAMPO2", nomCampo2)
        cmd.Parameters.AddWithValue("@VALORCAMPO2", valor2)
        cmd.Parameters.AddWithValue("@NOMBRECAMPO3", nomCampo3)
        cmd.Parameters.AddWithValue("@VALORCAMPO3", valor3)
        cmd.Parameters.AddWithValue("@NOMBRETABLA", nomTabla)
        cmd.Parameters.Add(New SqlParameter("@RETORNO", SqlDbType.Int))
        cmd.Parameters("@RETORNO").Direction = ParameterDirection.Output
        Try
            Using cn
                cn.Open()
                cmd.ExecuteNonQuery()
                Return CInt(cmd.Parameters("@RETORNO").Value)
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function ValidarExistenciaxTablax3Campos(ByVal nomTabla As String, ByVal nomCampo1 As String, ByVal valor1 As String, ByVal nomCampo2 As String, ByVal valor2 As String, ByVal nomCampo3 As String, ByVal valor3 As String, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer
        Dim cmd As New SqlCommand("_ValidarxTresParametros", cn, tr)
        'Dim cont As Integer = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@NOMBRECAMPO1", nomCampo1)
        cmd.Parameters.AddWithValue("@VALORCAMPO1", valor1)
        cmd.Parameters.AddWithValue("@NOMBRECAMPO2", nomCampo2)
        cmd.Parameters.AddWithValue("@VALORCAMPO2", valor2)
        cmd.Parameters.AddWithValue("@NOMBRECAMPO3", nomCampo3)
        cmd.Parameters.AddWithValue("@VALORCAMPO3", valor3)
        cmd.Parameters.AddWithValue("@NOMBRETABLA", nomTabla)
        cmd.Parameters.Add(New SqlParameter("@RETORNO", SqlDbType.Int))
        cmd.Parameters("@RETORNO").Direction = ParameterDirection.Output
        Try
            cmd.ExecuteNonQuery()
            Return CInt(cmd.Parameters("@RETORNO").Value)
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function ValidarxDosParametros(ByVal nomTabla As String, ByVal nomCampo1 As String, ByVal valor1 As String, ByVal nomCampo2 As String, ByVal valor2 As String) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ValidarxDosParametros", cn)
        'Dim cont As Integer = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@NOMBRECAMPO1", nomCampo1)
        cmd.Parameters.AddWithValue("@VALORCAMPO1", valor1)
        cmd.Parameters.AddWithValue("@NOMBRECAMPO2", nomCampo2)
        cmd.Parameters.AddWithValue("@VALORCAMPO2", valor2)
        cmd.Parameters.AddWithValue("@NOMBRETABLA", nomTabla)
        cmd.Parameters.Add(New SqlParameter("@RETORNO", SqlDbType.Int))
        cmd.Parameters("@RETORNO").Direction = ParameterDirection.Output
        Try
            Using cn
                cn.Open()
                cmd.ExecuteNonQuery()
                Return CInt(cmd.Parameters("@RETORNO").Value)
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function ValidarxDosParametros(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal nomTabla As String, ByVal nomCampo1 As String, ByVal valor1 As String, ByVal nomCampo2 As String, ByVal valor2 As String) As Integer
        Dim cmd As New SqlCommand("_ValidarxDosParametros", cn, tr)
        'Dim cont As Integer = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@NOMBRECAMPO1", nomCampo1)
        cmd.Parameters.AddWithValue("@VALORCAMPO1", valor1)
        cmd.Parameters.AddWithValue("@NOMBRECAMPO2", nomCampo2)
        cmd.Parameters.AddWithValue("@VALORCAMPO2", valor2)
        cmd.Parameters.AddWithValue("@NOMBRETABLA", nomTabla)
        cmd.Parameters.Add(New SqlParameter("@RETORNO", SqlDbType.Int))
        cmd.Parameters("@RETORNO").Direction = ParameterDirection.Output
        Try
            cmd.ExecuteNonQuery()
            Return CInt(cmd.Parameters("@RETORNO").Value)
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function ValidarxDosParametrosEnteros(ByVal nomTabla As String, ByVal nomCampo1 As String, ByVal valor1 As Integer, ByVal nomCampo2 As String, ByVal valor2 As Integer) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ValidarxDosParametrosEnteros", cn)
        'Dim cont As Integer = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@NOMBRECAMPO1", nomCampo1)
        cmd.Parameters.AddWithValue("@VALORCAMPO1", valor1)
        cmd.Parameters.AddWithValue("@NOMBRECAMPO2", nomCampo2)
        cmd.Parameters.AddWithValue("@VALORCAMPO2", valor2)
        cmd.Parameters.AddWithValue("@NOMBRETABLA", nomTabla)
        cmd.Parameters.Add(New SqlParameter("@RETORNO", SqlDbType.Int))
        cmd.Parameters("@RETORNO").Direction = ParameterDirection.Output
        Try
            Using cn
                cn.Open()
                cmd.ExecuteNonQuery()
                Return CInt(cmd.Parameters("@RETORNO").Value)
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function ValidarExistenciaxTablax1Campo(ByVal cn As SqlConnection, ByVal TR As SqlTransaction, ByVal nomTabla As String, ByVal nomCampo As String, ByVal valor As String) As Integer
        Dim cmd As New SqlCommand("_ValidarExistenciaxTablax1Campo", cn, TR)
        'Dim cont As Integer = 0
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@NOMBRECAMPO", nomCampo)
        cmd.Parameters.AddWithValue("@NOMBRETABLA", nomTabla)
        cmd.Parameters.AddWithValue("@VALORCAMPO", valor)
        cmd.Parameters.Add(New SqlParameter("@RETORNO", SqlDbType.Int))
        cmd.Parameters("@RETORNO").Direction = ParameterDirection.Output
        Try
            cmd.ExecuteNonQuery()
            Return CInt(cmd.Parameters("@RETORNO").Value)
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function ValidarTiendaxUsuario(cn As SqlConnection, idUsuario As Integer) As String
        Dim lista As List(Of Integer) = Nothing
        Dim cadenaIds As String = ""
        Using cmd As New SqlCommand("SP_SELECT_TIENDA_X_USUARIO", cn)
            With cmd
                .CommandType = CommandType.StoredProcedure

                .Parameters.Add(New SqlParameter("IdUsuario", DbType.Int32)).Value = idUsuario
            End With
            Dim idTienda As Integer = 0
            Try
                cadenaIds = cmd.ExecuteScalar
            Catch ex As Exception
                Throw ex
            End Try
            Return cadenaIds
        End Using

    End Function


End Class
