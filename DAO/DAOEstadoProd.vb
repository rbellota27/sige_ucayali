'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOEstadoProd
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaEstadoProd(ByVal estadoprod As Entidades.EstadoProd) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@ep_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = estadoprod.Descripcion
        ArrayParametros(1) = New SqlParameter("@ep_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = estadoprod.Estado
        Return HDAO.Insert(cn, "_EstadoProdInsert", ArrayParametros)
    End Function
    Public Function ActualizaEstadoProd(ByVal estadoprod As Entidades.EstadoProd) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@ep_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = estadoprod.Descripcion
        ArrayParametros(1) = New SqlParameter("@ep_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = estadoprod.Estado
        ArrayParametros(2) = New SqlParameter("@IdEstadoProd", SqlDbType.Int)
        ArrayParametros(2).Value = estadoprod.Id
        Return HDAO.Update(cn, "_EstadoProdUpdate", ArrayParametros)
    End Function
    Public Function SelectAll() As List(Of Entidades.EstadoProd)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstadoProdSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.EstadoProd)
                Do While lector.Read
                    Dim objEP As New Entidades.EstadoProd
                    objEP.Descripcion = CStr(lector.Item("ep_Nombre"))
                    objEP.Estado = CStr(lector.Item("ep_Estado"))
                    objEP.Id = CInt(lector.Item("IdEstadoProd"))
                    Lista.Add(objEP)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.EstadoProd)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstadoProdSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.EstadoProd)
                Do While lector.Read
                    Dim objEP As New Entidades.EstadoProd
                    objEP.Descripcion = CStr(lector.Item("ep_Nombre"))
                    objEP.Estado = CStr(lector.Item("ep_Estado"))
                    objEP.Id = CInt(lector.Item("IdEstadoProd"))
                    Lista.Add(objEP)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.EstadoProd)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstadoProdSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.EstadoProd)
                Do While lector.Read
                    Dim objEP As New Entidades.EstadoProd
                    objEP.Descripcion = CStr(lector.Item("ep_Nombre"))
                    objEP.Estado = CStr(lector.Item("ep_Estado"))
                    objEP.Id = CInt(lector.Item("IdEstadoProd"))
                    Lista.Add(objEP)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.EstadoProd)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstadoProdSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.EstadoProd)
                Do While lector.Read
                    Dim objEP As New Entidades.EstadoProd
                    objEP.Descripcion = CStr(lector.Item("ep_Nombre"))
                    objEP.Estado = CStr(lector.Item("ep_Estado"))
                    objEP.Id = CInt(lector.Item("IdEstadoProd"))
                    Lista.Add(objEP)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.EstadoProd)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstadoProdSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.EstadoProd)
                Do While lector.Read
                    Dim objEP As New Entidades.EstadoProd
                    objEP.Descripcion = CStr(lector.Item("ep_Nombre"))
                    objEP.Estado = CStr(lector.Item("ep_Estado"))
                    objEP.Id = CInt(lector.Item("IdEstadoProd"))
                    Lista.Add(objEP)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.EstadoProd)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstadoProdSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.EstadoProd)
                Do While lector.Read
                    Dim objEP As New Entidades.EstadoProd
                    objEP.Descripcion = CStr(lector.Item("ep_Nombre"))
                    objEP.Estado = CStr(lector.Item("ep_Estado"))
                    objEP.Id = CInt(lector.Item("IdEstadoProd"))
                    Lista.Add(objEP)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.EstadoProd)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstadoProdSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.EstadoProd)
                Do While lector.Read
                    Dim objEP As New Entidades.EstadoProd
                    objEP.Descripcion = CStr(lector.Item("ep_Nombre"))
                    objEP.Estado = CStr(lector.Item("ep_Estado"))
                    objEP.Id = CInt(lector.Item("IdEstadoProd"))
                    Lista.Add(objEP)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
