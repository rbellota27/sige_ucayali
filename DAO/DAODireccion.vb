'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAODireccion
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Sub InsertaDireccion(ByVal cn As SqlConnection, ByVal Ldir As List(Of Entidades.Direccion), ByVal T As SqlTransaction)
        'Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim Direccion As New Entidades.Direccion
        For Each Direccion In Ldir
            Dim ArrayParametros() As SqlParameter = New SqlParameter(13) {}
            ArrayParametros(0) = New SqlParameter("@IdPersona", SqlDbType.Int)
            ArrayParametros(0).Value = Direccion.IdPersona
            ArrayParametros(1) = New SqlParameter("@IdZonaTipo", SqlDbType.Int)
            ArrayParametros(1).Value = IIf(Direccion.IdZonaTipo = Nothing, DBNull.Value, Direccion.IdZonaTipo)
            ArrayParametros(2) = New SqlParameter("@dir_ZonaNombre", SqlDbType.VarChar)
            ArrayParametros(2).Value = IIf(Direccion.ZonaNombre = "", DBNull.Value, Direccion.ZonaNombre)
            ArrayParametros(3) = New SqlParameter("@IdViaTipo", SqlDbType.Int)
            ArrayParametros(3).Value = IIf(Direccion.IdViaTipo = Nothing, DBNull.Value, Direccion.IdViaTipo)
            ArrayParametros(4) = New SqlParameter("@dir_ViaNumero", SqlDbType.Char)
            ArrayParametros(4).Value = IIf(Direccion.ViaNumero = "", DBNull.Value, Direccion.ViaNumero)
            ArrayParametros(5) = New SqlParameter("@dir_Etapa", SqlDbType.Char)
            ArrayParametros(5).Value = IIf(Direccion.Etapa = "", DBNull.Value, Direccion.Etapa)
            ArrayParametros(6) = New SqlParameter("@dir_Mz", SqlDbType.Char)
            ArrayParametros(6).Value = IIf(Direccion.Mz = "", DBNull.Value, Direccion.Mz)
            ArrayParametros(7) = New SqlParameter("@dir_Lt", SqlDbType.Char)
            ArrayParametros(7).Value = IIf(Direccion.Lt = "", DBNull.Value, Direccion.Lt)
            ArrayParametros(8) = New SqlParameter("@dir_Interior", SqlDbType.Char)
            ArrayParametros(8).Value = IIf(Direccion.Interior = "", DBNull.Value, Direccion.Interior)
            ArrayParametros(9) = New SqlParameter("@dir_Direccion", SqlDbType.VarChar)
            ArrayParametros(9).Value = Direccion.Direccion
            ArrayParametros(10) = New SqlParameter("@dir_Referencia", SqlDbType.VarChar)
            ArrayParametros(10).Value = IIf(Direccion.Referencia = "", DBNull.Value, Direccion.Referencia)
            ArrayParametros(11) = New SqlParameter("@IdTipoDireccion", SqlDbType.Int)
            ArrayParametros(11).Value = Direccion.IdTipoDireccion
            ArrayParametros(12) = New SqlParameter("@dir_Ubigeo", SqlDbType.Char)
            ArrayParametros(12).Value = IIf(Direccion.Ubigeo = "", DBNull.Value, Direccion.Ubigeo)
            ArrayParametros(13) = New SqlParameter("@dir_viaNombre", SqlDbType.NVarChar)
            ArrayParametros(13).Value = IIf(Direccion.ViaNombre = "", DBNull.Value, Direccion.ViaNombre)

            HDAO.InsertaT(cn, "_DireccionInsert", ArrayParametros, T)
        Next
    End Sub
    Public Sub ActualizaDireccionT(ByVal cn As SqlConnection, ByVal Ldir As List(Of Entidades.Direccion), ByVal T As SqlTransaction)
        Dim Direccion As New Entidades.Direccion
        For Each Direccion In Ldir
            Dim ArrayParametros() As SqlParameter = New SqlParameter(14) {}
            ArrayParametros(0) = New SqlParameter("@IdPersona", SqlDbType.Int)
            ArrayParametros(0).Value = IIf(Direccion.IdPersona = Nothing, DBNull.Value, Direccion.IdPersona)
            ArrayParametros(1) = New SqlParameter("@IdDireccion", SqlDbType.Int)
            ArrayParametros(1).Value = IIf(Direccion.IdDireccion = Nothing, DBNull.Value, Direccion.IdDireccion)
            ArrayParametros(2) = New SqlParameter("@IdZonaTipo", SqlDbType.Int)
            ArrayParametros(2).Value = IIf(Direccion.IdZonaTipo = Nothing, DBNull.Value, Direccion.IdZonaTipo)
            ArrayParametros(3) = New SqlParameter("@dir_ZonaNombre", SqlDbType.VarChar)
            ArrayParametros(3).Value = IIf(Direccion.ZonaNombre = Nothing, DBNull.Value, Direccion.ZonaNombre)
            ArrayParametros(4) = New SqlParameter("@IdViaTipo", SqlDbType.Int)
            ArrayParametros(4).Value = IIf(Direccion.IdViaTipo = Nothing, DBNull.Value, Direccion.IdViaTipo)
            ArrayParametros(5) = New SqlParameter("@dir_ViaNombre", SqlDbType.VarChar)
            ArrayParametros(5).Value = IIf(Direccion.ViaNombre = Nothing, DBNull.Value, Direccion.ViaNombre)
            ArrayParametros(6) = New SqlParameter("@dir_ViaNumero", SqlDbType.Char)
            ArrayParametros(6).Value = IIf(Direccion.ViaNumero = Nothing, DBNull.Value, Direccion.ViaNumero)
            ArrayParametros(7) = New SqlParameter("@dir_Etapa", SqlDbType.Char)
            ArrayParametros(7).Value = IIf(Direccion.Etapa = Nothing, DBNull.Value, Direccion.Etapa)
            ArrayParametros(8) = New SqlParameter("@dir_Mz", SqlDbType.Char)
            ArrayParametros(8).Value = IIf(Direccion.Mz = Nothing, DBNull.Value, Direccion.Mz)
            ArrayParametros(9) = New SqlParameter("@dir_Lt", SqlDbType.Char)
            ArrayParametros(9).Value = IIf(Direccion.Lt = Nothing, DBNull.Value, Direccion.Lt)
            ArrayParametros(10) = New SqlParameter("@dir_Interior", SqlDbType.Char)
            ArrayParametros(10).Value = IIf(Direccion.Interior = Nothing, DBNull.Value, Direccion.Interior)
            ArrayParametros(11) = New SqlParameter("@dir_Direccion", SqlDbType.VarChar)
            ArrayParametros(11).Value = IIf(Direccion.Direccion = Nothing, DBNull.Value, Direccion.Direccion)
            ArrayParametros(12) = New SqlParameter("@dir_Referencia", SqlDbType.VarChar)
            ArrayParametros(12).Value = IIf(Direccion.Referencia = Nothing, DBNull.Value, Direccion.Referencia)
            ArrayParametros(13) = New SqlParameter("@IdTipoDireccion", SqlDbType.Int)
            ArrayParametros(13).Value = IIf(Direccion.IdTipoDireccion = Nothing, DBNull.Value, Direccion.IdTipoDireccion)
            ArrayParametros(14) = New SqlParameter("@dir_Ubigeo", SqlDbType.Char)
            ArrayParametros(14).Value = IIf(Direccion.Ubigeo = Nothing, DBNull.Value, Direccion.Ubigeo)

            HDAO.UpdateT(cn, "_DireccionUpdate", ArrayParametros, T)
        Next

    End Sub
    Public Function SelectxIdPersona(ByVal IdPersona As Integer) As Entidades.Direccion
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DireccionSelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Using cn
            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
            Dim objDireccion As New Entidades.Direccion
            If lector.Read Then
                objDireccion = New Entidades.Direccion
                objDireccion.IdDireccion = CInt(IIf(IsDBNull(lector.Item("IdDireccion")) = True, Nothing, lector.Item("IdDireccion")))
                objDireccion.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, Nothing, lector.Item("dir_Direccion")))
                objDireccion.Etapa = CStr(IIf(IsDBNull(lector.Item("dir_Etapa")) = True, Nothing, lector.Item("dir_Etapa")))
                objDireccion.IdTipoDireccion = CInt(IIf(IsDBNull(lector.Item("IdTipoDireccion")) = True, Nothing, lector.Item("IdTipoDireccion")))
                objDireccion.IdViaTipo = CInt(IIf(IsDBNull(lector.Item("IdViaTipo")) = True, Nothing, lector.Item("IdViaTipo")))
                objDireccion.IdZonaTipo = CInt(IIf(IsDBNull(lector.Item("IdZonaTipo")) = True, Nothing, lector.Item("IdZonaTipo")))
                objDireccion.Interior = CStr(IIf(IsDBNull(lector.Item("dir_Interior")) = True, Nothing, lector.Item("dir_Interior")))
                objDireccion.Lt = CStr(IIf(IsDBNull(lector.Item("dir_Lt")) = True, Nothing, lector.Item("dir_Lt")))
                objDireccion.Mz = CStr(IIf(IsDBNull(lector.Item("dir_Mz")) = True, Nothing, lector.Item("dir_Mz")))
                objDireccion.Referencia = CStr(IIf(IsDBNull(lector.Item("dir_Referencia")) = True, Nothing, lector.Item("dir_Referencia")))
                objDireccion.Ubigeo = CStr(IIf(IsDBNull(lector.Item("dir_Ubigeo")) = True, Nothing, lector.Item("dir_Ubigeo")))
                objDireccion.ViaNombre = CStr(IIf(IsDBNull(lector.Item("dir_viaNombre")) = True, Nothing, lector.Item("dir_viaNombre")))
                objDireccion.ViaNumero = CStr(IIf(IsDBNull(lector.Item("dir_ViaNumero")) = True, Nothing, lector.Item("dir_ViaNumero")))
                objDireccion.ZonaNombre = CStr(IIf(IsDBNull(lector.Item("dir_ZonaNombre")) = True, Nothing, lector.Item("dir_ZonaNombre")))
            End If
            lector.Close()
            Return objDireccion
        End Using
    End Function

    Public Function Selectx_IdPersona(ByVal IdPersona As Integer) As List(Of Entidades.Direccion)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_DireccionSelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Direccion)
                Do While lector.Read
                    Dim objDireccion As New Entidades.Direccion
                    objDireccion = New Entidades.Direccion
                    objDireccion.IdDireccion = CInt(IIf(IsDBNull(lector.Item("IdDireccion")) = True, Nothing, lector.Item("IdDireccion")))
                    objDireccion.Direccion = CStr(IIf(IsDBNull(lector.Item("dir_Direccion")) = True, Nothing, lector.Item("dir_Direccion")))
                    objDireccion.Etapa = CStr(IIf(IsDBNull(lector.Item("dir_Etapa")) = True, Nothing, lector.Item("dir_Etapa")))
                    objDireccion.IdTipoDireccion = CInt(IIf(IsDBNull(lector.Item("IdTipoDireccion")) = True, Nothing, lector.Item("IdTipoDireccion")))
                    objDireccion.IdViaTipo = CInt(IIf(IsDBNull(lector.Item("IdViaTipo")) = True, Nothing, lector.Item("IdViaTipo")))
                    objDireccion.IdZonaTipo = CInt(IIf(IsDBNull(lector.Item("IdZonaTipo")) = True, Nothing, lector.Item("IdZonaTipo")))
                    objDireccion.Interior = CStr(IIf(IsDBNull(lector.Item("dir_Interior")) = True, Nothing, lector.Item("dir_Interior")))
                    objDireccion.Lt = CStr(IIf(IsDBNull(lector.Item("dir_Lt")) = True, Nothing, lector.Item("dir_Lt")))
                    objDireccion.Mz = CStr(IIf(IsDBNull(lector.Item("dir_Mz")) = True, Nothing, lector.Item("dir_Mz")))
                    objDireccion.Referencia = CStr(IIf(IsDBNull(lector.Item("dir_Referencia")) = True, Nothing, lector.Item("dir_Referencia")))
                    objDireccion.Ubigeo = CStr(IIf(IsDBNull(lector.Item("dir_Ubigeo")) = True, Nothing, lector.Item("dir_Ubigeo")))
                    objDireccion.ViaNombre = CStr(IIf(IsDBNull(lector.Item("dir_viaNombre")) = True, Nothing, lector.Item("dir_viaNombre")))
                    objDireccion.ViaNumero = CStr(IIf(IsDBNull(lector.Item("dir_ViaNumero")) = True, Nothing, lector.Item("dir_ViaNumero")))
                    objDireccion.ZonaNombre = CStr(IIf(IsDBNull(lector.Item("dir_ZonaNombre")) = True, Nothing, lector.Item("dir_ZonaNombre")))
                    objDireccion.TipoDireccion = CStr(IIf(IsDBNull(lector.Item("td_Nombre")) = True, Nothing, lector.Item("td_Nombre")))
                    Lista.Add(objDireccion)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

End Class
