﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOCalidad
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectAllActivo() As List(Of Entidades.Calidad)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CalidadSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Calidad)
                Do While lector.Read
                    Dim objCalidad As New Entidades.Calidad
                    objCalidad.Abv = CStr(IIf(IsDBNull(lector.Item("Cal_Abv")) = True, "", lector.Item("Cal_Abv")))
                    objCalidad.Nombre = CStr(IIf(IsDBNull(lector.Item("Cal_Nombre")) = True, "", lector.Item("Cal_Nombre")))
                    objCalidad.Estado = CBool(IIf(IsDBNull(lector.Item("Cal_Estado")) = True, "", lector.Item("Cal_Estado")))
                    objCalidad.IdCalidad = CInt(IIf(IsDBNull(lector.Item("IdCalidad")) = True, 0, lector.Item("IdCalidad")))
                    objCalidad.Cal_Codigo = CStr(IIf(IsDBNull(lector.Item("Cal_Codigo")) = True, "", lector.Item("Cal_Codigo")))

                    Lista.Add(objCalidad)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.Calidad)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CalidadSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Calidad)
                Do While lector.Read
                    Dim objCalidad As New Entidades.Calidad
                    objCalidad.Abv = CStr(IIf(IsDBNull(lector.Item("Cal_Abv")) = True, "", lector.Item("Cal_Abv")))
                    objCalidad.Nombre = CStr(IIf(IsDBNull(lector.Item("Cal_Nombre")) = True, "", lector.Item("Cal_Nombre")))
                    objCalidad.Estado = CBool(IIf(IsDBNull(lector.Item("Cal_Estado")) = True, "", lector.Item("Cal_Estado")))
                    objCalidad.IdCalidad = CInt(IIf(IsDBNull(lector.Item("IdCalidad")) = True, 0, lector.Item("IdCalidad")))
                    objCalidad.Cal_Codigo = CStr(IIf(IsDBNull(lector.Item("Cal_Codigo")) = True, "", lector.Item("Cal_Codigo")))
                    Lista.Add(objCalidad)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Calidad)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CalidadSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Calidad)
                Do While lector.Read
                    Dim objCalidad As New Entidades.Calidad
                    objCalidad.Abv = CStr(IIf(IsDBNull(lector.Item("Cal_Abv")) = True, "", lector.Item("Cal_Abv")))
                    objCalidad.Nombre = CStr(IIf(IsDBNull(lector.Item("Cal_Nombre")) = True, "", lector.Item("Cal_Nombre")))
                    objCalidad.Estado = CBool(IIf(IsDBNull(lector.Item("Cal_Estado")) = True, "", lector.Item("Cal_Estado")))
                    objCalidad.IdCalidad = CInt(IIf(IsDBNull(lector.Item("IdCalidad")) = True, 0, lector.Item("IdCalidad")))
                    objCalidad.Cal_Codigo = CStr(IIf(IsDBNull(lector.Item("Cal_Codigo")) = True, "", lector.Item("Cal_Codigo")))
                    Lista.Add(objCalidad)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function InsertaCalidad(ByVal calidad As Entidades.Calidad) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@Cal_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = calidad.Nombre
        ArrayParametros(1) = New SqlParameter("@Cal_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = calidad.Abv

        ArrayParametros(2) = New SqlParameter("@Cal_Codigo", SqlDbType.Char, 2)
        ArrayParametros(2).Value = calidad.Cal_Codigo

        ArrayParametros(3) = New SqlParameter("@Cal_Estado", SqlDbType.Bit)
        ArrayParametros(3).Value = calidad.Estado
        Dim cmd As New SqlCommand("_CalidadInsert", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return True
    End Function

    Public Function ActualizaCalidad(ByVal Calidad As Entidades.Calidad) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@Cal_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = Calidad.Nombre
        ArrayParametros(1) = New SqlParameter("@Cal_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = Calidad.Abv

        ArrayParametros(2) = New SqlParameter("@Cal_Codigo", SqlDbType.Char, 2)
        ArrayParametros(2).Value = Calidad.Cal_Codigo

        ArrayParametros(3) = New SqlParameter("@Cal_Estado", SqlDbType.Bit)
        ArrayParametros(3).Value = Calidad.Estado
        ArrayParametros(4) = New SqlParameter("@IdCalidad", SqlDbType.Int)
        ArrayParametros(4).Value = Calidad.IdCalidad
        Return HDAO.Update(cn, "_CalidadUpdate", ArrayParametros)
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Calidad)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CalidadSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Calidad)
                Do While lector.Read
                    Dim objCalidad As New Entidades.Calidad
                    objCalidad.Abv = CStr(IIf(IsDBNull(lector.Item("Cal_Abv")) = True, "", lector.Item("Cal_Abv")))
                    objCalidad.Nombre = CStr(IIf(IsDBNull(lector.Item("Cal_Nombre")) = True, "", lector.Item("Cal_Nombre")))
                    objCalidad.Estado = CBool(IIf(IsDBNull(lector.Item("Cal_Estado")) = True, "", lector.Item("Cal_Estado")))
                    objCalidad.IdCalidad = CInt(IIf(IsDBNull(lector.Item("IdCalidad")) = True, 0, lector.Item("IdCalidad")))
                    objCalidad.Cal_Codigo = CStr(IIf(IsDBNull(lector.Item("Cal_Codigo")) = True, "", lector.Item("Cal_Codigo")))

                    Lista.Add(objCalidad)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Calidad)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CalidadSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Calidad)
                Do While lector.Read
                    Dim objCalidad As New Entidades.Calidad
                    objCalidad.Abv = CStr(IIf(IsDBNull(lector.Item("Cal_Abv")) = True, "", lector.Item("Cal_Abv")))
                    objCalidad.Nombre = CStr(IIf(IsDBNull(lector.Item("Cal_Nombre")) = True, "", lector.Item("Cal_Nombre")))
                    objCalidad.Estado = CBool(IIf(IsDBNull(lector.Item("Cal_Estado")) = True, "", lector.Item("Cal_Estado")))
                    objCalidad.IdCalidad = CInt(IIf(IsDBNull(lector.Item("IdCalidad")) = True, 0, lector.Item("IdCalidad")))
                    objCalidad.Cal_Codigo = CStr(IIf(IsDBNull(lector.Item("Cal_Codigo")) = True, "", lector.Item("Cal_Codigo")))
                    Lista.Add(objCalidad)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Calidad)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CalidadSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Calidad)
                Do While lector.Read
                    Dim objCalidad As New Entidades.Calidad
                    objCalidad.Abv = CStr(IIf(IsDBNull(lector.Item("Cal_Abv")) = True, "", lector.Item("Cal_Abv")))
                    objCalidad.Nombre = CStr(IIf(IsDBNull(lector.Item("Cal_Nombre")) = True, "", lector.Item("Cal_Nombre")))
                    objCalidad.Estado = CBool(IIf(IsDBNull(lector.Item("Cal_Estado")) = True, "", lector.Item("Cal_Estado")))
                    objCalidad.IdCalidad = CInt(IIf(IsDBNull(lector.Item("IdCalidad")) = True, 0, lector.Item("IdCalidad")))
                    objCalidad.Cal_Codigo = CStr(IIf(IsDBNull(lector.Item("Cal_Codigo")) = True, "", lector.Item("Cal_Codigo")))
                    Lista.Add(objCalidad)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Calidad)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CalidadSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Calidad)
                Do While lector.Read
                    Dim objCalidad As New Entidades.Calidad
                    objCalidad.Abv = CStr(IIf(IsDBNull(lector.Item("Cal_Abv")) = True, "", lector.Item("Cal_Abv")))
                    objCalidad.Nombre = CStr(IIf(IsDBNull(lector.Item("Cal_Nombre")) = True, "", lector.Item("Cal_Nombre")))
                    objCalidad.Estado = CBool(IIf(IsDBNull(lector.Item("Cal_Estado")) = True, "", lector.Item("Cal_Estado")))
                    objCalidad.IdCalidad = CInt(IIf(IsDBNull(lector.Item("IdCalidad")) = True, 0, lector.Item("IdCalidad")))
                    Lista.Add(objCalidad)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
