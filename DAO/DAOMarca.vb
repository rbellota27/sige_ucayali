'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOMarca
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaMarca(ByVal marca As Entidades.Marca) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@mar_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = marca.Descripcion
        ArrayParametros(1) = New SqlParameter("@mar_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = marca.Abv

        ArrayParametros(2) = New SqlParameter("@mar_Codigo", SqlDbType.Char, 2)
        ArrayParametros(2).Value = marca.mar_Codigo

        ArrayParametros(3) = New SqlParameter("@mar_Estado", SqlDbType.Char)
        ArrayParametros(3).Value = marca.Estado
        'Return HDAO.Insert(cn, "_MarcaInsert", ArrayParametros)
        Dim cmd As New SqlCommand("_MarcaInsert", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return True
    End Function
    Public Function ActualizaMarca(ByVal marca As Entidades.Marca) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@mar_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = marca.Descripcion
        ArrayParametros(1) = New SqlParameter("@mar_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = marca.Abv

        ArrayParametros(2) = New SqlParameter("@mar_Codigo", SqlDbType.Char, 2)
        ArrayParametros(2).Value = marca.mar_Codigo

        ArrayParametros(3) = New SqlParameter("@mar_Estado", SqlDbType.Char)
        ArrayParametros(3).Value = marca.Estado
        ArrayParametros(4) = New SqlParameter("@IdMarca", SqlDbType.Int)
        ArrayParametros(4).Value = marca.Id
        Return HDAO.Update(cn, "_MarcaUpdate", ArrayParametros)
    End Function
    Public Function SelectAll() As List(Of Entidades.Marca)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MarcaSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Marca)
                Do While lector.Read
                    Dim objMarca As New Entidades.Marca
                    objMarca.Abv = CStr(IIf(IsDBNull(lector.Item("mar_Abv")) = True, "", lector.Item("mar_Abv")))
                    objMarca.Descripcion = CStr(IIf(IsDBNull(lector.Item("mar_Nombre")) = True, "", lector.Item("mar_Nombre")))
                    objMarca.Estado = CStr(IIf(IsDBNull(lector.Item("mar_Estado")) = True, "", lector.Item("mar_Estado")))
                    objMarca.Id = CInt(IIf(IsDBNull(lector.Item("IdMarca")) = True, 0, lector.Item("IdMarca")))
                    objMarca.mar_Codigo = CStr(IIf(IsDBNull(lector.Item("mar_Codigo")) = True, "", lector.Item("mar_Codigo")))
                    Lista.Add(objMarca)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Marca)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MarcaSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Marca)
                Do While lector.Read
                    Dim objMarca As New Entidades.Marca
                    objMarca.Abv = CStr(IIf(IsDBNull(lector.Item("mar_Abv")) = True, "", lector.Item("mar_Abv")))
                    objMarca.Descripcion = CStr(IIf(IsDBNull(lector.Item("mar_Nombre")) = True, "", lector.Item("mar_Nombre")))
                    objMarca.Estado = CStr(IIf(IsDBNull(lector.Item("mar_Estado")) = True, "", lector.Item("mar_Estado")))
                    objMarca.Id = CInt(IIf(IsDBNull(lector.Item("IdMarca")) = True, 0, lector.Item("IdMarca")))
                    objMarca.mar_Codigo = CStr(IIf(IsDBNull(lector.Item("mar_Codigo")) = True, "", lector.Item("mar_Codigo")))
                    Lista.Add(objMarca)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Marca)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MarcaSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Marca)
                Do While lector.Read
                    Dim objMarca As New Entidades.Marca
                    objMarca.Abv = CStr(IIf(IsDBNull(lector.Item("mar_Abv")) = True, "", lector.Item("mar_Abv")))
                    objMarca.Descripcion = CStr(IIf(IsDBNull(lector.Item("mar_Nombre")) = True, "", lector.Item("mar_Nombre")))
                    objMarca.Estado = CStr(IIf(IsDBNull(lector.Item("mar_Estado")) = True, "", lector.Item("mar_Estado")))
                    objMarca.Id = CInt(IIf(IsDBNull(lector.Item("IdMarca")) = True, 0, lector.Item("IdMarca")))
                    objMarca.mar_Codigo = CStr(IIf(IsDBNull(lector.Item("mar_Codigo")) = True, "", lector.Item("mar_Codigo")))
                    Lista.Add(objMarca)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Marca)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MarcaSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Marca)
                Do While lector.Read
                    Dim objMarca As New Entidades.Marca
                    objMarca.Abv = CStr(IIf(IsDBNull(lector.Item("mar_Abv")) = True, "", lector.Item("mar_Abv")))
                    objMarca.Descripcion = CStr(IIf(IsDBNull(lector.Item("mar_Nombre")) = True, "", lector.Item("mar_Nombre")))
                    objMarca.Estado = CStr(IIf(IsDBNull(lector.Item("mar_Estado")) = True, "", lector.Item("mar_Estado")))
                    objMarca.Id = CInt(IIf(IsDBNull(lector.Item("IdMarca")) = True, 0, lector.Item("IdMarca")))
                    objMarca.mar_Codigo = CStr(IIf(IsDBNull(lector.Item("mar_Codigo")) = True, "", lector.Item("mar_Codigo")))
                    Lista.Add(objMarca)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Marca)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MarcaSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Marca)
                Do While lector.Read
                    Dim objMarca As New Entidades.Marca
                    objMarca.Abv = CStr(IIf(IsDBNull(lector.Item("mar_Abv")) = True, "", lector.Item("mar_Abv")))
                    objMarca.Descripcion = CStr(IIf(IsDBNull(lector.Item("mar_Nombre")) = True, "", lector.Item("mar_Nombre")))
                    objMarca.Estado = CStr(IIf(IsDBNull(lector.Item("mar_Estado")) = True, "", lector.Item("mar_Estado")))
                    objMarca.Id = CInt(IIf(IsDBNull(lector.Item("IdMarca")) = True, 0, lector.Item("IdMarca")))
                    objMarca.mar_Codigo = CStr(IIf(IsDBNull(lector.Item("mar_Codigo")) = True, "", lector.Item("mar_Codigo")))
                    Lista.Add(objMarca)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Marca)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MarcaSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Marca)
                Do While lector.Read
                    Dim objMarca As New Entidades.Marca
                    objMarca.Abv = CStr(IIf(IsDBNull(lector.Item("mar_Abv")) = True, "", lector.Item("mar_Abv")))
                    objMarca.Descripcion = CStr(IIf(IsDBNull(lector.Item("mar_Nombre")) = True, "", lector.Item("mar_Nombre")))
                    objMarca.Estado = CStr(IIf(IsDBNull(lector.Item("mar_Estado")) = True, "", lector.Item("mar_Estado")))
                    objMarca.Id = CInt(IIf(IsDBNull(lector.Item("IdMarca")) = True, 0, lector.Item("IdMarca")))
                    objMarca.mar_Codigo = CStr(IIf(IsDBNull(lector.Item("mar_Codigo")) = True, "", lector.Item("mar_Codigo")))
                    Lista.Add(objMarca)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Marca)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MarcaSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Marca)
                Do While lector.Read
                    Dim objMarca As New Entidades.Marca
                    objMarca.Abv = CStr(IIf(IsDBNull(lector.Item("mar_Abv")) = True, "", lector.Item("mar_Abv")))
                    objMarca.Descripcion = CStr(IIf(IsDBNull(lector.Item("mar_Nombre")) = True, "", lector.Item("mar_Nombre")))
                    objMarca.Estado = CStr(IIf(IsDBNull(lector.Item("mar_Estado")) = True, "", lector.Item("mar_Estado")))
                    objMarca.Id = CInt(IIf(IsDBNull(lector.Item("IdMarca")) = True, 0, lector.Item("IdMarca")))
                    Lista.Add(objMarca)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
