﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : caro freyre, verly
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 01-Oct-2009
'Hora    : 05:09 pm
'*************************************************
Imports System.Data.SqlClient
Public Class DAOCajaUsuario
    Private objHelperDao As New HelperDAO
    Private objConexion As New Conexion


    Public Function SelectCboxIdTiendaxIdUsuario(ByVal IdUsuario As Integer, ByVal IdTienda As Integer) As List(Of Entidades.Caja)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CajaUsuarioSelectCboxIdTiendaxIdUsuario", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Caja)
                Do While lector.Read
                    Dim obj As New Entidades.Caja()
                    obj.IdCaja = CInt(IIf(IsDBNull(lector("IdCaja")) = True, 0, lector("IdCaja")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector("caja_Nombre")) = True, "", lector("caja_Nombre")))
                    
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function









    Public Function ValidarCajaActivoxIdPersonaxIdTiendaxIdCaja(ByVal idpersona As Integer, ByVal idtienda As Integer, ByVal idcaja As Integer) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ValidarCajaActivoxIdPersonaxIdTiendaxIdCaja", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", idpersona)
        cmd.Parameters.AddWithValue("@IdTienda", idtienda)
        cmd.Parameters.AddWithValue("@IdCaja", idcaja)
        Try
            cn.Open()
            Dim lector As SqlDataReader = cmd.ExecuteReader
            If lector.Read Then
                Return CInt(lector("Contador"))
            Else
                Throw New Exception
            End If
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Sub Insert(ByVal objCajaUsuario As Entidades.CajaUsuario, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Dim parametros() As SqlParameter = New SqlParameter(2) {}
        parametros(0) = New SqlParameter("@IdCaja", SqlDbType.Int)
        parametros(0).Value = objCajaUsuario.IdCaja
        parametros(1) = New SqlParameter("@IdPersona", SqlDbType.Int)
        parametros(1).Value = objCajaUsuario.IdPersona
        parametros(2) = New SqlParameter("@caju_Estado", SqlDbType.Char)
        parametros(2).Value = objCajaUsuario.Estado
        objHelperDao.InsertaT(cn, "_CajaUsuarioInsert", parametros, tr)
    End Sub
    Public Sub Update(ByVal objCajaUsuario As Entidades.CajaUsuario, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Dim parametros() As SqlParameter = New SqlParameter(2) {}
        parametros(0) = New SqlParameter("@IdCaja", SqlDbType.Int)
        parametros(0).Value = objCajaUsuario.IdCaja
        parametros(1) = New SqlParameter("@IdPersona", SqlDbType.Int)
        parametros(1).Value = objCajaUsuario.IdPersona
        parametros(2) = New SqlParameter("@caju_Estado", SqlDbType.Char)
        parametros(2).Value = objCajaUsuario.Estado
        objHelperDao.UpdateT(cn, "_CajaUsuarioUpdate", parametros, tr)
    End Sub
    Public Function SelectAllxIdPersona(ByVal idpersona As Integer) As List(Of Entidades.CajaUsuario)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CajaUsuarioSelectAllxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", idpersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CajaUsuario)
                Do While lector.Read
                    Dim obj As New Entidades.CajaUsuario()
                    obj.IdCaja = CInt(IIf(IsDBNull(lector("IdCaja")) = True, 0, lector("IdCaja")))
                    obj.IdPersona = CInt(IIf(IsDBNull(lector("IdPersona")) = True, 0, lector("IdPersona")))
                    obj.Estado = CStr(IIf(IsDBNull(lector("caju_Estado")) = True, "0", lector("caju_Estado")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Sub DeletexIdCajaxIdPersona(ByVal idpersona As Integer, ByVal idcaja As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Dim cmd As New SqlCommand("_CajaUsuarioDeletexIdCajaxIdPersona", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", idpersona)
        cmd.Parameters.AddWithValue("@IdCaja", idcaja)
        If cmd.ExecuteNonQuery = 0 Then
            Throw New Exception
        End If
    End Sub

    Public Function SelectxIdPersona(ByVal idpersona As Integer) As List(Of Entidades.Caja)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CajaUsuarioSelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", idpersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Caja)
                Do While lector.Read
                    Dim obj As New Entidades.Caja
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("caju_Estado")) = True, "0", lector.Item("caju_Estado")))
                    obj.IdCaja = CInt(IIf(IsDBNull(lector.Item("IdCaja")) = True, 0, lector.Item("IdCaja")))
                    obj.IdTienda = CInt(IIf(IsDBNull(lector.Item("IdTienda")) = True, 0, lector.Item("IdTienda")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("caja_Nombre")) = True, "", lector.Item("caja_Nombre")))
                    obj.NomTienda = CStr(IIf(IsDBNull(lector.Item("tie_Nombre")) = True, "", lector.Item("tie_Nombre")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectCountxIdTienda(ByVal idtienda As Integer) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CajaUsuarioSelectCountxIdTienda", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTienda", idtienda)
        Dim lector As SqlDataReader
        Dim cont As Integer = 0
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                If lector.Read Then
                    cont = CInt(lector.Item("cont"))
                End If
                lector.Close()
                Return cont
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    'Autor  : Hans Quispe Espinoza
    'Fecha  : 1-09-2009
    'Asunto : Obtener Deudas de dicho Cliente
    'Modulo : Caja 
    'Web    : fmrCajaIngreso.aspx
    Public Function fnObtenerDeudasxIdCliente(ByVal _IdCliente As Integer) As List(Of Entidades.Cliente)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("DocumentoObtenerDeudaxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", _IdCliente)
        Dim lector As SqlDataReader

        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Cliente)
                While lector.Read
                    Dim objCliente As New Entidades.Cliente
                    objCliente.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    objCliente.Nom_Nombre = CStr(IIf(IsDBNull(lector.Item("mon_Nombre")) = True, "", lector.Item("mon_Nombre")))






                    objCliente.Mcu_Saldo = CDec(IIf(IsDBNull(lector.Item("mcu_Saldo")) = True, 0, lector.Item("mcu_Saldo")))
                    objCliente.Mcu_Monto = CDec(IIf(IsDBNull(lector.Item("mcu_Monto")) = True, 0, lector.Item("mcu_Monto")))
                    objCliente.IdTipoDocumento = CInt(IIf(IsDBNull(lector.Item("IdTipoDocumento")) = True, 0, lector.Item("IdTipoDocumento")))
                    objCliente.IdMovCuenta = CInt(IIf(IsDBNull(lector.Item("IdMovCuenta")) = True, 0, lector.Item("IdMovCuenta")))
                    objCliente.IdDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                    objCliente.Doc_Serie = CStr(IIf(IsDBNull(lector.Item("doc_Serie")) = True, "", lector.Item("doc_Serie")))
                    objCliente.IdMoneda = CInt(IIf(IsDBNull(lector("IdMoneda")) = True, 0, lector("IdMoneda")))
                    objCliente.DiasTrans = CStr(IIf(IsDBNull(lector.Item("DiasTrans")) = True, 0, lector.Item("DiasTrans")))
                    objCliente.Doc_FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                    objCliente.Doc_FechaVenc = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                    objCliente.FechaServidor = CDate(IIf(IsDBNull(lector.Item("FechaServidor")) = True, Nothing, lector.Item("FechaServidor")))
                    objCliente.Tdoc_NombreCorto = CStr(IIf(IsDBNull(lector.Item("tdoc_NombreCorto")) = True, "", lector.Item("tdoc_NombreCorto")))
                    Lista.Add(objCliente)
                End While
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function SelectCboxIdTiendaxIdUsuario_Caja(ByVal IdTienda As Integer, ByVal IdUsuario As Integer) As List(Of Entidades.Caja)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CajaSelectCboxIdTiendaxIdUsuario_Caja", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Caja)
                Do While lector.Read
                    Dim obj As New Entidades.Caja
                    obj.IdCaja = CInt(IIf(IsDBNull(lector("IdCaja")) = True, 0, lector("IdCaja")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector("caja_Nombre")) = True, 0, lector("caja_Nombre")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function







End Class

