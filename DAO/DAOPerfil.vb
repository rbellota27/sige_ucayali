﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOPerfil
    Private objConexion As New Conexion
    'Public Function PerfilUpdate(ByVal objPerfil As Entidades.Perfil) As Boolean

    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim hecho As Boolean = False
    '    Try

    '        cn.Open()

    '        Dim cmd As New SqlCommand("_PerfilUpdate", cn)
    '        cmd.CommandType = CommandType.StoredProcedure
    '        cmd.Parameters.AddWithValue("@perm_Descripcion", objPerfil.perm_Descripcion)
    '        cmd.Parameters.AddWithValue("@perf_Estado", objPerfil.Estado)
    '        cmd.Parameters.AddWithValue("@IdPerfil", objPerfil.IdPerfil)

    '        cmd.ExecuteNonQuery()

    '        hecho = True
    '    Catch ex As Exception
    '        hecho = False
    '    Finally
    '        If (cn.State = ConnectionState.Open) Then cn.Close()
    '    End Try

    '    Return hecho

    'End Function
    Public Function PerfilUpdate(ByVal Perfil As Entidades.Perfil, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@perm_Descripcion", SqlDbType.VarChar)
        ArrayParametros(0).Value = Perfil.perm_Descripcion
        ArrayParametros(1) = New SqlParameter("@perf_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = Perfil.Estado
        ArrayParametros(2) = New SqlParameter("@flagpermisodescmasivo", SqlDbType.Int)
        ArrayParametros(2).Value = Perfil.EstadoMasivo
        ArrayParametros(3) = New SqlParameter("@IdPerfil", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(Perfil.IdPerfil = Nothing, DBNull.Value, Perfil.IdPerfil)
        Dim cmd As New SqlCommand("_PerfilUpdate", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        If (cmd.ExecuteNonQuery = 0) Then
            Return False
        End If
        Return True
    End Function
    'Public Function PerfilInsert(ByVal objPerfil As Entidades.Perfil) As Boolean

    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim hecho As Boolean = False
    '    Try

    '        cn.Open()

    '        Dim cmd As New SqlCommand("_PerfilInsert", cn)
    '        cmd.CommandType = CommandType.StoredProcedure
    '        cmd.Parameters.AddWithValue("@perm_Descripcion", objPerfil.perm_Descripcion)
    '        cmd.Parameters.AddWithValue("@perf_Estado", objPerfil.Estado)

    '        cmd.ExecuteNonQuery()

    '        hecho = True
    '    Catch ex As Exception
    '        hecho = False
    '    Finally
    '        If (cn.State = ConnectionState.Open) Then cn.Close()
    '    End Try

    '    Return hecho

    ' End Function

    Public Function InsertaPerfil(ByVal Perfil As Entidades.Perfil, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer
        'Inserta solo Concepto
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@perm_Descripcion", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(Perfil.perm_Descripcion = Nothing, DBNull.Value, Perfil.perm_Descripcion)
        ArrayParametros(1) = New SqlParameter("@perf_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = IIf(Perfil.Estado = Nothing, DBNull.Value, Perfil.Estado)
        ArrayParametros(2) = New SqlParameter("@flagpermisodescmasivo", SqlDbType.Int)
        ArrayParametros(2).Value = IIf(Perfil.EstadoMasivo = Nothing, DBNull.Value, Perfil.EstadoMasivo)
        ArrayParametros(3) = New SqlParameter("@IdPerfil", SqlDbType.Int)
        ArrayParametros(3).Direction = ParameterDirection.Output
        Dim cmd As New SqlCommand("_PerfilInsert", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        cmd.ExecuteNonQuery()
        Return CInt(cmd.Parameters("@IdPerfil").Value)
    End Function




    Public Function SelectxIdPerfilxNombrexEstado(ByVal IdPerfil As Integer, ByVal Nombre As String, ByVal Estado As String, ByVal EstadoMasivo As String) As List(Of Entidades.Perfil)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PerfilSelectxIdPerfilxNombrexEstado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPerfil", IdPerfil)
        cmd.Parameters.AddWithValue("@Nombre", Nombre)
        cmd.Parameters.AddWithValue("@Estado", Estado)
        cmd.Parameters.AddWithValue("@EstadoMasivo", EstadoMasivo)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Perfil)
                Do While lector.Read
                    Dim obj As New Entidades.Perfil
                    With obj
                        .IdPerfil = CInt(lector.Item("IdPerfil"))
                        .NomPerfil = CStr(lector.Item("perf_Nombre"))
                        .Estado = CStr(lector.Item("perf_Estado"))
                        .EstadoMasivo = CStr(lector.Item("EstadoMasivo"))

                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function










    Public Function SelectCbo() As List(Of Entidades.Perfil)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PerfilSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Perfil)
                Do While lector.Read
                    Dim obj As New Entidades.Perfil
                    With obj
                        .IdPerfil = CInt(lector.Item("IdPerfil"))
                        .NomPerfil = CStr(lector.Item("perf_Nombre"))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectIdsxIdPersona(ByVal IdPersona As Integer) As List(Of Entidades.Perfil)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PerfilSelectIdsxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Perfil)
                Do While lector.Read
                    Dim obj As New Entidades.Perfil
                    With obj
                        .IdPerfil = CInt(lector.Item("IdPerfil"))
                    End With
                    Lista.Add(obj)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
