﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOTipoTablaValor
    Dim Helper As New DAO.HelperDAO
    Dim ObjConexion As New Conexion

    Public Function TipoTablaValorSelectAllxIdTipoTablaxEstado_Paginado( _
                ByVal idtipotabla As Integer, ByVal descripcion As String, _
                ByVal estado As Integer, ByVal PageIndex As Integer, _
                ByVal PageSize As Integer) As List(Of Entidades.TipoTablaValor)

        If IsNothing(descripcion) Then
            descripcion = ""
        Else
            descripcion = descripcion.Replace("*", "%")
        End If        

        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTablaValorSelectAllxIdTipoTablaxEstado_Paginado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoTabla", idtipotabla)
        cmd.Parameters.AddWithValue("@t_nombre", descripcion)
        cmd.Parameters.AddWithValue("@t_Estado", estado)
        cmd.Parameters.AddWithValue("@pageIndex", PageIndex)
        cmd.Parameters.AddWithValue("@pageSize", PageSize)


        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.TipoTablaValor)
                Do While lector.Read
                    Dim obj As New Entidades.TipoTablaValor
                    With obj
                        .IdTipoTablaValor = CInt(IIf(IsDBNull(lector.Item("IdTipoTablaValor")) = True, 0, lector.Item("IdTipoTablaValor")))
                        .IdTipoTabla = CInt(IIf(IsDBNull(lector.Item("IdTipoTabla")) = True, 0, lector.Item("IdTipoTabla")))
                        '.TipoUso = CStr(IIf(IsDBNull(lector.Item("TipoUso")) = True, "", lector.Item("TipoUso")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("ttv_Codigo")) = True, "", lector.Item("ttv_Codigo")))
                        .Nombre = CStr(IIf(IsDBNull(lector.Item("ttv_Nombre")) = True, "", lector.Item("ttv_Nombre")))
                        .Abv = CStr(IIf(IsDBNull(lector.Item("ttv_Abv")) = True, "", lector.Item("ttv_Abv")))
                        .Estado = CBool(IIf(IsDBNull(lector.Item("ttv_Estado")) = True, 0, lector.Item("ttv_Estado")))
                        .NomTabla = CStr(IIf(IsDBNull(lector.Item("tt_Nombre")) = True, "", lector.Item("tt_Nombre")))
                        lista.Add(obj)
                    End With
                Loop
                lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function InsertaTipoTablaValor(ByVal obj As Entidades.TipoTablaValor) As Boolean
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("IdTipoTabla", SqlDbType.Int)
        ArrayParametros(0).Value = obj.IdTipoTabla
        ArrayParametros(1) = New SqlParameter("ttv_Codigo", SqlDbType.Char, 10)
        ArrayParametros(1).Value = obj.Codigo
        ArrayParametros(2) = New SqlParameter("ttv_Nombre", SqlDbType.VarChar, 150)
        ArrayParametros(2).Value = obj.Nombre
        ArrayParametros(3) = New SqlParameter("ttv_Abv", SqlDbType.VarChar, 150)
        ArrayParametros(3).Value = obj.Abv
        ArrayParametros(4) = New SqlParameter("ttv_Estado", SqlDbType.Bit)
        ArrayParametros(4).Value = obj.Estado
        Dim cmd As New SqlCommand("_TipoTablaValorInsert", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return True
    End Function
    Public Function ActualizaTipoTablaValor(ByVal obj As Entidades.TipoTablaValor) As Boolean
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
        ArrayParametros(0) = New SqlParameter("IdTipoTablaValor", SqlDbType.Int)
        ArrayParametros(0).Value = obj.IdTipoTablaValor
        ArrayParametros(1) = New SqlParameter("IdTipoTabla", SqlDbType.Int)
        ArrayParametros(1).Value = obj.IdTipoTabla
        ArrayParametros(2) = New SqlParameter("ttv_Codigo", SqlDbType.Char, 20)
        ArrayParametros(2).Value = obj.Codigo
        ArrayParametros(3) = New SqlParameter("ttv_Nombre", SqlDbType.VarChar, 150)
        ArrayParametros(3).Value = obj.Nombre
        ArrayParametros(4) = New SqlParameter("ttv_Abv", SqlDbType.Char, 150)
        ArrayParametros(4).Value = obj.Abv
        ArrayParametros(5) = New SqlParameter("ttv_Estado", SqlDbType.Bit)
        ArrayParametros(5).Value = obj.Estado
        Dim cmd As New SqlCommand("_TipoTablaValorUpdate", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return True
    End Function
    Public Function SelectAllxIdTipoTablaxEstado(ByVal idtipotabla As Integer, ByVal estado As Integer) As List(Of Entidades.TipoTablaValor)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTablaValorSelectAllxIdTipoTablaxEstado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoTabla", idtipotabla)
        cmd.Parameters.AddWithValue("@Estado", estado)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.TipoTablaValor)
                Do While lector.Read
                    Dim obj As New Entidades.TipoTablaValor
                    With obj
                        .IdTipoTablaValor = CInt(IIf(IsDBNull(lector.Item("IdTipoTablaValor")) = True, 0, lector.Item("IdTipoTablaValor")))
                        .IdTipoTabla = CInt(IIf(IsDBNull(lector.Item("IdTipoTabla")) = True, 0, lector.Item("IdTipoTabla")))
                        '.TipoUso = CStr(IIf(IsDBNull(lector.Item("TipoUso")) = True, "", lector.Item("TipoUso")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("ttv_Codigo")) = True, "", lector.Item("ttv_Codigo")))
                        .Nombre = CStr(IIf(IsDBNull(lector.Item("ttv_Nombre")) = True, "", lector.Item("ttv_Nombre")))
                        .Abv = CStr(IIf(IsDBNull(lector.Item("ttv_Abv")) = True, "", lector.Item("ttv_Abv")))
                        .Estado = CBool(IIf(IsDBNull(lector.Item("ttv_Estado")) = True, 0, lector.Item("ttv_Estado")))
                        lista.Add(obj)
                    End With
                Loop
                lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxIdTipoTablaxEstado(ByVal idsublinea As Integer, ByVal idtipotabla As Integer, ByVal estado As Integer) As List(Of Entidades.TipoTablaValor)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTablaValorSelectAllxIdSubLineaxIdTipoTablaxEstado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        cmd.Parameters.AddWithValue("@IdTipoTabla", idtipotabla)
        cmd.Parameters.AddWithValue("@Estado", estado)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.TipoTablaValor)
                Do While lector.Read
                    Dim obj As New Entidades.TipoTablaValor
                    With obj
                        .IdTipoTablaValor = CInt(IIf(IsDBNull(lector.Item("IdTipoTablaValor")) = True, 0, lector.Item("IdTipoTablaValor")))
                        .IdTipoTabla = CInt(IIf(IsDBNull(lector.Item("IdTipoTabla")) = True, 0, lector.Item("IdTipoTabla")))
                        .TipoUso = CStr(IIf(IsDBNull(lector.Item("TipoUso")) = True, "", lector.Item("TipoUso")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("ttv_Codigo")) = True, "", lector.Item("ttv_Codigo")))
                        .Nombre = CStr(IIf(IsDBNull(lector.Item("ttv_Nombre")) = True, "", lector.Item("ttv_Nombre")))
                        .Abv = CStr(IIf(IsDBNull(lector.Item("ttv_Abv")) = True, "", lector.Item("ttv_Abv")))
                        .Estado = CBool(IIf(IsDBNull(lector.Item("ttv_Estado")) = True, 0, lector.Item("ttv_Estado")))
                        lista.Add(obj)
                    End With
                Loop
                lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxIdTipoTablaxnombre(ByVal idsublinea As Integer, ByVal idtipotabla As Integer, _
                                                 ByVal nombre As String, ByVal PageIndex As Integer, ByVal PageSize As Integer) As List(Of Entidades.TipoTablaValor)
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("selectTipotablavalor", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        cmd.Parameters.AddWithValue("@IdTipoTabla", idtipotabla)
        cmd.Parameters.AddWithValue("@ttvNombre", nombre)
        cmd.Parameters.AddWithValue("@PageIndex", PageIndex)
        cmd.Parameters.AddWithValue("@PageSize", PageSize)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.TipoTablaValor)
                Do While lector.Read
                    Dim obj As New Entidades.TipoTablaValor
                    With obj
                        .IdTipoTablaValor = CInt(IIf(IsDBNull(lector.Item("IdTipoTablaValor")) = True, 0, lector.Item("IdTipoTablaValor")))
                        .IdTipoTabla = CInt(IIf(IsDBNull(lector.Item("IdTipoTabla")) = True, 0, lector.Item("IdTipoTabla")))
                        .TipoUso = CStr(IIf(IsDBNull(lector.Item("TipoUso")) = True, "", lector.Item("TipoUso")))
                        .Codigo = CStr(IIf(IsDBNull(lector.Item("ttv_Codigo")) = True, "", lector.Item("ttv_Codigo")))
                        .Nombre = CStr(IIf(IsDBNull(lector.Item("ttv_Nombre")) = True, "", lector.Item("ttv_Nombre")))
                        .Abv = CStr(IIf(IsDBNull(lector.Item("ttv_Abv")) = True, "", lector.Item("ttv_Abv")))
                        .Estado = CBool(IIf(IsDBNull(lector.Item("ttv_Estado")) = True, 0, lector.Item("ttv_Estado")))
                        lista.Add(obj)
                    End With
                Loop
                lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function GenerarCodigo(ByVal idtipotabla As Integer) As Entidades.TipoTablaValor
        Dim cn As SqlConnection = ObjConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTablaValorGenerarCorrelativoxIdTipoTabla", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoTabla", idtipotabla)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim obj As Entidades.TipoTablaValor = Nothing
                If lector.Read Then
                    obj = New Entidades.TipoTablaValor
                    obj.Codigo = CStr(IIf(IsDBNull(lector.Item("Codigo")) = True, "", lector.Item("Codigo")))
                End If
                lector.Close()
                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
