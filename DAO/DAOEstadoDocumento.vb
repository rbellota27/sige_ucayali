'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOEstadoDocumento
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaEstadoDocumento(ByVal estadodocumento As Entidades.EstadoDocumento) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@edoc_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = estadodocumento.Descripcion
        ArrayParametros(1) = New SqlParameter("@edoc_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = estadodocumento.Estado
        Return HDAO.Insert(cn, "_EstadoDocumentoInsert", ArrayParametros)
    End Function
    Public Function ActualizaEstadoDocumento(ByVal estadodocumento As Entidades.EstadoDocumento) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@edoc_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = estadodocumento.Descripcion
        ArrayParametros(1) = New SqlParameter("@edoc_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = estadodocumento.Estado
        ArrayParametros(2) = New SqlParameter("@IdEstadoDoc", SqlDbType.Int)
        ArrayParametros(2).Value = estadodocumento.Id
        Return HDAO.Update(cn, "_EstadoDocumentoUpdate", ArrayParametros)
    End Function
    Public Function SelectAll() As List(Of Entidades.EstadoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstadoDocumentoSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.EstadoDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.EstadoDocumento
                    obj.Descripcion = CStr(lector.Item("edoc_Nombre"))
                    obj.Estado = CStr(lector.Item("edoc_Estado"))
                    obj.Id = CInt(lector.Item("IdEstadoDoc"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.EstadoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstadoDocumentoSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.EstadoDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.EstadoDocumento
                    obj.Descripcion = CStr(lector.Item("edoc_Nombre"))
                    obj.Estado = CStr(lector.Item("edoc_Estado"))
                    obj.Id = CInt(lector.Item("IdEstadoDoc"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.EstadoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstadoDocumentoSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.EstadoDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.EstadoDocumento
                    obj.Descripcion = CStr(lector.Item("edoc_Nombre"))
                    obj.Estado = CStr(lector.Item("edoc_Estado"))
                    obj.Id = CInt(lector.Item("IdEstadoDoc"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.EstadoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstadoDocumentoSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.EstadoDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.EstadoDocumento
                    obj.Descripcion = CStr(lector.Item("edoc_Nombre"))
                    obj.Estado = CStr(lector.Item("edoc_Estado"))
                    obj.Id = CInt(lector.Item("IdEstadoDoc"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.EstadoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstadoDocumentoSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.EstadoDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.EstadoDocumento
                    obj.Descripcion = CStr(lector.Item("edoc_Nombre"))
                    obj.Estado = CStr(lector.Item("edoc_Estado"))
                    obj.Id = CInt(lector.Item("IdEstadoDoc"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.EstadoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstadoDocumentoSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.EstadoDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.EstadoDocumento
                    obj.Descripcion = CStr(lector.Item("edoc_Nombre"))
                    obj.Estado = CStr(lector.Item("edoc_Estado"))
                    obj.Id = CInt(lector.Item("IdEstadoDoc"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.EstadoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstadoDocumentoSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.EstadoDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.EstadoDocumento
                    obj.Descripcion = CStr(lector.Item("edoc_Nombre"))
                    obj.Estado = CStr(lector.Item("edoc_Estado"))
                    obj.Id = CInt(lector.Item("IdEstadoDoc"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCbo() As List(Of Entidades.EstadoDocumento)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EstadoDocumentoSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.EstadoDocumento)
                Do While lector.Read
                    Dim obj As New Entidades.EstadoDocumento
                    obj.Descripcion = CStr(lector.Item("edoc_Nombre"))
                    obj.Id = CInt(lector.Item("IdEstadoDoc"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
