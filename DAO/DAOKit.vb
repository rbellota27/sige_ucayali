﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAOKit
    Inherits DAO.DAOMantenedor

    Private reader As SqlDataReader

    Public Function SelectComponentexIdKitDOC_OC(ByVal IdKit As Integer, ByVal IdDocumento As Integer) As List(Of Entidades.Kit)

        Dim lista As New List(Of Entidades.Kit)

        Try

            Dim p() As SqlParameter = New SqlParameter(1) {}
            p(0) = MyBase.getParam(IdKit, "@IdKit", SqlDbType.Int)
            p(1) = MyBase.getParam(IdDocumento, "@IdDocumento", SqlDbType.Int)

            MyBase.Cn = objConexion.ConexionSIGE
            MyBase.Cn.Open()

            reader = SqlHelper.ExecuteReader(MyBase.Cn, CommandType.StoredProcedure, "[dbo].[_Kit_SelectComponentexIdKitDOC_OC]", p)

            While (reader.Read)

                Dim objKit As New Entidades.Kit
                With objKit

                    .IdKit = MyBase.UCInt(reader("IdKit"))
                    .IdComponente = MyBase.UCInt(reader("IdComponente"))
                    .Cantidad_Comp = MyBase.UCDec(reader("kit_Cantidad_Comp"))
                    .IdUnidadMedida_Comp = MyBase.UCInt(reader("IdUnidadMedida_Comp"))
                    .Componente = MyBase.UCStr(reader("prod_Nombre"))
                    .CodigoProd_Comp = MyBase.UCStr(reader("prod_Codigo"))
                    .UnidadMedida_Comp = MyBase.UCStr(reader("um_NombreCorto"))

                    .IdMonedaPrecio_Comp = MyBase.UCInt(reader("IdMonedaPrecio_Comp"))
                    .Precio_Comp = MyBase.UCDec(reader("kit_Precio_Comp"))
                    .MonedaPrecio_Comp = MyBase.UCStr(reader("mon_Simbolo"))
                    .PorcentajeKit = MyBase.UCInt(reader("porcentkit"))
                End With

                lista.Add(objKit)

            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (MyBase.Cn.State = ConnectionState.Open) Then MyBase.Cn.Close()
        End Try

        Return lista

    End Function

    Public Function SelectComponentexIdKit_OC(ByVal IdKit As Integer, ByVal IdMoneda As Integer) As List(Of Entidades.Kit)

        Dim lista As New List(Of Entidades.Kit)

        Try

            Dim p() As SqlParameter = New SqlParameter(1) {}
            p(0) = MyBase.getParam(IdKit, "@IdKit", SqlDbType.Int)
            p(1) = MyBase.getParam(IdMoneda, "@IdMoneda", SqlDbType.Int)

            MyBase.Cn = objConexion.ConexionSIGE
            MyBase.Cn.Open()

            reader = SqlHelper.ExecuteReader(MyBase.Cn, CommandType.StoredProcedure, "_Kit_SelectComponentexIdKit_OC", p)

            While (reader.Read)

                Dim objKit As New Entidades.Kit
                With objKit

                    .IdKit = MyBase.UCInt(reader("IdKit"))
                    .IdComponente = MyBase.UCInt(reader("IdComponente"))
                    .Cantidad_Comp = MyBase.UCDec(reader("kit_Cantidad_Comp"))
                    .IdUnidadMedida_Comp = MyBase.UCInt(reader("IdUnidadMedida_Comp"))
                    .Componente = MyBase.UCStr(reader("prod_Nombre"))
                    .CodigoProd_Comp = MyBase.UCStr(reader("prod_Codigo"))
                    .UnidadMedida_Comp = MyBase.UCStr(reader("um_NombreCorto"))

                    .IdMonedaPrecio_Comp = MyBase.UCInt(reader("IdMonedaPrecio_Comp"))
                    .Precio_Comp = MyBase.UCDec(reader("kit_Precio_Comp"))
                    .MonedaPrecio_Comp = MyBase.UCStr(reader("mon_Simbolo"))
                    .PorcentajeKit = MyBase.UCInt(reader("porcentkit"))
                End With

                lista.Add(objKit)

            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (MyBase.Cn.State = ConnectionState.Open) Then MyBase.Cn.Close()
        End Try

        Return lista

    End Function

    Public Function SelectComponenteDetallexParams(ByVal IdKit As Integer, ByVal IdUnidadMedidaKit As Integer, ByVal CantidadKit As Decimal, ByVal IdMonedaKit As Integer, ByVal IdTienda As Integer, ByVal IdTipoPV As Integer) As List(Of Entidades.Kit)

        Dim lista As New List(Of Entidades.Kit)

        Try

            Dim p() As SqlParameter = New SqlParameter(5) {}
            p(0) = MyBase.getParam(IdKit, "@IdKit", SqlDbType.Int)
            p(1) = MyBase.getParam(IdUnidadMedidaKit, "@IdUnidadMedidaKit", SqlDbType.Int)
            p(2) = MyBase.getParam(IdMonedaKit, "@IdMonedaKit", SqlDbType.Int)
            p(3) = MyBase.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(4) = MyBase.getParam(IdTipoPV, "@IdTipoPV", SqlDbType.Int)
            p(5) = MyBase.getParam(CantidadKit, "@CantidadKit", SqlDbType.Decimal)

            MyBase.Cn = objConexion.ConexionSIGE
            MyBase.Cn.Open()

            reader = SqlHelper.ExecuteReader(MyBase.Cn, CommandType.StoredProcedure, "_Kit_SelectComponenteDetallexParams", p)

            While (reader.Read)

                Dim objKit As New Entidades.Kit
                With objKit

                    .IdKit = MyBase.UCInt(reader("IdKit"))
                    .IdComponente = MyBase.UCInt(reader("IdComponente"))
                    .Cantidad_Comp = MyBase.UCDec(reader("CantidadUnit_Comp"))
                    .IdUnidadMedida_Comp = MyBase.UCInt(reader("IdUnidadMedida_Comp"))
                    .IdMonedaPrecio_Comp = MyBase.UCInt(reader("IdMonedaPrecio_Comp"))
                    .Precio_Comp = MyBase.UCDec(reader("PrecioUnit_Comp"))

                    .Componente = MyBase.UCStr(reader("Componente"))
                    .CodigoProd_Comp = MyBase.UCStr(reader("CodigoProducto_Comp"))
                    .UnidadMedida_Comp = MyBase.UCStr(reader("UnidadMedida_Comp"))
                    .MonedaPrecio_Comp = MyBase.UCStr(reader("MonedaPrecio_Comp"))

                    .CantidadTotal_Comp = MyBase.UCDec(reader("CantidadTotal_Comp"))
                    .PrecioUnitEqKit_Comp = MyBase.UCDec(reader("PrecioUnitEqKit_Comp"))
                    .PrecioListaUnitEqKit_Comp = MyBase.UCDec(reader("PrecioListaUnitEqKit_Comp"))
                    .PrecioComercialUnitEqKit_Comp = MyBase.UCDec(reader("PrecioComercialUnitEqKit_Comp"))


                End With

                lista.Add(objKit)

            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (MyBase.Cn.State = ConnectionState.Open) Then MyBase.Cn.Close()
        End Try

        Return lista

    End Function
    Public Sub Kit_ActualizarxIdKitxIdComponente(ByVal objKit As Entidades.Kit)
        Dim p() As SqlParameter = New SqlParameter(2) {}
        p(0) = MyBase.getParam(objKit.IdKit, "@IdKit", SqlDbType.Int)
        p(1) = MyBase.getParam(objKit.IdComponente, "@IdComponente", SqlDbType.Int)
        p(2) = MyBase.getParam(objKit.Precio_Comp, "@kit_Precio_Comp", SqlDbType.Decimal)
        MyBase.Cn = objConexion.ConexionSIGE
        MyBase.Cn.Open()
        SqlHelper.ExecuteNonQuery(Cn, CommandType.StoredProcedure, "_Kit_Actualizar", p)

    End Sub
    Public Function SelectComponentexIdKit(ByVal IdKit As Integer) As List(Of Entidades.Kit)

        Dim lista As New List(Of Entidades.Kit)

        Try

            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = MyBase.getParam(IdKit, "@IdKit", SqlDbType.Int)

            MyBase.Cn = objConexion.ConexionSIGE
            MyBase.Cn.Open()

            reader = SqlHelper.ExecuteReader(MyBase.Cn, CommandType.StoredProcedure, "_Kit_SelectComponentexIdKit", p)

            While (reader.Read)

                Dim objKit As New Entidades.Kit
                With objKit

                    .IdKit = MyBase.UCInt(reader("IdKit"))
                    .IdComponente = MyBase.UCInt(reader("IdComponente"))
                    .Cantidad_Comp = MyBase.UCDec(reader("kit_Cantidad_Comp"))
                    .IdUnidadMedida_Comp = MyBase.UCInt(reader("IdUnidadMedida_Comp"))
                    .Componente = MyBase.UCStr(reader("prod_Nombre"))
                    .CodigoProd_Comp = MyBase.UCStr(reader("prod_Codigo"))
                    .UnidadMedida_Comp = MyBase.UCStr(reader("um_NombreCorto"))

                    .IdMonedaPrecio_Comp = MyBase.UCInt(reader("IdMonedaPrecio_Comp"))
                    .Precio_Comp = MyBase.UCDec(reader("kit_Precio_Comp"))
                    .MonedaPrecio_Comp = MyBase.UCStr(reader("mon_Simbolo"))
                    .PorcentajeKit = MyBase.UCInt(reader("porcentkit"))
                End With

                lista.Add(objKit)

            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (MyBase.Cn.State = ConnectionState.Open) Then MyBase.Cn.Close()
        End Try

        Return lista

    End Function

    Public Function SelectComponentexIdKit2(ByVal IdKit As Integer) As List(Of Entidades.Kit)

        Dim lista As New List(Of Entidades.Kit)

        Try

            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = MyBase.getParam(IdKit, "@IdKit", SqlDbType.Int)

            MyBase.Cn = objConexion.ConexionSIGE2
            MyBase.Cn.Open()

            reader = SqlHelper.ExecuteReader(MyBase.Cn, CommandType.StoredProcedure, "_Kit_SelectComponentexIdKit", p)

            While (reader.Read)

                Dim objKit As New Entidades.Kit
                With objKit

                    .IdKit = MyBase.UCInt(reader("IdKit"))
                    .IdComponente = MyBase.UCInt(reader("IdComponente"))
                    .Cantidad_Comp = MyBase.UCDec(reader("kit_Cantidad_Comp"))
                    .IdUnidadMedida_Comp = MyBase.UCInt(reader("IdUnidadMedida_Comp"))
                    .Componente = MyBase.UCStr(reader("prod_Nombre"))
                    .CodigoProd_Comp = MyBase.UCStr(reader("prod_Codigo"))
                    .UnidadMedida_Comp = MyBase.UCStr(reader("um_NombreCorto"))

                    .IdMonedaPrecio_Comp = MyBase.UCInt(reader("IdMonedaPrecio_Comp"))
                    .Precio_Comp = MyBase.UCDec(reader("kit_Precio_Comp"))
                    .MonedaPrecio_Comp = MyBase.UCStr(reader("mon_Simbolo"))
                    .PorcentajeKit = MyBase.UCInt(reader("porcentkit"))
                End With

                lista.Add(objKit)

            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (MyBase.Cn.State = ConnectionState.Open) Then MyBase.Cn.Close()
        End Try

        Return lista

    End Function
    'kit 16 -02-2011
    Public Sub Kit_Registrar(ByVal objKit As Entidades.Kit, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(6) {}
        p(0) = MyBase.getParam(objKit.IdKit, "@IdKit", SqlDbType.Int)
        p(1) = MyBase.getParam(objKit.IdComponente, "@IdComponente", SqlDbType.Int)
        p(2) = MyBase.getParam(objKit.Cantidad_Comp, "@kit_Cantidad_Comp", SqlDbType.Decimal)
        p(3) = MyBase.getParam(objKit.IdUnidadMedida_Comp, "@IdUnidadMedida_Comp", SqlDbType.Int)
        p(4) = MyBase.getParam(objKit.Precio_Comp, "@kit_Precio_Comp", SqlDbType.Decimal)
        p(5) = MyBase.getParam(objKit.IdMonedaPrecio_Comp, "@IdMonedaPrecio_Comp", SqlDbType.Int)
        p(6) = MyBase.getParam(objKit.PorcentajeKit, "@porcentajekit", SqlDbType.Int)
        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_Kit_Registrar", p)

    End Sub
    Public Function Kit_DeletexIdKitxIdComponente(ByVal IdKit As Integer, ByVal IdComponente As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean

        Dim p() As SqlParameter = New SqlParameter(1) {}
        p(0) = MyBase.getParam(IdKit, "@IdKit", SqlDbType.Int)
        p(1) = MyBase.getParam(IdComponente, "@IdComponente", SqlDbType.Int)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_Kit_DeletexIdKitxIdComponente", p)

    End Function

End Class
