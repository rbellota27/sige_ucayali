﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

'''  lunes 20 de diciembre

Public Class DAOSubLinea_TipoTabla
    Dim Helper As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function GrabaSubLineaTipoTabla(ByVal lista As List(Of Entidades.SubLinea_TipoTabla), ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Try
            For i As Integer = 0 To lista.Count - 1
                Dim ArrayParametros() As SqlParameter = New SqlParameter(6) {}
                ArrayParametros(0) = New SqlParameter("@IdLinea", SqlDbType.Int)
                ArrayParametros(0).Value = IIf(IsDBNull(lista.Item(i).IdLinea) = True, 0, lista.Item(i).IdLinea)
                ArrayParametros(1) = New SqlParameter("@IdSubLinea", SqlDbType.Int)
                ArrayParametros(1).Value = IIf(IsDBNull(lista.Item(i).IdSubLinea) = True, 0, lista.Item(i).IdSubLinea)
                ArrayParametros(2) = New SqlParameter("@IdTipoTabla", SqlDbType.Int)
                ArrayParametros(2).Value = IIf(IsDBNull(lista.Item(i).IdTipoTabla) = True, 0, lista.Item(i).IdTipoTabla)
                ArrayParametros(3) = New SqlParameter("@sltt_TipoUso", SqlDbType.Char, 2)
                ArrayParametros(3).Value = IIf(IsDBNull(lista.Item(i).TipoUso) = True, "", lista.Item(i).TipoUso)
                ArrayParametros(4) = New SqlParameter("@sltt_NroOrden", SqlDbType.TinyInt)
                ArrayParametros(4).Value = IIf(IsDBNull(lista.Item(i).NroOrden) = True, 0, lista.Item(i).NroOrden)
                ArrayParametros(5) = New SqlParameter("@sltt_ParteNombre", SqlDbType.Bit)
                ArrayParametros(5).Value = IIf(IsDBNull(lista.Item(i).ParteNombre) = True, 0, lista.Item(i).ParteNombre)
                ArrayParametros(6) = New SqlParameter("@sltt_Estado", SqlDbType.Bit)
                ArrayParametros(6).Value = IIf(IsDBNull(lista.Item(i).Estado) = True, 0, lista.Item(i).Estado)

                Dim cmd As New SqlCommand("InsUpd_SubLineaTipoTabla", cn, tr)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(ArrayParametros)
                cmd.ExecuteNonQuery()
            Next
            Return True
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function AnularSubLineaTipoTabla(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Dim ArrayParametros() As SqlParameter = New SqlParameter(0) {}
        'ArrayParametros(0) = New SqlParameter("@IdLinea", SqlDbType.Int)
        'ArrayParametros(0).Value = idlinea
        ArrayParametros(0) = New SqlParameter("@IdSubLinea", SqlDbType.Int)
        ArrayParametros(0).Value = idsublinea
        Dim cmd As New SqlCommand("_SubLineaTipoTablaAnular", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
        Return True
    End Function

    Public Function TipoTablaSublineaConFig_Paginado(ByVal idtipoexist As Integer, ByVal idlinea As Integer, _
                                                     ByVal idsublinea As Integer, ByVal estado As Integer, _
                                                     ByVal PageIndex As Integer, ByVal PageSize As Integer) As List(Of Entidades.SubLinea_TipoTabla)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TipoTablaSublineaConFig_Paginado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idtipoexist)
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        cmd.Parameters.AddWithValue("@Estado", estado)
        cmd.Parameters.AddWithValue("@PageIndex", PageIndex)
        cmd.Parameters.AddWithValue("@PageSize", PageSize)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea_TipoTabla)
                Do While lector.Read
                    Dim obj As New Entidades.SubLinea_TipoTabla
                    obj.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    obj.NomLinea = CStr(IIf(IsDBNull(lector.Item("Linea")) = True, "", lector.Item("Linea")))
                    obj.IdSubLinea = CInt(IIf(IsDBNull(lector.Item("IdSubLinea")) = True, 0, lector.Item("IdSubLinea")))
                    obj.NomSubLinea = CStr(IIf(IsDBNull(lector.Item("SubLinea")) = True, "", lector.Item("SubLinea")))
                    obj.Nombre = LTrim(RTrim(CStr(IIf(IsDBNull(lector.Item("Tabla")) = True, "", lector.Item("Tabla")))))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("Estado")) = True, 0, lector.Item("Estado")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxTipoExistxIdLineaxIdSubLineaxEstado(ByVal idtipoexist As Integer, ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal estado As Integer) As List(Of Entidades.SubLinea_TipoTabla)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SubLinea_TipoTablaSelectAllxTipoExistxIdLineaxIdSubLineaxEstado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idtipoexist)
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        cmd.Parameters.AddWithValue("@Estado", estado)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea_TipoTabla)
                Do While lector.Read
                    Dim obj As New Entidades.SubLinea_TipoTabla
                    obj.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    obj.NomLinea = CStr(IIf(IsDBNull(lector.Item("Linea")) = True, "", lector.Item("Linea")))
                    obj.IdSubLinea = CInt(IIf(IsDBNull(lector.Item("IdSubLinea")) = True, 0, lector.Item("IdSubLinea")))
                    obj.NomSubLinea = CStr(IIf(IsDBNull(lector.Item("SubLinea")) = True, "", lector.Item("SubLinea")))
                    obj.Nombre = LTrim(RTrim(CStr(IIf(IsDBNull(lector.Item("Tabla")) = True, "", lector.Item("Tabla")))))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("Estado")) = True, 0, lector.Item("Estado")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxIdTipoExistenciaxIdLineaxIdSubLinea(ByVal idlinea As Integer, ByVal idsublinea As Integer) As List(Of Entidades.SubLinea_TipoTabla)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SubLinea_TipoTablaSelectxIdTipoExistenciaxIdLineaxIdSubLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea_TipoTabla)
                Do While lector.Read
                    Dim obj As New Entidades.SubLinea_TipoTabla
                    obj.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    obj.IdSubLinea = CInt(IIf(IsDBNull(lector.Item("IdSubLinea")) = True, 0, lector.Item("IdSubLinea")))
                    obj.IdTipoTabla = CInt(IIf(IsDBNull(lector.Item("IdTipoTabla")) = True, 0, lector.Item("IdTipoTabla")))
                    obj.Nombre = LTrim(RTrim(CStr(IIf(IsDBNull(lector.Item("TipoTabla")) = True, "", lector.Item("TipoTabla")))))
                    obj.TipoUso = CStr(IIf(IsDBNull(lector.Item("TipoUso")) = True, "", lector.Item("TipoUso")))
                    obj.NomTipoUso = CStr(IIf(IsDBNull(lector.Item("NomTipoUso")) = True, "", lector.Item("NomTipoUso")))
                    obj.ParteNombre = CBool(IIf(IsDBNull(lector.Item("ParteNombre")) = True, 0, lector.Item("ParteNombre")))
                    obj.NroOrden = CInt(IIf(IsDBNull(lector.Item("NroOrden")) = True, 0, lector.Item("NroOrden")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("Estado")) = True, 0, lector.Item("Estado")))
                    obj.Indicador = CBool(IIf(IsDBNull(lector.Item("Indicador")) = True, 0, lector.Item("Indicador")))
                    obj.Longitud = CInt(IIf(IsDBNull(lector.Item("Longitud")) = True, "", lector.Item("Longitud")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxIdLineaxIdSubLinea(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal tipouso As String) As List(Of Entidades.SubLinea_TipoTabla)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SubLinea_TipoTablaSelectxIdLineaxIdSubLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        cmd.Parameters.AddWithValue("@TipoUso", tipouso)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea_TipoTabla)
                Do While lector.Read
                    Dim obj As New Entidades.SubLinea_TipoTabla
                    obj.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    obj.IdSubLinea = CInt(IIf(IsDBNull(lector.Item("IdSubLinea")) = True, 0, lector.Item("IdSubLinea")))
                    obj.IdTipoTabla = CInt(IIf(IsDBNull(lector.Item("IdTipoTabla")) = True, 0, lector.Item("IdTipoTabla")))
                    obj.Nombre = LTrim(RTrim(CStr(IIf(IsDBNull(lector.Item("TipoTabla")) = True, "", lector.Item("TipoTabla")))))
                    obj.TipoUso = CStr(IIf(IsDBNull(lector.Item("TipoUso")) = True, "", lector.Item("TipoUso")))
                    obj.NomTipoUso = CStr(IIf(IsDBNull(lector.Item("NomTipoUso")) = True, "", lector.Item("NomTipoUso")))
                    obj.ParteNombre = CBool(IIf(IsDBNull(lector.Item("ParteNombre")) = True, 0, lector.Item("ParteNombre")))
                    obj.NroOrden = CInt(IIf(IsDBNull(lector.Item("NroOrden")) = True, 0, lector.Item("NroOrden")))
                    'obj.Estado = CBool(IIf(IsDBNull(lector.Item("Estado")) = True, 0, lector.Item("Estado")))
                    obj.Indicador = False
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAllTipoTablaxIdSubLinea(ByVal idsublinea As Integer) As List(Of Entidades.SubLinea_TipoTabla)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SubLineaTipoTablaSelectAllxIdSubLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea_TipoTabla)
                Do While lector.Read
                    Dim obj As New Entidades.SubLinea_TipoTabla
                    obj.IdTipoTabla = CInt(IIf(IsDBNull(lector.Item("IdTipoTabla")) = True, 0, lector.Item("IdTipoTabla")))
                    obj.Nombre = LTrim(RTrim(CStr(IIf(IsDBNull(lector.Item("tt_Nombre")) = True, "", lector.Item("tt_Nombre")))))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAllTipoTablaxIdSubLinea2(ByVal idsublinea As Integer) As List(Of Entidades.SubLinea_TipoTabla)
        Dim cn As SqlConnection = objConexion.ConexionSIGE2
        Dim cmd As New SqlCommand("_SubLineaTipoTablaSelectAllxIdSubLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea_TipoTabla)
                Do While lector.Read
                    Dim obj As New Entidades.SubLinea_TipoTabla
                    obj.IdTipoTabla = CInt(IIf(IsDBNull(lector.Item("IdTipoTabla")) = True, 0, lector.Item("IdTipoTabla")))
                    obj.Nombre = LTrim(RTrim(CStr(IIf(IsDBNull(lector.Item("tt_Nombre")) = True, "", lector.Item("tt_Nombre")))))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectxIdLineaxIdSubLineaxIdProducto(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal tipouso As String, ByVal idproducto As Integer) As List(Of Entidades.SubLinea_TipoTabla)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_SubLinea_TipoTablaSelectxIdLineaxIdSubLineaxChk", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdSubLinea", idsublinea)
        cmd.Parameters.AddWithValue("@TipoUso", tipouso)
        cmd.Parameters.AddWithValue("@IdProducto", idproducto)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.SubLinea_TipoTabla)
                Do While lector.Read
                    Dim obj As New Entidades.SubLinea_TipoTabla

                    obj.IdTipoTabla = CInt(IIf(IsDBNull(lector.Item("IdTipoTabla")) = True, 0, lector.Item("IdTipoTabla")))
                    obj.Nombre = LTrim(RTrim(CStr(IIf(IsDBNull(lector.Item("TipoTabla")) = True, "", lector.Item("TipoTabla")))))
                    obj.Indicador = CBool(IIf(IsDBNull(lector.Item("Estado")) = True, 0, lector.Item("Estado")))
                    obj.Estado = CBool(IIf(IsDBNull(lector.Item("Estado")) = True, 0, lector.Item("Estado")))
                    obj.Codigo = LTrim(RTrim(CStr(IIf(IsDBNull(lector.Item("Codigo")) = True, "", lector.Item("Codigo")))))
                    'obj.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    'obj.IdSubLinea = CInt(IIf(IsDBNull(lector.Item("IdSubLinea")) = True, 0, lector.Item("IdSubLinea")))
                    'obj.NroOrden = CInt(IIf(IsDBNull(lector.Item("NroOrden")) = True, 0, lector.Item("NroOrden")))
                    'obj.TipoUso = CStr(IIf(IsDBNull(lector.Item("TipoUso")) = True, "", lector.Item("TipoUso")))
                    'obj.NomTipoUso = CStr(IIf(IsDBNull(lector.Item("NomTipoUso")) = True, "", lector.Item("NomTipoUso")))
                    'obj.ParteNombre = CBool(IIf(IsDBNull(lector.Item("ParteNombre")) = True, 0, lector.Item("ParteNombre")))

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
