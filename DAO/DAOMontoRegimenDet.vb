﻿
Imports System.Data.SqlClient

Public Class DAOMontoRegimenDet
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

    Public Sub InsertaMontoRegimenDet(ByVal cn As SqlConnection, ByVal montoregimen As Entidades.be_MontoRegimenDet, ByVal T As SqlTransaction)

        Dim ArrayParametros() As SqlParameter = New SqlParameter(6) {}

        ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = montoregimen.IdDocumento
        ArrayParametros(1) = New SqlParameter("@IdConcepto", SqlDbType.Int)
        ArrayParametros(1).Value = montoregimen.IdServicio
        ArrayParametros(2) = New SqlParameter("@IdTipoOperacion", SqlDbType.Int)
        ArrayParametros(2).Value = montoregimen.IdOperacion
        ArrayParametros(3) = New SqlParameter("@Serie_Doc", SqlDbType.VarChar)
        ArrayParametros(3).Value = montoregimen.Serie
        ArrayParametros(4) = New SqlParameter("@Anexo_Doc", SqlDbType.VarChar)
        ArrayParametros(4).Value = montoregimen.Codigo
        ArrayParametros(5) = New SqlParameter("@MontoDetracSunat", SqlDbType.Decimal)
        ArrayParametros(5).Value = montoregimen.MontoDetraccion
        ArrayParametros(6) = New SqlParameter("@IdEstado", SqlDbType.Int)
        ArrayParametros(6).Value = montoregimen.EstadoDet

        HDAO.InsertaT(cn, "SP_UpdateMontoRetencion", ArrayParametros, T)

    End Sub


End Class
