'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

' Modificado 25/10/2010

Imports System.Data.SqlClient

Public Class DAOLinea
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Private cn As SqlConnection
    Private cmd As SqlCommand
    Private lector As SqlDataReader
    'Public Function InsertaLinea(ByVal linea As Entidades.Linea) As Boolean
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim ArrayParametros() As SqlParameter = New SqlParameter(9) {}
    '    ArrayParametros(0) = New SqlParameter("@lin_Nombre", SqlDbType.VarChar)
    '    ArrayParametros(0).Value = IIf(linea.Descripcion = Nothing, DBNull.Value, linea.Descripcion)
    '    ArrayParametros(1) = New SqlParameter("@lin_Estado", SqlDbType.Char)
    '    ArrayParametros(1).Value = IIf(linea.Estado = Nothing, DBNull.Value, linea.Estado)
    '    ArrayParametros(2) = New SqlParameter("@IdTipoExistencia", SqlDbType.Int)
    '    ArrayParametros(2).Value = linea.IdTipoExistencia
    '    ArrayParametros(3) = New SqlParameter("@lin_CtaDebeCompra", SqlDbType.VarChar)
    '    ArrayParametros(3).Value = IIf(linea.CtaDebeCompra = Nothing, DBNull.Value, linea.CtaDebeCompra)
    '    ArrayParametros(4) = New SqlParameter("@lin_CtaHaberCompra", SqlDbType.VarChar)
    '    ArrayParametros(4).Value = IIf(linea.CtaHaberCompra = Nothing, DBNull.Value, linea.CtaHaberCompra)
    '    ArrayParametros(5) = New SqlParameter("@lin_CostoCompra", SqlDbType.VarChar)
    '    ArrayParametros(5).Value = IIf(linea.CostoCompra = Nothing, DBNull.Value, linea.CostoCompra)
    '    ArrayParametros(6) = New SqlParameter("@lin_CtaDebeVenta", SqlDbType.VarChar)
    '    ArrayParametros(6).Value = IIf(linea.CtaDebeVenta = Nothing, DBNull.Value, linea.CtaDebeVenta)
    '    ArrayParametros(7) = New SqlParameter("@lin_CtaHaberVenta", SqlDbType.VarChar)
    '    ArrayParametros(7).Value = IIf(linea.CtaHaberVenta = Nothing, DBNull.Value, linea.CtaHaberVenta)
    '    ArrayParametros(8) = New SqlParameter("@lin_CostoVenta", SqlDbType.VarChar)
    '    ArrayParametros(8).Value = IIf(linea.CostoVenta = Nothing, DBNull.Value, linea.CostoVenta)
    '    ArrayParametros(9) = New SqlParameter("@CodigoAntiguo", SqlDbType.VarChar)
    '    ArrayParametros(9).Value = IIf(linea.CodigoAntiguo = Nothing, DBNull.Value, linea.CodigoAntiguo)
    '    Return HDAO.Insert(cn, "_LineaInsert", ArrayParametros)
    'End Function
    'Public Function ActualizaLinea(ByVal linea As Entidades.Linea) As Boolean
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim ArrayParametros() As SqlParameter = New SqlParameter(10) {}

    '    ArrayParametros(0) = New SqlParameter("@lin_Nombre", SqlDbType.VarChar)
    '    ArrayParametros(0).Value = IIf(linea.Descripcion = Nothing, DBNull.Value, linea.Descripcion)
    '    ArrayParametros(1) = New SqlParameter("@lin_Estado", SqlDbType.Char)
    '    ArrayParametros(1).Value = IIf(linea.Estado = Nothing, DBNull.Value, linea.Estado)
    '    ArrayParametros(2) = New SqlParameter("@IdTipoExistencia", SqlDbType.Int)
    '    ArrayParametros(2).Value = linea.IdTipoExistencia
    '    ArrayParametros(3) = New SqlParameter("@lin_CtaDebeCompra", SqlDbType.VarChar)
    '    ArrayParametros(3).Value = IIf(linea.CtaDebeCompra = Nothing, DBNull.Value, linea.CtaDebeCompra)
    '    ArrayParametros(4) = New SqlParameter("@lin_CtaHaberCompra", SqlDbType.VarChar)
    '    ArrayParametros(4).Value = IIf(linea.CtaHaberCompra = Nothing, DBNull.Value, linea.CtaHaberCompra)
    '    ArrayParametros(5) = New SqlParameter("@lin_CostoCompra", SqlDbType.VarChar)
    '    ArrayParametros(5).Value = IIf(linea.CostoCompra = Nothing, DBNull.Value, linea.CostoCompra)
    '    ArrayParametros(6) = New SqlParameter("@lin_CtaDebeVenta", SqlDbType.VarChar)
    '    ArrayParametros(6).Value = IIf(linea.CtaDebeVenta = Nothing, DBNull.Value, linea.CtaDebeVenta)
    '    ArrayParametros(7) = New SqlParameter("@lin_CtaHaberVenta", SqlDbType.VarChar)
    '    ArrayParametros(7).Value = IIf(linea.CtaHaberVenta = Nothing, DBNull.Value, linea.CtaHaberVenta)
    '    ArrayParametros(8) = New SqlParameter("@lin_CostoVenta", SqlDbType.VarChar)
    '    ArrayParametros(8).Value = IIf(linea.CostoVenta = Nothing, DBNull.Value, linea.CostoVenta)
    '    ArrayParametros(9) = New SqlParameter("@IdLinea", SqlDbType.Int)
    '    ArrayParametros(9).Value = linea.Id
    '    ArrayParametros(10) = New SqlParameter("@CodigoAntiguo", SqlDbType.VarChar)
    '    ArrayParametros(10).Value = IIf(linea.CodigoAntiguo = Nothing, DBNull.Value, linea.CodigoAntiguo)
    '    Return HDAO.Update(cn, "_LineaUpdate", ArrayParametros)
    'End Function
    Public Function InsertaLinea(ByVal linea As Entidades.Linea, ByVal cn As SqlConnection, Optional ByVal tr As SqlTransaction = Nothing) As Integer
        Dim ArrayParametros() As SqlParameter = New SqlParameter(22) {}
        ArrayParametros(0) = New SqlParameter("@lin_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(linea.Descripcion = Nothing, DBNull.Value, linea.Descripcion)
        ArrayParametros(1) = New SqlParameter("@lin_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = IIf(linea.Estado = Nothing, DBNull.Value, linea.Estado)
        ArrayParametros(2) = New SqlParameter("@IdTipoExistencia", SqlDbType.Int)
        ArrayParametros(2).Value = linea.IdTipoExistencia
        ArrayParametros(3) = New SqlParameter("@lin_CtaDebeCompra", SqlDbType.VarChar)
        ArrayParametros(3).Value = IIf(linea.CtaDebeCompra = Nothing, DBNull.Value, linea.CtaDebeCompra)
        ArrayParametros(4) = New SqlParameter("@lin_CtaHaberCompra", SqlDbType.VarChar)
        ArrayParametros(4).Value = IIf(linea.CtaHaberCompra = Nothing, DBNull.Value, linea.CtaHaberCompra)
        ArrayParametros(5) = New SqlParameter("@lin_CostoCompra", SqlDbType.VarChar)
        ArrayParametros(5).Value = IIf(linea.CostoCompra = Nothing, DBNull.Value, linea.CostoCompra)
        ArrayParametros(6) = New SqlParameter("@lin_CtaDebeVenta", SqlDbType.VarChar)
        ArrayParametros(6).Value = IIf(linea.CtaDebeVenta = Nothing, DBNull.Value, linea.CtaDebeVenta)
        ArrayParametros(7) = New SqlParameter("@lin_CtaHaberVenta", SqlDbType.VarChar)
        ArrayParametros(7).Value = IIf(linea.CtaHaberVenta = Nothing, DBNull.Value, linea.CtaHaberVenta)
        ArrayParametros(8) = New SqlParameter("@lin_CostoVenta", SqlDbType.VarChar)
        ArrayParametros(8).Value = IIf(linea.CostoVenta = Nothing, DBNull.Value, linea.CostoVenta)

        ArrayParametros(9) = New SqlParameter("@FlagPais", SqlDbType.Bit)
        ArrayParametros(9).Value = IIf(linea.FlagPais = Nothing, DBNull.Value, linea.FlagPais)
        ArrayParametros(10) = New SqlParameter("@FlagFabricante", SqlDbType.Bit)
        ArrayParametros(10).Value = IIf(linea.FlagFabricante = Nothing, DBNull.Value, linea.FlagFabricante)
        ArrayParametros(11) = New SqlParameter("@FlagMarca", SqlDbType.Bit)
        ArrayParametros(11).Value = IIf(linea.FlagMarca = Nothing, DBNull.Value, linea.FlagMarca)
        ArrayParametros(12) = New SqlParameter("@FlagModelo", SqlDbType.Bit)
        ArrayParametros(12).Value = IIf(linea.FlagModelo = Nothing, DBNull.Value, linea.FlagModelo)
        ArrayParametros(13) = New SqlParameter("@FlagFormato", SqlDbType.Bit)
        ArrayParametros(13).Value = IIf(linea.FlagFormato = Nothing, DBNull.Value, linea.FlagFormato)
        ArrayParametros(14) = New SqlParameter("@FlagProveedor", SqlDbType.Bit)
        ArrayParametros(14).Value = IIf(linea.FlagProveedor = Nothing, DBNull.Value, linea.FlagProveedor)
        ArrayParametros(15) = New SqlParameter("@FlagEstilo", SqlDbType.Bit)
        ArrayParametros(15).Value = IIf(linea.FlagEstilo = Nothing, DBNull.Value, linea.FlagEstilo)
        ArrayParametros(16) = New SqlParameter("@FlagTransito", SqlDbType.Bit)
        ArrayParametros(16).Value = IIf(linea.FlagTransito = Nothing, DBNull.Value, linea.FlagTransito)
        ArrayParametros(17) = New SqlParameter("@FlagCalidad", SqlDbType.Bit)
        ArrayParametros(17).Value = IIf(linea.FlagCalidad = Nothing, DBNull.Value, linea.FlagCalidad)

        ArrayParametros(18) = New SqlParameter("@FlagColor", SqlDbType.Bit)
        ArrayParametros(18).Value = IIf(linea.FlagColor = Nothing, DBNull.Value, linea.FlagColor)

        ArrayParametros(19) = New SqlParameter("@CodigoAntiguo", SqlDbType.VarChar)
        ArrayParametros(19).Value = IIf(linea.CodigoAntiguo = Nothing, DBNull.Value, linea.CodigoAntiguo)
        ArrayParametros(20) = New SqlParameter("@lin_Orden", SqlDbType.Int)
        ArrayParametros(20).Value = IIf(linea.Orden = Nothing, DBNull.Value, linea.Orden)

        ArrayParametros(21) = New SqlParameter("@lin_Longitud", SqlDbType.TinyInt)
        ArrayParametros(21).Value = IIf(linea.Longitud = Nothing, DBNull.Value, linea.Longitud)

        ArrayParametros(22) = New SqlParameter("@IdLinea", SqlDbType.Int)
        ArrayParametros(22).Direction = ParameterDirection.Output
        ArrayParametros(22).Value = 0
        If tr IsNot Nothing Then
            Return HDAO.InsertaTParameterOutPut(cn, "_LineaInsert", ArrayParametros, tr)
        Else
            Return HDAO.Insert_ReturnParameter(cn, "_LineaInsert", ArrayParametros)
        End If
    End Function
    Public Function ActualizaLinea(ByVal objLinea As Entidades.Linea, ByVal cn As SqlConnection, Optional ByVal tr As SqlTransaction = Nothing) As Boolean
        Try
            Dim cmd As New SqlCommand("_LineaUpdate", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@lin_Nombre", objLinea.Descripcion)

            cmd.Parameters.AddWithValue("@lin_Estado", objLinea.Estado)
            cmd.Parameters.AddWithValue("@IdTipoExistencia", objLinea.IdTipoExistencia)
            cmd.Parameters.AddWithValue("@lin_CtaDebeCompra", objLinea.CtaDebeCompra)
            cmd.Parameters.AddWithValue("@lin_CtaHaberCompra", objLinea.CtaHaberCompra)
            cmd.Parameters.AddWithValue("@lin_CostoCompra", objLinea.CostoCompra)
            cmd.Parameters.AddWithValue("@lin_CtaDebeVenta", objLinea.CtaDebeVenta)
            cmd.Parameters.AddWithValue("@lin_CtaHaberVenta", objLinea.CtaHaberVenta)
            cmd.Parameters.AddWithValue("@lin_CostoVenta", objLinea.CostoVenta)
            cmd.Parameters.AddWithValue("@IdLinea", objLinea.Id)
            cmd.Parameters.AddWithValue("@FlagPais", objLinea.FlagPais)
            cmd.Parameters.AddWithValue("@FlagFabricante", objLinea.FlagFabricante)
            cmd.Parameters.AddWithValue("@FlagMarca", objLinea.FlagMarca)
            cmd.Parameters.AddWithValue("@FlagModelo", objLinea.FlagModelo)
            cmd.Parameters.AddWithValue("@FlagFormato", objLinea.FlagFormato)
            cmd.Parameters.AddWithValue("@FlagProveedor", objLinea.FlagProveedor)
            cmd.Parameters.AddWithValue("@FlagEstilo", objLinea.FlagEstilo)
            cmd.Parameters.AddWithValue("@FlagTransito", objLinea.FlagTransito)
            cmd.Parameters.AddWithValue("@FlagCalidad", objLinea.FlagCalidad)
            cmd.Parameters.AddWithValue("@FlagColor", objLinea.FlagColor)
            cmd.Parameters.AddWithValue("@CodigoAntiguo", objLinea.CodigoAntiguo)
            cmd.Parameters.AddWithValue("@lin_Orden", objLinea.Orden)
            cmd.Parameters.AddWithValue("@lin_Longitud", objLinea.Longitud)
            If cmd.ExecuteNonQuery = 0 Then
                Return False
            End If
            Return True
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAll() As List(Of Entidades.Linea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_LineaSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Linea)
                Do While lector.Read
                    Dim linea As New Entidades.Linea
                    linea.Id = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    linea.Descripcion = CStr(IIf(IsDBNull(lector.Item("lin_Nombre")) = True, "", lector.Item("lin_Nombre")))
                    linea.Estado = CStr(IIf(IsDBNull(lector.Item("lin_Estado")) = True, "", lector.Item("lin_Estado")))
                    linea.CostoCompra = CStr(IIf(IsDBNull(lector.Item("lin_CostoCompra")) = True, "", lector.Item("lin_CostoCompra")))
                    linea.CostoVenta = CStr(IIf(IsDBNull(lector.Item("lin_CostoVenta")) = True, "", lector.Item("lin_CostoVenta")))
                    linea.CtaDebeCompra = CStr(IIf(IsDBNull(lector.Item("lin_CtaDebeCompra")) = True, "", lector.Item("lin_CtaDebeCompra")))
                    linea.CtaDebeVenta = CStr(IIf(IsDBNull(lector.Item("lin_CtaDebeVenta")) = True, "", lector.Item("lin_CtaDebeVenta")))
                    linea.CtaHaberCompra = CStr(IIf(IsDBNull(lector.Item("lin_CtaHaberCompra")) = True, "", lector.Item("lin_CtaHaberCompra")))
                    linea.CtaHaberVenta = CStr(IIf(IsDBNull(lector.Item("lin_CtaHaberVenta")) = True, "", lector.Item("lin_CtaHaberVenta")))
                    linea.IdTipoExistencia = CInt(IIf(IsDBNull(lector.Item("IdTipoExistencia")) = True, 0, lector.Item("IdTipoExistencia")))
                    Lista.Add(linea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Linea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_LineaSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Linea)
                Do While lector.Read
                    Dim linea As New Entidades.Linea
                    linea.Id = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    linea.Descripcion = CStr(IIf(IsDBNull(lector.Item("lin_Nombre")) = True, "", lector.Item("lin_Nombre")))
                    linea.Estado = CStr(IIf(IsDBNull(lector.Item("lin_Estado")) = True, "", lector.Item("lin_Estado")))
                    linea.CostoCompra = CStr(IIf(IsDBNull(lector.Item("lin_CostoCompra")) = True, "", lector.Item("lin_CostoCompra")))
                    linea.CostoVenta = CStr(IIf(IsDBNull(lector.Item("lin_CostoVenta")) = True, "", lector.Item("lin_CostoVenta")))
                    linea.CtaDebeCompra = CStr(IIf(IsDBNull(lector.Item("lin_CtaDebeCompra")) = True, "", lector.Item("lin_CtaDebeCompra")))
                    linea.CtaDebeVenta = CStr(IIf(IsDBNull(lector.Item("lin_CtaDebeVenta")) = True, "", lector.Item("lin_CtaDebeVenta")))
                    linea.CtaHaberCompra = CStr(IIf(IsDBNull(lector.Item("lin_CtaHaberCompra")) = True, "", lector.Item("lin_CtaHaberCompra")))
                    linea.CtaHaberVenta = CStr(IIf(IsDBNull(lector.Item("lin_CtaHaberVenta")) = True, "", lector.Item("lin_CtaHaberVenta")))
                    linea.IdTipoExistencia = CInt(IIf(IsDBNull(lector.Item("IdTipoExistencia")) = True, 0, lector.Item("IdTipoExistencia")))
                    Lista.Add(linea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Linea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_LineaSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Linea)
                Do While lector.Read
                    Dim linea As New Entidades.Linea
                    linea.Id = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    linea.Descripcion = CStr(IIf(IsDBNull(lector.Item("lin_Nombre")) = True, "", lector.Item("lin_Nombre")))
                    linea.Estado = CStr(IIf(IsDBNull(lector.Item("lin_Estado")) = True, "", lector.Item("lin_Estado")))
                    linea.CostoCompra = CStr(IIf(IsDBNull(lector.Item("lin_CostoCompra")) = True, "", lector.Item("lin_CostoCompra")))
                    linea.CostoVenta = CStr(IIf(IsDBNull(lector.Item("lin_CostoVenta")) = True, "", lector.Item("lin_CostoVenta")))
                    linea.CtaDebeCompra = CStr(IIf(IsDBNull(lector.Item("lin_CtaDebeCompra")) = True, "", lector.Item("lin_CtaDebeCompra")))
                    linea.CtaDebeVenta = CStr(IIf(IsDBNull(lector.Item("lin_CtaDebeVenta")) = True, "", lector.Item("lin_CtaDebeVenta")))
                    linea.CtaHaberCompra = CStr(IIf(IsDBNull(lector.Item("lin_CtaHaberCompra")) = True, "", lector.Item("lin_CtaHaberCompra")))
                    linea.CtaHaberVenta = CStr(IIf(IsDBNull(lector.Item("lin_CtaHaberVenta")) = True, "", lector.Item("lin_CtaHaberVenta")))
                    linea.IdTipoExistencia = CInt(IIf(IsDBNull(lector.Item("IdTipoExistencia")) = True, 0, lector.Item("IdTipoExistencia")))
                    Lista.Add(linea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxTipoExistencia(ByVal idTipoEx As Integer) As List(Of Entidades.Linea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_LineaSelectActivoxTipoExistencia", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoEx", idTipoEx)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Linea)
                Do While lector.Read
                    Dim linea As New Entidades.Linea
                    linea.Id = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    linea.Descripcion = CStr(IIf(IsDBNull(lector.Item("lin_Nombre")) = True, "", lector.Item("lin_Nombre")))
                    linea.Estado = CStr(IIf(IsDBNull(lector.Item("lin_Estado")) = True, "", lector.Item("lin_Estado")))
                    linea.CostoCompra = CStr(IIf(IsDBNull(lector.Item("lin_CostoCompra")) = True, "", lector.Item("lin_CostoCompra")))
                    linea.CostoVenta = CStr(IIf(IsDBNull(lector.Item("lin_CostoVenta")) = True, "", lector.Item("lin_CostoVenta")))
                    linea.CtaDebeCompra = CStr(IIf(IsDBNull(lector.Item("lin_CtaDebeCompra")) = True, "", lector.Item("lin_CtaDebeCompra")))
                    linea.CtaDebeVenta = CStr(IIf(IsDBNull(lector.Item("lin_CtaDebeVenta")) = True, "", lector.Item("lin_CtaDebeVenta")))
                    linea.CtaHaberCompra = CStr(IIf(IsDBNull(lector.Item("lin_CtaHaberCompra")) = True, "", lector.Item("lin_CtaHaberCompra")))
                    linea.CtaHaberVenta = CStr(IIf(IsDBNull(lector.Item("lin_CtaHaberVenta")) = True, "", lector.Item("lin_CtaHaberVenta")))
                    linea.IdTipoExistencia = CInt(IIf(IsDBNull(lector.Item("IdTipoExistencia")) = True, 0, lector.Item("IdTipoExistencia")))
                    Lista.Add(linea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxIdLineaxEstado(ByVal idlinea As Integer, ByVal estado As String, ByVal idexistencia As Integer) As List(Of Entidades.Linea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_LineaSelectxIdLineaxEstado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@Estado", estado)
        cmd.Parameters.AddWithValue("@tipoext", idexistencia)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Linea)
                Do While lector.Read
                    Dim linea As New Entidades.Linea
                    linea.Id = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    linea.Descripcion = CStr(IIf(IsDBNull(lector.Item("lin_Nombre")) = True, "", lector.Item("lin_Nombre")))
                    linea.Estado = CStr(IIf(IsDBNull(lector.Item("lin_Estado")) = True, "", lector.Item("lin_Estado")))
                    linea.CostoCompra = CStr(IIf(IsDBNull(lector.Item("lin_CostoCompra")) = True, "", lector.Item("lin_CostoCompra")))
                    linea.CostoVenta = CStr(IIf(IsDBNull(lector.Item("lin_CostoVenta")) = True, "", lector.Item("lin_CostoVenta")))
                    linea.CtaDebeCompra = CStr(IIf(IsDBNull(lector.Item("lin_CtaDebeCompra")) = True, "", lector.Item("lin_CtaDebeCompra")))
                    linea.CtaDebeVenta = CStr(IIf(IsDBNull(lector.Item("lin_CtaDebeVenta")) = True, "", lector.Item("lin_CtaDebeVenta")))
                    linea.CtaHaberCompra = CStr(IIf(IsDBNull(lector.Item("lin_CtaHaberCompra")) = True, "", lector.Item("lin_CtaHaberCompra")))
                    linea.CtaHaberVenta = CStr(IIf(IsDBNull(lector.Item("lin_CtaHaberVenta")) = True, "", lector.Item("lin_CtaHaberVenta")))
                    linea.IdTipoExistencia = CInt(IIf(IsDBNull(lector.Item("IdTipoExistencia")) = True, 0, lector.Item("IdTipoExistencia")))
                    linea.CodigoAntiguo = CStr(IIf(IsDBNull(lector.Item("Cod_Antiguo")) = True, "", lector.Item("Cod_Antiguo")))
                    linea.Orden = CInt(IIf(IsDBNull(lector.Item("lin_Orden")) = True, 0, lector.Item("lin_Orden")))
                    linea.TipoExist = CStr(IIf(IsDBNull(lector.Item("TipoExist")) = True, "", lector.Item("TipoExist")))
                    Lista.Add(linea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCboLineaxIdtipoexistencia(ByVal idexistencia As Integer) As List(Of Entidades.Linea)
        objConexion = New DAO.Conexion
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_LineaSelectCboxIdtipoexistencia", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@id", idexistencia)
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Linea)
                Do While lector.Read
                    Dim obj As New Entidades.Linea
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdLinea")), 0, lector.Item("IdLinea")))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("lin_Nombre")), "", lector.Item("lin_Nombre")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function SelectCboLineaxIdtipoexistencia2(ByVal idexistencia As Integer) As List(Of Entidades.Linea)
        objConexion = New DAO.Conexion
        cn = objConexion.ConexionSIGE2
        cmd = New SqlCommand("_LineaSelectCboxIdtipoexistencia", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@id", idexistencia)
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Linea)
                Do While lector.Read
                    Dim obj As New Entidades.Linea
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdLinea")), 0, lector.Item("IdLinea")))
                    obj.Descripcion = CStr(IIf(IsDBNull(lector.Item("lin_Nombre")), "", lector.Item("lin_Nombre")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function SelectFlagActivoxIdLinea(ByVal idlinea As Integer) As Entidades.Linea
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_LineaFlagAllActivoxIdLinea", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim obj As Entidades.Linea = Nothing
                If lector.Read Then
                    obj = New Entidades.Linea
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdLinea")), 0, lector.Item("IdLinea")))
                    obj.FlagPais = CBool(IIf(IsDBNull(lector.Item("FlagPais")), False, lector.Item("FlagPais")))
                    obj.FlagFabricante = CBool(IIf(IsDBNull(lector.Item("FlagFabricante")), False, lector.Item("FlagFabricante")))
                    obj.FlagMarca = CBool(IIf(IsDBNull(lector.Item("FlagMarca")), False, lector.Item("FlagMarca")))
                    obj.FlagModelo = CBool(IIf(IsDBNull(lector.Item("FlagModelo")), False, lector.Item("FlagModelo")))
                    obj.FlagFormato = CBool(IIf(IsDBNull(lector.Item("FlagFormato")), False, lector.Item("FlagFormato")))
                    obj.FlagProveedor = CBool(IIf(IsDBNull(lector.Item("FlagProveedor")), False, lector.Item("FlagProveedor")))
                    obj.FlagEstilo = CBool(IIf(IsDBNull(lector.Item("FlagEstilo")), False, lector.Item("FlagEstilo")))
                    obj.FlagTransito = CBool(IIf(IsDBNull(lector.Item("FlagTransito")), False, lector.Item("FlagTransito")))
                    obj.FlagCalidad = CBool(IIf(IsDBNull(lector.Item("FlagCalidad")), False, lector.Item("FlagCalidad")))
                    obj.FlagColor = CBool(IIf(IsDBNull(lector.Item("FlagColor")), False, lector.Item("FlagColor")))
                    obj.FlagSubSubLinea = CBool(IIf(IsDBNull(lector.Item("FlagSubSubLinea")), False, lector.Item("FlagSubSubLinea")))
                End If
                lector.Close()
                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAtributoxIdLineaxEstado(ByVal idlinea As Integer, ByVal idexistencia As Integer) As List(Of Entidades.Linea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AtributoLineaSelectxIdLineaxEstado", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@tipoext", idexistencia)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Linea)
                Do While lector.Read
                    Dim linea As New Entidades.Linea
                    linea.Id = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    linea.OrdenPais = CStr(IIf(IsDBNull(lector.Item("PaisOrden")) = True, "", lector.Item("PaisOrden")))
                    linea.OrdenFabricante = CStr(IIf(IsDBNull(lector.Item("FablOrden")) = True, "", lector.Item("FablOrden")))
                    linea.OrdenMarca = CStr(IIf(IsDBNull(lector.Item("MarcaOrden")) = True, "", lector.Item("MarcaOrden")))
                    linea.OrdenModelo = CStr(IIf(IsDBNull(lector.Item("ModeloOrden")) = True, "", lector.Item("ModeloOrden")))
                    linea.OrdenFormato = CStr(IIf(IsDBNull(lector.Item("FormatoOrden")) = True, "", lector.Item("FormatoOrden")))
                    linea.OrdenProveedor = CStr(IIf(IsDBNull(lector.Item("ProveedorOrden")) = True, "", lector.Item("ProveedorOrden")))
                    linea.OrdenEstilo = CStr(IIf(IsDBNull(lector.Item("EstiloOrden")) = True, "", lector.Item("EstiloOrden")))
                    linea.OrdenTransito = CStr(IIf(IsDBNull(lector.Item("TransitoOrden")) = True, "", lector.Item("TransitoOrden")))
                    linea.OrdenCalidad = CStr(IIf(IsDBNull(lector.Item("CalidadOrden")) = True, "", lector.Item("CalidadOrden")))
                    linea.OrdenColor = CStr(IIf(IsDBNull(lector.Item("ColorOrden")) = True, "", lector.Item("ColorOrden")))
                    Lista.Add(linea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectValidarIdLineaxProducto(ByVal idlinea As Integer) As Entidades.Linea
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_LineaValidarIdLineaxProducto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim obj As Entidades.Linea = Nothing
                If lector.Read Then
                    obj = New Entidades.Linea
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdLinea")), 0, lector.Item("IdLinea")))
                    obj.FlagPais = CBool(lector.Item("FlagPais"))
                    obj.FlagFabricante = CBool(lector.Item("FlagFabricante"))
                    obj.FlagMarca = CBool(lector.Item("FlagMarca"))
                    obj.FlagModelo = CBool(lector.Item("FlagModelo"))
                    obj.FlagFormato = CBool(lector.Item("FlagFormato"))
                    obj.FlagProveedor = CBool(lector.Item("FlagProveedor"))
                    obj.FlagEstilo = CBool(lector.Item("FlagEstilo"))
                    obj.FlagTransito = CBool(lector.Item("FlagTransito"))
                    obj.FlagCalidad = CBool(lector.Item("FlagCalidad"))
                    obj.FlagColor = CBool(lector.Item("FlagColor"))
                End If
                lector.Close()
                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectActivoxTipoExistenciaCodigo(ByVal idTipoEx As Integer) As List(Of Entidades.Linea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_LineaSelectActivoxTipoExistenciaCodigo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdTipoEx", idTipoEx)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Linea)
                Do While lector.Read
                    Dim linea As New Entidades.Linea
                    linea.Codigo = CStr(IIf(IsDBNull(lector.Item("IdLinea")) = True, "", lector.Item("IdLinea")))
                    linea.Descripcion = CStr(IIf(IsDBNull(lector.Item("lin_Nombre")) = True, "", lector.Item("lin_Nombre")))
                    linea.Estado = CStr(IIf(IsDBNull(lector.Item("lin_Estado")) = True, "", lector.Item("lin_Estado")))
                    linea.CostoCompra = CStr(IIf(IsDBNull(lector.Item("lin_CostoCompra")) = True, "", lector.Item("lin_CostoCompra")))
                    linea.CostoVenta = CStr(IIf(IsDBNull(lector.Item("lin_CostoVenta")) = True, "", lector.Item("lin_CostoVenta")))
                    linea.CtaDebeCompra = CStr(IIf(IsDBNull(lector.Item("lin_CtaDebeCompra")) = True, "", lector.Item("lin_CtaDebeCompra")))
                    linea.CtaDebeVenta = CStr(IIf(IsDBNull(lector.Item("lin_CtaDebeVenta")) = True, "", lector.Item("lin_CtaDebeVenta")))
                    linea.CtaHaberCompra = CStr(IIf(IsDBNull(lector.Item("lin_CtaHaberCompra")) = True, "", lector.Item("lin_CtaHaberCompra")))
                    linea.CtaHaberVenta = CStr(IIf(IsDBNull(lector.Item("lin_CtaHaberVenta")) = True, "", lector.Item("lin_CtaHaberVenta")))
                    linea.IdTipoExistencia = CInt(IIf(IsDBNull(lector.Item("IdTipoExistencia")) = True, 0, lector.Item("IdTipoExistencia")))
                    Lista.Add(linea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    'Public Function SelectValidarIdLineaxProducto(ByVal idlinea As Integer) As Entidades.Linea
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim cmd As New SqlCommand("_LineaValidarIdLineaxProducto", cn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.AddWithValue("@IdLinea", idlinea)
    '    Dim lector As SqlDataReader
    '    Try
    '        Using cn
    '            cn.Open()
    '            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
    '            Dim obj As Entidades.Linea = Nothing
    '            If lector.Read Then
    '                obj = New Entidades.Linea
    '                obj.Id = CInt(IIf(IsDBNull(lector.Item("IdLinea")), 0, lector.Item("IdLinea")))
    '                obj.FlagPais = CBool(lector.Item("FlagPais"))
    '                obj.FlagFabricante = CBool(lector.Item("FlagFabricante"))
    '                obj.FlagMarca = CBool(lector.Item("FlagMarca"))
    '                obj.FlagModelo = CBool(lector.Item("FlagModelo"))
    '                obj.FlagFormato = CBool(lector.Item("FlagFormato"))
    '                obj.FlagProveedor = CBool(lector.Item("FlagProveedor"))
    '                obj.FlagEstilo = CBool(lector.Item("FlagEstilo"))
    '                obj.FlagTransito = CBool(lector.Item("FlagTransito"))
    '                obj.FlagCalidad = CBool(lector.Item("FlagCalidad"))
    '                obj.FlagColor = CBool(lector.Item("FlagColor"))
    '            End If
    '            lector.Close()
    '            Return obj
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Public Function ValidadLongitud() As Entidades.Linea
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_LineaValidadLongitud", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim obj As Entidades.Linea = Nothing
                If lector.Read Then
                    obj = New Entidades.Linea
                    obj.Longitud = CInt(IIf(IsDBNull(lector.Item("lin_Longitud")), 0, lector.Item("lin_Longitud")))
                    obj.Cantidad = CInt(IIf(IsDBNull(lector.Item("Cantidad")), 0, lector.Item("Cantidad")))
                End If
                lector.Close()
                Return obj
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
