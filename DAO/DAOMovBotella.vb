﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



'**************** MIERCOLES 14 JULIO

Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAOMovBotella
    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private cmd As SqlCommand
    Private tr As SqlCommand
    Private objDaoMantenedor As New DAO.DAOMantenedor

    Public Sub Insert(ByVal objMovBotella As Entidades.MovBotella, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(11) {}
        p(0) = objDaoMantenedor.getParam(objMovBotella.IdEmpresa, "@IdEmpresa", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(objMovBotella.IdTienda, "@IdTienda", SqlDbType.Int)
        p(2) = objDaoMantenedor.getParam(objMovBotella.IdUbicacion, "@IdUbicacion", SqlDbType.Int)
        p(3) = objDaoMantenedor.getParam(objMovBotella.FechaMov, "@mbot_FechaMov", SqlDbType.Date)
        p(4) = objDaoMantenedor.getParam(objMovBotella.TipoMov, "@mbot_TipoMov", SqlDbType.VarChar)
        p(5) = objDaoMantenedor.getParam(objMovBotella.IdTipoDocumento, "@IdTipoDocumento", SqlDbType.Int)
        p(6) = objDaoMantenedor.getParam(objMovBotella.NroDocumento, "@mbot_NroDocumento", SqlDbType.VarChar)
        p(7) = objDaoMantenedor.getParam(objMovBotella.Observacion, "@mbot_Observacion", SqlDbType.VarChar)
        p(8) = objDaoMantenedor.getParam(objMovBotella.Vigencia, "@mbot_Vigencia", SqlDbType.Int)
        p(9) = objDaoMantenedor.getParam(objMovBotella.Estado, "@mbot_Estado", SqlDbType.Bit)
        p(10) = objDaoMantenedor.getParam(objMovBotella.IdBotella, "@IdBotella", SqlDbType.Int)
        p(11) = objDaoMantenedor.getParam(objMovBotella.IdUsuarioInsert, "@IdUsuarioInsert", SqlDbType.Int)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_MovBotella_Insert", p)

    End Sub

End Class
