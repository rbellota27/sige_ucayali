'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOMontoRegimen
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Sub InsertaMontoRegimen(ByVal cn As SqlConnection, ByVal montoregimen As Entidades.MontoRegimen, ByVal T As SqlTransaction)
        'Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = montoregimen.IdDocumento
        ArrayParametros(1) = New SqlParameter("@mr_Monto", SqlDbType.Decimal)
        ArrayParametros(1).Value = montoregimen.Monto
        ArrayParametros(2) = New SqlParameter("@ro_Id", SqlDbType.Int)
        ArrayParametros(2).Value = montoregimen.ro_Id

        HDAO.InsertaT(cn, "_MontoRegimenInsert", ArrayParametros, T)

    End Sub
    Public Function ActualizaMontoRegimen(ByVal montoregimen As Entidades.MontoRegimen) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@IdDocumento", SqlDbType.Int)
        ArrayParametros(0).Value = montoregimen.IdDocumento
        ArrayParametros(1) = New SqlParameter("@mr_Monto", SqlDbType.Decimal)
        ArrayParametros(1).Value = montoregimen.Monto
        ArrayParametros(2) = New SqlParameter("@ro_Id", SqlDbType.Int)
        ArrayParametros(2).Value = montoregimen.ro_Id
        Return HDAO.Update(cn, "_MontoRegimenUpdate", ArrayParametros)
    End Function
    Public Function SelectPercepcionxIdDocumento(ByVal IdDocumento As Integer) As Decimal
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MontoRegimenSelectPercepcionxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Monto As Decimal = Nothing
                If lector.Read Then
                    Monto = Decimal.Round(CDec(lector.Item("mr_Monto")), 2)
                End If
                Return Monto
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectDetraccionxIdDocumento(ByVal IdDocumento As Integer) As Decimal
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MontoRegimenSelectDetraccionxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Monto As Decimal = Nothing
                If lector.Read Then
                    Monto = Decimal.Round(CDec(lector.Item("mr_Monto")), 2)
                End If
                Return Monto
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectRetencionxIdDocumento(ByVal IdDocumento As Integer) As Decimal
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MontoRegimenSelectRetencionxIdDocumento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Monto As Decimal = Nothing
                If lector.Read Then
                    Monto = Decimal.Round(CDec(lector.Item("mr_Monto")), 2)
                End If
                Return Monto
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Sub DeletexIdDocumento(ByVal IdDocumento As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As New SqlCommand("_MontoRegimenDeletexIdDocumento", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Sub
End Class
