﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOPos_TipoTarjeta
    Inherits DAOMantenedor

    Public Function SelectxIdPos(ByVal IdPos As Integer) As List(Of Entidades.Pos_TipoTarjeta)
        Cn = objConexion.ConexionSIGE()

        Dim Prm() As SqlParameter = New SqlParameter(0) {}
        Prm(0) = getParam(IdPos, "@IdPos", SqlDbType.Int)

        Dim lector As SqlDataReader
        Try
            Using Cn
                Cn.Open()

                lector = SqlHelper.ExecuteReader(Cn, CommandType.StoredProcedure, "_Pos_TipoTarjetaSelectxIdPos", Prm)

                Dim Lista As New List(Of Entidades.Pos_TipoTarjeta)
                With lector
                    Do While .Read

                        Dim obj As New Entidades.Pos_TipoTarjeta(IdPos, UCInt(.Item("IdTipoTarjeta")))

                        Lista.Add(obj)

                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Sub InsertTListxIdPos(ByVal L As List(Of Entidades.Pos_TipoTarjeta), _
    ByVal IdPos As Integer, _
    Optional ByRef Cnx As SqlConnection = Nothing, _
    Optional ByRef Trx As SqlTransaction = Nothing)

        For i As Integer = 0 To L.Count - 1
            L(i).IdPos = IdPos
            InsertT(L(i), Cnx, Trx)
        Next

    End Sub

    Public Function InsertT(ByVal x As Entidades.Pos_TipoTarjeta, _
 Optional ByRef Cnx As SqlConnection = Nothing, _
 Optional ByRef Trx As SqlTransaction = Nothing) As Integer

        Return QueryT(GetVectorParametros(x, modo_query.Insert), modo_query.Insert, "_Pos_TipoTarjetaInsert", Cnx, Trx)
    End Function

    Public Function DeleteTxIdPos(ByVal IdPos As Integer, _
Optional ByRef Cnx As SqlConnection = Nothing, _
Optional ByRef Trx As SqlTransaction = Nothing) As Integer

        Dim Prm() As SqlParameter = New SqlParameter(0) {}
        Prm(0) = getParam(IdPos, "@IdPos", SqlDbType.Int)
        Return QueryT(Prm, modo_query.Delete, "_Pos_TipoTarjetaDeletexIdPos", Cnx, Trx)
    End Function

    Public Function GetVectorParametros(ByVal x As Entidades.Pos_TipoTarjeta, ByVal op As modo_query) As SqlParameter()

        Dim Prm() As SqlParameter

        Select Case op
            Case modo_query.Insert
                Prm = New SqlParameter(1) {}
                Prm(0) = getParam(x.IdPos, "@IdPos", SqlDbType.Int)
                Prm(1) = getParam(x.IdTipoTarjeta, "@IdTipoTarjeta", SqlDbType.Int)
        End Select

        Return Prm
    End Function
End Class
