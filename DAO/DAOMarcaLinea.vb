﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOMarcaLinea
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectAllActivoxIdLineaIdTipoExistencia(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.MarcaLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MarcaLineaSelectAllActivoxIdLineaIdTipoExistencia", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idtipoexistencia)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MarcaLinea)
                Do While lector.Read
                    Dim objMarcaLinea As New Entidades.MarcaLinea
                    objMarcaLinea.IdMarca = CInt(IIf(IsDBNull(lector.Item("IdMarca")) = True, 0, lector.Item("IdMarca")))
                    objMarcaLinea.NomMarca = CStr(IIf(IsDBNull(lector.Item("NomMarca")) = True, "", lector.Item("NomMarca")))
                    objMarcaLinea.Abv = CStr(IIf(IsDBNull(lector.Item("NomCorto")) = True, "", lector.Item("NomCorto")))
                    objMarcaLinea.IdLinea = CInt(IIf(IsDBNull(lector.Item("IdLinea")) = True, 0, lector.Item("IdLinea")))
                    objMarcaLinea.IdTipoExistencia = CInt(IIf(IsDBNull(lector.Item("IdTipoExistencia")) = True, 0, lector.Item("IdTipoExistencia")))
                    objMarcaLinea.Estado = CBool(IIf(IsDBNull(lector.Item("ml_Estado")) = True, 0, lector.Item("ml_Estado")))
                    Lista.Add(objMarcaLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    ''***************
    Public Function SelectAllActivoxIdLineaIdTipoExistenciav2(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer) As List(Of Entidades.MarcaLinea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_MarcaLineaSelectAllActivoxIdLineaIdTipoExistenciav2", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdLinea", idlinea)
        cmd.Parameters.AddWithValue("@IdTipoExistencia", idtipoexistencia)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.MarcaLinea)
                Do While lector.Read
                    Dim objMarcaLinea As New Entidades.MarcaLinea
                    objMarcaLinea.IdMarcaOrden = CStr(IIf(IsDBNull(lector.Item("IdMarca")) = True, "", lector.Item("IdMarca")))
                    objMarcaLinea.NomMarca = CStr(IIf(IsDBNull(lector.Item("NomMarca")) = True, "", lector.Item("NomMarca")))
                    objMarcaLinea.Abv = CStr(IIf(IsDBNull(lector.Item("NomCorto")) = True, "", lector.Item("NomCorto")))
                    Lista.Add(objMarcaLinea)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function GrabaMarcaLineaT(ByVal IdLinea As Integer, ByVal cn As SqlConnection, ByVal listaMarcaLinea As List(Of Entidades.MarcaLinea), ByVal T As SqlTransaction) As Boolean
        Dim cmd As SqlCommand
        Try
            For i As Integer = 0 To listaMarcaLinea.Count - 1
                If T IsNot Nothing Then
                    cmd = New SqlCommand("InsUpd_MarcaLinea", cn, T)
                Else
                    cmd = New SqlCommand("InsUpd_MarcaLinea", cn)
                End If
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@IdLinea", IdLinea)
                cmd.Parameters.AddWithValue("@IdMarca", listaMarcaLinea.Item(i).IdMarca)
                cmd.Parameters.AddWithValue("@IdTipoExistencia", listaMarcaLinea.Item(i).IdTipoExistencia)
                cmd.Parameters.AddWithValue("@mlEstado", listaMarcaLinea.Item(i).Estado)
                cmd.Parameters.AddWithValue("@mlOrden", listaMarcaLinea.Item(i).Orden)
                Dim cont As Integer = cmd.ExecuteNonQuery
                If cmd.ExecuteNonQuery = 0 Then
                    Return False
                End If
            Next
            Return True
        Catch ex As Exception
            Return False
        Finally

        End Try
    End Function
End Class
