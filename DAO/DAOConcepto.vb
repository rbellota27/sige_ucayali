'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'*********************   LUNES 24052010



Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class DAOConcepto
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion

    Private objDaoMantenedor As New DAO.DAOMantenedor

    'Public Function InsertaConcepto(ByVal concepto As Entidades.Concepto) As Boolean
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
    '    ArrayParametros(0) = New SqlParameter("@con_Nombre", SqlDbType.VarChar)
    '    ArrayParametros(0).Value = concepto.Nombre
    '    ArrayParametros(1) = New SqlParameter("@con_Estado", SqlDbType.Char)
    '    ArrayParametros(1).Value = concepto.Estado
    '    ArrayParametros(2) = New SqlParameter("@con_CuentaContable", SqlDbType.VarChar)
    '    ArrayParametros(2).Value = concepto.CuentaContable
    '    ArrayParametros(3) = New SqlParameter("@con_TipoUso", SqlDbType.Int)
    '    ArrayParametros(3).Value = concepto.TipoUso
    '    Return HDAO.Insert(cn, "_ConceptoInsert", ArrayParametros)
    'End Function
    Public Function InsertaConcepto(ByVal Concepto As Entidades.Concepto, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer
        'Inserta solo Concepto
        Dim ArrayParametros() As SqlParameter = New SqlParameter(8) {}
        ArrayParametros(0) = New SqlParameter("@Con_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(Concepto.Nombre = Nothing, DBNull.Value, Concepto.Nombre)
        ArrayParametros(1) = New SqlParameter("@Con_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = IIf(Concepto.Estado = Nothing, DBNull.Value, Concepto.Estado)
        ArrayParametros(2) = New SqlParameter("@Con_CuentaContable", SqlDbType.VarChar)
        ArrayParametros(2).Value = IIf(Concepto.CuentaContable = Nothing, DBNull.Value, Concepto.CuentaContable)
        ArrayParametros(3) = New SqlParameter("@IdTipoGasto", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(Concepto.con_IdTipoGasto = Nothing, DBNull.Value, Concepto.con_IdTipoGasto)
        ArrayParametros(4) = New SqlParameter("@Con_ConceptoAdelanto", SqlDbType.Bit)
        ArrayParametros(4).Value = IIf(Concepto.ConceptoAdelanto = Nothing, False, Concepto.ConceptoAdelanto)
        ArrayParametros(5) = New SqlParameter("@IdConcepto", SqlDbType.Int)
        ArrayParametros(5).Direction = ParameterDirection.Output
        ArrayParametros(6) = New SqlParameter("@con_PorcentDetraccion", SqlDbType.Decimal)
        ArrayParametros(6).Value = IIf(Concepto.PorcentDetraccion = Nothing, False, Concepto.PorcentDetraccion)
        ArrayParametros(7) = New SqlParameter("@con_MontoMinDetraccion", SqlDbType.Decimal)
        ArrayParametros(7).Value = IIf(Concepto.MontoMinimoDetraccion = Nothing, False, Concepto.MontoMinimoDetraccion)
        ArrayParametros(8) = New SqlParameter("@con_NoAfectoIGV", SqlDbType.Bit)
        ArrayParametros(8).Value = Concepto.con_NoAfectoIGV

        Dim cmd As New SqlCommand("[_ConceptoInsert+]", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return CInt(cmd.Parameters("@IdConcepto").Value)
    End Function
    'Public Function ActualizaConcepto(ByVal concepto As Entidades.Concepto) As Boolean
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
    '    ArrayParametros(0) = New SqlParameter("@con_Nombre", SqlDbType.VarChar)
    '    ArrayParametros(0).Value = concepto.Nombre
    '    ArrayParametros(1) = New SqlParameter("@con_Estado", SqlDbType.Char)
    '    ArrayParametros(1).Value = concepto.Estado
    '    ArrayParametros(2) = New SqlParameter("@con_CuentaContable", SqlDbType.VarChar)
    '    ArrayParametros(2).Value = concepto.CuentaContable
    '    ArrayParametros(3) = New SqlParameter("@con_TipoUso", SqlDbType.Int)
    '    ArrayParametros(3).Value = concepto.TipoUso
    '    ArrayParametros(4) = New SqlParameter("@IdConcepto", SqlDbType.Int)
    '    ArrayParametros(4).Value = concepto.Id
    '    Return HDAO.Update(cn, "_ConceptoUpdate", ArrayParametros)
    'End Function
    Public Function ActualizaConcepto(ByVal Concepto As Entidades.Concepto, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Dim ArrayParametros() As SqlParameter = New SqlParameter(8) {}

        ArrayParametros(0) = New SqlParameter("@Con_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = IIf(Concepto.Nombre = Nothing, DBNull.Value, Concepto.Nombre)
        ArrayParametros(1) = New SqlParameter("@Con_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = IIf(Concepto.Estado = Nothing, DBNull.Value, Concepto.Estado)
        ArrayParametros(2) = New SqlParameter("@Con_CuentaContable", SqlDbType.VarChar)
        ArrayParametros(2).Value = IIf(Concepto.CuentaContable = Nothing, DBNull.Value, Concepto.CuentaContable)
        ArrayParametros(3) = New SqlParameter("@IdTipoGasto", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(Concepto.con_IdTipoGasto = Nothing, DBNull.Value, Concepto.con_IdTipoGasto)
        ArrayParametros(4) = New SqlParameter("@Con_ConceptoAdelanto", SqlDbType.Bit)
        ArrayParametros(4).Value = IIf(Concepto.ConceptoAdelanto = Nothing, False, Concepto.ConceptoAdelanto)
        ArrayParametros(5) = New SqlParameter("@IdConcepto", SqlDbType.Int)
        ArrayParametros(5).Value = IIf(Concepto.Id = Nothing, DBNull.Value, Concepto.Id)
        ArrayParametros(6) = New SqlParameter("@con_PorcentDetraccion", SqlDbType.Decimal)
        ArrayParametros(6).Value = IIf(Concepto.PorcentDetraccion = Nothing, False, Concepto.PorcentDetraccion)
        ArrayParametros(7) = New SqlParameter("@con_MontoMinDetraccion", SqlDbType.Decimal)
        ArrayParametros(7).Value = IIf(Concepto.MontoMinimoDetraccion = Nothing, False, Concepto.MontoMinimoDetraccion)
        ArrayParametros(8) = New SqlParameter("@con_NoAfectoIGV", SqlDbType.Bit)
        ArrayParametros(8).Value = Concepto.con_NoAfectoIGV

        Dim cmd As New SqlCommand("_ConceptoUpdate+", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Try
            If (cmd.ExecuteNonQuery = 0) Then
                Return False
            End If
        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return True
    End Function
    Public Function SelectAll() As List(Of Entidades.Concepto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ConceptoSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Concepto)
                Do While lector.Read
                    Dim obj As New Entidades.Concepto
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdConcepto")) = True, 0, lector.Item("IdConcepto")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("con_Nombre")) = True, "", lector.Item("con_Nombre")))
                    obj.CuentaContable = CStr(IIf(IsDBNull(lector.Item("con_CuentaContable")) = True, "", lector.Item("con_CuentaContable")))
                    obj.con_IdTipoGasto = CInt(IIf(IsDBNull(lector.Item("IdTipoGasto")) = True, 0, lector.Item("IdTipoGasto")))
                    obj.NombreTipoGasto = CStr(IIf(IsDBNull(lector.Item("tg_Nombre")) = True, "", lector.Item("tg_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("con_Estado")) = True, "", lector.Item("con_Estado")))
                    obj.ConceptoAdelanto = CBool(IIf(IsDBNull(lector.Item("Con_ConceptoAdelanto")) = True, 0, lector.Item("Con_ConceptoAdelanto")))
                    obj.PorcentDetraccion = objDaoMantenedor.UCDec(lector("con_PorcentDetraccion"))
                    obj.MontoMinimoDetraccion = objDaoMantenedor.UCDec(lector("con_MontoMinDetraccion"))
                    obj.con_NoAfectoIGV = objDaoMantenedor.UCBool(lector("con_NoAfectoIGV"))

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Concepto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ConceptoSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Concepto)
                Do While lector.Read
                    Dim obj As New Entidades.Concepto
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdConcepto")) = True, 0, lector.Item("IdConcepto")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("con_Nombre")) = True, "", lector.Item("con_Nombre")))
                    obj.CuentaContable = CStr(IIf(IsDBNull(lector.Item("con_CuentaContable")) = True, "", lector.Item("con_CuentaContable")))
                    obj.con_IdTipoGasto = CInt(IIf(IsDBNull(lector.Item("IdTipoGasto")) = True, 0, lector.Item("IdTipoGasto")))
                    obj.NombreTipoGasto = CStr(IIf(IsDBNull(lector.Item("tg_Nombre")) = True, "", lector.Item("tg_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("con_Estado")) = True, "", lector.Item("con_Estado")))
                    obj.ConceptoAdelanto = CBool(IIf(IsDBNull(lector.Item("Con_ConceptoAdelanto")) = True, 0, lector.Item("Con_ConceptoAdelanto")))
                    obj.PorcentDetraccion = objDaoMantenedor.UCDec(lector("con_PorcentDetraccion"))
                    obj.MontoMinimoDetraccion = objDaoMantenedor.UCDec(lector("con_MontoMinDetraccion"))
                    obj.con_NoAfectoIGV = objDaoMantenedor.UCBool(lector("con_NoAfectoIGV"))

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCbo() As List(Of Entidades.Concepto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ConceptoSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Concepto)
                Do While lector.Read
                    Dim obj As New Entidades.Concepto
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("con_Nombre")) = True, "", lector.Item("con_Nombre")))
                    obj.Id = CInt(lector.Item("IdConcepto"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Concepto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ConceptoSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Concepto)
                Do While lector.Read
                    Dim obj As New Entidades.Concepto
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdConcepto")) = True, 0, lector.Item("IdConcepto")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("con_Nombre")) = True, "", lector.Item("con_Nombre")))
                    obj.CuentaContable = CStr(IIf(IsDBNull(lector.Item("con_CuentaContable")) = True, "", lector.Item("con_CuentaContable")))
                    obj.con_IdTipoGasto = CInt(IIf(IsDBNull(lector.Item("IdTipoGasto")) = True, 0, lector.Item("IdTipoGasto")))
                    obj.NombreTipoGasto = CStr(IIf(IsDBNull(lector.Item("tg_Nombre")) = True, "", lector.Item("tg_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("con_Estado")) = True, "", lector.Item("con_Estado")))
                    obj.ConceptoAdelanto = CBool(IIf(IsDBNull(lector.Item("Con_ConceptoAdelanto")) = True, 0, lector.Item("Con_ConceptoAdelanto")))
                    obj.PorcentDetraccion = objDaoMantenedor.UCDec(lector("con_PorcentDetraccion"))
                    obj.MontoMinimoDetraccion = objDaoMantenedor.UCDec(lector("con_MontoMinDetraccion"))
                    obj.con_NoAfectoIGV = objDaoMantenedor.UCBool(lector("con_NoAfectoIGV"))

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Concepto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ConceptoSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Concepto)
                Do While lector.Read
                    Dim obj As New Entidades.Concepto
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdConcepto")) = True, 0, lector.Item("IdConcepto")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("con_Nombre")) = True, "", lector.Item("con_Nombre")))
                    obj.CuentaContable = CStr(IIf(IsDBNull(lector.Item("con_CuentaContable")) = True, "", lector.Item("con_CuentaContable")))
                    obj.con_IdTipoGasto = CInt(IIf(IsDBNull(lector.Item("IdTipoGasto")) = True, 0, lector.Item("IdTipoGasto")))
                    obj.NombreTipoGasto = CStr(IIf(IsDBNull(lector.Item("tg_Nombre")) = True, "", lector.Item("tg_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("con_Estado")) = True, "", lector.Item("con_Estado")))
                    obj.ConceptoAdelanto = CBool(IIf(IsDBNull(lector.Item("Con_ConceptoAdelanto")) = True, 0, lector.Item("Con_ConceptoAdelanto")))
                    obj.PorcentDetraccion = objDaoMantenedor.UCDec(lector("con_PorcentDetraccion"))
                    obj.MontoMinimoDetraccion = objDaoMantenedor.UCDec(lector("con_MontoMinDetraccion"))
                    obj.con_NoAfectoIGV = objDaoMantenedor.UCBool(lector("con_NoAfectoIGV"))

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Concepto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ConceptoSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Concepto)
                Do While lector.Read
                    Dim obj As New Entidades.Concepto
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdConcepto")) = True, 0, lector.Item("IdConcepto")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("con_Nombre")) = True, "", lector.Item("con_Nombre")))
                    obj.CuentaContable = CStr(IIf(IsDBNull(lector.Item("con_CuentaContable")) = True, "", lector.Item("con_CuentaContable")))
                    obj.con_IdTipoGasto = CInt(IIf(IsDBNull(lector.Item("IdTipoGasto")) = True, 0, lector.Item("IdTipoGasto")))
                    obj.NombreTipoGasto = CStr(IIf(IsDBNull(lector.Item("tg_Nombre")) = True, "", lector.Item("tg_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("con_Estado")) = True, "", lector.Item("con_Estado")))
                    obj.ConceptoAdelanto = CBool(IIf(IsDBNull(lector.Item("Con_ConceptoAdelanto")) = True, 0, lector.Item("Con_ConceptoAdelanto")))
                    obj.PorcentDetraccion = objDaoMantenedor.UCDec(lector("con_PorcentDetraccion"))
                    obj.MontoMinimoDetraccion = objDaoMantenedor.UCDec(lector("con_MontoMinDetraccion"))
                    obj.con_NoAfectoIGV = objDaoMantenedor.UCBool(lector("con_NoAfectoIGV"))

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Concepto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ConceptoSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Concepto)
                Do While lector.Read
                    Dim obj As New Entidades.Concepto
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdConcepto")) = True, 0, lector.Item("IdConcepto")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("con_Nombre")) = True, "", lector.Item("con_Nombre")))
                    obj.CuentaContable = CStr(IIf(IsDBNull(lector.Item("con_CuentaContable")) = True, "", lector.Item("con_CuentaContable")))
                    obj.con_IdTipoGasto = CInt(IIf(IsDBNull(lector.Item("IdTipoGasto")) = True, 0, lector.Item("IdTipoGasto")))
                    obj.NombreTipoGasto = CStr(IIf(IsDBNull(lector.Item("tg_Nombre")) = True, "", lector.Item("tg_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("con_Estado")) = True, "", lector.Item("con_Estado")))
                    obj.ConceptoAdelanto = CBool(IIf(IsDBNull(lector.Item("Con_ConceptoAdelanto")) = True, 0, lector.Item("Con_ConceptoAdelanto")))
                    obj.PorcentDetraccion = objDaoMantenedor.UCDec(lector("con_PorcentDetraccion"))
                    obj.MontoMinimoDetraccion = objDaoMantenedor.UCDec(lector("con_MontoMinDetraccion"))
                    obj.con_NoAfectoIGV = objDaoMantenedor.UCBool(lector("con_NoAfectoIGV"))

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Concepto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ConceptoSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Concepto)
                Do While lector.Read
                    Dim obj As New Entidades.Concepto
                    obj.Id = CInt(IIf(IsDBNull(lector.Item("IdConcepto")) = True, 0, lector.Item("IdConcepto")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("con_Nombre")) = True, "", lector.Item("con_Nombre")))
                    obj.CuentaContable = CStr(IIf(IsDBNull(lector.Item("con_CuentaContable")) = True, "", lector.Item("con_CuentaContable")))
                    obj.con_IdTipoGasto = CInt(IIf(IsDBNull(lector.Item("IdTipoGasto")) = True, 0, lector.Item("IdTipoGasto")))
                    obj.NombreTipoGasto = CStr(IIf(IsDBNull(lector.Item("tg_Nombre")) = True, "", lector.Item("tg_Nombre")))
                    obj.Estado = CStr(IIf(IsDBNull(lector.Item("con_Estado")) = True, "", lector.Item("con_Estado")))

                    obj.ConceptoAdelanto = CBool(IIf(IsDBNull(lector.Item("con_ConceptoAdelanto")) = True, 0, lector.Item("con_ConceptoAdelanto")))
                    obj.PorcentDetraccion = objDaoMantenedor.UCDec(lector("con_PorcentDetraccion"))
                    obj.MontoMinimoDetraccion = objDaoMantenedor.UCDec(lector("con_MontoMinDetraccion"))
                    obj.con_NoAfectoIGV = objDaoMantenedor.UCBool(lector("con_NoAfectoIGV"))

                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function CargarConceptosxFactura() As List(Of Entidades.Concepto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("usp_CargarConceptosxFactura", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Concepto)
                Do While lector.Read
                    Dim obj As New Entidades.Concepto
                    'obj.CuentaContable = CStr(IIf(IsDBNull(lector.Item("con_CuentaContable")) = True, "", lector.Item("con_CuentaContable")))
                    'obj.Estado = CStr(IIf(IsDBNull(lector.Item("con_Estado")) = True, "", lector.Item("con_Estado")))
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("con_Nombre")) = True, "", lector.Item("con_Nombre")))
                    obj.Id = CInt(lector.Item("IdConcepto"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    '********************************************************
    'M�todo Din, para obtener TblConcepto por cualquier campo
    'Autor   : Hans Fernando, Quispe Espinoza
    'M�dulo  : Caja-Documento
    'Sistema : Indusfer
    'Empresa : Digrafic SRL
    'Fecha   : 24-Set-2009
    'Parametros : psWhere, psOrder
    'Retorna : Lista de tipo Concepto
    '********************************************************
    Public Function fnSelTblConceptoDin(ByVal psWhere As String, ByVal psOrder As String) As List(Of Entidades.Concepto)
        Dim loList As New List(Of Entidades.Concepto)()
        Dim par As SqlParameter() = New SqlParameter(1) {}
        par(0) = New SqlParameter("@WhereCondition", System.Data.SqlDbType.VarChar, 500)
        par(0).Value = psWhere
        par(1) = New SqlParameter("@OrderByExpression", System.Data.SqlDbType.VarChar, 250)
        par(1).Value = psOrder
        Try
            Dim loDr As SqlDataReader = SqlHelper.ExecuteReader(objConexion.ConexionSIGE, CommandType.StoredProcedure, "usp_SelDocumentoDin", par)
            While (loDr.Read())
                Dim loConcepto As New Entidades.Concepto
                loConcepto.CuentaContable = Convert.ToString(CStr(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("con_CuentaContable"))) = True, "", loDr.GetValue(loDr.GetOrdinal("con_CuentaContable")))))
                loConcepto.Estado = Convert.ToString(CStr(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("con_Estado"))) = True, "", loDr.GetValue(loDr.GetOrdinal("con_Estado")))))
                loConcepto.Id = Convert.ToInt32(CInt(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("IdConcepto"))) = True, "", loDr.GetValue(loDr.GetOrdinal("IdConcepto")))))
                loConcepto.Nombre = Convert.ToString(CStr(IIf(IsDBNull(loDr.GetValue(loDr.GetOrdinal("con_Nombre"))) = True, "", loDr.GetValue(loDr.GetOrdinal("con_Nombre")))))
                loConcepto.ConceptoAdelanto = Convert.ToBoolean(IIf(IsDBNull(loDr.Item("con_ConceptoAdelanto")) = True, 0, loDr.Item("con_ConceptoAdelanto")))
                loList.Add(loConcepto)
            End While
            loDr.Close()
        Catch e As Exception
            Throw e
        Finally
        End Try
        Return loList
    End Function

    Public Function SelectCboxTipoUso(ByVal TipoUso As Integer) As List(Of Entidades.Concepto)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ConceptoSelectCboxTipoUso", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@TipoUso", TipoUso)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Concepto)
                Do While lector.Read
                    Dim obj As New Entidades.Concepto
                    obj.Nombre = CStr(IIf(IsDBNull(lector.Item("con_Nombre")) = True, "", lector.Item("con_Nombre")))
                    obj.Id = CInt(lector.Item("IdConcepto"))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
