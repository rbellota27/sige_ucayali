﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'**********************   VIERNES 25 06 2010

Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOValorReferencia_T

    Private objDaoMantenedor As New DAO.DAOMantenedor
    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private reader As SqlDataReader
    Private cmd As SqlCommand

    Public Function Registrar(ByVal objValorReferenciaT As Entidades.ValorReferencia_Transporte) As Boolean

        Dim hecho As Boolean = False

        Try

            Dim p() As SqlParameter = New SqlParameter(8) {}
            p(0) = objDaoMantenedor.getParam(objValorReferenciaT.IdValorReferenciaT, "@IdValorReferenciaT", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(objValorReferenciaT.IdUbigeoOrigen, "@IdUbigeoOrigen", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(objValorReferenciaT.IdUbigeoDestino, "@IdUbigeoDestino", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(objValorReferenciaT.Descripcion, "@vr_Descripcion", SqlDbType.VarChar)
            p(4) = objDaoMantenedor.getParam(objValorReferenciaT.IdMoneda, "@IdMoneda", SqlDbType.Int)
            p(5) = objDaoMantenedor.getParam(objValorReferenciaT.Monto, "@vr_Monto", SqlDbType.Decimal)
            p(6) = objDaoMantenedor.getParam(objValorReferenciaT.IdUsuarioInsert, "@IdUsuarioInsert", SqlDbType.Int)
            p(7) = objDaoMantenedor.getParam(objValorReferenciaT.IdUsuarioUpdate, "@IdUsuarioUpdate", SqlDbType.Int)
            p(8) = objDaoMantenedor.getParam(objValorReferenciaT.Estado, "@vr_Estado", SqlDbType.Bit)

            cn.Open()
            SqlHelper.ExecuteNonQuery(cn, CommandType.StoredProcedure, "_ValorReferencia_Transporte_Registrar", p)

            hecho = True

        Catch ex As Exception
            hecho = False
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return hecho

    End Function

    Public Function SelectxParams_DT(ByVal IdValorReferenciaT As Integer, ByVal Estado As Boolean, Optional ByVal descripcion As String = Nothing) As DataTable

        Dim ds As New DataSet
        Dim objConexion As New DAO.Conexion
        Try

            Dim p() As SqlParameter = New SqlParameter(2) {}
            p(0) = objDaoMantenedor.getParam(IdValorReferenciaT, "@IdValorReferenciaT", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(Estado, "@Estado", SqlDbType.Bit)
            p(2) = objDaoMantenedor.getParam(descripcion, "@descripcion", SqlDbType.VarChar)

            cmd = New SqlCommand("_ValorReferencia_Transporte_SelectxParams", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_ValorReferenciaT")

        Catch ex As Exception

            Throw ex
        Finally

        End Try

        Return ds.Tables("DT_ValorReferenciaT")


    End Function

    Public Function SelectxIdValorReferenciaT(ByVal IdValorReferenciaT As Integer) As Entidades.ValorReferencia_Transporte

        Dim obj As Entidades.ValorReferencia_Transporte = Nothing

        Try

            Dim p() As SqlParameter = New SqlParameter(2) {}
            p(0) = objDaoMantenedor.getParam(IdValorReferenciaT, "@IdValorReferenciaT", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(Nothing, "@Estado", SqlDbType.Bit)
            p(2) = objDaoMantenedor.getParam(Nothing, "@descripcion", SqlDbType.VarChar)

            cn.Open()
            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_ValorReferencia_Transporte_SelectxParams", p)
            If (reader.Read) Then
                obj = New Entidades.ValorReferencia_Transporte
                With obj

                    .IdValorReferenciaT = objDaoMantenedor.UCInt(reader("IdValorReferenciaT"))
                    .IdUbigeoOrigen = objDaoMantenedor.UCInt(reader("IdUbigeoOrigen"))
                    .IdUbigeoDestino = objDaoMantenedor.UCInt(reader("IdUbigeoDestino"))
                    .Descripcion = objDaoMantenedor.UCStr(reader("Descripcion"))
                    .IdMoneda = objDaoMantenedor.UCInt(reader("IdMoneda"))
                    .Monto = objDaoMantenedor.UCDec(reader("Monto"))
                    .IdUsuarioInsert = objDaoMantenedor.UCInt(reader("IdUsuarioInsert"))
                    .FechaInsert = objDaoMantenedor.UCDate(reader("FechaInsert"))
                    .IdUsuarioUpdate = objDaoMantenedor.UCInt(reader("IdUsuarioUpdate"))
                    .FechaUpdate = objDaoMantenedor.UCDate(reader("FechaUpdate"))
                    .Estado = objDaoMantenedor.UCBool(reader("Estado"))
                    .Moneda = objDaoMantenedor.UCStr(reader("Moneda"))
                    .CodDpto_Origen = objDaoMantenedor.UCStr(reader("CodDpto_Origen"))
                    .CodProv_Origen = objDaoMantenedor.UCStr(reader("CodProv_Origen"))
                    .CodDist_Origen = objDaoMantenedor.UCStr(reader("CodDist_Origen"))
                    .Origen = objDaoMantenedor.UCStr(reader("Origen"))
                    .CodDpto_Destino = objDaoMantenedor.UCStr(reader("CodDpto_Destino"))
                    .CodProv_Destino = objDaoMantenedor.UCStr(reader("CodProv_Destino"))
                    .CodDist_Destino = objDaoMantenedor.UCStr(reader("CodDist_Destino"))
                    .Destino = objDaoMantenedor.UCStr(reader("Destino"))

                End With
            End If
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return obj

    End Function

End Class
