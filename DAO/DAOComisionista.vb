﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAOComisionista

    Private objDaoMantenedor As New DAO.DAOMantenedor
    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private reader As SqlDataReader

    Public Function Comisionista_Select_Personas_Lista(ByVal IdComisionCab As Integer, ByVal IdEmpresa As Integer, _
                                               ByVal IdTienda As Integer, ByVal IdPerfil As Integer, ByVal IdRol As Integer, ByVal idConfigurado As Boolean) As List(Of Entidades.Comisionista)

        Dim Lista As New List(Of Entidades.Comisionista)
        Dim objComisionista As Entidades.Comisionista

        Try

            Dim p() As SqlParameter = New SqlParameter(6) {}
            p(0) = objDaoMantenedor.getParam(IdComisionCab, "@IdComisionCab", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(Nothing, "@IdPersona", SqlDbType.Int)
            p(4) = objDaoMantenedor.getParam(IdPerfil, "@IdPerfil", SqlDbType.Int)
            p(5) = objDaoMantenedor.getParam(IdRol, "@IdRol", SqlDbType.Int)
            p(6) = objDaoMantenedor.getParam(idConfigurado, "@Configurado", SqlDbType.Bit)

            cn.Open()
            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_Comisionista_Select_Personas", p)
            While (reader.Read)

                objComisionista = New Entidades.Comisionista

                With objComisionista

                    .IdComisionista = objDaoMantenedor.UCInt(reader("IdComisionista"))
                    .IdPersona = objDaoMantenedor.UCInt(reader("IdPersona"))
                    .IdComisionCab = objDaoMantenedor.UCInt(reader("IdComisionCab"))
                    .Estado = objDaoMantenedor.UCBool(reader("com_Estado"))
                    .baseComision = objDaoMantenedor.UCStr(reader("com_NombreBase"))
                    .valPorcentaje = objDaoMantenedor.UCDec(reader("com_Porcentaje"))
                    .IdEmpresa = objDaoMantenedor.UCInt(reader("IdEmpresa"))
                    .IdTienda = objDaoMantenedor.UCInt(reader("IdTienda"))
                    .IncluyeIgv = objDaoMantenedor.UCBool(reader("com_IncluyeIgv"))
                    .IdPerfil = objDaoMantenedor.UCInt(reader("IdPerfil"))
                    .Configurado = objDaoMantenedor.UCBool(reader("Configurado"))
                    .TipoCalculo = objDaoMantenedor.UCBool(reader("TipoCalculo"))
                End With
                Lista.Add(objComisionista)
                If idConfigurado = False Then
                    Exit While
                End If

            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return Lista

    End Function

    Public Function Comisionista_Select_Personas(ByVal IdComisionCab As Integer, ByVal IdEmpresa As Integer, _
                                                 ByVal IdTienda As Integer, ByVal IdPersona As Integer, _
                                                 ByVal IdPerfil As Integer, ByVal IdRol As Integer) As Entidades.Comisionista

        Dim objComisionista As New Entidades.Comisionista

        Try

            Dim p() As SqlParameter = New SqlParameter(6) {}
            p(0) = objDaoMantenedor.getParam(IdComisionCab, "@IdComisionCab", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(IdPersona, "@IdPersona", SqlDbType.Int)
            p(4) = objDaoMantenedor.getParam(IdPerfil, "@IdPerfil", SqlDbType.Int)
            p(5) = objDaoMantenedor.getParam(IdRol, "@IdRol", SqlDbType.Int)
            p(6) = objDaoMantenedor.getParam(Nothing, "@Configurado", SqlDbType.Bit)

            cn.Open()
            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_Comisionista_Select_Personas", p)
            While (reader.Read)



                With objComisionista

                    .IdComisionista = objDaoMantenedor.UCInt(reader("IdComisionista"))
                    .IdPersona = objDaoMantenedor.UCInt(reader("IdPersona"))
                    .IdComisionCab = objDaoMantenedor.UCInt(reader("IdComisionCab"))
                    .Estado = objDaoMantenedor.UCBool(reader("com_Estado"))
                    .baseComision = objDaoMantenedor.UCStr(reader("com_NombreBase"))
                    .valPorcentaje = objDaoMantenedor.UCDec(reader("com_Porcentaje"))
                    .IdEmpresa = objDaoMantenedor.UCInt(reader("IdEmpresa"))
                    .IdTienda = objDaoMantenedor.UCInt(reader("IdTienda"))
                    .IncluyeIgv = objDaoMantenedor.UCBool(reader("com_IncluyeIgv"))
                    .IdPerfil = objDaoMantenedor.UCInt(reader("IdPerfil"))
                    .Configurado = objDaoMantenedor.UCBool(reader("Configurado"))
                End With


            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objComisionista

    End Function


    Public Sub DeletexIdComisionCabxIdPersona(ByVal IdComisionCab As Integer, ByVal IdPersona As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim P() As SqlParameter = New SqlParameter(1) {}
        P(0) = objDaoMantenedor.getParam(IdComisionCab, "@IdComisionCab", SqlDbType.Int)
        P(1) = objDaoMantenedor.getParam(IdPersona, "@IdPersona", SqlDbType.Int)

        SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "_Comisionista_DeletexIdComisionCabxIdPersona", P)

    End Sub


    Public Function Comisionista_Insert(ByVal objComisionista As Entidades.Comisionista, ByVal cnx As SqlConnection, ByVal tr As SqlTransaction) As Integer

        Dim p() As SqlParameter = New SqlParameter(10) {}
        p(0) = objDaoMantenedor.getParam(objComisionista.IdPersona, "@IdPersona", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(objComisionista.IdComisionCab, "@IdComisionCab", SqlDbType.Int)
        p(2) = objDaoMantenedor.getParam(objComisionista.Estado, "@com_Estado", SqlDbType.Bit)
        p(3) = objDaoMantenedor.getParam(objComisionista.baseComision, "@com_NombreBase", SqlDbType.VarChar)
        p(4) = objDaoMantenedor.getParam(objComisionista.valPorcentaje, "@com_Porcentaje", SqlDbType.Decimal)
        p(5) = objDaoMantenedor.getParam(objComisionista.IdEmpresa, "@IdEmpresa", SqlDbType.Int)
        p(6) = objDaoMantenedor.getParam(objComisionista.IdTienda, "@IdTienda", SqlDbType.Int)
        p(7) = objDaoMantenedor.getParam(objComisionista.IncluyeIgv, "@com_IncluyeIgv", SqlDbType.Bit)
        p(8) = objDaoMantenedor.getParam(objComisionista.IdRol, "@IdRol", SqlDbType.Int)
        p(9) = objDaoMantenedor.getParam(objComisionista.IdPerfil, "@IdPerfil", SqlDbType.Int)
        p(10) = objDaoMantenedor.getParam(objComisionista.Configurado, "@Configurado", SqlDbType.Bit)

        Return CInt(SqlHelper.ExecuteScalar(tr, CommandType.StoredProcedure, "_Comisionista_Insert", p))

    End Function

    Public Function Registrar(ByVal objComisionista As Entidades.Comisionista, ByVal cnx As SqlConnection, ByVal tr As SqlTransaction) As Integer

        Dim p() As SqlParameter = New SqlParameter(4) {}
        p(0) = objDaoMantenedor.getParam(objComisionista.IdComisionista, "@IdComisionista", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(objComisionista.IdComisionCab, "@IdComisionCab", SqlDbType.Int)
        p(2) = objDaoMantenedor.getParam(objComisionista.Estado, "@com_Estado", SqlDbType.Bit)
        p(3) = objDaoMantenedor.getParam(objComisionista.baseComision, "@com_NombreBase", SqlDbType.VarChar)
        p(4) = objDaoMantenedor.getParam(objComisionista.valPorcentaje, "@com_Porcentaje", SqlDbType.Decimal)

        Return CInt(SqlHelper.ExecuteScalar(tr, CommandType.StoredProcedure, "_Comisionista_Registrar", p))

    End Function

    Public Function Comisionista_Insert_por_grupo(ByVal objComisionista As Entidades.Comisionista, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer

        Dim p() As SqlParameter = New SqlParameter(11) {}
        p(0) = objDaoMantenedor.getParam(objComisionista.IdComisionista, "@IdPersona", SqlDbType.Int)
        p(1) = objDaoMantenedor.getParam(objComisionista.IdComisionCab, "@IdComisionCab", SqlDbType.Int)
        p(2) = objDaoMantenedor.getParam(objComisionista.Estado, "@com_Estado", SqlDbType.Bit)
        p(3) = objDaoMantenedor.getParam(objComisionista.valPorcentaje, "@com_Porcentaje", SqlDbType.Decimal)
        p(4) = objDaoMantenedor.getParam(objComisionista.baseComision, "@com_NombreBase", SqlDbType.VarChar)
        p(5) = objDaoMantenedor.getParam(objComisionista.IdEmpresa, "@IdEmpresa", SqlDbType.Int)
        p(6) = objDaoMantenedor.getParam(objComisionista.IdTienda, "@IdTienda", SqlDbType.Int)
        p(7) = objDaoMantenedor.getParam(objComisionista.IncluyeIgv, "@com_IncluyeIgv", SqlDbType.Bit)
        p(8) = objDaoMantenedor.getParam(objComisionista.IdRol, "@IdRol", SqlDbType.Int)
        p(9) = objDaoMantenedor.getParam(objComisionista.IdPerfil, "@IdPerfil", SqlDbType.Int)
        p(10) = objDaoMantenedor.getParam(objComisionista.Configurado, "@Configurado", SqlDbType.Bit)
        p(11) = objDaoMantenedor.getParam(objComisionista.TipoCalculo, "@TipoCalCulo", SqlDbType.Bit)

        Return CInt(SqlHelper.ExecuteScalar(tr, CommandType.StoredProcedure, "_Comisionista_Insert_por_grupo", p))

    End Function
    Public Function Comisionista_SelectxParams(ByVal IdComisionCab As Integer) As List(Of Entidades.Comisionista)

        Dim listaComisionista As New List(Of Entidades.Comisionista)

        Try

            Dim p() As SqlParameter = New SqlParameter(0) {}
            p(0) = objDaoMantenedor.getParam(IdComisionCab, "@IdComisionCab", SqlDbType.Int)

            cn.Open()
            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_Comisionista_SelectxParams", p)
            While (reader.Read)

                Dim objComisionista As New Entidades.Comisionista

                With objComisionista

                    .IdComisionista = objDaoMantenedor.UCInt(reader("IdComisionista"))
                    .IdPersona = .IdComisionista
                    .IdComisionCab = objDaoMantenedor.UCInt(reader("IdComisionCab"))
                    .Estado = objDaoMantenedor.UCBool(reader("Estado"))
                    .Descripcion = objDaoMantenedor.UCStr(reader("Descripcion"))
                    .Dni = objDaoMantenedor.UCStr(reader("Dni"))
                    .Empresa = objDaoMantenedor.UCStr(reader("Empresa"))
                    .Tienda = objDaoMantenedor.UCStr(reader("Tienda"))
                    .Perfil = objDaoMantenedor.UCStr(reader("Perfil"))
                    .baseComision = objDaoMantenedor.UCStr(reader("com_NombreBase"))
                    .valPorcentaje = objDaoMantenedor.UCDec(reader("com_Porcentaje"))

                End With

                listaComisionista.Add(objComisionista)

            End While
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return listaComisionista

    End Function

End Class
