﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************** MARTES 02 FEBRERO 2010 HORA 8_04 PM
'*************** LUNES 18 ENERO 2010 HORA 11_51 AM

Imports System.Data.SqlClient
Public Class DAOTarjeta
    Protected HDAO As New DAO.HelperDAO
    Protected objConexion As New Conexion
    Protected cn As SqlConnection   'usado desde una clase heredera

    Public Overridable Function SelectAllActivo() As List(Of Entidades.Tarjeta)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TarjetaSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tarjeta)
                Do While lector.Read
                    Dim objTarjeta As New Entidades.Tarjeta
                    objTarjeta.Nombre = CStr(IIf(IsDBNull(lector.Item("t_Nombre")) = True, "", lector.Item("t_Nombre")))
                    objTarjeta.Estado = CBool(IIf(IsDBNull(lector.Item("t_Estado")) = True, "", lector.Item("t_Estado")))
                    objTarjeta.IdTarjeta = CInt(IIf(IsDBNull(lector.Item("IdTarjeta")) = True, 0, lector.Item("IdTarjeta")))
                    objTarjeta.IdTipoTarjeta = CInt(IIf(IsDBNull(lector.Item("IdTipoTarjeta")) = True, 0, lector.Item("IdTipoTarjeta")))
                    Lista.Add(objTarjeta)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Overridable Function SelectAll() As List(Of Entidades.Tarjeta)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TarjetaSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tarjeta)
                Do While lector.Read
                    Dim objTarjeta As New Entidades.Tarjeta
                    objTarjeta.Nombre = CStr(IIf(IsDBNull(lector.Item("t_Nombre")) = True, "", lector.Item("t_Nombre")))
                    objTarjeta.Estado = CBool(IIf(IsDBNull(lector.Item("t_Estado")) = True, "", lector.Item("t_Estado")))
                    objTarjeta.IdTarjeta = CInt(IIf(IsDBNull(lector.Item("IdTarjeta")) = True, 0, lector.Item("IdTarjeta")))
                    objTarjeta.IdTipoTarjeta = CInt(IIf(IsDBNull(lector.Item("IdTipoTarjeta")) = True, 0, lector.Item("IdTipoTarjeta")))
                    Lista.Add(objTarjeta)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Overridable Function SelectAllInactivo() As List(Of Entidades.Tarjeta)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TarjetaSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tarjeta)
                Do While lector.Read
                    Dim objTarjeta As New Entidades.Tarjeta
                    objTarjeta.Nombre = CStr(IIf(IsDBNull(lector.Item("t_Nombre")) = True, "", lector.Item("t_Nombre")))
                    objTarjeta.Estado = CBool(IIf(IsDBNull(lector.Item("t_Estado")) = True, "", lector.Item("t_Estado")))
                    objTarjeta.IdTarjeta = CInt(IIf(IsDBNull(lector.Item("IdTarjeta")) = True, 0, lector.Item("IdTarjeta")))
                    objTarjeta.IdTipoTarjeta = CInt(IIf(IsDBNull(lector.Item("IdTipoTarjeta")) = True, 0, lector.Item("IdTipoTarjeta")))
                    Lista.Add(objTarjeta)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function InsertaTarjeta(ByVal Tarjeta As Entidades.Tarjeta) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@t_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = Tarjeta.Nombre
        ArrayParametros(1) = New SqlParameter("@t_Estado", SqlDbType.Bit)
        ArrayParametros(1).Value = Tarjeta.Estado
        ArrayParametros(2) = New SqlParameter("@IdTipoTarjeta", SqlDbType.Int)
        ArrayParametros(2).Value = Tarjeta.IdTipoTarjeta

        Return HDAO.Insert(cn, "_TarjetaInsert", ArrayParametros)
    End Function
    Public Function ActualizaTarjeta(ByVal Tarjeta As Entidades.Tarjeta) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@t_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = Tarjeta.Nombre
        ArrayParametros(1) = New SqlParameter("@t_Estado", SqlDbType.Bit)
        ArrayParametros(1).Value = Tarjeta.Estado
        ArrayParametros(2) = New SqlParameter("@IdTarjeta", SqlDbType.Int)
        ArrayParametros(2).Value = Tarjeta.IdTarjeta
        ArrayParametros(3) = New SqlParameter("@IdTipoTarjeta", SqlDbType.Int)
        ArrayParametros(3).Value = Tarjeta.IdTipoTarjeta
        Return HDAO.Update(cn, "_TarjetaUpdate", ArrayParametros)
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Tarjeta)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TarjetaSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tarjeta)
                Do While lector.Read
                    Dim objTarjeta As New Entidades.Tarjeta
                    objTarjeta.Nombre = CStr(IIf(IsDBNull(lector.Item("t_Nombre")) = True, "", lector.Item("t_Nombre")))
                    objTarjeta.Estado = CBool(IIf(IsDBNull(lector.Item("t_Estado")) = True, "", lector.Item("t_Estado")))
                    objTarjeta.IdTarjeta = CInt(IIf(IsDBNull(lector.Item("IdTarjeta")) = True, 0, lector.Item("IdTarjeta")))
                    objTarjeta.IdTipoTarjeta = CInt(IIf(IsDBNull(lector.Item("IdTipoTarjeta")) = True, 0, lector.Item("IdTipoTarjeta")))
                    Lista.Add(objTarjeta)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Tarjeta)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TarjetaSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tarjeta)
                Do While lector.Read
                    Dim objTarjeta As New Entidades.Tarjeta
                    objTarjeta.Nombre = CStr(IIf(IsDBNull(lector.Item("t_Nombre")) = True, "", lector.Item("t_Nombre")))
                    objTarjeta.Estado = CBool(IIf(IsDBNull(lector.Item("t_Estado")) = True, "", lector.Item("t_Estado")))
                    objTarjeta.IdTarjeta = CInt(IIf(IsDBNull(lector.Item("IdTarjeta")) = True, 0, lector.Item("IdTarjeta")))
                    objTarjeta.IdTipoTarjeta = CInt(IIf(IsDBNull(lector.Item("IdTipoTarjeta")) = True, 0, lector.Item("IdTipoTarjeta")))
                    Lista.Add(objTarjeta)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Tarjeta)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TarjetaSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tarjeta)
                Do While lector.Read
                    Dim objTarjeta As New Entidades.Tarjeta
                    objTarjeta.Nombre = CStr(IIf(IsDBNull(lector.Item("t_Nombre")) = True, "", lector.Item("t_Nombre")))
                    objTarjeta.Estado = CBool(IIf(IsDBNull(lector.Item("t_Estado")) = True, "", lector.Item("t_Estado")))
                    objTarjeta.IdTarjeta = CInt(IIf(IsDBNull(lector.Item("IdTarjeta")) = True, 0, lector.Item("IdTarjeta")))
                    objTarjeta.IdTipoTarjeta = CInt(IIf(IsDBNull(lector.Item("IdTipoTarjeta")) = True, 0, lector.Item("IdTipoTarjeta")))
                    Lista.Add(objTarjeta)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Tarjeta)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_TarjetaSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tarjeta)
                Do While lector.Read
                    Dim objTarjeta As New Entidades.Tarjeta
                    objTarjeta.Nombre = CStr(IIf(IsDBNull(lector.Item("t_Nombre")) = True, "", lector.Item("t_Nombre")))
                    objTarjeta.Estado = CBool(IIf(IsDBNull(lector.Item("t_Estado")) = True, "", lector.Item("t_Estado")))
                    objTarjeta.IdTarjeta = CInt(IIf(IsDBNull(lector.Item("IdTarjeta")) = True, 0, lector.Item("IdTarjeta")))
                    objTarjeta.IdTipoTarjeta = CInt(IIf(IsDBNull(lector.Item("IdTipoTarjeta")) = True, 0, lector.Item("IdTipoTarjeta")))
                    Lista.Add(objTarjeta)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
