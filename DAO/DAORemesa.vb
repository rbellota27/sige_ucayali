﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAORemesa


    Public Sub Anular(ByVal IdDocumento As Integer, ByVal deleteMovBanco As Boolean, ByVal deleteRelacionDocumento As Boolean, ByVal anular As Boolean, ByVal sqlcn As SqlConnection, ByVal sqltr As SqlTransaction)
        Try

            Dim cmd As New SqlCommand("_DocumentoRemesa_DeshacerMov", sqlcn, sqltr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdDocumento", IdDocumento)
            cmd.Parameters.AddWithValue("@deleteMovBanco", deleteMovBanco)
            cmd.Parameters.AddWithValue("@deleteRelacionDocumento", deleteRelacionDocumento)
            cmd.Parameters.AddWithValue("@anular", deleteRelacionDocumento)

            cmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


End Class
