'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.


Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOCuentaPersona
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion


    Public Function CuentaPersonaCtaEmpresaTienda(ByVal dni As String, ByVal ruc As String, _
                                      ByVal RazonApe As String, ByVal tipo As Integer, _
                                      ByVal pageindex As Integer, ByVal pagesize As Integer, _
                                      ByVal idrol As Integer, ByVal estado As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer) As List(Of Entidades.CuentaPersona)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim paramentros() As SqlParameter = New SqlParameter(9) {}
        paramentros(0) = New SqlParameter("@Dni", SqlDbType.VarChar)
        paramentros(0).Value = dni
        paramentros(1) = New SqlParameter("@Ruc", SqlDbType.VarChar)
        paramentros(1).Value = ruc
        paramentros(2) = New SqlParameter("@razonApe", SqlDbType.VarChar)
        paramentros(2).Value = RazonApe
        paramentros(3) = New SqlParameter("@tipo", SqlDbType.Int)
        paramentros(3).Value = tipo
        paramentros(4) = New SqlParameter("@pageindex", SqlDbType.Int)
        paramentros(4).Value = pageindex
        paramentros(5) = New SqlParameter("@pageSize", SqlDbType.Int)
        paramentros(5).Value = pagesize
        paramentros(6) = New SqlParameter("@idRol", SqlDbType.Int)
        paramentros(6).Value = idrol
        paramentros(7) = New SqlParameter("@estado", SqlDbType.Int)
        paramentros(7).Value = estado
        paramentros(8) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        paramentros(8).Value = IdEmpresa
        paramentros(9) = New SqlParameter("@IdTienda", SqlDbType.Int)
        paramentros(9).Value = IdTienda

        Dim cmd As New SqlCommand("_CuentaPersonaCtaEmpresaTienda", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(paramentros)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CuentaPersona)
                Do While lector.Read
                    Dim obj As New Entidades.CuentaPersona
                    With obj
                        .IdPersona = CInt(lector("IdPersona"))
                        .Nombre = CStr(IIf(IsDBNull(lector("Nombre")), "---", lector("Nombre")))
                        .Ruc = CStr(IIf(IsDBNull(lector("Ruc")), "---", lector("Ruc")))
                        .Dni = CStr(IIf(IsDBNull(lector("Dni")), "---", lector("Dni")))
                        .cadTienda = CStr(IIf(IsDBNull(lector("cadTienda")), "", lector("cadTienda")))
                        .cadMoneda = CStr(IIf(IsDBNull(lector("cadMoneda")), "", lector("cadMoneda")))
                        .cadCargoMaximo = CStr(IIf(IsDBNull(lector("cadCargoMaximo")), "", lector("cadCargoMaximo")))
                        .cadDisponible = CStr(IIf(IsDBNull(lector("cadDisponible")), "", lector("cadDisponible")))
                        .cadSaldo = CStr(IIf(IsDBNull(lector("cadSaldo")), "", lector("cadSaldo")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function








    Public Function InsertaCuentaPersona(ByVal cuentapersona As Entidades.CuentaPersona) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@cp_FechaAlta", SqlDbType.DateTime)
        ArrayParametros(0).Value = cuentapersona.FechaAlta
        ArrayParametros(1) = New SqlParameter("@cp_CargoMaximo", SqlDbType.Decimal)
        ArrayParametros(1).Value = cuentapersona.CargoMaximo
        ArrayParametros(2) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(2).Value = cuentapersona.IdPersona
        ArrayParametros(3) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        ArrayParametros(3).Value = cuentapersona.IdMoneda
        ArrayParametros(4) = New SqlParameter("@cp_Estado", SqlDbType.Char)
        ArrayParametros(4).Value = cuentapersona.Estado
        Return HDAO.Insert(cn, "_CuentaPersonaInsert", ArrayParametros)
    End Function
    Public Function ActualizaCuentaPersona(ByVal cuentapersona As Entidades.CuentaPersona) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@cp_FechaAlta", SqlDbType.DateTime)
        ArrayParametros(0).Value = cuentapersona.FechaAlta
        ArrayParametros(1) = New SqlParameter("@cp_CargoMaximo", SqlDbType.Decimal)
        ArrayParametros(1).Value = cuentapersona.CargoMaximo
        ArrayParametros(2) = New SqlParameter("@cp_Saldo", SqlDbType.Decimal)
        ArrayParametros(2).Value = cuentapersona.Saldo
        ArrayParametros(2) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(2).Value = cuentapersona.IdPersona
        ArrayParametros(3) = New SqlParameter("@IdMoneda", SqlDbType.Int)
        ArrayParametros(3).Value = cuentapersona.IdMoneda
        ArrayParametros(4) = New SqlParameter("@cp_Estado", SqlDbType.Char)
        ArrayParametros(4).Value = cuentapersona.Estado
        Return HDAO.Update(cn, "_CuentaPersonaUpdate", ArrayParametros)
    End Function
    Public Function SelectGrillaCxCResumenDocumento(ByVal idpersona As Integer, ByVal idmoneda As Integer) As List(Of Entidades.CxCResumenView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("[_CR_CuentasxCobrarResumenDocumentoxMoneda]", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idmoneda", idmoneda)
        cmd.Parameters.AddWithValue("@idPersona", idpersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CxCResumenView)
                Do While lector.Read
                    Dim CxCD As New Entidades.CxCResumenView
                    CxCD.idDocumento = CInt(IIf(IsDBNull(lector.Item("IdDocumento")) = True, 0, lector.Item("IdDocumento")))
                    CxCD.tdoc_NombreCorto = CStr(IIf(IsDBNull(lector.Item("Documento")) = True, "", lector.Item("Documento")))
                    CxCD.doc_FechaEmision = CDate(IIf(IsDBNull(lector.Item("doc_FechaEmision")) = True, Nothing, lector.Item("doc_FechaEmision")))
                    CxCD.doc_FechaVencimiento = CDate(IIf(IsDBNull(lector.Item("doc_FechaVenc")) = True, Nothing, lector.Item("doc_FechaVenc")))
                    CxCD.nroDias = CInt(IIf(IsDBNull(lector.Item("diasVencidos")) = True, 0, lector.Item("diasVencidos")))
                    CxCD.mon_simbolo = CStr(lector.Item("moneda"))
                    CxCD.doc_TotalPagar = CDec(IIf(IsDBNull(lector.Item("mcu_Monto")) = True, Nothing, lector.Item("mcu_Monto")))
                    CxCD.abono = CDec(IIf(IsDBNull(lector.Item("TotalAbono")) = True, 0, lector.Item("TotalAbono")))
                    CxCD.Saldo = CDec(IIf(IsDBNull(lector.Item("mcu_Saldo")) = True, 0, lector.Item("mcu_Saldo")))
                    CxCD.Numero = CStr(IIf(IsDBNull(lector.Item("numero")) = True, "", lector.Item("numero")))
                    Lista.Add(CxCD)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectAllConsultaxIdPersona(ByVal IdPersona As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer) As List(Of Entidades.CuentaPersona)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CuentaPersonaSelectAllConsultaxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CuentaPersona)
                With lector
                    Do While .Read
                        Dim obj As New Entidades.CuentaPersona
                        obj.IdCuentaPersona = CInt(IIf(IsDBNull(.Item("IdCuentaPersona")) = True, 0, .Item("IdCuentaPersona")))
                        obj.IdMoneda = CInt(IIf(IsDBNull(.Item("IdMoneda")) = True, 0, .Item("IdMoneda")))
                        obj.MonedaSimbolo = CStr(IIf(IsDBNull(.Item("mon_Simbolo")) = True, "", .Item("mon_Simbolo")))
                        obj.CargoMaximo = CDec(IIf(IsDBNull(.Item("cp_CargoMaximo")) = True, 0, .Item("cp_CargoMaximo")))
                        obj.Saldo = CDec(IIf(IsDBNull(.Item("Saldo")) = True, 0, .Item("Saldo")))

                        obj.CuentaFormal = CBool(IIf(IsDBNull(.Item("cp_CuentaFormal")) = True, 0, .Item("cp_CuentaFormal")))
                        obj.IdTienda = CInt(IIf(IsDBNull(.Item("IdTienda")) = True, 0, .Item("IdTienda")))
                        obj.IdEmpresa = CInt(IIf(IsDBNull(.Item("IdEmpresa")) = True, 0, .Item("IdEmpresa")))
                        obj.IdSupervisor = CInt(IIf(IsDBNull(.Item("IdSupervisor")) = True, 0, .Item("IdSupervisor")))
                        obj.FechaAlta = CStr(IIf(IsDBNull(.Item("cp_FechaAlta")) = True, Nothing, .Item("cp_FechaAlta")))
                      
                        Lista.Add(obj)
                    Loop
                    .Close()
                End With
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectxIdPersona(ByVal IdPersona As Integer) As List(Of Entidades.CuentaPersona)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CuentaPersonaSelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CuentaPersona)
                With lector
                    Do While .Read
                        Dim obj As New Entidades.CuentaPersona
                        obj.IdCuentaPersona = CInt(IIf(IsDBNull(.Item("IdCuentaPersona")) = True, 0, .Item("IdCuentaPersona")))
                        obj.CargoMaximo = CDec(IIf(IsDBNull(.Item("cp_CargoMaximo")) = True, 0, .Item("cp_CargoMaximo")))
                        obj.Saldo = CDec(IIf(IsDBNull(.Item("cp_Saldo")) = True, 0, .Item("cp_Saldo")))
                        obj.IdPersona = CInt(IIf(IsDBNull(.Item("IdPersona")) = True, 0, .Item("IdPersona")))
                        obj.IdMoneda = CInt(IIf(IsDBNull(.Item("IdMoneda")) = True, 0, .Item("IdMoneda")))
                        obj.MonedaSimbolo = CStr(IIf(IsDBNull(.Item("mon_Simbolo")) = True, "", .Item("mon_Simbolo")))
                        obj.Estado = CStr(IIf(IsDBNull(.Item("cp_Estado")) = True, "0", .Item("cp_Estado")))

                        obj.CuentaFormal = CBool(IIf(IsDBNull(.Item("cp_CuentaFormal")) = True, 0, .Item("cp_CuentaFormal")))
                        obj.IdTienda = CInt(IIf(IsDBNull(.Item("IdTienda")) = True, 0, .Item("IdTienda")))
                        obj.IdEmpresa = CInt(IIf(IsDBNull(.Item("IdEmpresa")) = True, 0, .Item("IdEmpresa")))
                        obj.IdSupervisor = CInt(IIf(IsDBNull(.Item("IdSupervisor")) = True, 0, .Item("IdSupervisor")))
                        obj.FechaAlta = CStr(IIf(IsDBNull(.Item("cp_FechaAlta")) = True, Nothing, .Item("cp_FechaAlta")))
                        obj.cp_ObServ = CStr(IIf(IsDBNull(.Item("cp_Observ")) = True, " ", .Item("cp_Observ")))

                        Lista.Add(obj)
                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectxIdCtaPersona(ByVal IdCtaPersona As Integer) As List(Of Entidades.CuentaPersona)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("[_CtaPerHistSel]", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idctaPersona", IdCtaPersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CuentaPersona)
                With lector
                    Do While .Read
                        Dim obj As New Entidades.CuentaPersona
                        obj.IdCuentaPersona = CInt(IIf(IsDBNull(.Item("IdCuentaPersona")) = True, 0, .Item("IdCuentaPersona")))
                        obj.CargoMaximo = CDec(IIf(IsDBNull(.Item("cp_CargoMaximo")) = True, 0, .Item("cp_CargoMaximo")))
                        obj.Saldo = CDec(IIf(IsDBNull(.Item("cp_Saldo")) = True, 0, .Item("cp_Saldo")))
                        obj.IdPersona = CInt(IIf(IsDBNull(.Item("IdPersona")) = True, 0, .Item("IdPersona")))
                        obj.IdMoneda = CInt(IIf(IsDBNull(.Item("IdMoneda")) = True, 0, .Item("IdMoneda")))
                        obj.MonedaSimbolo = CStr(IIf(IsDBNull(.Item("mon_Simbolo")) = True, "", .Item("mon_Simbolo")))
                        obj.Estado = CStr(IIf(IsDBNull(.Item("cp_Estado")) = True, "0", .Item("cp_Estado")))

                        obj.CuentaFormal = CBool(IIf(IsDBNull(.Item("cp_CuentaFormal")) = True, 0, .Item("cp_CuentaFormal")))
                        obj.IdTienda = CInt(IIf(IsDBNull(.Item("IdTienda")) = True, 0, .Item("IdTienda")))
                        obj.IdEmpresa = CInt(IIf(IsDBNull(.Item("IdEmpresa")) = True, 0, .Item("IdEmpresa")))
                        obj.IdSupervisor = CInt(IIf(IsDBNull(.Item("IdSupervisor")) = True, 0, .Item("IdSupervisor")))
                        obj.FechaAlta = CStr(IIf(IsDBNull(.Item("cp_FechaAlta")) = True, Nothing, .Item("cp_FechaAlta")))
                        obj.cp_ObServ = CStr(IIf(IsDBNull(.Item("cp_Observ")) = True, " ", .Item("cp_Observ")))
                        obj.TiendaNom = CStr(IIf(IsDBNull(.Item("TiendaNom")) = True, " ", .Item("TiendaNom")))

                        Lista.Add(obj)
                    Loop
                    .Close()
                End With

                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub InsertaCuentaPersonaT(ByVal obj As Entidades.CuentaPersona, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim par() As SqlParameter = New SqlParameter(8) {}

        par(0) = New SqlParameter("@IdMoneda", System.Data.SqlDbType.Int)
        par(0).Value = IIf(obj.IdMoneda = 0, DBNull.Value, obj.IdMoneda)
        par(1) = New SqlParameter("@cp_Estado", System.Data.SqlDbType.NVarChar, 2)
        par(1).Value = obj.Estado
        par(2) = New SqlParameter("@cp_CargoMaximo", System.Data.SqlDbType.Decimal)
        par(2).Value = obj.CargoMaximo
        par(3) = New SqlParameter("@IdTienda", System.Data.SqlDbType.Int)
        par(3).Value = IIf(obj.IdTienda = 0, DBNull.Value, obj.IdTienda)
        par(4) = New SqlParameter("@IdEmpresa", System.Data.SqlDbType.Int)
        par(4).Value = obj.IdEmpresa
        par(5) = New SqlParameter("@cp_CuentaFormal", System.Data.SqlDbType.Bit)
        par(5).Value = obj.CuentaFormal
        par(6) = New SqlParameter("@IdSupervisor", System.Data.SqlDbType.Int)
        par(6).Value = IIf(obj.IdSupervisor = 0, DBNull.Value, obj.IdSupervisor)
        par(7) = New SqlParameter("@IdPersona", System.Data.SqlDbType.Int)
        par(7).Value = obj.IdPersona
        par(8) = New SqlParameter("@Observ", System.Data.SqlDbType.VarChar)
        par(8).Value = obj.cp_ObServ


        HDAO.InsertaT(cn, "_CuentaPersonaInsert", par, tr)

    End Sub

    Public Sub UpdateCuentaPersonaT(ByVal obj As Entidades.CuentaPersona, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim par() As SqlParameter = New SqlParameter(10) {}

        par(0) = New SqlParameter("@IdMoneda", System.Data.SqlDbType.Int)
        par(0).Value = IIf(obj.IdMoneda = 0, DBNull.Value, obj.IdMoneda)
        par(1) = New SqlParameter("@cp_Estado", System.Data.SqlDbType.NVarChar, 2)
        par(1).Value = obj.Estado
        par(2) = New SqlParameter("@cp_CargoMaximo", System.Data.SqlDbType.Decimal)
        par(2).Value = obj.CargoMaximo
        par(3) = New SqlParameter("@IdTienda", System.Data.SqlDbType.Int)
        par(3).Value = IIf(obj.IdTienda = 0, DBNull.Value, obj.IdTienda)
        par(4) = New SqlParameter("@IdEmpresa", System.Data.SqlDbType.Int)
        par(4).Value = obj.IdEmpresa
        par(5) = New SqlParameter("@cp_CuentaFormal", System.Data.SqlDbType.Bit)
        par(5).Value = obj.CuentaFormal
        par(6) = New SqlParameter("@IdSupervisor", System.Data.SqlDbType.Int)
        par(6).Value = IIf(obj.IdSupervisor = 0, DBNull.Value, obj.IdSupervisor)
        par(7) = New SqlParameter("@IdPersona", System.Data.SqlDbType.Int)
        par(7).Value = obj.IdPersona
        par(8) = New SqlParameter("@cp_Saldo", System.Data.SqlDbType.Decimal)
        par(8).Value = obj.Saldo
        par(9) = New SqlParameter("@IdCuentaPersona", System.Data.SqlDbType.Int)
        par(9).Value = obj.IdCuentaPersona
        par(10) = New SqlParameter("@Observ", System.Data.SqlDbType.VarChar)
        par(10).Value = obj.cp_ObServ


        HDAO.UpdateT(cn, "_CuentaPersonaUpdate", par, tr)

    End Sub
    Public Sub DeletexCuentaPersonaxIdPersona(ByVal idpersona As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Dim cmd As New SqlCommand("_CuentaPersonaDeletexIdPersona", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", idpersona)
        If cmd.ExecuteNonQuery = 0 Then
            Throw New Exception
        End If
    End Sub
    Public Sub DeletexIdCuentaPersona(ByVal idcuentapersona As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)
        Try
            Dim cmd As New SqlCommand("_CuentaPersonaDeletexIdCuentaPersona", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdCuentaPersona", idcuentapersona)
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    '********************************************************
    'M�todo Insertar, para Insertar Saldo Pendiente
    'Autor   : Hans Fernando, Quispe Espinoza
    'M�dulo  : Caja-Documento
    'Sistema : Indusfer
    'Empresa : Digrafic SRL
    'Fecha   : 5-Set-2009
    'Parametros : poBEMovCuenta
    'Retorna : IdMovCuenta
    '********************************************************
    Public Function fnInsTblCuentaPersona(ByVal obj As Entidades.CuentaPersona) As Integer

        Dim resul As Integer
        'conexion
        Dim loTx As SqlTransaction
        Dim loCn As New SqlConnection()
        loCn.ConnectionString = objConexion.ConexionSIGE.ConnectionString.ToString()

        'parametros
        Dim par() As SqlParameter = New SqlParameter(7) {}

        par(0) = New SqlParameter("@IdMoneda", System.Data.SqlDbType.Int)
        par(0).Value = obj.IdMoneda
        par(1) = New SqlParameter("@cp_Estado", System.Data.SqlDbType.NVarChar, 2)
        par(1).Value = obj.Estado
        par(2) = New SqlParameter("@cp_CargoMaximo", System.Data.SqlDbType.Decimal)
        par(2).Value = obj.CargoMaximo
        par(3) = New SqlParameter("@IdTienda", System.Data.SqlDbType.Int)
        par(3).Value = obj.IdTienda
        par(4) = New SqlParameter("@IdEmpresa", System.Data.SqlDbType.Int)
        par(4).Value = obj.IdEmpresa
        par(5) = New SqlParameter("@cp_CuentaFormal", System.Data.SqlDbType.Bit)
        par(5).Value = obj.CuentaFormal
        par(6) = New SqlParameter("@IdSupervisor", System.Data.SqlDbType.Int)
        par(6).Value = obj.IdSupervisor
        par(7) = New SqlParameter("@IdPersona", System.Data.SqlDbType.Int)
        par(7).Value = obj.IdPersona

        loCn.Open()
        loTx = loCn.BeginTransaction()
        Try
            resul = Convert.ToInt32(SqlHelper.ExecuteScalar(loTx, CommandType.StoredProcedure, "_CuentaPersonaInsert", par))
            loTx.Commit()
        Catch e As Exception
            loTx.Rollback()
            Throw (e)
        Finally
            loCn.Close()
            loCn.Dispose()
        End Try
        Return (resul)
    End Function
    '********************************************************
    'M�todo Insertar, para Insertar Saldo Pendiente
    'Autor   : Hans Fernando, Quispe Espinoza
    'M�dulo  : Caja-Documento
    'Sistema : Indusfer
    'Empresa : Digrafic SRL
    'Fecha   : 5-Set-2009
    'Parametros : poBEMovCuenta
    'Retorna : IdMovCuenta
    '********************************************************
    Public Function fnUdpTblCuentaPersona(ByVal obj As Entidades.CuentaPersona) As Integer

        Dim resul As Integer
        'conexion
        Dim loTx As SqlTransaction
        Dim loCn As New SqlConnection()
        loCn.ConnectionString = objConexion.ConexionSIGE.ConnectionString.ToString()

        'parametros
        Dim par() As SqlParameter = New SqlParameter(9) {}

        par(0) = New SqlParameter("@IdMoneda", System.Data.SqlDbType.Int)
        par(0).Value = obj.IdMoneda
        par(1) = New SqlParameter("@cp_Estado", System.Data.SqlDbType.NVarChar, 2)
        par(1).Value = obj.Estado
        par(2) = New SqlParameter("@cp_CargoMaximo", System.Data.SqlDbType.Decimal)
        par(2).Value = obj.CargoMaximo
        par(3) = New SqlParameter("@IdTienda", System.Data.SqlDbType.Int)
        par(3).Value = obj.IdTienda
        par(4) = New SqlParameter("@IdEmpresa", System.Data.SqlDbType.Int)
        par(4).Value = obj.IdEmpresa
        par(5) = New SqlParameter("@cp_CuentaFormal", System.Data.SqlDbType.Bit)
        par(5).Value = obj.CuentaFormal
        par(6) = New SqlParameter("@IdSupervisor", System.Data.SqlDbType.Int)
        par(6).Value = obj.IdSupervisor
        par(7) = New SqlParameter("@IdPersona", System.Data.SqlDbType.Int)
        par(7).Value = obj.IdPersona
        par(8) = New SqlParameter("@cp_Saldo", System.Data.SqlDbType.Decimal)
        par(8).Value = obj.Saldo
        par(9) = New SqlParameter("@venta_mes", System.Data.SqlDbType.Decimal)
        par(9).Value = obj.Venta_mes
        par(10) = New SqlParameter("@venta_a�o", System.Data.SqlDbType.Decimal)
        par(10).Value = obj.Venta_a�o
        par(11) = New SqlParameter("@IdCuentaPersona", System.Data.SqlDbType.Int)
        par(11).Value = obj.IdCuentaPersona

        loCn.Open()
        loTx = loCn.BeginTransaction()
        Try
            resul = Convert.ToInt32(SqlHelper.ExecuteScalar(loTx, CommandType.StoredProcedure, "_CuentaPersonaUpdate", par))
            loTx.Commit()
        Catch e As Exception
            loTx.Rollback()
            Throw (e)
        Finally
            loCn.Close()
            loCn.Dispose()
        End Try
        Return (resul)
    End Function

    Public Function SelectxParams(ByVal IdPersona As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer) As List(Of Entidades.CuentaPersona)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CuentaPersonaSelectxParams", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CuentaPersona)
                With lector
                    Do While .Read
                        Dim obj As New Entidades.CuentaPersona
                        obj.IdCuentaPersona = CInt(IIf(IsDBNull(.Item("IdCuentaPersona")) = True, 0, .Item("IdCuentaPersona")))
                        obj.IdMoneda = CInt(IIf(IsDBNull(.Item("IdMoneda")) = True, 0, .Item("IdMoneda")))
                        obj.IdPersona = CInt(IIf(IsDBNull(.Item("IdPersona")) = True, 0, .Item("IdPersona")))
                        obj.MonedaSimbolo = CStr(IIf(IsDBNull(.Item("mon_Simbolo")) = True, "", .Item("mon_Simbolo")))
                        obj.CargoMaximo = CDec(IIf(IsDBNull(.Item("cp_CargoMaximo")) = True, 0, .Item("cp_CargoMaximo")))
                        obj.Saldo = CDec(IIf(IsDBNull(.Item("cp_Saldo")) = True, 0, .Item("cp_Saldo")))

                        obj.IdEmpresa = CInt(IIf(IsDBNull(.Item("IdEmpresa")) = True, 0, .Item("IdEmpresa")))
                        obj.IdTienda = CInt(IIf(IsDBNull(.Item("IdTienda")) = True, 0, .Item("IdTienda")))

                        obj.CuentaFormal = CBool(IIf(IsDBNull(.Item("cp_CuentaFormal")) = True, 0, .Item("cp_CuentaFormal")))
                        obj.IdSupervisor = CInt(IIf(IsDBNull(.Item("IdSupervisor")) = True, 0, .Item("IdSupervisor")))
                        obj.FechaAlta = CStr(IIf(IsDBNull(.Item("cp_FechaAlta")) = True, Nothing, .Item("cp_FechaAlta")))
                      
                        Lista.Add(obj)
                    Loop
                    .Close()
                End With
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ULTIMAS_VENTASFUNCION(ByVal IdPersona As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer) As List(Of Entidades.CuentaPersona)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CR_ULTIMAS_VENTASFUNCION", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idpersona", IdPersona)
        cmd.Parameters.AddWithValue("@idempresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@idtienda", IdTienda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CuentaPersona)
                With lector
                    Do While .Read
                        Dim obj As New Entidades.CuentaPersona
                        obj.Venta_mes = CDec(IIf(IsDBNull(.Item("venta_mes")) = True, 0, .Item("venta_mes")))
                        obj.Venta_a�o = CDec(IIf(IsDBNull(.Item("venta_a�o")) = True, 0, .Item("venta_a�o")))

                        Lista.Add(obj)
                    Loop
                    .Close()
                End With
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function Mostrarventaxpersona(ByVal IdPersona As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer) As List(Of Entidades.CuentaPersona)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CR_ULTIMAS_VENTASDETALLE", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idpersona", IdPersona)
        cmd.Parameters.AddWithValue("@idempresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@idtienda", IdTienda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CuentaPersona)
                With lector
                    Do While .Read
                        Dim obj As New Entidades.CuentaPersona
                        obj.tdoc_NombreCorto = CStr(IIf(IsDBNull(.Item("tdoc_NombreCorto")) = True, "", .Item("tdoc_NombreCorto")))
                        obj.Numero = CStr(IIf(IsDBNull(.Item("Numero")) = True, "", .Item("Numero")))
                        obj.doc_FechaEmision = CStr(IIf(IsDBNull(.Item("doc_FechaEmision")) = True, "", .Item("doc_FechaEmision")))
                        obj.dc_UMedida = CStr(IIf(IsDBNull(.Item("dc_UMedida")) = True, "", .Item("dc_UMedida")))
                        obj.prod_Nombre = CStr(IIf(IsDBNull(.Item("prod_Nombre")) = True, "", .Item("prod_Nombre")))
                        obj.dc_Cantidad = CDec(IIf(IsDBNull(.Item("dc_Cantidad")) = True, 0, .Item("dc_Cantidad")))
                        obj.dc_PrecioCD = CDec(IIf(IsDBNull(.Item("dc_PrecioCD")) = True, 0, .Item("dc_PrecioCD")))
                        obj.total = CDec(IIf(IsDBNull(.Item("total")) = True, 0, .Item("total")))
                        obj.totalapagar = CDec(IIf(IsDBNull(.Item("totalapagar")) = True, 0, .Item("totalapagar")))

                        Lista.Add(obj)
                    Loop
                    .Close()
                End With
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function SelectxcomprasParams(ByVal IdPersona As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer) As List(Of Entidades.CuentaPersona)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ComprasPersonaSelectxParams", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idpersona", IdPersona)
        cmd.Parameters.AddWithValue("@idempresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@idtienda", IdTienda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CuentaPersona)
                With lector
                    Do While .Read
                        Dim obj As New Entidades.CuentaPersona
                        obj.IdCuentaPersona = CInt(IIf(IsDBNull(.Item("IdCuentaPersona")) = True, 0, .Item("IdCuentaPersona")))
                        obj.IdMoneda = CInt(IIf(IsDBNull(.Item("IdMoneda")) = True, 0, .Item("IdMoneda")))
                        obj.IdPersona = CInt(IIf(IsDBNull(.Item("IdPersona")) = True, 0, .Item("IdPersona")))
                        obj.MonedaSimbolo = CStr(IIf(IsDBNull(.Item("mon_Simbolo")) = True, "", .Item("mon_Simbolo")))
                        obj.CargoMaximo = CDec(IIf(IsDBNull(.Item("cp_CargoMaximo")) = True, 0, .Item("cp_CargoMaximo")))
                        obj.Saldo = CDec(IIf(IsDBNull(.Item("cp_Saldo")) = True, 0, .Item("cp_Saldo")))

                        obj.IdEmpresa = CInt(IIf(IsDBNull(.Item("IdEmpresa")) = True, 0, .Item("IdEmpresa")))
                        obj.IdTienda = CInt(IIf(IsDBNull(.Item("IdTienda")) = True, 0, .Item("IdTienda")))

                        obj.CuentaFormal = CBool(IIf(IsDBNull(.Item("cp_CuentaFormal")) = True, 0, .Item("cp_CuentaFormal")))
                        obj.IdSupervisor = CInt(IIf(IsDBNull(.Item("IdSupervisor")) = True, 0, .Item("IdSupervisor")))
                        obj.FechaAlta = CStr(IIf(IsDBNull(.Item("cp_FechaAlta")) = True, Nothing, .Item("cp_FechaAlta")))
                        obj.Venta_mes = CDec(IIf(IsDBNull(.Item("venta_mes")) = True, 0, .Item("venta_mes")))
                        obj.Venta_a�o = CDec(IIf(IsDBNull(.Item("venta_a�o")) = True, 0, .Item("venta_a�o")))

                        Lista.Add(obj)
                    Loop
                    .Close()
                End With
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Mostrarventaxpersonaxa�o(ByVal IdPersona As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer) As List(Of Entidades.CuentaPersona)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CR_ULTIMAS_VENTAS_A�O", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idpersona", IdPersona)
        cmd.Parameters.AddWithValue("@idempresa", IdEmpresa)
        cmd.Parameters.AddWithValue("@idtienda", IdTienda)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.CuentaPersona)
                With lector
                    Do While .Read
                        Dim obj As New Entidades.CuentaPersona
                        obj.tdoc_NombreCorto = CStr(IIf(IsDBNull(.Item("tdoc_NombreCorto")) = True, "", .Item("tdoc_NombreCorto")))
                        obj.prod_Nombre = CStr(IIf(IsDBNull(.Item("prod_Nombre")) = True, "", .Item("prod_Nombre")))
                        obj.dc_Cantidad = CDec(IIf(IsDBNull(.Item("dc_Cantidad")) = True, 0, .Item("dc_Cantidad")))
                        obj.dc_PrecioCD = CDec(IIf(IsDBNull(.Item("dc_PrecioCD")) = True, 0, .Item("dc_PrecioCD")))
                        obj.total = CDec(IIf(IsDBNull(.Item("total")) = True, 0, .Item("total")))
                        Lista.Add(obj)
                    Loop
                    .Close()
                End With
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
