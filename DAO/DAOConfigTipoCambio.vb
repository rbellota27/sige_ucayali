﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOConfigTipoCambio

    Private objConexion As New Conexion

    Public Sub ConfigTipoCambioUpdate(ByVal obj As Entidades.ConfigTipoCambio, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)


        Dim cmd As New SqlCommand("_ConfigTipoCambioUpdate", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.AddWithValue("@ctc_CompraC", obj.CompraC)
        cmd.Parameters.AddWithValue("@ctc_CompraOf", obj.CompraOf)
        cmd.Parameters.AddWithValue("@ctc_VentaC", obj.VentaC)
        cmd.Parameters.AddWithValue("@ctc_VentaOf", obj.VentaOf)
        cmd.Parameters.AddWithValue("@IdConfigTipoCambio", obj.IdConfigTipoCambio)

        cmd.Parameters.AddWithValue("@IdUsuario", IIf(obj.IdUsuario = Nothing, DBNull.Value, obj.IdUsuario))




        If cmd.ExecuteNonQuery <= 0 Then

            Throw New Exception

        End If

    End Sub



    Public Function SelectAll() As List(Of Entidades.ConfigTipoCambio)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ConfigTipoCambioSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ConfigTipoCambio)
                While lector.Read
                    Dim obj As New Entidades.ConfigTipoCambio
                    With obj

                        .CompraC = CBool(IIf(IsDBNull(lector.Item("ctc_CompraC")) = True, False, lector.Item("ctc_CompraC")))
                        .CompraOf = CBool(IIf(IsDBNull(lector.Item("ctc_CompraOf")) = True, False, lector.Item("ctc_CompraOf")))
                        .VentaC = CBool(IIf(IsDBNull(lector.Item("ctc_VentaC")) = True, False, lector.Item("ctc_VentaC")))
                        .VentaOf = CBool(IIf(IsDBNull(lector.Item("ctc_VentaOf")) = True, False, lector.Item("ctc_VentaOf")))
                        .IdConfigTipoCambio = CInt(IIf(IsDBNull(lector.Item("IdConfigTipoCambio")) = True, 0, lector.Item("IdConfigTipoCambio")))
                        .Descripcion = CStr(IIf(IsDBNull(lector.Item("ctc_Descripcion")) = True, "", lector.Item("ctc_Descripcion")))

                        .NombreComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                        .NombreNatural = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                        .RazonSocial = CStr(IIf(IsDBNull(lector.Item("jur_Rsocial")) = True, "", lector.Item("jur_Rsocial")))

                        .IdUsuario = CInt(IIf(IsDBNull(lector.Item("IdUsuario")) = True, 0, lector.Item("IdUsuario")))

                    End With
                    Lista.Add(obj)
                End While
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function




End Class
