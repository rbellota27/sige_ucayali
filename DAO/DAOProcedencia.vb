﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOProcedencia
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function SelectAllActivo_Cbo() As List(Of Entidades.Procedencia)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProcedenciaSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Procedencia)
                Do While lector.Read
                    Dim objProcedencia As New Entidades.Procedencia
                    objProcedencia.IdProcedencia = CStr(lector.Item("IdProcedencia"))
                    objProcedencia.Nombre = CStr(lector.Item("proc_Nombre"))
                    objProcedencia.Estado = CBool((lector.Item("proc_Estado")))
                    Lista.Add(objProcedencia)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function PerfilUsuarioSelectxIdPersona(ByVal idpersona As Integer) As List(Of Entidades.Procedencia)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProcedenciaSelectAllActivoxPais", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPais", idpersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Procedencia)
                Do While lector.Read
                    Dim obj As New Entidades.Procedencia(CStr(lector.Item("IdProcedencia")), CStr(lector.Item("proc_Nombre")), CBool(lector.Item("proc_Estado")))
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SelectAllActivoxIdPais(ByVal idpais As String) As List(Of Entidades.Procedencia)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProcedenciaSelectAllActivoxIdPais", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPais", idpais)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Procedencia)
                Do While lector.Read
                    Dim objPais As New Entidades.Procedencia
                    objPais.IdProcedencia = CStr(lector.Item("IdProcedencia"))
                    objPais.Nombre = CStr(IIf(IsDBNull(lector.Item("proc_Nombre")) = True, "", lector.Item("proc_Nombre")))
                    objPais.Estado = CBool(IIf(IsDBNull(lector.Item("proc_Estado")) = True, "", lector.Item("proc_Estado")))
                    Lista.Add(objPais)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function Insert(ByVal cn As SqlConnection, ByVal obj As Entidades.Procedencia, ByVal tr As SqlTransaction) As Boolean
        Dim cmd As New SqlCommand("_ProcedenciaInsert", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdProcedencia", obj.IdProcedencia)
        cmd.Parameters.AddWithValue("@Nombre", obj.Nombre)
        cmd.Parameters.AddWithValue("@Estado", obj.Estado)
        If cmd.ExecuteNonQuery = 0 Then
            Throw New Exception
        End If
        Return True
    End Function

End Class
