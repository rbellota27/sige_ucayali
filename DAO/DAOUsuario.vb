'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.


'********************   JUEVES 10 JUNIO 2010 HORA 12_46 PM

Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAOUsuario

    Private HDAO As New DAO.HelperDAO
    Private objConexion As New Conexion
    Private objDaoMantenedor As New DAO.DAOMantenedor
    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE

    Public Function SelectCboAtencion() As List(Of Entidades.Usuario)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_AtencionUsuarioSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Usuario)
                Do While lector.Read
                    Dim objusu As New Entidades.Usuario
                    objusu.IdPersona = CInt(lector.Item("IdPersona"))
                    objusu.Nombres = CStr(lector.Item("Nombre"))
                    Lista.Add(objusu)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function ActualizaUsuarioAtencionPago(ByVal ObjUsu As Entidades.Usuario) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(0).Value = ObjUsu.IdPersona
        ArrayParametros(1) = New SqlParameter("@flagAtencionPago", SqlDbType.Int)
        ArrayParametros(1).Value = ObjUsu.FlagAtencionPago
        Return HDAO.Update(cn, "UpdateUsuarioFlagAtencionPago", ArrayParametros)
    End Function


    Public Function Usuario_SelectxIdEmpresaxIdTiendaxIdPerfil_Distinct(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPerfil As Integer) As DataTable

        Dim ds As New DataSet

        Try

            Dim p() As SqlParameter = New SqlParameter(3) {}
            p(0) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdPerfil, "@IdPerfil", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(Nothing, "@IdPersona", SqlDbType.Int)

            Dim cmd As New SqlCommand("_Usuario_SelectxIdEmpresaxIdTiendaxIdPerfil_Distinct", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Usuario")

        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return ds.Tables("DT_Usuario")

    End Function

    Public Function Usuario_SelectxIdPersona(ByVal IdPersona As Integer) As Entidades.Comisionista

        Dim objComisionista As Entidades.Comisionista = Nothing

        Try


            Dim p() As SqlParameter = New SqlParameter(3) {}
            p(0) = objDaoMantenedor.getParam(Nothing, "@IdEmpresa", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(Nothing, "@IdTienda", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(Nothing, "@IdPerfil", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(IdPersona, "@IdPersona", SqlDbType.Int)

            cn.Open()
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_Usuario_SelectxIdEmpresaxIdTiendaxIdPerfil", p)

            If (reader.Read) Then

                objComisionista = New Entidades.Comisionista
                With objComisionista

                    .IdPersona = objDaoMantenedor.UCInt(reader("IdPersona"))
                    .Descripcion = objDaoMantenedor.UCStr(reader("Descripcion"))
                    .Dni = objDaoMantenedor.UCStr(reader("Dni"))
                    .Empresa = objDaoMantenedor.UCStr(reader("Empresa"))
                    .Tienda = objDaoMantenedor.UCStr(reader("Tienda"))
                    .Perfil = objDaoMantenedor.UCStr(reader("Perfil"))

                End With

            End If
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objComisionista

    End Function
    Public Function Usuario_SelectxIdEmpresaxIdTiendaxIdPerfil_DT(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPerfil As Integer) As DataTable

        Dim ds As New DataSet

        Try

            Dim p() As SqlParameter = New SqlParameter(3) {}
            p(0) = objDaoMantenedor.getParam(IdEmpresa, "@IdEmpresa", SqlDbType.Int)
            p(1) = objDaoMantenedor.getParam(IdTienda, "@IdTienda", SqlDbType.Int)
            p(2) = objDaoMantenedor.getParam(IdPerfil, "@IdPerfil", SqlDbType.Int)
            p(3) = objDaoMantenedor.getParam(Nothing, "@IdPersona", SqlDbType.Int)

            Dim cmd As New SqlCommand("_Usuario_SelectxIdEmpresaxIdTiendaxIdPerfil", cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(p)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Usuario")

        Catch ex As Exception
            Throw ex
        Finally

        End Try

        Return ds.Tables("DT_Usuario")

    End Function

    Public Function ChangePassword(ByVal IdUsuario As Integer, ByVal ClaveOld As String, ByVal ClaveNew As String, ByVal ClaveNewConfirm As String, ByVal cn As SqlConnection) As Boolean

        Dim p() As SqlParameter = New SqlParameter(3) {}

        p(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        p(0).Value = IIf(IdUsuario = Nothing, DBNull.Value, IdUsuario)

        p(1) = New SqlParameter("@ClaveOld", SqlDbType.VarChar)
        p(1).Value = IIf(ClaveOld = Nothing, DBNull.Value, ClaveOld)

        p(2) = New SqlParameter("@ClaveNew", SqlDbType.VarChar)
        p(2).Value = IIf(ClaveNew = Nothing, DBNull.Value, ClaveNew)

        p(3) = New SqlParameter("@ClaveNewConfirm", SqlDbType.VarChar)
        p(3).Value = IIf(ClaveNewConfirm = Nothing, DBNull.Value, ClaveNewConfirm)

        Dim cmd As New SqlCommand("_UsuarioChangePassword", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(p)
        If (cmd.ExecuteNonQuery <= 0) Then
            Return False
        Else
            Return True
        End If

    End Function

    Public Function ValidarPermisoxIdUsuarioxIdPermiso(ByVal IdUsuario As Integer, ByVal IdPermiso As Integer, ByVal cn As SqlConnection) As Integer

        Dim cmd As New SqlCommand("PerfilUsuario", cn)
        cmd.CommandType = CommandType.Text
        cmd.CommandText = " select dbo._fx_ValidarPermisoxIdUsuarioxIdPermiso ( " + CStr(IdUsuario) + "," + CStr(IdPermiso) + ")"

        Return CInt(cmd.ExecuteScalar)

    End Function


    Public Function ValidarPermisosRolTipoPv(ByVal IdUsuario As Integer, ByVal cn As SqlConnection) As Integer

        Dim cmd As New SqlCommand("PerfilUsuario", cn)

        cmd.CommandType = CommandType.Text

        cmd.CommandText = " select dbo._fx_ValidarPermisoRolTipoxIdUsuario ( " + CStr(IdUsuario) + ")"

        Return CInt(cmd.ExecuteScalar)

    End Function

    Public Function ValidarEdicionDocxIdUsuarioxIdPermiso(ByVal IdUsuario As Integer, ByVal IdPermiso As Integer, ByVal doc_serie As Integer, _
                                                          ByVal doc_codigo As Integer, ByVal idTIpodocumento As Integer, ByVal cn As SqlConnection) As Integer

        Dim cmd As New SqlCommand("PerfilUsuario", cn)
        cmd.CommandType = CommandType.Text
        cmd.CommandText = " select dbo.fun_validaEdicion( " + CStr(IdUsuario) + "," + CStr(IdPermiso) + "," + CStr(doc_serie) + "," + CStr(doc_codigo) + "," + CStr(idTIpodocumento) + ")"

        Return CInt(cmd.ExecuteScalar)

    End Function

    Public Function ValidarPermisoxIdUsuarioxIdPermisoxTipoOperacion(ByVal IdUsuario As Integer, ByVal CadenaIdPermiso As String, ByVal cn As SqlConnection) As List(Of Entidades.be_validacion)
        Dim lista As List(Of Entidades.be_validacion) = Nothing
        Dim cs As String = "select * from dbo.fun_ValidarPermisoxIdUsuarioxIdPermisoxTipoOperacion(@idusuario,@idpermiso)"
        Using cmd As New SqlCommand(cs, cn)
            cmd.CommandType = CommandType.Text

            cmd.Parameters.Add(New SqlParameter("@idusuario", SqlDbType.Int)).Value = IdUsuario
            cmd.Parameters.Add(New SqlParameter("@idpermiso", SqlDbType.NVarChar)).Value = CadenaIdPermiso

            Dim rdr As SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)
            If rdr IsNot Nothing Then
                lista = New List(Of Entidades.be_validacion)
                Dim objeto As Entidades.be_validacion = Nothing
                While rdr.Read()
                    objeto = New Entidades.be_validacion
                    With objeto
                        .idPermiso = rdr.GetInt32(0)
                        .idPerfil = rdr.GetInt32(1)
                        .Id = rdr.GetInt32(2)
                        .Nombre = rdr.GetString(3)
                    End With
                    lista.Add(objeto)
                End While
                rdr.Close()
            End If
        End Using
        Return lista
    End Function

    Public Function InsertaUsuario(ByVal usuario As Entidades.Usuario, _
    ByVal lista As List(Of Entidades.PerfilUsuario), _
    ByVal lista_area As List(Of Entidades.Usuario_Area)) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim ArrayParametros() As SqlParameter = New SqlParameter(6) {}
            ArrayParametros(0) = New SqlParameter("@us_Login", SqlDbType.VarChar)
            ArrayParametros(0).Value = IIf(usuario.Login = Nothing, DBNull.Value, usuario.Login)
            ArrayParametros(1) = New SqlParameter("@us_FechaAlta", SqlDbType.DateTime)
            ArrayParametros(1).Value = IIf(usuario.FechaAlta = Nothing, DBNull.Value, usuario.FechaAlta)
            ArrayParametros(2) = New SqlParameter("@us_FechaBaja", SqlDbType.DateTime)
            ArrayParametros(2).Value = IIf(usuario.FechaBaja = Nothing, DBNull.Value, usuario.FechaBaja)
            ArrayParametros(3) = New SqlParameter("@us_Clave", SqlDbType.VarChar)
            ArrayParametros(3).Value = IIf(usuario.Clave = Nothing, DBNull.Value, usuario.Clave)
            ArrayParametros(4) = New SqlParameter("@us_Estado", SqlDbType.Char)
            ArrayParametros(4).Value = IIf(usuario.Estado = Nothing, DBNull.Value, usuario.Estado)
            ArrayParametros(5) = New SqlParameter("@IdPersona", SqlDbType.Int)
            ArrayParametros(5).Value = IIf(usuario.IdPersona = Nothing, DBNull.Value, usuario.IdPersona)
            ArrayParametros(6) = New SqlParameter("@IdMotivoBaja", SqlDbType.Int)
            ArrayParametros(6).Value = IIf(usuario.IdMotivoBaja = Nothing, DBNull.Value, usuario.IdMotivoBaja)
            HDAO.InsertaT(cn, "_UsuarioInsert", ArrayParametros, tr)

            '**************inserto en Perfilusuario

            Dim objDaoPerfilUsuario As New DAO.DAOPerfilUsuario
            For i As Integer = 0 To lista.Count - 1
                objDaoPerfilUsuario.Insert(cn, lista(i), tr)
            Next
            '**************inserto en Usuario_Area
            If Not lista_area Is Nothing Then
                Dim objDaoUsuario_Area As New DAO.DAOUsuario_Area
                For i As Integer = 0 To lista_area.Count - 1
                    objDaoUsuario_Area.Insert(cn, lista_area(i), tr)
                Next
            End If

            tr.Commit()
            Return True
        Catch ex As Exception
            tr.Rollback()
            Return False
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function TodosUsuarios(ByVal IdPersona As Integer) As Entidades.Usuario
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("usp_SelectUsuarios", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Try
            cn.Open()
            Using cn
                Dim lector As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                lector.Read()
                Dim Usu As New Entidades.Usuario
                With Usu
                    .IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, "", lector.Item("IdPersona")))
                    .CorreoUsu = CStr(IIf(IsDBNull(lector.Item("corr_Nombre")) = True, "", lector.Item("corr_Nombre")))
                    .Nombres = CStr(IIf(IsDBNull(lector.Item("Nombres")) = True, "", lector.Item("Nombres")))
                End With
                Return Usu
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function

    Public Function ActualizaUsuario(ByVal usuario As Entidades.Usuario, _
    ByVal lista As List(Of Entidades.PerfilUsuario), _
    ByVal lista_area As List(Of Entidades.Usuario_Area), _
    ByVal UpdatePassword As Boolean) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim objDaoPerfilUsuario As New DAO.DAOPerfilUsuario
        Dim objDaoAreaUsuario As New DAO.DAOUsuario_Area

        '************************* obtengo la lista Perfil original
        Dim listaOld As List(Of Entidades.PerfilUsuario) = objDaoPerfilUsuario.PerfilUsuarioSelectxIdPersona(usuario.IdPersona)
        Dim i As Integer = listaOld.Count - 1
        While i >= 0
            Dim j As Integer = lista.Count - 1
            While j >= 0
                If listaOld.Item(i).IdPerfil = lista.Item(j).IdPerfil And listaOld.Item(i).IdPersona = lista.Item(j).IdPersona Then
                    listaOld.RemoveAt(i)
                    Exit While
                End If
                j = j - 1
            End While
            i = i - 1
        End While


        '************************* obtengo la lista_Area original
        Dim lista_AreaOld As List(Of Entidades.Usuario_Area) = objDaoAreaUsuario.SelectxIdUsuario(usuario.IdPersona)
        i = lista_AreaOld.Count - 1
        While i >= 0
            Dim j As Integer = lista_area.Count - 1
            While j >= 0
                If lista_AreaOld.Item(i).IdArea = lista_area.Item(j).IdArea And lista_AreaOld.Item(i).IdUsuario = lista_area.Item(j).IdUsuario Then
                    lista_AreaOld.RemoveAt(i)
                    Exit While
                End If
                j = j - 1
            End While
            i = i - 1
        End While

        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim cmd As New SqlCommand("_UsuarioUpdate", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            Dim ArrayParametros() As SqlParameter = New SqlParameter(7) {}
            ArrayParametros(0) = New SqlParameter("@us_Login", SqlDbType.VarChar)
            ArrayParametros(0).Value = IIf(usuario.Login = Nothing, DBNull.Value, usuario.Login)
            ArrayParametros(1) = New SqlParameter("@us_FechaAlta", SqlDbType.DateTime)
            ArrayParametros(1).Value = IIf(usuario.FechaAlta = Nothing, DBNull.Value, usuario.FechaAlta)
            ArrayParametros(2) = New SqlParameter("@us_FechaBaja", SqlDbType.DateTime)
            ArrayParametros(2).Value = IIf(usuario.FechaBaja = Nothing, DBNull.Value, usuario.FechaBaja)
            ArrayParametros(3) = New SqlParameter("@us_Clave", SqlDbType.VarChar)
            ArrayParametros(3).Value = IIf(usuario.Clave = Nothing, DBNull.Value, usuario.Clave)
            ArrayParametros(4) = New SqlParameter("@us_Estado", SqlDbType.Char)
            ArrayParametros(4).Value = IIf(usuario.Estado = Nothing, DBNull.Value, usuario.Estado)
            ArrayParametros(5) = New SqlParameter("@IdPersona", SqlDbType.Int)
            ArrayParametros(5).Value = IIf(usuario.IdPersona = Nothing, DBNull.Value, usuario.IdPersona)
            ArrayParametros(6) = New SqlParameter("@IdMotivoBaja", SqlDbType.Int)
            ArrayParametros(6).Value = IIf(usuario.IdMotivoBaja = Nothing, DBNull.Value, usuario.IdMotivoBaja)
            ArrayParametros(7) = New SqlParameter("@UpdatePassword", SqlDbType.Bit)
            ArrayParametros(7).Value = UpdatePassword
            cmd.Parameters.AddRange(ArrayParametros)
            If cmd.ExecuteNonQuery = 0 Then
                Throw New Exception
            End If

            '********************* elimino los registros no existentes
            For i = 0 To listaOld.Count - 1
                objDaoPerfilUsuario.DeletexIdPersonaxIdPerfil(cn, listaOld(i), tr)
            Next

            '********************* registro los nuevos productos
            Dim objUtil As New DAO.DAOUtil
            For i = 0 To lista.Count - 1
                If objUtil.ValidarxDosParametros(cn, tr, "PerfilUsuario", "IdPersona", lista(i).IdPersona.ToString, "IdPerfil", lista(i).IdPerfil.ToString) > 0 Then
                    '***************** actualizo
                    objDaoPerfilUsuario.Update(cn, lista(i), tr)
                Else
                    '****************** inserto
                    objDaoPerfilUsuario.Insert(cn, lista(i), tr)
                End If
            Next

            '********************* elimino los registros no existentes de la lista area
            For i = 0 To lista_AreaOld.Count - 1
                objDaoAreaUsuario.DeletexIdUsuarioxIdArea(cn, lista_AreaOld(i), tr)
            Next

            '********************* registro los nuevos productos - area

            For i = 0 To lista_area.Count - 1
                If objUtil.ValidarxDosParametros(cn, tr, "Usuario_Area", "IdUsuario", lista_area(i).IdUsuario.ToString, "IdArea", lista_area(i).IdArea.ToString) > 0 Then
                    '***************** actualizo
                    objDaoAreaUsuario.Update(cn, lista_area(i), tr)
                Else
                    '****************** inserto
                    objDaoAreaUsuario.Insert(cn, lista_area(i), tr)
                End If
            Next

            tr.Commit()
            Return True
        Catch ex As Exception
            tr.Rollback()
            Return False
        Finally

        End Try
    End Function
    Public Function ValidarIdentidad(ByVal Login As String, ByVal Clave As String) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UsuarioValidarIdentidad", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@us_Login", Login)
        cmd.Parameters.AddWithValue("@us_Clave", Clave)
        cmd.Parameters.AddWithValue("@retorno", 0).Direction = ParameterDirection.Output

        Using cn
            cn.Open()
            cmd.ExecuteNonQuery()
            cn.Close()
            Return CInt(IIf(IsDBNull(cmd.Parameters("@retorno").Value) = True, 0, cmd.Parameters("@retorno").Value))
        End Using
    End Function



    Public Function ValidarIdentidadPerfil(ByVal Login As String, ByVal Clave As String) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UsuarioValidarIdentidadPerfil", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@us_Login", Login)
        cmd.Parameters.AddWithValue("@us_Clave", Clave)
        cmd.Parameters.AddWithValue("@retorno", 0).Direction = ParameterDirection.Output

        'cmd.Parameters.AddWithValue(Ap(i).ParameterName, 0).Direction = ParameterDirection.Output
        'nomParameterOutPut = Ap(i).ParameterName

        'cmd.Parameters.Add(New SqlParameter("@retorno", SqlDbType.Int, 9, CStr(ParameterDirection.Output)))
        'cmd.Parameters(1).Direction = ParameterDirection.Output

        Using cn
            cn.Open()
            cmd.ExecuteNonQuery()
            cn.Close()
            Return CInt(IIf(IsDBNull(cmd.Parameters("@retorno").Value) = True, 0, cmd.Parameters("@retorno").Value))
        End Using
    End Function

    Public Function ValidarIdentidadPerfilPermisoMasivo(ByVal IdUsuario As Integer) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UsuarioValidarIdentidadPerfilPermisoMasivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario)
        cmd.Parameters.AddWithValue("@retorno", 0).Direction = ParameterDirection.Output

        Using cn
            cn.Open()
            cmd.ExecuteNonQuery()
            cn.Close()
            Return CInt(IIf(IsDBNull(cmd.Parameters("@retorno").Value) = True, 0, cmd.Parameters("@retorno").Value))
        End Using
    End Function


    Public Function SelectCbo() As List(Of Entidades.Natural)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UsuarioSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Natural)
                Do While lector.Read
                    Dim natural As New Entidades.Natural
                    natural.IdPersona = CInt(lector.Item("IdPersona"))
                    natural.Nombres = CStr(IIf(IsDBNull(lector.Item("Nombre")) = True, "", lector.Item("Nombre")))
                    Lista.Add(natural)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

    Public Function SelectUsuarioTiendaAreaxIdPersona(ByVal IdPersona As Integer) As List(Of Entidades.UsuarioTiendaArea)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UsuarioTiendaAreaSelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.UsuarioTiendaArea)
                Do While lector.Read
                    Dim UsuarioTiendaArea As New Entidades.UsuarioTiendaArea

                    UsuarioTiendaArea.IdArea = CInt(CStr(lector.Item("IdArea")))
                    UsuarioTiendaArea.IdTienda = CInt(lector.Item("IdTienda"))
                    UsuarioTiendaArea.IdPerfil = CInt(CStr(lector.Item("IdPerfil")))

                    Lista.Add(UsuarioTiendaArea)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Function

End Class
