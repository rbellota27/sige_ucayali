'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.


'************  VIERNES 04 06 2010

Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class DAOPersona
    Private HDAO As New DAO.HelperDAO
    Private objConexion As New Conexion
    Private objDaoMantenedor As New DAO.DAOMantenedor
    Private sqlCn As SqlConnection = objConexion.ConexionSIGE
    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader = Nothing

    Public Function PersonaDato(ByVal ObjPersonaDato As Entidades.PersonaDato) As List(Of Entidades.PersonaDato)
        Dim listaDatoPersona As New List(Of Entidades.PersonaDato)
        Dim obj As Entidades.PersonaDato

        Dim sqlPrm() As SqlParameter = New SqlParameter(5) {}
        sqlPrm(0) = New SqlParameter("@IdPersona", SqlDbType.Int)
        sqlPrm(0).Value = IIf(ObjPersonaDato.IdPersona = Nothing, DBNull.Value, ObjPersonaDato.IdPersona)
        sqlPrm(1) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        sqlPrm(1).Value = IIf(ObjPersonaDato.IdEmpresa = Nothing, DBNull.Value, ObjPersonaDato.IdEmpresa)
        sqlPrm(2) = New SqlParameter("@IdTienda", SqlDbType.Int)
        sqlPrm(2).Value = IIf(ObjPersonaDato.IdTienda = Nothing, DBNull.Value, ObjPersonaDato.IdTienda)
        sqlPrm(3) = New SqlParameter("@IdRol", SqlDbType.Int)
        sqlPrm(3).Value = IIf(ObjPersonaDato.IdRol = Nothing, DBNull.Value, ObjPersonaDato.IdRol)
        sqlPrm(4) = New SqlParameter("@IdPerfil", SqlDbType.Int)
        sqlPrm(4).Value = IIf(ObjPersonaDato.IdPerfil = Nothing, DBNull.Value, ObjPersonaDato.IdPerfil)
        sqlPrm(5) = New SqlParameter("@IdTipoTelefono", SqlDbType.Int)
        sqlPrm(5).Value = IIf(ObjPersonaDato.IdTipoTelefono = Nothing, DBNull.Value, ObjPersonaDato.IdTipoTelefono)

        sqlCn.Open()
        sqlCmd = New SqlCommand("[dbo].[_Persona_x]", sqlCn)
        sqlCmd.CommandType = CommandType.StoredProcedure
        sqlCmd.CommandTimeout = 0
        sqlCmd.Parameters.AddRange(sqlPrm)
        Try
            sqlReader = sqlCmd.ExecuteReader(CommandBehavior.CloseConnection)
            Do While sqlReader.Read
                obj = New Entidades.PersonaDato
                With obj
                    .IdPersona = CInt(IIf(IsDBNull(sqlReader("IdPersona")), 0, sqlReader("IdPersona")))
                    .Descripcion = CStr(IIf(IsDBNull(sqlReader("Descripcion")), 0, sqlReader("Descripcion")))
                    '.Str_IdTienda = CStr(IIf(IsDBNull(sqlReader("IdTienda")), "", sqlReader("IdTienda")))
                    '.Str_Tienda = CStr(IIf(IsDBNull(sqlReader("Tienda")), "", sqlReader("Tienda")))
                    '.Str_IdRol = CStr(IIf(IsDBNull(sqlReader("IdRol")), "", sqlReader("IdRol")))
                    '.Str_Rol = CStr(IIf(IsDBNull(sqlReader("Rol")), "", sqlReader("Rol")))
                    '.Str_IdPerfil = CStr(IIf(IsDBNull(sqlReader("IdPerfil")), "", sqlReader("IdPerfil")))
                    '.Str_Perfil = CStr(IIf(IsDBNull(sqlReader("Perfil")), "", sqlReader("Perfil")))
                    .Str_IdTipoTelefono = CStr(IIf(IsDBNull(sqlReader("IdTipoTelefono")), "", sqlReader("IdTipoTelefono")))
                    .Str_Telefono = CStr(IIf(IsDBNull(sqlReader("Telefono")), "", sqlReader("Telefono")))
                    .IdCargo = CStr(IIf(IsDBNull(sqlReader("IdCargo")), "", sqlReader("IdCargo")))
                    .Cargo = CStr(IIf(IsDBNull(sqlReader("Cargo")), "", sqlReader("Cargo")))
                End With
                listaDatoPersona.Add(obj)
            Loop
            sqlReader.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If sqlCn.State = ConnectionState.Open Then sqlCn.Close()
        End Try

        Return listaDatoPersona
    End Function
    Public Function ValidancionPerfilNuevoxIdUsuario(ByVal IdPersona As Integer) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("ValidandoPerfilNuevaPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim contador As Integer = 0
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim obj As New Entidades.Natural
                If lector.Read Then
                    contador = CInt(lector.Item("contador"))
                End If
                Return contador
                lector.Close()
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Open()
        End Try
    End Function
#Region "Otros"
    Public Function Registrar_MaestroObra(ByVal objPersonaView As Entidades.PersonaView, ByVal TipoPersona As String, ByVal IdEmpresa As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer

        Dim p() As SqlParameter = New SqlParameter(13) {}

        p(0) = New SqlParameter("@TipoPersona", SqlDbType.Char)
        p(0).Value = IIf(TipoPersona = Nothing, DBNull.Value, TipoPersona)

        p(1) = New SqlParameter("@IdTipoAgente", SqlDbType.Int)
        p(1).Value = IIf(objPersonaView.IdTipoAgente = Nothing, DBNull.Value, objPersonaView.IdTipoAgente)

        p(2) = New SqlParameter("@RazonSocial", SqlDbType.VarChar)
        p(2).Value = IIf(objPersonaView.RazonSocial = Nothing, DBNull.Value, objPersonaView.RazonSocial)

        p(3) = New SqlParameter("@ApPaterno", SqlDbType.VarChar)
        p(3).Value = IIf(objPersonaView.ApPaterno = Nothing, DBNull.Value, objPersonaView.ApPaterno)

        p(4) = New SqlParameter("@ApMaterno", SqlDbType.VarChar)
        p(4).Value = IIf(objPersonaView.ApMaterno = Nothing, DBNull.Value, objPersonaView.ApMaterno)

        p(5) = New SqlParameter("@Nombres", SqlDbType.VarChar)
        p(5).Value = IIf(objPersonaView.Nombres = Nothing, DBNull.Value, objPersonaView.Nombres)

        p(6) = New SqlParameter("@DNI", SqlDbType.VarChar)
        p(6).Value = IIf(objPersonaView.Dni = Nothing, DBNull.Value, objPersonaView.Dni)

        p(7) = New SqlParameter("@RUC", SqlDbType.VarChar)
        p(7).Value = IIf(objPersonaView.Ruc = Nothing, DBNull.Value, objPersonaView.Ruc)

        p(8) = New SqlParameter("@Direccion", SqlDbType.VarChar)
        p(8).Value = IIf(objPersonaView.Direccion = Nothing, DBNull.Value, objPersonaView.Direccion)

        p(9) = New SqlParameter("@Telefono", SqlDbType.VarChar)
        p(9).Value = IIf(objPersonaView.Telefeono = Nothing, DBNull.Value, objPersonaView.Telefeono)

        p(10) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        p(10).Value = IIf(IdEmpresa = Nothing, DBNull.Value, IdEmpresa)

        p(11) = objDaoMantenedor.getParam(objPersonaView.EMail, "@EMail", SqlDbType.VarChar)

        p(12) = objDaoMantenedor.getParam(objPersonaView.Ubigeo, "@Ubigeo", SqlDbType.VarChar)

        p(13) = New SqlParameter("@nat_FechaNac", SqlDbType.DateTime)
        p(13).Value = IIf(objPersonaView.FechaNac = Nothing, DBNull.Value, objPersonaView.FechaNac)

        Dim cmd As New SqlCommand("_Persona_RegistrarMaestroObras", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(p)

        Return CInt(cmd.ExecuteScalar())

    End Function
    Public Function RegistrarCliente_Ventas(ByVal objPersonaView As Entidades.PersonaView, ByVal TipoPersona As String, ByVal IdEmpresa As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Integer

        Dim p() As SqlParameter = New SqlParameter(13) {}

        p(0) = New SqlParameter("@TipoPersona", SqlDbType.Char)
        p(0).Value = IIf(TipoPersona = Nothing, DBNull.Value, TipoPersona)

        p(1) = New SqlParameter("@IdTipoAgente", SqlDbType.Int)
        p(1).Value = IIf(objPersonaView.IdTipoAgente = Nothing, DBNull.Value, objPersonaView.IdTipoAgente)

        p(2) = New SqlParameter("@RazonSocial", SqlDbType.VarChar)
        p(2).Value = IIf(objPersonaView.RazonSocial = Nothing, DBNull.Value, objPersonaView.RazonSocial)

        p(3) = New SqlParameter("@ApPaterno", SqlDbType.VarChar)
        p(3).Value = IIf(objPersonaView.ApPaterno = Nothing, DBNull.Value, objPersonaView.ApPaterno)

        p(4) = New SqlParameter("@ApMaterno", SqlDbType.VarChar)
        p(4).Value = IIf(objPersonaView.ApMaterno = Nothing, DBNull.Value, objPersonaView.ApMaterno)

        p(5) = New SqlParameter("@Nombres", SqlDbType.VarChar)
        p(5).Value = IIf(objPersonaView.Nombres = Nothing, DBNull.Value, objPersonaView.Nombres)

        p(6) = New SqlParameter("@DNI", SqlDbType.VarChar)
        p(6).Value = IIf(objPersonaView.Dni = Nothing, DBNull.Value, objPersonaView.Dni)

        p(7) = New SqlParameter("@RUC", SqlDbType.VarChar)
        p(7).Value = IIf(objPersonaView.Ruc = Nothing, DBNull.Value, objPersonaView.Ruc)

        p(8) = New SqlParameter("@Direccion", SqlDbType.VarChar)
        p(8).Value = IIf(objPersonaView.Direccion = Nothing, DBNull.Value, objPersonaView.Direccion)

        p(9) = New SqlParameter("@Telefono", SqlDbType.VarChar)
        p(9).Value = IIf(objPersonaView.Telefeono = Nothing, DBNull.Value, objPersonaView.Telefeono)

        p(10) = New SqlParameter("@TelefonoMovil", SqlDbType.VarChar)
        p(10).Value = IIf(objPersonaView.TelefeonoMovil = Nothing, DBNull.Value, objPersonaView.TelefeonoMovil)

        p(11) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        p(11).Value = IIf(IdEmpresa = Nothing, DBNull.Value, IdEmpresa)

        p(12) = objDaoMantenedor.getParam(objPersonaView.EMail, "@EMail", SqlDbType.VarChar)

        p(13) = objDaoMantenedor.getParam(objPersonaView.Ubigeo, "@Ubigeo", SqlDbType.VarChar)

        Dim cmd As New SqlCommand("_Persona_RegistrarCliente_Ventas", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(p)

        Return CInt(cmd.ExecuteScalar())

    End Function
    Public Function getDescripcionPersona(ByVal IdPersona As Integer) As String

        Dim descripcion As String = ""
        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Try


            cn.Open()

            Dim cmd As New SqlCommand("select dbo.fx_getDescripcionPersona(" + CStr(IdPersona) + ")", cn)
            cmd.CommandType = CommandType.Text

            descripcion = CStr(cmd.ExecuteScalar)

        Catch ex As Exception
            descripcion = ""
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return descripcion

    End Function
    Public Function getRUC(ByVal IdPersona As Integer) As String

        Dim RUC As String = ""
        Dim cn As SqlConnection = objConexion.ConexionSIGE

        Try


            cn.Open()

            Dim cmd As New SqlCommand("select dbo.fx_getRUC(" + CStr(IdPersona) + ")", cn)
            cmd.CommandType = CommandType.Text

            RUC = CStr(cmd.ExecuteScalar)

        Catch ex As Exception
            RUC = ""
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return RUC

    End Function

    Public Function listarPersonaFRMproveedor(ByVal dni As String) As Entidades.Natural
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_listarPersonaFRMproveedor", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@dni", dni)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim obj As New Entidades.Natural
                If lector.Read Then
                    With obj
                        .IdPersona = CInt(lector.Item("idPersona"))
                        .ApellidoMaterno = CStr(lector.Item("nat_Apepat"))
                        .ApellidoMaterno = CStr(lector.Item("nat_Apemat"))
                        .Nombres = CStr(lector.Item("nat_Nombres"))
                        .DNI = CStr(lector.Item("doc_Numero"))
                        .correo = CStr(lector.Item("corr_Nombre"))
                        .telefono = CStr(lector.Item("tel_Numero"))
                        .IdCargo = CInt(lector.Item("IdCargo"))
                    End With
                End If
                Return obj
                lector.Close()
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Open()
        End Try
    End Function

    Public Function listarPersonaNoProveedor(ByVal nrodoc As String) As Entidades.Persona
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_listarPersonaNoProveedor", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@nrodocumento", nrodoc)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim objPersona As New Entidades.Persona
                If lector.Read Then
                    objPersona = New Entidades.Persona
                    objPersona.Id = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    objPersona.NComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                    objPersona.WebSite = CStr(IIf(IsDBNull(lector.Item("per_WebSite")) = True, "", lector.Item("per_WebSite")))
                    objPersona.FechaAlta = CDate(IIf(IsDBNull(lector.Item("per_FechaAlta")) = True, Nothing, lector.Item("per_FechaAlta")))
                    objPersona.FechaBaja = CDate(IIf(IsDBNull(lector.Item("per_FechaBaja")) = True, Nothing, lector.Item("per_FechaBaja")))
                    objPersona.Estado = CStr(IIf(IsDBNull(lector.Item("per_estado")) = True, 0, lector.Item("per_estado")))
                    objPersona.IdGiro = CInt(IIf(IsDBNull(lector.Item("IdGiro")) = True, 0, lector.Item("IdGiro")))
                    objPersona.IdNacionalidad = CInt(IIf(IsDBNull(lector.Item("IdNacionalidad")) = True, 0, lector.Item("IdNacionalidad")))
                    objPersona.IdMotivoBaja = CInt(IIf(IsDBNull(lector.Item("IdMotivoBaja")) = True, 0, lector.Item("IdMotivoBaja")))
                    objPersona.IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                End If
                lector.Close()
                Return objPersona
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function


    Public Function InsertaPersona(ByVal cn As SqlConnection, ByVal persona As Entidades.Persona, ByVal T As SqlTransaction) As Integer
        'Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim APPersona() As SqlParameter = New SqlParameter(11) {}
        APPersona(0) = New SqlParameter("@IdRepresentanteL", SqlDbType.Int)
        APPersona(0).Value = IIf(persona.IdRepresentanteL = Nothing, DBNull.Value, persona.IdRepresentanteL)
        APPersona(1) = New SqlParameter("@IdCentroLaboral", SqlDbType.Int)
        APPersona(1).Value = IIf(persona.IdCentroLaboral = Nothing, DBNull.Value, persona.IdCentroLaboral)
        APPersona(2) = New SqlParameter("@IdContacto", SqlDbType.Int)
        APPersona(2).Value = IIf(persona.IdContacto = Nothing, DBNull.Value, persona.IdContacto)
        APPersona(3) = New SqlParameter("@per_NComercial", SqlDbType.VarChar)
        APPersona(3).Value = IIf(persona.NComercial = Nothing, DBNull.Value, persona.NComercial)
        APPersona(4) = New SqlParameter("@per_WebSite", SqlDbType.VarChar)
        APPersona(4).Value = IIf(persona.WebSite = Nothing, DBNull.Value, persona.WebSite)
        'APPersona(5) = New SqlParameter("@per_FechaAlta", SqlDbType.DateTime)
        'APPersona(5).Value = IIf(persona.FechaAlta = Nothing, DBNull.Value, persona.FechaAlta)
        APPersona(5) = New SqlParameter("@per_FechaBaja", SqlDbType.DateTime)
        APPersona(5).Value = IIf(persona.FechaBaja = Nothing, DBNull.Value, persona.FechaBaja)
        APPersona(6) = New SqlParameter("@per_estado", SqlDbType.Char)
        APPersona(6).Value = IIf(persona.Estado = Nothing, DBNull.Value, persona.Estado)
        APPersona(7) = New SqlParameter("@IdGiro", SqlDbType.Int)
        APPersona(7).Value = IIf(persona.IdGiro = Nothing, DBNull.Value, persona.IdGiro)
        APPersona(8) = New SqlParameter("@IdNacionalidad", SqlDbType.Int)
        APPersona(8).Value = IIf(persona.IdNacionalidad = Nothing, DBNull.Value, persona.IdNacionalidad)
        APPersona(9) = New SqlParameter("@IdMotivoBaja", SqlDbType.Int)
        APPersona(9).Value = IIf(persona.IdMotivoBaja = Nothing, DBNull.Value, persona.IdMotivoBaja)
        APPersona(10) = New SqlParameter("@IdPersona", SqlDbType.Int)
        APPersona(10).Direction = ParameterDirection.Output
        APPersona(11) = New SqlParameter("@IdTipoPV", SqlDbType.Int)
        APPersona(11).Value = IIf(persona.IdTipoPV = Nothing, DBNull.Value, persona.IdTipoPV)
        Return HDAO.InsertaTParameterOutPut(cn, "_PersonaInsert", APPersona, T)
    End Function

    Public Function InsertaPersonaT(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal persona As Entidades.Persona, ByVal Nat As Boolean, ByVal natural As Entidades.Natural, _
                                    ByVal Jur As Boolean, ByVal Juridica As Entidades.Juridica, _
                                    ByVal rol As Boolean, ByVal RolPersona As Entidades.RolPersona, _
                                    Optional ByVal Dir As Boolean = Nothing, Optional ByVal LDir As List(Of Entidades.Direccion) = Nothing, _
                                    Optional ByVal tel As Boolean = Nothing, Optional ByVal LTel As List(Of Entidades.Telefono) = Nothing, _
                                    Optional ByVal doi As Boolean = Nothing, Optional ByVal LDoid As List(Of Entidades.DocumentoI) = Nothing, _
                                    Optional ByVal cor As Boolean = Nothing, Optional ByVal LCor As List(Of Entidades.Correo) = Nothing, _
                                    Optional ByVal Age As Boolean = Nothing, Optional ByVal Agente As Entidades.PersonaTipoAgente = Nothing, _
                                    Optional ByVal profesion As Boolean = Nothing, Optional ByVal ProfesionPersona As Entidades.ProfesionPersona = Nothing, _
                                    Optional ByVal ocupacion As Boolean = Nothing, Optional ByVal OcupacionPersona As Entidades.OcupacionPersona = Nothing) As Integer







        '**** A�ade un registro a la tabla Natural (cuando la personeria=Natural)
        Dim IdPersona As Integer = InsertaPersona(cn, persona, tr)
        If Nat = True Then
            natural.IdPersona = IdPersona
            Dim DaoNatural As New DAO.DAONatural
            DaoNatural.InsertaNatural(cn, natural, tr)
        End If
        '**** A�ade un registro a la tabla Juridica (cuando la personeria=Juridica)
        If Jur = True Then
            Juridica.IdPersona = IdPersona
            Dim DaoJuridica As New DAOJuridica
            DaoJuridica.InsertaJuridica(cn, Juridica, tr)
        End If
        '***** Agrega direcciones
        If Dir = True Then
            Dim i As Integer = Nothing
            While i < LDir.Count
                LDir.Item(i).IdPersona = IdPersona
                i += 1
            End While
            Dim obj As New DAO.DAODireccion
            obj.InsertaDireccion(cn, LDir, tr)
        End If

        '***** Agrega Telefonos
        If tel = True Then
            Dim i As Integer = Nothing
            While i < LTel.Count
                LTel.Item(i).IdPersona = IdPersona
                i += 1
            End While
            Dim obj3 As New DAO.DAOTelefono
            obj3.InsertaTelefono(cn, LTel, tr)
        End If

        '***** Agrega Correos
        If cor = True Then
            Dim i As Integer = Nothing
            While i < LCor.Count
                LCor.Item(i).IdPersona = IdPersona
                i += 1
            End While
            Dim obj1 As New DAO.DAOCorreo
            obj1.InsertaCorreo(cn, LCor, tr)
        End If
        '***** Agrega Documento de Identidad
        If doi = True Then
            Dim i As Integer = Nothing
            While i < LDoid.Count
                LDoid.Item(i).IdPersona = IdPersona
                i += 1
            End While
            Dim obj2 As New DAO.DAODocumentoI
            obj2.InsertaDocumentoI(cn, LDoid, tr)
        End If

        '***** Agrega el Tipo de Agente Retenedor/Perceptor para el cliente
        If Age = True Then
            Agente.IdPersona = IdPersona
            Dim DaoPersonaTipoAgente As New DAO.DAOPersonaTipoAgente
            DaoPersonaTipoAgente.InsertaPersonaTipoAgente(cn, Agente, tr)
        End If
        '***** Agrega el Rol de la Persona
        If rol = True Then
            RolPersona.IdPersona = IdPersona
            Dim DaoRolPersona As New DAO.DAORolPersona
            DaoRolPersona.InsertaRolPersona(cn, RolPersona, tr)
        End If
        '***** Agrega la Profesion de la Persona (natural)
        If profesion = True Then
            ProfesionPersona.IdPersona = IdPersona
            Dim DaoProfesionPersona As New DAO.DAOProfesionPersona
            DaoProfesionPersona.InsertaProfesionPersona(cn, ProfesionPersona, tr)
        End If
        '***** Agrega la ocupacion de la persona (natural)
        If ocupacion = True Then
            OcupacionPersona.IdPersona = IdPersona
            Dim DaoOcupacionPersona As New DAO.DAOOcupacionPersona
            DaoOcupacionPersona.InsertaOcupacionPersona(cn, OcupacionPersona, tr)
        End If




        Return IdPersona

    End Function

    Public Function InsertaPersonaT(ByVal persona As Entidades.Persona, ByVal Nat As Boolean, ByVal natural As Entidades.Natural, _
                                    ByVal Jur As Boolean, ByVal Juridica As Entidades.Juridica, _
                                    ByVal rol As Boolean, ByVal RolPersona As Entidades.RolPersona, _
                                    Optional ByVal Dir As Boolean = Nothing, Optional ByVal LDir As List(Of Entidades.Direccion) = Nothing, _
                                    Optional ByVal tel As Boolean = Nothing, Optional ByVal LTel As List(Of Entidades.Telefono) = Nothing, _
                                    Optional ByVal doi As Boolean = Nothing, Optional ByVal LDoid As List(Of Entidades.DocumentoI) = Nothing, _
                                    Optional ByVal cor As Boolean = Nothing, Optional ByVal LCor As List(Of Entidades.Correo) = Nothing, _
                                    Optional ByVal Age As Boolean = Nothing, Optional ByVal Agente As Entidades.PersonaTipoAgente = Nothing, _
                                    Optional ByVal profesion As Boolean = Nothing, Optional ByVal ProfesionPersona As Entidades.ProfesionPersona = Nothing, _
                                    Optional ByVal ocupacion As Boolean = Nothing, Optional ByVal OcupacionPersona As Entidades.OcupacionPersona = Nothing) As Integer

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        cn.Open()
        Dim T As SqlTransaction = cn.BeginTransaction(Data.IsolationLevel.Serializable)

        Try
            '**** A�ade un registro a la tabla Natural (cuando la personeria=Natural)
            Dim IdPersona As Integer = InsertaPersona(cn, persona, T)
            If Nat = True Then
                natural.IdPersona = IdPersona
                Dim DaoNatural As New DAO.DAONatural
                DaoNatural.InsertaNatural(cn, natural, T)
            End If
            '**** A�ade un registro a la tabla Juridica (cuando la personeria=Juridica)
            If Jur = True Then
                Juridica.IdPersona = IdPersona
                Dim DaoJuridica As New DAOJuridica
                DaoJuridica.InsertaJuridica(cn, Juridica, T)
            End If
            '***** Agrega direcciones
            If Dir = True Then
                Dim i As Integer = Nothing
                While i < LDir.Count
                    LDir.Item(i).IdPersona = IdPersona
                    i += 1
                End While
                Dim obj As New DAO.DAODireccion
                obj.InsertaDireccion(cn, LDir, T)
            End If

            '***** Agrega Telefonos
            If tel = True Then
                Dim i As Integer = Nothing
                While i < LTel.Count
                    LTel.Item(i).IdPersona = IdPersona
                    i += 1
                End While
                Dim obj3 As New DAO.DAOTelefono
                obj3.InsertaTelefono(cn, LTel, T)
            End If

            '***** Agrega Correos
            If cor = True Then
                Dim i As Integer = Nothing
                While i < LCor.Count
                    LCor.Item(i).IdPersona = IdPersona
                    i += 1
                End While
                Dim obj1 As New DAO.DAOCorreo
                obj1.InsertaCorreo(cn, LCor, T)
            End If
            '***** Agrega Documento de Identidad
            If doi = True Then
                Dim i As Integer = Nothing
                While i < LDoid.Count
                    LDoid.Item(i).IdPersona = IdPersona
                    i += 1
                End While
                Dim obj2 As New DAO.DAODocumentoI
                obj2.InsertaDocumentoI(cn, LDoid, T)
            End If

            '***** Agrega el Tipo de Agente Retenedor/Perceptor para el cliente
            If Age = True Then
                Agente.IdPersona = IdPersona
                Dim DaoPersonaTipoAgente As New DAO.DAOPersonaTipoAgente
                DaoPersonaTipoAgente.InsertaPersonaTipoAgente(cn, Agente, T)
            End If
            '***** Agrega el Rol de la Persona
            If rol = True Then
                RolPersona.IdPersona = IdPersona
                Dim DaoRolPersona As New DAO.DAORolPersona
                DaoRolPersona.InsertaRolPersona(cn, RolPersona, T)
            End If
            '***** Agrega la Profesion de la Persona (natural)
            If profesion = True Then
                ProfesionPersona.IdPersona = IdPersona
                Dim DaoProfesionPersona As New DAO.DAOProfesionPersona
                DaoProfesionPersona.InsertaProfesionPersona(cn, ProfesionPersona, T)
            End If
            '***** Agrega la ocupacion de la persona (natural)
            If ocupacion = True Then
                OcupacionPersona.IdPersona = IdPersona
                Dim DaoOcupacionPersona As New DAO.DAOOcupacionPersona
                DaoOcupacionPersona.InsertaOcupacionPersona(cn, OcupacionPersona, T)
            End If
            '***** Finale....
            T.Commit()
            cn.Close()
            Return IdPersona
        Catch ex As Exception
            T.Rollback()
            'Throw ex
            Return -1
        End Try
    End Function
    Public Function ActualizaPersona(ByVal cn As SqlConnection, ByVal persona As Entidades.Persona, ByVal T As SqlTransaction) As Integer
        Dim ArrayParametros() As SqlParameter = New SqlParameter(12) {}
        ArrayParametros(0) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(persona.Id = Nothing, DBNull.Value, persona.Id)

        ArrayParametros(1) = New SqlParameter("@IdRepresentanteL", SqlDbType.Int)
        ArrayParametros(1).Value = IIf(persona.IdRepresentanteL = Nothing Or persona.IdRepresentanteL = 0, DBNull.Value, persona.IdRepresentanteL)

        ArrayParametros(2) = New SqlParameter("@IdCentroLaboral", SqlDbType.Int)
        ArrayParametros(2).Value = IIf(persona.IdCentroLaboral = Nothing, DBNull.Value, persona.IdCentroLaboral)

        ArrayParametros(3) = New SqlParameter("@IdContacto", SqlDbType.Int)
        ArrayParametros(3).Value = IIf(persona.IdContacto = Nothing, DBNull.Value, persona.IdContacto)

        ArrayParametros(4) = New SqlParameter("@per_NComercial", SqlDbType.VarChar)
        ArrayParametros(4).Value = IIf(persona.NComercial = Nothing, DBNull.Value, persona.NComercial)

        ArrayParametros(5) = New SqlParameter("@per_WebSite", SqlDbType.VarChar)
        ArrayParametros(5).Value = IIf(persona.WebSite = Nothing, DBNull.Value, persona.WebSite)

        ArrayParametros(6) = New SqlParameter("@per_FechaBaja", SqlDbType.DateTime)
        ArrayParametros(6).Value = IIf(persona.FechaBaja = Nothing, DBNull.Value, persona.FechaBaja)

        ArrayParametros(7) = New SqlParameter("@per_estado", SqlDbType.VarChar)
        ArrayParametros(7).Value = IIf(persona.Estado = Nothing, DBNull.Value, persona.Estado)

        ArrayParametros(8) = New SqlParameter("@IdGiro", SqlDbType.Int)
        ArrayParametros(8).Value = IIf(persona.IdGiro = Nothing, DBNull.Value, persona.IdGiro)

        ArrayParametros(9) = New SqlParameter("@IdNacionalidad", SqlDbType.Int)
        ArrayParametros(9).Value = IIf(persona.IdNacionalidad = Nothing, DBNull.Value, persona.IdNacionalidad)

        ArrayParametros(10) = New SqlParameter("@IdMotivoBaja", SqlDbType.Int)
        ArrayParametros(10).Value = IIf(persona.IdMotivoBaja = Nothing, DBNull.Value, persona.IdMotivoBaja)

        ArrayParametros(11) = New SqlParameter("@per_Propietario", SqlDbType.Bit)
        ArrayParametros(11).Value = IIf(persona.Propietario = Nothing, DBNull.Value, persona.Propietario)

        ArrayParametros(12) = New SqlParameter("@IdTipoPV", SqlDbType.Int)
        ArrayParametros(12).Value = IIf(persona.IdTipoPV = Nothing, DBNull.Value, persona.IdTipoPV)

        Dim cmd As New SqlCommand("_PersonaUpdate", cn, T)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Try
            Using cmd
                Return CInt(cmd.ExecuteScalar())
            End Using
        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Function Selectx_IdPersona(ByVal IdPersona As Integer) As Entidades.Persona
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_listarPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Using cn
            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
            Dim objPersona As New Entidades.Persona
            If lector.Read Then
                objPersona = New Entidades.Persona
                objPersona.Id = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                objPersona.NComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                objPersona.WebSite = CStr(IIf(IsDBNull(lector.Item("per_WebSite")) = True, "", lector.Item("per_WebSite")))
                objPersona.FechaAlta = CDate(IIf(IsDBNull(lector.Item("per_FechaAlta")) = True, Nothing, lector.Item("per_FechaAlta")))
                objPersona.FechaBaja = CDate(IIf(IsDBNull(lector.Item("per_FechaBaja")) = True, Nothing, lector.Item("per_FechaBaja")))
                objPersona.Estado = CStr(IIf(IsDBNull(lector.Item("per_estado")) = True, 0, lector.Item("per_estado")))
                objPersona.IdGiro = CInt(IIf(IsDBNull(lector.Item("IdGiro")) = True, 0, lector.Item("IdGiro")))
                objPersona.IdNacionalidad = CInt(IIf(IsDBNull(lector.Item("IdNacionalidad")) = True, 0, lector.Item("IdNacionalidad")))
                objPersona.IdMotivoBaja = CInt(IIf(IsDBNull(lector.Item("IdMotivoBaja")) = True, 0, lector.Item("IdMotivoBaja")))
                objPersona.IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
                objPersona.IdRepresentanteL = CInt(IIf(IsDBNull(lector.Item("IdRepresentanteL")) = True, 0, lector.Item("IdRepresentanteL")))
                objPersona.IdContacto = CInt(IIf(IsDBNull(lector.Item("IdContacto")) = True, 0, lector.Item("IdContacto")))
                objPersona.IdCentroLaboral = CInt(IIf(IsDBNull(lector.Item("IdCentroLaboral")) = True, 0, lector.Item("IdCentroLaboral")))
                objPersona.Propietario = CBool(IIf(IsDBNull(lector.Item("per_Propietario")) = True, False, lector.Item("per_Propietario")))
            End If
            lector.Close()
            Return objPersona
        End Using
    End Function

    Public Function SelectxIdPersona(ByVal IdPersona As Integer) As Entidades.Persona
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_PersonaSelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Using cn
            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
            Dim objPersona As New Entidades.Persona
            If lector.Read Then
                objPersona = New Entidades.Persona
                objPersona.Id = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                objPersona.NComercial = CStr(IIf(IsDBNull(lector.Item("per_NComercial")) = True, "", lector.Item("per_NComercial")))
                objPersona.WebSite = CStr(IIf(IsDBNull(lector.Item("per_WebSite")) = True, "", lector.Item("per_WebSite")))
                objPersona.FechaAlta = CDate(IIf(IsDBNull(lector.Item("per_FechaAlta")) = True, Nothing, lector.Item("per_FechaAlta")))
                objPersona.FechaBaja = CDate(IIf(IsDBNull(lector.Item("per_FechaBaja")) = True, Nothing, lector.Item("per_FechaBaja")))
                objPersona.Estado = CStr(IIf(IsDBNull(lector.Item("per_estado")) = True, 0, lector.Item("per_estado")))
                objPersona.IdGiro = CInt(IIf(IsDBNull(lector.Item("IdGiro")) = True, 0, lector.Item("IdGiro")))
                objPersona.IdNacionalidad = CInt(IIf(IsDBNull(lector.Item("IdNacionalidad")) = True, 0, lector.Item("IdNacionalidad")))
                objPersona.IdMotivoBaja = CInt(IIf(IsDBNull(lector.Item("IdMotivoBaja")) = True, 0, lector.Item("IdMotivoBaja")))
                objPersona.IdTipoPV = CInt(IIf(IsDBNull(lector.Item("IdTipoPV")) = True, 0, lector.Item("IdTipoPV")))
            End If
            lector.Close()
            Return objPersona
        End Using
    End Function

    Public Function ActualizaPersonaEstado(ByVal persona As Entidades.Persona) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Try
            Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
            ArrayParametros(0) = New SqlParameter("@IdPersona", SqlDbType.Int)
            ArrayParametros(0).Value = IIf(persona.Id = Nothing, DBNull.Value, persona.Id)
            ArrayParametros(1) = New SqlParameter("@per_estado", SqlDbType.VarChar)
            ArrayParametros(1).Value = IIf(persona.Estado = Nothing, DBNull.Value, persona.Estado)
            HDAO.Update(cn, "_PersonaUpdateEstado", ArrayParametros)
            Return 1
        Catch ex As Exception
            Return -1
        End Try
    End Function

    Public Function ActualizaPersonaT(ByVal persona As Entidades.Persona, _
                                    ByVal Nat As Boolean, ByVal natural As Entidades.Natural, _
                                    ByVal Jur As Boolean, ByVal Juridica As Entidades.Juridica, _
                                    ByVal Rol As Boolean, ByVal RolPersona As Entidades.RolPersona, _
                                    ByVal Dir As Boolean, ByVal LDir As List(Of Entidades.Direccion), _
                                    ByVal Tel As Boolean, ByVal LTel As List(Of Entidades.Telefono), _
                                    ByVal Doi As Boolean, ByVal LDId As List(Of Entidades.DocumentoI), _
                                    ByVal Cor As Boolean, ByVal LCor As List(Of Entidades.Correo), _
                                    ByVal Age As Boolean, ByVal Agente As Entidades.PersonaTipoAgente, _
                                    ByVal profesion As Boolean, ByVal ProfesionPersona As Entidades.ProfesionPersona, _
                                    ByVal ocupacion As Boolean, ByVal OcupacionPersona As Entidades.OcupacionPersona) As Integer
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim BuscarDato As New DAO.DAOUtil
        Using cn
            cn.Open()
            Dim T As SqlTransaction = cn.BeginTransaction(Data.IsolationLevel.Serializable)
            Dim ElIdPersona As Integer = persona.Id
            Try
                '***** Actualiza los datos de Persona
                ActualizaPersona(cn, persona, T)
                '***** Actualiza la Persona Juridica o Natural
                If Jur = True Then
                    Dim objJuridica As New DAO.DAOJuridica
                    objJuridica.ActualizaJuridica(cn, Juridica, T)
                End If
                If Nat = True Then
                    Dim objNatural As New DAO.DAONatural
                    objNatural.ActualizaNatural(cn, natural, T)
                End If
                '***** Actualiza la direccion
                If Dir = True Then
                    '***** Primero verifica si son datos nuevos o existentes en la tabla...
                    Dim objDireccion As New DAO.DAODireccion
                    If BuscarDato.ValidarExistenciaxTablax1Campo("Direccion", "IdPersona", CStr(ElIdPersona)) = 0 Then
                        '*** No Hay.. es nuevo, por lo tanto inserta la direccion en la tabla  
                        objDireccion.InsertaDireccion(cn, LDir, T)
                    Else
                        objDireccion.ActualizaDireccionT(cn, LDir, T)
                    End If
                End If
                '***** Actualiza Telefonos
                If Tel = True Then
                    '***** Verifica que existan datos previos de telefono, de ser asi los elimina e inserta los actuales...
                    Dim objTelefonos As New DAO.DAOTelefono
                    If BuscarDato.ValidarExistenciaxTablax1Campo("Telefono", "IdPersona", CStr(ElIdPersona)) = 0 Then
                        'Mo hay datos por lo tanto inserta los telefonos
                        objTelefonos.InsertaTelefono(cn, LTel, T)
                    Else
                        objTelefonos.DeletexIdPersona(cn, ElIdPersona, T)   '***** Borra telefonos existentes
                        objTelefonos.InsertaTelefono(cn, LTel, T)           '***** Ahora agrega los telefonos nuevos...
                    End If
                End If
                '***** Actualiza Correos
                If Cor = True Then
                    '***** Verifica que existan datos previos de Correos, de ser asi los elimina e inserta los actuales...
                    Dim objCorreos As New DAO.DAOCorreo
                    If BuscarDato.ValidarExistenciaxTablax1Campo("Correo", "IdPersona", CStr(ElIdPersona)) = 0 Then
                        'Mo hay datos por lo tanto inserta los telefonos
                        objCorreos.InsertaCorreo(cn, LCor, T)
                    Else
                        objCorreos.DeletexIdPersona(cn, ElIdPersona, T)     '***** Borra Correos existentes
                        objCorreos.InsertaCorreo(cn, LCor, T)               '***** Ahora agrega los Correos nuevos...
                    End If
                End If
                '***** Actualiza Documento de Identidad
                If Doi = True Then
                    '***** Primero verifica si son datos nuevos o existentes en la tabla...
                    Dim objDocumentoI As New DAO.DAODocumentoI
                    If BuscarDato.ValidarExistenciaxTablax1Campo("DocumentoI", "IdPersona", CStr(ElIdPersona)) = 0 Then
                        '*** No Hay.. es nuevo, por lo tanto inserta el documento identidad en la tabla
                        objDocumentoI.InsertaDocumentoI(cn, LDId, T)
                    Else
                        objDocumentoI.DeletexIdPersonaT(cn, ElIdPersona, T)     '***** Borra los Docs. de Ident. Existentes
                        objDocumentoI.InsertaDocumentoI(cn, LDId, T)            '***** Ahora agrega los nuevos...
                    End If
                End If
                '***** Actualiza Tipo de Agente (Retencion/Detraccion
                If Age = True Then
                    '***** Primero verifica si son datos nuevos o existentes en la tabla...
                    Dim objAgente As New DAO.DAOPersonaTipoAgente
                    If BuscarDato.ValidarExistenciaxTablax1Campo("PersonaTipoAgente", "IdPersona", CStr(ElIdPersona)) = 0 Then
                        '*** No Hay.. es nuevo, por lo tanto inserta el Agente en la tabla
                        objAgente.InsertaPersonaTipoAgente(cn, Agente, T)
                    Else
                        objAgente.ActualizaPersonaTipoAgenteT(cn, Agente, T)
                    End If
                End If
                '***** Profesion
                If profesion = True Then
                    '***** Primero verifica si son datos nuevos o existentes en la tabla...
                    Dim objProfesion As New DAO.DAOProfesionPersona
                    If BuscarDato.ValidarExistenciaxTablax1Campo("ProfesionPersona", "IdPersona", CStr(ElIdPersona)) = 0 Then
                        '*** No Hay.. es nuevo, por lo tanto inserta el Agente en la tabla
                        objProfesion.InsertaProfesionPersona(cn, ProfesionPersona, T)
                    Else
                        objProfesion.ActualizaProfesionPersonaT(cn, ProfesionPersona, T)
                    End If
                End If
                '***** Ocupacion
                If ocupacion = True Then
                    '***** Primero verifica si son datos nuevos o existentes en la tabla...
                    Dim objOcupacion As New DAO.DAOOcupacionPersona
                    If BuscarDato.ValidarExistenciaxTablax1Campo("OcupacionPersona", "IdPersona", CStr(ElIdPersona)) = 0 Then
                        '*** No Hay.. es nuevo, por lo tanto inserta el Agente en la tabla
                        objOcupacion.InsertaOcupacionPersona(cn, OcupacionPersona, T)
                    Else
                        objOcupacion.ActualizaOcupacionPersonaT(cn, OcupacionPersona, T)
                    End If
                End If
                '***** Actualiza RolPersona??
                If Rol = True Then
                    '***** Mueve los campos: rp_FechaBaja=Date(), rp_Estado="0", idMotivoBaja=Int
                    Dim objRol As New DAO.DAORolPersona
                    objRol.ActualizaRolPersonaT(cn, RolPersona, T)
                End If
                '***** Finale....
                T.Commit()
                cn.Close()
                Return 1
            Catch ex As Exception
                T.Rollback()
                'Throw ex
                Return -1
            End Try
        End Using
    End Function

#End Region

#Region "mantenimiento Persona"

    Public Function getDataSetConsultaPersona(ByVal nombres As String, ByVal numero As String, _
                                             ByVal tipo As Integer, ByVal estado As Integer, ByVal ordenar As Integer) As DataSet
        Dim da As SqlDataAdapter
        Dim ds As New DataSet
        Dim parametros() As SqlParameter = New SqlParameter(4) {}
        parametros(0) = New SqlParameter("@nombres", SqlDbType.VarChar)
        parametros(0).Value = nombres
        parametros(1) = New SqlParameter("@numero", SqlDbType.VarChar)
        parametros(1).Value = numero
        parametros(2) = New SqlParameter("@tipo", SqlDbType.VarChar)
        parametros(2).Value = tipo
        parametros(3) = New SqlParameter("@estado", SqlDbType.VarChar)
        parametros(3).Value = estado
        parametros(4) = New SqlParameter("@ordenar", SqlDbType.VarChar)
        parametros(4).Value = ordenar

        Try
            Dim cmd As New SqlCommand("_CR_Persona", objConexion.ConexionSIGE)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddRange(parametros)
            da = New SqlDataAdapter(cmd)
            da.Fill(ds, "DT_Persona")

        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function


    Public Function listarPersonaNaturalPersonaJuridica(ByVal dni As String, ByVal ruc As String, _
                                                        ByVal nombre As String, ByVal tipo As Integer, _
                                                        ByVal pageindex As Integer, ByVal pagesize As Integer) As List(Of Entidades.PersonaView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim paramentros() As SqlParameter = New SqlParameter(5) {}
        paramentros(0) = New SqlParameter("@Dni", SqlDbType.VarChar)
        paramentros(0).Value = dni
        paramentros(1) = New SqlParameter("@Ruc", SqlDbType.VarChar)
        paramentros(1).Value = ruc
        paramentros(2) = New SqlParameter("@nombre", SqlDbType.VarChar)
        paramentros(2).Value = nombre
        paramentros(3) = New SqlParameter("@tipo", SqlDbType.Int)
        paramentros(3).Value = tipo
        paramentros(4) = New SqlParameter("@pageindex", SqlDbType.Int)
        paramentros(4).Value = pageindex
        paramentros(5) = New SqlParameter("@pageSize", SqlDbType.Int)
        paramentros(5).Value = pagesize

        Dim cmd As New SqlCommand("_listarPersonaNaturalPersonaJuridica", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(paramentros)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim obj As New Entidades.PersonaView
                    With obj
                        .IdPersona = CInt(lector("IdPersona"))
                        .Nombre = CStr(IIf(IsDBNull(lector("Nombre")), "---", lector("Nombre")))
                        .Ruc = CStr(IIf(IsDBNull(lector("Ruc")), "---", lector("Ruc")))
                        .Dni = CStr(IIf(IsDBNull(lector("Dni")), "---", lector("Dni")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function listarxPersonaNaturalxPersonaJuridica(ByVal dni As String, ByVal ruc As String, ByVal RazonApe As String, ByVal tipo As Integer, _
                                                      ByVal pageindex As Integer, ByVal pagesize As Integer) As List(Of Entidades.PersonaView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim paramentros() As SqlParameter = New SqlParameter(5) {}
        paramentros(0) = New SqlParameter("@Dni", SqlDbType.VarChar)
        paramentros(0).Value = dni
        paramentros(1) = New SqlParameter("@Ruc", SqlDbType.VarChar)
        paramentros(1).Value = ruc
        paramentros(2) = New SqlParameter("@razonApe", SqlDbType.VarChar)
        paramentros(2).Value = RazonApe
        paramentros(3) = New SqlParameter("@tipo", SqlDbType.Int)
        paramentros(3).Value = tipo
        paramentros(4) = New SqlParameter("@pageindex", SqlDbType.Int)
        paramentros(4).Value = pageindex
        paramentros(5) = New SqlParameter("@pageSize", SqlDbType.Int)
        paramentros(5).Value = pagesize

        Dim cmd As New SqlCommand("_listarxPersonaNaturalxPersonaJuridica", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(paramentros)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim obj As New Entidades.PersonaView
                    With obj
                        .IdPersona = CInt(lector("IdPersona"))
                        '.TipoPersona = CInt(lector("TipoPersona"))
                        .Nombre = CStr(IIf(IsDBNull(lector("Nombre")), "---", lector("Nombre")))
                        .Ruc = CStr(IIf(IsDBNull(lector("Ruc")), "---", lector("Ruc")))
                        .Dni = CStr(IIf(IsDBNull(lector("Dni")), "---", lector("Dni")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function InsertaBDPersona(ByVal cn As SqlConnection, _
                                     ByVal objPersona As Entidades.Persona, ByVal tr As SqlTransaction) As String
        Dim parametros() As SqlParameter = New SqlParameter(11) {}
        parametros(0) = New SqlParameter("@IdRepresentanteL", SqlDbType.Int)
        parametros(0).Value = IIf(objPersona.IdRepresentanteL = Nothing Or objPersona.IdRepresentanteL = 0, DBNull.Value, objPersona.IdRepresentanteL)

        parametros(1) = New SqlParameter("@IdCentroLaboral", SqlDbType.Int)
        parametros(1).Value = IIf(objPersona.IdCentroLaboral = Nothing Or objPersona.IdCentroLaboral = 0, DBNull.Value, objPersona.IdCentroLaboral)

        parametros(2) = New SqlParameter("@IdContacto", SqlDbType.Int)
        parametros(2).Value = IIf(objPersona.IdContacto = Nothing Or objPersona.IdContacto = 0, DBNull.Value, objPersona.IdContacto)

        parametros(3) = New SqlParameter("@per_NComercial", SqlDbType.VarChar)
        parametros(3).Value = IIf(objPersona.NComercial = Nothing Or objPersona.NComercial = "", DBNull.Value, objPersona.NComercial)

        parametros(4) = New SqlParameter("@per_WebSite", SqlDbType.VarChar)
        parametros(4).Value = IIf(objPersona.WebSite = Nothing Or objPersona.WebSite = "", DBNull.Value, objPersona.WebSite)

        parametros(5) = New SqlParameter("@per_FechaBaja", SqlDbType.DateTime)
        parametros(5).Value = IIf(objPersona.FechaBaja = Nothing, DBNull.Value, objPersona.FechaBaja)

        parametros(6) = New SqlParameter("@per_estado", SqlDbType.Char)
        parametros(6).Value = IIf(objPersona.Estado = Nothing, DBNull.Value, objPersona.Estado)

        parametros(7) = New SqlParameter("@IdGiro", SqlDbType.Int)
        parametros(7).Value = IIf(objPersona.IdGiro = Nothing Or objPersona.IdGiro = 0, DBNull.Value, objPersona.IdGiro)

        parametros(8) = New SqlParameter("@IdNacionalidad", SqlDbType.Int)
        parametros(8).Value = IIf(objPersona.IdNacionalidad = Nothing Or objPersona.IdNacionalidad = 0, DBNull.Value, objPersona.IdNacionalidad)

        parametros(9) = New SqlParameter("@IdMotivoBaja", SqlDbType.Int)
        parametros(9).Value = IIf(objPersona.IdMotivoBaja = Nothing Or objPersona.IdMotivoBaja = 0, DBNull.Value, objPersona.IdMotivoBaja)

        parametros(10) = New SqlParameter("@per_Propietario", SqlDbType.Bit)
        parametros(10).Value = IIf(objPersona.Propietario = Nothing, DBNull.Value, objPersona.Propietario)

        parametros(11) = New SqlParameter("@IdTipoPV", SqlDbType.Int)
        parametros(11).Value = IIf(objPersona.IdTipoPV = Nothing Or objPersona.IdTipoPV = 0, DBNull.Value, objPersona.IdTipoPV)

        Dim cmd As New SqlCommand("_insertaPersona", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(parametros)
        Try
            Using cmd
                Return cmd.ExecuteScalar().ToString
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ListarPersonaComplemento(ByVal idPerona As Integer, ByVal tipoComplemento As String) As List(Of Entidades.PersonaView)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ListarPersonaComplemento", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idpersona", idPerona)
        cmd.Parameters.AddWithValue("@tipoComplemento", tipoComplemento)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim obj As New Entidades.PersonaView
                    With obj
                        .IdPersona = CInt(lector("IdPersona"))
                        .Nombre = CStr(IIf(IsDBNull(lector("Nombre")), "---", lector("Nombre")))
                        .Dni = CStr(IIf(IsDBNull(lector("DNI")), "---", lector("DNI")))
                        .Ruc = CStr(IIf(IsDBNull(lector("RUC")), "---", lector("RUC")))
                        Lista.Add(obj)
                    End With
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try

    End Function

    Function deleteDatosPersonaxActualizacion(ByVal idPersona As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction) As Boolean
        Try
            Dim cmd As New SqlCommand("_deleteDatosPersonaxActualizacion", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@idpersona", idPersona)
            Using cmd
                cmd.ExecuteNonQuery()
                Return True
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function InsertarContactoEmpresa(ByVal cn As SqlConnection, ByVal listaContactoEmpresa As List(Of Entidades.PersonaView), ByVal idPersona As Integer, ByVal tr As SqlTransaction) As Boolean
        Try
            Dim obj As Entidades.PersonaView
            For Each obj In listaContactoEmpresa
                Dim parametros() As SqlParameter = New SqlParameter(1) {}
                parametros(0) = New SqlParameter("@idcontacto", SqlDbType.Int)
                parametros(0).Value = obj.IdPersona
                parametros(1) = New SqlParameter("@idpersona", SqlDbType.Int)
                parametros(1).Value = idPersona
                Dim cmd As New SqlCommand("_InsertarContactoEmpresa", cn, tr)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(parametros)
                cmd.ExecuteNonQuery()
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Function




    Public Function InsertarNuevaPersona(ByVal tipoPersona As String, ByVal objPersona As Entidades.Persona, ByVal objNatural As Entidades.Natural, _
                                      ByVal objChofer As Entidades.Chofer, ByVal objEmpleado As Entidades.Empleado, ByVal objJuridica As Entidades.Juridica, _
                                      ByVal objListDireccion As List(Of Entidades.Direccion), ByVal objListTelefono As List(Of Entidades.TelefonoView), _
                                      ByVal objListCorreo As List(Of Entidades.CorreoView), ByVal objListaDocumentoI As List(Of Entidades.DocumentoI), _
                                      ByVal objListRolPersona As List(Of Entidades.RolPersona), ByVal objListProfesion As List(Of Entidades.ProfesionPersona), _
                                      ByVal objListOcupacion As List(Of Entidades.OcupacionPersona), ByVal ListaContactoEmpresa As List(Of Entidades.PersonaView), _
                                      ByVal List_PersonaTipoAgente As List(Of Entidades.PersonaTipoAgente)) As String

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim Cad$ = String.Empty
        Dim IdPersona As Integer = -1
        Dim datos() As String
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)
            Cad = InsertaBDPersona(cn, objPersona, tr)

            datos = Cad.Split(CChar(","))
            IdPersona = CInt(datos(0))

            If tipoPersona = "N" Then '***************************************************************
                objNatural.IdPersona = IdPersona
                Dim DaoNatural As New DAO.DAONatural
                DaoNatural.InsertaNatural(cn, objNatural, tr)

                '***** Agrega la Profesion de la Persona (natural)
                If objListProfesion.Count > 0 Then
                    Dim i As Integer = 0
                    While i < objListProfesion.Count
                        objListProfesion(i).IdPersona = IdPersona
                        i += 1
                    End While
                    Dim ObjDaoProfesionPersona As New DAO.DAOProfesionPersona
                    ObjDaoProfesionPersona.InsertaListProfesionPersona(cn, objListProfesion, tr)
                End If

                '***** Agrega la ocupacion de la persona (natural)
                If objListOcupacion.Count > 0 Then
                    Dim i As Integer = 0
                    While i < objListOcupacion.Count
                        objListOcupacion(i).IdPersona = IdPersona
                        i += 1
                    End While
                    Dim objDaoOcupacionPersona As New DAO.DAOOcupacionPersona
                    objDaoOcupacionPersona.InsertaListaOcupacionPersona(cn, objListOcupacion, tr)
                End If

                '***** Agrega Documento de Identidad
                If objListaDocumentoI.Count > 0 Then
                    Dim i As Integer = Nothing
                    While i < objListaDocumentoI.Count
                        objListaDocumentoI.Item(i).IdPersona = IdPersona
                        i += 1
                    End While
                    Dim objListDocumentoI As New DAO.DAODocumentoI
                    objListDocumentoI.InsertaDocumentoI(cn, objListaDocumentoI, tr)
                End If

                If objChofer.esChofer = True Then
                    objChofer.Id = IdPersona
                    If (New DAO.DAOChofer).InsertaChoferTransact(objChofer, cn, tr) = False Then Throw New Exception("Error al registrar a la persona como chofer")
                End If

                If objEmpleado.EsEmpleado = True Then
                    objEmpleado.IdPersona = IdPersona
                    ReDim Preserve datos(2)
                    datos(2) = (New DAO.DAOEmpleado).InsertaEmpleadoTransact(objEmpleado, cn, tr).Trim
                End If


            Else '************************************************************************************
                objJuridica.IdPersona = IdPersona
                Dim DaoJuridica As New DAOJuridica
                DaoJuridica.InsertaObjJuridica(cn, objJuridica, tr)

                If ListaContactoEmpresa.Count > 0 Then
                    InsertarContactoEmpresa(cn, ListaContactoEmpresa, IdPersona, tr)
                End If

            End If




            '***** Agrega direcciones
            If objListDireccion.Count > 0 Then
                Dim i As Integer = Nothing
                While i < objListDireccion.Count
                    objListDireccion(i).IdPersona = IdPersona
                    i += 1
                End While
                Dim objDaoDireccion As New DAO.DAODireccion
                objDaoDireccion.InsertaDireccion(cn, objListDireccion, tr)
            End If

            '***** Agrega Telefonos
            If objListTelefono.Count > 0 Then
                Dim i As Integer = Nothing
                While i < objListTelefono.Count
                    objListTelefono.Item(i).IdPersona = IdPersona
                    i += 1
                End While
                Dim objDaoTelefono As New DAO.DAOTelefono
                objDaoTelefono.InsertaListaTelefono(cn, objListTelefono, tr)
            End If

            '***** Agrega Correos
            If objListCorreo.Count > 0 Then
                Dim i As Integer = Nothing
                While i < objListCorreo.Count
                    objListCorreo.Item(i).IdPersona = IdPersona
                    i += 1
                End While
                Dim objDaoCorreo As New DAO.DAOCorreo
                objDaoCorreo.InsertaListaCorreo(cn, objListCorreo, tr)
            End If

            '***** Agrega el Rol de la Persona
            If objListRolPersona.Count > 0 Then
                Dim i As Integer = 0
                While i < objListRolPersona.Count
                    objListRolPersona(i).IdPersona = IdPersona
                    i += 1
                End While
                Dim objDaoRolPersona As New DAO.DAORolPersona
                objDaoRolPersona.InsertaListRolPersona(cn, objListRolPersona, tr)
            End If

            '***** Agrega el Tipo de Agente de la Persona
            Dim objDaoPersonaTipoAgente As DAO.DAOPersonaTipoAgente
            For Each obj As Entidades.PersonaTipoAgente In List_PersonaTipoAgente
                obj.IdPersona = IdPersona
                objDaoPersonaTipoAgente = New DAO.DAOPersonaTipoAgente
                objDaoPersonaTipoAgente.InsertaPersonaTipoAgente(cn, obj, tr)
            Next

            tr.Commit()
            If datos.Length > 2 Then Cad = Cad + "," + datos(2)
            Return Cad
        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function ActualizarPersona(ByVal tipoPersona As String, ByVal objPersona As Entidades.Persona, ByVal objNatural As Entidades.Natural, _
                                          ByVal objChofer As Entidades.Chofer, ByVal objEmpleado As Entidades.Empleado, ByVal objJuridica As Entidades.Juridica, _
                                          ByVal objListDireccion As List(Of Entidades.Direccion), ByVal objListTelefono As List(Of Entidades.TelefonoView), _
                                          ByVal objListCorreo As List(Of Entidades.CorreoView), ByVal objListaDocumentoI As List(Of Entidades.DocumentoI), _
                                          ByVal objListRolPersona As List(Of Entidades.RolPersona), ByVal objListProfesion As List(Of Entidades.ProfesionPersona), _
                                          ByVal objListOcupacion As List(Of Entidades.OcupacionPersona), ByVal ListaContactoEmpresa As List(Of Entidades.PersonaView), _
                                          ByVal List_PersonaTipoAgente As List(Of Entidades.PersonaTipoAgente)) As String

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim tr As SqlTransaction = Nothing
        Dim cad As String
        Dim IdPersona As Integer = -1
        Try
            cn.Open()
            tr = cn.BeginTransaction(IsolationLevel.Serializable)


            IdPersona = objPersona.Id

            deleteDatosPersonaxActualizacion(IdPersona, cn, tr)
            ActualizaPersona(cn, objPersona, tr)


            If tipoPersona = "N" Then
                objNatural.IdPersona = IdPersona
                Dim DaoNatural As New DAO.DAONatural

                DaoNatural.ActualizaNatural(cn, objNatural, tr)

                '***** Agrega la Profesion de la Persona (natural)
                If objListProfesion.Count > 0 Then
                    Dim i As Integer = 0
                    While i < objListProfesion.Count
                        objListProfesion(i).IdPersona = IdPersona
                        i += 1
                    End While
                    Dim ObjDaoProfesionPersona As New DAO.DAOProfesionPersona
                    ObjDaoProfesionPersona.InsertaListProfesionPersona(cn, objListProfesion, tr)
                End If

                '***** Agrega la ocupacion de la persona (natural)
                If objListOcupacion.Count > 0 Then
                    Dim i As Integer = 0
                    While i < objListOcupacion.Count
                        objListOcupacion(i).IdPersona = IdPersona
                        i += 1
                    End While
                    Dim objDaoOcupacionPersona As New DAO.DAOOcupacionPersona
                    objDaoOcupacionPersona.InsertaListaOcupacionPersona(cn, objListOcupacion, tr)
                End If

                '***** Agrega Documento de Identidad
                If objListaDocumentoI.Count > 0 Then
                    Dim i As Integer = Nothing
                    While i < objListaDocumentoI.Count
                        objListaDocumentoI.Item(i).IdPersona = IdPersona
                        i += 1
                    End While
                    Dim objListDocumentoI As New DAO.DAODocumentoI
                    objListDocumentoI.InsertaDocumentoI(cn, objListaDocumentoI, tr)
                End If


                If objChofer.esChofer = True And objChofer.TotalRegEnDocumento = -1 Then
                    objChofer.Id = IdPersona
                    '  NO Esta en la tabla chofer y No esta en la tabla Documento
                    If (New DAO.DAOChofer).InsertaChoferTransact(objChofer, cn, tr) = False Then Throw New Exception("Error al registrar a la persona como chofer")
                End If

                If objChofer.esChofer = True And objChofer.TotalRegEnDocumento >= 0 Then
                    objChofer.Id = IdPersona
                    ' Esta en la tabla chofer y en la tabla Documento
                    If (New DAO.DAOChofer).ActualizaChoferTransact(objChofer, cn, tr) = False Then Throw New Exception("Error al registrar a la persona como chofer")
                End If

                If objChofer.esChofer = False And objChofer.TotalRegEnDocumento = 0 Then
                    objChofer.Id = IdPersona
                    ' Esta en la tabla chofer y No esta en la tabla Documento
                    If (New DAO.DAOChofer).deleteChofer(IdPersona, cn, tr) = False Then Throw New Exception("Error al eliminar el registro de chofer")
                End If

                If objEmpleado.EsEmpleado = True And objEmpleado.EstaReg = -1 Then
                    objEmpleado.IdPersona = IdPersona
                    ' No esta en la tabla empleado
                    cad = (New DAO.DAOEmpleado).InsertaEmpleadoTransact(objEmpleado, cn, tr)
                End If

                If objEmpleado.EstaReg >= 0 Then
                    objEmpleado.IdPersona = IdPersona
                    ' Esta en la tabla empleado
                    If (New DAO.DAOEmpleado).ActualizaEmpleadoTransact(objEmpleado, cn, tr) = False Then Throw New Exception("Error al actualizar el registro empleado")
                End If

            Else
                objJuridica.IdPersona = IdPersona
                Dim DaoJuridica As New DAOJuridica
                DaoJuridica.ActualizaJuridica(cn, objJuridica, tr)

                If ListaContactoEmpresa.Count > 0 Then
                    InsertarContactoEmpresa(cn, ListaContactoEmpresa, IdPersona, tr)
                End If

            End If

            '***** Agrega direcciones
            If objListDireccion.Count > 0 Then
                Dim i As Integer = Nothing
                While i < objListDireccion.Count
                    objListDireccion(i).IdPersona = IdPersona
                    i += 1
                End While
                Dim objDaoDireccion As New DAO.DAODireccion
                objDaoDireccion.InsertaDireccion(cn, objListDireccion, tr)
            End If

            '***** Agrega Telefonos
            If objListTelefono.Count > 0 Then
                Dim i As Integer = Nothing
                While i < objListTelefono.Count
                    objListTelefono.Item(i).IdPersona = IdPersona
                    i += 1
                End While
                Dim objDaoTelefono As New DAO.DAOTelefono
                objDaoTelefono.InsertaListaTelefono(cn, objListTelefono, tr)
            End If

            '***** Agrega Correos
            If objListCorreo.Count > 0 Then
                Dim i As Integer = Nothing
                While i < objListCorreo.Count
                    objListCorreo.Item(i).IdPersona = IdPersona
                    i += 1
                End While
                Dim objDaoCorreo As New DAO.DAOCorreo
                objDaoCorreo.InsertaListaCorreo(cn, objListCorreo, tr)
            End If

            '***** Agrega el Rol de la Persona
            If objListRolPersona.Count > 0 Then
                Dim i As Integer = 0
                While i < objListRolPersona.Count
                    objListRolPersona(i).IdPersona = IdPersona
                    i += 1
                End While
                Dim objDaoRolPersona As New DAO.DAORolPersona
                objDaoRolPersona.InsertaListRolPersona(cn, objListRolPersona, tr)
            End If

            '***** Agrega el Tipo Agente de la Persona
            Dim objDaoPersonaTipoAgente As DAO.DAOPersonaTipoAgente
            For Each obj As Entidades.PersonaTipoAgente In List_PersonaTipoAgente
                obj.IdPersona = IdPersona
                objDaoPersonaTipoAgente = New DAO.DAOPersonaTipoAgente
                objDaoPersonaTipoAgente.InsertaPersonaTipoAgente(cn, obj, tr)
            Next

            tr.Commit()
            Return cad

        Catch ex As Exception
            tr.Rollback()
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

    Public Function listarxPersonaNaturalxPersonaJuridica(ByVal dni As String, ByVal ruc As String, _
                                       ByVal RazonApe As String, ByVal tipo As Integer, _
                                       ByVal pageindex As Integer, ByVal pagesize As Integer, _
                                       ByVal idrol As Integer, ByVal estado As Integer, Optional ByVal IdNacionalidad As Integer = -1) As List(Of Entidades.PersonaView)

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim paramentros() As SqlParameter = New SqlParameter(8) {}

        paramentros(0) = New SqlParameter("@Dni", SqlDbType.VarChar)
        paramentros(0).Value = dni
        paramentros(1) = New SqlParameter("@Ruc", SqlDbType.VarChar)
        paramentros(1).Value = ruc
        paramentros(2) = New SqlParameter("@razonApe", SqlDbType.VarChar)
        paramentros(2).Value = RazonApe
        paramentros(3) = New SqlParameter("@tipo", SqlDbType.Int)
        paramentros(3).Value = tipo
        paramentros(4) = New SqlParameter("@pageindex", SqlDbType.Int)
        paramentros(4).Value = pageindex
        paramentros(5) = New SqlParameter("@pageSize", SqlDbType.Int)
        paramentros(5).Value = pagesize
        paramentros(6) = New SqlParameter("@idRol", SqlDbType.Int)
        paramentros(6).Value = idrol
        paramentros(7) = New SqlParameter("@estado", SqlDbType.Int)
        paramentros(7).Value = estado
        paramentros(8) = New SqlParameter("@IdNacionalidad", SqlDbType.Int)
        paramentros(8).Value = IdNacionalidad

        Dim cmd As New SqlCommand("_listarxPersonaNaturalxPersonaJuridica", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(paramentros)

        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.PersonaView)
                Do While lector.Read
                    Dim obj As New Entidades.PersonaView
                    With obj
                        .IdPersona = CInt(lector("IdPersona"))
                        .Nombre = CStr(IIf(IsDBNull(lector("Nombre")), "---", lector("Nombre")))
                        .Ruc = CStr(IIf(IsDBNull(lector("Ruc")), "---", lector("Ruc")))
                        .Dni = CStr(IIf(IsDBNull(lector("Dni")), "---", lector("Dni")))
                        .Nacionalidad = CStr(IIf(IsDBNull(lector("Nacionalidad")), "---", lector("Nacionalidad")))
                        .Estado = CStr(IIf(IsDBNull(lector("estado")), "---", lector("estado")))
                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function




#End Region

End Class
