'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOProfesionPersona
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Sub InsertaProfesionPersona(ByVal cn As SqlConnection, ByVal profesionpersona As Entidades.ProfesionPersona, ByVal T As SqlTransaction)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@IdProfesion", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(profesionpersona.IdProfesion = Nothing, DBNull.Value, profesionpersona.IdProfesion)
        ArrayParametros(1) = New SqlParameter("@profp_Principal", SqlDbType.Bit)
        ArrayParametros(1).Value = IIf(profesionpersona.Principal = Nothing, DBNull.Value, profesionpersona.Principal)
        ArrayParametros(2) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(2).Value = IIf(profesionpersona.IdPersona = Nothing, DBNull.Value, profesionpersona.IdPersona)
        HDAO.InsertaT(cn, "_ProfesionPersonaInsert", ArrayParametros, T)
    End Sub

    Public Sub InsertaListProfesionPersona(ByVal cn As SqlConnection, ByVal profesionpersona As List(Of Entidades.ProfesionPersona), ByVal T As SqlTransaction)
        Dim objprofesionpersona As Entidades.ProfesionPersona
        For Each objprofesionpersona In profesionpersona
            Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
            ArrayParametros(0) = New SqlParameter("@IdProfesion", SqlDbType.Int)
            ArrayParametros(0).Value = IIf(objprofesionpersona.IdProfesion = Nothing, DBNull.Value, objprofesionpersona.IdProfesion)
            ArrayParametros(1) = New SqlParameter("@profp_Principal", SqlDbType.Bit)
            ArrayParametros(1).Value = IIf(objprofesionpersona.Principal = Nothing, DBNull.Value, objprofesionpersona.Principal)
            ArrayParametros(2) = New SqlParameter("@IdPersona", SqlDbType.Int)
            ArrayParametros(2).Value = IIf(objprofesionpersona.IdPersona = Nothing, DBNull.Value, objprofesionpersona.IdPersona)
            HDAO.InsertaT(cn, "_ProfesionPersonaInsert", ArrayParametros, T)
        Next
    End Sub


    Public Function ActualizaProfesionPersona(ByVal profesionpersona As Entidades.ProfesionPersona) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@IdProfesion", SqlDbType.Int)
        ArrayParametros(0).Value = profesionpersona.IdProfesion
        ArrayParametros(1) = New SqlParameter("@profp_Principal", SqlDbType.Bit)
        ArrayParametros(1).Value = profesionpersona.Principal
        ArrayParametros(2) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(2).Value = profesionpersona.IdPersona
        Return HDAO.Update(cn, "_ProfesionPersonaUpdate", ArrayParametros)
    End Function
    Public Function SelectxId(ByVal IdPersona As Integer) As Entidades.ProfesionPersona
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProfesionPersonaSelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Using cn
            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
            Dim objProfesionPersona As New Entidades.ProfesionPersona
            If lector.Read Then
                objProfesionPersona = New Entidades.ProfesionPersona
                objProfesionPersona.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                objProfesionPersona.IdProfesion = CInt(IIf(IsDBNull(lector.Item("IdProfesion")) = True, 0, lector.Item("IdProfesion")))
                objProfesionPersona.Principal = CBool(IIf(IsDBNull(lector.Item("profp_Principal")) = True, Nothing, lector.Item("profp_Principal")))
            End If
            lector.Close()
            Return objProfesionPersona
        End Using
    End Function
    Public Sub ActualizaProfesionPersonaT(ByVal cn As SqlConnection, ByVal profesion As Entidades.ProfesionPersona, ByVal T As SqlTransaction)
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@IdProfesion", SqlDbType.Int)
        ArrayParametros(0).Value = IIf(profesion.IdProfesion = Nothing, DBNull.Value, profesion.IdProfesion)
        ArrayParametros(1) = New SqlParameter("@profp_Principal", SqlDbType.Int)
        ArrayParametros(1).Value = IIf(profesion.Principal = Nothing, DBNull.Value, profesion.Principal)
        ArrayParametros(2) = New SqlParameter("@IdPersona", SqlDbType.Int)
        ArrayParametros(2).Value = IIf(profesion.IdPersona = Nothing, DBNull.Value, profesion.IdPersona)

        HDAO.UpdateT(cn, "_ProfesionPersonaUpdate", ArrayParametros, T)
    End Sub


    Public Function Selectx_Id(ByVal IdPersona As Integer) As List(Of Entidades.ProfesionPersona)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_ProfesionPersonaSelectxIdPersona", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdPersona", IdPersona)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.ProfesionPersona)
                Do While lector.Read
                    Dim objProfesionPersona As New Entidades.ProfesionPersona
                    objProfesionPersona = New Entidades.ProfesionPersona
                    objProfesionPersona.IdPersona = CInt(IIf(IsDBNull(lector.Item("IdPersona")) = True, 0, lector.Item("IdPersona")))
                    objProfesionPersona.IdProfesion = CInt(IIf(IsDBNull(lector.Item("IdProfesion")) = True, 0, lector.Item("IdProfesion")))
                    objProfesionPersona.Principal = CBool(IIf(IsDBNull(lector.Item("profp_Principal")) = True, Nothing, lector.Item("profp_Principal")))
                    Lista.Add(objProfesionPersona)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
    End Function

End Class
