﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOEmpresaTiendaUsuario
    Private objConexion As New DAO.Conexion
    Private objHelper As New DAO.HelperDAO
    Private cn As SqlConnection
    Private cmd As SqlCommand
    Private lector As SqlDataReader

    Public Function SelectEscalaTienda() As List(Of Entidades.Tienda)
        Dim Lista As New List(Of Entidades.Tienda)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_SelectEscalaTiendas", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)

            Do While lector.Read
                Dim tienda As New Entidades.Tienda
                tienda.IdEscala = CInt(IIf(IsDBNull(lector("idescala")), 0, lector("idescala")))
                tienda.NombreEscala = CStr(IIf(IsDBNull(lector("nombre_escala")), "", lector("nombre_escala")))

                Lista.Add(tienda)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function
    Public Function SelectTiendaObjetivo() As List(Of Entidades.Tienda)
        Dim Lista As New List(Of Entidades.Tienda)
        cn = objConexion.ConexionSIGE
        cmd = New SqlCommand("_SelectTiendaObjetivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            cn.Open()
            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)

            Do While lector.Read
                Dim tienda As New Entidades.Tienda
                tienda.Id = CInt(IIf(IsDBNull(lector("IdTienda")), 0, lector("IdTienda")))
                tienda.Nombre = CStr(IIf(IsDBNull(lector("tie_Nombre")), "", lector("tie_Nombre")))

                Lista.Add(tienda)
            Loop
            lector.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Lista
    End Function
    Public Function fn_GetTiendaPrincipal(ByVal IdUsuario As Integer) As String
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand
        cmd.CommandText = "SELECT dbo.fn_GetTiendaPrincipal(" + CStr(IdUsuario) + ")"
        cmd.Connection = cn
        cmd.CommandType = CommandType.Text
        cmd.CommandTimeout = 0
        Try
            cn.Open()
            Return cmd.ExecuteScalar.ToString
        Catch ex As Exception
            Throw ex
        Finally
            If cn.State = ConnectionState.Open Then cn.Close()
        End Try
        Return Nothing
    End Function

    Public Function SelectEmpresaxIdUsuario(ByVal Idusuario As Integer) As List(Of Entidades.Propietario)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EmpresaTiendaUsuarioSelectEmpresaxIdUsuario", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdUsuario", Idusuario)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Propietario)
                Do While lector.Read
                    Dim obj As New Entidades.Propietario
                    With obj

                        .Id = CInt(IIf(IsDBNull(lector("IdEmpresa")) = True, 0, lector("IdEmpresa")))
                        .NombreComercial = CStr(IIf(IsDBNull(lector("per_NComercial")) = True, "", lector("per_NComercial")))

                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxIdUsuario(ByVal Idusuario As Integer) As List(Of Entidades.EmpresaTiendaUsuario)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EmpresaTiendaUsuarioSelectxIdUsuario", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdUsuario", Idusuario)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.EmpresaTiendaUsuario)
                Do While lector.Read
                    Dim obj As New Entidades.EmpresaTiendaUsuario
                    With obj
                        .IdEmpresa = CInt(IIf(IsDBNull(lector("IdEmpresa")) = True, 0, lector("IdEmpresa")))
                        .IdTienda = CInt(IIf(IsDBNull(lector("IdTienda")) = True, 0, lector("IdTienda")))
                        .IdUsuario = CInt(IIf(IsDBNull(lector("IdUsuario")) = True, 0, lector("IdUsuario")))
                        .DescEmpresa = CStr(IIf(IsDBNull(lector("per_NComercial")) = True, "", lector("per_NComercial")))
                        .DescTienda = CStr(IIf(IsDBNull(lector("tie_Nombre")) = True, "", lector("tie_Nombre")))
                        .Principal = CBool(IIf(IsDBNull(lector("etu_Principal")) = True, 0, lector("etu_Principal")))
                        .Estado = CBool(IIf(IsDBNull(lector("etu_Estado")) = True, 0, lector("etu_Estado")))

                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Sub DeletexIdUsuario(ByVal IdUsuario As Integer, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim cmd As New SqlCommand("_EmpresaTiendaUsuarioDeletexIdUsuario", cn, tr)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario)
        cmd.ExecuteNonQuery()

    End Sub
    Public Sub Insert(ByVal objETU As Entidades.EmpresaTiendaUsuario, ByVal cn As SqlConnection, ByVal tr As SqlTransaction)

        Dim p() As SqlParameter = New SqlParameter(4) {}

        p(0) = New SqlParameter("@IdEmpresa", SqlDbType.Int)
        p(0).Value = IIf(objETU.IdEmpresa = Nothing, DBNull.Value, objETU.IdEmpresa)

        p(1) = New SqlParameter("@IdTienda", SqlDbType.Int)
        p(1).Value = IIf(objETU.IdTienda = Nothing, DBNull.Value, objETU.IdTienda)

        p(2) = New SqlParameter("@IdUsuario", SqlDbType.Int)
        p(2).Value = IIf(objETU.IdUsuario = Nothing, DBNull.Value, objETU.IdUsuario)

        p(3) = New SqlParameter("@Principal", SqlDbType.Bit)
        p(3).Value = IIf(objETU.Principal = Nothing, DBNull.Value, objETU.Principal)

        p(4) = New SqlParameter("@Estado", SqlDbType.Bit)
        p(4).Value = IIf(objETU.Estado = Nothing, DBNull.Value, objETU.Estado)



        objHelper.InsertaT(cn, "_EmpresaTiendaUsuarioInsert", p, tr)

    End Sub
    Public Function SelectTiendaxIdUsuario(ByVal Idusuario As Integer) As List(Of Entidades.Tienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EmpresaTiendaUsuarioSelectTiendaxIdUsuario", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdUsuario", Idusuario)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tienda)
                Do While lector.Read
                    Dim obj As New Entidades.Tienda
                    With obj

                        .Id = CInt(IIf(IsDBNull(lector("IdTienda")) = True, 0, lector("IdTienda")))


                        .Nombre = CStr(IIf(IsDBNull(lector("tie_Nombre")) = True, "", lector("tie_Nombre")))


                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectTiendaxIdEmpresaxIdUsuario(ByVal Idusuario As Integer, ByVal IdEmpresa As Integer) As List(Of Entidades.Tienda)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_EmpresaTiendaUsuarioSelectTiendaxIdEmpresaxIdUsuario", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdUsuario", Idusuario)
        cmd.Parameters.AddWithValue("@IdEmpresa", IdEmpresa)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Tienda)
                Do While lector.Read
                    Dim obj As New Entidades.Tienda
                    With obj

                        .Id = CInt(IIf(IsDBNull(lector("IdTienda")) = True, 0, lector("IdTienda")))


                        .Nombre = CStr(IIf(IsDBNull(lector("tie_Nombre")) = True, "", lector("tie_Nombre")))


                    End With
                    Lista.Add(obj)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function

End Class
