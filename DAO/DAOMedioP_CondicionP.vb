﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Chang Carnero, Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 27-Oct-2009
'Hora    : 08:02 pm
'*************************************************
Imports System.Data.SqlClient
Public Class DAOMedioP_CondicionP
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Sub InsertaListaCondicionPago(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal lista As List(Of Entidades.MedioPago_CondicionPago), ByVal IdCondicionP As Integer)
        Dim fila As New Entidades.MedioPago_CondicionPago
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@IdCondicionPago", SqlDbType.Int)
        ArrayParametros(0).Value = IdCondicionP
        For Each fila In lista
            ArrayParametros(1) = New SqlParameter("@IdMedioPago", SqlDbType.Int)
            ArrayParametros(1).Value = fila.IdMedioPago

            HDAO.InsertaT(cn, "_CondicionPago_MedioPagoInsert", ArrayParametros, tr)
        Next
    End Sub
    Public Function DeletexCondicionPago_MedioPagoBorrar(ByVal cn As SqlConnection, ByVal tr As SqlTransaction, ByVal IdCondicionPago As Integer) As Boolean
        Try
            Dim cmd As New SqlCommand("_CondicionPago_MedioPagoBorrar", cn, tr)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@IdCondicionPago", IdCondicionPago)
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Return False
        Finally

        End Try
    End Function
    'Para EDITAR la Condición de Pago mediante el IdMedioPago
    Public Function _CondicionPago_MedioPagoSelectxIdCondicionPago(ByVal IdMedioP As Integer) As List(Of Entidades.MedioPago_CondicionPago)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CondicionPago_MedioPagoSelectxIdcondicionPago", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@idCondicionPago", IdMedioP)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                'Dim cmd As New SqlCommand("_CondicionPago_MedioPagoSelectxIdCondicionPago", cn)
                'cmd.CommandType = CommandType.StoredProcedure
                'cmd.Parameters.AddWithValue("@idCondicionPago", IdMedioP)
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim lista As New List(Of Entidades.MedioPago_CondicionPago)
                Do While lector.Read
                    Dim obj As New Entidades.MedioPago_CondicionPago
                    obj.IdCondicionPago = CInt(lector.Item("IdCondicionPago"))
                    obj.IdMedioPago = CInt(lector.Item("IdMedioPago"))
                    obj.NomMedioPago = CStr(lector.Item("mp_Nombre"))
                    lista.Add(obj)
                Loop
                lector.Close()
                Return lista
            End Using
        Catch ex As Exception
            Throw New Exception
        Finally

        End Try
    End Function
    'Public Function SelectxIdBanco(ByVal IdBanco As Integer) As List(Of Entidades.Oficina)
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim cmd As New SqlCommand("_OficinaSelectxIdBanco1", cn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.AddWithValue("@IdBanco", IdBanco)
    '    Dim lector As SqlDataReader
    '    Try
    '        Using cn
    '            cn.Open()
    '            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
    '            Dim Lista As New List(Of Entidades.Oficina)
    '            Do While lector.Read
    '                Dim objOficina As New Entidades.Oficina
    '                objOficina.IdBanco = CInt(IIf(IsDBNull(lector.Item("IdBanco")) = True, 0, lector.Item("IdBanco")))
    '                objOficina.Id = CInt(IIf(IsDBNull(lector.Item("IdOficina")) = True, 0, lector.Item("IdOficina")))
    '                objOficina.Descripcion = CStr(IIf(IsDBNull(lector.Item("of_Nombre")) = True, 0, lector.Item("of_Nombre")))
    '                objOficina.Estado = CChar(IIf(IsDBNull(lector.Item("of_Estado")) = True, "", lector.Item("of_Estado")))
    '                Lista.Add(objOficina)
    '            Loop
    '            lector.Close()
    '            Return Lista
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    'Public Function _MotivoT_TipoOperacionSelectxIdTraslado(ByVal IdTraslado As Integer) As List(Of Entidades.MotivoT_TipoOperacion)
    '    Dim cn As SqlConnection = objConexion.ConexionSIGE
    '    Dim cmd As New SqlCommand("_MotivoT_TipoOperacionSelectxIdMotivoT", cn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.AddWithValue("@idTraslado", IdTraslado)
    '    Dim lector As SqlDataReader
    '    Try
    '        Using cn
    '            cn.Open()
    '            lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
    '            Dim lista As New List(Of Entidades.MotivoT_TipoOperacion)
    '            Do While lector.Read
    '                Dim obj As New Entidades.MotivoT_TipoOperacion
    '                obj.IdMotivoT = CInt(lector.Item("IdMotivoT"))
    '                obj.idTipoOperac = CInt(lector.Item("IdTipoOperacion"))
    '                obj.NombreTipoOperacion = CStr(lector.Item("top_Nombre"))
    '                lista.Add(obj)
    '            Loop
    '            lector.Close()
    '            Return lista
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
  
End Class
