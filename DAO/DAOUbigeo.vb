'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.



'**********************   VIERNES 25 06 2010

Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class DAOUbigeo
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Private cn As SqlConnection = (New DAO.Conexion).ConexionSIGE
    Private reader As SqlDataReader
    Private cmd As SqlCommand
    Private objDaoMantenedor As New DAO.DAOMantenedor

    Public Function InsertaUbigeo(ByVal ubigeo As Entidades.Ubigeo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@ub_CodDpto", SqlDbType.Char)
        ArrayParametros(0).Value = ubigeo.CodDpto
        ArrayParametros(1) = New SqlParameter("@ub_CodProv", SqlDbType.Char)
        ArrayParametros(1).Value = ubigeo.CodProv
        ArrayParametros(2) = New SqlParameter("@ub_CodDist", SqlDbType.Char)
        ArrayParametros(2).Value = ubigeo.CodDist
        ArrayParametros(3) = New SqlParameter("@ub_Nombre", SqlDbType.VarChar)
        ArrayParametros(3).Value = ubigeo.Nombre
        ArrayParametros(4) = New SqlParameter("@ub_Estado", SqlDbType.Char)
        ArrayParametros(4).Value = ubigeo.Estado
        Return HDAO.Insert(cn, "_UbigeoInsert", ArrayParametros)
    End Function
    Public Function ActualizaUbigeo(ByVal ubigeo As Entidades.Ubigeo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
        ArrayParametros(0) = New SqlParameter("@ub_CodDpto", SqlDbType.Char)
        ArrayParametros(0).Value = ubigeo.CodDpto
        ArrayParametros(1) = New SqlParameter("@ub_CodProv", SqlDbType.Char)
        ArrayParametros(1).Value = ubigeo.CodProv
        ArrayParametros(2) = New SqlParameter("@ub_CodDist", SqlDbType.Char)
        ArrayParametros(2).Value = ubigeo.CodDist
        ArrayParametros(3) = New SqlParameter("@ub_Nombre", SqlDbType.VarChar)
        ArrayParametros(3).Value = ubigeo.Nombre
        ArrayParametros(4) = New SqlParameter("@ub_Estado", SqlDbType.Char)
        ArrayParametros(4).Value = ubigeo.Estado
        ArrayParametros(5) = New SqlParameter("@IdUbigeo", SqlDbType.Int)
        ArrayParametros(5).Value = ubigeo.Id
        Return HDAO.Update(cn, "_UbigeoUpdate", ArrayParametros)
    End Function

    Public Function ActualizaUbigeoZona(ByVal ubigeo As Entidades.Ubigeo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}

        ArrayParametros(0) = New SqlParameter("@ub_CodDpto", SqlDbType.Char, 2)
        ArrayParametros(0).Value = ubigeo.CodDpto
        ArrayParametros(1) = New SqlParameter("@ub_CodProv", SqlDbType.Char, 2)
        ArrayParametros(1).Value = ubigeo.CodProv
        ArrayParametros(2) = New SqlParameter("@ub_CodDist", SqlDbType.Char, 2)
        ArrayParametros(2).Value = ubigeo.CodDist
        ArrayParametros(3) = New SqlParameter("@IdZona", SqlDbType.Int)
        ArrayParametros(3).Value = ubigeo.IdZona
        
        Return HDAO.Update(cn, "_UbigeoUpdateZona", ArrayParametros)
    End Function

    Public Function SelectAllDepartamentos() As List(Of Entidades.Ubigeo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UbigeoSelectAllDepartamentos", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Ubigeo)
                Do While lector.Read
                    Dim ub As New Entidades.Ubigeo
                    ub.CodDpto = CStr(lector.Item("ub_coddpto"))
                    ub.Nombre = CStr(lector.Item("ub_nombre"))
                    Lista.Add(ub)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllProvinciasxCodDpto(ByVal CodDpto As String) As List(Of Entidades.Ubigeo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UbigeoSelectAllProvinciasxCodDpto", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@ub_CodDpto", CodDpto)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Ubigeo)
                Do While lector.Read
                    Dim ub As New Entidades.Ubigeo
                    ub.CodProv = CStr(lector.Item("ub_codprov"))
                    ub.Nombre = CStr(lector.Item("ub_nombre"))
                    Lista.Add(ub)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllDistritosxCodDptoxCodProv(ByVal CodDpto As String, ByVal CodProv As String) As List(Of Entidades.Ubigeo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_UbigeoSelectAllDistritosxCodDptoxCodProv", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@ub_CodDpto", CodDpto)
        cmd.Parameters.AddWithValue("@ub_CodProv", CodProv)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Ubigeo)
                Do While lector.Read
                    Dim ub As New Entidades.Ubigeo
                    ub.CodDist = CStr(lector.Item("ub_coddist"))
                    ub.Nombre = CStr(lector.Item("ub_nombre"))
                    Lista.Add(ub)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllDatos() As DataTable

        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Try
            Using cn
                cn.Open()
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter("_UbigeoSelectAllDatos", cn)
                da.SelectCommand.CommandType = CommandType.StoredProcedure
                da.Fill(dt)
                Return dt
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SelectxCodDeptoxCodProvxCodDistrito(ByVal CodDepto As String, ByVal CodProv As String, ByVal CodDist As String) As Entidades.Ubigeo

        Dim objUbigeo As Entidades.Ubigeo = Nothing

        Try

            Dim p() As SqlParameter = New SqlParameter(2) {}
            p(0) = objDaoMantenedor.getParam(CodDepto, "@CodDepto", SqlDbType.VarChar)
            p(1) = objDaoMantenedor.getParam(CodProv, "@CodProv", SqlDbType.VarChar)
            p(2) = objDaoMantenedor.getParam(CodDist, "@CodDist", SqlDbType.VarChar)

            cn.Open()
            reader = SqlHelper.ExecuteReader(cn, CommandType.StoredProcedure, "_Ubigeo_SelectxCodDeptoxCodProvxCodDistrito", p)

            If (reader.Read) Then
                objUbigeo = New Entidades.Ubigeo
                With objUbigeo

                    .Id = objDaoMantenedor.UCInt(reader("IdUbigeo"))
                    .CodDpto = objDaoMantenedor.UCStr(reader("ub_CodDpto"))
                    .CodProv = objDaoMantenedor.UCStr(reader("ub_CodProv"))
                    .CodDist = objDaoMantenedor.UCStr(reader("ub_CodDist"))
                    .IdZona = objDaoMantenedor.UCInt(reader("IdZona"))
                    .Nombre = objDaoMantenedor.UCStr(reader("ub_Nombre"))
                    .Estado = objDaoMantenedor.UCStr(reader("ub_Estado"))

                End With
            End If
            reader.Close()

        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then cn.Close()
        End Try

        Return objUbigeo

    End Function

End Class
