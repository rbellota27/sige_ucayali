'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient
Public Class DAOCargo
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaCargo(ByVal cargo As Entidades.Cargo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(1) {}
        ArrayParametros(0) = New SqlParameter("@car_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = cargo.Nombre
        ArrayParametros(1) = New SqlParameter("@car_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = cargo.Estado
        Return HDAO.Insert(cn, "_CargoInsert", ArrayParametros)
    End Function
    Public Function ActualizaCargo(ByVal cargo As Entidades.Cargo) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(2) {}
        ArrayParametros(0) = New SqlParameter("@car_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = cargo.Nombre
        ArrayParametros(1) = New SqlParameter("@car_Estado", SqlDbType.Char)
        ArrayParametros(1).Value = cargo.Estado
        ArrayParametros(2) = New SqlParameter("@IdCargo", SqlDbType.Int)
        ArrayParametros(2).Value = cargo.Id
        Return HDAO.Update(cn, "_CargoUpdate", ArrayParametros)
    End Function
    Public Function SelectAll() As List(Of Entidades.Cargo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CargoSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Cargo)
                Do While lector.Read
                    Dim cargo As New Entidades.Cargo
                    cargo.Id = CInt(lector.Item("IdCargo"))
                    cargo.Nombre = CStr(lector.Item("car_Nombre"))
                    cargo.Estado = CStr(lector.Item("car_Estado"))
                    Lista.Add(cargo)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Cargo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CargoSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Cargo)
                Do While lector.Read
                    Dim cargo As New Entidades.Cargo
                    cargo.Id = CInt(lector.Item("IdCargo"))
                    cargo.Nombre = CStr(lector.Item("car_Nombre"))
                    cargo.Estado = CStr(lector.Item("car_Estado"))
                    Lista.Add(cargo)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Cargo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CargoSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Cargo)
                Do While lector.Read
                    Dim cargo As New Entidades.Cargo
                    cargo.Id = CInt(lector.Item("IdCargo"))
                    cargo.Nombre = CStr(lector.Item("car_Nombre"))
                    cargo.Estado = CStr(lector.Item("car_Estado"))
                    Lista.Add(cargo)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal IdCargo As Integer) As List(Of Entidades.Cargo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CargoSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@IdCargo", IdCargo)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Cargo)
                Do While lector.Read
                    Dim cargo As New Entidades.Cargo
                    cargo.Id = CInt(lector.Item("IdCargo"))
                    cargo.Nombre = CStr(lector.Item("car_Nombre"))
                    cargo.Estado = CStr(lector.Item("car_Estado"))
                    Lista.Add(cargo)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal Nombre As String) As List(Of Entidades.Cargo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CargoSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", Nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Cargo)
                Do While lector.Read
                    Dim cargo As New Entidades.Cargo
                    cargo.Id = CInt(lector.Item("IdCargo"))
                    cargo.Nombre = CStr(lector.Item("car_Nombre"))
                    cargo.Estado = CStr(lector.Item("car_Estado"))
                    Lista.Add(cargo)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal Nombre As String) As List(Of Entidades.Cargo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CargoSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", Nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Cargo)
                Do While lector.Read
                    Dim cargo As New Entidades.Cargo
                    cargo.Id = CInt(lector.Item("IdCargo"))
                    cargo.Nombre = CStr(lector.Item("car_Nombre"))
                    cargo.Estado = CStr(lector.Item("car_Estado"))
                    Lista.Add(cargo)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal Nombre As String) As List(Of Entidades.Cargo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CargoSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", Nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Cargo)
                Do While lector.Read
                    Dim cargo As New Entidades.Cargo
                    cargo.Id = CInt(lector.Item("IdCargo"))
                    cargo.Nombre = CStr(lector.Item("car_Nombre"))
                    cargo.Estado = CStr(lector.Item("car_Estado"))
                    Lista.Add(cargo)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectCbo() As List(Of Entidades.Cargo)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_CargoSelectCbo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Cargo)
                Do While lector.Read
                    Dim Cargo As New Entidades.Cargo
                    Cargo.Id = CInt(lector.Item("IdCargo"))
                    Cargo.Nombre = CStr(lector.Item("car_Nombre"))
                    Cargo.Estado = CStr(lector.Item("car_Estado"))
                    Lista.Add(Cargo)
                Loop
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
