'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOCargoProgramado
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaCargoProgramado(ByVal cargoprogramado As Entidades.CargoProgramado) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@IdMovCuenta", SqlDbType.Int)
        ArrayParametros(0).Value = cargoprogramado.IdMovCuenta
        ArrayParametros(1) = New SqlParameter("@cap_FechaProg", SqlDbType.DateTime)
        ArrayParametros(1).Value = cargoprogramado.FechaProg
        ArrayParametros(2) = New SqlParameter("@cap_Monto", SqlDbType.Decimal)
        ArrayParametros(2).Value = cargoprogramado.Monto
        ArrayParametros(3) = New SqlParameter("@cap_FechaPago", SqlDbType.DateTime)
        ArrayParametros(3).Value = cargoprogramado.FechaPago
        ArrayParametros(4) = New SqlParameter("@IdEstadoCargoP", SqlDbType.Int)
        ArrayParametros(4).Value = cargoprogramado.IdEstadoCargoP
        Return HDAO.Insert(cn, "_CargoProgramadoInsert", ArrayParametros)
    End Function
    Public Function ActualizaCargoProgramado(ByVal cargoprogramado As Entidades.CargoProgramado) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(5) {}
        ArrayParametros(0) = New SqlParameter("@IdMovCuenta", SqlDbType.Int)
        ArrayParametros(0).Value = cargoprogramado.IdMovCuenta
        ArrayParametros(1) = New SqlParameter("@cap_FechaProg", SqlDbType.DateTime)
        ArrayParametros(1).Value = cargoprogramado.FechaProg
        ArrayParametros(2) = New SqlParameter("@cap_Monto", SqlDbType.Decimal)
        ArrayParametros(2).Value = cargoprogramado.Monto
        ArrayParametros(3) = New SqlParameter("@cap_FechaPago", SqlDbType.DateTime)
        ArrayParametros(3).Value = cargoprogramado.FechaPago
        ArrayParametros(4) = New SqlParameter("@IdEstadoCargoP", SqlDbType.Int)
        ArrayParametros(4).Value = cargoprogramado.IdEstadoCargoP
        ArrayParametros(5) = New SqlParameter("@IdCargoProgramado", SqlDbType.Int)
        ArrayParametros(5).Value = cargoprogramado.IdCargoProgramado
        Return HDAO.Update(cn, "_CargoProgramadoUpdate", ArrayParametros)
    End Function




End Class
