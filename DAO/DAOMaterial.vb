'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re p�rrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Imports System.Data.SqlClient

Public Class DAOMaterial
    Dim HDAO As New DAO.HelperDAO
    Dim objConexion As New Conexion
    Public Function InsertaMaterial(ByVal material As Entidades.Material) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(3) {}
        ArrayParametros(0) = New SqlParameter("@mat_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = material.Descripcion
        ArrayParametros(1) = New SqlParameter("@mat_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = material.Abv

        ArrayParametros(2) = New SqlParameter("@mat_Codigo", SqlDbType.Char, 2)
        ArrayParametros(2).Value = material.For_Codigo

        ArrayParametros(3) = New SqlParameter("@mat_estado", SqlDbType.Char)
        ArrayParametros(3).Value = material.Estado
        'Return HDAO.Insert(cn, "_FormatoInsert", ArrayParametros)
        Dim cmd As New SqlCommand("_FormatoInsert", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddRange(ArrayParametros)
        Try
            cn.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally

        End Try
        Return True
    End Function
    Public Function ActualizaMaterial(ByVal material As Entidades.Material) As Boolean
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim ArrayParametros() As SqlParameter = New SqlParameter(4) {}
        ArrayParametros(0) = New SqlParameter("@mat_Nombre", SqlDbType.VarChar)
        ArrayParametros(0).Value = material.Descripcion
        ArrayParametros(1) = New SqlParameter("@mat_Abv", SqlDbType.VarChar)
        ArrayParametros(1).Value = material.Abv

        ArrayParametros(2) = New SqlParameter("@mat_Codigo", SqlDbType.Char, 2)
        ArrayParametros(2).Value = material.For_Codigo

        ArrayParametros(3) = New SqlParameter("@mat_estado", SqlDbType.Char)
        ArrayParametros(3).Value = material.Estado
        ArrayParametros(4) = New SqlParameter("@IdMaterial", SqlDbType.Int)
        ArrayParametros(4).Value = material.Id
        Return HDAO.Update(cn, "_FormatoUpdate", ArrayParametros)
    End Function
    Public Function SelectAll() As List(Of Entidades.Material)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_FormatoSelectAll", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Material)
                Do While lector.Read
                    Dim objMaterial As New Entidades.Material
                    objMaterial.Abv = CStr(IIf(IsDBNull(lector.Item("mat_Abv")) = True, "", lector.Item("mat_Abv")))
                    objMaterial.Descripcion = CStr(IIf(IsDBNull(lector.Item("mat_Nombre")) = True, "", lector.Item("mat_Nombre")))
                    objMaterial.Estado = CStr(IIf(IsDBNull(lector.Item("mat_estado")) = True, "", lector.Item("mat_estado")))
                    objMaterial.Id = CInt(IIf(IsDBNull(lector.Item("IdMaterial")) = True, 0, lector.Item("IdMaterial")))
                    objMaterial.For_Codigo = CStr(iif(IsDBNull(lector.Item("mat_Codigo")) = True, "", lector.Item("mat_Codigo")))
                    Lista.Add(objMaterial)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllActivo() As List(Of Entidades.Material)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_FormatoSelectAllActivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Material)
                Do While lector.Read
                    Dim objMaterial As New Entidades.Material
                    objMaterial.Abv = CStr(IIf(IsDBNull(lector.Item("mat_Abv")) = True, "", lector.Item("mat_Abv")))
                    objMaterial.Descripcion = CStr(IIf(IsDBNull(lector.Item("mat_Nombre")) = True, "", lector.Item("mat_Nombre")))
                    objMaterial.Estado = CStr(IIf(IsDBNull(lector.Item("mat_estado")) = True, "", lector.Item("mat_estado")))
                    objMaterial.Id = CInt(IIf(IsDBNull(lector.Item("IdMaterial")) = True, 0, lector.Item("IdMaterial")))
                    objMaterial.For_Codigo = CStr(IIf(IsDBNull(lector.Item("mat_Codigo")) = True, "", lector.Item("mat_Codigo")))
                    Lista.Add(objMaterial)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllInactivo() As List(Of Entidades.Material)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_FormatoSelectAllInactivo", cn)
        cmd.CommandType = CommandType.StoredProcedure
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Material)
                Do While lector.Read
                    Dim objMaterial As New Entidades.Material
                    objMaterial.Abv = CStr(IIf(IsDBNull(lector.Item("mat_Abv")) = True, "", lector.Item("mat_Abv")))
                    objMaterial.Descripcion = CStr(IIf(IsDBNull(lector.Item("mat_Nombre")) = True, "", lector.Item("mat_Nombre")))
                    objMaterial.Estado = CStr(IIf(IsDBNull(lector.Item("mat_estado")) = True, "", lector.Item("mat_estado")))
                    objMaterial.Id = CInt(IIf(IsDBNull(lector.Item("IdMaterial")) = True, 0, lector.Item("IdMaterial")))
                    objMaterial.For_Codigo = CStr(IIf(IsDBNull(lector.Item("mat_Codigo")) = True, "", lector.Item("mat_Codigo")))
                    Lista.Add(objMaterial)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectAllxNombre(ByVal nombre As String) As List(Of Entidades.Material)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_FormatoSelectAllxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Material)
                Do While lector.Read
                    Dim objMaterial As New Entidades.Material
                    objMaterial.Abv = CStr(IIf(IsDBNull(lector.Item("mat_Abv")) = True, "", lector.Item("mat_Abv")))
                    objMaterial.Descripcion = CStr(IIf(IsDBNull(lector.Item("mat_Nombre")) = True, "", lector.Item("mat_Nombre")))
                    objMaterial.Estado = CStr(IIf(IsDBNull(lector.Item("mat_estado")) = True, "", lector.Item("mat_estado")))
                    objMaterial.Id = CInt(IIf(IsDBNull(lector.Item("IdMaterial")) = True, 0, lector.Item("IdMaterial")))
                    objMaterial.For_Codigo = CStr(IIf(IsDBNull(lector.Item("mat_Codigo")) = True, "", lector.Item("mat_Codigo")))
                    Lista.Add(objMaterial)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectActivoxNombre(ByVal nombre As String) As List(Of Entidades.Material)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_FormatoSelectActivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Material)
                Do While lector.Read
                    Dim objMaterial As New Entidades.Material
                    objMaterial.Abv = CStr(IIf(IsDBNull(lector.Item("mat_Abv")) = True, "", lector.Item("mat_Abv")))
                    objMaterial.Descripcion = CStr(IIf(IsDBNull(lector.Item("mat_Nombre")) = True, "", lector.Item("mat_Nombre")))
                    objMaterial.Estado = CStr(IIf(IsDBNull(lector.Item("mat_estado")) = True, "", lector.Item("mat_estado")))
                    objMaterial.Id = CInt(IIf(IsDBNull(lector.Item("IdMaterial")) = True, 0, lector.Item("IdMaterial")))
                    objMaterial.For_Codigo = CStr(IIf(IsDBNull(lector.Item("mat_Codigo")) = True, "", lector.Item("mat_Codigo")))
                    Lista.Add(objMaterial)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectInactivoxNombre(ByVal nombre As String) As List(Of Entidades.Material)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_FormatoSelectInactivoxNombre", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Nombre", nombre)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Material)
                Do While lector.Read
                    Dim objMaterial As New Entidades.Material
                    objMaterial.Abv = CStr(IIf(IsDBNull(lector.Item("mat_Abv")) = True, "", lector.Item("mat_Abv")))
                    objMaterial.Descripcion = CStr(IIf(IsDBNull(lector.Item("mat_Nombre")) = True, "", lector.Item("mat_Nombre")))
                    objMaterial.Estado = CStr(IIf(IsDBNull(lector.Item("mat_estado")) = True, "", lector.Item("mat_estado")))
                    objMaterial.Id = CInt(IIf(IsDBNull(lector.Item("IdMaterial")) = True, 0, lector.Item("IdMaterial")))
                    objMaterial.For_Codigo = CStr(IIf(IsDBNull(lector.Item("mat_Codigo")) = True, "", lector.Item("mat_Codigo")))
                    Lista.Add(objMaterial)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
    Public Function SelectxId(ByVal id As Integer) As List(Of Entidades.Material)
        Dim cn As SqlConnection = objConexion.ConexionSIGE
        Dim cmd As New SqlCommand("_FormatoSelectxId", cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@Id", id)
        Dim lector As SqlDataReader
        Try
            Using cn
                cn.Open()
                lector = cmd.ExecuteReader(Data.CommandBehavior.CloseConnection)
                Dim Lista As New List(Of Entidades.Material)
                Do While lector.Read
                    Dim objMaterial As New Entidades.Material
                    objMaterial.Abv = CStr(IIf(IsDBNull(lector.Item("mat_Abv")) = True, "", lector.Item("mat_Abv")))
                    objMaterial.Descripcion = CStr(IIf(IsDBNull(lector.Item("mat_Nombre")) = True, "", lector.Item("mat_Nombre")))
                    objMaterial.Estado = CStr(IIf(IsDBNull(lector.Item("mat_estado")) = True, "", lector.Item("mat_estado")))
                    objMaterial.Id = CInt(IIf(IsDBNull(lector.Item("IdMaterial")) = True, 0, lector.Item("IdMaterial")))
                    Lista.Add(objMaterial)
                Loop
                lector.Close()
                Return Lista
            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Function
End Class
