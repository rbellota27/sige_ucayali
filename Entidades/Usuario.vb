'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Usuario
    Private _IdPersona As Integer
    Private _us_Login As String
    Private _us_Clave As String
    Private _us_Estado As String
    Private _us_FechaAlta As Date
    Private _us_FechaBaja As Date
    Private _IdMotivoBaja As Integer
    Private _corr_Nombre As String
    Private _nombres As String
    Private _FlagAtenPago As Integer

    Public Property FlagAtencionPago() As Integer
        Get
            Return Me._FlagAtenPago
        End Get
        Set(ByVal value As Integer)
            Me._FlagAtenPago = value
        End Set
    End Property
    Public Property Nombres() As String
        Get
            Return Me._nombres
        End Get
        Set(ByVal value As String)
            Me._nombres = value
        End Set
    End Property

    Public Property CorreoUsu() As String
        Get
            Return Me._corr_Nombre
        End Get
        Set(ByVal value As String)
            Me._corr_Nombre = value
        End Set
    End Property
    Public Property FechaAlta() As Date
        Get
            Return Me._us_FechaAlta
        End Get
        Set(ByVal value As Date)
            Me._us_FechaAlta = value
        End Set
    End Property

    Public Property FechaBaja() As Date
        Get
            Return Me._us_FechaBaja
        End Get
        Set(ByVal value As Date)
            Me._us_FechaBaja = value
        End Set
    End Property

    Public Property IdMotivoBaja() As Integer
        Get
            Return Me._IdMotivoBaja
        End Get
        Set(ByVal value As Integer)
            Me._IdMotivoBaja = value
        End Set
    End Property

    Public Property Login() As String
        Get
            Return Me._us_Login
        End Get
        Set(ByVal value As String)
            Me._us_Login = value
        End Set
    End Property

    Public Property Clave() As String
        Get
            Return Me._us_Clave
        End Get
        Set(ByVal value As String)
            Me._us_Clave = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._us_Estado
        End Get
        Set(ByVal value As String)
            Me._us_Estado = value
        End Set
    End Property

    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property
End Class
