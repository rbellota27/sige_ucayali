﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Pos_TipoTarjeta
    Protected _IdPos As Integer
    Protected _IdTipoTarjeta As Integer

    Public Sub New()

    End Sub

    Public Sub New(ByVal IdPos As Integer, ByVal IdTipoTarjeta As Integer)
        Me.IdPos = IdPos
        Me.IdTipoTarjeta = IdTipoTarjeta
    End Sub

    Public Property IdPos() As Integer
        Get
            Return _IdPos
        End Get
        Set(ByVal value As Integer)
            _IdPos = value
        End Set
    End Property

    Public Property IdTipoTarjeta() As Integer
        Get
            Return _IdTipoTarjeta
        End Get
        Set(ByVal value As Integer)
            _IdTipoTarjeta = value
        End Set
    End Property
End Class
