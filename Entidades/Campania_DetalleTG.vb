﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Campania_DetalleTG

    Private _IdCampaniaDetalle As Integer
    Private _IdProducto As Integer
    Private _IdUnidadMedida As Integer
    Private _IdMoneda As Integer
    Private _PrecioActual As Decimal
    Private _cdtg_Precio As Decimal
    Private _cdtg_Cantidad As Decimal
    Private _Cadena_IdUnidadMedida_Prod As String
    Private _Cadena_UnidadMedida_Prod As String
    Private _CodigoProducto As String
    Private _Producto As String
    Private _UnidadMedida As String
    Private _Moneda_Campania As String
    Private _ListaMoneda As List(Of Entidades.Moneda)
    Private _IdCampania As Integer
    Private _cdtg_cantCampania As Boolean

    Public Property cdtg_cantCampania() As Boolean
        Get
            Return _cdtg_cantCampania
        End Get
        Set(ByVal value As Boolean)
            _cdtg_cantCampania = value
        End Set
    End Property

    Public Property IdCampania() As Integer
        Get
            Return _IdCampania
        End Get
        Set(ByVal value As Integer)
            _IdCampania = value
        End Set
    End Property
    Public Property ListaMoneda() As List(Of Entidades.Moneda)
        Get
            Return Me._ListaMoneda
        End Get
        Set(ByVal value As List(Of Entidades.Moneda))
            Me._ListaMoneda = value
        End Set
    End Property

    Public Property PrecioActual() As Decimal
        Get
            Return Me._PrecioActual
        End Get
        Set(ByVal value As Decimal)
            Me._PrecioActual = value
        End Set
    End Property

    Public Property Moneda_Campania() As String
        Get
            Return Me._Moneda_Campania
        End Get
        Set(ByVal value As String)
            Me._Moneda_Campania = value
        End Set
    End Property
    Public Property UnidadMedida() As String
        Get
            Return Me._UnidadMedida
        End Get
        Set(ByVal value As String)
            Me._UnidadMedida = value
        End Set
    End Property

    Public Property CodigoProducto() As String
        Get
            Return Me._CodigoProducto
        End Get
        Set(ByVal value As String)
            Me._CodigoProducto = value
        End Set
    End Property

    Public Property Producto() As String
        Get
            Return Me._Producto
        End Get
        Set(ByVal value As String)
            Me._Producto = value
        End Set
    End Property

    Public Property Cadena_IdUnidadMedida_Prod() As String
        Get
            Return Me._Cadena_IdUnidadMedida_Prod
        End Get
        Set(ByVal value As String)
            Me._Cadena_IdUnidadMedida_Prod = value
        End Set
    End Property

    Public Property Cadena_UnidadMedida_Prod() As String
        Get
            Return Me._Cadena_UnidadMedida_Prod
        End Get
        Set(ByVal value As String)
            Me._Cadena_UnidadMedida_Prod = value
        End Set
    End Property

    Public Property IdCampaniaDetalle() As Integer
        Get
            Return _IdCampaniaDetalle
        End Get
        Set(ByVal value As Integer)
            _IdCampaniaDetalle = value
        End Set
    End Property

    Public Property IdProducto() As Integer
        Get
            Return _IdProducto
        End Get
        Set(ByVal value As Integer)
            _IdProducto = value
        End Set
    End Property

    Public Property IdUnidadMedida() As Integer
        Get
            Return _IdUnidadMedida
        End Get
        Set(ByVal value As Integer)
            _IdUnidadMedida = value
        End Set
    End Property

    Public Property IdMoneda() As Integer
        Get
            Return _IdMoneda
        End Get
        Set(ByVal value As Integer)
            _IdMoneda = value
        End Set
    End Property

    Public Property cdtg_Precio() As Decimal
        Get
            Return _cdtg_Precio
        End Get
        Set(ByVal value As Decimal)
            _cdtg_Precio = value
        End Set
    End Property

    Public Property cdtg_Cantidad() As Decimal
        Get
            Return _cdtg_Cantidad
        End Get
        Set(ByVal value As Decimal)
            _cdtg_Cantidad = value
        End Set
    End Property


    Public ReadOnly Property getListaUnidadMedida() As List(Of Entidades.UnidadMedida)
        Get

            Dim lista As New List(Of Entidades.UnidadMedida)

            Dim lista_IdUnidadMedida() As String = Cadena_IdUnidadMedida_Prod.Split(",")
            Dim lista_UnidadMedida() As String = Cadena_UnidadMedida_Prod.Split(",")

            For i As Integer = 0 To lista_IdUnidadMedida.Length - 1

                Dim objUnidadMedida As New Entidades.UnidadMedida
                With objUnidadMedida

                    .DescripcionCorto = lista_UnidadMedida(i)
                    .Id = CInt(lista_IdUnidadMedida(i))

                End With
                lista.Add(objUnidadMedida)

            Next

            Return lista

        End Get
    End Property


End Class
