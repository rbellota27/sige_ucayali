'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.


'*********************   MARTES 15062010

Public Class PagoCaja

    Private _IdDocumento As Integer
    Private _IdMedioPago As Integer
    Private _IdMoneda As Integer
    Private _IdTipoMovimiento As Integer
    Private _Pc_Monto As Decimal
    Private _Pc_Factor As Integer
    Private _Pc_Efectivo As Decimal
    Private _Pc_Vuelto As Decimal
    Private _Pc_Donacion As Decimal
    Private _Pc_Redondeo As Decimal
    Private _pc_NumeroOp As String
    Private _pc_NumeroCheque As String
    Private _pc_FechaACobrar As DateTime
    Private _IdBanco As Integer
    Private _IdCuentaBancaria As Integer
    Private _NomMedioPago As String
    Private _NomBanco As String
    Private _NumeroCuenta As String
    Private _NomMoneda As String
    Private _IdMonedaDestino As Integer
    Private _MontoEquivalenteDestino As Decimal
    Private _NomMonedaDestino As String
    Private _EfectivoInicial As Decimal
    Private _MontoEquivalenteVuelto As Decimal
    Private _descripcionPagoCaja As String
    Private _IdTarjeta As Integer
    Private _IdPost As Integer
    Private _IdMedioPagoInterfaz As Integer
    Private _IdDocumentoRef As Integer
    Private _IdCheque As Integer
    Private _vueltox As Decimal
    Private _difxtotalxpago As Decimal
    Public Property Vueltox() As Decimal
        Get
            Return Me._vueltox
        End Get
        Set(ByVal value As Decimal)
            Me._vueltox = value
        End Set
    End Property
    Public Property DifTotalxPago() As Decimal
        Get
            Return Me._difxtotalxpago
        End Get
        Set(ByVal value As Decimal)
            Me._difxtotalxpago = value
        End Set
    End Property

    Public Property IdCheque() As Integer
        Get
            Return Me._IdCheque
        End Get
        Set(ByVal value As Integer)
            Me._IdCheque = value
        End Set
    End Property

    Public Property IdDocumentoRef() As Integer
        Get
            Return Me._IdDocumentoRef
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumentoRef = value
        End Set
    End Property



    Public Property IdMedioPagoInterfaz() As Integer
        Get
            Return Me._IdMedioPagoInterfaz
        End Get
        Set(ByVal value As Integer)
            Me._IdMedioPagoInterfaz = value
        End Set
    End Property



    Public Property IdTarjeta() As Integer
        Get
            Return Me._IdTarjeta
        End Get
        Set(ByVal value As Integer)
            Me._IdTarjeta = value
        End Set
    End Property

    Public Property IdPost() As Integer
        Get
            Return Me._IdPost
        End Get
        Set(ByVal value As Integer)
            Me._IdPost = value
        End Set
    End Property


    Public Property descripcionPagoCaja() As String
        Get
            Return Me._descripcionPagoCaja
        End Get
        Set(ByVal value As String)
            Me._descripcionPagoCaja = value
        End Set
    End Property








    Public Property MontoEquivalenteVuelto() As Decimal
        Get
            Return Me._MontoEquivalenteVuelto
        End Get
        Set(ByVal value As Decimal)
            Me._MontoEquivalenteVuelto = value
        End Set
    End Property






    Public Property EfectivoInicial() As Decimal
        Get
            Return Me._EfectivoInicial
        End Get
        Set(ByVal value As Decimal)
            Me._EfectivoInicial = value
        End Set
    End Property

    Public ReadOnly Property getEfectivo() As Decimal
        Get
            If Me.IdMedioPago = 1 Then
                Return Me.Efectivo
            Else
                '**************** Si no es al contado, retornamos el Efectivo INICIAL
                Return Me.EfectivoInicial
            End If
        End Get
    End Property





    Public Function getClone() As Entidades.PagoCaja

        Dim objPagoCaja As New Entidades.PagoCaja

        With objPagoCaja

            .Donacion = Me.Donacion
            .Efectivo = Me.Efectivo
            .Factor = Me.Factor
            .FechaACobrar = Me.FechaACobrar
            .IdBanco = Me.IdBanco
            .IdCuentaBancaria = Me.IdCuentaBancaria
            .IdDocumento = Me.IdDocumento
            .IdMedioPago = Me.IdMedioPago
            .IdMoneda = Me.IdMoneda
            .IdMonedaDestino = Me.IdMonedaDestino
            .IdTipoMovimiento = Me.IdTipoMovimiento
            .Monto = Me.Monto
            .MontoEquivalenteDestino = Me.MontoEquivalenteDestino
            .NumeroCheque = Me.NumeroCheque
            .NumeroCuenta = Me.NumeroCuenta
            .NumeroOp = Me.NumeroOp
            .Redondeo = Me.Redondeo
            .Vuelto = Me.Vuelto

            .EfectivoInicial = Me.EfectivoInicial

            .IdPost = Me.IdPost
            .IdTarjeta = Me.IdTarjeta
            .IdDocumentoRef = Me.IdDocumentoRef


        End With

        Return objPagoCaja

    End Function

    Public ReadOnly Property getMonto() As Decimal
        Get

            Dim efectivo1 As Decimal = 0
            Dim vuelto1 As Decimal = 0
            If Efectivo <> Nothing Then
                efectivo1 = Efectivo
            End If
            If Vuelto <> Nothing Then
                vuelto1 = Vuelto
            End If

            If Me.IdMedioPagoInterfaz = 1 Then
                '************* INTERFAZ CONTADO UTILIZA VUELTO
                Return (efectivo1 - vuelto1)
            End If

            '************ OTRA INTERFAZ
            Return (efectivo1)

        End Get
    End Property



    Public Property NomMonedaDestino() As String
        Get
            Return Me._NomMonedaDestino
        End Get
        Set(ByVal value As String)
            Me._NomMonedaDestino = value
        End Set
    End Property



    Public Property IdMonedaDestino() As Integer
        Get
            Return Me._IdMonedaDestino
        End Get
        Set(ByVal value As Integer)
            Me._IdMonedaDestino = value
        End Set
    End Property

    Public Property MontoEquivalenteDestino() As Decimal
        Get
            Return Me._MontoEquivalenteDestino
        End Get
        Set(ByVal value As Decimal)
            Me._MontoEquivalenteDestino = value
        End Set
    End Property




    Public Property NomMoneda() As String
        Get
            Return Me._NomMoneda
        End Get
        Set(ByVal value As String)
            Me._NomMoneda = value
        End Set
    End Property
    Public Property NomBanco() As String
        Get
            Return Me._NomBanco
        End Get
        Set(ByVal value As String)
            Me._NomBanco = value
        End Set
    End Property
    Public Property NumeroCuenta() As String
        Get
            Return Me._NumeroCuenta
        End Get
        Set(ByVal value As String)
            Me._NumeroCuenta = value
        End Set
    End Property


    Public ReadOnly Property getDescripcionxMedioPago()
        Get

            Dim desc As String = ""




            desc = IIf(NomMoneda Is Nothing = True, "", NomMoneda) + " " + CStr(Math.Round(Efectivo, 2)) + " : (" + Me.NomMedioPago + ")"

            Select Case Me.IdMedioPago


                Case 1
                    '*********** Efectivo
                Case 3
                    '************ Transferencia
                    desc += " Banco: " + Me.NomBanco + " N� Cuenta: " + Me.NumeroCuenta + " N� Operaci�n: " + Me.NumeroOp
                Case 7
                    '************* Cheque
                    desc += " Banco: " + Me.NomBanco + " N� Cheque: " + Me.NumeroCheque + " Fecha a Cobrar: " + Format(Me.FechaACobrar, "dd/MM/yyyy")
                Case 8
                    '************* Dep�sito
                    desc += " Banco: " + Me.NomBanco + " N� Cuenta: " + Me.NumeroCuenta + " N� Dep�sito: " + Me.NumeroOp
                Case 9
                    '************** Comp Retenci�n
                    desc += " N� Dep�sito: " + Me.NumeroOp
            End Select


            Return desc
        End Get
    End Property


    Public Property NomMedioPago() As String
        Get
            Return Me._NomMedioPago
        End Get
        Set(ByVal value As String)
            Me._NomMedioPago = value
        End Set
    End Property

    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property
    Public Property IdMedioPago() As Integer
        Get
            Return Me._IdMedioPago
        End Get
        Set(ByVal value As Integer)
            Me._IdMedioPago = value
        End Set
    End Property
    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property
    Public Property IdTipoMovimiento() As Integer
        Get
            Return Me._IdTipoMovimiento
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoMovimiento = value
        End Set
    End Property
    Public Property Monto() As Decimal
        Get
            Return Me._Pc_Monto
        End Get
        Set(ByVal value As Decimal)
            Me._Pc_Monto = value
        End Set
    End Property
    Public Property Factor() As Integer
        Get
            Return Me._Pc_Factor
        End Get
        Set(ByVal value As Integer)
            Me._Pc_Factor = value
        End Set
    End Property
    Public Property Efectivo() As Decimal
        Get
            Return Me._Pc_Efectivo
        End Get
        Set(ByVal value As Decimal)
            Me._Pc_Efectivo = value
        End Set
    End Property
    Public Property Vuelto() As Decimal
        Get
            Return Me._Pc_Vuelto
        End Get
        Set(ByVal value As Decimal)
            Me._Pc_Vuelto = value
        End Set
    End Property
    Public Property Donacion() As Decimal
        Get
            Return Me._Pc_Donacion
        End Get
        Set(ByVal value As Decimal)
            Me._Pc_Donacion = value
        End Set
    End Property
    Public Property Redondeo() As Decimal
        Get
            Return Me._Pc_Redondeo
        End Get
        Set(ByVal value As Decimal)
            Me._Pc_Redondeo = value
        End Set
    End Property
    Public Property NumeroOp() As String
        Get
            Return Me._pc_NumeroOp
        End Get
        Set(ByVal value As String)
            Me._pc_NumeroOp = value
        End Set
    End Property
    Public Property NumeroCheque() As String
        Get
            Return Me._pc_NumeroCheque
        End Get
        Set(ByVal value As String)
            Me._pc_NumeroCheque = value
        End Set
    End Property
    Public Property FechaACobrar() As DateTime
        Get
            Return Me._pc_FechaACobrar
        End Get
        Set(ByVal value As DateTime)
            Me._pc_FechaACobrar = value
        End Set
    End Property
    Public Property IdBanco() As Integer
        Get
            Return Me._IdBanco
        End Get
        Set(ByVal value As Integer)
            Me._IdBanco = value
        End Set
    End Property

    Public Property IdCuentaBancaria() As Integer
        Get
            Return Me._IdCuentaBancaria
        End Get
        Set(ByVal value As Integer)
            Me._IdCuentaBancaria = value
        End Set
    End Property



End Class
