'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class UnidadMedida
    Private _IdUnidadMedida As Integer
    Private _um_CodigoSunat As String
    Private _um_NombreLargo As String
    Private _um_NombreCorto As String
    Private _um_Estado As String
    Private _descEstado As String
    Private _pumEquivalencia As Decimal = 0

    Public Property pumEquivalencia() As Decimal
        Get
            Return _pumEquivalencia
        End Get
        Set(ByVal value As Decimal)
            _pumEquivalencia = value
        End Set
    End Property

    Public Property Id() As Integer
        Get
            Return Me._IdUnidadMedida
        End Get
        Set(ByVal value As Integer)
            Me._IdUnidadMedida = value
        End Set
    End Property

    Public Property CodigoSunat() As String
        Get
            Return Me._um_CodigoSunat
        End Get
        Set(ByVal value As String)
            Me._um_CodigoSunat = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._um_NombreLargo
        End Get
        Set(ByVal value As String)
            Me._um_NombreLargo = value
        End Set
    End Property

    Public Property DescripcionCorto() As String
        Get
            Return Me._um_NombreCorto
        End Get
        Set(ByVal value As String)
            Me._um_NombreCorto = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._um_Estado
        End Get
        Set(ByVal value As String)
            Me._um_Estado = value
        End Set
    End Property
    Public ReadOnly Property DescEstado()
        Get
            Me._descEstado = "Activo"
            If Me._um_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
End Class
