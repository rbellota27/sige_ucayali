﻿Public Class be_RemesasContabilidad

    Private _Marcar As Integer = 0
    Private _IdMovBanco As Integer = 0
    Private _NombreBanco As String = String.Empty
    Private _CuentaBancaria As String = String.Empty
    Private _cuentaContable As String = String.Empty
    Private _CuentaCaja As String = String.Empty
    Private _Glosa As String = String.Empty
    Private _mban_FechaMov As String = String.Empty
    Private _mban_Monto As Decimal = 0
    Private _FechaEmisionReciboEgreso As String = String.Empty
    Private _NroVoucherContable As String = String.Empty
    Private _FechaAsiento As String = String.Empty

    Public Property Marcar() As Integer
        Get
            Return _Marcar
        End Get
        Set(value As Integer)
            _Marcar = value
        End Set
    End Property

    Public Property IdMovBanco() As Integer
        Get
            Return _IdMovBanco
        End Get
        Set(value As Integer)
            _IdMovBanco = value
        End Set
    End Property

    Public Property NombreBanco() As String
        Get
            Return _NombreBanco
        End Get
        Set(value As String)
            _NombreBanco = value
        End Set
    End Property

    Public Property CuentaBancaria() As String
        Get
            Return _CuentaBancaria
        End Get
        Set(value As String)
            _CuentaBancaria = value
        End Set
    End Property

    Public Property cuentaContable() As String
        Get
            Return _cuentaContable
        End Get
        Set(value As String)
            _cuentaContable = value
        End Set
    End Property

    Public Property CuentaCaja() As String
        Get
            Return _CuentaCaja
        End Get
        Set(value As String)
            _CuentaCaja = value
        End Set
    End Property

    Public Property Glosa() As String
        Get
            Return _Glosa
        End Get
        Set(value As String)
            _Glosa = value
        End Set
    End Property

    Public Property mban_FechaMov() As String
        Get
            Return _mban_FechaMov
        End Get
        Set(value As String)
            _mban_FechaMov = value
        End Set
    End Property

    Public Property mban_Monto() As Decimal
        Get
            Return _mban_Monto
        End Get
        Set(value As Decimal)
            _mban_Monto = value
        End Set
    End Property

    Public Property FechaEmisionReciboEgreso() As String
        Get
            Return _FechaEmisionReciboEgreso
        End Get
        Set(value As String)
            _FechaEmisionReciboEgreso = value
        End Set
    End Property

    Public Property NroVoucherContable() As String
        Get
            Return _NroVoucherContable
        End Get
        Set(value As String)
            _NroVoucherContable = value
        End Set
    End Property

    Public Property FechaAsiento() As String
        Get
            Return _FechaAsiento
        End Get
        Set(value As String)
            _FechaAsiento = value
        End Set
    End Property

End Class
