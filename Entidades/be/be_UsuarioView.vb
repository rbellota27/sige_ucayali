﻿Public Class be_UsuarioView
    Private _IdUsuario As Integer = 0
    Private _ar_NombreLargo As String = String.Empty


    Public Property IdUsuario() As Integer
        Get
            Return _IdUsuario
        End Get
        Set(value As Integer)
            _IdUsuario = value
        End Set

    End Property


    Public Property ar_NombreLargo() As String
        Get
            Return _ar_NombreLargo
        End Get
        Set(value As String)
            _ar_NombreLargo = value
        End Set
    End Property
End Class
