﻿Public Class be_reposicionStock
    Private _IdDocumento As Integer = 0
    Public Property idDocumento() As Integer
        Get
            Return _IdDocumento
        End Get
        Set(value As Integer)
            _IdDocumento = value
        End Set
    End Property

    Private _idProducto As Integer = 0
    Public Property idProducto() As Integer
        Get
            Return _idProducto
        End Get
        Set(value As Integer)
            _idProducto = value
        End Set
    End Property

    Private _cantidadProducto As Decimal = 0
    Public Property cantidadProducto() As Decimal
        Get
            Return _cantidadProducto
        End Get
        Set(value As Decimal)
            _cantidadProducto = value
        End Set
    End Property

    Private _idUnidadMedida As Integer = 0
    Public Property idUnidadMedida() As Integer
        Get
            Return _idUnidadMedida
        End Get
        Set(value As Integer)
            _idUnidadMedida = value
        End Set
    End Property

    Private _idUsuario As Integer = 0
    Public Property idUsuario() As Integer
        Get
            Return _idUsuario
        End Get
        Set(value As Integer)
            _idUsuario = value
        End Set
    End Property
End Class
