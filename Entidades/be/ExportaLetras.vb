﻿Public Class ExportaLetras
    Private _EstadoProtesto As String = String.Empty
    Private _FechaEnProtesto As String = String.Empty
    Private _EstadoLetraCambio As String = String.Empty
    Private _FechaCancelacion As String = String.Empty

    Private _doc_FechaEmision As String = String.Empty
    Private _doc_Serie As String = String.Empty
    Private _doc_Codigo As String = String.Empty
    Private _doc_Total As Decimal = 0
    Private _Saldo As Decimal = 0
    Private _MontoCancelado As Decimal = 0


    Private _RUCODNI As String = String.Empty
    Private _RazonSocial As String = String.Empty
    Private _doc_FechaVenc As String = String.Empty
    Private _FacturasRelacionadas As String = String.Empty
    Private _FechaEntregaVendedor As String = String.Empty
    Private _Nombre As String = String.Empty
    Private _FechaRecepcionLetraFirmada As String = String.Empty
    Private _FechaEnvioBanco As String = String.Empty
    Private _NroUnico As String = String.Empty
    Private _CondicionLetra As String = String.Empty
    'Private _Observacion As String = String.Empty
    Private _RecibosIngreso As String = String.Empty
    Private _Banco As String = String.Empty
    Private _IdEstadoLetra As Integer = 0

 
    Public Property EstadoProtesto() As String
        Get
            Return _EstadoProtesto
        End Get
        Set(value As String)
            _EstadoProtesto = value
        End Set
    End Property

    Public Property FechaEnProtesto() As String
        Get
            Return _FechaEnProtesto
        End Get
        Set(value As String)
            _FechaEnProtesto = value
        End Set
    End Property

    Public Property EstadoLetraCambio() As String
        Get
            Return _EstadoLetraCambio
        End Get
        Set(value As String)
            _EstadoLetraCambio = value
        End Set
    End Property
    Public Property FechaCancelacion() As String
        Get
            Return _FechaCancelacion
        End Get
        Set(value As String)
            _FechaCancelacion = value
        End Set
    End Property
    Public Property doc_FechaEmision() As String
        Get
            Return _doc_FechaEmision
        End Get
        Set(value As String)
            _doc_FechaEmision = value
        End Set
    End Property
    'Public Property doc_Serie() As String
    '    Get
    '        Return _doc_Serie
    '    End Get
    '    Set(value As String)
    '        _doc_Serie = value
    '    End Set
    'End Property
    Public Property doc_Codigo() As String
        Get
            Return _doc_Codigo
        End Get
        Set(value As String)
            _doc_Codigo = value
        End Set
    End Property
    Public Property doc_Total() As Decimal
        Get
            Return _doc_Total
        End Get
        Set(value As Decimal)
            _doc_Total = value
        End Set
    End Property

  

    Public Property Saldo() As Decimal
        Get
            Return _Saldo
        End Get
        Set(value As Decimal)
            _Saldo = value
        End Set
    End Property
    Public Property MontoCancelado() As Decimal
        Get
            Return _MontoCancelado
        End Get
        Set(value As Decimal)
            _MontoCancelado = value
        End Set
    End Property

    'Public Property RUCODNI() As String
    '    Get
    '        Return _RUCODNI
    '    End Get
    '    Set(value As String)
    '        _RUCODNI = value
    '    End Set
    'End Property
    Public Property RazonSocial() As String
        Get
            Return _RazonSocial
        End Get
        Set(value As String)
            _RazonSocial = value
        End Set
    End Property
    Public Property doc_FechaVenc() As String
        Get
            Return _doc_FechaVenc
        End Get
        Set(value As String)
            _doc_FechaVenc = value
        End Set
    End Property
    Public Property FacturasRelacionadas() As String
        Get
            Return _FacturasRelacionadas
        End Get
        Set(value As String)
            _FacturasRelacionadas = value
        End Set
    End Property
    Public Property FechaEntregaVendedor() As String
        Get
            Return _FechaEntregaVendedor
        End Get
        Set(value As String)
            _FechaEntregaVendedor = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(value As String)
            _Nombre = value
        End Set
    End Property
    Public Property FechaRecepcionLetraFirmada() As String
        Get
            Return _FechaRecepcionLetraFirmada
        End Get
        Set(value As String)
            _FechaRecepcionLetraFirmada = value
        End Set
    End Property
    Public Property FechaEnvioBanco() As String
        Get
            Return _FechaEnvioBanco
        End Get
        Set(value As String)
            _FechaEnvioBanco = value
        End Set
    End Property
    Public Property NroUnico() As String
        Get
            Return _NroUnico
        End Get
        Set(value As String)
            _NroUnico = value
        End Set
    End Property
    Public Property CondicionLetra() As String
        Get
            Return _CondicionLetra
        End Get
        Set(value As String)
            _CondicionLetra = value
        End Set
    End Property
    'Public Property Observacion() As String
    '    Get
    '        Return _Observacion
    '    End Get
    '    Set(value As String)
    '        _Observacion = value
    '    End Set
    'End Property
    Public Property RecibosIngreso() As String
        Get
            Return _RecibosIngreso
        End Get
        Set(value As String)
            _RecibosIngreso = value
        End Set
    End Property
    Public Property Banco() As String
        Get
            Return _Banco
        End Get
        Set(value As String)
            _Banco = value
        End Set
    End Property
    Public Property IdEstadoLetra() As Integer
        Get
            Return _IdEstadoLetra
        End Get
        Set(value As Integer)
            _IdEstadoLetra = value
        End Set
    End Property
  
 






End Class
