﻿Public Class be_programacionPagos
    Private _listaRequerimientos As List(Of Entidades.be_Requerimiento_x_pagar)
    Private _listaProgramaciones As List(Of Entidades.ProgramacionPago_CXP)
    Private _listaConcepto As List(Of Entidades.Concepto)
    Private _listaProvisiones As List(Of Entidades.Documento)
    Private _listaAplicaciones As List(Of Entidades.be_Requerimiento_x_pagar)
    Private _listaMontoRegimen As List(Of Entidades.be_montoRegimen)
    Private _listaFacturasxAplicar As List(Of Entidades.be_Requerimiento_x_pagar)

    Public Property listaFacturasxAplicar() As List(Of Entidades.be_Requerimiento_x_pagar)
        Get
            Return _listaFacturasxAplicar
        End Get
        Set(ByVal value As List(Of Entidades.be_Requerimiento_x_pagar))
            _listaFacturasxAplicar = value
        End Set
    End Property

    Public Property listaRequerimientos() As List(Of Entidades.be_Requerimiento_x_pagar)
        Get
            Return _listaRequerimientos
        End Get
        Set(ByVal value As List(Of Entidades.be_Requerimiento_x_pagar))
            _listaRequerimientos = value
        End Set
    End Property

    Public Property listaProgramaciones() As List(Of Entidades.ProgramacionPago_CXP)
        Get
            Return _listaProgramaciones
        End Get
        Set(ByVal value As List(Of Entidades.ProgramacionPago_CXP))
            _listaProgramaciones = value
        End Set
    End Property

    Public Property listaConcepto() As List(Of Entidades.Concepto)
        Get
            Return _listaConcepto
        End Get
        Set(ByVal value As List(Of Entidades.Concepto))
            _listaConcepto = value
        End Set
    End Property

    Public Property listaProvisiones() As List(Of Entidades.Documento)
        Get
            Return _listaProvisiones
        End Get
        Set(ByVal value As List(Of Entidades.Documento))
            _listaProvisiones = value
        End Set
    End Property

    Public Property listaAplicaciones() As List(Of Entidades.be_Requerimiento_x_pagar)
        Get
            Return _listaAplicaciones
        End Get
        Set(ByVal value As List(Of Entidades.be_Requerimiento_x_pagar))
            _listaAplicaciones = value
        End Set
    End Property

    Public Property listaMontoRegimen() As List(Of Entidades.be_montoRegimen)
        Get
            Return _listaMontoRegimen
        End Get
        Set(ByVal value As List(Of Entidades.be_montoRegimen))
            _listaMontoRegimen = value
        End Set
    End Property
End Class

Public Class Programacion
    Private _idDocumento As Integer = 0
    Public Property idDocumento() As Integer
        Get
            Return _idDocumento
        End Get
        Set(ByVal value As Integer)
            _idDocumento = value
        End Set
    End Property

    Private _docFechaEmision As String
    Public Property docFechaEmision() As String
        Get
            Return _docFechaEmision
        End Get
        Set(ByVal value As String)
            _docFechaEmision = value
        End Set
    End Property

    Private _codigo As String = String.Empty
    Public Property codigo() As String
        Get
            Return _codigo
        End Get
        Set(ByVal value As String)
            _codigo = value
        End Set
    End Property

    Private _proveedor As String = String.Empty
    Public Property proveedor() As String
        Get
            Return _proveedor
        End Get
        Set(ByVal value As String)
            _proveedor = value
        End Set
    End Property

    Private _totalFactura As Decimal = 0
    Public Property totalFactura() As Decimal
        Get
            Return _totalFactura
        End Get
        Set(ByVal value As Decimal)
            _totalFactura = value
        End Set
    End Property

    Private _nroDiasPlazo As Integer = 0
    Public Property nroDiasPlazo() As Integer
        Get
            Return _nroDiasPlazo
        End Get
        Set(ByVal value As Integer)
            _nroDiasPlazo = value
        End Set
    End Property

    Private _doc_fechaVencimiento As String
    Public Property doc_fechaVEncimiento() As String
        Get
            Return _doc_fechaVencimiento
        End Get
        Set(ByVal value As String)
            _doc_fechaVencimiento = value
        End Set
    End Property

    Private _medioPago As String = String.Empty
    Public Property medioPago() As String
        Get
            Return _medioPago
        End Get
        Set(ByVal value As String)
            _medioPago = value
        End Set
    End Property

    Private _doc_fechaCancelacion As String
    Public Property doc_fechaCancelacion() As String
        Get
            Return _doc_fechaCancelacion
        End Get
        Set(ByVal value As String)
            _doc_fechaCancelacion = value
        End Set
    End Property

    Private _banco As String = String.Empty
    Public Property banco() As String
        Get
            Return _banco
        End Get
        Set(ByVal value As String)
            _banco = value
        End Set
    End Property

    Private _nroOperacion As String = String.Empty
    Public Property nroOperacion() As String
        Get
            Return _nroOperacion
        End Get
        Set(ByVal value As String)
            _nroOperacion = value
        End Set
    End Property

    Private _montoCancelado As Decimal = 0
    Public Property montoCancelado() As Decimal
        Get
            Return _montoCancelado
        End Get
        Set(ByVal value As Decimal)
            _montoCancelado = value
        End Set
    End Property

    Private _nroOperacionDET As String = String.Empty
    Public Property nroOperacionDET() As String
        Get
            Return _nroOperacionDET
        End Get
        Set(ByVal value As String)
            _nroOperacionDET = value
        End Set
    End Property

    Private _montoCanceladoDET As Decimal = 0
    Public Property montoCanceladoDET() As Decimal
        Get
            Return _montoCanceladoDET
        End Get
        Set(ByVal value As Decimal)
            _montoCanceladoDET = value
        End Set
    End Property

    Private _retencion As String = String.Empty
    Public Property retencion() As String
        Get
            Return _retencion
        End Get
        Set(ByVal value As String)
            _retencion = value
        End Set
    End Property

    Private _nroRequerimiento As String = String.Empty
    Public Property nroRequerimiento() As String
        Get
            Return _nroRequerimiento
        End Get
        Set(ByVal value As String)
            _nroRequerimiento = value
        End Set
    End Property

    Private _estado As String = String.Empty
    Public Property estado() As String
        Get
            Return _estado
        End Get
        Set(ByVal value As String)
            _estado = value
        End Set
    End Property
End Class
