﻿Public Class be_frmCargaPrincipal

    Private _listaProveedores As List(Of be_ComboGenerico)
    Public Property listaProveedores() As List(Of be_ComboGenerico)
        Get
            Return _listaProveedores
        End Get
        Set(value As List(Of be_ComboGenerico))
            _listaProveedores = value
        End Set
    End Property

    Private _listaCancelacionesProgramadas As List(Of be_cancelacionDocumentos)
    Public Property listaCancelacionesProgramadas() As List(Of be_cancelacionDocumentos)
        Get
            Return _listaCancelacionesProgramadas
        End Get
        Set(value As List(Of be_cancelacionDocumentos))
            _listaCancelacionesProgramadas = value
        End Set
    End Property

    Private _listaArea As List(Of be_ComboGenerico)
    Public Property listaArea() As List(Of be_ComboGenerico)
        Get
            Return _listaArea
        End Get
        Set(value As List(Of be_ComboGenerico))
            _listaArea = value
        End Set
    End Property

    Private _listaBancos As List(Of be_ComboGenerico)
    Public Property listaBancos() As List(Of be_ComboGenerico)
        Get
            Return _listaBancos
        End Get
        Set(value As List(Of be_ComboGenerico))
            _listaBancos = value
        End Set
    End Property

    Private _idTiendaPrincipal As Integer = 0
    Public Property idTiendaPrincipal() As Integer
        Get
            Return _idTiendaPrincipal
        End Get
        Set(value As Integer)
            _idTiendaPrincipal = value
        End Set
    End Property

    Private _listaEmpresa As List(Of be_ComboGenerico)
    Public Property listaEmpresa() As List(Of be_ComboGenerico)
        Get
            Return _listaEmpresa
        End Get
        Set(value As List(Of be_ComboGenerico))
            _listaEmpresa = value
        End Set
    End Property

    Private _listaTienda As List(Of be_ComboGenerico)
    Public Property listaTienda() As List(Of be_ComboGenerico)
        Get
            Return _listaTienda
        End Get
        Set(value As List(Of be_ComboGenerico))
            _listaTienda = value
        End Set
    End Property

    Private _listaAlmacen As List(Of be_ComboGenerico)
    Public Property listaAlmacen() As List(Of be_ComboGenerico)
        Get
            Return _listaAlmacen
        End Get
        Set(value As List(Of be_ComboGenerico))
            _listaAlmacen = value
        End Set
    End Property

End Class
