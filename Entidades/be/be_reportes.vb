﻿Public Class be_reportes
    Private _idtblReporte As Integer = 0
    Public Property idtblReporte() As Integer
        Get
            Return _idtblReporte
        End Get
        Set(ByVal value As Integer)
            _idtblReporte = value
        End Set
    End Property

    Private _nombreReporte As String = String.Empty
    Public Property nombreReporte() As String
        Get
            Return _nombreReporte
        End Get
        Set(ByVal value As String)
            _nombreReporte = value
        End Set
    End Property

    Private _descripcionReporte As String = String.Empty
    Public Property descripcionReporte() As String
        Get
            Return _descripcionReporte
        End Get
        Set(ByVal value As String)
            _descripcionReporte = value
        End Set
    End Property

    Private _version As String = String.Empty
    Public Property version() As String
        Get
            Return _version
        End Get
        Set(ByVal value As String)
            _version = value
        End Set
    End Property

    Private _area As String = String.Empty
    Public Property area() As String
        Get
            Return _area
        End Get
        Set(ByVal value As String)
            _area = value
        End Set
    End Property
End Class
