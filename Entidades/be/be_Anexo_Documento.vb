﻿Public Class be_Anexo_Documento

    Private _IdDocumento As Integer = 0
    Private _Constancia As String = String.Empty


    Public Property IdDocumento() As Integer
        Get
            Return _IdDocumento
        End Get
        Set(value As Integer)
            _IdDocumento = value
        End Set
    End Property


    Public Property Constancia() As String
        Get
            Return _Constancia
        End Get
        Set(value As String)
            _Constancia = value
        End Set
    End Property


End Class
