﻿Public Class be_TipoOperacion
    Private _IdTipoOperacion As Integer = 0
    Private _top_Nombre As String = String.Empty


    Public Property IdTipoOperacion() As Integer
        Get
            Return _IdTipoOperacion
        End Get
        Set(value As Integer)
            _IdTipoOperacion = value
        End Set
    End Property

    Public Property top_Nombre() As String
        Get
            Return _top_Nombre
        End Get
        Set(value As String)
            _top_Nombre = value
        End Set
    End Property

End Class
