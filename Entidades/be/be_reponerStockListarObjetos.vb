﻿Public Class be_reponerStockListarObjetos
    Private _idTiendaPrincipal As Integer = 0
    Public Property idTiendaPrincipal() As Integer
        Get
            Return _idTiendaPrincipal
        End Get
        Set(value As Integer)
            _idTiendaPrincipal = value
        End Set
    End Property

    Private _listaEmpresa As List(Of be_ComboGenerico)
    Public Property listaEmpresa() As List(Of be_ComboGenerico)
        Get
            Return _listaEmpresa
        End Get
        Set(value As List(Of be_ComboGenerico))
            _listaEmpresa = value
        End Set
    End Property

    Private _listaTienda As List(Of be_ComboGenerico)
    Public Property listaTienda() As List(Of be_ComboGenerico)
        Get
            Return _listaTienda
        End Get
        Set(value As List(Of be_ComboGenerico))
            _listaTienda = value
        End Set
    End Property

    Private _listaAlmacen As List(Of be_ComboGenerico)
    Public Property listaAlmacen() As List(Of be_ComboGenerico)
        Get
            Return _listaAlmacen
        End Get
        Set(value As List(Of be_ComboGenerico))
            _listaAlmacen = value
        End Set
    End Property
End Class

Public Class be_ComboGenerico
    'Inherits Concepto
    Private _campo1 As Integer = 0
    Public Property campo1() As Integer
        Get
            Return _campo1
        End Get
        Set(ByVal value As Integer)
            _campo1 = value
        End Set
    End Property

    Private _campo2 As String = String.Empty
    Public Property campo2() As String
        Get
            Return _campo2
        End Get
        Set(ByVal value As String)
            _campo2 = value
        End Set
    End Property

End Class

Public Class be_montoRegimen
    Private _descripcion As String = String.Empty
    Private _montoRegimen As Decimal = 0
    Private _codigo As String = String.Empty
    Private _idDocumento As Integer = 0
    Private _tipoRegimen As Integer

    Public Property descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

    Public Property codigo() As String
        Get
            Return _codigo
        End Get
        Set(ByVal value As String)
            _codigo = value
        End Set
    End Property

    Public Property montoRegimen() As Decimal
        Get
            Return _montoRegimen
        End Get
        Set(ByVal value As Decimal)
            _montoRegimen = value
        End Set
    End Property

    Public Property idDocumento() As Integer
        Get
            Return _idDocumento
        End Get
        Set(ByVal value As Integer)
            _idDocumento = value
        End Set
    End Property

    Public Property tipoRegimen() As Integer
        Get
            Return _tipoRegimen
        End Get
        Set(ByVal value As Integer)
            _tipoRegimen = value
        End Set
    End Property

    Private _canNroOperacion As String = String.Empty
    Private _montoCancelado As Decimal = 0
    Private _fechaCancelacion As String = String.Empty
    Private _bancoCancelacion As String = String.Empty
    Private _DocumentoRetencion As String = String.Empty

    Public Property canNroOperacion() As String
        Get
            Return _canNroOperacion
        End Get
        Set(ByVal value As String)
            _canNroOperacion = value
        End Set
    End Property

    Public Property montoCancelado() As Decimal
        Get
            Return _montoCancelado
        End Get
        Set(ByVal value As Decimal)
            _montoCancelado = value
        End Set
    End Property

    Public Property fechaCancelacion() As String
        Get
            Return _fechaCancelacion
        End Get
        Set(ByVal value As String)
            _fechaCancelacion = value
        End Set
    End Property

    Public Property bancoCancelacion() As String
        Get
            Return _bancoCancelacion
        End Get
        Set(ByVal value As String)
            _bancoCancelacion = value
        End Set
    End Property

    Public Property DocumentoRetencion() As String
        Get
            Return _DocumentoRetencion
        End Get
        Set(ByVal value As String)
            _DocumentoRetencion = value
        End Set
    End Property

End Class
