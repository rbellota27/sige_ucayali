﻿Public Class be_detraccion
    Private _idDetraccion As Integer = 0
    Private _regNombre As String = String.Empty
    Private _regNombreAvreviado As String = String.Empty

    Public Property idDetraccion() As Integer
        Get
            Return _idDetraccion
        End Get
        Set(value As Integer)
            _idDetraccion = value
        End Set
    End Property

    Public Property regNombre() As String
        Get
            Return _regNombre
        End Get
        Set(value As String)
            _regNombre = value
        End Set
    End Property

    Public Property regNombreAvreviado() As String
        Get
            Return _regNombreAvreviado
        End Get
        Set(value As String)
            _regNombreAvreviado = value
        End Set
    End Property
End Class
