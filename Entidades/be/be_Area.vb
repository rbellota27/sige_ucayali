﻿Public Class be_Area
    Private _IdArea As Integer = 0
    Private _ar_NombreLargo As String = String.Empty


    Public Property IdArea() As Integer
        Get
            Return _IdArea
        End Get
        Set(value As Integer)
            _IdArea = value
        End Set

    End Property


    Public Property ar_NombreLargo() As String
        Get
            Return _ar_NombreLargo
        End Get
        Set(value As String)
            _ar_NombreLargo = value
        End Set
    End Property
End Class
