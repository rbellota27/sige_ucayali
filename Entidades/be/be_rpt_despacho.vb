﻿Imports System.Data.SqlDbType
Public Class be_rpt_despacho
    Private _turno As String = String.Empty
    Private _fecInicioDespacho As Date = Date.Today
    Private _vehPlaca As String = String.Empty
    Private _nroVuelta As Integer = 0
    Private _capacidadTotal As Decimal = 0
    Private _pesoUsado As Decimal = 0
    Private _nroDocumento As Integer = 0
    Private _conductor As String = String.Empty
    Private _codigo As String = String.Empty
    Private _pesoDocumento As Decimal = 0
    Private _clienteFinal As String = String.Empty
    Private _distrito As String = String.Empty
    Private _tienda As String = String.Empty

    Public Property turno() As String
        Get
            Return _turno
        End Get
        Set(value As String)
            _turno = value
        End Set
    End Property

    Public Property fecInicioDespacho() As Date
        Get
            Return _fecInicioDespacho
        End Get
        Set(value As Date)
            _fecInicioDespacho = value
        End Set
    End Property

    Public Property vehPlaca() As String
        Get
            Return _vehPlaca
        End Get
        Set(value As String)
            _vehPlaca = value
        End Set
    End Property

    Public Property nroVuelta() As Integer
        Get
            Return _nroVuelta
        End Get
        Set(value As Integer)
            _nroVuelta = value
        End Set
    End Property

    Public Property capacidadTotal() As Decimal
        Get
            Return _capacidadTotal
        End Get
        Set(value As Decimal)
            _capacidadTotal = value
        End Set
    End Property

    Public Property pesoUsado() As Decimal
        Get
            Return _pesoUsado
        End Get
        Set(value As Decimal)
            _pesoUsado = value
        End Set
    End Property

    Public Property nroDocumento() As Integer
        Get
            Return _nroDocumento
        End Get
        Set(value As Integer)
            _nroDocumento = value
        End Set
    End Property

    Public Property conductor() As String
        Get
            Return _conductor
        End Get
        Set(value As String)
            _conductor = value
        End Set
    End Property

    Public Property codigo() As String
        Get
            Return _codigo
        End Get
        Set(value As String)
            _codigo = value
        End Set
    End Property

    Public Property pesoDocumento() As Decimal
        Get
            Return _pesoDocumento
        End Get
        Set(value As Decimal)
            _pesoDocumento = value
        End Set
    End Property

    Public Property clienteFinal() As String
        Get
            Return _clienteFinal
        End Get
        Set(value As String)
            _clienteFinal = value
        End Set
    End Property

    Public Property distrito() As String
        Get
            Return _distrito
        End Get
        Set(value As String)
            _distrito = value
        End Set
    End Property

    Public Property tienda() As String
        Get
            Return _tienda
        End Get
        Set(value As String)
            _tienda = value
        End Set
    End Property
End Class
