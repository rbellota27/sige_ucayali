﻿Public Class be_facturacionElectronica

    Private _id As Integer = 0
    Public Property id As Integer
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            _id = value
        End Set
    End Property

    Private _codigo As String = String.Empty
    Public Property codigo As String
        Get
            Return _codigo
        End Get
        Set(ByVal value As String)
            _codigo = value
        End Set
    End Property

    Private _conceptoDigiflow As String = String.Empty
    Public Property conceptoDigiflow As String
        Get
            Return _conceptoDigiflow
        End Get
        Set(ByVal value As String)
            _conceptoDigiflow = value
        End Set
    End Property

    Private _campo As String = String.Empty
    Public Property campo As String
        Get
            Return _campo
        End Get
        Set(ByVal value As String)
            _campo = value
        End Set
    End Property

End Class
