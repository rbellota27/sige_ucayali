﻿
Public Class be_validacion
    Inherits TipoOperacion
    Private _idpermiso As Integer = 0
    Private _idperfil As Integer = 0
    Private _permValor As Integer = 0

    Public Property idPermiso() As Integer
        Get
            Return _idpermiso
        End Get
        Set(ByVal value As Integer)
            _idpermiso = value
        End Set
    End Property

    Public Property idPerfil() As Integer
        Get
            Return _idperfil
        End Get
        Set(ByVal value As Integer)
            _idperfil = value
        End Set
    End Property
End Class
