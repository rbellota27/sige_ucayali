﻿Public Class be_cancelacionDocumentos
    Inherits Documento_MovCuentaPorPagar

    Private _listaCuentasxPagar As List(Of Entidades.Documento_MovCuentaPorPagar) = Nothing

    Public Property listaCuentasxPagar() As List(Of Entidades.Documento_MovCuentaPorPagar)
        Get
            Return _listaCuentasxPagar
        End Get
        Set(ByVal value As List(Of Entidades.Documento_MovCuentaPorPagar))
            _listaCuentasxPagar = value
        End Set
    End Property

    Private _listaFacturasAplicadas As List(Of Entidades.be_Requerimiento_x_pagar) = Nothing

    Public Property listaFacturasAplicadas() As List(Of Entidades.be_Requerimiento_x_pagar)
        Get
            Return _listaFacturasAplicadas
        End Get
        Set(ByVal value As List(Of Entidades.be_Requerimiento_x_pagar))
            _listaFacturasAplicadas = value
        End Set
    End Property

    Private _idProveedor As Integer = 0
    Private _idtipoOperacion As Integer = 0
    Private _idRequerimiento As Integer = 0
    Private _codigo_req As String = String.Empty
    Private _idMovCtaPP As Integer = 0
    Private _IdProgramacionPago As Integer = 0
    Private _pp_FechaPagoProg As Date
    Private _pp_MontoPagoProg As Decimal = 0
    Private _proveedor As String = String.Empty
    Private _IdBanco As Integer = 0
    Private _IdMedioPago As Integer = 0
    Private _IdCuentaBancaria As Integer = 0
    Private _pp_Observacion As String = String.Empty
    Private _mp_Nombre As String = String.Empty
    Private _ban_Nombre As String = String.Empty
    Private _cb_numero As String = String.Empty
    Private _IdMoneda As Integer = 0
    Private _mon_Simbolo As String = String.Empty
    Private _tipocambio As Decimal = 0
    Private _nomMoneda As String = String.Empty
    Private _NroDocumentoRef As String = String.Empty
    Private _IdDocumentoRef As Integer = 0
    Private _pp_esDetraccion As Boolean = 0
    Private _pp_esRetencion As Boolean = 0
    Private _pla_cCuentaContable As String = String.Empty
    Private _monedaNacional As Decimal = 0
    Private _monedaExtranjera As Decimal = 0
    Private _montoProgramado As Decimal = 0
    Private _cadenaDocumentoReferencia As String = String.Empty
    Private _monedasimboloN As String = String.Empty
    Private _monedasimboloE As String = String.Empty
    Private _montoRetenido As Decimal = 0
    Private _flagCancelacion As Boolean = False
    Private _idRequerimientosCadena As String = String.Empty
    Private _nroRequerimientoCadena As String = String.Empty
    Private _monedaOriginal As String = String.Empty    

    Public Property idRequerimientosCadena() As String
        Get
            Return _idRequerimientosCadena
        End Get
        Set(ByVal value As String)
            _idRequerimientosCadena = value
        End Set
    End Property

    Public Property nroRequerimientoCadena() As String
        Get
            Return _nroRequerimientoCadena
        End Get
        Set(ByVal value As String)
            _nroRequerimientoCadena = value
        End Set
    End Property

    Public Property flagCancelacion() As Boolean
        Get
            Return _flagCancelacion
        End Get
        Set(ByVal value As Boolean)
            _flagCancelacion = value
        End Set
    End Property

    Public Property montoRetenido() As Decimal
        Get
            Return _montoRetenido
        End Get
        Set(value As Decimal)
            _montoRetenido = value
        End Set
    End Property

    Public Property monedasimboloE() As String
        Get
            Return _monedasimboloE
        End Get
        Set(value As String)
            _monedasimboloE = value
        End Set
    End Property

    Public Property monedasimboloN() As String
        Get
            Return _monedasimboloN
        End Get
        Set(value As String)
            _monedasimboloN = value
        End Set
    End Property

    Public Property cadenaDocumentoReferencia() As String
        Get
            Return _cadenaDocumentoReferencia
        End Get
        Set(value As String)
            _cadenaDocumentoReferencia = value
        End Set
    End Property

    Public Property montoProgramado() As Decimal
        Get
            Return _montoProgramado
        End Get
        Set(value As Decimal)
            _montoProgramado = value
        End Set
    End Property

    Public Property monedaNacional() As Decimal
        Get
            Return _monedaNacional
        End Get
        Set(value As Decimal)
            _monedaNacional = value
        End Set
    End Property

    Public Property monedaExtranjera() As Decimal
        Get
            Return _monedaExtranjera
        End Get
        Set(value As Decimal)
            _monedaExtranjera = value
        End Set
    End Property

    Public Property idProveedor() As Integer
        Get
            Return _idProveedor
        End Get
        Set(value As Integer)
            _idProveedor = value
        End Set
    End Property

    Public Property idtipoOperacion() As Integer
        Get
            Return _idtipoOperacion
        End Get
        Set(value As Integer)
            _idtipoOperacion = value
        End Set
    End Property

    Public Property idRequerimiento() As Integer
        Get
            Return _idRequerimiento
        End Get
        Set(value As Integer)
            _idRequerimiento = value
        End Set
    End Property

    Public Property codigo_req() As String
        Get
            Return _codigo_req
        End Get
        Set(value As String)
            _codigo_req = value
        End Set
    End Property

    Public Property idMovCtaPP() As Integer
        Get
            Return _idMovCtaPP
        End Get
        Set(value As Integer)
            _idMovCtaPP = value
        End Set
    End Property

    Public Property IdProgramacionPago() As Integer
        Get
            Return _IdProgramacionPago
        End Get
        Set(value As Integer)
            _IdProgramacionPago = value
        End Set
    End Property

    Public Property pp_FechaPagoProg() As Date
        Get
            Return _pp_FechaPagoProg
        End Get
        Set(value As Date)
            _pp_FechaPagoProg = value
        End Set
    End Property

    Public Property pp_MontoPagoProg() As Decimal
        Get
            Return _pp_MontoPagoProg
        End Get
        Set(value As Decimal)
            _pp_MontoPagoProg = value
        End Set
    End Property

    Public Property proveedor() As String
        Get
            Return _proveedor
        End Get
        Set(value As String)
            _proveedor = value
        End Set
    End Property

    Public Property IdBanco() As Integer
        Get
            Return _IdBanco
        End Get
        Set(value As Integer)
            _IdBanco = value
        End Set
    End Property

    Public Property IdMedioPago() As Integer
        Get
            Return _IdMedioPago
        End Get
        Set(value As Integer)
            _IdMedioPago = value
        End Set
    End Property

    Public Property IdCuentaBancaria() As Integer
        Get
            Return _IdCuentaBancaria
        End Get
        Set(value As Integer)
            _IdCuentaBancaria = value
        End Set
    End Property

    Public Property pp_Observacion() As String
        Get
            Return _pp_Observacion
        End Get
        Set(value As String)
            _pp_Observacion = value
        End Set
    End Property

    Public Property mp_Nombre() As String
        Get
            Return _mp_Nombre
        End Get
        Set(value As String)
            _mp_Nombre = value
        End Set
    End Property

    Public Property ban_Nombre() As String
        Get
            Return _ban_Nombre
        End Get
        Set(value As String)
            _ban_Nombre = value
        End Set
    End Property

    Public Property cb_numero() As String
        Get
            Return _cb_numero
        End Get
        Set(value As String)
            _cb_numero = value
        End Set
    End Property

    Public Property IdMoneda() As Integer
        Get
            Return _IdMoneda
        End Get
        Set(value As Integer)
            _IdMoneda = value
        End Set
    End Property

    Public Property mon_Simbolo() As String
        Get
            Return _mon_Simbolo
        End Get
        Set(value As String)
            _mon_Simbolo = value
        End Set
    End Property

    Public Property tipocambio() As Decimal
        Get
            Return _tipocambio
        End Get
        Set(value As Decimal)
            _tipocambio = value
        End Set
    End Property

    Public Property nomMoneda() As String
        Get
            Return _nomMoneda
        End Get
        Set(value As String)
            _nomMoneda = value
        End Set
    End Property

    Public Property NroDocumentoRef() As String
        Get
            Return _NroDocumentoRef
        End Get
        Set(value As String)
            _NroDocumentoRef = value
        End Set
    End Property

    Public Property IdDocumentoRef() As Integer
        Get
            Return _IdDocumentoRef
        End Get
        Set(value As Integer)
            _IdDocumentoRef = value
        End Set
    End Property

    Public Property pp_esDetraccion() As Boolean
        Get
            Return _pp_esDetraccion
        End Get
        Set(value As Boolean)
            _pp_esDetraccion = value
        End Set
    End Property

    Public Property pp_esRetencion() As Boolean
        Get
            Return _pp_esRetencion
        End Get
        Set(value As Boolean)
            _pp_esRetencion = value
        End Set
    End Property

    Public Property pla_cCuentaContable() As String
        Get
            Return _pla_cCuentaContable
        End Get
        Set(value As String)
            _pla_cCuentaContable = value
        End Set
    End Property

    Public Property monedaOriginal() As String
        Get
            Return _monedaOriginal
        End Get
        Set(ByVal value As String)
            _monedaOriginal = value
        End Set
    End Property
End Class
