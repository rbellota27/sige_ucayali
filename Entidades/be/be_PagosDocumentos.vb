﻿

Public Class be_PagosDocumentos

    'Private _est_reque As Integer = 0
    Private _Aprobado As Integer = 0
    Private _FechaAprobacion As Date = Date.Today
    Private _Area As String = String.Empty
    Private _Moneda As String = String.Empty
    Private _NumeroProgramacion As String = String.Empty
    Private _DescripcionPersona As String = String.Empty
    Private _FechaEmision As Date = Date.Today
    Private _FechaPago As Date = Date.Today
    Private _NroDocumento As String = String.Empty
    Private _TipoDocumento As String = String.Empty
    Private _TotalAPagar As Decimal = 0
    Private _MontoDetraccion As Decimal = 0
    Private _MontoRetencion As Decimal = 0
    Private _Pago As Decimal = 0
    Private _NombreBanco As String = String.Empty
    Private _idProgramacion As Integer = 0
    Private _IdDocumento As Integer = 0
    Private _Id As Integer = 0
    Private _IdArea As Integer = 0
    Private _IdPersona As Integer = 0
    Private _Alerta As Integer = 0
    Private _Deuda As Decimal = 0



    Public Property Aprobado() As Integer
        Get
            Return _Aprobado
        End Get
        Set(ByVal value As Integer)
            _Aprobado = value
        End Set
    End Property


    Public Property FechaAprobacion() As String
        Get
            Return _FechaAprobacion
        End Get
        Set(value As String)
            _FechaAprobacion = value
        End Set
    End Property


    Public Property Area() As String
        Get
            Return _Area
        End Get
        Set(value As String)
            _Area = value
        End Set
    End Property



    Public Property Moneda() As String
        Get
            Return _Moneda
        End Get
        Set(value As String)
            _Moneda = value
        End Set
    End Property


    Public Property NumeroProgramacion() As String
        Get
            Return _NumeroProgramacion
        End Get
        Set(value As String)
            _NumeroProgramacion = value
        End Set
    End Property

    Public Property DescripcionPersona() As String
        Get
            Return _DescripcionPersona
        End Get
        Set(value As String)
            _DescripcionPersona = value
        End Set
    End Property

    Public Property FechaEmision() As String
        Get
            Return _FechaEmision
        End Get
        Set(value As String)
            _FechaEmision = value
        End Set
    End Property

    Public Property FechaPago() As String
        Get
            Return _FechaPago
        End Get
        Set(value As String)
            _FechaPago = value
        End Set
    End Property

    Public Property NroDocumento() As String
        Get
            Return _NroDocumento
        End Get
        Set(value As String)
            _NroDocumento = value
        End Set
    End Property

    Public Property TipoDocumento() As String
        Get
            Return _TipoDocumento
        End Get
        Set(value As String)
            _TipoDocumento = value
        End Set
    End Property
    Public Property TotalAPagar() As Decimal
        Get
            Return _TotalAPagar
        End Get
        Set(ByVal value As Decimal)
            _TotalAPagar = value
        End Set
    End Property

    Public Property MontoDetraccion() As Decimal
        Get
            Return _MontoDetraccion
        End Get
        Set(ByVal value As Decimal)
            _MontoDetraccion = value
        End Set
    End Property
    Public Property MontoRetencion() As Decimal
        Get
            Return _MontoRetencion
        End Get
        Set(ByVal value As Decimal)
            _MontoRetencion = value
        End Set
    End Property

    Public Property Pago() As Decimal
        Get
            Return _Pago
        End Get
        Set(ByVal value As Decimal)
            _Pago = value
        End Set
    End Property

    Public Property NombreBanco() As String
        Get
            Return _NombreBanco
        End Get
        Set(value As String)
            _NombreBanco = value
        End Set
    End Property

    Public Property idProgramacion() As Integer
        Get
            Return _idProgramacion
        End Get
        Set(value As Integer)
            _idProgramacion = value
        End Set
    End Property

    Public Property IdDocumento() As Integer
        Get
            Return _IdDocumento
        End Get
        Set(value As Integer)
            _IdDocumento = value
        End Set
    End Property


    Public Property Id() As Integer
        Get
            Return _Id
        End Get
        Set(value As Integer)
            _Id = value
        End Set
    End Property


    Public Property IdArea() As Integer
        Get
            Return _IdArea
        End Get
        Set(value As Integer)
            _IdArea = value
        End Set
    End Property


    Public Property IdPersona() As Integer
        Get
            Return _IdPersona
        End Get
        Set(value As Integer)
            _IdPersona = value
        End Set
    End Property

    Public Property Alerta() As Integer
        Get
            Return _Alerta
        End Get
        Set(value As Integer)
            _Alerta = value
        End Set
    End Property


    Public Property Deuda() As Decimal
        Get
            Return _Deuda
        End Get
        Set(ByVal value As Decimal)
            _Deuda = value
        End Set
    End Property

End Class
