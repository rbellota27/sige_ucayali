﻿Public Class be_documentoAplicacionDeudasPendientes

    Private _idProvision As Integer = 0
    Public Property idProvision() As Integer
        Get
            Return _idProvision
        End Get
        Set(ByVal value As Integer)
            _idProvision = value
        End Set
    End Property

    Private _idFacturaAplicacion As Integer = 0
    Public Property idFacturaAplicacion() As Integer
        Get
            Return _idFacturaAplicacion
        End Get
        Set(ByVal value As Integer)
            _idFacturaAplicacion = value
        End Set
    End Property

    Private _montoAplicadoContraDeudaPendiente As Decimal = 0
    Public Property montoAplicadoContraDeudaPendiente() As Decimal
        Get
            Return _montoAplicadoContraDeudaPendiente
        End Get
        Set(ByVal value As Decimal)
            _montoAplicadoContraDeudaPendiente = value
        End Set
    End Property
End Class
