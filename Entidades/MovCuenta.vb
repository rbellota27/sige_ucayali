'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class MovCuenta
    Private _IdMovCuenta As Integer
    Private _IdCuentaPersona As Integer
    Private _IdPersona As Integer
    Private _mcu_Fecha As Date
    Private _mcu_Monto As Decimal
    Private _mcu_Factor As Integer
    Private _IdDocumento As Integer
    Private _IdDetalleRecibo As Integer
    Private _IdMovCuentaTipo As Integer
    Private _mcu_Saldo As Decimal
    Private _IdCargoCuenta As Integer


    Private _Codigo As String
    Private _Serie As String
    Private _nomTipoDocumento As String
    Private _IdMoneda As Integer
    Private _monedaSimbolo As String
    Public Property IdMovCuenta() As Integer
        Get
            Return Me._IdMovCuenta
        End Get
        Set(ByVal value As Integer)
            Me._IdMovCuenta = value
        End Set
    End Property

    Public Property IdCuentaPersona() As Integer
        Get
            Return Me._IdCuentaPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdCuentaPersona = value
        End Set
    End Property
    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property
    Public Property Fecha() As Date
        Get
            Return Me._mcu_Fecha
        End Get
        Set(ByVal value As Date)
            Me._mcu_Fecha = value
        End Set
    End Property

    Public Property Monto() As Decimal
        Get
            Return Me._mcu_Monto
        End Get
        Set(ByVal value As Decimal)
            Me._mcu_Monto = value
        End Set
    End Property

    Public Property Factor() As Integer
        Get
            Return Me._mcu_Factor
        End Get
        Set(ByVal value As Integer)
            Me._mcu_Factor = value
        End Set
    End Property

    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property
    Public Property IdDetalleRecibo() As Integer
        Get
            Return Me._IdDetalleRecibo
        End Get
        Set(ByVal value As Integer)
            Me._IdDetalleRecibo = value
        End Set
    End Property
    Public Property IdMovCuentaTipo() As Integer
        Get
            Return Me._IdMovCuentaTipo
        End Get
        Set(ByVal value As Integer)
            Me._IdMovCuentaTipo = value
        End Set
    End Property
    Public Property Saldo() As Decimal
        Get
            Return Me._mcu_Saldo
        End Get
        Set(ByVal value As Decimal)
            Me._mcu_Saldo = value
        End Set
    End Property
    Public Property IdCargoCuenta() As Integer
        Get
            Return Me._IdCargoCuenta
        End Get
        Set(ByVal value As Integer)
            Me._IdCargoCuenta = value
        End Set
    End Property


    '************************
    Public Property Codigo() As String
        Get
            Return Me._Codigo
        End Get
        Set(ByVal value As String)
            Me._Codigo = value
        End Set
    End Property

    Public Property Serie() As String
        Get
            Return Me._Serie
        End Get
        Set(ByVal value As String)
            Me._Serie = value
        End Set
    End Property

    Public Property NomTipoDocumento() As String
        Get
            Return Me._nomTipoDocumento
        End Get
        Set(ByVal value As String)
            Me._nomTipoDocumento = value
        End Set
    End Property

    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property

    Public Property MonedaSimbolo() As String
        Get
            Return Me._monedaSimbolo
        End Get
        Set(ByVal value As String)
            Me._monedaSimbolo = value
        End Set
    End Property

End Class
