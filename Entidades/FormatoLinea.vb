﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class FormatoLinea
    Inherits Material
    Private _IdFormato As Integer
    Private _NomFormato As String
    Private _IdLinea As Integer
    Private _IdTipoExistencia As Integer
    Private _flEstado As Boolean
    Private _flOrden As Integer
    Private _IdFormatoOrden As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal idformato As Integer, ByVal nomformato As String, ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal estado As Boolean, ByVal orden As Integer)
        _IdFormato = idformato
        _NomFormato = nomformato
        _IdLinea = idlinea
        _IdTipoExistencia = idtipoexistencia
        _flEstado = estado
        _flOrden = orden
    End Sub
    Public Sub New(ByVal idformato As Integer, ByVal nomformato As String, ByVal estado As Boolean)
        _IdFormato = idformato
        _NomFormato = nomformato
        _flEstado = estado
    End Sub

    Public Property IdFormato() As Integer
        Get
            Return _IdFormato
        End Get
        Set(ByVal value As Integer)
            _IdFormato = value
        End Set
    End Property
    Public Property NomFormato() As String
        Get
            Return _NomFormato
        End Get
        Set(ByVal value As String)
            _NomFormato = value
        End Set
    End Property
    Public Property IdLinea() As Integer
        Get
            Return _IdLinea
        End Get
        Set(ByVal value As Integer)
            _IdLinea = value
        End Set
    End Property
    Public Property IdTipoExistencia() As Integer
        Get
            Return _IdTipoExistencia
        End Get
        Set(ByVal value As Integer)
            _IdTipoExistencia = value
        End Set
    End Property
    Public Property Estado() As Boolean
        Get
            Return _flEstado
        End Get
        Set(ByVal value As Boolean)
            _flEstado = value
        End Set
    End Property
    Public Property Orden() As Integer
        Get
            Return _flOrden
        End Get
        Set(ByVal value As Integer)
            _flOrden = value
        End Set
    End Property
    Public Property IdFormatoOrden() As String
        Get
            Return _IdFormatoOrden
        End Get
        Set(ByVal value As String)
            _IdFormatoOrden = value
        End Set
    End Property
End Class
