'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class TipoAgente
    Private _IdAgente As Integer
    Private _ag_Nombre As String
    Private _ag_Estado As String
    Private _ag_tasa As Decimal
    Private _descEstado As String
    Private _ag_SujetoAPercepcion As Boolean
    Private _ag_SujetoARetencion As Boolean
    Private _ag_SujetoADetraccion As Boolean
    Private _tipoAgente As Integer = 0

    Public ReadOnly Property DescEstado() As String
        Get
            Me._descEstado = "Activo"
            If Me._ag_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public Property IdAgente() As Integer
        Get
            Return Me._IdAgente
        End Get
        Set(ByVal value As Integer)
            Me._IdAgente = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._ag_Nombre
        End Get
        Set(ByVal value As String)
            Me._ag_Nombre = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._ag_Estado
        End Get
        Set(ByVal value As String)
            Me._ag_Estado = value
        End Set
    End Property
    Public Property EstadoBoolean() As Boolean
        Get
            Return IIf(Me._ag_Estado = "1", True, False)
        End Get
        Set(ByVal value As Boolean)
            Me._ag_Estado = IIf(value, "1", "0")
        End Set
    End Property
    Public Property Tasa() As Decimal
        Get
            Return Me._ag_tasa
        End Get
        Set(ByVal value As Decimal)
            Me._ag_tasa = value
        End Set
    End Property

    Public Property SujetoAPercepcion() As Boolean
        Get
            Return Me._ag_SujetoAPercepcion
        End Get
        Set(ByVal value As Boolean)
            Me._ag_SujetoAPercepcion = value
        End Set
    End Property

    Public Property SujetoARetencion() As Boolean
        Get
            Return Me._ag_SujetoARetencion
        End Get
        Set(ByVal value As Boolean)
            Me._ag_SujetoARetencion = value
        End Set
    End Property

    Public Property SujetoADetraccion() As Boolean
        Get
            Return Me._ag_SujetoADetraccion
        End Get
        Set(ByVal value As Boolean)
            Me._ag_SujetoADetraccion = value
        End Set
    End Property

    Public Property tipoAgente() As Integer
        Get
            Return _tipoAgente
        End Get
        Set(ByVal value As Integer)
            _tipoAgente = value
        End Set
    End Property
End Class

