﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class TipoTabla
    Private _IdTipoTabla As Integer
    Private _tt_Nombre As String
    Private _tt_Abv As String
    Private _tt_Longitud As Integer
    Private _tt_Estado As Boolean
    Private _Item As Integer
    Public Sub New()
    End Sub
    Public Sub New(ByVal idtipotabla As Integer, ByVal nombre As String, ByVal estado As Boolean)
        _IdTipoTabla = idtipotabla
        _tt_Nombre = nombre
        _tt_Estado = estado
    End Sub
    Public Property IdTipoTabla() As Integer
        Get
            Return _IdTipoTabla
        End Get
        Set(ByVal value As Integer)
            _IdTipoTabla = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return _tt_Nombre
        End Get
        Set(ByVal value As String)
            _tt_Nombre = value
        End Set
    End Property
    Public Property Abv() As String
        Get
            Return _tt_Abv
        End Get
        Set(ByVal value As String)
            _tt_Abv = value
        End Set
    End Property
    Public Property Longitud() As Integer
        Get
            Return _tt_Longitud
        End Get
        Set(ByVal value As Integer)
            _tt_Longitud = value
        End Set
    End Property
    Public Property Estado() As Boolean
        Get
            Return _tt_Estado
        End Get
        Set(ByVal value As Boolean)
            _tt_Estado = value
        End Set
    End Property
    Public ReadOnly Property strEstado() As String
        Get
            If Estado = True Then Return "Activo"
            If Estado = False Then Return "Inactivo"
        End Get
    End Property

    Public Property Item() As Integer
        Get
            Return _Item
        End Get
        Set(ByVal value As Integer)
            _Item = value
        End Set
    End Property

End Class
