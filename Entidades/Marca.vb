'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Marca
    Private _IdMarca As Integer
    Private _mar_Nombre As String
    Private _mar_Abv As String
    Private _mar_Estado As String
    Private _descEstado As String
    Private _mar_Estadov2 As Boolean
    Private _mar_Codigo As String

    Sub New()
    End Sub

    Public Sub New(ByVal Idmarca As Integer, ByVal nombre As String)
        Me.Id = Idmarca
        Me.Descripcion = nombre
    End Sub
    Public Sub New(ByVal Idmarca As Integer, ByVal nombre As String, ByVal estado As Boolean)
        Me._IdMarca = Idmarca
        Me._mar_Nombre = nombre
        Me._mar_Estadov2 = estado
    End Sub


    Public ReadOnly Property DescEstado()
        Get
            Me._descEstado = "Activo"
            If Me._mar_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public Property Id() As Integer
        Get
            Return Me._IdMarca
        End Get
        Set(ByVal value As Integer)
            Me._IdMarca = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._mar_Nombre
        End Get
        Set(ByVal value As String)
            Me._mar_Nombre = value
        End Set
    End Property

    Public Property Abv() As String
        Get
            Return Me._mar_Abv
        End Get
        Set(ByVal value As String)
            Me._mar_Abv = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._mar_Estado
        End Get
        Set(ByVal value As String)
            Me._mar_Estado = value
        End Set
    End Property
    Public Property mar_Codigo() As String
        Get
            Return _mar_Codigo
        End Get
        Set(ByVal value As String)
            _mar_Codigo = value
        End Set
    End Property
End Class
