﻿Public Class ProgramacionReporte
    Private _IdProveedor As Integer
    Private _IdMovCtaPP As Integer
    Private _IdProgramacion As Integer
    Private _FechaFin As Date
    Private _FechaInicio As Date
    Private _IdCuentaProv As Integer
    Private _Proveedor As String
    Private _mcp_Fecha As Date
    Private _mcp_FechaCanc As Date
    Private _mcp_Monto As Decimal
    Private _mcp_Saldo As Decimal
    Private _IdDocumento As Integer
    Private _pp_FechaPagoProg As Date
    Private _pp_MontoPagoProg As Decimal
    Private _IdBanco As Integer
    Private _IdMedioPago As Integer
    Private _IdCuentaBancaria As Integer
    Private _doc_Serie As String
    Private _pp_Observacion As String
    Private _doc_Codigo As String
    Private _doc_FechaEmision As Date
    Private _doc_FechaVenc As Date
    Private _doc_TotalAPagar As Decimal
    Private _IdTipoDocumento As Integer
    Private _edoc_Nombre As String
    Private _mon_Simbolo As String
    Private _monedapago As String
    Private _mp_Nombre As String
    Private _ban_Nombre As String
    Private _cb_numero As String
    Private _NumeroDocumento As String
    Private _tipoDocumento As String

    Public Property TipoDocumento() As String
        Get
            Return Me._tipoDocumento
        End Get
        Set(ByVal value As String)
            Me._tipoDocumento = value
        End Set
    End Property
    Public Property NumeroDocumento() As String
        Get
            Return Me._NumeroDocumento
        End Get
        Set(ByVal value As String)
            Me._NumeroDocumento = value
        End Set
    End Property
    Public Property IdMovCtaPP() As Integer
        Get
            Return Me._IdMovCtaPP
        End Get
        Set(ByVal value As Integer)
            Me._IdMovCtaPP = value
        End Set
    End Property


    Public Property cb_numero() As String
        Get
            Return Me._cb_numero
        End Get
        Set(ByVal value As String)
            Me._cb_numero = value
        End Set
    End Property
    Public Property ban_Nombre() As String
        Get
            Return Me._ban_Nombre
        End Get
        Set(ByVal value As String)
            Me._ban_Nombre = value
        End Set
    End Property
    Public Property mp_Nombre() As String
        Get
            Return Me._mp_Nombre
        End Get
        Set(ByVal value As String)
            Me._mp_Nombre = value
        End Set
    End Property
    Public Property monedapago() As String
        Get
            Return Me._monedapago
        End Get
        Set(ByVal value As String)
            Me._monedapago = value
        End Set
    End Property
    Public Property mon_Simbolo() As String
        Get
            Return Me._mon_Simbolo
        End Get
        Set(ByVal value As String)
            Me._mon_Simbolo = value
        End Set
    End Property
    Public Property edoc_Nombre() As String
        Get
            Return Me._edoc_Nombre
        End Get
        Set(ByVal value As String)
            Me._edoc_Nombre = value
        End Set
    End Property
    Public Property IdTipoDocumento() As Integer
        Get
            Return Me._IdTipoDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoDocumento = value
        End Set
    End Property
    Public Property doc_TotalAPagar() As Decimal
        Get
            Return Me._doc_TotalAPagar
        End Get
        Set(ByVal value As Decimal)
            Me._doc_TotalAPagar = value
        End Set
    End Property
    Public Property doc_FechaEmision() As Date
        Get
            Return Me._doc_FechaEmision
        End Get
        Set(ByVal value As Date)
            Me._doc_FechaEmision = value
        End Set
    End Property
    Public Property doc_FechaVenc() As Date
        Get
            Return Me._doc_FechaVenc
        End Get
        Set(ByVal value As Date)
            Me._doc_FechaVenc = value
        End Set
    End Property

    Public Property doc_Serie() As String
        Get
            Return Me._doc_Serie
        End Get
        Set(ByVal value As String)
            Me._doc_Serie = value
        End Set
    End Property

    Public Property doc_Codigo() As String
        Get
            Return Me._doc_Codigo
        End Get
        Set(ByVal value As String)
            Me._doc_Codigo = value
        End Set
    End Property
    Public Property pp_Observacion() As String
        Get
            Return Me._pp_Observacion
        End Get
        Set(ByVal value As String)
            Me._pp_Observacion = value
        End Set
    End Property
    Public Property IdMedioPago() As Integer
        Get
            Return Me._IdMedioPago
        End Get
        Set(ByVal value As Integer)
            Me._IdMedioPago = value
        End Set
    End Property
    Public Property IdCuentaBancaria() As Integer
        Get
            Return Me._IdCuentaBancaria
        End Get
        Set(ByVal value As Integer)
            Me._IdCuentaBancaria = value
        End Set
    End Property

    Public Property IdBanco() As Integer
        Get
            Return Me._IdBanco
        End Get
        Set(ByVal value As Integer)
            Me._IdBanco = value
        End Set
    End Property

    Public Property pp_MontoPagoProg() As Decimal
        Get
            Return Me._pp_MontoPagoProg
        End Get
        Set(ByVal value As Decimal)
            Me._pp_MontoPagoProg = value
        End Set
    End Property

    Public Property pp_FechaPagoProg() As Date
        Get
            Return Me._pp_FechaPagoProg
        End Get
        Set(ByVal value As Date)
            Me._pp_FechaPagoProg = value
        End Set
    End Property

    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property
    Public Property mcp_Saldo() As Decimal
        Get
            Return Me._mcp_Saldo
        End Get
        Set(ByVal value As Decimal)
            Me._mcp_Saldo = value
        End Set
    End Property
    Public Property mcp_Monto() As Decimal
        Get
            Return Me._mcp_Monto
        End Get
        Set(ByVal value As Decimal)
            Me._mcp_Monto = value
        End Set
    End Property
    Public Property mcp_FechaCanc() As Date
        Get
            Return Me._mcp_FechaCanc
        End Get
        Set(ByVal value As Date)
            Me._mcp_FechaCanc = value
        End Set
    End Property
    Public Property mcp_Fecha() As Date
        Get
            Return Me._mcp_Fecha
        End Get
        Set(ByVal value As Date)
            Me._mcp_Fecha = value
        End Set
    End Property
    Public Property Proveedor() As String
        Get
            Return Me._Proveedor
        End Get
        Set(ByVal value As String)
            Me._Proveedor = value
        End Set
    End Property
    Public Property IdCuentaProv() As Integer
        Get
            Return Me._IdCuentaProv
        End Get
        Set(ByVal value As Integer)
            Me._IdCuentaProv = value
        End Set
    End Property
    Public Property IdProveedor() As Integer
        Get
            Return Me._IdProveedor
        End Get
        Set(ByVal value As Integer)
            Me._IdProveedor = value
        End Set
    End Property

    Public Property IdProgramacionPago() As Integer
        Get
            Return Me._IdProgramacion
        End Get
        Set(ByVal value As Integer)
            Me._IdProgramacion = value
        End Set
    End Property


    Public Property FechaFin() As Date
        Get
            Return Me._FechaFin
        End Get
        Set(ByVal value As Date)
            Me._FechaFin = value
        End Set
    End Property
    Public Property FechaInicio() As Date
        Get
            Return Me._FechaInicio
        End Get
        Set(ByVal value As Date)
            Me._FechaInicio = value
        End Set
    End Property
End Class
