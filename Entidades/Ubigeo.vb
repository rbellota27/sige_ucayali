'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Ubigeo
    Private _IdUbigeo As Integer
    Private _ub_CodDpto As String
    Private _ub_CodProv As String
    Private _ub_CodDist As String
    Private _ub_Nombre As String
    Private _ub_Estado As String
    Private _IdZona As Integer

    Private _UBIGEO As String

    Public Sub New()
    End Sub

    Public Sub New(ByVal UBIGEO As String)

        Me.CodDpto = UBIGEO.Substring(0, 2)
        Me.CodProv = UBIGEO.Substring(2, 2)
        Me.CodDist = UBIGEO.Substring(4, 2)

    End Sub


    Public Sub New(ByVal idubigeo As Integer, ByVal coddepto As String, ByVal codprov As String, ByVal coddist As String, ByVal nombre As String, ByVal estado As String)
        Me.Id = idubigeo
        Me.Nombre = nombre
        Me.CodDpto = coddepto
        Me.CodProv = codprov
        Me.CodDist = coddist
        Me.Estado = estado
    End Sub
    Public Property Estado() As String
        Get
            Return Me._ub_Estado
        End Get
        Set(ByVal value As String)
            Me._ub_Estado = value
        End Set
    End Property


    Public Property Id() As Integer
        Get
            Return Me._IdUbigeo
        End Get
        Set(ByVal value As Integer)
            Me._IdUbigeo = value
        End Set
    End Property

    Public Property CodDpto() As String
        Get
            Return Me._ub_CodDpto
        End Get
        Set(ByVal value As String)
            Me._ub_CodDpto = value
        End Set
    End Property

    Public Property CodProv() As String
        Get
            Return Me._ub_CodProv
        End Get
        Set(ByVal value As String)
            Me._ub_CodProv = value
        End Set
    End Property

    Public Property CodDist() As String
        Get
            Return Me._ub_CodDist
        End Get
        Set(ByVal value As String)
            Me._ub_CodDist = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._ub_Nombre
        End Get
        Set(ByVal value As String)
            Me._ub_Nombre = value
        End Set
    End Property

    Public Property IdZona() As Integer
        Get
            Return Me._IdZona
        End Get
        Set(ByVal value As Integer)
            Me._IdZona = value
        End Set
    End Property
End Class
