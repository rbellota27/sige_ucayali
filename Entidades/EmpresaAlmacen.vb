﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class EmpresaAlmacen

    Private _IdAlmacen As Integer
    Private _IdEmpresa As Integer
    Private _alm_Nombre As String
    Private _EstadoAI As Boolean
    Public Sub New()
    End Sub
    Public Sub New(ByVal idalmacen As Integer, ByVal nombre As String, ByVal idempresa As Integer, ByVal EstadoAI As Boolean)
        Me.IdAlmacen = idalmacen
        Me.Nombre = nombre
        Me.IdEmpresa = idempresa
        Me.EstadoAI = EstadoAI
    End Sub
    Public Property IdAlmacen() As Integer
        Get
            Return Me._IdAlmacen
        End Get
        Set(ByVal value As Integer)
            Me._IdAlmacen = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return Me._alm_Nombre
        End Get
        Set(ByVal value As String)
            Me._alm_Nombre = value
        End Set
    End Property
    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property
    Public Property EstadoAI() As Boolean
        Get
            Return Me._EstadoAI
        End Get
        Set(ByVal value As Boolean)
            Me._EstadoAI = value
        End Set
    End Property
End Class
