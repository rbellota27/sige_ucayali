﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class RegistroGasto

    Inherits DetalleConcepto

    Private _objconcepto As Object
    Private _objmoneda As Object
    Private _objmotivo As Object
    Private _objtipodocumento As Object
    Private _idMotivo As Integer
    Private _idTipoDocumento As Integer
    Private _idPersona As Integer
    Private _nomPersona As String

    Public Property IdPersona() As Integer
        Get
            Return _idPersona
        End Get
        Set(ByVal value As Integer)
            _idPersona = value
        End Set
    End Property

    Public Property NomPersona() As String
        Get
            Return _nomPersona
        End Get
        Set(ByVal value As String)
            _nomPersona = value
        End Set
    End Property

    Public Property objMotivo() As Object
        Get
            Return _objmotivo
        End Get
        Set(ByVal value As Object)
            _objmotivo = value
        End Set
    End Property

    Public Property IdTipoDocumento() As Integer
        Get
            Return _idTipoDocumento
        End Get
        Set(ByVal value As Integer)
            _idTipoDocumento = value
        End Set
    End Property

    Public Property IdMotivo() As Integer
        Get
            Return _idMotivo
        End Get
        Set(ByVal value As Integer)
            _idMotivo = value
        End Set
    End Property

    Public Property objTipoDocumento() As Object
        Get
            Return _objtipodocumento
        End Get
        Set(ByVal value As Object)
            _objtipodocumento = value
        End Set
    End Property

    Public Property objConcepto() As Object
        Get
            Return _objconcepto
        End Get
        Set(ByVal value As Object)
            _objconcepto = value
        End Set
    End Property

    Public Property objMoneda() As Object
        Get
            Return _objmoneda
        End Get
        Set(ByVal value As Object)
            _objmoneda = value
        End Set
    End Property

    Public Class CabeceraRegistroGasto
        Inherits Documento

        Private _idcentrocosto As String
        Private _IdUsuarioSupervisor As Integer
        Private _nombrePersona As String
        Private _Ruc As String
        Private _Dni As String
        Private _NomSupervisor As String
        Private _codigoRef As String
        Private _serieRef As String
        Private _idmoneda2 As Integer
        Private _doc_Total2 As Decimal

        Public Property doc_Total2() As Decimal
            Get
                Return _doc_Total2
            End Get
            Set(ByVal value As Decimal)
                _doc_Total2 = value
            End Set
        End Property

        Public Property Idmoneda2() As Integer
            Get
                Return _idmoneda2
            End Get
            Set(ByVal value As Integer)
                _idmoneda2 = value
            End Set
        End Property

        Public Property codigoRef() As String
            Get
                Return _codigoRef
            End Get
            Set(ByVal value As String)
                _codigoRef = value
            End Set
        End Property

        Public Property serieRef() As String
            Get
                Return _serieRef
            End Get
            Set(ByVal value As String)
                _serieRef = value
            End Set
        End Property

        Public Property NomSupervisor() As String
            Get
                Return _NomSupervisor
            End Get
            Set(ByVal value As String)
                _NomSupervisor = value
            End Set
        End Property

        Public Property Dni() As String
            Get
                Return _Dni
            End Get
            Set(ByVal value As String)
                _Dni = value
            End Set
        End Property

        Public Property Ruc() As String
            Get
                Return _Ruc
            End Get
            Set(ByVal value As String)
                _Ruc = value
            End Set
        End Property

        Public Property NombrePersona() As String
            Get
                Return _nombrePersona
            End Get
            Set(ByVal value As String)
                _nombrePersona = value
            End Set
        End Property

        Public Property IdCentroCosto() As String
            Get
                Return _idcentrocosto
            End Get
            Set(ByVal value As String)
                _idcentrocosto = value
            End Set
        End Property

        Public Property IdUsuarioSupervisor() As Integer
            Get
                Return _IdUsuarioSupervisor
            End Get
            Set(ByVal value As Integer)
                _IdUsuarioSupervisor = value
            End Set
        End Property

    End Class

End Class
