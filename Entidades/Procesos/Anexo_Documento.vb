﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Anexo_Documento
    Implements System.ICloneable

#Region "variables Privadas"
    Private _IdDocumento As Integer
    Private _doc_Importacion As Boolean
    Private _IdUsuarioSupervisor As Integer
    Private _IdMedioPago As Integer
    Private _NroDiasVigencia As Decimal

    Private _PrecImportacion As String
    Private _NroContenedores As String
    Private _TotalConcepto As Decimal
    Private _IdArea As Integer
    Private _IdAlias As Integer
    Private _IdMaestroObra As Integer
    Private _IdCentroCosto As Integer
    Private _IdTPImp As Integer
    Private _IdSerieCheque As Integer
    Private _anex_NroOperacion As String
    Private _MontoAfectoIgv As Decimal
    Private _MontoNoAfectoIgv As Decimal
    Private _Aprobar As Boolean
    Private _IdYear As Integer
    Private _IdSemana As Integer
    Private _FechaAprobacion As Date
    Private _MontoxSustentar As Decimal
    Private _Liquidado As Boolean
    Private _Codigo As String
    Private _Serie As String
    Private _TotalConceptoAbs As Decimal
    Private _MontoISC As Decimal
    Private _MontoOtroTributo As Decimal
    Private _RetencionHonorarios As Decimal
    Private _RetencionHonorariosPercent As Decimal
    Private _anex_DescuentoGlobal As Decimal
    Private _EstReque As Integer
    Private _Banco As String
    Private _CuentaBancaria As String
    Private _IdCuentabancaria As Integer
    Private _IdBanco As Integer
    Private _cb_CuentaInterbanca As String
    Private _swiftbanca As String
    Private _tipoRegimen As Integer
#End Region

#Region "Propiedades Publicas"
    Sub New()
    End Sub

    Public Property cb_CuentaInterbancaria() As String
        Get
            Return _cb_CuentaInterbanca
        End Get
        Set(ByVal value As String)
            _cb_CuentaInterbanca = value
        End Set
    End Property
    Public Property cb_SwiftBancario() As String
        Get
            Return _swiftbanca
        End Get
        Set(ByVal value As String)
            _swiftbanca = value
        End Set
    End Property
    Public Property Banco() As String
        Get
            Return _Banco
        End Get
        Set(ByVal value As String)
            _Banco = value
        End Set
    End Property

    Public Property IdBanco() As Integer
        Get
            Return _IdBanco
        End Get
        Set(ByVal value As Integer)
            _IdBanco = value
        End Set
    End Property
    Public Property NroCuentaBancaria() As String
        Get
            Return _CuentaBancaria
        End Get
        Set(ByVal value As String)
            _CuentaBancaria = value
        End Set
    End Property

    Public Property IdCuentaBancaria() As Integer
        Get
            Return _IdCuentabancaria
        End Get
        Set(ByVal value As Integer)
            _IdCuentabancaria = value
        End Set
    End Property
    Public Property EstadoRequerimiento() As Integer
        Get
            Return _EstReque
        End Get
        Set(ByVal value As Integer)
            _EstReque = value
        End Set
    End Property
    Public Property anex_DescuentoGlobal() As Decimal
        Get
            Return _anex_DescuentoGlobal
        End Get
        Set(ByVal value As Decimal)
            _anex_DescuentoGlobal = value
        End Set
    End Property

    Public Property RetencionHonorarios() As Decimal
        Get
            Return _RetencionHonorarios
        End Get
        Set(ByVal value As Decimal)
            _RetencionHonorarios = value
        End Set
    End Property

    Public ReadOnly Property RetencionHonorariosPercent() As Decimal
        Get
            'If _RetencionHonorarios > 0 And _MontoNoAfectoIgv > 0 Then   ' ******* BORRAR ABAJO 
            '    _RetencionHonorariosPercent = (_RetencionHonorarios / _MontoNoAfectoIgv) * 100
            'End If
            If _MontoxSustentar > 0 And _MontoNoAfectoIgv > 0 Then
                _RetencionHonorariosPercent = (_MontoxSustentar / _MontoNoAfectoIgv) * 100
            End If
            Return _RetencionHonorariosPercent
        End Get
    End Property



    Public Property MontoOtroTributo() As Decimal
        Get
            Return _MontoOtroTributo
        End Get
        Set(ByVal value As Decimal)
            _MontoOtroTributo = value
        End Set
    End Property

    Public Property MontoISC() As Decimal
        Get
            Return _MontoISC
        End Get
        Set(ByVal value As Decimal)
            _MontoISC = value
        End Set
    End Property

    Public Property TotalConceptoAbs() As Decimal
        Get
            Return _TotalConceptoAbs
        End Get
        Set(ByVal value As Decimal)
            _TotalConceptoAbs = value
        End Set
    End Property

    Public Property Serie() As String
        Get
            Return _Serie
        End Get
        Set(ByVal value As String)
            _Serie = value
        End Set
    End Property

    Public Property Codigo() As String
        Get
            Return _Codigo
        End Get
        Set(ByVal value As String)
            _Codigo = value
        End Set
    End Property


    Public Property MontoxSustentar() As Decimal
        Get
            Return Me._MontoxSustentar
        End Get
        Set(ByVal value As Decimal)
            Me._MontoxSustentar = value
        End Set
    End Property

    Public Property Liquidado() As Boolean
        Get
            Return Me._Liquidado
        End Get
        Set(ByVal value As Boolean)
            Me._Liquidado = value
        End Set
    End Property

    Public Property MontoAfectoIgv() As Decimal
        Get
            Return Me._MontoAfectoIgv
        End Get
        Set(ByVal value As Decimal)
            Me._MontoAfectoIgv = value
        End Set
    End Property

    Public Property MontoNoAfectoIgv() As Decimal
        Get
            Return Me._MontoNoAfectoIgv
        End Get
        Set(ByVal value As Decimal)
            Me._MontoNoAfectoIgv = value
        End Set
    End Property
    Public Property FechaAprobacion() As Date
        Get
            Return Me._FechaAprobacion
        End Get
        Set(ByVal value As Date)
            Me._FechaAprobacion = value
        End Set
    End Property

    Public ReadOnly Property getFechaAprobacionText() As String
        Get

            Dim FechaAprobacionText As String = ""
            If (Me.FechaAprobacion <> Nothing) Then
                FechaAprobacionText = Me.FechaAprobacion.ToString
            End If
            Return FechaAprobacionText
        End Get
    End Property

    Public Property IdYear() As Integer
        Get
            Return Me._IdYear
        End Get
        Set(ByVal value As Integer)
            Me._IdYear = value
        End Set
    End Property

    Public Property IdSemana() As Integer
        Get
            Return Me._IdSemana
        End Get
        Set(ByVal value As Integer)
            Me._IdSemana = value
        End Set
    End Property



    Public Property Aprobar() As Boolean
        Get
            Return Me._Aprobar
        End Get
        Set(ByVal value As Boolean)
            Me._Aprobar = value
        End Set
    End Property




    Public Property IdMaestroObra() As Integer
        Get
            Return Me._IdMaestroObra
        End Get
        Set(ByVal value As Integer)
            Me._IdMaestroObra = value
        End Set
    End Property



    Public Property IdAlias() As Integer
        Get
            Return Me._IdAlias
        End Get
        Set(ByVal value As Integer)
            Me._IdAlias = value
        End Set
    End Property


    Public Property IdArea() As Integer
        Get
            Return Me._IdArea
        End Get
        Set(ByVal value As Integer)
            Me._IdArea = value
        End Set
    End Property

    Public Property TotalConcepto() As Decimal
        Get
            Return Me._TotalConcepto
        End Get
        Set(ByVal value As Decimal)
            Me._TotalConcepto = value
        End Set
    End Property
    Public Property PrecImportacion() As String
        Get
            Return Me._PrecImportacion
        End Get
        Set(ByVal value As String)
            Me._PrecImportacion = value
        End Set
    End Property

    Public Property NroContenedores() As String
        Get
            Return Me._NroContenedores
        End Get
        Set(ByVal value As String)
            Me._NroContenedores = value
        End Set
    End Property
    Public Property NroDiasVigencia() As Decimal
        Get
            Return Me._NroDiasVigencia
        End Get
        Set(ByVal value As Decimal)
            Me._NroDiasVigencia = value
        End Set
    End Property

    Public Property IdDocumento() As Integer
        Get
            Return _IdDocumento
        End Get
        Set(ByVal value As Integer)
            _IdDocumento = value
        End Set
    End Property
    Public Property doc_Importacion() As Boolean
        Get
            Return doc_Importacion
        End Get
        Set(ByVal value As Boolean)
            _doc_Importacion = value
        End Set
    End Property
    Public Property IdUsuarioSupervisor() As Integer
        Get
            Return _IdUsuarioSupervisor
        End Get
        Set(ByVal value As Integer)
            _IdUsuarioSupervisor = value
        End Set
    End Property

    Public Property IdMedioPago() As Integer
        Get
            Return _IdMedioPago
        End Get
        Set(ByVal value As Integer)
            _IdMedioPago = value
        End Set
    End Property

    Public Property IdCentroCosto() As Integer
        Get
            Return _IdCentroCosto
        End Get
        Set(ByVal value As Integer)
            _IdCentroCosto = value
        End Set
    End Property

    Public Property IdTPImp() As Integer
        Get
            Return _IdTPImp
        End Get
        Set(ByVal value As Integer)
            _IdTPImp = value
        End Set
    End Property


    Public Property IdSerieCheque() As Integer
        Get
            Return _IdSerieCheque
        End Get
        Set(ByVal value As Integer)
            _IdSerieCheque = value
        End Set
    End Property

    Public Property NroOperacion() As String
        Get
            Return _anex_NroOperacion
        End Get
        Set(ByVal value As String)
            _anex_NroOperacion = value
        End Set
    End Property

    Public Property tipoRegimen() As Integer
        Get
            Return _tipoRegimen
        End Get
        Set(ByVal value As Integer)
            _tipoRegimen = value
        End Set
    End Property

#End Region
    Public Function getClone() As Anexo_Documento
        Return CType(Clone(), Anexo_Documento)
    End Function

    Public Function Clone() As Object Implements System.ICloneable.Clone
        Dim x As New Anexo_Documento

        x._IdDocumento = Me._IdDocumento
        x._doc_Importacion = Me._doc_Importacion
        x._IdUsuarioSupervisor = Me._IdUsuarioSupervisor
        x._IdMedioPago = Me._IdMedioPago
        x._NroDiasVigencia = Me._NroDiasVigencia

        x._PrecImportacion = Me._PrecImportacion
        x._NroContenedores = Me._NroContenedores
        x._TotalConcepto = Me._TotalConcepto
        x._IdArea = Me._IdArea
        x._IdAlias = Me._IdAlias
        x._IdMaestroObra = Me._IdMaestroObra
        x._IdCentroCosto = Me._IdCentroCosto
        x._IdTPImp = Me._IdTPImp
        x._IdSerieCheque = Me._IdSerieCheque
        x._EstReque = Me._EstReque
        x._Aprobar = Me._Aprobar
        x._IdYear = Me._IdYear
        x._IdSemana = Me._IdSemana
        x._anex_NroOperacion = Me._anex_NroOperacion

        Return x

    End Function







End Class
