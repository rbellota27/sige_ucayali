﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



'**********   VIERNES 05 MARZO 2010 HORA 09_36 AM









Public Class DocumentoNotaCredito
    Inherits Entidades.Documento

    Private _Observacion As String
    Private _Ruc As String
    Private _Dni As String

    Private _IdDocumentoRef As Integer
    Private _TipoDocumentoRef As String
    Private _DocRef_Serie As String
    Private _DocRef_Codigo As String
    Private _DocRef_FechaEmision As Date
    Private _DocRef_Total As Decimal
    Private _DocRef_TotalAPagar As Decimal
    Private _DocRef_Percepcion As Decimal


    Private _DocRef_IdMoneda As Integer

    Public Property DocRef_IdMoneda() As Integer
        Get
            Return Me._DocRef_IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._DocRef_IdMoneda = value
        End Set
    End Property




    Public Property IdDocumentoRef() As Integer
        Get
            Return Me._IdDocumentoRef
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumentoRef = value
        End Set
    End Property

    Public Property TipoDocumentoRef() As String
        Get
            Return Me._TipoDocumentoRef
        End Get
        Set(ByVal value As String)
            Me._TipoDocumentoRef = value
        End Set
    End Property

    Public Property DocRef_Serie() As String
        Get
            Return Me._DocRef_Serie
        End Get
        Set(ByVal value As String)
            Me._DocRef_Serie = value
        End Set
    End Property

    Public Property DocRef_Codigo() As String
        Get
            Return Me._DocRef_Codigo
        End Get
        Set(ByVal value As String)
            Me._DocRef_Codigo = value
        End Set
    End Property

    Public Property DocRef_FechaEmision() As Date
        Get
            Return Me._DocRef_FechaEmision
        End Get
        Set(ByVal value As Date)
            Me._DocRef_FechaEmision = value
        End Set
    End Property

    Public Property DocRef_Total() As Decimal
        Get
            Return Me._DocRef_Total
        End Get
        Set(ByVal value As Decimal)
            Me._DocRef_Total = value
        End Set
    End Property

    Public Property DocRef_TotalAPagar() As Decimal
        Get
            Return Me._DocRef_TotalAPagar
        End Get
        Set(ByVal value As Decimal)
            Me._DocRef_TotalAPagar = value
        End Set
    End Property

    Public Property DocRef_Percepcion() As Decimal
        Get
            Return Me._DocRef_Percepcion
        End Get
        Set(ByVal value As Decimal)
            Me._DocRef_Percepcion = value
        End Set
    End Property

    Public Property Ruc() As String
        Get
            Return Me._Ruc
        End Get
        Set(ByVal value As String)
            Me._Ruc = value
        End Set
    End Property

    Public Property Dni() As String
        Get
            Return Me._Dni
        End Get
        Set(ByVal value As String)
            Me._Dni = value
        End Set
    End Property


    Public Property Observacion() As String
        Get
            Return Me._Observacion
        End Get
        Set(ByVal value As String)
            Me._Observacion = value
        End Set
    End Property



End Class
