﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Nagamine Molina, Jorge
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 12-Octubre-2009
'Hora    : 01:10 pm
'*************************************************

Public Class Documento_MovCuenta
    Inherits Entidades.Documento

    Private _IdMovCuenta As Integer
    Private _MontoTotal As Decimal
    Private _Saldo As Decimal
    Private _NroDiasMora As Integer
    Private _NroDocumento As String
    Private _strTipoDocumentoRef As String
    Private _NuevoSaldo As Decimal


    Public Property NuevoSaldo() As Decimal
        Get
            Return _NuevoSaldo
        End Get
        Set(ByVal value As Decimal)
            _NuevoSaldo = value
        End Set
    End Property


    Public Property strTipoDocumentoRef() As String
        Get
            Return _strTipoDocumentoRef
        End Get
        Set(ByVal value As String)
            _strTipoDocumentoRef = value
        End Set
    End Property

    Public ReadOnly Property getTipoDocumentoRef() As List(Of Entidades.TipoDocumento)
        Get
            If strTipoDocumentoRef <> "" Then
                Dim lista As New List(Of Entidades.TipoDocumento)
                Dim TipoDocumentoRef() As String
                TipoDocumentoRef = strTipoDocumentoRef.Split(CChar(","))
                For x As Integer = 0 To TipoDocumentoRef.Length - 1
                    If TipoDocumentoRef(x) <> "" Then
                        lista.Add(New Entidades.TipoDocumento(0, TipoDocumentoRef(x)))
                    End If
                Next
                Return lista
            End If
            Return Nothing
        End Get
    End Property

    Public Property NroDocumento() As String
        Get
            Return Me._NroDocumento
        End Get
        Set(ByVal value As String)
            Me._NroDocumento = value
        End Set
    End Property


    Public ReadOnly Property getNroDocumento() As String

        Get

            Return Serie + " - " + Codigo

        End Get

    End Property




    Public Property IdMovCuenta() As Integer
        Get
            Return Me._IdMovCuenta
        End Get
        Set(ByVal value As Integer)
            Me._IdMovCuenta = value
        End Set
    End Property

    Public Property MontoTotal() As Decimal
        Get
            Return Me._MontoTotal
        End Get
        Set(ByVal value As Decimal)
            Me._MontoTotal = value
        End Set
    End Property

    Public Property Saldo() As Decimal
        Get
            Return Me._Saldo
        End Get
        Set(ByVal value As Decimal)
            Me._Saldo = value
        End Set
    End Property



    Public Property NroDiasMora() As Integer
        Get
            Return Me._NroDiasMora
        End Get
        Set(ByVal value As Integer)
            Me._NroDiasMora = value
        End Set
    End Property

End Class
