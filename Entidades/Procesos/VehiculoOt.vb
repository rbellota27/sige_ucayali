﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class VehiculoOt

    Private _id As Integer
    Private _idmodelo As Integer
    Private _placa As String
    Private _anoFabric As String
    Private _nrochasis As String
    Private _tipomotor As String
    Private _tipocombustible As String
    Private _nrocilindros As String
    Private _linea As String
    Private _marca As String
    Private _modelo As String
    Private _serie As String

    Public Property serie() As String
        Get
            Return _serie
        End Get
        Set(ByVal value As String)
            _serie = value
        End Set
    End Property

    Public Property modelo() As String
        Get
            Return _modelo
        End Get
        Set(ByVal value As String)
            _modelo = value
        End Set
    End Property

    Public Property marca() As String
        Get
            Return _marca
        End Get
        Set(ByVal value As String)
            _marca = value
        End Set
    End Property

    Public Property linea() As String
        Get
            Return _linea
        End Get
        Set(ByVal value As String)
            _linea = value
        End Set
    End Property

    Public Property nrocilindros() As String
        Get
            Return _nrocilindros
        End Get
        Set(ByVal value As String)
            _nrocilindros = value
        End Set
    End Property

    Public Property tipocombustible() As String
        Get
            Return _tipocombustible
        End Get
        Set(ByVal value As String)
            _tipocombustible = value
        End Set
    End Property

    Public Property tipomotor() As String
        Get
            Return _tipomotor
        End Get
        Set(ByVal value As String)
            _tipomotor = value
        End Set
    End Property


    Public Property id() As Integer
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            _id = value
        End Set
    End Property

    Public Property idmodelo() As Integer
        Get
            Return _idmodelo
        End Get
        Set(ByVal value As Integer)
            _idmodelo = value
        End Set
    End Property

    Public Property placa() As String
        Get
            Return _placa
        End Get
        Set(ByVal value As String)
            _placa = value
        End Set
    End Property

    Public Property anoFabric() As String
        Get
            Return _anoFabric
        End Get
        Set(ByVal value As String)
            _anoFabric = value
        End Set
    End Property

    Public Property nrochasis() As String
        Get
            Return _nrochasis
        End Get
        Set(ByVal value As String)
            _nrochasis = value
        End Set
    End Property

    Private _iddocumento As Integer
    Public Property iddocumento() As Integer
        Get
            Return _iddocumento
        End Get
        Set(ByVal value As Integer)
            _iddocumento = value
        End Set
    End Property

    Private _idproducto As Integer
    Public Property idproducto() As Integer
        Get
            Return _idproducto
        End Get
        Set(ByVal value As Integer)
            _idproducto = value
        End Set
    End Property

    Private _nomProducto As String
    Public Property nomProducto() As String
        Get
            Return _nomProducto
        End Get
        Set(ByVal value As String)
            _nomProducto = value
        End Set
    End Property

    Private _cantidad As Decimal
    Public Property cantidad() As Decimal
        Get
            Return _cantidad
        End Get
        Set(ByVal value As Decimal)
            _cantidad = value
        End Set
    End Property

    Private _estado As Boolean
    Public Property estado() As Boolean
        Get
            Return _estado
        End Get
        Set(ByVal value As Boolean)
            _estado = value
        End Set
    End Property

    Private _esservicio As Boolean
    Public Property esservicio() As Boolean
        Get
            Return _esservicio
        End Get
        Set(ByVal value As Boolean)
            _esservicio = value
        End Set
    End Property


End Class
