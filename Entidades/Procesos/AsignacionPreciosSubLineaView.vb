﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class AsignacionPreciosSubLineaView
    Private _IdProducto As Integer
    Private _nomProducto As String
    Private _IdUMPrincipal As Integer
    Private _nomUMPrincipal As String
    Private _PrecioCompra As Decimal
    Private _PorcentUtilFijo As Decimal
    Private _PorcentUtilVar As Decimal
    Private _escalarUtilidad As Decimal
    Private _escalarValor As Decimal
    'Private _escalarPrecioVentaFinal As Decimal

    'Public Property EscalarPrecioVentaFinal() As Decimal
    '    Get
    '        Return Me._escalarPrecioVentaFinal
    '    End Get
    '    Set(ByVal value As Decimal)
    '        Me._escalarPrecioVentaFinal = value
    '    End Set
    'End Property
    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property

    Public Property nomProducto() As String
        Get
            Return Me._nomProducto
        End Get
        Set(ByVal value As String)
            Me._nomProducto = value
        End Set
    End Property

    Public Property IdUMPrincipal() As Integer
        Get
            Return Me._IdUMPrincipal
        End Get
        Set(ByVal value As Integer)
            Me._IdUMPrincipal = value
        End Set
    End Property

    Public Property nomUMPrincipal() As String
        Get
            Return Me._nomUMPrincipal
        End Get
        Set(ByVal value As String)
            Me._nomUMPrincipal = value
        End Set
    End Property

    Public Property PrecioCompra() As Decimal
        Get
            Return Me._PrecioCompra
        End Get
        Set(ByVal value As Decimal)
            Me._PrecioCompra = value
        End Set
    End Property

    Public Property PorcentUtilFijo() As Decimal
        Get
            Return Me._PorcentUtilFijo
        End Get
        Set(ByVal value As Decimal)
            Me._PorcentUtilFijo = value
        End Set
    End Property

    Public Property PorcentUtilVar() As Decimal
        Get
            Return Me._PorcentUtilVar
        End Get
        Set(ByVal value As Decimal)
            Me._PorcentUtilVar = value
        End Set
    End Property

    Public Property escalarUtilidad() As Decimal
        Get
            Return Me._escalarUtilidad
        End Get
        Set(ByVal value As Decimal)
            Me._escalarUtilidad = value
        End Set
    End Property

    Public Property escalarValor() As Decimal
        Get
            Return Me._escalarValor
        End Get
        Set(ByVal value As Decimal)
            Me._escalarValor = value
        End Set
    End Property
End Class
