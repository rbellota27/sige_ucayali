﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Nagamine Molina, Jorge
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 30-Set-2009
'Hora    : 03:45 pm
'*************************************************
'****4 de febrero


Public Class ProductoTipoPVView
    Private _IdProducto As Integer
    Private _prod_Nombre As String
    Private _IdSubLInea As Integer
    Private _IdUnidadMedida As Integer
    Private _um_NombreCorto As String
    Private _pum_Equivalencia As Decimal
    Private _pum_UnidadPrincipal As Boolean
    Private _prod_PrecioCompra As Decimal
    Private _prod_Orden As Integer
    Private _PorcentUtilFijo As Decimal
    Private _PorcentUtilVar As Decimal
    Private _UtilValor As Decimal
    Private _PrecioFinal As Decimal
    Private _Estado As Boolean
    Private _NomMoneda As String
    Private _IdMoneda As Integer
    Private _IdMonedaPV As Integer
    Private _PVPublico As Decimal
    Private _IdMonedaPVPublico As Integer
    Private _NomMonedaPVPublico As String
    Private _CodigoProducto As String
    Private _CodigoAntiguo As String
    Private _CalOtraMoneda As Decimal
    Private _IdMonedaTiendaPrincipal As Integer
    Private _PrecioTiendaPricipal As Decimal
    Private _IdTiendaPrincipal As Integer


    Public Property IdTiendaPrincipal() As Integer
        Get
            Return _IdTiendaPrincipal
        End Get
        Set(ByVal value As Integer)
            _IdTiendaPrincipal = value
        End Set
    End Property

    Public Property PrecioTiendaPrincipal() As Decimal
        Get
            Return _PrecioTiendaPricipal
        End Get
        Set(ByVal value As Decimal)
            _PrecioTiendaPricipal = value
        End Set
    End Property

    Public Property IdMonedaTiendaPrincipal() As Integer
        Get
            Return _IdMonedaTiendaPrincipal
        End Get
        Set(ByVal value As Integer)
            _IdMonedaTiendaPrincipal = value
        End Set
    End Property

    Public Property CalOtraMoneda() As Decimal
        Get
            Return _CalOtraMoneda
        End Get
        Set(ByVal value As Decimal)
            _CalOtraMoneda = value
        End Set
    End Property

    Public Property CodigoAntiguo() As String
        Get
            Return Me._CodigoAntiguo
        End Get
        Set(ByVal value As String)
            Me._CodigoAntiguo = value
        End Set
    End Property

    Public Property CodigoProducto() As String
        Get
            Return Me._CodigoProducto
        End Get
        Set(ByVal value As String)
            Me._CodigoProducto = value
        End Set
    End Property


    Public Property PVPublico() As Decimal
        Get
            Return Me._PVPublico
        End Get
        Set(ByVal value As Decimal)
            Me._PVPublico = value
        End Set
    End Property
    Public Property Equivalencia() As Decimal
        Get
            Return Me._pum_Equivalencia
        End Get
        Set(ByVal value As Decimal)
            Me._pum_Equivalencia = value
        End Set
    End Property

    Public Property IdMonedaPVPublico() As Integer
        Get
            Return Me._IdMonedaPVPublico
        End Get
        Set(ByVal value As Integer)
            Me._IdMonedaPVPublico = value
        End Set
    End Property

    Public Property NomMonedaPVPublico() As String
        Get
            Return Me._NomMonedaPVPublico
        End Get
        Set(ByVal value As String)
            Me._NomMonedaPVPublico = value
        End Set
    End Property




    Public Property IdMonedaPV() As Integer
        Get
            Return Me._IdMonedaPV
        End Get
        Set(ByVal value As Integer)
            Me._IdMonedaPV = value
        End Set
    End Property

    Public Property NomMoneda() As String
        Get
            Return Me._NomMoneda
        End Get
        Set(ByVal value As String)
            Me._NomMoneda = value
        End Set
    End Property
    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property
    Public Property Estado() As Boolean
        Get
            Return Me._Estado
        End Get
        Set(ByVal value As Boolean)
            Me._Estado = value
        End Set
    End Property
    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property

    Public Property NomProducto() As String
        Get
            Return Me._prod_Nombre
        End Get
        Set(ByVal value As String)
            Me._prod_Nombre = value
        End Set
    End Property

    Public Property IdSubLInea() As Integer
        Get
            Return Me._IdSubLInea
        End Get
        Set(ByVal value As Integer)
            Me._IdSubLInea = value
        End Set
    End Property

    Public Property IdUMPrincipal() As Integer
        Get
            Return Me._IdUnidadMedida
        End Get
        Set(ByVal value As Integer)
            Me._IdUnidadMedida = value
        End Set
    End Property

    Public Property NomUMPrincipal() As String
        Get
            Return Me._um_NombreCorto
        End Get
        Set(ByVal value As String)
            Me._um_NombreCorto = value
        End Set
    End Property

    Public Property FlagUMPrincipal() As Boolean
        Get
            Return Me._pum_UnidadPrincipal
        End Get
        Set(ByVal value As Boolean)
            Me._pum_UnidadPrincipal = value
        End Set
    End Property

    Public Property PrecioCompra() As Decimal
        Get
            Return Me._prod_PrecioCompra
        End Get
        Set(ByVal value As Decimal)
            Me._prod_PrecioCompra = value
        End Set
    End Property

    Public Property NroOrden() As Integer
        Get
            Return Me._prod_Orden
        End Get
        Set(ByVal value As Integer)
            Me._prod_Orden = value
        End Set
    End Property

    Public Property PorcentUtilFijo() As Decimal
        Get
            Return Me._PorcentUtilFijo
        End Get
        Set(ByVal value As Decimal)
            Me._PorcentUtilFijo = value
        End Set
    End Property

    Public Property PorcentUtilVar() As Decimal
        Get
            Return Me._PorcentUtilVar
        End Get
        Set(ByVal value As Decimal)
            Me._PorcentUtilVar = value
        End Set
    End Property

    Public Property UtilValor() As Decimal
        Get
            Return Me._UtilValor
        End Get
        Set(ByVal value As Decimal)
            Me._UtilValor = value
        End Set
    End Property

    Public Property PrecioFinal() As Decimal
        Get
            Return Me._PrecioFinal
        End Get
        Set(ByVal value As Decimal)
            Me._PrecioFinal = value
        End Set
    End Property
End Class
