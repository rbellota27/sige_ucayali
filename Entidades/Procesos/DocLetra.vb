﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




'******************* LUNES 08 MARZO 2010 HORA 01_22 PM








Public Class DocLetra
    Inherits Entidades.Documento


    Private _Observaciones As String
    Private objLetraCambio As Entidades.LetraCambio
    Private _NomEstadoCancelacion As String
    Private _FechaPago As Date
    Private _DescDebitoCuenta As String
    Private _DescProtestado As String
    Private _DescRenegociado As String


    Private _lc_DebitoCuenta As Boolean
    Private _lc_Renegociado As Boolean
    Private _lc_Protestado As Boolean


    Private _MontoMovCuenta As Decimal
    Private _SaldoMovCuenta As Decimal

    Private objPersona_Cliente As Entidades.PersonaView
    Private objPersona_Aval As Entidades.PersonaView
    Private _ContAmortizaciones As Integer
    Private _IdMovCuenta As Integer


    Public Sub New()
        Me.objLetraCambio = New Entidades.LetraCambio
        Me.objPersona_Cliente = New Entidades.PersonaView
        Me.objPersona_Aval = New Entidades.PersonaView
    End Sub



    Public Property IdMovCuenta() As Integer
        Get
            Return _IdMovCuenta
        End Get
        Set(ByVal value As Integer)
            _IdMovCuenta = value
        End Set
    End Property


    Public Property ContAmortizaciones() As Integer
        Get
            Return Me._ContAmortizaciones
        End Get
        Set(ByVal value As Integer)
            Me._ContAmortizaciones = value
        End Set
    End Property

    Public Property MontoMovCuenta() As Decimal
        Get
            Return Me._MontoMovCuenta
        End Get
        Set(ByVal value As Decimal)
            Me._MontoMovCuenta = value
        End Set
    End Property

    Public Property SaldoMovCuenta() As Decimal
        Get
            Return Me._SaldoMovCuenta
        End Get
        Set(ByVal value As Decimal)
            Me._SaldoMovCuenta = value
        End Set
    End Property


    Public Property DebitoCuenta() As Boolean
        Get
            Return Me._lc_DebitoCuenta
        End Get
        Set(ByVal value As Boolean)
            Me._lc_DebitoCuenta = value
        End Set
    End Property

    Public Property Renegociado() As Boolean
        Get
            Return Me._lc_Renegociado
        End Get
        Set(ByVal value As Boolean)
            Me._lc_Renegociado = value
        End Set
    End Property

    Public Property Protestado() As Boolean
        Get
            Return Me._lc_Protestado
        End Get
        Set(ByVal value As Boolean)
            Me._lc_Protestado = value
        End Set
    End Property


    Public Property DescDebitoCuenta() As String
        Get
            Return Me._DescDebitoCuenta
        End Get
        Set(ByVal value As String)
            Me._DescDebitoCuenta = value
        End Set
    End Property

    Public Property DescProtestado() As String
        Get
            Return Me._DescProtestado
        End Get
        Set(ByVal value As String)
            Me._DescProtestado = value
        End Set
    End Property

    Public Property DescRenegociado() As String
        Get
            Return Me._DescRenegociado
        End Get
        Set(ByVal value As String)
            Me._DescRenegociado = value
        End Set
    End Property






    Public Property FechaPago() As Date
        Get
            Return Me._FechaPago
        End Get
        Set(ByVal value As Date)
            Me._FechaPago = value
        End Set
    End Property

    Public ReadOnly Property getFechaPagoText() As String
        Get

            If Me.FechaPago = Nothing Then
                Return ""
            Else
                Return Format(Me.FechaPago, "dd/MM/yyyy")
            End If


        End Get
    End Property


    Public Property NomEstadoCancelacion() As String
        Get
            Return Me._NomEstadoCancelacion
        End Get
        Set(ByVal value As String)
            Me._NomEstadoCancelacion = value
        End Set
    End Property














    Public ReadOnly Property getNroDocumento() As String
        Get
            Return MyBase.Serie + " - " + MyBase.Codigo
        End Get
    End Property





    Public Function getLetraCambio() As Entidades.LetraCambio
        Return Me.objLetraCambio
    End Function

    Public Function getPersona_Cliente() As Entidades.PersonaView
        Return Me.objPersona_Cliente
    End Function

    Public Function getPersona_Aval() As Entidades.PersonaView
        Return Me.objPersona_Aval
    End Function

    Public Property Observaciones() As String
        Get
            Return Me._Observaciones
        End Get
        Set(ByVal value As String)
            Me._Observaciones = value
        End Set
    End Property



End Class
