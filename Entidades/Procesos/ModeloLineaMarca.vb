﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class ModeloLineaMarca

    Public Sub New()
    End Sub

    Public Sub New(ByVal pid As Integer, ByVal pdescripcion As String)
        Me.idmodelo = pid
        Me.mod_nombre = pdescripcion
    End Sub

    Private _idmodelo As Integer
    Public Property idmodelo() As Integer
        Get
            Return _idmodelo
        End Get
        Set(ByVal value As Integer)
            _idmodelo = value
        End Set
    End Property
    Private _mod_nombre As String
    Public Property mod_nombre() As String
        Get
            Return _mod_nombre
        End Get
        Set(ByVal value As String)
            _mod_nombre = value
        End Set
    End Property
    Private _idlineamarca As Integer
    Public Property idlineamarca() As Integer
        Get
            Return _idlineamarca
        End Get
        Set(ByVal value As Integer)
            _idlineamarca = value
        End Set
    End Property
    Private _idlinea As Integer
    Public Property idlinea() As Integer
        Get
            Return _idlinea
        End Get
        Set(ByVal value As Integer)
            _idlinea = value
        End Set
    End Property
    Private _idmarca As Integer
    Public Property idmarca() As Integer
        Get
            Return _idmarca
        End Get
        Set(ByVal value As Integer)
            _idmarca = value
        End Set
    End Property


End Class
