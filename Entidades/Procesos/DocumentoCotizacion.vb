﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'**************** VIERNES 19 FEB 2010 HORA 02_58 PM



'*****************  MARTES 26 ENERO 2010 HORA 05_03 PM

Public Class DocumentoCotizacion
    Inherits Entidades.Documento

    Private objPersonaView As New Entidades.PersonaView
    Private objTipoAgente As New Entidades.TipoAgente
    Private objAnexo_Documento As New Entidades.Anexo_Documento
    Private objMaestroObra As New Entidades.PersonaView
    Private objImpuesto As New Entidades.Impuesto

    Private _IdDocumentoRef As Integer

    Public Property getObjImpuesto() As Entidades.Impuesto
        Get
            Return Me.objImpuesto
        End Get
        Set(ByVal value As Entidades.Impuesto)
            Me.objImpuesto = value
        End Set
    End Property

    Public Property IdDocumentoRef() As Integer
        Get
            Return Me._IdDocumentoRef
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumentoRef = value
        End Set
    End Property


    Public Property getObjMaestroObra() As Entidades.PersonaView
        Get
            Return Me.objMaestroObra
        End Get
        Set(ByVal value As Entidades.PersonaView)
            Me.objMaestroObra = value
        End Set
    End Property

    Public Property getObjPersonaView() As Entidades.PersonaView
        Get
            Return Me.objPersonaView
        End Get
        Set(ByVal value As Entidades.PersonaView)
            Me.objPersonaView = value
        End Set
    End Property

    Public Property getObjTipoAgente() As Entidades.TipoAgente
        Get
            Return Me.objTipoAgente
        End Get
        Set(ByVal value As Entidades.TipoAgente)
            Me.objTipoAgente = value
        End Set
    End Property

    Public Property getObjAnexoDocumento() As Entidades.Anexo_Documento
        Get
            Return Me.objAnexo_Documento
        End Get
        Set(ByVal value As Entidades.Anexo_Documento)
            Me.objAnexo_Documento = value
        End Set
    End Property

End Class
