﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class DocumentoCheque
    Inherits Documento
    Implements System.ICloneable

    Protected _Beneficiario As String
    Protected _AnexoDoc As Entidades.Anexo_Documento
    Protected _SerieCheque As Entidades.SerieChequeView
    Protected _MovBanco As Entidades.MovBanco
    Protected _AnexoMovBanco As Entidades.Anexo_MovBanco
    Protected _banco_nombre As String = String.Empty
    Protected _ctaBancaria_nombre As String = String.Empty
    Protected _nom_simbolo_nombre As String = String.Empty

    Public Property banco_nombre() As String
        Get
            Return _banco_nombre
        End Get
        Set(ByVal value As String)
            _banco_nombre = value
        End Set
    End Property

    Public Property ctaBancaria_nombre() As String
        Get
            Return _ctaBancaria_nombre
        End Get
        Set(ByVal value As String)
            _ctaBancaria_nombre = value
        End Set
    End Property

    Public Property nom_simbolo_nombre() As String
        Get
            Return _nom_simbolo_nombre
        End Get
        Set(ByVal value As String)
            _nom_simbolo_nombre = value
        End Set
    End Property

    Sub New()
        MyBase.new()
        _AnexoDoc = New Entidades.Anexo_Documento
        _SerieCheque = New Entidades.SerieChequeView
    End Sub

    Public Property ChequeNumero() As String
        Get
            Return Me.Codigo
        End Get
        Set(ByVal value As String)
            Me.Codigo = value
        End Set
    End Property
    'Me.Serie

    Public Property IdBeneficiario() As Integer
        Get
            Return Me.IdPersona
        End Get
        Set(ByVal value As Integer)
            Me.IdPersona = value
        End Set
    End Property

    Public Property Beneficiario() As String
        Get
            Return Me._Beneficiario
        End Get
        Set(ByVal value As String)
            Me._Beneficiario = value
        End Set
    End Property

    Public Property FechaACobrar() As Date
        Get
            Return Me.FechaAEntregar
        End Get
        Set(ByVal value As Date)
            Me.FechaAEntregar = value
        End Set
    End Property
    Public ReadOnly Property DescFechaACobrar() As String
        Get
            If Me.FechaACobrar = Nothing Then
                Return ""
            Else
                Return Format(Me.FechaACobrar, "dd/MM/yyyy")
            End If
        End Get
    End Property
    'Me.Fechaemision
    'Me.Fecharegistro
    Public ReadOnly Property DescFechaEmision() As String
        Get
            If Me.FechaEmision = Nothing Then
                Return ""
            Else
                Return Format(Me.FechaEmision, "dd/MM/yyyy")
            End If
        End Get
    End Property
    Public Property Monto() As Decimal
        Get
            Return Me.Total
        End Get
        Set(ByVal value As Decimal)
            Me.Total = value
            Me.TotalAPagar = value

        End Set
    End Property
    Public ReadOnly Property DescMonto() As String  'Retorna el número con 2 decimales
        Get
            Return Me.Monto.ToString("F")
        End Get
    End Property
    Public Property IdSerieCheque() As Integer
        Get
            Return Me._AnexoDoc.IdSerieCheque
        End Get
        Set(ByVal value As Integer)
            Me._AnexoDoc.IdSerieCheque = value
            Me.SerieCheque.Id = value
            Me.IdSerie = value  'Esto es usado a la hora de insertar un documento cheque. 
            'Es utilizado en las operaciones `pero no se graba este dato en la base de datos
            'Cuidado con ese Id Serie, este esta relacionado con la tabla Serie y no con la 
            'tabla SerieCheque, se recomienda eliminar esta linea de código
        End Set
    End Property

    Public ReadOnly Property MonedaSimbolo() As String
        Get
            Return Me.SerieCheque.mon_Simbolo
        End Get
    End Property

    Public ReadOnly Property Banco() As String
        Get
            Return Me.SerieCheque.Banco
        End Get
    End Property

    Public ReadOnly Property CuentaBancaria() As String
        Get
            Return Me.SerieCheque.CuentaBancaria
        End Get
    End Property

    Public Property AnexoDoc() As Entidades.Anexo_Documento
        Get
            Return _AnexoDoc
        End Get
        Set(ByVal value As Entidades.Anexo_Documento)
            Me._AnexoDoc = value
        End Set
    End Property

    Public Property SerieCheque() As Entidades.SerieChequeView
        Get
            Return _SerieCheque
        End Get
        Set(ByVal value As Entidades.SerieChequeView)
            Me._SerieCheque = value
        End Set
    End Property

    Public Function Clone() As Object Implements System.ICloneable.Clone
        Dim x As New DocumentoCheque

        'x = CType(Me.getClone, Entidades.DocumentoCheque)
        x.Id = Me.Id
        x.ChequeNumero = Me.ChequeNumero
        x.IdBeneficiario = Me.IdBeneficiario
        x.FechaACobrar = Me.FechaACobrar
        x.FechaEmision = Me.FechaEmision
        x.FechaRegistro = Me.FechaRegistro

        x.Serie = Me.Serie
        x.IdEstadoDoc = Me.IdEstadoDoc
        x.IdEstadoEntrega = Me.IdEstadoEntrega
        x.IdEstadoCancelacion = Me.IdEstadoCancelacion
        x.NomEstadoDocumento = Me.NomEstadoDocumento
        x.NomEstadoEntregado = Me.NomEstadoEntregado
        x.NomEstadoCancelacion = Me.NomEstadoCancelacion
        x.Monto = Me.Monto
        x.IdMoneda = Me.IdMoneda

        x.Beneficiario = Me.Beneficiario
        x.IdSerieCheque = Me.IdSerieCheque

        x.SerieCheque = Me.SerieCheque.Clone()
        x.AnexoDoc = Me.AnexoDoc.Clone()

        Return x
    End Function

    Public Overloads Function getClone() As DocumentoCheque
        Return CType(Clone(), DocumentoCheque)
    End Function

End Class
