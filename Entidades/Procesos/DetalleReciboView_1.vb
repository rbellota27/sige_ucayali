﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Nagamine Molina, Jorge
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 12-Octubre-2009
'Hora    : 01:10 pm
'*************************************************

Public Class DetalleReciboView_1
    Inherits Entidades.DetalleRecibo

    Private _ListaConcepto As List(Of Entidades.Concepto)
    Private _Moneda As String

    Public Property ListaConcepto() As List(Of Entidades.Concepto)
        Get
            Return Me._ListaConcepto
        End Get
        Set(ByVal value As List(Of Entidades.Concepto))
            Me._ListaConcepto = value
        End Set
    End Property



    Public Property Moneda() As String
        Get
            Return Me._Moneda
        End Get
        Set(ByVal value As String)
            Me._Moneda = value
        End Set
    End Property


End Class
