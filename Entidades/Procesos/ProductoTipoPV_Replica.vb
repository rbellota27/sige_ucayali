﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class ProductoTipoPV_Replica

    Private _IdTienda_Origen As Integer
    Private _IdTienda_Destino As Integer
    Private _IdLinea As Integer
    Private _IdSubLinea As Integer
    Private _IdTipoPV_Origen As Integer
    Private _IdTipoPV_Destino As Integer
    Private _PorcentPV As Decimal
    Private _addCostoFletexPeso As Boolean
    Private _IdProducto As Integer

    Public Property IdProducto() As Integer
        Get
            Return _IdProducto
        End Get
        Set(ByVal value As Integer)
            _IdProducto = value
        End Set
    End Property

    Public Property addCostoFletexPeso() As Boolean
        Get
            Return _addCostoFletexPeso
        End Get
        Set(ByVal value As Boolean)
            _addCostoFletexPeso = value
        End Set
    End Property

    Public Property IdTienda_Origen() As Integer
        Get
            Return Me._IdTienda_Origen
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda_Origen = value
        End Set
    End Property

    Public Property IdTienda_Destino() As Integer
        Get
            Return Me._IdTienda_Destino
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda_Destino = value
        End Set
    End Property

    Public Property IdLinea() As Integer
        Get
            Return Me._IdLinea
        End Get
        Set(ByVal value As Integer)
            Me._IdLinea = value
        End Set
    End Property

    Public Property IdSubLinea() As Integer
        Get
            Return Me._IdSubLinea
        End Get
        Set(ByVal value As Integer)
            Me._IdSubLinea = value
        End Set
    End Property

    Public Property IdTipoPV_Origen() As Integer
        Get
            Return Me._IdTipoPV_Origen
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoPV_Origen = value
        End Set
    End Property

    Public Property IdTipoPV_Destino() As Integer
        Get
            Return Me._IdTipoPV_Destino
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoPV_Destino = value
        End Set
    End Property

    Public Property PorcentPV() As Decimal
        Get
            Return Me._PorcentPV
        End Get
        Set(ByVal value As Decimal)
            Me._PorcentPV = value
        End Set
    End Property

End Class
