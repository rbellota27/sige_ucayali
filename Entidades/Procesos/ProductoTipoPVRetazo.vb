﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class ProductoTipoPVRetazo
    Private _IdProducto As Integer
    Private _prod_Nombre As String
    Private _IdSubLInea As Integer
    Private _IdUnidadMedida As Integer
    Private _um_NombreCorto As String
    Private _pum_retazo As Boolean
    Private _prod_PrecioCompra As Decimal
    Private _prod_Orden As Integer
    Private _pum_Equivalencia As Decimal
    Private _pum_PorcentRetazo As Decimal
    Private _PrecioCompraRetazo As Decimal
    Private _PrecioFinalUMPrincipal As Decimal
    Private _PorcentUtilFijo As Decimal
    Private _PorcentUtilVar As Decimal
    Private _UtilValor As Decimal
    Private _PrecioFinal As Decimal
    Private _Estado As String
    Private _NomUMPrincipal As String
    Private _IdMoneda As Integer
    Private _NomMoneda As String

    Private _IdMonedaPC_UMPrincipal As Integer

    Private _NomMonedaPC_UMPrincipal As String
    Private _NomMonedaPV_UMPrincipal As Decimal

    Private _PV_UMPrincipal As Decimal

    Public Property PV_UMPrincipal() As Decimal
        Get
            Return Me._PV_UMPrincipal
        End Get
        Set(ByVal value As Decimal)
            Me._PV_UMPrincipal = value
        End Set
    End Property


    Public Property NomMonedaPV_UMPrincipal() As Decimal
        Get
            Return Me._NomMonedaPV_UMPrincipal
        End Get
        Set(ByVal value As Decimal)
            Me._NomMonedaPV_UMPrincipal = value
        End Set
    End Property


    Public Property NomMonedaPC_UMPrincipal() As String
        Get
            Return Me._NomMonedaPC_UMPrincipal
        End Get
        Set(ByVal value As String)
            Me._NomMonedaPC_UMPrincipal = value
        End Set
    End Property

    Public Property IdMonedaPC_UMPrincipal() As Integer
        Get
            Return Me._IdMonedaPC_UMPrincipal
        End Get
        Set(ByVal value As Integer)
            Me._IdMonedaPC_UMPrincipal = value
        End Set
    End Property


    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property

    Public Property NomMoneda() As String
        Get
            Return Me._NomMoneda
        End Get
        Set(ByVal value As String)
            Me._NomMoneda = value
        End Set
    End Property

    Public ReadOnly Property FlagEstado() As Boolean
        Get
            If Me._Estado = "0" Then
                Return False
            Else
                Return True
            End If
        End Get
    End Property

    Public Property NomUMPrincipal() As String
        Get
            Return Me._NomUMPrincipal
        End Get
        Set(ByVal value As String)
            Me._NomUMPrincipal = value
        End Set
    End Property
    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property

    Public Property NomProducto() As String
        Get
            Return Me._prod_Nombre
        End Get
        Set(ByVal value As String)
            Me._prod_Nombre = value
        End Set
    End Property

    Public Property IdSubLInea() As Integer
        Get
            Return Me._IdSubLInea
        End Get
        Set(ByVal value As Integer)
            Me._IdSubLInea = value
        End Set
    End Property

    Public Property IdUnidadMedida() As Integer
        Get
            Return Me._IdUnidadMedida
        End Get
        Set(ByVal value As Integer)
            Me._IdUnidadMedida = value
        End Set
    End Property

    Public Property NomUnidadMedida() As String
        Get
            Return Me._um_NombreCorto
        End Get
        Set(ByVal value As String)
            Me._um_NombreCorto = value
        End Set
    End Property

    Public Property FlagRetazo() As Boolean
        Get
            Return Me._pum_retazo
        End Get
        Set(ByVal value As Boolean)
            Me._pum_retazo = value
        End Set
    End Property

    Public Property PrecioCompra() As Decimal
        Get
            Return Me._prod_PrecioCompra
        End Get
        Set(ByVal value As Decimal)
            Me._prod_PrecioCompra = value
        End Set
    End Property

    Public Property NroOrden() As Integer
        Get
            Return Me._prod_Orden
        End Get
        Set(ByVal value As Integer)
            Me._prod_Orden = value
        End Set
    End Property

    Public Property Equivalencia() As Decimal
        Get
            Return Me._pum_Equivalencia
        End Get
        Set(ByVal value As Decimal)
            Me._pum_Equivalencia = value
        End Set
    End Property

    Public Property PorcentAdicionalRetazo() As Decimal
        Get
            Return Me._pum_PorcentRetazo
        End Get
        Set(ByVal value As Decimal)
            Me._pum_PorcentRetazo = value
        End Set
    End Property

    Public Property PrecioCompraRetazo() As Decimal
        Get
            Return Me._PrecioCompraRetazo
        End Get
        Set(ByVal value As Decimal)
            Me._PrecioCompraRetazo = value
        End Set
    End Property

    Public Property PrecioFinalUMPrincipal() As Decimal
        Get
            Return Me._PrecioFinalUMPrincipal
        End Get
        Set(ByVal value As Decimal)
            Me._PrecioFinalUMPrincipal = value
        End Set
    End Property

    Public Property PorcentUtilFijo() As Decimal
        Get
            Return Me._PorcentUtilFijo
        End Get
        Set(ByVal value As Decimal)
            Me._PorcentUtilFijo = value
        End Set
    End Property

    Public Property PorcentUtilVar() As Decimal
        Get
            Return Me._PorcentUtilVar
        End Get
        Set(ByVal value As Decimal)
            Me._PorcentUtilVar = value
        End Set
    End Property

    Public Property UtilValor() As Decimal
        Get
            Return Me._UtilValor
        End Get
        Set(ByVal value As Decimal)
            Me._UtilValor = value
        End Set
    End Property

    Public Property PrecioFinal() As Decimal
        Get
            Return Me._PrecioFinal
        End Get
        Set(ByVal value As Decimal)
            Me._PrecioFinal = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._Estado
        End Get
        Set(ByVal value As String)
            Me._Estado = value
        End Set
    End Property
End Class
