﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class FinanciamientoOT
    Private _id As Integer
    Private _idpersona As Integer
    Private _idvehiculo As Integer
    Private _idbanco As Integer
    Private _iddocumento As Integer
    Private _monto As Decimal
    Private _fechadeposito As Date
    Private _numero As String
    Private _kilomentraje As Decimal

    Public Property kilometraje() As Decimal
        Get
            Return _kilomentraje
        End Get
        Set(ByVal value As Decimal)
            _kilomentraje = value
        End Set
    End Property

    Public Property id() As Integer
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            _id = value
        End Set
    End Property

    Public Property idPersona() As Integer
        Get
            Return _idpersona
        End Get
        Set(ByVal value As Integer)
            _idpersona = value
        End Set
    End Property

    Public Property idvehiculo() As Integer
        Get
            Return _idvehiculo
        End Get
        Set(ByVal value As Integer)
            _idvehiculo = value
        End Set
    End Property

    Public Property idbanco() As Integer
        Get
            Return _idbanco
        End Get
        Set(ByVal value As Integer)
            _idbanco = value
        End Set
    End Property

    Public Property iddocumento() As Integer
        Get
            Return _iddocumento
        End Get
        Set(ByVal value As Integer)
            _iddocumento = value
        End Set
    End Property

    Public Property monto() As Decimal
        Get
            Return _monto
        End Get
        Set(ByVal value As Decimal)
            _monto = value
        End Set
    End Property

    Public Property fechadeposito() As Date
        Get
            Return _fechadeposito
        End Get
        Set(ByVal value As Date)
            _fechadeposito = value
        End Set
    End Property


    Public Property numero() As String
        Get
            Return _numero
        End Get
        Set(ByVal value As String)
            _numero = value
        End Set
    End Property

End Class
