﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Nagamine Molina, Jorge
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 12-Octubre-2009
'Hora    : 01:10 pm
'*************************************************


'*************************************************
'Autor   : Nagamine Molina, Jorge
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 30-Set-2009
'Hora    : 03:15 pm
'*************************************************

Public Class DetalleDocumentoTomaInventario
    Inherits Entidades.DetalleDocumento


    Private _Faltante As Decimal
    Private _Sobrante As Decimal
    Private _CostoUnit As Decimal
    Private _CostoFaltante As Decimal
    Private _CostoSobrante As Decimal

    Private _CantidadTomaInv As Decimal
    Private _CantidadSistema As Decimal
    Private _DescEstado As String
    Private _Ajustado As String

    Private _CantidadAjuste As Decimal

    Public Property CantidadAjuste() As Decimal
        Get
            Return Me._CantidadAjuste
        End Get
        Set(ByVal value As Decimal)
            Me._CantidadAjuste = value
        End Set
    End Property




    Public Property CantidadTomaInv() As Decimal
        Get
            Return Me._CantidadTomaInv
        End Get
        Set(ByVal value As Decimal)
            Me._CantidadTomaInv = value
        End Set
    End Property

    Public Property CantidadSistema() As Decimal
        Get
            Return Me._CantidadSistema
        End Get
        Set(ByVal value As Decimal)
            Me._CantidadSistema = value
        End Set
    End Property

    Public Property DescEstado() As String
        Get
            Return Me._DescEstado
        End Get
        Set(ByVal value As String)
            Me._DescEstado = value
        End Set
    End Property

    Public Property Ajustado() As String
        Get
            Return Me._Ajustado
        End Get
        Set(ByVal value As String)
            Me._Ajustado = value
        End Set
    End Property

    Public Property Faltante() As Decimal
        Get
            Return Me._Faltante
        End Get
        Set(ByVal value As Decimal)
            Me._Faltante = value
        End Set
    End Property

    Public Property Sobrante() As Decimal
        Get
            Return Me._Sobrante
        End Get
        Set(ByVal value As Decimal)
            Me._Sobrante = value
        End Set
    End Property

    Public Property CostoUnit() As Decimal
        Get
            Return Me._CostoUnit
        End Get
        Set(ByVal value As Decimal)
            Me._CostoUnit = value
        End Set
    End Property

    Public Property CostoFaltante() As Decimal
        Get
            Return Me._CostoFaltante
        End Get
        Set(ByVal value As Decimal)
            Me._CostoFaltante = value
        End Set
    End Property

    Public Property CostoSobrante() As Decimal
        Get
            Return Me._CostoSobrante
        End Get
        Set(ByVal value As Decimal)
            Me._CostoSobrante = value
        End Set
    End Property



End Class
