﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class DocGuiaRemision
    Inherits Entidades.Documento

    Private _IdDocumentoRef As Integer
    Private _TipoDocumentoRef As String
    Private _SerieDocumentoRef As String
    Private _CodigoDocumentoRef As String
    Private _peso As Decimal = 0
    Private _distrito As String = String.Empty

    Public Property distrito() As String
        Get
            Return _distrito
        End Get
        Set(ByVal value As String)
            _distrito = value
        End Set
    End Property

    Public Property peso() As Decimal
        Get
            Return _peso
        End Get
        Set(ByVal value As Decimal)
            _peso = value
        End Set
    End Property

    Public Property IdDocumentoRef() As Integer
        Get
            Return Me._IdDocumentoRef
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumentoRef = value
        End Set
    End Property

    Public Property TipoDocumentoRef() As String
        Get
            Return Me._TipoDocumentoRef
        End Get
        Set(ByVal value As String)
            Me._TipoDocumentoRef = value
        End Set
    End Property

    Public Property SerieDocumentoRef() As String
        Get
            Return Me._SerieDocumentoRef
        End Get
        Set(ByVal value As String)
            Me._SerieDocumentoRef = value
        End Set
    End Property

    Public Property CodigoDocumentoRef() As String
        Get
            Return Me._CodigoDocumentoRef
        End Get
        Set(ByVal value As String)
            Me._CodigoDocumentoRef = value
        End Set
    End Property



End Class
