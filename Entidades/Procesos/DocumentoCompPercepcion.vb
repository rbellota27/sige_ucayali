﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class DocumentoCompPercepcion
    Inherits Entidades.Documento



    Private objTipoDocumento As Entidades.TipoDocumento
    Private _TotalSoles As Decimal
    Private _TotalDolares As Decimal
    Private _MonedaDestino As String
    Private _TotalAPagarEq As Decimal
    Private _NroDocumento As String
    Private _TipoCambio As Decimal
    Private _ContAbonos As Integer
    Private _DescPersona As String
    Private _Ruc As String
    Private _Dni As String

    Public Sub New()
        Me.objTipoDocumento = New Entidades.TipoDocumento
    End Sub



    Public Property ContAbonos() As Integer
        Get
            Return Me._ContAbonos
        End Get
        Set(ByVal value As Integer)
            Me._ContAbonos = value
        End Set
    End Property

    Public Property DescPersona() As String
        Get
            Return Me._DescPersona
        End Get
        Set(ByVal value As String)
            Me._DescPersona = value
        End Set
    End Property

    Public Property Ruc() As String
        Get
            Return Me._Ruc
        End Get
        Set(ByVal value As String)
            Me._Ruc = value
        End Set
    End Property

    Public Property Dni() As String
        Get
            Return Me._Dni
        End Get
        Set(ByVal value As String)
            Me._Dni = value
        End Set
    End Property


    Public Property NroDocumento() As String
        Get
            Return Me._NroDocumento
        End Get
        Set(ByVal value As String)
            Me._NroDocumento = value
        End Set
    End Property

    Public ReadOnly Property getTipoDocumento_CodSunat() As String
        Get
            Return Me.objTipoDocumento.CodigoSunat
        End Get
    End Property

    Public ReadOnly Property getTipoDocumento_DescripcionCorto() As String
        Get
            Return Me.objTipoDocumento.DescripcionCorto
        End Get
    End Property

    Public Property TipoCambio() As Decimal
        Get
            Return Me._TipoCambio
        End Get
        Set(ByVal value As Decimal)
            Me._TipoCambio = value
        End Set
    End Property



    Public Property TotalAPagarEq() As Decimal
        Get
            Return Me._TotalAPagarEq
        End Get
        Set(ByVal value As Decimal)
            Me._TotalAPagarEq = value
        End Set
    End Property


    Public Property MonedaDestino() As String
        Get
            Return Me._MonedaDestino
        End Get
        Set(ByVal value As String)
            Me._MonedaDestino = value
        End Set
    End Property


    Public Property TotalSoles() As Decimal
        Get
            Return Me._TotalSoles
        End Get
        Set(ByVal value As Decimal)
            Me._TotalSoles = value
        End Set
    End Property

    Public Property TotalDolares() As Decimal
        Get
            Return Me._TotalDolares
        End Get
        Set(ByVal value As Decimal)
            Me._TotalDolares = value
        End Set
    End Property

    Public Function getTipoDocumento() As Entidades.TipoDocumento

        Return Me.objTipoDocumento

    End Function



End Class
