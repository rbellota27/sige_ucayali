﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data
Public Class Centro_costo
    Private _IdCentroCosto As Integer
    Private _Cod_UniNeg As String
    Private _Cod_DepFunc As String
    Private _Cod_SubCodigo2 As String
    Private _Cod_SubCodigo3 As String
    Private _Cod_SubCodigo4 As String
    Private _strCod_UniNeg As String
    Private _strCod_DepFunc As String
    Private _strCod_SubCodigo2 As String
    Private _strCod_SubCodigo3 As String
    Private _strCod_SubCodigo4 As String
    Private _Nombre_CC As String
    Private _Estado_CC As Boolean
    Private _codigo As String
    Private _strcodigo As String
    Private _estado As Integer
    Private _Monto As Decimal

    Public Sub New()
    End Sub

    Public Property monto() As Decimal
        Get
            Return _Monto
        End Get
        Set(ByVal value As Decimal)
            _Monto = value
        End Set
    End Property

    Public Property codigo() As String
        Get
            Return _codigo
        End Get
        Set(ByVal value As String)
            _codigo = value
        End Set
    End Property

    Public Property Estado() As Integer
        Get
            Return _estado
        End Get
        Set(ByVal value As Integer)
            _estado = value
        End Set
    End Property

    Public Property IdCentroCosto() As Integer
        Get
            Return _IdCentroCosto
        End Get
        Set(ByVal value As Integer)
            _IdCentroCosto = value
        End Set
    End Property
    Public Property Cod_UniNeg() As String
        Get
            Return _Cod_UniNeg
        End Get
        Set(ByVal value As String)
            _Cod_UniNeg = value
        End Set
    End Property
    Public Property Cod_DepFunc() As String
        Get
            Return _Cod_DepFunc
        End Get
        Set(ByVal value As String)
            _Cod_DepFunc = value
        End Set
    End Property
    Public Property Cod_SubCodigo2() As String
        Get
            Return _Cod_SubCodigo2
        End Get
        Set(ByVal value As String)
            _Cod_SubCodigo2 = value
        End Set
    End Property
    Public Property Cod_SubCodigo3() As String
        Get
            Return _Cod_SubCodigo3
        End Get
        Set(ByVal value As String)
            _Cod_SubCodigo3 = value
        End Set
    End Property
    Public Property Cod_SubCodigo4() As String
        Get
            Return _Cod_SubCodigo4
        End Get
        Set(ByVal value As String)
            _Cod_SubCodigo4 = value
        End Set
    End Property
    Public Property Nombre_CC() As String
        Get
            Return _Nombre_CC
        End Get
        Set(ByVal value As String)
            _Nombre_CC = value
        End Set
    End Property
    Public Property Estado_CC() As Boolean
        Get
            Return _Estado_CC
        End Get
        Set(ByVal value As Boolean)
            _Estado_CC = value
        End Set
    End Property


    Public Property strCod_UniNeg() As String
        Get
            Return _strCod_UniNeg
        End Get
        Set(ByVal value As String)
            _strCod_UniNeg = value
        End Set
    End Property
    Public Property strCod_DepFunc() As String
        Get
            Return _strCod_DepFunc
        End Get
        Set(ByVal value As String)
            _strCod_DepFunc = value
        End Set
    End Property
    Public Property strCod_SubCodigo2() As String
        Get
            Return _strCod_SubCodigo2
        End Get
        Set(ByVal value As String)
            _strCod_SubCodigo2 = value
        End Set
    End Property
    Public Property strCod_SubCodigo3() As String
        Get
            Return _strCod_SubCodigo3
        End Get
        Set(ByVal value As String)
            _strCod_SubCodigo3 = value
        End Set
    End Property
    Public Property strCod_SubCodigo4() As String
        Get
            Return _strCod_SubCodigo4
        End Get
        Set(ByVal value As String)
            _strCod_SubCodigo4 = value
        End Set
    End Property

    Public ReadOnly Property strCodigo() As String
        Get
            Return Cod_UniNeg + Cod_DepFunc + Cod_SubCodigo2 + Cod_SubCodigo3 + Cod_SubCodigo4
        End Get
    End Property

End Class
