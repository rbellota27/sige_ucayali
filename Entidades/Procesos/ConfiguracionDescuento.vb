﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*****************  MARTES 26 ENERO 2010 HORA 05_03 PM


Public Class ConfiguracionDescuento

#Region "variables privadas"
    Private _IdDescuento As Integer
    Private _IdMedioPago As Integer
    Private _IdPerfil As Integer
    Private _IdTipoPV As Integer
    Private _IdCondicionPago As Integer
    Private _MedioPago As String
    Private _Perfil As String
    Private _TipoPrecioV As String
    Private _CondicionPago As String
    Private _desc_Porcentaje As Decimal
    Private _desc_FechaRegistro As Date
    Private _desc_Estado As String
    Private _desc_PrecioBaseDscto As String
#End Region

#Region "propiedades publicas"
    Public Property Desc_PrecioBaseDscto() As String
        Get
            Return _desc_PrecioBaseDscto
        End Get
        Set(ByVal value As String)
            _desc_PrecioBaseDscto = value
        End Set
    End Property
    'Public ReadOnly Property PrecioBaseDscto() As String
    '    Get
    '        If Desc_PrecioBaseDscto = "PL" Then
    '            Desc_PrecioBaseDscto = "Precio Lista"
    '        End If
    '        If Desc_PrecioBaseDscto = "PC" Then
    '            Desc_PrecioBaseDscto = "Precio Comercial"
    '        End If
    '        Return Desc_PrecioBaseDscto
    '    End Get
    'End Property



    Public Property IdDescuento() As Integer
        Get
            Return _IdDescuento
        End Get
        Set(ByVal value As Integer)
            _IdDescuento = value
        End Set
    End Property
    Public Property IdMedioPago() As Integer
        Get
            Return _IdMedioPago
        End Get
        Set(ByVal value As Integer)
            _IdMedioPago = value
        End Set
    End Property
    Public Property IdPerfil() As Integer
        Get
            Return _IdPerfil
        End Get
        Set(ByVal value As Integer)
            _IdPerfil = value
        End Set
    End Property
    Public Property IdTipoPV() As Integer
        Get
            Return _IdTipoPV
        End Get
        Set(ByVal value As Integer)
            _IdTipoPV = value
        End Set
    End Property
    Public Property IdCondicionPago() As Integer
        Get
            Return _IdCondicionPago
        End Get
        Set(ByVal value As Integer)
            _IdCondicionPago = value
        End Set
    End Property
    Public Property MedioPago() As String
        Get
            Return _MedioPago
        End Get
        Set(ByVal value As String)
            _MedioPago = value
        End Set
    End Property
    Public Property Perfil() As String
        Get
            Return _Perfil
        End Get
        Set(ByVal value As String)
            _Perfil = value
        End Set
    End Property
    Public Property TipoPrecioV() As String
        Get
            Return _TipoPrecioV
        End Get
        Set(ByVal value As String)
            _TipoPrecioV = value
        End Set
    End Property
    Public Property CondicionPago() As String
        Get
            Return _CondicionPago
        End Get
        Set(ByVal value As String)
            _CondicionPago = value
        End Set
    End Property
    Public Property desc_Porcentaje() As Decimal
        Get
            Return _desc_Porcentaje
        End Get
        Set(ByVal value As Decimal)
            _desc_Porcentaje = value
        End Set
    End Property
    Public Property desc_FechaRegistro() As Date
        Get
            Return _desc_FechaRegistro
        End Get
        Set(ByVal value As Date)
            _desc_FechaRegistro = value
        End Set
    End Property

    Public Property desc_Estado() As String
        Get
            Return _desc_Estado
        End Get
        Set(ByVal value As String)
            _desc_Estado = value
        End Set
    End Property


#End Region


End Class
