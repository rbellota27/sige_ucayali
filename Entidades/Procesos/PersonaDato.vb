﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class PersonaDato

    Private _IdPersona As Integer
    Private _Descripcion As String
    Private _Str_IdTienda As String
    Private _Str_Tienda As String
    Private _Str_IdRol As String
    Private _Str_Rol As String
    Private _Str_IdPerfil As String
    Private _Str_Perfil As String
    Private _Str_IdTipoTelefono As String
    Private _Str_Telefono As String
    Private _IdCargo As String
    Private _IdEmpresa As Integer
    Private _IdTienda As Integer
    Private _IdRol As Integer
    Private _IdPerfil As Integer
    Private _IdTipoTelefono As Integer
    Private _Cargo As String


    Public Property IdPersona() As Integer
        Get
            Return _IdPersona
        End Get
        Set(ByVal value As Integer)
            _IdPersona = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property

    Public Property IdEmpresa() As Integer
        Get
            Return _IdEmpresa
        End Get
        Set(ByVal value As Integer)
            _IdEmpresa = value
        End Set
    End Property
    Public Property IdTienda() As Integer
        Get
            Return _IdTienda
        End Get
        Set(ByVal value As Integer)
            _IdTienda = value
        End Set
    End Property
    Public Property IdRol() As Integer
        Get
            Return _IdRol
        End Get
        Set(ByVal value As Integer)
            _IdRol = value
        End Set
    End Property
    Public Property IdPerfil() As Integer
        Get
            Return _IdPerfil
        End Get
        Set(ByVal value As Integer)
            _IdPerfil = value
        End Set
    End Property
    Public Property IdTipoTelefono() As Integer
        Get
            Return _IdTipoTelefono
        End Get
        Set(ByVal value As Integer)
            _IdTipoTelefono = value
        End Set
    End Property

    Public Property Str_IdTienda() As String
        Get
            Return _Str_IdTienda
        End Get
        Set(ByVal value As String)
            _Str_IdTienda = value
        End Set
    End Property
    Public Property Str_Tienda() As String
        Get
            Return _Str_Tienda
        End Get
        Set(ByVal value As String)
            _Str_Tienda = value
        End Set
    End Property
    Public ReadOnly Property ListaTienda() As List(Of Entidades.Tienda)
        Get
            ListaTienda = New List(Of Entidades.Tienda)
            Dim ObjTienda As Entidades.Tienda
            If Str_IdTipoTelefono <> "" And Str_Telefono <> "" Then
                Dim IdTienda() As String = Str_IdTienda.Split(",")
                Dim Tienda() As String = Str_Tienda.Split(",")
                For i As Integer = 0 To IdTienda.Length - 1
                    ObjTienda = New Entidades.Tienda
                    ObjTienda.Id = IdTienda(i)
                    ObjTienda.Nombre = Tienda(i)
                    ListaTienda.Add(ObjTienda)
                Next
            End If
            Return ListaTienda
        End Get
    End Property
    Public Property Str_IdRol() As String
        Get
            Return _Str_IdRol
        End Get
        Set(ByVal value As String)
            _Str_IdRol = value
        End Set
    End Property
    Public Property Str_Rol() As String
        Get
            Return _Str_Rol
        End Get
        Set(ByVal value As String)
            _Str_Rol = value
        End Set
    End Property
    Public ReadOnly Property ListaRol() As List(Of Entidades.Rol)
        Get
            ListaRol = New List(Of Entidades.Rol)
            Dim ObjRol As Entidades.Rol
            If Str_IdTipoTelefono <> "" And Str_Telefono <> "" Then
                Dim IdRol() As String = Str_IdRol.Split(",")
                Dim Rol() As String = Str_Rol.Split(",")
                For i As Integer = 0 To IdRol.Length - 1
                    ObjRol = New Entidades.Rol
                    ObjRol.Id = IdRol(i)
                    ObjRol.Nombre = Rol(i)
                    ListaRol.Add(ObjRol)
                Next
            End If
            Return ListaRol
        End Get
    End Property
    Public Property Str_IdPerfil() As String
        Get
            Return _Str_IdPerfil
        End Get
        Set(ByVal value As String)
            _Str_IdPerfil = value
        End Set
    End Property
    Public Property Str_Perfil() As String
        Get
            Return _Str_Perfil
        End Get
        Set(ByVal value As String)
            _Str_Perfil = value
        End Set
    End Property
    Public ReadOnly Property ListaPerfil() As List(Of Entidades.Perfil)
        Get
            ListaPerfil = New List(Of Entidades.Perfil)
            Dim ObjPerfil As Entidades.Perfil
            If Str_IdTipoTelefono <> "" And Str_Telefono <> "" Then
                Dim IdPerfil() As String = Str_IdPerfil.Split(",")
                Dim Perfil() As String = Str_Perfil.Split(",")
                For i As Integer = 0 To IdPerfil.Length - 1
                    ObjPerfil = New Entidades.Perfil
                    ObjPerfil.IdPerfil = IdPerfil(i)
                    ObjPerfil.NomPerfil = Perfil(i)
                    ListaPerfil.Add(ObjPerfil)
                Next
            End If
            Return ListaPerfil
        End Get
    End Property

    Public Property Str_IdTipoTelefono() As String
        Get
            Return _Str_IdTipoTelefono
        End Get
        Set(ByVal value As String)
            _Str_IdTipoTelefono = value
        End Set
    End Property

    Public Property Str_Telefono() As String
        Get
            Return _Str_Telefono
        End Get
        Set(ByVal value As String)
            _Str_Telefono = value
        End Set
    End Property

    Public ReadOnly Property ListaTelefono() As List(Of Entidades.Telefono)
        Get
            ListaTelefono = New List(Of Entidades.Telefono)
            Dim ObjTelefono As Entidades.Telefono
            If Str_IdTipoTelefono <> "" And Str_Telefono <> "" Then
                Dim IdTipoTelefono() As String = Str_IdTipoTelefono.Split(",")
                Dim Telefono() As String = Str_Telefono.Split(",")
                For i As Integer = 0 To IdTipoTelefono.Length - 1
                    ObjTelefono = New Entidades.Telefono
                    ObjTelefono.IdTipoTelefono = IdTipoTelefono(i)
                    ObjTelefono.Numero = Telefono(i)
                    ListaTelefono.Add(ObjTelefono)
                Next
            End If
            Return ListaTelefono
        End Get
    End Property
    Public Property IdCargo() As String
        Get
            Return _IdCargo
        End Get
        Set(ByVal value As String)
            _IdCargo = value
        End Set
    End Property
    Public Property Cargo() As String
        Get
            Return _Cargo
        End Get
        Set(ByVal value As String)
            _Cargo = value
        End Set
    End Property

End Class
