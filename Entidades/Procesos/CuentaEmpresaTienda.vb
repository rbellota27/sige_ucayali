﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el 1re párrafo,  va a ser detectado y enjuiciado por la empresa DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class CuentaEmpresaTienda
    Private _IdEmpresa As Integer
    Private _IdTienda As Integer
    Private _IdMoneda As Integer
    Private _cet_MontoMax As Decimal
    Private _IdSupervisor As Integer
    Private _cet_Estado As Boolean
    Private _cet_NroMaxCuentas As Integer
    Private _cet_Saldo As Decimal
    Private _cstrMoneda As String
    Private _cstrEmpresa As String
    Private _cstrTienda As String
    Private _cstrSupervisor As String
    Private _Tipo As Integer
    Private _montoAnt As Decimal
    Private _saldoAnt As Decimal
    Private _CuentasActivas As Integer
    Private _CargoMaxCtasxTienda As Decimal

    Public Property CargoMaxCtasxTienda() As Decimal
        Get
            Return _CargoMaxCtasxTienda
        End Get
        Set(ByVal value As Decimal)
            _CargoMaxCtasxTienda = value
        End Set
    End Property


    Public Property saldoAnt() As Decimal
        Get
            Return _saldoAnt
        End Get
        Set(ByVal value As Decimal)
            _saldoAnt = value
        End Set
    End Property

    Public Property montoAnt() As Decimal
        Get
            Return _montoAnt
        End Get
        Set(ByVal value As Decimal)
            _montoAnt = value
        End Set
    End Property

    Public Property Tipo() As Integer
        Get
            Return _Tipo
        End Get
        Set(ByVal value As Integer)
            _Tipo = value
        End Set
    End Property

    Public Property cstrSupervisor() As String
        Get
            Return _cstrSupervisor
        End Get
        Set(ByVal value As String)
            _cstrSupervisor = value
        End Set
    End Property
    Public Property cstrEmpresa() As String
        Get
            Return _cstrEmpresa
        End Get
        Set(ByVal value As String)
            _cstrEmpresa = value
        End Set
    End Property
    Public Property cstrTienda() As String
        Get
            Return _cstrTienda
        End Get
        Set(ByVal value As String)
            _cstrTienda = value
        End Set
    End Property

    Public Property cstrMoneda() As String
        Get
            Return _cstrMoneda
        End Get
        Set(ByVal value As String)
            _cstrMoneda = value
        End Set
    End Property

    Public Property IdEmpresa() As Integer
        Get
            Return _IdEmpresa
        End Get
        Set(ByVal value As Integer)
            _IdEmpresa = value
        End Set
    End Property
    Public Property IdTienda() As Integer
        Get
            Return _IdTienda
        End Get
        Set(ByVal value As Integer)
            _IdTienda = value
        End Set
    End Property
    Public Property IdMoneda() As Integer
        Get
            Return _IdMoneda
        End Get
        Set(ByVal value As Integer)
            _IdMoneda = value
        End Set
    End Property
    Public Property cet_MontoMax() As Decimal
        Get
            Return _cet_MontoMax
        End Get
        Set(ByVal value As Decimal)
            _cet_MontoMax = value
        End Set
    End Property
    Public Property IdSupervisor() As Integer
        Get
            Return _IdSupervisor
        End Get
        Set(ByVal value As Integer)
            _IdSupervisor = value
        End Set
    End Property
    Public Property cet_Estado() As Boolean
        Get
            Return _cet_Estado
        End Get
        Set(ByVal value As Boolean)
            _cet_Estado = value
        End Set
    End Property
    Public Property cet_NroMaxCuentas() As Integer
        Get
            Return _cet_NroMaxCuentas
        End Get
        Set(ByVal value As Integer)
            _cet_NroMaxCuentas = value
        End Set
    End Property
    Public Property cet_Saldo() As Decimal
        Get
            Return _cet_Saldo
        End Get
        Set(ByVal value As Decimal)
            _cet_Saldo = value
        End Set
    End Property

    Public Property CuentasActivas() As Integer
        Get
            Return _CuentasActivas
        End Get
        Set(ByVal value As Integer)
            _CuentasActivas = value
        End Set
    End Property

    Public ReadOnly Property CuentasDisponibles() As Integer
        Get
            Return _cet_NroMaxCuentas - _CuentasActivas
        End Get
    End Property

End Class
