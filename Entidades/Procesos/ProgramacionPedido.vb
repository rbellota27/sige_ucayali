﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class ProgramacionPedido

#Region "variables privadas"
    Private _IdYear As Integer
    Private _IdSemana As Integer
    Private _cal_Mes As Integer
    Private _cal_FechaIni As Date
    Private _cal_FechaFin As Date
    Private _cal_DiasUtil As Integer
    Private _cal_Estado As Boolean
    Private _IdDocumento As Integer
    Private _CadIdsemana As String
#End Region

#Region "propiedades Publicas"

    Public Property CadIdsemana() As String
        Get
            Return _CadIdsemana
        End Get
        Set(ByVal value As String)
            _CadIdsemana = value
        End Set
    End Property

    Public Property IdDocumento() As Integer
        Get
            Return _IdDocumento
        End Get
        Set(ByVal value As Integer)
            _IdDocumento = value
        End Set
    End Property
    Public Property IdYear() As Integer
        Get
            Return _IdYear
        End Get
        Set(ByVal value As Integer)
            _IdYear = value
        End Set
    End Property
    Public Property IdSemana() As Integer
        Get
            Return _IdSemana
        End Get
        Set(ByVal value As Integer)
            _IdSemana = value
        End Set
    End Property
    Public Property cal_Mes() As Integer
        Get
            Return _cal_Mes
        End Get
        Set(ByVal value As Integer)
            _cal_Mes = value
        End Set
    End Property
    Public Property cal_FechaIni() As Date
        Get
            Return _cal_FechaIni
        End Get
        Set(ByVal value As Date)
            _cal_FechaIni = value
        End Set
    End Property
    Public Property cal_FechaFin() As Date
        Get
            Return _cal_FechaFin
        End Get
        Set(ByVal value As Date)
            _cal_FechaFin = value
        End Set
    End Property
    Public Property cal_DiasUtil() As Integer
        Get
            Return _cal_DiasUtil
        End Get
        Set(ByVal value As Integer)
            _cal_DiasUtil = value
        End Set
    End Property
    Public Property cal_Estado() As Boolean
        Get
            Return _cal_Estado
        End Get
        Set(ByVal value As Boolean)
            _cal_Estado = value
        End Set
    End Property

    Public ReadOnly Property strEstado() As String
        Get
            If cal_Estado = True Then
                Return "Activo"
            Else
                Return "Inactivo"
            End If
        End Get
    End Property


#End Region

End Class
