﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'LUNES 01 DE FEBRERO
Public Class Linea_Area
    Private _IdLinea As Integer
    Private _IdTipoExistencia As Integer
    Private _IdArea As Integer
    Private _LAEstado As Boolean
    Private _objArea As Object

    Public Property IdLinea() As Integer
        Get
            Return _IdLinea
        End Get
        Set(ByVal value As Integer)
            _IdLinea = value
        End Set
    End Property
    Public Property IdTipoExistencia() As Integer
        Get
            Return _IdTipoExistencia
        End Get
        Set(ByVal value As Integer)
            _IdTipoExistencia = value
        End Set
    End Property
    Public Property IdArea() As Integer
        Get
            Return _IdArea
        End Get
        Set(ByVal value As Integer)
            _IdArea = value
        End Set
    End Property
    Public Property LAEstado() As Boolean
        Get
            Return _LAEstado
        End Get
        Set(ByVal value As Boolean)
            _LAEstado = value
        End Set
    End Property

    Public Property objArea() As Object
        Get
            Return _objArea
        End Get
        Set(ByVal value As Object)
            _objArea = value
        End Set
    End Property

End Class
