﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class ProveedorLinea
    Private _IdProveedor As Integer
    Private _NomProveedor As String
    Private _IdLinea As Integer
    Private _IdTipoExistencia As Integer
    Private _plEstado As Boolean
    Private _plOrden As Integer
    Public Sub New()
    End Sub
    Public Sub New(ByVal idproveedor As Integer, ByVal nomproveedor As String, ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal estado As Boolean, ByVal orden As Integer)
        _IdProveedor = idproveedor
        _NomProveedor = nomproveedor
        _IdLinea = idlinea
        _IdTipoExistencia = idtipoexistencia
        _plEstado = estado
        _plOrden = orden
    End Sub
    Public Sub New(ByVal idproveedor As Integer, ByVal nomproveedor As String, ByVal estado As Boolean)
        _IdProveedor = idproveedor
        _NomProveedor = nomproveedor
        _plEstado = estado
    End Sub
    Public Property IdProveedor() As Integer
        Get
            Return _IdProveedor
        End Get
        Set(ByVal value As Integer)
            _IdProveedor = value
        End Set
    End Property
    Public Property NomProveedor() As String
        Get
            Return _NomProveedor
        End Get
        Set(ByVal value As String)
            _NomProveedor = value
        End Set
    End Property
    Public Property IdLinea() As Integer
        Get
            Return _IdLinea
        End Get
        Set(ByVal value As Integer)
            _IdLinea = value
        End Set
    End Property
    Public Property IdTipoExistencia() As Integer
        Get
            Return _IdTipoExistencia
        End Get
        Set(ByVal value As Integer)
            _IdTipoExistencia = value
        End Set
    End Property
    Public Property Estado() As Boolean
        Get
            Return _plEstado
        End Get
        Set(ByVal value As Boolean)
            _plEstado = value
        End Set
    End Property
    Public Property Orden() As Integer
        Get
            Return _plOrden
        End Get
        Set(ByVal value As Integer)
            _plOrden = value
        End Set
    End Property
End Class
