﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class ConfiguracionCorreo

    Private _IdTienda As Integer
    Private _IdEmpresa As Integer
    Private _IdConfiguracionCorreo As Integer
    Private _PuertoServidorCorreo As Integer
    Private _servidorCorreo As String
    Private _HostInteligente As String
    Private _CuentaCorreo As String
    Private _Clave As String
    Private _UsarCredencial As Boolean
    Private _Estado As Boolean
    Private _Ver_Estado As Integer
    Private _NomTienda As String
    Private _NomEmpresa As String

    Public Property NomTienda() As String
        Get
            Return _NomTienda
        End Get
        Set(ByVal value As String)
            _NomTienda = value
        End Set
    End Property

    Public Property NomEmpresa() As String
        Get
            Return _NomEmpresa
        End Get
        Set(ByVal value As String)
            _NomEmpresa = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property

    Public Property IdConfiguracionCorreo() As Integer
        Get
            Return _IdConfiguracionCorreo
        End Get
        Set(ByVal value As Integer)
            _IdConfiguracionCorreo = value
        End Set
    End Property
    Public Property servidorCorreo() As String
        Get
            Return _servidorCorreo
        End Get
        Set(ByVal value As String)
            _servidorCorreo = value
        End Set
    End Property
    Public Property PuertoServidorCorreo() As Integer
        Get
            Return _PuertoServidorCorreo
        End Get
        Set(ByVal value As Integer)
            _PuertoServidorCorreo = value
        End Set
    End Property
    Public Property HostInteligente() As String
        Get
            Return _HostInteligente
        End Get
        Set(ByVal value As String)
            _HostInteligente = value
        End Set
    End Property
    Public Property UsarCredencial() As Boolean
        Get
            Return _UsarCredencial
        End Get
        Set(ByVal value As Boolean)
            _UsarCredencial = value
        End Set
    End Property
    Public Property CuentaCorreo() As String
        Get
            Return _CuentaCorreo
        End Get
        Set(ByVal value As String)
            _CuentaCorreo = value
        End Set
    End Property
    Public Property Clave() As String
        Get
            Return _Clave
        End Get
        Set(ByVal value As String)
            _Clave = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return _Estado
        End Get
        Set(ByVal value As Boolean)
            _Estado = value
        End Set
    End Property

    Public Property Ver_Estado() As Integer
        Get
            Return _Ver_Estado
        End Get
        Set(ByVal value As Integer)
            _Ver_Estado = value
        End Set
    End Property

End Class
