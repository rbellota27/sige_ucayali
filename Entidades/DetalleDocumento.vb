'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class DetalleDocumento
    Private _descuentoOriginal As Decimal = 0
    Private _nroContador As Integer = 0
    Private _idTono As Integer
    Private _IdTipoOperacion As Integer
    Private _IdDetalleDocumento As Integer
    Private _IdDocumento As Integer
    Private _IdProducto As Integer
    Private _IdUnidadMedida As Integer
    Private _IdUMold As Integer
    Private _dc_Cantidad As Decimal
    Private _dc_CantxAtender As Decimal
    Private _dc_UMedida As String
    Private _dc_PrecioSD As Decimal
    Private _dc_Descuento As Decimal
    Private _dc_PrecioCD As Decimal
    Private _dc_Importe As Decimal
    Private _dc_Utilidad As Decimal
    Private _dc_TasaPercepcion As Decimal
    Private _dc_TasaDetraccion As Decimal
    Private _dc_Peso As Decimal
    Private _dc_UmPeso As String
    Private _nomProducto As String
    Private _CantADespachar As Decimal
    Private _IdAlmacen As Integer
    Private _IdUnidadMedidaPrincipal As Integer
    Private _IdDetalleAfecto As Integer
    Private _DetalleGlosa As String
    Private _CantADespacharOriginal As Decimal
    Private _Factor As Integer
    Private _IdSupervisor As Integer
    Private _PorcentDctoMaximo As Decimal
    Private _listaUM_Venta As List(Of Entidades.UnidadMedida)
    Private _ListaUM As List(Of Entidades.ProductoUMView)
    Private _Tono As List(Of Entidades.be_tonoXProducto)
    Private _listaSector As List(Of Entidades.be_Sector)
    Private _idSector As Integer = 0
    Private _cantidadTono As Decimal
    Private _StockDisponibleN As Decimal
    Private _PrecioLista As Decimal
    Private _PorcentDcto As Decimal
    Private _PorcentPercepcion As Decimal
    Private _VolumenVentaMin As Decimal
    Private _IdTienda As Integer
    Private _DctoValor_MaximoxPVenta As Decimal
    Private _DctoPorcent_MaximoxPVenta As Decimal
    Private _IdTipoPV As Integer
    Private _CantidadxAtenderText As String
    Private _cadenaUM As String
    Private _IdDocumentoRef As Integer
    Private _cantAprobada As Decimal
    Private _cantSolicitada As Decimal
    Private _Stock As Decimal
    Private _IdTipoDocumento As Integer
    Private _CostoMovIngreso As Decimal
    Private _CodigoProducto As String
    Private _pvComercial As Decimal
    Private _PrecioBaseDcto As String
    Private _CantidadDetalleAfecto As Decimal
    Private _Kit As Boolean
    Private _IdKit As Integer
    Private _ComponenteKit As Boolean
    Private _CantidadTransito As Decimal
    Private _Moneda As String
    Private _IdCampania As Integer
    Private _ExisteCampania_Producto As Boolean
    Private _IdUnidadMedida_Peso As Integer
    Private _listaUnidadMedida_Peso As List(Of Entidades.MagnitudUnidad)
    Private _Equivalencia As Decimal
    Private _Equivalencia1 As Decimal
    Private _dc_CantidadTransito, _dc_PrecioSinIGV, _dc_Descuento1, _dc_Descuento2, _dc_Descuento3 As Decimal
    Private _prod_CodigoBarras As String
    Private _desconcepto As String
    Private _montoconcepto As Decimal
    Private _IdProductoAux As Integer
    Private _IdCampaniaDetalle As Integer
    Private _kit_Cantidad_Comp As Decimal
    Private _UnidadMedidaPrincipal As String
    Private _UM1 As String
    Private _Equivalencia2 As Decimal
    Private _UM2 As String
    Private _StockDisponible As Decimal
    Private _StockComprometido As Decimal
    Private _CantMin As Decimal
    Private _DsctoOriginal As Decimal
    Private _NroDocumento As String
    Private _prod_NombreKit As String
    Private _CaxAtender As Decimal
    Private _StockTotalxAlmacen As Decimal
    Private _IdTipoCliente As Integer
    Private _UMProducto As String
    Private _UMDescCorta As String

    Public Property descuentoOriginal() As Decimal
        Get
            Return _descuentoOriginal
        End Get
        Set(ByVal value As Decimal)
            _descuentoOriginal = value
        End Set
    End Property

    Public Property nroContador() As Integer
        Get
            Return _nroContador
        End Get
        Set(ByVal value As Integer)
            _nroContador = value
        End Set
    End Property
    Public Property IdTipoCliente() As Integer
        Get
            Return _IdTipoCliente
        End Get
        Set(ByVal value As Integer)
            _IdTipoCliente = value
        End Set
    End Property
    Public Property StockTotalxAlmacen() As Decimal
        Get
            Return _StockTotalxAlmacen
        End Get
        Set(ByVal value As Decimal)
            _StockTotalxAlmacen = value
        End Set
    End Property
    Public Property CaxAtender() As Decimal
        Get
            Return _CaxAtender
        End Get
        Set(ByVal value As Decimal)
            _CaxAtender = value
        End Set
    End Property
    Public Property ProdNombreKit() As String
        Get
            Return _prod_NombreKit
        End Get
        Set(ByVal value As String)
            _prod_NombreKit = value
        End Set
    End Property

    Public Property DsctoOriginal() As Decimal
        Get
            Return _DsctoOriginal
        End Get
        Set(ByVal value As Decimal)
            _DsctoOriginal = value
        End Set
    End Property


    Public Property CantMin() As Decimal
        Get
            Return _CantMin
        End Get
        Set(ByVal value As Decimal)
            _CantMin = value
        End Set
    End Property

    Public Property StockDisponible() As Decimal
        Get
            Return _StockDisponible
        End Get
        Set(ByVal value As Decimal)
            _StockDisponible = value
        End Set
    End Property
    Public Property StockComprometido() As Decimal
        Get
            Return _StockComprometido
        End Get
        Set(ByVal value As Decimal)
            _StockComprometido = value
        End Set
    End Property

    Public Property UM2() As String

        Get
            Return _UM2
        End Get
        Set(ByVal value As String)
            _UM2 = value
        End Set
    End Property

    Public Property UM1() As String
        Get
            Return _UM1
        End Get
        Set(ByVal value As String)
            _UM1 = value
        End Set
    End Property

    Public Property kit_Cantidad_Comp() As Decimal
        Get
            Return _kit_Cantidad_Comp
        End Get
        Set(ByVal value As Decimal)
            _kit_Cantidad_Comp = value
        End Set
    End Property

    Public Property IdCampaniaDetalle() As Integer
        Get
            Return _IdCampaniaDetalle
        End Get
        Set(ByVal value As Integer)
            _IdCampaniaDetalle = value
        End Set
    End Property

    Public Property IdProductoAux() As Integer
        Get
            Return _IdProductoAux
        End Get
        Set(ByVal value As Integer)
            _IdProductoAux = value
        End Set
    End Property

    Public Property IdTipoOperacion() As String
        Get
            Return _IdTipoOperacion
        End Get
        Set(ByVal value As String)
            _IdTipoOperacion = value
        End Set
    End Property


    Public Property NroDocumento() As String
        Get
            Return _NroDocumento
        End Get
        Set(ByVal value As String)
            _NroDocumento = value
        End Set
    End Property


    Public Property prod_CodigoBarras() As String
        Get
            Return _prod_CodigoBarras
        End Get
        Set(ByVal value As String)
            _prod_CodigoBarras = value
        End Set
    End Property
    Public Property desconcepto() As String
        Get
            Return _desconcepto
        End Get
        Set(ByVal value As String)
            _desconcepto = value
        End Set
    End Property
    Public Property montoconcepto() As Decimal
        Get
            Return _montoconcepto
        End Get
        Set(ByVal value As Decimal)
            _montoconcepto = value
        End Set
    End Property
    Public Property dc_Descuento3() As Decimal
        Get
            Return _dc_Descuento3
        End Get
        Set(ByVal value As Decimal)
            _dc_Descuento3 = value
        End Set
    End Property

    Public Property dc_Descuento2() As Decimal
        Get
            Return _dc_Descuento2
        End Get
        Set(ByVal value As Decimal)
            _dc_Descuento2 = value
        End Set
    End Property

    Public Property dc_Descuento1() As Decimal
        Get
            Return _dc_Descuento1
        End Get
        Set(ByVal value As Decimal)
            _dc_Descuento1 = value
        End Set
    End Property

    Public Property dc_PrecioSinIGV() As Decimal
        Get
            Return _dc_PrecioSinIGV
        End Get
        Set(ByVal value As Decimal)
            _dc_PrecioSinIGV = value
        End Set
    End Property

    Public Property dc_CantidadTransito() As Decimal
        Get
            Return _dc_CantidadTransito
        End Get
        Set(ByVal value As Decimal)
            _dc_CantidadTransito = value
        End Set
    End Property

    Public Property Equivalencia2() As Decimal
        Get
            Return Me._Equivalencia2
        End Get
        Set(ByVal value As Decimal)
            Me._Equivalencia2 = value
        End Set
    End Property

    Public Property Equivalencia1() As Decimal
        Get
            Return Me._Equivalencia1
        End Get
        Set(ByVal value As Decimal)
            Me._Equivalencia1 = value
        End Set
    End Property

    Public Property Equivalencia() As Decimal
        Get
            Return Me._Equivalencia
        End Get
        Set(ByVal value As Decimal)
            Me._Equivalencia = value
        End Set
    End Property

    Public Property ListaUnidadMedida_Peso() As List(Of Entidades.MagnitudUnidad)
        Get
            Return Me._listaUnidadMedida_Peso
        End Get
        Set(ByVal value As List(Of Entidades.MagnitudUnidad))
            Me._listaUnidadMedida_Peso = value
        End Set
    End Property

    Public Property IdUnidadMedida_Peso() As Integer
        Get
            Return Me._IdUnidadMedida_Peso
        End Get
        Set(ByVal value As Integer)
            Me._IdUnidadMedida_Peso = value
        End Set
    End Property

    Public Property ExisteCampania_Producto() As Boolean
        Get
            Return Me._ExisteCampania_Producto
        End Get
        Set(ByVal value As Boolean)
            Me._ExisteCampania_Producto = value
        End Set
    End Property


    Public Property IdCampania() As Integer
        Get
            Return Me._IdCampania
        End Get
        Set(ByVal value As Integer)
            Me._IdCampania = value
        End Set
    End Property


    Public Property Moneda() As String
        Get
            Return Me._Moneda
        End Get
        Set(ByVal value As String)
            Me._Moneda = value
        End Set
    End Property



    Public Property CantidadTransito() As Decimal
        Get
            Return Me._CantidadTransito
        End Get
        Set(ByVal value As Decimal)
            Me._CantidadTransito = value
        End Set
    End Property


    Public Property Kit() As Boolean
        Get
            Return Me._Kit
        End Get
        Set(ByVal value As Boolean)
            Me._Kit = value
        End Set
    End Property

    Public Property IdKit() As Integer
        Get
            Return Me._IdKit
        End Get
        Set(ByVal value As Integer)
            Me._IdKit = value
        End Set
    End Property

    Public Property ComponenteKit() As Boolean
        Get
            Return Me._ComponenteKit
        End Get
        Set(ByVal value As Boolean)
            Me._ComponenteKit = value
        End Set
    End Property


    Public Property CantidadDetalleAfecto() As Decimal
        Get
            Return Me._CantidadDetalleAfecto
        End Get
        Set(ByVal value As Decimal)
            Me._CantidadDetalleAfecto = value
        End Set
    End Property

    Public Property PrecioBaseDcto() As String
        Get
            Return Me._PrecioBaseDcto
        End Get
        Set(ByVal value As String)
            Me._PrecioBaseDcto = value
        End Set
    End Property


    Public Property pvComercial() As Decimal
        Get
            Return Me._pvComercial
        End Get
        Set(ByVal value As Decimal)
            Me._pvComercial = value
        End Set
    End Property


    Public Property CodigoProducto() As String
        Get
            Return Me._CodigoProducto
        End Get
        Set(ByVal value As String)
            Me._CodigoProducto = value
        End Set
    End Property



    Public Property CostoMovIngreso() As Decimal
        Get
            Return Me._CostoMovIngreso
        End Get
        Set(ByVal value As Decimal)
            Me._CostoMovIngreso = value
        End Set
    End Property


    Public Property IdTipoDocumento() As Integer
        Get
            Return Me._IdTipoDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoDocumento = value
        End Set
    End Property

    Public Property Stock() As Decimal
        Get
            Return _Stock
        End Get
        Set(ByVal value As Decimal)
            _Stock = value
        End Set
    End Property

    Public Property cantSolicitada() As Decimal
        Get
            Return _cantSolicitada
        End Get
        Set(ByVal value As Decimal)
            _cantSolicitada = value
        End Set
    End Property

    Public Property cantAprobada() As Decimal
        Get
            Return _cantAprobada
        End Get
        Set(ByVal value As Decimal)
            _cantAprobada = value
        End Set
    End Property

    Public Property IdDocumentoRef() As Integer
        Get
            Return Me._IdDocumentoRef
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumentoRef = value
        End Set
    End Property



    Public Property IdTipoPV() As Integer
        Get
            Return Me._IdTipoPV
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoPV = value
        End Set
    End Property

    Public Property DctoPorcent_MaximoxPVenta() As Decimal
        Get
            Return Me._DctoPorcent_MaximoxPVenta
        End Get
        Set(ByVal value As Decimal)
            Me._DctoPorcent_MaximoxPVenta = value
        End Set
    End Property


    Public Property DctoValor_MaximoxPVenta() As Decimal
        Get
            Return Me._DctoValor_MaximoxPVenta
        End Get
        Set(ByVal value As Decimal)
            Me._DctoValor_MaximoxPVenta = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property



    Public Property VolumenVentaMin() As Decimal
        Get
            Return Me._VolumenVentaMin
        End Get
        Set(ByVal value As Decimal)
            Me._VolumenVentaMin = value
        End Set
    End Property


    Public Property PorcentPercepcion() As Decimal
        Get
            Return Me._PorcentPercepcion
        End Get
        Set(ByVal value As Decimal)
            Me._PorcentPercepcion = value
        End Set
    End Property



    Public Property PorcentDcto() As Decimal
        Get
            Return Me._PorcentDcto
        End Get
        Set(ByVal value As Decimal)
            Me._PorcentDcto = value
        End Set
    End Property



    Public Property PrecioLista() As Decimal
        Get
            Return Me._PrecioLista
        End Get
        Set(ByVal value As Decimal)
            Me._PrecioLista = value
        End Set
    End Property

    Public Property StockDisponibleN() As Decimal
        Get
            Return Me._StockDisponibleN
        End Get
        Set(ByVal value As Decimal)
            Me._StockDisponibleN = value
        End Set
    End Property


    Public Property ListaUM_Venta() As List(Of Entidades.UnidadMedida)
        Get
            Return Me._listaUM_Venta
        End Get
        Set(ByVal value As List(Of Entidades.UnidadMedida))
            Me._listaUM_Venta = value
        End Set
    End Property

    Public Property listaTonos() As List(Of Entidades.be_tonoXProducto)
        Get
            Return Me._Tono
        End Get
        Set(ByVal value As List(Of Entidades.be_tonoXProducto))
            Me._Tono = value
        End Set
    End Property

    Public Property listaSector() As List(Of Entidades.be_Sector)
        Get
            Return _listaSector
        End Get
        Set(ByVal value As List(Of Entidades.be_Sector))
            Me._listaSector = value
        End Set
    End Property

    Public Property idSector() As Integer
        Get
            Return _idSector
        End Get
        Set(ByVal value As Integer)
            Me._idSector = value
        End Set
    End Property

    Public Property cantidadTono() As Decimal
        Get
            Return _cantidadTono
        End Get
        Set(ByVal value As Decimal)
            _cantidadTono = value
        End Set
    End Property

    Public Property ListaUM() As List(Of Entidades.ProductoUMView)
        Get
            Return Me._ListaUM
        End Get
        Set(ByVal value As List(Of Entidades.ProductoUMView))
            Me._ListaUM = value
        End Set
    End Property

    Public Property cadenaUM() As String
        Get
            Return _cadenaUM
        End Get
        Set(ByVal value As String)
            _cadenaUM = value
        End Set
    End Property

    Public ReadOnly Property ListaUMedida() As List(Of Entidades.ProductoUMView)
        Get

            Dim Lista As New List(Of Entidades.ProductoUMView)

            Dim IdDescripcionUM() As String = cadenaUM.Split("=")

            For x As Integer = 0 To IdDescripcionUM.Length - 1
                Dim id_UM() As String = IdDescripcionUM(x).Split(",")
                Dim obj As New Entidades.ProductoUMView
                obj.IdUnidadMedida = id_UM(0)
                obj.NombreCortoUM = id_UM(1)
                Lista.Add(obj)
            Next

            Return Lista
        End Get
    End Property

    Public Property IdUsuarioSupervisor() As Integer
        Get
            Return Me._IdSupervisor
        End Get
        Set(ByVal value As Integer)
            Me._IdSupervisor = value
        End Set
    End Property

    Public Property PorcentDctoMaximo() As Decimal
        Get
            Return Me._PorcentDctoMaximo
        End Get
        Set(ByVal value As Decimal)
            Me._PorcentDctoMaximo = value
        End Set
    End Property
    Public Property Factor() As Integer
        Get
            Return Me._Factor
        End Get
        Set(ByVal value As Integer)
            Me._Factor = value
        End Set
    End Property

    Public Property CantADespacharOriginal() As Decimal
        Get
            Return Me._CantADespacharOriginal
        End Get
        Set(ByVal value As Decimal)
            Me._CantADespacharOriginal = value
        End Set
    End Property


    Public Property DetalleGlosa() As String
        Get
            Return Me._DetalleGlosa
        End Get
        Set(ByVal value As String)
            Me._DetalleGlosa = value
        End Set
    End Property

    Public Property CantidadxAtenderText() As String
        Get
            Return Me._CantidadxAtenderText
        End Get
        Set(ByVal value As String)
            Me._CantidadxAtenderText = value
        End Set
    End Property


    Public Property IdDetalleAfecto() As Integer
        Get
            Return Me._IdDetalleAfecto
        End Get
        Set(ByVal value As Integer)
            Me._IdDetalleAfecto = value
        End Set
    End Property
    Public Property IdUnidadMedidaPrincipal() As Integer
        Get
            Return Me._IdUnidadMedidaPrincipal
        End Get
        Set(ByVal value As Integer)
            Me._IdUnidadMedidaPrincipal = value
        End Set
    End Property
    Public Property IdDetalleDocumento() As Integer
        Get
            Return Me._IdDetalleDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDetalleDocumento = value
        End Set
    End Property
    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property

    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property

    Public Property idTono() As Integer
        Get
            Return _idTono
        End Get
        Set(ByVal value As Integer)
            _idTono = value
        End Set
    End Property

    Public Property IdUnidadMedida() As Integer
        Get
            Return Me._IdUnidadMedida
        End Get
        Set(ByVal value As Integer)
            Me._IdUnidadMedida = value
        End Set
    End Property
    Public Property IdUMold() As Integer
        Get
            Return Me._IdUMold
        End Get
        Set(ByVal value As Integer)
            Me._IdUMold = value
        End Set
    End Property
    Public Property Cantidad() As Decimal
        Get
            Return Me._dc_Cantidad
        End Get
        Set(ByVal value As Decimal)
            Me._dc_Cantidad = value
        End Set
    End Property

    Public Property CantxAtender() As Decimal
        Get
            Return Me._dc_CantxAtender
        End Get
        Set(ByVal value As Decimal)
            Me._dc_CantxAtender = value
        End Set
    End Property

    Public Property UMedida() As String
        Get
            Return Me._dc_UMedida
        End Get
        Set(ByVal value As String)
            Me._dc_UMedida = value
        End Set
    End Property

    Public Property UnidadMedidaPrincipal() As String
        Get
            Return Me._UnidadMedidaPrincipal
        End Get
        Set(ByVal value As String)
            Me._UnidadMedidaPrincipal = value
        End Set
    End Property


    Public Property PrecioSD() As Decimal
        Get
            Return Me._dc_PrecioSD
        End Get
        Set(ByVal value As Decimal)
            Me._dc_PrecioSD = value
        End Set
    End Property

    Public Property Descuento() As Decimal
        Get
            Return Me._dc_Descuento
        End Get
        Set(ByVal value As Decimal)
            Me._dc_Descuento = value
        End Set
    End Property

    Public Property PrecioCD() As Decimal
        Get
            Return Me._dc_PrecioCD
        End Get
        Set(ByVal value As Decimal)
            Me._dc_PrecioCD = value
        End Set
    End Property

    Public Property Importe() As Decimal
        Get
            Return Me._dc_Importe
        End Get
        Set(ByVal value As Decimal)
            Me._dc_Importe = value
        End Set
    End Property

    Public Property Utilidad() As Decimal
        Get
            Return Me._dc_Utilidad
        End Get
        Set(ByVal value As Decimal)
            Me._dc_Utilidad = value
        End Set
    End Property
    Public Property TasaPercepcion() As Decimal
        Get
            Return Me._dc_TasaPercepcion
        End Get
        Set(ByVal value As Decimal)
            Me._dc_TasaPercepcion = value
        End Set
    End Property

    Public Property TasaDetraccion() As Decimal
        Get
            Return Me._dc_TasaDetraccion
        End Get
        Set(ByVal value As Decimal)
            Me._dc_TasaDetraccion = value
        End Set
    End Property
    Public Property Peso() As Decimal
        Get
            Return Me._dc_Peso
        End Get
        Set(ByVal value As Decimal)
            Me._dc_Peso = value
        End Set
    End Property
    Public Property UmPeso() As String
        Get
            Return Me._dc_UmPeso
        End Get
        Set(ByVal value As String)
            Me._dc_UmPeso = value
        End Set
    End Property
    Public Property IdAlmacen() As Integer
        Get
            Return Me._IdAlmacen
        End Get
        Set(ByVal value As Integer)
            Me._IdAlmacen = value
        End Set
    End Property
    Public Property CantADespachar() As Decimal
        Get
            Return Me._CantADespachar
        End Get
        Set(ByVal value As Decimal)
            Me._CantADespachar = value
        End Set
    End Property

    Public Property NomProducto() As String
        Get
            Return Me._nomProducto
        End Get
        Set(ByVal value As String)
            Me._nomProducto = value
        End Set
    End Property

    Public Property UMProducto() As String
        Get
            Return Me._UMProducto
        End Get
        Set(ByVal value As String)
            Me._UMProducto = value
        End Set
    End Property
    Public Property UMDescCorta() As String
        Get
            Return Me._UMDescCorta
        End Get
        Set(ByVal value As String)
            Me._UMDescCorta = value
        End Set
    End Property
End Class
