﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class FabricanteLinea
    Private _IdFabricante As Integer
    Private _NomFabricante As String
    Private _IdLineas As Integer
    Private _IdTipoExistencia As Integer
    Private _fablEstado As Boolean
    Private _fablOrden As Integer
    Private _IdFabricanteOrden As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal idfabricante As Integer, ByVal nomfabricante As String, ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal estado As Boolean, ByVal orden As Integer)
        _IdFabricante = idfabricante
        _NomFabricante = nomfabricante
        _IdLineas = idlinea
        _IdTipoExistencia = idtipoexistencia
        _fablEstado = estado
        _fablOrden = orden
    End Sub
    Public Sub New(ByVal idfabricante As Integer, ByVal nomfabricante As String, ByVal estado As Boolean)
        _IdFabricante = idfabricante
        _NomFabricante = nomfabricante
        _fablEstado = estado
    End Sub
    Public Property IdFabricante() As Integer
        Get
            Return _IdFabricante
        End Get
        Set(ByVal value As Integer)
            _IdFabricante = value
        End Set
    End Property
    Public Property NomFabricante() As String
        Get
            Return _NomFabricante
        End Get
        Set(ByVal value As String)
            _NomFabricante = value
        End Set
    End Property
    Public Property IdLinea() As Integer
        Get
            Return _IdLineas
        End Get
        Set(ByVal value As Integer)
            _IdLineas = value
        End Set
    End Property
    Public Property IdTipoExistencia() As Integer
        Get
            Return _IdTipoExistencia
        End Get
        Set(ByVal value As Integer)
            _IdTipoExistencia = value
        End Set
    End Property
    Public Property Estado() As Boolean
        Get
            Return _fablEstado
        End Get
        Set(ByVal value As Boolean)
            _fablEstado = value
        End Set
    End Property
    Public Property Orden() As Integer
        Get
            Return _fablOrden
        End Get
        Set(ByVal value As Integer)
            _fablOrden = value
        End Set
    End Property
    Public Property IdFabricanteOrden() As String
        Get
            Return _IdFabricanteOrden
        End Get
        Set(ByVal value As String)
            _IdFabricanteOrden = value
        End Set
    End Property
End Class
