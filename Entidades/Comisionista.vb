﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'********************   JUEVES 10 JUNIO 2010 HORA 12_46 PM

Public Class Comisionista
    Inherits Entidades.PersonaView

    Private _IdComisionista As Integer
    Private _IdComisionCab As Integer
    Private _com_Estado As Boolean
    Private _IncluyeIgv As Boolean
    Private _Configurado As Boolean
    Private _Empresa As String
    Private _Tienda As String
    Private _IdEmpresa As Integer
    Private _IdTienda As Integer
    Private _IdPerfil As Integer
    Private _IdRol As Integer
    Private _Perfil As String
    Private _Rol As String
    Private _baseComision As String
    Private _valPorcentaje As Decimal
    Private _TipoCalculo As Boolean

    Public Property TipoCalculo() As Boolean
        Get
            Return _TipoCalculo
        End Get
        Set(ByVal value As Boolean)
            _TipoCalculo = value
        End Set
    End Property

    Public Property valPorcentaje() As Decimal
        Get
            Return _valPorcentaje
        End Get
        Set(ByVal value As Decimal)
            Me._valPorcentaje = value
        End Set
    End Property

    Public Property baseComision() As String
        Get
            Return Me._baseComision
        End Get
        Set(ByVal value As String)
            Me._baseComision = value
        End Set
    End Property

    Public Property Empresa() As String
        Get
            Return Me._Empresa
        End Get
        Set(ByVal value As String)
            Me._Empresa = value
        End Set
    End Property

    Public Property Tienda() As String
        Get
            Return Me._Tienda
        End Get
        Set(ByVal value As String)
            Me._Tienda = value
        End Set
    End Property

    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property IdPerfil() As Integer
        Get
            Return Me._IdPerfil
        End Get
        Set(ByVal value As Integer)
            Me._IdPerfil = value
        End Set
    End Property

    Public Property IdRol() As Integer
        Get
            Return Me._IdRol
        End Get
        Set(ByVal value As Integer)
            Me._IdRol = value
        End Set
    End Property

    Public Property Perfil() As String
        Get
            Return Me._Perfil
        End Get
        Set(ByVal value As String)
            Me._Perfil = value
        End Set
    End Property

    Public Property Rol() As String
        Get
            Return Me._Rol
        End Get
        Set(ByVal value As String)
            Me._Rol = value
        End Set
    End Property

    Public Property IdComisionista() As Integer
        Get
            Return Me._IdComisionista
        End Get
        Set(ByVal value As Integer)
            Me._IdComisionista = value
        End Set
    End Property

    Public Property IdComisionCab() As Integer
        Get
            Return Me._IdComisionCab
        End Get
        Set(ByVal value As Integer)
            Me._IdComisionCab = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return Me._com_Estado
        End Get
        Set(ByVal value As Boolean)
            Me._com_Estado = value
        End Set
    End Property

    Public Property IncluyeIgv() As Boolean
        Get
            Return Me._IncluyeIgv
        End Get
        Set(ByVal value As Boolean)
            Me._IncluyeIgv = value
        End Set
    End Property

    Public Property Configurado() As Boolean
        Get
            Return Me._Configurado
        End Get
        Set(ByVal value As Boolean)
            Me._Configurado = value
        End Set
    End Property


End Class
