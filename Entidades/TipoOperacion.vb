'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class TipoOperacion
    Private _IdTipoOperacion As Integer
    Private _top_CodigoSunat As String
    Private _top_Nombre As String
    Private _top_Estado As String
    Private _descEstado As String

    Public Sub New()
    End Sub
    Public Sub New(ByVal idtipooperacion As Integer, ByVal nombre As String)
        Me.Id = idtipooperacion
        Me.Nombre = nombre
    End Sub


    Public ReadOnly Property DescEstado() As String
        Get
            Me._descEstado = "Activo"
            If Me._top_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property



    Public Property Id() As Integer
        Get
            Return Me._IdTipoOperacion
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoOperacion = value
        End Set
    End Property

    Public Property CodigoSunat() As String
        Get
            Return Me._top_CodigoSunat
        End Get
        Set(ByVal value As String)
            Me._top_CodigoSunat = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._top_Nombre
        End Get
        Set(ByVal value As String)
            Me._top_Nombre = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._top_Estado
        End Get
        Set(ByVal value As String)
            Me._top_Estado = value
        End Set
    End Property
End Class
