'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Area
    Private _IdArea As Integer
    Private _ar_NombreLargo As String
    Private _ar_NombreCorto As String
    Private _ar_Estado As String
    Private _ar_DescEstado As String
    Private _EstadoAE As Boolean
    Private _idcentrocosto As String
    Private _IdCompuesto As String

    Public Sub New()
    End Sub
    Public Sub New(ByVal idarea As Integer, ByVal nombre As String, ByVal estadoae As Boolean)
        Me._IdArea = idarea
        Me._ar_NombreCorto = nombre
        Me._EstadoAE = estadoae
    End Sub
    Public Sub New(ByVal idarea As Integer, ByVal nombre As String)
        Me._IdArea = idarea
        Me._ar_NombreCorto = nombre
        Me._EstadoAE = EstadoAE
    End Sub

    Public Property IdCompuesto() As String
        Get
            Return _IdCompuesto
        End Get
        Set(ByVal value As String)
            _IdCompuesto = value
        End Set
    End Property

    Public Property IdCentroCosto() As String
        Get
            Return _idcentrocosto
        End Get
        Set(ByVal value As String)
            _idcentrocosto = value
        End Set
    End Property

    Public Property Id() As Integer
        Get
            Return Me._IdArea
        End Get
        Set(ByVal value As Integer)
            Me._IdArea = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._ar_NombreLargo
        End Get
        Set(ByVal value As String)
            Me._ar_NombreLargo = value
        End Set
    End Property

    Public Property DescripcionCorta() As String
        Get
            Return Me._ar_NombreCorto
        End Get
        Set(ByVal value As String)
            Me._ar_NombreCorto = value
        End Set
    End Property
    Public Property Estado() As String
        Get
            Return Me._ar_Estado
        End Get
        Set(ByVal value As String)
            Me._ar_Estado = value
        End Set
    End Property
    Public ReadOnly Property DescEstado() As String
        Get
            Me._ar_DescEstado = "Activo"
            If _ar_Estado = "0" Then
                Me._ar_DescEstado = "Inactivo"
            End If
            Return Me._ar_DescEstado
        End Get
    End Property
    Public Property EstadoAE() As Boolean
        Get
            Return _EstadoAE
        End Get
        Set(ByVal value As Boolean)
            _EstadoAE = value
        End Set
    End Property

End Class
