﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'Modificado 06 ABR 2010

'*************************************************
'Autor   : Chang Carnero, Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 09-Noviembre-2009
'Hora    : 05:30 pm
'*************************************************
Public Class Perfil
    Private _IdPerfil As Integer
    Private _NomPerfil As String
    Private _Estado As String
    Private _EstadoMasivo As String
    '******** Permiso
    Private _IdPermiso As Integer
    Private _Perm_Descrip As String


    Public Sub New()
    End Sub

    Public Sub New(ByVal idperfil As Integer, ByVal nombre As String)
        Me.IdPerfil = idperfil
        Me.NomPerfil = nombre
    End Sub

    Public ReadOnly Property DescEstado() As String
        Get

            If (Me.Estado = "1") Then
                Return "Activo"
            Else
                Return "Inactivo"
            End If
        End Get
    End Property


    Public Property IdPerfil() As Integer
        Get
            Return Me._IdPerfil
        End Get
        Set(ByVal value As Integer)
            Me._IdPerfil = value
        End Set
    End Property

    Public Property EstadoBoolean() As Boolean
        Get
            Return IIf(Me._Estado = "1", True, False)
        End Get
        Set(ByVal value As Boolean)
            Me._Estado = IIf(value, "1", "0")
        End Set
    End Property

    Public Property NomPerfil() As String
        Get
            Return Me._NomPerfil
        End Get
        Set(ByVal value As String)
            Me._NomPerfil = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._Estado
        End Get
        Set(ByVal value As String)
            Me._Estado = value
        End Set
    End Property

    Public Property EstadoMasivo() As String
        Get
            Return Me._EstadoMasivo
        End Get
        Set(ByVal value As String)
            Me._EstadoMasivo = value
        End Set
    End Property

    Public Property IdPermiso() As Integer
        Get
            Return _IdPermiso
        End Get
        Set(ByVal value As Integer)
            _IdPermiso = value
        End Set
    End Property
    Public Property perm_Descripcion() As String
        Get
            Return _Perm_Descrip
        End Get
        Set(ByVal value As String)
            _Perm_Descrip = value
        End Set
    End Property
End Class
