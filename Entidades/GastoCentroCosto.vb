﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.





'**************************   MARTES 11 MAYO 2010 HORA 04_05 PM






Public Class GastoCentroCosto
    Private _UnidadNegocio As String
    Private _DeptoFuncional As String
    Private _SubArea1 As String
    Private _SubArea2 As String
    Private _SubArea3 As String
    Private _IdCentroCosto As String
    Private _Monto As Decimal
    Private _Moneda As String
    Private _IdDocumento As Integer

    Public Property UnidadNegocio() As String
        Get
            Return Me._UnidadNegocio
        End Get
        Set(ByVal value As String)
            Me._UnidadNegocio = value
        End Set
    End Property

    Public Property DeptoFuncional() As String
        Get
            Return Me._DeptoFuncional
        End Get
        Set(ByVal value As String)
            Me._DeptoFuncional = value
        End Set
    End Property

    Public Property SubArea1() As String
        Get
            Return Me._SubArea1
        End Get
        Set(ByVal value As String)
            Me._SubArea1 = value
        End Set
    End Property

    Public Property SubArea2() As String
        Get
            Return Me._SubArea2
        End Get
        Set(ByVal value As String)
            Me._SubArea2 = value
        End Set
    End Property

    Public Property SubArea3() As String
        Get
            Return Me._SubArea3
        End Get
        Set(ByVal value As String)
            Me._SubArea3 = value
        End Set
    End Property

    Public Property IdCentroCosto() As String
        Get
            Return Me._IdCentroCosto
        End Get
        Set(ByVal value As String)
            Me._IdCentroCosto = value
        End Set
    End Property

    Public Property Monto() As Decimal
        Get
            Return Me._Monto
        End Get
        Set(ByVal value As Decimal)
            Me._Monto = value
        End Set
    End Property

    Public Property Moneda() As String
        Get
            Return Me._Moneda
        End Get
        Set(ByVal value As String)
            Me._Moneda = value
        End Set
    End Property

    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property


End Class
