'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class TipoMovimiento
    Private _IdTipoMovimiento As Integer
    Private _tm_Nombre As String
    Private _tm_Estado As String
    Private _descEstado As String
    Public ReadOnly Property DescEstado() As String
        Get
            Me._descEstado = "Activo"
            If Me._tm_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public Property Id() As Integer
        Get
            Return Me._IdTipoMovimiento
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoMovimiento = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._tm_Nombre
        End Get
        Set(ByVal value As String)
            Me._tm_Nombre = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._tm_Estado
        End Get
        Set(ByVal value As String)
            Me._tm_Estado = value
        End Set
    End Property
End Class
