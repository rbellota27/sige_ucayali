'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Ubicacion
    Private _IdUbicacion As Integer
    Private _ubi_Anaquel As Integer
    Private _ubi_Fila As Integer
    Private _ubi_Columna As Integer
    Private _IdProducto As Integer


    Public Property Id() As Integer
        Get
            Return Me._IdUbicacion
        End Get
        Set(ByVal value As Integer)
            Me._IdUbicacion = value
        End Set
    End Property

    Public Property Anaquel() As Integer
        Get
            Return Me._ubi_Anaquel
        End Get
        Set(ByVal value As Integer)
            Me._ubi_Anaquel = value
        End Set
    End Property

    Public Property Fila() As Integer
        Get
            Return Me._ubi_Fila
        End Get
        Set(ByVal value As Integer)
            Me._ubi_Fila = value
        End Set
    End Property

    Public Property Columna() As Integer
        Get
            Return Me._ubi_Columna
        End Get
        Set(ByVal value As Integer)
            Me._ubi_Columna = value
        End Set
    End Property

    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property
End Class
