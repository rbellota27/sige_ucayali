'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Oficina
    Private _IdOficina As Integer
    Private _of_Nombre As String
    Private _of_Estado As String
    Private _IdBanco As Integer
    Public Property Id() As Integer
        Get
            Return Me._IdOficina
        End Get
        Set(ByVal value As Integer)
            Me._IdOficina = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._of_Nombre
        End Get
        Set(ByVal value As String)
            Me._of_Nombre = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._of_Estado
        End Get
        Set(ByVal value As String)
            Me._of_Estado = value
        End Set
    End Property

    Public Property IdBanco() As Integer
        Get
            Return Me._IdBanco
        End Get
        Set(ByVal value As Integer)
            Me._IdBanco = value
        End Set
    End Property
    Public Sub New()

    End Sub
    Public Sub New(ByVal Idof As Integer, ByVal descrip As String, ByVal Estado As String)
        Me.Id = Idof
        Me.Descripcion = descrip
        Me.Estado = Estado
    End Sub
End Class
