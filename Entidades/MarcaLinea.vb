﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class MarcaLinea
    Inherits Marca
    Private _IdMarca As Integer
    Private _NomMarca As String
    Private _IdLinea As Integer
    Private _IdTipoExistencia As Integer
    Private _mlEstado As Boolean
    Private _mlOrden As Integer
    Private _IdMarcaOrden As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal idmarca As Integer, ByVal nommarcar As String, ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal estado As Boolean, ByVal orden As Integer)
        _IdMarca = idmarca
        _NomMarca = nommarcar
        _IdLinea = idlinea
        _IdTipoExistencia = idtipoexistencia
        _mlEstado = estado
        _mlOrden = orden
    End Sub
    Public Sub New(ByVal idmarca As Integer, ByVal nommarca As String, ByVal estado As Boolean)
        _IdMarca = idmarca
        _NomMarca = nommarca
        _mlEstado = estado
    End Sub
    Public Property IdMarca() As Integer
        Get
            Return _IdMarca
        End Get
        Set(ByVal value As Integer)
            _IdMarca = value
        End Set
    End Property
    Public Property NomMarca() As String
        Get
            Return _NomMarca
        End Get
        Set(ByVal value As String)
            _NomMarca = value
        End Set
    End Property
    Public Property IdLinea() As Integer
        Get
            Return _IdLinea
        End Get
        Set(ByVal value As Integer)
            _IdLinea = value
        End Set
    End Property
    Public Property IdTipoExistencia() As Integer
        Get
            Return _IdTipoExistencia
        End Get
        Set(ByVal value As Integer)
            _IdTipoExistencia = value
        End Set
    End Property
    Public Property Estado() As Boolean
        Get
            Return _mlEstado
        End Get
        Set(ByVal value As Boolean)
            _mlEstado = value
        End Set
    End Property
    Public Property Orden() As Integer
        Get
            Return _mlOrden
        End Get
        Set(ByVal value As Integer)
            _mlOrden = value
        End Set
    End Property
    Public Property IdMarcaOrden() As String
        Get
            Return _IdMarcaOrden
        End Get
        Set(ByVal value As String)
            _IdMarcaOrden = value
        End Set
    End Property
End Class
