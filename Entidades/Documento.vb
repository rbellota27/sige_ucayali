'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.


' jueves, 20 de Enero de 2011

Public Class Documento
    Inherits Anexo_Documento

    Private _nomproveedor As String = String.Empty
    Private _regNombre As String = String.Empty
    Private _enviarTablaCuenta As Boolean
    Private _IdTiendaSucursal As Integer
    Private _doc_Importacion As Object
    Private _anex_PrecImportacion As Integer
    Private _IdDocumento As Integer
    Private _IdAlmacen As Integer
    Private _doc_Codigo As String
    Private _doc_Serie As String
    Private _doc_FechaEmision As Date
    Private _doc_FechaIniTraslado As Date
    Private _doc_FechaRegistro As Date
    Private _doc_FechaAentregar As Date
    Private _doc_FechaEntrega As Date
    Private _doc_FechaVenc As Date
    Private _doc_ImporteTotal As Decimal
    Private _doc_Descuento As Decimal
    Private _doc_SubTotal As Decimal
    Private _doc_ValorReferencial As Decimal
    Private _doc_Igv As Decimal
    Private _doc_Total As Decimal
    Private _doc_TotalLetras As String
    Private _doc_TotalAPagar As Decimal
    Private _doc_Utilidad As Decimal
    Private _IdPersona As Integer
    Private _IdTransportista As Integer
    Private _opciondeuda As Integer
    Private _IdRemitente As Integer
    Private _IdDestinatario As Integer
    Private _IdEstadoDoc As Integer
    Private _IdCondicionPago As Integer
    Private _IdMoneda As Integer
    Private _IdUsuario As Integer
    Private _LugarEntrega As Integer
    Private _IdTipoOperacion As Integer
    Private _IdTienda As Integer
    Private _IdSerie As Integer
    Private _IdEmpresa As Integer
    Private _doc_ExportadoConta As String
    Private _doc_NroVoucherConta As String
    Private _IdChofer As Integer
    Private _IdMotivoT As Integer
    Private _IdTipoDocumento As Integer
    Private _IdVehiculo As Integer
    Private _IdEstadoCancelacion As Integer
    Private _IdEstadoEntrega As Integer
    Private _FechaCancelacion As Date
    Private _NomEstadoDocumento As String
    Private _NomTipoOperacion As String
    Private _NomTipoDocumento As String
    Private _NomAlmacen As String
    Private _FactorMov As Integer
    Private _IdArea As Integer
    Private _NomMoneda As String
    Private _Percepcion As Decimal
    Private _Vuelto As Decimal
    Private _IdTipoAgente As Integer
    Private _TasaAgente As Decimal
    Private _Retencion As Decimal
    Private _IdTipoPV As Integer
    Private _Detraccion As Decimal
    Private _PoseeOrdenDespacho As Boolean
    Private _IdCaja As Integer
    Private _NomEmpleado As String
    Private _PoseeAmortizaciones As Boolean
    Private _NomEmpresaTomaInv As String
    Private _IdDocRelacionado As Integer
    Private _PoseeDocRelacionado As String
    Private _DescripcionPersona As String
    Private _NomCondicionPago As String
    Private _PorcentIGV As Decimal
    Private _PorcentPercepcion As Decimal
    Private _DocumentoI As String
    Private _Direccion As String
    Private _PoseeCompPercepcion As Boolean
    Private _CompPercepcion As Boolean
    Private _NomEstadoEntregado As String
    Private _NomEstadoCancelacion As String
    Private _Contador As Integer
    Private _Tienda As String
    Private _Empresa As String
    Private _NroDocumento As String
    Private _Mensaje As String
    Private _MedioPago As String
    Private _Ruc As String
    Private _CentroCosto As String
    Private _IdMedioPagoCredito As Integer
    Private _contadorusu As Integer
    Private _IdTipoAlmacen As Integer
    Private _CantidadTransito As Decimal
    Private _IdUsuarioComision As Integer
    Private _Saldo As Decimal
    Private _Transportista As String
    Private _anex_igv As Boolean
    Private _poseeGRecepcion As Boolean
    Private _DeleteIdMaestroObra As Boolean
    Private _FechaInicio As Date
    Private _FechaFin As Date
    Private _Concepto As String
    Private _BaseSoles As Decimal
    Private _CadenaDocumentoRelacionado As String
    Private _idRequerimiento As Integer = 0
    Private _nroVoucher As String = String.Empty
    Private _montoAgente As Decimal = 0
    Private _montoPendiente As Decimal = 0
    Private _montoXPagar As Decimal = 0
    Private _otrosImpuestos As Decimal = 0

    Public Property otrosImpuestos() As Decimal
        Get
            Return _otrosImpuestos
        End Get
        Set(ByVal value As Decimal)
            _otrosImpuestos = value
        End Set
    End Property

    Public Property montoXPagar() As Decimal
        Get
            Return _montoXPagar
        End Get
        Set(ByVal value As Decimal)
            _montoXPagar = value
        End Set
    End Property

    Public Property montoPendiente() As Decimal
        Get
            Return _montoPendiente
        End Get
        Set(ByVal value As Decimal)
            _montoPendiente = value
        End Set
    End Property

    Public Property montoAgente() As Decimal
        Get
            Return _montoAgente
        End Get
        Set(ByVal value As Decimal)
            _montoAgente = value
        End Set
    End Property

    Public Property nroVoucher() As String
        Get
            Return _nroVoucher
        End Get
        Set(ByVal value As String)
            _nroVoucher = value
        End Set
    End Property

    Public Property idRequerimiento() As Integer
        Get
            Return _idRequerimiento
        End Get
        Set(value As Integer)
            _idRequerimiento = value
        End Set
    End Property

    Public Property nomproveedor() As String
        Get
            Return _nomproveedor
        End Get
        Set(value As String)
            _nomproveedor = value
        End Set
    End Property

    Public Property CadenaDocumentoRelacionado() As String
        Get
            Return Me._CadenaDocumentoRelacionado
        End Get
        Set(ByVal value As String)
            Me._CadenaDocumentoRelacionado = value
        End Set
    End Property
    Public Property ContadorUsu() As Integer
        Get
            Return Me._contadorusu
        End Get
        Set(ByVal value As Integer)
            Me._contadorusu = value
        End Set
    End Property
    Public Property Concepto() As String
        Get
            Return Me._Concepto
        End Get
        Set(ByVal value As String)
            Me._Concepto = value
        End Set
    End Property
    Public Property BaseSoles() As Decimal
        Get
            Return Me._BaseSoles
        End Get
        Set(ByVal value As Decimal)
            Me._BaseSoles = value
        End Set
    End Property
    Public Property CentroCosto() As String
        Get
            Return Me._CentroCosto
        End Get
        Set(ByVal value As String)
            Me._CentroCosto = value
        End Set
    End Property
    Public Property opciondeuda() As Integer
        Get
            Return Me._opciondeuda
        End Get
        Set(ByVal value As Integer)
            Me._opciondeuda = value
        End Set
    End Property
    Public Property Ruc() As String
        Get
            Return Me._Ruc
        End Get
        Set(ByVal value As String)
            Me._Ruc = value
        End Set
    End Property

    Public Property FechaFin() As Date
        Get
            Return Me._FechaFin
        End Get
        Set(ByVal value As Date)
            Me._FechaFin = value
        End Set
    End Property
    Public Property FechaInicio() As Date
        Get
            Return Me._FechaInicio
        End Get
        Set(ByVal value As Date)
            Me._FechaInicio = value
        End Set
    End Property

    Public Property CantidadTransito() As Decimal
        Get
            Return Me._CantidadTransito
        End Get
        Set(ByVal value As Decimal)
            Me._CantidadTransito = value
        End Set
    End Property

    Public Property DeleteIdMaestroObra() As Boolean
        Get
            Return _DeleteIdMaestroObra
        End Get
        Set(ByVal value As Boolean)
            _DeleteIdMaestroObra = value
        End Set
    End Property

    Public Property PoseeGRecepcion() As Boolean
        Get
            Return _poseeGRecepcion
        End Get
        Set(ByVal value As Boolean)
            _poseeGRecepcion = value
        End Set
    End Property

    Public Property anex_igv() As Boolean
        Get
            Return _anex_igv
        End Get
        Set(ByVal value As Boolean)
            _anex_igv = value
        End Set
    End Property

    Public Property Transportista() As String
        Get
            Return Me._Transportista
        End Get
        Set(ByVal value As String)
            Me._Transportista = value
        End Set
    End Property

    Public Property Mensaje() As String
        Get
            Return Me._Mensaje
        End Get
        Set(ByVal value As String)
            Me._Mensaje = value
        End Set
    End Property


    Public Property Saldo() As Decimal
        Get
            Return Me._Saldo
        End Get
        Set(ByVal value As Decimal)
            Me._Saldo = value
        End Set
    End Property

    Public Property IdUsuarioComision() As Integer
        Get
            Return Me._IdUsuarioComision
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuarioComision = value
        End Set
    End Property



    Public Property IdTipoAlmacen() As Integer
        Get
            Return Me._IdTipoAlmacen
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoAlmacen = value
        End Set
    End Property

    Public Property IdMedioPagoCredito() As Integer
        Get
            Return Me._IdMedioPagoCredito
        End Get
        Set(ByVal value As Integer)
            Me._IdMedioPagoCredito = value
        End Set
    End Property



    Public Property MedioPago() As String
        Get
            Return Me._MedioPago
        End Get
        Set(ByVal value As String)
            Me._MedioPago = value
        End Set
    End Property



    Public Property anex_PrecImportacion() As Integer
        Get
            Return _anex_PrecImportacion
        End Get
        Set(ByVal value As Integer)
            _anex_PrecImportacion = value
        End Set
    End Property

    Public Property NroDocumento() As String
        Get
            Return Me._NroDocumento
        End Get
        Set(ByVal value As String)
            Me._NroDocumento = value
        End Set
    End Property

    Public Property Tienda() As String
        Get
            Return Me._Tienda
        End Get
        Set(ByVal value As String)
            Me._Tienda = value
        End Set
    End Property

    Public Property Empresa() As String
        Get
            Return Me._Empresa
        End Get
        Set(ByVal value As String)
            Me._Empresa = value
        End Set
    End Property


    Public Property FechaCancelacion() As Date
        Get
            Return Me._FechaCancelacion
        End Get
        Set(ByVal value As Date)
            Me._FechaCancelacion = value
        End Set
    End Property

    Public Property NomEstadoCancelacion() As String
        Get
            Return _NomEstadoCancelacion
        End Get
        Set(ByVal value As String)
            _NomEstadoCancelacion = value
        End Set
    End Property

    Public Property NomEstadoEntregado() As String
        Get
            Return _NomEstadoEntregado
        End Get
        Set(ByVal value As String)
            _NomEstadoEntregado = value
        End Set
    End Property
    Public Property Contador() As Integer
        Get
            Return _Contador
        End Get
        Set(ByVal value As Integer)
            _Contador = value
        End Set
    End Property

    Public Property doc_Importacion() As Object
        Get
            Return _doc_Importacion
        End Get
        Set(ByVal value As Object)
            _doc_Importacion = value
        End Set
    End Property

    Public Property IdTiendaSucursal() As Integer
        Get
            Return _IdTiendaSucursal
        End Get
        Set(ByVal value As Integer)
            _IdTiendaSucursal = value
        End Set
    End Property

    Public Property CompPercepcion() As Boolean
        Get
            Return Me._CompPercepcion
        End Get
        Set(ByVal value As Boolean)
            Me._CompPercepcion = value
        End Set
    End Property

    Public Property PoseeCompPercepcion() As Boolean
        Get
            Return Me._PoseeCompPercepcion
        End Get
        Set(ByVal value As Boolean)
            Me._PoseeCompPercepcion = value
        End Set
    End Property


    Private _strIdRequerimientosCadena As String = String.Empty
    Public Property strIdRequerimientoCadena() As String
        Get
            Return Me._strIdRequerimientosCadena
        End Get
        Set(ByVal value As String)
            _strIdRequerimientosCadena = value

        End Set
    End Property
    Public Property Direccion() As String
        Get
            Return Me._Direccion
        End Get
        Set(ByVal value As String)
            Me._Direccion = value
        End Set
    End Property



    Public ReadOnly Property getFechaVctoText() As String
        Get

            Dim fechaVctoText As String = ""
            If (Me.FechaVenc <> Nothing) Then
                fechaVctoText = Format(Me.FechaVenc, "dd/MM/yyyy")
            End If
            Return fechaVctoText
        End Get
    End Property

    Public ReadOnly Property getFechaEmisionText() As String
        Get

            Dim fechaEmisionText As String = ""
            If (Me.FechaEmision <> Nothing) Then
                fechaEmisionText = Format(Me.FechaEmision, "dd/MM/yyyy")
            End If
            Return fechaEmisionText
        End Get
    End Property
    Public ReadOnly Property getFechaCancelacionText() As String
        Get
            Dim oFechaCancelacion As String = ""
            If Me.FechaCancelacion <> Nothing Then
                oFechaCancelacion = Format(Me.FechaCancelacion, "dd/MM/yyyy")
            End If
            Return oFechaCancelacion
        End Get
    End Property



    Public Property DocumentoI() As String
        Get
            Return Me._DocumentoI
        End Get
        Set(ByVal value As String)
            Me._DocumentoI = value
        End Set
    End Property


    Public Property PorcentPercepcion() As Decimal
        Get
            Return Me._PorcentPercepcion
        End Get
        Set(ByVal value As Decimal)
            Me._PorcentPercepcion = value
        End Set
    End Property
    Public Property PorcentIGV() As Decimal
        Get
            Return Me._PorcentIGV
        End Get
        Set(ByVal value As Decimal)
            Me._PorcentIGV = value
        End Set
    End Property

    Public Property NomCondicionPago() As String
        Get
            Return Me._NomCondicionPago
        End Get
        Set(ByVal value As String)
            Me._NomCondicionPago = value
        End Set
    End Property


    Public Property DescripcionPersona() As String
        Get
            Return Me._DescripcionPersona
        End Get
        Set(ByVal value As String)
            Me._DescripcionPersona = value
        End Set
    End Property



    Public Property PoseeDocRelacionado() As String
        Get
            Return Me._PoseeDocRelacionado
        End Get
        Set(ByVal value As String)
            Me._PoseeDocRelacionado = value
        End Set
    End Property




    Public Property IdDocRelacionado() As Integer
        Get
            Return Me._IdDocRelacionado
        End Get
        Set(ByVal value As Integer)
            Me._IdDocRelacionado = value
        End Set
    End Property

    Public Property NomEmpresaTomaInv() As String
        Get
            Return Me._NomEmpresaTomaInv
        End Get
        Set(ByVal value As String)
            Me._NomEmpresaTomaInv = value
        End Set
    End Property



    Public Property NomEmpleado() As String
        Get
            Return Me._NomEmpleado
        End Get
        Set(ByVal value As String)
            Me._NomEmpleado = value
        End Set
    End Property




    Public Property PoseeAmortizaciones() As Boolean
        Get
            Return Me._PoseeAmortizaciones
        End Get
        Set(ByVal value As Boolean)
            Me._PoseeAmortizaciones = value
        End Set
    End Property

    Public Property IdCaja() As Integer
        Get
            Return Me._IdCaja
        End Get
        Set(ByVal value As Integer)
            Me._IdCaja = value
        End Set
    End Property


    Public Property PoseeOrdenDespacho() As Boolean
        Get
            Return Me._PoseeOrdenDespacho
        End Get
        Set(ByVal value As Boolean)
            Me._PoseeOrdenDespacho = value
        End Set
    End Property



    Public Property regNombre() As String
        Get
            Return _regNombre
        End Get
        Set(value As String)
            _regNombre = value
        End Set
    End Property




    Public Property Detraccion() As Decimal
        Get
            Return Me._Detraccion
        End Get
        Set(ByVal value As Decimal)
            Me._Detraccion = value
        End Set
    End Property





    Public Property IdTipoPV() As Integer
        Get
            Return Me._IdTipoPV
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoPV = value
        End Set
    End Property

    Public Property Retencion() As Decimal
        Get
            Return Me._Retencion
        End Get
        Set(ByVal value As Decimal)
            Me._Retencion = value
        End Set
    End Property

    Public Property IdTipoAgente() As Integer
        Get
            Return Me._IdTipoAgente
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoAgente = value
        End Set
    End Property

    Public Property TasaAgente() As Decimal
        Get
            Return Me._TasaAgente
        End Get
        Set(ByVal value As Decimal)
            Me._TasaAgente = value
        End Set
    End Property


    Public Property Vuelto() As Decimal
        Get
            Return Me._Vuelto
        End Get
        Set(ByVal value As Decimal)
            Me._Vuelto = value
        End Set
    End Property

    Public Property Percepcion() As Decimal
        Get
            Return Me._Percepcion
        End Get
        Set(ByVal value As Decimal)
            Me._Percepcion = value
        End Set
    End Property


    Public Property NomMoneda() As String
        Get
            Return Me._NomMoneda
        End Get
        Set(ByVal value As String)
            Me._NomMoneda = value
        End Set
    End Property


    Public Property FactorMov() As Integer
        Get
            Return Me._FactorMov
        End Get
        Set(ByVal value As Integer)
            Me._FactorMov = value
        End Set
    End Property

    Public Property IdArea() As Integer
        Get
            Return Me._IdArea
        End Get
        Set(ByVal value As Integer)
            Me._IdArea = value
        End Set
    End Property

    Public Property NomAlmacen() As String
        Get
            Return Me._NomAlmacen
        End Get
        Set(ByVal value As String)
            Me._NomAlmacen = value
        End Set
    End Property

    Public Property NomEstadoDocumento() As String
        Get
            Return Me._NomEstadoDocumento
        End Get
        Set(ByVal value As String)
            Me._NomEstadoDocumento = value
        End Set
    End Property

    Public Property NomTipoOperacion() As String
        Get
            Return Me._NomTipoOperacion
        End Get
        Set(ByVal value As String)
            Me._NomTipoOperacion = value
        End Set
    End Property

    Public Property NomTipoDocumento() As String
        Get
            Return Me._NomTipoDocumento
        End Get
        Set(ByVal value As String)
            Me._NomTipoDocumento = value
        End Set
    End Property

    Public Property Id() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property

    Public Property IdAlmacen() As Integer
        Get
            Return Me._IdAlmacen
        End Get
        Set(ByVal value As Integer)
            Me._IdAlmacen = value
        End Set
    End Property

    Public Property Codigo() As String
        Get
            Return Me._doc_Codigo
        End Get
        Set(ByVal value As String)
            Me._doc_Codigo = value
        End Set
    End Property

    Public Property Serie() As String
        Get
            Return Me._doc_Serie
        End Get
        Set(ByVal value As String)
            Me._doc_Serie = value
        End Set
    End Property

    Public Property FechaEmision() As Date
        Get
            Return Me._doc_FechaEmision
        End Get
        Set(ByVal value As Date)
            Me._doc_FechaEmision = value
        End Set
    End Property

    Public Property FechaIniTraslado() As Date
        Get
            Return Me._doc_FechaIniTraslado
        End Get
        Set(ByVal value As Date)
            Me._doc_FechaIniTraslado = value
        End Set
    End Property

    Public Property FechaRegistro() As Date
        Get
            Return Me._doc_FechaRegistro
        End Get
        Set(ByVal value As Date)
            Me._doc_FechaRegistro = value
        End Set
    End Property

    Public Property FechaAEntregar() As Date
        Get
            Return Me._doc_FechaAentregar
        End Get
        Set(ByVal value As Date)
            Me._doc_FechaAentregar = value
        End Set
    End Property

    Public Property FechaEntrega() As Date
        Get
            Return Me._doc_FechaEntrega
        End Get
        Set(ByVal value As Date)
            Me._doc_FechaEntrega = value
        End Set
    End Property

    Public Property FechaVenc() As Date
        Get
            Return Me._doc_FechaVenc
        End Get
        Set(ByVal value As Date)
            Me._doc_FechaVenc = value
        End Set
    End Property

    Public Property ImporteTotal() As Decimal
        Get
            Return Me._doc_ImporteTotal
        End Get
        Set(ByVal value As Decimal)
            Me._doc_ImporteTotal = value
        End Set
    End Property

    Public Property Descuento() As Decimal
        Get
            Return Me._doc_Descuento
        End Get
        Set(ByVal value As Decimal)
            Me._doc_Descuento = value
        End Set
    End Property

    Public Property SubTotal() As Decimal
        Get
            Return Me._doc_SubTotal
        End Get
        Set(ByVal value As Decimal)
            Me._doc_SubTotal = value
        End Set
    End Property

    Public Property IGV() As Decimal
        Get
            Return Me._doc_Igv
        End Get
        Set(ByVal value As Decimal)
            Me._doc_Igv = value
        End Set
    End Property

    Public Property Total() As Decimal
        Get
            Return Me._doc_Total
        End Get
        Set(ByVal value As Decimal)
            Me._doc_Total = value
        End Set
    End Property
    Public Property TotalLetras() As String
        Get
            Return _doc_TotalLetras
        End Get
        Set(ByVal value As String)
            _doc_TotalLetras = value
        End Set
    End Property
    Public Property TotalAPagar() As Decimal
        Get
            Return Me._doc_TotalAPagar
        End Get
        Set(ByVal value As Decimal)
            Me._doc_TotalAPagar = value
        End Set
    End Property

    Public Property ValorReferencial() As Decimal
        Get
            Return Me._doc_ValorReferencial
        End Get
        Set(ByVal value As Decimal)
            Me._doc_ValorReferencial = value
        End Set
    End Property

    Public Property Utilidad() As Decimal
        Get
            Return Me._doc_Utilidad
        End Get
        Set(ByVal value As Decimal)
            Me._doc_Utilidad = value
        End Set
    End Property

    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Public Property IdUsuario() As Integer
        Get
            Return Me._IdUsuario
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuario = value
        End Set
    End Property

    Public Property IdTransportista() As Integer
        Get
            Return Me._IdTransportista
        End Get
        Set(ByVal value As Integer)
            Me._IdTransportista = value
        End Set
    End Property


    Public Property IdRemitente() As Integer
        Get
            Return Me._IdRemitente
        End Get
        Set(ByVal value As Integer)
            Me._IdRemitente = value
        End Set
    End Property

    Public Property IdDestinatario() As Integer
        Get
            Return Me._IdDestinatario
        End Get
        Set(ByVal value As Integer)
            Me._IdDestinatario = value
        End Set
    End Property

    Public Property IdEstadoDoc() As Integer
        Get
            Return Me._IdEstadoDoc
        End Get
        Set(ByVal value As Integer)
            Me._IdEstadoDoc = value
        End Set
    End Property
    Public Property IdEstadoCancelacion() As Integer
        Get
            Return Me._IdEstadoCancelacion
        End Get
        Set(ByVal value As Integer)
            Me._IdEstadoCancelacion = value
        End Set
    End Property
    Public Property IdEstadoEntrega() As Integer
        Get
            Return Me._IdEstadoEntrega
        End Get
        Set(ByVal value As Integer)
            Me._IdEstadoEntrega = value
        End Set
    End Property
    Public Property IdCondicionPago() As Integer
        Get
            Return Me._IdCondicionPago
        End Get
        Set(ByVal value As Integer)
            Me._IdCondicionPago = value
        End Set
    End Property

    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property

    Public Property LugarEntrega() As Integer
        Get
            Return Me._LugarEntrega
        End Get
        Set(ByVal value As Integer)
            Me._LugarEntrega = value
        End Set
    End Property

    Public Property IdTipoOperacion() As Integer
        Get
            Return Me._IdTipoOperacion
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoOperacion = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property IdSerie() As Integer
        Get
            Return Me._IdSerie
        End Get
        Set(ByVal value As Integer)
            Me._IdSerie = value
        End Set
    End Property

    Public Property ExportadoConta() As String
        Get
            Return Me._doc_ExportadoConta
        End Get
        Set(ByVal value As String)
            Me._doc_ExportadoConta = value
        End Set
    End Property

    Public Property NroVoucherConta() As String
        Get
            Return Me._doc_NroVoucherConta
        End Get
        Set(ByVal value As String)
            Me._doc_NroVoucherConta = value
        End Set
    End Property

    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property

    Public Property IdChofer() As Integer
        Get
            Return Me._IdChofer
        End Get
        Set(ByVal value As Integer)
            Me._IdChofer = value
        End Set
    End Property

    Public Property IdMotivoT() As Integer
        Get
            Return Me._IdMotivoT
        End Get
        Set(ByVal value As Integer)
            Me._IdMotivoT = value
        End Set
    End Property

    Public Property IdTipoDocumento() As Integer
        Get
            Return Me._IdTipoDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoDocumento = value
        End Set
    End Property

    Public Property IdVehiculo() As Integer
        Get
            Return Me._IdVehiculo
        End Get
        Set(ByVal value As Integer)
            Me._IdVehiculo = value
        End Set
    End Property



    Public Function getClone() As Entidades.Documento

        Dim objDocumentoNew As New Entidades.Documento

        With objDocumentoNew



            .Codigo = Me.Codigo
            .Descuento = Me.Descuento
            .ExportadoConta = Me.ExportadoConta
            .FechaAEntregar = Me.FechaAEntregar
            .FechaEmision = Me.FechaEmision
            .FechaEntrega = Me.FechaEntrega
            .FechaIniTraslado = Me.FechaIniTraslado
            .FechaRegistro = Me.FechaRegistro
            .FechaVenc = Me.FechaVenc
            .Id = Me.Id
            .IdAlmacen = Me.IdAlmacen
            .IdChofer = Me.IdChofer
            .IdCondicionPago = Me.IdCondicionPago
            .IdDestinatario = Me.IdDestinatario
            .IdEmpresa = Me.IdEmpresa
            .IdEstadoCancelacion = Me.IdEstadoCancelacion
            .IdEstadoDoc = Me.IdEstadoDoc
            .IdEstadoEntrega = Me.IdEstadoEntrega
            .IdMoneda = Me.IdMoneda
            .IdMotivoT = Me.IdMotivoT
            .IdPersona = Me.IdPersona
            .IdRemitente = Me.IdRemitente
            .IdSerie = Me.IdSerie
            .IdTienda = Me.IdTienda
            .IdTipoDocumento = Me.IdTipoDocumento
            .IdTipoOperacion = Me.IdTipoOperacion
            .IdTransportista = Me.IdTransportista
            .IdUsuario = Me.IdUsuario
            .IdVehiculo = Me.IdVehiculo
            .IGV = Me.IGV
            .ImporteTotal = Me.ImporteTotal
            .LugarEntrega = Me.LugarEntrega
            .NroVoucherConta = Me.NroVoucherConta
            .Serie = Me.Serie
            .SubTotal = Me.SubTotal
            .Total = Me.Total
            .TotalAPagar = Me.TotalAPagar
            .TotalLetras = Me.TotalLetras
            .Utilidad = Me.Utilidad
            .ValorReferencial = Me.ValorReferencial

            .IdArea = Me.IdArea
            .FactorMov = Me.FactorMov
            .Vuelto = Me.Vuelto
            .IdTipoAgente = Me.IdTipoAgente
            .TasaAgente = Me.TasaAgente
            .Retencion = Me.Retencion
            .NomMoneda = Me.NomMoneda
            .Percepcion = Me.Percepcion
            .IdCaja = Me.IdCaja
            .IdTipoPV = Me.IdTipoPV
            .IdCaja = Me.IdCaja
            .IdAlmacen = Me.IdAlmacen

            .IdMedioPagoCredito = Me.IdMedioPagoCredito
            .CompPercepcion = Me.CompPercepcion
            .FechaCancelacion = Me.FechaCancelacion

            .IdUsuarioComision = Me.IdUsuarioComision
            .DeleteIdMaestroObra = Me.DeleteIdMaestroObra

        End With

        Return objDocumentoNew

    End Function

    Public Property EnviarTablaCuenta() As Boolean
        Get
            Return _enviarTablaCuenta
        End Get
        Set(ByVal value As Boolean)
            _enviarTablaCuenta = value
        End Set
    End Property

End Class
