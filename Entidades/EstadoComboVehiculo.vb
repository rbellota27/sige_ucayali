﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class EstadoComboVehiculo

    Private _IdEstadoCboVehiculo As Integer
    Private _NombreEstadoCboVehiculo As String
    'Private _EstadoComboVehiculo As String

    Public Property IdCboVehiculo() As Integer
        Get
            Return Me._IdEstadoCboVehiculo
        End Get
        Set(ByVal value As Integer)
            Me._IdEstadoCboVehiculo = value
        End Set
    End Property

    Public Property NombreEstadoVahiculo() As String
        Get
            Return Me._NombreEstadoCboVehiculo
        End Get
        Set(ByVal value As String)
            Me._NombreEstadoCboVehiculo = value
        End Set
    End Property

    'Public Property EstadoCboVehiculo() As String
    '    Get
    '        Return Me._EstadoComboVehiculo
    '    End Get
    '    Set(ByVal value As String)
    '        Me._EstadoComboVehiculo = value
    '    End Set
    'End Property
End Class
