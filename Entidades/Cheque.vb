﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'*********************   MARTES 15062010

Public Class Cheque

    Private _IdMedioPago As Integer
    Private _IdMoneda As Integer
    Private _ch_Monto As Decimal
    Private _IdBanco As Integer
    Private _ch_Numero As String
    Private _ch_FechaCobrar As Date
    Private _ch_EstadoMov As String
    Private _ch_Estado As Boolean
    Private _ch_Observacion As String
    Private _IdCliente As Integer
    Private _IdUsuario As Integer
    Private _IdUsuarioSupervisor As Integer
    Private _ch_FechaMov As Date
    Private _ch_FechaRegistro As Date
    Private _IdCheque As Integer
    Private _IdTipoMovimiento As Integer

    Private _EstadoMov_Desc As String
    Private _MedioPago As String
    Private _Banco As String
    Private _Moneda As String
    Private _TipoMovimiento As String

    Private _IdMedioPagoInterfaz As Integer

    Private _IdEmpresa As Integer
    Private _IdTienda As Integer

    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property IdMedioPagoInterfaz() As Integer
        Get
            Return Me._IdMedioPagoInterfaz
        End Get
        Set(ByVal value As Integer)
            Me._IdMedioPagoInterfaz = value
        End Set
    End Property



    Public Property EstadoMov_Desc() As String
        Get
            Return Me._EstadoMov_Desc
        End Get
        Set(ByVal value As String)
            Me._EstadoMov_Desc = value
        End Set
    End Property

    Public Property MedioPago() As String
        Get
            Return Me._MedioPago
        End Get
        Set(ByVal value As String)
            Me._MedioPago = value
        End Set
    End Property

    Public Property Banco() As String
        Get
            Return Me._Banco
        End Get
        Set(ByVal value As String)
            Me._Banco = value
        End Set
    End Property

    Public Property Moneda() As String
        Get
            Return Me._Moneda
        End Get
        Set(ByVal value As String)
            Me._Moneda = value
        End Set
    End Property

    Public Property TipoMovimiento() As String
        Get
            Return Me._TipoMovimiento
        End Get
        Set(ByVal value As String)
            Me._TipoMovimiento = value
        End Set
    End Property



    Public Property IdTipoMovimiento() As Integer
        Get
            Return Me._IdTipoMovimiento
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoMovimiento = value
        End Set
    End Property

    Public Property IdCheque() As Integer
        Get
            Return Me._IdCheque
        End Get
        Set(ByVal value As Integer)
            Me._IdCheque = value
        End Set
    End Property



    Public Property IdMedioPago() As Integer
        Get
            Return Me._IdMedioPago
        End Get
        Set(ByVal value As Integer)
            Me._IdMedioPago = value
        End Set
    End Property

    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property

    Public Property Monto() As Decimal
        Get
            Return Me._ch_Monto
        End Get
        Set(ByVal value As Decimal)
            Me._ch_Monto = value
        End Set
    End Property

    Public Property IdBanco() As Integer
        Get
            Return Me._IdBanco
        End Get
        Set(ByVal value As Integer)
            Me._IdBanco = value
        End Set
    End Property

    Public Property Numero() As String
        Get
            Return Me._ch_Numero
        End Get
        Set(ByVal value As String)
            Me._ch_Numero = value
        End Set
    End Property

    Public Property FechaCobrar() As Date
        Get
            Return Me._ch_FechaCobrar
        End Get
        Set(ByVal value As Date)
            Me._ch_FechaCobrar = value
        End Set
    End Property

    Public Property EstadoMov() As String
        Get
            Return Me._ch_EstadoMov
        End Get
        Set(ByVal value As String)
            Me._ch_EstadoMov = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return Me._ch_Estado
        End Get
        Set(ByVal value As Boolean)
            Me._ch_Estado = value
        End Set
    End Property



    Public Property Observacion() As String
        Get
            Return Me._ch_Observacion
        End Get
        Set(ByVal value As String)
            Me._ch_Observacion = value
        End Set
    End Property

    Public Property IdCliente() As Integer
        Get
            Return Me._IdCliente
        End Get
        Set(ByVal value As Integer)
            Me._IdCliente = value
        End Set
    End Property

    Public Property IdUsuario() As Integer
        Get
            Return Me._IdUsuario
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuario = value
        End Set
    End Property

    Public Property IdUsuarioSupervisor() As Integer
        Get
            Return Me._IdUsuarioSupervisor
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuarioSupervisor = value
        End Set
    End Property

    Public Property FechaMov() As Date
        Get
            Return Me._ch_FechaMov
        End Get
        Set(ByVal value As Date)
            Me._ch_FechaMov = value
        End Set
    End Property

    Public Property FechaRegistro() As Date
        Get
            Return Me._ch_FechaRegistro
        End Get
        Set(ByVal value As Date)
            Me._ch_FechaRegistro = value
        End Set
    End Property


End Class
