﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Tienda_Concepto

    Private _IdConcepto As Integer
    Private _IdTienda As Integer
    Private _ObjTienda As Object
    Private _IdMoneda As Integer
    Private _ObjMoneda As Object
    Private _tie_monto As Decimal
    Private _tie_estado As Boolean
    Private _cadTienda As String
    Private _cadMoneda As String


    Public Property cadTienda() As String
        Get
            Return _cadTienda
        End Get
        Set(ByVal value As String)
            _cadTienda = value
        End Set
    End Property

    Public Property tie_estado() As Boolean
        Get
            Return _tie_estado
        End Get
        Set(ByVal value As Boolean)
            _tie_estado = value
        End Set
    End Property

    Public Property tie_monto() As Decimal
        Get
            Return _tie_monto
        End Get
        Set(ByVal value As Decimal)
            _tie_monto = value
        End Set
    End Property

    Public Property ObjMoneda() As Object
        Get
            If cadMoneda <> "" Then
                Dim lista As New List(Of Entidades.Moneda)
                Dim cortar() As String = cadMoneda.Split("=")
                For x As Integer = 0 To cortar.Length - 1
                    Dim obj As New Entidades.Moneda
                    Dim separar() As String = cortar(x).Split(",")
                    obj.Id = CInt(separar(0))
                    obj.Simbolo = CStr(separar(1))
                    lista.Add(obj)
                Next
                Return lista
            End If
            Return _ObjMoneda
        End Get
        Set(ByVal value As Object)
            _ObjMoneda = value
        End Set
    End Property

    Public Property cadMoneda() As String
        Get
            Return _cadMoneda
        End Get
        Set(ByVal value As String)
            _cadMoneda = value
        End Set
    End Property

    Public Property IdMoneda() As Integer
        Get
            Return _IdMoneda
        End Get
        Set(ByVal value As Integer)
            _IdMoneda = value
        End Set
    End Property

    Public Property ObjTienda() As Object
        Get
            If cadTienda <> "" Then
                Dim lista As New List(Of Entidades.Tienda)
                Dim cortar() As String = cadTienda.Split("~")
                For x As Integer = 0 To cortar.Length - 1
                    Dim obj As New Entidades.Tienda
                    Dim separar() As String = cortar(x).Split("|")
                    obj.Id = CInt(separar(0))
                    obj.Nombre = CStr(separar(1))
                    lista.Add(obj)
                Next
                Return lista
            End If
            Return _ObjTienda
        End Get
        Set(ByVal value As Object)
            _ObjTienda = value
        End Set
    End Property

    Public Property IdConcepto() As Integer
        Get
            Return _IdConcepto
        End Get
        Set(ByVal value As Integer)
            _IdConcepto = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return _IdTienda
        End Get
        Set(ByVal value As Integer)
            _IdTienda = value
        End Set
    End Property



End Class
