'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Correo
    Private _IdCorreo As Integer
    Private _corr_Nombre As String
    Private _IdPersona As Integer
    Private _IdTipoCorreo As Integer
    Public Property Id() As Integer
        Get
            Return Me._IdCorreo
        End Get
        Set(ByVal value As Integer)
            Me._IdCorreo = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._corr_Nombre
        End Get
        Set(ByVal value As String)
            Me._corr_Nombre = value
        End Set
    End Property

    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Public Property IdTipoCorreo() As Integer
        Get
            Return Me._IdTipoCorreo
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoCorreo = value
        End Set
    End Property
End Class
