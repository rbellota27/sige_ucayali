'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Serie
    Private _IdTienda As Integer
    Private _IdEmpresa As Integer
    Private _IdSerie As Integer
    Private _ser_Numero As String
    Private _ser_Inicio As Integer
    Private _ser_Cantidad As Integer
    Private _ser_FechaAlta As Date
    Private _ser_Estado As String
    Private _ser_Longitud As Integer
    Private _IdTipoDocumento As Integer
    Private _NomEmpresa As String
    Private _NomTienda As String
    Private _NomTipoDoc As String
    Private _descEstado As String
    Public ReadOnly Property DescFechaAlta() As String
        Get
            If Me._ser_FechaAlta = Nothing Then
                Return ""
            Else
                Return Format(Me._ser_FechaAlta, "dd/MM/yyyy")
            End If
        End Get
    End Property
    Public ReadOnly Property DescEstado() As String
        Get
            Me._descEstado = "Activo"
            If Me._ser_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public Property NomEmpresa() As String
        Get
            Return Me._NomEmpresa
        End Get
        Set(ByVal value As String)
            Me._NomEmpresa = value
        End Set
    End Property

    Public Property NomTienda() As String
        Get
            Return Me._NomTienda
        End Get
        Set(ByVal value As String)
            Me._NomTienda = value
        End Set
    End Property

    Public Property NomTipoDoc() As String
        Get
            Return Me._NomTipoDoc
        End Get
        Set(ByVal value As String)
            Me._NomTipoDoc = value
        End Set
    End Property
    Public Property IdSerie() As Integer
        Get
            Return Me._IdSerie
        End Get
        Set(ByVal value As Integer)
            Me._IdSerie = value
        End Set
    End Property

    Public Property Numero() As String
        Get
            Return Me._ser_Numero
        End Get
        Set(ByVal value As String)
            Me._ser_Numero = value
        End Set
    End Property

    Public Property Inicio() As Integer
        Get
            Return Me._ser_Inicio
        End Get
        Set(ByVal value As Integer)
            Me._ser_Inicio = value
        End Set
    End Property

    Public Property Cantidad() As Integer
        Get
            Return Me._ser_Cantidad
        End Get
        Set(ByVal value As Integer)
            Me._ser_Cantidad = value
        End Set
    End Property

    Public Property FechaAlta() As Date
        Get
            Return Me._ser_FechaAlta
        End Get
        Set(ByVal value As Date)
            Me._ser_FechaAlta = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._ser_Estado
        End Get
        Set(ByVal value As String)
            Me._ser_Estado = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property

    Public Property Longitud() As Integer
        Get
            Return Me._ser_Longitud
        End Get
        Set(ByVal value As Integer)
            Me._ser_Longitud = value
        End Set
    End Property

    Public Property IdTipoDocumento() As Integer
        Get
            Return Me._IdTipoDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoDocumento = value
        End Set
    End Property
End Class
