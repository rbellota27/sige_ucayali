﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Chigne Bazan, Dany
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 28-01-2010
'Hora    : 12: 20 pm
'*************************************************
Public Class ParametroGeneral 'declato las variables con su tipo datos
    Private _IdParametro As Integer
    Private _par_Descripcion As String
    Private _par_Valor As Decimal
    Private _par_Estado As Boolean
    Private _IdArea As Integer
    Private _objArea As Object
    Public Sub New()
    End Sub

    Public Property IdParametro() As Integer
        Get
            Return Me._IdParametro
        End Get
        Set(ByVal value As Integer)
            Me._IdParametro = value
        End Set
    End Property

    Public Property par_Descripcion() As String
        Get
            Return Me._par_Descripcion
        End Get
        Set(ByVal value As String)
            Me._par_Descripcion = value
        End Set
    End Property
    Public Property par_valor() As Decimal
        Get
            Return Me._par_Valor
        End Get
        Set(ByVal value As Decimal)
            _par_Valor = value
        End Set
    End Property
    Public Property par_Estado() As Boolean
        Get
            Return Me._par_Estado
        End Get
        Set(ByVal value As Boolean)
            _par_Estado = value
        End Set
    End Property
    Public Property IdArea() As Integer
        Get
            Return Me._IdArea
        End Get
        Set(ByVal value As Integer)
            Me._IdArea = value
        End Set
    End Property

    Public Sub New(ByVal idparametro As Integer, ByVal par_descripcion As String, ByVal par_valor As Decimal, ByVal par_estado As Boolean, ByVal idarea As Integer)
        Me._IdParametro = idparametro
        Me._par_Descripcion = par_descripcion
        Me._par_Valor = par_valor
        Me._par_Estado = par_estado
        Me._IdArea = idarea
    End Sub

    Public Property objArea() As Object
        Get
            Return _objArea
        End Get
        Set(ByVal value As Object)
            _objArea = value
        End Set
    End Property

End Class
