﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Public Class DsctoVolVtas


    Private _IdDsctoVolVtas As Integer
    Private _IdEmpresa As Integer
    Private _IdTienda As Integer
    Private _IdPerfil As Integer
    Private _IdCondicionPago As Integer
    Private _IdMedioPago As Integer
    Private _IdLinea As Integer
    Private _IdSubLinea As Integer
    Private _IdProducto As Integer
    Private _Dscto As Decimal
    Private _CantMin As Decimal
    Private _CantMax As Decimal
    Private _FechaIni As Date
    Private _FechaFin As Date
    Private _Excepcion As Boolean
    Private _Estado As Boolean

    Private _CodigoProducto As String
    Private _Producto As String
    Private _ProductoUM As String
    Private _ProductoPV As Decimal
    Private _PrecioDscto As Decimal
    Private _NoAfectar As Boolean
    Private _Moneda As String

    Private _Empresa As String
    Private _Tienda As String
    Private _Perfil As String
    Private _CondicionPago As String
    Private _MedioPago As String
    Private _Linea As String
    Private _SubLinea As String
    Private _IdTipoPV As Integer
    Private _IdUsuario As Integer
    Private _TipoPV As String

    Sub New()
    End Sub

    Sub New(ByVal V_IdDsctoVolVtas As Integer)
        _IdDsctoVolVtas = V_IdDsctoVolVtas
    End Sub

    Sub New(ByVal V_IdDsctoVolVtas As Integer, ByVal V_Estado As Boolean, ByVal V_FechaFin As Date)
        _IdDsctoVolVtas = V_IdDsctoVolVtas
        _Estado = V_Estado
        _FechaFin = V_FechaFin
    End Sub

    Sub New(ByVal V_IdDsctoVolVtas As Integer, ByVal V_Estado As Boolean, ByVal V_FechaIni As Date, ByVal V_FechaFin As Date, ByVal V_CantidadMin As Decimal, ByVal V_cantidadMax As Decimal, ByVal V_PorcentajeDscto As Decimal)
        _IdDsctoVolVtas = V_IdDsctoVolVtas
        _Estado = V_Estado
        _FechaIni = V_FechaIni
        _FechaFin = V_FechaFin
        _CantMin = V_CantidadMin
        _CantMax = V_cantidadMax
        _Dscto = V_PorcentajeDscto
    End Sub


    Public Property TipoPV() As String
        Get
            Return _TipoPV
        End Get
        Set(ByVal value As String)
            _TipoPV = value
        End Set
    End Property

    Public Property IdUsuario() As Integer
        Get
            Return _IdUsuario
        End Get
        Set(ByVal value As Integer)
            _IdUsuario = value
        End Set
    End Property

    Public Property IdTipoPV() As Integer
        Get
            Return _IdTipoPV
        End Get
        Set(ByVal value As Integer)
            _IdTipoPV = value
        End Set
    End Property

    Public Property Empresa() As String
        Get
            Return _Empresa
        End Get
        Set(ByVal value As String)
            _Empresa = value
        End Set
    End Property

    Public Property Tienda() As String
        Get
            Return _Tienda
        End Get
        Set(ByVal value As String)
            _Tienda = value
        End Set
    End Property

    Public Property Perfil() As String
        Get
            Return _Perfil
        End Get
        Set(ByVal value As String)
            _Perfil = value
        End Set
    End Property

    Public Property CondicionPago() As String
        Get
            Return _CondicionPago
        End Get
        Set(ByVal value As String)
            _CondicionPago = value
        End Set
    End Property

    Public Property MedioPago() As String
        Get
            Return _MedioPago
        End Get
        Set(ByVal value As String)
            _MedioPago = value
        End Set
    End Property

    Public Property Linea() As String
        Get
            Return _Linea
        End Get
        Set(ByVal value As String)
            _Linea = value
        End Set
    End Property

    Public Property SubLinea() As String
        Get
            Return _SubLinea
        End Get
        Set(ByVal value As String)
            _SubLinea = value
        End Set
    End Property

    Public Property Moneda() As String
        Get
            Return _Moneda
        End Get
        Set(ByVal value As String)
            _Moneda = value
        End Set
    End Property

    Public Property NoAfectar() As Boolean
        Get
            Return _NoAfectar
        End Get
        Set(ByVal value As Boolean)
            _NoAfectar = value
        End Set
    End Property

    Public Property CodigoProducto() As String
        Get
            Return _CodigoProducto
        End Get
        Set(ByVal value As String)
            _CodigoProducto = value
        End Set
    End Property

    Public Property Producto() As String
        Get
            Return _Producto
        End Get
        Set(ByVal value As String)
            _Producto = value
        End Set
    End Property

    Public Property ProductoUM() As String
        Get
            Return _ProductoUM
        End Get
        Set(ByVal value As String)
            _ProductoUM = value
        End Set
    End Property

    Public Property ProductoPV() As Decimal
        Get
            Return _ProductoPV
        End Get
        Set(ByVal value As Decimal)
            _ProductoPV = value
        End Set
    End Property

    Public Property PrecioDscto() As Decimal
        Get
            Return _PrecioDscto
        End Get
        Set(ByVal value As Decimal)
            _PrecioDscto = value
        End Set
    End Property


    Public Property IdDsctoVolVtas() As Integer
        Get
            Return _IdDsctoVolVtas
        End Get
        Set(ByVal value As Integer)
            _IdDsctoVolVtas = value
        End Set
    End Property

    Public Property IdEmpresa() As Integer
        Get
            Return _IdEmpresa
        End Get
        Set(ByVal value As Integer)
            _IdEmpresa = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return _IdTienda
        End Get
        Set(ByVal value As Integer)
            _IdTienda = value
        End Set
    End Property

    Public Property IdPerfil() As Integer
        Get
            Return _IdPerfil
        End Get
        Set(ByVal value As Integer)
            _IdPerfil = value
        End Set
    End Property

    Public Property IdCondicionPago() As Integer
        Get
            Return _IdCondicionPago
        End Get
        Set(ByVal value As Integer)
            _IdCondicionPago = value
        End Set
    End Property

    Public Property IdMedioPago() As Integer
        Get
            Return _IdMedioPago
        End Get
        Set(ByVal value As Integer)
            _IdMedioPago = value
        End Set
    End Property

    Public Property IdLinea() As Integer
        Get
            Return _IdLinea
        End Get
        Set(ByVal value As Integer)
            _IdLinea = value
        End Set
    End Property

    Public Property IdSubLinea() As Integer
        Get
            Return _IdSubLinea
        End Get
        Set(ByVal value As Integer)
            _IdSubLinea = value
        End Set
    End Property

    Public Property IdProducto() As Integer
        Get
            Return _IdProducto
        End Get
        Set(ByVal value As Integer)
            _IdProducto = value
        End Set
    End Property

    Public Property Dscto() As Decimal
        Get
            Return _Dscto
        End Get
        Set(ByVal value As Decimal)
            _Dscto = value
        End Set
    End Property

    Public Property CantMin() As Decimal
        Get
            Return _CantMin
        End Get
        Set(ByVal value As Decimal)
            _CantMin = value
        End Set
    End Property

    Public Property CantMax() As Decimal
        Get
            Return _CantMax
        End Get
        Set(ByVal value As Decimal)
            _CantMax = value
        End Set
    End Property

    Public Property FechaIni() As Date
        Get
            Return _FechaIni
        End Get
        Set(ByVal value As Date)
            _FechaIni = value
        End Set
    End Property

    Public Property FechaFin() As Date
        Get
            Return _FechaFin
        End Get
        Set(ByVal value As Date)
            _FechaFin = value
        End Set
    End Property

    Public Property Excepcion() As Boolean
        Get
            Return _Excepcion
        End Get
        Set(ByVal value As Boolean)
            _Excepcion = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return _Estado
        End Get
        Set(ByVal value As Boolean)
            _Estado = value
        End Set
    End Property



End Class
