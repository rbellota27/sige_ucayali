﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class MotivoGasto
    Private _IdMotGasto As Integer
    Private _Nombre_MG As String
    Private _NombreCorto_MG As String
    Private _Estado_MG As String
    Private _descEstado As String
    Private _cct_Estado As String
    Private _IdConcepto As Integer
    Private _con_Nombre As String

    Sub New()
    End Sub
    Public Sub New(ByVal IdMotGasto As Integer, ByVal Nombre As String)
        Me.Id = IdMotGasto
        Me.Nombre_MG = Nombre
    End Sub
    Public Sub New(ByVal IdMotGasto As Integer, ByVal Nombre As String, ByVal NombreCorto As String, ByVal estado As Boolean)
        Me._IdMotGasto = IdMotGasto
        Me._Nombre_MG = Nombre
        Me._NombreCorto_MG = NombreCorto
        Me._Estado_MG = estado
    End Sub
    Public ReadOnly Property DesEstado_MG()
        Get
            Me._descEstado = "Activo"
            If Me._Estado_MG = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public Property Id() As Integer
        Get
            Return Me._IdMotGasto
        End Get
        Set(ByVal value As Integer)
            Me._IdMotGasto = value
        End Set
    End Property
    Public Property Nombre_MG() As String
        Get
            Return Me._Nombre_MG

        End Get
        Set(ByVal value As String)
            Me._Nombre_MG = value
        End Set
    End Property
    Public Property NombreCorto_MG() As String
        Get
            Return Me._NombreCorto_MG
        End Get
        Set(ByVal value As String)
            Me._NombreCorto_MG = value
        End Set
    End Property
    Public Property Estado_MG() As String
        Get
            Return Me._Estado_MG
        End Get
        Set(ByVal value As String)
            Me._Estado_MG = value
        End Set
    End Property
    Public ReadOnly Property DescEstado()
        Get
            _descEstado = "Activo"
            If _Estado_MG = "0" Then
                _descEstado = "Inactivo"
            End If
            Return _descEstado
        End Get
    End Property

    Public Property IdConcepto() As Integer
        Get
            Return Me._IdConcepto
        End Get
        Set(ByVal value As Integer)
            Me._IdConcepto = value
        End Set
    End Property
    Public Property con_Nombre() As String
        Get
            Return _con_Nombre
        End Get
        Set(ByVal value As String)
            _con_Nombre = value
        End Set
    End Property



End Class
