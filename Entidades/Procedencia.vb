﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Procedencia
    Private _IdProcedencia As Integer
    Private _proc_Nombre As String
    Private _proc_Estado As Boolean
    Private _IdPais As Integer
    Public Sub New()
    End Sub
    Public Sub New(ByVal idprocedencia As String, ByVal nombre As String, ByVal estado As Boolean)
        Me._IdProcedencia = idprocedencia
        Me._proc_Nombre = nombre
        Me._proc_Estado = estado
    End Sub
    Public Property IdProcedencia() As String
        Get
            Return Me._IdProcedencia
        End Get
        Set(ByVal value As String)
            Me._IdProcedencia = value
        End Set
    End Property
    Public Property IdPais() As Integer
        Get
            Return Me._IdPais
        End Get
        Set(ByVal value As Integer)
            Me._IdPais = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return Me._proc_Nombre
        End Get
        Set(ByVal value As String)
            Me._proc_Nombre = value
        End Set
    End Property
    Public Property Estado() As Boolean
        Get
            Return Me._proc_Estado
        End Get
        Set(ByVal value As Boolean)
            Me._proc_Estado = value
        End Set
    End Property
End Class
