﻿Public Class be_usuario
    Private _IdPersona As Integer
    Private _Nombre As String
    Private _Dni As String
    Private _Ruc As String
    Private _dir_Direccion As String

    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property
    Public Property Dni() As String
        Get
            Return _Dni
        End Get
        Set(ByVal value As String)
            _Dni = value
        End Set
    End Property
    Public Property Ruc() As String
        Get
            Return _Ruc
        End Get
        Set(ByVal value As String)
            _Ruc = value
        End Set
    End Property
    Public Property Direccion() As String
        Get
            Return _dir_Direccion
        End Get
        Set(ByVal value As String)
            _dir_Direccion = value
        End Set
    End Property
End Class
