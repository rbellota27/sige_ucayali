'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class RelacionDocumento
    Inherits Documento
    Private _IdDocumento1 As Integer
    Private _IdDocumento2 As Integer

    Private _IdDocumento As Integer
    Private _IdDocumentoRef As Integer
    Private _Descripcion As String
    Private _IdTipoDocumento As Integer
    Private _SujetoDetraccion As Boolean = False
    Private _montoAmortizado As Decimal = 0
    Private _montoDeuda As Decimal = 0

    Public Sub New()
    End Sub
    Public Sub New(ByVal iddocumento1 As Integer, ByVal iddocumento2 As Integer)
        Me._IdDocumento1 = iddocumento1
        Me._IdDocumento2 = iddocumento2
    End Sub

    Public Property IdDocumento() As Integer
        Get
            Return _IdDocumento
        End Get
        Set(ByVal value As Integer)
            _IdDocumento = value
        End Set
    End Property

    Public Property IdDocumentoRef() As Integer
        Get
            Return _IdDocumentoRef
        End Get
        Set(ByVal value As Integer)
            _IdDocumentoRef = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property

    Public Property IdTipoDocumento() As Integer
        Get
            Return _IdTipoDocumento
        End Get
        Set(ByVal value As Integer)
            _IdTipoDocumento = value
        End Set
    End Property

    Public Property IdDocumento1() As Integer
        Get
            Return Me._IdDocumento1
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento1 = value
        End Set
    End Property

    Public Property IdDocumento2() As Integer
        Get
            Return Me._IdDocumento2
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento2 = value
        End Set
    End Property

    Public Property sujetoDetraccion As Boolean
        Get
            Return Me._SujetoDetraccion
        End Get
        Set(ByVal value As Boolean)
            Me._SujetoDetraccion = value
        End Set
    End Property

    Public Property montoAmortizado() As Decimal
        Get
            Return Me._montoAmortizado
        End Get
        Set(ByVal value As Decimal)
            Me._montoAmortizado = value
        End Set
    End Property

    Public Property montoDeuda() As Decimal
        Get
            Return Me._montoDeuda
        End Get
        Set(ByVal value As Decimal)
            Me._montoDeuda = value
        End Set
    End Property
End Class
