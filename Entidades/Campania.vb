﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'******************   LUNES 31 MAYO 2010 HORA 03_23 PM

Public Class Campania
    Private _IdCampania As Integer
    Private _IdEmpresa As Integer
    Private _IdTienda As Integer
    Private _cam_Descripcion As String
    Private _cam_FechaInicio As Date
    Private _cam_FechaFin As Date
    Private _cam_Estado As Boolean
    Private _IdUsuarioInsert As Integer
    Private _cam_FechaInsert As Date
    Private _IdUsuarioUpdate As Integer
    Private _cam_FechaUpdate As Date
    Private _Empresa As String
    Private _Tienda As String

    Public Property IdCampania() As Integer
        Get
            Return Me._IdCampania
        End Get
        Set(ByVal value As Integer)
            Me._IdCampania = value
        End Set
    End Property

    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._cam_Descripcion
        End Get
        Set(ByVal value As String)
            Me._cam_Descripcion = value
        End Set
    End Property

    Public Property FechaInicio() As Date
        Get
            Return Me._cam_FechaInicio
        End Get
        Set(ByVal value As Date)
            Me._cam_FechaInicio = value
        End Set
    End Property

    Public Property FechaFin() As Date
        Get
            Return Me._cam_FechaFin
        End Get
        Set(ByVal value As Date)
            Me._cam_FechaFin = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return Me._cam_Estado
        End Get
        Set(ByVal value As Boolean)
            Me._cam_Estado = value
        End Set
    End Property

    Public Property IdUsuarioInsert() As Integer
        Get
            Return Me._IdUsuarioInsert
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuarioInsert = value
        End Set
    End Property

    Public Property FechaInsert() As Date
        Get
            Return Me._cam_FechaInsert
        End Get
        Set(ByVal value As Date)
            Me._cam_FechaInsert = value
        End Set
    End Property

    Public Property IdUsuarioUpdate() As Integer
        Get
            Return Me._IdUsuarioUpdate
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuarioUpdate = value
        End Set
    End Property

    Public Property FechaUpdate() As Date
        Get
            Return Me._cam_FechaUpdate
        End Get
        Set(ByVal value As Date)
            Me._cam_FechaUpdate = value
        End Set
    End Property

    Public Property Empresa() As String
        Get
            Return Me._Empresa
        End Get
        Set(ByVal value As String)
            Me._Empresa = value
        End Set
    End Property

    Public Property Tienda() As String
        Get
            Return Me._Tienda
        End Get
        Set(ByVal value As String)
            Me._Tienda = value
        End Set
    End Property
End Class
