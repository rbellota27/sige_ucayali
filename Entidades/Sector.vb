﻿Public Class Sector
    Private _idSector As Integer = 0
    Private _nombre As String = String.Empty
    Private _descripcion As String = String.Empty
    Private _idControlador As Integer = 0
    Private _estado As String = String.Empty
    Private _idSublineas As String = String.Empty

    Public Property idSector() As Integer
        Get
            Return _idSector
        End Get
        Set(ByVal value As Integer)
            _idSector = value
        End Set
    End Property

    Public Property nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Property descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

    Public Property idControlador() As Integer
        Get
            Return _idControlador
        End Get
        Set(ByVal value As Integer)
            _idControlador = value
        End Set
    End Property

    Public Property estado() As String
        Get
            Return _estado
        End Get
        Set(ByVal value As String)
            _estado = value
        End Set
    End Property

    Public Property idSublineas() As String
        Get
            Return _idSublineas
        End Get
        Set(ByVal value As String)
            _idSublineas = value
        End Set
    End Property
End Class
