﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Chang Carnero, Edgar
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 30-Octubre-2009
'Hora    : 07:30 pm
'*************************************************
'*************************************************
'Autor   : Nagamine Molina, Jorge
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 29-Octubre-2009
'Hora    : 07:10 pm
'*************************************************
Public Class MedioPago_CondicionPago
    Private _IdCondicionPago As Integer
    Private _IdMedioPago As Integer
    Private _NomCondicionPago As String
    Public Property IdCondicionPago() As Integer
        Get
            Return _IdCondicionPago
        End Get
        Set(ByVal value As Integer)
            _IdCondicionPago = value
        End Set
    End Property
    Public Property IdMedioPago() As Integer
        Get
            Return _IdMedioPago
        End Get
        Set(ByVal value As Integer)
            _IdMedioPago = value
        End Set
    End Property
    Public Property NomMedioPago() As String
        Get
            Return _NomCondicionPago
        End Get
        Set(ByVal value As String)
            _NomCondicionPago = value
        End Set
    End Property
End Class
