'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'**************************   MARTES 11 MAYO 2010 HORA 10_45 AM
Public Class Concepto
    Private _IdConcepto As Integer
    Private _con_Nombre As String
    Private _con_Estado As String
    Private _con_CuentaContable As String
    Private _descEstado As String
    Private _descTipoUso As String
    Private _TipoUso As Integer
    Private _IdTipoGasto As Integer
    Private _NomTipoGasto As String
    Private _Moneda As String
    Private _PorcentDetraccion As Decimal
    Private _MontoMinimoDetraccion As Decimal
    Private _ConceptoAdelanto As Boolean
    Private _ConceptoCtaxRendir As Boolean
    Private _IdMoneda As Integer
    Private _Monto As Decimal
    Private _con_NoAfectoIGV As Boolean

    Public Property con_NoAfectoIGV() As Boolean
        Get
            Return Me._con_NoAfectoIGV
        End Get
        Set(ByVal value As Boolean)
            Me._con_NoAfectoIGV = value
        End Set
    End Property

    Public Property Monto() As Decimal
        Get
            Return Me._Monto
        End Get
        Set(ByVal value As Decimal)
            Me._Monto = value
        End Set
    End Property


    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property


    Public Sub New()
    End Sub
    

    Public Property Moneda() As String
        Get
            Return Me._Moneda
        End Get
        Set(ByVal value As String)
            Me._Moneda = value
        End Set
    End Property

    Public Property PorcentDetraccion() As Decimal
        Get
            Return Me._PorcentDetraccion
        End Get
        Set(ByVal value As Decimal)
            Me._PorcentDetraccion = value
        End Set
    End Property

    Public Property MontoMinimoDetraccion() As Decimal
        Get
            Return Me._MontoMinimoDetraccion
        End Get
        Set(ByVal value As Decimal)
            Me._MontoMinimoDetraccion = value
        End Set
    End Property

    Public Property ConceptoAdelanto() As Boolean
        Get
            Return Me._ConceptoAdelanto
        End Get
        Set(ByVal value As Boolean)
            Me._ConceptoAdelanto = value
        End Set
    End Property

    Public Property conceptoCtaxRendir() As Boolean
        Get
            Return Me._ConceptoCtaxRendir
        End Get
        Set(ByVal value As Boolean)
            Me._ConceptoCtaxRendir = value
        End Set
    End Property

    Public ReadOnly Property ConceptoAdelanto_Integer() As Integer
        Get
            If (Me.ConceptoAdelanto) Then
                Return 1
            Else
                Return 0
            End If
        End Get
    End Property

    Public ReadOnly Property DescConceptoAdelanto() As String
        Get
            If (Me.ConceptoAdelanto) Then
                Return "Activo"
            Else
                Return "Inactivo"
            End If
        End Get
    End Property

    Public Sub New(ByVal idconcepto As Integer, ByVal nombre As String)
        Me.Id = idconcepto
        Me.Nombre = nombre
    End Sub
    Public ReadOnly Property DescEstado()
        Get
            Me._descEstado = "Activo"
            If Me._con_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public ReadOnly Property DescTipoUso() As String
        Get
            If con_TipoUso = 1 Then _descTipoUso = "Solo recibo Ingreso"
            If con_TipoUso = -1 Then _descTipoUso = "Solo recibo Egreso"
            If con_TipoUso = 0 Then _descTipoUso = "Ambos"
            Return Me._descTipoUso
        End Get
    End Property
    Public Property Id() As Integer
        Get
            Return Me._IdConcepto
        End Get
        Set(ByVal value As Integer)
            Me._IdConcepto = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return Me._con_Nombre
        End Get
        Set(ByVal value As String)
            Me._con_Nombre = value
        End Set
    End Property
    Public Property Estado() As String
        Get
            Return Me._con_Estado
        End Get
        Set(ByVal value As String)
            Me._con_Estado = value
        End Set
    End Property
    Public Property CuentaContable() As String
        Get
            Return Me._con_CuentaContable
        End Get
        Set(ByVal value As String)
            Me._con_CuentaContable = value
        End Set
    End Property
    Public Property con_TipoUso() As Integer
        Get
            Return Me._TipoUso
        End Get
        Set(ByVal value As Integer)
            Me._TipoUso = value
        End Set
    End Property
    Public Property con_IdTipoGasto() As Integer
        Get
            Return _IdTipoGasto
        End Get
        Set(ByVal value As Integer)
            _IdTipoGasto = value
        End Set
    End Property
    Public Property NombreTipoGasto() As String
        Get
            Return _NomTipoGasto
        End Get
        Set(ByVal value As String)
            _NomTipoGasto = value
        End Set
    End Property

End Class
