'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class ProductoUM
    Private _IdProducto As Integer
    Private _IdUnidadMedida As Integer
    Private _pum_UnidadPrincipal As Boolean
    Private _pum_Equivalencia As Decimal
    Private _pum_Retazo As Boolean
    Private _PorcentRetazo As Decimal
    Private _Estado As String



    Private _NomUnidadMedida As String

    Public Property NomUnidadMedida() As String
        Get
            Return Me._NomUnidadMedida
        End Get
        Set(ByVal value As String)
            Me._NomUnidadMedida = value
        End Set
    End Property





    Public Property Estado() As String
        Get
            Return Me._Estado
        End Get
        Set(ByVal value As String)
            Me._Estado = value
        End Set
    End Property
    Public ReadOnly Property EstadoBool() As Boolean
        Get
            If Me._Estado = "1" Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    Public Property PorcentRetazo() As Decimal
        Get
            Return Me._PorcentRetazo
        End Get
        Set(ByVal value As Decimal)
            Me._PorcentRetazo = value
        End Set
    End Property
    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property

    Public Property IdUnidadMedida() As Integer
        Get
            Return Me._IdUnidadMedida
        End Get
        Set(ByVal value As Integer)
            Me._IdUnidadMedida = value
        End Set
    End Property

    Public Property UnidadPrincipal() As Boolean
        Get
            Return Me._pum_UnidadPrincipal
        End Get
        Set(ByVal value As Boolean)
            Me._pum_UnidadPrincipal = value
        End Set
    End Property

    Public Property Equivalencia() As Decimal
        Get
            Return Me._pum_Equivalencia
        End Get
        Set(ByVal value As Decimal)
            Me._pum_Equivalencia = value
        End Set
    End Property

    Public Property Retazo() As Boolean
        Get
            Return Me._pum_Retazo
        End Get
        Set(ByVal value As Boolean)
            Me._pum_Retazo = value
        End Set
    End Property
End Class
