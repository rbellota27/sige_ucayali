'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Recibo
    Private _IdRecibo As Integer
    Private _re_Codigo As String
    Private _re_Serie As String
    Private _re_Fecha As Date
    Private _re_Total As Decimal
    Private _IdMoneda As Integer
    Private _IdTienda As Integer
    Private _IdSerie As Integer
    Private _IdEmpresa As Integer
    Private _IdEstadoDoc As Integer
    Private _IdTipoMovimiento As Integer
    Private _IdUsuario As Integer
    Public Property Id() As Integer
        Get
            Return Me._IdRecibo
        End Get
        Set(ByVal value As Integer)
            Me._IdRecibo = value
        End Set
    End Property

    Public Property Codigo() As String
        Get
            Return Me._re_Codigo
        End Get
        Set(ByVal value As String)
            Me._re_Codigo = value
        End Set
    End Property

    Public Property Serie() As String
        Get
            Return Me._re_Serie
        End Get
        Set(ByVal value As String)
            Me._re_Serie = value
        End Set
    End Property

    Public Property Fecha() As Date
        Get
            Return Me._re_Fecha
        End Get
        Set(ByVal value As Date)
            Me._re_Fecha = value
        End Set
    End Property

    Public Property Total() As Decimal
        Get
            Return Me._re_Total
        End Get
        Set(ByVal value As Decimal)
            Me._re_Total = value
        End Set
    End Property

    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property IdSerie() As Integer
        Get
            Return Me._IdSerie
        End Get
        Set(ByVal value As Integer)
            Me._IdSerie = value
        End Set
    End Property

    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property

    Public Property IdEstadoDoc() As Integer
        Get
            Return Me._IdEstadoDoc
        End Get
        Set(ByVal value As Integer)
            Me._IdEstadoDoc = value
        End Set
    End Property

    Public Property IdTipoMovimiento() As Integer
        Get
            Return Me._IdTipoMovimiento
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoMovimiento = value
        End Set
    End Property

    Public Property IdUsuario() As Integer
        Get
            Return Me._IdUsuario
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuario = value
        End Set
    End Property
End Class
