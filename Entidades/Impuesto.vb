'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.



Public Class Impuesto
    Private _IdImpuesto As Integer
    Private _imp_Nombre As String
    Private _imp_Abv As String
    Private _imp_tasa As Decimal
    Private _imp_CuentaContable As String
    Private _imp_Estado As String
    Private _DescEstado As String = "Activo"

    Public Property Id() As Integer
        Get
            Return Me._IdImpuesto
        End Get
        Set(ByVal value As Integer)
            Me._IdImpuesto = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._imp_Nombre
        End Get
        Set(ByVal value As String)
            Me._imp_Nombre = value
        End Set
    End Property

    Public Property Abv() As String
        Get
            Return Me._imp_Abv
        End Get
        Set(ByVal value As String)
            Me._imp_Abv = value
        End Set
    End Property

    Public Property Tasa() As Decimal
        Get
            Return Me._imp_tasa
        End Get
        Set(ByVal value As Decimal)
            Me._imp_tasa = value
        End Set
    End Property

    Public Property CuentaContable() As String
        Get
            Return Me._imp_CuentaContable
        End Get
        Set(ByVal value As String)
            Me._imp_CuentaContable = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._imp_Estado
        End Get
        Set(ByVal value As String)
            Me._imp_Estado = value
        End Set
    End Property

    Public ReadOnly Property DescEstado()
        Get
            If Estado <> "1" Then
                _DescEstado = "Inactivo"
            End If
            Return _DescEstado
        End Get
    End Property

End Class
