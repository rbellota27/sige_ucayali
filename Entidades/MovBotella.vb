﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



'******************     SABADO 03 JULIO 2010


Public Class MovBotella
    Private _IdEmpresa As Integer
    Private _IdTienda As Integer
    Private _IdUbicacion As Integer
    Private _mbot_FechaMov As Date
    Private _mbot_TipoMov As String
    Private _IdTipoDocumento As Integer
    Private _mbot_NroDocumento As String
    Private _mbot_Observacion As String
    Private _mbot_Vigencia As Integer
    Private _mbot_FechaRegistro As Date
    Private _mbot_Estado As Boolean
    Private _IdBotella As Integer
    Private _IdUsuarioInsert As Integer
    Private _mbot_FechaInsert As Date
    Private _IdUsuarioUpdate As Integer
    Private _mbot_FechaUpdate As Date

    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property IdUbicacion() As Integer
        Get
            Return Me._IdUbicacion
        End Get
        Set(ByVal value As Integer)
            Me._IdUbicacion = value
        End Set
    End Property

    Public Property FechaMov() As Date
        Get
            Return Me._mbot_FechaMov
        End Get
        Set(ByVal value As Date)
            Me._mbot_FechaMov = value
        End Set
    End Property

    Public Property TipoMov() As String
        Get
            Return Me._mbot_TipoMov
        End Get
        Set(ByVal value As String)
            Me._mbot_TipoMov = value
        End Set
    End Property

    Public Property IdTipoDocumento() As Integer
        Get
            Return Me._IdTipoDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoDocumento = value
        End Set
    End Property

    Public Property NroDocumento() As String
        Get
            Return Me._mbot_NroDocumento
        End Get
        Set(ByVal value As String)
            Me._mbot_NroDocumento = value
        End Set
    End Property

    Public Property Observacion() As String
        Get
            Return Me._mbot_Observacion
        End Get
        Set(ByVal value As String)
            Me._mbot_Observacion = value
        End Set
    End Property

    Public Property Vigencia() As Integer
        Get
            Return Me._mbot_Vigencia
        End Get
        Set(ByVal value As Integer)
            Me._mbot_Vigencia = value
        End Set
    End Property

    Public Property FechaRegistro() As Date
        Get
            Return Me._mbot_FechaRegistro
        End Get
        Set(ByVal value As Date)
            Me._mbot_FechaRegistro = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return Me._mbot_Estado
        End Get
        Set(ByVal value As Boolean)
            Me._mbot_Estado = value
        End Set
    End Property

    Public Property IdBotella() As Integer
        Get
            Return Me._IdBotella
        End Get
        Set(ByVal value As Integer)
            Me._IdBotella = value
        End Set
    End Property

    Public Property IdUsuarioInsert() As Integer
        Get
            Return Me._IdUsuarioInsert
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuarioInsert = value
        End Set
    End Property

    Public Property FechaInsert() As Date
        Get
            Return Me._mbot_FechaInsert
        End Get
        Set(ByVal value As Date)
            Me._mbot_FechaInsert = value
        End Set
    End Property

    Public Property IdUsuarioUpdate() As Integer
        Get
            Return Me._IdUsuarioUpdate
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuarioUpdate = value
        End Set
    End Property

    Public Property FechaUpdate() As Date
        Get
            Return Me._mbot_FechaUpdate
        End Get
        Set(ByVal value As Date)
            Me._mbot_FechaUpdate = value
        End Set
    End Property

End Class
