'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Regimen
    Private _reg_Id As Integer
    Private _reg_Nombre As String
    Private _reg_Abv As String
    Private _reg_Estado As String
    Private _descEstado As String
    'campo utilizado en el mant de sublinea
    Private _Tasa As Decimal
    Public Sub New()

    End Sub
    Public Sub New(ByVal Id As Integer, ByVal nombre As String)
        Me.Id = Id
        Me.Nombre = nombre
    End Sub
    Public Sub New(ByVal Id As Integer, ByVal nombre As String, ByVal Abv As String, ByVal estado As String, ByVal Tasa As Decimal)
        Me.Id = Id
        Me.Nombre = nombre
        Me.Abv = Abv
        Me.Estado = estado
        Me.Tasa = Tasa
    End Sub
    Public Property Tasa() As Decimal
        Get
            Return Me._Tasa
        End Get
        Set(ByVal value As Decimal)
            Me._Tasa = value
        End Set
    End Property
    Public ReadOnly Property DescEstado() As String
        Get
            Me._descEstado = "Activo"
            If Me._reg_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public ReadOnly Property getPorcentUser() As Decimal
        Get
            Return Me.Tasa * 100
        End Get
    End Property

    Public Property Id() As Integer
        Get
            Return Me._reg_Id
        End Get
        Set(ByVal value As Integer)
            Me._reg_Id = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._reg_Nombre
        End Get
        Set(ByVal value As String)
            Me._reg_Nombre = value
        End Set
    End Property

    Public Property Abv() As String
        Get
            Return Me._reg_Abv
        End Get
        Set(ByVal value As String)
            Me._reg_Abv = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._reg_Estado
        End Get
        Set(ByVal value As String)
            Me._reg_Estado = value
        End Set
    End Property
End Class
