'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'09_02_2010
Public Class Color
    Private _IdColor As Integer
    Private _col_Nombre As String
    Private _col_Abv As String
    Private _col_Estado As String
    Private _descEstado As String
    Private _Estado As Boolean
    Private _Id As String
    Private _col_Codigo As String

    Public Sub New()
    End Sub
    Public Sub New(ByVal id As Integer, ByVal nomcorto As String, ByVal estado As Boolean)
        _IdColor = id
        _col_Abv = nomcorto
        _Estado = estado
    End Sub
    Public Sub New(ByVal id As String, ByVal nomcorto As String, ByVal estado As Boolean)
        _Id = id
        _col_Abv = nomcorto
        _Estado = estado
    End Sub
    Public Property EstadoBit() As Boolean
        Get
            Return _Estado
        End Get
        Set(ByVal value As Boolean)
            _Estado = value
        End Set
    End Property
    Public ReadOnly Property DescEstado()
        Get
            Me._descEstado = "Activo"
            If Me._col_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public Property Id() As Integer
        Get
            Return Me._IdColor
        End Get
        Set(ByVal value As Integer)
            Me._IdColor = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._col_Nombre
        End Get
        Set(ByVal value As String)
            Me._col_Nombre = value
        End Set
    End Property

    Public Property NombreCorto() As String
        Get
            Return Me._col_Abv
        End Get
        Set(ByVal value As String)
            Me._col_Abv = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._col_Estado
        End Get
        Set(ByVal value As String)
            Me._col_Estado = value
        End Set
    End Property
    Public Property IdColorS() As String
        Get
            Return _Id
        End Get
        Set(ByVal value As String)
            _Id = value
        End Set
    End Property
    Public Property Col_Codigo() As String
        Get
            Return _col_Codigo
        End Get
        Set(ByVal value As String)
            _col_Codigo = value
        End Set
    End Property
End Class
