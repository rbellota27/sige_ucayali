'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.




'*************    VIERNES 26 MARZO 2010 HORA 04_30 PM













Public Class PuntoLlegada
    Private _IdAlmacen As Integer
    Private _pll_Ubigeo As String
    Private _pll_Direccion As String
    Private _IdDocumento As Integer
    Private _IdTienda As Integer

    Private _IdTipoAlmacen As Integer

    Public Property IdTipoAlmacen() As Integer
        Get
            Return Me._IdTipoAlmacen
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoAlmacen = value
        End Set
    End Property



    Public Sub New()
    End Sub
    Public Sub New(ByVal idalmacen As Integer, ByVal ubigeo As String, ByVal direccion As String, ByVal iddocumento As Integer)
        Me.IdAlmacen = idalmacen
        Me.pll_Ubigeo = ubigeo
        Me.pll_Direccion = direccion
        Me.IdDocumento = iddocumento
    End Sub



    Public Property IdTienda() As Integer
        Get
            Return _IdTienda
        End Get
        Set(ByVal value As Integer)
            _IdTienda = value
        End Set
    End Property

    Public Property IdAlmacen() As Integer
        Get
            Return Me._IdAlmacen
        End Get
        Set(ByVal value As Integer)
            Me._IdAlmacen = value
        End Set
    End Property

    Public Property pll_Ubigeo() As String
        Get
            Return Me._pll_Ubigeo
        End Get
        Set(ByVal value As String)
            Me._pll_Ubigeo = value
        End Set
    End Property

    Public Property pll_Direccion() As String
        Get
            Return Me._pll_Direccion
        End Get
        Set(ByVal value As String)
            Me._pll_Direccion = value
        End Set
    End Property

    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property
End Class
