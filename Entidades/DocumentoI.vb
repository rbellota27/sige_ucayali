'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class DocumentoI
    Private _IdDocumento As Integer
    Private _doc_Numero As String
    Private _IdTipoDocumentoI As Integer
    Private _IdPersona As Integer

    Public Sub New()
    End Sub

    Public Sub New(ByVal iddocumento As Integer, ByVal numero As String, ByVal idtipodocumentoI As Integer, ByVal idpersona As Integer)
        Me.IdDocumento = iddocumento
        Me.Numero = numero
        Me.IdTipoDocumentoI = idtipodocumentoI
        Me.IdPersona = idpersona
    End Sub

    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property

    Public Property Numero() As String
        Get
            Return Me._doc_Numero
        End Get
        Set(ByVal value As String)
            Me._doc_Numero = value
        End Set
    End Property

    Public Property IdTipoDocumentoI() As Integer
        Get
            Return Me._IdTipoDocumentoI
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoDocumentoI = value
        End Set
    End Property
    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Dim _obj As Object
    Public Property objDocumento() As Object
        Get
            Return _obj
        End Get
        Set(ByVal value As Object)
            _obj = value
        End Set
    End Property


End Class
