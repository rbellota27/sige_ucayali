﻿Public Class be_transportista
    Private _idPersona As Integer = 0
    Private _transportista As String = String.Empty
    Private _doc_identidad As String = String.Empty
    Private _tel_numero As String = String.Empty
    Private _distrito As String = String.Empty

    Public Property idPersona() As Integer
        Get
            Return _idPersona
        End Get
        Set(ByVal value As Integer)
            _idPersona = value
        End Set
    End Property

    Public Property transportista() As String
        Get
            Return _transportista
        End Get
        Set(ByVal value As String)
            _transportista = value
        End Set
    End Property

    Public Property doc_identidad() As String
        Get
            Return _doc_identidad
        End Get
        Set(ByVal value As String)
            _doc_identidad = value
        End Set
    End Property

    Public Property tel_numero() As String
        Get
            Return _tel_numero
        End Get
        Set(ByVal value As String)
            _tel_numero = value
        End Set
    End Property

    Public Property distrito() As String
        Get
            Return _distrito
        End Get
        Set(ByVal value As String)
            _distrito = value
        End Set
    End Property
End Class
