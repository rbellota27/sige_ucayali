﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class UsuarioView
    Private _IdPersona As Integer
    Private _Nombre As String
    Private _DNI As String
    Private _Login As String
    Private _DescFechaAlta As String
    Private _DescFechaBaja As String
    Private _DescEstado As String
    Private _IdMotivoBaja As Integer
    Private _Estado As String
    Private _fechaAlta As Date
    Private _fechaBaja As Date
    Private _Password As String
    Private _IdPerfil As Integer
    Private _Marcar As Boolean
    Private _FlagAtenPago As Integer

    Public Property FlagAtencionPago() As Integer
        Get
            Return Me._FlagAtenPago
        End Get
        Set(ByVal value As Integer)
            Me._FlagAtenPago = value
        End Set
    End Property
    Public Property Clave() As String
        Get
            Return Me._Password
        End Get
        Set(ByVal value As String)
            Me._Password = value
        End Set
    End Property

    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return Me._Nombre
        End Get
        Set(ByVal value As String)
            Me._Nombre = value
        End Set
    End Property
    Public Property DNI() As String
        Get
            Return Me._DNI
        End Get
        Set(ByVal value As String)
            Me._DNI = value
        End Set
    End Property
    Public Property Login() As String
        Get
            Return Me._Login
        End Get
        Set(ByVal value As String)
            Me._Login = value
        End Set
    End Property
    Public ReadOnly Property DescFechaAlta() As String
        Get
            If Me._fechaAlta = Nothing Then
                Return ""
            Else
                Return Format(Me._fechaAlta, "dd/MM/yyyy")
            End If
        End Get
    End Property
    Public ReadOnly Property DescFechaBaja() As String
        Get
            If Me._fechaBaja = Nothing Then
                Return ""
            Else
                Return Format(Me._fechaBaja, "dd/MM/yyyy")
            End If
        End Get
    End Property
    Public ReadOnly Property DescEstado() As String
        Get
            If Me._Estado = "0" Then
                Return "Inactivo"
            Else
                Return "Activo"
            End If
        End Get
    End Property
    Public Property IdMotivoBaja() As Integer
        Get
            Return Me._IdMotivoBaja
        End Get
        Set(ByVal value As Integer)
            Me._IdMotivoBaja = value
        End Set
    End Property
    Public Property Estado() As String
        Get
            Return Me._Estado
        End Get
        Set(ByVal value As String)
            Me._Estado = value
        End Set
    End Property
    Public Property fechaAlta() As Date
        Get
            Return Me._fechaAlta
        End Get
        Set(ByVal value As Date)
            Me._fechaAlta = value
        End Set
    End Property
    Public Property fechaBaja() As Date
        Get
            Return Me._fechaBaja
        End Get
        Set(ByVal value As Date)
            Me._fechaBaja = value
        End Set
    End Property

    Public Property IdPerfil() As Integer
        Get
            Return Me._IdPerfil
        End Get
        Set(ByVal value As Integer)
            Me._IdPerfil = value
        End Set
    End Property



    Public Property Marcar() As Boolean
        Get
            Return Me._Marcar
        End Get
        Set(ByVal value As Boolean)
            Me._Marcar = value
        End Set
    End Property
End Class
