﻿Public Class be_picking

    Private _idDocumento As Integer = 0
    Private _cliente As String = String.Empty
    Private _idDetalleDocumento As Integer = 0
    Private _nroDocumento As String = String.Empty
    Private _tie_nombre As String = String.Empty
    Private _idProducto As Integer = 0
    Private _fecInicioDespacho As Date
    Private _veh_placa As String = String.Empty
    Private _id_despacho_controladores As Integer = 0
    Private _prod_codigo As String = String.Empty
    Private _prod_Nombre As String = String.Empty
    Private _lin_Nombre As String = String.Empty
    Private _sl_Nombre As String = String.Empty
    Private _unidadMedidaPrincipal As Integer = 0
    Private _dc_Cantidad As String = 0
    Private _unidadMedidaSalida As Integer = 0
    Private _nom_sector As String = String.Empty
    Private _idSector As Integer = 0
    Private _tonoProducto As List(Of be_tonoXProducto)
    Private _cantidadTono As String = String.Empty
    Private _nombreTono As String = String.Empty
    Private _nroVuelta As Integer = 0

    Public Property nombreTono() As String
        Get
            Return _nombreTono
        End Get
        Set(ByVal value As String)
            _nombreTono = value
        End Set
    End Property

    Public Property cliente() As String
        Get
            Return _cliente
        End Get
        Set(ByVal value As String)
            _cliente = value
        End Set
    End Property

    Public Property cantidadTono() As String
        Get
            Return _cantidadTono
        End Get
        Set(ByVal value As String)
            _cantidadTono = value
        End Set
    End Property

    Public Property tie_nombre() As String
        Get
            Return _tie_nombre
        End Get
        Set(value As String)
            _tie_nombre = value
        End Set
    End Property

    Public Property idDocumento() As Integer
        Get
            Return _idDocumento
        End Get
        Set(ByVal value As Integer)
            _idDocumento = value
        End Set
    End Property

    Public Property idDetalleDocumento() As Integer
        Get
            Return _idDetalleDocumento
        End Get
        Set(ByVal value As Integer)
            _idDetalleDocumento = value
        End Set
    End Property

    Public Property nroDocumento() As String
        Get
            Return _nroDocumento
        End Get
        Set(ByVal value As String)
            _nroDocumento = value
        End Set
    End Property

    Public Property idProducto() As Integer
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Integer)
            _idProducto = value
        End Set
    End Property

    Public Property fecInicioDespacho() As Date
        Get
            Return _fecInicioDespacho
        End Get
        Set(ByVal value As Date)
            _fecInicioDespacho = value
        End Set
    End Property

    Public Property veh_placa() As String
        Get
            Return _veh_placa
        End Get
        Set(ByVal value As String)
            _veh_placa = value
        End Set
    End Property

    Public Property id_despacho_controladores() As String
        Get
            Return _id_despacho_controladores
        End Get
        Set(ByVal value As String)
            _id_despacho_controladores = value
        End Set
    End Property

    Public Property prod_codigo() As String
        Get
            Return _prod_codigo
        End Get
        Set(ByVal value As String)
            _prod_codigo = value
        End Set
    End Property

    Public Property prod_Nombre() As String
        Get
            Return _prod_Nombre
        End Get
        Set(ByVal value As String)
            _prod_Nombre = value
        End Set
    End Property

    Public Property lin_Nombre() As String
        Get
            Return _lin_Nombre
        End Get
        Set(ByVal value As String)
            _lin_Nombre = value
        End Set
    End Property

    Public Property sl_Nombre() As String
        Get
            Return _sl_Nombre
        End Get
        Set(ByVal value As String)
            _sl_Nombre = value
        End Set
    End Property

    Public Property unidadMedidaPrincipal() As Integer
        Get
            Return _unidadMedidaPrincipal
        End Get
        Set(ByVal value As Integer)
            _unidadMedidaPrincipal = value
        End Set
    End Property

    Public Property dc_Cantidad() As String
        Get
            Return _dc_Cantidad
        End Get
        Set(ByVal value As String)
            _dc_Cantidad = value
        End Set
    End Property

    Public Property unidadMedidaSalida() As Integer
        Get
            Return _unidadMedidaSalida
        End Get
        Set(ByVal value As Integer)
            _unidadMedidaSalida = value
        End Set
    End Property

    Public Property nom_sector() As String
        Get
            Return _nom_sector
        End Get
        Set(ByVal value As String)
            _nom_sector = value
        End Set
    End Property

    Public Property idSector() As Integer
        Get
            Return _idSector
        End Get
        Set(ByVal value As Integer)
            _idSector = value
        End Set
    End Property

    Public Property tono() As List(Of be_tonoXProducto)
        Get
            Return _tonoProducto
        End Get
        Set(ByVal value As List(Of be_tonoXProducto))
            _tonoProducto = value
        End Set
    End Property

    Public Property nroVuelta() As Integer
        Get
            Return _nroVuelta
        End Get
        Set(ByVal value As Integer)
            _nroVuelta = value
        End Set
    End Property
End Class
