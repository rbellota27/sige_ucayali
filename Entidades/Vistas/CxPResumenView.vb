﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class CxPResumenView
    Private _idmoneda As Integer
    Private _mon_Simbolo As String
    Private _Subtotal As Decimal
    Private _tc_CompraC As Decimal
    Private _mon_base As String
    Private _TotalResumen As Decimal
    Private _rucDni As Double
    Private _numero As String
    Private _nombre As String
    Private _per_Ncomercial As String
    'Private _mon_simbolo As String 
    Private _Importe_Cliente As Decimal
    Private _idproveedor As String
    Private _idDocumento As Integer
    Private _tdoc_NombreCorto As String
    Private _doc_FechaEmision As DateTime
    Private _doc_FechaVencimiento As DateTime
    Private _NroDias As Decimal
    Private _doc_TotalPagar As Decimal
    Private _Abono As Decimal
    Private _SalSol As Decimal
    Private _SalDol As Decimal
    Private _saldo As Decimal
    Private _CuentaTipo As String
    Public Property CuentaTipo() As String
        Get
            Return _CuentaTipo
        End Get
        Set(ByVal value As String)
            _CuentaTipo = value
        End Set
    End Property

    Public Property Numero() As String
        Get
            Return _numero
        End Get
        Set(ByVal value As String)
            _numero = value
        End Set
    End Property
    Public Property Saldo() As Decimal
        Get
            Return _saldo
        End Get
        Set(ByVal value As Decimal)
            _saldo = value
        End Set
    End Property
    Public Property SalDol() As Decimal
        Get
            Return _SalDol
        End Get
        Set(ByVal value As Decimal)
            _SalDol = value
        End Set
    End Property
    Public Property SalSol() As Decimal
        Get
            Return _SalSol
        End Get
        Set(ByVal value As Decimal)
            _SalSol = value
        End Set
    End Property
    Public Property RucDni() As Double
        Get
            Return _rucDni
        End Get
        Set(ByVal value As Double)
            _rucDni = value
        End Set
    End Property
    Public Property idproveedor() As Integer
        Get
            Return _idproveedor
        End Get
        Set(ByVal value As Integer)
            _idproveedor = value
        End Set
    End Property
    Public Property IdMoneda() As Integer
        Get
            Return _idmoneda
        End Get
        Set(ByVal value As Integer)
            _idmoneda = value
        End Set
    End Property

    Public Property mon_simbolo() As String
        Get
            Return _mon_Simbolo
        End Get
        Set(ByVal value As String)
            _mon_Simbolo = value
        End Set
    End Property
    Public Property Subtotal() As Decimal
        Get
            Return Me._Subtotal
        End Get
        Set(ByVal value As Decimal)
            _Subtotal = value
        End Set
    End Property
    Public Property tc_CompraC() As Decimal
        Get
            Return Me._tc_CompraC
        End Get
        Set(ByVal value As Decimal)
            _tc_CompraC = value
        End Set
    End Property
    Public Property mon_base() As String
        Get
            Return Me._mon_base
        End Get
        Set(ByVal value As String)
            _mon_base = value
        End Set
    End Property
    Public Property TotalResumen() As Decimal
        Get
            Return Me._TotalResumen
        End Get
        Set(ByVal value As Decimal)
            _TotalResumen = value
        End Set
    End Property

    Public Property nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property
    Public Property per_NComercial() As String
        Get
            Return _per_Ncomercial
        End Get
        Set(ByVal value As String)
            _per_Ncomercial = value
        End Set
    End Property
    'Public ReadOnly Property getNombreParaMostrar() As String
    '    Get
    '        If Me.per_NComercial.Trim.Length > 0 Then
    '            Return Me.per_NComercial
    '        ElseIf Me.nombre.Trim.Length > 0 Then
    '            Return Me.nombre
    '        Else
    '            Return ""
    '        End If
    '    End Get
    'End Property
    Public Property importe_Cliente() As Decimal
        Get
            Return _Importe_Cliente
        End Get
        Set(ByVal value As Decimal)
            _Importe_Cliente = value
        End Set
    End Property
    Public Property idDocumento() As Integer
        Get
            Return _idDocumento
        End Get
        Set(ByVal value As Integer)
            _idDocumento = value
        End Set
    End Property
    Public Property tdoc_NombreCorto() As String
        Get
            Return _tdoc_NombreCorto
        End Get
        Set(ByVal value As String)
            _tdoc_NombreCorto = value
        End Set
    End Property
    Public Property doc_FechaEmision() As DateTime
        Get
            Return _doc_FechaEmision
        End Get
        Set(ByVal value As DateTime)
            _doc_FechaEmision = value
        End Set
    End Property
    Public Property doc_FechaVencimiento() As DateTime
        Get
            Return _doc_FechaVencimiento
        End Get
        Set(ByVal value As DateTime)
            _doc_FechaVencimiento = value
        End Set
    End Property
    Public Property nroDias() As Decimal
        Get
            Return _NroDias
        End Get
        Set(ByVal value As Decimal)
            _NroDias = value
        End Set
    End Property
    Public Property doc_TotalPagar() As Decimal
        Get
            Return _doc_TotalPagar
        End Get
        Set(ByVal value As Decimal)
            _doc_TotalPagar = value
        End Set
    End Property
    Public Property abono() As Decimal
        Get
            Return _Abono
        End Get
        Set(ByVal value As Decimal)
            _Abono = value
        End Set
    End Property



End Class
