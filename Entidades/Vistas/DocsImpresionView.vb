﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Public Class DocsImpresionView
    Private _idImpresion As Integer
    Private _tp_Nombre As String
    Private _timp As String
    Private _idTipoDoc As Integer
    Private _Docs As String
    Private _timpNombre As String

    Public Property Docs() As String
        Get
            Return _Docs
        End Get
        Set(ByVal value As String)
            _Docs = value
        End Set
    End Property

    Public Property IdImpresion() As Integer
        Get
            Return _idImpresion
        End Get
        Set(ByVal value As Integer)
            _idImpresion = value
        End Set
    End Property
    Public Property Tp_Nombre() As String
        Get
            Return _tp_Nombre
        End Get
        Set(ByVal value As String)
            _tp_Nombre = value
        End Set
    End Property
    Public Property Timp() As String
        Get
            Return _timp
        End Get
        Set(ByVal value As String)
            _timp = value
        End Set
    End Property
    Public Property IdTipoDocumento() As Integer
        Get
            Return _idTipoDoc
        End Get
        Set(ByVal value As Integer)
            _idTipoDoc = value
        End Set
    End Property
    Public Property TimpNombre() As String
        Get
            Return _timpNombre
        End Get
        Set(ByVal value As String)
            _timpNombre = value
        End Set
    End Property

End Class
