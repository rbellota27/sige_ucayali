﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'**********************   VIERNES 25 06 2010

Public Class GrillaProducto_M
    Private _IdProducto As Integer
    Private prod_Nombre As String
    Private lin_Nombre As String
    Private sl_Nombre As String
    Private per_NComercial As String
    Private ep_Nombre As String
    Private _PrecioCompra As Decimal
    Private _IdUnidadMedida As Integer
    Private _NomUMedida As String
    Private _IdSubLinea As Integer
    Private _IdLinea As Integer
    Private _FlagUMPrincipal As Boolean
    Private _IdMoneda As Integer
    Private _NomMoneda As String

    'Private _IdFabricante As Integer
    'Private _IdModelo As Integer
    'Private _NomModelo As String
    'Private _IdColor As Integer
    'Private _NomColor As String
    'Private _NomSubSubLinea As String
    Private _NoVisible As String
    Private _Cod_Proveedor As String

    Private _prod_Codigo As String
    Private _prod_Atributos As String

    Private _IdUnidadMedida_Peso As Integer

    Public Property IdUnidadMedida_Peso() As Integer
        Get
            Return Me._IdUnidadMedida_Peso
        End Get
        Set(ByVal value As Integer)
            Me._IdUnidadMedida_Peso = value
        End Set
    End Property


    Public Property NoVisible() As String
        Get
            Return _NoVisible
        End Get
        Set(ByVal value As String)
            _NoVisible = value
        End Set
    End Property
    Public Property Cod_Proveedor() As String
        Get
            Return _Cod_Proveedor
        End Get
        Set(ByVal value As String)
            _Cod_Proveedor = value
        End Set
    End Property
    Public Property prod_Codigo() As String
        Get
            Return _prod_Codigo
        End Get
        Set(ByVal value As String)
            _prod_Codigo = value
        End Set
    End Property
    Public Property prod_Atributos() As String
        Get
            Return _prod_Atributos
        End Get
        Set(ByVal value As String)
            _prod_Atributos = value
        End Set
    End Property
    Public ReadOnly Property FlagPrecioCompra() As Integer
        Get
            If Me._PrecioCompra = 0 Then
                Return 0
            Else
                Return 1
            End If
        End Get
    End Property



    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property

    Public Property NomMoneda() As String
        Get
            Return Me._NomMoneda
        End Get
        Set(ByVal value As String)
            Me._NomMoneda = value
        End Set
    End Property




    Public Property PrecioCompra() As Decimal
        Get
            Return Me._PrecioCompra
        End Get
        Set(ByVal value As Decimal)
            Me._PrecioCompra = value
        End Set
    End Property

    Public Property IdUnidadMedida() As Integer
        Get
            Return Me._IdUnidadMedida
        End Get
        Set(ByVal value As Integer)
            Me._IdUnidadMedida = value
        End Set
    End Property

    Public Property NomUMedida() As String
        Get
            Return Me._NomUMedida
        End Get
        Set(ByVal value As String)
            Me._NomUMedida = value
        End Set
    End Property

    Public Property IdSubLinea() As Integer
        Get
            Return Me._IdSubLinea
        End Get
        Set(ByVal value As Integer)
            Me._IdSubLinea = value
        End Set
    End Property

    Public Property IdLinea() As Integer
        Get
            Return Me._IdLinea
        End Get
        Set(ByVal value As Integer)
            Me._IdLinea = value
        End Set
    End Property

    Public Property FlagUMPrincipal() As Boolean
        Get
            Return Me._FlagUMPrincipal
        End Get
        Set(ByVal value As Boolean)
            Me._FlagUMPrincipal = value
        End Set
    End Property
    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property

    Public Property NomProducto() As String
        Get
            Return Me.prod_Nombre
        End Get
        Set(ByVal value As String)
            Me.prod_Nombre = value
        End Set
    End Property

    Public Property NomLinea() As String
        Get
            Return Me.lin_Nombre
        End Get
        Set(ByVal value As String)
            Me.lin_Nombre = value
        End Set
    End Property

    Public Property NomSubLinea() As String
        Get
            Return Me.sl_Nombre
        End Get
        Set(ByVal value As String)
            Me.sl_Nombre = value
        End Set
    End Property

    Public Property NomProveedor() As String
        Get
            Return Me.per_NComercial
        End Get
        Set(ByVal value As String)
            Me.per_NComercial = value
        End Set
    End Property

    Public Property NomEstado() As String
        Get
            Return Me.ep_Nombre
        End Get
        Set(ByVal value As String)
            Me.ep_Nombre = value
        End Set
    End Property

    'Private _forAbv, _marAbv, _fabNombreLargo, _paNombre, _procNombre As String
    'Public Property NomFormato() As String
    '    Get
    '        Return Me._forAbv
    '    End Get
    '    Set(ByVal value As String)
    '        Me._forAbv = value
    '    End Set
    'End Property
    'Public Property NomMarca() As String
    '    Get
    '        Return Me._marAbv
    '    End Get
    '    Set(ByVal value As String)
    '        Me._marAbv = value
    '    End Set
    'End Property
    'Public Property NomFabricante() As String
    '    Get
    '        Return Me._fabNombreLargo
    '    End Get
    '    Set(ByVal value As String)
    '        Me._fabNombreLargo = value
    '    End Set
    'End Property
    'Public Property NomPais() As String
    '    Get
    '        Return Me._paNombre
    '    End Get
    '    Set(ByVal value As String)
    '        Me._paNombre = value
    '    End Set
    'End Property
    'Public Property NomProcedencia() As String
    '    Get
    '        Return Me._procNombre
    '    End Get
    '    Set(ByVal value As String)
    '        Me._procNombre = value
    '    End Set
    'End Property

    'Private _est_Nombre, _Tran_Nombre, _Cal_Nombre As String
    'Public Property NomEstilo() As String
    '    Get
    '        Return Me._est_Nombre
    '    End Get
    '    Set(ByVal value As String)
    '        Me._est_Nombre = value
    '    End Set
    'End Property
    'Public Property NomTransito() As String
    '    Get
    '        Return Me._Tran_Nombre
    '    End Get
    '    Set(ByVal value As String)
    '        Me._Tran_Nombre = value
    '    End Set
    'End Property
    'Public Property NomCalidad() As String
    '    Get
    '        Return Me._Cal_Nombre
    '    End Get
    '    Set(ByVal value As String)
    '        Me._Cal_Nombre = value
    '    End Set
    'End Property
    'Public Property IdFabricante() As Integer
    '    Get
    '        Return _IdFabricante
    '    End Get
    '    Set(ByVal value As Integer)
    '        _IdFabricante = value
    '    End Set
    'End Property
    'Public Property IdModelo() As Integer
    '    Get
    '        Return _IdModelo
    '    End Get
    '    Set(ByVal value As Integer)
    '        _IdModelo = value
    '    End Set
    'End Property
    'Public Property NomModelo() As String
    '    Get
    '        Return _NomModelo
    '    End Get
    '    Set(ByVal value As String)
    '        _NomModelo = value
    '    End Set
    'End Property
    'Public Property IdColor() As Integer
    '    Get
    '        Return _IdColor
    '    End Get
    '    Set(ByVal value As Integer)
    '        _IdColor = value
    '    End Set
    'End Property
    'Public Property NomColor() As String
    '    Get
    '        Return _NomColor
    '    End Get
    '    Set(ByVal value As String)
    '        _NomColor = value
    '    End Set
    'End Property
    'Public Property NomSubSubLinea() As String
    '    Get
    '        Return _NomSubSubLinea
    '    End Get
    '    Set(ByVal value As String)
    '        _NomSubSubLinea = value
    '    End Set
    'End Property
End Class
