﻿Public Class be_cronograma
    Private _nomAgencia As String = String.Empty
    Private _idVehiculo As Integer = 0
    Private _idCronograma As Integer = 0
    Private _fecInicio As DateTime = Date.Now
    Private _fecFin As String = String.Empty
    Private _idEstadoDespacho As Integer = 0
    Private _idDocumentoRelacion As String = String.Empty
    Private _idConductor As Integer = 0
    Private _idAyudante As Integer = 0
    Private _idControl As Integer = 0
    Private _turno As String = String.Empty
    Private _idAgencia As Integer = 0
    Private _vehplaca As String = String.Empty
    Private _vehModelo As String = String.Empty
    Private _capacidadMaxima As Decimal = 0
    Private _capacidadAlcanzada As Decimal = 0
    Private _cantDocAsociado As Integer = 0
    Private _conductor As String = String.Empty
    Private _ayudante As String = String.Empty
    Private _control As String = String.Empty
    Private _nroVuelta As Integer = 0

    Public Property nomAgencia() As String
        Get
            Return _nomAgencia
        End Get
        Set(ByVal value As String)
            _nomAgencia = value
        End Set
    End Property

    Public Property idVehiculo() As Integer
        Get

            Return _idVehiculo
        End Get
        Set(ByVal value As Integer)
            _idVehiculo = value
        End Set
    End Property

    Public Property idCronograma() As Integer
        Get

            Return _idCronograma
        End Get
        Set(ByVal value As Integer)
            _idCronograma = value
        End Set
    End Property

    Public Property fecInicio() As DateTime
        Get
            Return _fecInicio
        End Get
        Set(ByVal value As DateTime)
            _fecInicio = value
        End Set
    End Property

    Public Property fecFin() As String
        Get
            Return _fecFin
        End Get
        Set(ByVal value As String)
            _fecFin = value
        End Set
    End Property

    Public Property idEstadoDespacho() As Integer
        Get

            Return _idEstadoDespacho
        End Get
        Set(ByVal value As Integer)
            _idEstadoDespacho = value
        End Set
    End Property

    Public Property idDocumentoRelacion() As String
        Get
            Return _idDocumentoRelacion
        End Get
        Set(ByVal value As String)
            _idDocumentoRelacion = value
        End Set
    End Property

    Public Property idConductor() As Integer
        Get

            Return _idConductor
        End Get
        Set(ByVal value As Integer)
            _idConductor = value
        End Set
    End Property

    Public Property idAyudante() As Integer
        Get

            Return _idAyudante
        End Get
        Set(ByVal value As Integer)
            _idAyudante = value
        End Set
    End Property

    Public Property idControl() As Integer
        Get

            Return _idControl
        End Get
        Set(ByVal value As Integer)
            _idControl = value
        End Set
    End Property

    Public Property turno() As String
        Get
            Return _turno
        End Get
        Set(ByVal value As String)
            _turno = value
        End Set
    End Property

    Public Property idAgencia() As Integer
        Get

            Return _idAgencia
        End Get
        Set(ByVal value As Integer)
            _idAgencia = value
        End Set
    End Property

    Public Property vehplaca() As String
        Get
            Return _vehplaca
        End Get
        Set(ByVal value As String)
            _vehplaca = value
        End Set
    End Property

    Public Property vehModelo() As String
        Get
            Return _vehModelo
        End Get
        Set(ByVal value As String)
            _vehModelo = value
        End Set
    End Property

    Public Property capacidadMaxima() As Decimal
        Get
            Return _capacidadMaxima
        End Get
        Set(ByVal value As Decimal)
            _capacidadMaxima = value
        End Set
    End Property

    Public Property capacidadAlcanzada() As Decimal
        Get
            Return _capacidadAlcanzada
        End Get
        Set(ByVal value As Decimal)
            _capacidadAlcanzada = value
        End Set
    End Property

    Public Property cantDocAsociado() As Integer
        Get

            Return _cantDocAsociado
        End Get
        Set(ByVal value As Integer)
            _cantDocAsociado = value
        End Set
    End Property

    Public Property conductor() As String
        Get
            Return _conductor
        End Get
        Set(ByVal value As String)
            _conductor = value
        End Set
    End Property

    Public Property ayudante() As String
        Get
            Return _ayudante
        End Get
        Set(ByVal value As String)
            _ayudante = value
        End Set
    End Property

    Public Property control() As String
        Get
            Return _control
        End Get
        Set(ByVal value As String)
            _control = value
        End Set
    End Property

    Public Property nroVuelta() As Integer
        Get
            Return _nroVuelta
        End Get
        Set(ByVal value As Integer)
            _nroVuelta = value
        End Set
    End Property
End Class
