﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class TiendaAreaPerfilView
    Private _IdTienda As Integer
    Private _IdArea As Integer
    Private _IdPerfil As Integer
    Private _NomTienda As String
    Private _NomArea As String
    Private _NomPerfil As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal idtienda As Integer, ByVal nomtienda As String, ByVal idarea As Integer, ByVal nomarea As String, ByVal idperfil As Integer, ByVal nomperfil As String)
        Me.IdTienda = idtienda
        Me.NomTienda = nomtienda
        Me.IdArea = idarea
        Me.NomArea = nomarea
        Me.IdPerfil = idperfil
        Me.NomPerfil = nomperfil
    End Sub
    Public Sub New(ByVal idarea As Integer, ByVal nomarea As String)
        Me.IdArea = idarea
        Me.NomArea = nomarea
    End Sub
    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property IdArea() As Integer
        Get
            Return Me._IdArea
        End Get
        Set(ByVal value As Integer)
            Me._IdArea = value
        End Set
    End Property

    Public Property IdPerfil() As Integer
        Get
            Return Me._IdPerfil
        End Get
        Set(ByVal value As Integer)
            Me._IdPerfil = value
        End Set
    End Property

    Public Property NomTienda() As String
        Get
            Return Me._NomTienda
        End Get
        Set(ByVal value As String)
            Me._NomTienda = value
        End Set
    End Property

    Public Property NomArea() As String
        Get
            Return Me._NomArea
        End Get
        Set(ByVal value As String)
            Me._NomArea = value
        End Set
    End Property

    Public Property NomPerfil() As String
        Get
            Return Me._NomPerfil
        End Get
        Set(ByVal value As String)
            Me._NomPerfil = value
        End Set
    End Property
End Class
