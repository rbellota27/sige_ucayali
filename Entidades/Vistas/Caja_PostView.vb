﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'MIERCOLES 17 DE FEBRERO    12 13 PM    Se agrego Datos de Banco y CuentaBancaria
'MIERCOLES 17 DE FEBRERO    11 36 AM    Se elimino IdTipoTarjeta y TipoTarjeta; por cambios en la tabla
''''''''''''''''''''''MIRAYA ANAMARIA, EDGAR 29/01/2010 4:50 pm
Public Class Caja_PostView
    Inherits Caja_Post

    Private _CajaNombre As String
    Private _CajaNumero As String

    Private _PostDescripcion As String
    Private _PostIdentificador As String

    Private _IdTienda As Integer
    Private _Tienda As String

    Private _IdBanco As Integer
    Private _Banco As String

    Private _IdCuentaBancaria As Integer
    Private _CuentaBancaria As String

    Public Property CajaNombre() As String
        Get
            Return Me._CajaNombre
        End Get
        Set(ByVal value As String)
            Me._CajaNombre = value
        End Set
    End Property
    Public Property CajaNumero() As String
        Get
            Return Me._CajaNumero
        End Get
        Set(ByVal value As String)
            Me._CajaNumero = value
        End Set
    End Property
    Public Property PostDescripcion() As String
        Get
            Return Me._PostDescripcion
        End Get
        Set(ByVal value As String)
            Me._PostDescripcion = value
        End Set
    End Property
    Public Property PostIdentificador() As String
        Get
            Return Me._PostIdentificador
        End Get
        Set(ByVal value As String)
            Me._PostIdentificador = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property Tienda() As String
        Get
            Return Me._Tienda
        End Get
        Set(ByVal value As String)
            Me._Tienda = value
        End Set
    End Property

    Public Property IdBanco() As Integer
        Get
            Return Me._IdBanco
        End Get
        Set(ByVal value As Integer)
            Me._IdBanco = value
        End Set
    End Property
    Public Property Banco() As String
        Get
            Return Me._Banco
        End Get
        Set(ByVal value As String)
            Me._Banco = value
        End Set
    End Property

    Public Property IdCuentaBancaria() As Integer
        Get
            Return Me._IdCuentaBancaria
        End Get
        Set(ByVal value As Integer)
            Me._IdCuentaBancaria = value
        End Set
    End Property
    Public Property CuentaBancaria() As String
        Get
            Return Me._CuentaBancaria
        End Get
        Set(ByVal value As String)
            Me._CuentaBancaria = value
        End Set
    End Property
End Class