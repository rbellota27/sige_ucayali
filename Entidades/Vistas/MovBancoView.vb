﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'''''''    26 julio 2010 Hora 12:08 am

'**************** VIERNES 19 FEB 2010 HORA 02_58 PM



''''''''''''''''''''''MIRAYA ANAMARIA, EDGAR 04/02/2010 10:23 am
Public Class MovBancoView
    Inherits MovBanco

    Protected _IdTipoTarjeta As Integer
    Protected _IdTipoConceptoBanco As Integer
    Protected _Banco As String
    Protected _CuentaBancaria As String
    Protected _Usuario As String
    Protected _Supervisor As String
    Protected _ConceptoMovBanco As String
    Protected _TipoConceptoBanco As String
    Protected _Tarjeta As String
    Protected _TipoTarjeta As String
    Protected _PersonaReferencia As String
    Protected _Caja As String
    Protected _Tienda As String
    Protected _Post As String
    Protected _SaldoDisponible As Decimal
    Protected _SaldoContable As Decimal
    Protected _Moneda As String
    Protected _MedioPago As String

    '*********** Atributos Opcionales de Procesamiento
    Private _IdMoneda As Integer
    Private _IdMedioPago As Integer
    Private _AnexoMovBanco As Entidades.Anexo_MovBanco
    Private _IdDocumentoRef As Integer
    Private _IdDocumentoRef2 As Integer
    Private _IdCheque As Integer
    Private _Referencia As String
    Private _Descripción_operación As String
    Private _Fecha As Date
    Private _Fecha_valuta As Date
    Private _DescFechaMov As Date
    Private _Sucursal_agencia As String
    Private _Operación_Número As String
    Private _IdentificadorExport As Integer
    Private _Referencia1 As String
    Private _Referencia2 As String
    Protected _Saldo As Decimal
    Private _Operación_Hora As String
    Private _UTC As String
    Private _idestructurabanco As Integer
    Private _nom_Hoja As String
    Public Property idestructurabanco() As Integer
        Get
            Return _idestructurabanco
        End Get
        Set(ByVal value As Integer)
            _idestructurabanco = value
        End Set
    End Property
    Public Property nom_Hoja() As String
        Get
            Return _nom_Hoja
        End Get
        Set(ByVal value As String)
            _nom_Hoja = value
        End Set
    End Property

    Public Property UTC() As String
        Get
            Return _UTC
        End Get
        Set(ByVal value As String)
            _UTC = value
        End Set
    End Property
    Public Property Operación_Hora() As String
        Get
            Return _Operación_Hora
        End Get
        Set(ByVal value As String)
            _Operación_Hora = value
        End Set
    End Property


    Public Property Saldo() As Decimal
        Get
            Return _Saldo
        End Get
        Set(ByVal value As Decimal)
            _Saldo = value
        End Set
    End Property
    Public Property Referencia1() As String
        Get
            Return _Referencia1
        End Get
        Set(ByVal value As String)
            _Referencia1 = value
        End Set
    End Property
    Public Property Referencia2() As String
        Get
            Return _Referencia2
        End Get
        Set(ByVal value As String)
            _Referencia2 = value
        End Set
    End Property

    Public Property IdentificadorExport() As Integer
        Get
            Return _IdentificadorExport
        End Get
        Set(ByVal value As Integer)
            _IdentificadorExport = value
        End Set
    End Property
    Public Property Operación_Número() As String
        Get
            Return _Operación_Número
        End Get
        Set(ByVal value As String)
            _Operación_Número = value
        End Set
    End Property
    Public Property Sucursal_agencia() As String
        Get
            Return _Sucursal_agencia
        End Get
        Set(ByVal value As String)
            _Sucursal_agencia = value
        End Set
    End Property

    Public Property Descripción_operación() As String
        Get
            Return _Descripción_operación
        End Get
        Set(ByVal value As String)
            _Descripción_operación = value
        End Set
    End Property
    Public Property Fecha() As Date
        Get
            Return _Fecha
        End Get
        Set(ByVal value As Date)
            _Fecha = value
        End Set
    End Property
    Public Property Fecha_valuta() As Date
        Get
            Return _Fecha_valuta
        End Get
        Set(ByVal value As Date)
            _Fecha_valuta = value
        End Set
    End Property
    Public AnexoMovBanco


    Public Property Referencia() As String
        Get
            Return _Referencia
        End Get
        Set(ByVal value As String)
            _Referencia = value
        End Set
    End Property

    Public Property IdDocumentoRef2() As Integer
        Get
            Return _IdDocumentoRef2
        End Get
        Set(ByVal value As Integer)
            _IdDocumentoRef2 = value
        End Set
    End Property

    Public Property IdDocumentoRef() As Integer
        Get
            Return _IdDocumentoRef
        End Get
        Set(ByVal value As Integer)
            _IdDocumentoRef = value
        End Set
    End Property

    Public Property IdCheque() As Integer
        Get
            Return _IdCheque
        End Get
        Set(ByVal value As Integer)
            _IdCheque = value
        End Set
    End Property

    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property

    Public Property IdMedioPago() As Integer
        Get
            Return Me._IdMedioPago
        End Get
        Set(ByVal value As Integer)
            Me._IdMedioPago = value
        End Set
    End Property






    Public Property IdTipoTarjeta() As Integer
        Get
            Return Me._IdTipoTarjeta
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoTarjeta = value
        End Set
    End Property
    Public Property IdTipoConceptoBanco() As Integer
        Get
            Return Me._IdTipoConceptoBanco
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoConceptoBanco = value
        End Set
    End Property

    Public Property Banco() As String
        Get
            Return Me._Banco
        End Get
        Set(ByVal value As String)
            Me._Banco = value
        End Set
    End Property
    Public Property CuentaBancaria() As String
        Get
            Return Me._CuentaBancaria
        End Get
        Set(ByVal value As String)
            Me._CuentaBancaria = value
        End Set
    End Property
    Public Property Usuario() As String
        Get
            Return Me._Usuario
        End Get
        Set(ByVal value As String)
            Me._Usuario = value
        End Set
    End Property
    Public Property Supervisor() As String
        Get
            Return Me._Supervisor
        End Get
        Set(ByVal value As String)
            Me._Supervisor = value
        End Set
    End Property
    Public Property ConceptoMovBanco() As String
        Get
            Return Me._ConceptoMovBanco
        End Get
        Set(ByVal value As String)
            Me._ConceptoMovBanco = value
        End Set
    End Property
    Public Property TipoConceptoBanco() As String
        Get
            Return Me._TipoConceptoBanco
        End Get
        Set(ByVal value As String)
            Me._TipoConceptoBanco = value
        End Set
    End Property
    Public Property TipoConceptoBancoResumen() As String
        Get
            Return Me._TipoConceptoBanco.Substring(0, 1)
        End Get
        Set(ByVal value As String)
            Me._TipoConceptoBanco = value
        End Set

        'Get
        '    Return Me._TipoConceptoBanco.Substring(0, 1)
        'End Get
    End Property
    Public Property Tarjeta() As String
        Get
            Return Me._Tarjeta
        End Get
        Set(ByVal value As String)
            Me._Tarjeta = value
        End Set
    End Property
    Public Property TipoTarjeta() As String
        Get
            Return Me._TipoTarjeta
        End Get
        Set(ByVal value As String)
            Me._TipoTarjeta = value
        End Set
    End Property
    Public Property PersonaReferencia() As String
        Get
            Return Me._PersonaReferencia
        End Get
        Set(ByVal value As String)
            Me._PersonaReferencia = value
        End Set
    End Property
    Public Property Caja() As String
        Get
            Return Me._Caja
        End Get
        Set(ByVal value As String)
            Me._Caja = value
        End Set
    End Property
    Public Property Tienda() As String
        Get
            Return Me._Tienda
        End Get
        Set(ByVal value As String)
            Me._Tienda = value
        End Set
    End Property
    Public Property Post() As String
        Get
            Return Me._Post
        End Get
        Set(ByVal value As String)
            Me._Post = value
        End Set
    End Property

    Public Property SaldoDisponible() As Decimal
        Get
            Return _SaldoDisponible
        End Get
        Set(ByVal value As Decimal)
            _SaldoDisponible = value
        End Set
    End Property
    Public Property SaldoContable() As Decimal
        Get
            Return _SaldoContable
        End Get
        Set(ByVal value As Decimal)
            _SaldoContable = value
        End Set
    End Property

    Public Property Moneda() As String
        Get
            Return _Moneda
        End Get
        Set(ByVal value As String)
            _Moneda = value
        End Set
    End Property

    Public Property MedioPago() As String
        Get
            Return _MedioPago
        End Get
        Set(ByVal value As String)
            _MedioPago = value
        End Set
    End Property
End Class
