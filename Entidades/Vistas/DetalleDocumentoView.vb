﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




'********** MIERCOLES 10 MARZO 2010 HORA 05_22 PM




Public Class DetalleDocumentoView
    Private _IdProducto As Integer
    Private _dc_Cantidad, _dc_PrecioCD, _dc_Descuento, _PDescuento, _dc_PrecioSD, _dc_Importe, _dc_Utilidad, _StockDisponible As Decimal
    Private _dc_UMedida, _prod_Nombre, _dc_UMedidaPeso As String
    'campos utilizado en el detalle de la Guia de Recepcion
    Private _dc_peso As Decimal
    Private _listaProdUM As List(Of Entidades.ProductoUMView) 'lista de las unidades de medida segun IDProducto
    Private _listaUMedida As List(Of Entidades.UnidadMedida) 'lista de todas las unidades de Medida activo
    '-------
    Private _Kit As Boolean
    Private _dc_IdUMedida As Integer
    Private _dc_IdUMedidaPeso As String
    Private _NomMoneda As String
    Private _IdMoneda As Integer
    Private _Percepcion, _Detraccion As Decimal
    Private _TienePrecioCompra As Integer
    Private _IdDetalleDocumento As Integer
    Private _IdUMPrincipal As Integer
    Private _NomUMPrincipal As String
    Private _IdDocumento As Integer
    Private _serie As String
    Private _Glosa As String
    Private _codBarraFabricante As String
    Private _idAuxiliar As Integer
    Private _dc_Descuento1, _dc_Descuento2, _dc_Descuento3, _dc_Costo_Imp As Decimal

    Public Property dc_Costo_Imp() As Decimal
        Get
            Return _dc_Costo_Imp
        End Get
        Set(ByVal value As Decimal)
            _dc_Costo_Imp = value
        End Set
    End Property

    Public Property codBarraFabricante() As String
        Get
            Return _codBarraFabricante
        End Get
        Set(ByVal value As String)
            _codBarraFabricante = value
        End Set
    End Property


    Public Property dc_Descuento3() As Decimal
        Get
            Return _dc_Descuento3
        End Get
        Set(ByVal value As Decimal)
            _dc_Descuento3 = value
        End Set
    End Property

    Public Property dc_Descuento2() As Decimal
        Get
            Return _dc_Descuento2
        End Get
        Set(ByVal value As Decimal)
            _dc_Descuento2 = value
        End Set
    End Property

    Public Property dc_Descuento1() As Decimal
        Get
            Return _dc_Descuento1
        End Get
        Set(ByVal value As Decimal)
            _dc_Descuento1 = value
        End Set
    End Property

    Private _CodigoProducto As String

    Public Property CodigoProducto() As String
        Get
            Return Me._CodigoProducto
        End Get
        Set(ByVal value As String)
            Me._CodigoProducto = value
        End Set
    End Property



    Public Property idAuxiliar() As Integer
        Get
            Return _idAuxiliar
        End Get
        Set(ByVal value As Integer)
            _idAuxiliar = value
        End Set
    End Property


    Private _IdTienda_PV As Integer

    Public ReadOnly Property HasGlosa() As Boolean
        Get

            If Me.Glosa IsNot Nothing Then

                If Me.Glosa.Trim.Length > 0 Then

                    Return True

                End If

            End If

            Return False

        End Get
    End Property

    Public Property serie() As String
        Get
            Return _serie
        End Get
        Set(ByVal value As String)
            _serie = value
        End Set
    End Property



    Public Property IdTienda_PV() As Integer
        Get
            Return Me._IdTienda_PV
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda_PV = value
        End Set
    End Property




    Public Property Glosa() As String
        Get
            Return Me._Glosa
        End Get
        Set(ByVal value As String)
            Me._Glosa = value
        End Set
    End Property

    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property
    Public Property IdUMPrincipal() As Integer
        Get
            Return Me._IdUMPrincipal
        End Get
        Set(ByVal value As Integer)
            Me._IdUMPrincipal = value
        End Set
    End Property

    Public Property NomUMPrincipal() As String
        Get
            Return Me._NomUMPrincipal
        End Get
        Set(ByVal value As String)
            Me._NomUMPrincipal = value
        End Set
    End Property

    Public Property IdDetalleDocumento() As Integer
        Get
            Return Me._IdDetalleDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDetalleDocumento = value
        End Set
    End Property
    Public Property TienePrecioCompra() As Integer
        Get
            Return Me._TienePrecioCompra
        End Get
        Set(ByVal value As Integer)
            Me._TienePrecioCompra = value
        End Set
    End Property

    Public Property NomMoneda() As String
        Get
            Return Me._NomMoneda
        End Get
        Set(ByVal value As String)
            Me._NomMoneda = value
        End Set
    End Property

    Public Property IdMoneda() As Integer ' Este Id se usa para la Venta
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property

    Public Property IdUMedida() As Integer
        Get
            Return Me._dc_IdUMedida
        End Get
        Set(ByVal value As Integer)
            Me._dc_IdUMedida = value
        End Set
    End Property

    Public Property IdUMedidaPeso() As String
        Get
            Return Me._dc_IdUMedidaPeso
        End Get
        Set(ByVal value As String)
            Me._dc_IdUMedidaPeso = value
        End Set
    End Property


    Public Property ListaProdUM() As List(Of Entidades.ProductoUMView)
        Get
            Return Me._listaProdUM
        End Get
        Set(ByVal value As List(Of Entidades.ProductoUMView))
            Me._listaProdUM = value
        End Set
    End Property


    Public Property ListaUMedida() As List(Of Entidades.UnidadMedida)
        Get
            Return Me._listaUMedida
        End Get
        Set(ByVal value As List(Of Entidades.UnidadMedida))
            Me._listaUMedida = value
        End Set
    End Property


    Public Property Peso() As Decimal
        Get
            Return _dc_peso
        End Get
        Set(ByVal value As Decimal)
            _dc_peso = value
        End Set
    End Property
    Public Property IdProducto() As Integer
        Get
            Return _IdProducto
        End Get
        Set(ByVal value As Integer)
            _IdProducto = value
        End Set
    End Property
    Public Property Cantidad() As Decimal
        Get
            Return _dc_Cantidad
        End Get
        Set(ByVal value As Decimal)
            _dc_Cantidad = value
        End Set
    End Property
    Public Property StockDisponible() As Decimal
        Get
            Return _StockDisponible
        End Get
        Set(ByVal value As Decimal)
            _StockDisponible = value
        End Set
    End Property
    Public Property UM() As String
        Get
            Return _dc_UMedida
        End Get
        Set(ByVal value As String)
            _dc_UMedida = value
        End Set
    End Property
    Public Property UMPeso() As String
        Get
            Return _dc_UMedidaPeso
        End Get
        Set(ByVal value As String)
            _dc_UMedidaPeso = value
        End Set
    End Property
    Public Property Descripcion() As String
        Get
            Return _prod_Nombre
        End Get
        Set(ByVal value As String)
            _prod_Nombre = value
        End Set
    End Property
    Public Property PrecioSD() As Decimal
        Get
            Return _dc_PrecioSD
        End Get
        Set(ByVal value As Decimal)
            _dc_PrecioSD = value
        End Set
    End Property
    Public Property Descuento() As Decimal
        Get
            Return _dc_Descuento
        End Get
        Set(ByVal value As Decimal)
            _dc_Descuento = value
        End Set
    End Property
    Public Property PDescuento() As Decimal
        Get
            If _dc_PrecioSD <> 0 And _PDescuento = 0 Then
                Return (_dc_Descuento / _dc_PrecioSD) * 100
            End If
        End Get
        Set(ByVal value As Decimal)
            _PDescuento = value
        End Set
    End Property

    Private _Pdscto As Decimal
    Public Property Pdscto() As Decimal
        Get
            Return _Pdscto
        End Get
        Set(ByVal value As Decimal)
            _Pdscto = value
        End Set
    End Property

    Public Property PrecioCD() As Decimal
        Get
            Return _dc_PrecioCD
        End Get
        Set(ByVal value As Decimal)
            _dc_PrecioCD = value
        End Set
    End Property
    Public Property Importe() As Decimal
        Get
            Return _dc_Importe
        End Get
        Set(ByVal value As Decimal)
            _dc_Importe = value
        End Set
    End Property
    Public Property Utilidad() As Decimal
        Get
            Return _dc_Utilidad
        End Get
        Set(ByVal value As Decimal)
            _dc_Utilidad = value
        End Set
    End Property
    Public Property Percepcion() As Decimal
        Get
            Return _Percepcion
        End Get
        Set(ByVal value As Decimal)
            _Percepcion = value
        End Set
    End Property
    Public Property Detraccion() As Decimal
        Get
            Return _Detraccion
        End Get
        Set(ByVal value As Decimal)
            _Detraccion = value
        End Set
    End Property
    Public ReadOnly Property getNomProducto() As String
        Get
            If Me._Glosa Is Nothing Then
                Return Me._prod_Nombre
            ElseIf Me._Glosa.Trim.Length > 0 Then
                Return Me._prod_Nombre + " (RETAZO) [" + Me._Glosa + "]"
            Else
                Return Me._prod_Nombre
            End If
        End Get
    End Property
    Public Property Kit() As Boolean
        Get
            Return Me._Kit
        End Get
        Set(ByVal value As Boolean)
            Me._Kit = value
        End Set
    End Property
End Class
