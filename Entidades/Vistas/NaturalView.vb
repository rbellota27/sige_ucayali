﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class NaturalView
    Private _IdPersona As Integer
    Private _Nombre As String
    Private _nat_FechaNac As Date
    Private _car_Nombre As String
    Private _DNI As String

    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._Nombre
        End Get
        Set(ByVal value As String)
            Me._Nombre = value
        End Set
    End Property

    Public Property FechaNac() As Date
        Get
            Return Me._nat_FechaNac
        End Get
        Set(ByVal value As Date)
            Me._nat_FechaNac = value
        End Set
    End Property

    Public Property NombreCargo() As String
        Get
            Return Me._car_Nombre
        End Get
        Set(ByVal value As String)
            Me._car_Nombre = value
        End Set
    End Property

    Public Property DNI() As String
        Get
            Return Me._DNI
        End Get
        Set(ByVal value As String)
            Me._DNI = value
        End Set
    End Property
    Public ReadOnly Property DescFechaNac() As String
        Get
            If Me.FechaNac = Nothing Then
                Return ""
            Else
                Return Format(Me.FechaNac, "dd/MM/yyyy")
            End If
        End Get
    End Property
End Class
