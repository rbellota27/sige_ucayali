﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

''''''''''''''''''''''MIRAYA ANAMARIA, EDGAR 01/02/2010 9:52 pm
Public Class PostView
    Inherits Post

    Private _Banco As String
    Private _CuentaBancaria As String

    Private _BancoCodigoSunat As String
    Private _CuentaBancariaCtaContable As String

    Private _IdPersona As Integer
    Private _Persona As String

    Public Property Banco() As String
        Get
            Return Me._Banco
        End Get
        Set(ByVal value As String)
            Me._Banco = value
        End Set
    End Property

    Public Property CuentaBancaria() As String
        Get
            Return Me._CuentaBancaria
        End Get
        Set(ByVal value As String)
            Me._CuentaBancaria = value
        End Set
    End Property

    Public Property BancoCodigoSunat() As String
        Get
            Return Me._BancoCodigoSunat
        End Get
        Set(ByVal value As String)
            Me._BancoCodigoSunat = value
        End Set
    End Property

    Public Property CuentaBancariaCtaContable() As String
        Get
            Return Me._CuentaBancariaCtaContable
        End Get
        Set(ByVal value As String)
            Me._CuentaBancariaCtaContable = value
        End Set
    End Property

    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property
    Public Property Persona() As String
        Get
            Return Me._Persona
        End Get
        Set(ByVal value As String)
            Me._Persona = value
        End Set
    End Property
End Class
