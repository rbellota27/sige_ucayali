﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class RegimenOperacionView
    Inherits RegimenOperacion

    Private _reg_nombre As String
    Private _reg_abv As String
    Private _top_codigoSunat As String
    Private _top_nombre As String

    Public Property RegimenNombre() As String
        Get
            Return Me._reg_nombre
        End Get
        Set(ByVal value As String)
            Me._reg_nombre = value
        End Set
    End Property

    Public Property RegimenAbv() As String
        Get
            Return Me._reg_abv
        End Get
        Set(ByVal value As String)
            Me._reg_abv = value
        End Set
    End Property
    Public Property TipoOperacionNombre() As String
        Get
            Return Me._top_nombre
        End Get
        Set(ByVal value As String)
            Me._top_nombre = value
        End Set
    End Property
    Public Property TipoOperacionCodSunat() As String
        Get
            Return Me._top_codigoSunat
        End Get
        Set(ByVal value As String)
            Me._top_codigoSunat = value
        End Set
    End Property

End Class
