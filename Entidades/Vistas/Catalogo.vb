﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Catalogo
    Private _IdProducto, _IdSubLinea, _IdUnidadMedida As Integer
    Private _Elegir As Boolean
    Private _Prod_Nombre, _pv_Nombre, _NoVisible, _Cod_Proveedor As String
    Private _StockReal, _StockComprometido, _ppv_Valor As Decimal
    Private _listaProdUM As List(Of Entidades.ProductoUMView) 'lista de las unidades de medida segun IDProducto
    Private _listaUM_Venta As List(Of Entidades.UnidadMedida) 'lista de las unidades de medida de VENTA 
    Private _sl_AfectoPercepcion, _sl_AfectoDetraccion As Boolean
    Private _Percepcion, _Detraccion As Decimal
    Private _NomLinea As String
    Private _NomSubLinea As String
    Private _SimbMoneda As String
    Private _Select As Boolean = False
    Private _IdTipoPV As Integer
    Private _NomTipoPV As String
    Private _IdTienda As Integer
    Private _CodigoSubLinea As String
    Private _Cantidad As Decimal
    Private _cadena As String
    Private _StockDisponibleN As Decimal
    Private _Tienda As String
    Private _Almacen As String
    Private _IdAlmacen As Integer
    Private _PrecioLista As Decimal
    Private _PorcentDctoMaximo As Decimal
    Private _VolumenVenta As Decimal
    Private _CodigoProducto As String
    Private _pvComercial As Decimal
    Private _PrecioBaseDcto As String
    Private _Kit As Boolean
    Private _ExisteCampania_Producto As Boolean
    Private _IdUnidadMedida_Peso As Integer
    Private _Prod_CodigoBarras As String
    Private _codBarraFabricante As String
    Private _IdTipoExistencia As Integer
    Private _StockEnTransito As Decimal
    Private _Flete1 As Decimal
    Private _Flete2 As Decimal
    Private _Equivalencia As String
    Private _Pais As String
    Private _Atributo1 As String
    Private _Formato As String
    Private _IsKit As Integer
    Private _UMProducto As String
    Private _idunidadMedidaPrincipal As Integer
    Private _cadenaComboUnidadMedida As String = String.Empty

    Public Property cadenaComboUnidadMedida() As String
        Get
            Return _cadenaComboUnidadMedida
        End Get
        Set(ByVal value As String)
            _cadenaComboUnidadMedida = value
        End Set
    End Property

    Public Property idunidadMedidaPrincipal() As Integer
        Get
            Return _idunidadMedidaPrincipal
        End Get
        Set(value As Integer)
            _idunidadMedidaPrincipal = value
        End Set
    End Property

    Public Property IsKit() As Integer
        Get
            Return _IsKit
        End Get
        Set(ByVal value As Integer)
            _IsKit = value
        End Set
    End Property
    Public Property Formato() As String
        Get
            Return _Formato
        End Get
        Set(ByVal value As String)
            _Formato = value
        End Set
    End Property
    Public Property Atributo1() As String
        Get
            Return _Atributo1
        End Get
        Set(ByVal value As String)
            _Atributo1 = value
        End Set
    End Property
    Public Property Pais() As String
        Get
            Return _Pais
        End Get
        Set(ByVal value As String)
            _Pais = value
        End Set
    End Property
    Public Property Equivalencia() As String
        Get
            Return _Equivalencia
        End Get
        Set(ByVal value As String)
            _Equivalencia = value
        End Set
    End Property


    Public ReadOnly Property StockComprometidoABS() As Decimal
        Get
            Dim stkComprometidoAbs As Decimal
            stkComprometidoAbs = StockAReal - StockDisponibleN
            Return Math.Abs(stkComprometidoAbs)
        End Get
    End Property

    Public Property Flete1() As Decimal
        Get
            Return _Flete1
        End Get
        Set(ByVal value As Decimal)
            _Flete1 = value
        End Set
    End Property
    Public Property Flete2() As Decimal
        Get
            Return _Flete2
        End Get
        Set(ByVal value As Decimal)
            _Flete2 = value
        End Set
    End Property

    Public Property StockEnTransito() As Decimal
        Get
            Return _StockEnTransito
        End Get
        Set(ByVal value As Decimal)
            _StockEnTransito = value
        End Set
    End Property

    Public Property codBarraFabricante() As String
        Get
            Return _codBarraFabricante
        End Get
        Set(ByVal value As String)
            _codBarraFabricante = value
        End Set
    End Property

    Public Property Prod_CodigoBarras() As String
        Get
            Return _Prod_CodigoBarras
        End Get
        Set(ByVal value As String)
            _Prod_CodigoBarras = value
        End Set
    End Property
    Public Property NoVisible() As String
        Get
            Return _NoVisible
        End Get
        Set(ByVal value As String)
            _NoVisible = value
        End Set
    End Property
    Public Property Cod_Proveedor() As String
        Get
            Return _Cod_Proveedor
        End Get
        Set(ByVal value As String)
            _Cod_Proveedor = value
        End Set
    End Property
    Public Property IdUnidadMedida_Peso() As Integer
        Get
            Return Me._IdUnidadMedida_Peso
        End Get
        Set(ByVal value As Integer)
            Me._IdUnidadMedida_Peso = value
        End Set
    End Property

    Public Property ExisteCampania_Producto() As Boolean
        Get
            Return Me._ExisteCampania_Producto
        End Get
        Set(ByVal value As Boolean)
            Me._ExisteCampania_Producto = value
        End Set
    End Property

    Public Property Kit() As Boolean
        Get
            Return Me._Kit
        End Get
        Set(ByVal value As Boolean)
            Me._Kit = value
        End Set
    End Property

    Public Property PrecioBaseDcto() As String
        Get
            Return Me._PrecioBaseDcto
        End Get
        Set(ByVal value As String)
            Me._PrecioBaseDcto = value
        End Set
    End Property

    Public Property pvComercial() As Decimal
        Get
            Return Me._pvComercial
        End Get
        Set(ByVal value As Decimal)
            Me._pvComercial = value
        End Set
    End Property

    Public Property CodigoProducto() As String
        Get
            Return Me._CodigoProducto
        End Get
        Set(ByVal value As String)
            Me._CodigoProducto = value
        End Set
    End Property

    Public Property VolumenVenta() As Decimal
        Get
            Return Me._VolumenVenta
        End Get
        Set(ByVal value As Decimal)
            Me._VolumenVenta = value
        End Set
    End Property

    Public Property PorcentDctoMaximo() As Decimal
        Get
            Return Me._PorcentDctoMaximo
        End Get
        Set(ByVal value As Decimal)
            Me._PorcentDctoMaximo = value
        End Set
    End Property

    Public Property PrecioLista() As Decimal
        Get
            Return Me._PrecioLista
        End Get
        Set(ByVal value As Decimal)
            Me._PrecioLista = value
        End Set
    End Property

    Public Property Tienda() As String
        Get
            Return Me._Tienda
        End Get
        Set(ByVal value As String)
            Me._Tienda = value
        End Set
    End Property

    Public Property Almacen() As String
        Get
            Return Me._Almacen
        End Get
        Set(ByVal value As String)
            Me._Almacen = value
        End Set
    End Property

    Public Property IdAlmacen() As Integer
        Get
            Return Me._IdAlmacen
        End Get
        Set(ByVal value As Integer)
            Me._IdAlmacen = value
        End Set
    End Property

    Public Property StockDisponibleN() As Decimal
        Get
            Return Me._StockDisponibleN
        End Get
        Set(ByVal value As Decimal)
            Me._StockDisponibleN = value
        End Set
    End Property

    Public Property cadenaUM() As String
        Get
            Return _cadena
        End Get
        Set(ByVal value As String)
            _cadena = value
        End Set
    End Property

    Public ReadOnly Property ListaUM() As List(Of Entidades.ProductoUMView)
        Get

            Dim Lista As New List(Of Entidades.ProductoUMView)

            Dim IdDescripcionUM() As String = cadenaUM.Split("=")

            For x As Integer = 0 To IdDescripcionUM.Length - 1
                Dim id_UM() As String = IdDescripcionUM(x).Split(",")
                Dim obj As New Entidades.ProductoUMView
                obj.IdUnidadMedida = id_UM(0)
                obj.NombreCortoUM = id_UM(1)
                Lista.Add(obj)
            Next

            Return Lista
        End Get
    End Property

    Public Property Cantidad() As Decimal
        Get
            Return Me._Cantidad
        End Get
        Set(ByVal value As Decimal)
            Me._Cantidad = value
        End Set
    End Property

    Public Property CodigoSubLinea() As String
        Get
            Return Me._CodigoSubLinea
        End Get
        Set(ByVal value As String)
            Me._CodigoSubLinea = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property NomTipoPV() As String
        Get
            Return Me._NomTipoPV
        End Get
        Set(ByVal value As String)
            Me._NomTipoPV = value
        End Set
    End Property

    Public Property IdTipoPV() As Integer
        Get
            Return Me._IdTipoPV
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoPV = value
        End Set
    End Property

    Public Property Seleccionado() As Boolean
        Get
            Return Me._Select
        End Get
        Set(ByVal value As Boolean)
            Me._Select = value
        End Set
    End Property

    Public Property NomLinea() As String
        Get
            Return Me._NomLinea
        End Get
        Set(ByVal value As String)
            Me._NomLinea = value
        End Set
    End Property

    Public Property NomSubLinea() As String
        Get
            Return Me._NomSubLinea
        End Get
        Set(ByVal value As String)
            Me._NomSubLinea = value
        End Set
    End Property

    Public Property SimbMoneda() As String
        Get
            Return Me._SimbMoneda
        End Get
        Set(ByVal value As String)
            Me._SimbMoneda = value
        End Set
    End Property

    Public Property IdProducto() As Integer
        Get
            Return _IdProducto
        End Get
        Set(ByVal value As Integer)
            _IdProducto = value
        End Set
    End Property

    Public Property ListaProdUM() As List(Of Entidades.ProductoUMView)
        Get
            Return Me._listaProdUM
        End Get
        Set(ByVal value As List(Of Entidades.ProductoUMView))
            Me._listaProdUM = value
        End Set
    End Property

    Public Property ListaUM_Venta() As List(Of Entidades.UnidadMedida)
        Get
            Return Me._listaUM_Venta
        End Get
        Set(ByVal value As List(Of Entidades.UnidadMedida))
            Me._listaUM_Venta = value
        End Set
    End Property

    Public Property IdUnidadMedida() As Integer
        Get
            Return _IdUnidadMedida
        End Get
        Set(ByVal value As Integer)
            _IdUnidadMedida = value
        End Set
    End Property

    Public Property Elegir() As Boolean
        Get
            Return _Elegir
        End Get
        Set(ByVal value As Boolean)
            _Elegir = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return _Prod_Nombre
        End Get
        Set(ByVal value As String)
            _Prod_Nombre = value
        End Set
    End Property

    Public Property PrecioSD() As Decimal
        Get
            Return _ppv_Valor
        End Get
        Set(ByVal value As Decimal)
            _ppv_Valor = value
        End Set
    End Property

    Public Property StockAReal() As Decimal
        Get
            Return _StockReal
        End Get
        Set(ByVal value As Decimal)
            _StockReal = value
        End Set
    End Property

    Public Property StockComprometido() As Decimal
        Get
            Return _StockComprometido
        End Get
        Set(ByVal value As Decimal)
            _StockComprometido = value
        End Set
    End Property

    Public ReadOnly Property StockDisponible() As Decimal
        Get
            Return _StockReal + _StockComprometido  '********* El Stock Comprometido se retorna NEGATIVO
        End Get
    End Property

    Public Property AfectoPercepcion() As Boolean
        Get
            Return _sl_AfectoPercepcion
        End Get
        Set(ByVal value As Boolean)
            _sl_AfectoPercepcion = value
        End Set
    End Property

    Public Property Percepcion() As Decimal
        Get
            Return _Percepcion
        End Get
        Set(ByVal value As Decimal)
            _Percepcion = value
        End Set
    End Property

    Public Property AfectoDetraccion() As Boolean
        Get
            Return _sl_AfectoDetraccion
        End Get
        Set(ByVal value As Boolean)
            _sl_AfectoDetraccion = value
        End Set
    End Property

    Public Property Detraccion() As Decimal
        Get
            Return _Detraccion
        End Get
        Set(ByVal value As Decimal)
            _Detraccion = value
        End Set
    End Property

    Public Property IdTipoExistencia() As Integer
        Get
            Return _IdTipoExistencia
        End Get
        Set(ByVal value As Integer)
            _IdTipoExistencia = value
        End Set
    End Property

    Public Property UMProducto() As String
        Get
            Return _UMProducto
        End Get
        Set(ByVal value As String)
            _UMProducto = value
        End Set
    End Property


    'Public ReadOnly Property getListaProdUM() As List(Of Entidades.ProductoUMView)
    '    Get

    '        Dim lista As New List(Of Entidades.ProductoUMView)

    '        For i As Integer = 0 To Me.ListaUM_Venta.Count - 1

    '            Dim objProductoUMView As New Entidades.ProductoUMView

    '            objProductoUMView.IdUnidadMedida = Me.ListaUM_Venta(i).Id
    '            objProductoUMView.IdProducto = Me.IdProducto
    '            objProductoUMView.NombreCortoUM = Me.ListaUM_Venta(i).DescripcionCorto

    '            lista.Add(objProductoUMView)

    '        Next

    '        Return lista
    '    End Get
    'End Property

    'Public Property IdSublinea() As Integer
    '    Get
    '        Return _IdSubLinea
    '    End Get
    '    Set(ByVal value As Integer)
    '        _IdSubLinea = value
    '    End Set
    'End Property
    'Public Property TipoPrecio() As String
    '    Get
    '        Return _pv_Nombre
    '    End Get
    '    Set(ByVal value As String)
    '        _pv_Nombre = value
    '    End Set
    'End Property
End Class
