﻿Public Class be_Sector
    Private _idSector As Integer = 0
    Private _nomSector As String = String.Empty

    Public Property idSector() As Integer
        Get
            Return _idSector
        End Get
        Set(ByVal value As Integer)
            _idSector = value
        End Set
    End Property

    Public Property nomSector() As String
        Get
            Return _nomSector
        End Get
        Set(ByVal value As String)
            _nomSector = value
        End Set
    End Property
End Class
