﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

''''''''''''''''''''''MIRAYA ANAMARIA, EDGAR 03/03/2010 3:14 pm

'Esta clase esta mapeada con la Vista correspondiente en la BD,
'para agregar cosas utilice esta clase, 
'se recomienda hacerlo tb en la vista correspondiente

Public Class SerieChequeView
    Inherits SerieCheque
    'Implements System.ICloneable

    Protected _CuentaBancaria As String
    Protected _SaldoContable As Decimal
    Protected _SaldoDisponible As Decimal
    Protected _Idmoneda As Integer
    Protected _mon_Simbolo As String
    Protected _Banco As String

    Public Property CuentaBancaria() As String
        Get
            Return Me._CuentaBancaria
        End Get
        Set(ByVal value As String)
            Me._CuentaBancaria = value
        End Set
    End Property
    Public ReadOnly Property DescCuentaBancaria() As String
        Get
            Return Me.mon_Simbolo + " " + Me._CuentaBancaria
        End Get
    End Property
    Public Property SaldoContable() As Decimal
        Get
            Return Me._SaldoContable
        End Get
        Set(ByVal value As Decimal)
            Me._SaldoContable = value
        End Set
    End Property
    Public ReadOnly Property DescSaldoContable() As String
        Get
            Return Me._SaldoContable.ToString("F")
        End Get
    End Property
    Public Property SaldoDisponible() As Decimal
        Get
            Return Me._SaldoDisponible
        End Get
        Set(ByVal value As Decimal)
            Me._SaldoDisponible = value
        End Set
    End Property
    Public ReadOnly Property DescSaldoDisponible() As String
        Get
            Return Me._SaldoDisponible.ToString("F")
        End Get
    End Property
    Public Property IdMoneda() As Integer
        Get
            Return Me._Idmoneda
        End Get
        Set(ByVal value As Integer)
            Me._Idmoneda = value
        End Set
    End Property
    Public Property mon_Simbolo() As String
        Get
            Return Me._mon_Simbolo
        End Get
        Set(ByVal value As String)
            Me._mon_Simbolo = value
        End Set
    End Property
    Public Property Banco() As String
        Get
            Return Me._Banco
        End Get
        Set(ByVal value As String)
            Me._Banco = value
        End Set
    End Property

    Public Overrides Function Clone() As Object 'Implements System.ICloneable.Clone
        Dim x As New SerieChequeView

        'x = CType(MyBase.Clone, SerieChequeView)
        'x = MyBase.Clone

        x._IdSerieCheque = Me._IdSerieCheque
        x._IdBanco = Me._IdBanco
        x._IdCuentaBancaria = Me._IdCuentaBancaria
        x._sc_Serie = Me._sc_Serie
        x._sc_NroInicio = Me._sc_NroInicio
        x._sc_NroFin = Me._sc_NroFin
        x._sc_Longitud = Me._sc_Longitud
        x._sc_Estado = Me._sc_Estado
        x._sc_Descripcion = Me._sc_Descripcion

        x.CuentaBancaria = Me.CuentaBancaria
        x.SaldoContable = Me.SaldoContable
        x.SaldoDisponible = Me.SaldoDisponible
        x.IdMoneda = Me.IdMoneda
        x.mon_Simbolo = Me.mon_Simbolo
        x.Banco = Me.Banco
        Return x
    End Function

End Class
