﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class ConsultaDeudaCxC_View
    Private _IdPersona As Integer
    Private _doc_Numero As String
    Private _Nombre As String
    Private _jur_RSocial As String
    Private _mon_Simbolo As String
    Private _cp_Saldo As Double

    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Public Property DocumentoI() As String
        Get
            Return _doc_Numero
        End Get
        Set(ByVal value As String)
            _doc_Numero = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property

    Public Property RazonSocial() As String
        Get
            Return _jur_RSocial
        End Get
        Set(ByVal value As String)
            _jur_RSocial = value
        End Set
    End Property

    Public Property SimboloM() As String
        Get
            Return _mon_Simbolo
        End Get
        Set(ByVal value As String)
            _mon_Simbolo = value
        End Set
    End Property

    Public Property Saldo() As Double
        Get
            Return _cp_Saldo
        End Get
        Set(ByVal value As Double)
            _cp_Saldo = value
        End Set
    End Property
    Public ReadOnly Property getNombreParaMostrar() As String
        Get
            If Me.RazonSocial.Trim.Length > 0 Then
                Return Me.RazonSocial
            ElseIf Me.Nombre.Trim.Length > 0 Then
                Return Me.Nombre
            Else
                Return ""
            End If
        End Get
    End Property
End Class
