﻿Public Class be_productoStock

    Private _idProducto As Integer = 0
    Private _idalmacen As Integer = 0
    Private _idsector As Integer = 0
    Private _idTono As Integer = 0
    Private _existencia As String = String.Empty
    Private _linea As String = String.Empty
    Private _subLinea As String = String.Empty
    Private _codSige As String = String.Empty
    Private _producto As String = String.Empty
    Private _almacen As String = String.Empty
    Private _tienda As String = String.Empty
    Private _sector As String = String.Empty
    Private _UnidadMedida As String = String.Empty
    Private _stockKardex As Decimal = 0
    Private _stockTono As String = String.Empty
    Private _tonoDisponible As String = String.Empty
    Private _listaSector As List(Of Entidades.be_Sector)

    Public Property idProducto() As Integer
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Integer)
            _idProducto = value
        End Set
    End Property

    Public Property idalmacen() As Integer
        Get
            Return _idalmacen
        End Get
        Set(ByVal value As Integer)
            _idalmacen = value
        End Set
    End Property

    Public Property idsector() As Integer
        Get
            Return _idsector
        End Get
        Set(ByVal value As Integer)
            _idsector = value
        End Set
    End Property

    Public Property idTono() As Integer
        Get
            Return _idTono
        End Get
        Set(ByVal value As Integer)
            _idTono = value
        End Set
    End Property

    Public Property existencia() As String
        Get
            Return _existencia
        End Get
        Set(ByVal value As String)
            _existencia = value
        End Set
    End Property

    Public Property linea() As String
        Get
            Return _linea
        End Get
        Set(ByVal value As String)
            _linea = value
        End Set
    End Property

    Public Property sublinea() As String
        Get
            Return _subLinea
        End Get
        Set(ByVal value As String)
            _subLinea = value
        End Set
    End Property

    Public Property codSige() As String
        Get
            Return _codSige
        End Get
        Set(ByVal value As String)
            _codSige = value
        End Set
    End Property

    Public Property producto() As String
        Get
            Return _producto
        End Get
        Set(ByVal value As String)
            _producto = value
        End Set
    End Property

    Public Property almacen() As String
        Get
            Return _almacen
        End Get
        Set(ByVal value As String)
            _almacen = value
        End Set
    End Property

    Public Property tienda() As String
        Get
            Return _tienda
        End Get
        Set(ByVal value As String)
            _tienda = value
        End Set
    End Property

    Public Property sector() As String
        Get
            Return _sector
        End Get
        Set(ByVal value As String)
            _sector = value
        End Set
    End Property

    Public Property UnidadMedida() As String
        Get
            Return _UnidadMedida
        End Get
        Set(ByVal value As String)
            _UnidadMedida = value
        End Set
    End Property

    Public Property stockKardex() As Decimal
        Get
            Return _stockKardex
        End Get
        Set(ByVal value As Decimal)
            _stockKardex = value
        End Set
    End Property

    Public Property stockTono() As String
        Get
            Return _stockTono
        End Get
        Set(ByVal value As String)
            _stockTono = value
        End Set
    End Property

    Public Property tonoDisponible() As String
        Get
            Return _tonoDisponible
        End Get
        Set(ByVal value As String)
            _tonoDisponible = value
        End Set
    End Property

    Public Property listaSector() As List(Of Entidades.be_Sector)
        Get
            Return _listaSector
        End Get
        Set(ByVal value As List(Of Entidades.be_Sector))
            _listaSector = value
        End Set
    End Property
End Class
