﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class GrillaProducto_E
    Private _IdProducto As Integer
    Private _Serie As String
    Private _NomProducto As String
    Private _NomComercial As String
    Private _NomPerNatural As String
    Private _NomEstado As String
    Private _NomPropietario As String
    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property
    Public Property Serie() As String
        Get
            Return Me._Serie
        End Get
        Set(ByVal value As String)
            Me._Serie = value
        End Set
    End Property
    Public Property NomProducto() As String
        Get
            Return Me._NomProducto
        End Get
        Set(ByVal value As String)
            Me._NomProducto = value
        End Set
    End Property
    Public Property NomComercial() As String
        Get
            Return Me._NomComercial
        End Get
        Set(ByVal value As String)
            Me._NomComercial = value
        End Set
    End Property
    Public Property NomPerNatural() As String
        Get
            Return Me._NomPerNatural
        End Get
        Set(ByVal value As String)
            Me._NomPerNatural = value
        End Set
    End Property
    Public Property NomEstado() As String
        Get
            Return Me._NomEstado
        End Get
        Set(ByVal value As String)
            Me._NomEstado = value
        End Set
    End Property
    Public ReadOnly Property NomPropietario() As String
        Get
            Me._NomPropietario = Me._NomComercial
            If Me._NomPropietario.Trim = "" Then
                Me._NomPropietario = Me._NomPerNatural
            End If
            Return Me._NomPropietario
        End Get
    End Property
End Class
