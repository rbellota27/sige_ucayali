﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class DetalleReciboView
    Private _IdMovCuenta As Integer
    Private _Concepto As String
    Private _IdMoneda As Integer
    Private _SaldoActual As Decimal
    Private _SaldoNuevo As Decimal
    Private _Importe As Decimal
    Private _MonedaSimbolo As String

    Public Property MonedaSimbolo() As String
        Get
            Return Me._MonedaSimbolo
        End Get
        Set(ByVal value As String)
            Me._MonedaSimbolo = value
        End Set
    End Property

    Public Property IdMovCuenta() As Integer
        Get
            Return Me._IdMovCuenta
        End Get
        Set(ByVal value As Integer)
            Me._IdMovCuenta = value
        End Set
    End Property

    Public Property Concepto() As String
        Get
            Return Me._Concepto
        End Get
        Set(ByVal value As String)
            Me._Concepto = value
        End Set
    End Property

    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property

    Public Property SaldoActual() As Decimal
        Get
            Return Me._SaldoActual
        End Get
        Set(ByVal value As Decimal)
            Me._SaldoActual = value
        End Set
    End Property

    Public Property SaldoNuevo() As Decimal
        Get
            Return Me._SaldoNuevo
        End Get
        Set(ByVal value As Decimal)
            Me._SaldoNuevo = value
        End Set
    End Property

    Public Property Importe() As Decimal
        Get
            Return Me._Importe
        End Get
        Set(ByVal value As Decimal)
            Me._Importe = value
        End Set
    End Property
End Class
