﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.








'**************************    MARTES  30 MARZO 2010 HORA 02_14 PM











Public Class ProductoUMCostoCompraView
    Private _IdProducto As Integer
    Private _prod_Nombre As String
    Private _um_NombreCorto As String
    Private _prod_PrecioCompra As Decimal
    Private _IdMoneda As Integer

    Private _CodigoProducto As String

    Public Property CodigoProducto() As String
        Get
            Return Me._CodigoProducto
        End Get
        Set(ByVal value As String)
            Me._CodigoProducto = value
        End Set
    End Property



    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property
    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property

    Public Property NomProducto() As String
        Get
            Return Me._prod_Nombre
        End Get
        Set(ByVal value As String)
            Me._prod_Nombre = value
        End Set
    End Property

    Public Property NomUMPrincipal() As String
        Get
            Return Me._um_NombreCorto
        End Get
        Set(ByVal value As String)
            Me._um_NombreCorto = value
        End Set
    End Property

    Public Property PrecioCompra() As Decimal
        Get
            Return Me._prod_PrecioCompra
        End Get
        Set(ByVal value As Decimal)
            Me._prod_PrecioCompra = value
        End Set
    End Property
End Class
