﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'***************** JUEVES 21 ENERO 2010 HORA 03_39 PM

Public Class DocumentoView

    Inherits Entidades.Documento

    Private _IdDocumento As Integer
    Private _doc_Codigo As String
    Private _doc_Serie As String
    Private _doc_FechaRegistro As Date
    Private _tie_Nombre As String
    Private _per_NComercial As String
    Private _tdoc_NombreCorto As String
    Private _RazonSocial As String
    Private _FechaEmision As String
    Private _NombreNatural As String
    Private _NomEstadoEntrega As String
    Private _DNI As String
    Private _RUC As String
    Private _Numero As String
    Private _NomAlmacen As String
    Private _strTipoDocumentoRef As String
    Private _strTipoOperacionRef As String

    Private _NomContactoProveedor As String
    Private _NomContactoPropietario As String
    Private _NomMonedaDestino As String
    Private _Caja As String
    Private _MotivoTraslado As String
    Private _TipoOperacion As String
    Private _strIdDocumentoRef As String
    Public Sub New()
    End Sub

    Public Property strIdDocumentoRef() As String
        Get
            Return _strIdDocumentoRef
        End Get
        Set(ByVal value As String)
            _strIdDocumentoRef = value
        End Set
    End Property
    Public Sub New(ByVal id As Integer, ByVal IdDocumento As String)
        Me.Id = id
        Me.IdDocRelacionado = IdDocumento
    End Sub

    'Public ReadOnly Property getIdDocumentoRef() As List(Of Entidades.DocumentoView)
    '    Get
    '        If strIdDocumentoRef <> "" Then
    '            Dim lista As New List(Of Entidades.DocumentoView)
    '            Dim IdDocumentoRef() As String
    '            IdDocumentoRef = strIdDocumentoRef.Split(CChar(","))
    '            For x As Integer = 0 To IdDocumentoRef.Length - 1
    '                If IdDocumentoRef(x) <> "" Then
    '                    lista.Add(New Entidades.DocumentoView(0, IdDocumentoRef(x)))
    '                End If
    '            Next
    '            Return lista
    '        End If
    '        Return Nothing
    '    End Get
    'End Property

    Public ReadOnly Property getTipoDocumentoRef() As List(Of Entidades.TipoDocumento)
        Get
            If strTipoDocumentoRef <> "" Then
                Dim lista As New List(Of Entidades.TipoDocumento)
                Dim TipoDocumentoRef() As String

                TipoDocumentoRef = strTipoDocumentoRef.Split(CChar(","))

                For x As Integer = 0 To TipoDocumentoRef.Length - 1
                    If TipoDocumentoRef(x) <> "" Then
                        lista.Add(New Entidades.TipoDocumento(0, TipoDocumentoRef(x)))
                    End If
                Next
                Return lista
            End If
            Return Nothing
        End Get
    End Property



    Public Property MotivoTraslado() As String
        Get
            Return _MotivoTraslado
        End Get
        Set(ByVal value As String)
            _MotivoTraslado = value
        End Set
    End Property


    Public Property TipoOperacion() As String
        Get
            Return _TipoOperacion
        End Get
        Set(ByVal value As String)
            _TipoOperacion = value
        End Set
    End Property



    Public Property Caja() As String
        Get
            Return _Caja
        End Get
        Set(ByVal value As String)
            _Caja = value
        End Set
    End Property

    Public Property NomMonedaDestino() As String
        Get
            Return Me._NomMonedaDestino
        End Get
        Set(ByVal value As String)
            Me._NomMonedaDestino = value
        End Set
    End Property

    Public Property NomContactoProveedor() As String
        Get
            Return _NomContactoProveedor
        End Get
        Set(ByVal value As String)
            _NomContactoProveedor = value
        End Set
    End Property

    Public Property NomContactoPropietario() As String
        Get
            Return _NomContactoPropietario
        End Get
        Set(ByVal value As String)
            _NomContactoPropietario = value
        End Set
    End Property


    Public Property strTipoDocumentoRef() As String
        Get
            Return _strTipoDocumentoRef
        End Get
        Set(ByVal value As String)
            _strTipoDocumentoRef = value
        End Set
    End Property

    Public Property strTipoOperacionRef() As String
        Get
            Return _strTipoOperacionRef
        End Get
        Set(ByVal value As String)
            _strTipoOperacionRef = value
        End Set
    End Property

    Public Property Numero() As String
        Get
            Return _Numero
        End Get
        Set(ByVal value As String)
            _Numero = value
        End Set
    End Property
    Public Property NomAlmacen() As String
        Get
            Return Me._NomAlmacen
        End Get
        Set(ByVal value As String)
            Me._NomAlmacen = value
        End Set
    End Property

    Public ReadOnly Property getNombreAMostrar() As String
        Get
            If Me.RazonSocial.Trim.Length > 0 Then
                Return Me.RazonSocial
            Else
                Return Me.NombreNatural
            End If
        End Get
    End Property





    Public Property DNI() As String
        Get
            Return Me._DNI
        End Get
        Set(ByVal value As String)
            Me._DNI = value
        End Set
    End Property

    Public Property RUC() As String
        Get
            Return Me._RUC
        End Get
        Set(ByVal value As String)
            Me._RUC = value
        End Set
    End Property

    Public Property RazonSocial() As String
        Get
            Return Me._RazonSocial
        End Get
        Set(ByVal value As String)
            Me._RazonSocial = value
        End Set
    End Property

    Public Property FechaEmision() As String
        Get
            Return Me._FechaEmision
        End Get
        Set(ByVal value As String)
            Me._FechaEmision = value
        End Set
    End Property

    Public Property NombreNatural() As String
        Get
            Return Me._NombreNatural
        End Get
        Set(ByVal value As String)
            Me._NombreNatural = value
        End Set
    End Property

    Public Property NomEstadoEntrega() As String
        Get
            Return Me._NomEstadoEntrega
        End Get
        Set(ByVal value As String)
            Me._NomEstadoEntrega = value
        End Set
    End Property





    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property

    Public Property Codigo() As String
        Get
            Return Me._doc_Codigo
        End Get
        Set(ByVal value As String)
            Me._doc_Codigo = value
        End Set
    End Property

    Public Property Serie() As String
        Get
            Return Me._doc_Serie
        End Get
        Set(ByVal value As String)
            Me._doc_Serie = value
        End Set
    End Property

    Public Property FechaRegistro() As Date
        Get
            Return Me._doc_FechaRegistro
        End Get
        Set(ByVal value As Date)
            Me._doc_FechaRegistro = value
        End Set
    End Property

    Public Property NomTienda() As String
        Get
            Return Me._tie_Nombre
        End Get
        Set(ByVal value As String)
            Me._tie_Nombre = value
        End Set
    End Property

    Public Property NomPropietario() As String
        Get
            Return Me._per_NComercial
        End Get
        Set(ByVal value As String)
            Me._per_NComercial = value
        End Set
    End Property

    Public Property NomTipoDocumento() As String
        Get
            Return Me._tdoc_NombreCorto
        End Get
        Set(ByVal value As String)
            Me._tdoc_NombreCorto = value
        End Set
    End Property


End Class
