﻿Public Class be_guiaRemision
    Private _idTienda As Integer = 0
    Private _idSerie As Integer = 0
    Private _docCodigo As String = String.Empty
    Private _idDocumento As Integer = 0
    Private _numero As String = String.Empty
    Private _peso As Decimal = 0
    Private _cliente As String = String.Empty
    Private _distrito As String = String.Empty
    Private _tie_nombre As String = String.Empty
    Private _fecFinDespacho As DateTime = Date.Now
    Private _mt_Nombre As String = String.Empty

    Public Property idTienda() As Integer
        Get
            Return _idTienda
        End Get
        Set(ByVal value As Integer)
            _idTienda = value
        End Set
    End Property

    Public Property idSerie() As Integer
        Get
            Return _idSerie
        End Get
        Set(ByVal value As Integer)
            _idSerie = value
        End Set
    End Property

    Public Property doc_Codigo() As String
        Get
            Return _docCodigo
        End Get
        Set(ByVal value As String)
            _docCodigo = value
        End Set
    End Property

    Public Property idDocumento() As Integer
        Get
            Return _idDocumento
        End Get
        Set(ByVal value As Integer)
            _idDocumento = value
        End Set
    End Property

    Public Property numero() As String
        Get
            Return _numero
        End Get
        Set(ByVal value As String)
            _numero = value
        End Set
    End Property

    Public Property peso() As Decimal
        Get
            Return _peso
        End Get
        Set(ByVal value As Decimal)
            _peso = value
        End Set
    End Property

    Public Property cliente() As String
        Get
            Return _cliente
        End Get
        Set(ByVal value As String)
            _cliente = value
        End Set
    End Property

    Public Property distrito() As String
        Get
            Return _distrito
        End Get
        Set(ByVal value As String)
            _distrito = value
        End Set
    End Property

    Public Property tie_nombre() As String
        Get
            Return _tie_nombre
        End Get
        Set(ByVal value As String)
            _tie_nombre = value
        End Set
    End Property

    Public Property fecFinDespacho() As DateTime
        Get
            Return _fecFinDespacho
        End Get
        Set(ByVal value As DateTime)
            _fecFinDespacho = value
        End Set
    End Property

    Public Property mt_Nombre() As String
        Get
            Return _mt_Nombre
        End Get
        Set(ByVal value As String)
            _mt_Nombre = value
        End Set
    End Property
End Class
