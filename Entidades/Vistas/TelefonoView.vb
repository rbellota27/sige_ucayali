﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class TelefonoView
    Private _IdTelefono As Integer
    Private _tel_Numero As String
    Private _tel_Anexo As String
    Private _tel_Prioridad As Boolean
    Private _IdPersona As Integer
    Private _IdTipoTelefono As Integer
    Private _Tipo As String
    Private _DescEstado As String
    Private _DescTipoTel As String
    Public Property Id() As Integer
        Get
            Return Me._IdTelefono
        End Get
        Set(ByVal value As Integer)
            Me._IdTelefono = value
        End Set
    End Property

    Public Property Numero() As String
        Get
            Return Me._tel_Numero
        End Get
        Set(ByVal value As String)
            Me._tel_Numero = value
        End Set
    End Property

    Public Property Anexo() As String
        Get
            Return Me._tel_Anexo
        End Get
        Set(ByVal value As String)
            Me._tel_Anexo = value
        End Set
    End Property

    Public Property Prioridad() As Boolean
        Get
            Return Me._tel_Prioridad
        End Get
        Set(ByVal value As Boolean)
            Me._tel_Prioridad = value
        End Set
    End Property

    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Public Property IdTipoTelefono() As Integer
        Get
            Return Me._IdTipoTelefono
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoTelefono = value
        End Set
    End Property
    Public Property Tipo() As String
        Get
            Return Me._Tipo
        End Get
        Set(ByVal value As String)
            Me._Tipo = value
        End Set
    End Property
    Public ReadOnly Property DescEstado() As String
        Get
            Me._DescEstado = "Si"
            If _tel_Prioridad = "0" Then
                Me._DescEstado = "No"
            End If
            Return Me._DescEstado
        End Get
    End Property
    Public Property DescTipoTel() As String
        Get
            Return Me._DescTipoTel
        End Get
        Set(ByVal value As String)
            Me._DescTipoTel = value
        End Set
    End Property

    Private _objTelefono As Object
    Public Property objTelefono() As Object
        Get
            Return _objTelefono
        End Get
        Set(ByVal value As Object)
            _objTelefono = value
        End Set
    End Property

End Class
