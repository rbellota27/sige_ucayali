﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

''''''''''''''''''''''MIRAYA ANAMARIA, EDGAR 03/02/2010 12:12 pm
Public Class ConceptoMovBancoView
    Inherits ConceptoMovBanco

    Protected _TipoConceptoBanco As String

    Public Property TipoConceptoBanco() As String
        Get
            Return Me._TipoConceptoBanco
        End Get
        Set(ByVal value As String)
            Me._TipoConceptoBanco = value
        End Set
    End Property


End Class
