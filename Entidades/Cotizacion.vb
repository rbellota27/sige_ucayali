'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Cotizacion
    Private _IdDocumento As Integer
    Private _cot_DiasOferta As Integer
    Private _cot_IncluyeTransporte As String
    Private _cot_PreciosConIgv As String
    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property

    Public Property DiasOferta() As Integer
        Get
            Return Me._cot_DiasOferta
        End Get
        Set(ByVal value As Integer)
            Me._cot_DiasOferta = value
        End Set
    End Property

    Public Property IncluyeTransporte() As String
        Get
            Return Me._cot_IncluyeTransporte
        End Get
        Set(ByVal value As String)
            Me._cot_IncluyeTransporte = value
        End Set
    End Property

    Public Property PreciosConIgv() As String
        Get
            Return Me._cot_PreciosConIgv
        End Get
        Set(ByVal value As String)
            Me._cot_PreciosConIgv = value
        End Set
    End Property
End Class
