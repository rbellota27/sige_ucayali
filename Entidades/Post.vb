﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'Jueves 18 Febrero 3:03 PM se cambio el new()
''''''''''''''''''''''MIRAYA ANAMARIA, EDGAR 01/02/2010 9:52 pm
Public Class Post
    'No cambiar esta clase, para agregar cosas utilice la clase PostView
    Protected _IdPost As Integer
    Protected _po_Descripcion As String
    Protected _po_Identificador As String
    Protected _po_Estado As Nullable(Of Boolean)
    Protected _IdBanco As Integer
    Protected _IdCuentaBancaria As Integer

    Public Sub New()
        'Esta inicialización es para las busquedas que se realizan en la DAO, sp _SelectxParams
        'Se inicializan los Id's con -1, que en este caso significa Todos,
        'para diferenciar de 0, que los combos utilizan como ninguno
        'solo los id's son inicalizados asi
        Me._IdPost = -1
        Me._po_Identificador = ""
        Me._po_Descripcion = ""
        Me._po_Estado = Nothing
        Me._IdBanco = -1
        Me._IdCuentaBancaria = -1

    End Sub

    Public Sub New(ByVal IdPost As Integer, ByVal po_Descripcion As String, _
                    ByVal po_Identificador As String, ByVal po_Estado As Boolean, _
                    ByVal IdBanco As Integer, ByVal IdCuentaBancaria As Integer)

        Me._IdPost = IdPost
        Me._po_Descripcion = po_Descripcion
        Me._po_Identificador = po_Identificador
        Me._po_Estado = po_Estado
        Me._IdBanco = IdBanco
        Me._IdCuentaBancaria = IdCuentaBancaria

    End Sub

    Public Property Id() As Integer
        Get
            Return Me._IdPost
        End Get
        Set(ByVal value As Integer)
            Me._IdPost = value
        End Set
    End Property
    Public Property Nombre() As String  'Equivale a la Descripcion 
        Get
            Return Me._po_Identificador
        End Get
        Set(ByVal value As String)
            Me._po_Identificador = value
        End Set
    End Property
    Public Property Estado() As Boolean?
        Get
            Return Me._po_Estado
        End Get
        Set(ByVal value As Boolean?)
            Me._po_Estado = value
        End Set
    End Property
    Public ReadOnly Property DescEstado() As String
        Get
            If Me._po_Estado Then
                Return "Activo"
            Else
                Return "Inactivo"
            End If
        End Get
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._po_Descripcion
        End Get
        Set(ByVal value As String)
            Me._po_Descripcion = value
        End Set
    End Property
    Public Property Identificador() As String
        Get
            Return Me._po_Identificador
        End Get
        Set(ByVal value As String)
            Me._po_Identificador = value
        End Set
    End Property
    Public Property IdBanco() As Integer
        Get
            Return Me._IdBanco
        End Get
        Set(ByVal value As Integer)
            Me._IdBanco = value
        End Set
    End Property
    Public Property IdCuentaBancaria() As Integer
        Get
            Return Me._IdCuentaBancaria
        End Get
        Set(ByVal value As Integer)
            Me._IdCuentaBancaria = value
        End Set
    End Property

End Class
