'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Fabricante
    Private _IdFabricante As Integer
    Private _fab_NombreLargo As String
    Private _fab_NombreCorto As String
    Private _fab_Estado As String
    Private _fab_codigo As String
    Private _descEstado As String
    Private _IdPais As String
    Private _paNombre As String
    Private _fabestadov2 As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal idfabricante As Integer, ByVal nombrecorto As String, ByVal estado As Boolean)
        Me._IdFabricante = idfabricante
        Me._fab_NombreCorto = nombrecorto
        Me._fabestadov2 = estado
    End Sub
    Public Property Id() As Integer
        Get
            Return Me._IdFabricante
        End Get
        Set(ByVal value As Integer)
            Me._IdFabricante = value
        End Set
    End Property
    Public Property NombreLargo() As String
        Get
            Return Me._fab_NombreLargo
        End Get
        Set(ByVal value As String)
            Me._fab_NombreLargo = value
        End Set
    End Property
    Public Property NombreCorto() As String
        Get
            Return Me._fab_NombreCorto
        End Get
        Set(ByVal value As String)
            Me._fab_NombreCorto = value
        End Set
    End Property
    Public Property fab_codigo() As String
        Get
            Return Me._fab_codigo
        End Get
        Set(ByVal value As String)
            Me._fab_codigo = value
        End Set
    End Property
    Public ReadOnly Property DescEstado()
        Get
            Me._descEstado = "Activo"
            If Me._fab_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public Property Estado() As String
        Get
            Return Me._fab_Estado
        End Get
        Set(ByVal value As String)
            Me._fab_Estado = value
        End Set
    End Property
    Public Property IdPais() As String
        Get
            Return Me._IdPais
        End Get
        Set(ByVal value As String)
            Me._IdPais = value
        End Set
    End Property
    Public Property NomPais() As String
        Get
            Return Me._paNombre
        End Get
        Set(ByVal value As String)
            Me._paNombre = value
        End Set
    End Property



End Class
