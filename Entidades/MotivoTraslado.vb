'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor     : Chang Carnero, Edgar
'Sistema   : SIGE
'Empresa   : Digrafic SRL
'Modificado: 02-Noviembre-2009
'Hora      : 01:35 am
'*************************************************
'*************************************************
'Autor     : Chang Carnero, Edgar
'Sistema   : SIGE
'Empresa   : Digrafic SRL
'Modificado: 12-Oct-2009
'Hora      : 12:22 pm
'*************************************************
Public Class MotivoTraslado
    Private _IdMotivoT As Integer
    Private _mt_Nombre As String
    Private _mt_Estado As String
    Private _descEstado As String
    Private _DocumentoReferente As Boolean
    Private _DescDocumentoReferente As String
    '******** Tipo Operaci�n
    Private _IdTipoOperacion As Integer
    Private _top_Nombre As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal idmotivoTraslado As Integer, ByVal nombre As String)
        Me.Id = idmotivoTraslado
        Me.Nombre = nombre
    End Sub
    Public ReadOnly Property DescEstado() As String
        Get
            Me._descEstado = "Activo"
            If Me._mt_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public Property Id() As Integer
        Get
            Return Me._IdMotivoT
        End Get
        Set(ByVal value As Integer)
            Me._IdMotivoT = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._mt_Nombre
        End Get
        Set(ByVal value As String)
            Me._mt_Nombre = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._mt_Estado
        End Get
        Set(ByVal value As String)
            Me._mt_Estado = value
        End Set
    End Property
    Public Property IdTipoOperacion() As Integer
        Get
            Return Me._IdTipoOperacion
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoOperacion = value
        End Set
    End Property
    Public Property NombreTipoOperacion() As String
        Get
            Return Me._top_Nombre
        End Get
        Set(ByVal value As String)
            Me._top_Nombre = value
        End Set
    End Property
    Public Property DocumentoReferente() As Boolean
        Get
            Return _DocumentoReferente
        End Get
        Set(ByVal value As Boolean)
            _DocumentoReferente = value
        End Set
    End Property
    Public ReadOnly Property DescDocumentoReferente() As String
        Get
            If DocumentoReferente = False Then _DescDocumentoReferente = "No Compromete Stock"
            If DocumentoReferente = True Then _DescDocumentoReferente = "Si Compromete Stock"
            Return _DescDocumentoReferente
        End Get
    End Property
End Class
