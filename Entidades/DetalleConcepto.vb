﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




'*********************   LUNES 24052010



Public Class DetalleConcepto
    Private _IdConcepto As Integer
    Private _IdDocumento As Integer
    Private _Monto As Decimal
    Private _IdMoneda As Integer
    Private _Moneda As String
    Private _Descripcion As String
    Private _FactorMov As Integer
    Private _listaConcepto As List(Of Entidades.Concepto)
    Private _IdFlete As Integer
    Private _IdTipoDocumentoRef As Integer
    Private _dg_NroDocumento As String
    Private _dr_Concepto As String
    Private _IdDetalleConcepto As Integer
    Private _IdProveedor As Integer
    Private _IdDocumentoRef As Integer
    Private _ConceptoAdelanto As Boolean
    Private _IdMovCuentaRef As Integer
    Private _Monto2 As Decimal
    Private _IdMotivoGasto As Integer
    Private _listaMotivoGasto As List(Of Entidades.MotivoGasto)
    Private _PorcentDetraccion As Decimal
    Private _MontoMinDetraccion As Decimal
    Private _IdProducto As Integer
    Private _con_NoAfectoIGV As Boolean
    Private _PorcentDsctoGlobal As Decimal
    Private _Sustento As Integer

    Public Sub New()
        Me._IdProducto = 0
    End Sub
    Public Property ResultSustento() As Integer
        Get
            Return Me._Sustento
        End Get
        Set(ByVal value As Integer)
            Me._Sustento = value
        End Set
    End Property

    Public Property PorcentDsctoGlobal() As Decimal
        Get
            Return _PorcentDsctoGlobal
        End Get
        Set(ByVal value As Decimal)
            _PorcentDsctoGlobal = value
        End Set
    End Property

    Public Property NoAfectoIGV() As Boolean
        Get
            Return Me._con_NoAfectoIGV
        End Get
        Set(ByVal value As Boolean)
            Me._con_NoAfectoIGV = value
        End Set
    End Property

    Public Property IdProducto() As Decimal
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Decimal)
            Me._IdProducto = value
        End Set
    End Property

    Public Property PorcentDetraccion() As Decimal
        Get
            Return Me._PorcentDetraccion
        End Get
        Set(ByVal value As Decimal)
            Me._PorcentDetraccion = value
        End Set
    End Property

    Public Property MontoMinDetraccion() As Decimal
        Get
            Return Me._MontoMinDetraccion
        End Get
        Set(ByVal value As Decimal)
            Me._MontoMinDetraccion = value
        End Set
    End Property

    Public Property ListaMotivoGasto() As List(Of Entidades.MotivoGasto)
        Get
            Return Me._listaMotivoGasto
        End Get
        Set(ByVal value As List(Of Entidades.MotivoGasto))
            Me._listaMotivoGasto = value
        End Set
    End Property

    Public Property IdMotivoGasto() As Integer
        Get
            Return Me._IdMotivoGasto
        End Get
        Set(ByVal value As Integer)
            Me._IdMotivoGasto = value
        End Set
    End Property

    Public Property Monto2() As Decimal
        Get
            Return Me._Monto2
        End Get
        Set(ByVal value As Decimal)
            Me._Monto2 = value
        End Set
    End Property

    Public Property IdMovCuentaRef() As Integer
        Get
            Return Me._IdMovCuentaRef
        End Get
        Set(ByVal value As Integer)
            Me._IdMovCuentaRef = value
        End Set
    End Property

    Public Property IdDocumentoRef() As Integer
        Get
            Return Me._IdDocumentoRef
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumentoRef = value
        End Set
    End Property

    Public Property ConceptoAdelanto() As Boolean
        Get
            Return Me._ConceptoAdelanto
        End Get
        Set(ByVal value As Boolean)
            Me._ConceptoAdelanto = value
        End Set
    End Property

    Public ReadOnly Property ConceptoAdelanto_Integer() As Integer
        Get
            If (Me.ConceptoAdelanto) Then
                Return 1
            Else
                Return 0
            End If
        End Get

    End Property

    Public ReadOnly Property NoAfectoIgv_Integer() As Integer
        Get
            If (Me.NoAfectoIGV) Then
                Return 1
            Else
                Return 0
            End If
        End Get

    End Property

    Public ReadOnly Property NoAfectoIgv_String() As String
        Get
            If (Me.NoAfectoIGV) Then
                Return "No Afecto I.G.V."
            Else
                Return ""
            End If
        End Get

    End Property

    Public Property IdDetalleConcepto() As Integer
        Get
            Return Me._IdDetalleConcepto
        End Get
        Set(ByVal value As Integer)
            Me._IdDetalleConcepto = value
        End Set
    End Property

    Public Property Concepto() As String
        Get
            Return Me._dr_Concepto
        End Get
        Set(ByVal value As String)
            Me._dr_Concepto = value
        End Set
    End Property

    Public Property NroDocumento() As String
        Get
            Return Me._dg_NroDocumento
        End Get
        Set(ByVal value As String)
            Me._dg_NroDocumento = value
        End Set
    End Property

    Public Property IdTipoDocumentoRef() As Integer
        Get
            Return Me._IdTipoDocumentoRef
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoDocumentoRef = value
        End Set
    End Property

    Public Property IdFlete() As Integer
        Get
            Return Me._IdFlete
        End Get
        Set(ByVal value As Integer)
            Me._IdFlete = value
        End Set
    End Property

    Public Property ListaConcepto() As List(Of Entidades.Concepto)
        Get
            Return Me._listaConcepto
        End Get
        Set(ByVal value As List(Of Entidades.Concepto))
            Me._listaConcepto = value
        End Set
    End Property

    Public Property IdConcepto() As Integer
        Get
            Return Me._IdConcepto
        End Get
        Set(ByVal value As Integer)
            Me._IdConcepto = value
        End Set
    End Property

    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property

    Public Property Monto() As Decimal
        Get
            Return Me._Monto
        End Get
        Set(ByVal value As Decimal)
            Me._Monto = value
        End Set
    End Property

    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property

    Public Property Moneda() As String
        Get
            Return Me._Moneda
        End Get
        Set(ByVal value As String)
            Me._Moneda = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._Descripcion
        End Get
        Set(ByVal value As String)
            Me._Descripcion = value
        End Set
    End Property

    Public Property FactorMov() As Integer
        Get
            Return Me._FactorMov
        End Get
        Set(ByVal value As Integer)
            Me._FactorMov = value
        End Set
    End Property

    Public Property IdProveedor() As Integer
        Get
            Return _IdProveedor
        End Get
        Set(ByVal value As Integer)
            _IdProveedor = value
        End Set
    End Property



End Class
