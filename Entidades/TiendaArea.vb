﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'cambios 03/02/2010
'*************************************************
'Autor   : Nagamine Molina, Jorge
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 21-Octubre-2009
'Hora    : 04:02 pm
'*************************************************


Public Class TiendaArea
    Private _IdTienda As Integer
    Private _IdArea As Integer
    Private _ar_NombreCorto As String
    Private _Id As Integer
    Private _EstadoAE As Boolean
    Private _IdCentroCosto As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal id As Integer, ByVal nombre As String, ByVal idtienda As Integer, ByVal estadoae As Boolean)
        Me._Id = id
        Me._ar_NombreCorto = nombre
        Me._IdTienda = idtienda
        Me._EstadoAE = estadoae

    End Sub
    Public Sub New(ByVal id As Integer, ByVal nombre As String, ByVal idtienda As Integer, ByVal parametroCentrocosto As String, ByVal estadoae As Boolean)
        Me._Id = id
        Me._ar_NombreCorto = nombre
        Me._IdTienda = idtienda
        Me._EstadoAE = estadoae
        Me.IdCentroCosto = parametroCentrocosto
    End Sub

    Public Property IdCentroCosto() As String
        Get
            Return _IdCentroCosto
        End Get
        Set(ByVal value As String)
            _IdCentroCosto = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property IdArea() As Integer
        Get
            Return Me._IdArea
        End Get
        Set(ByVal value As Integer)
            Me._IdArea = value
        End Set
    End Property
    Public Property DescripcionCorta() As String
        Get
            Return Me._ar_NombreCorto
        End Get
        Set(ByVal value As String)
            Me._ar_NombreCorto = value
        End Set
    End Property
    Public Property Id() As Integer
        Get
            Return Me._Id
        End Get
        Set(ByVal value As Integer)
            Me._Id = value
        End Set
    End Property
    Public Property EstadoAE() As Boolean
        Get
            Return Me._EstadoAE
        End Get
        Set(ByVal value As Boolean)
            Me._EstadoAE = value
        End Set
    End Property
End Class
