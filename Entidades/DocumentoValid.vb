﻿Public Class DocumentoValid
    Inherits Entidades.Documento

    Private _IdDocumento As Integer
    Private _doc_Codigo As String
    Private _doc_Serie As String
    Private _tdoc_NombreCorto As String
    Private _FechaEmision As String
    Private _strTipoDocumentoRef As String
    Private _Numero As String
  

    Public Property strTipoDocumentoRef() As String
        Get
            Return _strTipoDocumentoRef
        End Get
        Set(ByVal value As String)
            _strTipoDocumentoRef = value
        End Set
    End Property

    Public ReadOnly Property getTipoDocumentoRef() As List(Of Entidades.TipoDocumento)
        Get
            If strTipoDocumentoRef <> "" Then
                Dim lista As New List(Of Entidades.TipoDocumento)
                Dim TipoDocumentoRef() As String
                TipoDocumentoRef = strTipoDocumentoRef.Split(CChar(","))
                For x As Integer = 0 To TipoDocumentoRef.Length - 1
                    If TipoDocumentoRef(x) <> "" Then
                        lista.Add(New Entidades.TipoDocumento(0, TipoDocumentoRef(x)))
                    End If
                Next
                Return lista
            End If
            Return Nothing
        End Get
    End Property

    Public Property Numero() As String
        Get
            Return _Numero
        End Get
        Set(ByVal value As String)
            _Numero = value
        End Set
    End Property
    Public Property FechaEmision() As String
        Get
            Return Me._FechaEmision
        End Get
        Set(ByVal value As String)
            Me._FechaEmision = value
        End Set
    End Property


    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property

    Public Property Codigo() As String
        Get
            Return Me._doc_Codigo
        End Get
        Set(ByVal value As String)
            Me._doc_Codigo = value
        End Set
    End Property

    Public Property Serie() As String
        Get
            Return Me._doc_Serie
        End Get
        Set(ByVal value As String)
            Me._doc_Serie = value
        End Set
    End Property

  
    Public Property NomTipoDocumento() As String
        Get
            Return Me._tdoc_NombreCorto
        End Get
        Set(ByVal value As String)
            Me._tdoc_NombreCorto = value
        End Set
    End Property

End Class
