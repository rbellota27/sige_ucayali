﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class DocOrdenDespacho
    Private _doc_Codigo As String
    Private _IdDocumento As Integer
    Private _IdEmpresa As Integer
    Private _IdTienda As Integer
    Private _IdTipoDocumento As Integer
    Private _IdSerie As Integer
    Private _IdEstadoDoc As Integer
    Private _IdPersona As Integer
    Private _per_NComercial As String
    Private _jur_Rsocial As String
    Private _Nombre As String
    Private _Dni As String
    Private _Ruc As String
    Private _dir_Direccion As String


    Public ReadOnly Property getNombreMostrar() As String
        Get
            If Me._Nombre.Trim.Length > 0 Then
                Return Me._Nombre
            ElseIf Me._per_NComercial.Trim.Length > 0 Then
                Return Me._per_NComercial
            Else
                Return Me._jur_Rsocial
            End If
        End Get
    End Property


    Public Property Codigo() As String
        Get
            Return Me._doc_Codigo
        End Get
        Set(ByVal value As String)
            Me._doc_Codigo = value
        End Set
    End Property

    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property

    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property IdTipoDocumento() As Integer
        Get
            Return Me._IdTipoDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoDocumento = value
        End Set
    End Property

    Public Property IdSerie() As Integer
        Get
            Return Me._IdSerie
        End Get
        Set(ByVal value As Integer)
            Me._IdSerie = value
        End Set
    End Property

    Public Property IdEstadoDoc() As Integer
        Get
            Return Me._IdEstadoDoc
        End Get
        Set(ByVal value As Integer)
            Me._IdEstadoDoc = value
        End Set
    End Property

    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Public Property NomComercial() As String
        Get
            Return Me._per_NComercial
        End Get
        Set(ByVal value As String)
            Me._per_NComercial = value
        End Set
    End Property

    Public Property NomRSocial() As String
        Get
            Return Me._jur_Rsocial
        End Get
        Set(ByVal value As String)
            Me._jur_Rsocial = value
        End Set
    End Property

    Public Property NomCliente() As String
        Get
            Return Me._Nombre
        End Get
        Set(ByVal value As String)
            Me._Nombre = value
        End Set
    End Property

    Public Property Dni() As String
        Get
            Return Me._Dni
        End Get
        Set(ByVal value As String)
            Me._Dni = value
        End Set
    End Property

    Public Property Ruc() As String
        Get
            Return Me._Ruc
        End Get
        Set(ByVal value As String)
            Me._Ruc = value
        End Set
    End Property

    Public Property Direccion() As String
        Get
            Return Me._dir_Direccion
        End Get
        Set(ByVal value As String)
            Me._dir_Direccion = value
        End Set
    End Property
End Class
