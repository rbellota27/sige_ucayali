'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Material
    Private _IdMaterial As Integer
    Private _mat_Nombre As String
    Private _mat_Abv As String
    Private _mat_estado As String
    Private _descEstado As String
    Private _for_Codigo As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal idformato As Integer, ByVal abv As String, ByVal estado As Boolean)
        Me._IdMaterial = idformato
        Me._mat_Nombre = abv
        Me._mat_estado = estado
    End Sub
    Public ReadOnly Property DescEstado()
        Get
            Me._descEstado = "Activo"
            If Me._mat_estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public Property Id() As Integer
        Get
            Return Me._IdMaterial
        End Get
        Set(ByVal value As Integer)
            Me._IdMaterial = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._mat_Nombre
        End Get
        Set(ByVal value As String)
            Me._mat_Nombre = value
        End Set
    End Property

    Public Property Abv() As String
        Get
            Return Me._mat_Abv
        End Get
        Set(ByVal value As String)
            Me._mat_Abv = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._mat_estado
        End Get
        Set(ByVal value As String)
            Me._mat_estado = value
        End Set
    End Property
    Public Property For_Codigo() As String
        Get
            Return _for_Codigo
        End Get
        Set(ByVal value As String)
            _for_Codigo = value
        End Set
    End Property
End Class
