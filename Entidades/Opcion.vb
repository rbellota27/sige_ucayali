'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Opcion
    Private _IdOpcion As Integer
    Private _IdMenu As Integer
    Private _idUsuario As Integer
    Private _op_Nombre As String
    Private _op_Formulario As String
    Private _op_Estado As String
    Private _isPermitido As String
    Private _idperfil As Integer
    Private _Orden As Decimal
    Private _ParentName As String
    Private _dir_imagen As String = String.Empty
    Public Sub New(ByVal idopcion As Integer, ByVal texto As String)
        Me.Id = idopcion
        Me.Nombre = texto
    End Sub

    Public Sub New(ByVal idmenu As Integer, ByVal texto As String, ByVal Formulario As String, ByVal estado As String)
        Me.IdMenu = idmenu
        Me.Nombre = texto
        Me.Formulario = Formulario
        Me.Estado = estado
    End Sub
    Public Sub New()
    End Sub
    Public Sub New(ByVal ispermitido As Integer, ByVal id As Integer, ByVal nombre As String, ByVal formulario As String)
        Me.IsPermitido = ispermitido
        Me.Id = id
        Me.Nombre = nombre
        Me.Formulario = formulario
    End Sub

    Public Property dir_imagen() As String
        Get
            Return Me._dir_imagen
        End Get
        Set(ByVal value As String)
            Me._dir_imagen = value
        End Set
    End Property

    Public Property ParentName() As String
        Get
            Return Me._ParentName
        End Get
        Set(ByVal value As String)
            Me._ParentName = value
        End Set
    End Property



    Public Property Idperfil() As Integer
        Get
            Return _idperfil
        End Get
        Set(ByVal value As Integer)
            _idperfil = value
        End Set
    End Property


    Public Property Orden() As Decimal
        Get
            Return Me._Orden
        End Get
        Set(ByVal value As Decimal)
            Me._Orden = value
        End Set
    End Property


    Public Property IsPermitido() As String
        Get
            Return _isPermitido
        End Get
        Set(ByVal value As String)
            _isPermitido = value
        End Set
    End Property
    Public Property Id() As Integer
        Get
            Return Me._IdOpcion
        End Get
        Set(ByVal value As Integer)
            Me._IdOpcion = value
        End Set
    End Property

    Public Property IdMenu() As Integer
        Get
            Return Me._IdMenu
        End Get
        Set(ByVal value As Integer)
            Me._IdMenu = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._op_Nombre
        End Get
        Set(ByVal value As String)
            Me._op_Nombre = value
        End Set
    End Property

    Public Property Formulario() As String
        Get
            Return Me._op_Formulario
        End Get
        Set(ByVal value As String)
            Me._op_Formulario = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._op_Estado
        End Get
        Set(ByVal value As String)
            Me._op_Estado = value
        End Set
    End Property
    Public Property IdUsuario() As Integer
        Get
            Return _idUsuario
        End Get
        Set(ByVal value As Integer)
            _idUsuario = value
        End Set
    End Property
End Class
