﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'**********************   VIERNES 25 06 2010

Public Class MagnitudUnidad
    Private _idMagnitud As Integer
    Private _idUnidadMedida As Integer
    Private _mu_UnidadPrincipal As Boolean
    Private _mu_Equivalencia As Decimal
    Private _um_NombreCorto As String

    Public Property Equivalencia() As Decimal
        Get
            Return Me._mu_Equivalencia
        End Get
        Set(ByVal value As Decimal)
            Me._mu_Equivalencia = value
        End Set
    End Property

    Public Property UnidadPrincipal() As Boolean
        Get
            Return Me._mu_UnidadPrincipal
        End Get
        Set(ByVal value As Boolean)
            Me._mu_UnidadPrincipal = value
        End Set
    End Property

    Public Property idUnidadMedida() As Integer
        Get
            Return Me._idUnidadMedida
        End Get
        Set(ByVal value As Integer)
            Me._idUnidadMedida = value
        End Set
    End Property
    Public Property idMagnitud() As Integer
        Get
            Return Me._idMagnitud
        End Get
        Set(ByVal value As Integer)
            Me._idMagnitud = value

        End Set
    End Property
    Public Property NombreCortoUM() As String
        Get
            Return Me._um_NombreCorto
        End Get
        Set(ByVal value As String)
            Me._um_NombreCorto = value
        End Set
    End Property


End Class
