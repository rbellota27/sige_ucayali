'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Juridica
    Private _jur_Rsocial As String
    Private _IdPersona As Integer
    Private _jur_FechaAniversario As Date
    Private _ruc As String

    Private _IdAgente As Integer
    Public Property IdAgente() As Integer
        Get
            Return Me._IdAgente
        End Get
        Set(ByVal value As Integer)
            Me._IdAgente = value
        End Set
    End Property

    Public Property RUC() As String
        Get
            Return Me._ruc
        End Get
        Set(ByVal value As String)
            Me._ruc = value
        End Set
    End Property

    Public Property RazonSocial() As String
        Get
            Return Me._jur_Rsocial
        End Get
        Set(ByVal value As String)
            Me._jur_Rsocial = value
        End Set
    End Property

    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Public Property FechaAniversario() As Date
        Get
            Return Me._jur_FechaAniversario
        End Get
        Set(ByVal value As Date)
            Me._jur_FechaAniversario = value
        End Set
    End Property
End Class
