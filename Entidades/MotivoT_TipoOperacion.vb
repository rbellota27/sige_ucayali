﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class MotivoT_TipoOperacion
    Private _IdMotivoT As Integer
    Private _IdTipoOperacion As Integer
    Private _NombreTipoOperacion As String
    Public Property IdMotivoT() As Integer
        Get
            Return _IdMotivoT
        End Get
        Set(ByVal value As Integer)
            _IdMotivoT = value
        End Set
    End Property
    Public Property idTipoOperac() As Integer
        Get
            Return _IdTipoOperacion
        End Get
        Set(ByVal value As Integer)
            _IdTipoOperacion = value
        End Set
    End Property
    Public Property NombreTipoOperacion() As String
        Get
            Return _NombreTipoOperacion
        End Get
        Set(ByVal value As String)
            _NombreTipoOperacion = value
        End Set
    End Property
End Class
