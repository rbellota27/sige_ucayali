﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




'**************************   MARTES 11 MAYO 2010 HORA 10_45 AM








Public Class DetalleConcepto_Anexo
    Private _IdDocumento As Integer
    Private _IdDetalleConcepto As Integer
    Private _IdDetalleConcepto_Anexo As Integer
    Private _IdDocumentoRef As Integer

    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property

    Public Property IdDetalleConcepto() As Integer
        Get
            Return Me._IdDetalleConcepto
        End Get
        Set(ByVal value As Integer)
            Me._IdDetalleConcepto = value
        End Set
    End Property

    Public Property IdDetalleConcepto_Anexo() As Integer
        Get
            Return Me._IdDetalleConcepto_Anexo
        End Get
        Set(ByVal value As Integer)
            Me._IdDetalleConcepto_Anexo = value
        End Set
    End Property

    Public Property IdDocumentoRef() As Integer
        Get
            Return Me._IdDocumentoRef
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumentoRef = value
        End Set
    End Property
End Class
