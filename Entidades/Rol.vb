'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

' modificado 29-01-2010 se agrego los new's
Public Class Rol
    Private _IdRol As Integer
    Private _IdEmpresa As Integer
    Private _rol_Nombre As String
    Private _rol_Estado As String
    Private _descEstado As String
    Private _IdPersona As Integer

    Public Sub New()

    End Sub

    Public Sub New(ByVal IdRol As Integer, ByVal rol_Nombre As String, ByVal rol_Estado As String)
        Me._IdRol = IdRol
        Me._rol_Nombre = rol_Nombre
        Me._rol_Estado = rol_Estado
    End Sub
    Public ReadOnly Property DescEstado() As String
        Get
            Me._descEstado = "Activo"
            If Me._rol_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property

    Public Property Id() As Integer
        Get
            Return Me._IdRol
        End Get
        Set(ByVal value As Integer)
            Me._IdRol = value
        End Set
    End Property

    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property


    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property


    Public Property Nombre() As String
        Get
            Return Me._rol_Nombre
        End Get
        Set(ByVal value As String)
            Me._rol_Nombre = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._rol_Estado
        End Get
        Set(ByVal value As String)
            Me._rol_Estado = value
        End Set
    End Property
End Class
