'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'''''''    26 julio 2010 Hora 12:08 am

'**************** LUNES 15 MAR 2010 HORA 12_32 AM
'**************** LUNES 01 MAR 2010 HORA 11_33 AM
'**************** LUNES 22 FEB 2010 HORA 06_28 PM
'**************** VIERNES 19 FEB 2010 HORA 02_58 PM



Public Class CuentaPersona
    Private _IdCuentaPersona As Integer
    Private _cp_FechaAlta As String
    Private _cp_CargoMaximo As Decimal
    Private _venta_mes As Decimal
    Private _venta_a�o As Decimal
    Private _IdPersona As Integer
    Private _IdMoneda As Integer
    Private _cp_Estado As String

    ''******
    Private _Mon_Simbolo As String
    Private _Disponible As Decimal
    Private _Saldo As Decimal
    ''************
    Private _tdoc_NombreCorto As String
    Private _Numero As String
    Private _doc_FechaEmision As String
    Private _dc_UMedida As String

    Private _prod_Nombre As String
    Private _dc_Cantidad As Decimal
    Private _dc_PrecioCD As Decimal
    Private _total As Decimal
    Private _totalapagar As Decimal
    ''**************
    Private _IdEmpresa As Integer
    Private _IdTienda As Integer
    Private _cp_CuentaFormal As Boolean
    Private _IdSupervisor As Integer
    Private _cp_ObServ As String
    Private _tiendaNom As String
    Private _cadTienda As String
    Private _cadMoneda As String
    Private _cadCargoMaximo As String
    Private _cadDisponible As String
    Private _cadSaldo As String
    Private _ruc As String
    Private _dni As String
    Private _nombre As String

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Property Dni() As String
        Get
            Return _dni
        End Get
        Set(ByVal value As String)
            _dni = value
        End Set
    End Property

    Public Property Ruc() As String
        Get
            Return _ruc
        End Get
        Set(ByVal value As String)
            _ruc = value
        End Set
    End Property

    Public Property cadTienda() As String
        Get
            Return _cadTienda
        End Get
        Set(ByVal value As String)
            _cadTienda = value
        End Set
    End Property
    Public ReadOnly Property getTienda() As ArrayList
        Get
            If cadTienda.Length > 0 Then
                Dim cad1() As String = cadTienda.Split(",")
                Dim ArrayTienda As New ArrayList
                ArrayTienda.AddRange(cad1)
                Return ArrayTienda
            End If
            Return Nothing
        End Get
    End Property

    Public Property cadMoneda() As String
        Get
            Return _cadMoneda
        End Get
        Set(ByVal value As String)
            _cadMoneda = value
        End Set
    End Property
    Public ReadOnly Property getMoneda() As ArrayList
        Get
            If cadMoneda.Length > 0 Then
                Dim cad2() As String = cadMoneda.Split(",")
                Dim ArrayMoneda As New ArrayList
                ArrayMoneda.AddRange(cad2)
                Return ArrayMoneda
            End If
            Return Nothing
        End Get
    End Property

    Public Property cadCargoMaximo() As String
        Get
            Return _cadCargoMaximo
        End Get
        Set(ByVal value As String)
            _cadCargoMaximo = value
        End Set
    End Property
    Public ReadOnly Property getCargoMaximo() As ArrayList
        Get
            If cadCargoMaximo.Length > 0 Then
                Dim cad3() As String = cadCargoMaximo.Split(",")
                Dim ArrayCargoMaximo As New ArrayList
                ArrayCargoMaximo.AddRange(cad3)
                Return ArrayCargoMaximo
            End If
            Return Nothing
        End Get
    End Property



    Public Property cadDisponible() As String
        Get
            Return _cadDisponible
        End Get
        Set(ByVal value As String)
            _cadDisponible = value
        End Set
    End Property
    Public ReadOnly Property getDisponible() As ArrayList
        Get
            If cadDisponible.Length > 0 Then
                Dim cad4() As String = cadDisponible.Split(",")
                Dim ArrayDisponible As New ArrayList
                ArrayDisponible.AddRange(cad4)
                Return ArrayDisponible
            End If
            Return Nothing
        End Get
    End Property


    Public Property cadSaldo() As String
        Get
            Return _cadSaldo
        End Get
        Set(ByVal value As String)
            _cadSaldo = value
        End Set
    End Property
    Public ReadOnly Property getSaldo() As ArrayList
        Get
            If cadSaldo.Length > 0 Then
                Dim cad5() As String = cadSaldo.Split(",")
                Dim ArraySaldo As New ArrayList
                ArraySaldo.AddRange(cad5)
                Return ArraySaldo
            End If
            Return Nothing
        End Get
    End Property




    Public Sub New()
    End Sub
    Public Sub New(ByVal IdPersona As Integer, ByVal IdMoneda As Integer, ByVal Mon_Simbolo As String, _
                   ByVal CargoMaximo As Decimal, ByVal Saldo As Decimal, ByVal Estado As String)
        Me.IdPersona = IdPersona
        Me.CargoMaximo = CargoMaximo
        Me.Saldo = Saldo
        Me.IdMoneda = IdMoneda
        Me.MonedaSimbolo = Mon_Simbolo
        Me.Estado = Estado
    End Sub

    Public Sub New(ByVal IdPersona As Integer, ByVal IdMoneda As Integer, ByVal CargoMaximo As Decimal, _
                   ByVal Saldo As Decimal, ByVal Estado As String, Optional ByVal CuentaFormal As Boolean = False)
        Me.IdPersona = IdPersona
        Me.CargoMaximo = CargoMaximo
        Me.Saldo = Saldo
        Me.IdMoneda = IdMoneda
        Me.Estado = Estado
        Me.CuentaFormal = CuentaFormal
    End Sub
    Public Sub New(ByVal IdPersona As Integer, ByVal IdMoneda As Integer, ByVal Mon_Simbolo As String, ByVal CargoMaximo As Decimal, _
               ByVal Estado As String, ByVal Saldo As Decimal, ByVal obs As String, ByVal FechaAlta As String, Optional ByVal CuentaFormal As Boolean = False)
        Me.IdPersona = IdPersona
        Me.CargoMaximo = CargoMaximo
        Me.IdMoneda = IdMoneda
        Me.MonedaSimbolo = Mon_Simbolo
        Me.Estado = Estado
        Me.Saldo = Saldo
        Me.CuentaFormal = CuentaFormal
        cp_ObServ = obs
    End Sub
    Public Property cp_ObServ() As String
        Get
            Return _cp_ObServ
        End Get
        Set(ByVal value As String)
            _cp_ObServ = value
        End Set
    End Property

    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property IdSupervisor() As Integer
        Get
            Return Me._IdSupervisor
        End Get
        Set(ByVal value As Integer)
            Me._IdSupervisor = value
        End Set
    End Property


    Public Property Estado() As String
        Get
            Return Me._cp_Estado
        End Get
        Set(ByVal value As String)
            Me._cp_Estado = value
        End Set
    End Property

    Public Property IdCuentaPersona() As Integer
        Get
            Return Me._IdCuentaPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdCuentaPersona = value
        End Set
    End Property

    Public Property FechaAlta() As String
        Get
            Return Me._cp_FechaAlta
        End Get
        Set(ByVal value As String)
            Me._cp_FechaAlta = value
        End Set
    End Property
    ' luis
    Public Property dc_Cantidad() As Decimal
        Get
            Return Me._dc_Cantidad
        End Get
        Set(ByVal value As Decimal)
            Me._dc_Cantidad = value
        End Set
    End Property
    Public Property total() As Decimal
        Get
            Return Me._total
        End Get
        Set(ByVal value As Decimal)
            Me._total = value
        End Set
    End Property
    Public Property dc_PrecioCD() As Decimal
        Get
            Return Me._dc_PrecioCD
        End Get
        Set(ByVal value As Decimal)
            Me._dc_PrecioCD = value
        End Set
    End Property
    Public Property CargoMaximo() As Decimal
        Get
            Return Me._cp_CargoMaximo
        End Get
        Set(ByVal value As Decimal)
            Me._cp_CargoMaximo = value
        End Set
    End Property

    Public ReadOnly Property DescCargoMaximo() As String
        Get
            Return Me._cp_CargoMaximo.ToString("F")
        End Get
    End Property
    Public Property Saldo() As Decimal
        Get
            Return Me._Saldo
        End Get
        Set(ByVal value As Decimal)
            Me._Saldo = value
        End Set
    End Property
    Public Property totalapagar() As Decimal
        Get
            Return Me._totalapagar
        End Get
        Set(ByVal value As Decimal)
            Me._totalapagar = value
        End Set
    End Property

    Public Property Venta_mes() As Decimal
        Get
            Return Me._venta_mes

        End Get
        Set(ByVal value As Decimal)
            Me._venta_mes = value
        End Set
    End Property
    Public Property Venta_a�o() As Decimal
        Get
            Return Me._venta_a�o
        End Get
        Set(ByVal value As Decimal)
            Me._venta_a�o = value
        End Set
    End Property

    Public ReadOnly Property DescSaldo() As String
        Get
            Return Me._Saldo.ToString("F")
        End Get
    End Property

    Public ReadOnly Property Disponible() As Decimal
        Get
            'Revisar esto; el saldo es la diferencia entre el debe y el haber, o sea debe ser el disponible
            Return _cp_CargoMaximo - _Saldo

        End Get
    End Property
    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property
    Public Property prod_Nombre() As String
        Get
            Return Me._prod_Nombre
        End Get
        Set(ByVal value As String)
            Me._prod_Nombre = value
        End Set
    End Property
    Public Property Numero() As String
        Get
            Return Me._Numero
        End Get
        Set(ByVal value As String)
            Me._Numero = value
        End Set
    End Property
    Public Property dc_UMedida() As String
        Get
            Return Me._dc_UMedida
        End Get
        Set(ByVal value As String)
            Me._dc_UMedida = value
        End Set
    End Property

    Public Property doc_FechaEmision() As String
        Get
            Return Me._doc_FechaEmision
        End Get
        Set(ByVal value As String)
            Me._doc_FechaEmision = value
        End Set
    End Property
    Public Property tdoc_NombreCorto() As String
        Get
            Return Me._tdoc_NombreCorto
        End Get
        Set(ByVal value As String)
            Me._tdoc_NombreCorto = value
        End Set
    End Property
    Public Property MonedaSimbolo() As String
        Get
            Return Me._Mon_Simbolo
        End Get
        Set(ByVal value As String)
            Me._Mon_Simbolo = value
        End Set
    End Property

    Public Property CuentaFormal() As Boolean
        Get
            Return Me._cp_CuentaFormal
        End Get
        Set(ByVal value As Boolean)
            Me._cp_CuentaFormal = value
        End Set
    End Property

    Public ReadOnly Property DesCtaFormal() As String
        Get
            If _cp_CuentaFormal = True Then
                Return "Formal"
            Else
                Return "Informal"
            End If
        End Get
    End Property

    Public ReadOnly Property DesEstado() As String
        Get
            If _cp_Estado = True Then
                Return "Activo"
            Else
                Return "Inactivo"
            End If
        End Get
    End Property

    Public Property TiendaNom() As String
        Get
            Return _tiendaNom
        End Get
        Set(ByVal value As String)
            _tiendaNom = value
        End Set
    End Property

End Class
