'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'LUNES 15 DE FEBRERO 2010

Public Class MedioPago
    Private _IdMedioPago As Integer
    Private _mp_CodigoSunat As String
    Private _mp_Nombre As String
    Private _mp_Estado As String
    Private _descEstado As String
    Private _mp_Principal As Boolean   '********************4
    Private _mpi_Descripcion As String
    Private _mp_CuentaContable As String
    Private _IdMedioPagoInterfaz As Integer
    Private _IdConceptoMovBanco As Integer
    Private _IdTipoConceptoBanco As Integer
    Private _abrev As String
    Private _mp_autoMovbanco As Boolean

    Public Sub New()
    End Sub

    Public Property mp_autoMovbanco() As Boolean
        Get
            Return Me._mp_autoMovbanco
        End Get
        Set(ByVal value As Boolean)
            Me._mp_autoMovbanco = value
        End Set
    End Property

    Public Property Abrev() As String
        Get
            Return _abrev
        End Get
        Set(ByVal value As String)
            _abrev = value
        End Set
    End Property

    Public Property IdTipoConceptoBanco() As Integer
        Get
            Return _IdTipoConceptoBanco
        End Get
        Set(ByVal value As Integer)
            _IdTipoConceptoBanco = value
        End Set
    End Property

    Public Property IdConceptoMovBanco() As Integer
        Get
            Return _IdConceptoMovBanco
        End Get
        Set(ByVal value As Integer)
            _IdConceptoMovBanco = value
        End Set
    End Property

    Public Property IdMedioPagoInterfaz() As Integer
        Get
            Return Me._IdMedioPagoInterfaz
        End Get
        Set(ByVal value As Integer)
            Me._IdMedioPagoInterfaz = value
        End Set
    End Property

    Public Sub New(ByVal Idmediopago As Integer, ByVal nombre As String)
        Me.Id = Idmediopago
        Me.Nombre = nombre
    End Sub

    Public Property Principal() As Boolean
        Get
            Return Me._mp_Principal
        End Get
        Set(ByVal value As Boolean)
            Me._mp_Principal = value
        End Set
    End Property

    Public Property mpi_Descripcion() As String
        Get
            Return _mpi_Descripcion
        End Get
        Set(ByVal value As String)
            _mpi_Descripcion = value
        End Set
    End Property

    Public ReadOnly Property DescEstado()
        Get
            Me._descEstado = "Activo"
            If Me._mp_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public Property Id() As Integer
        Get
            Return Me._IdMedioPago
        End Get
        Set(ByVal value As Integer)
            Me._IdMedioPago = value
        End Set
    End Property

    Public Property CodigoSunat() As String
        Get
            Return Me._mp_CodigoSunat
        End Get
        Set(ByVal value As String)
            Me._mp_CodigoSunat = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._mp_Nombre
        End Get
        Set(ByVal value As String)
            Me._mp_Nombre = value
        End Set
    End Property
    Public Property Estado() As String
        Get
            Return Me._mp_Estado
        End Get
        Set(ByVal value As String)
            Me._mp_Estado = value
        End Set
    End Property
    Public Property mp_CuentaContable() As String
        Get
            Return _mp_CuentaContable
        End Get
        Set(ByVal value As String)
            _mp_CuentaContable = value
        End Set
    End Property

End Class
