'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class TipoPrecioV
    Private _IdTipoPv As Integer
    Private _pv_Nombre As String
    Private _pv_Abv As String
    Private _pv_Estado As String
    Private _descEstado As String

    Private _pv_Default As Boolean
    Private _IdTipoPVRef As Integer
    Private _pv_NombreRef As String
    Private _pv_AfectoCargoTarjeta As Boolean

    Public Sub New()
    End Sub
    Public Sub New(ByVal IdTipoPV As Integer, ByVal descripcion As String)

        Me.IdTipoPv = IdTipoPV
        Me.Nombre = descripcion

    End Sub

    Public ReadOnly Property DescEstado() As String
        Get
            Me._descEstado = "Activo"
            If Me._pv_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public Property IdTipoPv() As Integer
        Get
            Return Me._IdTipoPv
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoPv = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._pv_Nombre
        End Get
        Set(ByVal value As String)
            Me._pv_Nombre = value
        End Set
    End Property

    Public Property Abv() As String
        Get
            Return Me._pv_Abv
        End Get
        Set(ByVal value As String)
            Me._pv_Abv = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._pv_Estado
        End Get
        Set(ByVal value As String)
            Me._pv_Estado = value
        End Set
    End Property
    Public Property EstadoBoolean() As Boolean
        Get
            Return IIf(Me._pv_Estado = "1", True, False)
        End Get
        Set(ByVal value As Boolean)
            Me._pv_Estado = IIf(value, "1", "0")
        End Set
    End Property
    Public Property TipoPvDefault() As Boolean
        Get
            Return Me._pv_Default
        End Get
        Set(ByVal value As Boolean)
            Me._pv_Default = value
        End Set
    End Property

    Public Property IdTipoPvPredecesor() As Integer
        Get
            Return Me._IdTipoPVRef
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoPVRef = value
        End Set
    End Property

    Public Property NombrePredecesor() As String
        Get
            Return Me._pv_NombreRef
        End Get
        Set(ByVal value As String)
            Me._pv_NombreRef = value
        End Set
    End Property

    Public Property AfectoCargoTarjeta() As Boolean
        Get
            Return Me._pv_AfectoCargoTarjeta
        End Get
        Set(ByVal value As Boolean)
            Me._pv_AfectoCargoTarjeta = value
        End Set
    End Property

End Class
