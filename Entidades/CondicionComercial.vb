'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class CondicionComercial
    Private _IdCondicionCC As Integer
    Private _cct_Nombre As String
    Private _cct_Estado As String
    Private _cct_tipousodias As String
    Private _IdTipoDocumento As Integer
    Private _NomTipoDocumento As String
    Private _select As Boolean = False
    Private _DescEstado As String
    Private _DescTipoUso As String
    Private _dcc_fecha As String
    Private _IdDocumento As Integer
    Private _NroDias As Integer
    Private _flagcc As Integer
    Private _dcc_fechabl As String

    Public Sub New()
    End Sub
    Public Property dcc_fechabl() As String
        Get
            Return _dcc_fechabl
        End Get
        Set(ByVal value As String)
            _dcc_fechabl = value
        End Set
    End Property
    Public Sub New(ByVal Id As Integer, ByVal Descripcion As String)
        Me.Id = Id
        Me.Descripcion = Descripcion
    End Sub
    Public Property FlagCC() As Integer
        Get
            Return _flagcc
        End Get
        Set(ByVal value As Integer)
            _flagcc = value
        End Set
    End Property
    Public Property NroDiasCondi() As Integer
        Get
            Return _NroDias
        End Get
        Set(ByVal value As Integer)
            _NroDias = value
        End Set
    End Property
    Public Property IdDocumento() As Integer
        Get
            Return _IdDocumento
        End Get
        Set(ByVal value As Integer)
            _IdDocumento = value
        End Set
    End Property

    Public Property dcc_fecha() As String
        Get
            Return _dcc_fecha
        End Get
        Set(ByVal value As String)
            _dcc_fecha = value
        End Set
    End Property

    Public Property Seleccionado() As Boolean
        Get
            Return Me._select
        End Get
        Set(ByVal value As Boolean)
            Me._select = value
        End Set
    End Property

    Public Property Id() As Integer
        Get
            Return Me._IdCondicionCC
        End Get
        Set(ByVal value As Integer)
            Me._IdCondicionCC = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._cct_Nombre
        End Get
        Set(ByVal value As String)
            Me._cct_Nombre = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._cct_Estado
        End Get
        Set(ByVal value As String)
            Me._cct_Estado = value
        End Set
    End Property

    Public Property IdTipoDocumento() As Integer
        Get
            Return Me._IdTipoDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoDocumento = value
        End Set
    End Property
    Public Property NomTipoDocumento() As String
        Get
            Return _NomTipoDocumento
        End Get
        Set(ByVal value As String)
            _NomTipoDocumento = value
        End Set
    End Property

    Public Property cct_tipousodias() As String
        Get
            Return _cct_tipousodias
        End Get
        Set(ByVal value As String)
            _cct_tipousodias = value
        End Set
    End Property
    Public ReadOnly Property DescEstado()
        Get
            _DescEstado = "Activo"
            If _cct_Estado = "0" Then
                _DescEstado = "Inactivo"
            End If
            Return _DescEstado
        End Get
    End Property
    Public ReadOnly Property DescTipoUsoNroDias() As String
        Get
            If cct_tipousodias = "R" Then _DescTipoUso = "Cr�dito"
            If cct_tipousodias = "FV" Then _DescTipoUso = "Fecha de Vencimiento"
            If cct_tipousodias = "NULL" Then _DescTipoUso = "Ninguno"
            Return _DescTipoUso
        End Get
    End Property
End Class
