﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'LUNES 01 DE FEBRERO
Public Class TipoDocumento_MedioPago
    Private _IdTipoDocumento As Integer
    Private _IdMedioPago As Integer
    Private _Estado As Boolean
    Private _MedioPago As String
    Private _TipoDocumento As String
    Private _ObjTipoDocumento As Object

    Public Property ObjTipoDocumento() As Object
        Get
            Return _ObjTipoDocumento
        End Get
        Set(ByVal value As Object)
            _ObjTipoDocumento = value
        End Set
    End Property

    Public Property IdTipoDocumento() As Integer
        Get
            Return Me._IdTipoDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoDocumento = value
        End Set
    End Property

    Public Property IdMedioPago() As Integer
        Get
            Return Me._IdMedioPago
        End Get
        Set(ByVal value As Integer)
            Me._IdMedioPago = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return Me._Estado
        End Get
        Set(ByVal value As Boolean)
            Me._Estado = value
        End Set
    End Property

    Public Property MedioPago() As String
        Get
            Return Me._MedioPago
        End Get
        Set(ByVal value As String)
            Me._MedioPago = value
        End Set
    End Property

    Public Property TipoDocumento() As String
        Get
            Return Me._TipoDocumento
        End Get
        Set(ByVal value As String)
            Me._TipoDocumento = value
        End Set
    End Property

End Class
