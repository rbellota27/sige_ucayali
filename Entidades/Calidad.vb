﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Calidad
    Private _IdCalidad As Integer
    Private _CalNombre As String
    Private _CalAbv As String
    Private _CalEstado As Boolean
    Private _descEstado As String
    Private _cal_codigo As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal idcalidad As Integer, ByVal nombre As String, ByVal estado As Boolean)
        Me._IdCalidad = idcalidad
        Me._CalNombre = nombre
        Me._CalEstado = estado
    End Sub
    Public ReadOnly Property DescEstado()
        Get
            Me._descEstado = "Activo"
            If Me._CalEstado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public Property IdCalidad() As Integer
        Get
            Return Me._IdCalidad
        End Get
        Set(ByVal value As Integer)
            Me._IdCalidad = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return Me._CalNombre
        End Get
        Set(ByVal value As String)
            Me._CalNombre = value
        End Set
    End Property
    Public Property Abv() As String
        Get
            Return Me._CalAbv
        End Get
        Set(ByVal value As String)
            Me._CalAbv = value
        End Set
    End Property
    Public Property Estado() As Boolean
        Get
            Return Me._CalEstado
        End Get
        Set(ByVal value As Boolean)
            Me._CalEstado = value
        End Set
    End Property

    Public ReadOnly Property strEstado() As String
        Get
            If Estado = True Then Return "1"
            If Estado = False Then Return "0"
        End Get
    End Property
    Public Property Cal_Codigo() As String
        Get
            Return _cal_codigo
        End Get
        Set(ByVal value As String)
            _cal_codigo = value
        End Set
    End Property
End Class
