﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

''''''''''''''''''''''MIRAYA ANAMARIA, EDGAR 03/02/2010 11:57 am
Public Class ConceptoMovBanco
    'No cambiar esta clase, para agregar cosas utilice la clase ConceptoMovBancoView
    Protected _IdConceptoMovBanco As Integer
    Protected _cban_Descripcion As String
    Protected _cban_DescripcionBreve As String
    Protected _cban_EstadoAutoAprobacion As Nullable(Of Boolean)
    Protected _cban_Estado As Nullable(Of Boolean)
    Protected _IdTipoConceptoBanco As Integer

    Public Sub New()
        Me._IdConceptoMovBanco = -1
        Me._cban_Descripcion = ""
        Me._cban_DescripcionBreve = ""
        Me._cban_EstadoAutoAprobacion = Nothing
        Me._cban_Estado = Nothing
        Me._IdTipoConceptoBanco = -1

    End Sub

    Public Sub New(ByVal IdConceptoMovBanco As Integer, ByVal cban_Descripcion As String, _
                    ByVal cban_DescripcionBreve As String, _
                    ByVal cban_EstadoAutoAprobacion As Boolean, ByVal cban_Estado As Boolean, _
                    ByVal IdTipoConceptoBanco As Integer)

        Me._IdConceptoMovBanco = IdConceptoMovBanco
        Me._cban_Descripcion = cban_Descripcion
        Me._cban_DescripcionBreve = cban_DescripcionBreve
        Me._cban_EstadoAutoAprobacion = cban_EstadoAutoAprobacion
        Me._cban_Estado = cban_Estado
        Me._IdTipoConceptoBanco = IdTipoConceptoBanco
    End Sub

    Public Property Id() As Integer
        Get
            Return Me._IdConceptoMovBanco
        End Get
        Set(ByVal value As Integer)
            Me._IdConceptoMovBanco = value
        End Set
    End Property
    Public Property Nombre() As String  'Equivale a la Descripcion 
        Get
            Return Me._cban_Descripcion
        End Get
        Set(ByVal value As String)
            Me._cban_Descripcion = value
        End Set
    End Property
    Public Property Estado() As Boolean?
        Get
            Return Me._cban_Estado
        End Get
        Set(ByVal value As Boolean?)
            Me._cban_Estado = value
        End Set
    End Property
    Public ReadOnly Property DescEstado() As String
        Get
            If Me._cban_Estado Then
                Return "Activo"
            Else
                Return "Inactivo"
            End If
        End Get
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._cban_Descripcion
        End Get
        Set(ByVal value As String)
            Me._cban_Descripcion = value
        End Set
    End Property
    Public Property DescripcionBreve() As String
        Get
            Return Me._cban_DescripcionBreve
        End Get
        Set(ByVal value As String)
            Me._cban_DescripcionBreve = value
        End Set
    End Property
    Public Property IdTipoConceptoBanco() As Integer
        Get
            Return Me._IdTipoConceptoBanco
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoConceptoBanco = value
        End Set
    End Property

    Public Property EstadoAutoAprobacion() As Boolean?
        Get
            Return Me._cban_EstadoAutoAprobacion
        End Get
        Set(ByVal value As Boolean?)
            Me._cban_EstadoAutoAprobacion = value
        End Set
    End Property
    Public ReadOnly Property DescEstadoAutoAprobacion() As String
        Get
            If Me._cban_EstadoAutoAprobacion Then
                Return "Activo"
            Else
                Return "Inactivo"
            End If
        End Get
    End Property
End Class
