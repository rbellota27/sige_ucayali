'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Almacen
    Private _IdAlmacen As Integer
    Private _alm_Nombre As String
    Private _alm_Ubigeo As String
    Private _alm_Direccion As String
    Private _alm_Referencia As String
    Private _alm_Area As Integer
    Private _alm_Estado As String
    Private _descEstado As String
    Private _seleccionado As Boolean
    Private _TalPrincipal As Boolean

    Private _IdAlmacenRef As Integer
    Private _IdtipoAlmacen As Integer

    Private _Tipoalm_Nombre As String
    Private _alm_NombreRef As String

    Private _Tipoalm_Principal As Integer

    Private _Dpto, _Prov, _Dist, _NomArea As String
    Private _CtroDistribuicion As String
    Public Property CtroDistribuicion() As String
        Get
            Return _CtroDistribuicion
        End Get
        Set(ByVal value As String)
            _CtroDistribuicion = value
        End Set
    End Property

    Public Property Tipoalm_Principal() As Integer
        Get
            Return _Tipoalm_Principal
        End Get
        Set(ByVal value As Integer)
            _Tipoalm_Principal = value
        End Set
    End Property


    Public Property IdAlmacenRef() As Integer
        Get
            Return _IdAlmacenRef
        End Get
        Set(ByVal value As Integer)
            _IdAlmacenRef = value
        End Set
    End Property
    Public Property IdtipoAlmacen() As Integer
        Get
            Return _IdtipoAlmacen
        End Get
        Set(ByVal value As Integer)
            _IdtipoAlmacen = value
        End Set
    End Property
    Public Property Tipoalm_Nombre() As String
        Get
            Return _Tipoalm_Nombre
        End Get
        Set(ByVal value As String)
            _Tipoalm_Nombre = value
        End Set
    End Property
    Public Property Alm_NombreRef() As String
        Get
            Return _alm_NombreRef
        End Get
        Set(ByVal value As String)
            _alm_NombreRef = value
        End Set
    End Property

    Public Sub New()
    End Sub

    Public Sub New(ByVal idalmacen As Integer, ByVal nombre As String)
        Me.IdAlmacen = idalmacen
        Me.Nombre = nombre
    End Sub

    Public Property Seleccionado() As Boolean
        Get
            Return _seleccionado
        End Get
        Set(ByVal value As Boolean)
            _seleccionado = value
        End Set
    End Property

    Public ReadOnly Property DescEstado() As String
        Get
            Me._descEstado = "Activo"
            If Me._alm_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public Property IdAlmacen() As Integer
        Get
            Return Me._IdAlmacen
        End Get
        Set(ByVal value As Integer)
            Me._IdAlmacen = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return Me._alm_Nombre
        End Get
        Set(ByVal value As String)
            Me._alm_Nombre = value
        End Set
    End Property
    Public Property Ubigeo() As String
        Get
            Return Me._alm_Ubigeo
        End Get
        Set(ByVal value As String)
            Me._alm_Ubigeo = value
        End Set
    End Property

    Public Property Direccion() As String
        Get
            Return Me._alm_Direccion
        End Get
        Set(ByVal value As String)
            Me._alm_Direccion = value
        End Set
    End Property

    Public Property Referencia() As String
        Get
            Return Me._alm_Referencia
        End Get
        Set(ByVal value As String)
            Me._alm_Referencia = value
        End Set
    End Property

    Public Property Area() As Integer
        Get
            Return Me._alm_Area
        End Get
        Set(ByVal value As Integer)
            Me._alm_Area = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._alm_Estado
        End Get
        Set(ByVal value As String)
            Me._alm_Estado = value
        End Set
    End Property
    Public Property Dpto() As String
        Get
            Return _Dpto
        End Get
        Set(ByVal value As String)
            _Dpto = value
        End Set
    End Property
    Public Property Prov() As String
        Get
            Return _Prov
        End Get
        Set(ByVal value As String)
            _Prov = value
        End Set
    End Property
    Public Property Dist() As String
        Get
            Return _Dist
        End Get
        Set(ByVal value As String)
            _Dist = value
        End Set
    End Property
    Public Property NomArea() As String
        Get
            Return _NomArea
        End Get
        Set(ByVal value As String)
            _NomArea = value
        End Set
    End Property
    Public Property EstadoAI() As Boolean
        Get
            Return _seleccionado
        End Get
        Set(ByVal value As Boolean)
            _seleccionado = value
        End Set
    End Property
    Public Property TalPrincipal() As Boolean
        Get
            Return Me._TalPrincipal
        End Get
        Set(ByVal value As Boolean)
            Me._TalPrincipal = value
        End Set
    End Property
End Class
