﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class CalidadLinea
    Inherits Calidad
    Private _IdCalidad As Integer
    Private _NomCalidad As String
    Private _IdLinea As Integer
    Private _IdTipoExistencia As Integer
    Private _clEstado As Boolean
    Private _clOrden As Integer
    Private _IdCalidadOrden As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal idcalidad As Integer, ByVal nomcalidad As String, ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal estado As Boolean, ByVal orden As Integer)
        _IdCalidad = idcalidad
        _NomCalidad = nomcalidad
        _IdLinea = idlinea
        _IdTipoExistencia = idtipoexistencia
        _clEstado = estado
        _clOrden = orden
    End Sub
    Public Sub New(ByVal idcalidad As Integer, ByVal nomcalidad As String, ByVal estado As Boolean)
        _IdCalidad = idcalidad
        _NomCalidad = nomcalidad
        _clEstado = estado
    End Sub
    Public Property IdCalidad() As Integer
        Get
            Return _IdCalidad
        End Get
        Set(ByVal value As Integer)
            _IdCalidad = value
        End Set
    End Property
    Public Property NomCalidad() As String
        Get
            Return _NomCalidad
        End Get
        Set(ByVal value As String)
            _NomCalidad = value
        End Set
    End Property
    Public Property IdLinea() As Integer
        Get
            Return _IdLinea
        End Get
        Set(ByVal value As Integer)
            _IdLinea = value
        End Set
    End Property
    Public Property IdTipoExistencia() As Integer
        Get
            Return _IdTipoExistencia
        End Get
        Set(ByVal value As Integer)
            _IdTipoExistencia = value
        End Set
    End Property
    Public Property Estado() As Boolean
        Get
            Return _clEstado
        End Get
        Set(ByVal value As Boolean)
            _clEstado = value
        End Set
    End Property
    Public Property Orden() As Integer
        Get
            Return _clOrden
        End Get
        Set(ByVal value As Integer)
            _clOrden = value
        End Set
    End Property
    Public Property IdCalidadOrden() As String
        Get
            Return _IdCalidadOrden
        End Get
        Set(ByVal value As String)
            _IdCalidadOrden = value
        End Set
    End Property
End Class
