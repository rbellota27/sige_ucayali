﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class AliasMant
    Private _IdAlias As Integer
    Private _al_Descripcion As String
    Private _al_Estado As Boolean
    Private _IdPersona As Integer
    Private _Persona As String


    Public Sub New()
    End Sub


    Public Property Persona() As String
        Get
            Return _Persona
        End Get
        Set(ByVal value As String)
            _Persona = value
        End Set
    End Property

    Public Property IdPersona() As Integer
        Get
            Return _IdPersona
        End Get
        Set(ByVal value As Integer)
            _IdPersona = value
        End Set
    End Property

    Public Property IdAlias() As Integer
        Get
            Return _IdAlias
        End Get
        Set(ByVal value As Integer)
            _IdAlias = value
        End Set
    End Property
    Public Property Descripcion() As String
        Get
            Return _al_Descripcion
        End Get
        Set(ByVal value As String)
            _al_Descripcion = value
        End Set
    End Property
    Public Property Estado() As Boolean
        Get
            Return _al_Estado
        End Get
        Set(ByVal value As Boolean)
            _al_Estado = value
        End Set
    End Property
    Public ReadOnly Property strEstado() As String
        Get
            If Estado = True Then Return "Activo"
            If Estado = False Then Return "Inactivo"
        End Get
    End Property
    Public ReadOnly Property intEstado() As String
        Get
            If Estado = True Then Return "1"
            If Estado = False Then Return "0"
        End Get
    End Property

End Class
