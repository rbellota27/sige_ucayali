﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************** NAGAMINE MOLINA, JORGE CHRISTIAN    LUNES 18 ENERO 2010 HORA 11_51 AM

Public Class TipoComision
    Private _IdTipoComision As Integer
    Private _tcomNombre As String
    Private _descEstado As String
    Private _tcomEstado As Boolean
    Public Sub New()
    End Sub
    Public Sub New(ByVal idtipocomision As Integer, ByVal nombre As String, ByVal estado As Boolean)
        Me._IdTipoComision = idtipocomision
        Me._tcomNombre = nombre
        Me._tcomEstado = estado
    End Sub
    Public ReadOnly Property DescEstado()
        Get
            Me._descEstado = "Activo"
            If Me._tcomEstado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public Property IdTipoComision() As Integer
        Get
            Return Me._IdTipoComision
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoComision = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return Me._tcomNombre
        End Get
        Set(ByVal value As String)
            Me._tcomNombre = value
        End Set
    End Property
    Public Property Estado() As Boolean
        Get
            Return Me._tcomEstado
        End Get
        Set(ByVal value As Boolean)
            Me._tcomEstado = value
        End Set
    End Property
    Public ReadOnly Property strEstado() As String
        Get
            If Estado = True Then Return "1"
            If Estado = False Then Return "0"
        End Get
    End Property
End Class
