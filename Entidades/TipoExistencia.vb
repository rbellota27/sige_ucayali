'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'LUNES 01 DE FEBRERO
Public Class TipoExistencia
    Private _IdTipoExistencia As Integer
    Private _tex_CodigoSunat As String
    Private _tex_Nombre As String
    Private _tex_Estado As String
    Private _descEstado As String
    Private _TieneRelacion As Integer


    Public Sub New()
    End Sub
    Public Sub New(ByVal IdTipoExistencia As Integer, ByVal descripcion As String)

        Me.Id = IdTipoExistencia
        Me.Nombre = descripcion

    End Sub

    Public Property TieneRelacion() As Integer
        Get
            Return _TieneRelacion
        End Get
        Set(ByVal value As Integer)
            _TieneRelacion = value
        End Set
    End Property


    Public ReadOnly Property DescEstado() As String
        Get
            Me._descEstado = "Activo"
            If Me._tex_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property

    Public Property Id() As Integer
        Get
            Return Me._IdTipoExistencia
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoExistencia = value
        End Set
    End Property

    Public Property CodigoSunat() As String
        Get
            Return Me._tex_CodigoSunat
        End Get
        Set(ByVal value As String)
            Me._tex_CodigoSunat = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._tex_Nombre
        End Get
        Set(ByVal value As String)
            Me._tex_Nombre = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._tex_Estado
        End Get
        Set(ByVal value As String)
            Me._tex_Estado = value
        End Set
    End Property
End Class
