'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class DetalleGuia
    Private _IdProducto As Integer
    Private _dg_cantidad As Decimal
    Private _dg_UMedida As String
    Private _dg_Peso As Decimal
    Private _IdDocumento As Integer
    Private _dg_UmPeso As String
    Private _costoUnitSaldoInicial As Decimal
    Private _dg_IdUnidadMedida As Integer
    Private _NomProducto As String

    Public Property NomProducto() As String
        Get
            Return Me._NomProducto
        End Get
        Set(ByVal value As String)
            Me._NomProducto = value
        End Set
    End Property
    Public Property IdUnidadMedida() As Integer
        Get
            Return Me._dg_IdUnidadMedida
        End Get
        Set(ByVal value As Integer)
            Me._dg_IdUnidadMedida = value
        End Set
    End Property
    Public Property CostoUnitSaldoInicial() As Decimal
        Get
            Return Me._costoUnitSaldoInicial
        End Get
        Set(ByVal value As Decimal)
            Me._costoUnitSaldoInicial = value
        End Set
    End Property
    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property

    Public Property Cantidad() As Decimal
        Get
            Return Me._dg_cantidad
        End Get
        Set(ByVal value As Decimal)
            Me._dg_cantidad = value
        End Set
    End Property

    Public Property UMedida() As String
        Get
            Return Me._dg_UMedida
        End Get
        Set(ByVal value As String)
            Me._dg_UMedida = value
        End Set
    End Property

    Public Property Peso() As Decimal
        Get
            Return Me._dg_Peso
        End Get
        Set(ByVal value As Decimal)
            Me._dg_Peso = value
        End Set
    End Property

    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property

    Public Property UMPeso() As String
        Get
            Return Me._dg_UmPeso
        End Get
        Set(ByVal value As String)
            Me._dg_UmPeso = value
        End Set
    End Property
End Class
