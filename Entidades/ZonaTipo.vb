'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class ZonaTipo
    Private _IdZonaTipo As Integer
    Private _zt_Nombre As String
    Private _zt_Estado As String
    Private _zt_Abv As String
    Public _descEstado As String

    Public Sub New()

    End Sub

    Public Sub New(ByVal v_IdZonaTipo As Integer, Optional ByVal v_nombre As String = "------", Optional ByVal v_Abv As String = "------")
        Me.Id = v_IdZonaTipo
        Me.Nombre = v_nombre
        Me.Abv = v_Abv
    End Sub

    Public ReadOnly Property DescEstado() As String
        Get
            Me._descEstado = "Activo"
            If Me._zt_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public Property Id() As Integer
        Get
            Return Me._IdZonaTipo
        End Get
        Set(ByVal value As Integer)
            Me._IdZonaTipo = value
        End Set
    End Property
    Public Property Abv() As String
        Get
            Return Me._zt_Abv
        End Get
        Set(ByVal value As String)
            Me._zt_Abv = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return Me._zt_Nombre
        End Get
        Set(ByVal value As String)
            Me._zt_Nombre = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._zt_Estado
        End Get
        Set(ByVal value As String)
            Me._zt_Estado = value
        End Set
    End Property
End Class
