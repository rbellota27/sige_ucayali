'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class MetodoValuacion
    Private _IdMetodoV As Integer
    Private _mv_NombreLargo As String
    Private _mv_NombreCorto As String
    Private _mv_Estado As String
    Private _mv_Vigente As Boolean

    Public Property Vigente() As Boolean
        Get
            Return Me._mv_Vigente
        End Get
        Set(ByVal value As Boolean)
            Me._mv_Vigente = value
        End Set
    End Property

    Public Property Id() As Integer
        Get
            Return Me._IdMetodoV
        End Get
        Set(ByVal value As Integer)
            Me._IdMetodoV = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._mv_NombreLargo
        End Get
        Set(ByVal value As String)
            Me._mv_NombreLargo = value
        End Set
    End Property

    Public Property DescripcionCorto() As String
        Get
            Return Me._mv_NombreCorto
        End Get
        Set(ByVal value As String)
            Me._mv_NombreCorto = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._mv_Estado
        End Get
        Set(ByVal value As String)
            Me._mv_Estado = value
        End Set
    End Property
End Class
