﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'**************************   MARTES 11 MAYO 2010 HORA 10_45 AM

Public Class ProgramacionPago_CXP
    Private _IdMovCtaPP As Integer
    Private _idMovCtaPPCadena As String = String.Empty
    Private _IdProgramacionPago As Integer
    Private _IdRequerimiento As Integer
    Private _pp_FechaPagoProg As Date
    Private _pp_MontoPagoProg As Decimal
    Private _IdMedioPago As Integer
    Private _IdBanco As Integer
    Private _IdCuentaBancaria As Integer
    Private _pp_Observacion As String

    Private _IdTipoMoneda As Integer
    Private _TipoCambio As Decimal
    Private _MedioPago As String
    Private _Banco As String
    Private _NroCuentaBancaria As String
    Private _Moneda As String
    Private _IdMoneda As Integer
    Private _MonedaAdd As String

    Private _NroDocumentoRef As String
    Private _IdDocumentoRef As Integer
    Private _porc_agente As Decimal = 0
    Private _idEsDetraccion As Boolean
    Private _idEsRetencion As Boolean
    Private _idEsPercepcion As Boolean

    Private _saldoProgramacion As Decimal = 0
    Private _montoSujetoADetraccion As Decimal = 0
    Private _montoSujetoARetencion As Decimal = 0

    Private _idConcepto As Integer = 0

    Private _esAdelanto As Boolean = False
    Private _esCtaPorRendir As Boolean = False

    Private _montoPagoTotalRedondeo As Decimal = 0
    Private _nroVoucher As String = String.Empty

    Public Property nroVoucher() As String
        Get
            Return _nroVoucher
        End Get
        Set(value As String)
            _nroVoucher = value
        End Set
    End Property

    Public Property montoPagoTotalRedondeo() As Decimal
        Get
            Return _montoPagoTotalRedondeo
        End Get
        Set(ByVal value As Decimal)
            _montoPagoTotalRedondeo = value
        End Set
    End Property

    Public Property esAdelanto() As Boolean
        Get
            Return _esAdelanto
        End Get
        Set(ByVal value As Boolean)
            Me._esAdelanto = value
        End Set
    End Property

    Public Property esCtaPorRendir() As Boolean
        Get
            Return _esCtaPorRendir
        End Get
        Set(ByVal value As Boolean)
            Me._esCtaPorRendir = value
        End Set
    End Property

    Public Property montoSujetoADetraccion() As Decimal
        Get
            Return Me._montoSujetoADetraccion
        End Get
        Set(value As Decimal)
            Me._montoSujetoADetraccion = value
        End Set
    End Property


    Public Property montoSujetoARetencion() As Decimal
        Get
            Return Me._montoSujetoARetencion
        End Get
        Set(ByVal value As Decimal)
            Me._montoSujetoARetencion = value
        End Set
    End Property

    Public Property IdDocumentoRef() As Integer
        Get
            Return Me._IdDocumentoRef
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumentoRef = value
        End Set
    End Property

    Public Property IdRequerimiento() As Integer
        Get
            Return Me._IdRequerimiento
        End Get
        Set(ByVal value As Integer)
            Me._IdRequerimiento = value
        End Set
    End Property

    Public Property NroDocumentoRef() As String
        Get
            Return Me._NroDocumentoRef
        End Get
        Set(ByVal value As String)
            Me._NroDocumentoRef = value
        End Set
    End Property



    Public Property MonedaNew() As String
        Get
            Return Me._MonedaAdd
        End Get
        Set(ByVal value As String)
            Me._MonedaAdd = value
        End Set
    End Property

    Public Property TipoCambio() As Decimal
        Get
            Return Me._TipoCambio
        End Get
        Set(ByVal value As Decimal)
            Me._TipoCambio = value
        End Set
    End Property

    Public Property IdTipoMoneda() As Integer
        Get
            Return Me._IdTipoMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoMoneda = value
        End Set
    End Property
    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property


    Public Property MedioPago() As String
        Get
            Return Me._MedioPago
        End Get
        Set(ByVal value As String)
            Me._MedioPago = value
        End Set
    End Property

    Public Property Banco() As String
        Get
            Return Me._Banco
        End Get
        Set(ByVal value As String)
            Me._Banco = value
        End Set
    End Property

    Public Property NroCuentaBancaria() As String
        Get
            Return Me._NroCuentaBancaria
        End Get
        Set(ByVal value As String)
            Me._NroCuentaBancaria = value
        End Set
    End Property

    Public Property Moneda() As String
        Get
            Return Me._Moneda
        End Get
        Set(ByVal value As String)
            Me._Moneda = value
        End Set
    End Property

    Public Property IdMovCtaPP() As Integer
        Get
            Return Me._IdMovCtaPP
        End Get
        Set(ByVal value As Integer)
            Me._IdMovCtaPP = value
        End Set
    End Property

    Public Property idMovCtaPPCadena() As String
        Get
            Return Me._idMovCtaPPCadena
        End Get
        Set(ByVal value As String)
            Me._idMovCtaPPCadena = value
        End Set
    End Property

    Public Property IdProgramacionPago() As Integer
        Get
            Return Me._IdProgramacionPago
        End Get
        Set(ByVal value As Integer)
            Me._IdProgramacionPago = value
        End Set
    End Property

    Public Property FechaPagoProg() As Date
        Get
            Return Me._pp_FechaPagoProg
        End Get
        Set(ByVal value As Date)
            Me._pp_FechaPagoProg = value
        End Set
    End Property

    Public Property MontoPagoProg() As Decimal
        Get
            Return Me._pp_MontoPagoProg
        End Get
        Set(ByVal value As Decimal)
            Me._pp_MontoPagoProg = value
        End Set
    End Property

    Public Property IdMedioPago() As Integer
        Get
            Return Me._IdMedioPago
        End Get
        Set(ByVal value As Integer)
            Me._IdMedioPago = value
        End Set
    End Property

    Public Property IdBanco() As Integer
        Get
            Return Me._IdBanco
        End Get
        Set(ByVal value As Integer)
            Me._IdBanco = value
        End Set
    End Property

    Public Property IdCuentaBancaria() As Integer
        Get
            Return Me._IdCuentaBancaria
        End Get
        Set(ByVal value As Integer)
            Me._IdCuentaBancaria = value
        End Set
    End Property

    Public Property Observacion() As String
        Get
            Return Me._pp_Observacion
        End Get
        Set(ByVal value As String)
            Me._pp_Observacion = value
        End Set
    End Property

    Public Property porc_agente() As Decimal
        Get
            Return Me._porc_agente
        End Get
        Set(ByVal value As Decimal)
            Me._porc_agente = value
        End Set
    End Property

    Public Property idEsDetraccion() As Boolean
        Get
            Return Me._idEsDetraccion
        End Get
        Set(ByVal value As Boolean)
            _idEsDetraccion = value
        End Set
    End Property

    Public Property idEsRetencion() As Boolean
        Get
            Return Me._idEsRetencion
        End Get
        Set(ByVal value As Boolean)
            _idEsRetencion = value
        End Set
    End Property

    Public Property idEsPercepcion() As Boolean
        Get
            Return Me._idEsPercepcion
        End Get
        Set(ByVal value As Boolean)
            _idEsPercepcion = value
        End Set
    End Property

    Public Property saldoProgramacion() As Decimal
        Get
            Return _saldoProgramacion
        End Get
        Set(value As Decimal)
            _saldoProgramacion = value
        End Set
    End Property

    Public Property idConcepto() As Integer
        Get
            Return _idConcepto
        End Get
        Set(ByVal value As Integer)
            _idConcepto = value
        End Set
    End Property

    Private _can_NroOperacion As String = String.Empty
    Private _can_montoDeposito As Decimal = 0
    Private _bancoCancelacion As String = String.Empty
    Private _can_fechaPago As String = String.Empty

    Public Property can_NroOperacion() As String
        Get
            Return _can_NroOperacion
        End Get
        Set(ByVal value As String)
            _can_NroOperacion = value
        End Set
    End Property

    Public Property can_montoDeposito() As Decimal
        Get
            Return _can_montoDeposito
        End Get
        Set(ByVal value As Decimal)
            _can_montoDeposito = value
        End Set
    End Property

    Public Property bancoCancelacion() As String
        Get
            Return _bancoCancelacion
        End Get
        Set(ByVal value As String)
            _bancoCancelacion = value
        End Set
    End Property

    Public Property can_fechaPago() As String
        Get
            Return _can_fechaPago
        End Get
        Set(ByVal value As String)
            _can_fechaPago = value
        End Set
    End Property
End Class
