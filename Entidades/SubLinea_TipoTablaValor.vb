﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class SubLinea_TipoTablaValor
    Inherits TipoTablaValor
    Private _IdTipoTabla As Integer
    Private _IdSubLinea As Integer
    Private _IdTipoTablaValor As Integer
    Private _slttv_Estado As Boolean
    Private _NomSubLinea As String
    Private _NomTipoTabla As String
    Private _TipoUso As String
    Private _objSubOrigen As Object
    Private _objSubCodigo As Object
    Private _objSubAtributos As Object
    Private _objSubSinUso As Object
    Private _Indicador As Boolean
    Public Sub New()
    End Sub
    Public Sub New(ByVal vidsublinea As Integer, ByVal vnomsublinea As String, ByVal vidtipotabla As Integer, ByVal vnomtipotabla As String, ByVal vtipouso As String, ByVal vidtipotablavalor As Integer, ByVal vcodigo As String, ByVal vnomtipotablavalor As String, ByVal vestado As Boolean, ByVal vindicador As Boolean)
        IdSubLinea = vidsublinea
        NomSubLinea = vnomsublinea
        IdTipoTabla = vidtipotabla
        NomTipoTabla = vnomtipotabla
        TipoUso = vtipouso
        IdTipoTablaValor = vidtipotablavalor
        Codigo = vcodigo
        Nombre = vnomtipotablavalor
        Estado = vestado
        Indicador = vindicador
    End Sub
    Public Overloads Property IdTipoTabla() As Integer
        Get
            Return _IdTipoTabla
        End Get
        Set(ByVal value As Integer)
            _IdTipoTabla = value
        End Set
    End Property
    Public Property IdSubLinea() As Integer
        Get
            Return _IdSubLinea
        End Get
        Set(ByVal value As Integer)
            _IdSubLinea = value
        End Set
    End Property
    Public Overloads Property IdTipoTablaValor() As Integer
        Get
            Return _IdTipoTablaValor
        End Get
        Set(ByVal value As Integer)
            _IdTipoTablaValor = value
        End Set
    End Property
    Public Overloads Property Estado() As Boolean
        Get
            Return _slttv_Estado
        End Get
        Set(ByVal value As Boolean)
            _slttv_Estado = value
        End Set
    End Property
    Public Overloads ReadOnly Property strEstado() As String
        Get
            If Estado = True Then Return "Activo"
            If Estado = False Then Return "Inactivo"
        End Get
    End Property
    Public Property NomSubLinea() As String
        Get
            Return _NomSubLinea
        End Get
        Set(ByVal value As String)
            _NomSubLinea = value
        End Set
    End Property
    Public Property NomTipoTabla() As String
        Get
            Return _NomTipoTabla
        End Get
        Set(ByVal value As String)
            _NomTipoTabla = value
        End Set
    End Property
    Private _IdLinea As Integer
    Private _NomLinea As String
    Public Property IdLinea() As Integer
        Get
            Return _IdLinea
        End Get
        Set(ByVal value As Integer)
            _IdLinea = value
        End Set
    End Property
    Public Property NomLinea() As String
        Get
            Return _NomLinea
        End Get
        Set(ByVal value As String)
            _NomLinea = value
        End Set
    End Property
    Public Overloads Property TipoUso() As String
        Get
            Return _TipoUso
        End Get
        Set(ByVal value As String)
            _TipoUso = value
        End Set
    End Property
    Private _NroOrden As Integer
    Private _ParteNombre As Boolean
    Public Property NroOrden() As Integer
        Get
            Return _NroOrden
        End Get
        Set(ByVal value As Integer)
            _NroOrden = value
        End Set
    End Property
    Public Property ParteNombre() As Boolean
        Get
            Return _ParteNombre
        End Get
        Set(ByVal value As Boolean)
            _ParteNombre = value
        End Set
    End Property
    Public Property objSubOrigen() As Object
        Get
            Return _objSubOrigen
        End Get
        Set(ByVal value As Object)
            _objSubOrigen = value
        End Set
    End Property
    Public Property objSubCodigo() As Object
        Get
            Return _objSubCodigo
        End Get
        Set(ByVal value As Object)
            _objSubCodigo = value
        End Set
    End Property
    Public Property objSubAtributos() As Object
        Get
            Return _objSubAtributos
        End Get
        Set(ByVal value As Object)
            _objSubAtributos = value
        End Set
    End Property
    Public Property objSubSinUso() As Object
        Get
            Return _objSubSinUso
        End Get
        Set(ByVal value As Object)
            _objSubSinUso = value
        End Set
    End Property
    Public Property Indicador() As Boolean
        Get
            Return _Indicador
        End Get
        Set(ByVal value As Boolean)
            _Indicador = value
        End Set
    End Property
End Class
