'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class DetalleMovAlmacen
    Private _IdTienda As Integer
    Private _IdAlmacen As Integer
    Private _IdEmpresa As Integer
    Private _IdMovAlmacen As Integer
    Private _IdProducto As Integer
    Private _IdDetalleMovAlmacen As Integer
    Private _dma_Factor As Integer
    Private _dma_Cantidad As Decimal
    Private _dma_Costo As Decimal
    Private _dma_Total As Decimal
    Private _IdDocumento As Integer

    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property
    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property IdAlmacen() As Integer
        Get
            Return Me._IdAlmacen
        End Get
        Set(ByVal value As Integer)
            Me._IdAlmacen = value
        End Set
    End Property

    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property

    Public Property IdMovAlmacen() As Integer
        Get
            Return Me._IdMovAlmacen
        End Get
        Set(ByVal value As Integer)
            Me._IdMovAlmacen = value
        End Set
    End Property

    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property

    Public Property IdDetalleMovAlmacen() As Integer
        Get
            Return Me._IdDetalleMovAlmacen
        End Get
        Set(ByVal value As Integer)
            Me._IdDetalleMovAlmacen = value
        End Set
    End Property

    Public Property Factor() As Integer
        Get
            Return Me._dma_Factor
        End Get
        Set(ByVal value As Integer)
            Me._dma_Factor = value
        End Set
    End Property

    Public Property Cantidad() As Decimal
        Get
            Return Me._dma_Cantidad
        End Get
        Set(ByVal value As Decimal)
            Me._dma_Cantidad = value
        End Set
    End Property

    Public Property Costo() As Decimal
        Get
            Return Me._dma_Costo
        End Get
        Set(ByVal value As Decimal)
            Me._dma_Costo = value
        End Set
    End Property

    Public Property Total() As Decimal
        Get
            Return Me._dma_Total
        End Get
        Set(ByVal value As Decimal)
            Me._dma_Total = value
        End Set
    End Property
End Class
