﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Pais

    Private _IdPais As String
    Private _IdProcedencia As String
    Private _pa_Nombre As String
    Private _pa_Estado As Boolean
    Private _strEstado As String
    Private _pa_Codigo As String
    Private _pa_Base As String
    Private _pa_BaseBit As Boolean

    Public Sub New()
    End Sub
    Public Sub New(ByVal idpais As String, ByVal idprocedencia As String, ByVal nombre As String, ByVal estado As Boolean)
        Me._IdPais = idpais
        Me._IdProcedencia = idprocedencia
        Me._pa_Nombre = nombre
        Me._pa_Estado = estado
    End Sub
    Public Sub New(ByVal idpais As String, ByVal nombre As String, ByVal estado As Boolean)
        Me._IdPais = idpais
        Me._pa_Nombre = nombre
    End Sub
    Public Property IdPais() As String
        Get
            Return Me._IdPais
        End Get
        Set(ByVal value As String)
            Me._IdPais = value
        End Set
    End Property
    Public Property IdProcedencia() As String
        Get
            Return Me._IdProcedencia
        End Get
        Set(ByVal value As String)
            Me._IdProcedencia = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return Me._pa_Nombre
        End Get
        Set(ByVal value As String)
            Me._pa_Nombre = value
        End Set
    End Property
    Public Property Estado() As Boolean
        Get
            Return Me._pa_Estado
        End Get
        Set(ByVal value As Boolean)
            Me._pa_Estado = value
        End Set
    End Property

    Public ReadOnly Property strEstado() As String
        Get
            If Estado = True Then Return "Activo"
            If Estado = False Then Return "Inactivo"
        End Get
    End Property
    Public Property pa_Codigo() As String
        Get
            Return _pa_Codigo
        End Get
        Set(ByVal value As String)
            _pa_Codigo = value
        End Set
    End Property
    Public Property pa_Base() As String
        Get
            Return _pa_Base
        End Get
        Set(ByVal value As String)
            _pa_Base = value
        End Set
    End Property
    Public Property pa_BaseBit() As Boolean
        Get
            Return _pa_BaseBit
        End Get
        Set(ByVal value As Boolean)
            _pa_BaseBit = value
        End Set
    End Property

End Class
