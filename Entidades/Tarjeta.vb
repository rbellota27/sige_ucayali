﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************** MIRAYA ANAMARIA, EDGAR    MARTES 02 FEBRERO 2010 HORA 6_53 PM  'IdTipoTarjeta
'*************** NAGAMINE MOLINA, JORGE CHRISTIAN    LUNES 18 ENERO 2010 HORA 11_51 AM

Public Class Tarjeta
    Protected _IdTarjeta As Integer
    Protected _tNombre As String
    Protected _descEstado As String
    Protected _tEstado As Nullable(Of Boolean)
    Protected _IdTipoTarjeta As Integer

    Public Sub New()
        Me._IdTarjeta = -1
        Me._tNombre = ""
        Me._tEstado = Nothing
        Me._IdTipoTarjeta = -1
    End Sub

    Public Sub New(ByVal idtarjeta As Integer, ByVal nombre As String, ByVal estado As Boolean, Optional ByVal IdTipoTarjeta As Integer = -1)
        Me._IdTarjeta = idtarjeta
        Me._tNombre = nombre
        Me._tEstado = estado
        Me._IdTipoTarjeta = IdTipoTarjeta
    End Sub

    Public ReadOnly Property DescEstado() As String
        Get
            If Estado Then
                Return "Activo"
            Else
                Return "Inactivo"
            End If
        End Get
    End Property

    Public Property Id() As Integer
        Get
            Return Me._IdTarjeta
        End Get
        Set(ByVal value As Integer)
            Me._IdTarjeta = value
        End Set
    End Property
    Public Property IdTarjeta() As Integer
        Get
            Return Me._IdTarjeta
        End Get
        Set(ByVal value As Integer)
            Me._IdTarjeta = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return Me._tNombre
        End Get
        Set(ByVal value As String)
            Me._tNombre = value
        End Set
    End Property
    Public Property Estado() As Boolean?
        Get
            Return Me._tEstado
        End Get
        Set(ByVal value As Boolean?)
            Me._tEstado = value
        End Set
    End Property
    Public ReadOnly Property strEstado() As String
        Get
            If Estado Then
                Return "1"
            Else
                Return "0"
            End If
        End Get
    End Property

    Public Property IdTipoTarjeta() As Integer
        Get
            Return Me._IdTipoTarjeta
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoTarjeta = value
        End Set
    End Property
End Class
