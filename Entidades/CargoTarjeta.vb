﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'*************************************************
'Autor   : Chigne Bazan, Dany
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 12-01-2009
'Hora    : 09:40 am
'*************************************************
Public Class CargoTarjeta
    Private _IdCargoTarjeta As Integer
    Private _ct_Nombre As String
    Private _ct_Valor As Decimal
    Private _ct_Estado As String
    Private _ct_FechaReg As Date

    Private _ct_DescEstado As String
    Private _ct_EstadoAE As Boolean
    Private _CodigoCargoTarjeta As Integer
    Public Sub New()
    End Sub
    Public Sub New(ByVal IdCargoTarjeta As Integer, ByVal ct_Valor As String, ByVal estadoae As Boolean)
        Me._IdCargoTarjeta = IdCargoTarjeta
        Me._ct_Valor = ct_Valor
        Me._ct_EstadoAE = estadoae
    End Sub

    Public Sub New(ByVal IdCargoTarjeta As Integer, ByVal ct_Valor As String)
        Me._IdCargoTarjeta = IdCargoTarjeta
        Me._ct_Valor = ct_Valor
        Me._ct_EstadoAE = estadoae
    End Sub

    Public Property CodigoCargoTarjeta() As Integer
        Get
            Return Me._CodigoCargoTarjeta
        End Get
        Set(ByVal value As Integer)
            Me._CodigoCargoTarjeta = value
        End Set
    End Property
    Public Property IdCargoTarjeta() As Integer
        Get
            Return Me._IdCargoTarjeta
        End Get
        Set(ByVal value As Integer)
            Me._IdCargoTarjeta = value
        End Set
    End Property
    Public Property Id() As Integer
        Get
            Return Me._IdCargoTarjeta
        End Get
        Set(ByVal value As Integer)
            Me._IdCargoTarjeta = value
        End Set
    End Property

    Public Property ct_Nombre() As String
        Get
            Return Me._ct_Nombre
        End Get
        Set(ByVal value As String)
            Me._ct_Nombre = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._ct_Estado
        End Get
        Set(ByVal value As String)
            Me._ct_Estado = value
        End Set
    End Property
    Public Property ct_FechaReg() As Date
        Get
            Return Me._ct_FechaReg
        End Get
        Set(ByVal value As Date)
            Me._ct_FechaReg = value
        End Set
    End Property
    Public Property ct_Valor() As Decimal
        Get
            Return Me._ct_Valor
        End Get
        Set(ByVal value As Decimal)
            Me._ct_Valor = value
        End Set
    End Property

    Public Property ct_Estado() As String
        Get
            Return Me._ct_Estado
        End Get
        Set(ByVal value As String)
            Me._ct_Estado = value
        End Set
    End Property
    'Public Property ct_FechaReg() As Date
    '    Get
    '        Return Me._ct_FechaReg
    '    End Get
    '    Set(ByVal value As Date)
    '        Me._ct_FechaReg = value
    '    End Set
    'End Property
    Public ReadOnly Property DescEstado() As String
        Get

            If _ct_Estado = "1" Then
                Me._ct_DescEstado = "Activo"
            Else
                Me._ct_DescEstado = "Inactivo"
            End If
            Return Me._ct_DescEstado
        End Get
    End Property

    Public Property EstadoAE() As Boolean
        Get
            Return _ct_EstadoAE
        End Get
        Set(ByVal value As Boolean)
            _ct_EstadoAE = value
        End Set
    End Property

End Class
