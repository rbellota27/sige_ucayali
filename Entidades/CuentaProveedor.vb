﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'**************************   lunes 28 junio 2010 


Public Class CuentaProveedor
    Private _IdCuentaProv As Integer
    Private _IdProveedor As Integer
    Private _IdPropietario As Integer
    Private _IdMoneda As Integer
    Private _Mon_Simbolo As String
    Private _cprov_FechaAlta As Date
    Private _cprov_CargoMax As Decimal
    Private _cprov_Saldo As Decimal
    Private _cpro_Estado As String
    Private _descEstado As String
    Private _DeudaPendiente As Decimal
    Private _obserProv As String
    Private _MonedaSimbolo As String
    Private _FechaAlta As DateTime
    Private _ListaMoneda As List(Of Entidades.Moneda) = Nothing

    Public Property ListaMoneda() As List(Of Entidades.Moneda)
        Get
            Return _ListaMoneda
        End Get
        Set(ByVal value As List(Of Entidades.Moneda))
            _ListaMoneda = value
        End Set
    End Property


    Public Property MonedaSimbolo() As String
        Get
            Return _MonedaSimbolo
        End Get
        Set(ByVal value As String)
            _MonedaSimbolo = value
        End Set
    End Property

    Public Property ObservProv() As String
        Get
            Return _obserProv
        End Get
        Set(ByVal value As String)
            _obserProv = value
        End Set
    End Property


    Public Property DeudaPendiente() As Decimal
        Get
            Return Me._DeudaPendiente
        End Get
        Set(ByVal value As Decimal)
            Me._DeudaPendiente = value
        End Set
    End Property


    Public Sub New()
    End Sub

    Public Sub New(ByVal IdProveedor As Integer, ByVal IdPropietario As Integer, ByVal IdMoneda As Integer, ByVal Mon_Simbolo As String, _
         ByVal cprov_CargoMax As Decimal, ByVal cprov_Saldo As Decimal, ByVal cpro_Estado As String, ByVal cp_observacion As String, ByVal FechaAlta As String, ByVal idctaprov As Integer)
        Me._IdProveedor = IdProveedor
        Me._IdPropietario = IdPropietario
        Me._IdMoneda = IdMoneda
        Me.MonedaSimbolo = Mon_Simbolo
        Me._cprov_CargoMax = cprov_CargoMax
        Me._cprov_Saldo = cprov_Saldo
        Me._obserProv = cp_observacion
        Me._cpro_Estado = cpro_Estado
        Me._cprov_FechaAlta = FechaAlta
        _IdCuentaProv = idctaprov
    End Sub
    Public Sub New(ByVal IdProveedor As Integer, ByVal IdMoneda As Integer, _
            ByVal cprov_CargoMax As Decimal, ByVal cprov_Saldo As Decimal, ByVal cpro_Estado As String)
        Me._IdProveedor = IdProveedor
        Me._IdMoneda = IdMoneda
        Me._cprov_CargoMax = cprov_CargoMax
        Me._cprov_Saldo = cprov_Saldo
        Me._cpro_Estado = cpro_Estado
    End Sub
    Public Sub New(ByVal IdProveedor As Integer, ByVal IdMoneda As Integer, ByVal Mon_Simbolo As String, _
        ByVal cprov_CargoMax As Decimal, ByVal cprov_Saldo As Decimal, ByVal cpro_Estado As String)
        Me._IdProveedor = IdProveedor
        Me._IdMoneda = IdMoneda
        Me.Mon_Simbolo = Mon_Simbolo
        Me._cprov_CargoMax = cprov_CargoMax
        Me._cprov_Saldo = cprov_Saldo
        Me._cpro_Estado = cpro_Estado
    End Sub

    Public Property IdCuentaProv() As Integer
        Get
            Return Me._IdCuentaProv
        End Get
        Set(ByVal value As Integer)
            Me._IdCuentaProv = value
        End Set
    End Property
    Public Property Id() As Integer
        Get
            Return Me._IdCuentaProv
        End Get
        Set(ByVal value As Integer)
            Me._IdCuentaProv = value
        End Set
    End Property
    Public Property IdProveedor() As Integer
        Get
            Return Me._IdProveedor
        End Get
        Set(ByVal value As Integer)
            Me._IdProveedor = value
        End Set
    End Property
    Public Property IdPropietario() As Integer
        Get
            Return Me._IdPropietario
        End Get
        Set(ByVal value As Integer)
            Me._IdPropietario = value
        End Set
    End Property
    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property
    Public Property Mon_Simbolo() As String '
        Get
            Return Me._Mon_Simbolo
        End Get
        Set(ByVal value As String)
            Me._Mon_Simbolo = value
        End Set
    End Property
    Public Property cprov_FechaAlta() As String
        Get
            Return Me._cprov_FechaAlta
        End Get
        Set(ByVal value As String)
            Me._cprov_FechaAlta = value
        End Set
    End Property

    Public Property cprov_CargoMax() As Decimal
        Get
            Return Me._cprov_CargoMax
        End Get
        Set(ByVal value As Decimal)
            Me._cprov_CargoMax = value
        End Set
    End Property

    Public Property cprov_Saldo() As Decimal
        Get
            Return Me._cprov_Saldo
        End Get
        Set(ByVal value As Decimal)
            Me._cprov_Saldo = value
        End Set
    End Property
    Public Property Estado() As String
        Get
            Return Me._cpro_Estado
        End Get
        Set(ByVal value As String)
            Me._cpro_Estado = value
        End Set
    End Property
    Public Property cpro_Estado() As String
        Get
            Return Me._cpro_Estado
        End Get
        Set(ByVal value As String)
            Me._cpro_Estado = value
        End Set
    End Property
    Public ReadOnly Property DescEstado()
        Get
            If Me._cpro_Estado = True Then
                Me._descEstado = "Activo"
            Else
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property

End Class
