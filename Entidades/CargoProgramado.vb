'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class CargoProgramado
    Private _IdMovCuenta As Integer
    Private _IdCargoProgramado As Integer
    Private _cap_FechaProg As Date
    Private _cap_Monto As Decimal
    Private _cap_FechaPago As Date
    Private _IdEstadoCargoP As Integer
    Public Property IdMovCuenta() As Integer
        Get
            Return Me._IdMovCuenta
        End Get
        Set(ByVal value As Integer)
            Me._IdMovCuenta = value
        End Set
    End Property

    Public Property IdCargoProgramado() As Integer
        Get
            Return Me._IdCargoProgramado
        End Get
        Set(ByVal value As Integer)
            Me._IdCargoProgramado = value
        End Set
    End Property

    Public Property FechaProg() As Date
        Get
            Return Me._cap_FechaProg
        End Get
        Set(ByVal value As Date)
            Me._cap_FechaProg = value
        End Set
    End Property

    Public Property Monto() As Decimal
        Get
            Return Me._cap_Monto
        End Get
        Set(ByVal value As Decimal)
            Me._cap_Monto = value
        End Set
    End Property

    Public Property FechaPago() As Date
        Get
            Return Me._cap_FechaPago
        End Get
        Set(ByVal value As Date)
            Me._cap_FechaPago = value
        End Set
    End Property

    Public Property IdEstadoCargoP() As Integer
        Get
            Return Me._IdEstadoCargoP
        End Get
        Set(ByVal value As Integer)
            Me._IdEstadoCargoP = value
        End Set
    End Property
End Class
