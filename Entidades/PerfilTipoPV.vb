﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Public Class PerfilTipoPV
    Private _IdPerfil As Integer
    Private _IdTipoPV As Integer
    Private _NombreTipoPV As String
    Private _PerPV_Estado As Char
    Private _perpv_estadopermisomasivo As Char
    Public Property IdPerfil() As Integer
        Get
            Return _IdPerfil
        End Get
        Set(ByVal value As Integer)
            _IdPerfil = value
        End Set
    End Property
    Public Property IdTipoPV() As Integer
        Get
            Return _IdTipoPV
        End Get
        Set(ByVal value As Integer)
            _IdTipoPV = value
        End Set
    End Property
    Public Property NombreTipoPV() As String
        Get
            Return _NombreTipoPV
        End Get
        Set(ByVal value As String)
            _NombreTipoPV = value
        End Set
    End Property
    Public Property PerPV_Estado() As Char
        Get
            Return _PerPV_Estado
        End Get
        Set(ByVal value As Char)
            _PerPV_Estado = value
        End Set
    End Property

    Public ReadOnly Property VerEstado() As Boolean
        Get
            If PerPV_Estado = "1" Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property VerPermiso() As Boolean
        Get
            If _perpv_estadopermisomasivo = "1" Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

End Class
