'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Vehiculo
    'despachos
    Private _turno As String = String.Empty
    Private _IdVehiculo As Integer = 0
    Private _IdConductor As Integer = 0
    Private _IdAyudante As Integer = 0
    Private _IdControl As Integer = 0
    Private _veh_Capacidad As Decimal = 0
    Private _veh_Placa As String = String.Empty
    Private _veh_Modelo As String = String.Empty
    Private _veh_NConstanciaIns As String = String.Empty
    Private _veh_ConfVeh As String = String.Empty
    Private _veh_Tara As Decimal
    Private _IdProducto As Integer
    Private _EstadosVehiculo As String
    Private _NomProducto As String
    '*********TipoExistencia - Linea -SubLinea - EstadoProd
    Private _NomTipoExistencia As String
    Private _IdTipoExistencia As Integer
    Private _NomLinea As String
    Private _IdLinea As Integer
    Private _NomSubLinea As String
    Private _IdSublinea As Integer
    Private _NomEstadoProd As String
    Private _IdEstadoProd As Integer
    '=============DATOS TRANSPORTISTA PARTICULAR====================
    Private _idPersona As Integer = 0
    Private _nom_transportista As String = String.Empty
    Private _documentoIdentidad As String = String.Empty
    Private _TelefonoTransportista As String = String.Empty
    Private _ditritoTransportista As String = String.Empty

    Public Property idPersona() As Integer
        Get
            Return _idPersona
        End Get
        Set(ByVal value As Integer)
            _idPersona = value
        End Set
    End Property

    Public Property nom_transportista() As String
        Get
            Return _nom_transportista
        End Get
        Set(ByVal value As String)
            _nom_transportista = value
        End Set
    End Property

    Public Property documentoIdentidad() As String
        Get
            Return _documentoIdentidad
        End Get
        Set(ByVal value As String)
            _documentoIdentidad = value
        End Set
    End Property

    Public Property telefonoTransportista() As String
        Get
            Return _TelefonoTransportista
        End Get
        Set(ByVal value As String)
            _TelefonoTransportista = value
        End Set
    End Property

    Public Property ditritoTransportista() As String
        Get
            Return _ditritoTransportista
        End Get
        Set(ByVal value As String)
            _ditritoTransportista = value
        End Set
    End Property

    Public Property Turno() As String
        Get
            Return Me._turno
        End Get
        Set(ByVal value As String)
            _turno = value
        End Set
    End Property
    Public Property IdVehiculo As Integer
        Get
            Return Me._IdVehiculo
        End Get
        Set(ByVal value As Integer)
            _IdVehiculo = value
        End Set
    End Property

    Public Property IdConductor As Integer
        Get
            Return Me._IdConductor
        End Get
        Set(ByVal value As Integer)
            _IdConductor = value
        End Set
    End Property

    Public Property IdAyudante As Integer
        Get
            Return Me._IdAyudante
        End Get
        Set(ByVal value As Integer)
            _IdAyudante = value
        End Set
    End Property

    Public Property IdControl As Integer
        Get
            Return Me._IdControl
        End Get
        Set(ByVal value As Integer)
            _IdControl = value
        End Set
    End Property

    Public Property veh_Capacidad() As Decimal
        Get
            Return Me._veh_Capacidad
        End Get
        Set(ByVal value As Decimal)
            _veh_Capacidad = value
        End Set
    End Property

    Public Property nombreTipoExistencia() As String
        Get
            Return Me._NomTipoExistencia
        End Get
        Set(ByVal value As String)
            _NomTipoExistencia = value
        End Set
    End Property
    Public Property IdTipoExistencia() As Integer
        Get
            Return _IdTipoExistencia
        End Get
        Set(ByVal value As Integer)
            _IdTipoExistencia = value
        End Set
    End Property
    Public Property NomLinea() As String
        Get
            Return _NomLinea
        End Get
        Set(ByVal value As String)
            _NomLinea = value
        End Set
    End Property
    Public Property IdLinea() As Integer
        Get
            Return _IdLinea
        End Get
        Set(ByVal value As Integer)
            _IdLinea = value
        End Set
    End Property
    Public Property NomSubLinea() As String
        Get
            Return _NomSubLinea
        End Get
        Set(ByVal value As String)
            _NomSubLinea = value
        End Set
    End Property
    Public Property IdSubLinea() As Integer
        Get
            Return _IdSublinea
        End Get
        Set(ByVal value As Integer)
            _IdSublinea = value
        End Set
    End Property
    Public Property IdEstadoProd() As Integer
        Get
            Return _IdEstadoProd
        End Get
        Set(ByVal value As Integer)
            _IdEstadoProd = value
        End Set
    End Property
    Public Property NomEstadoProd() As String
        Get
            Return _NomEstadoProd
        End Get
        Set(ByVal value As String)
            _NomEstadoProd = value
        End Set
    End Property
    Public Property Placa() As String
        Get
            Return Me._veh_Placa
        End Get
        Set(ByVal value As String)
            Me._veh_Placa = value
        End Set
    End Property

    Public Property Modelo() As String
        Get
            Return Me._veh_Modelo
        End Get
        Set(ByVal value As String)
            Me._veh_Modelo = value
        End Set
    End Property

    Public Property ConstanciaIns() As String
        Get
            Return Me._veh_NConstanciaIns
        End Get
        Set(ByVal value As String)
            Me._veh_NConstanciaIns = value
        End Set
    End Property

    Public Property ConfVeh() As String
        Get
            Return Me._veh_ConfVeh
        End Get
        Set(ByVal value As String)
            Me._veh_ConfVeh = value
        End Set
    End Property

    Public Property Tara() As Decimal
        Get
            Return Me._veh_Tara
        End Get
        Set(ByVal value As Decimal)
            Me._veh_Tara = value
        End Set
    End Property

    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property

    Public Property EstadosVehiculo() As String
        Get
            Return Me._EstadosVehiculo
        End Get
        Set(ByVal value As String)
            Me._EstadosVehiculo = value
        End Set
    End Property

    Public Property NomProducto() As String
        Get
            Return Me._NomProducto
        End Get
        Set(ByVal value As String)
            Me._NomProducto = value
        End Set
    End Property



End Class
