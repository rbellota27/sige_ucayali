'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : Nagamine Molina, Jorge
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 12-Octubre-2009
'Hora    : 01:10 pm
'*************************************************

Public Class DetalleRecibo
    Private _IdRecibo As Integer
    Private _dr_Concepto As String
    Private _dr_Monto As Decimal
    Private _IdConcepto As Integer
    Private _IdMovCuentaAfecto As Integer
    Private _IdDocumentoAfecto As Integer


    Private _IdDocumento As Integer

    Private _IdDetalleRecibo As Integer


    Private _TipoRecibo As String
    '***** I : Ingreso
    '***** E : Egreso



    Public Property TipoRecibo() As String
        Get
            Return Me._TipoRecibo
        End Get
        Set(ByVal value As String)
            Me._TipoRecibo = value
        End Set
    End Property




    Public Property IdDetalleRecibo() As Integer
        Get
            Return Me._IdDetalleRecibo
        End Get
        Set(ByVal value As Integer)
            Me._IdDetalleRecibo = value
        End Set
    End Property


    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property



    Public Property IdMovCuentaAfecto() As Integer
        Get
            Return Me._IdMovCuentaAfecto
        End Get
        Set(ByVal value As Integer)
            Me._IdMovCuentaAfecto = value
        End Set
    End Property

    Public Property IdDocumentoAfecto() As Integer
        Get
            Return Me._IdDocumentoAfecto
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumentoAfecto = value
        End Set
    End Property

    Public Property IdRecibo() As Integer
        Get
            Return Me._IdRecibo
        End Get
        Set(ByVal value As Integer)
            Me._IdRecibo = value
        End Set
    End Property

    Public Property Concepto() As String
        Get
            Return Me._dr_Concepto
        End Get
        Set(ByVal value As String)
            Me._dr_Concepto = value
        End Set
    End Property

    Public Property Monto() As Decimal
        Get
            Return Me._dr_Monto
        End Get
        Set(ByVal value As Decimal)
            Me._dr_Monto = value
        End Set
    End Property

    Public Property IdConcepto() As Integer
        Get
            Return Me._IdConcepto
        End Get
        Set(ByVal value As Integer)
            Me._IdConcepto = value
        End Set
    End Property
End Class
