'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class MovRetazo
    Private _IdProducto As Integer
    Private _IdTienda As Integer
    Private _IdAlmacen As Integer
    Private _IdRetazo As Integer
    Private _IdMovAlmacen As Integer
    Private _IdEmpresa As Integer
    Private _mr_Fecha As Date
    Private _mr_Cantidad As Decimal
    Private _mr_Factor As Integer
    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property

    Public Property IdRetazo() As Integer
        Get
            Return Me._IdRetazo
        End Get
        Set(ByVal value As Integer)
            Me._IdRetazo = value
        End Set
    End Property

    Public Property Fecha() As Date
        Get
            Return Me._mr_Fecha
        End Get
        Set(ByVal value As Date)
            Me._mr_Fecha = value
        End Set
    End Property

    Public Property Cantidad() As Decimal
        Get
            Return Me._mr_Cantidad
        End Get
        Set(ByVal value As Decimal)
            Me._mr_Cantidad = value
        End Set
    End Property

    Public Property Factor() As Integer
        Get
            Return Me._mr_Factor
        End Get
        Set(ByVal value As Integer)
            Me._mr_Factor = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property IdAlmacen() As Integer
        Get
            Return Me._IdAlmacen
        End Get
        Set(ByVal value As Integer)
            Me._IdAlmacen = value
        End Set
    End Property

    Public Property IdMovAlmacen() As Integer
        Get
            Return Me._IdMovAlmacen
        End Get
        Set(ByVal value As Integer)
            Me._IdMovAlmacen = value
        End Set
    End Property

    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property
End Class
