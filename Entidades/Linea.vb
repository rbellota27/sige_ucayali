'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

' Modificado 25/10/2010

Public Class Linea
    Private _IdLinea As Integer
    Private _IdTipoExistencia As Integer
    Private _lin_Nombre As String
    Private _lin_Estado As String
    Private _lin_CtaDebeCompra As String
    Private _lin_CtaHaberCompra As String
    Private _lin_CostoCompra As String
    Private _lin_CtaDebeVenta As String
    Private _lin_CtaHaberVenta As String
    Private _lin_CostoVenta As String
    Public _descEstado As String
    Public _CodigoAntiguo As String
    Public _FlagPais As Boolean
    Public _FlagFabricante As Boolean
    Public _FlagMarca As Boolean
    Public _FlagModelo As Boolean
    Public _FlagFormato As Boolean
    Public _FlagProveedor As Boolean
    Public _FlagEstilo As Boolean
    Public _FlagTransito As Boolean
    Public _FlagCalidad As Boolean
    Public _FlagColor As Boolean
    Public _FlagSubSubLinea As Boolean
    Public _OrdenPais As String
    Public _OrdenFabricante As String
    Public _OrdenMarca As String
    Public _OrdenModelo As String
    Public _OrdenFormato As String
    Public _OrdenProveedor As String
    Public _OrdenEstilo As String
    Public _OrdenTransito As String
    Public _OrdenCalidad As String
    Public _OrdenColor As String
    Public _OrdenSubSubLinea As String
    Public _estado As Boolean
    Public _Codigo As String
    Public _lin_Orden As Integer
    Public _lin_Longitud As Integer
    Public _Cantidad As Integer
    Private _tipoExistencia As String

    Public Sub New()
    End Sub
    Public Sub New(ByVal idlinea As Integer, ByVal nombre As String)
        Me.Id = idlinea
        Me.Descripcion = nombre
    End Sub
    Public Sub New(ByVal idlinea As Integer, ByVal nombre As String, ByVal estado As Boolean)
        Me.Id = idlinea
        Me.Descripcion = nombre
        Me.EstadoBit = estado
    End Sub
    Public Sub New(ByVal codigo As String, ByVal nombre As String, ByVal estado As Boolean)
        Me.Codigo = codigo
        Me.Descripcion = nombre
        Me.EstadoBit = estado
    End Sub
    Public Property Id() As Integer
        Get
            Return Me._IdLinea
        End Get
        Set(ByVal value As Integer)
            Me._IdLinea = value
        End Set
    End Property
    Public Property Descripcion() As String
        Get
            Return Me._lin_Nombre
        End Get
        Set(ByVal value As String)
            Me._lin_Nombre = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._lin_Estado
        End Get
        Set(ByVal value As String)
            Me._lin_Estado = value
        End Set
    End Property

    Public Property IdTipoExistencia() As Integer
        Get
            Return Me._IdTipoExistencia
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoExistencia = value
        End Set
    End Property

    Public Property CtaDebeCompra() As String
        Get
            Return Me._lin_CtaDebeCompra
        End Get
        Set(ByVal value As String)
            Me._lin_CtaDebeCompra = value
        End Set
    End Property

    Public Property CtaHaberCompra() As String
        Get
            Return Me._lin_CtaHaberCompra
        End Get
        Set(ByVal value As String)
            Me._lin_CtaHaberCompra = value
        End Set
    End Property

    Public Property CostoCompra() As String
        Get
            Return Me._lin_CostoCompra
        End Get
        Set(ByVal value As String)
            Me._lin_CostoCompra = value
        End Set
    End Property

    Public Property CtaDebeVenta() As String
        Get
            Return Me._lin_CtaDebeVenta
        End Get
        Set(ByVal value As String)
            Me._lin_CtaDebeVenta = value
        End Set
    End Property

    Public Property CtaHaberVenta() As String
        Get
            Return Me._lin_CtaHaberVenta
        End Get
        Set(ByVal value As String)
            Me._lin_CtaHaberVenta = value
        End Set
    End Property

    Public Property CostoVenta() As String
        Get
            Return Me._lin_CostoVenta
        End Get
        Set(ByVal value As String)
            Me._lin_CostoVenta = value
        End Set
    End Property
    Public ReadOnly Property DescEstado() As String
        Get
            Me._descEstado = "Activo"
            If Me._lin_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property

    Public Property CodigoAntiguo() As String
        Get
            Return _CodigoAntiguo
        End Get
        Set(ByVal value As String)
            _CodigoAntiguo = value
        End Set
    End Property
    Public Property FlagPais() As Boolean
        Get
            Return _FlagPais
        End Get
        Set(ByVal value As Boolean)
            _FlagPais = value
        End Set
    End Property
    Public Property FlagFabricante() As Boolean
        Get
            Return _FlagFabricante
        End Get
        Set(ByVal value As Boolean)
            _FlagFabricante = value
        End Set
    End Property
    Public Property FlagMarca() As Boolean
        Get
            Return _FlagMarca
        End Get
        Set(ByVal value As Boolean)
            _FlagMarca = value
        End Set
    End Property
    Public Property FlagModelo() As Boolean
        Get
            Return _FlagModelo
        End Get
        Set(ByVal value As Boolean)
            _FlagModelo = value
        End Set
    End Property
    Public Property FlagFormato() As Boolean
        Get
            Return _FlagFormato
        End Get
        Set(ByVal value As Boolean)
            _FlagFormato = value
        End Set
    End Property
    Public Property FlagProveedor() As Boolean
        Get
            Return _FlagProveedor
        End Get
        Set(ByVal value As Boolean)
            _FlagProveedor = value
        End Set
    End Property
    Public Property FlagEstilo() As Boolean
        Get
            Return _FlagEstilo
        End Get
        Set(ByVal value As Boolean)
            _FlagEstilo = value
        End Set
    End Property
    Public Property FlagTransito() As Boolean
        Get
            Return _FlagTransito
        End Get
        Set(ByVal value As Boolean)
            _FlagTransito = value
        End Set
    End Property
    Public Property FlagCalidad() As Boolean
        Get
            Return _FlagCalidad
        End Get
        Set(ByVal value As Boolean)
            _FlagCalidad = value
        End Set
    End Property
    Public Property FlagColor() As Boolean
        Get
            Return _FlagColor
        End Get
        Set(ByVal value As Boolean)
            _FlagColor = value
        End Set
    End Property
    Public Property FlagSubSubLinea() As Boolean
        Get
            Return _FlagSubSubLinea
        End Get
        Set(ByVal value As Boolean)
            _FlagSubSubLinea = value
        End Set
    End Property

    Public Property OrdenPais() As String
        Get
            Return _OrdenPais
        End Get
        Set(ByVal value As String)
            _OrdenPais = value
        End Set
    End Property

    Public Property OrdenFabricante() As String
        Get
            Return _OrdenFabricante
        End Get
        Set(ByVal value As String)
            _OrdenFabricante = value
        End Set
    End Property
    Public Property OrdenMarca() As String
        Get
            Return _OrdenMarca
        End Get
        Set(ByVal value As String)
            _OrdenMarca = value
        End Set
    End Property
    Public Property OrdenModelo() As String
        Get
            Return _OrdenModelo
        End Get
        Set(ByVal value As String)
            _OrdenModelo = value
        End Set
    End Property
    Public Property OrdenFormato() As String
        Get
            Return _OrdenFormato
        End Get
        Set(ByVal value As String)
            _OrdenFormato = value
        End Set
    End Property
    Public Property OrdenProveedor() As String
        Get
            Return _OrdenProveedor
        End Get
        Set(ByVal value As String)
            _OrdenProveedor = value
        End Set
    End Property
    Public Property OrdenEstilo() As String
        Get
            Return _OrdenEstilo
        End Get
        Set(ByVal value As String)
            _OrdenEstilo = value
        End Set
    End Property
    Public Property OrdenTransito() As String
        Get
            Return _OrdenTransito
        End Get
        Set(ByVal value As String)
            _OrdenTransito = value
        End Set
    End Property
    Public Property OrdenCalidad() As String
        Get
            Return _OrdenCalidad
        End Get
        Set(ByVal value As String)
            _OrdenCalidad = value
        End Set
    End Property
    Public Property OrdenColor() As String
        Get
            Return _OrdenColor
        End Get
        Set(ByVal value As String)
            _OrdenColor = value
        End Set
    End Property
    Public Property OrdenSubSubLinea() As String
        Get
            Return _OrdenSubSubLinea
        End Get
        Set(ByVal value As String)
            _OrdenSubSubLinea = value
        End Set
    End Property
    Public Property EstadoBit() As Boolean
        Get
            Return _estado
        End Get
        Set(ByVal value As Boolean)
            _estado = value
        End Set
    End Property
    Public Property Codigo() As String
        Get
            Return _Codigo
        End Get
        Set(ByVal value As String)
            _Codigo = value
        End Set
    End Property
    Public Property Orden() As Integer
        Get
            Return _lin_Orden
        End Get
        Set(ByVal value As Integer)
            _lin_Orden = value
        End Set
    End Property
    Public Property Longitud() As Integer
        Get
            Return _lin_Longitud
        End Get
        Set(ByVal value As Integer)
            _lin_Longitud = value
        End Set
    End Property
    Public Property Cantidad() As Integer
        Get
            Return _Cantidad
        End Get
        Set(ByVal value As Integer)
            _Cantidad = value
        End Set
    End Property

    Public Property TipoExist() As String
        Get
            Return _tipoExistencia
        End Get
        Set(ByVal value As String)
            _tipoExistencia = value
        End Set
    End Property

End Class
