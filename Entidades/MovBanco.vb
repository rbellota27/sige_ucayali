﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

''''''''''''''''''''''MIRAYA ANAMARIA, EDGAR 03/02/2010 6:52 pm
Public Class MovBanco
    'No cambiar esta clase, esta mapeada con la tabla correspondiente en la BD,
    'para agregar cosas utilice la clase MovBancoView
    Private _IdMovBanco As Integer
    Private _IdBanco As Integer
    Private _IdCuentaBancaria As Integer

    Private _mban_NroOperacion As String
    Private _mban_Observacion As String
    Private _mban_Factor As Integer
    Private _mban_Monto As Decimal

    Private _mban_FechaRegistro As DateTime
    Private _mban_FechaMov As DateTime
    Private _mban_FechaAprobacion As DateTime

    Private _mban_EstadoAprobacion As Boolean?
    Private _mban_EstadoMov As Boolean?

    Private _IdConceptoMovBanco As Integer

    Private _IdUsuario As Integer
    Private _IdSupervisor As Integer
    Private _IdTarjeta As Integer
    Private _IdPersonaRef As Integer

    Protected _IdCaja As Integer
    Protected _IdTienda As Integer

    Protected _IdPost As Integer
    Protected _IdBanco2 As Integer
    Protected _IdCuentaBancaria2 As Integer
    Private _Referencia1x As String
    Private _Referencia2x As String

    
    Public Sub New()
        Me._IdMovBanco = Nothing
        Me._IdBanco = Nothing
        Me._IdCuentaBancaria2 = Nothing
        Me._IdBanco2 = Nothing
        Me._mban_NroOperacion = Nothing
        Me._mban_Observacion = Nothing
        Me._mban_Factor = Nothing
        Me._mban_Monto = Nothing
        Me._mban_FechaRegistro = Nothing
        Me._mban_FechaMov = Nothing
        Me._mban_FechaAprobacion = Nothing
        Me._IdCuentaBancaria = Nothing
        Me._mban_EstadoAprobacion = Nothing
        Me._mban_EstadoMov = Nothing

        Me._IdConceptoMovBanco = Nothing

        Me._IdUsuario = Nothing
        Me._IdSupervisor = Nothing
        Me._IdTarjeta = Nothing
        Me._IdPersonaRef = Nothing

        Me._IdCaja = Nothing
        Me._IdTienda = Nothing

        Me._IdPost = Nothing
        Me._Referencia1x = Nothing
        Me._Referencia2x = Nothing
    End Sub

    Public Sub New( _
        ByVal IdMovBanco As Integer, _
        ByVal IdBanco As Integer, _
        ByVal IdCuentaBancaria As Integer, _
        ByVal NroOperacion As String, _
        ByVal Observacion As String, _
        ByVal Factor As Integer, _
        ByVal Monto As Decimal, _
        ByVal FechaRegistro As DateTime, _
        ByVal FechaMov As DateTime, _
        ByVal FechaAprobacion As DateTime, _
        ByVal EstadoAprobacion As Boolean, _
        ByVal EstadoMov As Boolean, _
        ByVal IdConceptoMovBanco As Integer, _
        ByVal IdUsuario As Integer, _
        ByVal IdSupervisor As Integer, _
        ByVal IdTarjeta As Integer, _
        ByVal IdPersonaRef As Integer, _
        ByVal IdCaja As Integer, _
        ByVal IdTienda As Integer, _
        ByVal IdPost As Integer, _
        ByVal IdBanco2 As Integer, _
        ByVal IdCuentaBancaria2 As Integer, _
        ByVal _Referencia1x As String, _
        ByVal _Referencia2x As String _
        )

        Me._IdMovBanco = IdMovBanco
        Me._IdBanco = IdBanco
        Me._IdCuentaBancaria = IdCuentaBancaria
        Me._IdBanco2 = IdBanco2
        Me._IdCuentaBancaria2 = IdCuentaBancaria2
        Me._mban_NroOperacion = NroOperacion
        Me._mban_Observacion = Observacion
        Me._mban_Factor = Factor
        Me._mban_Monto = Monto
        Me._mban_FechaRegistro = FechaRegistro
        Me._mban_FechaMov = FechaMov
        Me._mban_FechaAprobacion = FechaAprobacion

        Me._mban_EstadoAprobacion = EstadoAprobacion
        Me._mban_EstadoMov = EstadoMov

        Me._IdConceptoMovBanco = IdConceptoMovBanco

        Me._IdUsuario = IdUsuario
        Me._IdSupervisor = IdSupervisor
        Me._IdTarjeta = IdTarjeta
        Me._IdPersonaRef = IdPersonaRef
        Me._Referencia1x = Referencia1x
        Me._Referencia2x = Referencia2x
        Me._IdCaja = IdCaja
        Me._IdTienda = IdTienda

        Me._IdPost = IdPost

    End Sub
    Public Property IdCuentaBancaria2() As Integer
        Get
            Return Me._IdCuentaBancaria2
        End Get
        Set(ByVal value As Integer)
            Me._IdCuentaBancaria2 = value
        End Set
    End Property
    Public Property Referencia1x() As String
        Get
            Return _Referencia1x
        End Get
        Set(ByVal value As String)
            _Referencia1x = value
        End Set
    End Property
    Public Property Referencia2x() As String
        Get
            Return _Referencia2x
        End Get
        Set(ByVal value As String)
            _Referencia2x = value
        End Set
    End Property
    Public Property IdBanco2() As Integer
        Get
            Return Me._IdBanco2
        End Get
        Set(ByVal value As Integer)
            Me._IdBanco2 = value
        End Set
    End Property
    Public Property IdMovBanco() As Integer
        Get
            Return Me._IdMovBanco
        End Get
        Set(ByVal value As Integer)
            Me._IdMovBanco = value
        End Set
    End Property
    Public Property IdBanco() As Integer
        Get
            Return Me._IdBanco
        End Get
        Set(ByVal value As Integer)
            Me._IdBanco = value
        End Set
    End Property
    Public Property IdCuentaBancaria() As Integer
        Get
            Return Me._IdCuentaBancaria
        End Get
        Set(ByVal value As Integer)
            Me._IdCuentaBancaria = value
        End Set
    End Property
    Public Property NroOperacion() As String
        Get
            Return Me._mban_NroOperacion
        End Get
        Set(ByVal value As String)
            Me._mban_NroOperacion = value
        End Set
    End Property
    Public Property Observacion() As String
        Get
            Return Me._mban_Observacion
        End Get
        Set(ByVal value As String)
            Me._mban_Observacion = value
        End Set
    End Property
    Public Property Factor() As Integer
        Get
            Return Me._mban_Factor
        End Get
        Set(ByVal value As Integer)
            Me._mban_Factor = value
        End Set
    End Property
    Public Property Monto() As Decimal
        Get
            Return Me._mban_Monto
        End Get
        Set(ByVal value As Decimal)
            Me._mban_Monto = value
        End Set
    End Property
    Public Property DescMonto() As String  'Retorna el número con 2 decimales
        Get
            Return Me._mban_Monto.ToString("F")
        End Get
        Set(ByVal value As String)
            Me._mban_Monto = value
        End Set

        'Get
        '    Return Me._mban_Monto.ToString("F")
        'End Get
    End Property
    Public Property FechaRegistro() As DateTime
        Get
            Return Me._mban_FechaRegistro
        End Get
        Set(ByVal value As DateTime)
            Me._mban_FechaRegistro = value
        End Set
    End Property
    Public Property FechaMov() As DateTime
        Get
            Return Me._mban_FechaMov
        End Get
        Set(ByVal value As DateTime)
            Me._mban_FechaMov = value
        End Set
    End Property
    Public Property FechaAprobacion() As DateTime
        Get
            Return Me._mban_FechaAprobacion
        End Get
        Set(ByVal value As DateTime)
            Me._mban_FechaAprobacion = value
        End Set
    End Property

    Public Property DescFechaRegistro() As Date
        Get
            Return Me._mban_FechaRegistro
        End Get
        Set(ByVal value As Date)
            Me._mban_FechaRegistro = value
        End Set

        'Get
        '    If Me._mban_FechaRegistro = Nothing Then
        '        Return ""
        '    Else
        '        Return Format(Me._mban_FechaRegistro, "dd/MM/yyyy")
        '    End If
        'End Get
    End Property
    Public Property DescFechaMov() As Date

        Get
            Return Me._mban_FechaMov
        End Get
        Set(ByVal value As Date)
            Me._mban_FechaMov = value
        End Set

        'Get
        '    If Me._mban_FechaMov = Nothing Then
        '        Return ""
        '    Else
        '        Return Format(Me._mban_FechaMov, "dd/MM/yyyy")
        '    End If
        'End Get
    End Property
    Public Property DescFechaAprobacion() As Date

        Get
            Return Me._mban_FechaAprobacion
        End Get
        Set(ByVal value As Date)
            Me._mban_FechaAprobacion = value
        End Set


        'Get
        '    If Me._mban_FechaAprobacion = Nothing Then
        '        Return ""
        '    Else
        '        Return Format(Me._mban_FechaAprobacion, "dd/MM/yyyy")
        '    End If
        'End Get
    End Property

    Public Property EstadoAprobacion() As Boolean
        Get
            If Me._mban_EstadoAprobacion.HasValue = False Then
                Me._mban_EstadoAprobacion = False
            End If
            Return Me._mban_EstadoAprobacion
        End Get
        Set(ByVal value As Boolean)
            Me._mban_EstadoAprobacion = value
        End Set
    End Property
    Public Property EstadoAprobacionNullable() As Boolean?
        Get
            Return Me._mban_EstadoAprobacion
        End Get
        Set(ByVal value As Boolean?)
            Me._mban_EstadoAprobacion = value
        End Set
    End Property
    Public ReadOnly Property DescEstadoAprobacion() As String
        Get
            If Me._mban_EstadoAprobacion Then
                Return "Activo"
            Else
                Return "Inactivo"
            End If
        End Get
    End Property
    Public Property EstadoMov() As Boolean
        Get
            If Me._mban_EstadoMov.HasValue = False Then
                Me._mban_EstadoMov = False
            End If
            Return Me._mban_EstadoMov
        End Get
        Set(ByVal value As Boolean)
            Me._mban_EstadoMov = value
        End Set
    End Property
    Public Property EstadoMovNullable() As Boolean?
        Get
            Return Me._mban_EstadoMov
        End Get
        Set(ByVal value As Boolean?)
            Me._mban_EstadoMov = value
        End Set
    End Property
    Public ReadOnly Property DescEstadoMov() As String
        Get
            If Me._mban_EstadoMov Then
                Return "Activo"
            Else
                Return "Inactivo"
            End If
        End Get
    End Property

    Public Property IdConceptoMovBanco() As Integer
        Get
            Return Me._IdConceptoMovBanco
        End Get
        Set(ByVal value As Integer)
            Me._IdConceptoMovBanco = value
        End Set
    End Property

    Public Property IdUsuario() As Integer
        Get
            Return Me._IdUsuario
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuario = value
        End Set
    End Property

    Public Property IdSupervisor() As Integer
        Get
            Return Me._IdSupervisor
        End Get
        Set(ByVal value As Integer)
            Me._IdSupervisor = value
        End Set
    End Property

    Public Property IdTarjeta() As Integer
        Get
            Return Me._IdTarjeta
        End Get
        Set(ByVal value As Integer)
            Me._IdTarjeta = value
        End Set
    End Property
    Public Property IdPersonaRef() As Integer
        Get
            Return Me._IdPersonaRef
        End Get
        Set(ByVal value As Integer)
            Me._IdPersonaRef = value
        End Set
    End Property
    Public Property IdCaja() As Integer
        Get
            Return Me._IdCaja
        End Get
        Set(ByVal value As Integer)
            Me._IdCaja = value
        End Set
    End Property
    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property
    Public Property IdPost() As Integer
        Get
            Return Me._IdPost
        End Get
        Set(ByVal value As Integer)
            Me._IdPost = value
        End Set
    End Property

End Class
