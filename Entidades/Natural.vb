'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.


'********************   JUEVES 10 JUNIO 2010 HORA 12_46 PM

Public Class Natural
    Private _nat_Apepat As String
    Private _nat_Apemat As String
    Private _nat_Nombres As String
    Private _IdPersona As Integer
    Private _IdMadre As Integer
    Private _IdPadre As Integer
    Private _nat_UbigeoNac As String
    Private _IdConyuge As Integer
    Private _nat_FechaNac As Date
    Private _nat_FechaMat As Date
    Private _IdCargo As Integer
    Private _IdEstadoCivil As Integer
    Private _IdCondTrabajo As Integer
    Private _dni As String
    Private _correo As String
    Private _telefono As String
    Private _Sexo As String

    Public Property Sexo() As String
        Get
            Return Me._Sexo
        End Get
        Set(ByVal value As String)
            Me._Sexo = value
        End Set
    End Property


    Public Property telefono() As String
        Get
            Return _telefono
        End Get
        Set(ByVal value As String)
            _telefono = value
        End Set
    End Property

    Public Property correo() As String
        Get
            Return Me._correo
        End Get
        Set(ByVal value As String)
            Me._correo = value
        End Set
    End Property


    Public Property DNI() As String
        Get
            Return Me._dni
        End Get
        Set(ByVal value As String)
            Me._dni = value
        End Set
    End Property

    Public Property ApellidoPaterno() As String
        Get
            Return Me._nat_Apepat
        End Get
        Set(ByVal value As String)
            Me._nat_Apepat = value
        End Set
    End Property

    Public Property ApellidoMaterno() As String
        Get
            Return Me._nat_Apemat
        End Get
        Set(ByVal value As String)
            Me._nat_Apemat = value
        End Set
    End Property

    Public Property Nombres() As String
        Get
            Return Me._nat_Nombres
        End Get
        Set(ByVal value As String)
            Me._nat_Nombres = value
        End Set
    End Property

    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Public Property IdMadre() As Integer
        Get
            Return Me._IdMadre
        End Get
        Set(ByVal value As Integer)
            Me._IdMadre = value
        End Set
    End Property

    Public Property IdPadre() As Integer
        Get
            Return Me._IdPadre
        End Get
        Set(ByVal value As Integer)
            Me._IdPadre = value
        End Set
    End Property

    Public Property UbigeoNac() As String
        Get
            Return Me._nat_UbigeoNac
        End Get
        Set(ByVal value As String)
            Me._nat_UbigeoNac = value
        End Set
    End Property

    Public Property IdConyuge() As Integer
        Get
            Return Me._IdConyuge
        End Get
        Set(ByVal value As Integer)
            Me._IdConyuge = value
        End Set
    End Property

    Public Property FechaNac() As Date
        Get
            Return Me._nat_FechaNac
        End Get
        Set(ByVal value As Date)
            Me._nat_FechaNac = value
        End Set
    End Property

    Public Property FechaMat() As Date
        Get
            Return Me._nat_FechaMat
        End Get
        Set(ByVal value As Date)
            Me._nat_FechaMat = value
        End Set
    End Property

    Public Property IdCargo() As Integer
        Get
            Return Me._IdCargo
        End Get
        Set(ByVal value As Integer)
            Me._IdCargo = value
        End Set
    End Property

    Public Property IdEstadoCivil() As Integer
        Get
            Return Me._IdEstadoCivil
        End Get
        Set(ByVal value As Integer)
            Me._IdEstadoCivil = value
        End Set
    End Property

    Public Property IdCondTrabajo() As Integer
        Get
            Return Me._IdCondTrabajo
        End Get
        Set(ByVal value As Integer)
            Me._IdCondTrabajo = value
        End Set
    End Property
End Class
