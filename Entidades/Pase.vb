﻿Public Class Pase
    Private _IdPase As Integer
    Private _CodigoPase As String
    Private _FechaInicio As Date
    Private _FechaFin As Date
    Private _FechaRegistro As Date
    Private _DescripcionPersona As String
    Private _IdUsuario As Integer
    Public Property IdPase() As Integer
        Get
            Return Me._IdPase
        End Get
        Set(ByVal value As Integer)
            Me._IdPase = value
        End Set
    End Property
    Public Property IdUsuario() As Integer
        Get
            Return Me._IdUsuario
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuario = value
        End Set
    End Property
    Public Property CodigoPase() As String
        Get
            Return Me._CodigoPase
        End Get
        Set(ByVal value As String)
            Me._CodigoPase = value
        End Set
    End Property
    Public Property FechaInicio() As Date
        Get
            Return Me._FechaInicio
        End Get
        Set(ByVal value As Date)
            Me._FechaInicio = value
        End Set
    End Property
    Public Property FechaFin() As Date
        Get
            Return Me._FechaFin
        End Get
        Set(ByVal value As Date)
            Me._FechaFin = value
        End Set
    End Property
    Public Property FechaRegistro() As Date
        Get
            Return Me._FechaRegistro
        End Get
        Set(ByVal value As Date)
            Me._FechaRegistro = value
        End Set
    End Property
    Public Property DescripcionPersona() As String
        Get
            Return Me._DescripcionPersona
        End Get
        Set(ByVal value As String)
            Me._DescripcionPersona = value
        End Set
    End Property
End Class
