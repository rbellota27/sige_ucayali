﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Cliente
    Private _IdPersona As Integer
    Private _nom_Nombre As String
    Private _mcu_Saldo As Decimal
    Private _IdMoneda As Integer
    Private _IdTipoDocumento As Integer
    Private _mcu_Monto As Decimal
    Private _doc_FechaVenc As Date
    Private _doc_FechaEmision As Date
    Private _IdDocumento As Integer
    Private _DiasTrans As String
    Private _FechaServidor As Date
    Private _IdMovCuenta As Integer
    Private _tdoc_Nombre As String
    Private _doc_Serie As String
    Private _tdoc_NombreCorto As String
    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property
    Public Property IdMovCuenta() As Integer
        Get
            Return Me._IdMovCuenta
        End Get
        Set(ByVal value As Integer)
            Me._IdMovCuenta = value
        End Set
    End Property
    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property
    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property
    Public Property IdTipoDocumento() As Integer
        Get
            Return Me._IdTipoDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoDocumento = value
        End Set
    End Property

    Public Property Nom_Nombre() As String
        Get
            Return Me._nom_Nombre
        End Get
        Set(ByVal value As String)
            Me._nom_Nombre = value
        End Set
    End Property
    Public Property Tdoc_Nombre() As String
        Get
            Return Me._tdoc_Nombre
        End Get
        Set(ByVal value As String)
            Me._tdoc_Nombre = value
        End Set
    End Property
    Public Property Doc_FechaVenc() As Date
        Get
            Return Me._doc_FechaVenc
        End Get
        Set(ByVal value As Date)
            Me._doc_FechaVenc = value
        End Set
    End Property
    Public Property Doc_FechaEmision() As Date
        Get
            Return Me._doc_FechaEmision
        End Get
        Set(ByVal value As Date)
            Me._doc_FechaEmision = value
        End Set
    End Property
    Public Property FechaServidor() As Date
        Get
            Return Me._FechaServidor
        End Get
        Set(ByVal value As Date)
            Me._FechaServidor = value
        End Set
    End Property
    Public Property DiasTrans() As String
        Get
            Return Me._DiasTrans
        End Get
        Set(ByVal value As String)
            Me._DiasTrans = value
        End Set
    End Property
    Public Property Mcu_Monto() As Decimal
        Get
            Return Me._mcu_Monto
        End Get
        Set(ByVal value As Decimal)
            Me._mcu_Monto = value
        End Set
    End Property
    Public Property Mcu_Saldo() As Decimal
        Get
            Return Me._mcu_Saldo
        End Get
        Set(ByVal value As Decimal)
            Me._mcu_Saldo = value
        End Set
    End Property
    Public Property Doc_Serie() As String
        Get
            Return Me._doc_Serie
        End Get
        Set(ByVal value As String)
            Me._doc_Serie = value
        End Set
    End Property
    Public Property Tdoc_NombreCorto() As String
        Get
            Return Me._tdoc_NombreCorto
        End Get
        Set(ByVal value As String)
            Me._tdoc_NombreCorto = value
        End Set
    End Property
End Class
