﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************************************************
'Autor   : SALAZAR RODRIGUEZ LUIS
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 14-Enero-2010
'Hora    : 03:27 pm
'*************************************************
Public Class DocImportacion
    Inherits Anexo_Documento
    Private _IdDocumento As Integer
    Private _IdImportacion As String
    Private _ImpSerie As Integer
    Private _ImpCorrelativoSerie As Integer
    Private _NroOrdenCompra As String
    Private _ImpIdProveedor As Integer
    Private _ImpRazonSocial As String
    Private _ImpRuc As String
    Private _ImpDireccion As String
    Private _ImpIdProducto As Integer
    Private _ImpNomProducto As String
    Private _ImpIdUM As Integer
    Private _ImpNomUM As String
    Private _ImpCantidad As Decimal
    Private _ImpPeso As Decimal
    Private _ImpCosto As Decimal
    Private _ImpProrrateoPeso As Decimal
    Private _ImpProrrateoMonto As Decimal
    Private _ImpTotalGasto As Decimal
    Private _ImpIdTipoDoc As Integer
    Private _ImpNomTipoDoc As String
    Private _ImpFechaEmision As Date
    Private _ImpIdConcepto As Integer
    Private _ImpNomConcepto As String
    Private _ImpMonSimbolo As String
    Private _ImpCalculo As String
    Private _ImpSubTotalGasto As Decimal
    Private _ImpNroDoc As String
    Private _ImpIdMoneda As Integer
    Private _IdDetalleConcepto As Integer
    Private _NroDocumento As String
    Private _IdDocumento2 As Integer
    Private _objTipoDocumento As Object
    Private _NomEmpresa As String
    Private _NomTienda As String
    Private _FechaEmisionOC As Date
    Private _strTipoDocumentoRef As String

    Public Sub New()
    End Sub
    Public Sub New(ByVal idtipodoc As Integer, ByVal nomtipodoc As String)
        _ImpIdTipoDoc = idtipodoc
        _ImpNomTipoDoc = nomtipodoc
    End Sub

    Public Property strTipoDocumentoRef() As String
        Get
            Return _strTipoDocumentoRef
        End Get
        Set(ByVal value As String)
            _strTipoDocumentoRef = value
        End Set
    End Property

    Public ReadOnly Property getTipoDocumentoRef() As List(Of Entidades.TipoDocumento)
        Get
            If strTipoDocumentoRef <> "" Then
                Dim lista As New List(Of Entidades.TipoDocumento)
                Dim TipoDocumentoRef() As String
                TipoDocumentoRef = strTipoDocumentoRef.Split(CChar(","))
                For x As Integer = 0 To TipoDocumentoRef.Length - 1
                    If TipoDocumentoRef(x) <> "" Then
                        lista.Add(New Entidades.TipoDocumento(0, TipoDocumentoRef(x)))
                    End If
                Next
                Return lista
            End If
            Return Nothing
        End Get
    End Property

    
    Public Property IdDocumento() As Integer
        Get
            Return _IdDocumento
        End Get
        Set(ByVal value As Integer)
            _IdDocumento = value
        End Set
    End Property

    Public Property objTipoDocumento() As Object
        Get
            Return _objTipoDocumento
        End Get
        Set(ByVal value As Object)
            _objTipoDocumento = value
        End Set
    End Property

    Public Property IdImportacion() As String
        Get
            Return _IdImportacion
        End Get
        Set(ByVal value As String)
            _IdImportacion = value
        End Set
    End Property
    Public Property Serie() As Integer
        Get
            Return _ImpSerie
        End Get
        Set(ByVal value As Integer)
            _ImpSerie = value
        End Set
    End Property
    Public Property CorrelativoSerie() As Integer
        Get
            Return _ImpCorrelativoSerie
        End Get
        Set(ByVal value As Integer)
            _ImpCorrelativoSerie = value
        End Set
    End Property
    Public Property NroOrdenCompra() As String
        Get
            Return _NroOrdenCompra
        End Get
        Set(ByVal value As String)
            _NroOrdenCompra = value
        End Set
    End Property
    Public Property IdProveedor() As Integer
        Get
            Return _ImpIdProveedor
        End Get
        Set(ByVal value As Integer)
            _ImpIdProveedor = value
        End Set
    End Property
    Public Property RazonSocial() As String
        Get
            Return Me._ImpRazonSocial
        End Get
        Set(ByVal value As String)
            Me._ImpRazonSocial = value
        End Set
    End Property
    Public Property Ruc() As String
        Get
            Return _ImpRuc
        End Get
        Set(ByVal value As String)
            _ImpRuc = value
        End Set
    End Property
    Public Property Direccion() As String
        Get
            Return _ImpDireccion
        End Get
        Set(ByVal value As String)
            _ImpDireccion = value
        End Set
    End Property
    Public Property IdProducto() As Integer
        Get
            Return _ImpIdProducto
        End Get
        Set(ByVal value As Integer)
            _ImpIdProducto = value
        End Set
    End Property
    Public Property NomProducto() As String
        Get
            Return _ImpNomProducto
        End Get
        Set(ByVal value As String)
            _ImpNomProducto = value
        End Set
    End Property
    Public Property IdUM() As Integer
        Get
            Return _ImpIdUM
        End Get
        Set(ByVal value As Integer)
            _ImpIdUM = value
        End Set
    End Property
    Public Property NomUM() As String
        Get
            Return _ImpNomUM
        End Get
        Set(ByVal value As String)
            _ImpNomUM = value
        End Set
    End Property
    Public Property Cantidad() As Decimal
        Get
            Return _ImpCantidad
        End Get
        Set(ByVal value As Decimal)
            _ImpCantidad = value
        End Set
    End Property
    Public Property Peso() As Decimal
        Get
            Return _ImpPeso
        End Get
        Set(ByVal value As Decimal)
            _ImpPeso = value
        End Set
    End Property
    Public Property Costo() As Decimal
        Get
            Return _ImpCosto
        End Get
        Set(ByVal value As Decimal)
            _ImpCosto = value
        End Set
    End Property
    Public Property ProrrateoPeso() As Decimal
        Get
            Return _ImpProrrateoPeso
        End Get
        Set(ByVal value As Decimal)
            _ImpProrrateoPeso = value
        End Set
    End Property
    Public Property ProrrateoMonto() As Decimal
        Get
            Return _ImpProrrateoMonto
        End Get
        Set(ByVal value As Decimal)
            _ImpProrrateoMonto = value
        End Set
    End Property
    Public Property TotalGasto() As Decimal
        Get
            Return _ImpTotalGasto
        End Get
        Set(ByVal value As Decimal)
            _ImpTotalGasto = value
        End Set
    End Property
    Public Property IdTipoDocumento() As Integer
        Get
            Return _ImpIdTipoDoc
        End Get
        Set(ByVal value As Integer)
            _ImpIdTipoDoc = value
        End Set
    End Property
    Public Property NomTipoDoc() As String
        Get
            Return _ImpNomTipoDoc
        End Get
        Set(ByVal value As String)
            _ImpNomTipoDoc = value
        End Set
    End Property
    Public Property FechaEmision() As Date
        Get
            Return _ImpFechaEmision
        End Get
        Set(ByVal value As Date)
            _ImpFechaEmision = value
        End Set
    End Property
    Public Property IdConcepto() As Integer
        Get
            Return _ImpIdConcepto
        End Get
        Set(ByVal value As Integer)
            _ImpIdConcepto = value
        End Set
    End Property
    Public Property NomConcepto() As String
        Get
            Return _ImpNomConcepto
        End Get
        Set(ByVal value As String)
            _ImpNomConcepto = value
        End Set
    End Property
    Public Property IdMoneda() As Integer
        Get
            Return _ImpIdMoneda
        End Get
        Set(ByVal value As Integer)
            _ImpIdMoneda = value
        End Set
    End Property
    Public Property MonSimbolo() As String
        Get
            Return _ImpMonSimbolo
        End Get
        Set(ByVal value As String)
            _ImpMonSimbolo = value
        End Set
    End Property
    Public Property NomCalculo() As String
        Get
            Return _ImpCalculo
        End Get
        Set(ByVal value As String)
            _ImpCalculo = value
        End Set
    End Property
    Public Property SubTotalGasto() As Decimal
        Get
            Return _ImpSubTotalGasto
        End Get
        Set(ByVal value As Decimal)
            _ImpSubTotalGasto = value
        End Set
    End Property
    Public Property NroDoc() As String
        Get
            Return _ImpNroDoc
        End Get
        Set(ByVal value As String)
            _ImpNroDoc = value
        End Set
    End Property

    Private _doc_Codigo As String
    Private _doc_Serie As String
    Private _IdTienda As Integer
    Private _IdEmpresa As Integer
    Private _doc_FechaRegistro As Date
    Private _doc_FechaSalida As Date
    Private _doc_FechaLLegada As Date
    Private _doc_FechaIngMilla As Date
    Private _ImpTotal_Imp As Decimal
    Private _ImpTotalOC As Decimal
    Private _ImpPesoTotal As Decimal
    Private _IdUsuario As Integer
    Private _IdTipoOperacion As Integer
    Sub New(ByVal IdDocumento As Integer, ByVal doc_Codigo As String, ByVal doc_Serie As String, ByVal IdSerie As Integer, _
        ByVal IdTienda As Integer, ByVal IdEmpresa As Integer, ByVal doc_FechaEmision As Date, ByVal doc_FechaRegistro As Date, _
        ByVal doc_FechaSalida As Date, ByVal doc_FechaLLegada As Date, ByVal doc_FechaIngMilla As Date, ByVal ImpTotal_Imp As Decimal, _
        ByVal ImpTotalOC As Decimal, ByVal ImpPesoTotal As Decimal, ByVal IdUsuario As Integer, _
        ByVal IdMoneda As Integer, ByVal IdTipoDocumento As Integer, ByVal IdTipoOperacion As Integer)
        _IdDocumento = IdDocumento
        _doc_Codigo = doc_Codigo
        _doc_Serie = doc_Serie
        _ImpSerie = IdSerie
        _IdTienda = IdTienda
        _IdEmpresa = IdEmpresa
        _ImpFechaEmision = doc_FechaEmision
        _doc_FechaRegistro = doc_FechaRegistro
        _doc_FechaSalida = doc_FechaSalida
        _doc_FechaLLegada = doc_FechaLLegada
        _doc_FechaIngMilla = doc_FechaIngMilla
        _ImpTotal_Imp = ImpTotal_Imp
        _ImpTotalOC = ImpTotalOC
        _ImpPesoTotal = ImpPesoTotal
        _IdUsuario = IdUsuario
        _ImpIdMoneda = IdMoneda
        _ImpIdTipoDoc = IdTipoDocumento
        _IdTipoOperacion = IdTipoOperacion
    End Sub

    Public Property doc_Codigo() As String
        Get
            Return _doc_Codigo
        End Get
        Set(ByVal value As String)
            _doc_Codigo = value
        End Set
    End Property
    Public Property doc_Serie() As String
        Get
            Return _doc_Serie
        End Get
        Set(ByVal value As String)
            _doc_Serie = value
        End Set
    End Property
    Public Property IdTienda() As Integer
        Get
            Return _IdTienda
        End Get
        Set(ByVal value As Integer)
            _IdTienda = value
        End Set
    End Property
    Public Property IdEmpresa() As Integer
        Get
            Return _IdEmpresa
        End Get
        Set(ByVal value As Integer)
            _IdEmpresa = value
        End Set
    End Property
    Public Property doc_FechaRegistro() As Date
        Get
            Return _doc_FechaRegistro
        End Get
        Set(ByVal value As Date)
            _doc_FechaRegistro = value
        End Set
    End Property
    Public Property doc_FechaSalida() As Date
        Get
            Return _doc_FechaSalida
        End Get
        Set(ByVal value As Date)
            _doc_FechaSalida = value
        End Set
    End Property
    Public Property doc_FechaLLegada() As Date
        Get
            Return _doc_FechaLLegada
        End Get
        Set(ByVal value As Date)
            _doc_FechaLLegada = value
        End Set
    End Property
    Public Property doc_FechaIngMilla() As Date
        Get
            Return _doc_FechaIngMilla
        End Get
        Set(ByVal value As Date)
            _doc_FechaIngMilla = value
        End Set
    End Property
    Public Property ImpTotal_Imp() As Decimal
        Get
            Return _ImpTotal_Imp
        End Get
        Set(ByVal value As Decimal)
            _ImpTotal_Imp = value
        End Set
    End Property
    Public Property ImpTotalOC() As Decimal
        Get
            Return _ImpTotalOC
        End Get
        Set(ByVal value As Decimal)
            _ImpTotalOC = value
        End Set
    End Property
    Public Property ImpPesoTotal() As Decimal
        Get
            Return _ImpPesoTotal
        End Get
        Set(ByVal value As Decimal)
            _ImpPesoTotal = value
        End Set
    End Property
    Public Property IdUsuario() As Integer
        Get
            Return _IdUsuario
        End Get
        Set(ByVal value As Integer)
            _IdUsuario = value
        End Set
    End Property
    Public Property IdTipoOperacion() As Integer
        Get
            Return _IdTipoOperacion
        End Get
        Set(ByVal value As Integer)
            _IdTipoOperacion = value
        End Set
    End Property

    Public Property IdDetalleConcepto() As Integer
        Get
            Return _IdDetalleConcepto
        End Get
        Set(ByVal value As Integer)
            _IdDetalleConcepto = value
        End Set
    End Property
    Public Property NroDocumento() As String
        Get
            Return _NroDocumento
        End Get
        Set(ByVal value As String)
            _NroDocumento = value
        End Set
    End Property
    Public Property IdDocumento2() As Integer
        Get
            Return _IdDocumento2
        End Get
        Set(ByVal value As Integer)
            _IdDocumento2 = value
        End Set
    End Property
    Public Property NomEmpresa() As String
        Get
            Return _NomEmpresa
        End Get
        Set(ByVal value As String)
            _NomEmpresa = value
        End Set
    End Property
    Public Property NomTienda() As String
        Get
            Return _NomTienda
        End Get
        Set(ByVal value As String)
            _NomTienda = value
        End Set
    End Property
    Public Property FechaEmisionOC() As Date
        Get
            Return _FechaEmisionOC
        End Get
        Set(ByVal value As Date)
            _FechaEmisionOC = value
        End Set
    End Property
End Class
