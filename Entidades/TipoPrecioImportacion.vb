﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class TipoPrecioImportacion
    Private _IdTPImp As Integer
    Private _Nombre_TPImp As String
    Private _Nombre_CortoTPImp As String
    Private _Estado_TPImp As Boolean
    Public Sub New()
    End Sub
    Public Sub New(ByVal id As Integer, ByVal nomcorto As String, ByVal estado As Boolean)
        _IdTPImp = id
        _Nombre_CortoTPImp = nomcorto
        _Estado_TPImp = estado
    End Sub
    Public Property Id() As Integer
        Get
            Return _IdTPImp
        End Get
        Set(ByVal value As Integer)
            _IdTPImp = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return _Nombre_TPImp
        End Get
        Set(ByVal value As String)
            _Nombre_TPImp = value
        End Set
    End Property
    Public Property NomCorto() As String
        Get
            Return _Nombre_CortoTPImp
        End Get
        Set(ByVal value As String)
            _Nombre_CortoTPImp = value
        End Set
    End Property
    Public Property Estado() As Boolean
        Get
            Return _Estado_TPImp
        End Get
        Set(ByVal value As Boolean)
            _Estado_TPImp = value
        End Set
    End Property
    Public ReadOnly Property DescEstado() As String
        Get
            If Me._Estado_TPImp Then
                Return "Activo"
            Else
                Return "Inactivo"
            End If
        End Get
    End Property
End Class
