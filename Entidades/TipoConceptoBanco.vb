﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'03/02/2010 10:31 am    modificado new() y el estado
Public Class TipoConceptoBanco
    Private _IdTipoConceptoBanco As Integer
    Private _tcba_Descripcion As String
    Private _tcba_Factor As Integer
    Private _tcba_Estado As Nullable(Of Boolean)

    Public Sub New()
        Me.Id = 0
        Me.Descripcion = ""
        Me.Factor = 0
        Me.Estado = Nothing
    End Sub

    Public Sub New(ByVal Id As Integer, ByVal Descripcion As String, ByVal Factor As Integer, ByVal Estado As Boolean)
        Me.Id = Id
        Me.Descripcion = Descripcion
        Me.Factor = Factor
        Me.Estado = Estado
    End Sub

    Public Property Id() As Integer
        Get
            Return Me._IdTipoConceptoBanco
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoConceptoBanco = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return Me._tcba_Descripcion
        End Get
        Set(ByVal value As String)
            Me._tcba_Descripcion = value
        End Set
    End Property
    Public Property Descripcion() As String
        Get
            Return Me._tcba_Descripcion
        End Get
        Set(ByVal value As String)
            Me._tcba_Descripcion = value
        End Set
    End Property
    Public Property Factor() As Integer
        Get
            Return Me._tcba_Factor
        End Get
        Set(ByVal value As Integer)
            Me._tcba_Factor = value
        End Set
    End Property
    Public Property Estado() As Boolean?
        Get
            Return Me._tcba_Estado
        End Get
        Set(ByVal value As Boolean?)
            Me._tcba_Estado = value
        End Set
    End Property
    Public ReadOnly Property DescEstado() As String
        Get
            If Me.Estado = True Then
                Return "Activo"
            Else
                Return "Inactivo"
            End If
        End Get
    End Property
    Public ReadOnly Property EstadoNumStr() As String
        Get
            If Me.Estado = True Then
                Return "1"
            Else
                Return "0"
            End If
        End Get
    End Property
End Class
