﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class FormaProducto
    Private _IdFormaProducto As Integer
    Private _fp_Nombre As String
    Private _fp_Abv As String
    Private _fp_Estado As String
    Private _descEstado As String

    Public Property Id() As Integer
        Get
            Return Me._IdFormaProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdFormaProducto = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._fp_Nombre
        End Get
        Set(ByVal value As String)
            Me._fp_Nombre = value
        End Set
    End Property

    Public Property Abv() As String
        Get
            Return Me._fp_Abv
        End Get
        Set(ByVal value As String)
            Me._fp_Abv = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._fp_Estado
        End Get
        Set(ByVal value As String)
            Me._fp_Estado = value
        End Set
    End Property
    Public ReadOnly Property DescEstado()
        Get
            Me._descEstado = "Activo"
            If Me._fp_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
End Class
