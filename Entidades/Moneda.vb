'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Moneda
    Private _IdMoneda As Integer
    Private _mon_CodigoSunat As String
    Private _mon_Nombre As String
    Private _mon_Simbolo As String
    Private _mon_Base As Boolean
    Private _mon_Estado As String
    Private _descEstado As String
    Public ReadOnly Property DescEstado() As String
        Get
            Me._descEstado = "Activo"
            If Me._mon_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
    Public Sub New()
    End Sub
    Public Sub New(ByVal idmoneda As Integer, ByVal nombre As String, ByVal simbolo As String)
        Me.Id = idmoneda
        Me.Descripcion = nombre
        Me.Simbolo = simbolo
    End Sub
    Public ReadOnly Property DescMonedaBase() As String
        Get
            If _mon_Base Then
                Return "S�"
            End If
            Return "No"
        End Get
    End Property
    Public Property Id() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property

    Public Property CodigoSunat() As String
        Get
            Return Me._mon_CodigoSunat
        End Get
        Set(ByVal value As String)
            Me._mon_CodigoSunat = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._mon_Nombre
        End Get
        Set(ByVal value As String)
            Me._mon_Nombre = value
        End Set
    End Property

    Public Property Simbolo() As String
        Get
            Return Me._mon_Simbolo
        End Get
        Set(ByVal value As String)
            Me._mon_Simbolo = value
        End Set
    End Property

    Public Property Base() As Boolean
        Get
            Return Me._mon_Base
        End Get
        Set(ByVal value As Boolean)
            Me._mon_Base = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._mon_Estado
        End Get
        Set(ByVal value As String)
            Me._mon_Estado = value
        End Set
    End Property
End Class
