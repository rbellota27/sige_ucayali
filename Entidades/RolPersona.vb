'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class RolPersona
    Private _IdPersona As Integer
    Private _IdUsuario As Integer
    Private _IdEmpresa As Integer
    Private _IdRol As Integer
    Private _rp_FechaAlta As Date
    Private _rp_FechaBaja As Date
    Private _rp_Estado As Boolean
    Private _IdMotivoBaja As Integer
    Private _Descripcion As String

    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property

    Public Property FechaAlta() As Date
        Get
            Return Me._rp_FechaAlta
        End Get
        Set(ByVal value As Date)
            Me._rp_FechaAlta = value
        End Set
    End Property

    Public Property FechaBaja() As Date
        Get
            Return Me._rp_FechaBaja
        End Get
        Set(ByVal value As Date)
            Me._rp_FechaBaja = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return Me._rp_Estado
        End Get
        Set(ByVal value As Boolean)
            Me._rp_Estado = value
        End Set
    End Property

    Public Property IdMotivoBaja() As Integer
        Get
            Return Me._IdMotivoBaja
        End Get
        Set(ByVal value As Integer)
            Me._IdMotivoBaja = value
        End Set
    End Property

    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Public Property IdUsuario() As Integer
        Get
            Return Me._IdUsuario
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuario = value
        End Set
    End Property

    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property

    Public Property IdRol() As Integer
        Get
            Return Me._IdRol
        End Get
        Set(ByVal value As Integer)
            Me._IdRol = value
        End Set
    End Property

    Private _objEmpresa As Object
    Public Property objEmpresa() As Object
        Get
            Return _objEmpresa
        End Get
        Set(ByVal value As Object)
            _objEmpresa = value
        End Set
    End Property

    Private _objRol As Object
    Public Property objRol() As Object
        Get
            Return _objRol
        End Get
        Set(ByVal value As Object)
            _objRol = value
        End Set
    End Property

    Private _objMotivoBaja As Object
    Public Property objMotivoBaja() As Object
        Get
            Return _objMotivoBaja
        End Get
        Set(ByVal value As Object)
            _objMotivoBaja = value
        End Set
    End Property

End Class
