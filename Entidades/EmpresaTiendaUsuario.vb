﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class EmpresaTiendaUsuario
    Private _IdEmpresa As Integer
    Private _IdTienda As Integer
    Private _IdUsuario As Integer
    Private _Principal As Boolean
    Private _Estado As Boolean
    Private _DescEmpresa As String
    Private _DescTienda As String

    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property IdUsuario() As Integer
        Get
            Return Me._IdUsuario
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuario = value
        End Set
    End Property

    Public Property Principal() As Boolean
        Get
            Return Me._Principal
        End Get
        Set(ByVal value As Boolean)
            Me._Principal = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return Me._Estado
        End Get
        Set(ByVal value As Boolean)
            Me._Estado = value
        End Set
    End Property

    Public Property DescEmpresa() As String
        Get
            Return Me._DescEmpresa
        End Get
        Set(ByVal value As String)
            Me._DescEmpresa = value
        End Set
    End Property

    Public Property DescTienda() As String
        Get
            Return Me._DescTienda
        End Get
        Set(ByVal value As String)
            Me._DescTienda = value
        End Set
    End Property
End Class
