﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'**********************   VIERNES 25 06 2010

Public Class ValorReferencia_Transporte
    Private _IdValorReferenciaT As Integer
    Private _IdUbigeoOrigen As Integer
    Private _IdUbigeoDestino As Integer
    Private _Descripcion As String
    Private _IdMoneda As Integer
    Private _Monto As Decimal
    Private _IdUsuarioInsert As Integer
    Private _FechaInsert As Date
    Private _IdUsuarioUpdate As Integer
    Private _FechaUpdate As Date
    Private _Estado As Boolean
    Private _Moneda As String
    Private _CodDpto_Origen As String
    Private _CodProv_Origen As String
    Private _CodDist_Origen As String
    Private _Origen As String
    Private _CodDpto_Destino As String
    Private _CodProv_Destino As String
    Private _CodDist_Destino As String
    Private _Destino As String

    Public Property IdValorReferenciaT() As Integer
        Get
            Return Me._IdValorReferenciaT
        End Get
        Set(ByVal value As Integer)
            Me._IdValorReferenciaT = value
        End Set
    End Property

    Public Property IdUbigeoOrigen() As Integer
        Get
            Return Me._IdUbigeoOrigen
        End Get
        Set(ByVal value As Integer)
            Me._IdUbigeoOrigen = value
        End Set
    End Property

    Public Property IdUbigeoDestino() As Integer
        Get
            Return Me._IdUbigeoDestino
        End Get
        Set(ByVal value As Integer)
            Me._IdUbigeoDestino = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._Descripcion
        End Get
        Set(ByVal value As String)
            Me._Descripcion = value
        End Set
    End Property

    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property

    Public Property Monto() As Decimal
        Get
            Return Me._Monto
        End Get
        Set(ByVal value As Decimal)
            Me._Monto = value
        End Set
    End Property

    Public Property IdUsuarioInsert() As Integer
        Get
            Return Me._IdUsuarioInsert
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuarioInsert = value
        End Set
    End Property

    Public Property FechaInsert() As Date
        Get
            Return Me._FechaInsert
        End Get
        Set(ByVal value As Date)
            Me._FechaInsert = value
        End Set
    End Property

    Public Property IdUsuarioUpdate() As Integer
        Get
            Return Me._IdUsuarioUpdate
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuarioUpdate = value
        End Set
    End Property

    Public Property FechaUpdate() As Date
        Get
            Return Me._FechaUpdate
        End Get
        Set(ByVal value As Date)
            Me._FechaUpdate = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return Me._Estado
        End Get
        Set(ByVal value As Boolean)
            Me._Estado = value
        End Set
    End Property

    Public Property Moneda() As String
        Get
            Return Me._Moneda
        End Get
        Set(ByVal value As String)
            Me._Moneda = value
        End Set
    End Property

    Public Property CodDpto_Origen() As String
        Get
            Return Me._CodDpto_Origen
        End Get
        Set(ByVal value As String)
            Me._CodDpto_Origen = value
        End Set
    End Property

    Public Property CodProv_Origen() As String
        Get
            Return Me._CodProv_Origen
        End Get
        Set(ByVal value As String)
            Me._CodProv_Origen = value
        End Set
    End Property

    Public Property CodDist_Origen() As String
        Get
            Return Me._CodDist_Origen
        End Get
        Set(ByVal value As String)
            Me._CodDist_Origen = value
        End Set
    End Property

    Public Property Origen() As String
        Get
            Return Me._Origen
        End Get
        Set(ByVal value As String)
            Me._Origen = value
        End Set
    End Property

    Public Property CodDpto_Destino() As String
        Get
            Return Me._CodDpto_Destino
        End Get
        Set(ByVal value As String)
            Me._CodDpto_Destino = value
        End Set
    End Property

    Public Property CodProv_Destino() As String
        Get
            Return Me._CodProv_Destino
        End Get
        Set(ByVal value As String)
            Me._CodProv_Destino = value
        End Set
    End Property

    Public Property CodDist_Destino() As String
        Get
            Return Me._CodDist_Destino
        End Get
        Set(ByVal value As String)
            Me._CodDist_Destino = value
        End Set
    End Property

    Public Property Destino() As String
        Get
            Return Me._Destino
        End Get
        Set(ByVal value As String)
            Me._Destino = value
        End Set
    End Property

End Class
