﻿Public Class beZonaAlmacenamiento

    Private _IdZonaAlmacenamiento As Integer = 0
    Private _NombreZonaAlmc As String = String.Empty
    Private _Tienda As String = String.Empty
    Private _IdTienda As Integer = 0

    Public Property IdZonaAlmacenamiento() As Integer
        Get
            Return _IdZonaAlmacenamiento
        End Get
        Set(value As Integer)
            _IdZonaAlmacenamiento = value
        End Set
    End Property


    Public Property NombreZonaAlmc() As String
        Get
            Return _NombreZonaAlmc
        End Get
        Set(value As String)
            _NombreZonaAlmc = value
        End Set
    End Property

    Public Property Tienda() As String
        Get
            Return _Tienda
        End Get
        Set(value As String)
            _Tienda = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return _IdTienda
        End Get
        Set(value As Integer)
            _IdTienda = value
        End Set
    End Property

End Class
