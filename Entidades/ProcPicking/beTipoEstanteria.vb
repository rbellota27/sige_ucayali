﻿Public Class beTipoEstanteria


    Private _IdTipoEstanteria As Integer = 0
    Private _NombreTipoEstanteria As String = String.Empty
    Private _CodEstanteria As String = String.Empty
    Private _Estado As Integer = 0
    Private _ParametroUbic As Integer = 0
    Private _Observacion As String = String.Empty
    Private _Imagen As String = String.Empty



    Public Property IdTipoEstanteria() As Integer
        Get
            Return _IdTipoEstanteria
        End Get
        Set(value As Integer)
            _IdTipoEstanteria = value
        End Set
    End Property


    Public Property NombreTipoEstanteria() As String
        Get
            Return _NombreTipoEstanteria
        End Get
        Set(value As String)
            _NombreTipoEstanteria = value
        End Set
    End Property

    Public Property CodEstanteria() As String
        Get
            Return _CodEstanteria
        End Get
        Set(value As String)
            _CodEstanteria = value
        End Set
    End Property



    Public Property Estado() As Integer
        Get
            Return _Estado
        End Get
        Set(value As Integer)
            _Estado = value
        End Set
    End Property



    Public Property ParametroUbic() As Integer
        Get
            Return _ParametroUbic
        End Get
        Set(value As Integer)
            _ParametroUbic = value
        End Set
    End Property



    Public Property Observacion() As String
        Get
            Return _Observacion
        End Get
        Set(value As String)
            _Observacion = value
        End Set
    End Property


    Public Property Imagen() As String
        Get
            Return _Imagen
        End Get
        Set(value As String)
            _Imagen = value
        End Set
    End Property



End Class
