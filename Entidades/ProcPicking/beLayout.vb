﻿Public Class beLayout
    Private _IdUbicacion As Integer = 0
    Private _IdEstanteria As Integer = 0
    Private _IdZona As Integer = 0
    Private _NroColumna As Integer = 0
    Private _NroFila As Integer = 0
    Private _FondoD As Decimal
    Private _AnchoL As Decimal
    Private _AlturaH As Decimal
    Private _PesoMax As Decimal
    Private _CodEstanteria As String = String.Empty
    Private _IdTipoPaleta As Integer = 0
    Private _crossdocking As Boolean
    Private _picking As Boolean
    Private _Piso As Integer = 0
    Private _OrdenRecorrido As Integer = 0
    Private _MedAncho As Integer = 0
    Private _MedAltura As Integer = 0


    Public Property IdUbicacion() As Integer
        Get
            Return _IdUbicacion
        End Get
        Set(value As Integer)
            _IdUbicacion = value
        End Set
    End Property


    Public Property IdEstanteria() As Integer
        Get
            Return _IdEstanteria
        End Get
        Set(value As Integer)
            _IdEstanteria = value
        End Set
    End Property


    Public Property IdZona() As Integer
        Get
            Return _IdZona
        End Get
        Set(value As Integer)
            _IdZona = value
        End Set
    End Property


    Public Property NroColumna() As Integer
        Get
            Return _NroColumna
        End Get
        Set(value As Integer)
            _NroColumna = value
        End Set
    End Property


    Public Property NroFila() As Integer
        Get
            Return _NroFila
        End Get
        Set(value As Integer)
            _NroFila = value
        End Set
    End Property

    Public Property FondoD() As Decimal
        Get
            Return _FondoD
        End Get
        Set(value As Decimal)
            _FondoD = value
        End Set
    End Property

    Public Property AnchoL() As Decimal
        Get
            Return _AnchoL
        End Get
        Set(value As Decimal)
            _AnchoL = value
        End Set
    End Property

    Public Property AlturaH() As Decimal
        Get
            Return _AlturaH
        End Get
        Set(value As Decimal)
            _AlturaH = value
        End Set
    End Property

    Public Property PesoMax() As Decimal
        Get
            Return _PesoMax
        End Get
        Set(value As Decimal)
            _PesoMax = value
        End Set
    End Property

    Public Property CodEstanteria() As String
        Get
            Return _CodEstanteria
        End Get
        Set(value As String)
            _CodEstanteria = value
        End Set
    End Property

    Public Property IdTipoPaleta() As Integer
        Get
            Return _IdTipoPaleta
        End Get
        Set(value As Integer)
            _IdTipoPaleta = value
        End Set
    End Property


    Public Property crossdocking() As Boolean
        Get
            Return _crossdocking
        End Get
        Set(value As Boolean)
            _crossdocking = value
        End Set
    End Property

    Public Property picking() As Boolean
        Get
            Return _picking
        End Get
        Set(value As Boolean)
            _picking = value
        End Set
    End Property

    Public Property Piso() As Integer
        Get
            Return _Piso
        End Get
        Set(value As Integer)
            _Piso = value
        End Set
    End Property

    Public Property OrdenRecorrido() As Integer
        Get
            Return _OrdenRecorrido
        End Get
        Set(value As Integer)
            _OrdenRecorrido = value
        End Set
    End Property


    Public Property MedAncho() As Integer
        Get
            Return _MedAncho
        End Get
        Set(value As Integer)
            _MedAncho = value
        End Set
    End Property

    Public Property MedAltura() As Integer
        Get
            Return _MedAltura
        End Get
        Set(value As Integer)
            _MedAltura = value
        End Set
    End Property


End Class
