﻿Public Class bePersonalMilla

    Private _IdEmpleado As Integer
    Private _NombreEmpleado As String = String.Empty
    Private _Codigo As String = String.Empty


    Public Property IdEmpleado() As Integer
        Get
            Return _IdEmpleado
        End Get
        Set(value As Integer)
            _IdEmpleado = (value)
        End Set
    End Property


    Public Property NombreEmpleado() As String
        Get
            Return _NombreEmpleado
        End Get
        Set(value As String)
            _NombreEmpleado = value
        End Set
    End Property


    Public Property Codigo() As String
        Get
            Return _Codigo
        End Get
        Set(value As String)
            _Codigo = value
        End Set
    End Property
End Class
