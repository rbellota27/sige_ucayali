﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class ProductoTipoTablaValor
    Private _IdProducto As Integer
    Private _IdTipoTabla As Integer
    Private _IdTipoTablaValor As Integer
    Public Sub New()
    End Sub
    Public Sub New(ByVal idtipotabla As Integer, ByVal idtipotablavalor As Integer)
        _IdTipoTabla = idtipotabla
        _IdTipoTablaValor = idtipotablavalor
    End Sub
    Public Property IdProducto() As Integer
        Get
            Return _IdProducto
        End Get
        Set(ByVal value As Integer)
            _IdProducto = value
        End Set
    End Property
    Public Property IdTipoTabla() As Integer
        Get
            Return _IdTipoTabla
        End Get
        Set(ByVal value As Integer)
            _IdTipoTabla = value
        End Set
    End Property
    Public Property IdTipoTablaValor() As Integer
        Get
            Return _IdTipoTablaValor
        End Get
        Set(ByVal value As Integer)
            _IdTipoTablaValor = value
        End Set
    End Property
End Class
