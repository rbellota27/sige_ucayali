﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class ImportacionDetalle
    Private _IdDocumento As Integer
    Private _ImpIdProducto As Integer
    Private _ImpPeso As Decimal
    Private _ImpCosto As Decimal
    Private _IdDetalleDocumento As Integer
    Private _dc_Importe As Decimal
    Private _ImpIdUM As Integer
    Private _ImpCantidad As Decimal

    Private _ImpIdProveedor As Integer
    Private _ImpRazonSocial As String
    Private _ImpRuc As String
    Private _ImpDireccion As String
    Private _ImpIdTipoDoc As Integer
    Private _ImpNomTipoDoc As String
    Private _ImpFechaEmision As Date
    Private _ImpNomProducto As String
    Private _ImpNomUM As String
    Private _NroOrdenCompra As String
    Private _PrecioUnitario As Decimal
    Private _AbvMagnitud As String
    Private _IdDetalleAfecto As Integer
    Private _Arancel As Decimal
    Private _CodigoProducto As String

    Sub New()
    End Sub
    Sub New(ByVal iddocumento As Integer, ByVal iddetalleDocumento As Integer, ByVal idproducto As Integer, _
            ByVal idum As Integer, ByVal cantidad As Decimal, ByVal costoproduccion As Decimal, ByVal peso As Decimal, _
            ByVal costoimportacion As Decimal)
        _IdDocumento = iddocumento
        _IdDetalleDocumento = iddetalleDocumento
        _ImpIdProducto = idproducto
        _dc_Importe = costoproduccion
        _ImpPeso = peso
        _ImpCosto = costoimportacion
    End Sub


    Public Property CodigoProducto() As String
        Get
            Return _CodigoProducto
        End Get
        Set(ByVal value As String)
            _CodigoProducto = value
        End Set
    End Property


    Public Property Arancel() As Decimal
        Get
            Return _Arancel
        End Get
        Set(ByVal value As Decimal)
            _Arancel = value
        End Set
    End Property

    Public Property IdDocumento() As Integer
        Get
            Return _IdDocumento
        End Get
        Set(ByVal value As Integer)
            _IdDocumento = value
        End Set
    End Property
    Public Property IdDetalleDocumento() As Integer
        Get
            Return _IdDetalleDocumento
        End Get
        Set(ByVal value As Integer)
            _IdDetalleDocumento = value
        End Set
    End Property
    Public Property dc_Importe() As Decimal
        Get
            Return _dc_Importe
        End Get
        Set(ByVal value As Decimal)
            _dc_Importe = value
        End Set
    End Property
    Public Property Peso() As Decimal
        Get
            Return _ImpPeso
        End Get
        Set(ByVal value As Decimal)
            _ImpPeso = value
        End Set
    End Property
    Public Property Costo() As Decimal
        Get
            Return _ImpCosto
        End Get
        Set(ByVal value As Decimal)
            _ImpCosto = value
        End Set
    End Property
    Public Property IdProducto() As Integer
        Get
            Return _ImpIdProducto
        End Get
        Set(ByVal value As Integer)
            _ImpIdProducto = value
        End Set
    End Property
    Public Property IdUM() As Integer
        Get
            Return _ImpIdUM
        End Get
        Set(ByVal value As Integer)
            _ImpIdUM = value
        End Set
    End Property
    Public Property Cantidad() As Decimal
        Get
            Return _ImpCantidad
        End Get
        Set(ByVal value As Decimal)
            _ImpCantidad = value
        End Set
    End Property
    Public Property IdTipoDocumento() As Integer
        Get
            Return _ImpIdTipoDoc
        End Get
        Set(ByVal value As Integer)
            _ImpIdTipoDoc = value
        End Set
    End Property
    Public Property NomTipoDoc() As String
        Get
            Return _ImpNomTipoDoc
        End Get
        Set(ByVal value As String)
            _ImpNomTipoDoc = value
        End Set
    End Property
    Public Property FechaEmision() As Date
        Get
            Return _ImpFechaEmision
        End Get
        Set(ByVal value As Date)
            _ImpFechaEmision = value
        End Set
    End Property
    Public Property NomProducto() As String
        Get
            Return _ImpNomProducto
        End Get
        Set(ByVal value As String)
            _ImpNomProducto = value
        End Set
    End Property
    Public Property NomUM() As String
        Get
            Return _ImpNomUM
        End Get
        Set(ByVal value As String)
            _ImpNomUM = value
        End Set
    End Property
    Public Property NroOrdenCompra() As String
        Get
            Return _NroOrdenCompra
        End Get
        Set(ByVal value As String)
            _NroOrdenCompra = value
        End Set
    End Property
    Public Property IdProveedor() As Integer
        Get
            Return _ImpIdProveedor
        End Get
        Set(ByVal value As Integer)
            _ImpIdProveedor = value
        End Set
    End Property
    Public Property RazonSocial() As String
        Get
            Return Me._ImpRazonSocial
        End Get
        Set(ByVal value As String)
            Me._ImpRazonSocial = value
        End Set
    End Property
    Public Property Ruc() As String
        Get
            Return _ImpRuc
        End Get
        Set(ByVal value As String)
            _ImpRuc = value
        End Set
    End Property
    Public Property Direccion() As String
        Get
            Return _ImpDireccion
        End Get
        Set(ByVal value As String)
            _ImpDireccion = value
        End Set
    End Property
    Public Property PrecioUnitario() As Decimal
        Get
            Return _PrecioUnitario
        End Get
        Set(ByVal value As Decimal)
            _PrecioUnitario = value
        End Set
    End Property
    Public Property AbvMagnitud() As String
        Get
            Return _AbvMagnitud
        End Get
        Set(ByVal value As String)
            _AbvMagnitud = value
        End Set
    End Property
    Public Property IdDetalleAfecto() As Integer
        Get
            Return _IdDetalleAfecto
        End Get
        Set(ByVal value As Integer)
            _IdDetalleAfecto = value
        End Set
    End Property
End Class
