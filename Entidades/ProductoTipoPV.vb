'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class ProductoTipoPV

    Private _IdProducto As Integer
    Private _IdTipoPv As Integer
    Private _ppv_PUtilFijo As Decimal
    Private _ppv_PUtilVariable As Decimal
    Private _ppv_Valor As Decimal
    Private _ppv_Utilidad As Decimal
    Private _IdUsuario As Integer
    Private _ppv_Estado As String
    Private _IdTienda As Integer
    Private _IdUnidadMedida As Integer
    Private _NomUMedida As String
    Private _Equivalencia As Decimal
    Private _PrecioCompraxUMPrincipal As Decimal
    Private _NomMoneda As String
    Private _NomMonedaPC As String
    Private _IdMoneda As Integer

    Private _pum_Retazo As Boolean
    Private _pum_PorcentRetazo As Decimal
    Private _pum_UnidadPrincipal As Boolean
    Private _PV_Soles As Decimal
    Private _PV_Dolares As Decimal
    Private _CambiarxEquivalencia As Boolean
    Private _TipoPV As String
    Private _CostoFlete As Decimal
    Private _IdTiendaDestino As Integer
    Private _Tienda As String
    Private _TiendaDestino As String
    Private _IdTipoPV_Origen As Integer
    Private _IdTipoPV_Destino As Integer
    Private _PorcentPV As Decimal
    Private _NuevoPVDestino As Decimal
    Private _ActualPVDestino As Decimal
    Private _addCostoFletexPeso As Boolean


    Private _IdMonedaTiendaPrincipal As Integer
    Private _PrecioTiendaPricipal As Decimal
    Private _IdTiendaPrincipal As Integer


    Public Property IdTiendaPrincipal() As Integer
        Get
            Return _IdTiendaPrincipal
        End Get
        Set(ByVal value As Integer)
            _IdTiendaPrincipal = value
        End Set
    End Property

    Public Property PrecioTiendaPrincipal() As Decimal
        Get
            Return _PrecioTiendaPricipal
        End Get
        Set(ByVal value As Decimal)
            _PrecioTiendaPricipal = value
        End Set
    End Property

    Public Property IdMonedaTiendaPrincipal() As Integer
        Get
            Return _IdMonedaTiendaPrincipal
        End Get
        Set(ByVal value As Integer)
            _IdMonedaTiendaPrincipal = value
        End Set
    End Property


    Public Property NuevoPVDestino() As Decimal
        Get
            Return _NuevoPVDestino
        End Get
        Set(ByVal value As Decimal)
            _NuevoPVDestino = value
        End Set
    End Property

    Public Property ActualPVDestino() As Decimal
        Get
            Return _ActualPVDestino
        End Get
        Set(ByVal value As Decimal)
            _ActualPVDestino = value
        End Set
    End Property

    Public Property addCostoFletexPeso() As Boolean
        Get
            Return _addCostoFletexPeso
        End Get
        Set(ByVal value As Boolean)
            _addCostoFletexPeso = value
        End Set
    End Property

    Public Property IdTipoPV_Origen() As Integer
        Get
            Return Me._IdTipoPV_Origen
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoPV_Origen = value
        End Set
    End Property

    Public Property IdTipoPV_Destino() As Integer
        Get
            Return Me._IdTipoPV_Destino
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoPV_Destino = value
        End Set
    End Property

    Public Property PorcentPV() As Decimal
        Get
            Return Me._PorcentPV
        End Get
        Set(ByVal value As Decimal)
            Me._PorcentPV = value
        End Set
    End Property


    Public Property TiendaDestino() As String
        Get
            Return _TiendaDestino
        End Get
        Set(ByVal value As String)
            _TiendaDestino = value
        End Set
    End Property

    Public Property Tienda() As String
        Get
            Return _Tienda
        End Get
        Set(ByVal value As String)
            _Tienda = value
        End Set
    End Property

    Public Property IdTiendaDestino() As Integer
        Get
            Return _IdTiendaDestino
        End Get
        Set(ByVal value As Integer)
            _IdTiendaDestino = value
        End Set
    End Property

    Public Property CostoFlete() As Decimal
        Get
            Return _CostoFlete
        End Get
        Set(ByVal value As Decimal)
            _CostoFlete = value
        End Set
    End Property

    Public Property TipoPV() As String
        Get
            Return _TipoPV
        End Get
        Set(ByVal value As String)
            _TipoPV = value
        End Set
    End Property

    Public Property CambiarxEquivalencia() As Boolean
        Get
            Return _CambiarxEquivalencia
        End Get
        Set(ByVal value As Boolean)
            _CambiarxEquivalencia = value
        End Set
    End Property

    Public Property PV_Soles() As Decimal
        Get
            Return Me._PV_Soles
        End Get
        Set(ByVal value As Decimal)
            Me._PV_Soles = value
        End Set
    End Property

    Public Property PV_Dolares() As Decimal
        Get
            Return Me._PV_Dolares
        End Get
        Set(ByVal value As Decimal)
            Me._PV_Dolares = value
        End Set
    End Property




    Public ReadOnly Property getDescUMPrincipal() As String
        Get

            If Me.UnidadPrincipal Then
                Return "S�"
            Else
                Return "--"
            End If

        End Get
    End Property

    Public ReadOnly Property getDescUMRetazo() As String
        Get

            If Me.Retazo Then
                Return "S�"
            Else
                Return "--"
            End If

        End Get
    End Property





    Public Property Retazo() As Boolean
        Get
            Return Me._pum_Retazo
        End Get
        Set(ByVal value As Boolean)
            Me._pum_Retazo = value
        End Set
    End Property

    Public Property PorcentRetazo() As Decimal
        Get
            Return Me._pum_PorcentRetazo
        End Get
        Set(ByVal value As Decimal)
            Me._pum_PorcentRetazo = value
        End Set
    End Property

    Public Property UnidadPrincipal() As Boolean
        Get
            Return Me._pum_UnidadPrincipal
        End Get
        Set(ByVal value As Boolean)
            Me._pum_UnidadPrincipal = value
        End Set
    End Property
    Public Property NomMoneda() As String
        Get
            Return Me._NomMoneda
        End Get
        Set(ByVal value As String)
            Me._NomMoneda = value
        End Set
    End Property
    Public Property NomMonedaPC() As String
        Get
            Return Me._NomMonedaPC
        End Get
        Set(ByVal value As String)
            Me._NomMonedaPC = value
        End Set
    End Property

    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property
    Public Property PrecioCompraxUMPrincipal() As Decimal
        Get
            Return Me._PrecioCompraxUMPrincipal
        End Get
        Set(ByVal value As Decimal)
            Me._PrecioCompraxUMPrincipal = value
        End Set
    End Property
    Public Property Equivalencia() As Decimal
        Get
            Return Me._Equivalencia
        End Get
        Set(ByVal value As Decimal)
            Me._Equivalencia = value
        End Set
    End Property
    Public ReadOnly Property getPrecioCompraxRetazo() As Decimal
        Get
            Return Math.Round((Me._PrecioCompraxUMPrincipal * _Equivalencia), 4)
        End Get
    End Property

    Public Property NomUMedida() As String
        Get
            Return Me._NomUMedida
        End Get
        Set(ByVal value As String)
            Me._NomUMedida = value
        End Set
    End Property

    Public ReadOnly Property EstadoBoolean() As Boolean
        Get
            If Me._ppv_Estado = "1" Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property
    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property IdUnidadMedida() As Integer
        Get
            Return Me._IdUnidadMedida
        End Get
        Set(ByVal value As Integer)
            Me._IdUnidadMedida = value
        End Set
    End Property
    Public Property Estado() As String
        Get
            Return Me._ppv_Estado
        End Get
        Set(ByVal value As String)
            Me._ppv_Estado = value
        End Set
    End Property
    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property

    Public Property IdTipoPv() As Integer
        Get
            Return Me._IdTipoPv
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoPv = value
        End Set
    End Property

    Public Property PUtilFijo() As Decimal
        Get
            Return Me._ppv_PUtilFijo
        End Get
        Set(ByVal value As Decimal)
            Me._ppv_PUtilFijo = value
        End Set
    End Property

    Public Property PUtilVariable() As Decimal
        Get
            Return Me._ppv_PUtilVariable
        End Get
        Set(ByVal value As Decimal)
            Me._ppv_PUtilVariable = value
        End Set
    End Property

    Public Property Valor() As Decimal
        Get
            Return Me._ppv_Valor
        End Get
        Set(ByVal value As Decimal)
            Me._ppv_Valor = value
        End Set
    End Property

    Public Property Utilidad() As Decimal
        Get
            Return Me._ppv_Utilidad
        End Get
        Set(ByVal value As Decimal)
            Me._ppv_Utilidad = value
        End Set
    End Property

    Public Property IdUsuario() As Integer
        Get
            Return Me._IdUsuario
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuario = value
        End Set
    End Property






End Class
