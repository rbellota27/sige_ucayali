﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class ConfigTipoCambio
    Private _IdConfigTipoCambio As Integer
    Private _ctc_Descripcion As String
    Private _ctc_CompraOf As Boolean
    Private _ctc_CompraC As Boolean
    Private _ctc_VentaC As Boolean
    Private _ctc_VentaOf As Boolean



    Private _IdUsuario As Integer
    Private _NombreComercial As String
    Private _RazonSocial As String
    Private _NombreNatural As String


    Public ReadOnly Property getNombreAMostrar() As String
        Get

            If Me.RazonSocial.Trim.Length > 0 Then

                Return Me.RazonSocial

            ElseIf Me.NombreComercial.Trim.Length > 0 Then

                Return Me.NombreComercial

            Else

                Return Me.NombreNatural

            End If


        End Get
    End Property

    Public Property IdUsuario() As Integer
        Get
            Return Me._IdUsuario
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuario = value
        End Set
    End Property

    Public Property NombreComercial() As String
        Get
            Return Me._NombreComercial
        End Get
        Set(ByVal value As String)
            Me._NombreComercial = value
        End Set
    End Property

    Public Property RazonSocial() As String
        Get
            Return Me._RazonSocial
        End Get
        Set(ByVal value As String)
            Me._RazonSocial = value
        End Set
    End Property

    Public Property NombreNatural() As String
        Get
            Return Me._NombreNatural
        End Get
        Set(ByVal value As String)
            Me._NombreNatural = value
        End Set
    End Property





    Public Property IdConfigTipoCambio() As Integer
        Get
            Return Me._IdConfigTipoCambio
        End Get
        Set(ByVal value As Integer)
            Me._IdConfigTipoCambio = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._ctc_Descripcion
        End Get
        Set(ByVal value As String)
            Me._ctc_Descripcion = value
        End Set
    End Property

    Public Property CompraOf() As Boolean
        Get
            Return Me._ctc_CompraOf
        End Get
        Set(ByVal value As Boolean)
            Me._ctc_CompraOf = value
        End Set
    End Property

    Public Property CompraC() As Boolean
        Get
            Return Me._ctc_CompraC
        End Get
        Set(ByVal value As Boolean)
            Me._ctc_CompraC = value
        End Set
    End Property

    Public Property VentaC() As Boolean
        Get
            Return Me._ctc_VentaC
        End Get
        Set(ByVal value As Boolean)
            Me._ctc_VentaC = value
        End Set
    End Property

    Public Property VentaOf() As Boolean
        Get
            Return Me._ctc_VentaOf
        End Get
        Set(ByVal value As Boolean)
            Me._ctc_VentaOf = value
        End Set
    End Property






End Class
