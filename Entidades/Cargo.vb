'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Cargo
    Private _IdCargo As Integer
    Private _car_Nombre As String
    Private _car_Estado As String
    Private _descEstado As String
    Public Property Id() As Integer
        Get
            Return Me._IdCargo
        End Get
        Set(ByVal value As Integer)
            Me._IdCargo = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._car_Nombre
        End Get
        Set(ByVal value As String)
            Me._car_Nombre = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._car_Estado
        End Get
        Set(ByVal value As String)
            Me._car_Estado = value
        End Set
    End Property
    Public ReadOnly Property DescEstado() As String
        Get
            Me._descEstado = "Activo"
            If _car_Estado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property
End Class
