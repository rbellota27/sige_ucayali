'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class ProductoRegimen
    Private _reg_Id As Integer
    Private _IdSubLInea As Integer
    Private _pr_Tasa As Decimal
    Public Property reg_Id() As Integer
        Get
            Return Me._reg_Id
        End Get
        Set(ByVal value As Integer)
            Me._reg_Id = value
        End Set
    End Property

    Public Property IdSubLInea() As Integer
        Get
            Return Me._IdSubLInea
        End Get
        Set(ByVal value As Integer)
            Me._IdSubLInea = value
        End Set
    End Property

    Public Property Tasa() As Decimal
        Get
            Return Me._pr_Tasa
        End Get
        Set(ByVal value As Decimal)
            Me._pr_Tasa = value
        End Set
    End Property
End Class
