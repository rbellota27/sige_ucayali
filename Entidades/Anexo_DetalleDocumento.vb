﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


'******************   LUNES 31 MAYO 2010 HORA 03_23 PM

Public Class Anexo_DetalleDocumento
    Private _IdDetalleDocumento As Integer
    Private _IdDocumento As Integer
    Private _IdTipoPV As Integer
    Private _CantidadAprobada As Decimal
    Private _ComponenteKit As Boolean
    Private _IdKit As Integer
    Private _IdProductoRef As Integer
    Private _IdCampania As Integer

    Public Property IdProductoRef() As Integer
        Get
            Return _IdProductoRef
        End Get
        Set(ByVal value As Integer)
            _IdProductoRef = value
        End Set
    End Property

    Public Property IdCampania() As Integer
        Get
            Return Me._IdCampania
        End Get
        Set(ByVal value As Integer)
            Me._IdCampania = value
        End Set
    End Property



    Public Property ComponenteKit() As Boolean
        Get
            Return Me._ComponenteKit
        End Get
        Set(ByVal value As Boolean)
            Me._ComponenteKit = value
        End Set
    End Property

    Public Property IdKit() As Integer
        Get
            Return Me._IdKit
        End Get
        Set(ByVal value As Integer)
            Me._IdKit = value
        End Set
    End Property


    Public Property CantidadAprobada() As Decimal
        Get
            Return Me._CantidadAprobada
        End Get
        Set(ByVal value As Decimal)
            Me._CantidadAprobada = value
        End Set
    End Property



    Public Property IdDetalleDocumento() As Integer
        Get
            Return Me._IdDetalleDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDetalleDocumento = value
        End Set
    End Property

    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property

    Public Property IdTipoPV() As Integer
        Get
            Return Me._IdTipoPV
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoPV = value
        End Set
    End Property


End Class
