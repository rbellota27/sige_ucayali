﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Color_Linea
    Inherits Color
    Private _coll_Orden As Integer
    Private _coll_Estado As Boolean
    Private _coll_Flag As Boolean
    Private _idlinea As Integer
    Private _idtipoexistencia As Integer

    Public Sub New()
    End Sub
    Public Sub New(ByVal idcolor As Integer, ByVal nomcolor As String, ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal estado As Boolean, ByVal orden As Integer)
        Id = idcolor
        Nombre = nomcolor
        _idlinea = idlinea
        _idtipoexistencia = idtipoexistencia
        _coll_Estado = estado
        _coll_Orden = orden
    End Sub
    Public Sub New(ByVal idcolor As Integer, ByVal nomcolor As String, ByVal estado As Boolean)
        Id = idcolor
        NombreCorto = nomcolor
        EstadoBit = estado
    End Sub
    Public Property IdLinea() As Integer
        Get
            Return _idlinea
        End Get
        Set(ByVal value As Integer)
            _idlinea = value
        End Set
    End Property
    Public Property IdTipoExistencia() As Integer
        Get
            Return _idtipoexistencia
        End Get
        Set(ByVal value As Integer)
            _idtipoexistencia = value
        End Set
    End Property
    Public Property Orden() As Integer
        Get
            Return _coll_Orden
        End Get
        Set(ByVal value As Integer)
            _coll_Orden = value
        End Set
    End Property
    Public Property CLEstado() As Boolean
        Get
            Return _coll_Estado
        End Get
        Set(ByVal value As Boolean)
            _coll_Estado = value
        End Set
    End Property
    Public Property Flag() As Boolean
        Get
            Return _coll_Flag
        End Get
        Set(ByVal value As Boolean)
            _coll_Flag = value
        End Set
    End Property
End Class
