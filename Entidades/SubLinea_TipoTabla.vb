﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class SubLinea_TipoTabla
    Inherits TipoTabla
    Private _IdSubLinea As Integer
    Private _IdTipoTabla As Integer
    Private _sltt_TipoUso As String
    Private _sltt_NroOrden As Integer
    Private _sltt_ParteNombre As Boolean
    Private _sltt_Estado As Boolean
    Private _NomTipoUso As String
    Private _IdLinea As Integer
    Private _Codigo As String
    Private _objTipoTabla As Object
    Private _IdTipoTablaValor As Integer
    Private _Indicador As Boolean
    Private _objSubOrigen As Object
    Private _objSubCodigo As Object
    Private _objSubAtributos As Object
    Private _objSubSinUso As Object
    Public Sub New()
    End Sub
    Public Sub New(ByVal vidlinea As Integer, ByVal vidsublinea As Integer, ByVal vidtipotabla As Integer, ByVal vnombre As String, _
                   ByVal vtipouso As String, ByVal vnomtipouso As String, ByVal vnroorden As Integer, _
                   ByVal vpartenombre As Boolean, ByVal vestado As Boolean, ByVal vindicador As Boolean, ByVal vLongitud As String)
        IdLinea = vidlinea
        IdSubLinea = vidsublinea
        IdTipoTabla = vidtipotabla
        Nombre = vnombre
        TipoUso = vtipouso
        NomTipoUso = vnomtipouso
        NroOrden = vnroorden
        ParteNombre = vpartenombre
        Estado = vestado
        Indicador = vindicador
        Longitud = vLongitud
    End Sub
    Public Sub New(ByVal vidtipotabla As Integer, ByVal vnombre As String, ByVal vestado As Boolean)
        IdTipoTabla = vidtipotabla
        Nombre = vnombre
        Estado = vestado
    End Sub
    Public Property IdSubLinea() As Integer
        Get
            Return _IdSubLinea
        End Get
        Set(ByVal value As Integer)
            _IdSubLinea = value
        End Set
    End Property
    Public Overloads Property IdTipoTabla() As Integer
        Get
            Return _IdTipoTabla
        End Get
        Set(ByVal value As Integer)
            _IdTipoTabla = value
        End Set
    End Property
    Public Property TipoUso() As String
        Get
            Return _sltt_TipoUso
        End Get
        Set(ByVal value As String)
            _sltt_TipoUso = value
        End Set
    End Property
    Public Property NroOrden() As Integer
        Get
            Return _sltt_NroOrden
        End Get
        Set(ByVal value As Integer)
            _sltt_NroOrden = value
        End Set
    End Property
    Public Property ParteNombre() As Boolean
        Get
            Return _sltt_ParteNombre
        End Get
        Set(ByVal value As Boolean)
            _sltt_ParteNombre = value
        End Set
    End Property
    Public Overloads Property Estado() As Boolean
        Get
            Return _sltt_Estado
        End Get
        Set(ByVal value As Boolean)
            _sltt_Estado = value
        End Set
    End Property
    Public Overloads ReadOnly Property strEstado() As String
        Get
            If Estado = True Then Return "Activo"
            If Estado = False Then Return "Inactivo"
        End Get
    End Property
    Public Property NomTipoUso() As String
        Get
            Return _NomTipoUso
        End Get
        Set(ByVal value As String)
            _NomTipoUso = value
        End Set
    End Property
    Public Property IdLinea() As Integer
        Get
            Return _IdLinea
        End Get
        Set(ByVal value As Integer)
            _IdLinea = value
        End Set
    End Property
    Public Property Codigo() As String
        Get
            Return _Codigo
        End Get
        Set(ByVal value As String)
            _Codigo = value
        End Set
    End Property

    Private _NomLinea As String
    Private _NomSubLinea As String
    Public Property NomLinea() As String
        Get
            Return _NomLinea
        End Get
        Set(ByVal value As String)
            _NomLinea = value
        End Set
    End Property
    Public Property NomSubLinea() As String
        Get
            Return _NomSubLinea
        End Get
        Set(ByVal value As String)
            _NomSubLinea = value
        End Set
    End Property
    Public Property objTipoTabla() As Object
        Get
            Return _objTipoTabla
        End Get
        Set(ByVal value As Object)
            _objTipoTabla = value
        End Set
    End Property
    Public Property IdTipoTablaValor() As Integer
        Get
            Return _IdTipoTablaValor
        End Get
        Set(ByVal value As Integer)
            _IdTipoTablaValor = value
        End Set
    End Property
    Public Property Indicador() As Boolean
        Get
            Return _Indicador
        End Get
        Set(ByVal value As Boolean)
            _Indicador = value
        End Set
    End Property
    Public Property objSubOrigen() As Object
        Get
            Return _objSubOrigen
        End Get
        Set(ByVal value As Object)
            _objSubOrigen = value
        End Set
    End Property
    Public Property objSubCodigo() As Object
        Get
            Return _objSubCodigo
        End Get
        Set(ByVal value As Object)
            _objSubCodigo = value
        End Set
    End Property
    Public Property objSubAtributos() As Object
        Get
            Return _objSubAtributos
        End Get
        Set(ByVal value As Object)
            _objSubAtributos = value
        End Set
    End Property
    Public Property objSubSinUso() As Object
        Get
            Return _objSubSinUso
        End Get
        Set(ByVal value As Object)
            _objSubSinUso = value
        End Set
    End Property

End Class
