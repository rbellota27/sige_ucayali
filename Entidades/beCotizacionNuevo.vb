﻿Public Class beCotizacionNuevo

	Private _FechaActual As Date
	Public Property FechaActual() As Date
		Get
			Return _FechaActual
		End Get
		Set(ByVal value As Date)
			_FechaActual = value
		End Set
	End Property

	Private _listaParametros As List(Of Decimal)
	Public Property listaParametros() As List(Of Decimal)
		Get
			Return _listaParametros
		End Get
		Set(ByVal value As List(Of Decimal))
			_listaParametros = value
		End Set
	End Property

	Private _TiendaUbigeo As beCampoCadena
	Public Property TiendaUbigeo() As beCampoCadena
		Get
			Return _TiendaUbigeo
		End Get
		Set(ByVal value As beCampoCadena)
			_TiendaUbigeo = value
		End Set
	End Property

	Private _NroDocumento As String
	Public Property NroDocumento() As String
		Get
			Return _NroDocumento
		End Get
		Set(ByVal value As String)
			_NroDocumento = value
		End Set
	End Property

	Private _listaDepartamentoPartida As List(Of beCboDepartamento)
	Public Property listaDepartamentoPartida() As List(Of beCboDepartamento)
		Get
			Return _listaDepartamentoPartida
		End Get
		Set(ByVal value As List(Of beCboDepartamento))
			_listaDepartamentoPartida = value
		End Set
	End Property

	Private _listaProvinciaPartida As List(Of beCboProvincia)
	Public Property listaProvinciaPartida() As List(Of beCboProvincia)
		Get
			Return _listaProvinciaPartida
		End Get
		Set(ByVal value As List(Of beCboProvincia))
			_listaProvinciaPartida = value
		End Set
	End Property

	Private _listaDistritoPartida As List(Of beCboDistrito)
	Public Property listaDistritoPartida() As List(Of beCboDistrito)
		Get
			Return _listaDistritoPartida
		End Get
		Set(ByVal value As List(Of beCboDistrito))
			_listaDistritoPartida = value
		End Set
	End Property

	Private _IGV As Decimal
	Public Property IGV() As Decimal
		Get
			Return _IGV
		End Get
		Set(ByVal value As Decimal)
			_IGV = value
		End Set
	End Property

End Class
