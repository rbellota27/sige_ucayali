﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*********************   LUNES 24052010

Public Class CostoFletexPeso

    Private _IdTiendaOrigen As Integer
    Private _IdTiendaDestino As Integer
    Private _IdMoneda As Integer
    Private _cfp_costoxPesoUnit As Decimal
    Private _IdUnidadMedida_Peso As Integer
    Private _UnidadMedida As String
    Private _Moneda As String

    Public Property IdTiendaOrigen() As Integer
        Get
            Return Me._IdTiendaOrigen
        End Get
        Set(ByVal value As Integer)
            Me._IdTiendaOrigen = value
        End Set
    End Property

    Public Property IdTiendaDestino() As Integer
        Get
            Return Me._IdTiendaDestino
        End Get
        Set(ByVal value As Integer)
            Me._IdTiendaDestino = value
        End Set
    End Property

    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property

    Public Property costoxPesoUnit() As Decimal
        Get
            Return Me._cfp_costoxPesoUnit
        End Get
        Set(ByVal value As Decimal)
            Me._cfp_costoxPesoUnit = value
        End Set
    End Property

    Public Property IdUnidadMedida_Peso() As Integer
        Get
            Return Me._IdUnidadMedida_Peso
        End Get
        Set(ByVal value As Integer)
            Me._IdUnidadMedida_Peso = value
        End Set
    End Property

    Public Property UnidadMedida() As String
        Get
            Return Me._UnidadMedida
        End Get
        Set(ByVal value As String)
            Me._UnidadMedida = value
        End Set
    End Property

    Public Property Moneda() As String
        Get
            Return Me._Moneda
        End Get
        Set(ByVal value As String)
            Me._Moneda = value
        End Set
    End Property

End Class
