﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'************  VIERNES 04 06 2010

Public Class Comision_Cab
    Private _comc_Descripcion As String
    Private _comc_Abv As String
    Private _comc_Estado As Boolean
    Private _comc_Factura As Boolean
    Private _comc_Boleta As Boolean
    Private _IdComisionCab As Integer
    Private _IdTipoComision As Integer


    Public Property IdTipoComision() As Integer
        Get
            Return _IdTipoComision
        End Get
        Set(ByVal value As Integer)
            _IdTipoComision = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._comc_Descripcion
        End Get
        Set(ByVal value As String)
            Me._comc_Descripcion = value
        End Set
    End Property

    Public Property Abv() As String
        Get
            Return Me._comc_Abv
        End Get
        Set(ByVal value As String)
            Me._comc_Abv = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return Me._comc_Estado
        End Get
        Set(ByVal value As Boolean)
            Me._comc_Estado = value
        End Set
    End Property

    Public Property Factura() As Boolean
        Get
            Return Me._comc_Factura
        End Get
        Set(ByVal value As Boolean)
            Me._comc_Factura = value
        End Set
    End Property

    Public Property Boleta() As Boolean
        Get
            Return Me._comc_Boleta
        End Get
        Set(ByVal value As Boolean)
            Me._comc_Boleta = value
        End Set
    End Property

    Public Property IdComisionCab() As Integer
        Get
            Return Me._IdComisionCab
        End Get
        Set(ByVal value As Integer)
            Me._IdComisionCab = value
        End Set
    End Property

End Class
