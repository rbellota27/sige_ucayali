﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Caja_AperturaCierre

    Private _IdCaja_AperturaCierre As Integer
    Private _IdCaja As Integer
    Private _IdUsuario As Integer
    Private _cac_FechaApertura As Nullable(Of DateTime)
    Private _cac_FechaRegistroApertura As DateTime
    Private _cac_FechaCierre As Nullable(Of DateTime)
    Private _cac_FechaRegistroCierre As DateTime
    Private _IdMoneda As Integer

    Private _IdDocumentoApertura As Integer
    Private _DB_NAME As String
    Private _NomPersona As String
    Private _caja_Nombre As String
    Private _mon_Simbolo As String
    Private _TotalCajaEfectivo As Decimal
    Private _tdoc_NombreCorto As String

    Private _cac_MontoApertura As Decimal
    Private _cac_MontoCierre As Decimal

    Private _cac_ControlCaja As String
    Private _ControlCaja As String


    Public Property ControlCaja() As String
        Get
            Return _ControlCaja
        End Get
        Set(ByVal value As String)
            _ControlCaja = value
        End Set
    End Property

    Public Property cac_ControlCaja() As String
        Get
            Return _cac_ControlCaja
        End Get
        Set(ByVal value As String)
            _cac_ControlCaja = value
        End Set
    End Property

    Public Property cac_MontoCierre() As Decimal
        Get
            Return _cac_MontoCierre
        End Get
        Set(ByVal value As Decimal)
            _cac_MontoCierre = value
        End Set
    End Property

    Public Property tdoc_NombreCorto() As String
        Get
            Return _tdoc_NombreCorto
        End Get
        Set(ByVal value As String)
            _tdoc_NombreCorto = value
        End Set
    End Property

    Public Property TotalCajaEfectivo() As Decimal
        Get
            Return _TotalCajaEfectivo
        End Get
        Set(ByVal value As Decimal)
            _TotalCajaEfectivo = value
        End Set
    End Property

    Public Property mon_Simbolo() As String
        Get
            Return _mon_Simbolo
        End Get
        Set(ByVal value As String)
            _mon_Simbolo = value
        End Set
    End Property

    Public Property caja_Nombre() As String
        Get
            Return _caja_Nombre
        End Get
        Set(ByVal value As String)
            _caja_Nombre = value
        End Set
    End Property

    Public Property NomPersona() As String
        Get
            Return _NomPersona
        End Get
        Set(ByVal value As String)
            _NomPersona = value
        End Set
    End Property

    Public Property IdCaja_AperturaCierre() As Integer
        Get
            Return _IdCaja_AperturaCierre
        End Get
        Set(ByVal value As Integer)
            _IdCaja_AperturaCierre = value
        End Set
    End Property

    Public Property IdCaja() As Integer
        Get
            Return _IdCaja
        End Get
        Set(ByVal value As Integer)
            _IdCaja = value
        End Set
    End Property

    Public Property IdUsuario() As Integer
        Get
            Return _IdUsuario
        End Get
        Set(ByVal value As Integer)
            _IdUsuario = value
        End Set
    End Property

    Public Property cac_FechaApertura() As Nullable(Of DateTime)
        Get
            Return _cac_FechaApertura
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _cac_FechaApertura = value
        End Set
    End Property

    Public Property cac_FechaRegistroApertura() As DateTime
        Get
            Return _cac_FechaRegistroApertura
        End Get
        Set(ByVal value As DateTime)
            _cac_FechaRegistroApertura = value
        End Set
    End Property

    Public Property cac_FechaCierre() As Nullable(Of DateTime)
        Get
            Return _cac_FechaCierre
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _cac_FechaCierre = value
        End Set
    End Property

    Public Property cac_FechaRegistroCierre() As DateTime
        Get
            Return _cac_FechaRegistroCierre
        End Get
        Set(ByVal value As DateTime)
            _cac_FechaRegistroCierre = value
        End Set
    End Property

    Public Property IdMoneda() As Integer
        Get
            Return _IdMoneda
        End Get
        Set(ByVal value As Integer)
            _IdMoneda = value
        End Set
    End Property

    Public Property cac_MontoApertura() As Decimal
        Get
            Return _cac_MontoApertura
        End Get
        Set(ByVal value As Decimal)
            _cac_MontoApertura = value
        End Set
    End Property

    Public Property IdDocumentoApertura() As Integer
        Get
            Return _IdDocumentoApertura
        End Get
        Set(ByVal value As Integer)
            _IdDocumentoApertura = value
        End Set
    End Property

    Public Property DB_NAME() As String
        Get
            Return _DB_NAME
        End Get
        Set(ByVal value As String)
            _DB_NAME = value
        End Set
    End Property



End Class
