'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class RegimenOperacion

    Private _ro_Id As Integer
    Private _ro_CuentaContable As String
    Private _ro_Estado As String
    Private _IdTipoOperacion As Integer

    Public Property IdRegimen() As Integer
        Get
            Return Me._ro_Id
        End Get
        Set(ByVal value As Integer)
            Me._ro_Id = value
        End Set
    End Property

    Public Property CuentaContable() As String
        Get
            Return Me._ro_CuentaContable
        End Get
        Set(ByVal value As String)
            Me._ro_CuentaContable = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._ro_Estado
        End Get
        Set(ByVal value As String)
            Me._ro_Estado = value
        End Set
    End Property

    Public Property IdTipoOperacion() As Integer
        Get
            Return Me._IdTipoOperacion
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoOperacion = value
        End Set
    End Property
    Public ReadOnly Property DescEstado() As String
        Get
            If Me.Estado = "1" Then
                Return "Activo"
            Else : Return "Inactivo"
            End If
        End Get

    End Property
End Class
