﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Usuario_Area
    Private _IdUsuario As Integer
    Private _IdArea As Integer
    Private _ua_estado As Boolean
    Private _NombreArea As String

    Public Sub New()

    End Sub

    Public Sub New(ByVal IdUsuario As Integer, ByVal IdArea As Integer, ByVal Estado As Boolean, ByVal NombreArea As String)
        Me.IdUsuario = IdUsuario
        Me.IdArea = IdArea
        Me.Estado = Estado
        Me.NombreArea = NombreArea
    End Sub

    Public Property IdUsuario() As Integer
        Get
            Return Me._IdUsuario
        End Get
        Set(ByVal value As Integer)
            Me._IdUsuario = value
        End Set
    End Property
    Public Property IdArea() As Integer
        Get
            Return Me._IdArea
        End Get
        Set(ByVal value As Integer)
            Me._IdArea = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return Me._ua_estado
        End Get
        Set(ByVal value As Boolean)
            Me._ua_estado = value
        End Set
    End Property

    Public ReadOnly Property DescEstado() As String
        Get
            If Estado Then
                Return "Activo"
            Else
                Return "Inactivo"
            End If
        End Get
    End Property

    Public Property NombreArea() As String
        Get
            Return Me._NombreArea
        End Get
        Set(ByVal value As String)
            Me._NombreArea = value
        End Set
    End Property

    Private _ar_NombreCorto As String

    Public Property ar_NombreCorto() As String
        Get
            Return _ar_NombreCorto
        End Get
        Set(ByVal value As String)
            _ar_NombreCorto = value
        End Set
    End Property


End Class
