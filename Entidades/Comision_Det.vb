﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'************  VIERNES 04 06 2010

Public Class Comision_Det
    Private _IdComisionCab As Integer
    Private _IdProducto As Integer
    Private _IdUnidadMedida As Integer
    Private _comd_FechaInicio As Date
    Private _comd_FechaFin As Date
    Private _comd_ValorComision As Decimal
    Private _comd_TipoCalculoCom As String
    Private _IdMoneda As Integer
    Private _comd_PrecioBaseComision As String
    Private _comd_IncluyeIgv As Boolean
    Private _comd_Dcto_Inicio As Decimal
    Private _comd_Dcto_Fin As Decimal
    Private _comd_PrecioBaseDcto As String
    Private _IdEmpresa As Integer
    Private _IdTienda As Integer
    Private _comd_Estado As Boolean
    Private _IdComisionDet As Integer
    Private _ListaMoneda As List(Of Entidades.Moneda)
    Private _Cadena_IdUnidadMedida_Prod As String
    Private _Cadena_UnidadMedida_Prod As String
    Private _CodigoProducto As String
    Private _Producto As String

    Public Property Producto() As String
        Get
            Return Me._Producto
        End Get
        Set(ByVal value As String)
            Me._Producto = value
        End Set
    End Property

    Public Property CodigoProducto() As String
        Get
            Return Me._CodigoProducto
        End Get
        Set(ByVal value As String)
            Me._CodigoProducto = value
        End Set
    End Property

    Public Property ListaMoneda() As List(Of Entidades.Moneda)
        Get
            Return Me._ListaMoneda
        End Get
        Set(ByVal value As List(Of Entidades.Moneda))
            Me._ListaMoneda = value
        End Set
    End Property

    Public Property IdComisionCab() As Integer
        Get
            Return Me._IdComisionCab
        End Get
        Set(ByVal value As Integer)
            Me._IdComisionCab = value
        End Set
    End Property

    Public Property IdProducto() As Integer
        Get
            Return Me._IdProducto
        End Get
        Set(ByVal value As Integer)
            Me._IdProducto = value
        End Set
    End Property

    Public Property IdUnidadMedida() As Integer
        Get
            Return Me._IdUnidadMedida
        End Get
        Set(ByVal value As Integer)
            Me._IdUnidadMedida = value
        End Set
    End Property

    Public Property FechaInicio() As Date
        Get
            Return Me._comd_FechaInicio
        End Get
        Set(ByVal value As Date)
            Me._comd_FechaInicio = value
        End Set
    End Property

    Public Property FechaFin() As Date
        Get
            Return Me._comd_FechaFin
        End Get
        Set(ByVal value As Date)
            Me._comd_FechaFin = value
        End Set
    End Property

    Public Property ValorComision() As Decimal
        Get
            Return Me._comd_ValorComision
        End Get
        Set(ByVal value As Decimal)
            Me._comd_ValorComision = value
        End Set
    End Property

    Public Property TipoCalculoCom() As String
        Get
            Return Me._comd_TipoCalculoCom
        End Get
        Set(ByVal value As String)
            Me._comd_TipoCalculoCom = value
        End Set
    End Property

    Public Property IdMoneda() As Integer
        Get
            Return Me._IdMoneda
        End Get
        Set(ByVal value As Integer)
            Me._IdMoneda = value
        End Set
    End Property

    Public Property PrecioBaseComision() As String
        Get
            Return Me._comd_PrecioBaseComision
        End Get
        Set(ByVal value As String)
            Me._comd_PrecioBaseComision = value
        End Set
    End Property

    Public Property IncluyeIgv() As Boolean
        Get
            Return Me._comd_IncluyeIgv
        End Get
        Set(ByVal value As Boolean)
            Me._comd_IncluyeIgv = value
        End Set
    End Property

    Public Property Dcto_Inicio() As Decimal
        Get
            Return Me._comd_Dcto_Inicio
        End Get
        Set(ByVal value As Decimal)
            Me._comd_Dcto_Inicio = value
        End Set
    End Property

    Public Property Dcto_Fin() As Decimal
        Get
            Return Me._comd_Dcto_Fin
        End Get
        Set(ByVal value As Decimal)
            Me._comd_Dcto_Fin = value
        End Set
    End Property

    Public Property PrecioBaseDcto() As String
        Get
            Return Me._comd_PrecioBaseDcto
        End Get
        Set(ByVal value As String)
            Me._comd_PrecioBaseDcto = value
        End Set
    End Property

    Public Property IdEmpresa() As Integer
        Get
            Return Me._IdEmpresa
        End Get
        Set(ByVal value As Integer)
            Me._IdEmpresa = value
        End Set
    End Property

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return Me._comd_Estado
        End Get
        Set(ByVal value As Boolean)
            Me._comd_Estado = value
        End Set
    End Property

    Public Property IdComisionDet() As Integer
        Get
            Return Me._IdComisionDet
        End Get
        Set(ByVal value As Integer)
            Me._IdComisionDet = value
        End Set
    End Property

    
    Public Property Cadena_IdUnidadMedida_Prod() As String
        Get
            Return Me._Cadena_IdUnidadMedida_Prod
        End Get
        Set(ByVal value As String)
            Me._Cadena_IdUnidadMedida_Prod = value
        End Set
    End Property

    Public Property Cadena_UnidadMedida_Prod() As String
        Get
            Return Me._Cadena_UnidadMedida_Prod
        End Get
        Set(ByVal value As String)
            Me._Cadena_UnidadMedida_Prod = value
        End Set
    End Property

    Public ReadOnly Property getListaUnidadMedida() As List(Of Entidades.UnidadMedida)
        Get

            Dim lista As New List(Of Entidades.UnidadMedida)

            Dim lista_IdUnidadMedida() As String = Cadena_IdUnidadMedida_Prod.Split(",")
            Dim lista_UnidadMedida() As String = Cadena_UnidadMedida_Prod.Split(",")

            For i As Integer = 0 To lista_IdUnidadMedida.Length - 1

                Dim objUnidadMedida As New Entidades.UnidadMedida
                With objUnidadMedida

                    .DescripcionCorto = lista_UnidadMedida(i)
                    .Id = CInt(lista_IdUnidadMedida(i))

                End With
                lista.Add(objUnidadMedida)

            Next

            Return lista

        End Get
    End Property

End Class
