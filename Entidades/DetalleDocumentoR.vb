'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class DetalleDocumentoR

    Private _dcr_TotalDocumento As Decimal
    Private _dcr_Serie As String
    Private _dcr_NumDocumento As String
    Private _dcr_FechaEmision As Date
    Private _dcr_TipoCambio As Decimal
    Private _dcr_Porcentaje As Decimal
    Private _dcr_Importe As Decimal
    Private _dcr_ImporteFinal As Decimal
    Private _IdMonedaDocRef As Integer
    Private _IdDocumentoRef As Integer
    Private _IdDocumento As Integer

    Public Property dcr_TotalDocumento() As Decimal
        Get
            Return Me._dcr_TotalDocumento
        End Get
        Set(ByVal value As Decimal)
            Me._dcr_TotalDocumento = value
        End Set
    End Property

    Public Property dcr_Serie() As String
        Get
            Return Me._dcr_Serie
        End Get
        Set(ByVal value As String)
            Me._dcr_Serie = value
        End Set
    End Property

    Public Property dcr_NumDocumento() As String
        Get
            Return Me._dcr_NumDocumento
        End Get
        Set(ByVal value As String)
            Me._dcr_NumDocumento = value
        End Set
    End Property

    Public Property dcr_FechaEmision() As Date
        Get
            Return Me._dcr_FechaEmision
        End Get
        Set(ByVal value As Date)
            Me._dcr_FechaEmision = value
        End Set
    End Property

    Public Property dcr_TipoCambio() As Decimal
        Get
            Return Me._dcr_TipoCambio
        End Get
        Set(ByVal value As Decimal)
            Me._dcr_TipoCambio = value
        End Set
    End Property

    Public Property dcr_Porcentaje() As Decimal
        Get
            Return Me._dcr_Porcentaje
        End Get
        Set(ByVal value As Decimal)
            Me._dcr_Porcentaje = value
        End Set
    End Property

    Public Property dcr_Importe() As Decimal
        Get
            Return Me._dcr_Importe
        End Get
        Set(ByVal value As Decimal)
            Me._dcr_Importe = value
        End Set
    End Property

    Public Property dcr_ImporteFinal() As Decimal
        Get
            Return Me._dcr_ImporteFinal
        End Get
        Set(ByVal value As Decimal)
            Me._dcr_ImporteFinal = value
        End Set
    End Property

    Public Property IdMonedaDocRef() As Integer
        Get
            Return Me._IdMonedaDocRef
        End Get
        Set(ByVal value As Integer)
            Me._IdMonedaDocRef = value
        End Set
    End Property

    Public Property IdDocumentoRef() As Integer
        Get
            Return Me._IdDocumentoRef
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumentoRef = value
        End Set
    End Property

    Public Property IdDocumento() As Integer
        Get
            Return Me._IdDocumento
        End Get
        Set(ByVal value As Integer)
            Me._IdDocumento = value
        End Set
    End Property

End Class
