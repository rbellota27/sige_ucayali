﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class MovBanco_Cheque

    Private _IdMovBanco As Integer
    Private _IdBanco As Integer
    Private _IdCuentaBancaria As Integer
    Private _IdCheque As Integer
    Private _DB_NAME As String


    Public Sub New()
    End Sub

    Public Sub New(ByVal v_IdMovBanco As Integer, ByVal v_IdBanco As Integer, ByVal v_IdCuentaBancaria As Integer, ByVal v_IdCheque As Integer)
        _IdMovBanco = v_IdMovBanco
        _IdBanco = v_IdBanco
        _IdCuentaBancaria = v_IdCuentaBancaria
        _IdCheque = v_IdCheque
    End Sub

    Public Property IdMovBanco() As Integer
        Get
            Return _IdMovBanco
        End Get
        Set(ByVal value As Integer)
            _IdMovBanco = value
        End Set
    End Property

    Public Property IdBanco() As Integer
        Get
            Return _IdBanco
        End Get
        Set(ByVal value As Integer)
            _IdBanco = value
        End Set
    End Property

    Public Property IdCuentaBancaria() As Integer
        Get
            Return _IdCuentaBancaria
        End Get
        Set(ByVal value As Integer)
            _IdCuentaBancaria = value
        End Set
    End Property

    Public Property IdCheque() As Integer
        Get
            Return _IdCheque
        End Get
        Set(ByVal value As Integer)
            _IdCheque = value
        End Set
    End Property

    Public Property DB_NAME() As String
        Get
            Return _DB_NAME
        End Get
        Set(ByVal value As String)
            _DB_NAME = value
        End Set
    End Property


End Class
