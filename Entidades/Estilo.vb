﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class Estilo
    Private _IdEstilo As Integer
    Private _est_Nombre As String
    Private _est_Abv As String
    Private _est_Estado As String
    Private _descEstado As String
    Private _est_Estadov2 As Boolean
    Private _est_Codigo As String
    Sub New()
    End Sub

    Public Sub New(ByVal Idestilo As Integer, ByVal nombre As String)
        Me.Id = Idestilo
        Me.Descripcion = nombre
    End Sub
    Public Sub New(ByVal Idestilo As Integer, ByVal nombre As String, ByVal estado As Boolean)
        Me._IdEstilo = Idestilo
        Me._est_Nombre = nombre
        Me._est_Estadov2 = estado
    End Sub


    'Public ReadOnly Property DescEstado()
    '    Get
    '        Me._descEstado = "Activo"
    '        If Me._est_Estado = "0" Then
    '            Me._descEstado = "Inactivo"
    '        End If
    '        Return Me._descEstado
    '    End Get
    'End Property
    Public Property Id() As Integer
        Get
            Return Me._IdEstilo
        End Get
        Set(ByVal value As Integer)
            Me._IdEstilo = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._est_Nombre
        End Get
        Set(ByVal value As String)
            Me._est_Nombre = value
        End Set
    End Property

    Public Property Abv() As String
        Get
            Return Me._est_Abv
        End Get
        Set(ByVal value As String)
            Me._est_Abv = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._est_Estado
        End Get
        Set(ByVal value As String)
            Me._est_Estado = value
        End Set
    End Property
    Public ReadOnly Property DescEstado()
        Get
            Me._descEstado = "Activo"
            If Me.strEstado = "0" Then
                Me._descEstado = "Inactivo"
            End If
            Return Me._descEstado
        End Get
    End Property

    Public ReadOnly Property strEstado() As String
        Get
            If Estado = True Then Return "1"
            If Estado = False Then Return "0"
        End Get
    End Property
    Public Property Est_Codigo() As String
        Get
            Return _est_Codigo
        End Get
        Set(ByVal value As String)
            _est_Codigo = value
        End Set
    End Property
    Public Property EstadoBit() As Boolean
        Get
            Return _est_Estadov2
        End Get
        Set(ByVal value As Boolean)
            _est_Estadov2 = value
        End Set
    End Property
End Class
