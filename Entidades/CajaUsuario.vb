﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class CajaUsuario
    Private _IdCaja As Integer
    Private _IdPersona As Integer
    Private _Estado As String
    Private _NomCaja As String
    Private _NomTienda As String
    Private _IdTienda As Integer

    Public Sub New()
    End Sub
    Public Sub New(ByVal idcaja As Integer, ByVal idpersona As Integer, ByVal estado As String)
        Me.IdCaja = idcaja
        Me.IdPersona = idpersona
        Me.Estado = estado
    End Sub
    Public Sub New(ByVal idcaja As Integer, ByVal idpersona As Integer, ByVal estado As String, ByVal idtienda As Integer, ByVal nomcaja As String, ByVal nomtienda As String)
        Me.IdCaja = idcaja
        Me.IdPersona = idpersona
        Me.Estado = estado
        Me.IdTienda = idtienda
        Me.NomCaja = nomcaja
        Me.NomTienda = nomtienda
    End Sub

    Public Property IdTienda() As Integer
        Get
            Return Me._IdTienda
        End Get
        Set(ByVal value As Integer)
            Me._IdTienda = value
        End Set
    End Property
    Public Property NomCaja() As String
        Get
            Return Me._NomCaja
        End Get
        Set(ByVal value As String)
            Me._NomCaja = value
        End Set
    End Property

    Public Property NomTienda() As String
        Get
            Return Me._NomTienda
        End Get
        Set(ByVal value As String)
            Me._NomTienda = value
        End Set
    End Property


    Public Property IdCaja() As Integer
        Get
            Return Me._IdCaja
        End Get
        Set(ByVal value As Integer)
            Me._IdCaja = value
        End Set
    End Property

    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return Me._Estado
        End Get
        Set(ByVal value As String)
            Me._Estado = value
        End Set
    End Property
End Class
