﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

'*************** NAGAMINE MOLINA, JORGE CHRISTIAN    LUNES 18 ENERO 2010 HORA 05_04 PM


'********************************************************  
'Autor      : Chang Carnero, Edgar  
'Módulo     : Concepto  
'Sistema    : Sanicenter  
'Empresa    : Digrafic SRL  
'Modificado : 14-Enero-2010  
'Hora       : 01:00:00 pm  
'********************************************************               
Public Class Concepto_TipoDocumento
    Private _IdConcepto As Integer
    Private _IdTipoDocumento As Integer
    Private _IdMoneda As Integer
    Dim _NomMoneda As Object
    Private _Nom_Simbolo As String
    Private _doc_Numero As String
    Private _Ctdoc_Valor As Decimal
    Private _Ctdoc_Tope As Decimal
    Private _DescTipoDoc As String
    Private _con_TipoCalculo As String
    Private _con_FleteFijo As Boolean
    Private _con_FletexKilo As Boolean
    Private _SelccionarFlete As Object
    Private _fletexkilofijo As String
    Private _cadMoneda As String
    Private _NomConcepto As String
    Private _con_ZonaUbigeo As Boolean

    Public Property con_ZonaUbigeo() As Boolean
        Get
            Return _con_ZonaUbigeo
        End Get
        Set(ByVal value As Boolean)
            _con_ZonaUbigeo = value
        End Set
    End Property

    Public Property IdConcepto() As Integer
        Get
            Return _IdConcepto
        End Get
        Set(ByVal value As Integer)
            _IdConcepto = value
        End Set
    End Property
    Public Property IdTipoDocumento() As Integer
        Get
            Return _IdTipoDocumento
        End Get
        Set(ByVal value As Integer)
            _IdTipoDocumento = value
        End Set
    End Property
    Public Property IdMoneda() As Integer
        Get
            Return _IdMoneda
        End Get
        Set(ByVal value As Integer)
            _IdMoneda = value
        End Set
    End Property
    Public Property NombreMoneda() As Object
        Get
            If cadMoneda <> "" Then
                Dim lista As New List(Of Entidades.Concepto_TipoDocumento)
                Dim cortar() As String = cadMoneda.Split("=")
                For x As Integer = 0 To cortar.Length - 1
                    Dim obj As New Entidades.Concepto_TipoDocumento
                    Dim separar() As String = cortar(x).Split(",")
                    obj.IdMoneda = CInt(separar(0))
                    obj.SimboloMoneda = CStr(separar(1))
                    lista.Add(obj)
                Next
                Return lista
            End If
            Return _NomMoneda
        End Get
        Set(ByVal value As Object)
            _NomMoneda = value
        End Set
    End Property
    Public Property SimboloMoneda() As String
        Get
            Return _Nom_Simbolo
        End Get
        Set(ByVal value As String)
            _Nom_Simbolo = value
        End Set
    End Property
    Public Property CtDoc_Valor() As Decimal
        Get
            Return _Ctdoc_Valor
        End Get
        Set(ByVal value As Decimal)
            _Ctdoc_Valor = value
        End Set
    End Property
    Public Property CtDoc_Tope() As Decimal
        Get
            Return _Ctdoc_Tope
        End Get
        Set(ByVal value As Decimal)
            _Ctdoc_Tope = value
        End Set
    End Property
    Public Property DescTipoDocumento() As String
        Get
            Return _DescTipoDoc
        End Get
        Set(ByVal value As String)
            _DescTipoDoc = value
        End Set
    End Property
    Public Property con_TipoCalculo() As String
        Get
            Return _con_TipoCalculo
        End Get
        Set(ByVal value As String)
            _con_TipoCalculo = value
        End Set
    End Property
    Public Property con_FleteFijo() As Boolean
        Get
            Return _con_FleteFijo
        End Get
        Set(ByVal value As Boolean)
            _con_FleteFijo = value
        End Set
    End Property
    Public Property con_FletexKilo() As Boolean
        Get
            Return _con_FletexKilo
        End Get
        Set(ByVal value As Boolean)
            _con_FletexKilo = value
        End Set
    End Property
    Public Property SeleccionarFlete() As Object
        Get
            Return _SelccionarFlete
        End Get
        Set(ByVal value As Object)
            _SelccionarFlete = value
        End Set
    End Property

    Public Property fletexkilofijo() As String
        Get
            Return _fletexkilofijo
        End Get
        Set(ByVal value As String)
            _fletexkilofijo = value
        End Set
    End Property

    Public Property cadMoneda() As String
        Get
            Return _cadMoneda
        End Get
        Set(ByVal value As String)
            _cadMoneda = value
        End Set
    End Property
    Public Property NomConcepto() As String
        Get
            Return _NomConcepto
        End Get
        Set(ByVal value As String)
            _NomConcepto = value
        End Set
    End Property


End Class
