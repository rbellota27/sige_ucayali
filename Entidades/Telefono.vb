'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Telefono
    Private _IdTelefono As Integer
    Private _tel_Numero As String
    Private _tel_Anexo As String
    Private _tel_Prioridad As Boolean
    Private _IdPersona As Integer
    Private _IdTipoTelefono As Integer
    Public Property Id() As Integer
        Get
            Return Me._IdTelefono
        End Get
        Set(ByVal value As Integer)
            Me._IdTelefono = value
        End Set
    End Property

    Public Property Numero() As String
        Get
            Return Me._tel_Numero
        End Get
        Set(ByVal value As String)
            Me._tel_Numero = value
        End Set
    End Property

    Public Property Anexo() As String
        Get
            Return Me._tel_Anexo
        End Get
        Set(ByVal value As String)
            Me._tel_Anexo = value
        End Set
    End Property

    Public Property Prioridad() As Boolean
        Get
            Return Me._tel_Prioridad
        End Get
        Set(ByVal value As Boolean)
            Me._tel_Prioridad = value
        End Set
    End Property

    Public Property IdPersona() As Integer
        Get
            Return Me._IdPersona
        End Get
        Set(ByVal value As Integer)
            Me._IdPersona = value
        End Set
    End Property

    Public Property IdTipoTelefono() As Integer
        Get
            Return Me._IdTipoTelefono
        End Get
        Set(ByVal value As Integer)
            Me._IdTipoTelefono = value
        End Set
    End Property
End Class
