'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Public Class Chofer
    Private _ch_NLicencia As String
    Private _IdChofer As Integer
    Private _Categoria As String
    Private _Nombre As String
    Private _DNI As String
    Private _esChofer As Boolean
    Private _TotalReg As Integer
    Private _estado As Integer

    Public Property TotalRegEnDocumento() As Integer
        Get
            Return Me._TotalReg
        End Get
        Set(ByVal value As Integer)
            Me._TotalReg = value
        End Set
    End Property

    Public Property esChofer() As Boolean
        Get
            Return _esChofer
        End Get
        Set(ByVal value As Boolean)
            _esChofer = value
        End Set
    End Property

    Public Property Categoria() As String
        Get
            Return Me._Categoria
        End Get
        Set(ByVal value As String)
            Me._Categoria = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._Nombre
        End Get
        Set(ByVal value As String)
            Me._Nombre = value
        End Set
    End Property

    Public Property DNI() As String
        Get
            Return Me._DNI
        End Get
        Set(ByVal value As String)
            Me._DNI = value
        End Set
    End Property

    Public Property Licencia() As String
        Get
            Return Me._ch_NLicencia
        End Get
        Set(ByVal value As String)
            Me._ch_NLicencia = value
        End Set
    End Property

    Public Property Id() As Integer
        Get
            Return Me._IdChofer
        End Get
        Set(ByVal value As Integer)
            Me._IdChofer = value
        End Set
    End Property

    Public Property estado() As Integer
        Get
            Return Me._estado
        End Get
        Set(ByVal value As Integer)
            Me._estado = value
        End Set
    End Property
End Class
